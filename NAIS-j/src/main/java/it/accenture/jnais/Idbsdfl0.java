package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.DForzLiqDao;
import it.accenture.jnais.commons.data.to.IDForzLiq;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.DForzLiqIdbsdfl0;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idbsdfl0Data;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.redefines.DflAaCnbzDal2007Ef;
import it.accenture.jnais.ws.redefines.DflAaCnbzEnd2000Ef;
import it.accenture.jnais.ws.redefines.DflAaCnbzEnd2006Ef;
import it.accenture.jnais.ws.redefines.DflAccpreAccCalc;
import it.accenture.jnais.ws.redefines.DflAccpreAccDfz;
import it.accenture.jnais.ws.redefines.DflAccpreAccEfflq;
import it.accenture.jnais.ws.redefines.DflAccpreSostCalc;
import it.accenture.jnais.ws.redefines.DflAccpreSostDfz;
import it.accenture.jnais.ws.redefines.DflAccpreSostEfflq;
import it.accenture.jnais.ws.redefines.DflAccpreVisCalc;
import it.accenture.jnais.ws.redefines.DflAccpreVisDfz;
import it.accenture.jnais.ws.redefines.DflAccpreVisEfflq;
import it.accenture.jnais.ws.redefines.DflAlqCnbtInpstfmC;
import it.accenture.jnais.ws.redefines.DflAlqCnbtInpstfmD;
import it.accenture.jnais.ws.redefines.DflAlqCnbtInpstfmE;
import it.accenture.jnais.ws.redefines.DflAlqTaxSepCalc;
import it.accenture.jnais.ws.redefines.DflAlqTaxSepDfz;
import it.accenture.jnais.ws.redefines.DflAlqTaxSepEfflq;
import it.accenture.jnais.ws.redefines.DflCnbtInpstfmCalc;
import it.accenture.jnais.ws.redefines.DflCnbtInpstfmDfz;
import it.accenture.jnais.ws.redefines.DflCnbtInpstfmEfflq;
import it.accenture.jnais.ws.redefines.DflCndeDal2007Calc;
import it.accenture.jnais.ws.redefines.DflCndeDal2007Dfz;
import it.accenture.jnais.ws.redefines.DflCndeDal2007Efflq;
import it.accenture.jnais.ws.redefines.DflCndeEnd2000Calc;
import it.accenture.jnais.ws.redefines.DflCndeEnd2000Dfz;
import it.accenture.jnais.ws.redefines.DflCndeEnd2000Efflq;
import it.accenture.jnais.ws.redefines.DflCndeEnd2006Calc;
import it.accenture.jnais.ws.redefines.DflCndeEnd2006Dfz;
import it.accenture.jnais.ws.redefines.DflCndeEnd2006Efflq;
import it.accenture.jnais.ws.redefines.DflIcnbInpstfmCalc;
import it.accenture.jnais.ws.redefines.DflIcnbInpstfmDfz;
import it.accenture.jnais.ws.redefines.DflIcnbInpstfmEfflq;
import it.accenture.jnais.ws.redefines.DflIdMoviChiu;
import it.accenture.jnais.ws.redefines.DflIimpst252Calc;
import it.accenture.jnais.ws.redefines.DflIimpst252Dfz;
import it.accenture.jnais.ws.redefines.DflIimpst252Efflq;
import it.accenture.jnais.ws.redefines.DflIimpstPrvrCalc;
import it.accenture.jnais.ws.redefines.DflIimpstPrvrDfz;
import it.accenture.jnais.ws.redefines.DflIimpstPrvrEfflq;
import it.accenture.jnais.ws.redefines.DflIintPrestCalc;
import it.accenture.jnais.ws.redefines.DflIintPrestDfz;
import it.accenture.jnais.ws.redefines.DflIintPrestEfflq;
import it.accenture.jnais.ws.redefines.DflImpbBolloDettC;
import it.accenture.jnais.ws.redefines.DflImpbBolloDettD;
import it.accenture.jnais.ws.redefines.DflImpbBolloDettL;
import it.accenture.jnais.ws.redefines.DflImpbIs1382011c;
import it.accenture.jnais.ws.redefines.DflImpbIs1382011d;
import it.accenture.jnais.ws.redefines.DflImpbIs1382011l;
import it.accenture.jnais.ws.redefines.DflImpbIs662014c;
import it.accenture.jnais.ws.redefines.DflImpbIs662014d;
import it.accenture.jnais.ws.redefines.DflImpbIs662014l;
import it.accenture.jnais.ws.redefines.DflImpbIsCalc;
import it.accenture.jnais.ws.redefines.DflImpbIsDfz;
import it.accenture.jnais.ws.redefines.DflImpbIsEfflq;
import it.accenture.jnais.ws.redefines.DflImpbRitAccCalc;
import it.accenture.jnais.ws.redefines.DflImpbRitAccDfz;
import it.accenture.jnais.ws.redefines.DflImpbRitAccEfflq;
import it.accenture.jnais.ws.redefines.DflImpbTaxSepCalc;
import it.accenture.jnais.ws.redefines.DflImpbTaxSepDfz;
import it.accenture.jnais.ws.redefines.DflImpbTaxSepEfflq;
import it.accenture.jnais.ws.redefines.DflImpbTfrCalc;
import it.accenture.jnais.ws.redefines.DflImpbTfrDfz;
import it.accenture.jnais.ws.redefines.DflImpbTfrEfflq;
import it.accenture.jnais.ws.redefines.DflImpbVis1382011c;
import it.accenture.jnais.ws.redefines.DflImpbVis1382011d;
import it.accenture.jnais.ws.redefines.DflImpbVis1382011l;
import it.accenture.jnais.ws.redefines.DflImpbVis662014c;
import it.accenture.jnais.ws.redefines.DflImpbVis662014d;
import it.accenture.jnais.ws.redefines.DflImpbVis662014l;
import it.accenture.jnais.ws.redefines.DflImpbVisCalc;
import it.accenture.jnais.ws.redefines.DflImpbVisDfz;
import it.accenture.jnais.ws.redefines.DflImpbVisEfflq;
import it.accenture.jnais.ws.redefines.DflImpExcontrEff;
import it.accenture.jnais.ws.redefines.DflImpIntrRitPagC;
import it.accenture.jnais.ws.redefines.DflImpIntrRitPagD;
import it.accenture.jnais.ws.redefines.DflImpIntrRitPagL;
import it.accenture.jnais.ws.redefines.DflImpLrdCalc;
import it.accenture.jnais.ws.redefines.DflImpLrdDfz;
import it.accenture.jnais.ws.redefines.DflImpLrdEfflq;
import it.accenture.jnais.ws.redefines.DflImpNetCalc;
import it.accenture.jnais.ws.redefines.DflImpNetDfz;
import it.accenture.jnais.ws.redefines.DflImpNetEfflq;
import it.accenture.jnais.ws.redefines.DflImpst252Calc;
import it.accenture.jnais.ws.redefines.DflImpst252Efflq;
import it.accenture.jnais.ws.redefines.DflImpstBolloDettC;
import it.accenture.jnais.ws.redefines.DflImpstBolloDettD;
import it.accenture.jnais.ws.redefines.DflImpstBolloDettL;
import it.accenture.jnais.ws.redefines.DflImpstBolloTotVc;
import it.accenture.jnais.ws.redefines.DflImpstBolloTotVd;
import it.accenture.jnais.ws.redefines.DflImpstBolloTotVl;
import it.accenture.jnais.ws.redefines.DflImpstDaRimbEff;
import it.accenture.jnais.ws.redefines.DflImpstPrvrCalc;
import it.accenture.jnais.ws.redefines.DflImpstPrvrDfz;
import it.accenture.jnais.ws.redefines.DflImpstPrvrEfflq;
import it.accenture.jnais.ws.redefines.DflImpstSostCalc;
import it.accenture.jnais.ws.redefines.DflImpstSostDfz;
import it.accenture.jnais.ws.redefines.DflImpstSostEfflq;
import it.accenture.jnais.ws.redefines.DflImpstVis1382011c;
import it.accenture.jnais.ws.redefines.DflImpstVis1382011d;
import it.accenture.jnais.ws.redefines.DflImpstVis1382011l;
import it.accenture.jnais.ws.redefines.DflImpstVis662014c;
import it.accenture.jnais.ws.redefines.DflImpstVis662014d;
import it.accenture.jnais.ws.redefines.DflImpstVis662014l;
import it.accenture.jnais.ws.redefines.DflImpstVisCalc;
import it.accenture.jnais.ws.redefines.DflImpstVisDfz;
import it.accenture.jnais.ws.redefines.DflImpstVisEfflq;
import it.accenture.jnais.ws.redefines.DflIntrPrestCalc;
import it.accenture.jnais.ws.redefines.DflIntrPrestDfz;
import it.accenture.jnais.ws.redefines.DflIntrPrestEfflq;
import it.accenture.jnais.ws.redefines.DflIs1382011c;
import it.accenture.jnais.ws.redefines.DflIs1382011d;
import it.accenture.jnais.ws.redefines.DflIs1382011l;
import it.accenture.jnais.ws.redefines.DflIs662014c;
import it.accenture.jnais.ws.redefines.DflIs662014d;
import it.accenture.jnais.ws.redefines.DflIs662014l;
import it.accenture.jnais.ws.redefines.DflMmCnbzDal2007Ef;
import it.accenture.jnais.ws.redefines.DflMmCnbzEnd2000Ef;
import it.accenture.jnais.ws.redefines.DflMmCnbzEnd2006Ef;
import it.accenture.jnais.ws.redefines.DflMontDal2007Calc;
import it.accenture.jnais.ws.redefines.DflMontDal2007Dfz;
import it.accenture.jnais.ws.redefines.DflMontDal2007Efflq;
import it.accenture.jnais.ws.redefines.DflMontEnd2000Calc;
import it.accenture.jnais.ws.redefines.DflMontEnd2000Dfz;
import it.accenture.jnais.ws.redefines.DflMontEnd2000Efflq;
import it.accenture.jnais.ws.redefines.DflMontEnd2006Calc;
import it.accenture.jnais.ws.redefines.DflMontEnd2006Dfz;
import it.accenture.jnais.ws.redefines.DflMontEnd2006Efflq;
import it.accenture.jnais.ws.redefines.DflResPreAttCalc;
import it.accenture.jnais.ws.redefines.DflResPreAttDfz;
import it.accenture.jnais.ws.redefines.DflResPreAttEfflq;
import it.accenture.jnais.ws.redefines.DflResPrstzCalc;
import it.accenture.jnais.ws.redefines.DflResPrstzDfz;
import it.accenture.jnais.ws.redefines.DflResPrstzEfflq;
import it.accenture.jnais.ws.redefines.DflRitAccCalc;
import it.accenture.jnais.ws.redefines.DflRitAccDfz;
import it.accenture.jnais.ws.redefines.DflRitAccEfflq;
import it.accenture.jnais.ws.redefines.DflRitIrpefCalc;
import it.accenture.jnais.ws.redefines.DflRitIrpefDfz;
import it.accenture.jnais.ws.redefines.DflRitIrpefEfflq;
import it.accenture.jnais.ws.redefines.DflRitTfrCalc;
import it.accenture.jnais.ws.redefines.DflRitTfrDfz;
import it.accenture.jnais.ws.redefines.DflRitTfrEfflq;
import it.accenture.jnais.ws.redefines.DflTaxSepCalc;
import it.accenture.jnais.ws.redefines.DflTaxSepDfz;
import it.accenture.jnais.ws.redefines.DflTaxSepEfflq;

/**Original name: IDBSDFL0<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  02 LUG 2014.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Idbsdfl0 extends Program implements IDForzLiq {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private DForzLiqDao dForzLiqDao = new DForzLiqDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Idbsdfl0Data ws = new Idbsdfl0Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: D-FORZ-LIQ
    private DForzLiqIdbsdfl0 dForzLiq;

    //==== METHODS ====
    /**Original name: PROGRAM_IDBSDFL0_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, DForzLiqIdbsdfl0 dForzLiq) {
        this.idsv0003 = idsv0003;
        this.dForzLiq = dForzLiq;
        // COB_CODE: PERFORM A000-INIZIO                    THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO          THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS          THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO          THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                        a300ElaboraIdEff();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                        a400ElaboraIdpEff();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO          THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS          THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO          THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                        b300ElaboraIdCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                        b400ElaboraIdpCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                        b500ElaboraIboCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                        b600ElaboraIbsCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                        b700ElaboraIdoCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //              SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-PRIMARY-KEY
                //                 PERFORM A200-ELABORA-PK          THRU A200-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO         THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS         THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO         THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.PRIMARY_KEY:// COB_CODE: PERFORM A200-ELABORA-PK          THRU A200-EX
                        a200ElaboraPk();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO         THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS         THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO         THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Idbsdfl0 getInstance() {
        return ((Idbsdfl0)Programs.getInstance(Idbsdfl0.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'IDBSDFL0'   TO IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("IDBSDFL0");
        // COB_CODE: MOVE 'D_FORZ_LIQ' TO IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("D_FORZ_LIQ");
        // COB_CODE: MOVE '00'                     TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                   TO   IDSV0003-SQLCODE
        //                                              IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                              IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-AGG-STORICO-SOLO-INS  OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isAggStoricoSoloIns() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-PK<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a200ElaboraPk() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-PK          THRU A210-EX
        //              WHEN IDSV0003-INSERT
        //                 PERFORM A220-INSERT-PK          THRU A220-EX
        //              WHEN IDSV0003-UPDATE
        //                 PERFORM A230-UPDATE-PK          THRU A230-EX
        //              WHEN IDSV0003-DELETE
        //                 PERFORM A240-DELETE-PK          THRU A240-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-PK          THRU A210-EX
            a210SelectPk();
        }
        else if (idsv0003.getOperazione().isInsert()) {
            // COB_CODE: PERFORM A220-INSERT-PK          THRU A220-EX
            a220InsertPk();
        }
        else if (idsv0003.getOperazione().isUpdate()) {
            // COB_CODE: PERFORM A230-UPDATE-PK          THRU A230-EX
            a230UpdatePk();
        }
        else if (idsv0003.getOperazione().isDelete()) {
            // COB_CODE: PERFORM A240-DELETE-PK          THRU A240-EX
            a240DeletePk();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A300-ELABORA-ID-EFF<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void a300ElaboraIdEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A310-SELECT-ID-EFF          THRU A310-EX
            a310SelectIdEff();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A400-ELABORA-IDP-EFF<br>*/
    private void a400ElaboraIdpEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
            a410SelectIdpEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
            a460OpenCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
            a470CloseCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
            a480FetchFirstIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
            a490FetchNextIdpEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A500-ELABORA-IBO<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a500ElaboraIbo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A510-SELECT-IBO             THRU A510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A510-SELECT-IBO             THRU A510-EX
            a510SelectIbo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
            a560OpenCursorIbo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
            a570CloseCursorIbo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
            a580FetchFirstIbo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
            a590FetchNextIbo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A600-ELABORA-IBS<br>*/
    private void a600ElaboraIbs() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A610-SELECT-IBS             THRU A610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A610-SELECT-IBS             THRU A610-EX
            a610SelectIbs();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
            a660OpenCursorIbs();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
            a670CloseCursorIbs();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
            a680FetchFirstIbs();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
            a690FetchNextIbs();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A700-ELABORA-IDO<br>*/
    private void a700ElaboraIdo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A710-SELECT-IDO                 THRU A710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A710-SELECT-IDO                 THRU A710-EX
            a710SelectIdo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
            a760OpenCursorIdo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
            a770CloseCursorIdo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
            a780FetchFirstIdo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
            a790FetchNextIdo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B300-ELABORA-ID-CPZ<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void b300ElaboraIdCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
            b310SelectIdCpz();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B400-ELABORA-IDP-CPZ<br>*/
    private void b400ElaboraIdpCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
            b410SelectIdpCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
            b460OpenCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
            b470CloseCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
            b480FetchFirstIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
            b490FetchNextIdpCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B500-ELABORA-IBO-CPZ<br>*/
    private void b500ElaboraIboCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
            b510SelectIboCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
            b560OpenCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
            b570CloseCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
            b580FetchFirstIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B600-ELABORA-IBS-CPZ<br>*/
    private void b600ElaboraIbsCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
            b610SelectIbsCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
            b660OpenCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
            b670CloseCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
            b680FetchFirstIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B700-ELABORA-IDO-CPZ<br>*/
    private void b700ElaboraIdoCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
            b710SelectIdoCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
            b760OpenCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
            b770CloseCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
            b780FetchFirstIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A210-SELECT-PK<br>
	 * <pre>----
	 * ----  gestione PK
	 * ----</pre>*/
    private void a210SelectPk() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_D_FORZ_LIQ
        //                ,ID_LIQ
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,COD_COMP_ANIA
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,IMP_LRD_CALC
        //                ,IMP_LRD_DFZ
        //                ,IMP_LRD_EFFLQ
        //                ,IMP_NET_CALC
        //                ,IMP_NET_DFZ
        //                ,IMP_NET_EFFLQ
        //                ,IMPST_PRVR_CALC
        //                ,IMPST_PRVR_DFZ
        //                ,IMPST_PRVR_EFFLQ
        //                ,IMPST_VIS_CALC
        //                ,IMPST_VIS_DFZ
        //                ,IMPST_VIS_EFFLQ
        //                ,RIT_ACC_CALC
        //                ,RIT_ACC_DFZ
        //                ,RIT_ACC_EFFLQ
        //                ,RIT_IRPEF_CALC
        //                ,RIT_IRPEF_DFZ
        //                ,RIT_IRPEF_EFFLQ
        //                ,IMPST_SOST_CALC
        //                ,IMPST_SOST_DFZ
        //                ,IMPST_SOST_EFFLQ
        //                ,TAX_SEP_CALC
        //                ,TAX_SEP_DFZ
        //                ,TAX_SEP_EFFLQ
        //                ,INTR_PREST_CALC
        //                ,INTR_PREST_DFZ
        //                ,INTR_PREST_EFFLQ
        //                ,ACCPRE_SOST_CALC
        //                ,ACCPRE_SOST_DFZ
        //                ,ACCPRE_SOST_EFFLQ
        //                ,ACCPRE_VIS_CALC
        //                ,ACCPRE_VIS_DFZ
        //                ,ACCPRE_VIS_EFFLQ
        //                ,ACCPRE_ACC_CALC
        //                ,ACCPRE_ACC_DFZ
        //                ,ACCPRE_ACC_EFFLQ
        //                ,RES_PRSTZ_CALC
        //                ,RES_PRSTZ_DFZ
        //                ,RES_PRSTZ_EFFLQ
        //                ,RES_PRE_ATT_CALC
        //                ,RES_PRE_ATT_DFZ
        //                ,RES_PRE_ATT_EFFLQ
        //                ,IMP_EXCONTR_EFF
        //                ,IMPB_VIS_CALC
        //                ,IMPB_VIS_EFFLQ
        //                ,IMPB_VIS_DFZ
        //                ,IMPB_RIT_ACC_CALC
        //                ,IMPB_RIT_ACC_EFFLQ
        //                ,IMPB_RIT_ACC_DFZ
        //                ,IMPB_TFR_CALC
        //                ,IMPB_TFR_EFFLQ
        //                ,IMPB_TFR_DFZ
        //                ,IMPB_IS_CALC
        //                ,IMPB_IS_EFFLQ
        //                ,IMPB_IS_DFZ
        //                ,IMPB_TAX_SEP_CALC
        //                ,IMPB_TAX_SEP_EFFLQ
        //                ,IMPB_TAX_SEP_DFZ
        //                ,IINT_PREST_CALC
        //                ,IINT_PREST_EFFLQ
        //                ,IINT_PREST_DFZ
        //                ,MONT_END2000_CALC
        //                ,MONT_END2000_EFFLQ
        //                ,MONT_END2000_DFZ
        //                ,MONT_END2006_CALC
        //                ,MONT_END2006_EFFLQ
        //                ,MONT_END2006_DFZ
        //                ,MONT_DAL2007_CALC
        //                ,MONT_DAL2007_EFFLQ
        //                ,MONT_DAL2007_DFZ
        //                ,IIMPST_PRVR_CALC
        //                ,IIMPST_PRVR_EFFLQ
        //                ,IIMPST_PRVR_DFZ
        //                ,IIMPST_252_CALC
        //                ,IIMPST_252_EFFLQ
        //                ,IIMPST_252_DFZ
        //                ,IMPST_252_CALC
        //                ,IMPST_252_EFFLQ
        //                ,RIT_TFR_CALC
        //                ,RIT_TFR_EFFLQ
        //                ,RIT_TFR_DFZ
        //                ,CNBT_INPSTFM_CALC
        //                ,CNBT_INPSTFM_EFFLQ
        //                ,CNBT_INPSTFM_DFZ
        //                ,ICNB_INPSTFM_CALC
        //                ,ICNB_INPSTFM_EFFLQ
        //                ,ICNB_INPSTFM_DFZ
        //                ,CNDE_END2000_CALC
        //                ,CNDE_END2000_EFFLQ
        //                ,CNDE_END2000_DFZ
        //                ,CNDE_END2006_CALC
        //                ,CNDE_END2006_EFFLQ
        //                ,CNDE_END2006_DFZ
        //                ,CNDE_DAL2007_CALC
        //                ,CNDE_DAL2007_EFFLQ
        //                ,CNDE_DAL2007_DFZ
        //                ,AA_CNBZ_END2000_EF
        //                ,AA_CNBZ_END2006_EF
        //                ,AA_CNBZ_DAL2007_EF
        //                ,MM_CNBZ_END2000_EF
        //                ,MM_CNBZ_END2006_EF
        //                ,MM_CNBZ_DAL2007_EF
        //                ,IMPST_DA_RIMB_EFF
        //                ,ALQ_TAX_SEP_CALC
        //                ,ALQ_TAX_SEP_EFFLQ
        //                ,ALQ_TAX_SEP_DFZ
        //                ,ALQ_CNBT_INPSTFM_C
        //                ,ALQ_CNBT_INPSTFM_E
        //                ,ALQ_CNBT_INPSTFM_D
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,IMPB_VIS_1382011C
        //                ,IMPB_VIS_1382011D
        //                ,IMPB_VIS_1382011L
        //                ,IMPST_VIS_1382011C
        //                ,IMPST_VIS_1382011D
        //                ,IMPST_VIS_1382011L
        //                ,IMPB_IS_1382011C
        //                ,IMPB_IS_1382011D
        //                ,IMPB_IS_1382011L
        //                ,IS_1382011C
        //                ,IS_1382011D
        //                ,IS_1382011L
        //                ,IMP_INTR_RIT_PAG_C
        //                ,IMP_INTR_RIT_PAG_D
        //                ,IMP_INTR_RIT_PAG_L
        //                ,IMPB_BOLLO_DETT_C
        //                ,IMPB_BOLLO_DETT_D
        //                ,IMPB_BOLLO_DETT_L
        //                ,IMPST_BOLLO_DETT_C
        //                ,IMPST_BOLLO_DETT_D
        //                ,IMPST_BOLLO_DETT_L
        //                ,IMPST_BOLLO_TOT_VC
        //                ,IMPST_BOLLO_TOT_VD
        //                ,IMPST_BOLLO_TOT_VL
        //                ,IMPB_VIS_662014C
        //                ,IMPB_VIS_662014D
        //                ,IMPB_VIS_662014L
        //                ,IMPST_VIS_662014C
        //                ,IMPST_VIS_662014D
        //                ,IMPST_VIS_662014L
        //                ,IMPB_IS_662014C
        //                ,IMPB_IS_662014D
        //                ,IMPB_IS_662014L
        //                ,IS_662014C
        //                ,IS_662014D
        //                ,IS_662014L
        //             INTO
        //                :DFL-ID-D-FORZ-LIQ
        //               ,:DFL-ID-LIQ
        //               ,:DFL-ID-MOVI-CRZ
        //               ,:DFL-ID-MOVI-CHIU
        //                :IND-DFL-ID-MOVI-CHIU
        //               ,:DFL-COD-COMP-ANIA
        //               ,:DFL-DT-INI-EFF-DB
        //               ,:DFL-DT-END-EFF-DB
        //               ,:DFL-IMP-LRD-CALC
        //                :IND-DFL-IMP-LRD-CALC
        //               ,:DFL-IMP-LRD-DFZ
        //                :IND-DFL-IMP-LRD-DFZ
        //               ,:DFL-IMP-LRD-EFFLQ
        //                :IND-DFL-IMP-LRD-EFFLQ
        //               ,:DFL-IMP-NET-CALC
        //                :IND-DFL-IMP-NET-CALC
        //               ,:DFL-IMP-NET-DFZ
        //                :IND-DFL-IMP-NET-DFZ
        //               ,:DFL-IMP-NET-EFFLQ
        //                :IND-DFL-IMP-NET-EFFLQ
        //               ,:DFL-IMPST-PRVR-CALC
        //                :IND-DFL-IMPST-PRVR-CALC
        //               ,:DFL-IMPST-PRVR-DFZ
        //                :IND-DFL-IMPST-PRVR-DFZ
        //               ,:DFL-IMPST-PRVR-EFFLQ
        //                :IND-DFL-IMPST-PRVR-EFFLQ
        //               ,:DFL-IMPST-VIS-CALC
        //                :IND-DFL-IMPST-VIS-CALC
        //               ,:DFL-IMPST-VIS-DFZ
        //                :IND-DFL-IMPST-VIS-DFZ
        //               ,:DFL-IMPST-VIS-EFFLQ
        //                :IND-DFL-IMPST-VIS-EFFLQ
        //               ,:DFL-RIT-ACC-CALC
        //                :IND-DFL-RIT-ACC-CALC
        //               ,:DFL-RIT-ACC-DFZ
        //                :IND-DFL-RIT-ACC-DFZ
        //               ,:DFL-RIT-ACC-EFFLQ
        //                :IND-DFL-RIT-ACC-EFFLQ
        //               ,:DFL-RIT-IRPEF-CALC
        //                :IND-DFL-RIT-IRPEF-CALC
        //               ,:DFL-RIT-IRPEF-DFZ
        //                :IND-DFL-RIT-IRPEF-DFZ
        //               ,:DFL-RIT-IRPEF-EFFLQ
        //                :IND-DFL-RIT-IRPEF-EFFLQ
        //               ,:DFL-IMPST-SOST-CALC
        //                :IND-DFL-IMPST-SOST-CALC
        //               ,:DFL-IMPST-SOST-DFZ
        //                :IND-DFL-IMPST-SOST-DFZ
        //               ,:DFL-IMPST-SOST-EFFLQ
        //                :IND-DFL-IMPST-SOST-EFFLQ
        //               ,:DFL-TAX-SEP-CALC
        //                :IND-DFL-TAX-SEP-CALC
        //               ,:DFL-TAX-SEP-DFZ
        //                :IND-DFL-TAX-SEP-DFZ
        //               ,:DFL-TAX-SEP-EFFLQ
        //                :IND-DFL-TAX-SEP-EFFLQ
        //               ,:DFL-INTR-PREST-CALC
        //                :IND-DFL-INTR-PREST-CALC
        //               ,:DFL-INTR-PREST-DFZ
        //                :IND-DFL-INTR-PREST-DFZ
        //               ,:DFL-INTR-PREST-EFFLQ
        //                :IND-DFL-INTR-PREST-EFFLQ
        //               ,:DFL-ACCPRE-SOST-CALC
        //                :IND-DFL-ACCPRE-SOST-CALC
        //               ,:DFL-ACCPRE-SOST-DFZ
        //                :IND-DFL-ACCPRE-SOST-DFZ
        //               ,:DFL-ACCPRE-SOST-EFFLQ
        //                :IND-DFL-ACCPRE-SOST-EFFLQ
        //               ,:DFL-ACCPRE-VIS-CALC
        //                :IND-DFL-ACCPRE-VIS-CALC
        //               ,:DFL-ACCPRE-VIS-DFZ
        //                :IND-DFL-ACCPRE-VIS-DFZ
        //               ,:DFL-ACCPRE-VIS-EFFLQ
        //                :IND-DFL-ACCPRE-VIS-EFFLQ
        //               ,:DFL-ACCPRE-ACC-CALC
        //                :IND-DFL-ACCPRE-ACC-CALC
        //               ,:DFL-ACCPRE-ACC-DFZ
        //                :IND-DFL-ACCPRE-ACC-DFZ
        //               ,:DFL-ACCPRE-ACC-EFFLQ
        //                :IND-DFL-ACCPRE-ACC-EFFLQ
        //               ,:DFL-RES-PRSTZ-CALC
        //                :IND-DFL-RES-PRSTZ-CALC
        //               ,:DFL-RES-PRSTZ-DFZ
        //                :IND-DFL-RES-PRSTZ-DFZ
        //               ,:DFL-RES-PRSTZ-EFFLQ
        //                :IND-DFL-RES-PRSTZ-EFFLQ
        //               ,:DFL-RES-PRE-ATT-CALC
        //                :IND-DFL-RES-PRE-ATT-CALC
        //               ,:DFL-RES-PRE-ATT-DFZ
        //                :IND-DFL-RES-PRE-ATT-DFZ
        //               ,:DFL-RES-PRE-ATT-EFFLQ
        //                :IND-DFL-RES-PRE-ATT-EFFLQ
        //               ,:DFL-IMP-EXCONTR-EFF
        //                :IND-DFL-IMP-EXCONTR-EFF
        //               ,:DFL-IMPB-VIS-CALC
        //                :IND-DFL-IMPB-VIS-CALC
        //               ,:DFL-IMPB-VIS-EFFLQ
        //                :IND-DFL-IMPB-VIS-EFFLQ
        //               ,:DFL-IMPB-VIS-DFZ
        //                :IND-DFL-IMPB-VIS-DFZ
        //               ,:DFL-IMPB-RIT-ACC-CALC
        //                :IND-DFL-IMPB-RIT-ACC-CALC
        //               ,:DFL-IMPB-RIT-ACC-EFFLQ
        //                :IND-DFL-IMPB-RIT-ACC-EFFLQ
        //               ,:DFL-IMPB-RIT-ACC-DFZ
        //                :IND-DFL-IMPB-RIT-ACC-DFZ
        //               ,:DFL-IMPB-TFR-CALC
        //                :IND-DFL-IMPB-TFR-CALC
        //               ,:DFL-IMPB-TFR-EFFLQ
        //                :IND-DFL-IMPB-TFR-EFFLQ
        //               ,:DFL-IMPB-TFR-DFZ
        //                :IND-DFL-IMPB-TFR-DFZ
        //               ,:DFL-IMPB-IS-CALC
        //                :IND-DFL-IMPB-IS-CALC
        //               ,:DFL-IMPB-IS-EFFLQ
        //                :IND-DFL-IMPB-IS-EFFLQ
        //               ,:DFL-IMPB-IS-DFZ
        //                :IND-DFL-IMPB-IS-DFZ
        //               ,:DFL-IMPB-TAX-SEP-CALC
        //                :IND-DFL-IMPB-TAX-SEP-CALC
        //               ,:DFL-IMPB-TAX-SEP-EFFLQ
        //                :IND-DFL-IMPB-TAX-SEP-EFFLQ
        //               ,:DFL-IMPB-TAX-SEP-DFZ
        //                :IND-DFL-IMPB-TAX-SEP-DFZ
        //               ,:DFL-IINT-PREST-CALC
        //                :IND-DFL-IINT-PREST-CALC
        //               ,:DFL-IINT-PREST-EFFLQ
        //                :IND-DFL-IINT-PREST-EFFLQ
        //               ,:DFL-IINT-PREST-DFZ
        //                :IND-DFL-IINT-PREST-DFZ
        //               ,:DFL-MONT-END2000-CALC
        //                :IND-DFL-MONT-END2000-CALC
        //               ,:DFL-MONT-END2000-EFFLQ
        //                :IND-DFL-MONT-END2000-EFFLQ
        //               ,:DFL-MONT-END2000-DFZ
        //                :IND-DFL-MONT-END2000-DFZ
        //               ,:DFL-MONT-END2006-CALC
        //                :IND-DFL-MONT-END2006-CALC
        //               ,:DFL-MONT-END2006-EFFLQ
        //                :IND-DFL-MONT-END2006-EFFLQ
        //               ,:DFL-MONT-END2006-DFZ
        //                :IND-DFL-MONT-END2006-DFZ
        //               ,:DFL-MONT-DAL2007-CALC
        //                :IND-DFL-MONT-DAL2007-CALC
        //               ,:DFL-MONT-DAL2007-EFFLQ
        //                :IND-DFL-MONT-DAL2007-EFFLQ
        //               ,:DFL-MONT-DAL2007-DFZ
        //                :IND-DFL-MONT-DAL2007-DFZ
        //               ,:DFL-IIMPST-PRVR-CALC
        //                :IND-DFL-IIMPST-PRVR-CALC
        //               ,:DFL-IIMPST-PRVR-EFFLQ
        //                :IND-DFL-IIMPST-PRVR-EFFLQ
        //               ,:DFL-IIMPST-PRVR-DFZ
        //                :IND-DFL-IIMPST-PRVR-DFZ
        //               ,:DFL-IIMPST-252-CALC
        //                :IND-DFL-IIMPST-252-CALC
        //               ,:DFL-IIMPST-252-EFFLQ
        //                :IND-DFL-IIMPST-252-EFFLQ
        //               ,:DFL-IIMPST-252-DFZ
        //                :IND-DFL-IIMPST-252-DFZ
        //               ,:DFL-IMPST-252-CALC
        //                :IND-DFL-IMPST-252-CALC
        //               ,:DFL-IMPST-252-EFFLQ
        //                :IND-DFL-IMPST-252-EFFLQ
        //               ,:DFL-RIT-TFR-CALC
        //                :IND-DFL-RIT-TFR-CALC
        //               ,:DFL-RIT-TFR-EFFLQ
        //                :IND-DFL-RIT-TFR-EFFLQ
        //               ,:DFL-RIT-TFR-DFZ
        //                :IND-DFL-RIT-TFR-DFZ
        //               ,:DFL-CNBT-INPSTFM-CALC
        //                :IND-DFL-CNBT-INPSTFM-CALC
        //               ,:DFL-CNBT-INPSTFM-EFFLQ
        //                :IND-DFL-CNBT-INPSTFM-EFFLQ
        //               ,:DFL-CNBT-INPSTFM-DFZ
        //                :IND-DFL-CNBT-INPSTFM-DFZ
        //               ,:DFL-ICNB-INPSTFM-CALC
        //                :IND-DFL-ICNB-INPSTFM-CALC
        //               ,:DFL-ICNB-INPSTFM-EFFLQ
        //                :IND-DFL-ICNB-INPSTFM-EFFLQ
        //               ,:DFL-ICNB-INPSTFM-DFZ
        //                :IND-DFL-ICNB-INPSTFM-DFZ
        //               ,:DFL-CNDE-END2000-CALC
        //                :IND-DFL-CNDE-END2000-CALC
        //               ,:DFL-CNDE-END2000-EFFLQ
        //                :IND-DFL-CNDE-END2000-EFFLQ
        //               ,:DFL-CNDE-END2000-DFZ
        //                :IND-DFL-CNDE-END2000-DFZ
        //               ,:DFL-CNDE-END2006-CALC
        //                :IND-DFL-CNDE-END2006-CALC
        //               ,:DFL-CNDE-END2006-EFFLQ
        //                :IND-DFL-CNDE-END2006-EFFLQ
        //               ,:DFL-CNDE-END2006-DFZ
        //                :IND-DFL-CNDE-END2006-DFZ
        //               ,:DFL-CNDE-DAL2007-CALC
        //                :IND-DFL-CNDE-DAL2007-CALC
        //               ,:DFL-CNDE-DAL2007-EFFLQ
        //                :IND-DFL-CNDE-DAL2007-EFFLQ
        //               ,:DFL-CNDE-DAL2007-DFZ
        //                :IND-DFL-CNDE-DAL2007-DFZ
        //               ,:DFL-AA-CNBZ-END2000-EF
        //                :IND-DFL-AA-CNBZ-END2000-EF
        //               ,:DFL-AA-CNBZ-END2006-EF
        //                :IND-DFL-AA-CNBZ-END2006-EF
        //               ,:DFL-AA-CNBZ-DAL2007-EF
        //                :IND-DFL-AA-CNBZ-DAL2007-EF
        //               ,:DFL-MM-CNBZ-END2000-EF
        //                :IND-DFL-MM-CNBZ-END2000-EF
        //               ,:DFL-MM-CNBZ-END2006-EF
        //                :IND-DFL-MM-CNBZ-END2006-EF
        //               ,:DFL-MM-CNBZ-DAL2007-EF
        //                :IND-DFL-MM-CNBZ-DAL2007-EF
        //               ,:DFL-IMPST-DA-RIMB-EFF
        //                :IND-DFL-IMPST-DA-RIMB-EFF
        //               ,:DFL-ALQ-TAX-SEP-CALC
        //                :IND-DFL-ALQ-TAX-SEP-CALC
        //               ,:DFL-ALQ-TAX-SEP-EFFLQ
        //                :IND-DFL-ALQ-TAX-SEP-EFFLQ
        //               ,:DFL-ALQ-TAX-SEP-DFZ
        //                :IND-DFL-ALQ-TAX-SEP-DFZ
        //               ,:DFL-ALQ-CNBT-INPSTFM-C
        //                :IND-DFL-ALQ-CNBT-INPSTFM-C
        //               ,:DFL-ALQ-CNBT-INPSTFM-E
        //                :IND-DFL-ALQ-CNBT-INPSTFM-E
        //               ,:DFL-ALQ-CNBT-INPSTFM-D
        //                :IND-DFL-ALQ-CNBT-INPSTFM-D
        //               ,:DFL-DS-RIGA
        //               ,:DFL-DS-OPER-SQL
        //               ,:DFL-DS-VER
        //               ,:DFL-DS-TS-INI-CPTZ
        //               ,:DFL-DS-TS-END-CPTZ
        //               ,:DFL-DS-UTENTE
        //               ,:DFL-DS-STATO-ELAB
        //               ,:DFL-IMPB-VIS-1382011C
        //                :IND-DFL-IMPB-VIS-1382011C
        //               ,:DFL-IMPB-VIS-1382011D
        //                :IND-DFL-IMPB-VIS-1382011D
        //               ,:DFL-IMPB-VIS-1382011L
        //                :IND-DFL-IMPB-VIS-1382011L
        //               ,:DFL-IMPST-VIS-1382011C
        //                :IND-DFL-IMPST-VIS-1382011C
        //               ,:DFL-IMPST-VIS-1382011D
        //                :IND-DFL-IMPST-VIS-1382011D
        //               ,:DFL-IMPST-VIS-1382011L
        //                :IND-DFL-IMPST-VIS-1382011L
        //               ,:DFL-IMPB-IS-1382011C
        //                :IND-DFL-IMPB-IS-1382011C
        //               ,:DFL-IMPB-IS-1382011D
        //                :IND-DFL-IMPB-IS-1382011D
        //               ,:DFL-IMPB-IS-1382011L
        //                :IND-DFL-IMPB-IS-1382011L
        //               ,:DFL-IS-1382011C
        //                :IND-DFL-IS-1382011C
        //               ,:DFL-IS-1382011D
        //                :IND-DFL-IS-1382011D
        //               ,:DFL-IS-1382011L
        //                :IND-DFL-IS-1382011L
        //               ,:DFL-IMP-INTR-RIT-PAG-C
        //                :IND-DFL-IMP-INTR-RIT-PAG-C
        //               ,:DFL-IMP-INTR-RIT-PAG-D
        //                :IND-DFL-IMP-INTR-RIT-PAG-D
        //               ,:DFL-IMP-INTR-RIT-PAG-L
        //                :IND-DFL-IMP-INTR-RIT-PAG-L
        //               ,:DFL-IMPB-BOLLO-DETT-C
        //                :IND-DFL-IMPB-BOLLO-DETT-C
        //               ,:DFL-IMPB-BOLLO-DETT-D
        //                :IND-DFL-IMPB-BOLLO-DETT-D
        //               ,:DFL-IMPB-BOLLO-DETT-L
        //                :IND-DFL-IMPB-BOLLO-DETT-L
        //               ,:DFL-IMPST-BOLLO-DETT-C
        //                :IND-DFL-IMPST-BOLLO-DETT-C
        //               ,:DFL-IMPST-BOLLO-DETT-D
        //                :IND-DFL-IMPST-BOLLO-DETT-D
        //               ,:DFL-IMPST-BOLLO-DETT-L
        //                :IND-DFL-IMPST-BOLLO-DETT-L
        //               ,:DFL-IMPST-BOLLO-TOT-VC
        //                :IND-DFL-IMPST-BOLLO-TOT-VC
        //               ,:DFL-IMPST-BOLLO-TOT-VD
        //                :IND-DFL-IMPST-BOLLO-TOT-VD
        //               ,:DFL-IMPST-BOLLO-TOT-VL
        //                :IND-DFL-IMPST-BOLLO-TOT-VL
        //               ,:DFL-IMPB-VIS-662014C
        //                :IND-DFL-IMPB-VIS-662014C
        //               ,:DFL-IMPB-VIS-662014D
        //                :IND-DFL-IMPB-VIS-662014D
        //               ,:DFL-IMPB-VIS-662014L
        //                :IND-DFL-IMPB-VIS-662014L
        //               ,:DFL-IMPST-VIS-662014C
        //                :IND-DFL-IMPST-VIS-662014C
        //               ,:DFL-IMPST-VIS-662014D
        //                :IND-DFL-IMPST-VIS-662014D
        //               ,:DFL-IMPST-VIS-662014L
        //                :IND-DFL-IMPST-VIS-662014L
        //               ,:DFL-IMPB-IS-662014C
        //                :IND-DFL-IMPB-IS-662014C
        //               ,:DFL-IMPB-IS-662014D
        //                :IND-DFL-IMPB-IS-662014D
        //               ,:DFL-IMPB-IS-662014L
        //                :IND-DFL-IMPB-IS-662014L
        //               ,:DFL-IS-662014C
        //                :IND-DFL-IS-662014C
        //               ,:DFL-IS-662014D
        //                :IND-DFL-IS-662014D
        //               ,:DFL-IS-662014L
        //                :IND-DFL-IS-662014L
        //             FROM D_FORZ_LIQ
        //             WHERE     DS_RIGA = :DFL-DS-RIGA
        //           END-EXEC.
        dForzLiqDao.selectByDflDsRiga(dForzLiq.getDflDsRiga(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A220-INSERT-PK<br>*/
    private void a220InsertPk() {
        // COB_CODE: PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.
        z400SeqRiga();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX
            z150ValorizzaDataServicesI();
            // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX
            z200SetIndicatoriNull();
            // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX
            z900ConvertiNToX();
            // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX
            z960LengthVchar();
            // COB_CODE: EXEC SQL
            //              INSERT
            //              INTO D_FORZ_LIQ
            //                  (
            //                     ID_D_FORZ_LIQ
            //                    ,ID_LIQ
            //                    ,ID_MOVI_CRZ
            //                    ,ID_MOVI_CHIU
            //                    ,COD_COMP_ANIA
            //                    ,DT_INI_EFF
            //                    ,DT_END_EFF
            //                    ,IMP_LRD_CALC
            //                    ,IMP_LRD_DFZ
            //                    ,IMP_LRD_EFFLQ
            //                    ,IMP_NET_CALC
            //                    ,IMP_NET_DFZ
            //                    ,IMP_NET_EFFLQ
            //                    ,IMPST_PRVR_CALC
            //                    ,IMPST_PRVR_DFZ
            //                    ,IMPST_PRVR_EFFLQ
            //                    ,IMPST_VIS_CALC
            //                    ,IMPST_VIS_DFZ
            //                    ,IMPST_VIS_EFFLQ
            //                    ,RIT_ACC_CALC
            //                    ,RIT_ACC_DFZ
            //                    ,RIT_ACC_EFFLQ
            //                    ,RIT_IRPEF_CALC
            //                    ,RIT_IRPEF_DFZ
            //                    ,RIT_IRPEF_EFFLQ
            //                    ,IMPST_SOST_CALC
            //                    ,IMPST_SOST_DFZ
            //                    ,IMPST_SOST_EFFLQ
            //                    ,TAX_SEP_CALC
            //                    ,TAX_SEP_DFZ
            //                    ,TAX_SEP_EFFLQ
            //                    ,INTR_PREST_CALC
            //                    ,INTR_PREST_DFZ
            //                    ,INTR_PREST_EFFLQ
            //                    ,ACCPRE_SOST_CALC
            //                    ,ACCPRE_SOST_DFZ
            //                    ,ACCPRE_SOST_EFFLQ
            //                    ,ACCPRE_VIS_CALC
            //                    ,ACCPRE_VIS_DFZ
            //                    ,ACCPRE_VIS_EFFLQ
            //                    ,ACCPRE_ACC_CALC
            //                    ,ACCPRE_ACC_DFZ
            //                    ,ACCPRE_ACC_EFFLQ
            //                    ,RES_PRSTZ_CALC
            //                    ,RES_PRSTZ_DFZ
            //                    ,RES_PRSTZ_EFFLQ
            //                    ,RES_PRE_ATT_CALC
            //                    ,RES_PRE_ATT_DFZ
            //                    ,RES_PRE_ATT_EFFLQ
            //                    ,IMP_EXCONTR_EFF
            //                    ,IMPB_VIS_CALC
            //                    ,IMPB_VIS_EFFLQ
            //                    ,IMPB_VIS_DFZ
            //                    ,IMPB_RIT_ACC_CALC
            //                    ,IMPB_RIT_ACC_EFFLQ
            //                    ,IMPB_RIT_ACC_DFZ
            //                    ,IMPB_TFR_CALC
            //                    ,IMPB_TFR_EFFLQ
            //                    ,IMPB_TFR_DFZ
            //                    ,IMPB_IS_CALC
            //                    ,IMPB_IS_EFFLQ
            //                    ,IMPB_IS_DFZ
            //                    ,IMPB_TAX_SEP_CALC
            //                    ,IMPB_TAX_SEP_EFFLQ
            //                    ,IMPB_TAX_SEP_DFZ
            //                    ,IINT_PREST_CALC
            //                    ,IINT_PREST_EFFLQ
            //                    ,IINT_PREST_DFZ
            //                    ,MONT_END2000_CALC
            //                    ,MONT_END2000_EFFLQ
            //                    ,MONT_END2000_DFZ
            //                    ,MONT_END2006_CALC
            //                    ,MONT_END2006_EFFLQ
            //                    ,MONT_END2006_DFZ
            //                    ,MONT_DAL2007_CALC
            //                    ,MONT_DAL2007_EFFLQ
            //                    ,MONT_DAL2007_DFZ
            //                    ,IIMPST_PRVR_CALC
            //                    ,IIMPST_PRVR_EFFLQ
            //                    ,IIMPST_PRVR_DFZ
            //                    ,IIMPST_252_CALC
            //                    ,IIMPST_252_EFFLQ
            //                    ,IIMPST_252_DFZ
            //                    ,IMPST_252_CALC
            //                    ,IMPST_252_EFFLQ
            //                    ,RIT_TFR_CALC
            //                    ,RIT_TFR_EFFLQ
            //                    ,RIT_TFR_DFZ
            //                    ,CNBT_INPSTFM_CALC
            //                    ,CNBT_INPSTFM_EFFLQ
            //                    ,CNBT_INPSTFM_DFZ
            //                    ,ICNB_INPSTFM_CALC
            //                    ,ICNB_INPSTFM_EFFLQ
            //                    ,ICNB_INPSTFM_DFZ
            //                    ,CNDE_END2000_CALC
            //                    ,CNDE_END2000_EFFLQ
            //                    ,CNDE_END2000_DFZ
            //                    ,CNDE_END2006_CALC
            //                    ,CNDE_END2006_EFFLQ
            //                    ,CNDE_END2006_DFZ
            //                    ,CNDE_DAL2007_CALC
            //                    ,CNDE_DAL2007_EFFLQ
            //                    ,CNDE_DAL2007_DFZ
            //                    ,AA_CNBZ_END2000_EF
            //                    ,AA_CNBZ_END2006_EF
            //                    ,AA_CNBZ_DAL2007_EF
            //                    ,MM_CNBZ_END2000_EF
            //                    ,MM_CNBZ_END2006_EF
            //                    ,MM_CNBZ_DAL2007_EF
            //                    ,IMPST_DA_RIMB_EFF
            //                    ,ALQ_TAX_SEP_CALC
            //                    ,ALQ_TAX_SEP_EFFLQ
            //                    ,ALQ_TAX_SEP_DFZ
            //                    ,ALQ_CNBT_INPSTFM_C
            //                    ,ALQ_CNBT_INPSTFM_E
            //                    ,ALQ_CNBT_INPSTFM_D
            //                    ,DS_RIGA
            //                    ,DS_OPER_SQL
            //                    ,DS_VER
            //                    ,DS_TS_INI_CPTZ
            //                    ,DS_TS_END_CPTZ
            //                    ,DS_UTENTE
            //                    ,DS_STATO_ELAB
            //                    ,IMPB_VIS_1382011C
            //                    ,IMPB_VIS_1382011D
            //                    ,IMPB_VIS_1382011L
            //                    ,IMPST_VIS_1382011C
            //                    ,IMPST_VIS_1382011D
            //                    ,IMPST_VIS_1382011L
            //                    ,IMPB_IS_1382011C
            //                    ,IMPB_IS_1382011D
            //                    ,IMPB_IS_1382011L
            //                    ,IS_1382011C
            //                    ,IS_1382011D
            //                    ,IS_1382011L
            //                    ,IMP_INTR_RIT_PAG_C
            //                    ,IMP_INTR_RIT_PAG_D
            //                    ,IMP_INTR_RIT_PAG_L
            //                    ,IMPB_BOLLO_DETT_C
            //                    ,IMPB_BOLLO_DETT_D
            //                    ,IMPB_BOLLO_DETT_L
            //                    ,IMPST_BOLLO_DETT_C
            //                    ,IMPST_BOLLO_DETT_D
            //                    ,IMPST_BOLLO_DETT_L
            //                    ,IMPST_BOLLO_TOT_VC
            //                    ,IMPST_BOLLO_TOT_VD
            //                    ,IMPST_BOLLO_TOT_VL
            //                    ,IMPB_VIS_662014C
            //                    ,IMPB_VIS_662014D
            //                    ,IMPB_VIS_662014L
            //                    ,IMPST_VIS_662014C
            //                    ,IMPST_VIS_662014D
            //                    ,IMPST_VIS_662014L
            //                    ,IMPB_IS_662014C
            //                    ,IMPB_IS_662014D
            //                    ,IMPB_IS_662014L
            //                    ,IS_662014C
            //                    ,IS_662014D
            //                    ,IS_662014L
            //                  )
            //              VALUES
            //                  (
            //                    :DFL-ID-D-FORZ-LIQ
            //                    ,:DFL-ID-LIQ
            //                    ,:DFL-ID-MOVI-CRZ
            //                    ,:DFL-ID-MOVI-CHIU
            //                     :IND-DFL-ID-MOVI-CHIU
            //                    ,:DFL-COD-COMP-ANIA
            //                    ,:DFL-DT-INI-EFF-DB
            //                    ,:DFL-DT-END-EFF-DB
            //                    ,:DFL-IMP-LRD-CALC
            //                     :IND-DFL-IMP-LRD-CALC
            //                    ,:DFL-IMP-LRD-DFZ
            //                     :IND-DFL-IMP-LRD-DFZ
            //                    ,:DFL-IMP-LRD-EFFLQ
            //                     :IND-DFL-IMP-LRD-EFFLQ
            //                    ,:DFL-IMP-NET-CALC
            //                     :IND-DFL-IMP-NET-CALC
            //                    ,:DFL-IMP-NET-DFZ
            //                     :IND-DFL-IMP-NET-DFZ
            //                    ,:DFL-IMP-NET-EFFLQ
            //                     :IND-DFL-IMP-NET-EFFLQ
            //                    ,:DFL-IMPST-PRVR-CALC
            //                     :IND-DFL-IMPST-PRVR-CALC
            //                    ,:DFL-IMPST-PRVR-DFZ
            //                     :IND-DFL-IMPST-PRVR-DFZ
            //                    ,:DFL-IMPST-PRVR-EFFLQ
            //                     :IND-DFL-IMPST-PRVR-EFFLQ
            //                    ,:DFL-IMPST-VIS-CALC
            //                     :IND-DFL-IMPST-VIS-CALC
            //                    ,:DFL-IMPST-VIS-DFZ
            //                     :IND-DFL-IMPST-VIS-DFZ
            //                    ,:DFL-IMPST-VIS-EFFLQ
            //                     :IND-DFL-IMPST-VIS-EFFLQ
            //                    ,:DFL-RIT-ACC-CALC
            //                     :IND-DFL-RIT-ACC-CALC
            //                    ,:DFL-RIT-ACC-DFZ
            //                     :IND-DFL-RIT-ACC-DFZ
            //                    ,:DFL-RIT-ACC-EFFLQ
            //                     :IND-DFL-RIT-ACC-EFFLQ
            //                    ,:DFL-RIT-IRPEF-CALC
            //                     :IND-DFL-RIT-IRPEF-CALC
            //                    ,:DFL-RIT-IRPEF-DFZ
            //                     :IND-DFL-RIT-IRPEF-DFZ
            //                    ,:DFL-RIT-IRPEF-EFFLQ
            //                     :IND-DFL-RIT-IRPEF-EFFLQ
            //                    ,:DFL-IMPST-SOST-CALC
            //                     :IND-DFL-IMPST-SOST-CALC
            //                    ,:DFL-IMPST-SOST-DFZ
            //                     :IND-DFL-IMPST-SOST-DFZ
            //                    ,:DFL-IMPST-SOST-EFFLQ
            //                     :IND-DFL-IMPST-SOST-EFFLQ
            //                    ,:DFL-TAX-SEP-CALC
            //                     :IND-DFL-TAX-SEP-CALC
            //                    ,:DFL-TAX-SEP-DFZ
            //                     :IND-DFL-TAX-SEP-DFZ
            //                    ,:DFL-TAX-SEP-EFFLQ
            //                     :IND-DFL-TAX-SEP-EFFLQ
            //                    ,:DFL-INTR-PREST-CALC
            //                     :IND-DFL-INTR-PREST-CALC
            //                    ,:DFL-INTR-PREST-DFZ
            //                     :IND-DFL-INTR-PREST-DFZ
            //                    ,:DFL-INTR-PREST-EFFLQ
            //                     :IND-DFL-INTR-PREST-EFFLQ
            //                    ,:DFL-ACCPRE-SOST-CALC
            //                     :IND-DFL-ACCPRE-SOST-CALC
            //                    ,:DFL-ACCPRE-SOST-DFZ
            //                     :IND-DFL-ACCPRE-SOST-DFZ
            //                    ,:DFL-ACCPRE-SOST-EFFLQ
            //                     :IND-DFL-ACCPRE-SOST-EFFLQ
            //                    ,:DFL-ACCPRE-VIS-CALC
            //                     :IND-DFL-ACCPRE-VIS-CALC
            //                    ,:DFL-ACCPRE-VIS-DFZ
            //                     :IND-DFL-ACCPRE-VIS-DFZ
            //                    ,:DFL-ACCPRE-VIS-EFFLQ
            //                     :IND-DFL-ACCPRE-VIS-EFFLQ
            //                    ,:DFL-ACCPRE-ACC-CALC
            //                     :IND-DFL-ACCPRE-ACC-CALC
            //                    ,:DFL-ACCPRE-ACC-DFZ
            //                     :IND-DFL-ACCPRE-ACC-DFZ
            //                    ,:DFL-ACCPRE-ACC-EFFLQ
            //                     :IND-DFL-ACCPRE-ACC-EFFLQ
            //                    ,:DFL-RES-PRSTZ-CALC
            //                     :IND-DFL-RES-PRSTZ-CALC
            //                    ,:DFL-RES-PRSTZ-DFZ
            //                     :IND-DFL-RES-PRSTZ-DFZ
            //                    ,:DFL-RES-PRSTZ-EFFLQ
            //                     :IND-DFL-RES-PRSTZ-EFFLQ
            //                    ,:DFL-RES-PRE-ATT-CALC
            //                     :IND-DFL-RES-PRE-ATT-CALC
            //                    ,:DFL-RES-PRE-ATT-DFZ
            //                     :IND-DFL-RES-PRE-ATT-DFZ
            //                    ,:DFL-RES-PRE-ATT-EFFLQ
            //                     :IND-DFL-RES-PRE-ATT-EFFLQ
            //                    ,:DFL-IMP-EXCONTR-EFF
            //                     :IND-DFL-IMP-EXCONTR-EFF
            //                    ,:DFL-IMPB-VIS-CALC
            //                     :IND-DFL-IMPB-VIS-CALC
            //                    ,:DFL-IMPB-VIS-EFFLQ
            //                     :IND-DFL-IMPB-VIS-EFFLQ
            //                    ,:DFL-IMPB-VIS-DFZ
            //                     :IND-DFL-IMPB-VIS-DFZ
            //                    ,:DFL-IMPB-RIT-ACC-CALC
            //                     :IND-DFL-IMPB-RIT-ACC-CALC
            //                    ,:DFL-IMPB-RIT-ACC-EFFLQ
            //                     :IND-DFL-IMPB-RIT-ACC-EFFLQ
            //                    ,:DFL-IMPB-RIT-ACC-DFZ
            //                     :IND-DFL-IMPB-RIT-ACC-DFZ
            //                    ,:DFL-IMPB-TFR-CALC
            //                     :IND-DFL-IMPB-TFR-CALC
            //                    ,:DFL-IMPB-TFR-EFFLQ
            //                     :IND-DFL-IMPB-TFR-EFFLQ
            //                    ,:DFL-IMPB-TFR-DFZ
            //                     :IND-DFL-IMPB-TFR-DFZ
            //                    ,:DFL-IMPB-IS-CALC
            //                     :IND-DFL-IMPB-IS-CALC
            //                    ,:DFL-IMPB-IS-EFFLQ
            //                     :IND-DFL-IMPB-IS-EFFLQ
            //                    ,:DFL-IMPB-IS-DFZ
            //                     :IND-DFL-IMPB-IS-DFZ
            //                    ,:DFL-IMPB-TAX-SEP-CALC
            //                     :IND-DFL-IMPB-TAX-SEP-CALC
            //                    ,:DFL-IMPB-TAX-SEP-EFFLQ
            //                     :IND-DFL-IMPB-TAX-SEP-EFFLQ
            //                    ,:DFL-IMPB-TAX-SEP-DFZ
            //                     :IND-DFL-IMPB-TAX-SEP-DFZ
            //                    ,:DFL-IINT-PREST-CALC
            //                     :IND-DFL-IINT-PREST-CALC
            //                    ,:DFL-IINT-PREST-EFFLQ
            //                     :IND-DFL-IINT-PREST-EFFLQ
            //                    ,:DFL-IINT-PREST-DFZ
            //                     :IND-DFL-IINT-PREST-DFZ
            //                    ,:DFL-MONT-END2000-CALC
            //                     :IND-DFL-MONT-END2000-CALC
            //                    ,:DFL-MONT-END2000-EFFLQ
            //                     :IND-DFL-MONT-END2000-EFFLQ
            //                    ,:DFL-MONT-END2000-DFZ
            //                     :IND-DFL-MONT-END2000-DFZ
            //                    ,:DFL-MONT-END2006-CALC
            //                     :IND-DFL-MONT-END2006-CALC
            //                    ,:DFL-MONT-END2006-EFFLQ
            //                     :IND-DFL-MONT-END2006-EFFLQ
            //                    ,:DFL-MONT-END2006-DFZ
            //                     :IND-DFL-MONT-END2006-DFZ
            //                    ,:DFL-MONT-DAL2007-CALC
            //                     :IND-DFL-MONT-DAL2007-CALC
            //                    ,:DFL-MONT-DAL2007-EFFLQ
            //                     :IND-DFL-MONT-DAL2007-EFFLQ
            //                    ,:DFL-MONT-DAL2007-DFZ
            //                     :IND-DFL-MONT-DAL2007-DFZ
            //                    ,:DFL-IIMPST-PRVR-CALC
            //                     :IND-DFL-IIMPST-PRVR-CALC
            //                    ,:DFL-IIMPST-PRVR-EFFLQ
            //                     :IND-DFL-IIMPST-PRVR-EFFLQ
            //                    ,:DFL-IIMPST-PRVR-DFZ
            //                     :IND-DFL-IIMPST-PRVR-DFZ
            //                    ,:DFL-IIMPST-252-CALC
            //                     :IND-DFL-IIMPST-252-CALC
            //                    ,:DFL-IIMPST-252-EFFLQ
            //                     :IND-DFL-IIMPST-252-EFFLQ
            //                    ,:DFL-IIMPST-252-DFZ
            //                     :IND-DFL-IIMPST-252-DFZ
            //                    ,:DFL-IMPST-252-CALC
            //                     :IND-DFL-IMPST-252-CALC
            //                    ,:DFL-IMPST-252-EFFLQ
            //                     :IND-DFL-IMPST-252-EFFLQ
            //                    ,:DFL-RIT-TFR-CALC
            //                     :IND-DFL-RIT-TFR-CALC
            //                    ,:DFL-RIT-TFR-EFFLQ
            //                     :IND-DFL-RIT-TFR-EFFLQ
            //                    ,:DFL-RIT-TFR-DFZ
            //                     :IND-DFL-RIT-TFR-DFZ
            //                    ,:DFL-CNBT-INPSTFM-CALC
            //                     :IND-DFL-CNBT-INPSTFM-CALC
            //                    ,:DFL-CNBT-INPSTFM-EFFLQ
            //                     :IND-DFL-CNBT-INPSTFM-EFFLQ
            //                    ,:DFL-CNBT-INPSTFM-DFZ
            //                     :IND-DFL-CNBT-INPSTFM-DFZ
            //                    ,:DFL-ICNB-INPSTFM-CALC
            //                     :IND-DFL-ICNB-INPSTFM-CALC
            //                    ,:DFL-ICNB-INPSTFM-EFFLQ
            //                     :IND-DFL-ICNB-INPSTFM-EFFLQ
            //                    ,:DFL-ICNB-INPSTFM-DFZ
            //                     :IND-DFL-ICNB-INPSTFM-DFZ
            //                    ,:DFL-CNDE-END2000-CALC
            //                     :IND-DFL-CNDE-END2000-CALC
            //                    ,:DFL-CNDE-END2000-EFFLQ
            //                     :IND-DFL-CNDE-END2000-EFFLQ
            //                    ,:DFL-CNDE-END2000-DFZ
            //                     :IND-DFL-CNDE-END2000-DFZ
            //                    ,:DFL-CNDE-END2006-CALC
            //                     :IND-DFL-CNDE-END2006-CALC
            //                    ,:DFL-CNDE-END2006-EFFLQ
            //                     :IND-DFL-CNDE-END2006-EFFLQ
            //                    ,:DFL-CNDE-END2006-DFZ
            //                     :IND-DFL-CNDE-END2006-DFZ
            //                    ,:DFL-CNDE-DAL2007-CALC
            //                     :IND-DFL-CNDE-DAL2007-CALC
            //                    ,:DFL-CNDE-DAL2007-EFFLQ
            //                     :IND-DFL-CNDE-DAL2007-EFFLQ
            //                    ,:DFL-CNDE-DAL2007-DFZ
            //                     :IND-DFL-CNDE-DAL2007-DFZ
            //                    ,:DFL-AA-CNBZ-END2000-EF
            //                     :IND-DFL-AA-CNBZ-END2000-EF
            //                    ,:DFL-AA-CNBZ-END2006-EF
            //                     :IND-DFL-AA-CNBZ-END2006-EF
            //                    ,:DFL-AA-CNBZ-DAL2007-EF
            //                     :IND-DFL-AA-CNBZ-DAL2007-EF
            //                    ,:DFL-MM-CNBZ-END2000-EF
            //                     :IND-DFL-MM-CNBZ-END2000-EF
            //                    ,:DFL-MM-CNBZ-END2006-EF
            //                     :IND-DFL-MM-CNBZ-END2006-EF
            //                    ,:DFL-MM-CNBZ-DAL2007-EF
            //                     :IND-DFL-MM-CNBZ-DAL2007-EF
            //                    ,:DFL-IMPST-DA-RIMB-EFF
            //                     :IND-DFL-IMPST-DA-RIMB-EFF
            //                    ,:DFL-ALQ-TAX-SEP-CALC
            //                     :IND-DFL-ALQ-TAX-SEP-CALC
            //                    ,:DFL-ALQ-TAX-SEP-EFFLQ
            //                     :IND-DFL-ALQ-TAX-SEP-EFFLQ
            //                    ,:DFL-ALQ-TAX-SEP-DFZ
            //                     :IND-DFL-ALQ-TAX-SEP-DFZ
            //                    ,:DFL-ALQ-CNBT-INPSTFM-C
            //                     :IND-DFL-ALQ-CNBT-INPSTFM-C
            //                    ,:DFL-ALQ-CNBT-INPSTFM-E
            //                     :IND-DFL-ALQ-CNBT-INPSTFM-E
            //                    ,:DFL-ALQ-CNBT-INPSTFM-D
            //                     :IND-DFL-ALQ-CNBT-INPSTFM-D
            //                    ,:DFL-DS-RIGA
            //                    ,:DFL-DS-OPER-SQL
            //                    ,:DFL-DS-VER
            //                    ,:DFL-DS-TS-INI-CPTZ
            //                    ,:DFL-DS-TS-END-CPTZ
            //                    ,:DFL-DS-UTENTE
            //                    ,:DFL-DS-STATO-ELAB
            //                    ,:DFL-IMPB-VIS-1382011C
            //                     :IND-DFL-IMPB-VIS-1382011C
            //                    ,:DFL-IMPB-VIS-1382011D
            //                     :IND-DFL-IMPB-VIS-1382011D
            //                    ,:DFL-IMPB-VIS-1382011L
            //                     :IND-DFL-IMPB-VIS-1382011L
            //                    ,:DFL-IMPST-VIS-1382011C
            //                     :IND-DFL-IMPST-VIS-1382011C
            //                    ,:DFL-IMPST-VIS-1382011D
            //                     :IND-DFL-IMPST-VIS-1382011D
            //                    ,:DFL-IMPST-VIS-1382011L
            //                     :IND-DFL-IMPST-VIS-1382011L
            //                    ,:DFL-IMPB-IS-1382011C
            //                     :IND-DFL-IMPB-IS-1382011C
            //                    ,:DFL-IMPB-IS-1382011D
            //                     :IND-DFL-IMPB-IS-1382011D
            //                    ,:DFL-IMPB-IS-1382011L
            //                     :IND-DFL-IMPB-IS-1382011L
            //                    ,:DFL-IS-1382011C
            //                     :IND-DFL-IS-1382011C
            //                    ,:DFL-IS-1382011D
            //                     :IND-DFL-IS-1382011D
            //                    ,:DFL-IS-1382011L
            //                     :IND-DFL-IS-1382011L
            //                    ,:DFL-IMP-INTR-RIT-PAG-C
            //                     :IND-DFL-IMP-INTR-RIT-PAG-C
            //                    ,:DFL-IMP-INTR-RIT-PAG-D
            //                     :IND-DFL-IMP-INTR-RIT-PAG-D
            //                    ,:DFL-IMP-INTR-RIT-PAG-L
            //                     :IND-DFL-IMP-INTR-RIT-PAG-L
            //                    ,:DFL-IMPB-BOLLO-DETT-C
            //                     :IND-DFL-IMPB-BOLLO-DETT-C
            //                    ,:DFL-IMPB-BOLLO-DETT-D
            //                     :IND-DFL-IMPB-BOLLO-DETT-D
            //                    ,:DFL-IMPB-BOLLO-DETT-L
            //                     :IND-DFL-IMPB-BOLLO-DETT-L
            //                    ,:DFL-IMPST-BOLLO-DETT-C
            //                     :IND-DFL-IMPST-BOLLO-DETT-C
            //                    ,:DFL-IMPST-BOLLO-DETT-D
            //                     :IND-DFL-IMPST-BOLLO-DETT-D
            //                    ,:DFL-IMPST-BOLLO-DETT-L
            //                     :IND-DFL-IMPST-BOLLO-DETT-L
            //                    ,:DFL-IMPST-BOLLO-TOT-VC
            //                     :IND-DFL-IMPST-BOLLO-TOT-VC
            //                    ,:DFL-IMPST-BOLLO-TOT-VD
            //                     :IND-DFL-IMPST-BOLLO-TOT-VD
            //                    ,:DFL-IMPST-BOLLO-TOT-VL
            //                     :IND-DFL-IMPST-BOLLO-TOT-VL
            //                    ,:DFL-IMPB-VIS-662014C
            //                     :IND-DFL-IMPB-VIS-662014C
            //                    ,:DFL-IMPB-VIS-662014D
            //                     :IND-DFL-IMPB-VIS-662014D
            //                    ,:DFL-IMPB-VIS-662014L
            //                     :IND-DFL-IMPB-VIS-662014L
            //                    ,:DFL-IMPST-VIS-662014C
            //                     :IND-DFL-IMPST-VIS-662014C
            //                    ,:DFL-IMPST-VIS-662014D
            //                     :IND-DFL-IMPST-VIS-662014D
            //                    ,:DFL-IMPST-VIS-662014L
            //                     :IND-DFL-IMPST-VIS-662014L
            //                    ,:DFL-IMPB-IS-662014C
            //                     :IND-DFL-IMPB-IS-662014C
            //                    ,:DFL-IMPB-IS-662014D
            //                     :IND-DFL-IMPB-IS-662014D
            //                    ,:DFL-IMPB-IS-662014L
            //                     :IND-DFL-IMPB-IS-662014L
            //                    ,:DFL-IS-662014C
            //                     :IND-DFL-IS-662014C
            //                    ,:DFL-IS-662014D
            //                     :IND-DFL-IS-662014D
            //                    ,:DFL-IS-662014L
            //                     :IND-DFL-IS-662014L
            //                  )
            //           END-EXEC
            dForzLiqDao.insertRec(this);
            // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
            a100CheckReturnCode();
        }
    }

    /**Original name: A230-UPDATE-PK<br>*/
    private void a230UpdatePk() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE D_FORZ_LIQ SET
        //                   ID_D_FORZ_LIQ          =
        //                :DFL-ID-D-FORZ-LIQ
        //                  ,ID_LIQ                 =
        //                :DFL-ID-LIQ
        //                  ,ID_MOVI_CRZ            =
        //                :DFL-ID-MOVI-CRZ
        //                  ,ID_MOVI_CHIU           =
        //                :DFL-ID-MOVI-CHIU
        //                                       :IND-DFL-ID-MOVI-CHIU
        //                  ,COD_COMP_ANIA          =
        //                :DFL-COD-COMP-ANIA
        //                  ,DT_INI_EFF             =
        //           :DFL-DT-INI-EFF-DB
        //                  ,DT_END_EFF             =
        //           :DFL-DT-END-EFF-DB
        //                  ,IMP_LRD_CALC           =
        //                :DFL-IMP-LRD-CALC
        //                                       :IND-DFL-IMP-LRD-CALC
        //                  ,IMP_LRD_DFZ            =
        //                :DFL-IMP-LRD-DFZ
        //                                       :IND-DFL-IMP-LRD-DFZ
        //                  ,IMP_LRD_EFFLQ          =
        //                :DFL-IMP-LRD-EFFLQ
        //                                       :IND-DFL-IMP-LRD-EFFLQ
        //                  ,IMP_NET_CALC           =
        //                :DFL-IMP-NET-CALC
        //                                       :IND-DFL-IMP-NET-CALC
        //                  ,IMP_NET_DFZ            =
        //                :DFL-IMP-NET-DFZ
        //                                       :IND-DFL-IMP-NET-DFZ
        //                  ,IMP_NET_EFFLQ          =
        //                :DFL-IMP-NET-EFFLQ
        //                                       :IND-DFL-IMP-NET-EFFLQ
        //                  ,IMPST_PRVR_CALC        =
        //                :DFL-IMPST-PRVR-CALC
        //                                       :IND-DFL-IMPST-PRVR-CALC
        //                  ,IMPST_PRVR_DFZ         =
        //                :DFL-IMPST-PRVR-DFZ
        //                                       :IND-DFL-IMPST-PRVR-DFZ
        //                  ,IMPST_PRVR_EFFLQ       =
        //                :DFL-IMPST-PRVR-EFFLQ
        //                                       :IND-DFL-IMPST-PRVR-EFFLQ
        //                  ,IMPST_VIS_CALC         =
        //                :DFL-IMPST-VIS-CALC
        //                                       :IND-DFL-IMPST-VIS-CALC
        //                  ,IMPST_VIS_DFZ          =
        //                :DFL-IMPST-VIS-DFZ
        //                                       :IND-DFL-IMPST-VIS-DFZ
        //                  ,IMPST_VIS_EFFLQ        =
        //                :DFL-IMPST-VIS-EFFLQ
        //                                       :IND-DFL-IMPST-VIS-EFFLQ
        //                  ,RIT_ACC_CALC           =
        //                :DFL-RIT-ACC-CALC
        //                                       :IND-DFL-RIT-ACC-CALC
        //                  ,RIT_ACC_DFZ            =
        //                :DFL-RIT-ACC-DFZ
        //                                       :IND-DFL-RIT-ACC-DFZ
        //                  ,RIT_ACC_EFFLQ          =
        //                :DFL-RIT-ACC-EFFLQ
        //                                       :IND-DFL-RIT-ACC-EFFLQ
        //                  ,RIT_IRPEF_CALC         =
        //                :DFL-RIT-IRPEF-CALC
        //                                       :IND-DFL-RIT-IRPEF-CALC
        //                  ,RIT_IRPEF_DFZ          =
        //                :DFL-RIT-IRPEF-DFZ
        //                                       :IND-DFL-RIT-IRPEF-DFZ
        //                  ,RIT_IRPEF_EFFLQ        =
        //                :DFL-RIT-IRPEF-EFFLQ
        //                                       :IND-DFL-RIT-IRPEF-EFFLQ
        //                  ,IMPST_SOST_CALC        =
        //                :DFL-IMPST-SOST-CALC
        //                                       :IND-DFL-IMPST-SOST-CALC
        //                  ,IMPST_SOST_DFZ         =
        //                :DFL-IMPST-SOST-DFZ
        //                                       :IND-DFL-IMPST-SOST-DFZ
        //                  ,IMPST_SOST_EFFLQ       =
        //                :DFL-IMPST-SOST-EFFLQ
        //                                       :IND-DFL-IMPST-SOST-EFFLQ
        //                  ,TAX_SEP_CALC           =
        //                :DFL-TAX-SEP-CALC
        //                                       :IND-DFL-TAX-SEP-CALC
        //                  ,TAX_SEP_DFZ            =
        //                :DFL-TAX-SEP-DFZ
        //                                       :IND-DFL-TAX-SEP-DFZ
        //                  ,TAX_SEP_EFFLQ          =
        //                :DFL-TAX-SEP-EFFLQ
        //                                       :IND-DFL-TAX-SEP-EFFLQ
        //                  ,INTR_PREST_CALC        =
        //                :DFL-INTR-PREST-CALC
        //                                       :IND-DFL-INTR-PREST-CALC
        //                  ,INTR_PREST_DFZ         =
        //                :DFL-INTR-PREST-DFZ
        //                                       :IND-DFL-INTR-PREST-DFZ
        //                  ,INTR_PREST_EFFLQ       =
        //                :DFL-INTR-PREST-EFFLQ
        //                                       :IND-DFL-INTR-PREST-EFFLQ
        //                  ,ACCPRE_SOST_CALC       =
        //                :DFL-ACCPRE-SOST-CALC
        //                                       :IND-DFL-ACCPRE-SOST-CALC
        //                  ,ACCPRE_SOST_DFZ        =
        //                :DFL-ACCPRE-SOST-DFZ
        //                                       :IND-DFL-ACCPRE-SOST-DFZ
        //                  ,ACCPRE_SOST_EFFLQ      =
        //                :DFL-ACCPRE-SOST-EFFLQ
        //                                       :IND-DFL-ACCPRE-SOST-EFFLQ
        //                  ,ACCPRE_VIS_CALC        =
        //                :DFL-ACCPRE-VIS-CALC
        //                                       :IND-DFL-ACCPRE-VIS-CALC
        //                  ,ACCPRE_VIS_DFZ         =
        //                :DFL-ACCPRE-VIS-DFZ
        //                                       :IND-DFL-ACCPRE-VIS-DFZ
        //                  ,ACCPRE_VIS_EFFLQ       =
        //                :DFL-ACCPRE-VIS-EFFLQ
        //                                       :IND-DFL-ACCPRE-VIS-EFFLQ
        //                  ,ACCPRE_ACC_CALC        =
        //                :DFL-ACCPRE-ACC-CALC
        //                                       :IND-DFL-ACCPRE-ACC-CALC
        //                  ,ACCPRE_ACC_DFZ         =
        //                :DFL-ACCPRE-ACC-DFZ
        //                                       :IND-DFL-ACCPRE-ACC-DFZ
        //                  ,ACCPRE_ACC_EFFLQ       =
        //                :DFL-ACCPRE-ACC-EFFLQ
        //                                       :IND-DFL-ACCPRE-ACC-EFFLQ
        //                  ,RES_PRSTZ_CALC         =
        //                :DFL-RES-PRSTZ-CALC
        //                                       :IND-DFL-RES-PRSTZ-CALC
        //                  ,RES_PRSTZ_DFZ          =
        //                :DFL-RES-PRSTZ-DFZ
        //                                       :IND-DFL-RES-PRSTZ-DFZ
        //                  ,RES_PRSTZ_EFFLQ        =
        //                :DFL-RES-PRSTZ-EFFLQ
        //                                       :IND-DFL-RES-PRSTZ-EFFLQ
        //                  ,RES_PRE_ATT_CALC       =
        //                :DFL-RES-PRE-ATT-CALC
        //                                       :IND-DFL-RES-PRE-ATT-CALC
        //                  ,RES_PRE_ATT_DFZ        =
        //                :DFL-RES-PRE-ATT-DFZ
        //                                       :IND-DFL-RES-PRE-ATT-DFZ
        //                  ,RES_PRE_ATT_EFFLQ      =
        //                :DFL-RES-PRE-ATT-EFFLQ
        //                                       :IND-DFL-RES-PRE-ATT-EFFLQ
        //                  ,IMP_EXCONTR_EFF        =
        //                :DFL-IMP-EXCONTR-EFF
        //                                       :IND-DFL-IMP-EXCONTR-EFF
        //                  ,IMPB_VIS_CALC          =
        //                :DFL-IMPB-VIS-CALC
        //                                       :IND-DFL-IMPB-VIS-CALC
        //                  ,IMPB_VIS_EFFLQ         =
        //                :DFL-IMPB-VIS-EFFLQ
        //                                       :IND-DFL-IMPB-VIS-EFFLQ
        //                  ,IMPB_VIS_DFZ           =
        //                :DFL-IMPB-VIS-DFZ
        //                                       :IND-DFL-IMPB-VIS-DFZ
        //                  ,IMPB_RIT_ACC_CALC      =
        //                :DFL-IMPB-RIT-ACC-CALC
        //                                       :IND-DFL-IMPB-RIT-ACC-CALC
        //                  ,IMPB_RIT_ACC_EFFLQ     =
        //                :DFL-IMPB-RIT-ACC-EFFLQ
        //                                       :IND-DFL-IMPB-RIT-ACC-EFFLQ
        //                  ,IMPB_RIT_ACC_DFZ       =
        //                :DFL-IMPB-RIT-ACC-DFZ
        //                                       :IND-DFL-IMPB-RIT-ACC-DFZ
        //                  ,IMPB_TFR_CALC          =
        //                :DFL-IMPB-TFR-CALC
        //                                       :IND-DFL-IMPB-TFR-CALC
        //                  ,IMPB_TFR_EFFLQ         =
        //                :DFL-IMPB-TFR-EFFLQ
        //                                       :IND-DFL-IMPB-TFR-EFFLQ
        //                  ,IMPB_TFR_DFZ           =
        //                :DFL-IMPB-TFR-DFZ
        //                                       :IND-DFL-IMPB-TFR-DFZ
        //                  ,IMPB_IS_CALC           =
        //                :DFL-IMPB-IS-CALC
        //                                       :IND-DFL-IMPB-IS-CALC
        //                  ,IMPB_IS_EFFLQ          =
        //                :DFL-IMPB-IS-EFFLQ
        //                                       :IND-DFL-IMPB-IS-EFFLQ
        //                  ,IMPB_IS_DFZ            =
        //                :DFL-IMPB-IS-DFZ
        //                                       :IND-DFL-IMPB-IS-DFZ
        //                  ,IMPB_TAX_SEP_CALC      =
        //                :DFL-IMPB-TAX-SEP-CALC
        //                                       :IND-DFL-IMPB-TAX-SEP-CALC
        //                  ,IMPB_TAX_SEP_EFFLQ     =
        //                :DFL-IMPB-TAX-SEP-EFFLQ
        //                                       :IND-DFL-IMPB-TAX-SEP-EFFLQ
        //                  ,IMPB_TAX_SEP_DFZ       =
        //                :DFL-IMPB-TAX-SEP-DFZ
        //                                       :IND-DFL-IMPB-TAX-SEP-DFZ
        //                  ,IINT_PREST_CALC        =
        //                :DFL-IINT-PREST-CALC
        //                                       :IND-DFL-IINT-PREST-CALC
        //                  ,IINT_PREST_EFFLQ       =
        //                :DFL-IINT-PREST-EFFLQ
        //                                       :IND-DFL-IINT-PREST-EFFLQ
        //                  ,IINT_PREST_DFZ         =
        //                :DFL-IINT-PREST-DFZ
        //                                       :IND-DFL-IINT-PREST-DFZ
        //                  ,MONT_END2000_CALC      =
        //                :DFL-MONT-END2000-CALC
        //                                       :IND-DFL-MONT-END2000-CALC
        //                  ,MONT_END2000_EFFLQ     =
        //                :DFL-MONT-END2000-EFFLQ
        //                                       :IND-DFL-MONT-END2000-EFFLQ
        //                  ,MONT_END2000_DFZ       =
        //                :DFL-MONT-END2000-DFZ
        //                                       :IND-DFL-MONT-END2000-DFZ
        //                  ,MONT_END2006_CALC      =
        //                :DFL-MONT-END2006-CALC
        //                                       :IND-DFL-MONT-END2006-CALC
        //                  ,MONT_END2006_EFFLQ     =
        //                :DFL-MONT-END2006-EFFLQ
        //                                       :IND-DFL-MONT-END2006-EFFLQ
        //                  ,MONT_END2006_DFZ       =
        //                :DFL-MONT-END2006-DFZ
        //                                       :IND-DFL-MONT-END2006-DFZ
        //                  ,MONT_DAL2007_CALC      =
        //                :DFL-MONT-DAL2007-CALC
        //                                       :IND-DFL-MONT-DAL2007-CALC
        //                  ,MONT_DAL2007_EFFLQ     =
        //                :DFL-MONT-DAL2007-EFFLQ
        //                                       :IND-DFL-MONT-DAL2007-EFFLQ
        //                  ,MONT_DAL2007_DFZ       =
        //                :DFL-MONT-DAL2007-DFZ
        //                                       :IND-DFL-MONT-DAL2007-DFZ
        //                  ,IIMPST_PRVR_CALC       =
        //                :DFL-IIMPST-PRVR-CALC
        //                                       :IND-DFL-IIMPST-PRVR-CALC
        //                  ,IIMPST_PRVR_EFFLQ      =
        //                :DFL-IIMPST-PRVR-EFFLQ
        //                                       :IND-DFL-IIMPST-PRVR-EFFLQ
        //                  ,IIMPST_PRVR_DFZ        =
        //                :DFL-IIMPST-PRVR-DFZ
        //                                       :IND-DFL-IIMPST-PRVR-DFZ
        //                  ,IIMPST_252_CALC        =
        //                :DFL-IIMPST-252-CALC
        //                                       :IND-DFL-IIMPST-252-CALC
        //                  ,IIMPST_252_EFFLQ       =
        //                :DFL-IIMPST-252-EFFLQ
        //                                       :IND-DFL-IIMPST-252-EFFLQ
        //                  ,IIMPST_252_DFZ         =
        //                :DFL-IIMPST-252-DFZ
        //                                       :IND-DFL-IIMPST-252-DFZ
        //                  ,IMPST_252_CALC         =
        //                :DFL-IMPST-252-CALC
        //                                       :IND-DFL-IMPST-252-CALC
        //                  ,IMPST_252_EFFLQ        =
        //                :DFL-IMPST-252-EFFLQ
        //                                       :IND-DFL-IMPST-252-EFFLQ
        //                  ,RIT_TFR_CALC           =
        //                :DFL-RIT-TFR-CALC
        //                                       :IND-DFL-RIT-TFR-CALC
        //                  ,RIT_TFR_EFFLQ          =
        //                :DFL-RIT-TFR-EFFLQ
        //                                       :IND-DFL-RIT-TFR-EFFLQ
        //                  ,RIT_TFR_DFZ            =
        //                :DFL-RIT-TFR-DFZ
        //                                       :IND-DFL-RIT-TFR-DFZ
        //                  ,CNBT_INPSTFM_CALC      =
        //                :DFL-CNBT-INPSTFM-CALC
        //                                       :IND-DFL-CNBT-INPSTFM-CALC
        //                  ,CNBT_INPSTFM_EFFLQ     =
        //                :DFL-CNBT-INPSTFM-EFFLQ
        //                                       :IND-DFL-CNBT-INPSTFM-EFFLQ
        //                  ,CNBT_INPSTFM_DFZ       =
        //                :DFL-CNBT-INPSTFM-DFZ
        //                                       :IND-DFL-CNBT-INPSTFM-DFZ
        //                  ,ICNB_INPSTFM_CALC      =
        //                :DFL-ICNB-INPSTFM-CALC
        //                                       :IND-DFL-ICNB-INPSTFM-CALC
        //                  ,ICNB_INPSTFM_EFFLQ     =
        //                :DFL-ICNB-INPSTFM-EFFLQ
        //                                       :IND-DFL-ICNB-INPSTFM-EFFLQ
        //                  ,ICNB_INPSTFM_DFZ       =
        //                :DFL-ICNB-INPSTFM-DFZ
        //                                       :IND-DFL-ICNB-INPSTFM-DFZ
        //                  ,CNDE_END2000_CALC      =
        //                :DFL-CNDE-END2000-CALC
        //                                       :IND-DFL-CNDE-END2000-CALC
        //                  ,CNDE_END2000_EFFLQ     =
        //                :DFL-CNDE-END2000-EFFLQ
        //                                       :IND-DFL-CNDE-END2000-EFFLQ
        //                  ,CNDE_END2000_DFZ       =
        //                :DFL-CNDE-END2000-DFZ
        //                                       :IND-DFL-CNDE-END2000-DFZ
        //                  ,CNDE_END2006_CALC      =
        //                :DFL-CNDE-END2006-CALC
        //                                       :IND-DFL-CNDE-END2006-CALC
        //                  ,CNDE_END2006_EFFLQ     =
        //                :DFL-CNDE-END2006-EFFLQ
        //                                       :IND-DFL-CNDE-END2006-EFFLQ
        //                  ,CNDE_END2006_DFZ       =
        //                :DFL-CNDE-END2006-DFZ
        //                                       :IND-DFL-CNDE-END2006-DFZ
        //                  ,CNDE_DAL2007_CALC      =
        //                :DFL-CNDE-DAL2007-CALC
        //                                       :IND-DFL-CNDE-DAL2007-CALC
        //                  ,CNDE_DAL2007_EFFLQ     =
        //                :DFL-CNDE-DAL2007-EFFLQ
        //                                       :IND-DFL-CNDE-DAL2007-EFFLQ
        //                  ,CNDE_DAL2007_DFZ       =
        //                :DFL-CNDE-DAL2007-DFZ
        //                                       :IND-DFL-CNDE-DAL2007-DFZ
        //                  ,AA_CNBZ_END2000_EF     =
        //                :DFL-AA-CNBZ-END2000-EF
        //                                       :IND-DFL-AA-CNBZ-END2000-EF
        //                  ,AA_CNBZ_END2006_EF     =
        //                :DFL-AA-CNBZ-END2006-EF
        //                                       :IND-DFL-AA-CNBZ-END2006-EF
        //                  ,AA_CNBZ_DAL2007_EF     =
        //                :DFL-AA-CNBZ-DAL2007-EF
        //                                       :IND-DFL-AA-CNBZ-DAL2007-EF
        //                  ,MM_CNBZ_END2000_EF     =
        //                :DFL-MM-CNBZ-END2000-EF
        //                                       :IND-DFL-MM-CNBZ-END2000-EF
        //                  ,MM_CNBZ_END2006_EF     =
        //                :DFL-MM-CNBZ-END2006-EF
        //                                       :IND-DFL-MM-CNBZ-END2006-EF
        //                  ,MM_CNBZ_DAL2007_EF     =
        //                :DFL-MM-CNBZ-DAL2007-EF
        //                                       :IND-DFL-MM-CNBZ-DAL2007-EF
        //                  ,IMPST_DA_RIMB_EFF      =
        //                :DFL-IMPST-DA-RIMB-EFF
        //                                       :IND-DFL-IMPST-DA-RIMB-EFF
        //                  ,ALQ_TAX_SEP_CALC       =
        //                :DFL-ALQ-TAX-SEP-CALC
        //                                       :IND-DFL-ALQ-TAX-SEP-CALC
        //                  ,ALQ_TAX_SEP_EFFLQ      =
        //                :DFL-ALQ-TAX-SEP-EFFLQ
        //                                       :IND-DFL-ALQ-TAX-SEP-EFFLQ
        //                  ,ALQ_TAX_SEP_DFZ        =
        //                :DFL-ALQ-TAX-SEP-DFZ
        //                                       :IND-DFL-ALQ-TAX-SEP-DFZ
        //                  ,ALQ_CNBT_INPSTFM_C     =
        //                :DFL-ALQ-CNBT-INPSTFM-C
        //                                       :IND-DFL-ALQ-CNBT-INPSTFM-C
        //                  ,ALQ_CNBT_INPSTFM_E     =
        //                :DFL-ALQ-CNBT-INPSTFM-E
        //                                       :IND-DFL-ALQ-CNBT-INPSTFM-E
        //                  ,ALQ_CNBT_INPSTFM_D     =
        //                :DFL-ALQ-CNBT-INPSTFM-D
        //                                       :IND-DFL-ALQ-CNBT-INPSTFM-D
        //                  ,DS_RIGA                =
        //                :DFL-DS-RIGA
        //                  ,DS_OPER_SQL            =
        //                :DFL-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :DFL-DS-VER
        //                  ,DS_TS_INI_CPTZ         =
        //                :DFL-DS-TS-INI-CPTZ
        //                  ,DS_TS_END_CPTZ         =
        //                :DFL-DS-TS-END-CPTZ
        //                  ,DS_UTENTE              =
        //                :DFL-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :DFL-DS-STATO-ELAB
        //                  ,IMPB_VIS_1382011C      =
        //                :DFL-IMPB-VIS-1382011C
        //                                       :IND-DFL-IMPB-VIS-1382011C
        //                  ,IMPB_VIS_1382011D      =
        //                :DFL-IMPB-VIS-1382011D
        //                                       :IND-DFL-IMPB-VIS-1382011D
        //                  ,IMPB_VIS_1382011L      =
        //                :DFL-IMPB-VIS-1382011L
        //                                       :IND-DFL-IMPB-VIS-1382011L
        //                  ,IMPST_VIS_1382011C     =
        //                :DFL-IMPST-VIS-1382011C
        //                                       :IND-DFL-IMPST-VIS-1382011C
        //                  ,IMPST_VIS_1382011D     =
        //                :DFL-IMPST-VIS-1382011D
        //                                       :IND-DFL-IMPST-VIS-1382011D
        //                  ,IMPST_VIS_1382011L     =
        //                :DFL-IMPST-VIS-1382011L
        //                                       :IND-DFL-IMPST-VIS-1382011L
        //                  ,IMPB_IS_1382011C       =
        //                :DFL-IMPB-IS-1382011C
        //                                       :IND-DFL-IMPB-IS-1382011C
        //                  ,IMPB_IS_1382011D       =
        //                :DFL-IMPB-IS-1382011D
        //                                       :IND-DFL-IMPB-IS-1382011D
        //                  ,IMPB_IS_1382011L       =
        //                :DFL-IMPB-IS-1382011L
        //                                       :IND-DFL-IMPB-IS-1382011L
        //                  ,IS_1382011C            =
        //                :DFL-IS-1382011C
        //                                       :IND-DFL-IS-1382011C
        //                  ,IS_1382011D            =
        //                :DFL-IS-1382011D
        //                                       :IND-DFL-IS-1382011D
        //                  ,IS_1382011L            =
        //                :DFL-IS-1382011L
        //                                       :IND-DFL-IS-1382011L
        //                  ,IMP_INTR_RIT_PAG_C     =
        //                :DFL-IMP-INTR-RIT-PAG-C
        //                                       :IND-DFL-IMP-INTR-RIT-PAG-C
        //                  ,IMP_INTR_RIT_PAG_D     =
        //                :DFL-IMP-INTR-RIT-PAG-D
        //                                       :IND-DFL-IMP-INTR-RIT-PAG-D
        //                  ,IMP_INTR_RIT_PAG_L     =
        //                :DFL-IMP-INTR-RIT-PAG-L
        //                                       :IND-DFL-IMP-INTR-RIT-PAG-L
        //                  ,IMPB_BOLLO_DETT_C      =
        //                :DFL-IMPB-BOLLO-DETT-C
        //                                       :IND-DFL-IMPB-BOLLO-DETT-C
        //                  ,IMPB_BOLLO_DETT_D      =
        //                :DFL-IMPB-BOLLO-DETT-D
        //                                       :IND-DFL-IMPB-BOLLO-DETT-D
        //                  ,IMPB_BOLLO_DETT_L      =
        //                :DFL-IMPB-BOLLO-DETT-L
        //                                       :IND-DFL-IMPB-BOLLO-DETT-L
        //                  ,IMPST_BOLLO_DETT_C     =
        //                :DFL-IMPST-BOLLO-DETT-C
        //                                       :IND-DFL-IMPST-BOLLO-DETT-C
        //                  ,IMPST_BOLLO_DETT_D     =
        //                :DFL-IMPST-BOLLO-DETT-D
        //                                       :IND-DFL-IMPST-BOLLO-DETT-D
        //                  ,IMPST_BOLLO_DETT_L     =
        //                :DFL-IMPST-BOLLO-DETT-L
        //                                       :IND-DFL-IMPST-BOLLO-DETT-L
        //                  ,IMPST_BOLLO_TOT_VC     =
        //                :DFL-IMPST-BOLLO-TOT-VC
        //                                       :IND-DFL-IMPST-BOLLO-TOT-VC
        //                  ,IMPST_BOLLO_TOT_VD     =
        //                :DFL-IMPST-BOLLO-TOT-VD
        //                                       :IND-DFL-IMPST-BOLLO-TOT-VD
        //                  ,IMPST_BOLLO_TOT_VL     =
        //                :DFL-IMPST-BOLLO-TOT-VL
        //                                       :IND-DFL-IMPST-BOLLO-TOT-VL
        //                  ,IMPB_VIS_662014C       =
        //                :DFL-IMPB-VIS-662014C
        //                                       :IND-DFL-IMPB-VIS-662014C
        //                  ,IMPB_VIS_662014D       =
        //                :DFL-IMPB-VIS-662014D
        //                                       :IND-DFL-IMPB-VIS-662014D
        //                  ,IMPB_VIS_662014L       =
        //                :DFL-IMPB-VIS-662014L
        //                                       :IND-DFL-IMPB-VIS-662014L
        //                  ,IMPST_VIS_662014C      =
        //                :DFL-IMPST-VIS-662014C
        //                                       :IND-DFL-IMPST-VIS-662014C
        //                  ,IMPST_VIS_662014D      =
        //                :DFL-IMPST-VIS-662014D
        //                                       :IND-DFL-IMPST-VIS-662014D
        //                  ,IMPST_VIS_662014L      =
        //                :DFL-IMPST-VIS-662014L
        //                                       :IND-DFL-IMPST-VIS-662014L
        //                  ,IMPB_IS_662014C        =
        //                :DFL-IMPB-IS-662014C
        //                                       :IND-DFL-IMPB-IS-662014C
        //                  ,IMPB_IS_662014D        =
        //                :DFL-IMPB-IS-662014D
        //                                       :IND-DFL-IMPB-IS-662014D
        //                  ,IMPB_IS_662014L        =
        //                :DFL-IMPB-IS-662014L
        //                                       :IND-DFL-IMPB-IS-662014L
        //                  ,IS_662014C             =
        //                :DFL-IS-662014C
        //                                       :IND-DFL-IS-662014C
        //                  ,IS_662014D             =
        //                :DFL-IS-662014D
        //                                       :IND-DFL-IS-662014D
        //                  ,IS_662014L             =
        //                :DFL-IS-662014L
        //                                       :IND-DFL-IS-662014L
        //                WHERE     DS_RIGA = :DFL-DS-RIGA
        //           END-EXEC.
        dForzLiqDao.updateRec(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A240-DELETE-PK<br>*/
    private void a240DeletePk() {
        // COB_CODE: EXEC SQL
        //                DELETE
        //                FROM D_FORZ_LIQ
        //                WHERE     DS_RIGA = :DFL-DS-RIGA
        //           END-EXEC.
        dForzLiqDao.deleteByDflDsRiga(dForzLiq.getDflDsRiga());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A305-DECLARE-CURSOR-ID-EFF<br>
	 * <pre>----
	 * ----  gestione ID Effetto
	 * ----</pre>*/
    private void a305DeclareCursorIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-ID-UPD-EFF-DFL CURSOR FOR
        //              SELECT
        //                     ID_D_FORZ_LIQ
        //                    ,ID_LIQ
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,COD_COMP_ANIA
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,IMP_LRD_CALC
        //                    ,IMP_LRD_DFZ
        //                    ,IMP_LRD_EFFLQ
        //                    ,IMP_NET_CALC
        //                    ,IMP_NET_DFZ
        //                    ,IMP_NET_EFFLQ
        //                    ,IMPST_PRVR_CALC
        //                    ,IMPST_PRVR_DFZ
        //                    ,IMPST_PRVR_EFFLQ
        //                    ,IMPST_VIS_CALC
        //                    ,IMPST_VIS_DFZ
        //                    ,IMPST_VIS_EFFLQ
        //                    ,RIT_ACC_CALC
        //                    ,RIT_ACC_DFZ
        //                    ,RIT_ACC_EFFLQ
        //                    ,RIT_IRPEF_CALC
        //                    ,RIT_IRPEF_DFZ
        //                    ,RIT_IRPEF_EFFLQ
        //                    ,IMPST_SOST_CALC
        //                    ,IMPST_SOST_DFZ
        //                    ,IMPST_SOST_EFFLQ
        //                    ,TAX_SEP_CALC
        //                    ,TAX_SEP_DFZ
        //                    ,TAX_SEP_EFFLQ
        //                    ,INTR_PREST_CALC
        //                    ,INTR_PREST_DFZ
        //                    ,INTR_PREST_EFFLQ
        //                    ,ACCPRE_SOST_CALC
        //                    ,ACCPRE_SOST_DFZ
        //                    ,ACCPRE_SOST_EFFLQ
        //                    ,ACCPRE_VIS_CALC
        //                    ,ACCPRE_VIS_DFZ
        //                    ,ACCPRE_VIS_EFFLQ
        //                    ,ACCPRE_ACC_CALC
        //                    ,ACCPRE_ACC_DFZ
        //                    ,ACCPRE_ACC_EFFLQ
        //                    ,RES_PRSTZ_CALC
        //                    ,RES_PRSTZ_DFZ
        //                    ,RES_PRSTZ_EFFLQ
        //                    ,RES_PRE_ATT_CALC
        //                    ,RES_PRE_ATT_DFZ
        //                    ,RES_PRE_ATT_EFFLQ
        //                    ,IMP_EXCONTR_EFF
        //                    ,IMPB_VIS_CALC
        //                    ,IMPB_VIS_EFFLQ
        //                    ,IMPB_VIS_DFZ
        //                    ,IMPB_RIT_ACC_CALC
        //                    ,IMPB_RIT_ACC_EFFLQ
        //                    ,IMPB_RIT_ACC_DFZ
        //                    ,IMPB_TFR_CALC
        //                    ,IMPB_TFR_EFFLQ
        //                    ,IMPB_TFR_DFZ
        //                    ,IMPB_IS_CALC
        //                    ,IMPB_IS_EFFLQ
        //                    ,IMPB_IS_DFZ
        //                    ,IMPB_TAX_SEP_CALC
        //                    ,IMPB_TAX_SEP_EFFLQ
        //                    ,IMPB_TAX_SEP_DFZ
        //                    ,IINT_PREST_CALC
        //                    ,IINT_PREST_EFFLQ
        //                    ,IINT_PREST_DFZ
        //                    ,MONT_END2000_CALC
        //                    ,MONT_END2000_EFFLQ
        //                    ,MONT_END2000_DFZ
        //                    ,MONT_END2006_CALC
        //                    ,MONT_END2006_EFFLQ
        //                    ,MONT_END2006_DFZ
        //                    ,MONT_DAL2007_CALC
        //                    ,MONT_DAL2007_EFFLQ
        //                    ,MONT_DAL2007_DFZ
        //                    ,IIMPST_PRVR_CALC
        //                    ,IIMPST_PRVR_EFFLQ
        //                    ,IIMPST_PRVR_DFZ
        //                    ,IIMPST_252_CALC
        //                    ,IIMPST_252_EFFLQ
        //                    ,IIMPST_252_DFZ
        //                    ,IMPST_252_CALC
        //                    ,IMPST_252_EFFLQ
        //                    ,RIT_TFR_CALC
        //                    ,RIT_TFR_EFFLQ
        //                    ,RIT_TFR_DFZ
        //                    ,CNBT_INPSTFM_CALC
        //                    ,CNBT_INPSTFM_EFFLQ
        //                    ,CNBT_INPSTFM_DFZ
        //                    ,ICNB_INPSTFM_CALC
        //                    ,ICNB_INPSTFM_EFFLQ
        //                    ,ICNB_INPSTFM_DFZ
        //                    ,CNDE_END2000_CALC
        //                    ,CNDE_END2000_EFFLQ
        //                    ,CNDE_END2000_DFZ
        //                    ,CNDE_END2006_CALC
        //                    ,CNDE_END2006_EFFLQ
        //                    ,CNDE_END2006_DFZ
        //                    ,CNDE_DAL2007_CALC
        //                    ,CNDE_DAL2007_EFFLQ
        //                    ,CNDE_DAL2007_DFZ
        //                    ,AA_CNBZ_END2000_EF
        //                    ,AA_CNBZ_END2006_EF
        //                    ,AA_CNBZ_DAL2007_EF
        //                    ,MM_CNBZ_END2000_EF
        //                    ,MM_CNBZ_END2006_EF
        //                    ,MM_CNBZ_DAL2007_EF
        //                    ,IMPST_DA_RIMB_EFF
        //                    ,ALQ_TAX_SEP_CALC
        //                    ,ALQ_TAX_SEP_EFFLQ
        //                    ,ALQ_TAX_SEP_DFZ
        //                    ,ALQ_CNBT_INPSTFM_C
        //                    ,ALQ_CNBT_INPSTFM_E
        //                    ,ALQ_CNBT_INPSTFM_D
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,IMPB_VIS_1382011C
        //                    ,IMPB_VIS_1382011D
        //                    ,IMPB_VIS_1382011L
        //                    ,IMPST_VIS_1382011C
        //                    ,IMPST_VIS_1382011D
        //                    ,IMPST_VIS_1382011L
        //                    ,IMPB_IS_1382011C
        //                    ,IMPB_IS_1382011D
        //                    ,IMPB_IS_1382011L
        //                    ,IS_1382011C
        //                    ,IS_1382011D
        //                    ,IS_1382011L
        //                    ,IMP_INTR_RIT_PAG_C
        //                    ,IMP_INTR_RIT_PAG_D
        //                    ,IMP_INTR_RIT_PAG_L
        //                    ,IMPB_BOLLO_DETT_C
        //                    ,IMPB_BOLLO_DETT_D
        //                    ,IMPB_BOLLO_DETT_L
        //                    ,IMPST_BOLLO_DETT_C
        //                    ,IMPST_BOLLO_DETT_D
        //                    ,IMPST_BOLLO_DETT_L
        //                    ,IMPST_BOLLO_TOT_VC
        //                    ,IMPST_BOLLO_TOT_VD
        //                    ,IMPST_BOLLO_TOT_VL
        //                    ,IMPB_VIS_662014C
        //                    ,IMPB_VIS_662014D
        //                    ,IMPB_VIS_662014L
        //                    ,IMPST_VIS_662014C
        //                    ,IMPST_VIS_662014D
        //                    ,IMPST_VIS_662014L
        //                    ,IMPB_IS_662014C
        //                    ,IMPB_IS_662014D
        //                    ,IMPB_IS_662014L
        //                    ,IS_662014C
        //                    ,IS_662014D
        //                    ,IS_662014L
        //              FROM D_FORZ_LIQ
        //              WHERE     ID_D_FORZ_LIQ = :DFL-ID-D-FORZ-LIQ
        //                    AND DS_TS_END_CPTZ = :WS-TS-INFINITO
        //                    AND DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //              ORDER BY DT_INI_EFF ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A310-SELECT-ID-EFF<br>*/
    private void a310SelectIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_D_FORZ_LIQ
        //                ,ID_LIQ
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,COD_COMP_ANIA
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,IMP_LRD_CALC
        //                ,IMP_LRD_DFZ
        //                ,IMP_LRD_EFFLQ
        //                ,IMP_NET_CALC
        //                ,IMP_NET_DFZ
        //                ,IMP_NET_EFFLQ
        //                ,IMPST_PRVR_CALC
        //                ,IMPST_PRVR_DFZ
        //                ,IMPST_PRVR_EFFLQ
        //                ,IMPST_VIS_CALC
        //                ,IMPST_VIS_DFZ
        //                ,IMPST_VIS_EFFLQ
        //                ,RIT_ACC_CALC
        //                ,RIT_ACC_DFZ
        //                ,RIT_ACC_EFFLQ
        //                ,RIT_IRPEF_CALC
        //                ,RIT_IRPEF_DFZ
        //                ,RIT_IRPEF_EFFLQ
        //                ,IMPST_SOST_CALC
        //                ,IMPST_SOST_DFZ
        //                ,IMPST_SOST_EFFLQ
        //                ,TAX_SEP_CALC
        //                ,TAX_SEP_DFZ
        //                ,TAX_SEP_EFFLQ
        //                ,INTR_PREST_CALC
        //                ,INTR_PREST_DFZ
        //                ,INTR_PREST_EFFLQ
        //                ,ACCPRE_SOST_CALC
        //                ,ACCPRE_SOST_DFZ
        //                ,ACCPRE_SOST_EFFLQ
        //                ,ACCPRE_VIS_CALC
        //                ,ACCPRE_VIS_DFZ
        //                ,ACCPRE_VIS_EFFLQ
        //                ,ACCPRE_ACC_CALC
        //                ,ACCPRE_ACC_DFZ
        //                ,ACCPRE_ACC_EFFLQ
        //                ,RES_PRSTZ_CALC
        //                ,RES_PRSTZ_DFZ
        //                ,RES_PRSTZ_EFFLQ
        //                ,RES_PRE_ATT_CALC
        //                ,RES_PRE_ATT_DFZ
        //                ,RES_PRE_ATT_EFFLQ
        //                ,IMP_EXCONTR_EFF
        //                ,IMPB_VIS_CALC
        //                ,IMPB_VIS_EFFLQ
        //                ,IMPB_VIS_DFZ
        //                ,IMPB_RIT_ACC_CALC
        //                ,IMPB_RIT_ACC_EFFLQ
        //                ,IMPB_RIT_ACC_DFZ
        //                ,IMPB_TFR_CALC
        //                ,IMPB_TFR_EFFLQ
        //                ,IMPB_TFR_DFZ
        //                ,IMPB_IS_CALC
        //                ,IMPB_IS_EFFLQ
        //                ,IMPB_IS_DFZ
        //                ,IMPB_TAX_SEP_CALC
        //                ,IMPB_TAX_SEP_EFFLQ
        //                ,IMPB_TAX_SEP_DFZ
        //                ,IINT_PREST_CALC
        //                ,IINT_PREST_EFFLQ
        //                ,IINT_PREST_DFZ
        //                ,MONT_END2000_CALC
        //                ,MONT_END2000_EFFLQ
        //                ,MONT_END2000_DFZ
        //                ,MONT_END2006_CALC
        //                ,MONT_END2006_EFFLQ
        //                ,MONT_END2006_DFZ
        //                ,MONT_DAL2007_CALC
        //                ,MONT_DAL2007_EFFLQ
        //                ,MONT_DAL2007_DFZ
        //                ,IIMPST_PRVR_CALC
        //                ,IIMPST_PRVR_EFFLQ
        //                ,IIMPST_PRVR_DFZ
        //                ,IIMPST_252_CALC
        //                ,IIMPST_252_EFFLQ
        //                ,IIMPST_252_DFZ
        //                ,IMPST_252_CALC
        //                ,IMPST_252_EFFLQ
        //                ,RIT_TFR_CALC
        //                ,RIT_TFR_EFFLQ
        //                ,RIT_TFR_DFZ
        //                ,CNBT_INPSTFM_CALC
        //                ,CNBT_INPSTFM_EFFLQ
        //                ,CNBT_INPSTFM_DFZ
        //                ,ICNB_INPSTFM_CALC
        //                ,ICNB_INPSTFM_EFFLQ
        //                ,ICNB_INPSTFM_DFZ
        //                ,CNDE_END2000_CALC
        //                ,CNDE_END2000_EFFLQ
        //                ,CNDE_END2000_DFZ
        //                ,CNDE_END2006_CALC
        //                ,CNDE_END2006_EFFLQ
        //                ,CNDE_END2006_DFZ
        //                ,CNDE_DAL2007_CALC
        //                ,CNDE_DAL2007_EFFLQ
        //                ,CNDE_DAL2007_DFZ
        //                ,AA_CNBZ_END2000_EF
        //                ,AA_CNBZ_END2006_EF
        //                ,AA_CNBZ_DAL2007_EF
        //                ,MM_CNBZ_END2000_EF
        //                ,MM_CNBZ_END2006_EF
        //                ,MM_CNBZ_DAL2007_EF
        //                ,IMPST_DA_RIMB_EFF
        //                ,ALQ_TAX_SEP_CALC
        //                ,ALQ_TAX_SEP_EFFLQ
        //                ,ALQ_TAX_SEP_DFZ
        //                ,ALQ_CNBT_INPSTFM_C
        //                ,ALQ_CNBT_INPSTFM_E
        //                ,ALQ_CNBT_INPSTFM_D
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,IMPB_VIS_1382011C
        //                ,IMPB_VIS_1382011D
        //                ,IMPB_VIS_1382011L
        //                ,IMPST_VIS_1382011C
        //                ,IMPST_VIS_1382011D
        //                ,IMPST_VIS_1382011L
        //                ,IMPB_IS_1382011C
        //                ,IMPB_IS_1382011D
        //                ,IMPB_IS_1382011L
        //                ,IS_1382011C
        //                ,IS_1382011D
        //                ,IS_1382011L
        //                ,IMP_INTR_RIT_PAG_C
        //                ,IMP_INTR_RIT_PAG_D
        //                ,IMP_INTR_RIT_PAG_L
        //                ,IMPB_BOLLO_DETT_C
        //                ,IMPB_BOLLO_DETT_D
        //                ,IMPB_BOLLO_DETT_L
        //                ,IMPST_BOLLO_DETT_C
        //                ,IMPST_BOLLO_DETT_D
        //                ,IMPST_BOLLO_DETT_L
        //                ,IMPST_BOLLO_TOT_VC
        //                ,IMPST_BOLLO_TOT_VD
        //                ,IMPST_BOLLO_TOT_VL
        //                ,IMPB_VIS_662014C
        //                ,IMPB_VIS_662014D
        //                ,IMPB_VIS_662014L
        //                ,IMPST_VIS_662014C
        //                ,IMPST_VIS_662014D
        //                ,IMPST_VIS_662014L
        //                ,IMPB_IS_662014C
        //                ,IMPB_IS_662014D
        //                ,IMPB_IS_662014L
        //                ,IS_662014C
        //                ,IS_662014D
        //                ,IS_662014L
        //             INTO
        //                :DFL-ID-D-FORZ-LIQ
        //               ,:DFL-ID-LIQ
        //               ,:DFL-ID-MOVI-CRZ
        //               ,:DFL-ID-MOVI-CHIU
        //                :IND-DFL-ID-MOVI-CHIU
        //               ,:DFL-COD-COMP-ANIA
        //               ,:DFL-DT-INI-EFF-DB
        //               ,:DFL-DT-END-EFF-DB
        //               ,:DFL-IMP-LRD-CALC
        //                :IND-DFL-IMP-LRD-CALC
        //               ,:DFL-IMP-LRD-DFZ
        //                :IND-DFL-IMP-LRD-DFZ
        //               ,:DFL-IMP-LRD-EFFLQ
        //                :IND-DFL-IMP-LRD-EFFLQ
        //               ,:DFL-IMP-NET-CALC
        //                :IND-DFL-IMP-NET-CALC
        //               ,:DFL-IMP-NET-DFZ
        //                :IND-DFL-IMP-NET-DFZ
        //               ,:DFL-IMP-NET-EFFLQ
        //                :IND-DFL-IMP-NET-EFFLQ
        //               ,:DFL-IMPST-PRVR-CALC
        //                :IND-DFL-IMPST-PRVR-CALC
        //               ,:DFL-IMPST-PRVR-DFZ
        //                :IND-DFL-IMPST-PRVR-DFZ
        //               ,:DFL-IMPST-PRVR-EFFLQ
        //                :IND-DFL-IMPST-PRVR-EFFLQ
        //               ,:DFL-IMPST-VIS-CALC
        //                :IND-DFL-IMPST-VIS-CALC
        //               ,:DFL-IMPST-VIS-DFZ
        //                :IND-DFL-IMPST-VIS-DFZ
        //               ,:DFL-IMPST-VIS-EFFLQ
        //                :IND-DFL-IMPST-VIS-EFFLQ
        //               ,:DFL-RIT-ACC-CALC
        //                :IND-DFL-RIT-ACC-CALC
        //               ,:DFL-RIT-ACC-DFZ
        //                :IND-DFL-RIT-ACC-DFZ
        //               ,:DFL-RIT-ACC-EFFLQ
        //                :IND-DFL-RIT-ACC-EFFLQ
        //               ,:DFL-RIT-IRPEF-CALC
        //                :IND-DFL-RIT-IRPEF-CALC
        //               ,:DFL-RIT-IRPEF-DFZ
        //                :IND-DFL-RIT-IRPEF-DFZ
        //               ,:DFL-RIT-IRPEF-EFFLQ
        //                :IND-DFL-RIT-IRPEF-EFFLQ
        //               ,:DFL-IMPST-SOST-CALC
        //                :IND-DFL-IMPST-SOST-CALC
        //               ,:DFL-IMPST-SOST-DFZ
        //                :IND-DFL-IMPST-SOST-DFZ
        //               ,:DFL-IMPST-SOST-EFFLQ
        //                :IND-DFL-IMPST-SOST-EFFLQ
        //               ,:DFL-TAX-SEP-CALC
        //                :IND-DFL-TAX-SEP-CALC
        //               ,:DFL-TAX-SEP-DFZ
        //                :IND-DFL-TAX-SEP-DFZ
        //               ,:DFL-TAX-SEP-EFFLQ
        //                :IND-DFL-TAX-SEP-EFFLQ
        //               ,:DFL-INTR-PREST-CALC
        //                :IND-DFL-INTR-PREST-CALC
        //               ,:DFL-INTR-PREST-DFZ
        //                :IND-DFL-INTR-PREST-DFZ
        //               ,:DFL-INTR-PREST-EFFLQ
        //                :IND-DFL-INTR-PREST-EFFLQ
        //               ,:DFL-ACCPRE-SOST-CALC
        //                :IND-DFL-ACCPRE-SOST-CALC
        //               ,:DFL-ACCPRE-SOST-DFZ
        //                :IND-DFL-ACCPRE-SOST-DFZ
        //               ,:DFL-ACCPRE-SOST-EFFLQ
        //                :IND-DFL-ACCPRE-SOST-EFFLQ
        //               ,:DFL-ACCPRE-VIS-CALC
        //                :IND-DFL-ACCPRE-VIS-CALC
        //               ,:DFL-ACCPRE-VIS-DFZ
        //                :IND-DFL-ACCPRE-VIS-DFZ
        //               ,:DFL-ACCPRE-VIS-EFFLQ
        //                :IND-DFL-ACCPRE-VIS-EFFLQ
        //               ,:DFL-ACCPRE-ACC-CALC
        //                :IND-DFL-ACCPRE-ACC-CALC
        //               ,:DFL-ACCPRE-ACC-DFZ
        //                :IND-DFL-ACCPRE-ACC-DFZ
        //               ,:DFL-ACCPRE-ACC-EFFLQ
        //                :IND-DFL-ACCPRE-ACC-EFFLQ
        //               ,:DFL-RES-PRSTZ-CALC
        //                :IND-DFL-RES-PRSTZ-CALC
        //               ,:DFL-RES-PRSTZ-DFZ
        //                :IND-DFL-RES-PRSTZ-DFZ
        //               ,:DFL-RES-PRSTZ-EFFLQ
        //                :IND-DFL-RES-PRSTZ-EFFLQ
        //               ,:DFL-RES-PRE-ATT-CALC
        //                :IND-DFL-RES-PRE-ATT-CALC
        //               ,:DFL-RES-PRE-ATT-DFZ
        //                :IND-DFL-RES-PRE-ATT-DFZ
        //               ,:DFL-RES-PRE-ATT-EFFLQ
        //                :IND-DFL-RES-PRE-ATT-EFFLQ
        //               ,:DFL-IMP-EXCONTR-EFF
        //                :IND-DFL-IMP-EXCONTR-EFF
        //               ,:DFL-IMPB-VIS-CALC
        //                :IND-DFL-IMPB-VIS-CALC
        //               ,:DFL-IMPB-VIS-EFFLQ
        //                :IND-DFL-IMPB-VIS-EFFLQ
        //               ,:DFL-IMPB-VIS-DFZ
        //                :IND-DFL-IMPB-VIS-DFZ
        //               ,:DFL-IMPB-RIT-ACC-CALC
        //                :IND-DFL-IMPB-RIT-ACC-CALC
        //               ,:DFL-IMPB-RIT-ACC-EFFLQ
        //                :IND-DFL-IMPB-RIT-ACC-EFFLQ
        //               ,:DFL-IMPB-RIT-ACC-DFZ
        //                :IND-DFL-IMPB-RIT-ACC-DFZ
        //               ,:DFL-IMPB-TFR-CALC
        //                :IND-DFL-IMPB-TFR-CALC
        //               ,:DFL-IMPB-TFR-EFFLQ
        //                :IND-DFL-IMPB-TFR-EFFLQ
        //               ,:DFL-IMPB-TFR-DFZ
        //                :IND-DFL-IMPB-TFR-DFZ
        //               ,:DFL-IMPB-IS-CALC
        //                :IND-DFL-IMPB-IS-CALC
        //               ,:DFL-IMPB-IS-EFFLQ
        //                :IND-DFL-IMPB-IS-EFFLQ
        //               ,:DFL-IMPB-IS-DFZ
        //                :IND-DFL-IMPB-IS-DFZ
        //               ,:DFL-IMPB-TAX-SEP-CALC
        //                :IND-DFL-IMPB-TAX-SEP-CALC
        //               ,:DFL-IMPB-TAX-SEP-EFFLQ
        //                :IND-DFL-IMPB-TAX-SEP-EFFLQ
        //               ,:DFL-IMPB-TAX-SEP-DFZ
        //                :IND-DFL-IMPB-TAX-SEP-DFZ
        //               ,:DFL-IINT-PREST-CALC
        //                :IND-DFL-IINT-PREST-CALC
        //               ,:DFL-IINT-PREST-EFFLQ
        //                :IND-DFL-IINT-PREST-EFFLQ
        //               ,:DFL-IINT-PREST-DFZ
        //                :IND-DFL-IINT-PREST-DFZ
        //               ,:DFL-MONT-END2000-CALC
        //                :IND-DFL-MONT-END2000-CALC
        //               ,:DFL-MONT-END2000-EFFLQ
        //                :IND-DFL-MONT-END2000-EFFLQ
        //               ,:DFL-MONT-END2000-DFZ
        //                :IND-DFL-MONT-END2000-DFZ
        //               ,:DFL-MONT-END2006-CALC
        //                :IND-DFL-MONT-END2006-CALC
        //               ,:DFL-MONT-END2006-EFFLQ
        //                :IND-DFL-MONT-END2006-EFFLQ
        //               ,:DFL-MONT-END2006-DFZ
        //                :IND-DFL-MONT-END2006-DFZ
        //               ,:DFL-MONT-DAL2007-CALC
        //                :IND-DFL-MONT-DAL2007-CALC
        //               ,:DFL-MONT-DAL2007-EFFLQ
        //                :IND-DFL-MONT-DAL2007-EFFLQ
        //               ,:DFL-MONT-DAL2007-DFZ
        //                :IND-DFL-MONT-DAL2007-DFZ
        //               ,:DFL-IIMPST-PRVR-CALC
        //                :IND-DFL-IIMPST-PRVR-CALC
        //               ,:DFL-IIMPST-PRVR-EFFLQ
        //                :IND-DFL-IIMPST-PRVR-EFFLQ
        //               ,:DFL-IIMPST-PRVR-DFZ
        //                :IND-DFL-IIMPST-PRVR-DFZ
        //               ,:DFL-IIMPST-252-CALC
        //                :IND-DFL-IIMPST-252-CALC
        //               ,:DFL-IIMPST-252-EFFLQ
        //                :IND-DFL-IIMPST-252-EFFLQ
        //               ,:DFL-IIMPST-252-DFZ
        //                :IND-DFL-IIMPST-252-DFZ
        //               ,:DFL-IMPST-252-CALC
        //                :IND-DFL-IMPST-252-CALC
        //               ,:DFL-IMPST-252-EFFLQ
        //                :IND-DFL-IMPST-252-EFFLQ
        //               ,:DFL-RIT-TFR-CALC
        //                :IND-DFL-RIT-TFR-CALC
        //               ,:DFL-RIT-TFR-EFFLQ
        //                :IND-DFL-RIT-TFR-EFFLQ
        //               ,:DFL-RIT-TFR-DFZ
        //                :IND-DFL-RIT-TFR-DFZ
        //               ,:DFL-CNBT-INPSTFM-CALC
        //                :IND-DFL-CNBT-INPSTFM-CALC
        //               ,:DFL-CNBT-INPSTFM-EFFLQ
        //                :IND-DFL-CNBT-INPSTFM-EFFLQ
        //               ,:DFL-CNBT-INPSTFM-DFZ
        //                :IND-DFL-CNBT-INPSTFM-DFZ
        //               ,:DFL-ICNB-INPSTFM-CALC
        //                :IND-DFL-ICNB-INPSTFM-CALC
        //               ,:DFL-ICNB-INPSTFM-EFFLQ
        //                :IND-DFL-ICNB-INPSTFM-EFFLQ
        //               ,:DFL-ICNB-INPSTFM-DFZ
        //                :IND-DFL-ICNB-INPSTFM-DFZ
        //               ,:DFL-CNDE-END2000-CALC
        //                :IND-DFL-CNDE-END2000-CALC
        //               ,:DFL-CNDE-END2000-EFFLQ
        //                :IND-DFL-CNDE-END2000-EFFLQ
        //               ,:DFL-CNDE-END2000-DFZ
        //                :IND-DFL-CNDE-END2000-DFZ
        //               ,:DFL-CNDE-END2006-CALC
        //                :IND-DFL-CNDE-END2006-CALC
        //               ,:DFL-CNDE-END2006-EFFLQ
        //                :IND-DFL-CNDE-END2006-EFFLQ
        //               ,:DFL-CNDE-END2006-DFZ
        //                :IND-DFL-CNDE-END2006-DFZ
        //               ,:DFL-CNDE-DAL2007-CALC
        //                :IND-DFL-CNDE-DAL2007-CALC
        //               ,:DFL-CNDE-DAL2007-EFFLQ
        //                :IND-DFL-CNDE-DAL2007-EFFLQ
        //               ,:DFL-CNDE-DAL2007-DFZ
        //                :IND-DFL-CNDE-DAL2007-DFZ
        //               ,:DFL-AA-CNBZ-END2000-EF
        //                :IND-DFL-AA-CNBZ-END2000-EF
        //               ,:DFL-AA-CNBZ-END2006-EF
        //                :IND-DFL-AA-CNBZ-END2006-EF
        //               ,:DFL-AA-CNBZ-DAL2007-EF
        //                :IND-DFL-AA-CNBZ-DAL2007-EF
        //               ,:DFL-MM-CNBZ-END2000-EF
        //                :IND-DFL-MM-CNBZ-END2000-EF
        //               ,:DFL-MM-CNBZ-END2006-EF
        //                :IND-DFL-MM-CNBZ-END2006-EF
        //               ,:DFL-MM-CNBZ-DAL2007-EF
        //                :IND-DFL-MM-CNBZ-DAL2007-EF
        //               ,:DFL-IMPST-DA-RIMB-EFF
        //                :IND-DFL-IMPST-DA-RIMB-EFF
        //               ,:DFL-ALQ-TAX-SEP-CALC
        //                :IND-DFL-ALQ-TAX-SEP-CALC
        //               ,:DFL-ALQ-TAX-SEP-EFFLQ
        //                :IND-DFL-ALQ-TAX-SEP-EFFLQ
        //               ,:DFL-ALQ-TAX-SEP-DFZ
        //                :IND-DFL-ALQ-TAX-SEP-DFZ
        //               ,:DFL-ALQ-CNBT-INPSTFM-C
        //                :IND-DFL-ALQ-CNBT-INPSTFM-C
        //               ,:DFL-ALQ-CNBT-INPSTFM-E
        //                :IND-DFL-ALQ-CNBT-INPSTFM-E
        //               ,:DFL-ALQ-CNBT-INPSTFM-D
        //                :IND-DFL-ALQ-CNBT-INPSTFM-D
        //               ,:DFL-DS-RIGA
        //               ,:DFL-DS-OPER-SQL
        //               ,:DFL-DS-VER
        //               ,:DFL-DS-TS-INI-CPTZ
        //               ,:DFL-DS-TS-END-CPTZ
        //               ,:DFL-DS-UTENTE
        //               ,:DFL-DS-STATO-ELAB
        //               ,:DFL-IMPB-VIS-1382011C
        //                :IND-DFL-IMPB-VIS-1382011C
        //               ,:DFL-IMPB-VIS-1382011D
        //                :IND-DFL-IMPB-VIS-1382011D
        //               ,:DFL-IMPB-VIS-1382011L
        //                :IND-DFL-IMPB-VIS-1382011L
        //               ,:DFL-IMPST-VIS-1382011C
        //                :IND-DFL-IMPST-VIS-1382011C
        //               ,:DFL-IMPST-VIS-1382011D
        //                :IND-DFL-IMPST-VIS-1382011D
        //               ,:DFL-IMPST-VIS-1382011L
        //                :IND-DFL-IMPST-VIS-1382011L
        //               ,:DFL-IMPB-IS-1382011C
        //                :IND-DFL-IMPB-IS-1382011C
        //               ,:DFL-IMPB-IS-1382011D
        //                :IND-DFL-IMPB-IS-1382011D
        //               ,:DFL-IMPB-IS-1382011L
        //                :IND-DFL-IMPB-IS-1382011L
        //               ,:DFL-IS-1382011C
        //                :IND-DFL-IS-1382011C
        //               ,:DFL-IS-1382011D
        //                :IND-DFL-IS-1382011D
        //               ,:DFL-IS-1382011L
        //                :IND-DFL-IS-1382011L
        //               ,:DFL-IMP-INTR-RIT-PAG-C
        //                :IND-DFL-IMP-INTR-RIT-PAG-C
        //               ,:DFL-IMP-INTR-RIT-PAG-D
        //                :IND-DFL-IMP-INTR-RIT-PAG-D
        //               ,:DFL-IMP-INTR-RIT-PAG-L
        //                :IND-DFL-IMP-INTR-RIT-PAG-L
        //               ,:DFL-IMPB-BOLLO-DETT-C
        //                :IND-DFL-IMPB-BOLLO-DETT-C
        //               ,:DFL-IMPB-BOLLO-DETT-D
        //                :IND-DFL-IMPB-BOLLO-DETT-D
        //               ,:DFL-IMPB-BOLLO-DETT-L
        //                :IND-DFL-IMPB-BOLLO-DETT-L
        //               ,:DFL-IMPST-BOLLO-DETT-C
        //                :IND-DFL-IMPST-BOLLO-DETT-C
        //               ,:DFL-IMPST-BOLLO-DETT-D
        //                :IND-DFL-IMPST-BOLLO-DETT-D
        //               ,:DFL-IMPST-BOLLO-DETT-L
        //                :IND-DFL-IMPST-BOLLO-DETT-L
        //               ,:DFL-IMPST-BOLLO-TOT-VC
        //                :IND-DFL-IMPST-BOLLO-TOT-VC
        //               ,:DFL-IMPST-BOLLO-TOT-VD
        //                :IND-DFL-IMPST-BOLLO-TOT-VD
        //               ,:DFL-IMPST-BOLLO-TOT-VL
        //                :IND-DFL-IMPST-BOLLO-TOT-VL
        //               ,:DFL-IMPB-VIS-662014C
        //                :IND-DFL-IMPB-VIS-662014C
        //               ,:DFL-IMPB-VIS-662014D
        //                :IND-DFL-IMPB-VIS-662014D
        //               ,:DFL-IMPB-VIS-662014L
        //                :IND-DFL-IMPB-VIS-662014L
        //               ,:DFL-IMPST-VIS-662014C
        //                :IND-DFL-IMPST-VIS-662014C
        //               ,:DFL-IMPST-VIS-662014D
        //                :IND-DFL-IMPST-VIS-662014D
        //               ,:DFL-IMPST-VIS-662014L
        //                :IND-DFL-IMPST-VIS-662014L
        //               ,:DFL-IMPB-IS-662014C
        //                :IND-DFL-IMPB-IS-662014C
        //               ,:DFL-IMPB-IS-662014D
        //                :IND-DFL-IMPB-IS-662014D
        //               ,:DFL-IMPB-IS-662014L
        //                :IND-DFL-IMPB-IS-662014L
        //               ,:DFL-IS-662014C
        //                :IND-DFL-IS-662014C
        //               ,:DFL-IS-662014D
        //                :IND-DFL-IS-662014D
        //               ,:DFL-IS-662014L
        //                :IND-DFL-IS-662014L
        //             FROM D_FORZ_LIQ
        //             WHERE     ID_D_FORZ_LIQ = :DFL-ID-D-FORZ-LIQ
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        dForzLiqDao.selectRec(dForzLiq.getDflIdDForzLiq(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A330-UPDATE-ID-EFF<br>*/
    private void a330UpdateIdEff() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE D_FORZ_LIQ SET
        //                   ID_D_FORZ_LIQ          =
        //                :DFL-ID-D-FORZ-LIQ
        //                  ,ID_LIQ                 =
        //                :DFL-ID-LIQ
        //                  ,ID_MOVI_CRZ            =
        //                :DFL-ID-MOVI-CRZ
        //                  ,ID_MOVI_CHIU           =
        //                :DFL-ID-MOVI-CHIU
        //                                       :IND-DFL-ID-MOVI-CHIU
        //                  ,COD_COMP_ANIA          =
        //                :DFL-COD-COMP-ANIA
        //                  ,DT_INI_EFF             =
        //           :DFL-DT-INI-EFF-DB
        //                  ,DT_END_EFF             =
        //           :DFL-DT-END-EFF-DB
        //                  ,IMP_LRD_CALC           =
        //                :DFL-IMP-LRD-CALC
        //                                       :IND-DFL-IMP-LRD-CALC
        //                  ,IMP_LRD_DFZ            =
        //                :DFL-IMP-LRD-DFZ
        //                                       :IND-DFL-IMP-LRD-DFZ
        //                  ,IMP_LRD_EFFLQ          =
        //                :DFL-IMP-LRD-EFFLQ
        //                                       :IND-DFL-IMP-LRD-EFFLQ
        //                  ,IMP_NET_CALC           =
        //                :DFL-IMP-NET-CALC
        //                                       :IND-DFL-IMP-NET-CALC
        //                  ,IMP_NET_DFZ            =
        //                :DFL-IMP-NET-DFZ
        //                                       :IND-DFL-IMP-NET-DFZ
        //                  ,IMP_NET_EFFLQ          =
        //                :DFL-IMP-NET-EFFLQ
        //                                       :IND-DFL-IMP-NET-EFFLQ
        //                  ,IMPST_PRVR_CALC        =
        //                :DFL-IMPST-PRVR-CALC
        //                                       :IND-DFL-IMPST-PRVR-CALC
        //                  ,IMPST_PRVR_DFZ         =
        //                :DFL-IMPST-PRVR-DFZ
        //                                       :IND-DFL-IMPST-PRVR-DFZ
        //                  ,IMPST_PRVR_EFFLQ       =
        //                :DFL-IMPST-PRVR-EFFLQ
        //                                       :IND-DFL-IMPST-PRVR-EFFLQ
        //                  ,IMPST_VIS_CALC         =
        //                :DFL-IMPST-VIS-CALC
        //                                       :IND-DFL-IMPST-VIS-CALC
        //                  ,IMPST_VIS_DFZ          =
        //                :DFL-IMPST-VIS-DFZ
        //                                       :IND-DFL-IMPST-VIS-DFZ
        //                  ,IMPST_VIS_EFFLQ        =
        //                :DFL-IMPST-VIS-EFFLQ
        //                                       :IND-DFL-IMPST-VIS-EFFLQ
        //                  ,RIT_ACC_CALC           =
        //                :DFL-RIT-ACC-CALC
        //                                       :IND-DFL-RIT-ACC-CALC
        //                  ,RIT_ACC_DFZ            =
        //                :DFL-RIT-ACC-DFZ
        //                                       :IND-DFL-RIT-ACC-DFZ
        //                  ,RIT_ACC_EFFLQ          =
        //                :DFL-RIT-ACC-EFFLQ
        //                                       :IND-DFL-RIT-ACC-EFFLQ
        //                  ,RIT_IRPEF_CALC         =
        //                :DFL-RIT-IRPEF-CALC
        //                                       :IND-DFL-RIT-IRPEF-CALC
        //                  ,RIT_IRPEF_DFZ          =
        //                :DFL-RIT-IRPEF-DFZ
        //                                       :IND-DFL-RIT-IRPEF-DFZ
        //                  ,RIT_IRPEF_EFFLQ        =
        //                :DFL-RIT-IRPEF-EFFLQ
        //                                       :IND-DFL-RIT-IRPEF-EFFLQ
        //                  ,IMPST_SOST_CALC        =
        //                :DFL-IMPST-SOST-CALC
        //                                       :IND-DFL-IMPST-SOST-CALC
        //                  ,IMPST_SOST_DFZ         =
        //                :DFL-IMPST-SOST-DFZ
        //                                       :IND-DFL-IMPST-SOST-DFZ
        //                  ,IMPST_SOST_EFFLQ       =
        //                :DFL-IMPST-SOST-EFFLQ
        //                                       :IND-DFL-IMPST-SOST-EFFLQ
        //                  ,TAX_SEP_CALC           =
        //                :DFL-TAX-SEP-CALC
        //                                       :IND-DFL-TAX-SEP-CALC
        //                  ,TAX_SEP_DFZ            =
        //                :DFL-TAX-SEP-DFZ
        //                                       :IND-DFL-TAX-SEP-DFZ
        //                  ,TAX_SEP_EFFLQ          =
        //                :DFL-TAX-SEP-EFFLQ
        //                                       :IND-DFL-TAX-SEP-EFFLQ
        //                  ,INTR_PREST_CALC        =
        //                :DFL-INTR-PREST-CALC
        //                                       :IND-DFL-INTR-PREST-CALC
        //                  ,INTR_PREST_DFZ         =
        //                :DFL-INTR-PREST-DFZ
        //                                       :IND-DFL-INTR-PREST-DFZ
        //                  ,INTR_PREST_EFFLQ       =
        //                :DFL-INTR-PREST-EFFLQ
        //                                       :IND-DFL-INTR-PREST-EFFLQ
        //                  ,ACCPRE_SOST_CALC       =
        //                :DFL-ACCPRE-SOST-CALC
        //                                       :IND-DFL-ACCPRE-SOST-CALC
        //                  ,ACCPRE_SOST_DFZ        =
        //                :DFL-ACCPRE-SOST-DFZ
        //                                       :IND-DFL-ACCPRE-SOST-DFZ
        //                  ,ACCPRE_SOST_EFFLQ      =
        //                :DFL-ACCPRE-SOST-EFFLQ
        //                                       :IND-DFL-ACCPRE-SOST-EFFLQ
        //                  ,ACCPRE_VIS_CALC        =
        //                :DFL-ACCPRE-VIS-CALC
        //                                       :IND-DFL-ACCPRE-VIS-CALC
        //                  ,ACCPRE_VIS_DFZ         =
        //                :DFL-ACCPRE-VIS-DFZ
        //                                       :IND-DFL-ACCPRE-VIS-DFZ
        //                  ,ACCPRE_VIS_EFFLQ       =
        //                :DFL-ACCPRE-VIS-EFFLQ
        //                                       :IND-DFL-ACCPRE-VIS-EFFLQ
        //                  ,ACCPRE_ACC_CALC        =
        //                :DFL-ACCPRE-ACC-CALC
        //                                       :IND-DFL-ACCPRE-ACC-CALC
        //                  ,ACCPRE_ACC_DFZ         =
        //                :DFL-ACCPRE-ACC-DFZ
        //                                       :IND-DFL-ACCPRE-ACC-DFZ
        //                  ,ACCPRE_ACC_EFFLQ       =
        //                :DFL-ACCPRE-ACC-EFFLQ
        //                                       :IND-DFL-ACCPRE-ACC-EFFLQ
        //                  ,RES_PRSTZ_CALC         =
        //                :DFL-RES-PRSTZ-CALC
        //                                       :IND-DFL-RES-PRSTZ-CALC
        //                  ,RES_PRSTZ_DFZ          =
        //                :DFL-RES-PRSTZ-DFZ
        //                                       :IND-DFL-RES-PRSTZ-DFZ
        //                  ,RES_PRSTZ_EFFLQ        =
        //                :DFL-RES-PRSTZ-EFFLQ
        //                                       :IND-DFL-RES-PRSTZ-EFFLQ
        //                  ,RES_PRE_ATT_CALC       =
        //                :DFL-RES-PRE-ATT-CALC
        //                                       :IND-DFL-RES-PRE-ATT-CALC
        //                  ,RES_PRE_ATT_DFZ        =
        //                :DFL-RES-PRE-ATT-DFZ
        //                                       :IND-DFL-RES-PRE-ATT-DFZ
        //                  ,RES_PRE_ATT_EFFLQ      =
        //                :DFL-RES-PRE-ATT-EFFLQ
        //                                       :IND-DFL-RES-PRE-ATT-EFFLQ
        //                  ,IMP_EXCONTR_EFF        =
        //                :DFL-IMP-EXCONTR-EFF
        //                                       :IND-DFL-IMP-EXCONTR-EFF
        //                  ,IMPB_VIS_CALC          =
        //                :DFL-IMPB-VIS-CALC
        //                                       :IND-DFL-IMPB-VIS-CALC
        //                  ,IMPB_VIS_EFFLQ         =
        //                :DFL-IMPB-VIS-EFFLQ
        //                                       :IND-DFL-IMPB-VIS-EFFLQ
        //                  ,IMPB_VIS_DFZ           =
        //                :DFL-IMPB-VIS-DFZ
        //                                       :IND-DFL-IMPB-VIS-DFZ
        //                  ,IMPB_RIT_ACC_CALC      =
        //                :DFL-IMPB-RIT-ACC-CALC
        //                                       :IND-DFL-IMPB-RIT-ACC-CALC
        //                  ,IMPB_RIT_ACC_EFFLQ     =
        //                :DFL-IMPB-RIT-ACC-EFFLQ
        //                                       :IND-DFL-IMPB-RIT-ACC-EFFLQ
        //                  ,IMPB_RIT_ACC_DFZ       =
        //                :DFL-IMPB-RIT-ACC-DFZ
        //                                       :IND-DFL-IMPB-RIT-ACC-DFZ
        //                  ,IMPB_TFR_CALC          =
        //                :DFL-IMPB-TFR-CALC
        //                                       :IND-DFL-IMPB-TFR-CALC
        //                  ,IMPB_TFR_EFFLQ         =
        //                :DFL-IMPB-TFR-EFFLQ
        //                                       :IND-DFL-IMPB-TFR-EFFLQ
        //                  ,IMPB_TFR_DFZ           =
        //                :DFL-IMPB-TFR-DFZ
        //                                       :IND-DFL-IMPB-TFR-DFZ
        //                  ,IMPB_IS_CALC           =
        //                :DFL-IMPB-IS-CALC
        //                                       :IND-DFL-IMPB-IS-CALC
        //                  ,IMPB_IS_EFFLQ          =
        //                :DFL-IMPB-IS-EFFLQ
        //                                       :IND-DFL-IMPB-IS-EFFLQ
        //                  ,IMPB_IS_DFZ            =
        //                :DFL-IMPB-IS-DFZ
        //                                       :IND-DFL-IMPB-IS-DFZ
        //                  ,IMPB_TAX_SEP_CALC      =
        //                :DFL-IMPB-TAX-SEP-CALC
        //                                       :IND-DFL-IMPB-TAX-SEP-CALC
        //                  ,IMPB_TAX_SEP_EFFLQ     =
        //                :DFL-IMPB-TAX-SEP-EFFLQ
        //                                       :IND-DFL-IMPB-TAX-SEP-EFFLQ
        //                  ,IMPB_TAX_SEP_DFZ       =
        //                :DFL-IMPB-TAX-SEP-DFZ
        //                                       :IND-DFL-IMPB-TAX-SEP-DFZ
        //                  ,IINT_PREST_CALC        =
        //                :DFL-IINT-PREST-CALC
        //                                       :IND-DFL-IINT-PREST-CALC
        //                  ,IINT_PREST_EFFLQ       =
        //                :DFL-IINT-PREST-EFFLQ
        //                                       :IND-DFL-IINT-PREST-EFFLQ
        //                  ,IINT_PREST_DFZ         =
        //                :DFL-IINT-PREST-DFZ
        //                                       :IND-DFL-IINT-PREST-DFZ
        //                  ,MONT_END2000_CALC      =
        //                :DFL-MONT-END2000-CALC
        //                                       :IND-DFL-MONT-END2000-CALC
        //                  ,MONT_END2000_EFFLQ     =
        //                :DFL-MONT-END2000-EFFLQ
        //                                       :IND-DFL-MONT-END2000-EFFLQ
        //                  ,MONT_END2000_DFZ       =
        //                :DFL-MONT-END2000-DFZ
        //                                       :IND-DFL-MONT-END2000-DFZ
        //                  ,MONT_END2006_CALC      =
        //                :DFL-MONT-END2006-CALC
        //                                       :IND-DFL-MONT-END2006-CALC
        //                  ,MONT_END2006_EFFLQ     =
        //                :DFL-MONT-END2006-EFFLQ
        //                                       :IND-DFL-MONT-END2006-EFFLQ
        //                  ,MONT_END2006_DFZ       =
        //                :DFL-MONT-END2006-DFZ
        //                                       :IND-DFL-MONT-END2006-DFZ
        //                  ,MONT_DAL2007_CALC      =
        //                :DFL-MONT-DAL2007-CALC
        //                                       :IND-DFL-MONT-DAL2007-CALC
        //                  ,MONT_DAL2007_EFFLQ     =
        //                :DFL-MONT-DAL2007-EFFLQ
        //                                       :IND-DFL-MONT-DAL2007-EFFLQ
        //                  ,MONT_DAL2007_DFZ       =
        //                :DFL-MONT-DAL2007-DFZ
        //                                       :IND-DFL-MONT-DAL2007-DFZ
        //                  ,IIMPST_PRVR_CALC       =
        //                :DFL-IIMPST-PRVR-CALC
        //                                       :IND-DFL-IIMPST-PRVR-CALC
        //                  ,IIMPST_PRVR_EFFLQ      =
        //                :DFL-IIMPST-PRVR-EFFLQ
        //                                       :IND-DFL-IIMPST-PRVR-EFFLQ
        //                  ,IIMPST_PRVR_DFZ        =
        //                :DFL-IIMPST-PRVR-DFZ
        //                                       :IND-DFL-IIMPST-PRVR-DFZ
        //                  ,IIMPST_252_CALC        =
        //                :DFL-IIMPST-252-CALC
        //                                       :IND-DFL-IIMPST-252-CALC
        //                  ,IIMPST_252_EFFLQ       =
        //                :DFL-IIMPST-252-EFFLQ
        //                                       :IND-DFL-IIMPST-252-EFFLQ
        //                  ,IIMPST_252_DFZ         =
        //                :DFL-IIMPST-252-DFZ
        //                                       :IND-DFL-IIMPST-252-DFZ
        //                  ,IMPST_252_CALC         =
        //                :DFL-IMPST-252-CALC
        //                                       :IND-DFL-IMPST-252-CALC
        //                  ,IMPST_252_EFFLQ        =
        //                :DFL-IMPST-252-EFFLQ
        //                                       :IND-DFL-IMPST-252-EFFLQ
        //                  ,RIT_TFR_CALC           =
        //                :DFL-RIT-TFR-CALC
        //                                       :IND-DFL-RIT-TFR-CALC
        //                  ,RIT_TFR_EFFLQ          =
        //                :DFL-RIT-TFR-EFFLQ
        //                                       :IND-DFL-RIT-TFR-EFFLQ
        //                  ,RIT_TFR_DFZ            =
        //                :DFL-RIT-TFR-DFZ
        //                                       :IND-DFL-RIT-TFR-DFZ
        //                  ,CNBT_INPSTFM_CALC      =
        //                :DFL-CNBT-INPSTFM-CALC
        //                                       :IND-DFL-CNBT-INPSTFM-CALC
        //                  ,CNBT_INPSTFM_EFFLQ     =
        //                :DFL-CNBT-INPSTFM-EFFLQ
        //                                       :IND-DFL-CNBT-INPSTFM-EFFLQ
        //                  ,CNBT_INPSTFM_DFZ       =
        //                :DFL-CNBT-INPSTFM-DFZ
        //                                       :IND-DFL-CNBT-INPSTFM-DFZ
        //                  ,ICNB_INPSTFM_CALC      =
        //                :DFL-ICNB-INPSTFM-CALC
        //                                       :IND-DFL-ICNB-INPSTFM-CALC
        //                  ,ICNB_INPSTFM_EFFLQ     =
        //                :DFL-ICNB-INPSTFM-EFFLQ
        //                                       :IND-DFL-ICNB-INPSTFM-EFFLQ
        //                  ,ICNB_INPSTFM_DFZ       =
        //                :DFL-ICNB-INPSTFM-DFZ
        //                                       :IND-DFL-ICNB-INPSTFM-DFZ
        //                  ,CNDE_END2000_CALC      =
        //                :DFL-CNDE-END2000-CALC
        //                                       :IND-DFL-CNDE-END2000-CALC
        //                  ,CNDE_END2000_EFFLQ     =
        //                :DFL-CNDE-END2000-EFFLQ
        //                                       :IND-DFL-CNDE-END2000-EFFLQ
        //                  ,CNDE_END2000_DFZ       =
        //                :DFL-CNDE-END2000-DFZ
        //                                       :IND-DFL-CNDE-END2000-DFZ
        //                  ,CNDE_END2006_CALC      =
        //                :DFL-CNDE-END2006-CALC
        //                                       :IND-DFL-CNDE-END2006-CALC
        //                  ,CNDE_END2006_EFFLQ     =
        //                :DFL-CNDE-END2006-EFFLQ
        //                                       :IND-DFL-CNDE-END2006-EFFLQ
        //                  ,CNDE_END2006_DFZ       =
        //                :DFL-CNDE-END2006-DFZ
        //                                       :IND-DFL-CNDE-END2006-DFZ
        //                  ,CNDE_DAL2007_CALC      =
        //                :DFL-CNDE-DAL2007-CALC
        //                                       :IND-DFL-CNDE-DAL2007-CALC
        //                  ,CNDE_DAL2007_EFFLQ     =
        //                :DFL-CNDE-DAL2007-EFFLQ
        //                                       :IND-DFL-CNDE-DAL2007-EFFLQ
        //                  ,CNDE_DAL2007_DFZ       =
        //                :DFL-CNDE-DAL2007-DFZ
        //                                       :IND-DFL-CNDE-DAL2007-DFZ
        //                  ,AA_CNBZ_END2000_EF     =
        //                :DFL-AA-CNBZ-END2000-EF
        //                                       :IND-DFL-AA-CNBZ-END2000-EF
        //                  ,AA_CNBZ_END2006_EF     =
        //                :DFL-AA-CNBZ-END2006-EF
        //                                       :IND-DFL-AA-CNBZ-END2006-EF
        //                  ,AA_CNBZ_DAL2007_EF     =
        //                :DFL-AA-CNBZ-DAL2007-EF
        //                                       :IND-DFL-AA-CNBZ-DAL2007-EF
        //                  ,MM_CNBZ_END2000_EF     =
        //                :DFL-MM-CNBZ-END2000-EF
        //                                       :IND-DFL-MM-CNBZ-END2000-EF
        //                  ,MM_CNBZ_END2006_EF     =
        //                :DFL-MM-CNBZ-END2006-EF
        //                                       :IND-DFL-MM-CNBZ-END2006-EF
        //                  ,MM_CNBZ_DAL2007_EF     =
        //                :DFL-MM-CNBZ-DAL2007-EF
        //                                       :IND-DFL-MM-CNBZ-DAL2007-EF
        //                  ,IMPST_DA_RIMB_EFF      =
        //                :DFL-IMPST-DA-RIMB-EFF
        //                                       :IND-DFL-IMPST-DA-RIMB-EFF
        //                  ,ALQ_TAX_SEP_CALC       =
        //                :DFL-ALQ-TAX-SEP-CALC
        //                                       :IND-DFL-ALQ-TAX-SEP-CALC
        //                  ,ALQ_TAX_SEP_EFFLQ      =
        //                :DFL-ALQ-TAX-SEP-EFFLQ
        //                                       :IND-DFL-ALQ-TAX-SEP-EFFLQ
        //                  ,ALQ_TAX_SEP_DFZ        =
        //                :DFL-ALQ-TAX-SEP-DFZ
        //                                       :IND-DFL-ALQ-TAX-SEP-DFZ
        //                  ,ALQ_CNBT_INPSTFM_C     =
        //                :DFL-ALQ-CNBT-INPSTFM-C
        //                                       :IND-DFL-ALQ-CNBT-INPSTFM-C
        //                  ,ALQ_CNBT_INPSTFM_E     =
        //                :DFL-ALQ-CNBT-INPSTFM-E
        //                                       :IND-DFL-ALQ-CNBT-INPSTFM-E
        //                  ,ALQ_CNBT_INPSTFM_D     =
        //                :DFL-ALQ-CNBT-INPSTFM-D
        //                                       :IND-DFL-ALQ-CNBT-INPSTFM-D
        //                  ,DS_RIGA                =
        //                :DFL-DS-RIGA
        //                  ,DS_OPER_SQL            =
        //                :DFL-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :DFL-DS-VER
        //                  ,DS_TS_INI_CPTZ         =
        //                :DFL-DS-TS-INI-CPTZ
        //                  ,DS_TS_END_CPTZ         =
        //                :DFL-DS-TS-END-CPTZ
        //                  ,DS_UTENTE              =
        //                :DFL-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :DFL-DS-STATO-ELAB
        //                  ,IMPB_VIS_1382011C      =
        //                :DFL-IMPB-VIS-1382011C
        //                                       :IND-DFL-IMPB-VIS-1382011C
        //                  ,IMPB_VIS_1382011D      =
        //                :DFL-IMPB-VIS-1382011D
        //                                       :IND-DFL-IMPB-VIS-1382011D
        //                  ,IMPB_VIS_1382011L      =
        //                :DFL-IMPB-VIS-1382011L
        //                                       :IND-DFL-IMPB-VIS-1382011L
        //                  ,IMPST_VIS_1382011C     =
        //                :DFL-IMPST-VIS-1382011C
        //                                       :IND-DFL-IMPST-VIS-1382011C
        //                  ,IMPST_VIS_1382011D     =
        //                :DFL-IMPST-VIS-1382011D
        //                                       :IND-DFL-IMPST-VIS-1382011D
        //                  ,IMPST_VIS_1382011L     =
        //                :DFL-IMPST-VIS-1382011L
        //                                       :IND-DFL-IMPST-VIS-1382011L
        //                  ,IMPB_IS_1382011C       =
        //                :DFL-IMPB-IS-1382011C
        //                                       :IND-DFL-IMPB-IS-1382011C
        //                  ,IMPB_IS_1382011D       =
        //                :DFL-IMPB-IS-1382011D
        //                                       :IND-DFL-IMPB-IS-1382011D
        //                  ,IMPB_IS_1382011L       =
        //                :DFL-IMPB-IS-1382011L
        //                                       :IND-DFL-IMPB-IS-1382011L
        //                  ,IS_1382011C            =
        //                :DFL-IS-1382011C
        //                                       :IND-DFL-IS-1382011C
        //                  ,IS_1382011D            =
        //                :DFL-IS-1382011D
        //                                       :IND-DFL-IS-1382011D
        //                  ,IS_1382011L            =
        //                :DFL-IS-1382011L
        //                                       :IND-DFL-IS-1382011L
        //                  ,IMP_INTR_RIT_PAG_C     =
        //                :DFL-IMP-INTR-RIT-PAG-C
        //                                       :IND-DFL-IMP-INTR-RIT-PAG-C
        //                  ,IMP_INTR_RIT_PAG_D     =
        //                :DFL-IMP-INTR-RIT-PAG-D
        //                                       :IND-DFL-IMP-INTR-RIT-PAG-D
        //                  ,IMP_INTR_RIT_PAG_L     =
        //                :DFL-IMP-INTR-RIT-PAG-L
        //                                       :IND-DFL-IMP-INTR-RIT-PAG-L
        //                  ,IMPB_BOLLO_DETT_C      =
        //                :DFL-IMPB-BOLLO-DETT-C
        //                                       :IND-DFL-IMPB-BOLLO-DETT-C
        //                  ,IMPB_BOLLO_DETT_D      =
        //                :DFL-IMPB-BOLLO-DETT-D
        //                                       :IND-DFL-IMPB-BOLLO-DETT-D
        //                  ,IMPB_BOLLO_DETT_L      =
        //                :DFL-IMPB-BOLLO-DETT-L
        //                                       :IND-DFL-IMPB-BOLLO-DETT-L
        //                  ,IMPST_BOLLO_DETT_C     =
        //                :DFL-IMPST-BOLLO-DETT-C
        //                                       :IND-DFL-IMPST-BOLLO-DETT-C
        //                  ,IMPST_BOLLO_DETT_D     =
        //                :DFL-IMPST-BOLLO-DETT-D
        //                                       :IND-DFL-IMPST-BOLLO-DETT-D
        //                  ,IMPST_BOLLO_DETT_L     =
        //                :DFL-IMPST-BOLLO-DETT-L
        //                                       :IND-DFL-IMPST-BOLLO-DETT-L
        //                  ,IMPST_BOLLO_TOT_VC     =
        //                :DFL-IMPST-BOLLO-TOT-VC
        //                                       :IND-DFL-IMPST-BOLLO-TOT-VC
        //                  ,IMPST_BOLLO_TOT_VD     =
        //                :DFL-IMPST-BOLLO-TOT-VD
        //                                       :IND-DFL-IMPST-BOLLO-TOT-VD
        //                  ,IMPST_BOLLO_TOT_VL     =
        //                :DFL-IMPST-BOLLO-TOT-VL
        //                                       :IND-DFL-IMPST-BOLLO-TOT-VL
        //                  ,IMPB_VIS_662014C       =
        //                :DFL-IMPB-VIS-662014C
        //                                       :IND-DFL-IMPB-VIS-662014C
        //                  ,IMPB_VIS_662014D       =
        //                :DFL-IMPB-VIS-662014D
        //                                       :IND-DFL-IMPB-VIS-662014D
        //                  ,IMPB_VIS_662014L       =
        //                :DFL-IMPB-VIS-662014L
        //                                       :IND-DFL-IMPB-VIS-662014L
        //                  ,IMPST_VIS_662014C      =
        //                :DFL-IMPST-VIS-662014C
        //                                       :IND-DFL-IMPST-VIS-662014C
        //                  ,IMPST_VIS_662014D      =
        //                :DFL-IMPST-VIS-662014D
        //                                       :IND-DFL-IMPST-VIS-662014D
        //                  ,IMPST_VIS_662014L      =
        //                :DFL-IMPST-VIS-662014L
        //                                       :IND-DFL-IMPST-VIS-662014L
        //                  ,IMPB_IS_662014C        =
        //                :DFL-IMPB-IS-662014C
        //                                       :IND-DFL-IMPB-IS-662014C
        //                  ,IMPB_IS_662014D        =
        //                :DFL-IMPB-IS-662014D
        //                                       :IND-DFL-IMPB-IS-662014D
        //                  ,IMPB_IS_662014L        =
        //                :DFL-IMPB-IS-662014L
        //                                       :IND-DFL-IMPB-IS-662014L
        //                  ,IS_662014C             =
        //                :DFL-IS-662014C
        //                                       :IND-DFL-IS-662014C
        //                  ,IS_662014D             =
        //                :DFL-IS-662014D
        //                                       :IND-DFL-IS-662014D
        //                  ,IS_662014L             =
        //                :DFL-IS-662014L
        //                                       :IND-DFL-IS-662014L
        //                WHERE     DS_RIGA = :DFL-DS-RIGA
        //                   AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        dForzLiqDao.updateRec1(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A360-OPEN-CURSOR-ID-EFF<br>*/
    private void a360OpenCursorIdEff() {
        // COB_CODE: PERFORM A305-DECLARE-CURSOR-ID-EFF THRU A305-EX.
        a305DeclareCursorIdEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-ID-UPD-EFF-DFL
        //           END-EXEC.
        dForzLiqDao.openCIdUpdEffDfl(dForzLiq.getDflIdDForzLiq(), ws.getIdsv0010().getWsTsInfinito(), ws.getIdsv0010().getWsDataInizioEffettoDb(), idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A370-CLOSE-CURSOR-ID-EFF<br>*/
    private void a370CloseCursorIdEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-ID-UPD-EFF-DFL
        //           END-EXEC.
        dForzLiqDao.closeCIdUpdEffDfl();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A390-FETCH-NEXT-ID-EFF<br>*/
    private void a390FetchNextIdEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-ID-UPD-EFF-DFL
        //           INTO
        //                :DFL-ID-D-FORZ-LIQ
        //               ,:DFL-ID-LIQ
        //               ,:DFL-ID-MOVI-CRZ
        //               ,:DFL-ID-MOVI-CHIU
        //                :IND-DFL-ID-MOVI-CHIU
        //               ,:DFL-COD-COMP-ANIA
        //               ,:DFL-DT-INI-EFF-DB
        //               ,:DFL-DT-END-EFF-DB
        //               ,:DFL-IMP-LRD-CALC
        //                :IND-DFL-IMP-LRD-CALC
        //               ,:DFL-IMP-LRD-DFZ
        //                :IND-DFL-IMP-LRD-DFZ
        //               ,:DFL-IMP-LRD-EFFLQ
        //                :IND-DFL-IMP-LRD-EFFLQ
        //               ,:DFL-IMP-NET-CALC
        //                :IND-DFL-IMP-NET-CALC
        //               ,:DFL-IMP-NET-DFZ
        //                :IND-DFL-IMP-NET-DFZ
        //               ,:DFL-IMP-NET-EFFLQ
        //                :IND-DFL-IMP-NET-EFFLQ
        //               ,:DFL-IMPST-PRVR-CALC
        //                :IND-DFL-IMPST-PRVR-CALC
        //               ,:DFL-IMPST-PRVR-DFZ
        //                :IND-DFL-IMPST-PRVR-DFZ
        //               ,:DFL-IMPST-PRVR-EFFLQ
        //                :IND-DFL-IMPST-PRVR-EFFLQ
        //               ,:DFL-IMPST-VIS-CALC
        //                :IND-DFL-IMPST-VIS-CALC
        //               ,:DFL-IMPST-VIS-DFZ
        //                :IND-DFL-IMPST-VIS-DFZ
        //               ,:DFL-IMPST-VIS-EFFLQ
        //                :IND-DFL-IMPST-VIS-EFFLQ
        //               ,:DFL-RIT-ACC-CALC
        //                :IND-DFL-RIT-ACC-CALC
        //               ,:DFL-RIT-ACC-DFZ
        //                :IND-DFL-RIT-ACC-DFZ
        //               ,:DFL-RIT-ACC-EFFLQ
        //                :IND-DFL-RIT-ACC-EFFLQ
        //               ,:DFL-RIT-IRPEF-CALC
        //                :IND-DFL-RIT-IRPEF-CALC
        //               ,:DFL-RIT-IRPEF-DFZ
        //                :IND-DFL-RIT-IRPEF-DFZ
        //               ,:DFL-RIT-IRPEF-EFFLQ
        //                :IND-DFL-RIT-IRPEF-EFFLQ
        //               ,:DFL-IMPST-SOST-CALC
        //                :IND-DFL-IMPST-SOST-CALC
        //               ,:DFL-IMPST-SOST-DFZ
        //                :IND-DFL-IMPST-SOST-DFZ
        //               ,:DFL-IMPST-SOST-EFFLQ
        //                :IND-DFL-IMPST-SOST-EFFLQ
        //               ,:DFL-TAX-SEP-CALC
        //                :IND-DFL-TAX-SEP-CALC
        //               ,:DFL-TAX-SEP-DFZ
        //                :IND-DFL-TAX-SEP-DFZ
        //               ,:DFL-TAX-SEP-EFFLQ
        //                :IND-DFL-TAX-SEP-EFFLQ
        //               ,:DFL-INTR-PREST-CALC
        //                :IND-DFL-INTR-PREST-CALC
        //               ,:DFL-INTR-PREST-DFZ
        //                :IND-DFL-INTR-PREST-DFZ
        //               ,:DFL-INTR-PREST-EFFLQ
        //                :IND-DFL-INTR-PREST-EFFLQ
        //               ,:DFL-ACCPRE-SOST-CALC
        //                :IND-DFL-ACCPRE-SOST-CALC
        //               ,:DFL-ACCPRE-SOST-DFZ
        //                :IND-DFL-ACCPRE-SOST-DFZ
        //               ,:DFL-ACCPRE-SOST-EFFLQ
        //                :IND-DFL-ACCPRE-SOST-EFFLQ
        //               ,:DFL-ACCPRE-VIS-CALC
        //                :IND-DFL-ACCPRE-VIS-CALC
        //               ,:DFL-ACCPRE-VIS-DFZ
        //                :IND-DFL-ACCPRE-VIS-DFZ
        //               ,:DFL-ACCPRE-VIS-EFFLQ
        //                :IND-DFL-ACCPRE-VIS-EFFLQ
        //               ,:DFL-ACCPRE-ACC-CALC
        //                :IND-DFL-ACCPRE-ACC-CALC
        //               ,:DFL-ACCPRE-ACC-DFZ
        //                :IND-DFL-ACCPRE-ACC-DFZ
        //               ,:DFL-ACCPRE-ACC-EFFLQ
        //                :IND-DFL-ACCPRE-ACC-EFFLQ
        //               ,:DFL-RES-PRSTZ-CALC
        //                :IND-DFL-RES-PRSTZ-CALC
        //               ,:DFL-RES-PRSTZ-DFZ
        //                :IND-DFL-RES-PRSTZ-DFZ
        //               ,:DFL-RES-PRSTZ-EFFLQ
        //                :IND-DFL-RES-PRSTZ-EFFLQ
        //               ,:DFL-RES-PRE-ATT-CALC
        //                :IND-DFL-RES-PRE-ATT-CALC
        //               ,:DFL-RES-PRE-ATT-DFZ
        //                :IND-DFL-RES-PRE-ATT-DFZ
        //               ,:DFL-RES-PRE-ATT-EFFLQ
        //                :IND-DFL-RES-PRE-ATT-EFFLQ
        //               ,:DFL-IMP-EXCONTR-EFF
        //                :IND-DFL-IMP-EXCONTR-EFF
        //               ,:DFL-IMPB-VIS-CALC
        //                :IND-DFL-IMPB-VIS-CALC
        //               ,:DFL-IMPB-VIS-EFFLQ
        //                :IND-DFL-IMPB-VIS-EFFLQ
        //               ,:DFL-IMPB-VIS-DFZ
        //                :IND-DFL-IMPB-VIS-DFZ
        //               ,:DFL-IMPB-RIT-ACC-CALC
        //                :IND-DFL-IMPB-RIT-ACC-CALC
        //               ,:DFL-IMPB-RIT-ACC-EFFLQ
        //                :IND-DFL-IMPB-RIT-ACC-EFFLQ
        //               ,:DFL-IMPB-RIT-ACC-DFZ
        //                :IND-DFL-IMPB-RIT-ACC-DFZ
        //               ,:DFL-IMPB-TFR-CALC
        //                :IND-DFL-IMPB-TFR-CALC
        //               ,:DFL-IMPB-TFR-EFFLQ
        //                :IND-DFL-IMPB-TFR-EFFLQ
        //               ,:DFL-IMPB-TFR-DFZ
        //                :IND-DFL-IMPB-TFR-DFZ
        //               ,:DFL-IMPB-IS-CALC
        //                :IND-DFL-IMPB-IS-CALC
        //               ,:DFL-IMPB-IS-EFFLQ
        //                :IND-DFL-IMPB-IS-EFFLQ
        //               ,:DFL-IMPB-IS-DFZ
        //                :IND-DFL-IMPB-IS-DFZ
        //               ,:DFL-IMPB-TAX-SEP-CALC
        //                :IND-DFL-IMPB-TAX-SEP-CALC
        //               ,:DFL-IMPB-TAX-SEP-EFFLQ
        //                :IND-DFL-IMPB-TAX-SEP-EFFLQ
        //               ,:DFL-IMPB-TAX-SEP-DFZ
        //                :IND-DFL-IMPB-TAX-SEP-DFZ
        //               ,:DFL-IINT-PREST-CALC
        //                :IND-DFL-IINT-PREST-CALC
        //               ,:DFL-IINT-PREST-EFFLQ
        //                :IND-DFL-IINT-PREST-EFFLQ
        //               ,:DFL-IINT-PREST-DFZ
        //                :IND-DFL-IINT-PREST-DFZ
        //               ,:DFL-MONT-END2000-CALC
        //                :IND-DFL-MONT-END2000-CALC
        //               ,:DFL-MONT-END2000-EFFLQ
        //                :IND-DFL-MONT-END2000-EFFLQ
        //               ,:DFL-MONT-END2000-DFZ
        //                :IND-DFL-MONT-END2000-DFZ
        //               ,:DFL-MONT-END2006-CALC
        //                :IND-DFL-MONT-END2006-CALC
        //               ,:DFL-MONT-END2006-EFFLQ
        //                :IND-DFL-MONT-END2006-EFFLQ
        //               ,:DFL-MONT-END2006-DFZ
        //                :IND-DFL-MONT-END2006-DFZ
        //               ,:DFL-MONT-DAL2007-CALC
        //                :IND-DFL-MONT-DAL2007-CALC
        //               ,:DFL-MONT-DAL2007-EFFLQ
        //                :IND-DFL-MONT-DAL2007-EFFLQ
        //               ,:DFL-MONT-DAL2007-DFZ
        //                :IND-DFL-MONT-DAL2007-DFZ
        //               ,:DFL-IIMPST-PRVR-CALC
        //                :IND-DFL-IIMPST-PRVR-CALC
        //               ,:DFL-IIMPST-PRVR-EFFLQ
        //                :IND-DFL-IIMPST-PRVR-EFFLQ
        //               ,:DFL-IIMPST-PRVR-DFZ
        //                :IND-DFL-IIMPST-PRVR-DFZ
        //               ,:DFL-IIMPST-252-CALC
        //                :IND-DFL-IIMPST-252-CALC
        //               ,:DFL-IIMPST-252-EFFLQ
        //                :IND-DFL-IIMPST-252-EFFLQ
        //               ,:DFL-IIMPST-252-DFZ
        //                :IND-DFL-IIMPST-252-DFZ
        //               ,:DFL-IMPST-252-CALC
        //                :IND-DFL-IMPST-252-CALC
        //               ,:DFL-IMPST-252-EFFLQ
        //                :IND-DFL-IMPST-252-EFFLQ
        //               ,:DFL-RIT-TFR-CALC
        //                :IND-DFL-RIT-TFR-CALC
        //               ,:DFL-RIT-TFR-EFFLQ
        //                :IND-DFL-RIT-TFR-EFFLQ
        //               ,:DFL-RIT-TFR-DFZ
        //                :IND-DFL-RIT-TFR-DFZ
        //               ,:DFL-CNBT-INPSTFM-CALC
        //                :IND-DFL-CNBT-INPSTFM-CALC
        //               ,:DFL-CNBT-INPSTFM-EFFLQ
        //                :IND-DFL-CNBT-INPSTFM-EFFLQ
        //               ,:DFL-CNBT-INPSTFM-DFZ
        //                :IND-DFL-CNBT-INPSTFM-DFZ
        //               ,:DFL-ICNB-INPSTFM-CALC
        //                :IND-DFL-ICNB-INPSTFM-CALC
        //               ,:DFL-ICNB-INPSTFM-EFFLQ
        //                :IND-DFL-ICNB-INPSTFM-EFFLQ
        //               ,:DFL-ICNB-INPSTFM-DFZ
        //                :IND-DFL-ICNB-INPSTFM-DFZ
        //               ,:DFL-CNDE-END2000-CALC
        //                :IND-DFL-CNDE-END2000-CALC
        //               ,:DFL-CNDE-END2000-EFFLQ
        //                :IND-DFL-CNDE-END2000-EFFLQ
        //               ,:DFL-CNDE-END2000-DFZ
        //                :IND-DFL-CNDE-END2000-DFZ
        //               ,:DFL-CNDE-END2006-CALC
        //                :IND-DFL-CNDE-END2006-CALC
        //               ,:DFL-CNDE-END2006-EFFLQ
        //                :IND-DFL-CNDE-END2006-EFFLQ
        //               ,:DFL-CNDE-END2006-DFZ
        //                :IND-DFL-CNDE-END2006-DFZ
        //               ,:DFL-CNDE-DAL2007-CALC
        //                :IND-DFL-CNDE-DAL2007-CALC
        //               ,:DFL-CNDE-DAL2007-EFFLQ
        //                :IND-DFL-CNDE-DAL2007-EFFLQ
        //               ,:DFL-CNDE-DAL2007-DFZ
        //                :IND-DFL-CNDE-DAL2007-DFZ
        //               ,:DFL-AA-CNBZ-END2000-EF
        //                :IND-DFL-AA-CNBZ-END2000-EF
        //               ,:DFL-AA-CNBZ-END2006-EF
        //                :IND-DFL-AA-CNBZ-END2006-EF
        //               ,:DFL-AA-CNBZ-DAL2007-EF
        //                :IND-DFL-AA-CNBZ-DAL2007-EF
        //               ,:DFL-MM-CNBZ-END2000-EF
        //                :IND-DFL-MM-CNBZ-END2000-EF
        //               ,:DFL-MM-CNBZ-END2006-EF
        //                :IND-DFL-MM-CNBZ-END2006-EF
        //               ,:DFL-MM-CNBZ-DAL2007-EF
        //                :IND-DFL-MM-CNBZ-DAL2007-EF
        //               ,:DFL-IMPST-DA-RIMB-EFF
        //                :IND-DFL-IMPST-DA-RIMB-EFF
        //               ,:DFL-ALQ-TAX-SEP-CALC
        //                :IND-DFL-ALQ-TAX-SEP-CALC
        //               ,:DFL-ALQ-TAX-SEP-EFFLQ
        //                :IND-DFL-ALQ-TAX-SEP-EFFLQ
        //               ,:DFL-ALQ-TAX-SEP-DFZ
        //                :IND-DFL-ALQ-TAX-SEP-DFZ
        //               ,:DFL-ALQ-CNBT-INPSTFM-C
        //                :IND-DFL-ALQ-CNBT-INPSTFM-C
        //               ,:DFL-ALQ-CNBT-INPSTFM-E
        //                :IND-DFL-ALQ-CNBT-INPSTFM-E
        //               ,:DFL-ALQ-CNBT-INPSTFM-D
        //                :IND-DFL-ALQ-CNBT-INPSTFM-D
        //               ,:DFL-DS-RIGA
        //               ,:DFL-DS-OPER-SQL
        //               ,:DFL-DS-VER
        //               ,:DFL-DS-TS-INI-CPTZ
        //               ,:DFL-DS-TS-END-CPTZ
        //               ,:DFL-DS-UTENTE
        //               ,:DFL-DS-STATO-ELAB
        //               ,:DFL-IMPB-VIS-1382011C
        //                :IND-DFL-IMPB-VIS-1382011C
        //               ,:DFL-IMPB-VIS-1382011D
        //                :IND-DFL-IMPB-VIS-1382011D
        //               ,:DFL-IMPB-VIS-1382011L
        //                :IND-DFL-IMPB-VIS-1382011L
        //               ,:DFL-IMPST-VIS-1382011C
        //                :IND-DFL-IMPST-VIS-1382011C
        //               ,:DFL-IMPST-VIS-1382011D
        //                :IND-DFL-IMPST-VIS-1382011D
        //               ,:DFL-IMPST-VIS-1382011L
        //                :IND-DFL-IMPST-VIS-1382011L
        //               ,:DFL-IMPB-IS-1382011C
        //                :IND-DFL-IMPB-IS-1382011C
        //               ,:DFL-IMPB-IS-1382011D
        //                :IND-DFL-IMPB-IS-1382011D
        //               ,:DFL-IMPB-IS-1382011L
        //                :IND-DFL-IMPB-IS-1382011L
        //               ,:DFL-IS-1382011C
        //                :IND-DFL-IS-1382011C
        //               ,:DFL-IS-1382011D
        //                :IND-DFL-IS-1382011D
        //               ,:DFL-IS-1382011L
        //                :IND-DFL-IS-1382011L
        //               ,:DFL-IMP-INTR-RIT-PAG-C
        //                :IND-DFL-IMP-INTR-RIT-PAG-C
        //               ,:DFL-IMP-INTR-RIT-PAG-D
        //                :IND-DFL-IMP-INTR-RIT-PAG-D
        //               ,:DFL-IMP-INTR-RIT-PAG-L
        //                :IND-DFL-IMP-INTR-RIT-PAG-L
        //               ,:DFL-IMPB-BOLLO-DETT-C
        //                :IND-DFL-IMPB-BOLLO-DETT-C
        //               ,:DFL-IMPB-BOLLO-DETT-D
        //                :IND-DFL-IMPB-BOLLO-DETT-D
        //               ,:DFL-IMPB-BOLLO-DETT-L
        //                :IND-DFL-IMPB-BOLLO-DETT-L
        //               ,:DFL-IMPST-BOLLO-DETT-C
        //                :IND-DFL-IMPST-BOLLO-DETT-C
        //               ,:DFL-IMPST-BOLLO-DETT-D
        //                :IND-DFL-IMPST-BOLLO-DETT-D
        //               ,:DFL-IMPST-BOLLO-DETT-L
        //                :IND-DFL-IMPST-BOLLO-DETT-L
        //               ,:DFL-IMPST-BOLLO-TOT-VC
        //                :IND-DFL-IMPST-BOLLO-TOT-VC
        //               ,:DFL-IMPST-BOLLO-TOT-VD
        //                :IND-DFL-IMPST-BOLLO-TOT-VD
        //               ,:DFL-IMPST-BOLLO-TOT-VL
        //                :IND-DFL-IMPST-BOLLO-TOT-VL
        //               ,:DFL-IMPB-VIS-662014C
        //                :IND-DFL-IMPB-VIS-662014C
        //               ,:DFL-IMPB-VIS-662014D
        //                :IND-DFL-IMPB-VIS-662014D
        //               ,:DFL-IMPB-VIS-662014L
        //                :IND-DFL-IMPB-VIS-662014L
        //               ,:DFL-IMPST-VIS-662014C
        //                :IND-DFL-IMPST-VIS-662014C
        //               ,:DFL-IMPST-VIS-662014D
        //                :IND-DFL-IMPST-VIS-662014D
        //               ,:DFL-IMPST-VIS-662014L
        //                :IND-DFL-IMPST-VIS-662014L
        //               ,:DFL-IMPB-IS-662014C
        //                :IND-DFL-IMPB-IS-662014C
        //               ,:DFL-IMPB-IS-662014D
        //                :IND-DFL-IMPB-IS-662014D
        //               ,:DFL-IMPB-IS-662014L
        //                :IND-DFL-IMPB-IS-662014L
        //               ,:DFL-IS-662014C
        //                :IND-DFL-IS-662014C
        //               ,:DFL-IS-662014D
        //                :IND-DFL-IS-662014D
        //               ,:DFL-IS-662014L
        //                :IND-DFL-IS-662014L
        //           END-EXEC.
        dForzLiqDao.fetchCIdUpdEffDfl(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A370-CLOSE-CURSOR-ID-EFF THRU A370-EX
            a370CloseCursorIdEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A405-DECLARE-CURSOR-IDP-EFF<br>
	 * <pre>----
	 * ----  gestione IDP Effetto
	 * ----</pre>*/
    private void a405DeclareCursorIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A410-SELECT-IDP-EFF<br>*/
    private void a410SelectIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A460-OPEN-CURSOR-IDP-EFF<br>*/
    private void a460OpenCursorIdpEff() {
        // COB_CODE: PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.
        a405DeclareCursorIdpEff();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A470-CLOSE-CURSOR-IDP-EFF<br>*/
    private void a470CloseCursorIdpEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A480-FETCH-FIRST-IDP-EFF<br>*/
    private void a480FetchFirstIdpEff() {
        // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.
        a460OpenCursorIdpEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
            a490FetchNextIdpEff();
        }
    }

    /**Original name: A490-FETCH-NEXT-IDP-EFF<br>*/
    private void a490FetchNextIdpEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A505-DECLARE-CURSOR-IBO<br>
	 * <pre>----
	 * ----  gestione IBO Effetto e non
	 * ----</pre>*/
    private void a505DeclareCursorIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A510-SELECT-IBO<br>*/
    private void a510SelectIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A560-OPEN-CURSOR-IBO<br>*/
    private void a560OpenCursorIbo() {
        // COB_CODE: PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.
        a505DeclareCursorIbo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A570-CLOSE-CURSOR-IBO<br>*/
    private void a570CloseCursorIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A580-FETCH-FIRST-IBO<br>*/
    private void a580FetchFirstIbo() {
        // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.
        a560OpenCursorIbo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
            a590FetchNextIbo();
        }
    }

    /**Original name: A590-FETCH-NEXT-IBO<br>*/
    private void a590FetchNextIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A605-DECLARE-CURSOR-IBS<br>
	 * <pre>----
	 * ----  gestione IBS Effetto e non
	 * ----</pre>*/
    private void a605DeclareCursorIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A610-SELECT-IBS<br>*/
    private void a610SelectIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A660-OPEN-CURSOR-IBS<br>*/
    private void a660OpenCursorIbs() {
        // COB_CODE: PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.
        a605DeclareCursorIbs();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A670-CLOSE-CURSOR-IBS<br>*/
    private void a670CloseCursorIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A680-FETCH-FIRST-IBS<br>*/
    private void a680FetchFirstIbs() {
        // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.
        a660OpenCursorIbs();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
            a690FetchNextIbs();
        }
    }

    /**Original name: A690-FETCH-NEXT-IBS<br>*/
    private void a690FetchNextIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A705-DECLARE-CURSOR-IDO<br>
	 * <pre>----
	 * ----  gestione IDO Effetto e non
	 * ----</pre>*/
    private void a705DeclareCursorIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A710-SELECT-IDO<br>*/
    private void a710SelectIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A760-OPEN-CURSOR-IDO<br>*/
    private void a760OpenCursorIdo() {
        // COB_CODE: PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.
        a705DeclareCursorIdo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A770-CLOSE-CURSOR-IDO<br>*/
    private void a770CloseCursorIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A780-FETCH-FIRST-IDO<br>*/
    private void a780FetchFirstIdo() {
        // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.
        a760OpenCursorIdo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
            a790FetchNextIdo();
        }
    }

    /**Original name: A790-FETCH-NEXT-IDO<br>*/
    private void a790FetchNextIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B310-SELECT-ID-CPZ<br>
	 * <pre>----
	 * ----  gestione ID Competenza
	 * ----</pre>*/
    private void b310SelectIdCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_D_FORZ_LIQ
        //                ,ID_LIQ
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,COD_COMP_ANIA
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,IMP_LRD_CALC
        //                ,IMP_LRD_DFZ
        //                ,IMP_LRD_EFFLQ
        //                ,IMP_NET_CALC
        //                ,IMP_NET_DFZ
        //                ,IMP_NET_EFFLQ
        //                ,IMPST_PRVR_CALC
        //                ,IMPST_PRVR_DFZ
        //                ,IMPST_PRVR_EFFLQ
        //                ,IMPST_VIS_CALC
        //                ,IMPST_VIS_DFZ
        //                ,IMPST_VIS_EFFLQ
        //                ,RIT_ACC_CALC
        //                ,RIT_ACC_DFZ
        //                ,RIT_ACC_EFFLQ
        //                ,RIT_IRPEF_CALC
        //                ,RIT_IRPEF_DFZ
        //                ,RIT_IRPEF_EFFLQ
        //                ,IMPST_SOST_CALC
        //                ,IMPST_SOST_DFZ
        //                ,IMPST_SOST_EFFLQ
        //                ,TAX_SEP_CALC
        //                ,TAX_SEP_DFZ
        //                ,TAX_SEP_EFFLQ
        //                ,INTR_PREST_CALC
        //                ,INTR_PREST_DFZ
        //                ,INTR_PREST_EFFLQ
        //                ,ACCPRE_SOST_CALC
        //                ,ACCPRE_SOST_DFZ
        //                ,ACCPRE_SOST_EFFLQ
        //                ,ACCPRE_VIS_CALC
        //                ,ACCPRE_VIS_DFZ
        //                ,ACCPRE_VIS_EFFLQ
        //                ,ACCPRE_ACC_CALC
        //                ,ACCPRE_ACC_DFZ
        //                ,ACCPRE_ACC_EFFLQ
        //                ,RES_PRSTZ_CALC
        //                ,RES_PRSTZ_DFZ
        //                ,RES_PRSTZ_EFFLQ
        //                ,RES_PRE_ATT_CALC
        //                ,RES_PRE_ATT_DFZ
        //                ,RES_PRE_ATT_EFFLQ
        //                ,IMP_EXCONTR_EFF
        //                ,IMPB_VIS_CALC
        //                ,IMPB_VIS_EFFLQ
        //                ,IMPB_VIS_DFZ
        //                ,IMPB_RIT_ACC_CALC
        //                ,IMPB_RIT_ACC_EFFLQ
        //                ,IMPB_RIT_ACC_DFZ
        //                ,IMPB_TFR_CALC
        //                ,IMPB_TFR_EFFLQ
        //                ,IMPB_TFR_DFZ
        //                ,IMPB_IS_CALC
        //                ,IMPB_IS_EFFLQ
        //                ,IMPB_IS_DFZ
        //                ,IMPB_TAX_SEP_CALC
        //                ,IMPB_TAX_SEP_EFFLQ
        //                ,IMPB_TAX_SEP_DFZ
        //                ,IINT_PREST_CALC
        //                ,IINT_PREST_EFFLQ
        //                ,IINT_PREST_DFZ
        //                ,MONT_END2000_CALC
        //                ,MONT_END2000_EFFLQ
        //                ,MONT_END2000_DFZ
        //                ,MONT_END2006_CALC
        //                ,MONT_END2006_EFFLQ
        //                ,MONT_END2006_DFZ
        //                ,MONT_DAL2007_CALC
        //                ,MONT_DAL2007_EFFLQ
        //                ,MONT_DAL2007_DFZ
        //                ,IIMPST_PRVR_CALC
        //                ,IIMPST_PRVR_EFFLQ
        //                ,IIMPST_PRVR_DFZ
        //                ,IIMPST_252_CALC
        //                ,IIMPST_252_EFFLQ
        //                ,IIMPST_252_DFZ
        //                ,IMPST_252_CALC
        //                ,IMPST_252_EFFLQ
        //                ,RIT_TFR_CALC
        //                ,RIT_TFR_EFFLQ
        //                ,RIT_TFR_DFZ
        //                ,CNBT_INPSTFM_CALC
        //                ,CNBT_INPSTFM_EFFLQ
        //                ,CNBT_INPSTFM_DFZ
        //                ,ICNB_INPSTFM_CALC
        //                ,ICNB_INPSTFM_EFFLQ
        //                ,ICNB_INPSTFM_DFZ
        //                ,CNDE_END2000_CALC
        //                ,CNDE_END2000_EFFLQ
        //                ,CNDE_END2000_DFZ
        //                ,CNDE_END2006_CALC
        //                ,CNDE_END2006_EFFLQ
        //                ,CNDE_END2006_DFZ
        //                ,CNDE_DAL2007_CALC
        //                ,CNDE_DAL2007_EFFLQ
        //                ,CNDE_DAL2007_DFZ
        //                ,AA_CNBZ_END2000_EF
        //                ,AA_CNBZ_END2006_EF
        //                ,AA_CNBZ_DAL2007_EF
        //                ,MM_CNBZ_END2000_EF
        //                ,MM_CNBZ_END2006_EF
        //                ,MM_CNBZ_DAL2007_EF
        //                ,IMPST_DA_RIMB_EFF
        //                ,ALQ_TAX_SEP_CALC
        //                ,ALQ_TAX_SEP_EFFLQ
        //                ,ALQ_TAX_SEP_DFZ
        //                ,ALQ_CNBT_INPSTFM_C
        //                ,ALQ_CNBT_INPSTFM_E
        //                ,ALQ_CNBT_INPSTFM_D
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,IMPB_VIS_1382011C
        //                ,IMPB_VIS_1382011D
        //                ,IMPB_VIS_1382011L
        //                ,IMPST_VIS_1382011C
        //                ,IMPST_VIS_1382011D
        //                ,IMPST_VIS_1382011L
        //                ,IMPB_IS_1382011C
        //                ,IMPB_IS_1382011D
        //                ,IMPB_IS_1382011L
        //                ,IS_1382011C
        //                ,IS_1382011D
        //                ,IS_1382011L
        //                ,IMP_INTR_RIT_PAG_C
        //                ,IMP_INTR_RIT_PAG_D
        //                ,IMP_INTR_RIT_PAG_L
        //                ,IMPB_BOLLO_DETT_C
        //                ,IMPB_BOLLO_DETT_D
        //                ,IMPB_BOLLO_DETT_L
        //                ,IMPST_BOLLO_DETT_C
        //                ,IMPST_BOLLO_DETT_D
        //                ,IMPST_BOLLO_DETT_L
        //                ,IMPST_BOLLO_TOT_VC
        //                ,IMPST_BOLLO_TOT_VD
        //                ,IMPST_BOLLO_TOT_VL
        //                ,IMPB_VIS_662014C
        //                ,IMPB_VIS_662014D
        //                ,IMPB_VIS_662014L
        //                ,IMPST_VIS_662014C
        //                ,IMPST_VIS_662014D
        //                ,IMPST_VIS_662014L
        //                ,IMPB_IS_662014C
        //                ,IMPB_IS_662014D
        //                ,IMPB_IS_662014L
        //                ,IS_662014C
        //                ,IS_662014D
        //                ,IS_662014L
        //             INTO
        //                :DFL-ID-D-FORZ-LIQ
        //               ,:DFL-ID-LIQ
        //               ,:DFL-ID-MOVI-CRZ
        //               ,:DFL-ID-MOVI-CHIU
        //                :IND-DFL-ID-MOVI-CHIU
        //               ,:DFL-COD-COMP-ANIA
        //               ,:DFL-DT-INI-EFF-DB
        //               ,:DFL-DT-END-EFF-DB
        //               ,:DFL-IMP-LRD-CALC
        //                :IND-DFL-IMP-LRD-CALC
        //               ,:DFL-IMP-LRD-DFZ
        //                :IND-DFL-IMP-LRD-DFZ
        //               ,:DFL-IMP-LRD-EFFLQ
        //                :IND-DFL-IMP-LRD-EFFLQ
        //               ,:DFL-IMP-NET-CALC
        //                :IND-DFL-IMP-NET-CALC
        //               ,:DFL-IMP-NET-DFZ
        //                :IND-DFL-IMP-NET-DFZ
        //               ,:DFL-IMP-NET-EFFLQ
        //                :IND-DFL-IMP-NET-EFFLQ
        //               ,:DFL-IMPST-PRVR-CALC
        //                :IND-DFL-IMPST-PRVR-CALC
        //               ,:DFL-IMPST-PRVR-DFZ
        //                :IND-DFL-IMPST-PRVR-DFZ
        //               ,:DFL-IMPST-PRVR-EFFLQ
        //                :IND-DFL-IMPST-PRVR-EFFLQ
        //               ,:DFL-IMPST-VIS-CALC
        //                :IND-DFL-IMPST-VIS-CALC
        //               ,:DFL-IMPST-VIS-DFZ
        //                :IND-DFL-IMPST-VIS-DFZ
        //               ,:DFL-IMPST-VIS-EFFLQ
        //                :IND-DFL-IMPST-VIS-EFFLQ
        //               ,:DFL-RIT-ACC-CALC
        //                :IND-DFL-RIT-ACC-CALC
        //               ,:DFL-RIT-ACC-DFZ
        //                :IND-DFL-RIT-ACC-DFZ
        //               ,:DFL-RIT-ACC-EFFLQ
        //                :IND-DFL-RIT-ACC-EFFLQ
        //               ,:DFL-RIT-IRPEF-CALC
        //                :IND-DFL-RIT-IRPEF-CALC
        //               ,:DFL-RIT-IRPEF-DFZ
        //                :IND-DFL-RIT-IRPEF-DFZ
        //               ,:DFL-RIT-IRPEF-EFFLQ
        //                :IND-DFL-RIT-IRPEF-EFFLQ
        //               ,:DFL-IMPST-SOST-CALC
        //                :IND-DFL-IMPST-SOST-CALC
        //               ,:DFL-IMPST-SOST-DFZ
        //                :IND-DFL-IMPST-SOST-DFZ
        //               ,:DFL-IMPST-SOST-EFFLQ
        //                :IND-DFL-IMPST-SOST-EFFLQ
        //               ,:DFL-TAX-SEP-CALC
        //                :IND-DFL-TAX-SEP-CALC
        //               ,:DFL-TAX-SEP-DFZ
        //                :IND-DFL-TAX-SEP-DFZ
        //               ,:DFL-TAX-SEP-EFFLQ
        //                :IND-DFL-TAX-SEP-EFFLQ
        //               ,:DFL-INTR-PREST-CALC
        //                :IND-DFL-INTR-PREST-CALC
        //               ,:DFL-INTR-PREST-DFZ
        //                :IND-DFL-INTR-PREST-DFZ
        //               ,:DFL-INTR-PREST-EFFLQ
        //                :IND-DFL-INTR-PREST-EFFLQ
        //               ,:DFL-ACCPRE-SOST-CALC
        //                :IND-DFL-ACCPRE-SOST-CALC
        //               ,:DFL-ACCPRE-SOST-DFZ
        //                :IND-DFL-ACCPRE-SOST-DFZ
        //               ,:DFL-ACCPRE-SOST-EFFLQ
        //                :IND-DFL-ACCPRE-SOST-EFFLQ
        //               ,:DFL-ACCPRE-VIS-CALC
        //                :IND-DFL-ACCPRE-VIS-CALC
        //               ,:DFL-ACCPRE-VIS-DFZ
        //                :IND-DFL-ACCPRE-VIS-DFZ
        //               ,:DFL-ACCPRE-VIS-EFFLQ
        //                :IND-DFL-ACCPRE-VIS-EFFLQ
        //               ,:DFL-ACCPRE-ACC-CALC
        //                :IND-DFL-ACCPRE-ACC-CALC
        //               ,:DFL-ACCPRE-ACC-DFZ
        //                :IND-DFL-ACCPRE-ACC-DFZ
        //               ,:DFL-ACCPRE-ACC-EFFLQ
        //                :IND-DFL-ACCPRE-ACC-EFFLQ
        //               ,:DFL-RES-PRSTZ-CALC
        //                :IND-DFL-RES-PRSTZ-CALC
        //               ,:DFL-RES-PRSTZ-DFZ
        //                :IND-DFL-RES-PRSTZ-DFZ
        //               ,:DFL-RES-PRSTZ-EFFLQ
        //                :IND-DFL-RES-PRSTZ-EFFLQ
        //               ,:DFL-RES-PRE-ATT-CALC
        //                :IND-DFL-RES-PRE-ATT-CALC
        //               ,:DFL-RES-PRE-ATT-DFZ
        //                :IND-DFL-RES-PRE-ATT-DFZ
        //               ,:DFL-RES-PRE-ATT-EFFLQ
        //                :IND-DFL-RES-PRE-ATT-EFFLQ
        //               ,:DFL-IMP-EXCONTR-EFF
        //                :IND-DFL-IMP-EXCONTR-EFF
        //               ,:DFL-IMPB-VIS-CALC
        //                :IND-DFL-IMPB-VIS-CALC
        //               ,:DFL-IMPB-VIS-EFFLQ
        //                :IND-DFL-IMPB-VIS-EFFLQ
        //               ,:DFL-IMPB-VIS-DFZ
        //                :IND-DFL-IMPB-VIS-DFZ
        //               ,:DFL-IMPB-RIT-ACC-CALC
        //                :IND-DFL-IMPB-RIT-ACC-CALC
        //               ,:DFL-IMPB-RIT-ACC-EFFLQ
        //                :IND-DFL-IMPB-RIT-ACC-EFFLQ
        //               ,:DFL-IMPB-RIT-ACC-DFZ
        //                :IND-DFL-IMPB-RIT-ACC-DFZ
        //               ,:DFL-IMPB-TFR-CALC
        //                :IND-DFL-IMPB-TFR-CALC
        //               ,:DFL-IMPB-TFR-EFFLQ
        //                :IND-DFL-IMPB-TFR-EFFLQ
        //               ,:DFL-IMPB-TFR-DFZ
        //                :IND-DFL-IMPB-TFR-DFZ
        //               ,:DFL-IMPB-IS-CALC
        //                :IND-DFL-IMPB-IS-CALC
        //               ,:DFL-IMPB-IS-EFFLQ
        //                :IND-DFL-IMPB-IS-EFFLQ
        //               ,:DFL-IMPB-IS-DFZ
        //                :IND-DFL-IMPB-IS-DFZ
        //               ,:DFL-IMPB-TAX-SEP-CALC
        //                :IND-DFL-IMPB-TAX-SEP-CALC
        //               ,:DFL-IMPB-TAX-SEP-EFFLQ
        //                :IND-DFL-IMPB-TAX-SEP-EFFLQ
        //               ,:DFL-IMPB-TAX-SEP-DFZ
        //                :IND-DFL-IMPB-TAX-SEP-DFZ
        //               ,:DFL-IINT-PREST-CALC
        //                :IND-DFL-IINT-PREST-CALC
        //               ,:DFL-IINT-PREST-EFFLQ
        //                :IND-DFL-IINT-PREST-EFFLQ
        //               ,:DFL-IINT-PREST-DFZ
        //                :IND-DFL-IINT-PREST-DFZ
        //               ,:DFL-MONT-END2000-CALC
        //                :IND-DFL-MONT-END2000-CALC
        //               ,:DFL-MONT-END2000-EFFLQ
        //                :IND-DFL-MONT-END2000-EFFLQ
        //               ,:DFL-MONT-END2000-DFZ
        //                :IND-DFL-MONT-END2000-DFZ
        //               ,:DFL-MONT-END2006-CALC
        //                :IND-DFL-MONT-END2006-CALC
        //               ,:DFL-MONT-END2006-EFFLQ
        //                :IND-DFL-MONT-END2006-EFFLQ
        //               ,:DFL-MONT-END2006-DFZ
        //                :IND-DFL-MONT-END2006-DFZ
        //               ,:DFL-MONT-DAL2007-CALC
        //                :IND-DFL-MONT-DAL2007-CALC
        //               ,:DFL-MONT-DAL2007-EFFLQ
        //                :IND-DFL-MONT-DAL2007-EFFLQ
        //               ,:DFL-MONT-DAL2007-DFZ
        //                :IND-DFL-MONT-DAL2007-DFZ
        //               ,:DFL-IIMPST-PRVR-CALC
        //                :IND-DFL-IIMPST-PRVR-CALC
        //               ,:DFL-IIMPST-PRVR-EFFLQ
        //                :IND-DFL-IIMPST-PRVR-EFFLQ
        //               ,:DFL-IIMPST-PRVR-DFZ
        //                :IND-DFL-IIMPST-PRVR-DFZ
        //               ,:DFL-IIMPST-252-CALC
        //                :IND-DFL-IIMPST-252-CALC
        //               ,:DFL-IIMPST-252-EFFLQ
        //                :IND-DFL-IIMPST-252-EFFLQ
        //               ,:DFL-IIMPST-252-DFZ
        //                :IND-DFL-IIMPST-252-DFZ
        //               ,:DFL-IMPST-252-CALC
        //                :IND-DFL-IMPST-252-CALC
        //               ,:DFL-IMPST-252-EFFLQ
        //                :IND-DFL-IMPST-252-EFFLQ
        //               ,:DFL-RIT-TFR-CALC
        //                :IND-DFL-RIT-TFR-CALC
        //               ,:DFL-RIT-TFR-EFFLQ
        //                :IND-DFL-RIT-TFR-EFFLQ
        //               ,:DFL-RIT-TFR-DFZ
        //                :IND-DFL-RIT-TFR-DFZ
        //               ,:DFL-CNBT-INPSTFM-CALC
        //                :IND-DFL-CNBT-INPSTFM-CALC
        //               ,:DFL-CNBT-INPSTFM-EFFLQ
        //                :IND-DFL-CNBT-INPSTFM-EFFLQ
        //               ,:DFL-CNBT-INPSTFM-DFZ
        //                :IND-DFL-CNBT-INPSTFM-DFZ
        //               ,:DFL-ICNB-INPSTFM-CALC
        //                :IND-DFL-ICNB-INPSTFM-CALC
        //               ,:DFL-ICNB-INPSTFM-EFFLQ
        //                :IND-DFL-ICNB-INPSTFM-EFFLQ
        //               ,:DFL-ICNB-INPSTFM-DFZ
        //                :IND-DFL-ICNB-INPSTFM-DFZ
        //               ,:DFL-CNDE-END2000-CALC
        //                :IND-DFL-CNDE-END2000-CALC
        //               ,:DFL-CNDE-END2000-EFFLQ
        //                :IND-DFL-CNDE-END2000-EFFLQ
        //               ,:DFL-CNDE-END2000-DFZ
        //                :IND-DFL-CNDE-END2000-DFZ
        //               ,:DFL-CNDE-END2006-CALC
        //                :IND-DFL-CNDE-END2006-CALC
        //               ,:DFL-CNDE-END2006-EFFLQ
        //                :IND-DFL-CNDE-END2006-EFFLQ
        //               ,:DFL-CNDE-END2006-DFZ
        //                :IND-DFL-CNDE-END2006-DFZ
        //               ,:DFL-CNDE-DAL2007-CALC
        //                :IND-DFL-CNDE-DAL2007-CALC
        //               ,:DFL-CNDE-DAL2007-EFFLQ
        //                :IND-DFL-CNDE-DAL2007-EFFLQ
        //               ,:DFL-CNDE-DAL2007-DFZ
        //                :IND-DFL-CNDE-DAL2007-DFZ
        //               ,:DFL-AA-CNBZ-END2000-EF
        //                :IND-DFL-AA-CNBZ-END2000-EF
        //               ,:DFL-AA-CNBZ-END2006-EF
        //                :IND-DFL-AA-CNBZ-END2006-EF
        //               ,:DFL-AA-CNBZ-DAL2007-EF
        //                :IND-DFL-AA-CNBZ-DAL2007-EF
        //               ,:DFL-MM-CNBZ-END2000-EF
        //                :IND-DFL-MM-CNBZ-END2000-EF
        //               ,:DFL-MM-CNBZ-END2006-EF
        //                :IND-DFL-MM-CNBZ-END2006-EF
        //               ,:DFL-MM-CNBZ-DAL2007-EF
        //                :IND-DFL-MM-CNBZ-DAL2007-EF
        //               ,:DFL-IMPST-DA-RIMB-EFF
        //                :IND-DFL-IMPST-DA-RIMB-EFF
        //               ,:DFL-ALQ-TAX-SEP-CALC
        //                :IND-DFL-ALQ-TAX-SEP-CALC
        //               ,:DFL-ALQ-TAX-SEP-EFFLQ
        //                :IND-DFL-ALQ-TAX-SEP-EFFLQ
        //               ,:DFL-ALQ-TAX-SEP-DFZ
        //                :IND-DFL-ALQ-TAX-SEP-DFZ
        //               ,:DFL-ALQ-CNBT-INPSTFM-C
        //                :IND-DFL-ALQ-CNBT-INPSTFM-C
        //               ,:DFL-ALQ-CNBT-INPSTFM-E
        //                :IND-DFL-ALQ-CNBT-INPSTFM-E
        //               ,:DFL-ALQ-CNBT-INPSTFM-D
        //                :IND-DFL-ALQ-CNBT-INPSTFM-D
        //               ,:DFL-DS-RIGA
        //               ,:DFL-DS-OPER-SQL
        //               ,:DFL-DS-VER
        //               ,:DFL-DS-TS-INI-CPTZ
        //               ,:DFL-DS-TS-END-CPTZ
        //               ,:DFL-DS-UTENTE
        //               ,:DFL-DS-STATO-ELAB
        //               ,:DFL-IMPB-VIS-1382011C
        //                :IND-DFL-IMPB-VIS-1382011C
        //               ,:DFL-IMPB-VIS-1382011D
        //                :IND-DFL-IMPB-VIS-1382011D
        //               ,:DFL-IMPB-VIS-1382011L
        //                :IND-DFL-IMPB-VIS-1382011L
        //               ,:DFL-IMPST-VIS-1382011C
        //                :IND-DFL-IMPST-VIS-1382011C
        //               ,:DFL-IMPST-VIS-1382011D
        //                :IND-DFL-IMPST-VIS-1382011D
        //               ,:DFL-IMPST-VIS-1382011L
        //                :IND-DFL-IMPST-VIS-1382011L
        //               ,:DFL-IMPB-IS-1382011C
        //                :IND-DFL-IMPB-IS-1382011C
        //               ,:DFL-IMPB-IS-1382011D
        //                :IND-DFL-IMPB-IS-1382011D
        //               ,:DFL-IMPB-IS-1382011L
        //                :IND-DFL-IMPB-IS-1382011L
        //               ,:DFL-IS-1382011C
        //                :IND-DFL-IS-1382011C
        //               ,:DFL-IS-1382011D
        //                :IND-DFL-IS-1382011D
        //               ,:DFL-IS-1382011L
        //                :IND-DFL-IS-1382011L
        //               ,:DFL-IMP-INTR-RIT-PAG-C
        //                :IND-DFL-IMP-INTR-RIT-PAG-C
        //               ,:DFL-IMP-INTR-RIT-PAG-D
        //                :IND-DFL-IMP-INTR-RIT-PAG-D
        //               ,:DFL-IMP-INTR-RIT-PAG-L
        //                :IND-DFL-IMP-INTR-RIT-PAG-L
        //               ,:DFL-IMPB-BOLLO-DETT-C
        //                :IND-DFL-IMPB-BOLLO-DETT-C
        //               ,:DFL-IMPB-BOLLO-DETT-D
        //                :IND-DFL-IMPB-BOLLO-DETT-D
        //               ,:DFL-IMPB-BOLLO-DETT-L
        //                :IND-DFL-IMPB-BOLLO-DETT-L
        //               ,:DFL-IMPST-BOLLO-DETT-C
        //                :IND-DFL-IMPST-BOLLO-DETT-C
        //               ,:DFL-IMPST-BOLLO-DETT-D
        //                :IND-DFL-IMPST-BOLLO-DETT-D
        //               ,:DFL-IMPST-BOLLO-DETT-L
        //                :IND-DFL-IMPST-BOLLO-DETT-L
        //               ,:DFL-IMPST-BOLLO-TOT-VC
        //                :IND-DFL-IMPST-BOLLO-TOT-VC
        //               ,:DFL-IMPST-BOLLO-TOT-VD
        //                :IND-DFL-IMPST-BOLLO-TOT-VD
        //               ,:DFL-IMPST-BOLLO-TOT-VL
        //                :IND-DFL-IMPST-BOLLO-TOT-VL
        //               ,:DFL-IMPB-VIS-662014C
        //                :IND-DFL-IMPB-VIS-662014C
        //               ,:DFL-IMPB-VIS-662014D
        //                :IND-DFL-IMPB-VIS-662014D
        //               ,:DFL-IMPB-VIS-662014L
        //                :IND-DFL-IMPB-VIS-662014L
        //               ,:DFL-IMPST-VIS-662014C
        //                :IND-DFL-IMPST-VIS-662014C
        //               ,:DFL-IMPST-VIS-662014D
        //                :IND-DFL-IMPST-VIS-662014D
        //               ,:DFL-IMPST-VIS-662014L
        //                :IND-DFL-IMPST-VIS-662014L
        //               ,:DFL-IMPB-IS-662014C
        //                :IND-DFL-IMPB-IS-662014C
        //               ,:DFL-IMPB-IS-662014D
        //                :IND-DFL-IMPB-IS-662014D
        //               ,:DFL-IMPB-IS-662014L
        //                :IND-DFL-IMPB-IS-662014L
        //               ,:DFL-IS-662014C
        //                :IND-DFL-IS-662014C
        //               ,:DFL-IS-662014D
        //                :IND-DFL-IS-662014D
        //               ,:DFL-IS-662014L
        //                :IND-DFL-IS-662014L
        //             FROM D_FORZ_LIQ
        //             WHERE     ID_D_FORZ_LIQ = :DFL-ID-D-FORZ-LIQ
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        dForzLiqDao.selectRec1(dForzLiq.getDflIdDForzLiq(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B405-DECLARE-CURSOR-IDP-CPZ<br>
	 * <pre>----
	 * ----  gestione IDP Competenza
	 * ----</pre>*/
    private void b405DeclareCursorIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B410-SELECT-IDP-CPZ<br>*/
    private void b410SelectIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B460-OPEN-CURSOR-IDP-CPZ<br>*/
    private void b460OpenCursorIdpCpz() {
        // COB_CODE: PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.
        b405DeclareCursorIdpCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B470-CLOSE-CURSOR-IDP-CPZ<br>*/
    private void b470CloseCursorIdpCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B480-FETCH-FIRST-IDP-CPZ<br>*/
    private void b480FetchFirstIdpCpz() {
        // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.
        b460OpenCursorIdpCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
            b490FetchNextIdpCpz();
        }
    }

    /**Original name: B490-FETCH-NEXT-IDP-CPZ<br>*/
    private void b490FetchNextIdpCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B505-DECLARE-CURSOR-IBO-CPZ<br>
	 * <pre>----
	 * ----  gestione IBO Competenza
	 * ----</pre>*/
    private void b505DeclareCursorIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B510-SELECT-IBO-CPZ<br>*/
    private void b510SelectIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B560-OPEN-CURSOR-IBO-CPZ<br>*/
    private void b560OpenCursorIboCpz() {
        // COB_CODE: PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.
        b505DeclareCursorIboCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B570-CLOSE-CURSOR-IBO-CPZ<br>*/
    private void b570CloseCursorIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B580-FETCH-FIRST-IBO-CPZ<br>*/
    private void b580FetchFirstIboCpz() {
        // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.
        b560OpenCursorIboCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
    }

    /**Original name: B590-FETCH-NEXT-IBO-CPZ<br>*/
    private void b590FetchNextIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B605-DECLARE-CURSOR-IBS-CPZ<br>
	 * <pre>----
	 * ----  gestione IBS Competenza
	 * ----</pre>*/
    private void b605DeclareCursorIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B610-SELECT-IBS-CPZ<br>*/
    private void b610SelectIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B660-OPEN-CURSOR-IBS-CPZ<br>*/
    private void b660OpenCursorIbsCpz() {
        // COB_CODE: PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.
        b605DeclareCursorIbsCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B670-CLOSE-CURSOR-IBS-CPZ<br>*/
    private void b670CloseCursorIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B680-FETCH-FIRST-IBS-CPZ<br>*/
    private void b680FetchFirstIbsCpz() {
        // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.
        b660OpenCursorIbsCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
    }

    /**Original name: B690-FETCH-NEXT-IBS-CPZ<br>*/
    private void b690FetchNextIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B705-DECLARE-CURSOR-IDO-CPZ<br>
	 * <pre>----
	 * ----  gestione IDO Competenza
	 * ----</pre>*/
    private void b705DeclareCursorIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B710-SELECT-IDO-CPZ<br>*/
    private void b710SelectIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B760-OPEN-CURSOR-IDO-CPZ<br>*/
    private void b760OpenCursorIdoCpz() {
        // COB_CODE: PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.
        b705DeclareCursorIdoCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B770-CLOSE-CURSOR-IDO-CPZ<br>*/
    private void b770CloseCursorIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B780-FETCH-FIRST-IDO-CPZ<br>*/
    private void b780FetchFirstIdoCpz() {
        // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.
        b760OpenCursorIdoCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
    }

    /**Original name: B790-FETCH-NEXT-IDO-CPZ<br>*/
    private void b790FetchNextIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-DFL-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO DFL-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-ID-MOVI-CHIU-NULL
            dForzLiq.getDflIdMoviChiu().setDflIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflIdMoviChiu.Len.DFL_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-DFL-IMP-LRD-CALC = -1
        //              MOVE HIGH-VALUES TO DFL-IMP-LRD-CALC-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpLrdCalc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMP-LRD-CALC-NULL
            dForzLiq.getDflImpLrdCalc().setDflImpLrdCalcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpLrdCalc.Len.DFL_IMP_LRD_CALC_NULL));
        }
        // COB_CODE: IF IND-DFL-IMP-LRD-DFZ = -1
        //              MOVE HIGH-VALUES TO DFL-IMP-LRD-DFZ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpLrdDfz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMP-LRD-DFZ-NULL
            dForzLiq.getDflImpLrdDfz().setDflImpLrdDfzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpLrdDfz.Len.DFL_IMP_LRD_DFZ_NULL));
        }
        // COB_CODE: IF IND-DFL-IMP-LRD-EFFLQ = -1
        //              MOVE HIGH-VALUES TO DFL-IMP-LRD-EFFLQ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpLrdEfflq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMP-LRD-EFFLQ-NULL
            dForzLiq.getDflImpLrdEfflq().setDflImpLrdEfflqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpLrdEfflq.Len.DFL_IMP_LRD_EFFLQ_NULL));
        }
        // COB_CODE: IF IND-DFL-IMP-NET-CALC = -1
        //              MOVE HIGH-VALUES TO DFL-IMP-NET-CALC-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpNetCalc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMP-NET-CALC-NULL
            dForzLiq.getDflImpNetCalc().setDflImpNetCalcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpNetCalc.Len.DFL_IMP_NET_CALC_NULL));
        }
        // COB_CODE: IF IND-DFL-IMP-NET-DFZ = -1
        //              MOVE HIGH-VALUES TO DFL-IMP-NET-DFZ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpNetDfz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMP-NET-DFZ-NULL
            dForzLiq.getDflImpNetDfz().setDflImpNetDfzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpNetDfz.Len.DFL_IMP_NET_DFZ_NULL));
        }
        // COB_CODE: IF IND-DFL-IMP-NET-EFFLQ = -1
        //              MOVE HIGH-VALUES TO DFL-IMP-NET-EFFLQ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpNetEfflq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMP-NET-EFFLQ-NULL
            dForzLiq.getDflImpNetEfflq().setDflImpNetEfflqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpNetEfflq.Len.DFL_IMP_NET_EFFLQ_NULL));
        }
        // COB_CODE: IF IND-DFL-IMPST-PRVR-CALC = -1
        //              MOVE HIGH-VALUES TO DFL-IMPST-PRVR-CALC-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpstPrvrCalc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMPST-PRVR-CALC-NULL
            dForzLiq.getDflImpstPrvrCalc().setDflImpstPrvrCalcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpstPrvrCalc.Len.DFL_IMPST_PRVR_CALC_NULL));
        }
        // COB_CODE: IF IND-DFL-IMPST-PRVR-DFZ = -1
        //              MOVE HIGH-VALUES TO DFL-IMPST-PRVR-DFZ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpstPrvrDfz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMPST-PRVR-DFZ-NULL
            dForzLiq.getDflImpstPrvrDfz().setDflImpstPrvrDfzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpstPrvrDfz.Len.DFL_IMPST_PRVR_DFZ_NULL));
        }
        // COB_CODE: IF IND-DFL-IMPST-PRVR-EFFLQ = -1
        //              MOVE HIGH-VALUES TO DFL-IMPST-PRVR-EFFLQ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpstPrvrEfflq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMPST-PRVR-EFFLQ-NULL
            dForzLiq.getDflImpstPrvrEfflq().setDflImpstPrvrEfflqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpstPrvrEfflq.Len.DFL_IMPST_PRVR_EFFLQ_NULL));
        }
        // COB_CODE: IF IND-DFL-IMPST-VIS-CALC = -1
        //              MOVE HIGH-VALUES TO DFL-IMPST-VIS-CALC-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpstVisCalc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMPST-VIS-CALC-NULL
            dForzLiq.getDflImpstVisCalc().setDflImpstVisCalcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpstVisCalc.Len.DFL_IMPST_VIS_CALC_NULL));
        }
        // COB_CODE: IF IND-DFL-IMPST-VIS-DFZ = -1
        //              MOVE HIGH-VALUES TO DFL-IMPST-VIS-DFZ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpstVisDfz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMPST-VIS-DFZ-NULL
            dForzLiq.getDflImpstVisDfz().setDflImpstVisDfzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpstVisDfz.Len.DFL_IMPST_VIS_DFZ_NULL));
        }
        // COB_CODE: IF IND-DFL-IMPST-VIS-EFFLQ = -1
        //              MOVE HIGH-VALUES TO DFL-IMPST-VIS-EFFLQ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpstVisEfflq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMPST-VIS-EFFLQ-NULL
            dForzLiq.getDflImpstVisEfflq().setDflImpstVisEfflqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpstVisEfflq.Len.DFL_IMPST_VIS_EFFLQ_NULL));
        }
        // COB_CODE: IF IND-DFL-RIT-ACC-CALC = -1
        //              MOVE HIGH-VALUES TO DFL-RIT-ACC-CALC-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getRitAccCalc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-RIT-ACC-CALC-NULL
            dForzLiq.getDflRitAccCalc().setDflRitAccCalcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflRitAccCalc.Len.DFL_RIT_ACC_CALC_NULL));
        }
        // COB_CODE: IF IND-DFL-RIT-ACC-DFZ = -1
        //              MOVE HIGH-VALUES TO DFL-RIT-ACC-DFZ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getRitAccDfz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-RIT-ACC-DFZ-NULL
            dForzLiq.getDflRitAccDfz().setDflRitAccDfzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflRitAccDfz.Len.DFL_RIT_ACC_DFZ_NULL));
        }
        // COB_CODE: IF IND-DFL-RIT-ACC-EFFLQ = -1
        //              MOVE HIGH-VALUES TO DFL-RIT-ACC-EFFLQ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getRitAccEfflq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-RIT-ACC-EFFLQ-NULL
            dForzLiq.getDflRitAccEfflq().setDflRitAccEfflqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflRitAccEfflq.Len.DFL_RIT_ACC_EFFLQ_NULL));
        }
        // COB_CODE: IF IND-DFL-RIT-IRPEF-CALC = -1
        //              MOVE HIGH-VALUES TO DFL-RIT-IRPEF-CALC-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getRitIrpefCalc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-RIT-IRPEF-CALC-NULL
            dForzLiq.getDflRitIrpefCalc().setDflRitIrpefCalcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflRitIrpefCalc.Len.DFL_RIT_IRPEF_CALC_NULL));
        }
        // COB_CODE: IF IND-DFL-RIT-IRPEF-DFZ = -1
        //              MOVE HIGH-VALUES TO DFL-RIT-IRPEF-DFZ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getRitIrpefDfz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-RIT-IRPEF-DFZ-NULL
            dForzLiq.getDflRitIrpefDfz().setDflRitIrpefDfzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflRitIrpefDfz.Len.DFL_RIT_IRPEF_DFZ_NULL));
        }
        // COB_CODE: IF IND-DFL-RIT-IRPEF-EFFLQ = -1
        //              MOVE HIGH-VALUES TO DFL-RIT-IRPEF-EFFLQ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getRitIrpefEfflq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-RIT-IRPEF-EFFLQ-NULL
            dForzLiq.getDflRitIrpefEfflq().setDflRitIrpefEfflqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflRitIrpefEfflq.Len.DFL_RIT_IRPEF_EFFLQ_NULL));
        }
        // COB_CODE: IF IND-DFL-IMPST-SOST-CALC = -1
        //              MOVE HIGH-VALUES TO DFL-IMPST-SOST-CALC-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpstSostCalc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMPST-SOST-CALC-NULL
            dForzLiq.getDflImpstSostCalc().setDflImpstSostCalcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpstSostCalc.Len.DFL_IMPST_SOST_CALC_NULL));
        }
        // COB_CODE: IF IND-DFL-IMPST-SOST-DFZ = -1
        //              MOVE HIGH-VALUES TO DFL-IMPST-SOST-DFZ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpstSostDfz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMPST-SOST-DFZ-NULL
            dForzLiq.getDflImpstSostDfz().setDflImpstSostDfzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpstSostDfz.Len.DFL_IMPST_SOST_DFZ_NULL));
        }
        // COB_CODE: IF IND-DFL-IMPST-SOST-EFFLQ = -1
        //              MOVE HIGH-VALUES TO DFL-IMPST-SOST-EFFLQ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpstSostEfflq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMPST-SOST-EFFLQ-NULL
            dForzLiq.getDflImpstSostEfflq().setDflImpstSostEfflqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpstSostEfflq.Len.DFL_IMPST_SOST_EFFLQ_NULL));
        }
        // COB_CODE: IF IND-DFL-TAX-SEP-CALC = -1
        //              MOVE HIGH-VALUES TO DFL-TAX-SEP-CALC-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getTaxSepCalc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-TAX-SEP-CALC-NULL
            dForzLiq.getDflTaxSepCalc().setDflTaxSepCalcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflTaxSepCalc.Len.DFL_TAX_SEP_CALC_NULL));
        }
        // COB_CODE: IF IND-DFL-TAX-SEP-DFZ = -1
        //              MOVE HIGH-VALUES TO DFL-TAX-SEP-DFZ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getTaxSepDfz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-TAX-SEP-DFZ-NULL
            dForzLiq.getDflTaxSepDfz().setDflTaxSepDfzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflTaxSepDfz.Len.DFL_TAX_SEP_DFZ_NULL));
        }
        // COB_CODE: IF IND-DFL-TAX-SEP-EFFLQ = -1
        //              MOVE HIGH-VALUES TO DFL-TAX-SEP-EFFLQ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getTaxSepEfflq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-TAX-SEP-EFFLQ-NULL
            dForzLiq.getDflTaxSepEfflq().setDflTaxSepEfflqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflTaxSepEfflq.Len.DFL_TAX_SEP_EFFLQ_NULL));
        }
        // COB_CODE: IF IND-DFL-INTR-PREST-CALC = -1
        //              MOVE HIGH-VALUES TO DFL-INTR-PREST-CALC-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getIntrPrestCalc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-INTR-PREST-CALC-NULL
            dForzLiq.getDflIntrPrestCalc().setDflIntrPrestCalcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflIntrPrestCalc.Len.DFL_INTR_PREST_CALC_NULL));
        }
        // COB_CODE: IF IND-DFL-INTR-PREST-DFZ = -1
        //              MOVE HIGH-VALUES TO DFL-INTR-PREST-DFZ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getIntrPrestDfz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-INTR-PREST-DFZ-NULL
            dForzLiq.getDflIntrPrestDfz().setDflIntrPrestDfzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflIntrPrestDfz.Len.DFL_INTR_PREST_DFZ_NULL));
        }
        // COB_CODE: IF IND-DFL-INTR-PREST-EFFLQ = -1
        //              MOVE HIGH-VALUES TO DFL-INTR-PREST-EFFLQ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getIntrPrestEfflq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-INTR-PREST-EFFLQ-NULL
            dForzLiq.getDflIntrPrestEfflq().setDflIntrPrestEfflqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflIntrPrestEfflq.Len.DFL_INTR_PREST_EFFLQ_NULL));
        }
        // COB_CODE: IF IND-DFL-ACCPRE-SOST-CALC = -1
        //              MOVE HIGH-VALUES TO DFL-ACCPRE-SOST-CALC-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getAccpreSostCalc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-ACCPRE-SOST-CALC-NULL
            dForzLiq.getDflAccpreSostCalc().setDflAccpreSostCalcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflAccpreSostCalc.Len.DFL_ACCPRE_SOST_CALC_NULL));
        }
        // COB_CODE: IF IND-DFL-ACCPRE-SOST-DFZ = -1
        //              MOVE HIGH-VALUES TO DFL-ACCPRE-SOST-DFZ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getAccpreSostDfz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-ACCPRE-SOST-DFZ-NULL
            dForzLiq.getDflAccpreSostDfz().setDflAccpreSostDfzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflAccpreSostDfz.Len.DFL_ACCPRE_SOST_DFZ_NULL));
        }
        // COB_CODE: IF IND-DFL-ACCPRE-SOST-EFFLQ = -1
        //              MOVE HIGH-VALUES TO DFL-ACCPRE-SOST-EFFLQ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getAccpreSostEfflq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-ACCPRE-SOST-EFFLQ-NULL
            dForzLiq.getDflAccpreSostEfflq().setDflAccpreSostEfflqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflAccpreSostEfflq.Len.DFL_ACCPRE_SOST_EFFLQ_NULL));
        }
        // COB_CODE: IF IND-DFL-ACCPRE-VIS-CALC = -1
        //              MOVE HIGH-VALUES TO DFL-ACCPRE-VIS-CALC-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getAccpreVisCalc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-ACCPRE-VIS-CALC-NULL
            dForzLiq.getDflAccpreVisCalc().setDflAccpreVisCalcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflAccpreVisCalc.Len.DFL_ACCPRE_VIS_CALC_NULL));
        }
        // COB_CODE: IF IND-DFL-ACCPRE-VIS-DFZ = -1
        //              MOVE HIGH-VALUES TO DFL-ACCPRE-VIS-DFZ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getAccpreVisDfz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-ACCPRE-VIS-DFZ-NULL
            dForzLiq.getDflAccpreVisDfz().setDflAccpreVisDfzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflAccpreVisDfz.Len.DFL_ACCPRE_VIS_DFZ_NULL));
        }
        // COB_CODE: IF IND-DFL-ACCPRE-VIS-EFFLQ = -1
        //              MOVE HIGH-VALUES TO DFL-ACCPRE-VIS-EFFLQ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getAccpreVisEfflq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-ACCPRE-VIS-EFFLQ-NULL
            dForzLiq.getDflAccpreVisEfflq().setDflAccpreVisEfflqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflAccpreVisEfflq.Len.DFL_ACCPRE_VIS_EFFLQ_NULL));
        }
        // COB_CODE: IF IND-DFL-ACCPRE-ACC-CALC = -1
        //              MOVE HIGH-VALUES TO DFL-ACCPRE-ACC-CALC-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getAccpreAccCalc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-ACCPRE-ACC-CALC-NULL
            dForzLiq.getDflAccpreAccCalc().setDflAccpreAccCalcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflAccpreAccCalc.Len.DFL_ACCPRE_ACC_CALC_NULL));
        }
        // COB_CODE: IF IND-DFL-ACCPRE-ACC-DFZ = -1
        //              MOVE HIGH-VALUES TO DFL-ACCPRE-ACC-DFZ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getAccpreAccDfz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-ACCPRE-ACC-DFZ-NULL
            dForzLiq.getDflAccpreAccDfz().setDflAccpreAccDfzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflAccpreAccDfz.Len.DFL_ACCPRE_ACC_DFZ_NULL));
        }
        // COB_CODE: IF IND-DFL-ACCPRE-ACC-EFFLQ = -1
        //              MOVE HIGH-VALUES TO DFL-ACCPRE-ACC-EFFLQ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getAccpreAccEfflq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-ACCPRE-ACC-EFFLQ-NULL
            dForzLiq.getDflAccpreAccEfflq().setDflAccpreAccEfflqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflAccpreAccEfflq.Len.DFL_ACCPRE_ACC_EFFLQ_NULL));
        }
        // COB_CODE: IF IND-DFL-RES-PRSTZ-CALC = -1
        //              MOVE HIGH-VALUES TO DFL-RES-PRSTZ-CALC-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getResPrstzCalc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-RES-PRSTZ-CALC-NULL
            dForzLiq.getDflResPrstzCalc().setDflResPrstzCalcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflResPrstzCalc.Len.DFL_RES_PRSTZ_CALC_NULL));
        }
        // COB_CODE: IF IND-DFL-RES-PRSTZ-DFZ = -1
        //              MOVE HIGH-VALUES TO DFL-RES-PRSTZ-DFZ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getResPrstzDfz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-RES-PRSTZ-DFZ-NULL
            dForzLiq.getDflResPrstzDfz().setDflResPrstzDfzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflResPrstzDfz.Len.DFL_RES_PRSTZ_DFZ_NULL));
        }
        // COB_CODE: IF IND-DFL-RES-PRSTZ-EFFLQ = -1
        //              MOVE HIGH-VALUES TO DFL-RES-PRSTZ-EFFLQ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getResPrstzEfflq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-RES-PRSTZ-EFFLQ-NULL
            dForzLiq.getDflResPrstzEfflq().setDflResPrstzEfflqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflResPrstzEfflq.Len.DFL_RES_PRSTZ_EFFLQ_NULL));
        }
        // COB_CODE: IF IND-DFL-RES-PRE-ATT-CALC = -1
        //              MOVE HIGH-VALUES TO DFL-RES-PRE-ATT-CALC-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getResPreAttCalc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-RES-PRE-ATT-CALC-NULL
            dForzLiq.getDflResPreAttCalc().setDflResPreAttCalcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflResPreAttCalc.Len.DFL_RES_PRE_ATT_CALC_NULL));
        }
        // COB_CODE: IF IND-DFL-RES-PRE-ATT-DFZ = -1
        //              MOVE HIGH-VALUES TO DFL-RES-PRE-ATT-DFZ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getResPreAttDfz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-RES-PRE-ATT-DFZ-NULL
            dForzLiq.getDflResPreAttDfz().setDflResPreAttDfzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflResPreAttDfz.Len.DFL_RES_PRE_ATT_DFZ_NULL));
        }
        // COB_CODE: IF IND-DFL-RES-PRE-ATT-EFFLQ = -1
        //              MOVE HIGH-VALUES TO DFL-RES-PRE-ATT-EFFLQ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getResPreAttEfflq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-RES-PRE-ATT-EFFLQ-NULL
            dForzLiq.getDflResPreAttEfflq().setDflResPreAttEfflqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflResPreAttEfflq.Len.DFL_RES_PRE_ATT_EFFLQ_NULL));
        }
        // COB_CODE: IF IND-DFL-IMP-EXCONTR-EFF = -1
        //              MOVE HIGH-VALUES TO DFL-IMP-EXCONTR-EFF-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpExcontrEff() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMP-EXCONTR-EFF-NULL
            dForzLiq.getDflImpExcontrEff().setDflImpExcontrEffNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpExcontrEff.Len.DFL_IMP_EXCONTR_EFF_NULL));
        }
        // COB_CODE: IF IND-DFL-IMPB-VIS-CALC = -1
        //              MOVE HIGH-VALUES TO DFL-IMPB-VIS-CALC-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpbVisCalc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMPB-VIS-CALC-NULL
            dForzLiq.getDflImpbVisCalc().setDflImpbVisCalcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpbVisCalc.Len.DFL_IMPB_VIS_CALC_NULL));
        }
        // COB_CODE: IF IND-DFL-IMPB-VIS-EFFLQ = -1
        //              MOVE HIGH-VALUES TO DFL-IMPB-VIS-EFFLQ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpbVisEfflq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMPB-VIS-EFFLQ-NULL
            dForzLiq.getDflImpbVisEfflq().setDflImpbVisEfflqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpbVisEfflq.Len.DFL_IMPB_VIS_EFFLQ_NULL));
        }
        // COB_CODE: IF IND-DFL-IMPB-VIS-DFZ = -1
        //              MOVE HIGH-VALUES TO DFL-IMPB-VIS-DFZ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpbVisDfz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMPB-VIS-DFZ-NULL
            dForzLiq.getDflImpbVisDfz().setDflImpbVisDfzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpbVisDfz.Len.DFL_IMPB_VIS_DFZ_NULL));
        }
        // COB_CODE: IF IND-DFL-IMPB-RIT-ACC-CALC = -1
        //              MOVE HIGH-VALUES TO DFL-IMPB-RIT-ACC-CALC-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpbRitAccCalc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMPB-RIT-ACC-CALC-NULL
            dForzLiq.getDflImpbRitAccCalc().setDflImpbRitAccCalcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpbRitAccCalc.Len.DFL_IMPB_RIT_ACC_CALC_NULL));
        }
        // COB_CODE: IF IND-DFL-IMPB-RIT-ACC-EFFLQ = -1
        //              MOVE HIGH-VALUES TO DFL-IMPB-RIT-ACC-EFFLQ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpbRitAccEfflq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMPB-RIT-ACC-EFFLQ-NULL
            dForzLiq.getDflImpbRitAccEfflq().setDflImpbRitAccEfflqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpbRitAccEfflq.Len.DFL_IMPB_RIT_ACC_EFFLQ_NULL));
        }
        // COB_CODE: IF IND-DFL-IMPB-RIT-ACC-DFZ = -1
        //              MOVE HIGH-VALUES TO DFL-IMPB-RIT-ACC-DFZ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpbRitAccDfz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMPB-RIT-ACC-DFZ-NULL
            dForzLiq.getDflImpbRitAccDfz().setDflImpbRitAccDfzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpbRitAccDfz.Len.DFL_IMPB_RIT_ACC_DFZ_NULL));
        }
        // COB_CODE: IF IND-DFL-IMPB-TFR-CALC = -1
        //              MOVE HIGH-VALUES TO DFL-IMPB-TFR-CALC-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpbTfrCalc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMPB-TFR-CALC-NULL
            dForzLiq.getDflImpbTfrCalc().setDflImpbTfrCalcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpbTfrCalc.Len.DFL_IMPB_TFR_CALC_NULL));
        }
        // COB_CODE: IF IND-DFL-IMPB-TFR-EFFLQ = -1
        //              MOVE HIGH-VALUES TO DFL-IMPB-TFR-EFFLQ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpbTfrEfflq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMPB-TFR-EFFLQ-NULL
            dForzLiq.getDflImpbTfrEfflq().setDflImpbTfrEfflqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpbTfrEfflq.Len.DFL_IMPB_TFR_EFFLQ_NULL));
        }
        // COB_CODE: IF IND-DFL-IMPB-TFR-DFZ = -1
        //              MOVE HIGH-VALUES TO DFL-IMPB-TFR-DFZ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpbTfrDfz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMPB-TFR-DFZ-NULL
            dForzLiq.getDflImpbTfrDfz().setDflImpbTfrDfzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpbTfrDfz.Len.DFL_IMPB_TFR_DFZ_NULL));
        }
        // COB_CODE: IF IND-DFL-IMPB-IS-CALC = -1
        //              MOVE HIGH-VALUES TO DFL-IMPB-IS-CALC-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpbIsCalc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMPB-IS-CALC-NULL
            dForzLiq.getDflImpbIsCalc().setDflImpbIsCalcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpbIsCalc.Len.DFL_IMPB_IS_CALC_NULL));
        }
        // COB_CODE: IF IND-DFL-IMPB-IS-EFFLQ = -1
        //              MOVE HIGH-VALUES TO DFL-IMPB-IS-EFFLQ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpbIsEfflq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMPB-IS-EFFLQ-NULL
            dForzLiq.getDflImpbIsEfflq().setDflImpbIsEfflqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpbIsEfflq.Len.DFL_IMPB_IS_EFFLQ_NULL));
        }
        // COB_CODE: IF IND-DFL-IMPB-IS-DFZ = -1
        //              MOVE HIGH-VALUES TO DFL-IMPB-IS-DFZ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpbIsDfz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMPB-IS-DFZ-NULL
            dForzLiq.getDflImpbIsDfz().setDflImpbIsDfzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpbIsDfz.Len.DFL_IMPB_IS_DFZ_NULL));
        }
        // COB_CODE: IF IND-DFL-IMPB-TAX-SEP-CALC = -1
        //              MOVE HIGH-VALUES TO DFL-IMPB-TAX-SEP-CALC-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpbTaxSepCalc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMPB-TAX-SEP-CALC-NULL
            dForzLiq.getDflImpbTaxSepCalc().setDflImpbTaxSepCalcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpbTaxSepCalc.Len.DFL_IMPB_TAX_SEP_CALC_NULL));
        }
        // COB_CODE: IF IND-DFL-IMPB-TAX-SEP-EFFLQ = -1
        //              MOVE HIGH-VALUES TO DFL-IMPB-TAX-SEP-EFFLQ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpbTaxSepEfflq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMPB-TAX-SEP-EFFLQ-NULL
            dForzLiq.getDflImpbTaxSepEfflq().setDflImpbTaxSepEfflqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpbTaxSepEfflq.Len.DFL_IMPB_TAX_SEP_EFFLQ_NULL));
        }
        // COB_CODE: IF IND-DFL-IMPB-TAX-SEP-DFZ = -1
        //              MOVE HIGH-VALUES TO DFL-IMPB-TAX-SEP-DFZ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpbTaxSepDfz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMPB-TAX-SEP-DFZ-NULL
            dForzLiq.getDflImpbTaxSepDfz().setDflImpbTaxSepDfzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpbTaxSepDfz.Len.DFL_IMPB_TAX_SEP_DFZ_NULL));
        }
        // COB_CODE: IF IND-DFL-IINT-PREST-CALC = -1
        //              MOVE HIGH-VALUES TO DFL-IINT-PREST-CALC-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getIintPrestCalc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IINT-PREST-CALC-NULL
            dForzLiq.getDflIintPrestCalc().setDflIintPrestCalcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflIintPrestCalc.Len.DFL_IINT_PREST_CALC_NULL));
        }
        // COB_CODE: IF IND-DFL-IINT-PREST-EFFLQ = -1
        //              MOVE HIGH-VALUES TO DFL-IINT-PREST-EFFLQ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getIintPrestEfflq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IINT-PREST-EFFLQ-NULL
            dForzLiq.getDflIintPrestEfflq().setDflIintPrestEfflqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflIintPrestEfflq.Len.DFL_IINT_PREST_EFFLQ_NULL));
        }
        // COB_CODE: IF IND-DFL-IINT-PREST-DFZ = -1
        //              MOVE HIGH-VALUES TO DFL-IINT-PREST-DFZ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getIintPrestDfz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IINT-PREST-DFZ-NULL
            dForzLiq.getDflIintPrestDfz().setDflIintPrestDfzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflIintPrestDfz.Len.DFL_IINT_PREST_DFZ_NULL));
        }
        // COB_CODE: IF IND-DFL-MONT-END2000-CALC = -1
        //              MOVE HIGH-VALUES TO DFL-MONT-END2000-CALC-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getMontEnd2000Calc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-MONT-END2000-CALC-NULL
            dForzLiq.getDflMontEnd2000Calc().setDflMontEnd2000CalcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflMontEnd2000Calc.Len.DFL_MONT_END2000_CALC_NULL));
        }
        // COB_CODE: IF IND-DFL-MONT-END2000-EFFLQ = -1
        //              MOVE HIGH-VALUES TO DFL-MONT-END2000-EFFLQ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getMontEnd2000Efflq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-MONT-END2000-EFFLQ-NULL
            dForzLiq.getDflMontEnd2000Efflq().setDflMontEnd2000EfflqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflMontEnd2000Efflq.Len.DFL_MONT_END2000_EFFLQ_NULL));
        }
        // COB_CODE: IF IND-DFL-MONT-END2000-DFZ = -1
        //              MOVE HIGH-VALUES TO DFL-MONT-END2000-DFZ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getMontEnd2000Dfz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-MONT-END2000-DFZ-NULL
            dForzLiq.getDflMontEnd2000Dfz().setDflMontEnd2000DfzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflMontEnd2000Dfz.Len.DFL_MONT_END2000_DFZ_NULL));
        }
        // COB_CODE: IF IND-DFL-MONT-END2006-CALC = -1
        //              MOVE HIGH-VALUES TO DFL-MONT-END2006-CALC-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getMontEnd2006Calc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-MONT-END2006-CALC-NULL
            dForzLiq.getDflMontEnd2006Calc().setDflMontEnd2006CalcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflMontEnd2006Calc.Len.DFL_MONT_END2006_CALC_NULL));
        }
        // COB_CODE: IF IND-DFL-MONT-END2006-EFFLQ = -1
        //              MOVE HIGH-VALUES TO DFL-MONT-END2006-EFFLQ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getMontEnd2006Efflq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-MONT-END2006-EFFLQ-NULL
            dForzLiq.getDflMontEnd2006Efflq().setDflMontEnd2006EfflqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflMontEnd2006Efflq.Len.DFL_MONT_END2006_EFFLQ_NULL));
        }
        // COB_CODE: IF IND-DFL-MONT-END2006-DFZ = -1
        //              MOVE HIGH-VALUES TO DFL-MONT-END2006-DFZ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getMontEnd2006Dfz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-MONT-END2006-DFZ-NULL
            dForzLiq.getDflMontEnd2006Dfz().setDflMontEnd2006DfzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflMontEnd2006Dfz.Len.DFL_MONT_END2006_DFZ_NULL));
        }
        // COB_CODE: IF IND-DFL-MONT-DAL2007-CALC = -1
        //              MOVE HIGH-VALUES TO DFL-MONT-DAL2007-CALC-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getMontDal2007Calc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-MONT-DAL2007-CALC-NULL
            dForzLiq.getDflMontDal2007Calc().setDflMontDal2007CalcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflMontDal2007Calc.Len.DFL_MONT_DAL2007_CALC_NULL));
        }
        // COB_CODE: IF IND-DFL-MONT-DAL2007-EFFLQ = -1
        //              MOVE HIGH-VALUES TO DFL-MONT-DAL2007-EFFLQ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getMontDal2007Efflq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-MONT-DAL2007-EFFLQ-NULL
            dForzLiq.getDflMontDal2007Efflq().setDflMontDal2007EfflqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflMontDal2007Efflq.Len.DFL_MONT_DAL2007_EFFLQ_NULL));
        }
        // COB_CODE: IF IND-DFL-MONT-DAL2007-DFZ = -1
        //              MOVE HIGH-VALUES TO DFL-MONT-DAL2007-DFZ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getMontDal2007Dfz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-MONT-DAL2007-DFZ-NULL
            dForzLiq.getDflMontDal2007Dfz().setDflMontDal2007DfzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflMontDal2007Dfz.Len.DFL_MONT_DAL2007_DFZ_NULL));
        }
        // COB_CODE: IF IND-DFL-IIMPST-PRVR-CALC = -1
        //              MOVE HIGH-VALUES TO DFL-IIMPST-PRVR-CALC-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getIimpstPrvrCalc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IIMPST-PRVR-CALC-NULL
            dForzLiq.getDflIimpstPrvrCalc().setDflIimpstPrvrCalcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflIimpstPrvrCalc.Len.DFL_IIMPST_PRVR_CALC_NULL));
        }
        // COB_CODE: IF IND-DFL-IIMPST-PRVR-EFFLQ = -1
        //              MOVE HIGH-VALUES TO DFL-IIMPST-PRVR-EFFLQ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getIimpstPrvrEfflq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IIMPST-PRVR-EFFLQ-NULL
            dForzLiq.getDflIimpstPrvrEfflq().setDflIimpstPrvrEfflqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflIimpstPrvrEfflq.Len.DFL_IIMPST_PRVR_EFFLQ_NULL));
        }
        // COB_CODE: IF IND-DFL-IIMPST-PRVR-DFZ = -1
        //              MOVE HIGH-VALUES TO DFL-IIMPST-PRVR-DFZ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getIimpstPrvrDfz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IIMPST-PRVR-DFZ-NULL
            dForzLiq.getDflIimpstPrvrDfz().setDflIimpstPrvrDfzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflIimpstPrvrDfz.Len.DFL_IIMPST_PRVR_DFZ_NULL));
        }
        // COB_CODE: IF IND-DFL-IIMPST-252-CALC = -1
        //              MOVE HIGH-VALUES TO DFL-IIMPST-252-CALC-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getIimpst252Calc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IIMPST-252-CALC-NULL
            dForzLiq.getDflIimpst252Calc().setDflIimpst252CalcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflIimpst252Calc.Len.DFL_IIMPST252_CALC_NULL));
        }
        // COB_CODE: IF IND-DFL-IIMPST-252-EFFLQ = -1
        //              MOVE HIGH-VALUES TO DFL-IIMPST-252-EFFLQ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getIimpst252Efflq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IIMPST-252-EFFLQ-NULL
            dForzLiq.getDflIimpst252Efflq().setDflIimpst252EfflqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflIimpst252Efflq.Len.DFL_IIMPST252_EFFLQ_NULL));
        }
        // COB_CODE: IF IND-DFL-IIMPST-252-DFZ = -1
        //              MOVE HIGH-VALUES TO DFL-IIMPST-252-DFZ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getIimpst252Dfz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IIMPST-252-DFZ-NULL
            dForzLiq.getDflIimpst252Dfz().setDflIimpst252DfzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflIimpst252Dfz.Len.DFL_IIMPST252_DFZ_NULL));
        }
        // COB_CODE: IF IND-DFL-IMPST-252-CALC = -1
        //              MOVE HIGH-VALUES TO DFL-IMPST-252-CALC-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpst252Calc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMPST-252-CALC-NULL
            dForzLiq.getDflImpst252Calc().setDflImpst252CalcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpst252Calc.Len.DFL_IMPST252_CALC_NULL));
        }
        // COB_CODE: IF IND-DFL-IMPST-252-EFFLQ = -1
        //              MOVE HIGH-VALUES TO DFL-IMPST-252-EFFLQ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpst252Efflq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMPST-252-EFFLQ-NULL
            dForzLiq.getDflImpst252Efflq().setDflImpst252EfflqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpst252Efflq.Len.DFL_IMPST252_EFFLQ_NULL));
        }
        // COB_CODE: IF IND-DFL-RIT-TFR-CALC = -1
        //              MOVE HIGH-VALUES TO DFL-RIT-TFR-CALC-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getRitTfrCalc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-RIT-TFR-CALC-NULL
            dForzLiq.getDflRitTfrCalc().setDflRitTfrCalcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflRitTfrCalc.Len.DFL_RIT_TFR_CALC_NULL));
        }
        // COB_CODE: IF IND-DFL-RIT-TFR-EFFLQ = -1
        //              MOVE HIGH-VALUES TO DFL-RIT-TFR-EFFLQ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getRitTfrEfflq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-RIT-TFR-EFFLQ-NULL
            dForzLiq.getDflRitTfrEfflq().setDflRitTfrEfflqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflRitTfrEfflq.Len.DFL_RIT_TFR_EFFLQ_NULL));
        }
        // COB_CODE: IF IND-DFL-RIT-TFR-DFZ = -1
        //              MOVE HIGH-VALUES TO DFL-RIT-TFR-DFZ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getRitTfrDfz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-RIT-TFR-DFZ-NULL
            dForzLiq.getDflRitTfrDfz().setDflRitTfrDfzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflRitTfrDfz.Len.DFL_RIT_TFR_DFZ_NULL));
        }
        // COB_CODE: IF IND-DFL-CNBT-INPSTFM-CALC = -1
        //              MOVE HIGH-VALUES TO DFL-CNBT-INPSTFM-CALC-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getCnbtInpstfmCalc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-CNBT-INPSTFM-CALC-NULL
            dForzLiq.getDflCnbtInpstfmCalc().setDflCnbtInpstfmCalcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflCnbtInpstfmCalc.Len.DFL_CNBT_INPSTFM_CALC_NULL));
        }
        // COB_CODE: IF IND-DFL-CNBT-INPSTFM-EFFLQ = -1
        //              MOVE HIGH-VALUES TO DFL-CNBT-INPSTFM-EFFLQ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getCnbtInpstfmEfflq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-CNBT-INPSTFM-EFFLQ-NULL
            dForzLiq.getDflCnbtInpstfmEfflq().setDflCnbtInpstfmEfflqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflCnbtInpstfmEfflq.Len.DFL_CNBT_INPSTFM_EFFLQ_NULL));
        }
        // COB_CODE: IF IND-DFL-CNBT-INPSTFM-DFZ = -1
        //              MOVE HIGH-VALUES TO DFL-CNBT-INPSTFM-DFZ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getCnbtInpstfmDfz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-CNBT-INPSTFM-DFZ-NULL
            dForzLiq.getDflCnbtInpstfmDfz().setDflCnbtInpstfmDfzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflCnbtInpstfmDfz.Len.DFL_CNBT_INPSTFM_DFZ_NULL));
        }
        // COB_CODE: IF IND-DFL-ICNB-INPSTFM-CALC = -1
        //              MOVE HIGH-VALUES TO DFL-ICNB-INPSTFM-CALC-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getIcnbInpstfmCalc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-ICNB-INPSTFM-CALC-NULL
            dForzLiq.getDflIcnbInpstfmCalc().setDflIcnbInpstfmCalcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflIcnbInpstfmCalc.Len.DFL_ICNB_INPSTFM_CALC_NULL));
        }
        // COB_CODE: IF IND-DFL-ICNB-INPSTFM-EFFLQ = -1
        //              MOVE HIGH-VALUES TO DFL-ICNB-INPSTFM-EFFLQ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getIcnbInpstfmEfflq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-ICNB-INPSTFM-EFFLQ-NULL
            dForzLiq.getDflIcnbInpstfmEfflq().setDflIcnbInpstfmEfflqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflIcnbInpstfmEfflq.Len.DFL_ICNB_INPSTFM_EFFLQ_NULL));
        }
        // COB_CODE: IF IND-DFL-ICNB-INPSTFM-DFZ = -1
        //              MOVE HIGH-VALUES TO DFL-ICNB-INPSTFM-DFZ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getIcnbInpstfmDfz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-ICNB-INPSTFM-DFZ-NULL
            dForzLiq.getDflIcnbInpstfmDfz().setDflIcnbInpstfmDfzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflIcnbInpstfmDfz.Len.DFL_ICNB_INPSTFM_DFZ_NULL));
        }
        // COB_CODE: IF IND-DFL-CNDE-END2000-CALC = -1
        //              MOVE HIGH-VALUES TO DFL-CNDE-END2000-CALC-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getCndeEnd2000Calc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-CNDE-END2000-CALC-NULL
            dForzLiq.getDflCndeEnd2000Calc().setDflCndeEnd2000CalcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflCndeEnd2000Calc.Len.DFL_CNDE_END2000_CALC_NULL));
        }
        // COB_CODE: IF IND-DFL-CNDE-END2000-EFFLQ = -1
        //              MOVE HIGH-VALUES TO DFL-CNDE-END2000-EFFLQ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getCndeEnd2000Efflq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-CNDE-END2000-EFFLQ-NULL
            dForzLiq.getDflCndeEnd2000Efflq().setDflCndeEnd2000EfflqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflCndeEnd2000Efflq.Len.DFL_CNDE_END2000_EFFLQ_NULL));
        }
        // COB_CODE: IF IND-DFL-CNDE-END2000-DFZ = -1
        //              MOVE HIGH-VALUES TO DFL-CNDE-END2000-DFZ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getCndeEnd2000Dfz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-CNDE-END2000-DFZ-NULL
            dForzLiq.getDflCndeEnd2000Dfz().setDflCndeEnd2000DfzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflCndeEnd2000Dfz.Len.DFL_CNDE_END2000_DFZ_NULL));
        }
        // COB_CODE: IF IND-DFL-CNDE-END2006-CALC = -1
        //              MOVE HIGH-VALUES TO DFL-CNDE-END2006-CALC-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getCndeEnd2006Calc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-CNDE-END2006-CALC-NULL
            dForzLiq.getDflCndeEnd2006Calc().setDflCndeEnd2006CalcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflCndeEnd2006Calc.Len.DFL_CNDE_END2006_CALC_NULL));
        }
        // COB_CODE: IF IND-DFL-CNDE-END2006-EFFLQ = -1
        //              MOVE HIGH-VALUES TO DFL-CNDE-END2006-EFFLQ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getCndeEnd2006Efflq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-CNDE-END2006-EFFLQ-NULL
            dForzLiq.getDflCndeEnd2006Efflq().setDflCndeEnd2006EfflqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflCndeEnd2006Efflq.Len.DFL_CNDE_END2006_EFFLQ_NULL));
        }
        // COB_CODE: IF IND-DFL-CNDE-END2006-DFZ = -1
        //              MOVE HIGH-VALUES TO DFL-CNDE-END2006-DFZ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getCndeEnd2006Dfz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-CNDE-END2006-DFZ-NULL
            dForzLiq.getDflCndeEnd2006Dfz().setDflCndeEnd2006DfzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflCndeEnd2006Dfz.Len.DFL_CNDE_END2006_DFZ_NULL));
        }
        // COB_CODE: IF IND-DFL-CNDE-DAL2007-CALC = -1
        //              MOVE HIGH-VALUES TO DFL-CNDE-DAL2007-CALC-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getCndeDal2007Calc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-CNDE-DAL2007-CALC-NULL
            dForzLiq.getDflCndeDal2007Calc().setDflCndeDal2007CalcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflCndeDal2007Calc.Len.DFL_CNDE_DAL2007_CALC_NULL));
        }
        // COB_CODE: IF IND-DFL-CNDE-DAL2007-EFFLQ = -1
        //              MOVE HIGH-VALUES TO DFL-CNDE-DAL2007-EFFLQ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getCndeDal2007Efflq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-CNDE-DAL2007-EFFLQ-NULL
            dForzLiq.getDflCndeDal2007Efflq().setDflCndeDal2007EfflqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflCndeDal2007Efflq.Len.DFL_CNDE_DAL2007_EFFLQ_NULL));
        }
        // COB_CODE: IF IND-DFL-CNDE-DAL2007-DFZ = -1
        //              MOVE HIGH-VALUES TO DFL-CNDE-DAL2007-DFZ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getCndeDal2007Dfz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-CNDE-DAL2007-DFZ-NULL
            dForzLiq.getDflCndeDal2007Dfz().setDflCndeDal2007DfzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflCndeDal2007Dfz.Len.DFL_CNDE_DAL2007_DFZ_NULL));
        }
        // COB_CODE: IF IND-DFL-AA-CNBZ-END2000-EF = -1
        //              MOVE HIGH-VALUES TO DFL-AA-CNBZ-END2000-EF-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getAaCnbzEnd2000Ef() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-AA-CNBZ-END2000-EF-NULL
            dForzLiq.getDflAaCnbzEnd2000Ef().setDflAaCnbzEnd2000EfNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflAaCnbzEnd2000Ef.Len.DFL_AA_CNBZ_END2000_EF_NULL));
        }
        // COB_CODE: IF IND-DFL-AA-CNBZ-END2006-EF = -1
        //              MOVE HIGH-VALUES TO DFL-AA-CNBZ-END2006-EF-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getAaCnbzEnd2006Ef() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-AA-CNBZ-END2006-EF-NULL
            dForzLiq.getDflAaCnbzEnd2006Ef().setDflAaCnbzEnd2006EfNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflAaCnbzEnd2006Ef.Len.DFL_AA_CNBZ_END2006_EF_NULL));
        }
        // COB_CODE: IF IND-DFL-AA-CNBZ-DAL2007-EF = -1
        //              MOVE HIGH-VALUES TO DFL-AA-CNBZ-DAL2007-EF-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getAaCnbzDal2007Ef() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-AA-CNBZ-DAL2007-EF-NULL
            dForzLiq.getDflAaCnbzDal2007Ef().setDflAaCnbzDal2007EfNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflAaCnbzDal2007Ef.Len.DFL_AA_CNBZ_DAL2007_EF_NULL));
        }
        // COB_CODE: IF IND-DFL-MM-CNBZ-END2000-EF = -1
        //              MOVE HIGH-VALUES TO DFL-MM-CNBZ-END2000-EF-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getMmCnbzEnd2000Ef() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-MM-CNBZ-END2000-EF-NULL
            dForzLiq.getDflMmCnbzEnd2000Ef().setDflMmCnbzEnd2000EfNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflMmCnbzEnd2000Ef.Len.DFL_MM_CNBZ_END2000_EF_NULL));
        }
        // COB_CODE: IF IND-DFL-MM-CNBZ-END2006-EF = -1
        //              MOVE HIGH-VALUES TO DFL-MM-CNBZ-END2006-EF-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getMmCnbzEnd2006Ef() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-MM-CNBZ-END2006-EF-NULL
            dForzLiq.getDflMmCnbzEnd2006Ef().setDflMmCnbzEnd2006EfNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflMmCnbzEnd2006Ef.Len.DFL_MM_CNBZ_END2006_EF_NULL));
        }
        // COB_CODE: IF IND-DFL-MM-CNBZ-DAL2007-EF = -1
        //              MOVE HIGH-VALUES TO DFL-MM-CNBZ-DAL2007-EF-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getMmCnbzDal2007Ef() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-MM-CNBZ-DAL2007-EF-NULL
            dForzLiq.getDflMmCnbzDal2007Ef().setDflMmCnbzDal2007EfNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflMmCnbzDal2007Ef.Len.DFL_MM_CNBZ_DAL2007_EF_NULL));
        }
        // COB_CODE: IF IND-DFL-IMPST-DA-RIMB-EFF = -1
        //              MOVE HIGH-VALUES TO DFL-IMPST-DA-RIMB-EFF-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpstDaRimbEff() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMPST-DA-RIMB-EFF-NULL
            dForzLiq.getDflImpstDaRimbEff().setDflImpstDaRimbEffNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpstDaRimbEff.Len.DFL_IMPST_DA_RIMB_EFF_NULL));
        }
        // COB_CODE: IF IND-DFL-ALQ-TAX-SEP-CALC = -1
        //              MOVE HIGH-VALUES TO DFL-ALQ-TAX-SEP-CALC-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getAlqTaxSepCalc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-ALQ-TAX-SEP-CALC-NULL
            dForzLiq.getDflAlqTaxSepCalc().setDflAlqTaxSepCalcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflAlqTaxSepCalc.Len.DFL_ALQ_TAX_SEP_CALC_NULL));
        }
        // COB_CODE: IF IND-DFL-ALQ-TAX-SEP-EFFLQ = -1
        //              MOVE HIGH-VALUES TO DFL-ALQ-TAX-SEP-EFFLQ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getAlqTaxSepEfflq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-ALQ-TAX-SEP-EFFLQ-NULL
            dForzLiq.getDflAlqTaxSepEfflq().setDflAlqTaxSepEfflqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflAlqTaxSepEfflq.Len.DFL_ALQ_TAX_SEP_EFFLQ_NULL));
        }
        // COB_CODE: IF IND-DFL-ALQ-TAX-SEP-DFZ = -1
        //              MOVE HIGH-VALUES TO DFL-ALQ-TAX-SEP-DFZ-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getAlqTaxSepDfz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-ALQ-TAX-SEP-DFZ-NULL
            dForzLiq.getDflAlqTaxSepDfz().setDflAlqTaxSepDfzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflAlqTaxSepDfz.Len.DFL_ALQ_TAX_SEP_DFZ_NULL));
        }
        // COB_CODE: IF IND-DFL-ALQ-CNBT-INPSTFM-C = -1
        //              MOVE HIGH-VALUES TO DFL-ALQ-CNBT-INPSTFM-C-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getAlqCnbtInpstfmC() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-ALQ-CNBT-INPSTFM-C-NULL
            dForzLiq.getDflAlqCnbtInpstfmC().setDflAlqCnbtInpstfmCNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflAlqCnbtInpstfmC.Len.DFL_ALQ_CNBT_INPSTFM_C_NULL));
        }
        // COB_CODE: IF IND-DFL-ALQ-CNBT-INPSTFM-E = -1
        //              MOVE HIGH-VALUES TO DFL-ALQ-CNBT-INPSTFM-E-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getAlqCnbtInpstfmE() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-ALQ-CNBT-INPSTFM-E-NULL
            dForzLiq.getDflAlqCnbtInpstfmE().setDflAlqCnbtInpstfmENull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflAlqCnbtInpstfmE.Len.DFL_ALQ_CNBT_INPSTFM_E_NULL));
        }
        // COB_CODE: IF IND-DFL-ALQ-CNBT-INPSTFM-D = -1
        //              MOVE HIGH-VALUES TO DFL-ALQ-CNBT-INPSTFM-D-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getAlqCnbtInpstfmD() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-ALQ-CNBT-INPSTFM-D-NULL
            dForzLiq.getDflAlqCnbtInpstfmD().setDflAlqCnbtInpstfmDNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflAlqCnbtInpstfmD.Len.DFL_ALQ_CNBT_INPSTFM_D_NULL));
        }
        // COB_CODE: IF IND-DFL-IMPB-VIS-1382011C = -1
        //              MOVE HIGH-VALUES TO DFL-IMPB-VIS-1382011C-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpbVis1382011c() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMPB-VIS-1382011C-NULL
            dForzLiq.getDflImpbVis1382011c().setDflImpbVis1382011cNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpbVis1382011c.Len.DFL_IMPB_VIS1382011C_NULL));
        }
        // COB_CODE: IF IND-DFL-IMPB-VIS-1382011D = -1
        //              MOVE HIGH-VALUES TO DFL-IMPB-VIS-1382011D-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpbVis1382011d() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMPB-VIS-1382011D-NULL
            dForzLiq.getDflImpbVis1382011d().setDflImpbVis1382011dNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpbVis1382011d.Len.DFL_IMPB_VIS1382011D_NULL));
        }
        // COB_CODE: IF IND-DFL-IMPB-VIS-1382011L = -1
        //              MOVE HIGH-VALUES TO DFL-IMPB-VIS-1382011L-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpbVis1382011l() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMPB-VIS-1382011L-NULL
            dForzLiq.getDflImpbVis1382011l().setDflImpbVis1382011lNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpbVis1382011l.Len.DFL_IMPB_VIS1382011L_NULL));
        }
        // COB_CODE: IF IND-DFL-IMPST-VIS-1382011C = -1
        //              MOVE HIGH-VALUES TO DFL-IMPST-VIS-1382011C-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpstVis1382011c() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMPST-VIS-1382011C-NULL
            dForzLiq.getDflImpstVis1382011c().setDflImpstVis1382011cNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpstVis1382011c.Len.DFL_IMPST_VIS1382011C_NULL));
        }
        // COB_CODE: IF IND-DFL-IMPST-VIS-1382011D = -1
        //              MOVE HIGH-VALUES TO DFL-IMPST-VIS-1382011D-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpstVis1382011d() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMPST-VIS-1382011D-NULL
            dForzLiq.getDflImpstVis1382011d().setDflImpstVis1382011dNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpstVis1382011d.Len.DFL_IMPST_VIS1382011D_NULL));
        }
        // COB_CODE: IF IND-DFL-IMPST-VIS-1382011L = -1
        //              MOVE HIGH-VALUES TO DFL-IMPST-VIS-1382011L-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpstVis1382011l() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMPST-VIS-1382011L-NULL
            dForzLiq.getDflImpstVis1382011l().setDflImpstVis1382011lNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpstVis1382011l.Len.DFL_IMPST_VIS1382011L_NULL));
        }
        // COB_CODE: IF IND-DFL-IMPB-IS-1382011C = -1
        //              MOVE HIGH-VALUES TO DFL-IMPB-IS-1382011C-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpbIs1382011c() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMPB-IS-1382011C-NULL
            dForzLiq.getDflImpbIs1382011c().setDflImpbIs1382011cNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpbIs1382011c.Len.DFL_IMPB_IS1382011C_NULL));
        }
        // COB_CODE: IF IND-DFL-IMPB-IS-1382011D = -1
        //              MOVE HIGH-VALUES TO DFL-IMPB-IS-1382011D-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpbIs1382011d() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMPB-IS-1382011D-NULL
            dForzLiq.getDflImpbIs1382011d().setDflImpbIs1382011dNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpbIs1382011d.Len.DFL_IMPB_IS1382011D_NULL));
        }
        // COB_CODE: IF IND-DFL-IMPB-IS-1382011L = -1
        //              MOVE HIGH-VALUES TO DFL-IMPB-IS-1382011L-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpbIs1382011l() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMPB-IS-1382011L-NULL
            dForzLiq.getDflImpbIs1382011l().setDflImpbIs1382011lNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpbIs1382011l.Len.DFL_IMPB_IS1382011L_NULL));
        }
        // COB_CODE: IF IND-DFL-IS-1382011C = -1
        //              MOVE HIGH-VALUES TO DFL-IS-1382011C-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getIs1382011c() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IS-1382011C-NULL
            dForzLiq.getDflIs1382011c().setDflIs1382011cNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflIs1382011c.Len.DFL_IS1382011C_NULL));
        }
        // COB_CODE: IF IND-DFL-IS-1382011D = -1
        //              MOVE HIGH-VALUES TO DFL-IS-1382011D-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getIs1382011d() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IS-1382011D-NULL
            dForzLiq.getDflIs1382011d().setDflIs1382011dNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflIs1382011d.Len.DFL_IS1382011D_NULL));
        }
        // COB_CODE: IF IND-DFL-IS-1382011L = -1
        //              MOVE HIGH-VALUES TO DFL-IS-1382011L-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getIs1382011l() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IS-1382011L-NULL
            dForzLiq.getDflIs1382011l().setDflIs1382011lNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflIs1382011l.Len.DFL_IS1382011L_NULL));
        }
        // COB_CODE: IF IND-DFL-IMP-INTR-RIT-PAG-C = -1
        //              MOVE HIGH-VALUES TO DFL-IMP-INTR-RIT-PAG-C-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpIntrRitPagC() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMP-INTR-RIT-PAG-C-NULL
            dForzLiq.getDflImpIntrRitPagC().setDflImpIntrRitPagCNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpIntrRitPagC.Len.DFL_IMP_INTR_RIT_PAG_C_NULL));
        }
        // COB_CODE: IF IND-DFL-IMP-INTR-RIT-PAG-D = -1
        //              MOVE HIGH-VALUES TO DFL-IMP-INTR-RIT-PAG-D-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpIntrRitPagD() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMP-INTR-RIT-PAG-D-NULL
            dForzLiq.getDflImpIntrRitPagD().setDflImpIntrRitPagDNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpIntrRitPagD.Len.DFL_IMP_INTR_RIT_PAG_D_NULL));
        }
        // COB_CODE: IF IND-DFL-IMP-INTR-RIT-PAG-L = -1
        //              MOVE HIGH-VALUES TO DFL-IMP-INTR-RIT-PAG-L-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpIntrRitPagL() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMP-INTR-RIT-PAG-L-NULL
            dForzLiq.getDflImpIntrRitPagL().setDflImpIntrRitPagLNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpIntrRitPagL.Len.DFL_IMP_INTR_RIT_PAG_L_NULL));
        }
        // COB_CODE: IF IND-DFL-IMPB-BOLLO-DETT-C = -1
        //              MOVE HIGH-VALUES TO DFL-IMPB-BOLLO-DETT-C-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpbBolloDettC() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMPB-BOLLO-DETT-C-NULL
            dForzLiq.getDflImpbBolloDettC().setDflImpbBolloDettCNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpbBolloDettC.Len.DFL_IMPB_BOLLO_DETT_C_NULL));
        }
        // COB_CODE: IF IND-DFL-IMPB-BOLLO-DETT-D = -1
        //              MOVE HIGH-VALUES TO DFL-IMPB-BOLLO-DETT-D-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpbBolloDettD() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMPB-BOLLO-DETT-D-NULL
            dForzLiq.getDflImpbBolloDettD().setDflImpbBolloDettDNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpbBolloDettD.Len.DFL_IMPB_BOLLO_DETT_D_NULL));
        }
        // COB_CODE: IF IND-DFL-IMPB-BOLLO-DETT-L = -1
        //              MOVE HIGH-VALUES TO DFL-IMPB-BOLLO-DETT-L-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpbBolloDettL() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMPB-BOLLO-DETT-L-NULL
            dForzLiq.getDflImpbBolloDettL().setDflImpbBolloDettLNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpbBolloDettL.Len.DFL_IMPB_BOLLO_DETT_L_NULL));
        }
        // COB_CODE: IF IND-DFL-IMPST-BOLLO-DETT-C = -1
        //              MOVE HIGH-VALUES TO DFL-IMPST-BOLLO-DETT-C-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpstBolloDettC() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMPST-BOLLO-DETT-C-NULL
            dForzLiq.getDflImpstBolloDettC().setDflImpstBolloDettCNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpstBolloDettC.Len.DFL_IMPST_BOLLO_DETT_C_NULL));
        }
        // COB_CODE: IF IND-DFL-IMPST-BOLLO-DETT-D = -1
        //              MOVE HIGH-VALUES TO DFL-IMPST-BOLLO-DETT-D-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpstBolloDettD() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMPST-BOLLO-DETT-D-NULL
            dForzLiq.getDflImpstBolloDettD().setDflImpstBolloDettDNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpstBolloDettD.Len.DFL_IMPST_BOLLO_DETT_D_NULL));
        }
        // COB_CODE: IF IND-DFL-IMPST-BOLLO-DETT-L = -1
        //              MOVE HIGH-VALUES TO DFL-IMPST-BOLLO-DETT-L-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpstBolloDettL() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMPST-BOLLO-DETT-L-NULL
            dForzLiq.getDflImpstBolloDettL().setDflImpstBolloDettLNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpstBolloDettL.Len.DFL_IMPST_BOLLO_DETT_L_NULL));
        }
        // COB_CODE: IF IND-DFL-IMPST-BOLLO-TOT-VC = -1
        //              MOVE HIGH-VALUES TO DFL-IMPST-BOLLO-TOT-VC-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpstBolloTotVc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMPST-BOLLO-TOT-VC-NULL
            dForzLiq.getDflImpstBolloTotVc().setDflImpstBolloTotVcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpstBolloTotVc.Len.DFL_IMPST_BOLLO_TOT_VC_NULL));
        }
        // COB_CODE: IF IND-DFL-IMPST-BOLLO-TOT-VD = -1
        //              MOVE HIGH-VALUES TO DFL-IMPST-BOLLO-TOT-VD-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpstBolloTotVd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMPST-BOLLO-TOT-VD-NULL
            dForzLiq.getDflImpstBolloTotVd().setDflImpstBolloTotVdNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpstBolloTotVd.Len.DFL_IMPST_BOLLO_TOT_VD_NULL));
        }
        // COB_CODE: IF IND-DFL-IMPST-BOLLO-TOT-VL = -1
        //              MOVE HIGH-VALUES TO DFL-IMPST-BOLLO-TOT-VL-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpstBolloTotVl() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMPST-BOLLO-TOT-VL-NULL
            dForzLiq.getDflImpstBolloTotVl().setDflImpstBolloTotVlNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpstBolloTotVl.Len.DFL_IMPST_BOLLO_TOT_VL_NULL));
        }
        // COB_CODE: IF IND-DFL-IMPB-VIS-662014C = -1
        //              MOVE HIGH-VALUES TO DFL-IMPB-VIS-662014C-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpbVis662014c() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMPB-VIS-662014C-NULL
            dForzLiq.getDflImpbVis662014c().setDflImpbVis662014cNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpbVis662014c.Len.DFL_IMPB_VIS662014C_NULL));
        }
        // COB_CODE: IF IND-DFL-IMPB-VIS-662014D = -1
        //              MOVE HIGH-VALUES TO DFL-IMPB-VIS-662014D-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpbVis662014d() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMPB-VIS-662014D-NULL
            dForzLiq.getDflImpbVis662014d().setDflImpbVis662014dNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpbVis662014d.Len.DFL_IMPB_VIS662014D_NULL));
        }
        // COB_CODE: IF IND-DFL-IMPB-VIS-662014L = -1
        //              MOVE HIGH-VALUES TO DFL-IMPB-VIS-662014L-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpbVis662014l() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMPB-VIS-662014L-NULL
            dForzLiq.getDflImpbVis662014l().setDflImpbVis662014lNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpbVis662014l.Len.DFL_IMPB_VIS662014L_NULL));
        }
        // COB_CODE: IF IND-DFL-IMPST-VIS-662014C = -1
        //              MOVE HIGH-VALUES TO DFL-IMPST-VIS-662014C-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpstVis662014c() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMPST-VIS-662014C-NULL
            dForzLiq.getDflImpstVis662014c().setDflImpstVis662014cNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpstVis662014c.Len.DFL_IMPST_VIS662014C_NULL));
        }
        // COB_CODE: IF IND-DFL-IMPST-VIS-662014D = -1
        //              MOVE HIGH-VALUES TO DFL-IMPST-VIS-662014D-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpstVis662014d() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMPST-VIS-662014D-NULL
            dForzLiq.getDflImpstVis662014d().setDflImpstVis662014dNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpstVis662014d.Len.DFL_IMPST_VIS662014D_NULL));
        }
        // COB_CODE: IF IND-DFL-IMPST-VIS-662014L = -1
        //              MOVE HIGH-VALUES TO DFL-IMPST-VIS-662014L-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpstVis662014l() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMPST-VIS-662014L-NULL
            dForzLiq.getDflImpstVis662014l().setDflImpstVis662014lNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpstVis662014l.Len.DFL_IMPST_VIS662014L_NULL));
        }
        // COB_CODE: IF IND-DFL-IMPB-IS-662014C = -1
        //              MOVE HIGH-VALUES TO DFL-IMPB-IS-662014C-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpbIs662014c() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMPB-IS-662014C-NULL
            dForzLiq.getDflImpbIs662014c().setDflImpbIs662014cNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpbIs662014c.Len.DFL_IMPB_IS662014C_NULL));
        }
        // COB_CODE: IF IND-DFL-IMPB-IS-662014D = -1
        //              MOVE HIGH-VALUES TO DFL-IMPB-IS-662014D-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpbIs662014d() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMPB-IS-662014D-NULL
            dForzLiq.getDflImpbIs662014d().setDflImpbIs662014dNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpbIs662014d.Len.DFL_IMPB_IS662014D_NULL));
        }
        // COB_CODE: IF IND-DFL-IMPB-IS-662014L = -1
        //              MOVE HIGH-VALUES TO DFL-IMPB-IS-662014L-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getImpbIs662014l() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IMPB-IS-662014L-NULL
            dForzLiq.getDflImpbIs662014l().setDflImpbIs662014lNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflImpbIs662014l.Len.DFL_IMPB_IS662014L_NULL));
        }
        // COB_CODE: IF IND-DFL-IS-662014C = -1
        //              MOVE HIGH-VALUES TO DFL-IS-662014C-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getIs662014c() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IS-662014C-NULL
            dForzLiq.getDflIs662014c().setDflIs662014cNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflIs662014c.Len.DFL_IS662014C_NULL));
        }
        // COB_CODE: IF IND-DFL-IS-662014D = -1
        //              MOVE HIGH-VALUES TO DFL-IS-662014D-NULL
        //           END-IF
        if (ws.getIndDForzLiq().getIs662014d() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IS-662014D-NULL
            dForzLiq.getDflIs662014d().setDflIs662014dNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflIs662014d.Len.DFL_IS662014D_NULL));
        }
        // COB_CODE: IF IND-DFL-IS-662014L = -1
        //              MOVE HIGH-VALUES TO DFL-IS-662014L-NULL
        //           END-IF.
        if (ws.getIndDForzLiq().getIs662014l() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFL-IS-662014L-NULL
            dForzLiq.getDflIs662014l().setDflIs662014lNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflIs662014l.Len.DFL_IS662014L_NULL));
        }
    }

    /**Original name: Z150-VALORIZZA-DATA-SERVICES-I<br>*/
    private void z150ValorizzaDataServicesI() {
        // COB_CODE: MOVE 'I' TO DFL-DS-OPER-SQL
        dForzLiq.setDflDsOperSqlFormatted("I");
        // COB_CODE: MOVE 0                   TO DFL-DS-VER
        dForzLiq.setDflDsVer(0);
        // COB_CODE: MOVE IDSV0003-USER-NAME TO DFL-DS-UTENTE
        dForzLiq.setDflDsUtente(idsv0003.getUserName());
        // COB_CODE: MOVE '1'                   TO DFL-DS-STATO-ELAB.
        dForzLiq.setDflDsStatoElabFormatted("1");
    }

    /**Original name: Z160-VALORIZZA-DATA-SERVICES-U<br>*/
    private void z160ValorizzaDataServicesU() {
        // COB_CODE: MOVE 'U' TO DFL-DS-OPER-SQL
        dForzLiq.setDflDsOperSqlFormatted("U");
        // COB_CODE: MOVE IDSV0003-USER-NAME TO DFL-DS-UTENTE.
        dForzLiq.setDflDsUtente(idsv0003.getUserName());
    }

    /**Original name: Z200-SET-INDICATORI-NULL<br>*/
    private void z200SetIndicatoriNull() {
        // COB_CODE: IF DFL-ID-MOVI-CHIU-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-ID-MOVI-CHIU
        //           ELSE
        //              MOVE 0 TO IND-DFL-ID-MOVI-CHIU
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflIdMoviChiu().getDflIdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-ID-MOVI-CHIU
            ws.getIndDForzLiq().setIdMoviChiu(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-ID-MOVI-CHIU
            ws.getIndDForzLiq().setIdMoviChiu(((short)0));
        }
        // COB_CODE: IF DFL-IMP-LRD-CALC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMP-LRD-CALC
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMP-LRD-CALC
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpLrdCalc().getDflImpLrdCalcNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMP-LRD-CALC
            ws.getIndDForzLiq().setImpLrdCalc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMP-LRD-CALC
            ws.getIndDForzLiq().setImpLrdCalc(((short)0));
        }
        // COB_CODE: IF DFL-IMP-LRD-DFZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMP-LRD-DFZ
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMP-LRD-DFZ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpLrdDfz().getDflImpLrdDfzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMP-LRD-DFZ
            ws.getIndDForzLiq().setImpLrdDfz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMP-LRD-DFZ
            ws.getIndDForzLiq().setImpLrdDfz(((short)0));
        }
        // COB_CODE: IF DFL-IMP-LRD-EFFLQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMP-LRD-EFFLQ
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMP-LRD-EFFLQ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpLrdEfflq().getDflImpLrdEfflqNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMP-LRD-EFFLQ
            ws.getIndDForzLiq().setImpLrdEfflq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMP-LRD-EFFLQ
            ws.getIndDForzLiq().setImpLrdEfflq(((short)0));
        }
        // COB_CODE: IF DFL-IMP-NET-CALC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMP-NET-CALC
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMP-NET-CALC
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpNetCalc().getDflImpNetCalcNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMP-NET-CALC
            ws.getIndDForzLiq().setImpNetCalc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMP-NET-CALC
            ws.getIndDForzLiq().setImpNetCalc(((short)0));
        }
        // COB_CODE: IF DFL-IMP-NET-DFZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMP-NET-DFZ
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMP-NET-DFZ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpNetDfz().getDflImpNetDfzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMP-NET-DFZ
            ws.getIndDForzLiq().setImpNetDfz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMP-NET-DFZ
            ws.getIndDForzLiq().setImpNetDfz(((short)0));
        }
        // COB_CODE: IF DFL-IMP-NET-EFFLQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMP-NET-EFFLQ
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMP-NET-EFFLQ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpNetEfflq().getDflImpNetEfflqNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMP-NET-EFFLQ
            ws.getIndDForzLiq().setImpNetEfflq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMP-NET-EFFLQ
            ws.getIndDForzLiq().setImpNetEfflq(((short)0));
        }
        // COB_CODE: IF DFL-IMPST-PRVR-CALC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMPST-PRVR-CALC
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMPST-PRVR-CALC
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpstPrvrCalc().getDflImpstPrvrCalcNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMPST-PRVR-CALC
            ws.getIndDForzLiq().setImpstPrvrCalc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMPST-PRVR-CALC
            ws.getIndDForzLiq().setImpstPrvrCalc(((short)0));
        }
        // COB_CODE: IF DFL-IMPST-PRVR-DFZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMPST-PRVR-DFZ
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMPST-PRVR-DFZ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpstPrvrDfz().getDflImpstPrvrDfzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMPST-PRVR-DFZ
            ws.getIndDForzLiq().setImpstPrvrDfz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMPST-PRVR-DFZ
            ws.getIndDForzLiq().setImpstPrvrDfz(((short)0));
        }
        // COB_CODE: IF DFL-IMPST-PRVR-EFFLQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMPST-PRVR-EFFLQ
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMPST-PRVR-EFFLQ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpstPrvrEfflq().getDflImpstPrvrEfflqNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMPST-PRVR-EFFLQ
            ws.getIndDForzLiq().setImpstPrvrEfflq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMPST-PRVR-EFFLQ
            ws.getIndDForzLiq().setImpstPrvrEfflq(((short)0));
        }
        // COB_CODE: IF DFL-IMPST-VIS-CALC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMPST-VIS-CALC
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMPST-VIS-CALC
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpstVisCalc().getDflImpstVisCalcNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMPST-VIS-CALC
            ws.getIndDForzLiq().setImpstVisCalc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMPST-VIS-CALC
            ws.getIndDForzLiq().setImpstVisCalc(((short)0));
        }
        // COB_CODE: IF DFL-IMPST-VIS-DFZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMPST-VIS-DFZ
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMPST-VIS-DFZ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpstVisDfz().getDflImpstVisDfzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMPST-VIS-DFZ
            ws.getIndDForzLiq().setImpstVisDfz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMPST-VIS-DFZ
            ws.getIndDForzLiq().setImpstVisDfz(((short)0));
        }
        // COB_CODE: IF DFL-IMPST-VIS-EFFLQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMPST-VIS-EFFLQ
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMPST-VIS-EFFLQ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpstVisEfflq().getDflImpstVisEfflqNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMPST-VIS-EFFLQ
            ws.getIndDForzLiq().setImpstVisEfflq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMPST-VIS-EFFLQ
            ws.getIndDForzLiq().setImpstVisEfflq(((short)0));
        }
        // COB_CODE: IF DFL-RIT-ACC-CALC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-RIT-ACC-CALC
        //           ELSE
        //              MOVE 0 TO IND-DFL-RIT-ACC-CALC
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflRitAccCalc().getDflRitAccCalcNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-RIT-ACC-CALC
            ws.getIndDForzLiq().setRitAccCalc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-RIT-ACC-CALC
            ws.getIndDForzLiq().setRitAccCalc(((short)0));
        }
        // COB_CODE: IF DFL-RIT-ACC-DFZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-RIT-ACC-DFZ
        //           ELSE
        //              MOVE 0 TO IND-DFL-RIT-ACC-DFZ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflRitAccDfz().getDflRitAccDfzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-RIT-ACC-DFZ
            ws.getIndDForzLiq().setRitAccDfz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-RIT-ACC-DFZ
            ws.getIndDForzLiq().setRitAccDfz(((short)0));
        }
        // COB_CODE: IF DFL-RIT-ACC-EFFLQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-RIT-ACC-EFFLQ
        //           ELSE
        //              MOVE 0 TO IND-DFL-RIT-ACC-EFFLQ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflRitAccEfflq().getDflRitAccEfflqNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-RIT-ACC-EFFLQ
            ws.getIndDForzLiq().setRitAccEfflq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-RIT-ACC-EFFLQ
            ws.getIndDForzLiq().setRitAccEfflq(((short)0));
        }
        // COB_CODE: IF DFL-RIT-IRPEF-CALC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-RIT-IRPEF-CALC
        //           ELSE
        //              MOVE 0 TO IND-DFL-RIT-IRPEF-CALC
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflRitIrpefCalc().getDflRitIrpefCalcNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-RIT-IRPEF-CALC
            ws.getIndDForzLiq().setRitIrpefCalc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-RIT-IRPEF-CALC
            ws.getIndDForzLiq().setRitIrpefCalc(((short)0));
        }
        // COB_CODE: IF DFL-RIT-IRPEF-DFZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-RIT-IRPEF-DFZ
        //           ELSE
        //              MOVE 0 TO IND-DFL-RIT-IRPEF-DFZ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflRitIrpefDfz().getDflRitIrpefDfzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-RIT-IRPEF-DFZ
            ws.getIndDForzLiq().setRitIrpefDfz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-RIT-IRPEF-DFZ
            ws.getIndDForzLiq().setRitIrpefDfz(((short)0));
        }
        // COB_CODE: IF DFL-RIT-IRPEF-EFFLQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-RIT-IRPEF-EFFLQ
        //           ELSE
        //              MOVE 0 TO IND-DFL-RIT-IRPEF-EFFLQ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflRitIrpefEfflq().getDflRitIrpefEfflqNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-RIT-IRPEF-EFFLQ
            ws.getIndDForzLiq().setRitIrpefEfflq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-RIT-IRPEF-EFFLQ
            ws.getIndDForzLiq().setRitIrpefEfflq(((short)0));
        }
        // COB_CODE: IF DFL-IMPST-SOST-CALC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMPST-SOST-CALC
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMPST-SOST-CALC
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpstSostCalc().getDflImpstSostCalcNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMPST-SOST-CALC
            ws.getIndDForzLiq().setImpstSostCalc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMPST-SOST-CALC
            ws.getIndDForzLiq().setImpstSostCalc(((short)0));
        }
        // COB_CODE: IF DFL-IMPST-SOST-DFZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMPST-SOST-DFZ
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMPST-SOST-DFZ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpstSostDfz().getDflImpstSostDfzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMPST-SOST-DFZ
            ws.getIndDForzLiq().setImpstSostDfz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMPST-SOST-DFZ
            ws.getIndDForzLiq().setImpstSostDfz(((short)0));
        }
        // COB_CODE: IF DFL-IMPST-SOST-EFFLQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMPST-SOST-EFFLQ
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMPST-SOST-EFFLQ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpstSostEfflq().getDflImpstSostEfflqNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMPST-SOST-EFFLQ
            ws.getIndDForzLiq().setImpstSostEfflq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMPST-SOST-EFFLQ
            ws.getIndDForzLiq().setImpstSostEfflq(((short)0));
        }
        // COB_CODE: IF DFL-TAX-SEP-CALC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-TAX-SEP-CALC
        //           ELSE
        //              MOVE 0 TO IND-DFL-TAX-SEP-CALC
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflTaxSepCalc().getDflTaxSepCalcNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-TAX-SEP-CALC
            ws.getIndDForzLiq().setTaxSepCalc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-TAX-SEP-CALC
            ws.getIndDForzLiq().setTaxSepCalc(((short)0));
        }
        // COB_CODE: IF DFL-TAX-SEP-DFZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-TAX-SEP-DFZ
        //           ELSE
        //              MOVE 0 TO IND-DFL-TAX-SEP-DFZ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflTaxSepDfz().getDflTaxSepDfzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-TAX-SEP-DFZ
            ws.getIndDForzLiq().setTaxSepDfz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-TAX-SEP-DFZ
            ws.getIndDForzLiq().setTaxSepDfz(((short)0));
        }
        // COB_CODE: IF DFL-TAX-SEP-EFFLQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-TAX-SEP-EFFLQ
        //           ELSE
        //              MOVE 0 TO IND-DFL-TAX-SEP-EFFLQ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflTaxSepEfflq().getDflTaxSepEfflqNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-TAX-SEP-EFFLQ
            ws.getIndDForzLiq().setTaxSepEfflq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-TAX-SEP-EFFLQ
            ws.getIndDForzLiq().setTaxSepEfflq(((short)0));
        }
        // COB_CODE: IF DFL-INTR-PREST-CALC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-INTR-PREST-CALC
        //           ELSE
        //              MOVE 0 TO IND-DFL-INTR-PREST-CALC
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflIntrPrestCalc().getDflIntrPrestCalcNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-INTR-PREST-CALC
            ws.getIndDForzLiq().setIntrPrestCalc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-INTR-PREST-CALC
            ws.getIndDForzLiq().setIntrPrestCalc(((short)0));
        }
        // COB_CODE: IF DFL-INTR-PREST-DFZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-INTR-PREST-DFZ
        //           ELSE
        //              MOVE 0 TO IND-DFL-INTR-PREST-DFZ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflIntrPrestDfz().getDflIntrPrestDfzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-INTR-PREST-DFZ
            ws.getIndDForzLiq().setIntrPrestDfz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-INTR-PREST-DFZ
            ws.getIndDForzLiq().setIntrPrestDfz(((short)0));
        }
        // COB_CODE: IF DFL-INTR-PREST-EFFLQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-INTR-PREST-EFFLQ
        //           ELSE
        //              MOVE 0 TO IND-DFL-INTR-PREST-EFFLQ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflIntrPrestEfflq().getDflIntrPrestEfflqNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-INTR-PREST-EFFLQ
            ws.getIndDForzLiq().setIntrPrestEfflq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-INTR-PREST-EFFLQ
            ws.getIndDForzLiq().setIntrPrestEfflq(((short)0));
        }
        // COB_CODE: IF DFL-ACCPRE-SOST-CALC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-ACCPRE-SOST-CALC
        //           ELSE
        //              MOVE 0 TO IND-DFL-ACCPRE-SOST-CALC
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflAccpreSostCalc().getDflAccpreSostCalcNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-ACCPRE-SOST-CALC
            ws.getIndDForzLiq().setAccpreSostCalc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-ACCPRE-SOST-CALC
            ws.getIndDForzLiq().setAccpreSostCalc(((short)0));
        }
        // COB_CODE: IF DFL-ACCPRE-SOST-DFZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-ACCPRE-SOST-DFZ
        //           ELSE
        //              MOVE 0 TO IND-DFL-ACCPRE-SOST-DFZ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflAccpreSostDfz().getDflAccpreSostDfzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-ACCPRE-SOST-DFZ
            ws.getIndDForzLiq().setAccpreSostDfz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-ACCPRE-SOST-DFZ
            ws.getIndDForzLiq().setAccpreSostDfz(((short)0));
        }
        // COB_CODE: IF DFL-ACCPRE-SOST-EFFLQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-ACCPRE-SOST-EFFLQ
        //           ELSE
        //              MOVE 0 TO IND-DFL-ACCPRE-SOST-EFFLQ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflAccpreSostEfflq().getDflAccpreSostEfflqNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-ACCPRE-SOST-EFFLQ
            ws.getIndDForzLiq().setAccpreSostEfflq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-ACCPRE-SOST-EFFLQ
            ws.getIndDForzLiq().setAccpreSostEfflq(((short)0));
        }
        // COB_CODE: IF DFL-ACCPRE-VIS-CALC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-ACCPRE-VIS-CALC
        //           ELSE
        //              MOVE 0 TO IND-DFL-ACCPRE-VIS-CALC
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflAccpreVisCalc().getDflAccpreVisCalcNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-ACCPRE-VIS-CALC
            ws.getIndDForzLiq().setAccpreVisCalc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-ACCPRE-VIS-CALC
            ws.getIndDForzLiq().setAccpreVisCalc(((short)0));
        }
        // COB_CODE: IF DFL-ACCPRE-VIS-DFZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-ACCPRE-VIS-DFZ
        //           ELSE
        //              MOVE 0 TO IND-DFL-ACCPRE-VIS-DFZ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflAccpreVisDfz().getDflAccpreVisDfzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-ACCPRE-VIS-DFZ
            ws.getIndDForzLiq().setAccpreVisDfz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-ACCPRE-VIS-DFZ
            ws.getIndDForzLiq().setAccpreVisDfz(((short)0));
        }
        // COB_CODE: IF DFL-ACCPRE-VIS-EFFLQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-ACCPRE-VIS-EFFLQ
        //           ELSE
        //              MOVE 0 TO IND-DFL-ACCPRE-VIS-EFFLQ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflAccpreVisEfflq().getDflAccpreVisEfflqNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-ACCPRE-VIS-EFFLQ
            ws.getIndDForzLiq().setAccpreVisEfflq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-ACCPRE-VIS-EFFLQ
            ws.getIndDForzLiq().setAccpreVisEfflq(((short)0));
        }
        // COB_CODE: IF DFL-ACCPRE-ACC-CALC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-ACCPRE-ACC-CALC
        //           ELSE
        //              MOVE 0 TO IND-DFL-ACCPRE-ACC-CALC
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflAccpreAccCalc().getDflAccpreAccCalcNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-ACCPRE-ACC-CALC
            ws.getIndDForzLiq().setAccpreAccCalc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-ACCPRE-ACC-CALC
            ws.getIndDForzLiq().setAccpreAccCalc(((short)0));
        }
        // COB_CODE: IF DFL-ACCPRE-ACC-DFZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-ACCPRE-ACC-DFZ
        //           ELSE
        //              MOVE 0 TO IND-DFL-ACCPRE-ACC-DFZ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflAccpreAccDfz().getDflAccpreAccDfzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-ACCPRE-ACC-DFZ
            ws.getIndDForzLiq().setAccpreAccDfz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-ACCPRE-ACC-DFZ
            ws.getIndDForzLiq().setAccpreAccDfz(((short)0));
        }
        // COB_CODE: IF DFL-ACCPRE-ACC-EFFLQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-ACCPRE-ACC-EFFLQ
        //           ELSE
        //              MOVE 0 TO IND-DFL-ACCPRE-ACC-EFFLQ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflAccpreAccEfflq().getDflAccpreAccEfflqNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-ACCPRE-ACC-EFFLQ
            ws.getIndDForzLiq().setAccpreAccEfflq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-ACCPRE-ACC-EFFLQ
            ws.getIndDForzLiq().setAccpreAccEfflq(((short)0));
        }
        // COB_CODE: IF DFL-RES-PRSTZ-CALC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-RES-PRSTZ-CALC
        //           ELSE
        //              MOVE 0 TO IND-DFL-RES-PRSTZ-CALC
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflResPrstzCalc().getDflResPrstzCalcNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-RES-PRSTZ-CALC
            ws.getIndDForzLiq().setResPrstzCalc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-RES-PRSTZ-CALC
            ws.getIndDForzLiq().setResPrstzCalc(((short)0));
        }
        // COB_CODE: IF DFL-RES-PRSTZ-DFZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-RES-PRSTZ-DFZ
        //           ELSE
        //              MOVE 0 TO IND-DFL-RES-PRSTZ-DFZ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflResPrstzDfz().getDflResPrstzDfzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-RES-PRSTZ-DFZ
            ws.getIndDForzLiq().setResPrstzDfz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-RES-PRSTZ-DFZ
            ws.getIndDForzLiq().setResPrstzDfz(((short)0));
        }
        // COB_CODE: IF DFL-RES-PRSTZ-EFFLQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-RES-PRSTZ-EFFLQ
        //           ELSE
        //              MOVE 0 TO IND-DFL-RES-PRSTZ-EFFLQ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflResPrstzEfflq().getDflResPrstzEfflqNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-RES-PRSTZ-EFFLQ
            ws.getIndDForzLiq().setResPrstzEfflq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-RES-PRSTZ-EFFLQ
            ws.getIndDForzLiq().setResPrstzEfflq(((short)0));
        }
        // COB_CODE: IF DFL-RES-PRE-ATT-CALC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-RES-PRE-ATT-CALC
        //           ELSE
        //              MOVE 0 TO IND-DFL-RES-PRE-ATT-CALC
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflResPreAttCalc().getDflResPreAttCalcNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-RES-PRE-ATT-CALC
            ws.getIndDForzLiq().setResPreAttCalc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-RES-PRE-ATT-CALC
            ws.getIndDForzLiq().setResPreAttCalc(((short)0));
        }
        // COB_CODE: IF DFL-RES-PRE-ATT-DFZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-RES-PRE-ATT-DFZ
        //           ELSE
        //              MOVE 0 TO IND-DFL-RES-PRE-ATT-DFZ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflResPreAttDfz().getDflResPreAttDfzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-RES-PRE-ATT-DFZ
            ws.getIndDForzLiq().setResPreAttDfz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-RES-PRE-ATT-DFZ
            ws.getIndDForzLiq().setResPreAttDfz(((short)0));
        }
        // COB_CODE: IF DFL-RES-PRE-ATT-EFFLQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-RES-PRE-ATT-EFFLQ
        //           ELSE
        //              MOVE 0 TO IND-DFL-RES-PRE-ATT-EFFLQ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflResPreAttEfflq().getDflResPreAttEfflqNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-RES-PRE-ATT-EFFLQ
            ws.getIndDForzLiq().setResPreAttEfflq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-RES-PRE-ATT-EFFLQ
            ws.getIndDForzLiq().setResPreAttEfflq(((short)0));
        }
        // COB_CODE: IF DFL-IMP-EXCONTR-EFF-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMP-EXCONTR-EFF
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMP-EXCONTR-EFF
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpExcontrEff().getDflImpExcontrEffNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMP-EXCONTR-EFF
            ws.getIndDForzLiq().setImpExcontrEff(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMP-EXCONTR-EFF
            ws.getIndDForzLiq().setImpExcontrEff(((short)0));
        }
        // COB_CODE: IF DFL-IMPB-VIS-CALC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMPB-VIS-CALC
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMPB-VIS-CALC
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpbVisCalc().getDflImpbVisCalcNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMPB-VIS-CALC
            ws.getIndDForzLiq().setImpbVisCalc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMPB-VIS-CALC
            ws.getIndDForzLiq().setImpbVisCalc(((short)0));
        }
        // COB_CODE: IF DFL-IMPB-VIS-EFFLQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMPB-VIS-EFFLQ
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMPB-VIS-EFFLQ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpbVisEfflq().getDflImpbVisEfflqNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMPB-VIS-EFFLQ
            ws.getIndDForzLiq().setImpbVisEfflq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMPB-VIS-EFFLQ
            ws.getIndDForzLiq().setImpbVisEfflq(((short)0));
        }
        // COB_CODE: IF DFL-IMPB-VIS-DFZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMPB-VIS-DFZ
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMPB-VIS-DFZ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpbVisDfz().getDflImpbVisDfzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMPB-VIS-DFZ
            ws.getIndDForzLiq().setImpbVisDfz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMPB-VIS-DFZ
            ws.getIndDForzLiq().setImpbVisDfz(((short)0));
        }
        // COB_CODE: IF DFL-IMPB-RIT-ACC-CALC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMPB-RIT-ACC-CALC
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMPB-RIT-ACC-CALC
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpbRitAccCalc().getDflImpbRitAccCalcNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMPB-RIT-ACC-CALC
            ws.getIndDForzLiq().setImpbRitAccCalc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMPB-RIT-ACC-CALC
            ws.getIndDForzLiq().setImpbRitAccCalc(((short)0));
        }
        // COB_CODE: IF DFL-IMPB-RIT-ACC-EFFLQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMPB-RIT-ACC-EFFLQ
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMPB-RIT-ACC-EFFLQ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpbRitAccEfflq().getDflImpbRitAccEfflqNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMPB-RIT-ACC-EFFLQ
            ws.getIndDForzLiq().setImpbRitAccEfflq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMPB-RIT-ACC-EFFLQ
            ws.getIndDForzLiq().setImpbRitAccEfflq(((short)0));
        }
        // COB_CODE: IF DFL-IMPB-RIT-ACC-DFZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMPB-RIT-ACC-DFZ
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMPB-RIT-ACC-DFZ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpbRitAccDfz().getDflImpbRitAccDfzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMPB-RIT-ACC-DFZ
            ws.getIndDForzLiq().setImpbRitAccDfz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMPB-RIT-ACC-DFZ
            ws.getIndDForzLiq().setImpbRitAccDfz(((short)0));
        }
        // COB_CODE: IF DFL-IMPB-TFR-CALC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMPB-TFR-CALC
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMPB-TFR-CALC
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpbTfrCalc().getDflImpbTfrCalcNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMPB-TFR-CALC
            ws.getIndDForzLiq().setImpbTfrCalc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMPB-TFR-CALC
            ws.getIndDForzLiq().setImpbTfrCalc(((short)0));
        }
        // COB_CODE: IF DFL-IMPB-TFR-EFFLQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMPB-TFR-EFFLQ
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMPB-TFR-EFFLQ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpbTfrEfflq().getDflImpbTfrEfflqNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMPB-TFR-EFFLQ
            ws.getIndDForzLiq().setImpbTfrEfflq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMPB-TFR-EFFLQ
            ws.getIndDForzLiq().setImpbTfrEfflq(((short)0));
        }
        // COB_CODE: IF DFL-IMPB-TFR-DFZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMPB-TFR-DFZ
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMPB-TFR-DFZ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpbTfrDfz().getDflImpbTfrDfzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMPB-TFR-DFZ
            ws.getIndDForzLiq().setImpbTfrDfz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMPB-TFR-DFZ
            ws.getIndDForzLiq().setImpbTfrDfz(((short)0));
        }
        // COB_CODE: IF DFL-IMPB-IS-CALC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMPB-IS-CALC
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMPB-IS-CALC
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpbIsCalc().getDflImpbIsCalcNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMPB-IS-CALC
            ws.getIndDForzLiq().setImpbIsCalc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMPB-IS-CALC
            ws.getIndDForzLiq().setImpbIsCalc(((short)0));
        }
        // COB_CODE: IF DFL-IMPB-IS-EFFLQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMPB-IS-EFFLQ
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMPB-IS-EFFLQ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpbIsEfflq().getDflImpbIsEfflqNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMPB-IS-EFFLQ
            ws.getIndDForzLiq().setImpbIsEfflq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMPB-IS-EFFLQ
            ws.getIndDForzLiq().setImpbIsEfflq(((short)0));
        }
        // COB_CODE: IF DFL-IMPB-IS-DFZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMPB-IS-DFZ
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMPB-IS-DFZ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpbIsDfz().getDflImpbIsDfzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMPB-IS-DFZ
            ws.getIndDForzLiq().setImpbIsDfz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMPB-IS-DFZ
            ws.getIndDForzLiq().setImpbIsDfz(((short)0));
        }
        // COB_CODE: IF DFL-IMPB-TAX-SEP-CALC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMPB-TAX-SEP-CALC
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMPB-TAX-SEP-CALC
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpbTaxSepCalc().getDflImpbTaxSepCalcNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMPB-TAX-SEP-CALC
            ws.getIndDForzLiq().setImpbTaxSepCalc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMPB-TAX-SEP-CALC
            ws.getIndDForzLiq().setImpbTaxSepCalc(((short)0));
        }
        // COB_CODE: IF DFL-IMPB-TAX-SEP-EFFLQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMPB-TAX-SEP-EFFLQ
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMPB-TAX-SEP-EFFLQ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpbTaxSepEfflq().getDflImpbTaxSepEfflqNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMPB-TAX-SEP-EFFLQ
            ws.getIndDForzLiq().setImpbTaxSepEfflq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMPB-TAX-SEP-EFFLQ
            ws.getIndDForzLiq().setImpbTaxSepEfflq(((short)0));
        }
        // COB_CODE: IF DFL-IMPB-TAX-SEP-DFZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMPB-TAX-SEP-DFZ
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMPB-TAX-SEP-DFZ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpbTaxSepDfz().getDflImpbTaxSepDfzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMPB-TAX-SEP-DFZ
            ws.getIndDForzLiq().setImpbTaxSepDfz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMPB-TAX-SEP-DFZ
            ws.getIndDForzLiq().setImpbTaxSepDfz(((short)0));
        }
        // COB_CODE: IF DFL-IINT-PREST-CALC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IINT-PREST-CALC
        //           ELSE
        //              MOVE 0 TO IND-DFL-IINT-PREST-CALC
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflIintPrestCalc().getDflIintPrestCalcNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IINT-PREST-CALC
            ws.getIndDForzLiq().setIintPrestCalc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IINT-PREST-CALC
            ws.getIndDForzLiq().setIintPrestCalc(((short)0));
        }
        // COB_CODE: IF DFL-IINT-PREST-EFFLQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IINT-PREST-EFFLQ
        //           ELSE
        //              MOVE 0 TO IND-DFL-IINT-PREST-EFFLQ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflIintPrestEfflq().getDflIintPrestEfflqNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IINT-PREST-EFFLQ
            ws.getIndDForzLiq().setIintPrestEfflq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IINT-PREST-EFFLQ
            ws.getIndDForzLiq().setIintPrestEfflq(((short)0));
        }
        // COB_CODE: IF DFL-IINT-PREST-DFZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IINT-PREST-DFZ
        //           ELSE
        //              MOVE 0 TO IND-DFL-IINT-PREST-DFZ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflIintPrestDfz().getDflIintPrestDfzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IINT-PREST-DFZ
            ws.getIndDForzLiq().setIintPrestDfz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IINT-PREST-DFZ
            ws.getIndDForzLiq().setIintPrestDfz(((short)0));
        }
        // COB_CODE: IF DFL-MONT-END2000-CALC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-MONT-END2000-CALC
        //           ELSE
        //              MOVE 0 TO IND-DFL-MONT-END2000-CALC
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflMontEnd2000Calc().getDflMontEnd2000CalcNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-MONT-END2000-CALC
            ws.getIndDForzLiq().setMontEnd2000Calc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-MONT-END2000-CALC
            ws.getIndDForzLiq().setMontEnd2000Calc(((short)0));
        }
        // COB_CODE: IF DFL-MONT-END2000-EFFLQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-MONT-END2000-EFFLQ
        //           ELSE
        //              MOVE 0 TO IND-DFL-MONT-END2000-EFFLQ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflMontEnd2000Efflq().getDflMontEnd2000EfflqNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-MONT-END2000-EFFLQ
            ws.getIndDForzLiq().setMontEnd2000Efflq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-MONT-END2000-EFFLQ
            ws.getIndDForzLiq().setMontEnd2000Efflq(((short)0));
        }
        // COB_CODE: IF DFL-MONT-END2000-DFZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-MONT-END2000-DFZ
        //           ELSE
        //              MOVE 0 TO IND-DFL-MONT-END2000-DFZ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflMontEnd2000Dfz().getDflMontEnd2000DfzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-MONT-END2000-DFZ
            ws.getIndDForzLiq().setMontEnd2000Dfz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-MONT-END2000-DFZ
            ws.getIndDForzLiq().setMontEnd2000Dfz(((short)0));
        }
        // COB_CODE: IF DFL-MONT-END2006-CALC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-MONT-END2006-CALC
        //           ELSE
        //              MOVE 0 TO IND-DFL-MONT-END2006-CALC
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflMontEnd2006Calc().getDflMontEnd2006CalcNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-MONT-END2006-CALC
            ws.getIndDForzLiq().setMontEnd2006Calc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-MONT-END2006-CALC
            ws.getIndDForzLiq().setMontEnd2006Calc(((short)0));
        }
        // COB_CODE: IF DFL-MONT-END2006-EFFLQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-MONT-END2006-EFFLQ
        //           ELSE
        //              MOVE 0 TO IND-DFL-MONT-END2006-EFFLQ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflMontEnd2006Efflq().getDflMontEnd2006EfflqNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-MONT-END2006-EFFLQ
            ws.getIndDForzLiq().setMontEnd2006Efflq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-MONT-END2006-EFFLQ
            ws.getIndDForzLiq().setMontEnd2006Efflq(((short)0));
        }
        // COB_CODE: IF DFL-MONT-END2006-DFZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-MONT-END2006-DFZ
        //           ELSE
        //              MOVE 0 TO IND-DFL-MONT-END2006-DFZ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflMontEnd2006Dfz().getDflMontEnd2006DfzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-MONT-END2006-DFZ
            ws.getIndDForzLiq().setMontEnd2006Dfz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-MONT-END2006-DFZ
            ws.getIndDForzLiq().setMontEnd2006Dfz(((short)0));
        }
        // COB_CODE: IF DFL-MONT-DAL2007-CALC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-MONT-DAL2007-CALC
        //           ELSE
        //              MOVE 0 TO IND-DFL-MONT-DAL2007-CALC
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflMontDal2007Calc().getDflMontDal2007CalcNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-MONT-DAL2007-CALC
            ws.getIndDForzLiq().setMontDal2007Calc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-MONT-DAL2007-CALC
            ws.getIndDForzLiq().setMontDal2007Calc(((short)0));
        }
        // COB_CODE: IF DFL-MONT-DAL2007-EFFLQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-MONT-DAL2007-EFFLQ
        //           ELSE
        //              MOVE 0 TO IND-DFL-MONT-DAL2007-EFFLQ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflMontDal2007Efflq().getDflMontDal2007EfflqNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-MONT-DAL2007-EFFLQ
            ws.getIndDForzLiq().setMontDal2007Efflq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-MONT-DAL2007-EFFLQ
            ws.getIndDForzLiq().setMontDal2007Efflq(((short)0));
        }
        // COB_CODE: IF DFL-MONT-DAL2007-DFZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-MONT-DAL2007-DFZ
        //           ELSE
        //              MOVE 0 TO IND-DFL-MONT-DAL2007-DFZ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflMontDal2007Dfz().getDflMontDal2007DfzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-MONT-DAL2007-DFZ
            ws.getIndDForzLiq().setMontDal2007Dfz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-MONT-DAL2007-DFZ
            ws.getIndDForzLiq().setMontDal2007Dfz(((short)0));
        }
        // COB_CODE: IF DFL-IIMPST-PRVR-CALC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IIMPST-PRVR-CALC
        //           ELSE
        //              MOVE 0 TO IND-DFL-IIMPST-PRVR-CALC
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflIimpstPrvrCalc().getDflIimpstPrvrCalcNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IIMPST-PRVR-CALC
            ws.getIndDForzLiq().setIimpstPrvrCalc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IIMPST-PRVR-CALC
            ws.getIndDForzLiq().setIimpstPrvrCalc(((short)0));
        }
        // COB_CODE: IF DFL-IIMPST-PRVR-EFFLQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IIMPST-PRVR-EFFLQ
        //           ELSE
        //              MOVE 0 TO IND-DFL-IIMPST-PRVR-EFFLQ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflIimpstPrvrEfflq().getDflIimpstPrvrEfflqNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IIMPST-PRVR-EFFLQ
            ws.getIndDForzLiq().setIimpstPrvrEfflq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IIMPST-PRVR-EFFLQ
            ws.getIndDForzLiq().setIimpstPrvrEfflq(((short)0));
        }
        // COB_CODE: IF DFL-IIMPST-PRVR-DFZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IIMPST-PRVR-DFZ
        //           ELSE
        //              MOVE 0 TO IND-DFL-IIMPST-PRVR-DFZ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflIimpstPrvrDfz().getDflIimpstPrvrDfzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IIMPST-PRVR-DFZ
            ws.getIndDForzLiq().setIimpstPrvrDfz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IIMPST-PRVR-DFZ
            ws.getIndDForzLiq().setIimpstPrvrDfz(((short)0));
        }
        // COB_CODE: IF DFL-IIMPST-252-CALC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IIMPST-252-CALC
        //           ELSE
        //              MOVE 0 TO IND-DFL-IIMPST-252-CALC
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflIimpst252Calc().getDflIimpst252CalcNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IIMPST-252-CALC
            ws.getIndDForzLiq().setIimpst252Calc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IIMPST-252-CALC
            ws.getIndDForzLiq().setIimpst252Calc(((short)0));
        }
        // COB_CODE: IF DFL-IIMPST-252-EFFLQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IIMPST-252-EFFLQ
        //           ELSE
        //              MOVE 0 TO IND-DFL-IIMPST-252-EFFLQ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflIimpst252Efflq().getDflIimpst252EfflqNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IIMPST-252-EFFLQ
            ws.getIndDForzLiq().setIimpst252Efflq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IIMPST-252-EFFLQ
            ws.getIndDForzLiq().setIimpst252Efflq(((short)0));
        }
        // COB_CODE: IF DFL-IIMPST-252-DFZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IIMPST-252-DFZ
        //           ELSE
        //              MOVE 0 TO IND-DFL-IIMPST-252-DFZ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflIimpst252Dfz().getDflIimpst252DfzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IIMPST-252-DFZ
            ws.getIndDForzLiq().setIimpst252Dfz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IIMPST-252-DFZ
            ws.getIndDForzLiq().setIimpst252Dfz(((short)0));
        }
        // COB_CODE: IF DFL-IMPST-252-CALC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMPST-252-CALC
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMPST-252-CALC
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpst252Calc().getDflImpst252CalcNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMPST-252-CALC
            ws.getIndDForzLiq().setImpst252Calc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMPST-252-CALC
            ws.getIndDForzLiq().setImpst252Calc(((short)0));
        }
        // COB_CODE: IF DFL-IMPST-252-EFFLQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMPST-252-EFFLQ
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMPST-252-EFFLQ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpst252Efflq().getDflImpst252EfflqNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMPST-252-EFFLQ
            ws.getIndDForzLiq().setImpst252Efflq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMPST-252-EFFLQ
            ws.getIndDForzLiq().setImpst252Efflq(((short)0));
        }
        // COB_CODE: IF DFL-RIT-TFR-CALC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-RIT-TFR-CALC
        //           ELSE
        //              MOVE 0 TO IND-DFL-RIT-TFR-CALC
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflRitTfrCalc().getDflRitTfrCalcNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-RIT-TFR-CALC
            ws.getIndDForzLiq().setRitTfrCalc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-RIT-TFR-CALC
            ws.getIndDForzLiq().setRitTfrCalc(((short)0));
        }
        // COB_CODE: IF DFL-RIT-TFR-EFFLQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-RIT-TFR-EFFLQ
        //           ELSE
        //              MOVE 0 TO IND-DFL-RIT-TFR-EFFLQ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflRitTfrEfflq().getDflRitTfrEfflqNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-RIT-TFR-EFFLQ
            ws.getIndDForzLiq().setRitTfrEfflq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-RIT-TFR-EFFLQ
            ws.getIndDForzLiq().setRitTfrEfflq(((short)0));
        }
        // COB_CODE: IF DFL-RIT-TFR-DFZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-RIT-TFR-DFZ
        //           ELSE
        //              MOVE 0 TO IND-DFL-RIT-TFR-DFZ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflRitTfrDfz().getDflRitTfrDfzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-RIT-TFR-DFZ
            ws.getIndDForzLiq().setRitTfrDfz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-RIT-TFR-DFZ
            ws.getIndDForzLiq().setRitTfrDfz(((short)0));
        }
        // COB_CODE: IF DFL-CNBT-INPSTFM-CALC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-CNBT-INPSTFM-CALC
        //           ELSE
        //              MOVE 0 TO IND-DFL-CNBT-INPSTFM-CALC
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflCnbtInpstfmCalc().getDflCnbtInpstfmCalcNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-CNBT-INPSTFM-CALC
            ws.getIndDForzLiq().setCnbtInpstfmCalc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-CNBT-INPSTFM-CALC
            ws.getIndDForzLiq().setCnbtInpstfmCalc(((short)0));
        }
        // COB_CODE: IF DFL-CNBT-INPSTFM-EFFLQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-CNBT-INPSTFM-EFFLQ
        //           ELSE
        //              MOVE 0 TO IND-DFL-CNBT-INPSTFM-EFFLQ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflCnbtInpstfmEfflq().getDflCnbtInpstfmEfflqNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-CNBT-INPSTFM-EFFLQ
            ws.getIndDForzLiq().setCnbtInpstfmEfflq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-CNBT-INPSTFM-EFFLQ
            ws.getIndDForzLiq().setCnbtInpstfmEfflq(((short)0));
        }
        // COB_CODE: IF DFL-CNBT-INPSTFM-DFZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-CNBT-INPSTFM-DFZ
        //           ELSE
        //              MOVE 0 TO IND-DFL-CNBT-INPSTFM-DFZ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflCnbtInpstfmDfz().getDflCnbtInpstfmDfzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-CNBT-INPSTFM-DFZ
            ws.getIndDForzLiq().setCnbtInpstfmDfz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-CNBT-INPSTFM-DFZ
            ws.getIndDForzLiq().setCnbtInpstfmDfz(((short)0));
        }
        // COB_CODE: IF DFL-ICNB-INPSTFM-CALC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-ICNB-INPSTFM-CALC
        //           ELSE
        //              MOVE 0 TO IND-DFL-ICNB-INPSTFM-CALC
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflIcnbInpstfmCalc().getDflIcnbInpstfmCalcNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-ICNB-INPSTFM-CALC
            ws.getIndDForzLiq().setIcnbInpstfmCalc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-ICNB-INPSTFM-CALC
            ws.getIndDForzLiq().setIcnbInpstfmCalc(((short)0));
        }
        // COB_CODE: IF DFL-ICNB-INPSTFM-EFFLQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-ICNB-INPSTFM-EFFLQ
        //           ELSE
        //              MOVE 0 TO IND-DFL-ICNB-INPSTFM-EFFLQ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflIcnbInpstfmEfflq().getDflIcnbInpstfmEfflqNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-ICNB-INPSTFM-EFFLQ
            ws.getIndDForzLiq().setIcnbInpstfmEfflq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-ICNB-INPSTFM-EFFLQ
            ws.getIndDForzLiq().setIcnbInpstfmEfflq(((short)0));
        }
        // COB_CODE: IF DFL-ICNB-INPSTFM-DFZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-ICNB-INPSTFM-DFZ
        //           ELSE
        //              MOVE 0 TO IND-DFL-ICNB-INPSTFM-DFZ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflIcnbInpstfmDfz().getDflIcnbInpstfmDfzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-ICNB-INPSTFM-DFZ
            ws.getIndDForzLiq().setIcnbInpstfmDfz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-ICNB-INPSTFM-DFZ
            ws.getIndDForzLiq().setIcnbInpstfmDfz(((short)0));
        }
        // COB_CODE: IF DFL-CNDE-END2000-CALC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-CNDE-END2000-CALC
        //           ELSE
        //              MOVE 0 TO IND-DFL-CNDE-END2000-CALC
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflCndeEnd2000Calc().getDflCndeEnd2000CalcNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-CNDE-END2000-CALC
            ws.getIndDForzLiq().setCndeEnd2000Calc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-CNDE-END2000-CALC
            ws.getIndDForzLiq().setCndeEnd2000Calc(((short)0));
        }
        // COB_CODE: IF DFL-CNDE-END2000-EFFLQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-CNDE-END2000-EFFLQ
        //           ELSE
        //              MOVE 0 TO IND-DFL-CNDE-END2000-EFFLQ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflCndeEnd2000Efflq().getDflCndeEnd2000EfflqNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-CNDE-END2000-EFFLQ
            ws.getIndDForzLiq().setCndeEnd2000Efflq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-CNDE-END2000-EFFLQ
            ws.getIndDForzLiq().setCndeEnd2000Efflq(((short)0));
        }
        // COB_CODE: IF DFL-CNDE-END2000-DFZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-CNDE-END2000-DFZ
        //           ELSE
        //              MOVE 0 TO IND-DFL-CNDE-END2000-DFZ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflCndeEnd2000Dfz().getDflCndeEnd2000DfzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-CNDE-END2000-DFZ
            ws.getIndDForzLiq().setCndeEnd2000Dfz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-CNDE-END2000-DFZ
            ws.getIndDForzLiq().setCndeEnd2000Dfz(((short)0));
        }
        // COB_CODE: IF DFL-CNDE-END2006-CALC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-CNDE-END2006-CALC
        //           ELSE
        //              MOVE 0 TO IND-DFL-CNDE-END2006-CALC
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflCndeEnd2006Calc().getDflCndeEnd2006CalcNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-CNDE-END2006-CALC
            ws.getIndDForzLiq().setCndeEnd2006Calc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-CNDE-END2006-CALC
            ws.getIndDForzLiq().setCndeEnd2006Calc(((short)0));
        }
        // COB_CODE: IF DFL-CNDE-END2006-EFFLQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-CNDE-END2006-EFFLQ
        //           ELSE
        //              MOVE 0 TO IND-DFL-CNDE-END2006-EFFLQ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflCndeEnd2006Efflq().getDflCndeEnd2006EfflqNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-CNDE-END2006-EFFLQ
            ws.getIndDForzLiq().setCndeEnd2006Efflq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-CNDE-END2006-EFFLQ
            ws.getIndDForzLiq().setCndeEnd2006Efflq(((short)0));
        }
        // COB_CODE: IF DFL-CNDE-END2006-DFZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-CNDE-END2006-DFZ
        //           ELSE
        //              MOVE 0 TO IND-DFL-CNDE-END2006-DFZ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflCndeEnd2006Dfz().getDflCndeEnd2006DfzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-CNDE-END2006-DFZ
            ws.getIndDForzLiq().setCndeEnd2006Dfz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-CNDE-END2006-DFZ
            ws.getIndDForzLiq().setCndeEnd2006Dfz(((short)0));
        }
        // COB_CODE: IF DFL-CNDE-DAL2007-CALC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-CNDE-DAL2007-CALC
        //           ELSE
        //              MOVE 0 TO IND-DFL-CNDE-DAL2007-CALC
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflCndeDal2007Calc().getDflCndeDal2007CalcNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-CNDE-DAL2007-CALC
            ws.getIndDForzLiq().setCndeDal2007Calc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-CNDE-DAL2007-CALC
            ws.getIndDForzLiq().setCndeDal2007Calc(((short)0));
        }
        // COB_CODE: IF DFL-CNDE-DAL2007-EFFLQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-CNDE-DAL2007-EFFLQ
        //           ELSE
        //              MOVE 0 TO IND-DFL-CNDE-DAL2007-EFFLQ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflCndeDal2007Efflq().getDflCndeDal2007EfflqNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-CNDE-DAL2007-EFFLQ
            ws.getIndDForzLiq().setCndeDal2007Efflq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-CNDE-DAL2007-EFFLQ
            ws.getIndDForzLiq().setCndeDal2007Efflq(((short)0));
        }
        // COB_CODE: IF DFL-CNDE-DAL2007-DFZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-CNDE-DAL2007-DFZ
        //           ELSE
        //              MOVE 0 TO IND-DFL-CNDE-DAL2007-DFZ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflCndeDal2007Dfz().getDflCndeDal2007DfzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-CNDE-DAL2007-DFZ
            ws.getIndDForzLiq().setCndeDal2007Dfz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-CNDE-DAL2007-DFZ
            ws.getIndDForzLiq().setCndeDal2007Dfz(((short)0));
        }
        // COB_CODE: IF DFL-AA-CNBZ-END2000-EF-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-AA-CNBZ-END2000-EF
        //           ELSE
        //              MOVE 0 TO IND-DFL-AA-CNBZ-END2000-EF
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflAaCnbzEnd2000Ef().getDflAaCnbzEnd2000EfNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-AA-CNBZ-END2000-EF
            ws.getIndDForzLiq().setAaCnbzEnd2000Ef(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-AA-CNBZ-END2000-EF
            ws.getIndDForzLiq().setAaCnbzEnd2000Ef(((short)0));
        }
        // COB_CODE: IF DFL-AA-CNBZ-END2006-EF-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-AA-CNBZ-END2006-EF
        //           ELSE
        //              MOVE 0 TO IND-DFL-AA-CNBZ-END2006-EF
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflAaCnbzEnd2006Ef().getDflAaCnbzEnd2006EfNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-AA-CNBZ-END2006-EF
            ws.getIndDForzLiq().setAaCnbzEnd2006Ef(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-AA-CNBZ-END2006-EF
            ws.getIndDForzLiq().setAaCnbzEnd2006Ef(((short)0));
        }
        // COB_CODE: IF DFL-AA-CNBZ-DAL2007-EF-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-AA-CNBZ-DAL2007-EF
        //           ELSE
        //              MOVE 0 TO IND-DFL-AA-CNBZ-DAL2007-EF
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflAaCnbzDal2007Ef().getDflAaCnbzDal2007EfNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-AA-CNBZ-DAL2007-EF
            ws.getIndDForzLiq().setAaCnbzDal2007Ef(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-AA-CNBZ-DAL2007-EF
            ws.getIndDForzLiq().setAaCnbzDal2007Ef(((short)0));
        }
        // COB_CODE: IF DFL-MM-CNBZ-END2000-EF-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-MM-CNBZ-END2000-EF
        //           ELSE
        //              MOVE 0 TO IND-DFL-MM-CNBZ-END2000-EF
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflMmCnbzEnd2000Ef().getDflMmCnbzEnd2000EfNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-MM-CNBZ-END2000-EF
            ws.getIndDForzLiq().setMmCnbzEnd2000Ef(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-MM-CNBZ-END2000-EF
            ws.getIndDForzLiq().setMmCnbzEnd2000Ef(((short)0));
        }
        // COB_CODE: IF DFL-MM-CNBZ-END2006-EF-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-MM-CNBZ-END2006-EF
        //           ELSE
        //              MOVE 0 TO IND-DFL-MM-CNBZ-END2006-EF
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflMmCnbzEnd2006Ef().getDflMmCnbzEnd2006EfNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-MM-CNBZ-END2006-EF
            ws.getIndDForzLiq().setMmCnbzEnd2006Ef(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-MM-CNBZ-END2006-EF
            ws.getIndDForzLiq().setMmCnbzEnd2006Ef(((short)0));
        }
        // COB_CODE: IF DFL-MM-CNBZ-DAL2007-EF-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-MM-CNBZ-DAL2007-EF
        //           ELSE
        //              MOVE 0 TO IND-DFL-MM-CNBZ-DAL2007-EF
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflMmCnbzDal2007Ef().getDflMmCnbzDal2007EfNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-MM-CNBZ-DAL2007-EF
            ws.getIndDForzLiq().setMmCnbzDal2007Ef(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-MM-CNBZ-DAL2007-EF
            ws.getIndDForzLiq().setMmCnbzDal2007Ef(((short)0));
        }
        // COB_CODE: IF DFL-IMPST-DA-RIMB-EFF-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMPST-DA-RIMB-EFF
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMPST-DA-RIMB-EFF
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpstDaRimbEff().getDflImpstDaRimbEffNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMPST-DA-RIMB-EFF
            ws.getIndDForzLiq().setImpstDaRimbEff(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMPST-DA-RIMB-EFF
            ws.getIndDForzLiq().setImpstDaRimbEff(((short)0));
        }
        // COB_CODE: IF DFL-ALQ-TAX-SEP-CALC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-ALQ-TAX-SEP-CALC
        //           ELSE
        //              MOVE 0 TO IND-DFL-ALQ-TAX-SEP-CALC
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflAlqTaxSepCalc().getDflAlqTaxSepCalcNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-ALQ-TAX-SEP-CALC
            ws.getIndDForzLiq().setAlqTaxSepCalc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-ALQ-TAX-SEP-CALC
            ws.getIndDForzLiq().setAlqTaxSepCalc(((short)0));
        }
        // COB_CODE: IF DFL-ALQ-TAX-SEP-EFFLQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-ALQ-TAX-SEP-EFFLQ
        //           ELSE
        //              MOVE 0 TO IND-DFL-ALQ-TAX-SEP-EFFLQ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflAlqTaxSepEfflq().getDflAlqTaxSepEfflqNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-ALQ-TAX-SEP-EFFLQ
            ws.getIndDForzLiq().setAlqTaxSepEfflq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-ALQ-TAX-SEP-EFFLQ
            ws.getIndDForzLiq().setAlqTaxSepEfflq(((short)0));
        }
        // COB_CODE: IF DFL-ALQ-TAX-SEP-DFZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-ALQ-TAX-SEP-DFZ
        //           ELSE
        //              MOVE 0 TO IND-DFL-ALQ-TAX-SEP-DFZ
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflAlqTaxSepDfz().getDflAlqTaxSepDfzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-ALQ-TAX-SEP-DFZ
            ws.getIndDForzLiq().setAlqTaxSepDfz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-ALQ-TAX-SEP-DFZ
            ws.getIndDForzLiq().setAlqTaxSepDfz(((short)0));
        }
        // COB_CODE: IF DFL-ALQ-CNBT-INPSTFM-C-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-ALQ-CNBT-INPSTFM-C
        //           ELSE
        //              MOVE 0 TO IND-DFL-ALQ-CNBT-INPSTFM-C
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflAlqCnbtInpstfmC().getDflAlqCnbtInpstfmCNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-ALQ-CNBT-INPSTFM-C
            ws.getIndDForzLiq().setAlqCnbtInpstfmC(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-ALQ-CNBT-INPSTFM-C
            ws.getIndDForzLiq().setAlqCnbtInpstfmC(((short)0));
        }
        // COB_CODE: IF DFL-ALQ-CNBT-INPSTFM-E-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-ALQ-CNBT-INPSTFM-E
        //           ELSE
        //              MOVE 0 TO IND-DFL-ALQ-CNBT-INPSTFM-E
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflAlqCnbtInpstfmE().getDflAlqCnbtInpstfmENullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-ALQ-CNBT-INPSTFM-E
            ws.getIndDForzLiq().setAlqCnbtInpstfmE(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-ALQ-CNBT-INPSTFM-E
            ws.getIndDForzLiq().setAlqCnbtInpstfmE(((short)0));
        }
        // COB_CODE: IF DFL-ALQ-CNBT-INPSTFM-D-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-ALQ-CNBT-INPSTFM-D
        //           ELSE
        //              MOVE 0 TO IND-DFL-ALQ-CNBT-INPSTFM-D
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflAlqCnbtInpstfmD().getDflAlqCnbtInpstfmDNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-ALQ-CNBT-INPSTFM-D
            ws.getIndDForzLiq().setAlqCnbtInpstfmD(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-ALQ-CNBT-INPSTFM-D
            ws.getIndDForzLiq().setAlqCnbtInpstfmD(((short)0));
        }
        // COB_CODE: IF DFL-IMPB-VIS-1382011C-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMPB-VIS-1382011C
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMPB-VIS-1382011C
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpbVis1382011c().getDflImpbVis1382011cNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMPB-VIS-1382011C
            ws.getIndDForzLiq().setImpbVis1382011c(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMPB-VIS-1382011C
            ws.getIndDForzLiq().setImpbVis1382011c(((short)0));
        }
        // COB_CODE: IF DFL-IMPB-VIS-1382011D-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMPB-VIS-1382011D
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMPB-VIS-1382011D
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpbVis1382011d().getDflImpbVis1382011dNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMPB-VIS-1382011D
            ws.getIndDForzLiq().setImpbVis1382011d(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMPB-VIS-1382011D
            ws.getIndDForzLiq().setImpbVis1382011d(((short)0));
        }
        // COB_CODE: IF DFL-IMPB-VIS-1382011L-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMPB-VIS-1382011L
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMPB-VIS-1382011L
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpbVis1382011l().getDflImpbVis1382011lNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMPB-VIS-1382011L
            ws.getIndDForzLiq().setImpbVis1382011l(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMPB-VIS-1382011L
            ws.getIndDForzLiq().setImpbVis1382011l(((short)0));
        }
        // COB_CODE: IF DFL-IMPST-VIS-1382011C-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMPST-VIS-1382011C
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMPST-VIS-1382011C
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpstVis1382011c().getDflImpstVis1382011cNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMPST-VIS-1382011C
            ws.getIndDForzLiq().setImpstVis1382011c(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMPST-VIS-1382011C
            ws.getIndDForzLiq().setImpstVis1382011c(((short)0));
        }
        // COB_CODE: IF DFL-IMPST-VIS-1382011D-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMPST-VIS-1382011D
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMPST-VIS-1382011D
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpstVis1382011d().getDflImpstVis1382011dNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMPST-VIS-1382011D
            ws.getIndDForzLiq().setImpstVis1382011d(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMPST-VIS-1382011D
            ws.getIndDForzLiq().setImpstVis1382011d(((short)0));
        }
        // COB_CODE: IF DFL-IMPST-VIS-1382011L-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMPST-VIS-1382011L
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMPST-VIS-1382011L
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpstVis1382011l().getDflImpstVis1382011lNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMPST-VIS-1382011L
            ws.getIndDForzLiq().setImpstVis1382011l(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMPST-VIS-1382011L
            ws.getIndDForzLiq().setImpstVis1382011l(((short)0));
        }
        // COB_CODE: IF DFL-IMPB-IS-1382011C-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMPB-IS-1382011C
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMPB-IS-1382011C
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpbIs1382011c().getDflImpbIs1382011cNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMPB-IS-1382011C
            ws.getIndDForzLiq().setImpbIs1382011c(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMPB-IS-1382011C
            ws.getIndDForzLiq().setImpbIs1382011c(((short)0));
        }
        // COB_CODE: IF DFL-IMPB-IS-1382011D-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMPB-IS-1382011D
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMPB-IS-1382011D
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpbIs1382011d().getDflImpbIs1382011dNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMPB-IS-1382011D
            ws.getIndDForzLiq().setImpbIs1382011d(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMPB-IS-1382011D
            ws.getIndDForzLiq().setImpbIs1382011d(((short)0));
        }
        // COB_CODE: IF DFL-IMPB-IS-1382011L-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMPB-IS-1382011L
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMPB-IS-1382011L
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpbIs1382011l().getDflImpbIs1382011lNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMPB-IS-1382011L
            ws.getIndDForzLiq().setImpbIs1382011l(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMPB-IS-1382011L
            ws.getIndDForzLiq().setImpbIs1382011l(((short)0));
        }
        // COB_CODE: IF DFL-IS-1382011C-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IS-1382011C
        //           ELSE
        //              MOVE 0 TO IND-DFL-IS-1382011C
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflIs1382011c().getDflIs1382011cNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IS-1382011C
            ws.getIndDForzLiq().setIs1382011c(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IS-1382011C
            ws.getIndDForzLiq().setIs1382011c(((short)0));
        }
        // COB_CODE: IF DFL-IS-1382011D-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IS-1382011D
        //           ELSE
        //              MOVE 0 TO IND-DFL-IS-1382011D
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflIs1382011d().getDflIs1382011dNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IS-1382011D
            ws.getIndDForzLiq().setIs1382011d(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IS-1382011D
            ws.getIndDForzLiq().setIs1382011d(((short)0));
        }
        // COB_CODE: IF DFL-IS-1382011L-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IS-1382011L
        //           ELSE
        //              MOVE 0 TO IND-DFL-IS-1382011L
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflIs1382011l().getDflIs1382011lNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IS-1382011L
            ws.getIndDForzLiq().setIs1382011l(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IS-1382011L
            ws.getIndDForzLiq().setIs1382011l(((short)0));
        }
        // COB_CODE: IF DFL-IMP-INTR-RIT-PAG-C-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMP-INTR-RIT-PAG-C
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMP-INTR-RIT-PAG-C
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpIntrRitPagC().getDflImpIntrRitPagCNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMP-INTR-RIT-PAG-C
            ws.getIndDForzLiq().setImpIntrRitPagC(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMP-INTR-RIT-PAG-C
            ws.getIndDForzLiq().setImpIntrRitPagC(((short)0));
        }
        // COB_CODE: IF DFL-IMP-INTR-RIT-PAG-D-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMP-INTR-RIT-PAG-D
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMP-INTR-RIT-PAG-D
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpIntrRitPagD().getDflImpIntrRitPagDNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMP-INTR-RIT-PAG-D
            ws.getIndDForzLiq().setImpIntrRitPagD(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMP-INTR-RIT-PAG-D
            ws.getIndDForzLiq().setImpIntrRitPagD(((short)0));
        }
        // COB_CODE: IF DFL-IMP-INTR-RIT-PAG-L-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMP-INTR-RIT-PAG-L
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMP-INTR-RIT-PAG-L
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpIntrRitPagL().getDflImpIntrRitPagLNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMP-INTR-RIT-PAG-L
            ws.getIndDForzLiq().setImpIntrRitPagL(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMP-INTR-RIT-PAG-L
            ws.getIndDForzLiq().setImpIntrRitPagL(((short)0));
        }
        // COB_CODE: IF DFL-IMPB-BOLLO-DETT-C-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMPB-BOLLO-DETT-C
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMPB-BOLLO-DETT-C
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpbBolloDettC().getDflImpbBolloDettCNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMPB-BOLLO-DETT-C
            ws.getIndDForzLiq().setImpbBolloDettC(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMPB-BOLLO-DETT-C
            ws.getIndDForzLiq().setImpbBolloDettC(((short)0));
        }
        // COB_CODE: IF DFL-IMPB-BOLLO-DETT-D-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMPB-BOLLO-DETT-D
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMPB-BOLLO-DETT-D
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpbBolloDettD().getDflImpbBolloDettDNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMPB-BOLLO-DETT-D
            ws.getIndDForzLiq().setImpbBolloDettD(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMPB-BOLLO-DETT-D
            ws.getIndDForzLiq().setImpbBolloDettD(((short)0));
        }
        // COB_CODE: IF DFL-IMPB-BOLLO-DETT-L-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMPB-BOLLO-DETT-L
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMPB-BOLLO-DETT-L
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpbBolloDettL().getDflImpbBolloDettLNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMPB-BOLLO-DETT-L
            ws.getIndDForzLiq().setImpbBolloDettL(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMPB-BOLLO-DETT-L
            ws.getIndDForzLiq().setImpbBolloDettL(((short)0));
        }
        // COB_CODE: IF DFL-IMPST-BOLLO-DETT-C-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMPST-BOLLO-DETT-C
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMPST-BOLLO-DETT-C
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpstBolloDettC().getDflImpstBolloDettCNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMPST-BOLLO-DETT-C
            ws.getIndDForzLiq().setImpstBolloDettC(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMPST-BOLLO-DETT-C
            ws.getIndDForzLiq().setImpstBolloDettC(((short)0));
        }
        // COB_CODE: IF DFL-IMPST-BOLLO-DETT-D-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMPST-BOLLO-DETT-D
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMPST-BOLLO-DETT-D
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpstBolloDettD().getDflImpstBolloDettDNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMPST-BOLLO-DETT-D
            ws.getIndDForzLiq().setImpstBolloDettD(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMPST-BOLLO-DETT-D
            ws.getIndDForzLiq().setImpstBolloDettD(((short)0));
        }
        // COB_CODE: IF DFL-IMPST-BOLLO-DETT-L-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMPST-BOLLO-DETT-L
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMPST-BOLLO-DETT-L
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpstBolloDettL().getDflImpstBolloDettLNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMPST-BOLLO-DETT-L
            ws.getIndDForzLiq().setImpstBolloDettL(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMPST-BOLLO-DETT-L
            ws.getIndDForzLiq().setImpstBolloDettL(((short)0));
        }
        // COB_CODE: IF DFL-IMPST-BOLLO-TOT-VC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMPST-BOLLO-TOT-VC
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMPST-BOLLO-TOT-VC
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpstBolloTotVc().getDflImpstBolloTotVcNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMPST-BOLLO-TOT-VC
            ws.getIndDForzLiq().setImpstBolloTotVc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMPST-BOLLO-TOT-VC
            ws.getIndDForzLiq().setImpstBolloTotVc(((short)0));
        }
        // COB_CODE: IF DFL-IMPST-BOLLO-TOT-VD-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMPST-BOLLO-TOT-VD
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMPST-BOLLO-TOT-VD
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpstBolloTotVd().getDflImpstBolloTotVdNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMPST-BOLLO-TOT-VD
            ws.getIndDForzLiq().setImpstBolloTotVd(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMPST-BOLLO-TOT-VD
            ws.getIndDForzLiq().setImpstBolloTotVd(((short)0));
        }
        // COB_CODE: IF DFL-IMPST-BOLLO-TOT-VL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMPST-BOLLO-TOT-VL
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMPST-BOLLO-TOT-VL
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpstBolloTotVl().getDflImpstBolloTotVlNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMPST-BOLLO-TOT-VL
            ws.getIndDForzLiq().setImpstBolloTotVl(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMPST-BOLLO-TOT-VL
            ws.getIndDForzLiq().setImpstBolloTotVl(((short)0));
        }
        // COB_CODE: IF DFL-IMPB-VIS-662014C-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMPB-VIS-662014C
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMPB-VIS-662014C
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpbVis662014c().getDflImpbVis662014cNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMPB-VIS-662014C
            ws.getIndDForzLiq().setImpbVis662014c(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMPB-VIS-662014C
            ws.getIndDForzLiq().setImpbVis662014c(((short)0));
        }
        // COB_CODE: IF DFL-IMPB-VIS-662014D-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMPB-VIS-662014D
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMPB-VIS-662014D
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpbVis662014d().getDflImpbVis662014dNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMPB-VIS-662014D
            ws.getIndDForzLiq().setImpbVis662014d(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMPB-VIS-662014D
            ws.getIndDForzLiq().setImpbVis662014d(((short)0));
        }
        // COB_CODE: IF DFL-IMPB-VIS-662014L-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMPB-VIS-662014L
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMPB-VIS-662014L
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpbVis662014l().getDflImpbVis662014lNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMPB-VIS-662014L
            ws.getIndDForzLiq().setImpbVis662014l(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMPB-VIS-662014L
            ws.getIndDForzLiq().setImpbVis662014l(((short)0));
        }
        // COB_CODE: IF DFL-IMPST-VIS-662014C-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMPST-VIS-662014C
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMPST-VIS-662014C
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpstVis662014c().getDflImpstVis662014cNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMPST-VIS-662014C
            ws.getIndDForzLiq().setImpstVis662014c(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMPST-VIS-662014C
            ws.getIndDForzLiq().setImpstVis662014c(((short)0));
        }
        // COB_CODE: IF DFL-IMPST-VIS-662014D-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMPST-VIS-662014D
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMPST-VIS-662014D
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpstVis662014d().getDflImpstVis662014dNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMPST-VIS-662014D
            ws.getIndDForzLiq().setImpstVis662014d(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMPST-VIS-662014D
            ws.getIndDForzLiq().setImpstVis662014d(((short)0));
        }
        // COB_CODE: IF DFL-IMPST-VIS-662014L-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMPST-VIS-662014L
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMPST-VIS-662014L
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpstVis662014l().getDflImpstVis662014lNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMPST-VIS-662014L
            ws.getIndDForzLiq().setImpstVis662014l(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMPST-VIS-662014L
            ws.getIndDForzLiq().setImpstVis662014l(((short)0));
        }
        // COB_CODE: IF DFL-IMPB-IS-662014C-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMPB-IS-662014C
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMPB-IS-662014C
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpbIs662014c().getDflImpbIs662014cNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMPB-IS-662014C
            ws.getIndDForzLiq().setImpbIs662014c(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMPB-IS-662014C
            ws.getIndDForzLiq().setImpbIs662014c(((short)0));
        }
        // COB_CODE: IF DFL-IMPB-IS-662014D-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMPB-IS-662014D
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMPB-IS-662014D
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpbIs662014d().getDflImpbIs662014dNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMPB-IS-662014D
            ws.getIndDForzLiq().setImpbIs662014d(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMPB-IS-662014D
            ws.getIndDForzLiq().setImpbIs662014d(((short)0));
        }
        // COB_CODE: IF DFL-IMPB-IS-662014L-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IMPB-IS-662014L
        //           ELSE
        //              MOVE 0 TO IND-DFL-IMPB-IS-662014L
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflImpbIs662014l().getDflImpbIs662014lNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IMPB-IS-662014L
            ws.getIndDForzLiq().setImpbIs662014l(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IMPB-IS-662014L
            ws.getIndDForzLiq().setImpbIs662014l(((short)0));
        }
        // COB_CODE: IF DFL-IS-662014C-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IS-662014C
        //           ELSE
        //              MOVE 0 TO IND-DFL-IS-662014C
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflIs662014c().getDflIs662014cNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IS-662014C
            ws.getIndDForzLiq().setIs662014c(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IS-662014C
            ws.getIndDForzLiq().setIs662014c(((short)0));
        }
        // COB_CODE: IF DFL-IS-662014D-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IS-662014D
        //           ELSE
        //              MOVE 0 TO IND-DFL-IS-662014D
        //           END-IF
        if (Characters.EQ_HIGH.test(dForzLiq.getDflIs662014d().getDflIs662014dNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IS-662014D
            ws.getIndDForzLiq().setIs662014d(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IS-662014D
            ws.getIndDForzLiq().setIs662014d(((short)0));
        }
        // COB_CODE: IF DFL-IS-662014L-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFL-IS-662014L
        //           ELSE
        //              MOVE 0 TO IND-DFL-IS-662014L
        //           END-IF.
        if (Characters.EQ_HIGH.test(dForzLiq.getDflIs662014l().getDflIs662014lNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFL-IS-662014L
            ws.getIndDForzLiq().setIs662014l(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFL-IS-662014L
            ws.getIndDForzLiq().setIs662014l(((short)0));
        }
    }

    /**Original name: Z400-SEQ-RIGA<br>*/
    private void z400SeqRiga() {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_RIGA
        //              INTO : DFL-DS-RIGA
        //           END-EXEC.
        //TODO: Implement: valuesStmt;
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: Z500-AGGIORNAMENTO-STORICO<br>*/
    private void z500AggiornamentoStorico() {
        // COB_CODE: MOVE D-FORZ-LIQ TO WS-BUFFER-TABLE.
        ws.setWsBufferTable(dForzLiq.getdForzLiqFormatted());
        // COB_CODE: MOVE DFL-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.
        ws.setWsIdMoviCrz(TruncAbs.toInt(dForzLiq.getDflIdMoviCrz(), 9));
        // COB_CODE: PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.
        a360OpenCursorIdEff();
        // COB_CODE: PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-SQL
        //              END-IF
        //           END-PERFORM.
        while (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX
            a390FetchNextIdEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              END-IF
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: MOVE WS-ID-MOVI-CRZ   TO DFL-ID-MOVI-CHIU
                dForzLiq.getDflIdMoviChiu().setDflIdMoviChiu(ws.getWsIdMoviCrz());
                // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
                //                                 TO DFL-DS-TS-END-CPTZ
                dForzLiq.setDflDsTsEndCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
                // COB_CODE: PERFORM A330-UPDATE-ID-EFF THRU A330-EX
                a330UpdateIdEff();
                // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
                //                 END-IF
                //           END-IF
                if (idsv0003.getSqlcode().isSuccessfulSql()) {
                    // COB_CODE: MOVE WS-ID-MOVI-CRZ TO DFL-ID-MOVI-CRZ
                    dForzLiq.setDflIdMoviCrz(ws.getWsIdMoviCrz());
                    // COB_CODE: MOVE HIGH-VALUES    TO DFL-ID-MOVI-CHIU-NULL
                    dForzLiq.getDflIdMoviChiu().setDflIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflIdMoviChiu.Len.DFL_ID_MOVI_CHIU_NULL));
                    // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
                    //                               TO DFL-DT-END-EFF
                    dForzLiq.setDflDtEndEff(idsv0003.getDataInizioEffetto());
                    // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
                    //                               TO DFL-DS-TS-INI-CPTZ
                    dForzLiq.setDflDsTsIniCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
                    // COB_CODE: MOVE WS-TS-INFINITO
                    //                               TO DFL-DS-TS-END-CPTZ
                    dForzLiq.setDflDsTsEndCptz(ws.getIdsv0010().getWsTsInfinito());
                    // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
                    //              PERFORM A220-INSERT-PK THRU A220-EX
                    //           END-IF
                    if (idsv0003.getSqlcode().isSuccessfulSql()) {
                        // COB_CODE: PERFORM A220-INSERT-PK THRU A220-EX
                        a220InsertPk();
                    }
                }
            }
        }
        // COB_CODE: IF IDSV0003-NOT-FOUND
        //              END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF NOT IDSV0003-DELETE-LOGICA
            //              PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX
            //           ELSE
            //              SET IDSV0003-SUCCESSFUL-SQL TO TRUE
            //           END-IF
            if (!idsv0003.getOperazione().isDeleteLogica()) {
                // COB_CODE: PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX
                z600InsertNuovaRigaStorica();
            }
            else {
                // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL TO TRUE
                idsv0003.getSqlcode().setSuccessfulSql();
            }
        }
    }

    /**Original name: Z550-AGG-STORICO-SOLO-INS<br>*/
    private void z550AggStoricoSoloIns() {
        // COB_CODE: MOVE D-FORZ-LIQ TO WS-BUFFER-TABLE.
        ws.setWsBufferTable(dForzLiq.getdForzLiqFormatted());
        // COB_CODE: MOVE DFL-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.
        ws.setWsIdMoviCrz(TruncAbs.toInt(dForzLiq.getDflIdMoviCrz(), 9));
        // COB_CODE: PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX.
        z600InsertNuovaRigaStorica();
    }

    /**Original name: Z600-INSERT-NUOVA-RIGA-STORICA<br>*/
    private void z600InsertNuovaRigaStorica() {
        // COB_CODE: MOVE WS-BUFFER-TABLE TO D-FORZ-LIQ.
        dForzLiq.setdForzLiqFormatted(ws.getWsBufferTableFormatted());
        // COB_CODE: MOVE WS-ID-MOVI-CRZ  TO DFL-ID-MOVI-CRZ.
        dForzLiq.setDflIdMoviCrz(ws.getWsIdMoviCrz());
        // COB_CODE: MOVE HIGH-VALUES     TO DFL-ID-MOVI-CHIU-NULL.
        dForzLiq.getDflIdMoviChiu().setDflIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DflIdMoviChiu.Len.DFL_ID_MOVI_CHIU_NULL));
        // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
        //                                TO DFL-DT-INI-EFF.
        dForzLiq.setDflDtIniEff(idsv0003.getDataInizioEffetto());
        // COB_CODE: MOVE WS-DT-INFINITO
        //                                TO DFL-DT-END-EFF.
        dForzLiq.setDflDtEndEff(ws.getIdsv0010().getWsDtInfinito());
        // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
        //                                TO DFL-DS-TS-INI-CPTZ.
        dForzLiq.setDflDsTsIniCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
        // COB_CODE: MOVE WS-TS-INFINITO
        //                                TO DFL-DS-TS-END-CPTZ.
        dForzLiq.setDflDsTsEndCptz(ws.getIdsv0010().getWsTsInfinito());
        // COB_CODE: MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
        //                                TO DFL-COD-COMP-ANIA.
        dForzLiq.setDflCodCompAnia(idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A220-INSERT-PK THRU A220-EX.
        a220InsertPk();
    }

    /**Original name: Z900-CONVERTI-N-TO-X<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da 9(8) comp-3 a date
	 * ----</pre>*/
    private void z900ConvertiNToX() {
        // COB_CODE: MOVE DFL-DT-INI-EFF TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(dForzLiq.getDflDtIniEff(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO DFL-DT-INI-EFF-DB
        ws.getIdbvdfl3().setFlDtIniEffDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: MOVE DFL-DT-END-EFF TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(dForzLiq.getDflDtEndEff(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO DFL-DT-END-EFF-DB.
        ws.getIdbvdfl3().setFlDtEndEffDb(ws.getIdsv0010().getWsDateX());
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE DFL-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getIdbvdfl3().getFlDtIniEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO DFL-DT-INI-EFF
        dForzLiq.setDflDtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE DFL-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getIdbvdfl3().getFlDtEndEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO DFL-DT-END-EFF.
        dForzLiq.setDflDtEndEff(ws.getIdsv0010().getWsDateN());
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public int getAaCnbzDal2007Ef() {
        return dForzLiq.getDflAaCnbzDal2007Ef().getDflAaCnbzDal2007Ef();
    }

    @Override
    public void setAaCnbzDal2007Ef(int aaCnbzDal2007Ef) {
        this.dForzLiq.getDflAaCnbzDal2007Ef().setDflAaCnbzDal2007Ef(aaCnbzDal2007Ef);
    }

    @Override
    public Integer getAaCnbzDal2007EfObj() {
        if (ws.getIndDForzLiq().getAaCnbzDal2007Ef() >= 0) {
            return ((Integer)getAaCnbzDal2007Ef());
        }
        else {
            return null;
        }
    }

    @Override
    public void setAaCnbzDal2007EfObj(Integer aaCnbzDal2007EfObj) {
        if (aaCnbzDal2007EfObj != null) {
            setAaCnbzDal2007Ef(((int)aaCnbzDal2007EfObj));
            ws.getIndDForzLiq().setAaCnbzDal2007Ef(((short)0));
        }
        else {
            ws.getIndDForzLiq().setAaCnbzDal2007Ef(((short)-1));
        }
    }

    @Override
    public int getAaCnbzEnd2000Ef() {
        return dForzLiq.getDflAaCnbzEnd2000Ef().getDflAaCnbzEnd2000Ef();
    }

    @Override
    public void setAaCnbzEnd2000Ef(int aaCnbzEnd2000Ef) {
        this.dForzLiq.getDflAaCnbzEnd2000Ef().setDflAaCnbzEnd2000Ef(aaCnbzEnd2000Ef);
    }

    @Override
    public Integer getAaCnbzEnd2000EfObj() {
        if (ws.getIndDForzLiq().getAaCnbzEnd2000Ef() >= 0) {
            return ((Integer)getAaCnbzEnd2000Ef());
        }
        else {
            return null;
        }
    }

    @Override
    public void setAaCnbzEnd2000EfObj(Integer aaCnbzEnd2000EfObj) {
        if (aaCnbzEnd2000EfObj != null) {
            setAaCnbzEnd2000Ef(((int)aaCnbzEnd2000EfObj));
            ws.getIndDForzLiq().setAaCnbzEnd2000Ef(((short)0));
        }
        else {
            ws.getIndDForzLiq().setAaCnbzEnd2000Ef(((short)-1));
        }
    }

    @Override
    public int getAaCnbzEnd2006Ef() {
        return dForzLiq.getDflAaCnbzEnd2006Ef().getDflAaCnbzEnd2006Ef();
    }

    @Override
    public void setAaCnbzEnd2006Ef(int aaCnbzEnd2006Ef) {
        this.dForzLiq.getDflAaCnbzEnd2006Ef().setDflAaCnbzEnd2006Ef(aaCnbzEnd2006Ef);
    }

    @Override
    public Integer getAaCnbzEnd2006EfObj() {
        if (ws.getIndDForzLiq().getAaCnbzEnd2006Ef() >= 0) {
            return ((Integer)getAaCnbzEnd2006Ef());
        }
        else {
            return null;
        }
    }

    @Override
    public void setAaCnbzEnd2006EfObj(Integer aaCnbzEnd2006EfObj) {
        if (aaCnbzEnd2006EfObj != null) {
            setAaCnbzEnd2006Ef(((int)aaCnbzEnd2006EfObj));
            ws.getIndDForzLiq().setAaCnbzEnd2006Ef(((short)0));
        }
        else {
            ws.getIndDForzLiq().setAaCnbzEnd2006Ef(((short)-1));
        }
    }

    @Override
    public AfDecimal getAccpreAccCalc() {
        return dForzLiq.getDflAccpreAccCalc().getDflAccpreAccCalc();
    }

    @Override
    public void setAccpreAccCalc(AfDecimal accpreAccCalc) {
        this.dForzLiq.getDflAccpreAccCalc().setDflAccpreAccCalc(accpreAccCalc.copy());
    }

    @Override
    public AfDecimal getAccpreAccCalcObj() {
        if (ws.getIndDForzLiq().getAccpreAccCalc() >= 0) {
            return getAccpreAccCalc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAccpreAccCalcObj(AfDecimal accpreAccCalcObj) {
        if (accpreAccCalcObj != null) {
            setAccpreAccCalc(new AfDecimal(accpreAccCalcObj, 15, 3));
            ws.getIndDForzLiq().setAccpreAccCalc(((short)0));
        }
        else {
            ws.getIndDForzLiq().setAccpreAccCalc(((short)-1));
        }
    }

    @Override
    public AfDecimal getAccpreAccDfz() {
        return dForzLiq.getDflAccpreAccDfz().getDflAccpreAccDfz();
    }

    @Override
    public void setAccpreAccDfz(AfDecimal accpreAccDfz) {
        this.dForzLiq.getDflAccpreAccDfz().setDflAccpreAccDfz(accpreAccDfz.copy());
    }

    @Override
    public AfDecimal getAccpreAccDfzObj() {
        if (ws.getIndDForzLiq().getAccpreAccDfz() >= 0) {
            return getAccpreAccDfz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAccpreAccDfzObj(AfDecimal accpreAccDfzObj) {
        if (accpreAccDfzObj != null) {
            setAccpreAccDfz(new AfDecimal(accpreAccDfzObj, 15, 3));
            ws.getIndDForzLiq().setAccpreAccDfz(((short)0));
        }
        else {
            ws.getIndDForzLiq().setAccpreAccDfz(((short)-1));
        }
    }

    @Override
    public AfDecimal getAccpreAccEfflq() {
        return dForzLiq.getDflAccpreAccEfflq().getDflAccpreAccEfflq();
    }

    @Override
    public void setAccpreAccEfflq(AfDecimal accpreAccEfflq) {
        this.dForzLiq.getDflAccpreAccEfflq().setDflAccpreAccEfflq(accpreAccEfflq.copy());
    }

    @Override
    public AfDecimal getAccpreAccEfflqObj() {
        if (ws.getIndDForzLiq().getAccpreAccEfflq() >= 0) {
            return getAccpreAccEfflq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAccpreAccEfflqObj(AfDecimal accpreAccEfflqObj) {
        if (accpreAccEfflqObj != null) {
            setAccpreAccEfflq(new AfDecimal(accpreAccEfflqObj, 15, 3));
            ws.getIndDForzLiq().setAccpreAccEfflq(((short)0));
        }
        else {
            ws.getIndDForzLiq().setAccpreAccEfflq(((short)-1));
        }
    }

    @Override
    public AfDecimal getAccpreSostCalc() {
        return dForzLiq.getDflAccpreSostCalc().getDflAccpreSostCalc();
    }

    @Override
    public void setAccpreSostCalc(AfDecimal accpreSostCalc) {
        this.dForzLiq.getDflAccpreSostCalc().setDflAccpreSostCalc(accpreSostCalc.copy());
    }

    @Override
    public AfDecimal getAccpreSostCalcObj() {
        if (ws.getIndDForzLiq().getAccpreSostCalc() >= 0) {
            return getAccpreSostCalc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAccpreSostCalcObj(AfDecimal accpreSostCalcObj) {
        if (accpreSostCalcObj != null) {
            setAccpreSostCalc(new AfDecimal(accpreSostCalcObj, 15, 3));
            ws.getIndDForzLiq().setAccpreSostCalc(((short)0));
        }
        else {
            ws.getIndDForzLiq().setAccpreSostCalc(((short)-1));
        }
    }

    @Override
    public AfDecimal getAccpreSostDfz() {
        return dForzLiq.getDflAccpreSostDfz().getDflAccpreSostDfz();
    }

    @Override
    public void setAccpreSostDfz(AfDecimal accpreSostDfz) {
        this.dForzLiq.getDflAccpreSostDfz().setDflAccpreSostDfz(accpreSostDfz.copy());
    }

    @Override
    public AfDecimal getAccpreSostDfzObj() {
        if (ws.getIndDForzLiq().getAccpreSostDfz() >= 0) {
            return getAccpreSostDfz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAccpreSostDfzObj(AfDecimal accpreSostDfzObj) {
        if (accpreSostDfzObj != null) {
            setAccpreSostDfz(new AfDecimal(accpreSostDfzObj, 15, 3));
            ws.getIndDForzLiq().setAccpreSostDfz(((short)0));
        }
        else {
            ws.getIndDForzLiq().setAccpreSostDfz(((short)-1));
        }
    }

    @Override
    public AfDecimal getAccpreSostEfflq() {
        return dForzLiq.getDflAccpreSostEfflq().getDflAccpreSostEfflq();
    }

    @Override
    public void setAccpreSostEfflq(AfDecimal accpreSostEfflq) {
        this.dForzLiq.getDflAccpreSostEfflq().setDflAccpreSostEfflq(accpreSostEfflq.copy());
    }

    @Override
    public AfDecimal getAccpreSostEfflqObj() {
        if (ws.getIndDForzLiq().getAccpreSostEfflq() >= 0) {
            return getAccpreSostEfflq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAccpreSostEfflqObj(AfDecimal accpreSostEfflqObj) {
        if (accpreSostEfflqObj != null) {
            setAccpreSostEfflq(new AfDecimal(accpreSostEfflqObj, 15, 3));
            ws.getIndDForzLiq().setAccpreSostEfflq(((short)0));
        }
        else {
            ws.getIndDForzLiq().setAccpreSostEfflq(((short)-1));
        }
    }

    @Override
    public AfDecimal getAccpreVisCalc() {
        return dForzLiq.getDflAccpreVisCalc().getDflAccpreVisCalc();
    }

    @Override
    public void setAccpreVisCalc(AfDecimal accpreVisCalc) {
        this.dForzLiq.getDflAccpreVisCalc().setDflAccpreVisCalc(accpreVisCalc.copy());
    }

    @Override
    public AfDecimal getAccpreVisCalcObj() {
        if (ws.getIndDForzLiq().getAccpreVisCalc() >= 0) {
            return getAccpreVisCalc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAccpreVisCalcObj(AfDecimal accpreVisCalcObj) {
        if (accpreVisCalcObj != null) {
            setAccpreVisCalc(new AfDecimal(accpreVisCalcObj, 15, 3));
            ws.getIndDForzLiq().setAccpreVisCalc(((short)0));
        }
        else {
            ws.getIndDForzLiq().setAccpreVisCalc(((short)-1));
        }
    }

    @Override
    public AfDecimal getAccpreVisDfz() {
        return dForzLiq.getDflAccpreVisDfz().getDflAccpreVisDfz();
    }

    @Override
    public void setAccpreVisDfz(AfDecimal accpreVisDfz) {
        this.dForzLiq.getDflAccpreVisDfz().setDflAccpreVisDfz(accpreVisDfz.copy());
    }

    @Override
    public AfDecimal getAccpreVisDfzObj() {
        if (ws.getIndDForzLiq().getAccpreVisDfz() >= 0) {
            return getAccpreVisDfz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAccpreVisDfzObj(AfDecimal accpreVisDfzObj) {
        if (accpreVisDfzObj != null) {
            setAccpreVisDfz(new AfDecimal(accpreVisDfzObj, 15, 3));
            ws.getIndDForzLiq().setAccpreVisDfz(((short)0));
        }
        else {
            ws.getIndDForzLiq().setAccpreVisDfz(((short)-1));
        }
    }

    @Override
    public AfDecimal getAccpreVisEfflq() {
        return dForzLiq.getDflAccpreVisEfflq().getDflAccpreVisEfflq();
    }

    @Override
    public void setAccpreVisEfflq(AfDecimal accpreVisEfflq) {
        this.dForzLiq.getDflAccpreVisEfflq().setDflAccpreVisEfflq(accpreVisEfflq.copy());
    }

    @Override
    public AfDecimal getAccpreVisEfflqObj() {
        if (ws.getIndDForzLiq().getAccpreVisEfflq() >= 0) {
            return getAccpreVisEfflq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAccpreVisEfflqObj(AfDecimal accpreVisEfflqObj) {
        if (accpreVisEfflqObj != null) {
            setAccpreVisEfflq(new AfDecimal(accpreVisEfflqObj, 15, 3));
            ws.getIndDForzLiq().setAccpreVisEfflq(((short)0));
        }
        else {
            ws.getIndDForzLiq().setAccpreVisEfflq(((short)-1));
        }
    }

    @Override
    public AfDecimal getAlqCnbtInpstfmC() {
        return dForzLiq.getDflAlqCnbtInpstfmC().getDflAlqCnbtInpstfmC();
    }

    @Override
    public void setAlqCnbtInpstfmC(AfDecimal alqCnbtInpstfmC) {
        this.dForzLiq.getDflAlqCnbtInpstfmC().setDflAlqCnbtInpstfmC(alqCnbtInpstfmC.copy());
    }

    @Override
    public AfDecimal getAlqCnbtInpstfmCObj() {
        if (ws.getIndDForzLiq().getAlqCnbtInpstfmC() >= 0) {
            return getAlqCnbtInpstfmC();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAlqCnbtInpstfmCObj(AfDecimal alqCnbtInpstfmCObj) {
        if (alqCnbtInpstfmCObj != null) {
            setAlqCnbtInpstfmC(new AfDecimal(alqCnbtInpstfmCObj, 6, 3));
            ws.getIndDForzLiq().setAlqCnbtInpstfmC(((short)0));
        }
        else {
            ws.getIndDForzLiq().setAlqCnbtInpstfmC(((short)-1));
        }
    }

    @Override
    public AfDecimal getAlqCnbtInpstfmD() {
        return dForzLiq.getDflAlqCnbtInpstfmD().getDflAlqCnbtInpstfmD();
    }

    @Override
    public void setAlqCnbtInpstfmD(AfDecimal alqCnbtInpstfmD) {
        this.dForzLiq.getDflAlqCnbtInpstfmD().setDflAlqCnbtInpstfmD(alqCnbtInpstfmD.copy());
    }

    @Override
    public AfDecimal getAlqCnbtInpstfmDObj() {
        if (ws.getIndDForzLiq().getAlqCnbtInpstfmD() >= 0) {
            return getAlqCnbtInpstfmD();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAlqCnbtInpstfmDObj(AfDecimal alqCnbtInpstfmDObj) {
        if (alqCnbtInpstfmDObj != null) {
            setAlqCnbtInpstfmD(new AfDecimal(alqCnbtInpstfmDObj, 6, 3));
            ws.getIndDForzLiq().setAlqCnbtInpstfmD(((short)0));
        }
        else {
            ws.getIndDForzLiq().setAlqCnbtInpstfmD(((short)-1));
        }
    }

    @Override
    public AfDecimal getAlqCnbtInpstfmE() {
        return dForzLiq.getDflAlqCnbtInpstfmE().getDflAlqCnbtInpstfmE();
    }

    @Override
    public void setAlqCnbtInpstfmE(AfDecimal alqCnbtInpstfmE) {
        this.dForzLiq.getDflAlqCnbtInpstfmE().setDflAlqCnbtInpstfmE(alqCnbtInpstfmE.copy());
    }

    @Override
    public AfDecimal getAlqCnbtInpstfmEObj() {
        if (ws.getIndDForzLiq().getAlqCnbtInpstfmE() >= 0) {
            return getAlqCnbtInpstfmE();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAlqCnbtInpstfmEObj(AfDecimal alqCnbtInpstfmEObj) {
        if (alqCnbtInpstfmEObj != null) {
            setAlqCnbtInpstfmE(new AfDecimal(alqCnbtInpstfmEObj, 6, 3));
            ws.getIndDForzLiq().setAlqCnbtInpstfmE(((short)0));
        }
        else {
            ws.getIndDForzLiq().setAlqCnbtInpstfmE(((short)-1));
        }
    }

    @Override
    public AfDecimal getAlqTaxSepCalc() {
        return dForzLiq.getDflAlqTaxSepCalc().getDflAlqTaxSepCalc();
    }

    @Override
    public void setAlqTaxSepCalc(AfDecimal alqTaxSepCalc) {
        this.dForzLiq.getDflAlqTaxSepCalc().setDflAlqTaxSepCalc(alqTaxSepCalc.copy());
    }

    @Override
    public AfDecimal getAlqTaxSepCalcObj() {
        if (ws.getIndDForzLiq().getAlqTaxSepCalc() >= 0) {
            return getAlqTaxSepCalc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAlqTaxSepCalcObj(AfDecimal alqTaxSepCalcObj) {
        if (alqTaxSepCalcObj != null) {
            setAlqTaxSepCalc(new AfDecimal(alqTaxSepCalcObj, 6, 3));
            ws.getIndDForzLiq().setAlqTaxSepCalc(((short)0));
        }
        else {
            ws.getIndDForzLiq().setAlqTaxSepCalc(((short)-1));
        }
    }

    @Override
    public AfDecimal getAlqTaxSepDfz() {
        return dForzLiq.getDflAlqTaxSepDfz().getDflAlqTaxSepDfz();
    }

    @Override
    public void setAlqTaxSepDfz(AfDecimal alqTaxSepDfz) {
        this.dForzLiq.getDflAlqTaxSepDfz().setDflAlqTaxSepDfz(alqTaxSepDfz.copy());
    }

    @Override
    public AfDecimal getAlqTaxSepDfzObj() {
        if (ws.getIndDForzLiq().getAlqTaxSepDfz() >= 0) {
            return getAlqTaxSepDfz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAlqTaxSepDfzObj(AfDecimal alqTaxSepDfzObj) {
        if (alqTaxSepDfzObj != null) {
            setAlqTaxSepDfz(new AfDecimal(alqTaxSepDfzObj, 6, 3));
            ws.getIndDForzLiq().setAlqTaxSepDfz(((short)0));
        }
        else {
            ws.getIndDForzLiq().setAlqTaxSepDfz(((short)-1));
        }
    }

    @Override
    public AfDecimal getAlqTaxSepEfflq() {
        return dForzLiq.getDflAlqTaxSepEfflq().getDflAlqTaxSepEfflq();
    }

    @Override
    public void setAlqTaxSepEfflq(AfDecimal alqTaxSepEfflq) {
        this.dForzLiq.getDflAlqTaxSepEfflq().setDflAlqTaxSepEfflq(alqTaxSepEfflq.copy());
    }

    @Override
    public AfDecimal getAlqTaxSepEfflqObj() {
        if (ws.getIndDForzLiq().getAlqTaxSepEfflq() >= 0) {
            return getAlqTaxSepEfflq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAlqTaxSepEfflqObj(AfDecimal alqTaxSepEfflqObj) {
        if (alqTaxSepEfflqObj != null) {
            setAlqTaxSepEfflq(new AfDecimal(alqTaxSepEfflqObj, 6, 3));
            ws.getIndDForzLiq().setAlqTaxSepEfflq(((short)0));
        }
        else {
            ws.getIndDForzLiq().setAlqTaxSepEfflq(((short)-1));
        }
    }

    @Override
    public AfDecimal getCnbtInpstfmCalc() {
        return dForzLiq.getDflCnbtInpstfmCalc().getDflCnbtInpstfmCalc();
    }

    @Override
    public void setCnbtInpstfmCalc(AfDecimal cnbtInpstfmCalc) {
        this.dForzLiq.getDflCnbtInpstfmCalc().setDflCnbtInpstfmCalc(cnbtInpstfmCalc.copy());
    }

    @Override
    public AfDecimal getCnbtInpstfmCalcObj() {
        if (ws.getIndDForzLiq().getCnbtInpstfmCalc() >= 0) {
            return getCnbtInpstfmCalc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCnbtInpstfmCalcObj(AfDecimal cnbtInpstfmCalcObj) {
        if (cnbtInpstfmCalcObj != null) {
            setCnbtInpstfmCalc(new AfDecimal(cnbtInpstfmCalcObj, 15, 3));
            ws.getIndDForzLiq().setCnbtInpstfmCalc(((short)0));
        }
        else {
            ws.getIndDForzLiq().setCnbtInpstfmCalc(((short)-1));
        }
    }

    @Override
    public AfDecimal getCnbtInpstfmDfz() {
        return dForzLiq.getDflCnbtInpstfmDfz().getDflCnbtInpstfmDfz();
    }

    @Override
    public void setCnbtInpstfmDfz(AfDecimal cnbtInpstfmDfz) {
        this.dForzLiq.getDflCnbtInpstfmDfz().setDflCnbtInpstfmDfz(cnbtInpstfmDfz.copy());
    }

    @Override
    public AfDecimal getCnbtInpstfmDfzObj() {
        if (ws.getIndDForzLiq().getCnbtInpstfmDfz() >= 0) {
            return getCnbtInpstfmDfz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCnbtInpstfmDfzObj(AfDecimal cnbtInpstfmDfzObj) {
        if (cnbtInpstfmDfzObj != null) {
            setCnbtInpstfmDfz(new AfDecimal(cnbtInpstfmDfzObj, 15, 3));
            ws.getIndDForzLiq().setCnbtInpstfmDfz(((short)0));
        }
        else {
            ws.getIndDForzLiq().setCnbtInpstfmDfz(((short)-1));
        }
    }

    @Override
    public AfDecimal getCnbtInpstfmEfflq() {
        return dForzLiq.getDflCnbtInpstfmEfflq().getDflCnbtInpstfmEfflq();
    }

    @Override
    public void setCnbtInpstfmEfflq(AfDecimal cnbtInpstfmEfflq) {
        this.dForzLiq.getDflCnbtInpstfmEfflq().setDflCnbtInpstfmEfflq(cnbtInpstfmEfflq.copy());
    }

    @Override
    public AfDecimal getCnbtInpstfmEfflqObj() {
        if (ws.getIndDForzLiq().getCnbtInpstfmEfflq() >= 0) {
            return getCnbtInpstfmEfflq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCnbtInpstfmEfflqObj(AfDecimal cnbtInpstfmEfflqObj) {
        if (cnbtInpstfmEfflqObj != null) {
            setCnbtInpstfmEfflq(new AfDecimal(cnbtInpstfmEfflqObj, 15, 3));
            ws.getIndDForzLiq().setCnbtInpstfmEfflq(((short)0));
        }
        else {
            ws.getIndDForzLiq().setCnbtInpstfmEfflq(((short)-1));
        }
    }

    @Override
    public AfDecimal getCndeDal2007Calc() {
        return dForzLiq.getDflCndeDal2007Calc().getDflCndeDal2007Calc();
    }

    @Override
    public void setCndeDal2007Calc(AfDecimal cndeDal2007Calc) {
        this.dForzLiq.getDflCndeDal2007Calc().setDflCndeDal2007Calc(cndeDal2007Calc.copy());
    }

    @Override
    public AfDecimal getCndeDal2007CalcObj() {
        if (ws.getIndDForzLiq().getCndeDal2007Calc() >= 0) {
            return getCndeDal2007Calc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCndeDal2007CalcObj(AfDecimal cndeDal2007CalcObj) {
        if (cndeDal2007CalcObj != null) {
            setCndeDal2007Calc(new AfDecimal(cndeDal2007CalcObj, 15, 3));
            ws.getIndDForzLiq().setCndeDal2007Calc(((short)0));
        }
        else {
            ws.getIndDForzLiq().setCndeDal2007Calc(((short)-1));
        }
    }

    @Override
    public AfDecimal getCndeDal2007Dfz() {
        return dForzLiq.getDflCndeDal2007Dfz().getDflCndeDal2007Dfz();
    }

    @Override
    public void setCndeDal2007Dfz(AfDecimal cndeDal2007Dfz) {
        this.dForzLiq.getDflCndeDal2007Dfz().setDflCndeDal2007Dfz(cndeDal2007Dfz.copy());
    }

    @Override
    public AfDecimal getCndeDal2007DfzObj() {
        if (ws.getIndDForzLiq().getCndeDal2007Dfz() >= 0) {
            return getCndeDal2007Dfz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCndeDal2007DfzObj(AfDecimal cndeDal2007DfzObj) {
        if (cndeDal2007DfzObj != null) {
            setCndeDal2007Dfz(new AfDecimal(cndeDal2007DfzObj, 15, 3));
            ws.getIndDForzLiq().setCndeDal2007Dfz(((short)0));
        }
        else {
            ws.getIndDForzLiq().setCndeDal2007Dfz(((short)-1));
        }
    }

    @Override
    public AfDecimal getCndeDal2007Efflq() {
        return dForzLiq.getDflCndeDal2007Efflq().getDflCndeDal2007Efflq();
    }

    @Override
    public void setCndeDal2007Efflq(AfDecimal cndeDal2007Efflq) {
        this.dForzLiq.getDflCndeDal2007Efflq().setDflCndeDal2007Efflq(cndeDal2007Efflq.copy());
    }

    @Override
    public AfDecimal getCndeDal2007EfflqObj() {
        if (ws.getIndDForzLiq().getCndeDal2007Efflq() >= 0) {
            return getCndeDal2007Efflq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCndeDal2007EfflqObj(AfDecimal cndeDal2007EfflqObj) {
        if (cndeDal2007EfflqObj != null) {
            setCndeDal2007Efflq(new AfDecimal(cndeDal2007EfflqObj, 15, 3));
            ws.getIndDForzLiq().setCndeDal2007Efflq(((short)0));
        }
        else {
            ws.getIndDForzLiq().setCndeDal2007Efflq(((short)-1));
        }
    }

    @Override
    public AfDecimal getCndeEnd2000Calc() {
        return dForzLiq.getDflCndeEnd2000Calc().getDflCndeEnd2000Calc();
    }

    @Override
    public void setCndeEnd2000Calc(AfDecimal cndeEnd2000Calc) {
        this.dForzLiq.getDflCndeEnd2000Calc().setDflCndeEnd2000Calc(cndeEnd2000Calc.copy());
    }

    @Override
    public AfDecimal getCndeEnd2000CalcObj() {
        if (ws.getIndDForzLiq().getCndeEnd2000Calc() >= 0) {
            return getCndeEnd2000Calc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCndeEnd2000CalcObj(AfDecimal cndeEnd2000CalcObj) {
        if (cndeEnd2000CalcObj != null) {
            setCndeEnd2000Calc(new AfDecimal(cndeEnd2000CalcObj, 15, 3));
            ws.getIndDForzLiq().setCndeEnd2000Calc(((short)0));
        }
        else {
            ws.getIndDForzLiq().setCndeEnd2000Calc(((short)-1));
        }
    }

    @Override
    public AfDecimal getCndeEnd2000Dfz() {
        return dForzLiq.getDflCndeEnd2000Dfz().getDflCndeEnd2000Dfz();
    }

    @Override
    public void setCndeEnd2000Dfz(AfDecimal cndeEnd2000Dfz) {
        this.dForzLiq.getDflCndeEnd2000Dfz().setDflCndeEnd2000Dfz(cndeEnd2000Dfz.copy());
    }

    @Override
    public AfDecimal getCndeEnd2000DfzObj() {
        if (ws.getIndDForzLiq().getCndeEnd2000Dfz() >= 0) {
            return getCndeEnd2000Dfz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCndeEnd2000DfzObj(AfDecimal cndeEnd2000DfzObj) {
        if (cndeEnd2000DfzObj != null) {
            setCndeEnd2000Dfz(new AfDecimal(cndeEnd2000DfzObj, 15, 3));
            ws.getIndDForzLiq().setCndeEnd2000Dfz(((short)0));
        }
        else {
            ws.getIndDForzLiq().setCndeEnd2000Dfz(((short)-1));
        }
    }

    @Override
    public AfDecimal getCndeEnd2000Efflq() {
        return dForzLiq.getDflCndeEnd2000Efflq().getDflCndeEnd2000Efflq();
    }

    @Override
    public void setCndeEnd2000Efflq(AfDecimal cndeEnd2000Efflq) {
        this.dForzLiq.getDflCndeEnd2000Efflq().setDflCndeEnd2000Efflq(cndeEnd2000Efflq.copy());
    }

    @Override
    public AfDecimal getCndeEnd2000EfflqObj() {
        if (ws.getIndDForzLiq().getCndeEnd2000Efflq() >= 0) {
            return getCndeEnd2000Efflq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCndeEnd2000EfflqObj(AfDecimal cndeEnd2000EfflqObj) {
        if (cndeEnd2000EfflqObj != null) {
            setCndeEnd2000Efflq(new AfDecimal(cndeEnd2000EfflqObj, 15, 3));
            ws.getIndDForzLiq().setCndeEnd2000Efflq(((short)0));
        }
        else {
            ws.getIndDForzLiq().setCndeEnd2000Efflq(((short)-1));
        }
    }

    @Override
    public AfDecimal getCndeEnd2006Calc() {
        return dForzLiq.getDflCndeEnd2006Calc().getDflCndeEnd2006Calc();
    }

    @Override
    public void setCndeEnd2006Calc(AfDecimal cndeEnd2006Calc) {
        this.dForzLiq.getDflCndeEnd2006Calc().setDflCndeEnd2006Calc(cndeEnd2006Calc.copy());
    }

    @Override
    public AfDecimal getCndeEnd2006CalcObj() {
        if (ws.getIndDForzLiq().getCndeEnd2006Calc() >= 0) {
            return getCndeEnd2006Calc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCndeEnd2006CalcObj(AfDecimal cndeEnd2006CalcObj) {
        if (cndeEnd2006CalcObj != null) {
            setCndeEnd2006Calc(new AfDecimal(cndeEnd2006CalcObj, 15, 3));
            ws.getIndDForzLiq().setCndeEnd2006Calc(((short)0));
        }
        else {
            ws.getIndDForzLiq().setCndeEnd2006Calc(((short)-1));
        }
    }

    @Override
    public AfDecimal getCndeEnd2006Dfz() {
        return dForzLiq.getDflCndeEnd2006Dfz().getDflCndeEnd2006Dfz();
    }

    @Override
    public void setCndeEnd2006Dfz(AfDecimal cndeEnd2006Dfz) {
        this.dForzLiq.getDflCndeEnd2006Dfz().setDflCndeEnd2006Dfz(cndeEnd2006Dfz.copy());
    }

    @Override
    public AfDecimal getCndeEnd2006DfzObj() {
        if (ws.getIndDForzLiq().getCndeEnd2006Dfz() >= 0) {
            return getCndeEnd2006Dfz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCndeEnd2006DfzObj(AfDecimal cndeEnd2006DfzObj) {
        if (cndeEnd2006DfzObj != null) {
            setCndeEnd2006Dfz(new AfDecimal(cndeEnd2006DfzObj, 15, 3));
            ws.getIndDForzLiq().setCndeEnd2006Dfz(((short)0));
        }
        else {
            ws.getIndDForzLiq().setCndeEnd2006Dfz(((short)-1));
        }
    }

    @Override
    public AfDecimal getCndeEnd2006Efflq() {
        return dForzLiq.getDflCndeEnd2006Efflq().getDflCndeEnd2006Efflq();
    }

    @Override
    public void setCndeEnd2006Efflq(AfDecimal cndeEnd2006Efflq) {
        this.dForzLiq.getDflCndeEnd2006Efflq().setDflCndeEnd2006Efflq(cndeEnd2006Efflq.copy());
    }

    @Override
    public AfDecimal getCndeEnd2006EfflqObj() {
        if (ws.getIndDForzLiq().getCndeEnd2006Efflq() >= 0) {
            return getCndeEnd2006Efflq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCndeEnd2006EfflqObj(AfDecimal cndeEnd2006EfflqObj) {
        if (cndeEnd2006EfflqObj != null) {
            setCndeEnd2006Efflq(new AfDecimal(cndeEnd2006EfflqObj, 15, 3));
            ws.getIndDForzLiq().setCndeEnd2006Efflq(((short)0));
        }
        else {
            ws.getIndDForzLiq().setCndeEnd2006Efflq(((short)-1));
        }
    }

    @Override
    public int getCodCompAnia() {
        return dForzLiq.getDflCodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.dForzLiq.setDflCodCompAnia(codCompAnia);
    }

    @Override
    public long getDflDsRiga() {
        return dForzLiq.getDflDsRiga();
    }

    @Override
    public void setDflDsRiga(long dflDsRiga) {
        this.dForzLiq.setDflDsRiga(dflDsRiga);
    }

    @Override
    public char getDsOperSql() {
        return dForzLiq.getDflDsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.dForzLiq.setDflDsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return dForzLiq.getDflDsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.dForzLiq.setDflDsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsEndCptz() {
        return dForzLiq.getDflDsTsEndCptz();
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.dForzLiq.setDflDsTsEndCptz(dsTsEndCptz);
    }

    @Override
    public long getDsTsIniCptz() {
        return dForzLiq.getDflDsTsIniCptz();
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.dForzLiq.setDflDsTsIniCptz(dsTsIniCptz);
    }

    @Override
    public String getDsUtente() {
        return dForzLiq.getDflDsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.dForzLiq.setDflDsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return dForzLiq.getDflDsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.dForzLiq.setDflDsVer(dsVer);
    }

    @Override
    public String getDtEndEffDb() {
        return ws.getIdbvdfl3().getFlDtEndEffDb();
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        this.ws.getIdbvdfl3().setFlDtEndEffDb(dtEndEffDb);
    }

    @Override
    public String getDtIniEffDb() {
        return ws.getIdbvdfl3().getFlDtIniEffDb();
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        this.ws.getIdbvdfl3().setFlDtIniEffDb(dtIniEffDb);
    }

    @Override
    public AfDecimal getIcnbInpstfmCalc() {
        return dForzLiq.getDflIcnbInpstfmCalc().getDflIcnbInpstfmCalc();
    }

    @Override
    public void setIcnbInpstfmCalc(AfDecimal icnbInpstfmCalc) {
        this.dForzLiq.getDflIcnbInpstfmCalc().setDflIcnbInpstfmCalc(icnbInpstfmCalc.copy());
    }

    @Override
    public AfDecimal getIcnbInpstfmCalcObj() {
        if (ws.getIndDForzLiq().getIcnbInpstfmCalc() >= 0) {
            return getIcnbInpstfmCalc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIcnbInpstfmCalcObj(AfDecimal icnbInpstfmCalcObj) {
        if (icnbInpstfmCalcObj != null) {
            setIcnbInpstfmCalc(new AfDecimal(icnbInpstfmCalcObj, 15, 3));
            ws.getIndDForzLiq().setIcnbInpstfmCalc(((short)0));
        }
        else {
            ws.getIndDForzLiq().setIcnbInpstfmCalc(((short)-1));
        }
    }

    @Override
    public AfDecimal getIcnbInpstfmDfz() {
        return dForzLiq.getDflIcnbInpstfmDfz().getDflIcnbInpstfmDfz();
    }

    @Override
    public void setIcnbInpstfmDfz(AfDecimal icnbInpstfmDfz) {
        this.dForzLiq.getDflIcnbInpstfmDfz().setDflIcnbInpstfmDfz(icnbInpstfmDfz.copy());
    }

    @Override
    public AfDecimal getIcnbInpstfmDfzObj() {
        if (ws.getIndDForzLiq().getIcnbInpstfmDfz() >= 0) {
            return getIcnbInpstfmDfz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIcnbInpstfmDfzObj(AfDecimal icnbInpstfmDfzObj) {
        if (icnbInpstfmDfzObj != null) {
            setIcnbInpstfmDfz(new AfDecimal(icnbInpstfmDfzObj, 15, 3));
            ws.getIndDForzLiq().setIcnbInpstfmDfz(((short)0));
        }
        else {
            ws.getIndDForzLiq().setIcnbInpstfmDfz(((short)-1));
        }
    }

    @Override
    public AfDecimal getIcnbInpstfmEfflq() {
        return dForzLiq.getDflIcnbInpstfmEfflq().getDflIcnbInpstfmEfflq();
    }

    @Override
    public void setIcnbInpstfmEfflq(AfDecimal icnbInpstfmEfflq) {
        this.dForzLiq.getDflIcnbInpstfmEfflq().setDflIcnbInpstfmEfflq(icnbInpstfmEfflq.copy());
    }

    @Override
    public AfDecimal getIcnbInpstfmEfflqObj() {
        if (ws.getIndDForzLiq().getIcnbInpstfmEfflq() >= 0) {
            return getIcnbInpstfmEfflq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIcnbInpstfmEfflqObj(AfDecimal icnbInpstfmEfflqObj) {
        if (icnbInpstfmEfflqObj != null) {
            setIcnbInpstfmEfflq(new AfDecimal(icnbInpstfmEfflqObj, 15, 3));
            ws.getIndDForzLiq().setIcnbInpstfmEfflq(((short)0));
        }
        else {
            ws.getIndDForzLiq().setIcnbInpstfmEfflq(((short)-1));
        }
    }

    @Override
    public int getIdDForzLiq() {
        return dForzLiq.getDflIdDForzLiq();
    }

    @Override
    public void setIdDForzLiq(int idDForzLiq) {
        this.dForzLiq.setDflIdDForzLiq(idDForzLiq);
    }

    @Override
    public int getIdLiq() {
        return dForzLiq.getDflIdLiq();
    }

    @Override
    public void setIdLiq(int idLiq) {
        this.dForzLiq.setDflIdLiq(idLiq);
    }

    @Override
    public int getIdMoviChiu() {
        return dForzLiq.getDflIdMoviChiu().getDflIdMoviChiu();
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        this.dForzLiq.getDflIdMoviChiu().setDflIdMoviChiu(idMoviChiu);
    }

    @Override
    public Integer getIdMoviChiuObj() {
        if (ws.getIndDForzLiq().getIdMoviChiu() >= 0) {
            return ((Integer)getIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        if (idMoviChiuObj != null) {
            setIdMoviChiu(((int)idMoviChiuObj));
            ws.getIndDForzLiq().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndDForzLiq().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getIdMoviCrz() {
        return dForzLiq.getDflIdMoviCrz();
    }

    @Override
    public void setIdMoviCrz(int idMoviCrz) {
        this.dForzLiq.setDflIdMoviCrz(idMoviCrz);
    }

    @Override
    public AfDecimal getIimpst252Calc() {
        return dForzLiq.getDflIimpst252Calc().getDflIimpst252Calc();
    }

    @Override
    public void setIimpst252Calc(AfDecimal iimpst252Calc) {
        this.dForzLiq.getDflIimpst252Calc().setDflIimpst252Calc(iimpst252Calc.copy());
    }

    @Override
    public AfDecimal getIimpst252CalcObj() {
        if (ws.getIndDForzLiq().getIimpst252Calc() >= 0) {
            return getIimpst252Calc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIimpst252CalcObj(AfDecimal iimpst252CalcObj) {
        if (iimpst252CalcObj != null) {
            setIimpst252Calc(new AfDecimal(iimpst252CalcObj, 15, 3));
            ws.getIndDForzLiq().setIimpst252Calc(((short)0));
        }
        else {
            ws.getIndDForzLiq().setIimpst252Calc(((short)-1));
        }
    }

    @Override
    public AfDecimal getIimpst252Dfz() {
        return dForzLiq.getDflIimpst252Dfz().getDflIimpst252Dfz();
    }

    @Override
    public void setIimpst252Dfz(AfDecimal iimpst252Dfz) {
        this.dForzLiq.getDflIimpst252Dfz().setDflIimpst252Dfz(iimpst252Dfz.copy());
    }

    @Override
    public AfDecimal getIimpst252DfzObj() {
        if (ws.getIndDForzLiq().getIimpst252Dfz() >= 0) {
            return getIimpst252Dfz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIimpst252DfzObj(AfDecimal iimpst252DfzObj) {
        if (iimpst252DfzObj != null) {
            setIimpst252Dfz(new AfDecimal(iimpst252DfzObj, 15, 3));
            ws.getIndDForzLiq().setIimpst252Dfz(((short)0));
        }
        else {
            ws.getIndDForzLiq().setIimpst252Dfz(((short)-1));
        }
    }

    @Override
    public AfDecimal getIimpst252Efflq() {
        return dForzLiq.getDflIimpst252Efflq().getDflIimpst252Efflq();
    }

    @Override
    public void setIimpst252Efflq(AfDecimal iimpst252Efflq) {
        this.dForzLiq.getDflIimpst252Efflq().setDflIimpst252Efflq(iimpst252Efflq.copy());
    }

    @Override
    public AfDecimal getIimpst252EfflqObj() {
        if (ws.getIndDForzLiq().getIimpst252Efflq() >= 0) {
            return getIimpst252Efflq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIimpst252EfflqObj(AfDecimal iimpst252EfflqObj) {
        if (iimpst252EfflqObj != null) {
            setIimpst252Efflq(new AfDecimal(iimpst252EfflqObj, 15, 3));
            ws.getIndDForzLiq().setIimpst252Efflq(((short)0));
        }
        else {
            ws.getIndDForzLiq().setIimpst252Efflq(((short)-1));
        }
    }

    @Override
    public AfDecimal getIimpstPrvrCalc() {
        return dForzLiq.getDflIimpstPrvrCalc().getDflIimpstPrvrCalc();
    }

    @Override
    public void setIimpstPrvrCalc(AfDecimal iimpstPrvrCalc) {
        this.dForzLiq.getDflIimpstPrvrCalc().setDflIimpstPrvrCalc(iimpstPrvrCalc.copy());
    }

    @Override
    public AfDecimal getIimpstPrvrCalcObj() {
        if (ws.getIndDForzLiq().getIimpstPrvrCalc() >= 0) {
            return getIimpstPrvrCalc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIimpstPrvrCalcObj(AfDecimal iimpstPrvrCalcObj) {
        if (iimpstPrvrCalcObj != null) {
            setIimpstPrvrCalc(new AfDecimal(iimpstPrvrCalcObj, 15, 3));
            ws.getIndDForzLiq().setIimpstPrvrCalc(((short)0));
        }
        else {
            ws.getIndDForzLiq().setIimpstPrvrCalc(((short)-1));
        }
    }

    @Override
    public AfDecimal getIimpstPrvrDfz() {
        return dForzLiq.getDflIimpstPrvrDfz().getDflIimpstPrvrDfz();
    }

    @Override
    public void setIimpstPrvrDfz(AfDecimal iimpstPrvrDfz) {
        this.dForzLiq.getDflIimpstPrvrDfz().setDflIimpstPrvrDfz(iimpstPrvrDfz.copy());
    }

    @Override
    public AfDecimal getIimpstPrvrDfzObj() {
        if (ws.getIndDForzLiq().getIimpstPrvrDfz() >= 0) {
            return getIimpstPrvrDfz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIimpstPrvrDfzObj(AfDecimal iimpstPrvrDfzObj) {
        if (iimpstPrvrDfzObj != null) {
            setIimpstPrvrDfz(new AfDecimal(iimpstPrvrDfzObj, 15, 3));
            ws.getIndDForzLiq().setIimpstPrvrDfz(((short)0));
        }
        else {
            ws.getIndDForzLiq().setIimpstPrvrDfz(((short)-1));
        }
    }

    @Override
    public AfDecimal getIimpstPrvrEfflq() {
        return dForzLiq.getDflIimpstPrvrEfflq().getDflIimpstPrvrEfflq();
    }

    @Override
    public void setIimpstPrvrEfflq(AfDecimal iimpstPrvrEfflq) {
        this.dForzLiq.getDflIimpstPrvrEfflq().setDflIimpstPrvrEfflq(iimpstPrvrEfflq.copy());
    }

    @Override
    public AfDecimal getIimpstPrvrEfflqObj() {
        if (ws.getIndDForzLiq().getIimpstPrvrEfflq() >= 0) {
            return getIimpstPrvrEfflq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIimpstPrvrEfflqObj(AfDecimal iimpstPrvrEfflqObj) {
        if (iimpstPrvrEfflqObj != null) {
            setIimpstPrvrEfflq(new AfDecimal(iimpstPrvrEfflqObj, 15, 3));
            ws.getIndDForzLiq().setIimpstPrvrEfflq(((short)0));
        }
        else {
            ws.getIndDForzLiq().setIimpstPrvrEfflq(((short)-1));
        }
    }

    @Override
    public AfDecimal getIintPrestCalc() {
        return dForzLiq.getDflIintPrestCalc().getDflIintPrestCalc();
    }

    @Override
    public void setIintPrestCalc(AfDecimal iintPrestCalc) {
        this.dForzLiq.getDflIintPrestCalc().setDflIintPrestCalc(iintPrestCalc.copy());
    }

    @Override
    public AfDecimal getIintPrestCalcObj() {
        if (ws.getIndDForzLiq().getIintPrestCalc() >= 0) {
            return getIintPrestCalc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIintPrestCalcObj(AfDecimal iintPrestCalcObj) {
        if (iintPrestCalcObj != null) {
            setIintPrestCalc(new AfDecimal(iintPrestCalcObj, 15, 3));
            ws.getIndDForzLiq().setIintPrestCalc(((short)0));
        }
        else {
            ws.getIndDForzLiq().setIintPrestCalc(((short)-1));
        }
    }

    @Override
    public AfDecimal getIintPrestDfz() {
        return dForzLiq.getDflIintPrestDfz().getDflIintPrestDfz();
    }

    @Override
    public void setIintPrestDfz(AfDecimal iintPrestDfz) {
        this.dForzLiq.getDflIintPrestDfz().setDflIintPrestDfz(iintPrestDfz.copy());
    }

    @Override
    public AfDecimal getIintPrestDfzObj() {
        if (ws.getIndDForzLiq().getIintPrestDfz() >= 0) {
            return getIintPrestDfz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIintPrestDfzObj(AfDecimal iintPrestDfzObj) {
        if (iintPrestDfzObj != null) {
            setIintPrestDfz(new AfDecimal(iintPrestDfzObj, 15, 3));
            ws.getIndDForzLiq().setIintPrestDfz(((short)0));
        }
        else {
            ws.getIndDForzLiq().setIintPrestDfz(((short)-1));
        }
    }

    @Override
    public AfDecimal getIintPrestEfflq() {
        return dForzLiq.getDflIintPrestEfflq().getDflIintPrestEfflq();
    }

    @Override
    public void setIintPrestEfflq(AfDecimal iintPrestEfflq) {
        this.dForzLiq.getDflIintPrestEfflq().setDflIintPrestEfflq(iintPrestEfflq.copy());
    }

    @Override
    public AfDecimal getIintPrestEfflqObj() {
        if (ws.getIndDForzLiq().getIintPrestEfflq() >= 0) {
            return getIintPrestEfflq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIintPrestEfflqObj(AfDecimal iintPrestEfflqObj) {
        if (iintPrestEfflqObj != null) {
            setIintPrestEfflq(new AfDecimal(iintPrestEfflqObj, 15, 3));
            ws.getIndDForzLiq().setIintPrestEfflq(((short)0));
        }
        else {
            ws.getIndDForzLiq().setIintPrestEfflq(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpExcontrEff() {
        return dForzLiq.getDflImpExcontrEff().getDflImpExcontrEff();
    }

    @Override
    public void setImpExcontrEff(AfDecimal impExcontrEff) {
        this.dForzLiq.getDflImpExcontrEff().setDflImpExcontrEff(impExcontrEff.copy());
    }

    @Override
    public AfDecimal getImpExcontrEffObj() {
        if (ws.getIndDForzLiq().getImpExcontrEff() >= 0) {
            return getImpExcontrEff();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpExcontrEffObj(AfDecimal impExcontrEffObj) {
        if (impExcontrEffObj != null) {
            setImpExcontrEff(new AfDecimal(impExcontrEffObj, 15, 3));
            ws.getIndDForzLiq().setImpExcontrEff(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpExcontrEff(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpIntrRitPagC() {
        return dForzLiq.getDflImpIntrRitPagC().getDflImpIntrRitPagC();
    }

    @Override
    public void setImpIntrRitPagC(AfDecimal impIntrRitPagC) {
        this.dForzLiq.getDflImpIntrRitPagC().setDflImpIntrRitPagC(impIntrRitPagC.copy());
    }

    @Override
    public AfDecimal getImpIntrRitPagCObj() {
        if (ws.getIndDForzLiq().getImpIntrRitPagC() >= 0) {
            return getImpIntrRitPagC();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpIntrRitPagCObj(AfDecimal impIntrRitPagCObj) {
        if (impIntrRitPagCObj != null) {
            setImpIntrRitPagC(new AfDecimal(impIntrRitPagCObj, 15, 3));
            ws.getIndDForzLiq().setImpIntrRitPagC(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpIntrRitPagC(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpIntrRitPagD() {
        return dForzLiq.getDflImpIntrRitPagD().getDflImpIntrRitPagD();
    }

    @Override
    public void setImpIntrRitPagD(AfDecimal impIntrRitPagD) {
        this.dForzLiq.getDflImpIntrRitPagD().setDflImpIntrRitPagD(impIntrRitPagD.copy());
    }

    @Override
    public AfDecimal getImpIntrRitPagDObj() {
        if (ws.getIndDForzLiq().getImpIntrRitPagD() >= 0) {
            return getImpIntrRitPagD();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpIntrRitPagDObj(AfDecimal impIntrRitPagDObj) {
        if (impIntrRitPagDObj != null) {
            setImpIntrRitPagD(new AfDecimal(impIntrRitPagDObj, 15, 3));
            ws.getIndDForzLiq().setImpIntrRitPagD(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpIntrRitPagD(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpIntrRitPagL() {
        return dForzLiq.getDflImpIntrRitPagL().getDflImpIntrRitPagL();
    }

    @Override
    public void setImpIntrRitPagL(AfDecimal impIntrRitPagL) {
        this.dForzLiq.getDflImpIntrRitPagL().setDflImpIntrRitPagL(impIntrRitPagL.copy());
    }

    @Override
    public AfDecimal getImpIntrRitPagLObj() {
        if (ws.getIndDForzLiq().getImpIntrRitPagL() >= 0) {
            return getImpIntrRitPagL();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpIntrRitPagLObj(AfDecimal impIntrRitPagLObj) {
        if (impIntrRitPagLObj != null) {
            setImpIntrRitPagL(new AfDecimal(impIntrRitPagLObj, 15, 3));
            ws.getIndDForzLiq().setImpIntrRitPagL(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpIntrRitPagL(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpLrdCalc() {
        return dForzLiq.getDflImpLrdCalc().getDflImpLrdCalc();
    }

    @Override
    public void setImpLrdCalc(AfDecimal impLrdCalc) {
        this.dForzLiq.getDflImpLrdCalc().setDflImpLrdCalc(impLrdCalc.copy());
    }

    @Override
    public AfDecimal getImpLrdCalcObj() {
        if (ws.getIndDForzLiq().getImpLrdCalc() >= 0) {
            return getImpLrdCalc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpLrdCalcObj(AfDecimal impLrdCalcObj) {
        if (impLrdCalcObj != null) {
            setImpLrdCalc(new AfDecimal(impLrdCalcObj, 15, 3));
            ws.getIndDForzLiq().setImpLrdCalc(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpLrdCalc(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpLrdDfz() {
        return dForzLiq.getDflImpLrdDfz().getDflImpLrdDfz();
    }

    @Override
    public void setImpLrdDfz(AfDecimal impLrdDfz) {
        this.dForzLiq.getDflImpLrdDfz().setDflImpLrdDfz(impLrdDfz.copy());
    }

    @Override
    public AfDecimal getImpLrdDfzObj() {
        if (ws.getIndDForzLiq().getImpLrdDfz() >= 0) {
            return getImpLrdDfz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpLrdDfzObj(AfDecimal impLrdDfzObj) {
        if (impLrdDfzObj != null) {
            setImpLrdDfz(new AfDecimal(impLrdDfzObj, 15, 3));
            ws.getIndDForzLiq().setImpLrdDfz(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpLrdDfz(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpLrdEfflq() {
        return dForzLiq.getDflImpLrdEfflq().getDflImpLrdEfflq();
    }

    @Override
    public void setImpLrdEfflq(AfDecimal impLrdEfflq) {
        this.dForzLiq.getDflImpLrdEfflq().setDflImpLrdEfflq(impLrdEfflq.copy());
    }

    @Override
    public AfDecimal getImpLrdEfflqObj() {
        if (ws.getIndDForzLiq().getImpLrdEfflq() >= 0) {
            return getImpLrdEfflq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpLrdEfflqObj(AfDecimal impLrdEfflqObj) {
        if (impLrdEfflqObj != null) {
            setImpLrdEfflq(new AfDecimal(impLrdEfflqObj, 15, 3));
            ws.getIndDForzLiq().setImpLrdEfflq(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpLrdEfflq(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpNetCalc() {
        return dForzLiq.getDflImpNetCalc().getDflImpNetCalc();
    }

    @Override
    public void setImpNetCalc(AfDecimal impNetCalc) {
        this.dForzLiq.getDflImpNetCalc().setDflImpNetCalc(impNetCalc.copy());
    }

    @Override
    public AfDecimal getImpNetCalcObj() {
        if (ws.getIndDForzLiq().getImpNetCalc() >= 0) {
            return getImpNetCalc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpNetCalcObj(AfDecimal impNetCalcObj) {
        if (impNetCalcObj != null) {
            setImpNetCalc(new AfDecimal(impNetCalcObj, 15, 3));
            ws.getIndDForzLiq().setImpNetCalc(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpNetCalc(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpNetDfz() {
        return dForzLiq.getDflImpNetDfz().getDflImpNetDfz();
    }

    @Override
    public void setImpNetDfz(AfDecimal impNetDfz) {
        this.dForzLiq.getDflImpNetDfz().setDflImpNetDfz(impNetDfz.copy());
    }

    @Override
    public AfDecimal getImpNetDfzObj() {
        if (ws.getIndDForzLiq().getImpNetDfz() >= 0) {
            return getImpNetDfz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpNetDfzObj(AfDecimal impNetDfzObj) {
        if (impNetDfzObj != null) {
            setImpNetDfz(new AfDecimal(impNetDfzObj, 15, 3));
            ws.getIndDForzLiq().setImpNetDfz(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpNetDfz(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpNetEfflq() {
        return dForzLiq.getDflImpNetEfflq().getDflImpNetEfflq();
    }

    @Override
    public void setImpNetEfflq(AfDecimal impNetEfflq) {
        this.dForzLiq.getDflImpNetEfflq().setDflImpNetEfflq(impNetEfflq.copy());
    }

    @Override
    public AfDecimal getImpNetEfflqObj() {
        if (ws.getIndDForzLiq().getImpNetEfflq() >= 0) {
            return getImpNetEfflq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpNetEfflqObj(AfDecimal impNetEfflqObj) {
        if (impNetEfflqObj != null) {
            setImpNetEfflq(new AfDecimal(impNetEfflqObj, 15, 3));
            ws.getIndDForzLiq().setImpNetEfflq(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpNetEfflq(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbBolloDettC() {
        return dForzLiq.getDflImpbBolloDettC().getDflImpbBolloDettC();
    }

    @Override
    public void setImpbBolloDettC(AfDecimal impbBolloDettC) {
        this.dForzLiq.getDflImpbBolloDettC().setDflImpbBolloDettC(impbBolloDettC.copy());
    }

    @Override
    public AfDecimal getImpbBolloDettCObj() {
        if (ws.getIndDForzLiq().getImpbBolloDettC() >= 0) {
            return getImpbBolloDettC();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbBolloDettCObj(AfDecimal impbBolloDettCObj) {
        if (impbBolloDettCObj != null) {
            setImpbBolloDettC(new AfDecimal(impbBolloDettCObj, 15, 3));
            ws.getIndDForzLiq().setImpbBolloDettC(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpbBolloDettC(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbBolloDettD() {
        return dForzLiq.getDflImpbBolloDettD().getDflImpbBolloDettD();
    }

    @Override
    public void setImpbBolloDettD(AfDecimal impbBolloDettD) {
        this.dForzLiq.getDflImpbBolloDettD().setDflImpbBolloDettD(impbBolloDettD.copy());
    }

    @Override
    public AfDecimal getImpbBolloDettDObj() {
        if (ws.getIndDForzLiq().getImpbBolloDettD() >= 0) {
            return getImpbBolloDettD();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbBolloDettDObj(AfDecimal impbBolloDettDObj) {
        if (impbBolloDettDObj != null) {
            setImpbBolloDettD(new AfDecimal(impbBolloDettDObj, 15, 3));
            ws.getIndDForzLiq().setImpbBolloDettD(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpbBolloDettD(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbBolloDettL() {
        return dForzLiq.getDflImpbBolloDettL().getDflImpbBolloDettL();
    }

    @Override
    public void setImpbBolloDettL(AfDecimal impbBolloDettL) {
        this.dForzLiq.getDflImpbBolloDettL().setDflImpbBolloDettL(impbBolloDettL.copy());
    }

    @Override
    public AfDecimal getImpbBolloDettLObj() {
        if (ws.getIndDForzLiq().getImpbBolloDettL() >= 0) {
            return getImpbBolloDettL();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbBolloDettLObj(AfDecimal impbBolloDettLObj) {
        if (impbBolloDettLObj != null) {
            setImpbBolloDettL(new AfDecimal(impbBolloDettLObj, 15, 3));
            ws.getIndDForzLiq().setImpbBolloDettL(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpbBolloDettL(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbIs1382011c() {
        return dForzLiq.getDflImpbIs1382011c().getDflImpbIs1382011c();
    }

    @Override
    public void setImpbIs1382011c(AfDecimal impbIs1382011c) {
        this.dForzLiq.getDflImpbIs1382011c().setDflImpbIs1382011c(impbIs1382011c.copy());
    }

    @Override
    public AfDecimal getImpbIs1382011cObj() {
        if (ws.getIndDForzLiq().getImpbIs1382011c() >= 0) {
            return getImpbIs1382011c();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbIs1382011cObj(AfDecimal impbIs1382011cObj) {
        if (impbIs1382011cObj != null) {
            setImpbIs1382011c(new AfDecimal(impbIs1382011cObj, 15, 3));
            ws.getIndDForzLiq().setImpbIs1382011c(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpbIs1382011c(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbIs1382011d() {
        return dForzLiq.getDflImpbIs1382011d().getDflImpbIs1382011d();
    }

    @Override
    public void setImpbIs1382011d(AfDecimal impbIs1382011d) {
        this.dForzLiq.getDflImpbIs1382011d().setDflImpbIs1382011d(impbIs1382011d.copy());
    }

    @Override
    public AfDecimal getImpbIs1382011dObj() {
        if (ws.getIndDForzLiq().getImpbIs1382011d() >= 0) {
            return getImpbIs1382011d();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbIs1382011dObj(AfDecimal impbIs1382011dObj) {
        if (impbIs1382011dObj != null) {
            setImpbIs1382011d(new AfDecimal(impbIs1382011dObj, 15, 3));
            ws.getIndDForzLiq().setImpbIs1382011d(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpbIs1382011d(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbIs1382011l() {
        return dForzLiq.getDflImpbIs1382011l().getDflImpbIs1382011l();
    }

    @Override
    public void setImpbIs1382011l(AfDecimal impbIs1382011l) {
        this.dForzLiq.getDflImpbIs1382011l().setDflImpbIs1382011l(impbIs1382011l.copy());
    }

    @Override
    public AfDecimal getImpbIs1382011lObj() {
        if (ws.getIndDForzLiq().getImpbIs1382011l() >= 0) {
            return getImpbIs1382011l();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbIs1382011lObj(AfDecimal impbIs1382011lObj) {
        if (impbIs1382011lObj != null) {
            setImpbIs1382011l(new AfDecimal(impbIs1382011lObj, 15, 3));
            ws.getIndDForzLiq().setImpbIs1382011l(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpbIs1382011l(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbIs662014c() {
        return dForzLiq.getDflImpbIs662014c().getDflImpbIs662014c();
    }

    @Override
    public void setImpbIs662014c(AfDecimal impbIs662014c) {
        this.dForzLiq.getDflImpbIs662014c().setDflImpbIs662014c(impbIs662014c.copy());
    }

    @Override
    public AfDecimal getImpbIs662014cObj() {
        if (ws.getIndDForzLiq().getImpbIs662014c() >= 0) {
            return getImpbIs662014c();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbIs662014cObj(AfDecimal impbIs662014cObj) {
        if (impbIs662014cObj != null) {
            setImpbIs662014c(new AfDecimal(impbIs662014cObj, 15, 3));
            ws.getIndDForzLiq().setImpbIs662014c(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpbIs662014c(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbIs662014d() {
        return dForzLiq.getDflImpbIs662014d().getDflImpbIs662014d();
    }

    @Override
    public void setImpbIs662014d(AfDecimal impbIs662014d) {
        this.dForzLiq.getDflImpbIs662014d().setDflImpbIs662014d(impbIs662014d.copy());
    }

    @Override
    public AfDecimal getImpbIs662014dObj() {
        if (ws.getIndDForzLiq().getImpbIs662014d() >= 0) {
            return getImpbIs662014d();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbIs662014dObj(AfDecimal impbIs662014dObj) {
        if (impbIs662014dObj != null) {
            setImpbIs662014d(new AfDecimal(impbIs662014dObj, 15, 3));
            ws.getIndDForzLiq().setImpbIs662014d(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpbIs662014d(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbIs662014l() {
        return dForzLiq.getDflImpbIs662014l().getDflImpbIs662014l();
    }

    @Override
    public void setImpbIs662014l(AfDecimal impbIs662014l) {
        this.dForzLiq.getDflImpbIs662014l().setDflImpbIs662014l(impbIs662014l.copy());
    }

    @Override
    public AfDecimal getImpbIs662014lObj() {
        if (ws.getIndDForzLiq().getImpbIs662014l() >= 0) {
            return getImpbIs662014l();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbIs662014lObj(AfDecimal impbIs662014lObj) {
        if (impbIs662014lObj != null) {
            setImpbIs662014l(new AfDecimal(impbIs662014lObj, 15, 3));
            ws.getIndDForzLiq().setImpbIs662014l(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpbIs662014l(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbIsCalc() {
        return dForzLiq.getDflImpbIsCalc().getDflImpbIsCalc();
    }

    @Override
    public void setImpbIsCalc(AfDecimal impbIsCalc) {
        this.dForzLiq.getDflImpbIsCalc().setDflImpbIsCalc(impbIsCalc.copy());
    }

    @Override
    public AfDecimal getImpbIsCalcObj() {
        if (ws.getIndDForzLiq().getImpbIsCalc() >= 0) {
            return getImpbIsCalc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbIsCalcObj(AfDecimal impbIsCalcObj) {
        if (impbIsCalcObj != null) {
            setImpbIsCalc(new AfDecimal(impbIsCalcObj, 15, 3));
            ws.getIndDForzLiq().setImpbIsCalc(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpbIsCalc(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbIsDfz() {
        return dForzLiq.getDflImpbIsDfz().getDflImpbIsDfz();
    }

    @Override
    public void setImpbIsDfz(AfDecimal impbIsDfz) {
        this.dForzLiq.getDflImpbIsDfz().setDflImpbIsDfz(impbIsDfz.copy());
    }

    @Override
    public AfDecimal getImpbIsDfzObj() {
        if (ws.getIndDForzLiq().getImpbIsDfz() >= 0) {
            return getImpbIsDfz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbIsDfzObj(AfDecimal impbIsDfzObj) {
        if (impbIsDfzObj != null) {
            setImpbIsDfz(new AfDecimal(impbIsDfzObj, 15, 3));
            ws.getIndDForzLiq().setImpbIsDfz(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpbIsDfz(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbIsEfflq() {
        return dForzLiq.getDflImpbIsEfflq().getDflImpbIsEfflq();
    }

    @Override
    public void setImpbIsEfflq(AfDecimal impbIsEfflq) {
        this.dForzLiq.getDflImpbIsEfflq().setDflImpbIsEfflq(impbIsEfflq.copy());
    }

    @Override
    public AfDecimal getImpbIsEfflqObj() {
        if (ws.getIndDForzLiq().getImpbIsEfflq() >= 0) {
            return getImpbIsEfflq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbIsEfflqObj(AfDecimal impbIsEfflqObj) {
        if (impbIsEfflqObj != null) {
            setImpbIsEfflq(new AfDecimal(impbIsEfflqObj, 15, 3));
            ws.getIndDForzLiq().setImpbIsEfflq(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpbIsEfflq(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbRitAccCalc() {
        return dForzLiq.getDflImpbRitAccCalc().getDflImpbRitAccCalc();
    }

    @Override
    public void setImpbRitAccCalc(AfDecimal impbRitAccCalc) {
        this.dForzLiq.getDflImpbRitAccCalc().setDflImpbRitAccCalc(impbRitAccCalc.copy());
    }

    @Override
    public AfDecimal getImpbRitAccCalcObj() {
        if (ws.getIndDForzLiq().getImpbRitAccCalc() >= 0) {
            return getImpbRitAccCalc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbRitAccCalcObj(AfDecimal impbRitAccCalcObj) {
        if (impbRitAccCalcObj != null) {
            setImpbRitAccCalc(new AfDecimal(impbRitAccCalcObj, 15, 3));
            ws.getIndDForzLiq().setImpbRitAccCalc(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpbRitAccCalc(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbRitAccDfz() {
        return dForzLiq.getDflImpbRitAccDfz().getDflImpbRitAccDfz();
    }

    @Override
    public void setImpbRitAccDfz(AfDecimal impbRitAccDfz) {
        this.dForzLiq.getDflImpbRitAccDfz().setDflImpbRitAccDfz(impbRitAccDfz.copy());
    }

    @Override
    public AfDecimal getImpbRitAccDfzObj() {
        if (ws.getIndDForzLiq().getImpbRitAccDfz() >= 0) {
            return getImpbRitAccDfz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbRitAccDfzObj(AfDecimal impbRitAccDfzObj) {
        if (impbRitAccDfzObj != null) {
            setImpbRitAccDfz(new AfDecimal(impbRitAccDfzObj, 15, 3));
            ws.getIndDForzLiq().setImpbRitAccDfz(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpbRitAccDfz(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbRitAccEfflq() {
        return dForzLiq.getDflImpbRitAccEfflq().getDflImpbRitAccEfflq();
    }

    @Override
    public void setImpbRitAccEfflq(AfDecimal impbRitAccEfflq) {
        this.dForzLiq.getDflImpbRitAccEfflq().setDflImpbRitAccEfflq(impbRitAccEfflq.copy());
    }

    @Override
    public AfDecimal getImpbRitAccEfflqObj() {
        if (ws.getIndDForzLiq().getImpbRitAccEfflq() >= 0) {
            return getImpbRitAccEfflq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbRitAccEfflqObj(AfDecimal impbRitAccEfflqObj) {
        if (impbRitAccEfflqObj != null) {
            setImpbRitAccEfflq(new AfDecimal(impbRitAccEfflqObj, 15, 3));
            ws.getIndDForzLiq().setImpbRitAccEfflq(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpbRitAccEfflq(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbTaxSepCalc() {
        return dForzLiq.getDflImpbTaxSepCalc().getDflImpbTaxSepCalc();
    }

    @Override
    public void setImpbTaxSepCalc(AfDecimal impbTaxSepCalc) {
        this.dForzLiq.getDflImpbTaxSepCalc().setDflImpbTaxSepCalc(impbTaxSepCalc.copy());
    }

    @Override
    public AfDecimal getImpbTaxSepCalcObj() {
        if (ws.getIndDForzLiq().getImpbTaxSepCalc() >= 0) {
            return getImpbTaxSepCalc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbTaxSepCalcObj(AfDecimal impbTaxSepCalcObj) {
        if (impbTaxSepCalcObj != null) {
            setImpbTaxSepCalc(new AfDecimal(impbTaxSepCalcObj, 15, 3));
            ws.getIndDForzLiq().setImpbTaxSepCalc(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpbTaxSepCalc(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbTaxSepDfz() {
        return dForzLiq.getDflImpbTaxSepDfz().getDflImpbTaxSepDfz();
    }

    @Override
    public void setImpbTaxSepDfz(AfDecimal impbTaxSepDfz) {
        this.dForzLiq.getDflImpbTaxSepDfz().setDflImpbTaxSepDfz(impbTaxSepDfz.copy());
    }

    @Override
    public AfDecimal getImpbTaxSepDfzObj() {
        if (ws.getIndDForzLiq().getImpbTaxSepDfz() >= 0) {
            return getImpbTaxSepDfz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbTaxSepDfzObj(AfDecimal impbTaxSepDfzObj) {
        if (impbTaxSepDfzObj != null) {
            setImpbTaxSepDfz(new AfDecimal(impbTaxSepDfzObj, 15, 3));
            ws.getIndDForzLiq().setImpbTaxSepDfz(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpbTaxSepDfz(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbTaxSepEfflq() {
        return dForzLiq.getDflImpbTaxSepEfflq().getDflImpbTaxSepEfflq();
    }

    @Override
    public void setImpbTaxSepEfflq(AfDecimal impbTaxSepEfflq) {
        this.dForzLiq.getDflImpbTaxSepEfflq().setDflImpbTaxSepEfflq(impbTaxSepEfflq.copy());
    }

    @Override
    public AfDecimal getImpbTaxSepEfflqObj() {
        if (ws.getIndDForzLiq().getImpbTaxSepEfflq() >= 0) {
            return getImpbTaxSepEfflq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbTaxSepEfflqObj(AfDecimal impbTaxSepEfflqObj) {
        if (impbTaxSepEfflqObj != null) {
            setImpbTaxSepEfflq(new AfDecimal(impbTaxSepEfflqObj, 15, 3));
            ws.getIndDForzLiq().setImpbTaxSepEfflq(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpbTaxSepEfflq(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbTfrCalc() {
        return dForzLiq.getDflImpbTfrCalc().getDflImpbTfrCalc();
    }

    @Override
    public void setImpbTfrCalc(AfDecimal impbTfrCalc) {
        this.dForzLiq.getDflImpbTfrCalc().setDflImpbTfrCalc(impbTfrCalc.copy());
    }

    @Override
    public AfDecimal getImpbTfrCalcObj() {
        if (ws.getIndDForzLiq().getImpbTfrCalc() >= 0) {
            return getImpbTfrCalc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbTfrCalcObj(AfDecimal impbTfrCalcObj) {
        if (impbTfrCalcObj != null) {
            setImpbTfrCalc(new AfDecimal(impbTfrCalcObj, 15, 3));
            ws.getIndDForzLiq().setImpbTfrCalc(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpbTfrCalc(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbTfrDfz() {
        return dForzLiq.getDflImpbTfrDfz().getDflImpbTfrDfz();
    }

    @Override
    public void setImpbTfrDfz(AfDecimal impbTfrDfz) {
        this.dForzLiq.getDflImpbTfrDfz().setDflImpbTfrDfz(impbTfrDfz.copy());
    }

    @Override
    public AfDecimal getImpbTfrDfzObj() {
        if (ws.getIndDForzLiq().getImpbTfrDfz() >= 0) {
            return getImpbTfrDfz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbTfrDfzObj(AfDecimal impbTfrDfzObj) {
        if (impbTfrDfzObj != null) {
            setImpbTfrDfz(new AfDecimal(impbTfrDfzObj, 15, 3));
            ws.getIndDForzLiq().setImpbTfrDfz(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpbTfrDfz(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbTfrEfflq() {
        return dForzLiq.getDflImpbTfrEfflq().getDflImpbTfrEfflq();
    }

    @Override
    public void setImpbTfrEfflq(AfDecimal impbTfrEfflq) {
        this.dForzLiq.getDflImpbTfrEfflq().setDflImpbTfrEfflq(impbTfrEfflq.copy());
    }

    @Override
    public AfDecimal getImpbTfrEfflqObj() {
        if (ws.getIndDForzLiq().getImpbTfrEfflq() >= 0) {
            return getImpbTfrEfflq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbTfrEfflqObj(AfDecimal impbTfrEfflqObj) {
        if (impbTfrEfflqObj != null) {
            setImpbTfrEfflq(new AfDecimal(impbTfrEfflqObj, 15, 3));
            ws.getIndDForzLiq().setImpbTfrEfflq(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpbTfrEfflq(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbVis1382011c() {
        return dForzLiq.getDflImpbVis1382011c().getDflImpbVis1382011c();
    }

    @Override
    public void setImpbVis1382011c(AfDecimal impbVis1382011c) {
        this.dForzLiq.getDflImpbVis1382011c().setDflImpbVis1382011c(impbVis1382011c.copy());
    }

    @Override
    public AfDecimal getImpbVis1382011cObj() {
        if (ws.getIndDForzLiq().getImpbVis1382011c() >= 0) {
            return getImpbVis1382011c();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbVis1382011cObj(AfDecimal impbVis1382011cObj) {
        if (impbVis1382011cObj != null) {
            setImpbVis1382011c(new AfDecimal(impbVis1382011cObj, 15, 3));
            ws.getIndDForzLiq().setImpbVis1382011c(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpbVis1382011c(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbVis1382011d() {
        return dForzLiq.getDflImpbVis1382011d().getDflImpbVis1382011d();
    }

    @Override
    public void setImpbVis1382011d(AfDecimal impbVis1382011d) {
        this.dForzLiq.getDflImpbVis1382011d().setDflImpbVis1382011d(impbVis1382011d.copy());
    }

    @Override
    public AfDecimal getImpbVis1382011dObj() {
        if (ws.getIndDForzLiq().getImpbVis1382011d() >= 0) {
            return getImpbVis1382011d();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbVis1382011dObj(AfDecimal impbVis1382011dObj) {
        if (impbVis1382011dObj != null) {
            setImpbVis1382011d(new AfDecimal(impbVis1382011dObj, 15, 3));
            ws.getIndDForzLiq().setImpbVis1382011d(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpbVis1382011d(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbVis1382011l() {
        return dForzLiq.getDflImpbVis1382011l().getDflImpbVis1382011l();
    }

    @Override
    public void setImpbVis1382011l(AfDecimal impbVis1382011l) {
        this.dForzLiq.getDflImpbVis1382011l().setDflImpbVis1382011l(impbVis1382011l.copy());
    }

    @Override
    public AfDecimal getImpbVis1382011lObj() {
        if (ws.getIndDForzLiq().getImpbVis1382011l() >= 0) {
            return getImpbVis1382011l();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbVis1382011lObj(AfDecimal impbVis1382011lObj) {
        if (impbVis1382011lObj != null) {
            setImpbVis1382011l(new AfDecimal(impbVis1382011lObj, 15, 3));
            ws.getIndDForzLiq().setImpbVis1382011l(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpbVis1382011l(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbVis662014c() {
        return dForzLiq.getDflImpbVis662014c().getDflImpbVis662014c();
    }

    @Override
    public void setImpbVis662014c(AfDecimal impbVis662014c) {
        this.dForzLiq.getDflImpbVis662014c().setDflImpbVis662014c(impbVis662014c.copy());
    }

    @Override
    public AfDecimal getImpbVis662014cObj() {
        if (ws.getIndDForzLiq().getImpbVis662014c() >= 0) {
            return getImpbVis662014c();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbVis662014cObj(AfDecimal impbVis662014cObj) {
        if (impbVis662014cObj != null) {
            setImpbVis662014c(new AfDecimal(impbVis662014cObj, 15, 3));
            ws.getIndDForzLiq().setImpbVis662014c(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpbVis662014c(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbVis662014d() {
        return dForzLiq.getDflImpbVis662014d().getDflImpbVis662014d();
    }

    @Override
    public void setImpbVis662014d(AfDecimal impbVis662014d) {
        this.dForzLiq.getDflImpbVis662014d().setDflImpbVis662014d(impbVis662014d.copy());
    }

    @Override
    public AfDecimal getImpbVis662014dObj() {
        if (ws.getIndDForzLiq().getImpbVis662014d() >= 0) {
            return getImpbVis662014d();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbVis662014dObj(AfDecimal impbVis662014dObj) {
        if (impbVis662014dObj != null) {
            setImpbVis662014d(new AfDecimal(impbVis662014dObj, 15, 3));
            ws.getIndDForzLiq().setImpbVis662014d(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpbVis662014d(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbVis662014l() {
        return dForzLiq.getDflImpbVis662014l().getDflImpbVis662014l();
    }

    @Override
    public void setImpbVis662014l(AfDecimal impbVis662014l) {
        this.dForzLiq.getDflImpbVis662014l().setDflImpbVis662014l(impbVis662014l.copy());
    }

    @Override
    public AfDecimal getImpbVis662014lObj() {
        if (ws.getIndDForzLiq().getImpbVis662014l() >= 0) {
            return getImpbVis662014l();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbVis662014lObj(AfDecimal impbVis662014lObj) {
        if (impbVis662014lObj != null) {
            setImpbVis662014l(new AfDecimal(impbVis662014lObj, 15, 3));
            ws.getIndDForzLiq().setImpbVis662014l(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpbVis662014l(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbVisCalc() {
        return dForzLiq.getDflImpbVisCalc().getDflImpbVisCalc();
    }

    @Override
    public void setImpbVisCalc(AfDecimal impbVisCalc) {
        this.dForzLiq.getDflImpbVisCalc().setDflImpbVisCalc(impbVisCalc.copy());
    }

    @Override
    public AfDecimal getImpbVisCalcObj() {
        if (ws.getIndDForzLiq().getImpbVisCalc() >= 0) {
            return getImpbVisCalc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbVisCalcObj(AfDecimal impbVisCalcObj) {
        if (impbVisCalcObj != null) {
            setImpbVisCalc(new AfDecimal(impbVisCalcObj, 15, 3));
            ws.getIndDForzLiq().setImpbVisCalc(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpbVisCalc(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbVisDfz() {
        return dForzLiq.getDflImpbVisDfz().getDflImpbVisDfz();
    }

    @Override
    public void setImpbVisDfz(AfDecimal impbVisDfz) {
        this.dForzLiq.getDflImpbVisDfz().setDflImpbVisDfz(impbVisDfz.copy());
    }

    @Override
    public AfDecimal getImpbVisDfzObj() {
        if (ws.getIndDForzLiq().getImpbVisDfz() >= 0) {
            return getImpbVisDfz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbVisDfzObj(AfDecimal impbVisDfzObj) {
        if (impbVisDfzObj != null) {
            setImpbVisDfz(new AfDecimal(impbVisDfzObj, 15, 3));
            ws.getIndDForzLiq().setImpbVisDfz(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpbVisDfz(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbVisEfflq() {
        return dForzLiq.getDflImpbVisEfflq().getDflImpbVisEfflq();
    }

    @Override
    public void setImpbVisEfflq(AfDecimal impbVisEfflq) {
        this.dForzLiq.getDflImpbVisEfflq().setDflImpbVisEfflq(impbVisEfflq.copy());
    }

    @Override
    public AfDecimal getImpbVisEfflqObj() {
        if (ws.getIndDForzLiq().getImpbVisEfflq() >= 0) {
            return getImpbVisEfflq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbVisEfflqObj(AfDecimal impbVisEfflqObj) {
        if (impbVisEfflqObj != null) {
            setImpbVisEfflq(new AfDecimal(impbVisEfflqObj, 15, 3));
            ws.getIndDForzLiq().setImpbVisEfflq(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpbVisEfflq(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpst252Calc() {
        return dForzLiq.getDflImpst252Calc().getDflImpst252Calc();
    }

    @Override
    public void setImpst252Calc(AfDecimal impst252Calc) {
        this.dForzLiq.getDflImpst252Calc().setDflImpst252Calc(impst252Calc.copy());
    }

    @Override
    public AfDecimal getImpst252CalcObj() {
        if (ws.getIndDForzLiq().getImpst252Calc() >= 0) {
            return getImpst252Calc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpst252CalcObj(AfDecimal impst252CalcObj) {
        if (impst252CalcObj != null) {
            setImpst252Calc(new AfDecimal(impst252CalcObj, 15, 3));
            ws.getIndDForzLiq().setImpst252Calc(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpst252Calc(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpst252Efflq() {
        return dForzLiq.getDflImpst252Efflq().getDflImpst252Efflq();
    }

    @Override
    public void setImpst252Efflq(AfDecimal impst252Efflq) {
        this.dForzLiq.getDflImpst252Efflq().setDflImpst252Efflq(impst252Efflq.copy());
    }

    @Override
    public AfDecimal getImpst252EfflqObj() {
        if (ws.getIndDForzLiq().getImpst252Efflq() >= 0) {
            return getImpst252Efflq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpst252EfflqObj(AfDecimal impst252EfflqObj) {
        if (impst252EfflqObj != null) {
            setImpst252Efflq(new AfDecimal(impst252EfflqObj, 15, 3));
            ws.getIndDForzLiq().setImpst252Efflq(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpst252Efflq(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstBolloDettC() {
        return dForzLiq.getDflImpstBolloDettC().getDflImpstBolloDettC();
    }

    @Override
    public void setImpstBolloDettC(AfDecimal impstBolloDettC) {
        this.dForzLiq.getDflImpstBolloDettC().setDflImpstBolloDettC(impstBolloDettC.copy());
    }

    @Override
    public AfDecimal getImpstBolloDettCObj() {
        if (ws.getIndDForzLiq().getImpstBolloDettC() >= 0) {
            return getImpstBolloDettC();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstBolloDettCObj(AfDecimal impstBolloDettCObj) {
        if (impstBolloDettCObj != null) {
            setImpstBolloDettC(new AfDecimal(impstBolloDettCObj, 15, 3));
            ws.getIndDForzLiq().setImpstBolloDettC(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpstBolloDettC(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstBolloDettD() {
        return dForzLiq.getDflImpstBolloDettD().getDflImpstBolloDettD();
    }

    @Override
    public void setImpstBolloDettD(AfDecimal impstBolloDettD) {
        this.dForzLiq.getDflImpstBolloDettD().setDflImpstBolloDettD(impstBolloDettD.copy());
    }

    @Override
    public AfDecimal getImpstBolloDettDObj() {
        if (ws.getIndDForzLiq().getImpstBolloDettD() >= 0) {
            return getImpstBolloDettD();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstBolloDettDObj(AfDecimal impstBolloDettDObj) {
        if (impstBolloDettDObj != null) {
            setImpstBolloDettD(new AfDecimal(impstBolloDettDObj, 15, 3));
            ws.getIndDForzLiq().setImpstBolloDettD(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpstBolloDettD(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstBolloDettL() {
        return dForzLiq.getDflImpstBolloDettL().getDflImpstBolloDettL();
    }

    @Override
    public void setImpstBolloDettL(AfDecimal impstBolloDettL) {
        this.dForzLiq.getDflImpstBolloDettL().setDflImpstBolloDettL(impstBolloDettL.copy());
    }

    @Override
    public AfDecimal getImpstBolloDettLObj() {
        if (ws.getIndDForzLiq().getImpstBolloDettL() >= 0) {
            return getImpstBolloDettL();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstBolloDettLObj(AfDecimal impstBolloDettLObj) {
        if (impstBolloDettLObj != null) {
            setImpstBolloDettL(new AfDecimal(impstBolloDettLObj, 15, 3));
            ws.getIndDForzLiq().setImpstBolloDettL(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpstBolloDettL(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstBolloTotVc() {
        return dForzLiq.getDflImpstBolloTotVc().getDflImpstBolloTotVc();
    }

    @Override
    public void setImpstBolloTotVc(AfDecimal impstBolloTotVc) {
        this.dForzLiq.getDflImpstBolloTotVc().setDflImpstBolloTotVc(impstBolloTotVc.copy());
    }

    @Override
    public AfDecimal getImpstBolloTotVcObj() {
        if (ws.getIndDForzLiq().getImpstBolloTotVc() >= 0) {
            return getImpstBolloTotVc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstBolloTotVcObj(AfDecimal impstBolloTotVcObj) {
        if (impstBolloTotVcObj != null) {
            setImpstBolloTotVc(new AfDecimal(impstBolloTotVcObj, 15, 3));
            ws.getIndDForzLiq().setImpstBolloTotVc(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpstBolloTotVc(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstBolloTotVd() {
        return dForzLiq.getDflImpstBolloTotVd().getDflImpstBolloTotVd();
    }

    @Override
    public void setImpstBolloTotVd(AfDecimal impstBolloTotVd) {
        this.dForzLiq.getDflImpstBolloTotVd().setDflImpstBolloTotVd(impstBolloTotVd.copy());
    }

    @Override
    public AfDecimal getImpstBolloTotVdObj() {
        if (ws.getIndDForzLiq().getImpstBolloTotVd() >= 0) {
            return getImpstBolloTotVd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstBolloTotVdObj(AfDecimal impstBolloTotVdObj) {
        if (impstBolloTotVdObj != null) {
            setImpstBolloTotVd(new AfDecimal(impstBolloTotVdObj, 15, 3));
            ws.getIndDForzLiq().setImpstBolloTotVd(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpstBolloTotVd(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstBolloTotVl() {
        return dForzLiq.getDflImpstBolloTotVl().getDflImpstBolloTotVl();
    }

    @Override
    public void setImpstBolloTotVl(AfDecimal impstBolloTotVl) {
        this.dForzLiq.getDflImpstBolloTotVl().setDflImpstBolloTotVl(impstBolloTotVl.copy());
    }

    @Override
    public AfDecimal getImpstBolloTotVlObj() {
        if (ws.getIndDForzLiq().getImpstBolloTotVl() >= 0) {
            return getImpstBolloTotVl();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstBolloTotVlObj(AfDecimal impstBolloTotVlObj) {
        if (impstBolloTotVlObj != null) {
            setImpstBolloTotVl(new AfDecimal(impstBolloTotVlObj, 15, 3));
            ws.getIndDForzLiq().setImpstBolloTotVl(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpstBolloTotVl(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstDaRimbEff() {
        return dForzLiq.getDflImpstDaRimbEff().getDflImpstDaRimbEff();
    }

    @Override
    public void setImpstDaRimbEff(AfDecimal impstDaRimbEff) {
        this.dForzLiq.getDflImpstDaRimbEff().setDflImpstDaRimbEff(impstDaRimbEff.copy());
    }

    @Override
    public AfDecimal getImpstDaRimbEffObj() {
        if (ws.getIndDForzLiq().getImpstDaRimbEff() >= 0) {
            return getImpstDaRimbEff();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstDaRimbEffObj(AfDecimal impstDaRimbEffObj) {
        if (impstDaRimbEffObj != null) {
            setImpstDaRimbEff(new AfDecimal(impstDaRimbEffObj, 15, 3));
            ws.getIndDForzLiq().setImpstDaRimbEff(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpstDaRimbEff(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstPrvrCalc() {
        return dForzLiq.getDflImpstPrvrCalc().getDflImpstPrvrCalc();
    }

    @Override
    public void setImpstPrvrCalc(AfDecimal impstPrvrCalc) {
        this.dForzLiq.getDflImpstPrvrCalc().setDflImpstPrvrCalc(impstPrvrCalc.copy());
    }

    @Override
    public AfDecimal getImpstPrvrCalcObj() {
        if (ws.getIndDForzLiq().getImpstPrvrCalc() >= 0) {
            return getImpstPrvrCalc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstPrvrCalcObj(AfDecimal impstPrvrCalcObj) {
        if (impstPrvrCalcObj != null) {
            setImpstPrvrCalc(new AfDecimal(impstPrvrCalcObj, 15, 3));
            ws.getIndDForzLiq().setImpstPrvrCalc(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpstPrvrCalc(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstPrvrDfz() {
        return dForzLiq.getDflImpstPrvrDfz().getDflImpstPrvrDfz();
    }

    @Override
    public void setImpstPrvrDfz(AfDecimal impstPrvrDfz) {
        this.dForzLiq.getDflImpstPrvrDfz().setDflImpstPrvrDfz(impstPrvrDfz.copy());
    }

    @Override
    public AfDecimal getImpstPrvrDfzObj() {
        if (ws.getIndDForzLiq().getImpstPrvrDfz() >= 0) {
            return getImpstPrvrDfz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstPrvrDfzObj(AfDecimal impstPrvrDfzObj) {
        if (impstPrvrDfzObj != null) {
            setImpstPrvrDfz(new AfDecimal(impstPrvrDfzObj, 15, 3));
            ws.getIndDForzLiq().setImpstPrvrDfz(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpstPrvrDfz(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstPrvrEfflq() {
        return dForzLiq.getDflImpstPrvrEfflq().getDflImpstPrvrEfflq();
    }

    @Override
    public void setImpstPrvrEfflq(AfDecimal impstPrvrEfflq) {
        this.dForzLiq.getDflImpstPrvrEfflq().setDflImpstPrvrEfflq(impstPrvrEfflq.copy());
    }

    @Override
    public AfDecimal getImpstPrvrEfflqObj() {
        if (ws.getIndDForzLiq().getImpstPrvrEfflq() >= 0) {
            return getImpstPrvrEfflq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstPrvrEfflqObj(AfDecimal impstPrvrEfflqObj) {
        if (impstPrvrEfflqObj != null) {
            setImpstPrvrEfflq(new AfDecimal(impstPrvrEfflqObj, 15, 3));
            ws.getIndDForzLiq().setImpstPrvrEfflq(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpstPrvrEfflq(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstSostCalc() {
        return dForzLiq.getDflImpstSostCalc().getDflImpstSostCalc();
    }

    @Override
    public void setImpstSostCalc(AfDecimal impstSostCalc) {
        this.dForzLiq.getDflImpstSostCalc().setDflImpstSostCalc(impstSostCalc.copy());
    }

    @Override
    public AfDecimal getImpstSostCalcObj() {
        if (ws.getIndDForzLiq().getImpstSostCalc() >= 0) {
            return getImpstSostCalc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstSostCalcObj(AfDecimal impstSostCalcObj) {
        if (impstSostCalcObj != null) {
            setImpstSostCalc(new AfDecimal(impstSostCalcObj, 15, 3));
            ws.getIndDForzLiq().setImpstSostCalc(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpstSostCalc(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstSostDfz() {
        return dForzLiq.getDflImpstSostDfz().getDflImpstSostDfz();
    }

    @Override
    public void setImpstSostDfz(AfDecimal impstSostDfz) {
        this.dForzLiq.getDflImpstSostDfz().setDflImpstSostDfz(impstSostDfz.copy());
    }

    @Override
    public AfDecimal getImpstSostDfzObj() {
        if (ws.getIndDForzLiq().getImpstSostDfz() >= 0) {
            return getImpstSostDfz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstSostDfzObj(AfDecimal impstSostDfzObj) {
        if (impstSostDfzObj != null) {
            setImpstSostDfz(new AfDecimal(impstSostDfzObj, 15, 3));
            ws.getIndDForzLiq().setImpstSostDfz(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpstSostDfz(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstSostEfflq() {
        return dForzLiq.getDflImpstSostEfflq().getDflImpstSostEfflq();
    }

    @Override
    public void setImpstSostEfflq(AfDecimal impstSostEfflq) {
        this.dForzLiq.getDflImpstSostEfflq().setDflImpstSostEfflq(impstSostEfflq.copy());
    }

    @Override
    public AfDecimal getImpstSostEfflqObj() {
        if (ws.getIndDForzLiq().getImpstSostEfflq() >= 0) {
            return getImpstSostEfflq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstSostEfflqObj(AfDecimal impstSostEfflqObj) {
        if (impstSostEfflqObj != null) {
            setImpstSostEfflq(new AfDecimal(impstSostEfflqObj, 15, 3));
            ws.getIndDForzLiq().setImpstSostEfflq(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpstSostEfflq(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstVis1382011c() {
        return dForzLiq.getDflImpstVis1382011c().getDflImpstVis1382011c();
    }

    @Override
    public void setImpstVis1382011c(AfDecimal impstVis1382011c) {
        this.dForzLiq.getDflImpstVis1382011c().setDflImpstVis1382011c(impstVis1382011c.copy());
    }

    @Override
    public AfDecimal getImpstVis1382011cObj() {
        if (ws.getIndDForzLiq().getImpstVis1382011c() >= 0) {
            return getImpstVis1382011c();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstVis1382011cObj(AfDecimal impstVis1382011cObj) {
        if (impstVis1382011cObj != null) {
            setImpstVis1382011c(new AfDecimal(impstVis1382011cObj, 15, 3));
            ws.getIndDForzLiq().setImpstVis1382011c(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpstVis1382011c(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstVis1382011d() {
        return dForzLiq.getDflImpstVis1382011d().getDflImpstVis1382011d();
    }

    @Override
    public void setImpstVis1382011d(AfDecimal impstVis1382011d) {
        this.dForzLiq.getDflImpstVis1382011d().setDflImpstVis1382011d(impstVis1382011d.copy());
    }

    @Override
    public AfDecimal getImpstVis1382011dObj() {
        if (ws.getIndDForzLiq().getImpstVis1382011d() >= 0) {
            return getImpstVis1382011d();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstVis1382011dObj(AfDecimal impstVis1382011dObj) {
        if (impstVis1382011dObj != null) {
            setImpstVis1382011d(new AfDecimal(impstVis1382011dObj, 15, 3));
            ws.getIndDForzLiq().setImpstVis1382011d(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpstVis1382011d(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstVis1382011l() {
        return dForzLiq.getDflImpstVis1382011l().getDflImpstVis1382011l();
    }

    @Override
    public void setImpstVis1382011l(AfDecimal impstVis1382011l) {
        this.dForzLiq.getDflImpstVis1382011l().setDflImpstVis1382011l(impstVis1382011l.copy());
    }

    @Override
    public AfDecimal getImpstVis1382011lObj() {
        if (ws.getIndDForzLiq().getImpstVis1382011l() >= 0) {
            return getImpstVis1382011l();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstVis1382011lObj(AfDecimal impstVis1382011lObj) {
        if (impstVis1382011lObj != null) {
            setImpstVis1382011l(new AfDecimal(impstVis1382011lObj, 15, 3));
            ws.getIndDForzLiq().setImpstVis1382011l(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpstVis1382011l(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstVis662014c() {
        return dForzLiq.getDflImpstVis662014c().getDflImpstVis662014c();
    }

    @Override
    public void setImpstVis662014c(AfDecimal impstVis662014c) {
        this.dForzLiq.getDflImpstVis662014c().setDflImpstVis662014c(impstVis662014c.copy());
    }

    @Override
    public AfDecimal getImpstVis662014cObj() {
        if (ws.getIndDForzLiq().getImpstVis662014c() >= 0) {
            return getImpstVis662014c();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstVis662014cObj(AfDecimal impstVis662014cObj) {
        if (impstVis662014cObj != null) {
            setImpstVis662014c(new AfDecimal(impstVis662014cObj, 15, 3));
            ws.getIndDForzLiq().setImpstVis662014c(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpstVis662014c(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstVis662014d() {
        return dForzLiq.getDflImpstVis662014d().getDflImpstVis662014d();
    }

    @Override
    public void setImpstVis662014d(AfDecimal impstVis662014d) {
        this.dForzLiq.getDflImpstVis662014d().setDflImpstVis662014d(impstVis662014d.copy());
    }

    @Override
    public AfDecimal getImpstVis662014dObj() {
        if (ws.getIndDForzLiq().getImpstVis662014d() >= 0) {
            return getImpstVis662014d();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstVis662014dObj(AfDecimal impstVis662014dObj) {
        if (impstVis662014dObj != null) {
            setImpstVis662014d(new AfDecimal(impstVis662014dObj, 15, 3));
            ws.getIndDForzLiq().setImpstVis662014d(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpstVis662014d(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstVis662014l() {
        return dForzLiq.getDflImpstVis662014l().getDflImpstVis662014l();
    }

    @Override
    public void setImpstVis662014l(AfDecimal impstVis662014l) {
        this.dForzLiq.getDflImpstVis662014l().setDflImpstVis662014l(impstVis662014l.copy());
    }

    @Override
    public AfDecimal getImpstVis662014lObj() {
        if (ws.getIndDForzLiq().getImpstVis662014l() >= 0) {
            return getImpstVis662014l();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstVis662014lObj(AfDecimal impstVis662014lObj) {
        if (impstVis662014lObj != null) {
            setImpstVis662014l(new AfDecimal(impstVis662014lObj, 15, 3));
            ws.getIndDForzLiq().setImpstVis662014l(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpstVis662014l(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstVisCalc() {
        return dForzLiq.getDflImpstVisCalc().getDflImpstVisCalc();
    }

    @Override
    public void setImpstVisCalc(AfDecimal impstVisCalc) {
        this.dForzLiq.getDflImpstVisCalc().setDflImpstVisCalc(impstVisCalc.copy());
    }

    @Override
    public AfDecimal getImpstVisCalcObj() {
        if (ws.getIndDForzLiq().getImpstVisCalc() >= 0) {
            return getImpstVisCalc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstVisCalcObj(AfDecimal impstVisCalcObj) {
        if (impstVisCalcObj != null) {
            setImpstVisCalc(new AfDecimal(impstVisCalcObj, 15, 3));
            ws.getIndDForzLiq().setImpstVisCalc(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpstVisCalc(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstVisDfz() {
        return dForzLiq.getDflImpstVisDfz().getDflImpstVisDfz();
    }

    @Override
    public void setImpstVisDfz(AfDecimal impstVisDfz) {
        this.dForzLiq.getDflImpstVisDfz().setDflImpstVisDfz(impstVisDfz.copy());
    }

    @Override
    public AfDecimal getImpstVisDfzObj() {
        if (ws.getIndDForzLiq().getImpstVisDfz() >= 0) {
            return getImpstVisDfz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstVisDfzObj(AfDecimal impstVisDfzObj) {
        if (impstVisDfzObj != null) {
            setImpstVisDfz(new AfDecimal(impstVisDfzObj, 15, 3));
            ws.getIndDForzLiq().setImpstVisDfz(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpstVisDfz(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstVisEfflq() {
        return dForzLiq.getDflImpstVisEfflq().getDflImpstVisEfflq();
    }

    @Override
    public void setImpstVisEfflq(AfDecimal impstVisEfflq) {
        this.dForzLiq.getDflImpstVisEfflq().setDflImpstVisEfflq(impstVisEfflq.copy());
    }

    @Override
    public AfDecimal getImpstVisEfflqObj() {
        if (ws.getIndDForzLiq().getImpstVisEfflq() >= 0) {
            return getImpstVisEfflq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstVisEfflqObj(AfDecimal impstVisEfflqObj) {
        if (impstVisEfflqObj != null) {
            setImpstVisEfflq(new AfDecimal(impstVisEfflqObj, 15, 3));
            ws.getIndDForzLiq().setImpstVisEfflq(((short)0));
        }
        else {
            ws.getIndDForzLiq().setImpstVisEfflq(((short)-1));
        }
    }

    @Override
    public AfDecimal getIntrPrestCalc() {
        return dForzLiq.getDflIntrPrestCalc().getDflIntrPrestCalc();
    }

    @Override
    public void setIntrPrestCalc(AfDecimal intrPrestCalc) {
        this.dForzLiq.getDflIntrPrestCalc().setDflIntrPrestCalc(intrPrestCalc.copy());
    }

    @Override
    public AfDecimal getIntrPrestCalcObj() {
        if (ws.getIndDForzLiq().getIntrPrestCalc() >= 0) {
            return getIntrPrestCalc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIntrPrestCalcObj(AfDecimal intrPrestCalcObj) {
        if (intrPrestCalcObj != null) {
            setIntrPrestCalc(new AfDecimal(intrPrestCalcObj, 15, 3));
            ws.getIndDForzLiq().setIntrPrestCalc(((short)0));
        }
        else {
            ws.getIndDForzLiq().setIntrPrestCalc(((short)-1));
        }
    }

    @Override
    public AfDecimal getIntrPrestDfz() {
        return dForzLiq.getDflIntrPrestDfz().getDflIntrPrestDfz();
    }

    @Override
    public void setIntrPrestDfz(AfDecimal intrPrestDfz) {
        this.dForzLiq.getDflIntrPrestDfz().setDflIntrPrestDfz(intrPrestDfz.copy());
    }

    @Override
    public AfDecimal getIntrPrestDfzObj() {
        if (ws.getIndDForzLiq().getIntrPrestDfz() >= 0) {
            return getIntrPrestDfz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIntrPrestDfzObj(AfDecimal intrPrestDfzObj) {
        if (intrPrestDfzObj != null) {
            setIntrPrestDfz(new AfDecimal(intrPrestDfzObj, 15, 3));
            ws.getIndDForzLiq().setIntrPrestDfz(((short)0));
        }
        else {
            ws.getIndDForzLiq().setIntrPrestDfz(((short)-1));
        }
    }

    @Override
    public AfDecimal getIntrPrestEfflq() {
        return dForzLiq.getDflIntrPrestEfflq().getDflIntrPrestEfflq();
    }

    @Override
    public void setIntrPrestEfflq(AfDecimal intrPrestEfflq) {
        this.dForzLiq.getDflIntrPrestEfflq().setDflIntrPrestEfflq(intrPrestEfflq.copy());
    }

    @Override
    public AfDecimal getIntrPrestEfflqObj() {
        if (ws.getIndDForzLiq().getIntrPrestEfflq() >= 0) {
            return getIntrPrestEfflq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIntrPrestEfflqObj(AfDecimal intrPrestEfflqObj) {
        if (intrPrestEfflqObj != null) {
            setIntrPrestEfflq(new AfDecimal(intrPrestEfflqObj, 15, 3));
            ws.getIndDForzLiq().setIntrPrestEfflq(((short)0));
        }
        else {
            ws.getIndDForzLiq().setIntrPrestEfflq(((short)-1));
        }
    }

    @Override
    public AfDecimal getIs1382011c() {
        return dForzLiq.getDflIs1382011c().getDflIs1382011c();
    }

    @Override
    public void setIs1382011c(AfDecimal is1382011c) {
        this.dForzLiq.getDflIs1382011c().setDflIs1382011c(is1382011c.copy());
    }

    @Override
    public AfDecimal getIs1382011cObj() {
        if (ws.getIndDForzLiq().getIs1382011c() >= 0) {
            return getIs1382011c();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIs1382011cObj(AfDecimal is1382011cObj) {
        if (is1382011cObj != null) {
            setIs1382011c(new AfDecimal(is1382011cObj, 15, 3));
            ws.getIndDForzLiq().setIs1382011c(((short)0));
        }
        else {
            ws.getIndDForzLiq().setIs1382011c(((short)-1));
        }
    }

    @Override
    public AfDecimal getIs1382011d() {
        return dForzLiq.getDflIs1382011d().getDflIs1382011d();
    }

    @Override
    public void setIs1382011d(AfDecimal is1382011d) {
        this.dForzLiq.getDflIs1382011d().setDflIs1382011d(is1382011d.copy());
    }

    @Override
    public AfDecimal getIs1382011dObj() {
        if (ws.getIndDForzLiq().getIs1382011d() >= 0) {
            return getIs1382011d();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIs1382011dObj(AfDecimal is1382011dObj) {
        if (is1382011dObj != null) {
            setIs1382011d(new AfDecimal(is1382011dObj, 15, 3));
            ws.getIndDForzLiq().setIs1382011d(((short)0));
        }
        else {
            ws.getIndDForzLiq().setIs1382011d(((short)-1));
        }
    }

    @Override
    public AfDecimal getIs1382011l() {
        return dForzLiq.getDflIs1382011l().getDflIs1382011l();
    }

    @Override
    public void setIs1382011l(AfDecimal is1382011l) {
        this.dForzLiq.getDflIs1382011l().setDflIs1382011l(is1382011l.copy());
    }

    @Override
    public AfDecimal getIs1382011lObj() {
        if (ws.getIndDForzLiq().getIs1382011l() >= 0) {
            return getIs1382011l();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIs1382011lObj(AfDecimal is1382011lObj) {
        if (is1382011lObj != null) {
            setIs1382011l(new AfDecimal(is1382011lObj, 15, 3));
            ws.getIndDForzLiq().setIs1382011l(((short)0));
        }
        else {
            ws.getIndDForzLiq().setIs1382011l(((short)-1));
        }
    }

    @Override
    public AfDecimal getIs662014c() {
        return dForzLiq.getDflIs662014c().getDflIs662014c();
    }

    @Override
    public void setIs662014c(AfDecimal is662014c) {
        this.dForzLiq.getDflIs662014c().setDflIs662014c(is662014c.copy());
    }

    @Override
    public AfDecimal getIs662014cObj() {
        if (ws.getIndDForzLiq().getIs662014c() >= 0) {
            return getIs662014c();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIs662014cObj(AfDecimal is662014cObj) {
        if (is662014cObj != null) {
            setIs662014c(new AfDecimal(is662014cObj, 15, 3));
            ws.getIndDForzLiq().setIs662014c(((short)0));
        }
        else {
            ws.getIndDForzLiq().setIs662014c(((short)-1));
        }
    }

    @Override
    public AfDecimal getIs662014d() {
        return dForzLiq.getDflIs662014d().getDflIs662014d();
    }

    @Override
    public void setIs662014d(AfDecimal is662014d) {
        this.dForzLiq.getDflIs662014d().setDflIs662014d(is662014d.copy());
    }

    @Override
    public AfDecimal getIs662014dObj() {
        if (ws.getIndDForzLiq().getIs662014d() >= 0) {
            return getIs662014d();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIs662014dObj(AfDecimal is662014dObj) {
        if (is662014dObj != null) {
            setIs662014d(new AfDecimal(is662014dObj, 15, 3));
            ws.getIndDForzLiq().setIs662014d(((short)0));
        }
        else {
            ws.getIndDForzLiq().setIs662014d(((short)-1));
        }
    }

    @Override
    public AfDecimal getIs662014l() {
        return dForzLiq.getDflIs662014l().getDflIs662014l();
    }

    @Override
    public void setIs662014l(AfDecimal is662014l) {
        this.dForzLiq.getDflIs662014l().setDflIs662014l(is662014l.copy());
    }

    @Override
    public AfDecimal getIs662014lObj() {
        if (ws.getIndDForzLiq().getIs662014l() >= 0) {
            return getIs662014l();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIs662014lObj(AfDecimal is662014lObj) {
        if (is662014lObj != null) {
            setIs662014l(new AfDecimal(is662014lObj, 15, 3));
            ws.getIndDForzLiq().setIs662014l(((short)0));
        }
        else {
            ws.getIndDForzLiq().setIs662014l(((short)-1));
        }
    }

    @Override
    public int getMmCnbzDal2007Ef() {
        return dForzLiq.getDflMmCnbzDal2007Ef().getDflMmCnbzDal2007Ef();
    }

    @Override
    public void setMmCnbzDal2007Ef(int mmCnbzDal2007Ef) {
        this.dForzLiq.getDflMmCnbzDal2007Ef().setDflMmCnbzDal2007Ef(mmCnbzDal2007Ef);
    }

    @Override
    public Integer getMmCnbzDal2007EfObj() {
        if (ws.getIndDForzLiq().getMmCnbzDal2007Ef() >= 0) {
            return ((Integer)getMmCnbzDal2007Ef());
        }
        else {
            return null;
        }
    }

    @Override
    public void setMmCnbzDal2007EfObj(Integer mmCnbzDal2007EfObj) {
        if (mmCnbzDal2007EfObj != null) {
            setMmCnbzDal2007Ef(((int)mmCnbzDal2007EfObj));
            ws.getIndDForzLiq().setMmCnbzDal2007Ef(((short)0));
        }
        else {
            ws.getIndDForzLiq().setMmCnbzDal2007Ef(((short)-1));
        }
    }

    @Override
    public int getMmCnbzEnd2000Ef() {
        return dForzLiq.getDflMmCnbzEnd2000Ef().getDflMmCnbzEnd2000Ef();
    }

    @Override
    public void setMmCnbzEnd2000Ef(int mmCnbzEnd2000Ef) {
        this.dForzLiq.getDflMmCnbzEnd2000Ef().setDflMmCnbzEnd2000Ef(mmCnbzEnd2000Ef);
    }

    @Override
    public Integer getMmCnbzEnd2000EfObj() {
        if (ws.getIndDForzLiq().getMmCnbzEnd2000Ef() >= 0) {
            return ((Integer)getMmCnbzEnd2000Ef());
        }
        else {
            return null;
        }
    }

    @Override
    public void setMmCnbzEnd2000EfObj(Integer mmCnbzEnd2000EfObj) {
        if (mmCnbzEnd2000EfObj != null) {
            setMmCnbzEnd2000Ef(((int)mmCnbzEnd2000EfObj));
            ws.getIndDForzLiq().setMmCnbzEnd2000Ef(((short)0));
        }
        else {
            ws.getIndDForzLiq().setMmCnbzEnd2000Ef(((short)-1));
        }
    }

    @Override
    public int getMmCnbzEnd2006Ef() {
        return dForzLiq.getDflMmCnbzEnd2006Ef().getDflMmCnbzEnd2006Ef();
    }

    @Override
    public void setMmCnbzEnd2006Ef(int mmCnbzEnd2006Ef) {
        this.dForzLiq.getDflMmCnbzEnd2006Ef().setDflMmCnbzEnd2006Ef(mmCnbzEnd2006Ef);
    }

    @Override
    public Integer getMmCnbzEnd2006EfObj() {
        if (ws.getIndDForzLiq().getMmCnbzEnd2006Ef() >= 0) {
            return ((Integer)getMmCnbzEnd2006Ef());
        }
        else {
            return null;
        }
    }

    @Override
    public void setMmCnbzEnd2006EfObj(Integer mmCnbzEnd2006EfObj) {
        if (mmCnbzEnd2006EfObj != null) {
            setMmCnbzEnd2006Ef(((int)mmCnbzEnd2006EfObj));
            ws.getIndDForzLiq().setMmCnbzEnd2006Ef(((short)0));
        }
        else {
            ws.getIndDForzLiq().setMmCnbzEnd2006Ef(((short)-1));
        }
    }

    @Override
    public AfDecimal getMontDal2007Calc() {
        return dForzLiq.getDflMontDal2007Calc().getDflMontDal2007Calc();
    }

    @Override
    public void setMontDal2007Calc(AfDecimal montDal2007Calc) {
        this.dForzLiq.getDflMontDal2007Calc().setDflMontDal2007Calc(montDal2007Calc.copy());
    }

    @Override
    public AfDecimal getMontDal2007CalcObj() {
        if (ws.getIndDForzLiq().getMontDal2007Calc() >= 0) {
            return getMontDal2007Calc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setMontDal2007CalcObj(AfDecimal montDal2007CalcObj) {
        if (montDal2007CalcObj != null) {
            setMontDal2007Calc(new AfDecimal(montDal2007CalcObj, 15, 3));
            ws.getIndDForzLiq().setMontDal2007Calc(((short)0));
        }
        else {
            ws.getIndDForzLiq().setMontDal2007Calc(((short)-1));
        }
    }

    @Override
    public AfDecimal getMontDal2007Dfz() {
        return dForzLiq.getDflMontDal2007Dfz().getDflMontDal2007Dfz();
    }

    @Override
    public void setMontDal2007Dfz(AfDecimal montDal2007Dfz) {
        this.dForzLiq.getDflMontDal2007Dfz().setDflMontDal2007Dfz(montDal2007Dfz.copy());
    }

    @Override
    public AfDecimal getMontDal2007DfzObj() {
        if (ws.getIndDForzLiq().getMontDal2007Dfz() >= 0) {
            return getMontDal2007Dfz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setMontDal2007DfzObj(AfDecimal montDal2007DfzObj) {
        if (montDal2007DfzObj != null) {
            setMontDal2007Dfz(new AfDecimal(montDal2007DfzObj, 15, 3));
            ws.getIndDForzLiq().setMontDal2007Dfz(((short)0));
        }
        else {
            ws.getIndDForzLiq().setMontDal2007Dfz(((short)-1));
        }
    }

    @Override
    public AfDecimal getMontDal2007Efflq() {
        return dForzLiq.getDflMontDal2007Efflq().getDflMontDal2007Efflq();
    }

    @Override
    public void setMontDal2007Efflq(AfDecimal montDal2007Efflq) {
        this.dForzLiq.getDflMontDal2007Efflq().setDflMontDal2007Efflq(montDal2007Efflq.copy());
    }

    @Override
    public AfDecimal getMontDal2007EfflqObj() {
        if (ws.getIndDForzLiq().getMontDal2007Efflq() >= 0) {
            return getMontDal2007Efflq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setMontDal2007EfflqObj(AfDecimal montDal2007EfflqObj) {
        if (montDal2007EfflqObj != null) {
            setMontDal2007Efflq(new AfDecimal(montDal2007EfflqObj, 15, 3));
            ws.getIndDForzLiq().setMontDal2007Efflq(((short)0));
        }
        else {
            ws.getIndDForzLiq().setMontDal2007Efflq(((short)-1));
        }
    }

    @Override
    public AfDecimal getMontEnd2000Calc() {
        return dForzLiq.getDflMontEnd2000Calc().getDflMontEnd2000Calc();
    }

    @Override
    public void setMontEnd2000Calc(AfDecimal montEnd2000Calc) {
        this.dForzLiq.getDflMontEnd2000Calc().setDflMontEnd2000Calc(montEnd2000Calc.copy());
    }

    @Override
    public AfDecimal getMontEnd2000CalcObj() {
        if (ws.getIndDForzLiq().getMontEnd2000Calc() >= 0) {
            return getMontEnd2000Calc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setMontEnd2000CalcObj(AfDecimal montEnd2000CalcObj) {
        if (montEnd2000CalcObj != null) {
            setMontEnd2000Calc(new AfDecimal(montEnd2000CalcObj, 15, 3));
            ws.getIndDForzLiq().setMontEnd2000Calc(((short)0));
        }
        else {
            ws.getIndDForzLiq().setMontEnd2000Calc(((short)-1));
        }
    }

    @Override
    public AfDecimal getMontEnd2000Dfz() {
        return dForzLiq.getDflMontEnd2000Dfz().getDflMontEnd2000Dfz();
    }

    @Override
    public void setMontEnd2000Dfz(AfDecimal montEnd2000Dfz) {
        this.dForzLiq.getDflMontEnd2000Dfz().setDflMontEnd2000Dfz(montEnd2000Dfz.copy());
    }

    @Override
    public AfDecimal getMontEnd2000DfzObj() {
        if (ws.getIndDForzLiq().getMontEnd2000Dfz() >= 0) {
            return getMontEnd2000Dfz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setMontEnd2000DfzObj(AfDecimal montEnd2000DfzObj) {
        if (montEnd2000DfzObj != null) {
            setMontEnd2000Dfz(new AfDecimal(montEnd2000DfzObj, 15, 3));
            ws.getIndDForzLiq().setMontEnd2000Dfz(((short)0));
        }
        else {
            ws.getIndDForzLiq().setMontEnd2000Dfz(((short)-1));
        }
    }

    @Override
    public AfDecimal getMontEnd2000Efflq() {
        return dForzLiq.getDflMontEnd2000Efflq().getDflMontEnd2000Efflq();
    }

    @Override
    public void setMontEnd2000Efflq(AfDecimal montEnd2000Efflq) {
        this.dForzLiq.getDflMontEnd2000Efflq().setDflMontEnd2000Efflq(montEnd2000Efflq.copy());
    }

    @Override
    public AfDecimal getMontEnd2000EfflqObj() {
        if (ws.getIndDForzLiq().getMontEnd2000Efflq() >= 0) {
            return getMontEnd2000Efflq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setMontEnd2000EfflqObj(AfDecimal montEnd2000EfflqObj) {
        if (montEnd2000EfflqObj != null) {
            setMontEnd2000Efflq(new AfDecimal(montEnd2000EfflqObj, 15, 3));
            ws.getIndDForzLiq().setMontEnd2000Efflq(((short)0));
        }
        else {
            ws.getIndDForzLiq().setMontEnd2000Efflq(((short)-1));
        }
    }

    @Override
    public AfDecimal getMontEnd2006Calc() {
        return dForzLiq.getDflMontEnd2006Calc().getDflMontEnd2006Calc();
    }

    @Override
    public void setMontEnd2006Calc(AfDecimal montEnd2006Calc) {
        this.dForzLiq.getDflMontEnd2006Calc().setDflMontEnd2006Calc(montEnd2006Calc.copy());
    }

    @Override
    public AfDecimal getMontEnd2006CalcObj() {
        if (ws.getIndDForzLiq().getMontEnd2006Calc() >= 0) {
            return getMontEnd2006Calc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setMontEnd2006CalcObj(AfDecimal montEnd2006CalcObj) {
        if (montEnd2006CalcObj != null) {
            setMontEnd2006Calc(new AfDecimal(montEnd2006CalcObj, 15, 3));
            ws.getIndDForzLiq().setMontEnd2006Calc(((short)0));
        }
        else {
            ws.getIndDForzLiq().setMontEnd2006Calc(((short)-1));
        }
    }

    @Override
    public AfDecimal getMontEnd2006Dfz() {
        return dForzLiq.getDflMontEnd2006Dfz().getDflMontEnd2006Dfz();
    }

    @Override
    public void setMontEnd2006Dfz(AfDecimal montEnd2006Dfz) {
        this.dForzLiq.getDflMontEnd2006Dfz().setDflMontEnd2006Dfz(montEnd2006Dfz.copy());
    }

    @Override
    public AfDecimal getMontEnd2006DfzObj() {
        if (ws.getIndDForzLiq().getMontEnd2006Dfz() >= 0) {
            return getMontEnd2006Dfz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setMontEnd2006DfzObj(AfDecimal montEnd2006DfzObj) {
        if (montEnd2006DfzObj != null) {
            setMontEnd2006Dfz(new AfDecimal(montEnd2006DfzObj, 15, 3));
            ws.getIndDForzLiq().setMontEnd2006Dfz(((short)0));
        }
        else {
            ws.getIndDForzLiq().setMontEnd2006Dfz(((short)-1));
        }
    }

    @Override
    public AfDecimal getMontEnd2006Efflq() {
        return dForzLiq.getDflMontEnd2006Efflq().getDflMontEnd2006Efflq();
    }

    @Override
    public void setMontEnd2006Efflq(AfDecimal montEnd2006Efflq) {
        this.dForzLiq.getDflMontEnd2006Efflq().setDflMontEnd2006Efflq(montEnd2006Efflq.copy());
    }

    @Override
    public AfDecimal getMontEnd2006EfflqObj() {
        if (ws.getIndDForzLiq().getMontEnd2006Efflq() >= 0) {
            return getMontEnd2006Efflq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setMontEnd2006EfflqObj(AfDecimal montEnd2006EfflqObj) {
        if (montEnd2006EfflqObj != null) {
            setMontEnd2006Efflq(new AfDecimal(montEnd2006EfflqObj, 15, 3));
            ws.getIndDForzLiq().setMontEnd2006Efflq(((short)0));
        }
        else {
            ws.getIndDForzLiq().setMontEnd2006Efflq(((short)-1));
        }
    }

    @Override
    public AfDecimal getResPreAttCalc() {
        return dForzLiq.getDflResPreAttCalc().getDflResPreAttCalc();
    }

    @Override
    public void setResPreAttCalc(AfDecimal resPreAttCalc) {
        this.dForzLiq.getDflResPreAttCalc().setDflResPreAttCalc(resPreAttCalc.copy());
    }

    @Override
    public AfDecimal getResPreAttCalcObj() {
        if (ws.getIndDForzLiq().getResPreAttCalc() >= 0) {
            return getResPreAttCalc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setResPreAttCalcObj(AfDecimal resPreAttCalcObj) {
        if (resPreAttCalcObj != null) {
            setResPreAttCalc(new AfDecimal(resPreAttCalcObj, 15, 3));
            ws.getIndDForzLiq().setResPreAttCalc(((short)0));
        }
        else {
            ws.getIndDForzLiq().setResPreAttCalc(((short)-1));
        }
    }

    @Override
    public AfDecimal getResPreAttDfz() {
        return dForzLiq.getDflResPreAttDfz().getDflResPreAttDfz();
    }

    @Override
    public void setResPreAttDfz(AfDecimal resPreAttDfz) {
        this.dForzLiq.getDflResPreAttDfz().setDflResPreAttDfz(resPreAttDfz.copy());
    }

    @Override
    public AfDecimal getResPreAttDfzObj() {
        if (ws.getIndDForzLiq().getResPreAttDfz() >= 0) {
            return getResPreAttDfz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setResPreAttDfzObj(AfDecimal resPreAttDfzObj) {
        if (resPreAttDfzObj != null) {
            setResPreAttDfz(new AfDecimal(resPreAttDfzObj, 15, 3));
            ws.getIndDForzLiq().setResPreAttDfz(((short)0));
        }
        else {
            ws.getIndDForzLiq().setResPreAttDfz(((short)-1));
        }
    }

    @Override
    public AfDecimal getResPreAttEfflq() {
        return dForzLiq.getDflResPreAttEfflq().getDflResPreAttEfflq();
    }

    @Override
    public void setResPreAttEfflq(AfDecimal resPreAttEfflq) {
        this.dForzLiq.getDflResPreAttEfflq().setDflResPreAttEfflq(resPreAttEfflq.copy());
    }

    @Override
    public AfDecimal getResPreAttEfflqObj() {
        if (ws.getIndDForzLiq().getResPreAttEfflq() >= 0) {
            return getResPreAttEfflq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setResPreAttEfflqObj(AfDecimal resPreAttEfflqObj) {
        if (resPreAttEfflqObj != null) {
            setResPreAttEfflq(new AfDecimal(resPreAttEfflqObj, 15, 3));
            ws.getIndDForzLiq().setResPreAttEfflq(((short)0));
        }
        else {
            ws.getIndDForzLiq().setResPreAttEfflq(((short)-1));
        }
    }

    @Override
    public AfDecimal getResPrstzCalc() {
        return dForzLiq.getDflResPrstzCalc().getDflResPrstzCalc();
    }

    @Override
    public void setResPrstzCalc(AfDecimal resPrstzCalc) {
        this.dForzLiq.getDflResPrstzCalc().setDflResPrstzCalc(resPrstzCalc.copy());
    }

    @Override
    public AfDecimal getResPrstzCalcObj() {
        if (ws.getIndDForzLiq().getResPrstzCalc() >= 0) {
            return getResPrstzCalc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setResPrstzCalcObj(AfDecimal resPrstzCalcObj) {
        if (resPrstzCalcObj != null) {
            setResPrstzCalc(new AfDecimal(resPrstzCalcObj, 15, 3));
            ws.getIndDForzLiq().setResPrstzCalc(((short)0));
        }
        else {
            ws.getIndDForzLiq().setResPrstzCalc(((short)-1));
        }
    }

    @Override
    public AfDecimal getResPrstzDfz() {
        return dForzLiq.getDflResPrstzDfz().getDflResPrstzDfz();
    }

    @Override
    public void setResPrstzDfz(AfDecimal resPrstzDfz) {
        this.dForzLiq.getDflResPrstzDfz().setDflResPrstzDfz(resPrstzDfz.copy());
    }

    @Override
    public AfDecimal getResPrstzDfzObj() {
        if (ws.getIndDForzLiq().getResPrstzDfz() >= 0) {
            return getResPrstzDfz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setResPrstzDfzObj(AfDecimal resPrstzDfzObj) {
        if (resPrstzDfzObj != null) {
            setResPrstzDfz(new AfDecimal(resPrstzDfzObj, 15, 3));
            ws.getIndDForzLiq().setResPrstzDfz(((short)0));
        }
        else {
            ws.getIndDForzLiq().setResPrstzDfz(((short)-1));
        }
    }

    @Override
    public AfDecimal getResPrstzEfflq() {
        return dForzLiq.getDflResPrstzEfflq().getDflResPrstzEfflq();
    }

    @Override
    public void setResPrstzEfflq(AfDecimal resPrstzEfflq) {
        this.dForzLiq.getDflResPrstzEfflq().setDflResPrstzEfflq(resPrstzEfflq.copy());
    }

    @Override
    public AfDecimal getResPrstzEfflqObj() {
        if (ws.getIndDForzLiq().getResPrstzEfflq() >= 0) {
            return getResPrstzEfflq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setResPrstzEfflqObj(AfDecimal resPrstzEfflqObj) {
        if (resPrstzEfflqObj != null) {
            setResPrstzEfflq(new AfDecimal(resPrstzEfflqObj, 15, 3));
            ws.getIndDForzLiq().setResPrstzEfflq(((short)0));
        }
        else {
            ws.getIndDForzLiq().setResPrstzEfflq(((short)-1));
        }
    }

    @Override
    public AfDecimal getRitAccCalc() {
        return dForzLiq.getDflRitAccCalc().getDflRitAccCalc();
    }

    @Override
    public void setRitAccCalc(AfDecimal ritAccCalc) {
        this.dForzLiq.getDflRitAccCalc().setDflRitAccCalc(ritAccCalc.copy());
    }

    @Override
    public AfDecimal getRitAccCalcObj() {
        if (ws.getIndDForzLiq().getRitAccCalc() >= 0) {
            return getRitAccCalc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRitAccCalcObj(AfDecimal ritAccCalcObj) {
        if (ritAccCalcObj != null) {
            setRitAccCalc(new AfDecimal(ritAccCalcObj, 15, 3));
            ws.getIndDForzLiq().setRitAccCalc(((short)0));
        }
        else {
            ws.getIndDForzLiq().setRitAccCalc(((short)-1));
        }
    }

    @Override
    public AfDecimal getRitAccDfz() {
        return dForzLiq.getDflRitAccDfz().getDflRitAccDfz();
    }

    @Override
    public void setRitAccDfz(AfDecimal ritAccDfz) {
        this.dForzLiq.getDflRitAccDfz().setDflRitAccDfz(ritAccDfz.copy());
    }

    @Override
    public AfDecimal getRitAccDfzObj() {
        if (ws.getIndDForzLiq().getRitAccDfz() >= 0) {
            return getRitAccDfz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRitAccDfzObj(AfDecimal ritAccDfzObj) {
        if (ritAccDfzObj != null) {
            setRitAccDfz(new AfDecimal(ritAccDfzObj, 15, 3));
            ws.getIndDForzLiq().setRitAccDfz(((short)0));
        }
        else {
            ws.getIndDForzLiq().setRitAccDfz(((short)-1));
        }
    }

    @Override
    public AfDecimal getRitAccEfflq() {
        return dForzLiq.getDflRitAccEfflq().getDflRitAccEfflq();
    }

    @Override
    public void setRitAccEfflq(AfDecimal ritAccEfflq) {
        this.dForzLiq.getDflRitAccEfflq().setDflRitAccEfflq(ritAccEfflq.copy());
    }

    @Override
    public AfDecimal getRitAccEfflqObj() {
        if (ws.getIndDForzLiq().getRitAccEfflq() >= 0) {
            return getRitAccEfflq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRitAccEfflqObj(AfDecimal ritAccEfflqObj) {
        if (ritAccEfflqObj != null) {
            setRitAccEfflq(new AfDecimal(ritAccEfflqObj, 15, 3));
            ws.getIndDForzLiq().setRitAccEfflq(((short)0));
        }
        else {
            ws.getIndDForzLiq().setRitAccEfflq(((short)-1));
        }
    }

    @Override
    public AfDecimal getRitIrpefCalc() {
        return dForzLiq.getDflRitIrpefCalc().getDflRitIrpefCalc();
    }

    @Override
    public void setRitIrpefCalc(AfDecimal ritIrpefCalc) {
        this.dForzLiq.getDflRitIrpefCalc().setDflRitIrpefCalc(ritIrpefCalc.copy());
    }

    @Override
    public AfDecimal getRitIrpefCalcObj() {
        if (ws.getIndDForzLiq().getRitIrpefCalc() >= 0) {
            return getRitIrpefCalc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRitIrpefCalcObj(AfDecimal ritIrpefCalcObj) {
        if (ritIrpefCalcObj != null) {
            setRitIrpefCalc(new AfDecimal(ritIrpefCalcObj, 15, 3));
            ws.getIndDForzLiq().setRitIrpefCalc(((short)0));
        }
        else {
            ws.getIndDForzLiq().setRitIrpefCalc(((short)-1));
        }
    }

    @Override
    public AfDecimal getRitIrpefDfz() {
        return dForzLiq.getDflRitIrpefDfz().getDflRitIrpefDfz();
    }

    @Override
    public void setRitIrpefDfz(AfDecimal ritIrpefDfz) {
        this.dForzLiq.getDflRitIrpefDfz().setDflRitIrpefDfz(ritIrpefDfz.copy());
    }

    @Override
    public AfDecimal getRitIrpefDfzObj() {
        if (ws.getIndDForzLiq().getRitIrpefDfz() >= 0) {
            return getRitIrpefDfz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRitIrpefDfzObj(AfDecimal ritIrpefDfzObj) {
        if (ritIrpefDfzObj != null) {
            setRitIrpefDfz(new AfDecimal(ritIrpefDfzObj, 15, 3));
            ws.getIndDForzLiq().setRitIrpefDfz(((short)0));
        }
        else {
            ws.getIndDForzLiq().setRitIrpefDfz(((short)-1));
        }
    }

    @Override
    public AfDecimal getRitIrpefEfflq() {
        return dForzLiq.getDflRitIrpefEfflq().getDflRitIrpefEfflq();
    }

    @Override
    public void setRitIrpefEfflq(AfDecimal ritIrpefEfflq) {
        this.dForzLiq.getDflRitIrpefEfflq().setDflRitIrpefEfflq(ritIrpefEfflq.copy());
    }

    @Override
    public AfDecimal getRitIrpefEfflqObj() {
        if (ws.getIndDForzLiq().getRitIrpefEfflq() >= 0) {
            return getRitIrpefEfflq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRitIrpefEfflqObj(AfDecimal ritIrpefEfflqObj) {
        if (ritIrpefEfflqObj != null) {
            setRitIrpefEfflq(new AfDecimal(ritIrpefEfflqObj, 15, 3));
            ws.getIndDForzLiq().setRitIrpefEfflq(((short)0));
        }
        else {
            ws.getIndDForzLiq().setRitIrpefEfflq(((short)-1));
        }
    }

    @Override
    public AfDecimal getRitTfrCalc() {
        return dForzLiq.getDflRitTfrCalc().getDflRitTfrCalc();
    }

    @Override
    public void setRitTfrCalc(AfDecimal ritTfrCalc) {
        this.dForzLiq.getDflRitTfrCalc().setDflRitTfrCalc(ritTfrCalc.copy());
    }

    @Override
    public AfDecimal getRitTfrCalcObj() {
        if (ws.getIndDForzLiq().getRitTfrCalc() >= 0) {
            return getRitTfrCalc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRitTfrCalcObj(AfDecimal ritTfrCalcObj) {
        if (ritTfrCalcObj != null) {
            setRitTfrCalc(new AfDecimal(ritTfrCalcObj, 15, 3));
            ws.getIndDForzLiq().setRitTfrCalc(((short)0));
        }
        else {
            ws.getIndDForzLiq().setRitTfrCalc(((short)-1));
        }
    }

    @Override
    public AfDecimal getRitTfrDfz() {
        return dForzLiq.getDflRitTfrDfz().getDflRitTfrDfz();
    }

    @Override
    public void setRitTfrDfz(AfDecimal ritTfrDfz) {
        this.dForzLiq.getDflRitTfrDfz().setDflRitTfrDfz(ritTfrDfz.copy());
    }

    @Override
    public AfDecimal getRitTfrDfzObj() {
        if (ws.getIndDForzLiq().getRitTfrDfz() >= 0) {
            return getRitTfrDfz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRitTfrDfzObj(AfDecimal ritTfrDfzObj) {
        if (ritTfrDfzObj != null) {
            setRitTfrDfz(new AfDecimal(ritTfrDfzObj, 15, 3));
            ws.getIndDForzLiq().setRitTfrDfz(((short)0));
        }
        else {
            ws.getIndDForzLiq().setRitTfrDfz(((short)-1));
        }
    }

    @Override
    public AfDecimal getRitTfrEfflq() {
        return dForzLiq.getDflRitTfrEfflq().getDflRitTfrEfflq();
    }

    @Override
    public void setRitTfrEfflq(AfDecimal ritTfrEfflq) {
        this.dForzLiq.getDflRitTfrEfflq().setDflRitTfrEfflq(ritTfrEfflq.copy());
    }

    @Override
    public AfDecimal getRitTfrEfflqObj() {
        if (ws.getIndDForzLiq().getRitTfrEfflq() >= 0) {
            return getRitTfrEfflq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRitTfrEfflqObj(AfDecimal ritTfrEfflqObj) {
        if (ritTfrEfflqObj != null) {
            setRitTfrEfflq(new AfDecimal(ritTfrEfflqObj, 15, 3));
            ws.getIndDForzLiq().setRitTfrEfflq(((short)0));
        }
        else {
            ws.getIndDForzLiq().setRitTfrEfflq(((short)-1));
        }
    }

    @Override
    public AfDecimal getTaxSepCalc() {
        return dForzLiq.getDflTaxSepCalc().getDflTaxSepCalc();
    }

    @Override
    public void setTaxSepCalc(AfDecimal taxSepCalc) {
        this.dForzLiq.getDflTaxSepCalc().setDflTaxSepCalc(taxSepCalc.copy());
    }

    @Override
    public AfDecimal getTaxSepCalcObj() {
        if (ws.getIndDForzLiq().getTaxSepCalc() >= 0) {
            return getTaxSepCalc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTaxSepCalcObj(AfDecimal taxSepCalcObj) {
        if (taxSepCalcObj != null) {
            setTaxSepCalc(new AfDecimal(taxSepCalcObj, 15, 3));
            ws.getIndDForzLiq().setTaxSepCalc(((short)0));
        }
        else {
            ws.getIndDForzLiq().setTaxSepCalc(((short)-1));
        }
    }

    @Override
    public AfDecimal getTaxSepDfz() {
        return dForzLiq.getDflTaxSepDfz().getDflTaxSepDfz();
    }

    @Override
    public void setTaxSepDfz(AfDecimal taxSepDfz) {
        this.dForzLiq.getDflTaxSepDfz().setDflTaxSepDfz(taxSepDfz.copy());
    }

    @Override
    public AfDecimal getTaxSepDfzObj() {
        if (ws.getIndDForzLiq().getTaxSepDfz() >= 0) {
            return getTaxSepDfz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTaxSepDfzObj(AfDecimal taxSepDfzObj) {
        if (taxSepDfzObj != null) {
            setTaxSepDfz(new AfDecimal(taxSepDfzObj, 15, 3));
            ws.getIndDForzLiq().setTaxSepDfz(((short)0));
        }
        else {
            ws.getIndDForzLiq().setTaxSepDfz(((short)-1));
        }
    }

    @Override
    public AfDecimal getTaxSepEfflq() {
        return dForzLiq.getDflTaxSepEfflq().getDflTaxSepEfflq();
    }

    @Override
    public void setTaxSepEfflq(AfDecimal taxSepEfflq) {
        this.dForzLiq.getDflTaxSepEfflq().setDflTaxSepEfflq(taxSepEfflq.copy());
    }

    @Override
    public AfDecimal getTaxSepEfflqObj() {
        if (ws.getIndDForzLiq().getTaxSepEfflq() >= 0) {
            return getTaxSepEfflq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTaxSepEfflqObj(AfDecimal taxSepEfflqObj) {
        if (taxSepEfflqObj != null) {
            setTaxSepEfflq(new AfDecimal(taxSepEfflqObj, 15, 3));
            ws.getIndDForzLiq().setTaxSepEfflq(((short)0));
        }
        else {
            ws.getIndDForzLiq().setTaxSepEfflq(((short)-1));
        }
    }
}
