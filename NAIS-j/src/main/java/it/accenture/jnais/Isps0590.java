package it.accenture.jnais;

import com.bphx.ctu.af.core.pointer.IPointerManager;
import com.bphx.ctu.af.core.program.DynamicCall;
import com.bphx.ctu.af.core.program.DynamicCallException;
import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.ws.AreaIdsv0001;
import it.accenture.jnais.ws.AreaIoIsps0590;
import it.accenture.jnais.ws.enums.Ispc0001GravitaErr;
import it.accenture.jnais.ws.Ieai9901Area;
import it.accenture.jnais.ws.Isps0590Data;
import it.accenture.jnais.ws.Lccc0001;
import javax.inject.Inject;

/**Original name: ISPS0590<br>
 * <pre>*****************************************************************
 * *                                                        ********
 * *    PORTAFOGLIO VITA ITALIA  VER 1.0                    ********
 * *                                                        ********
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2012.
 * DATE-COMPILED.
 * ----------------------------------------------------------------*
 *   PROGRAMMA ..... ISPS0590
 *   TIPOLOGIA...... Servizio Actuator
 *   PROCESSO....... XXX
 *   FUNZIONE....... XXX
 *   DESCRIZIONE.... Programma di chiamata ai servizi Actuator
 * ----------------------------------------------------------------*</pre>*/
public class Isps0590 extends Program {

    //==== PROPERTIES ====
    @Inject
    private IPointerManager pointerManager;
    //Original name: WORKING-STORAGE
    private Isps0590Data ws = new Isps0590Data();
    //Original name: AREA-IDSV0001
    private AreaIdsv0001 areaIdsv0001;
    //Original name: WCOM-AREA-STATI
    private Lccc0001 lccc0001;
    //Original name: AREA-IO-ISPS0590
    private AreaIoIsps0590 areaIoIsps0590;

    //==== CONSTRUCTORS ====
    public Isps0590() {
        registerListeners();
    }

    //==== METHODS ====
    /**Original name: PROGRAM_ISPS0590_FIRST_SENTENCES<br>
	 * <pre>---------------------------------------------------------------*</pre>*/
    public long execute(AreaIdsv0001 areaIdsv0001, Lccc0001 lccc0001, AreaIoIsps0590 areaIoIsps0590) {
        this.areaIdsv0001 = areaIdsv0001;
        this.lccc0001 = lccc0001;
        this.areaIoIsps0590 = areaIoIsps0590;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: IF IDSV0001-ESITO-OK
        //                 THRU EX-S1000
        //           END-IF.
        if (this.areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: PERFORM S1000-ELABORAZIONE
            //              THRU EX-S1000
            s1000Elaborazione();
        }
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Isps0590 getInstance() {
        return ((Isps0590)Programs.getInstance(Isps0590.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *  OPERAZIONI INIZIALI                                            *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *   ELABORAZIONE                                                  *
	 * ----------------------------------------------------------------*</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: PERFORM S1050-PREP-AREA-ISPS0590
        //              THRU EX-S1050.
        s1050PrepAreaIsps0590();
        // COB_CODE: PERFORM S1100-CALL-ISPS0590
        //              THRU EX-S1100.
        s1100CallIsps0590();
    }

    /**Original name: S1050-PREP-AREA-ISPS0590<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZA AREA SERVIZIO
	 * ----------------------------------------------------------------*</pre>*/
    private void s1050PrepAreaIsps0590() {
        // COB_CODE: MOVE LOW-VALUES               TO AREA-PRODUCT-SERVICES.
        ws.getAreaProductServices().initAreaProductServicesLowValues();
        // COB_CODE: MOVE IDSV0001-MODALITA-ESECUTIVA
        //                                         TO IJCCMQ01-MODALITA-ESECUTIVA.
        ws.getAreaProductServices().setIjccmq01ModalitaEsecutiva(areaIdsv0001.getAreaComune().getModalitaEsecutiva().getModalitaEsecutiva());
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //                                         TO ISPC0590-COD-COMPAGNIA-ANIA.
        areaIoIsps0590.getDatiInput().setCodCompagniaAniaFormatted(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAniaFormatted());
        // COB_CODE: SET IJCCMQ01-ESITO-OK         TO TRUE.
        ws.getAreaProductServices().setIjccmq01EsitoOk();
        // COB_CODE: MOVE 'ISPS0590'               TO IJCCMQ01-JAVA-SERVICE-NAME.
        ws.getAreaProductServices().setIjccmq01JavaServiceName("ISPS0590");
    }

    /**Original name: S1100-CALL-ISPS0590<br>
	 * <pre>----------------------------------------------------------------*
	 *   CHIAMATA AL SERVIZIO
	 * ----------------------------------------------------------------*</pre>*/
    private void s1100CallIsps0590() {
        // COB_CODE: MOVE LENGTH OF AREA-IO-ISPS0590
        //                                    TO IJCCMQ01-LENGTH-DATI-SERVIZIO.
        ws.getAreaProductServices().setIjccmq01LengthDatiServizio(AreaIoIsps0590.Len.AREA_IO_ISPS0590);
        // COB_CODE: MOVE AREA-IO-ISPS0590
        //                                    TO IJCCMQ01-AREA-DATI-SERVIZIO.
        ws.getAreaProductServices().setIjccmq01AreaDatiServizioBytes(areaIoIsps0590.getAreaIoIsps0590Bytes());
        // COB_CODE: SET WS-ADDRESS           TO ADDRESS OF AREA-PRODUCT-SERVICES.
        ws.getMq01Address().setWsAddress(pointerManager.addressOf(ws.getAreaProductServices()));
        // COB_CODE: MOVE IDSV0001-LIVELLO-DEBUG   TO IJCCMQ00-LIVELLO-DEBUG
        ws.getAreaProductServices().setIjccmq00LivelloDebugFormatted(areaIdsv0001.getAreaComune().getLivelloDebug().getIdsi0011LivelloDebugFormatted());
        // COB_CODE: MOVE IDSV0001-AREA-ADDRESSES  TO IJCCMQ01-AREA-ADDRESSES
        ws.getAreaProductServices().setIjccmq01AreaAddressesBytes(areaIdsv0001.getAreaComune().getIdsv0001AreaAddressesBytes());
        // COB_CODE: MOVE IDSV0001-USER-NAME       TO IJCCMQ01-USER-NAME
        ws.getAreaProductServices().setIjccmq01UserName(areaIdsv0001.getAreaComune().getUserName());
        // COB_CODE: CALL INTERF-MQSERIES USING MQ01-ADDRESS
        //                                      AREA-PRODUCT-SERVICES
        //           ON EXCEPTION
        //                 THRU EX-S0290
        //           END-CALL.
        try {
            DynamicCall.invoke(ws.getInterfMqseries(), ws.getMq01Address(), ws.getAreaProductServices());
        }
        catch (DynamicCallException __ex) {
            // COB_CODE: MOVE WK-PGM           TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'ISPS0590'       TO CALL-DESC
            ws.getIdsv0002().setCallDesc("ISPS0590");
            // COB_CODE: MOVE 'S1100-CALL-ISPS0590'
            //                                 TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S1100-CALL-ISPS0590");
            // COB_CODE: PERFORM S0290-ERRORE-DI-SISTEMA
            //              THRU EX-S0290
            s0290ErroreDiSistema();
        }
        //--> RETURNCODE ACTUATOR
        // COB_CODE:      IF IJCCMQ01-ESITO-OK
        //                      THRU EX-S0320-OUTPUT-PRODOTTO
        //                ELSE
        //           *--> GESTIONE ERRORI AREA IJCSMQ01
        //                   END-PERFORM
        //                END-IF.
        if (ws.getAreaProductServices().isIjccmq01EsitoOk()) {
            // COB_CODE: MOVE IJCCMQ01-AREA-DATI-SERVIZIO
            //                                 TO AREA-IO-ISPS0590
            areaIoIsps0590.setAreaIoIsps0590Bytes(ws.getAreaProductServices().getIjccmq01AreaDatiServizioBytes());
            // COB_CODE: MOVE ISPC0590-AREA-ERRORI
            //                                 TO ISPC0001-AREA-ERRORI
            ws.getIeav9903().getIspc0001AreaErrori().setIspc0001AreaErroriBytes(areaIoIsps0590.getAreaErroriBytes());
            // COB_CODE: PERFORM S0320-OUTPUT-PRODOTTO
            //              THRU EX-S0320-OUTPUT-PRODOTTO
            s0320OutputProdotto();
        }
        else {
            //--> GESTIONE ERRORI AREA IJCSMQ01
            // COB_CODE: SET IDSV0001-ESITO-KO TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE IJCCMQ01-MAX-ELE-ERRORI
            //                                 TO IDSV0001-MAX-ELE-ERRORI
            areaIdsv0001.setMaxEleErrori(ws.getAreaProductServices().getIjccmq01MaxEleErrori());
            // COB_CODE: PERFORM VARYING IX-TAB-ERR FROM 1 BY 1
            //                     UNTIL IX-TAB-ERR > IJCCMQ01-MAX-ELE-ERRORI
            //                               TO IDSV0001-LIV-GRAVITA-BE(IX-TAB-ERR)
            //           END-PERFORM
            ws.getIxIndici().setTabErr(((short)1));
            while (!(ws.getIxIndici().getTabErr() > ws.getAreaProductServices().getIjccmq01MaxEleErrori())) {
                // COB_CODE: MOVE IJCCMQ01-COD-ERRORE(IX-TAB-ERR)
                //                             TO IDSV0001-COD-ERRORE(IX-TAB-ERR)
                areaIdsv0001.getEleErrori(ws.getIxIndici().getTabErr()).setIdsv0001CodErroreFormatted(ws.getAreaProductServices().getIjccmq01CodErroreFormatted(ws.getIxIndici().getTabErr()));
                // COB_CODE: MOVE IJCCMQ01-DESC-ERRORE(IX-TAB-ERR)
                //                             TO IDSV0001-DESC-ERRORE(IX-TAB-ERR)
                areaIdsv0001.getEleErrori(ws.getIxIndici().getTabErr()).setDescErrore(ws.getAreaProductServices().getIjccmq01DescErrore(ws.getIxIndici().getTabErr()));
                // COB_CODE: MOVE IJCCMQ01-LIV-GRAVITA-BE(IX-TAB-ERR)
                //                           TO IDSV0001-LIV-GRAVITA-BE(IX-TAB-ERR)
                areaIdsv0001.getEleErrori(ws.getIxIndici().getTabErr()).setIdsv0001LivGravitaBeFormatted(ws.getAreaProductServices().getIjccmq01LivGravitaBeFormatted(ws.getIxIndici().getTabErr()));
                ws.getIxIndici().setTabErr(Trunc.toShort(ws.getIxIndici().getTabErr() + 1, 4));
            }
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *     OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    /**Original name: S0290-ERRORE-DI-SISTEMA<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES GESTIONE ERRORE
	 * ----------------------------------------------------------------*
	 * **-------------------------------------------------------------**
	 *     PROGRAMMA ..... IERP9902
	 *     TIPOLOGIA...... COPY
	 *     DESCRIZIONE.... ROUTINE GENERALIZZATA GESTIONE ERRORI CALL
	 * **-------------------------------------------------------------**</pre>*/
    private void s0290ErroreDiSistema() {
        // COB_CODE: MOVE '005006'           TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErroreFormatted("005006");
        // COB_CODE: MOVE CALL-DESC          TO IEAI9901-PARAMETRI-ERR.
        ws.getIeai9901Area().setParametriErr(ws.getIdsv0002().getCallDesc());
        // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
        //              THRU EX-S0300.
        s0300RicercaGravitaErrore();
    }

    /**Original name: S0300-RICERCA-GRAVITA-ERRORE<br>
	 * <pre>MUOVO L'AREA DEL SERVIZIO NELL'AREA DATI DELL'AREA DI CONTESTO.
	 * ----->VALORIZZO I CAMPI DELLA IDSV0001</pre>*/
    private void s0300RicercaGravitaErrore() {
        Ieas9900 ieas9900 = null;
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR       TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE  'IEAS9900'              TO CALL-PGM
        ws.getIdsv0002().setCallPgm("IEAS9900");
        // COB_CODE: CALL CALL-PGM USING IDSV0001-AREA-CONTESTO
        //                               IEAI9901-AREA
        //                               IEAO9901-AREA
        //             ON EXCEPTION
        //                   THRU EX-S0310-ERRORE-FATALE
        //           END-CALL.
        try {
            ieas9900 = Ieas9900.getInstance();
            ieas9900.run(areaIdsv0001, ws.getIeai9901Area(), ws.getIeao9901Area());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
            areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
            // COB_CODE: PERFORM S0310-ERRORE-FATALE
            //              THRU EX-S0310-ERRORE-FATALE
            s0310ErroreFatale();
        }
        //SE LA CHIAMATA AL SERVIZIO HA ESITO POSITIVO (NESSUN ERRORE DI
        //SISTEMA, VALORIZZO L'AREA DI OUTPUT.
        //INCREMENTO L'INDICE DEGLI ERRORI
        // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
        areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
        //SE L'INDICE E' MINORE DI 10 ...
        // COB_CODE:      IF IDSV0001-MAX-ELE-ERRORI NOT GREATER 10
        //           *SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
        //                   END-IF
        //                ELSE
        //           *IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        //                       TO IDSV0001-DESC-ERRORE-ESTESA
        //                END-IF.
        if (areaIdsv0001.getMaxEleErrori() <= 10) {
            //SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
            // COB_CODE: IF IEAO9901-COD-ERRORE-990 = ZEROES
            //                      EX-S0300-IMPOSTA-ERRORE
            //           ELSE
            //                   IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
            //           END-IF
            if (Characters.EQ_ZERO.test(ws.getIeao9901Area().getCodErrore990Formatted())) {
                // COB_CODE: PERFORM S0300-IMPOSTA-ERRORE THRU
                //                   EX-S0300-IMPOSTA-ERRORE
                s0300ImpostaErrore();
            }
            else {
                // COB_CODE: MOVE 'IEAS9900'
                //             TO IDSV0001-COD-SERVIZIO-BE
                areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
                // COB_CODE: MOVE IEAO9901-LABEL-ERR-990
                //             TO IDSV0001-LABEL-ERR
                areaIdsv0001.getLogErrore().setLabelErr(ws.getIeao9901Area().getLabelErr990());
                // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
                //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
                // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
                areaIdsv0001.getEsito().setIdsv0001EsitoKo();
                // IMPOSTO IL CODICE ERRORE GESTITO ALL'INTERNO DEL PROGRAMMA
                // COB_CODE: MOVE IEAO9901-COD-ERRORE-990
                //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeao9901Area().getCodErrore990Formatted());
                // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
                //             TO IDSV0001-DESC-ERRORE-ESTESA
                //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
            }
        }
        else {
            //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
            // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
            //             TO IDSV0001-LABEL-ERR
            areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
            // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 'IEAS9900'
            //             TO IDSV0001-COD-SERVIZIO-BE
            areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
            // COB_CODE: MOVE 'OVERFLOW IMPAGINAZIONE ERRORI'
            //             TO IDSV0001-DESC-ERRORE-ESTESA
            areaIdsv0001.getLogErrore().setDescErroreEstesa("OVERFLOW IMPAGINAZIONE ERRORI");
        }
    }

    /**Original name: S0300-IMPOSTA-ERRORE<br>
	 * <pre>  se la gravit— di tutti gli errori dell'elaborazione
	 *   h minore di zero ( ad esempio -3) vuol dire che il return-code
	 *   dell'elaborazione complessiva deve essere abbassato da '08' a
	 *   '04'. Per fare cir utilizzo il flag IDSV0001-FORZ-RC-04
	 * IMPOSTO LA GRAVITA'</pre>*/
    private void s0300ImpostaErrore() {
        // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
        // COB_CODE: EVALUATE TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = -3
        //                       IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        //             WHEN  IEAO9901-LIV-GRAVITA = 0 OR 1 OR 2
        //                   SET IDSV0001-FORZ-RC-04-NO  TO TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = 3 OR 4
        //                   SET IDSV0001-ESITO-KO       TO TRUE
        //           END-EVALUATE
        if (ws.getIeao9901Area().getLivGravita() == -3) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-YES TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04Yes();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 2  TO
            //               IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
            areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)2));
        }
        else if (ws.getIeao9901Area().getLivGravita() == 0 || ws.getIeao9901Area().getLivGravita() == 1 || ws.getIeao9901Area().getLivGravita() == 2) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
        }
        else if (ws.getIeao9901Area().getLivGravita() == 3 || ws.getIeao9901Area().getLivGravita() == 4) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        }
        // IMPOSTO IL CODICE ERRORE
        // COB_CODE: MOVE IEAI9901-COD-ERRORE
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeai9901Area().getCodErroreFormatted());
        // SE IL LIVELLO DI GRAVITA' RESTITUITO DALLO IAS9900 E' MAGGIORE
        // DI 2 (ERRORE BLOCCANTE)...IMPOSTO L'ESITO E I DATI DI CONTESTO
        //RELATIVI ALL'ERRORE BLOCCANTE
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE
        //             TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR
        //             TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
        //             TO IDSV0001-DESC-ERRORE-ESTESA
        //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
        // COB_CODE: MOVE SPACES          TO IEAI9901-COD-SERVIZIO-BE
        //                                  IEAI9901-LABEL-ERR
        //                                   IEAI9901-PARAMETRI-ERR.
        ws.getIeai9901Area().setCodServizioBe("");
        ws.getIeai9901Area().setLabelErr("");
        ws.getIeai9901Area().setParametriErr("");
        // COB_CODE: MOVE ZEROES          TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErrore(0);
    }

    /**Original name: S0310-ERRORE-FATALE<br>
	 * <pre>IMPOSTO LA GRAVITA' ERRORE</pre>*/
    private void s0310ErroreFatale() {
        // COB_CODE: MOVE  3
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)3));
        // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
        areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        //IMPOSTO IL CODICE ERRORE (ERRORE FISSO)
        // COB_CODE: MOVE 900001
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErrore(900001);
        //IMPOSTO NOME DEL MODULO
        // COB_CODE: MOVE 'IEAS9900'               TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
        //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
        //              TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
        // COB_CODE: MOVE  'ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900'
        //              TO IDSV0001-DESC-ERRORE-ESTESA
        //                 IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
    }

    /**Original name: S0320-OUTPUT-PRODOTTO<br>
	 * <pre>----------------------------------------------------------------*
	 *   COPY DI PROCEDURE CONTENETE LA GESTIONE DEGLI ERRORI DERIVANTI
	 *   DAL SERVIZIO DI PRODOTTO
	 *   SMISTA QUELLI CHE SONO ERRORI BLOCCANTI E LE EVENTUALI DEROGHE
	 * ----------------------------------------------------------------*
	 * --> STEP ELABORAZIONE</pre>*/
    private void s0320OutputProdotto() {
        // COB_CODE: IF ISPC0001-STEP-ELAB EQUAL SPACES OR HIGH-VALUE
        //              MOVE '*'            TO ISPC0001-STEP-ELAB
        //           END-IF
        if (Conditions.eq(ws.getIeav9903().getIspc0001StepElab(), Types.SPACE_CHAR) || Conditions.eq(ws.getIeav9903().getIspc0001StepElab(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE '*'            TO ISPC0001-STEP-ELAB
            ws.getIeav9903().setIspc0001StepElabFormatted("*");
        }
        // COB_CODE: IF WCOM-NUM-ELE-MOT-DEROGA =  SPACES OR HIGH-VALUE
        //                                                OR LOW-VALUE
        //              MOVE ZEROES         TO WCOM-NUM-ELE-MOT-DEROGA
        //           END-IF
        if (lccc0001.getDatiDeroghe().getNumEleMotDeroga() == Types.INVALID_SHORT_VAL || lccc0001.getDatiDeroghe().getNumEleMotDeroga() == Types.HIGH_SHORT_VAL || lccc0001.getDatiDeroghe().getNumEleMotDeroga() == Types.LOW_SHORT_VAL) {
            // COB_CODE: MOVE ZEROES         TO WCOM-NUM-ELE-MOT-DEROGA
            lccc0001.getDatiDeroghe().setNumEleMotDeroga(((short)0));
        }
        //    PULIZIA AUTOMATICA DELLE DEROGHE NON PIU' VALIDE
        //    PER LO STEP ELABORATIVO CORRENTE
        //    LO STEP ELABORATIVO CI VIENE RESTITUITO DAL VALORIZZATORE
        //    VARIABILI E CANCELLIAMO TUTTE LE DEROGHE RELATIVE ALLO STEP
        //    ELABORATIVO CORRENTE, PRIMA DI MEMORIZZARE QUELLE APPENA
        //    RESTITUITE DAL SERVIZIO DI PRODOTTO
        // COB_CODE: MOVE ZEROES            TO IX-ERR-GENERIC
        ws.getIeav9903().setIxErrGeneric(0);
        // COB_CODE: PERFORM VARYING IX-ERR-DEROG FROM 1 BY 1
        //                     UNTIL IX-ERR-DEROG > WCOM-NUM-ELE-MOT-DEROGA
        //                END-IF
        //           END-PERFORM
        ws.getIeav9903().setIxErrDerog(1);
        while (!(ws.getIeav9903().getIxErrDerog() > lccc0001.getDatiDeroghe().getNumEleMotDeroga())) {
            // COB_CODE:           IF ISPC0001-STEP-ELAB NOT EQUAL
            //                        WCOM-MOT-DER-STEP-ELAB(IX-ERR-DEROG)
            //                             TO WCOM-TAB-MOT-DEROGA(IX-ERR-GENERIC)
            //                     ELSE
            //           *             CANCELLA LA DEROGA
            //                           TO WCOM-TAB-MOT-DEROGA(IX-ERR-DEROG)
            //                     END-IF
            if (ws.getIeav9903().getIspc0001StepElab() != lccc0001.getDatiDeroghe().getTabMotDeroga(ws.getIeav9903().getIxErrDerog()).getMotDerStepElab()) {
                // COB_CODE: ADD 1         TO IX-ERR-GENERIC
                ws.getIeav9903().setIxErrGeneric(Trunc.toInt(1 + ws.getIeav9903().getIxErrGeneric(), 4));
                // COB_CODE: MOVE WCOM-TAB-MOT-DEROGA(IX-ERR-DEROG)
                //             TO WCOM-TAB-MOT-DEROGA(IX-ERR-GENERIC)
                lccc0001.getDatiDeroghe().getTabMotDeroga(ws.getIeav9903().getIxErrGeneric()).setWcomTabMotDerogaBytes(lccc0001.getDatiDeroghe().getTabMotDeroga(ws.getIeav9903().getIxErrDerog()).getWcomTabMotDerogaBytes());
            }
            else {
                //             CANCELLA LA DEROGA
                // COB_CODE: MOVE SPACES
                //             TO WCOM-TAB-MOT-DEROGA(IX-ERR-DEROG)
                lccc0001.getDatiDeroghe().getTabMotDeroga(ws.getIeav9903().getIxErrDerog()).initTabMotDerogaSpaces();
            }
            ws.getIeav9903().setIxErrDerog(Trunc.toInt(ws.getIeav9903().getIxErrDerog() + 1, 4));
        }
        // COB_CODE: MOVE IX-ERR-GENERIC TO WCOM-NUM-ELE-MOT-DEROGA
        lccc0001.getDatiDeroghe().setNumEleMotDeroga(Trunc.toShort(ws.getIeav9903().getIxErrGeneric(), 3));
        //        RETURN CODE = 00
        // COB_CODE:      EVALUATE ISPC0001-ESITO
        //           *        RETURN CODE = 00
        //                    WHEN 00
        //           *          ERRORE DI SISTEMA DURANTE LA CHIAMATA AL SERVIZIO
        //           *          DI ACTUATOR: $ (COD-ERR= $ - GRAVITA = $)
        //                      END-PERFORM
        //           *        RETURN CODE = 01
        //                    WHEN 01
        //                        UNTIL IX-ERR-GENERIC > ISPC0001-ERR-NUM-ELE
        //                END-EVALUATE.
        switch (ws.getIeav9903().getIspc0001AreaErrori().getIspc0001Esito()) {

            case ((short)0)://          ERRORE DI SISTEMA DURANTE LA CHIAMATA AL SERVIZIO
                //          DI ACTUATOR: $ (COD-ERR= $ - GRAVITA = $)
                // COB_CODE: PERFORM VARYING IX-ERR-GENERIC FROM 1 BY 1
                //             UNTIL IX-ERR-GENERIC > ISPC0001-ERR-NUM-ELE
                //                  THRU EX-S0322
                //           END-PERFORM
                ws.getIeav9903().setIxErrGeneric(1);
                while (!(ws.getIeav9903().getIxErrGeneric() > ws.getIeav9903().getIspc0001AreaErrori().getIspc0001ErrNumEle())) {
                    // COB_CODE: MOVE '005217'                TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005217");
                    // COB_CODE: PERFORM S0322-SALVA-ERRORE
                    //              THRU EX-S0322
                    s0322SalvaErrore();
                    ws.getIeav9903().setIxErrGeneric(Trunc.toInt(ws.getIeav9903().getIxErrGeneric() + 1, 4));
                }
                //        RETURN CODE = 01
                break;

            case ((short)1):// COB_CODE: PERFORM S0321-ERRORI-DEROGA
                //              THRU EX-S0321
                //           VARYING IX-ERR-GENERIC FROM 1 BY 1
                //             UNTIL IX-ERR-GENERIC > ISPC0001-ERR-NUM-ELE
                ws.getIeav9903().setIxErrGeneric(1);
                while (!(ws.getIeav9903().getIxErrGeneric() > ws.getIeav9903().getIspc0001AreaErrori().getIspc0001ErrNumEle())) {
                    s0321ErroriDeroga();
                    ws.getIeav9903().setIxErrGeneric(Trunc.toInt(ws.getIeav9903().getIxErrGeneric() + 1, 4));
                }
                break;

            default:break;
        }
    }

    /**Original name: S0322-SALVA-ERRORE<br>
	 * <pre>----------------------------------------------------------------*
	 *    SALVA UN ERRORE NELL'AREA COMUNE
	 * ----------------------------------------------------------------*</pre>*/
    private void s0322SalvaErrore() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'IERP9903'                   TO IEAI9901-COD-SERVIZIO-BE
        ws.getIeai9901Area().setCodServizioBe("IERP9903");
        // COB_CODE: MOVE 'S0320-OUTPUT-PRODOTTO'      TO IEAI9901-LABEL-ERR
        ws.getIeai9901Area().setLabelErr("S0320-OUTPUT-PRODOTTO");
        // COB_CODE: STRING ISPC0001-DESCRIZIONE-ERR(IX-ERR-GENERIC)  ';'
        //                  ISPC0001-COD-ERR(IX-ERR-GENERIC)          ';'
        //                  ISPC0001-GRAVITA-ERR(IX-ERR-GENERIC)
        //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
        //           END-STRING
        concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getIeav9903().getIspc0001AreaErrori().getTabErrori(ws.getIeav9903().getIxErrGeneric()).getIspc0001DescrizioneErrFormatted(), ";", ws.getIeav9903().getIspc0001AreaErrori().getTabErrori(ws.getIeav9903().getIxErrGeneric()).getIspc0001CodErrFormatted(), ";", ws.getIeav9903().getIspc0001AreaErrori().getTabErrori(ws.getIeav9903().getIxErrGeneric()).getGravitaErr().getIspc0001GravitaErrAsString());
        ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
        // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
        //              THRU EX-S0300.
        s0300RicercaGravitaErrore();
    }

    /**Original name: S0321-ERRORI-DEROGA<br>
	 * <pre>----------------------------------------------------------------*
	 *    GESTIONE ERRORI-DEROGHE
	 * ----------------------------------------------------------------*
	 *         Errore Applicativo Warning
	 *         (proveniente dall’applicazione Actuator)
	 *         Errore Applicativo Warning
	 *         (proveniente da controllo caricato dall’utente)</pre>*/
    private void s0321ErroriDeroga() {
        // COB_CODE:      EVALUATE TRUE
        //           *        Errore Applicativo Warning
        //           *        (proveniente dall’applicazione Actuator)
        //                    WHEN ISPC0001-GR-WARNING-ACT(IX-ERR-GENERIC)
        //           *        Errore Applicativo Warning
        //           *        (proveniente da controllo caricato dall’utente)
        //                    WHEN ISPC0001-GR-WARNING-UTE(IX-ERR-GENERIC)
        //           *             ERRORE APPLICATIVO DEL SERVIZIO DI ACTUATOR: $
        //           *             (COD-ERR= $ - GRAVITA = $)
        //                            THRU EX-S0322
        //           *        Errore Applicativo Bloccante
        //           *        (proveniente da controllo caricato dall’utente)
        //                    WHEN ISPC0001-GR-BLOCCANTE(IX-ERR-GENERIC)
        //           *             ERRORE APPLICATIVO DEL SERVIZIO DI ACTUATOR: $
        //           *             (COD-ERR= $ - GRAVITA = $)
        //                            THRU EX-S0322
        //           *        Errore Applicativo NON Bloccante
        //           *        (proveniente da controllo caricato dall’utente)
        //                    WHEN ISPC0001-GR-NON-BLOCC(IX-ERR-GENERIC)
        //           *             ERRORE APPLICATIVO DEL SERVIZIO DI ACTUATOR: $
        //           *             (COD-ERR= $ - GRAVITA = $)
        //                            THRU EX-S0322
        //           *        Errore Applicativo Bloccante corrispondente
        //           *        ad un controllo di actuator con esito
        //           *        "Operazione non consentita"
        //                    WHEN ISPC0001-GR-OPER-NON-CONS(IX-ERR-GENERIC)
        //                         END-IF
        //           *        Si tratta di una anomalia derogabile da utenti
        //           *        con livello autorizzativo minore di Livello Deroga
        //                    WHEN OTHER
        //                            THRU EX-S0323
        //                END-EVALUATE.
        switch (ws.getIeav9903().getIspc0001AreaErrori().getTabErrori(ws.getIeav9903().getIxErrGeneric()).getGravitaErr().getIspc0001GravitaErrFormatted()) {

            case Ispc0001GravitaErr.WARNING_ACT:
            case Ispc0001GravitaErr.WARNING_UTE://             ERRORE APPLICATIVO DEL SERVIZIO DI ACTUATOR: $
                //             (COD-ERR= $ - GRAVITA = $)
                // COB_CODE: MOVE '005218'                 TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("005218");
                // COB_CODE: PERFORM S0322-SALVA-ERRORE
                //              THRU EX-S0322
                s0322SalvaErrore();
                //        Errore Applicativo Bloccante
                //        (proveniente da controllo caricato dall’utente)
                break;

            case Ispc0001GravitaErr.BLOCCANTE://             ERRORE APPLICATIVO DEL SERVIZIO DI ACTUATOR: $
                //             (COD-ERR= $ - GRAVITA = $)
                // COB_CODE: MOVE '005220'                 TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("005220");
                // COB_CODE: PERFORM S0322-SALVA-ERRORE
                //              THRU EX-S0322
                s0322SalvaErrore();
                //        Errore Applicativo NON Bloccante
                //        (proveniente da controllo caricato dall’utente)
                break;

            case Ispc0001GravitaErr.NON_BLOCC://             ERRORE APPLICATIVO DEL SERVIZIO DI ACTUATOR: $
                //             (COD-ERR= $ - GRAVITA = $)
                // COB_CODE: MOVE '005219'                 TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("005219");
                // COB_CODE: PERFORM S0322-SALVA-ERRORE
                //              THRU EX-S0322
                s0322SalvaErrore();
                //        Errore Applicativo Bloccante corrispondente
                //        ad un controllo di actuator con esito
                //        "Operazione non consentita"
                break;

            case Ispc0001GravitaErr.OPER_NON_CONS:// COB_CODE:               IF ISPC0001-LIVELLO-DEROGA(IX-ERR-GENERIC) = 0
                //           *                OPERAZIONE NON CONSENTITA DA ACTUATOR: $
                //           *                (COD-ERR= $ - GRAVITA = $)
                //                               THRU EX-S0322
                //                         END-IF
                if (ws.getIeav9903().getIspc0001AreaErrori().getTabErrori(ws.getIeav9903().getIxErrGeneric()).getIspc0001LivelloDeroga() == 0) {
                    //                OPERAZIONE NON CONSENTITA DA ACTUATOR: $
                    //                (COD-ERR= $ - GRAVITA = $)
                    // COB_CODE: MOVE '005221'              TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005221");
                    // COB_CODE: PERFORM S0322-SALVA-ERRORE
                    //              THRU EX-S0322
                    s0322SalvaErrore();
                }
                //        Si tratta di una anomalia derogabile da utenti
                //        con livello autorizzativo minore di Livello Deroga
                break;

            default:// COB_CODE: PERFORM S0323-SALVA-DEROGA
                //              THRU EX-S0323
                s0323SalvaDeroga();
                break;
        }
    }

    /**Original name: S0323-SALVA-DEROGA<br>
	 * <pre>----------------------------------------------------------------*
	 *    SALVA UNA DEROGA NELL'AREA COMUNE
	 * ----------------------------------------------------------------*
	 *     SE LA DEROGA NON E' GIA' PRESENTE LA INSERISCE</pre>*/
    private void s0323SalvaDeroga() {
        // COB_CODE: SET CERCA-DEROGA-ERR                 TO TRUE
        ws.getIeav9903().getWsFlagRicDerogaErr().setCercaDerogaErr();
        // COB_CODE: PERFORM VARYING IX-ERR-DEROG FROM 1 BY 1
        //                     UNTIL IX-ERR-DEROG > WCOM-NUM-ELE-MOT-DEROGA
        //              END-IF
        //           END-PERFORM
        ws.getIeav9903().setIxErrDerog(1);
        while (!(ws.getIeav9903().getIxErrDerog() > lccc0001.getDatiDeroghe().getNumEleMotDeroga())) {
            // COB_CODE: IF  WCOM-COD-ERR(IX-ERR-DEROG)   EQUAL
            //               ISPC0001-COD-ERR(IX-ERR-GENERIC)
            //                  SET DEROGA-PRESENTE-ERR       TO TRUE
            //           END-IF
            if (Conditions.eq(lccc0001.getDatiDeroghe().getTabMotDeroga(ws.getIeav9903().getIxErrDerog()).getCodErr(), ws.getIeav9903().getIspc0001AreaErrori().getTabErrori(ws.getIeav9903().getIxErrGeneric()).getIspc0001CodErr())) {
                // COB_CODE: SET DEROGA-PRESENTE-ERR       TO TRUE
                ws.getIeav9903().getWsFlagRicDerogaErr().setDerogaPresenteErr();
            }
            ws.getIeav9903().setIxErrDerog(Trunc.toInt(ws.getIeav9903().getIxErrDerog() + 1, 4));
        }
        // COB_CODE: IF NOT DEROGA-PRESENTE-ERR
        //                TO WCOM-MOT-DER-STEP-ELAB(WCOM-NUM-ELE-MOT-DEROGA)
        //           END-IF.
        if (!ws.getIeav9903().getWsFlagRicDerogaErr().isDerogaPresenteErr()) {
            // COB_CODE: ADD 1  TO WCOM-NUM-ELE-MOT-DEROGA
            lccc0001.getDatiDeroghe().setNumEleMotDeroga(Trunc.toShort(1 + lccc0001.getDatiDeroghe().getNumEleMotDeroga(), 3));
            // COB_CODE: SET WCOM-ST-ADD(WCOM-NUM-ELE-MOT-DEROGA)
            //            TO TRUE
            lccc0001.getDatiDeroghe().getTabMotDeroga(lccc0001.getDatiDeroghe().getNumEleMotDeroga()).getStatus().setWcomStAdd();
            // COB_CODE: MOVE ZERO
            //             TO WCOM-ID-MOT-DEROGA(WCOM-NUM-ELE-MOT-DEROGA)
            lccc0001.getDatiDeroghe().getTabMotDeroga(lccc0001.getDatiDeroghe().getNumEleMotDeroga()).setIdMotDeroga(0);
            // COB_CODE: MOVE ISPC0001-COD-ERR(IX-ERR-GENERIC)
            //             TO WCOM-COD-ERR(WCOM-NUM-ELE-MOT-DEROGA)
            lccc0001.getDatiDeroghe().getTabMotDeroga(lccc0001.getDatiDeroghe().getNumEleMotDeroga()).setCodErr(ws.getIeav9903().getIspc0001AreaErrori().getTabErrori(ws.getIeav9903().getIxErrGeneric()).getIspc0001CodErr());
            // COB_CODE: MOVE ISPC0001-DESCRIZIONE-ERR(IX-ERR-GENERIC)
            //             TO WCOM-DESCRIZIONE-ERR(WCOM-NUM-ELE-MOT-DEROGA)
            lccc0001.getDatiDeroghe().getTabMotDeroga(lccc0001.getDatiDeroghe().getNumEleMotDeroga()).setDescrizioneErr(ws.getIeav9903().getIspc0001AreaErrori().getTabErrori(ws.getIeav9903().getIxErrGeneric()).getIspc0001DescrizioneErr());
            //       BS = DEROGA DI BUSINESS (CIOE' RESTITUITA DA ACTUATOR)
            // COB_CODE: MOVE 'BS'
            //             TO WCOM-TP-ERR(WCOM-NUM-ELE-MOT-DEROGA)
            lccc0001.getDatiDeroghe().getTabMotDeroga(lccc0001.getDatiDeroghe().getNumEleMotDeroga()).setTpErr("BS");
            // COB_CODE: MOVE ISPC0001-GRAVITA-ERR(IX-ERR-GENERIC)
            //             TO WCOM-TP-MOT-DEROGA(WCOM-NUM-ELE-MOT-DEROGA)
            lccc0001.getDatiDeroghe().getTabMotDeroga(lccc0001.getDatiDeroghe().getNumEleMotDeroga()).setTpMotDeroga(ws.getIeav9903().getIspc0001AreaErrori().getTabErrori(ws.getIeav9903().getIxErrGeneric()).getGravitaErr().getIspc0001GravitaErrFormatted());
            // COB_CODE: MOVE ISPC0001-LIVELLO-DEROGA(IX-ERR-GENERIC)
            //            TO WCOM-COD-LIV-AUTORIZZATIVO(WCOM-NUM-ELE-MOT-DEROGA)
            lccc0001.getDatiDeroghe().getTabMotDeroga(lccc0001.getDatiDeroghe().getNumEleMotDeroga()).setCodLivAutorizzativo(ws.getIeav9903().getIspc0001AreaErrori().getTabErrori(ws.getIeav9903().getIxErrGeneric()).getIspc0001LivelloDeroga());
            // COB_CODE: MOVE ISPC0001-STEP-ELAB
            //             TO WCOM-MOT-DER-STEP-ELAB(WCOM-NUM-ELE-MOT-DEROGA)
            lccc0001.getDatiDeroghe().getTabMotDeroga(lccc0001.getDatiDeroghe().getNumEleMotDeroga()).setMotDerStepElab(ws.getIeav9903().getIspc0001StepElab());
        }
    }

    public void registerListeners() {
        ws.getAreaProductServices().getIjccmq01LengthDatiServizioNotifier().addListener(ws.getAreaProductServices().getIjccmq01CarListener());
    }
}
