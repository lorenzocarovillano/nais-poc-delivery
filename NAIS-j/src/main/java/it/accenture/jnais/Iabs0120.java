package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.date.CalendarUtil;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.LogErroreDao;
import it.accenture.jnais.commons.data.to.ILogErrore;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Iabs0120Data;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.LogErroreIabs0120;
import it.accenture.jnais.ws.redefines.LorTipoOggetto;

/**Original name: IABS0120<br>
 * <pre>AUTHOR.        ATS NAPOLI.
 * DATE-WRITTEN.  23 LUG 2007.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULI PER ACCESSO RISORSE DB               *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Iabs0120 extends Program implements ILogErrore {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private LogErroreDao logErroreDao = new LogErroreDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Iabs0120Data ws = new Iabs0120Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: LOG-ERRORE
    private LogErroreIabs0120 logErrore;

    //==== METHODS ====
    /**Original name: PROGRAM_IABS0120_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, LogErroreIabs0120 logErrore) {
        this.idsv0003 = idsv0003;
        this.logErrore = logErrore;
        // COB_CODE: PERFORM A000-INIZIO                    THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
        //              END-EVALUATE
        //           ELSE
        //             END-IF
        //           END-IF.
        if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
            // COB_CODE: EVALUATE TRUE
            //              WHEN IDSV0003-ID
            //                 PERFORM A300-ELABORA-ID       THRU A300-EX
            //              WHEN IDSV0003-ID-PADRE
            //                 PERFORM A400-ELABORA-IDP      THRU A400-EX
            //              WHEN IDSV0003-IB-OGGETTO
            //                 PERFORM A500-ELABORA-IBO      THRU A500-EX
            //              WHEN IDSV0003-IB-SECONDARIO
            //                 PERFORM A600-ELABORA-IBS      THRU A600-EX
            //              WHEN IDSV0003-ID-OGGETTO
            //                 PERFORM A700-ELABORA-IDO      THRU A700-EX
            //              WHEN OTHER
            //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
            //           END-EVALUATE
            switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM A300-ELABORA-ID       THRU A300-EX
                    a300ElaboraId();
                    break;

                case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM A400-ELABORA-IDP      THRU A400-EX
                    a400ElaboraIdp();
                    break;

                case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO      THRU A500-EX
                    a500ElaboraIbo();
                    break;

                case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS      THRU A600-EX
                    a600ElaboraIbs();
                    break;

                case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO      THRU A700-EX
                    a700ElaboraIdo();
                    break;

                default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                    this.idsv0003.getReturnCode().setInvalidLevelOper();
                    break;
            }
        }
        else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
            // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
            //              END-EVALUATE
            //           ELSE
            //              SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
            //           END-IF
            // COB_CODE: EVALUATE TRUE
            //              WHEN IDSV0003-PRIMARY-KEY
            //                 PERFORM A200-ELABORA-PK       THRU A200-EX
            //              WHEN IDSV0003-ID-OGGETTO
            //                 PERFORM A700-ELABORA-IDO      THRU A700-EX
            //              WHEN OTHER
            //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
            //           END-EVALUATE
            switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                case Idsv0003LivelloOperazione.PRIMARY_KEY:// COB_CODE: PERFORM A200-ELABORA-PK       THRU A200-EX
                    a200ElaboraPk();
                    break;

                case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO      THRU A700-EX
                    a700ElaboraIdo();
                    break;

                default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                    this.idsv0003.getReturnCode().setInvalidLevelOper();
                    break;
            }
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
            this.idsv0003.getReturnCode().setInvalidLevelOper();
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Iabs0120 getInstance() {
        return ((Iabs0120)Programs.getInstance(Iabs0120.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'IABS0120'             TO   IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("IABS0120");
        // COB_CODE: MOVE 'LOG_ERRORE'           TO   IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("LOG_ERRORE");
        // COB_CODE: MOVE '00'                     TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                   TO   IDSV0003-SQLCODE
        //                                              IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                              IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: MOVE ZEROES                  TO WS-TIMESTAMP-NUM.
        ws.getWsTimestamp().setWsTimestampNum(0);
        // COB_CODE: ACCEPT WS-TIMESTAMP(1:8)     FROM DATE YYYYMMDD.
        ws.getWsTimestamp().setWsTimestamp(Functions.setSubstring(ws.getWsTimestamp().getWsTimestamp(), CalendarUtil.getDateYYYYMMDD(), 1, 8));
        // COB_CODE: ACCEPT WS-TIMESTAMP(9:6)     FROM TIME.
        ws.getWsTimestamp().setWsTimestamp(Functions.setSubstring(ws.getWsTimestamp().getWsTimestamp(), CalendarUtil.getTimeHHMMSSMM(), 9, 6));
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-PK<br>*/
    private void a200ElaboraPk() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-PK          THRU A210-EX
        //              WHEN IDSV0003-INSERT
        //                 PERFORM A220-INSERT-PK          THRU A220-EX
        //              WHEN IDSV0003-UPDATE
        //                 PERFORM A230-UPDATE-PK          THRU A230-EX
        //              WHEN IDSV0003-DELETE
        //                 PERFORM A240-DELETE-PK          THRU A240-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-PK          THRU A210-EX
            a210SelectPk();
        }
        else if (idsv0003.getOperazione().isInsert()) {
            // COB_CODE: PERFORM A220-INSERT-PK          THRU A220-EX
            a220InsertPk();
        }
        else if (idsv0003.getOperazione().isUpdate()) {
            // COB_CODE: PERFORM A230-UPDATE-PK          THRU A230-EX
            a230UpdatePk();
        }
        else if (idsv0003.getOperazione().isDelete()) {
            // COB_CODE: PERFORM A240-DELETE-PK          THRU A240-EX
            a240DeletePk();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A300-ELABORA-ID<br>*/
    private void a300ElaboraId() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A310-SELECT-ID              THRU A310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A360-OPEN-CURSOR-ID         THRU A360-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A370-CLOSE-CURSOR-ID        THRU A370-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A380-FETCH-FIRST-ID         THRU A380-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A390-FETCH-NEXT-ID          THRU A390-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A310-SELECT-ID              THRU A310-EX
            a310SelectId();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A360-OPEN-CURSOR-ID         THRU A360-EX
            a360OpenCursorId();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A370-CLOSE-CURSOR-ID        THRU A370-EX
            a370CloseCursorId();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A380-FETCH-FIRST-ID         THRU A380-EX
            a380FetchFirstId();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT-ID          THRU A390-EX
            a390FetchNextId();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A400-ELABORA-IDP<br>*/
    private void a400ElaboraIdp() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A410-SELECT-IDP         THRU A410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A460-OPEN-CURSOR-IDP    THRU A460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A470-CLOSE-CURSOR-IDP   THRU A470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A480-FETCH-FIRST-IDP    THRU A480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A490-FETCH-NEXT-IDP     THRU A490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A410-SELECT-IDP         THRU A410-EX
            a410SelectIdp();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP    THRU A460-EX
            a460OpenCursorIdp();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-IDP   THRU A470-EX
            a470CloseCursorIdp();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A480-FETCH-FIRST-IDP    THRU A480-EX
            a480FetchFirstIdp();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP     THRU A490-EX
            a490FetchNextIdp();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A500-ELABORA-IBO<br>*/
    private void a500ElaboraIbo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A510-SELECT-IBO         THRU A510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A560-OPEN-CURSOR-IBO    THRU A560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A570-CLOSE-CURSOR-IBO   THRU A570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A580-FETCH-FIRST-IBO    THRU A580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A510-SELECT-IBO         THRU A510-EX
            a510SelectIbo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO    THRU A560-EX
            a560OpenCursorIbo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A570-CLOSE-CURSOR-IBO   THRU A570-EX
            a570CloseCursorIbo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A580-FETCH-FIRST-IBO    THRU A580-EX
            a580FetchFirstIbo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
            a590FetchNextIbo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A600-ELABORA-IBS<br>*/
    private void a600ElaboraIbs() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A610-SELECT-IBS         THRU A610-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A610-SELECT-IBS         THRU A610-EX
            a610SelectIbs();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A700-ELABORA-IDO<br>*/
    private void a700ElaboraIdo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A710-SELECT-IDO         THRU A710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A760-OPEN-CURSOR-IDO    THRU A760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A770-CLOSE-CURSOR-IDO   THRU A770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A780-FETCH-FIRST-IDO    THRU A780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A710-SELECT-IDO         THRU A710-EX
            a710SelectIdo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO    THRU A760-EX
            a760OpenCursorIdo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A770-CLOSE-CURSOR-IDO   THRU A770-EX
            a770CloseCursorIdo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A780-FETCH-FIRST-IDO    THRU A780-EX
            a780FetchFirstIdo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
            a790FetchNextIdo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A210-SELECT-PK<br>
	 * <pre>----
	 * ----  gestione PK
	 * ----</pre>*/
    private void a210SelectPk() {
        // COB_CODE: EXEC SQL
        //             SELECT
        //                 ID_LOG_ERRORE
        //                ,PROG_LOG_ERRORE
        //                ,ID_GRAVITA_ERRORE
        //                ,DESC_ERRORE_ESTESA
        //                ,COD_MAIN_BATCH
        //                ,COD_SERVIZIO_BE
        //                ,LABEL_ERR
        //                ,OPER_TABELLA
        //                ,NOME_TABELLA
        //                ,STATUS_TABELLA
        //                ,KEY_TABELLA
        //                ,TIMESTAMP_REG
        //                ,TIPO_OGGETTO
        //                ,IB_OGGETTO
        //             INTO
        //                  :LOR-ID-LOG-ERRORE
        //                 ,:LOR-PROG-LOG-ERRORE
        //                 ,:LOR-ID-GRAVITA-ERRORE
        //                 ,:LOR-DESC-ERRORE-ESTESA-VCHAR
        //                 ,:LOR-COD-MAIN-BATCH
        //                 ,:LOR-COD-SERVIZIO-BE
        //                 ,:LOR-LABEL-ERR
        //                  :IND-LOR-LABEL-ERR
        //                 ,:LOR-OPER-TABELLA
        //                  :IND-LOR-OPER-TABELLA
        //                 ,:LOR-NOME-TABELLA
        //                  :IND-LOR-NOME-TABELLA
        //                 ,:LOR-STATUS-TABELLA
        //                  :IND-LOR-STATUS-TABELLA
        //                 ,:LOR-KEY-TABELLA-VCHAR
        //                  :IND-LOR-KEY-TABELLA
        //                 ,:LOR-TIMESTAMP-REG
        //                 ,:LOR-TIPO-OGGETTO
        //                  :IND-LOR-TIPO-OGGETTO
        //                 ,:LOR-IB-OGGETTO
        //                  :IND-LOR-IB-OGGETTO
        //             FROM LOG_ERRORE
        //             WHERE     ID_LOG_ERRORE = :LOR-ID-LOG-ERRORE
        //                   AND PROG_LOG_ERRORE = :LOR-PROG-LOG-ERRORE
        //           END-EXEC.
        logErroreDao.selectRec(logErrore.getLorIdLogErrore(), logErrore.getLorProgLogErrore(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A220-INSERT-PK<br>*/
    private void a220InsertPk() {
        // COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES THRU Z150-EX.
        z150ValorizzaDataServices();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //              INSERT
        //              INTO LOG_ERRORE
        //                  (
        //                     ID_LOG_ERRORE
        //                    ,PROG_LOG_ERRORE
        //                    ,ID_GRAVITA_ERRORE
        //                    ,DESC_ERRORE_ESTESA
        //                    ,COD_MAIN_BATCH
        //                    ,COD_SERVIZIO_BE
        //                    ,LABEL_ERR
        //                    ,OPER_TABELLA
        //                    ,NOME_TABELLA
        //                    ,STATUS_TABELLA
        //                    ,KEY_TABELLA
        //                    ,TIMESTAMP_REG
        //                    ,TIPO_OGGETTO
        //                    ,IB_OGGETTO
        //                  )
        //              VALUES
        //                  (
        //                  :LOR-ID-LOG-ERRORE
        //                 ,:LOR-PROG-LOG-ERRORE
        //                 ,:LOR-ID-GRAVITA-ERRORE
        //                 ,:LOR-DESC-ERRORE-ESTESA-VCHAR
        //                 ,:LOR-COD-MAIN-BATCH
        //                 ,:LOR-COD-SERVIZIO-BE
        //                 ,:LOR-LABEL-ERR
        //                  :IND-LOR-LABEL-ERR
        //                 ,:LOR-OPER-TABELLA
        //                  :IND-LOR-OPER-TABELLA
        //                 ,:LOR-NOME-TABELLA
        //                  :IND-LOR-NOME-TABELLA
        //                 ,:LOR-STATUS-TABELLA
        //                  :IND-LOR-STATUS-TABELLA
        //                 ,:LOR-KEY-TABELLA-VCHAR
        //                  :IND-LOR-KEY-TABELLA
        //                 ,:LOR-TIMESTAMP-REG
        //                 ,:LOR-TIPO-OGGETTO
        //                  :IND-LOR-TIPO-OGGETTO
        //                 ,:LOR-IB-OGGETTO
        //                  :IND-LOR-IB-OGGETTO
        //                  )
        //           END-EXEC.
        logErroreDao.insertRec(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A230-UPDATE-PK<br>*/
    private void a230UpdatePk() {
        // COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES THRU Z150-EX.
        z150ValorizzaDataServices();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE LOG_ERRORE SET
        //                   ID_LOG_ERRORE          = :LOR-ID-LOG-ERRORE
        //                  ,PROG_LOG_ERRORE        = :LOR-PROG-LOG-ERRORE
        //                  ,ID_GRAVITA_ERRORE      = :LOR-ID-GRAVITA-ERRORE
        //                  ,DESC_ERRORE_ESTESA     =
        //                                        :LOR-DESC-ERRORE-ESTESA-VCHAR
        //                  ,COD_MAIN_BATCH         = :LOR-COD-MAIN-BATCH
        //                  ,COD_SERVIZIO_BE        = :LOR-COD-SERVIZIO-BE
        //                  ,LABEL_ERR              = :LOR-LABEL-ERR
        //                                            :IND-LOR-LABEL-ERR
        //                  ,OPER_TABELLA           = :LOR-OPER-TABELLA
        //                                            :IND-LOR-OPER-TABELLA
        //                  ,NOME_TABELLA           = :LOR-NOME-TABELLA
        //                                            :IND-LOR-NOME-TABELLA
        //                  ,STATUS_TABELLA         = :LOR-STATUS-TABELLA
        //                                            :IND-LOR-STATUS-TABELLA
        //                  ,KEY_TABELLA            = :LOR-KEY-TABELLA-VCHAR
        //                                            :IND-LOR-KEY-TABELLA
        //                  ,TIMESTAMP_REG          = :LOR-TIMESTAMP-REG
        //                  ,TIPO_OGGETTO           = :LOR-TIPO-OGGETTO
        //                                            :IND-LOR-TIPO-OGGETTO
        //                  ,IB_OGGETTO             = :LOR-IB-OGGETTO
        //                                            :IND-LOR-IB-OGGETTO
        //                WHERE     ID_LOG_ERRORE = :LOR-ID-LOG-ERRORE
        //                   AND PROG_LOG_ERRORE = :LOR-PROG-LOG-ERRORE
        //           END-EXEC.
        logErroreDao.updateRec(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A240-DELETE-PK<br>*/
    private void a240DeletePk() {
        // COB_CODE: EXEC SQL
        //                DELETE
        //                FROM LOG_ERRORE
        //                WHERE     ID_LOG_ERRORE = :LOR-ID-LOG-ERRORE
        //                   AND PROG_LOG_ERRORE = :LOR-PROG-LOG-ERRORE
        //           END-EXEC.
        logErroreDao.deleteRec(logErrore.getLorIdLogErrore(), logErrore.getLorProgLogErrore());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A310-SELECT-ID<br>*/
    private void a310SelectId() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A360-OPEN-CURSOR-ID<br>*/
    private void a360OpenCursorId() {
        // COB_CODE: PERFORM A305-DECLARE-CURSOR-ID THRU A305-EX.
        //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IABS0120.cbl:line=436, because the code is unreachable.
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A370-CLOSE-CURSOR-ID<br>*/
    private void a370CloseCursorId() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A380-FETCH-FIRST-ID<br>*/
    private void a380FetchFirstId() {
        // COB_CODE: PERFORM A360-OPEN-CURSOR-ID    THRU A360-EX.
        a360OpenCursorId();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A390-FETCH-NEXT-ID THRU A390-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT-ID THRU A390-EX
            a390FetchNextId();
        }
    }

    /**Original name: A390-FETCH-NEXT-ID<br>*/
    private void a390FetchNextId() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A410-SELECT-IDP<br>*/
    private void a410SelectIdp() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A460-OPEN-CURSOR-IDP<br>*/
    private void a460OpenCursorIdp() {
        // COB_CODE: PERFORM A405-DECLARE-CURSOR-IDP THRU A405-EX.
        //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IABS0120.cbl:line=474, because the code is unreachable.
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A470-CLOSE-CURSOR-IDP<br>*/
    private void a470CloseCursorIdp() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A480-FETCH-FIRST-IDP<br>*/
    private void a480FetchFirstIdp() {
        // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP    THRU A460-EX.
        a460OpenCursorIdp();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A490-FETCH-NEXT-IDP THRU A490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP THRU A490-EX
            a490FetchNextIdp();
        }
    }

    /**Original name: A490-FETCH-NEXT-IDP<br>*/
    private void a490FetchNextIdp() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A510-SELECT-IBO<br>*/
    private void a510SelectIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A560-OPEN-CURSOR-IBO<br>*/
    private void a560OpenCursorIbo() {
        // COB_CODE: PERFORM A505-DECLARE-CURSOR-IBO THRU A505-EX.
        //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IABS0120.cbl:line=512, because the code is unreachable.
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A570-CLOSE-CURSOR-IBO<br>*/
    private void a570CloseCursorIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A580-FETCH-FIRST-IBO<br>*/
    private void a580FetchFirstIbo() {
        // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO    THRU A560-EX.
        a560OpenCursorIbo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A590-FETCH-NEXT-IBO THRU A590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO THRU A590-EX
            a590FetchNextIbo();
        }
    }

    /**Original name: A590-FETCH-NEXT-IBO<br>*/
    private void a590FetchNextIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A610-SELECT-IBS<br>
	 * <pre>----
	 * ----  gestione IBS
	 * ----</pre>*/
    private void a610SelectIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A710-SELECT-IDO<br>*/
    private void a710SelectIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A760-OPEN-CURSOR-IDO<br>*/
    private void a760OpenCursorIdo() {
        // COB_CODE: PERFORM A705-DECLARE-CURSOR-IDO THRU A705-EX.
        //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IABS0120.cbl:line=557, because the code is unreachable.
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A770-CLOSE-CURSOR-IDO<br>*/
    private void a770CloseCursorIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A780-FETCH-FIRST-IDO<br>*/
    private void a780FetchFirstIdo() {
        // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO    THRU A760-EX.
        a760OpenCursorIdo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A790-FETCH-NEXT-IDO THRU A790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO THRU A790-EX
            a790FetchNextIdo();
        }
    }

    /**Original name: A790-FETCH-NEXT-IDO<br>*/
    private void a790FetchNextIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>----
	 * ----  utilità comuni a tutti i livelli operazione
	 * ----</pre>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-LOR-LABEL-ERR = -1
        //              MOVE HIGH-VALUES TO LOR-LABEL-ERR
        //           END-IF
        if (ws.getIndLogErrore().getLabelErr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LOR-LABEL-ERR
            logErrore.setLorLabelErr(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LogErroreIabs0120.Len.LOR_LABEL_ERR));
        }
        // COB_CODE: IF IND-LOR-OPER-TABELLA = -1
        //              MOVE HIGH-VALUES TO LOR-OPER-TABELLA-NULL
        //           END-IF
        if (ws.getIndLogErrore().getOperTabella() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LOR-OPER-TABELLA-NULL
            logErrore.setLorOperTabella(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LogErroreIabs0120.Len.LOR_OPER_TABELLA));
        }
        // COB_CODE: IF IND-LOR-NOME-TABELLA = -1
        //              MOVE HIGH-VALUES TO LOR-NOME-TABELLA
        //           END-IF
        if (ws.getIndLogErrore().getNomeTabella() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LOR-NOME-TABELLA
            logErrore.setLorNomeTabella(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LogErroreIabs0120.Len.LOR_NOME_TABELLA));
        }
        // COB_CODE: IF IND-LOR-STATUS-TABELLA = -1
        //              MOVE HIGH-VALUES TO LOR-STATUS-TABELLA
        //           END-IF
        if (ws.getIndLogErrore().getStatusTabella() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LOR-STATUS-TABELLA
            logErrore.setLorStatusTabella(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LogErroreIabs0120.Len.LOR_STATUS_TABELLA));
        }
        // COB_CODE: IF IND-LOR-KEY-TABELLA = -1
        //              MOVE HIGH-VALUES TO LOR-KEY-TABELLA
        //           END-IF
        if (ws.getIndLogErrore().getKeyTabella() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LOR-KEY-TABELLA
            logErrore.setLorKeyTabella(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LogErroreIabs0120.Len.LOR_KEY_TABELLA));
        }
        // COB_CODE: IF IND-LOR-TIPO-OGGETTO = -1
        //              MOVE HIGH-VALUES TO LOR-TIPO-OGGETTO-NULL
        //           END-IF
        if (ws.getIndLogErrore().getTipoOggetto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LOR-TIPO-OGGETTO-NULL
            logErrore.getLorTipoOggetto().setLorTipoOggettoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LorTipoOggetto.Len.LOR_TIPO_OGGETTO_NULL));
        }
        // COB_CODE: IF IND-LOR-IB-OGGETTO = -1
        //              MOVE HIGH-VALUES TO LOR-IB-OGGETTO
        //           END-IF.
        if (ws.getIndLogErrore().getIbOggetto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LOR-IB-OGGETTO
            logErrore.setLorIbOggetto(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LogErroreIabs0120.Len.LOR_IB_OGGETTO));
        }
    }

    /**Original name: Z150-VALORIZZA-DATA-SERVICES<br>*/
    private void z150ValorizzaDataServices() {
        // COB_CODE: PERFORM Z180-MAX-PROG-LOG-ERRORE THRU Z180-EX.
        z180MaxProgLogErrore();
        // COB_CODE: MOVE WS-TIMESTAMP-NUM        TO LOR-TIMESTAMP-REG.
        logErrore.setLorTimestampReg(ws.getWsTimestamp().getWsTimestampNum());
    }

    /**Original name: Z180-MAX-PROG-LOG-ERRORE<br>*/
    private void z180MaxProgLogErrore() {
        // COB_CODE: EXEC SQL
        //                 SELECT VALUE(MAX(PROG_LOG_ERRORE) , 0) + 1
        //                 INTO :LOR-PROG-LOG-ERRORE
        //                 FROM LOG_ERRORE
        //                 WHERE ID_LOG_ERRORE = :LOR-ID-LOG-ERRORE
        //           END-EXEC.
        logErrore.setLorProgLogErrore(logErroreDao.selectByLorIdLogErrore(logErrore.getLorIdLogErrore(), logErrore.getLorProgLogErrore()));
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE     THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: Z200-SET-INDICATORI-NULL<br>*/
    private void z200SetIndicatoriNull() {
        // COB_CODE: IF LOR-LABEL-ERR = HIGH-VALUES
        //              MOVE -1 TO IND-LOR-LABEL-ERR
        //           ELSE
        //              MOVE 0 TO IND-LOR-LABEL-ERR
        //           END-IF
        if (Characters.EQ_HIGH.test(logErrore.getLorLabelErr(), LogErroreIabs0120.Len.LOR_LABEL_ERR)) {
            // COB_CODE: MOVE -1 TO IND-LOR-LABEL-ERR
            ws.getIndLogErrore().setLabelErr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LOR-LABEL-ERR
            ws.getIndLogErrore().setLabelErr(((short)0));
        }
        // COB_CODE: IF LOR-OPER-TABELLA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LOR-OPER-TABELLA
        //           ELSE
        //              MOVE 0 TO IND-LOR-OPER-TABELLA
        //           END-IF
        if (Characters.EQ_HIGH.test(logErrore.getLorOperTabellaFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LOR-OPER-TABELLA
            ws.getIndLogErrore().setOperTabella(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LOR-OPER-TABELLA
            ws.getIndLogErrore().setOperTabella(((short)0));
        }
        // COB_CODE: IF LOR-NOME-TABELLA = HIGH-VALUES
        //              MOVE -1 TO IND-LOR-NOME-TABELLA
        //           ELSE
        //              MOVE 0 TO IND-LOR-NOME-TABELLA
        //           END-IF
        if (Characters.EQ_HIGH.test(logErrore.getLorNomeTabellaFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LOR-NOME-TABELLA
            ws.getIndLogErrore().setNomeTabella(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LOR-NOME-TABELLA
            ws.getIndLogErrore().setNomeTabella(((short)0));
        }
        // COB_CODE: IF LOR-STATUS-TABELLA = HIGH-VALUES
        //              MOVE -1 TO IND-LOR-STATUS-TABELLA
        //           ELSE
        //              MOVE 0 TO IND-LOR-STATUS-TABELLA
        //           END-IF
        if (Characters.EQ_HIGH.test(logErrore.getLorStatusTabellaFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LOR-STATUS-TABELLA
            ws.getIndLogErrore().setStatusTabella(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LOR-STATUS-TABELLA
            ws.getIndLogErrore().setStatusTabella(((short)0));
        }
        // COB_CODE: IF LOR-KEY-TABELLA = HIGH-VALUES
        //              MOVE -1 TO IND-LOR-KEY-TABELLA
        //           ELSE
        //              MOVE 0 TO IND-LOR-KEY-TABELLA
        //           END-IF
        if (Characters.EQ_HIGH.test(logErrore.getLorKeyTabella(), LogErroreIabs0120.Len.LOR_KEY_TABELLA)) {
            // COB_CODE: MOVE -1 TO IND-LOR-KEY-TABELLA
            ws.getIndLogErrore().setKeyTabella(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LOR-KEY-TABELLA
            ws.getIndLogErrore().setKeyTabella(((short)0));
        }
        // COB_CODE: IF LOR-TIPO-OGGETTO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-LOR-TIPO-OGGETTO
        //           ELSE
        //              MOVE 0 TO IND-LOR-TIPO-OGGETTO
        //           END-IF
        if (Characters.EQ_HIGH.test(logErrore.getLorTipoOggetto().getLorTipoOggettoNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LOR-TIPO-OGGETTO
            ws.getIndLogErrore().setTipoOggetto(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LOR-TIPO-OGGETTO
            ws.getIndLogErrore().setTipoOggetto(((short)0));
        }
        // COB_CODE: IF LOR-IB-OGGETTO = HIGH-VALUES
        //              MOVE -1 TO IND-LOR-IB-OGGETTO
        //           ELSE
        //              MOVE 0 TO IND-LOR-IB-OGGETTO
        //           END-IF.
        if (Characters.EQ_HIGH.test(logErrore.getLorIbOggettoFormatted())) {
            // COB_CODE: MOVE -1 TO IND-LOR-IB-OGGETTO
            ws.getIndLogErrore().setIbOggetto(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-LOR-IB-OGGETTO
            ws.getIndLogErrore().setIbOggetto(((short)0));
        }
    }

    /**Original name: Z500-AGGIORNAMENTO-STORICO<br>*/
    private void z500AggiornamentoStorico() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z900-CONVERTI-N-TO-X<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da 9(8) comp-3 a date
	 * ----</pre>*/
    private void z900ConvertiNToX() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z960-LENGTH-VCHAR<br>*/
    private void z960LengthVchar() {
        // COB_CODE: MOVE LENGTH OF LOR-DESC-ERRORE-ESTESA
        //                       TO LOR-DESC-ERRORE-ESTESA-LEN
        logErrore.setLorDescErroreEstesaLen(((short)LogErroreIabs0120.Len.LOR_DESC_ERRORE_ESTESA));
        // COB_CODE: MOVE LENGTH OF LOR-KEY-TABELLA
        //                       TO LOR-KEY-TABELLA-LEN.
        logErrore.setLorKeyTabellaLen(((short)LogErroreIabs0120.Len.LOR_KEY_TABELLA));
    }

    @Override
    public String getCodMainBatch() {
        return logErrore.getLorCodMainBatch();
    }

    @Override
    public void setCodMainBatch(String codMainBatch) {
        this.logErrore.setLorCodMainBatch(codMainBatch);
    }

    @Override
    public String getCodServizioBe() {
        return logErrore.getLorCodServizioBe();
    }

    @Override
    public void setCodServizioBe(String codServizioBe) {
        this.logErrore.setLorCodServizioBe(codServizioBe);
    }

    @Override
    public String getDescErroreEstesaVchar() {
        return logErrore.getLorDescErroreEstesaVcharFormatted();
    }

    @Override
    public void setDescErroreEstesaVchar(String descErroreEstesaVchar) {
        this.logErrore.setLorDescErroreEstesaVcharFormatted(descErroreEstesaVchar);
    }

    @Override
    public String getIbOggetto() {
        return logErrore.getLorIbOggetto();
    }

    @Override
    public void setIbOggetto(String ibOggetto) {
        this.logErrore.setLorIbOggetto(ibOggetto);
    }

    @Override
    public String getIbOggettoObj() {
        if (ws.getIndLogErrore().getIbOggetto() >= 0) {
            return getIbOggetto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIbOggettoObj(String ibOggettoObj) {
        if (ibOggettoObj != null) {
            setIbOggetto(ibOggettoObj);
            ws.getIndLogErrore().setIbOggetto(((short)0));
        }
        else {
            ws.getIndLogErrore().setIbOggetto(((short)-1));
        }
    }

    @Override
    public int getIdGravitaErrore() {
        return logErrore.getLorIdGravitaErrore();
    }

    @Override
    public void setIdGravitaErrore(int idGravitaErrore) {
        this.logErrore.setLorIdGravitaErrore(idGravitaErrore);
    }

    @Override
    public int getIdLogErrore() {
        return logErrore.getLorIdLogErrore();
    }

    @Override
    public void setIdLogErrore(int idLogErrore) {
        this.logErrore.setLorIdLogErrore(idLogErrore);
    }

    @Override
    public String getKeyTabellaVchar() {
        return logErrore.getLorKeyTabellaVcharFormatted();
    }

    @Override
    public void setKeyTabellaVchar(String keyTabellaVchar) {
        this.logErrore.setLorKeyTabellaVcharFormatted(keyTabellaVchar);
    }

    @Override
    public String getKeyTabellaVcharObj() {
        if (ws.getIndLogErrore().getKeyTabella() >= 0) {
            return getKeyTabellaVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setKeyTabellaVcharObj(String keyTabellaVcharObj) {
        if (keyTabellaVcharObj != null) {
            setKeyTabellaVchar(keyTabellaVcharObj);
            ws.getIndLogErrore().setKeyTabella(((short)0));
        }
        else {
            ws.getIndLogErrore().setKeyTabella(((short)-1));
        }
    }

    @Override
    public String getLabelErr() {
        return logErrore.getLorLabelErr();
    }

    @Override
    public void setLabelErr(String labelErr) {
        this.logErrore.setLorLabelErr(labelErr);
    }

    @Override
    public String getLabelErrObj() {
        if (ws.getIndLogErrore().getLabelErr() >= 0) {
            return getLabelErr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setLabelErrObj(String labelErrObj) {
        if (labelErrObj != null) {
            setLabelErr(labelErrObj);
            ws.getIndLogErrore().setLabelErr(((short)0));
        }
        else {
            ws.getIndLogErrore().setLabelErr(((short)-1));
        }
    }

    @Override
    public String getNomeTabella() {
        return logErrore.getLorNomeTabella();
    }

    @Override
    public void setNomeTabella(String nomeTabella) {
        this.logErrore.setLorNomeTabella(nomeTabella);
    }

    @Override
    public String getNomeTabellaObj() {
        if (ws.getIndLogErrore().getNomeTabella() >= 0) {
            return getNomeTabella();
        }
        else {
            return null;
        }
    }

    @Override
    public void setNomeTabellaObj(String nomeTabellaObj) {
        if (nomeTabellaObj != null) {
            setNomeTabella(nomeTabellaObj);
            ws.getIndLogErrore().setNomeTabella(((short)0));
        }
        else {
            ws.getIndLogErrore().setNomeTabella(((short)-1));
        }
    }

    @Override
    public String getOperTabella() {
        return logErrore.getLorOperTabella();
    }

    @Override
    public void setOperTabella(String operTabella) {
        this.logErrore.setLorOperTabella(operTabella);
    }

    @Override
    public String getOperTabellaObj() {
        if (ws.getIndLogErrore().getOperTabella() >= 0) {
            return getOperTabella();
        }
        else {
            return null;
        }
    }

    @Override
    public void setOperTabellaObj(String operTabellaObj) {
        if (operTabellaObj != null) {
            setOperTabella(operTabellaObj);
            ws.getIndLogErrore().setOperTabella(((short)0));
        }
        else {
            ws.getIndLogErrore().setOperTabella(((short)-1));
        }
    }

    @Override
    public int getProgLogErrore() {
        return logErrore.getLorProgLogErrore();
    }

    @Override
    public void setProgLogErrore(int progLogErrore) {
        this.logErrore.setLorProgLogErrore(progLogErrore);
    }

    @Override
    public String getStatusTabella() {
        return logErrore.getLorStatusTabella();
    }

    @Override
    public void setStatusTabella(String statusTabella) {
        this.logErrore.setLorStatusTabella(statusTabella);
    }

    @Override
    public String getStatusTabellaObj() {
        if (ws.getIndLogErrore().getStatusTabella() >= 0) {
            return getStatusTabella();
        }
        else {
            return null;
        }
    }

    @Override
    public void setStatusTabellaObj(String statusTabellaObj) {
        if (statusTabellaObj != null) {
            setStatusTabella(statusTabellaObj);
            ws.getIndLogErrore().setStatusTabella(((short)0));
        }
        else {
            ws.getIndLogErrore().setStatusTabella(((short)-1));
        }
    }

    @Override
    public long getTimestampReg() {
        return logErrore.getLorTimestampReg();
    }

    @Override
    public void setTimestampReg(long timestampReg) {
        this.logErrore.setLorTimestampReg(timestampReg);
    }

    @Override
    public short getTipoOggetto() {
        return logErrore.getLorTipoOggetto().getLorTipoOggetto();
    }

    @Override
    public void setTipoOggetto(short tipoOggetto) {
        this.logErrore.getLorTipoOggetto().setLorTipoOggetto(tipoOggetto);
    }

    @Override
    public Short getTipoOggettoObj() {
        if (ws.getIndLogErrore().getTipoOggetto() >= 0) {
            return ((Short)getTipoOggetto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTipoOggettoObj(Short tipoOggettoObj) {
        if (tipoOggettoObj != null) {
            setTipoOggetto(((short)tipoOggettoObj));
            ws.getIndLogErrore().setTipoOggetto(((short)0));
        }
        else {
            ws.getIndLogErrore().setTipoOggetto(((short)-1));
        }
    }
}
