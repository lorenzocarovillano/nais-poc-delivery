package it.accenture.jnais;

import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Idsv0003CampiEsito;
import it.accenture.jnais.ws.enums.Idsv0003Sqlcode;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.Lvvs3160Data;
import it.accenture.jnais.ws.redefines.Wp61CptIni30062014;
import it.accenture.jnais.ws.redefines.Wp61CptRivto31122011;
import it.accenture.jnais.ws.redefines.Wp61IdAdes;
import it.accenture.jnais.ws.redefines.Wp61IdMoviChiu;
import it.accenture.jnais.ws.redefines.Wp61IdTrchDiGar;
import it.accenture.jnais.ws.redefines.Wp61ImpbIs30062014;
import it.accenture.jnais.ws.redefines.Wp61ImpbIs31122011;
import it.accenture.jnais.ws.redefines.Wp61ImpbIsRpP2011;
import it.accenture.jnais.ws.redefines.Wp61ImpbIsRpP62014;
import it.accenture.jnais.ws.redefines.Wp61ImpbVis30062014;
import it.accenture.jnais.ws.redefines.Wp61ImpbVis31122011;
import it.accenture.jnais.ws.redefines.Wp61ImpbVisRpP2011;
import it.accenture.jnais.ws.redefines.Wp61ImpbVisRpP62014;
import it.accenture.jnais.ws.redefines.Wp61MontLrdDal2007;
import it.accenture.jnais.ws.redefines.Wp61MontLrdEnd2000;
import it.accenture.jnais.ws.redefines.Wp61MontLrdEnd2006;
import it.accenture.jnais.ws.redefines.Wp61PreLrdDal2007;
import it.accenture.jnais.ws.redefines.Wp61PreLrdEnd2000;
import it.accenture.jnais.ws.redefines.Wp61PreLrdEnd2006;
import it.accenture.jnais.ws.redefines.Wp61PreRshV30062014;
import it.accenture.jnais.ws.redefines.Wp61PreRshV31122011;
import it.accenture.jnais.ws.redefines.Wp61PreV30062014;
import it.accenture.jnais.ws.redefines.Wp61PreV31122011;
import it.accenture.jnais.ws.redefines.Wp61RendtoLrdDal2007;
import it.accenture.jnais.ws.redefines.Wp61RendtoLrdEnd2000;
import it.accenture.jnais.ws.redefines.Wp61RendtoLrdEnd2006;
import it.accenture.jnais.ws.redefines.Wp61RisMat30062014;
import it.accenture.jnais.ws.redefines.Wp61RisMat31122011;
import static java.lang.Math.abs;

/**Original name: LVVS3160<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2014.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *   PROGRAMMA...... LVVS3160
 *   TIPOLOGIA...... SERVIZIO
 *   PROCESSO....... XXX
 *   FUNZIONE....... XXX
 *   DESCRIZIONE.... NUOVA FISCALITA' 2014
 *                   CALCOLO DELLA VARIABILE VLRDVISE2014
 * **------------------------------------------------------------***</pre>*/
public class Lvvs3160 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lvvs3160Data ws = new Lvvs3160Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS3160
    private Ivvc0213 ivvc0213;

    //==== METHODS ====
    /**Original name: PROGRAM_LVVS3160_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: PERFORM S1000-ELABORAZIONE
        //              THRU EX-S1000
        s1000Elaborazione();
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lvvs3160 getInstance() {
        return ((Lvvs3160)Programs.getInstance(Lvvs3160.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                        IX-INDICI
        //                                             IVVC0213-TAB-OUTPUT.
        initIxIndici();
        initTabOutput();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET COMUN-TROV-NO                 TO TRUE.
        ws.getFlagComunTrov().setNo();
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE
        //             TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: INITIALIZE AREA-IO-D-CRIST
        //                      AREA-IO-D-CRIST-COLL.
        initAreaIoDCrist();
        initAreaIoDCristColl();
        // COB_CODE: PERFORM L450-LEGGI-POLIZZA
        //              THRU L450-LEGGI-POLIZZA-EX
        l450LeggiPolizza();
        // COB_CODE: MOVE IVVC0213-TIPO-MOVIMENTO
        //             TO WS-MOVI-ORI-COLL-COM
        //                WS-MOVI-ORI-COLL
        ws.getWsMoviOriCollCom().setWsMoviOriCollComFormatted(ivvc0213.getTipoMovimentoFormatted());
        ws.getWsMoviOriColl().setWsMoviOriCollFormatted(ivvc0213.getTipoMovimentoFormatted());
        //    IF IVVC0213-TIPO-MOVIMENTO = WS-MOVI-ORI-COLL-COM
        //    OR IVVC0213-TIPO-MOVIMENTO = WS-MOVI-ORI-COLL
        //       SET FL-TIPO-COLLETTIVA   TO TRUE
        //    ELSE
        //       SET FL-TIPO-INDIVIDUALE  TO TRUE
        //    END-IF
        // COB_CODE: IF FL-TIPO-INDIVIDUALE
        //                 THRU VERIFICA-MOVIMENTO-EX
        //           ELSE
        //                 THRU LEGGI-D-CRIST-X-ADE-EX
        //           END-IF
        if (ws.getFlTipoPolizza().isIndividuale()) {
            // COB_CODE: PERFORM VERIFICA-MOVIMENTO
            //              THRU VERIFICA-MOVIMENTO-EX
            verificaMovimento();
        }
        else {
            // COB_CODE: PERFORM LEGGI-D-CRIST-X-ADE
            //              THRU LEGGI-D-CRIST-X-ADE-EX
            leggiDCristXAde();
        }
        //--> PERFORM DI CALCOLO ANNO CORRENTE
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //                  THRU S1150-CALCOLO-ANNO-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM S1150-CALCOLO-ANNO
            //              THRU S1150-CALCOLO-ANNO-EX
            s1150CalcoloAnno();
        }
        //--> PERFORM DI CALCOLO DELL'IMPORTO
        // COB_CODE: IF FL-TIPO-INDIVIDUALE
        //              END-IF
        //           ELSE
        //              END-IF
        //           END-IF.
        if (ws.getFlTipoPolizza().isIndividuale()) {
            // COB_CODE:         IF  IDSV0003-SUCCESSFUL-RC
            //           *    AND IDSV0003-SUCCESSFUL-SQL
            //                          THRU S1200-CALCOLO-IMPORTO-EX
            //                   END-IF
            if (idsv0003.getReturnCode().isSuccessfulRc()) {
                //    AND IDSV0003-SUCCESSFUL-SQL
                // COB_CODE: PERFORM S1200-CALCOLO-IMPORTO
                //              THRU S1200-CALCOLO-IMPORTO-EX
                s1200CalcoloImporto();
            }
        }
        else if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
            //                 THRU S1201-CALC-IMP-COLL-EX
            //           END-IF
            // COB_CODE: PERFORM S1201-CALC-IMP-COLL
            //              THRU S1201-CALC-IMP-COLL-EX
            s1201CalcImpColl();
        }
    }

    /**Original name: L450-LEGGI-POLIZZA<br>
	 * <pre>----------------------------------------------------------------*
	 * -- LETTURA DELLA POLIZZA
	 * ----------------------------------------------------------------*</pre>*/
    private void l450LeggiPolizza() {
        Idbspol0 idbspol0 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE POLI.
        initPoli();
        // COB_CODE: SET FL-TIPO-INDIVIDUALE TO TRUE
        ws.getFlTipoPolizza().setIndividuale();
        // COB_CODE: MOVE IVVC0213-ID-POLIZZA       TO POL-ID-POLI.
        ws.getPoli().setPolIdPoli(ivvc0213.getIdPolizza());
        // COB_CODE: MOVE 'IDBSPOL0'                TO WK-PGM-CALLED.
        ws.setWkPgmCalled("IDBSPOL0");
        //  --> Tipo operazione
        // COB_CODE: SET IDSV0003-SELECT            TO TRUE.
        idsv0003.getOperazione().setSelect();
        //  --> Tipo livello
        // COB_CODE: SET IDSV0003-ID                TO TRUE.
        idsv0003.getLivelloOperazione().setIdsi0011Id();
        // COB_CODE: CALL WK-PGM-CALLED USING IDSV0003 POLI
        //           ON EXCEPTION
        //              SET IDSV0003-INVALID-OPER   TO TRUE
        //           END-CALL.
        try {
            idbspol0 = Idbspol0.getInstance();
            idbspol0.run(idsv0003, ws.getPoli());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-PGM-CALLED          TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgmCalled());
            // COB_CODE: MOVE 'CALL-IDBSPOL0 ERRORE CHIAMATA'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("CALL-IDBSPOL0 ERRORE CHIAMATA");
            // COB_CODE: SET IDSV0003-INVALID-OPER   TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              END-IF
        //           ELSE
        //              END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: IF POL-TP-FRM-ASSVA = 'IN'
            //              SET FL-TIPO-INDIVIDUALE TO TRUE
            //           ELSE
            //              SET FL-TIPO-COLLETTIVA TO TRUE
            //           END-IF
            if (Conditions.eq(ws.getPoli().getPolTpFrmAssva(), "IN")) {
                // COB_CODE: SET FL-TIPO-INDIVIDUALE TO TRUE
                ws.getFlTipoPolizza().setIndividuale();
            }
            else {
                // COB_CODE: SET FL-TIPO-COLLETTIVA TO TRUE
                ws.getFlTipoPolizza().setCollettiva();
            }
        }
        else {
            // COB_CODE: MOVE WK-PGM-CALLED          TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgmCalled());
            // COB_CODE: STRING 'CHIAMATA IDBSPOL0 ;'
            //                  IDSV0003-RETURN-CODE ';'
            //                  IDSV0003-SQLCODE
            //                  DELIMITED BY SIZE INTO
            //                  IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA IDBSPOL0 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              SET IDSV0003-FIELD-NOT-VALUED TO TRUE
            //           ELSE
            //              SET IDSV0003-INVALID-OPER     TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isNotFound()) {
                // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-OPER     TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
        }
    }

    /**Original name: LEGGI-D-CRIST-X-ADE<br>
	 * <pre>----------------------------------------------------------------*
	 * --> PER LE COLLETTIVE VIENE LETTA D CRISTALLIZATI PER ADESIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void leggiDCristXAde() {
        Ldbsh650 ldbsh650 = null;
        // COB_CODE: SET FINE-LETT-P61-NO           TO TRUE
        ws.getFlagFineLetturaP61().setNo();
        // COB_CODE: INITIALIZE D-CRIST
        initDCrist();
        // COB_CODE: MOVE ZEROES
        //             TO WK-TOTALE
        ws.setWkTotale(new AfDecimal(0, 18, 7));
        //--> TIPO OPERAZIONE
        // COB_CODE: SET IDSV0003-FETCH-FIRST       TO TRUE
        idsv0003.getOperazione().setFetchFirst();
        //--> INIZIALIZZA CODICE DI RITORNO
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC    TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        //
        // COB_CODE: PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC
        //                      OR FINE-LETT-P61-SI
        //                 END-IF
        //           END-PERFORM.
        while (!(!idsv0003.getReturnCode().isSuccessfulRc() || ws.getFlagFineLetturaP61().isSi())) {
            // COB_CODE: INITIALIZE D-CRIST
            initDCrist();
            //
            // COB_CODE: MOVE IVVC0213-ID-ADESIONE   TO P61-ID-ADES
            ws.getdCrist().getP61IdAdes().setP61IdAdes(ivvc0213.getIdAdesione());
            // COB_CODE: SET IDSV0003-TRATT-X-COMPETENZA TO TRUE
            idsv0003.getTrattamentoStoricita().setTrattXCompetenza();
            //--> LIVELLO OPERAZIONE
            // COB_CODE: SET IDSV0003-WHERE-CONDITION    TO TRUE
            idsv0003.getLivelloOperazione().setWhereCondition();
            //--> INIZIALIZZA CODICE DI RITORNO
            // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC  TO TRUE
            idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
            // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL TO TRUE
            idsv0003.getSqlcode().setSuccessfulSql();
            // COB_CODE: CALL  LDBSH650   USING IDSV0003 D-CRIST
            //             ON EXCEPTION
            //                SET IDSV0003-INVALID-OPER   TO TRUE
            //           END-CALL
            try {
                ldbsh650 = Ldbsh650.getInstance();
                ldbsh650.run(idsv0003, ws.getdCrist());
            }
            catch (ProgramExecutionException __ex) {
                // COB_CODE: MOVE LDBSH650          TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbsh650());
                // COB_CODE: MOVE 'CALL-LDBSH650 ERRORE CHIAMATA'
                //             TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBSH650 ERRORE CHIAMATA");
                // COB_CODE: SET IDSV0003-INVALID-OPER   TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
            // COB_CODE:           IF IDSV0003-SUCCESSFUL-RC
            //                         END-EVALUATE
            //                      ELSE
            //           *--> GESTIRE ERRORE
            //                         SET FINE-LETT-P61-SI TO TRUE
            //                      END-IF
            if (idsv0003.getReturnCode().isSuccessfulRc()) {
                // COB_CODE:               EVALUATE TRUE
                //                             WHEN IDSV0003-NOT-FOUND
                //           *-->    NESSUN DATO IN TABELLA
                //                               END-IF
                //                             WHEN IDSV0003-SUCCESSFUL-SQL
                //           *-->   OPERAZIONE ESEGUITA CORRETTAMENTE
                //                                  SET IDSV0003-FETCH-NEXT   TO TRUE
                //                             WHEN OTHER
                //           *--->   ERRORE DI ACCESSO AL DB
                //                                  SET FINE-LETT-P61-SI TO TRUE
                //                         END-EVALUATE
                switch (idsv0003.getSqlcode().getSqlcode()) {

                    case Idsv0003Sqlcode.NOT_FOUND://-->    NESSUN DATO IN TABELLA
                        // COB_CODE: SET FINE-LETT-P61-SI           TO TRUE
                        ws.getFlagFineLetturaP61().setSi();
                        // COB_CODE: IF NOT IDSV0003-FETCH-FIRST
                        //              SET IDSV0003-SUCCESSFUL-RC  TO TRUE
                        //           END-IF
                        if (!idsv0003.getOperazione().isFetchFirst()) {
                            // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL TO TRUE
                            idsv0003.getSqlcode().setSuccessfulSql();
                            // COB_CODE: SET IDSV0003-SUCCESSFUL-RC  TO TRUE
                            idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
                        }
                        break;

                    case Idsv0003Sqlcode.SUCCESSFUL_SQL://-->   OPERAZIONE ESEGUITA CORRETTAMENTE
                        // COB_CODE: IF P61-IMPB-VIS-RP-P62014-NULL NOT = HIGH-VALUE
                        //                                         AND LOW-VALUES
                        //                                         AND SPACES
                        //                 TO WK-TOTALE
                        //           END-IF
                        if (!Characters.EQ_HIGH.test(ws.getdCrist().getP61ImpbVisRpP62014().getP61ImpbVisRpP62014NullFormatted()) && !Characters.EQ_LOW.test(ws.getdCrist().getP61ImpbVisRpP62014().getP61ImpbVisRpP62014NullFormatted()) && !Characters.EQ_SPACE.test(ws.getdCrist().getP61ImpbVisRpP62014().getP61ImpbVisRpP62014Null())) {
                            // COB_CODE: ADD P61-IMPB-VIS-RP-P62014
                            //             TO WK-TOTALE
                            ws.setWkTotale(Trunc.toDecimal(ws.getdCrist().getP61ImpbVisRpP62014().getP61ImpbVisRpP62014().add(ws.getWkTotale()), 18, 7));
                        }
                        // COB_CODE: SET IDSV0003-FETCH-NEXT   TO TRUE
                        idsv0003.getOperazione().setFetchNext();
                        break;

                    default://--->   ERRORE DI ACCESSO AL DB
                        // COB_CODE: MOVE LDBSH650
                        //             TO IDSV0003-COD-SERVIZIO-BE
                        idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbsh650());
                        // COB_CODE: MOVE 'CALL-LDBSH650 ERRORE CHIAMATA'
                        //             TO IDSV0003-DESCRIZ-ERR-DB2
                        idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBSH650 ERRORE CHIAMATA");
                        // COB_CODE: SET IDSV0003-INVALID-OPER   TO TRUE
                        idsv0003.getReturnCode().setInvalidOper();
                        // COB_CODE: SET FINE-LETT-P61-SI TO TRUE
                        ws.getFlagFineLetturaP61().setSi();
                        break;
                }
            }
            else {
                //--> GESTIRE ERRORE
                // COB_CODE: MOVE LDBSH650
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbsh650());
                // COB_CODE: MOVE 'CALL-LDBSH650 ERRORE CHIAMATA'
                //             TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBSH650 ERRORE CHIAMATA");
                // COB_CODE: SET IDSV0003-INVALID-OPER   TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
                // COB_CODE: SET FINE-LETT-P61-SI TO TRUE
                ws.getFlagFineLetturaP61().setSi();
            }
        }
    }

    /**Original name: S1201-CALC-IMP-COLL<br>
	 * <pre>----------------------------------------------------------------*
	 *    CALCOLO IMPORTO PER LE COLLETTIVE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1201CalcImpColl() {
        // COB_CODE: MOVE 0   TO IVVC0213-VAL-IMP-O
        ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(0, 18, 7));
        //    MOVE 0   TO WK-TOTALE
        //    IF WK-DATA-FINE-D <= 20120101
        // COB_CODE: IF WK-TOTALE IS NUMERIC
        //           AND WK-TOTALE > ZEROES
        //               MOVE WK-TOTALE TO IVVC0213-VAL-IMP-O
        //           ELSE
        //              MOVE ZEROES   TO WK-TOTALE
        //           END-IF.
        if (Functions.isNumber(ws.getWkTotale()) && ws.getWkTotale().compareTo(0) > 0) {
            // COB_CODE: MOVE WK-TOTALE TO IVVC0213-VAL-IMP-O
            ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(ws.getWkTotale(), 18, 7));
        }
        else {
            // COB_CODE: MOVE ZEROES   TO WK-TOTALE
            ws.setWkTotale(new AfDecimal(0, 18, 7));
        }
    }

    /**Original name: S1100-VALORIZZA-DCLGEN<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 *     RISPETTIVE AREE DCLGEN IN WORKING
	 * ----------------------------------------------------------------*</pre>*/
    private void s1100ValorizzaDclgen() {
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-DATI-CRIST
        //                TO DP61-AREA-D-CRIST
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias(), ws.getIvvc0218().getAliasDatiCrist())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO DP61-AREA-D-CRIST
            ws.setDp61AreaDCristFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxDclgen()).getLunghezza() - 1));
        }
    }

    /**Original name: S1150-CALCOLO-ANNO<br>
	 * <pre>----------------------------------------------------------------*
	 *    CALCOLO ANNO
	 * ----------------------------------------------------------------*</pre>*/
    private void s1150CalcoloAnno() {
        // COB_CODE: MOVE IVVC0213-DT-DECOR
        //             TO WK-DATA-FINE-D.
        ws.getWkDataFine().setWkDataFineDFormatted(ivvc0213.getDatiLivello().getIvvc0213DtDecorFormatted());
    }

    /**Original name: S1200-CALCOLO-IMPORTO<br>
	 * <pre>----------------------------------------------------------------*
	 *    CALCOLO IMPORTO
	 * ----------------------------------------------------------------*</pre>*/
    private void s1200CalcoloImporto() {
        // COB_CODE: MOVE 0   TO IVVC0213-VAL-IMP-O
        ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(0, 18, 7));
        // COB_CODE: IF DP61-ELE-D-CRIST-MAX > 0
        //              END-IF
        //           END-IF.
        if (ws.getDp61EleDCristMax() > 0) {
            // COB_CODE: IF WK-DATA-FINE-D < 20140701
            //              END-IF
            //           END-IF
            if (ws.getWkDataFine().getWkDataFineD() < 20140701) {
                // COB_CODE: IF DP61-IMPB-VIS-RP-P62014-NULL
                //              EQUAL HIGH-VALUES OR LOW-VALUE OR SPACES
                //              CONTINUE
                //           ELSE
                //                TO IVVC0213-VAL-IMP-O
                //           END-IF
                if (Characters.EQ_HIGH.test(ws.getLccvp6111().getDati().getWp61ImpbVisRpP62014().getDp61ImpbVisRpP62014NullFormatted()) || Characters.EQ_LOW.test(ws.getLccvp6111().getDati().getWp61ImpbVisRpP62014().getDp61ImpbVisRpP62014NullFormatted()) || Characters.EQ_SPACE.test(ws.getLccvp6111().getDati().getWp61ImpbVisRpP62014().getWp61ImpbVisRpP62014Null())) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: MOVE DP61-IMPB-VIS-RP-P62014
                    //             TO IVVC0213-VAL-IMP-O
                    ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(ws.getLccvp6111().getDati().getWp61ImpbVisRpP62014().getWp61ImpbVisRpP62014(), 18, 7));
                }
            }
        }
    }

    /**Original name: VERIFICA-MOVIMENTO<br>
	 * <pre>----------------------------------------------------------------*
	 *     VERIFICA MOVIMENTO
	 * ----------------------------------------------------------------*
	 * --> SE CI TROVIAMO IN FASE DI LIQUIDAZIONE DEVO RECUPERARE LE
	 * --> IMMAGINI IN FASE DI COMUNICAZIONE</pre>*/
    private void verificaMovimento() {
        // COB_CODE: MOVE IVVC0213-TIPO-MOVI-ORIG
        //             TO WS-MOVIMENTO
        //                WS-MOVI-ORIG
        ws.getWsMovimento().setWsMovimentoFormatted(ivvc0213.getTipoMoviOrigFormatted());
        ws.getWsMoviOrig().setWsMoviOrigFormatted(ivvc0213.getTipoMoviOrigFormatted());
        // COB_CODE:      IF LIQUI-RISTOT-IND OR LIQUI-RISPAR-POLIND OR
        //                   LIQUI-SCAPOL OR LIQUI-SININD OR LIQUI-RECIND OR
        //                   LIQUI-RPP-TAKE-PROFIT
        //                OR LIQUI-RPP-REDDITO-PROGR
        //                OR LIQUI-RISTOT-INCAPIENZA
        //                OR LIQUI-RPP-BENEFICIO-CONTR
        //                OR LIQUI-RISTOT-INCAP
        //           *-->    RECUPERO IL MOVIMENTO DI COMUNICAZIONE
        //                   END-IF
        //                ELSE
        //           *-->    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
        //           *-->    RISPETTIVE AREE DCLGEN IN WORKING
        //                           SPACES OR LOW-VALUE OR HIGH-VALUE
        //                END-IF.
        if (ws.getWsMovimento().isLiquiRistotInd() || ws.getWsMovimento().isLiquiRisparPolind() || ws.getWsMovimento().isLiquiScapol() || ws.getWsMovimento().isLiquiSinind() || ws.getWsMovimento().isLiquiRecind() || ws.getWsMovimento().isLiquiRppTakeProfit() || ws.getWsMovimento().isLiquiRppRedditoProgr() || ws.getWsMovimento().isLiquiRistotIncapienza() || ws.getWsMovimento().isLiquiRppBeneficioContr() || ws.getWsMovimento().isLiquiRistotIncap()) {
            //-->    RECUPERO IL MOVIMENTO DI COMUNICAZIONE
            // COB_CODE: PERFORM RECUP-MOVI-COMUN
            //              THRU RECUP-MOVI-COMUN-EX
            recupMoviComun();
            // COB_CODE:         IF COMUN-TROV-SI
            //                         THRU LETTURA-D-CRIST-EX
            //                   ELSE
            //           *-->       ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
            //           *-->       RISPETTIVE AREE DCLGEN IN WORKING
            //                              SPACES OR LOW-VALUE OR HIGH-VALUE
            //                   END-IF
            if (ws.getFlagComunTrov().isSi()) {
                // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA
                //             TO WK-DATA-CPTZ-RIP
                ws.getWkVarMoviComun().setCptzRip(idsv0003.getDataCompetenza());
                // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
                //             TO WK-DATA-EFF-RIP
                ws.getWkVarMoviComun().setEffRip(idsv0003.getDataInizioEffetto());
                // COB_CODE: MOVE WK-DATA-CPTZ-PREC
                //             TO IDSV0003-DATA-COMPETENZA
                idsv0003.setDataCompetenza(ws.getWkVarMoviComun().getCptzPrec());
                // COB_CODE: MOVE WK-DATA-EFF-PREC
                //             TO IDSV0003-DATA-INIZIO-EFFETTO
                //                IDSV0003-DATA-FINE-EFFETTO
                idsv0003.setDataInizioEffetto(ws.getWkVarMoviComun().getEffPrec());
                idsv0003.setDataFineEffetto(ws.getWkVarMoviComun().getEffPrec());
                // COB_CODE: PERFORM INIZIA-TOT-P61
                //              THRU INIZIA-TOT-P61-EX
                iniziaTotP61();
                // COB_CODE: PERFORM LETTURA-D-CRIST
                //              THRU LETTURA-D-CRIST-EX
                letturaDCrist();
            }
            else {
                //-->       ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
                //-->       RISPETTIVE AREE DCLGEN IN WORKING
                // COB_CODE: PERFORM S1100-VALORIZZA-DCLGEN
                //              THRU S1100-VALORIZZA-DCLGEN-EX
                //           VARYING IX-DCLGEN FROM 1 BY 1
                //             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
                //                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
                //                   SPACES OR LOW-VALUE OR HIGH-VALUE
                ws.setIxDclgen(((short)1));
                while (!(ws.getIxDclgen() > ivvc0213.getEleInfoMax() || Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias()) || Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()) || Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()))) {
                    s1100ValorizzaDclgen();
                    ws.setIxDclgen(Trunc.toShort(ws.getIxDclgen() + 1, 4));
                }
            }
        }
        else {
            //-->    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
            //-->    RISPETTIVE AREE DCLGEN IN WORKING
            // COB_CODE: PERFORM S1100-VALORIZZA-DCLGEN
            //              THRU S1100-VALORIZZA-DCLGEN-EX
            //           VARYING IX-DCLGEN FROM 1 BY 1
            //             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
            //                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
            //                   SPACES OR LOW-VALUE OR HIGH-VALUE
            ws.setIxDclgen(((short)1));
            while (!(ws.getIxDclgen() > ivvc0213.getEleInfoMax() || Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias()) || Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()) || Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()))) {
                s1100ValorizzaDclgen();
                ws.setIxDclgen(Trunc.toShort(ws.getIxDclgen() + 1, 4));
            }
        }
    }

    /**Original name: RECUP-MOVI-COMUN<br>
	 * <pre>----------------------------------------------------------------*
	 *     Recupero il movimento di comunicazione
	 * ----------------------------------------------------------------*</pre>*/
    private void recupMoviComun() {
        Ldbs6040 ldbs6040 = null;
        // COB_CODE: SET INIT-CUR-MOV               TO TRUE
        ws.getFlagCurMov().setInitCurMov();
        // COB_CODE: SET COMUN-TROV-NO              TO TRUE
        ws.getFlagComunTrov().setNo();
        // COB_CODE: INITIALIZE MOVI
        initMovi();
        //--> TIPO OPERAZIONE
        // COB_CODE: SET IDSV0003-FETCH-FIRST       TO TRUE
        idsv0003.getOperazione().setFetchFirst();
        //--> INIZIALIZZA CODICE DI RITORNO
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC    TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        //    SET  NO-ULTIMA-LETTURA         TO TRUE
        //
        // COB_CODE: PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC
        //                          OR FINE-CUR-MOV
        //                          OR COMUN-TROV-SI
        //               END-IF
        //           END-PERFORM.
        while (!(!idsv0003.getReturnCode().isSuccessfulRc() || ws.getFlagCurMov().isFineCurMov() || ws.getFlagComunTrov().isSi())) {
            // COB_CODE: INITIALIZE MOVI
            initMovi();
            //
            // COB_CODE: MOVE IVVC0213-ID-POLIZZA    TO MOV-ID-OGG
            ws.getMovi().getMovIdOgg().setMovIdOgg(ivvc0213.getIdPolizza());
            // COB_CODE: MOVE 'PO'                   TO MOV-TP-OGG
            ws.getMovi().setMovTpOgg("PO");
            // COB_CODE: SET IDSV0003-TRATT-SENZA-STOR   TO TRUE
            idsv0003.getTrattamentoStoricita().setTrattSenzaStor();
            //-->    LIVELLO OPERAZIONE
            // COB_CODE: SET IDSV0003-WHERE-CONDITION TO TRUE
            idsv0003.getLivelloOperazione().setWhereCondition();
            //-->    INIZIALIZZA CODICE DI RITORNO
            // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC  TO TRUE
            idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
            // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL TO TRUE
            idsv0003.getSqlcode().setSuccessfulSql();
            // COB_CODE: CALL  LDBS6040   USING IDSV0003 MOVI
            //             ON EXCEPTION
            //                SET IDSV0003-INVALID-OPER   TO TRUE
            //           END-CALL
            try {
                ldbs6040 = Ldbs6040.getInstance();
                ldbs6040.run(idsv0003, ws.getMovi());
            }
            catch (ProgramExecutionException __ex) {
                // COB_CODE: MOVE LDBS6040          TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbs6040());
                // COB_CODE: MOVE 'CALL-LDBS6040 ERRORE CHIAMATA'
                //             TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBS6040 ERRORE CHIAMATA");
                // COB_CODE: SET IDSV0003-INVALID-OPER   TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
            // COB_CODE:         IF IDSV0003-SUCCESSFUL-RC
            //                       END-EVALUATE
            //                    ELSE
            //           *-->        GESTIRE ERRORE
            //                       SET IDSV0003-INVALID-OPER   TO TRUE
            //                    END-IF
            if (idsv0003.getReturnCode().isSuccessfulRc()) {
                // COB_CODE:             EVALUATE TRUE
                //                           WHEN IDSV0003-NOT-FOUND
                //           *-->              NESSUN DATO IN TABELLA
                //           *-->              LIQUIDAZIONE CONTESTUALE - UTILIZZO L'AREA DEL
                //           *-->              BOLLO IN INPUT
                //                             SET FINE-CUR-MOV TO TRUE
                //                           WHEN IDSV0003-SUCCESSFUL-SQL
                //           *-->              OPERAZIONE ESEGUITA CORRETTAMENTE
                //           *-->              TROVO IL MOVIMENTO DI COMUNICAZIONE
                //                             END-IF
                //                           WHEN OTHER
                //           *--->                ERRORE DI ACCESSO AL DB
                //                                SET IDSV0003-INVALID-OPER   TO TRUE
                //                       END-EVALUATE
                switch (idsv0003.getSqlcode().getSqlcode()) {

                    case Idsv0003Sqlcode.NOT_FOUND://-->              NESSUN DATO IN TABELLA
                        //-->              LIQUIDAZIONE CONTESTUALE - UTILIZZO L'AREA DEL
                        //-->              BOLLO IN INPUT
                        // COB_CODE: SET FINE-CUR-MOV TO TRUE
                        ws.getFlagCurMov().setFineCurMov();
                        break;

                    case Idsv0003Sqlcode.SUCCESSFUL_SQL://-->              OPERAZIONE ESEGUITA CORRETTAMENTE
                        //-->              TROVO IL MOVIMENTO DI COMUNICAZIONE
                        // COB_CODE: MOVE MOV-TP-MOVI      TO WS-MOVIMENTO
                        ws.getWsMovimento().setWsMovimento(TruncAbs.toInt(ws.getMovi().getMovTpMovi().getMovTpMovi(), 5));
                        //                 IF COMUN-RISPAR-IND
                        //                 OR RISTO-INDIVI
                        //                 OR VARIA-OPZION
                        //                 OR SINIS-INDIVI
                        //                 OR RECES-INDIVI
                        // COB_CODE:                   IF (COMUN-RISPAR-IND AND WS-LIQUI-RISPAR-POLIND)
                        //                             OR (RISTO-INDIVI AND WS-LIQUI-RISTOT-IND)
                        //                             OR (COMUN-RISTOT-INCAP AND
                        //                                 WS-LIQUI-RISTOT-INC-DA-RP)
                        //                             OR (VARIA-OPZION AND WS-LIQUI-SCAPOL)
                        //                             OR (SINIS-INDIVI AND WS-LIQUI-SININD)
                        //                             OR (RECES-INDIVI AND WS-LIQUI-RECIND)
                        //                             OR (COMUN-RISTOT-INCAPIENZA
                        //                             AND WS-LIQUI-RISTOT-INCAPIENZA)
                        //                             OR (RPP-REDDITO-PROGRAMMATO
                        //                             AND WS-LIQUI-RPP-REDDITO-PROGR)
                        //                             OR (RPP-TAKE-PROFIT AND WS-LIQUI-TAKE-PROFIT)
                        //                             OR (RPP-BENEFICIO-CONTR
                        //                             AND WS-LIQUI-RPP-BENEFICIO-CONTR)
                        //           *                    MOVE MOV-ID-MOVI       TO WK-ID-MOVI-COMUN
                        //           *                    SET SI-ULTIMA-LETTURA  TO TRUE
                        //                                SET FINE-CUR-MOV   TO TRUE
                        //                             ELSE
                        //                                SET IDSV0003-FETCH-NEXT   TO TRUE
                        //                             END-IF
                        if (ws.getWsMovimento().isComunRisparInd() && ws.getWsMoviOrig().isRisparPolind() || ws.getWsMovimento().isRistoIndivi() && ws.getWsMoviOrig().isRistotInd() || ws.getWsMovimento().isComunRistotIncap() && ws.getWsMoviOrig().isRistotIncDaRp() || ws.getWsMovimento().isVariaOpzion() && ws.getWsMoviOrig().isScapol() || ws.getWsMovimento().isSinisIndivi() && ws.getWsMoviOrig().isSinind() || ws.getWsMovimento().isRecesIndivi() && ws.getWsMoviOrig().isRecind() || ws.getWsMovimento().isComunRistotIncapienza() && ws.getWsMoviOrig().isRistotIncapienza() || ws.getWsMovimento().isRppRedditoProgrammato() && ws.getWsMoviOrig().isRppRedditoProgr() || ws.getWsMovimento().isRppTakeProfit() && ws.getWsMoviOrig().isTakeProfit() || ws.getWsMovimento().isRppBeneficioContr() && ws.getWsMoviOrig().isRppBeneficioContr()) {
                            //                    MOVE MOV-ID-MOVI       TO WK-ID-MOVI-COMUN
                            //                    SET SI-ULTIMA-LETTURA  TO TRUE
                            // COB_CODE: MOVE MOV-DT-EFF        TO WK-DATA-EFF-PREC
                            ws.getWkVarMoviComun().setEffPrec(ws.getMovi().getMovDtEff());
                            // COB_CODE: COMPUTE WK-DATA-CPTZ-PREC =  MOV-DS-TS-CPTZ
                            //                                     - 1
                            ws.getWkVarMoviComun().setCptzPrec(Trunc.toLong(ws.getMovi().getMovDsTsCptz() - 1, 18));
                            // COB_CODE: SET COMUN-TROV-SI   TO TRUE
                            ws.getFlagComunTrov().setSi();
                            // COB_CODE: PERFORM CLOSE-MOVI
                            //              THRU CLOSE-MOVI-EX
                            closeMovi();
                            // COB_CODE: SET FINE-CUR-MOV   TO TRUE
                            ws.getFlagCurMov().setFineCurMov();
                        }
                        else {
                            // COB_CODE: SET IDSV0003-FETCH-NEXT   TO TRUE
                            idsv0003.getOperazione().setFetchNext();
                        }
                        break;

                    default://--->                ERRORE DI ACCESSO AL DB
                        // COB_CODE: MOVE LDBS6040
                        //             TO IDSV0003-COD-SERVIZIO-BE
                        idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbs6040());
                        // COB_CODE: MOVE 'CALL-LDBS6040 ERRORE CHIAMATA'
                        //             TO IDSV0003-DESCRIZ-ERR-DB2
                        idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBS6040 ERRORE CHIAMATA");
                        // COB_CODE: SET IDSV0003-INVALID-OPER   TO TRUE
                        idsv0003.getReturnCode().setInvalidOper();
                        break;
                }
            }
            else {
                //-->        GESTIRE ERRORE
                // COB_CODE: MOVE LDBS6040
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbs6040());
                // COB_CODE: MOVE 'CALL-LDBS6040 ERRORE CHIAMATA'
                //             TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBS6040 ERRORE CHIAMATA");
                // COB_CODE: SET IDSV0003-INVALID-OPER   TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
        }
        // COB_CODE: MOVE IDSV0003-TIPO-MOVIMENTO TO WS-MOVIMENTO.
        ws.getWsMovimento().setWsMovimentoFormatted(idsv0003.getTipoMovimentoFormatted());
    }

    /**Original name: LETTURA-D-CRIST<br>
	 * <pre>----------------------------------------------------------------*
	 *     RECUPERO DATI CRISTALLIZZATI DA MOVIMENTO DI COMUNICAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void letturaDCrist() {
        Idbsp610 idbsp610 = null;
        // COB_CODE: SET FINE-LETT-P61-NO           TO TRUE
        ws.getFlagFineLetturaP61().setNo();
        // COB_CODE: INITIALIZE D-CRIST
        initDCrist();
        // COB_CODE: MOVE 0 TO DP61-ELE-D-CRIST-MAX
        ws.setDp61EleDCristMax(((short)0));
        // COB_CODE: MOVE 0 TO IX-TAB-P61
        ws.setIxTabP61(0);
        //--> TIPO OPERAZIONE
        // COB_CODE: SET IDSV0003-FETCH-FIRST       TO TRUE
        idsv0003.getOperazione().setFetchFirst();
        //--> INIZIALIZZA CODICE DI RITORNO
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC    TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        //
        // COB_CODE: PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC
        //                      OR FINE-LETT-P61-SI
        //                 END-IF
        //           END-PERFORM.
        while (!(!idsv0003.getReturnCode().isSuccessfulRc() || ws.getFlagFineLetturaP61().isSi())) {
            // COB_CODE: INITIALIZE D-CRIST
            initDCrist();
            //
            // COB_CODE: MOVE IVVC0213-ID-POLIZZA    TO P61-ID-POLI
            ws.getdCrist().setP61IdPoli(ivvc0213.getIdPolizza());
            // COB_CODE: SET IDSV0003-TRATT-X-COMPETENZA TO TRUE
            idsv0003.getTrattamentoStoricita().setTrattXCompetenza();
            //--> LIVELLO OPERAZIONE
            // COB_CODE: SET IDSV0003-ID-PADRE        TO TRUE
            idsv0003.getLivelloOperazione().setIdsi0011IdPadre();
            //--> INIZIALIZZA CODICE DI RITORNO
            // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC  TO TRUE
            idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
            // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL TO TRUE
            idsv0003.getSqlcode().setSuccessfulSql();
            // COB_CODE: CALL  IDBSP610   USING IDSV0003 D-CRIST
            //             ON EXCEPTION
            //                SET IDSV0003-INVALID-OPER   TO TRUE
            //           END-CALL
            try {
                idbsp610 = Idbsp610.getInstance();
                idbsp610.run(idsv0003, ws.getdCrist());
            }
            catch (ProgramExecutionException __ex) {
                // COB_CODE: MOVE IDBSP610          TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getIdbsp610());
                // COB_CODE: MOVE 'CALL-IDBSP610 ERRORE CHIAMATA'
                //             TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("CALL-IDBSP610 ERRORE CHIAMATA");
                // COB_CODE: SET IDSV0003-INVALID-OPER   TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
            // COB_CODE:           IF IDSV0003-SUCCESSFUL-RC
            //                         END-EVALUATE
            //                      ELSE
            //           *-->          GESTIRE ERRORE
            //                         SET FINE-LETT-P61-SI TO TRUE
            //                      END-IF
            if (idsv0003.getReturnCode().isSuccessfulRc()) {
                // COB_CODE:               EVALUATE TRUE
                //                                  SET IDSV0003-FETCH-NEXT   TO TRUE
                //                             WHEN IDSV0003-NOT-FOUND
                //           *-->                NESSUN DATO IN TABELLA
                //                               END-IF
                //                             WHEN OTHER
                //           *--->                  ERRORE DI ACCESSO AL DB
                //                                  SET FINE-LETT-P61-SI TO TRUE
                //                         END-EVALUATE
                switch (idsv0003.getSqlcode().getSqlcode()) {

                    case Idsv0003Sqlcode.SUCCESSFUL_SQL://-->                   OPERAZIONE ESEGUITA CORRETTAMENTE
                        // COB_CODE: code not available
                        ws.setIntRegister1(1);
                        ws.setDp61EleDCristMax(Trunc.toShort(ws.getIntRegister1() + ws.getDp61EleDCristMax(), 4));
                        ws.setIxTabP61(Trunc.toInt(abs(ws.getIntRegister1() + ws.getIxTabP61()), 9));
                        // COB_CODE: PERFORM VALORIZZA-OUTPUT-P61
                        //              THRU VALORIZZA-OUTPUT-P61-EX
                        valorizzaOutputP61();
                        // COB_CODE: SET IDSV0003-FETCH-NEXT   TO TRUE
                        idsv0003.getOperazione().setFetchNext();
                        break;

                    case Idsv0003Sqlcode.NOT_FOUND://-->                NESSUN DATO IN TABELLA
                        // COB_CODE: SET FINE-LETT-P61-SI           TO TRUE
                        ws.getFlagFineLetturaP61().setSi();
                        // COB_CODE: IF NOT IDSV0003-FETCH-FIRST
                        //              SET IDSV0003-SUCCESSFUL-RC  TO TRUE
                        //           END-IF
                        if (!idsv0003.getOperazione().isFetchFirst()) {
                            // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL TO TRUE
                            idsv0003.getSqlcode().setSuccessfulSql();
                            // COB_CODE: SET IDSV0003-SUCCESSFUL-RC  TO TRUE
                            idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
                        }
                        break;

                    default://--->                  ERRORE DI ACCESSO AL DB
                        // COB_CODE: MOVE IDBSP610
                        //             TO IDSV0003-COD-SERVIZIO-BE
                        idsv0003.getCampiEsito().setCodServizioBe(ws.getIdbsp610());
                        // COB_CODE: MOVE 'CALL-IDBSP610 ERRORE CHIAMATA'
                        //             TO IDSV0003-DESCRIZ-ERR-DB2
                        idsv0003.getCampiEsito().setDescrizErrDb2("CALL-IDBSP610 ERRORE CHIAMATA");
                        // COB_CODE: SET IDSV0003-INVALID-OPER   TO TRUE
                        idsv0003.getReturnCode().setInvalidOper();
                        // COB_CODE: SET FINE-LETT-P61-SI TO TRUE
                        ws.getFlagFineLetturaP61().setSi();
                        break;
                }
            }
            else {
                //-->          GESTIRE ERRORE
                // COB_CODE: MOVE IDBSP610
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getIdbsp610());
                // COB_CODE: MOVE 'CALL-IDBSP610 ERRORE CHIAMATA'
                //             TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("CALL-IDBSP610 ERRORE CHIAMATA");
                // COB_CODE: SET IDSV0003-INVALID-OPER   TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
                // COB_CODE: SET FINE-LETT-P61-SI TO TRUE
                ws.getFlagFineLetturaP61().setSi();
            }
        }
    }

    /**Original name: CLOSE-MOVI<br>
	 * <pre>----------------------------------------------------------------*
	 *     CHIUDO IL CURSORE SULLA TABELLA MOVIMENTO LDBS6040
	 * ----------------------------------------------------------------*</pre>*/
    private void closeMovi() {
        Ldbs6040 ldbs6040 = null;
        // COB_CODE: SET IDSV0003-CLOSE-CURSOR            TO TRUE.
        idsv0003.getOperazione().setIdsv0003CloseCursor();
        // COB_CODE: CALL LDBS6040 USING IDSV0003 MOVI
        //           ON EXCEPTION
        //              SET IDSV0003-INVALID-OPER TO TRUE
        //           END-CALL.
        try {
            ldbs6040 = Ldbs6040.getInstance();
            ldbs6040.run(idsv0003, ws.getMovi());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE LDBS6040
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbs6040());
            // COB_CODE: MOVE 'ERRORE CALL LDBS6040 CLOSE'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("ERRORE CALL LDBS6040 CLOSE");
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE: IF NOT IDSV0003-SUCCESSFUL-RC
        //           OR NOT IDSV0003-SUCCESSFUL-SQL
        //                TO TRUE
        //           END-IF.
        if (!idsv0003.getReturnCode().isSuccessfulRc() || !idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: MOVE WK-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'ERRORE CLOSE CURSORE LDBS6040'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("ERRORE CLOSE CURSORE LDBS6040");
            // COB_CODE: SET IDSV0003-INVALID-OPER
            //            TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: MOVE SPACES                     TO IVVC0213-VAL-STR-O.
        ivvc0213.getTabOutput().setValStrO("");
        // COB_CODE: MOVE 0                          TO IVVC0213-VAL-PERC-O.
        ivvc0213.getTabOutput().setValPercO(Trunc.toDecimal(0, 14, 9));
        // COB_CODE: IF COMUN-TROV-SI
        //                   IDSV0003-DATA-FINE-EFFETTO
        //           END-IF
        if (ws.getFlagComunTrov().isSi()) {
            // COB_CODE: MOVE WK-DATA-CPTZ-RIP
            //             TO IDSV0003-DATA-COMPETENZA
            idsv0003.setDataCompetenza(ws.getWkVarMoviComun().getCptzRip());
            // COB_CODE: MOVE WK-DATA-EFF-RIP
            //             TO IDSV0003-DATA-INIZIO-EFFETTO
            //                IDSV0003-DATA-FINE-EFFETTO
            idsv0003.setDataInizioEffetto(ws.getWkVarMoviComun().getEffRip());
            idsv0003.setDataFineEffetto(ws.getWkVarMoviComun().getEffRip());
        }
        //
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    /**Original name: VALORIZZA-OUTPUT-P61<br>
	 * <pre>----------------------------------------------------------------*
	 *    COPY D-CRIST
	 * ----------------------------------------------------------------*
	 * ------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    VALORIZZA OUTPUT COPY LCCVP613
	 *    ULTIMO AGG. 24 OTT 2019
	 * ------------------------------------------------------------</pre>*/
    private void valorizzaOutputP61() {
        // COB_CODE: MOVE P61-ID-D-CRIST
        //             TO (SF)-ID-PTF
        ws.getLccvp6111().setIdPtf(ws.getdCrist().getP61IdDCrist());
        // COB_CODE: MOVE P61-ID-D-CRIST
        //             TO (SF)-ID-D-CRIST
        ws.getLccvp6111().getDati().setWp61IdDCrist(ws.getdCrist().getP61IdDCrist());
        // COB_CODE: MOVE P61-ID-POLI
        //             TO (SF)-ID-POLI
        ws.getLccvp6111().getDati().setWp61IdPoli(ws.getdCrist().getP61IdPoli());
        // COB_CODE: MOVE P61-COD-COMP-ANIA
        //             TO (SF)-COD-COMP-ANIA
        ws.getLccvp6111().getDati().setWp61CodCompAnia(ws.getdCrist().getP61CodCompAnia());
        // COB_CODE: MOVE P61-ID-MOVI-CRZ
        //             TO (SF)-ID-MOVI-CRZ
        ws.getLccvp6111().getDati().setWp61IdMoviCrz(ws.getdCrist().getP61IdMoviCrz());
        // COB_CODE: IF P61-ID-MOVI-CHIU-NULL = HIGH-VALUES
        //                TO (SF)-ID-MOVI-CHIU-NULL
        //           ELSE
        //                TO (SF)-ID-MOVI-CHIU
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getdCrist().getP61IdMoviChiu().getP61IdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE P61-ID-MOVI-CHIU-NULL
            //             TO (SF)-ID-MOVI-CHIU-NULL
            ws.getLccvp6111().getDati().getWp61IdMoviChiu().setWp61IdMoviChiuNull(ws.getdCrist().getP61IdMoviChiu().getP61IdMoviChiuNull());
        }
        else {
            // COB_CODE: MOVE P61-ID-MOVI-CHIU
            //             TO (SF)-ID-MOVI-CHIU
            ws.getLccvp6111().getDati().getWp61IdMoviChiu().setWp61IdMoviChiu(ws.getdCrist().getP61IdMoviChiu().getP61IdMoviChiu());
        }
        // COB_CODE: MOVE P61-DT-INI-EFF
        //             TO (SF)-DT-INI-EFF
        ws.getLccvp6111().getDati().setWp61DtIniEff(ws.getdCrist().getP61DtIniEff());
        // COB_CODE: MOVE P61-DT-END-EFF
        //             TO (SF)-DT-END-EFF
        ws.getLccvp6111().getDati().setWp61DtEndEff(ws.getdCrist().getP61DtEndEff());
        // COB_CODE: MOVE P61-COD-PROD
        //             TO (SF)-COD-PROD
        ws.getLccvp6111().getDati().setWp61CodProd(ws.getdCrist().getP61CodProd());
        // COB_CODE: MOVE P61-DT-DECOR
        //             TO (SF)-DT-DECOR
        ws.getLccvp6111().getDati().setWp61DtDecor(ws.getdCrist().getP61DtDecor());
        // COB_CODE: MOVE P61-DS-RIGA
        //             TO (SF)-DS-RIGA
        ws.getLccvp6111().getDati().setWp61DsRiga(ws.getdCrist().getP61DsRiga());
        // COB_CODE: MOVE P61-DS-OPER-SQL
        //             TO (SF)-DS-OPER-SQL
        ws.getLccvp6111().getDati().setWp61DsOperSql(ws.getdCrist().getP61DsOperSql());
        // COB_CODE: MOVE P61-DS-VER
        //             TO (SF)-DS-VER
        ws.getLccvp6111().getDati().setWp61DsVer(ws.getdCrist().getP61DsVer());
        // COB_CODE: MOVE P61-DS-TS-INI-CPTZ
        //             TO (SF)-DS-TS-INI-CPTZ
        ws.getLccvp6111().getDati().setWp61DsTsIniCptz(ws.getdCrist().getP61DsTsIniCptz());
        // COB_CODE: MOVE P61-DS-TS-END-CPTZ
        //             TO (SF)-DS-TS-END-CPTZ
        ws.getLccvp6111().getDati().setWp61DsTsEndCptz(ws.getdCrist().getP61DsTsEndCptz());
        // COB_CODE: MOVE P61-DS-UTENTE
        //             TO (SF)-DS-UTENTE
        ws.getLccvp6111().getDati().setWp61DsUtente(ws.getdCrist().getP61DsUtente());
        // COB_CODE: MOVE P61-DS-STATO-ELAB
        //             TO (SF)-DS-STATO-ELAB
        ws.getLccvp6111().getDati().setWp61DsStatoElab(ws.getdCrist().getP61DsStatoElab());
        // COB_CODE: IF P61-RIS-MAT-31122011-NULL = HIGH-VALUES
        //                TO (SF)-RIS-MAT-31122011-NULL
        //           ELSE
        //                TO (SF)-RIS-MAT-31122011
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getdCrist().getP61RisMat31122011().getP61RisMat31122011NullFormatted())) {
            // COB_CODE: MOVE P61-RIS-MAT-31122011-NULL
            //             TO (SF)-RIS-MAT-31122011-NULL
            ws.getLccvp6111().getDati().getWp61RisMat31122011().setWp61RisMat31122011Null(ws.getdCrist().getP61RisMat31122011().getP61RisMat31122011Null());
        }
        else {
            // COB_CODE: MOVE P61-RIS-MAT-31122011
            //             TO (SF)-RIS-MAT-31122011
            ws.getLccvp6111().getDati().getWp61RisMat31122011().setWp61RisMat31122011(Trunc.toDecimal(ws.getdCrist().getP61RisMat31122011().getP61RisMat31122011(), 15, 3));
        }
        // COB_CODE: IF P61-PRE-V-31122011-NULL = HIGH-VALUES
        //                TO (SF)-PRE-V-31122011-NULL
        //           ELSE
        //                TO (SF)-PRE-V-31122011
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getdCrist().getP61PreV31122011().getP61PreV31122011NullFormatted())) {
            // COB_CODE: MOVE P61-PRE-V-31122011-NULL
            //             TO (SF)-PRE-V-31122011-NULL
            ws.getLccvp6111().getDati().getWp61PreV31122011().setWp61PreV31122011Null(ws.getdCrist().getP61PreV31122011().getP61PreV31122011Null());
        }
        else {
            // COB_CODE: MOVE P61-PRE-V-31122011
            //             TO (SF)-PRE-V-31122011
            ws.getLccvp6111().getDati().getWp61PreV31122011().setWp61PreV31122011(Trunc.toDecimal(ws.getdCrist().getP61PreV31122011().getP61PreV31122011(), 15, 3));
        }
        // COB_CODE: IF P61-PRE-RSH-V-31122011-NULL = HIGH-VALUES
        //                TO (SF)-PRE-RSH-V-31122011-NULL
        //           ELSE
        //                TO (SF)-PRE-RSH-V-31122011
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getdCrist().getP61PreRshV31122011().getP61PreRshV31122011NullFormatted())) {
            // COB_CODE: MOVE P61-PRE-RSH-V-31122011-NULL
            //             TO (SF)-PRE-RSH-V-31122011-NULL
            ws.getLccvp6111().getDati().getWp61PreRshV31122011().setWp61PreRshV31122011Null(ws.getdCrist().getP61PreRshV31122011().getP61PreRshV31122011Null());
        }
        else {
            // COB_CODE: MOVE P61-PRE-RSH-V-31122011
            //             TO (SF)-PRE-RSH-V-31122011
            ws.getLccvp6111().getDati().getWp61PreRshV31122011().setWp61PreRshV31122011(Trunc.toDecimal(ws.getdCrist().getP61PreRshV31122011().getP61PreRshV31122011(), 15, 3));
        }
        // COB_CODE: IF P61-CPT-RIVTO-31122011-NULL = HIGH-VALUES
        //                TO (SF)-CPT-RIVTO-31122011-NULL
        //           ELSE
        //                TO (SF)-CPT-RIVTO-31122011
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getdCrist().getP61CptRivto31122011().getP61CptRivto31122011NullFormatted())) {
            // COB_CODE: MOVE P61-CPT-RIVTO-31122011-NULL
            //             TO (SF)-CPT-RIVTO-31122011-NULL
            ws.getLccvp6111().getDati().getWp61CptRivto31122011().setWp61CptRivto31122011Null(ws.getdCrist().getP61CptRivto31122011().getP61CptRivto31122011Null());
        }
        else {
            // COB_CODE: MOVE P61-CPT-RIVTO-31122011
            //             TO (SF)-CPT-RIVTO-31122011
            ws.getLccvp6111().getDati().getWp61CptRivto31122011().setWp61CptRivto31122011(Trunc.toDecimal(ws.getdCrist().getP61CptRivto31122011().getP61CptRivto31122011(), 15, 3));
        }
        // COB_CODE: IF P61-IMPB-VIS-31122011-NULL = HIGH-VALUES
        //                TO (SF)-IMPB-VIS-31122011-NULL
        //           ELSE
        //                TO (SF)-IMPB-VIS-31122011
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getdCrist().getP61ImpbVis31122011().getP61ImpbVis31122011NullFormatted())) {
            // COB_CODE: MOVE P61-IMPB-VIS-31122011-NULL
            //             TO (SF)-IMPB-VIS-31122011-NULL
            ws.getLccvp6111().getDati().getWp61ImpbVis31122011().setWp61ImpbVis31122011Null(ws.getdCrist().getP61ImpbVis31122011().getP61ImpbVis31122011Null());
        }
        else {
            // COB_CODE: MOVE P61-IMPB-VIS-31122011
            //             TO (SF)-IMPB-VIS-31122011
            ws.getLccvp6111().getDati().getWp61ImpbVis31122011().setWp61ImpbVis31122011(Trunc.toDecimal(ws.getdCrist().getP61ImpbVis31122011().getP61ImpbVis31122011(), 15, 3));
        }
        // COB_CODE: IF P61-IMPB-IS-31122011-NULL = HIGH-VALUES
        //                TO (SF)-IMPB-IS-31122011-NULL
        //           ELSE
        //                TO (SF)-IMPB-IS-31122011
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getdCrist().getP61ImpbIs31122011().getP61ImpbIs31122011NullFormatted())) {
            // COB_CODE: MOVE P61-IMPB-IS-31122011-NULL
            //             TO (SF)-IMPB-IS-31122011-NULL
            ws.getLccvp6111().getDati().getWp61ImpbIs31122011().setWp61ImpbIs31122011Null(ws.getdCrist().getP61ImpbIs31122011().getP61ImpbIs31122011Null());
        }
        else {
            // COB_CODE: MOVE P61-IMPB-IS-31122011
            //             TO (SF)-IMPB-IS-31122011
            ws.getLccvp6111().getDati().getWp61ImpbIs31122011().setWp61ImpbIs31122011(Trunc.toDecimal(ws.getdCrist().getP61ImpbIs31122011().getP61ImpbIs31122011(), 15, 3));
        }
        // COB_CODE: IF P61-IMPB-VIS-RP-P2011-NULL = HIGH-VALUES
        //                TO (SF)-IMPB-VIS-RP-P2011-NULL
        //           ELSE
        //                TO (SF)-IMPB-VIS-RP-P2011
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getdCrist().getP61ImpbVisRpP2011().getP61ImpbVisRpP2011NullFormatted())) {
            // COB_CODE: MOVE P61-IMPB-VIS-RP-P2011-NULL
            //             TO (SF)-IMPB-VIS-RP-P2011-NULL
            ws.getLccvp6111().getDati().getWp61ImpbVisRpP2011().setWp61ImpbVisRpP2011Null(ws.getdCrist().getP61ImpbVisRpP2011().getP61ImpbVisRpP2011Null());
        }
        else {
            // COB_CODE: MOVE P61-IMPB-VIS-RP-P2011
            //             TO (SF)-IMPB-VIS-RP-P2011
            ws.getLccvp6111().getDati().getWp61ImpbVisRpP2011().setWp61ImpbVisRpP2011(Trunc.toDecimal(ws.getdCrist().getP61ImpbVisRpP2011().getP61ImpbVisRpP2011(), 15, 3));
        }
        // COB_CODE: IF P61-IMPB-IS-RP-P2011-NULL = HIGH-VALUES
        //                TO (SF)-IMPB-IS-RP-P2011-NULL
        //           ELSE
        //                TO (SF)-IMPB-IS-RP-P2011
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getdCrist().getP61ImpbIsRpP2011().getP61ImpbIsRpP2011NullFormatted())) {
            // COB_CODE: MOVE P61-IMPB-IS-RP-P2011-NULL
            //             TO (SF)-IMPB-IS-RP-P2011-NULL
            ws.getLccvp6111().getDati().getWp61ImpbIsRpP2011().setWp61ImpbIsRpP2011Null(ws.getdCrist().getP61ImpbIsRpP2011().getP61ImpbIsRpP2011Null());
        }
        else {
            // COB_CODE: MOVE P61-IMPB-IS-RP-P2011
            //             TO (SF)-IMPB-IS-RP-P2011
            ws.getLccvp6111().getDati().getWp61ImpbIsRpP2011().setWp61ImpbIsRpP2011(Trunc.toDecimal(ws.getdCrist().getP61ImpbIsRpP2011().getP61ImpbIsRpP2011(), 15, 3));
        }
        // COB_CODE: IF P61-PRE-V-30062014-NULL = HIGH-VALUES
        //                TO (SF)-PRE-V-30062014-NULL
        //           ELSE
        //                TO (SF)-PRE-V-30062014
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getdCrist().getP61PreV30062014().getP61PreV30062014NullFormatted())) {
            // COB_CODE: MOVE P61-PRE-V-30062014-NULL
            //             TO (SF)-PRE-V-30062014-NULL
            ws.getLccvp6111().getDati().getWp61PreV30062014().setWp61PreV30062014Null(ws.getdCrist().getP61PreV30062014().getP61PreV30062014Null());
        }
        else {
            // COB_CODE: MOVE P61-PRE-V-30062014
            //             TO (SF)-PRE-V-30062014
            ws.getLccvp6111().getDati().getWp61PreV30062014().setWp61PreV30062014(Trunc.toDecimal(ws.getdCrist().getP61PreV30062014().getP61PreV30062014(), 15, 3));
        }
        // COB_CODE: IF P61-PRE-RSH-V-30062014-NULL = HIGH-VALUES
        //                TO (SF)-PRE-RSH-V-30062014-NULL
        //           ELSE
        //                TO (SF)-PRE-RSH-V-30062014
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getdCrist().getP61PreRshV30062014().getP61PreRshV30062014NullFormatted())) {
            // COB_CODE: MOVE P61-PRE-RSH-V-30062014-NULL
            //             TO (SF)-PRE-RSH-V-30062014-NULL
            ws.getLccvp6111().getDati().getWp61PreRshV30062014().setWp61PreRshV30062014Null(ws.getdCrist().getP61PreRshV30062014().getP61PreRshV30062014Null());
        }
        else {
            // COB_CODE: MOVE P61-PRE-RSH-V-30062014
            //             TO (SF)-PRE-RSH-V-30062014
            ws.getLccvp6111().getDati().getWp61PreRshV30062014().setWp61PreRshV30062014(Trunc.toDecimal(ws.getdCrist().getP61PreRshV30062014().getP61PreRshV30062014(), 15, 3));
        }
        // COB_CODE: IF P61-CPT-INI-30062014-NULL = HIGH-VALUES
        //                TO (SF)-CPT-INI-30062014-NULL
        //           ELSE
        //                TO (SF)-CPT-INI-30062014
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getdCrist().getP61CptIni30062014().getP61CptIni30062014NullFormatted())) {
            // COB_CODE: MOVE P61-CPT-INI-30062014-NULL
            //             TO (SF)-CPT-INI-30062014-NULL
            ws.getLccvp6111().getDati().getWp61CptIni30062014().setWp61CptIni30062014Null(ws.getdCrist().getP61CptIni30062014().getP61CptIni30062014Null());
        }
        else {
            // COB_CODE: MOVE P61-CPT-INI-30062014
            //             TO (SF)-CPT-INI-30062014
            ws.getLccvp6111().getDati().getWp61CptIni30062014().setWp61CptIni30062014(Trunc.toDecimal(ws.getdCrist().getP61CptIni30062014().getP61CptIni30062014(), 15, 3));
        }
        // COB_CODE: IF P61-IMPB-VIS-30062014-NULL = HIGH-VALUES
        //                TO (SF)-IMPB-VIS-30062014-NULL
        //           ELSE
        //                TO (SF)-IMPB-VIS-30062014
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getdCrist().getP61ImpbVis30062014().getP61ImpbVis30062014NullFormatted())) {
            // COB_CODE: MOVE P61-IMPB-VIS-30062014-NULL
            //             TO (SF)-IMPB-VIS-30062014-NULL
            ws.getLccvp6111().getDati().getWp61ImpbVis30062014().setWp61ImpbVis30062014Null(ws.getdCrist().getP61ImpbVis30062014().getP61ImpbVis30062014Null());
        }
        else {
            // COB_CODE: MOVE P61-IMPB-VIS-30062014
            //             TO (SF)-IMPB-VIS-30062014
            ws.getLccvp6111().getDati().getWp61ImpbVis30062014().setWp61ImpbVis30062014(Trunc.toDecimal(ws.getdCrist().getP61ImpbVis30062014().getP61ImpbVis30062014(), 15, 3));
        }
        // COB_CODE: IF P61-IMPB-IS-30062014-NULL = HIGH-VALUES
        //                TO (SF)-IMPB-IS-30062014-NULL
        //           ELSE
        //                TO (SF)-IMPB-IS-30062014
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getdCrist().getP61ImpbIs30062014().getP61ImpbIs30062014NullFormatted())) {
            // COB_CODE: MOVE P61-IMPB-IS-30062014-NULL
            //             TO (SF)-IMPB-IS-30062014-NULL
            ws.getLccvp6111().getDati().getWp61ImpbIs30062014().setWp61ImpbIs30062014Null(ws.getdCrist().getP61ImpbIs30062014().getP61ImpbIs30062014Null());
        }
        else {
            // COB_CODE: MOVE P61-IMPB-IS-30062014
            //             TO (SF)-IMPB-IS-30062014
            ws.getLccvp6111().getDati().getWp61ImpbIs30062014().setWp61ImpbIs30062014(Trunc.toDecimal(ws.getdCrist().getP61ImpbIs30062014().getP61ImpbIs30062014(), 15, 3));
        }
        // COB_CODE: IF P61-IMPB-VIS-RP-P62014-NULL = HIGH-VALUES
        //                TO (SF)-IMPB-VIS-RP-P62014-NULL
        //           ELSE
        //                TO (SF)-IMPB-VIS-RP-P62014
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getdCrist().getP61ImpbVisRpP62014().getP61ImpbVisRpP62014NullFormatted())) {
            // COB_CODE: MOVE P61-IMPB-VIS-RP-P62014-NULL
            //             TO (SF)-IMPB-VIS-RP-P62014-NULL
            ws.getLccvp6111().getDati().getWp61ImpbVisRpP62014().setWp61ImpbVisRpP62014Null(ws.getdCrist().getP61ImpbVisRpP62014().getP61ImpbVisRpP62014Null());
        }
        else {
            // COB_CODE: MOVE P61-IMPB-VIS-RP-P62014
            //             TO (SF)-IMPB-VIS-RP-P62014
            ws.getLccvp6111().getDati().getWp61ImpbVisRpP62014().setWp61ImpbVisRpP62014(Trunc.toDecimal(ws.getdCrist().getP61ImpbVisRpP62014().getP61ImpbVisRpP62014(), 15, 3));
        }
        // COB_CODE: IF P61-IMPB-IS-RP-P62014-NULL = HIGH-VALUES
        //                TO (SF)-IMPB-IS-RP-P62014-NULL
        //           ELSE
        //                TO (SF)-IMPB-IS-RP-P62014
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getdCrist().getP61ImpbIsRpP62014().getP61ImpbIsRpP62014NullFormatted())) {
            // COB_CODE: MOVE P61-IMPB-IS-RP-P62014-NULL
            //             TO (SF)-IMPB-IS-RP-P62014-NULL
            ws.getLccvp6111().getDati().getWp61ImpbIsRpP62014().setWp61ImpbIsRpP62014Null(ws.getdCrist().getP61ImpbIsRpP62014().getP61ImpbIsRpP62014Null());
        }
        else {
            // COB_CODE: MOVE P61-IMPB-IS-RP-P62014
            //             TO (SF)-IMPB-IS-RP-P62014
            ws.getLccvp6111().getDati().getWp61ImpbIsRpP62014().setWp61ImpbIsRpP62014(Trunc.toDecimal(ws.getdCrist().getP61ImpbIsRpP62014().getP61ImpbIsRpP62014(), 15, 3));
        }
        // COB_CODE: IF P61-RIS-MAT-30062014-NULL = HIGH-VALUES
        //                TO (SF)-RIS-MAT-30062014-NULL
        //           ELSE
        //                TO (SF)-RIS-MAT-30062014
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getdCrist().getP61RisMat30062014().getP61RisMat30062014NullFormatted())) {
            // COB_CODE: MOVE P61-RIS-MAT-30062014-NULL
            //             TO (SF)-RIS-MAT-30062014-NULL
            ws.getLccvp6111().getDati().getWp61RisMat30062014().setWp61RisMat30062014Null(ws.getdCrist().getP61RisMat30062014().getP61RisMat30062014Null());
        }
        else {
            // COB_CODE: MOVE P61-RIS-MAT-30062014
            //             TO (SF)-RIS-MAT-30062014
            ws.getLccvp6111().getDati().getWp61RisMat30062014().setWp61RisMat30062014(Trunc.toDecimal(ws.getdCrist().getP61RisMat30062014().getP61RisMat30062014(), 15, 3));
        }
        // COB_CODE: IF P61-ID-ADES-NULL = HIGH-VALUES
        //                TO (SF)-ID-ADES-NULL
        //           ELSE
        //                TO (SF)-ID-ADES
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getdCrist().getP61IdAdes().getP61IdAdesNullFormatted())) {
            // COB_CODE: MOVE P61-ID-ADES-NULL
            //             TO (SF)-ID-ADES-NULL
            ws.getLccvp6111().getDati().getWp61IdAdes().setWp61IdAdesNull(ws.getdCrist().getP61IdAdes().getP61IdAdesNull());
        }
        else {
            // COB_CODE: MOVE P61-ID-ADES
            //             TO (SF)-ID-ADES
            ws.getLccvp6111().getDati().getWp61IdAdes().setWp61IdAdes(ws.getdCrist().getP61IdAdes().getP61IdAdes());
        }
        // COB_CODE: IF P61-MONT-LRD-END2000-NULL = HIGH-VALUES
        //                TO (SF)-MONT-LRD-END2000-NULL
        //           ELSE
        //                TO (SF)-MONT-LRD-END2000
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getdCrist().getP61MontLrdEnd2000().getP61MontLrdEnd2000NullFormatted())) {
            // COB_CODE: MOVE P61-MONT-LRD-END2000-NULL
            //             TO (SF)-MONT-LRD-END2000-NULL
            ws.getLccvp6111().getDati().getWp61MontLrdEnd2000().setWp61MontLrdEnd2000Null(ws.getdCrist().getP61MontLrdEnd2000().getP61MontLrdEnd2000Null());
        }
        else {
            // COB_CODE: MOVE P61-MONT-LRD-END2000
            //             TO (SF)-MONT-LRD-END2000
            ws.getLccvp6111().getDati().getWp61MontLrdEnd2000().setWp61MontLrdEnd2000(Trunc.toDecimal(ws.getdCrist().getP61MontLrdEnd2000().getP61MontLrdEnd2000(), 15, 3));
        }
        // COB_CODE: IF P61-PRE-LRD-END2000-NULL = HIGH-VALUES
        //                TO (SF)-PRE-LRD-END2000-NULL
        //           ELSE
        //                TO (SF)-PRE-LRD-END2000
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getdCrist().getP61PreLrdEnd2000().getP61PreLrdEnd2000NullFormatted())) {
            // COB_CODE: MOVE P61-PRE-LRD-END2000-NULL
            //             TO (SF)-PRE-LRD-END2000-NULL
            ws.getLccvp6111().getDati().getWp61PreLrdEnd2000().setWp61PreLrdEnd2000Null(ws.getdCrist().getP61PreLrdEnd2000().getP61PreLrdEnd2000Null());
        }
        else {
            // COB_CODE: MOVE P61-PRE-LRD-END2000
            //             TO (SF)-PRE-LRD-END2000
            ws.getLccvp6111().getDati().getWp61PreLrdEnd2000().setWp61PreLrdEnd2000(Trunc.toDecimal(ws.getdCrist().getP61PreLrdEnd2000().getP61PreLrdEnd2000(), 15, 3));
        }
        // COB_CODE: IF P61-RENDTO-LRD-END2000-NULL = HIGH-VALUES
        //                TO (SF)-RENDTO-LRD-END2000-NULL
        //           ELSE
        //                TO (SF)-RENDTO-LRD-END2000
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getdCrist().getP61RendtoLrdEnd2000().getP61RendtoLrdEnd2000NullFormatted())) {
            // COB_CODE: MOVE P61-RENDTO-LRD-END2000-NULL
            //             TO (SF)-RENDTO-LRD-END2000-NULL
            ws.getLccvp6111().getDati().getWp61RendtoLrdEnd2000().setWp61RendtoLrdEnd2000Null(ws.getdCrist().getP61RendtoLrdEnd2000().getP61RendtoLrdEnd2000Null());
        }
        else {
            // COB_CODE: MOVE P61-RENDTO-LRD-END2000
            //             TO (SF)-RENDTO-LRD-END2000
            ws.getLccvp6111().getDati().getWp61RendtoLrdEnd2000().setWp61RendtoLrdEnd2000(Trunc.toDecimal(ws.getdCrist().getP61RendtoLrdEnd2000().getP61RendtoLrdEnd2000(), 15, 3));
        }
        // COB_CODE: IF P61-MONT-LRD-END2006-NULL = HIGH-VALUES
        //                TO (SF)-MONT-LRD-END2006-NULL
        //           ELSE
        //                TO (SF)-MONT-LRD-END2006
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getdCrist().getP61MontLrdEnd2006().getP61MontLrdEnd2006NullFormatted())) {
            // COB_CODE: MOVE P61-MONT-LRD-END2006-NULL
            //             TO (SF)-MONT-LRD-END2006-NULL
            ws.getLccvp6111().getDati().getWp61MontLrdEnd2006().setWp61MontLrdEnd2006Null(ws.getdCrist().getP61MontLrdEnd2006().getP61MontLrdEnd2006Null());
        }
        else {
            // COB_CODE: MOVE P61-MONT-LRD-END2006
            //             TO (SF)-MONT-LRD-END2006
            ws.getLccvp6111().getDati().getWp61MontLrdEnd2006().setWp61MontLrdEnd2006(Trunc.toDecimal(ws.getdCrist().getP61MontLrdEnd2006().getP61MontLrdEnd2006(), 15, 3));
        }
        // COB_CODE: IF P61-PRE-LRD-END2006-NULL = HIGH-VALUES
        //                TO (SF)-PRE-LRD-END2006-NULL
        //           ELSE
        //                TO (SF)-PRE-LRD-END2006
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getdCrist().getP61PreLrdEnd2006().getP61PreLrdEnd2006NullFormatted())) {
            // COB_CODE: MOVE P61-PRE-LRD-END2006-NULL
            //             TO (SF)-PRE-LRD-END2006-NULL
            ws.getLccvp6111().getDati().getWp61PreLrdEnd2006().setWp61PreLrdEnd2006Null(ws.getdCrist().getP61PreLrdEnd2006().getP61PreLrdEnd2006Null());
        }
        else {
            // COB_CODE: MOVE P61-PRE-LRD-END2006
            //             TO (SF)-PRE-LRD-END2006
            ws.getLccvp6111().getDati().getWp61PreLrdEnd2006().setWp61PreLrdEnd2006(Trunc.toDecimal(ws.getdCrist().getP61PreLrdEnd2006().getP61PreLrdEnd2006(), 15, 3));
        }
        // COB_CODE: IF P61-RENDTO-LRD-END2006-NULL = HIGH-VALUES
        //                TO (SF)-RENDTO-LRD-END2006-NULL
        //           ELSE
        //                TO (SF)-RENDTO-LRD-END2006
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getdCrist().getP61RendtoLrdEnd2006().getP61RendtoLrdEnd2006NullFormatted())) {
            // COB_CODE: MOVE P61-RENDTO-LRD-END2006-NULL
            //             TO (SF)-RENDTO-LRD-END2006-NULL
            ws.getLccvp6111().getDati().getWp61RendtoLrdEnd2006().setWp61RendtoLrdEnd2006Null(ws.getdCrist().getP61RendtoLrdEnd2006().getP61RendtoLrdEnd2006Null());
        }
        else {
            // COB_CODE: MOVE P61-RENDTO-LRD-END2006
            //             TO (SF)-RENDTO-LRD-END2006
            ws.getLccvp6111().getDati().getWp61RendtoLrdEnd2006().setWp61RendtoLrdEnd2006(Trunc.toDecimal(ws.getdCrist().getP61RendtoLrdEnd2006().getP61RendtoLrdEnd2006(), 15, 3));
        }
        // COB_CODE: IF P61-MONT-LRD-DAL2007-NULL = HIGH-VALUES
        //                TO (SF)-MONT-LRD-DAL2007-NULL
        //           ELSE
        //                TO (SF)-MONT-LRD-DAL2007
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getdCrist().getP61MontLrdDal2007().getP61MontLrdDal2007NullFormatted())) {
            // COB_CODE: MOVE P61-MONT-LRD-DAL2007-NULL
            //             TO (SF)-MONT-LRD-DAL2007-NULL
            ws.getLccvp6111().getDati().getWp61MontLrdDal2007().setWp61MontLrdDal2007Null(ws.getdCrist().getP61MontLrdDal2007().getP61MontLrdDal2007Null());
        }
        else {
            // COB_CODE: MOVE P61-MONT-LRD-DAL2007
            //             TO (SF)-MONT-LRD-DAL2007
            ws.getLccvp6111().getDati().getWp61MontLrdDal2007().setWp61MontLrdDal2007(Trunc.toDecimal(ws.getdCrist().getP61MontLrdDal2007().getP61MontLrdDal2007(), 15, 3));
        }
        // COB_CODE: IF P61-PRE-LRD-DAL2007-NULL = HIGH-VALUES
        //                TO (SF)-PRE-LRD-DAL2007-NULL
        //           ELSE
        //                TO (SF)-PRE-LRD-DAL2007
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getdCrist().getP61PreLrdDal2007().getP61PreLrdDal2007NullFormatted())) {
            // COB_CODE: MOVE P61-PRE-LRD-DAL2007-NULL
            //             TO (SF)-PRE-LRD-DAL2007-NULL
            ws.getLccvp6111().getDati().getWp61PreLrdDal2007().setWp61PreLrdDal2007Null(ws.getdCrist().getP61PreLrdDal2007().getP61PreLrdDal2007Null());
        }
        else {
            // COB_CODE: MOVE P61-PRE-LRD-DAL2007
            //             TO (SF)-PRE-LRD-DAL2007
            ws.getLccvp6111().getDati().getWp61PreLrdDal2007().setWp61PreLrdDal2007(Trunc.toDecimal(ws.getdCrist().getP61PreLrdDal2007().getP61PreLrdDal2007(), 15, 3));
        }
        // COB_CODE: IF P61-RENDTO-LRD-DAL2007-NULL = HIGH-VALUES
        //                TO (SF)-RENDTO-LRD-DAL2007-NULL
        //           ELSE
        //                TO (SF)-RENDTO-LRD-DAL2007
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getdCrist().getP61RendtoLrdDal2007().getP61RendtoLrdDal2007NullFormatted())) {
            // COB_CODE: MOVE P61-RENDTO-LRD-DAL2007-NULL
            //             TO (SF)-RENDTO-LRD-DAL2007-NULL
            ws.getLccvp6111().getDati().getWp61RendtoLrdDal2007().setWp61RendtoLrdDal2007Null(ws.getdCrist().getP61RendtoLrdDal2007().getP61RendtoLrdDal2007Null());
        }
        else {
            // COB_CODE: MOVE P61-RENDTO-LRD-DAL2007
            //             TO (SF)-RENDTO-LRD-DAL2007
            ws.getLccvp6111().getDati().getWp61RendtoLrdDal2007().setWp61RendtoLrdDal2007(Trunc.toDecimal(ws.getdCrist().getP61RendtoLrdDal2007().getP61RendtoLrdDal2007(), 15, 3));
        }
        // COB_CODE: IF P61-ID-TRCH-DI-GAR-NULL = HIGH-VALUES
        //                TO (SF)-ID-TRCH-DI-GAR-NULL
        //           ELSE
        //                TO (SF)-ID-TRCH-DI-GAR
        //           END-IF.
        if (Characters.EQ_HIGH.test(ws.getdCrist().getP61IdTrchDiGar().getP61IdTrchDiGarNullFormatted())) {
            // COB_CODE: MOVE P61-ID-TRCH-DI-GAR-NULL
            //             TO (SF)-ID-TRCH-DI-GAR-NULL
            ws.getLccvp6111().getDati().getWp61IdTrchDiGar().setWp61IdTrchDiGarNull(ws.getdCrist().getP61IdTrchDiGar().getP61IdTrchDiGarNull());
        }
        else {
            // COB_CODE: MOVE P61-ID-TRCH-DI-GAR
            //             TO (SF)-ID-TRCH-DI-GAR
            ws.getLccvp6111().getDati().getWp61IdTrchDiGar().setWp61IdTrchDiGar(ws.getdCrist().getP61IdTrchDiGar().getP61IdTrchDiGar());
        }
    }

    /**Original name: INIZIA-TOT-P61<br>
	 * <pre>------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    INIZIALIZZAZIONE CAMPI NULL COPY LCCVP614
	 *    ULTIMO AGG. 24 OTT 2019
	 * ------------------------------------------------------------</pre>*/
    private void iniziaTotP61() {
        // COB_CODE: PERFORM INIZIA-ZEROES-P61 THRU INIZIA-ZEROES-P61-EX
        //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LCCVP614:line=10, because the code is unreachable.
        // COB_CODE: PERFORM INIZIA-SPACES-P61 THRU INIZIA-SPACES-P61-EX
        //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LCCVP614:line=12, because the code is unreachable.
        // COB_CODE: PERFORM INIZIA-NULL-P61 THRU INIZIA-NULL-P61-EX.
        iniziaNullP61();
    }

    /**Original name: INIZIA-NULL-P61<br>*/
    private void iniziaNullP61() {
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-ID-MOVI-CHIU-NULL
        ws.getLccvp6111().getDati().getWp61IdMoviChiu().setWp61IdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Wp61IdMoviChiu.Len.WP61_ID_MOVI_CHIU_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-RIS-MAT-31122011-NULL
        ws.getLccvp6111().getDati().getWp61RisMat31122011().setWp61RisMat31122011Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Wp61RisMat31122011.Len.WP61_RIS_MAT31122011_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PRE-V-31122011-NULL
        ws.getLccvp6111().getDati().getWp61PreV31122011().setWp61PreV31122011Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Wp61PreV31122011.Len.WP61_PRE_V31122011_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PRE-RSH-V-31122011-NULL
        ws.getLccvp6111().getDati().getWp61PreRshV31122011().setWp61PreRshV31122011Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Wp61PreRshV31122011.Len.WP61_PRE_RSH_V31122011_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-CPT-RIVTO-31122011-NULL
        ws.getLccvp6111().getDati().getWp61CptRivto31122011().setWp61CptRivto31122011Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Wp61CptRivto31122011.Len.WP61_CPT_RIVTO31122011_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMPB-VIS-31122011-NULL
        ws.getLccvp6111().getDati().getWp61ImpbVis31122011().setWp61ImpbVis31122011Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Wp61ImpbVis31122011.Len.WP61_IMPB_VIS31122011_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMPB-IS-31122011-NULL
        ws.getLccvp6111().getDati().getWp61ImpbIs31122011().setWp61ImpbIs31122011Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Wp61ImpbIs31122011.Len.WP61_IMPB_IS31122011_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMPB-VIS-RP-P2011-NULL
        ws.getLccvp6111().getDati().getWp61ImpbVisRpP2011().setWp61ImpbVisRpP2011Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Wp61ImpbVisRpP2011.Len.WP61_IMPB_VIS_RP_P2011_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMPB-IS-RP-P2011-NULL
        ws.getLccvp6111().getDati().getWp61ImpbIsRpP2011().setWp61ImpbIsRpP2011Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Wp61ImpbIsRpP2011.Len.WP61_IMPB_IS_RP_P2011_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PRE-V-30062014-NULL
        ws.getLccvp6111().getDati().getWp61PreV30062014().setWp61PreV30062014Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Wp61PreV30062014.Len.WP61_PRE_V30062014_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PRE-RSH-V-30062014-NULL
        ws.getLccvp6111().getDati().getWp61PreRshV30062014().setWp61PreRshV30062014Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Wp61PreRshV30062014.Len.WP61_PRE_RSH_V30062014_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-CPT-INI-30062014-NULL
        ws.getLccvp6111().getDati().getWp61CptIni30062014().setWp61CptIni30062014Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Wp61CptIni30062014.Len.WP61_CPT_INI30062014_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMPB-VIS-30062014-NULL
        ws.getLccvp6111().getDati().getWp61ImpbVis30062014().setWp61ImpbVis30062014Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Wp61ImpbVis30062014.Len.WP61_IMPB_VIS30062014_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMPB-IS-30062014-NULL
        ws.getLccvp6111().getDati().getWp61ImpbIs30062014().setWp61ImpbIs30062014Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Wp61ImpbIs30062014.Len.WP61_IMPB_IS30062014_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMPB-VIS-RP-P62014-NULL
        ws.getLccvp6111().getDati().getWp61ImpbVisRpP62014().setWp61ImpbVisRpP62014Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Wp61ImpbVisRpP62014.Len.WP61_IMPB_VIS_RP_P62014_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMPB-IS-RP-P62014-NULL
        ws.getLccvp6111().getDati().getWp61ImpbIsRpP62014().setWp61ImpbIsRpP62014Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Wp61ImpbIsRpP62014.Len.WP61_IMPB_IS_RP_P62014_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-RIS-MAT-30062014-NULL
        ws.getLccvp6111().getDati().getWp61RisMat30062014().setWp61RisMat30062014Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Wp61RisMat30062014.Len.WP61_RIS_MAT30062014_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-ID-ADES-NULL
        ws.getLccvp6111().getDati().getWp61IdAdes().setWp61IdAdesNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Wp61IdAdes.Len.WP61_ID_ADES_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-MONT-LRD-END2000-NULL
        ws.getLccvp6111().getDati().getWp61MontLrdEnd2000().setWp61MontLrdEnd2000Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Wp61MontLrdEnd2000.Len.WP61_MONT_LRD_END2000_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PRE-LRD-END2000-NULL
        ws.getLccvp6111().getDati().getWp61PreLrdEnd2000().setWp61PreLrdEnd2000Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Wp61PreLrdEnd2000.Len.WP61_PRE_LRD_END2000_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-RENDTO-LRD-END2000-NULL
        ws.getLccvp6111().getDati().getWp61RendtoLrdEnd2000().setWp61RendtoLrdEnd2000Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Wp61RendtoLrdEnd2000.Len.WP61_RENDTO_LRD_END2000_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-MONT-LRD-END2006-NULL
        ws.getLccvp6111().getDati().getWp61MontLrdEnd2006().setWp61MontLrdEnd2006Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Wp61MontLrdEnd2006.Len.WP61_MONT_LRD_END2006_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PRE-LRD-END2006-NULL
        ws.getLccvp6111().getDati().getWp61PreLrdEnd2006().setWp61PreLrdEnd2006Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Wp61PreLrdEnd2006.Len.WP61_PRE_LRD_END2006_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-RENDTO-LRD-END2006-NULL
        ws.getLccvp6111().getDati().getWp61RendtoLrdEnd2006().setWp61RendtoLrdEnd2006Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Wp61RendtoLrdEnd2006.Len.WP61_RENDTO_LRD_END2006_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-MONT-LRD-DAL2007-NULL
        ws.getLccvp6111().getDati().getWp61MontLrdDal2007().setWp61MontLrdDal2007Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Wp61MontLrdDal2007.Len.WP61_MONT_LRD_DAL2007_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PRE-LRD-DAL2007-NULL
        ws.getLccvp6111().getDati().getWp61PreLrdDal2007().setWp61PreLrdDal2007Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Wp61PreLrdDal2007.Len.WP61_PRE_LRD_DAL2007_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-RENDTO-LRD-DAL2007-NULL
        ws.getLccvp6111().getDati().getWp61RendtoLrdDal2007().setWp61RendtoLrdDal2007Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Wp61RendtoLrdDal2007.Len.WP61_RENDTO_LRD_DAL2007_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-ID-TRCH-DI-GAR-NULL.
        ws.getLccvp6111().getDati().getWp61IdTrchDiGar().setWp61IdTrchDiGarNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Wp61IdTrchDiGar.Len.WP61_ID_TRCH_DI_GAR_NULL));
    }

    public void initIxIndici() {
        ws.setIxDclgen(((short)0));
        ws.setIxTabP61Formatted("000000000");
    }

    public void initTabOutput() {
        ivvc0213.getTabOutput().setCodVariabileO("");
        ivvc0213.getTabOutput().setTpDatoO(Types.SPACE_CHAR);
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        ivvc0213.getTabOutput().setValPercO(new AfDecimal(0, 14, 9));
        ivvc0213.getTabOutput().setValStrO("");
    }

    public void initAreaIoDCrist() {
        ws.setDp61EleDCristMax(((short)0));
        ws.getLccvp6111().getStatus().setStatus(Types.SPACE_CHAR);
        ws.getLccvp6111().setIdPtf(0);
        ws.getLccvp6111().getDati().setWp61IdDCrist(0);
        ws.getLccvp6111().getDati().setWp61IdPoli(0);
        ws.getLccvp6111().getDati().setWp61CodCompAnia(0);
        ws.getLccvp6111().getDati().setWp61IdMoviCrz(0);
        ws.getLccvp6111().getDati().getWp61IdMoviChiu().setWp61IdMoviChiu(0);
        ws.getLccvp6111().getDati().setWp61DtIniEff(0);
        ws.getLccvp6111().getDati().setWp61DtEndEff(0);
        ws.getLccvp6111().getDati().setWp61CodProd("");
        ws.getLccvp6111().getDati().setWp61DtDecor(0);
        ws.getLccvp6111().getDati().setWp61DsRiga(0);
        ws.getLccvp6111().getDati().setWp61DsOperSql(Types.SPACE_CHAR);
        ws.getLccvp6111().getDati().setWp61DsVer(0);
        ws.getLccvp6111().getDati().setWp61DsTsIniCptz(0);
        ws.getLccvp6111().getDati().setWp61DsTsEndCptz(0);
        ws.getLccvp6111().getDati().setWp61DsUtente("");
        ws.getLccvp6111().getDati().setWp61DsStatoElab(Types.SPACE_CHAR);
        ws.getLccvp6111().getDati().getWp61RisMat31122011().setWp61RisMat31122011(new AfDecimal(0, 15, 3));
        ws.getLccvp6111().getDati().getWp61PreV31122011().setWp61PreV31122011(new AfDecimal(0, 15, 3));
        ws.getLccvp6111().getDati().getWp61PreRshV31122011().setWp61PreRshV31122011(new AfDecimal(0, 15, 3));
        ws.getLccvp6111().getDati().getWp61CptRivto31122011().setWp61CptRivto31122011(new AfDecimal(0, 15, 3));
        ws.getLccvp6111().getDati().getWp61ImpbVis31122011().setWp61ImpbVis31122011(new AfDecimal(0, 15, 3));
        ws.getLccvp6111().getDati().getWp61ImpbIs31122011().setWp61ImpbIs31122011(new AfDecimal(0, 15, 3));
        ws.getLccvp6111().getDati().getWp61ImpbVisRpP2011().setWp61ImpbVisRpP2011(new AfDecimal(0, 15, 3));
        ws.getLccvp6111().getDati().getWp61ImpbIsRpP2011().setWp61ImpbIsRpP2011(new AfDecimal(0, 15, 3));
        ws.getLccvp6111().getDati().getWp61PreV30062014().setWp61PreV30062014(new AfDecimal(0, 15, 3));
        ws.getLccvp6111().getDati().getWp61PreRshV30062014().setWp61PreRshV30062014(new AfDecimal(0, 15, 3));
        ws.getLccvp6111().getDati().getWp61CptIni30062014().setWp61CptIni30062014(new AfDecimal(0, 15, 3));
        ws.getLccvp6111().getDati().getWp61ImpbVis30062014().setWp61ImpbVis30062014(new AfDecimal(0, 15, 3));
        ws.getLccvp6111().getDati().getWp61ImpbIs30062014().setWp61ImpbIs30062014(new AfDecimal(0, 15, 3));
        ws.getLccvp6111().getDati().getWp61ImpbVisRpP62014().setWp61ImpbVisRpP62014(new AfDecimal(0, 15, 3));
        ws.getLccvp6111().getDati().getWp61ImpbIsRpP62014().setWp61ImpbIsRpP62014(new AfDecimal(0, 15, 3));
        ws.getLccvp6111().getDati().getWp61RisMat30062014().setWp61RisMat30062014(new AfDecimal(0, 15, 3));
        ws.getLccvp6111().getDati().getWp61IdAdes().setWp61IdAdes(0);
        ws.getLccvp6111().getDati().getWp61MontLrdEnd2000().setWp61MontLrdEnd2000(new AfDecimal(0, 15, 3));
        ws.getLccvp6111().getDati().getWp61PreLrdEnd2000().setWp61PreLrdEnd2000(new AfDecimal(0, 15, 3));
        ws.getLccvp6111().getDati().getWp61RendtoLrdEnd2000().setWp61RendtoLrdEnd2000(new AfDecimal(0, 15, 3));
        ws.getLccvp6111().getDati().getWp61MontLrdEnd2006().setWp61MontLrdEnd2006(new AfDecimal(0, 15, 3));
        ws.getLccvp6111().getDati().getWp61PreLrdEnd2006().setWp61PreLrdEnd2006(new AfDecimal(0, 15, 3));
        ws.getLccvp6111().getDati().getWp61RendtoLrdEnd2006().setWp61RendtoLrdEnd2006(new AfDecimal(0, 15, 3));
        ws.getLccvp6111().getDati().getWp61MontLrdDal2007().setWp61MontLrdDal2007(new AfDecimal(0, 15, 3));
        ws.getLccvp6111().getDati().getWp61PreLrdDal2007().setWp61PreLrdDal2007(new AfDecimal(0, 15, 3));
        ws.getLccvp6111().getDati().getWp61RendtoLrdDal2007().setWp61RendtoLrdDal2007(new AfDecimal(0, 15, 3));
        ws.getLccvp6111().getDati().getWp61IdTrchDiGar().setWp61IdTrchDiGar(0);
    }

    public void initAreaIoDCristColl() {
        ws.setWp61EleDCristMax(((short)0));
        ws.getLccvp6112().getStatus().setStatus(Types.SPACE_CHAR);
        ws.getLccvp6112().setIdPtf(0);
        ws.getLccvp6112().getDati().setWp61IdDCrist(0);
        ws.getLccvp6112().getDati().setWp61IdPoli(0);
        ws.getLccvp6112().getDati().setWp61CodCompAnia(0);
        ws.getLccvp6112().getDati().setWp61IdMoviCrz(0);
        ws.getLccvp6112().getDati().getWp61IdMoviChiu().setWp61IdMoviChiu(0);
        ws.getLccvp6112().getDati().setWp61DtIniEff(0);
        ws.getLccvp6112().getDati().setWp61DtEndEff(0);
        ws.getLccvp6112().getDati().setWp61CodProd("");
        ws.getLccvp6112().getDati().setWp61DtDecor(0);
        ws.getLccvp6112().getDati().setWp61DsRiga(0);
        ws.getLccvp6112().getDati().setWp61DsOperSql(Types.SPACE_CHAR);
        ws.getLccvp6112().getDati().setWp61DsVer(0);
        ws.getLccvp6112().getDati().setWp61DsTsIniCptz(0);
        ws.getLccvp6112().getDati().setWp61DsTsEndCptz(0);
        ws.getLccvp6112().getDati().setWp61DsUtente("");
        ws.getLccvp6112().getDati().setWp61DsStatoElab(Types.SPACE_CHAR);
        ws.getLccvp6112().getDati().getWp61RisMat31122011().setWp61RisMat31122011(new AfDecimal(0, 15, 3));
        ws.getLccvp6112().getDati().getWp61PreV31122011().setWp61PreV31122011(new AfDecimal(0, 15, 3));
        ws.getLccvp6112().getDati().getWp61PreRshV31122011().setWp61PreRshV31122011(new AfDecimal(0, 15, 3));
        ws.getLccvp6112().getDati().getWp61CptRivto31122011().setWp61CptRivto31122011(new AfDecimal(0, 15, 3));
        ws.getLccvp6112().getDati().getWp61ImpbVis31122011().setWp61ImpbVis31122011(new AfDecimal(0, 15, 3));
        ws.getLccvp6112().getDati().getWp61ImpbIs31122011().setWp61ImpbIs31122011(new AfDecimal(0, 15, 3));
        ws.getLccvp6112().getDati().getWp61ImpbVisRpP2011().setWp61ImpbVisRpP2011(new AfDecimal(0, 15, 3));
        ws.getLccvp6112().getDati().getWp61ImpbIsRpP2011().setWp61ImpbIsRpP2011(new AfDecimal(0, 15, 3));
        ws.getLccvp6112().getDati().getWp61PreV30062014().setWp61PreV30062014(new AfDecimal(0, 15, 3));
        ws.getLccvp6112().getDati().getWp61PreRshV30062014().setWp61PreRshV30062014(new AfDecimal(0, 15, 3));
        ws.getLccvp6112().getDati().getWp61CptIni30062014().setWp61CptIni30062014(new AfDecimal(0, 15, 3));
        ws.getLccvp6112().getDati().getWp61ImpbVis30062014().setWp61ImpbVis30062014(new AfDecimal(0, 15, 3));
        ws.getLccvp6112().getDati().getWp61ImpbIs30062014().setWp61ImpbIs30062014(new AfDecimal(0, 15, 3));
        ws.getLccvp6112().getDati().getWp61ImpbVisRpP62014().setWp61ImpbVisRpP62014(new AfDecimal(0, 15, 3));
        ws.getLccvp6112().getDati().getWp61ImpbIsRpP62014().setWp61ImpbIsRpP62014(new AfDecimal(0, 15, 3));
        ws.getLccvp6112().getDati().getWp61RisMat30062014().setWp61RisMat30062014(new AfDecimal(0, 15, 3));
        ws.getLccvp6112().getDati().getWp61IdAdes().setWp61IdAdes(0);
        ws.getLccvp6112().getDati().getWp61MontLrdEnd2000().setWp61MontLrdEnd2000(new AfDecimal(0, 15, 3));
        ws.getLccvp6112().getDati().getWp61PreLrdEnd2000().setWp61PreLrdEnd2000(new AfDecimal(0, 15, 3));
        ws.getLccvp6112().getDati().getWp61RendtoLrdEnd2000().setWp61RendtoLrdEnd2000(new AfDecimal(0, 15, 3));
        ws.getLccvp6112().getDati().getWp61MontLrdEnd2006().setWp61MontLrdEnd2006(new AfDecimal(0, 15, 3));
        ws.getLccvp6112().getDati().getWp61PreLrdEnd2006().setWp61PreLrdEnd2006(new AfDecimal(0, 15, 3));
        ws.getLccvp6112().getDati().getWp61RendtoLrdEnd2006().setWp61RendtoLrdEnd2006(new AfDecimal(0, 15, 3));
        ws.getLccvp6112().getDati().getWp61MontLrdDal2007().setWp61MontLrdDal2007(new AfDecimal(0, 15, 3));
        ws.getLccvp6112().getDati().getWp61PreLrdDal2007().setWp61PreLrdDal2007(new AfDecimal(0, 15, 3));
        ws.getLccvp6112().getDati().getWp61RendtoLrdDal2007().setWp61RendtoLrdDal2007(new AfDecimal(0, 15, 3));
        ws.getLccvp6112().getDati().getWp61IdTrchDiGar().setWp61IdTrchDiGar(0);
    }

    public void initPoli() {
        ws.getPoli().setPolIdPoli(0);
        ws.getPoli().setPolIdMoviCrz(0);
        ws.getPoli().getPolIdMoviChiu().setPolIdMoviChiu(0);
        ws.getPoli().setPolIbOgg("");
        ws.getPoli().setPolIbProp("");
        ws.getPoli().getPolDtProp().setPolDtProp(0);
        ws.getPoli().setPolDtIniEff(0);
        ws.getPoli().setPolDtEndEff(0);
        ws.getPoli().setPolCodCompAnia(0);
        ws.getPoli().setPolDtDecor(0);
        ws.getPoli().setPolDtEmis(0);
        ws.getPoli().setPolTpPoli("");
        ws.getPoli().getPolDurAa().setPolDurAa(0);
        ws.getPoli().getPolDurMm().setPolDurMm(0);
        ws.getPoli().getPolDtScad().setPolDtScad(0);
        ws.getPoli().setPolCodProd("");
        ws.getPoli().setPolDtIniVldtProd(0);
        ws.getPoli().setPolCodConv("");
        ws.getPoli().setPolCodRamo("");
        ws.getPoli().getPolDtIniVldtConv().setPolDtIniVldtConv(0);
        ws.getPoli().getPolDtApplzConv().setPolDtApplzConv(0);
        ws.getPoli().setPolTpFrmAssva("");
        ws.getPoli().setPolTpRgmFisc("");
        ws.getPoli().setPolFlEstas(Types.SPACE_CHAR);
        ws.getPoli().setPolFlRshComun(Types.SPACE_CHAR);
        ws.getPoli().setPolFlRshComunCond(Types.SPACE_CHAR);
        ws.getPoli().setPolTpLivGenzTit("");
        ws.getPoli().setPolFlCopFinanz(Types.SPACE_CHAR);
        ws.getPoli().setPolTpApplzDir("");
        ws.getPoli().getPolSpeMed().setPolSpeMed(new AfDecimal(0, 15, 3));
        ws.getPoli().getPolDirEmis().setPolDirEmis(new AfDecimal(0, 15, 3));
        ws.getPoli().getPolDir1oVers().setPolDir1oVers(new AfDecimal(0, 15, 3));
        ws.getPoli().getPolDirVersAgg().setPolDirVersAgg(new AfDecimal(0, 15, 3));
        ws.getPoli().setPolCodDvs("");
        ws.getPoli().setPolFlFntAz(Types.SPACE_CHAR);
        ws.getPoli().setPolFlFntAder(Types.SPACE_CHAR);
        ws.getPoli().setPolFlFntTfr(Types.SPACE_CHAR);
        ws.getPoli().setPolFlFntVolo(Types.SPACE_CHAR);
        ws.getPoli().setPolTpOpzAScad("");
        ws.getPoli().getPolAaDiffProrDflt().setPolAaDiffProrDflt(0);
        ws.getPoli().setPolFlVerProd("");
        ws.getPoli().getPolDurGg().setPolDurGg(0);
        ws.getPoli().getPolDirQuiet().setPolDirQuiet(new AfDecimal(0, 15, 3));
        ws.getPoli().setPolTpPtfEstno("");
        ws.getPoli().setPolFlCumPreCntr(Types.SPACE_CHAR);
        ws.getPoli().setPolFlAmmbMovi(Types.SPACE_CHAR);
        ws.getPoli().setPolConvGeco("");
        ws.getPoli().setPolDsRiga(0);
        ws.getPoli().setPolDsOperSql(Types.SPACE_CHAR);
        ws.getPoli().setPolDsVer(0);
        ws.getPoli().setPolDsTsIniCptz(0);
        ws.getPoli().setPolDsTsEndCptz(0);
        ws.getPoli().setPolDsUtente("");
        ws.getPoli().setPolDsStatoElab(Types.SPACE_CHAR);
        ws.getPoli().setPolFlScudoFisc(Types.SPACE_CHAR);
        ws.getPoli().setPolFlTrasfe(Types.SPACE_CHAR);
        ws.getPoli().setPolFlTfrStrc(Types.SPACE_CHAR);
        ws.getPoli().getPolDtPresc().setPolDtPresc(0);
        ws.getPoli().setPolCodConvAgg("");
        ws.getPoli().setPolSubcatProd("");
        ws.getPoli().setPolFlQuestAdegzAss(Types.SPACE_CHAR);
        ws.getPoli().setPolCodTpa("");
        ws.getPoli().getPolIdAccComm().setPolIdAccComm(0);
        ws.getPoli().setPolFlPoliCpiPr(Types.SPACE_CHAR);
        ws.getPoli().setPolFlPoliBundling(Types.SPACE_CHAR);
        ws.getPoli().setPolIndPoliPrinColl(Types.SPACE_CHAR);
        ws.getPoli().setPolFlVndBundle(Types.SPACE_CHAR);
        ws.getPoli().setPolIbBs("");
        ws.getPoli().setPolFlPoliIfp(Types.SPACE_CHAR);
    }

    public void initDCrist() {
        ws.getdCrist().setP61IdDCrist(0);
        ws.getdCrist().setP61IdPoli(0);
        ws.getdCrist().setP61CodCompAnia(0);
        ws.getdCrist().setP61IdMoviCrz(0);
        ws.getdCrist().getP61IdMoviChiu().setP61IdMoviChiu(0);
        ws.getdCrist().setP61DtIniEff(0);
        ws.getdCrist().setP61DtEndEff(0);
        ws.getdCrist().setP61CodProd("");
        ws.getdCrist().setP61DtDecor(0);
        ws.getdCrist().setP61DsRiga(0);
        ws.getdCrist().setP61DsOperSql(Types.SPACE_CHAR);
        ws.getdCrist().setP61DsVer(0);
        ws.getdCrist().setP61DsTsIniCptz(0);
        ws.getdCrist().setP61DsTsEndCptz(0);
        ws.getdCrist().setP61DsUtente("");
        ws.getdCrist().setP61DsStatoElab(Types.SPACE_CHAR);
        ws.getdCrist().getP61RisMat31122011().setP61RisMat31122011(new AfDecimal(0, 15, 3));
        ws.getdCrist().getP61PreV31122011().setP61PreV31122011(new AfDecimal(0, 15, 3));
        ws.getdCrist().getP61PreRshV31122011().setP61PreRshV31122011(new AfDecimal(0, 15, 3));
        ws.getdCrist().getP61CptRivto31122011().setP61CptRivto31122011(new AfDecimal(0, 15, 3));
        ws.getdCrist().getP61ImpbVis31122011().setP61ImpbVis31122011(new AfDecimal(0, 15, 3));
        ws.getdCrist().getP61ImpbIs31122011().setP61ImpbIs31122011(new AfDecimal(0, 15, 3));
        ws.getdCrist().getP61ImpbVisRpP2011().setP61ImpbVisRpP2011(new AfDecimal(0, 15, 3));
        ws.getdCrist().getP61ImpbIsRpP2011().setP61ImpbIsRpP2011(new AfDecimal(0, 15, 3));
        ws.getdCrist().getP61PreV30062014().setP61PreV30062014(new AfDecimal(0, 15, 3));
        ws.getdCrist().getP61PreRshV30062014().setP61PreRshV30062014(new AfDecimal(0, 15, 3));
        ws.getdCrist().getP61CptIni30062014().setP61CptIni30062014(new AfDecimal(0, 15, 3));
        ws.getdCrist().getP61ImpbVis30062014().setP61ImpbVis30062014(new AfDecimal(0, 15, 3));
        ws.getdCrist().getP61ImpbIs30062014().setP61ImpbIs30062014(new AfDecimal(0, 15, 3));
        ws.getdCrist().getP61ImpbVisRpP62014().setP61ImpbVisRpP62014(new AfDecimal(0, 15, 3));
        ws.getdCrist().getP61ImpbIsRpP62014().setP61ImpbIsRpP62014(new AfDecimal(0, 15, 3));
        ws.getdCrist().getP61RisMat30062014().setP61RisMat30062014(new AfDecimal(0, 15, 3));
        ws.getdCrist().getP61IdAdes().setP61IdAdes(0);
        ws.getdCrist().getP61MontLrdEnd2000().setP61MontLrdEnd2000(new AfDecimal(0, 15, 3));
        ws.getdCrist().getP61PreLrdEnd2000().setP61PreLrdEnd2000(new AfDecimal(0, 15, 3));
        ws.getdCrist().getP61RendtoLrdEnd2000().setP61RendtoLrdEnd2000(new AfDecimal(0, 15, 3));
        ws.getdCrist().getP61MontLrdEnd2006().setP61MontLrdEnd2006(new AfDecimal(0, 15, 3));
        ws.getdCrist().getP61PreLrdEnd2006().setP61PreLrdEnd2006(new AfDecimal(0, 15, 3));
        ws.getdCrist().getP61RendtoLrdEnd2006().setP61RendtoLrdEnd2006(new AfDecimal(0, 15, 3));
        ws.getdCrist().getP61MontLrdDal2007().setP61MontLrdDal2007(new AfDecimal(0, 15, 3));
        ws.getdCrist().getP61PreLrdDal2007().setP61PreLrdDal2007(new AfDecimal(0, 15, 3));
        ws.getdCrist().getP61RendtoLrdDal2007().setP61RendtoLrdDal2007(new AfDecimal(0, 15, 3));
        ws.getdCrist().getP61IdTrchDiGar().setP61IdTrchDiGar(0);
    }

    public void initMovi() {
        ws.getMovi().setMovIdMovi(0);
        ws.getMovi().setMovCodCompAnia(0);
        ws.getMovi().getMovIdOgg().setMovIdOgg(0);
        ws.getMovi().setMovIbOgg("");
        ws.getMovi().setMovIbMovi("");
        ws.getMovi().setMovTpOgg("");
        ws.getMovi().getMovIdRich().setMovIdRich(0);
        ws.getMovi().getMovTpMovi().setMovTpMovi(0);
        ws.getMovi().setMovDtEff(0);
        ws.getMovi().getMovIdMoviAnn().setMovIdMoviAnn(0);
        ws.getMovi().getMovIdMoviCollg().setMovIdMoviCollg(0);
        ws.getMovi().setMovDsOperSql(Types.SPACE_CHAR);
        ws.getMovi().setMovDsVer(0);
        ws.getMovi().setMovDsTsCptz(0);
        ws.getMovi().setMovDsUtente("");
        ws.getMovi().setMovDsStatoElab(Types.SPACE_CHAR);
    }
}
