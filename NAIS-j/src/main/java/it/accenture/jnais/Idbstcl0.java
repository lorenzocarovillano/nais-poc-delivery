package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.TcontLiqDao;
import it.accenture.jnais.commons.data.to.ITcontLiq;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idbstcl0Data;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.redefines.TclDtVlt;
import it.accenture.jnais.ws.redefines.TclIdMoviChiu;
import it.accenture.jnais.ws.redefines.TclImpbAcc;
import it.accenture.jnais.ws.redefines.TclImpbImpst252;
import it.accenture.jnais.ws.redefines.TclImpbImpstPrvr;
import it.accenture.jnais.ws.redefines.TclImpbIs;
import it.accenture.jnais.ws.redefines.TclImpbIs1382011;
import it.accenture.jnais.ws.redefines.TclImpbIs662014;
import it.accenture.jnais.ws.redefines.TclImpbTfr;
import it.accenture.jnais.ws.redefines.TclImpbVis;
import it.accenture.jnais.ws.redefines.TclImpbVis1382011;
import it.accenture.jnais.ws.redefines.TclImpbVis662014;
import it.accenture.jnais.ws.redefines.TclImpCortvo;
import it.accenture.jnais.ws.redefines.TclImpDirLiq;
import it.accenture.jnais.ws.redefines.TclImpEfflq;
import it.accenture.jnais.ws.redefines.TclImpIntrPrest;
import it.accenture.jnais.ws.redefines.TclImpIntrRitPag;
import it.accenture.jnais.ws.redefines.TclImpIs;
import it.accenture.jnais.ws.redefines.TclImpLrdLiqto;
import it.accenture.jnais.ws.redefines.TclImpNetLiqto;
import it.accenture.jnais.ws.redefines.TclImpPrest;
import it.accenture.jnais.ws.redefines.TclImpRat;
import it.accenture.jnais.ws.redefines.TclImpRimb;
import it.accenture.jnais.ws.redefines.TclImpRitAcc;
import it.accenture.jnais.ws.redefines.TclImpRitTfr;
import it.accenture.jnais.ws.redefines.TclImpRitVis;
import it.accenture.jnais.ws.redefines.TclImpst252;
import it.accenture.jnais.ws.redefines.TclImpstPrvr;
import it.accenture.jnais.ws.redefines.TclImpstSost1382011;
import it.accenture.jnais.ws.redefines.TclImpstSost662014;
import it.accenture.jnais.ws.redefines.TclImpstVis1382011;
import it.accenture.jnais.ws.redefines.TclImpstVis662014;
import it.accenture.jnais.ws.redefines.TclImpUti;
import it.accenture.jnais.ws.TcontLiq;

/**Original name: IDBSTCL0<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  02 LUG 2014.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Idbstcl0 extends Program implements ITcontLiq {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private TcontLiqDao tcontLiqDao = new TcontLiqDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Idbstcl0Data ws = new Idbstcl0Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: TCONT-LIQ
    private TcontLiq tcontLiq;

    //==== METHODS ====
    /**Original name: PROGRAM_IDBSTCL0_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, TcontLiq tcontLiq) {
        this.idsv0003 = idsv0003;
        this.tcontLiq = tcontLiq;
        // COB_CODE: PERFORM A000-INIZIO                    THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO          THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS          THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO          THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                        a300ElaboraIdEff();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                        a400ElaboraIdpEff();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO          THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS          THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO          THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                        b300ElaboraIdCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                        b400ElaboraIdpCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                        b500ElaboraIboCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                        b600ElaboraIbsCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                        b700ElaboraIdoCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //              SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-PRIMARY-KEY
                //                 PERFORM A200-ELABORA-PK          THRU A200-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO         THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS         THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO         THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.PRIMARY_KEY:// COB_CODE: PERFORM A200-ELABORA-PK          THRU A200-EX
                        a200ElaboraPk();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO         THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS         THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO         THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Idbstcl0 getInstance() {
        return ((Idbstcl0)Programs.getInstance(Idbstcl0.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'IDBSTCL0'   TO IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("IDBSTCL0");
        // COB_CODE: MOVE 'TCONT_LIQ' TO IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("TCONT_LIQ");
        // COB_CODE: MOVE '00'                     TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                   TO   IDSV0003-SQLCODE
        //                                              IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                              IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-AGG-STORICO-SOLO-INS  OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isAggStoricoSoloIns() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-PK<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a200ElaboraPk() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-PK          THRU A210-EX
        //              WHEN IDSV0003-INSERT
        //                 PERFORM A220-INSERT-PK          THRU A220-EX
        //              WHEN IDSV0003-UPDATE
        //                 PERFORM A230-UPDATE-PK          THRU A230-EX
        //              WHEN IDSV0003-DELETE
        //                 PERFORM A240-DELETE-PK          THRU A240-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-PK          THRU A210-EX
            a210SelectPk();
        }
        else if (idsv0003.getOperazione().isInsert()) {
            // COB_CODE: PERFORM A220-INSERT-PK          THRU A220-EX
            a220InsertPk();
        }
        else if (idsv0003.getOperazione().isUpdate()) {
            // COB_CODE: PERFORM A230-UPDATE-PK          THRU A230-EX
            a230UpdatePk();
        }
        else if (idsv0003.getOperazione().isDelete()) {
            // COB_CODE: PERFORM A240-DELETE-PK          THRU A240-EX
            a240DeletePk();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A300-ELABORA-ID-EFF<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void a300ElaboraIdEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A310-SELECT-ID-EFF          THRU A310-EX
            a310SelectIdEff();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A400-ELABORA-IDP-EFF<br>*/
    private void a400ElaboraIdpEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
            a410SelectIdpEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
            a460OpenCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
            a470CloseCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
            a480FetchFirstIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
            a490FetchNextIdpEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A500-ELABORA-IBO<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a500ElaboraIbo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A510-SELECT-IBO             THRU A510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A510-SELECT-IBO             THRU A510-EX
            a510SelectIbo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
            a560OpenCursorIbo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
            a570CloseCursorIbo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
            a580FetchFirstIbo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
            a590FetchNextIbo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A600-ELABORA-IBS<br>*/
    private void a600ElaboraIbs() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A610-SELECT-IBS             THRU A610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A610-SELECT-IBS             THRU A610-EX
            a610SelectIbs();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
            a660OpenCursorIbs();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
            a670CloseCursorIbs();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
            a680FetchFirstIbs();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
            a690FetchNextIbs();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A700-ELABORA-IDO<br>*/
    private void a700ElaboraIdo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A710-SELECT-IDO                 THRU A710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A710-SELECT-IDO                 THRU A710-EX
            a710SelectIdo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
            a760OpenCursorIdo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
            a770CloseCursorIdo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
            a780FetchFirstIdo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
            a790FetchNextIdo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B300-ELABORA-ID-CPZ<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void b300ElaboraIdCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
            b310SelectIdCpz();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B400-ELABORA-IDP-CPZ<br>*/
    private void b400ElaboraIdpCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
            b410SelectIdpCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
            b460OpenCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
            b470CloseCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
            b480FetchFirstIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
            b490FetchNextIdpCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B500-ELABORA-IBO-CPZ<br>*/
    private void b500ElaboraIboCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
            b510SelectIboCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
            b560OpenCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
            b570CloseCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
            b580FetchFirstIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B600-ELABORA-IBS-CPZ<br>*/
    private void b600ElaboraIbsCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
            b610SelectIbsCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
            b660OpenCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
            b670CloseCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
            b680FetchFirstIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B700-ELABORA-IDO-CPZ<br>*/
    private void b700ElaboraIdoCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
            b710SelectIdoCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
            b760OpenCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
            b770CloseCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
            b780FetchFirstIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A210-SELECT-PK<br>
	 * <pre>----
	 * ----  gestione PK
	 * ----</pre>*/
    private void a210SelectPk() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_TCONT_LIQ
        //                ,ID_PERC_LIQ
        //                ,ID_BNFICR_LIQ
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,DT_VLT
        //                ,IMP_LRD_LIQTO
        //                ,IMP_PREST
        //                ,IMP_INTR_PREST
        //                ,IMP_RAT
        //                ,IMP_UTI
        //                ,IMP_RIT_TFR
        //                ,IMP_RIT_ACC
        //                ,IMP_RIT_VIS
        //                ,IMPB_TFR
        //                ,IMPB_ACC
        //                ,IMPB_VIS
        //                ,IMP_RIMB
        //                ,IMP_CORTVO
        //                ,IMPB_IMPST_PRVR
        //                ,IMPST_PRVR
        //                ,IMPB_IMPST_252
        //                ,IMPST_252
        //                ,IMP_IS
        //                ,IMP_DIR_LIQ
        //                ,IMP_NET_LIQTO
        //                ,IMP_EFFLQ
        //                ,COD_DVS
        //                ,TP_MEZ_PAG_ACCR
        //                ,ESTR_CNT_CORR_ACCR
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,TP_STAT_TIT
        //                ,IMPB_VIS_1382011
        //                ,IMPST_VIS_1382011
        //                ,IMPST_SOST_1382011
        //                ,IMP_INTR_RIT_PAG
        //                ,IMPB_IS
        //                ,IMPB_IS_1382011
        //                ,IMPB_VIS_662014
        //                ,IMPST_VIS_662014
        //                ,IMPB_IS_662014
        //                ,IMPST_SOST_662014
        //             INTO
        //                :TCL-ID-TCONT-LIQ
        //               ,:TCL-ID-PERC-LIQ
        //               ,:TCL-ID-BNFICR-LIQ
        //               ,:TCL-ID-MOVI-CRZ
        //               ,:TCL-ID-MOVI-CHIU
        //                :IND-TCL-ID-MOVI-CHIU
        //               ,:TCL-DT-INI-EFF-DB
        //               ,:TCL-DT-END-EFF-DB
        //               ,:TCL-COD-COMP-ANIA
        //               ,:TCL-DT-VLT-DB
        //                :IND-TCL-DT-VLT
        //               ,:TCL-IMP-LRD-LIQTO
        //                :IND-TCL-IMP-LRD-LIQTO
        //               ,:TCL-IMP-PREST
        //                :IND-TCL-IMP-PREST
        //               ,:TCL-IMP-INTR-PREST
        //                :IND-TCL-IMP-INTR-PREST
        //               ,:TCL-IMP-RAT
        //                :IND-TCL-IMP-RAT
        //               ,:TCL-IMP-UTI
        //                :IND-TCL-IMP-UTI
        //               ,:TCL-IMP-RIT-TFR
        //                :IND-TCL-IMP-RIT-TFR
        //               ,:TCL-IMP-RIT-ACC
        //                :IND-TCL-IMP-RIT-ACC
        //               ,:TCL-IMP-RIT-VIS
        //                :IND-TCL-IMP-RIT-VIS
        //               ,:TCL-IMPB-TFR
        //                :IND-TCL-IMPB-TFR
        //               ,:TCL-IMPB-ACC
        //                :IND-TCL-IMPB-ACC
        //               ,:TCL-IMPB-VIS
        //                :IND-TCL-IMPB-VIS
        //               ,:TCL-IMP-RIMB
        //                :IND-TCL-IMP-RIMB
        //               ,:TCL-IMP-CORTVO
        //                :IND-TCL-IMP-CORTVO
        //               ,:TCL-IMPB-IMPST-PRVR
        //                :IND-TCL-IMPB-IMPST-PRVR
        //               ,:TCL-IMPST-PRVR
        //                :IND-TCL-IMPST-PRVR
        //               ,:TCL-IMPB-IMPST-252
        //                :IND-TCL-IMPB-IMPST-252
        //               ,:TCL-IMPST-252
        //                :IND-TCL-IMPST-252
        //               ,:TCL-IMP-IS
        //                :IND-TCL-IMP-IS
        //               ,:TCL-IMP-DIR-LIQ
        //                :IND-TCL-IMP-DIR-LIQ
        //               ,:TCL-IMP-NET-LIQTO
        //                :IND-TCL-IMP-NET-LIQTO
        //               ,:TCL-IMP-EFFLQ
        //                :IND-TCL-IMP-EFFLQ
        //               ,:TCL-COD-DVS
        //                :IND-TCL-COD-DVS
        //               ,:TCL-TP-MEZ-PAG-ACCR
        //                :IND-TCL-TP-MEZ-PAG-ACCR
        //               ,:TCL-ESTR-CNT-CORR-ACCR
        //                :IND-TCL-ESTR-CNT-CORR-ACCR
        //               ,:TCL-DS-RIGA
        //               ,:TCL-DS-OPER-SQL
        //               ,:TCL-DS-VER
        //               ,:TCL-DS-TS-INI-CPTZ
        //               ,:TCL-DS-TS-END-CPTZ
        //               ,:TCL-DS-UTENTE
        //               ,:TCL-DS-STATO-ELAB
        //               ,:TCL-TP-STAT-TIT
        //                :IND-TCL-TP-STAT-TIT
        //               ,:TCL-IMPB-VIS-1382011
        //                :IND-TCL-IMPB-VIS-1382011
        //               ,:TCL-IMPST-VIS-1382011
        //                :IND-TCL-IMPST-VIS-1382011
        //               ,:TCL-IMPST-SOST-1382011
        //                :IND-TCL-IMPST-SOST-1382011
        //               ,:TCL-IMP-INTR-RIT-PAG
        //                :IND-TCL-IMP-INTR-RIT-PAG
        //               ,:TCL-IMPB-IS
        //                :IND-TCL-IMPB-IS
        //               ,:TCL-IMPB-IS-1382011
        //                :IND-TCL-IMPB-IS-1382011
        //               ,:TCL-IMPB-VIS-662014
        //                :IND-TCL-IMPB-VIS-662014
        //               ,:TCL-IMPST-VIS-662014
        //                :IND-TCL-IMPST-VIS-662014
        //               ,:TCL-IMPB-IS-662014
        //                :IND-TCL-IMPB-IS-662014
        //               ,:TCL-IMPST-SOST-662014
        //                :IND-TCL-IMPST-SOST-662014
        //             FROM TCONT_LIQ
        //             WHERE     DS_RIGA = :TCL-DS-RIGA
        //           END-EXEC.
        tcontLiqDao.selectByTclDsRiga(tcontLiq.getTclDsRiga(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A220-INSERT-PK<br>*/
    private void a220InsertPk() {
        // COB_CODE: PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.
        z400SeqRiga();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX
            z150ValorizzaDataServicesI();
            // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX
            z200SetIndicatoriNull();
            // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX
            z900ConvertiNToX();
            // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX
            z960LengthVchar();
            // COB_CODE: EXEC SQL
            //              INSERT
            //              INTO TCONT_LIQ
            //                  (
            //                     ID_TCONT_LIQ
            //                    ,ID_PERC_LIQ
            //                    ,ID_BNFICR_LIQ
            //                    ,ID_MOVI_CRZ
            //                    ,ID_MOVI_CHIU
            //                    ,DT_INI_EFF
            //                    ,DT_END_EFF
            //                    ,COD_COMP_ANIA
            //                    ,DT_VLT
            //                    ,IMP_LRD_LIQTO
            //                    ,IMP_PREST
            //                    ,IMP_INTR_PREST
            //                    ,IMP_RAT
            //                    ,IMP_UTI
            //                    ,IMP_RIT_TFR
            //                    ,IMP_RIT_ACC
            //                    ,IMP_RIT_VIS
            //                    ,IMPB_TFR
            //                    ,IMPB_ACC
            //                    ,IMPB_VIS
            //                    ,IMP_RIMB
            //                    ,IMP_CORTVO
            //                    ,IMPB_IMPST_PRVR
            //                    ,IMPST_PRVR
            //                    ,IMPB_IMPST_252
            //                    ,IMPST_252
            //                    ,IMP_IS
            //                    ,IMP_DIR_LIQ
            //                    ,IMP_NET_LIQTO
            //                    ,IMP_EFFLQ
            //                    ,COD_DVS
            //                    ,TP_MEZ_PAG_ACCR
            //                    ,ESTR_CNT_CORR_ACCR
            //                    ,DS_RIGA
            //                    ,DS_OPER_SQL
            //                    ,DS_VER
            //                    ,DS_TS_INI_CPTZ
            //                    ,DS_TS_END_CPTZ
            //                    ,DS_UTENTE
            //                    ,DS_STATO_ELAB
            //                    ,TP_STAT_TIT
            //                    ,IMPB_VIS_1382011
            //                    ,IMPST_VIS_1382011
            //                    ,IMPST_SOST_1382011
            //                    ,IMP_INTR_RIT_PAG
            //                    ,IMPB_IS
            //                    ,IMPB_IS_1382011
            //                    ,IMPB_VIS_662014
            //                    ,IMPST_VIS_662014
            //                    ,IMPB_IS_662014
            //                    ,IMPST_SOST_662014
            //                  )
            //              VALUES
            //                  (
            //                    :TCL-ID-TCONT-LIQ
            //                    ,:TCL-ID-PERC-LIQ
            //                    ,:TCL-ID-BNFICR-LIQ
            //                    ,:TCL-ID-MOVI-CRZ
            //                    ,:TCL-ID-MOVI-CHIU
            //                     :IND-TCL-ID-MOVI-CHIU
            //                    ,:TCL-DT-INI-EFF-DB
            //                    ,:TCL-DT-END-EFF-DB
            //                    ,:TCL-COD-COMP-ANIA
            //                    ,:TCL-DT-VLT-DB
            //                     :IND-TCL-DT-VLT
            //                    ,:TCL-IMP-LRD-LIQTO
            //                     :IND-TCL-IMP-LRD-LIQTO
            //                    ,:TCL-IMP-PREST
            //                     :IND-TCL-IMP-PREST
            //                    ,:TCL-IMP-INTR-PREST
            //                     :IND-TCL-IMP-INTR-PREST
            //                    ,:TCL-IMP-RAT
            //                     :IND-TCL-IMP-RAT
            //                    ,:TCL-IMP-UTI
            //                     :IND-TCL-IMP-UTI
            //                    ,:TCL-IMP-RIT-TFR
            //                     :IND-TCL-IMP-RIT-TFR
            //                    ,:TCL-IMP-RIT-ACC
            //                     :IND-TCL-IMP-RIT-ACC
            //                    ,:TCL-IMP-RIT-VIS
            //                     :IND-TCL-IMP-RIT-VIS
            //                    ,:TCL-IMPB-TFR
            //                     :IND-TCL-IMPB-TFR
            //                    ,:TCL-IMPB-ACC
            //                     :IND-TCL-IMPB-ACC
            //                    ,:TCL-IMPB-VIS
            //                     :IND-TCL-IMPB-VIS
            //                    ,:TCL-IMP-RIMB
            //                     :IND-TCL-IMP-RIMB
            //                    ,:TCL-IMP-CORTVO
            //                     :IND-TCL-IMP-CORTVO
            //                    ,:TCL-IMPB-IMPST-PRVR
            //                     :IND-TCL-IMPB-IMPST-PRVR
            //                    ,:TCL-IMPST-PRVR
            //                     :IND-TCL-IMPST-PRVR
            //                    ,:TCL-IMPB-IMPST-252
            //                     :IND-TCL-IMPB-IMPST-252
            //                    ,:TCL-IMPST-252
            //                     :IND-TCL-IMPST-252
            //                    ,:TCL-IMP-IS
            //                     :IND-TCL-IMP-IS
            //                    ,:TCL-IMP-DIR-LIQ
            //                     :IND-TCL-IMP-DIR-LIQ
            //                    ,:TCL-IMP-NET-LIQTO
            //                     :IND-TCL-IMP-NET-LIQTO
            //                    ,:TCL-IMP-EFFLQ
            //                     :IND-TCL-IMP-EFFLQ
            //                    ,:TCL-COD-DVS
            //                     :IND-TCL-COD-DVS
            //                    ,:TCL-TP-MEZ-PAG-ACCR
            //                     :IND-TCL-TP-MEZ-PAG-ACCR
            //                    ,:TCL-ESTR-CNT-CORR-ACCR
            //                     :IND-TCL-ESTR-CNT-CORR-ACCR
            //                    ,:TCL-DS-RIGA
            //                    ,:TCL-DS-OPER-SQL
            //                    ,:TCL-DS-VER
            //                    ,:TCL-DS-TS-INI-CPTZ
            //                    ,:TCL-DS-TS-END-CPTZ
            //                    ,:TCL-DS-UTENTE
            //                    ,:TCL-DS-STATO-ELAB
            //                    ,:TCL-TP-STAT-TIT
            //                     :IND-TCL-TP-STAT-TIT
            //                    ,:TCL-IMPB-VIS-1382011
            //                     :IND-TCL-IMPB-VIS-1382011
            //                    ,:TCL-IMPST-VIS-1382011
            //                     :IND-TCL-IMPST-VIS-1382011
            //                    ,:TCL-IMPST-SOST-1382011
            //                     :IND-TCL-IMPST-SOST-1382011
            //                    ,:TCL-IMP-INTR-RIT-PAG
            //                     :IND-TCL-IMP-INTR-RIT-PAG
            //                    ,:TCL-IMPB-IS
            //                     :IND-TCL-IMPB-IS
            //                    ,:TCL-IMPB-IS-1382011
            //                     :IND-TCL-IMPB-IS-1382011
            //                    ,:TCL-IMPB-VIS-662014
            //                     :IND-TCL-IMPB-VIS-662014
            //                    ,:TCL-IMPST-VIS-662014
            //                     :IND-TCL-IMPST-VIS-662014
            //                    ,:TCL-IMPB-IS-662014
            //                     :IND-TCL-IMPB-IS-662014
            //                    ,:TCL-IMPST-SOST-662014
            //                     :IND-TCL-IMPST-SOST-662014
            //                  )
            //           END-EXEC
            tcontLiqDao.insertRec(this);
            // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
            a100CheckReturnCode();
        }
    }

    /**Original name: A230-UPDATE-PK<br>*/
    private void a230UpdatePk() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE TCONT_LIQ SET
        //                   ID_TCONT_LIQ           =
        //                :TCL-ID-TCONT-LIQ
        //                  ,ID_PERC_LIQ            =
        //                :TCL-ID-PERC-LIQ
        //                  ,ID_BNFICR_LIQ          =
        //                :TCL-ID-BNFICR-LIQ
        //                  ,ID_MOVI_CRZ            =
        //                :TCL-ID-MOVI-CRZ
        //                  ,ID_MOVI_CHIU           =
        //                :TCL-ID-MOVI-CHIU
        //                                       :IND-TCL-ID-MOVI-CHIU
        //                  ,DT_INI_EFF             =
        //           :TCL-DT-INI-EFF-DB
        //                  ,DT_END_EFF             =
        //           :TCL-DT-END-EFF-DB
        //                  ,COD_COMP_ANIA          =
        //                :TCL-COD-COMP-ANIA
        //                  ,DT_VLT                 =
        //           :TCL-DT-VLT-DB
        //                                       :IND-TCL-DT-VLT
        //                  ,IMP_LRD_LIQTO          =
        //                :TCL-IMP-LRD-LIQTO
        //                                       :IND-TCL-IMP-LRD-LIQTO
        //                  ,IMP_PREST              =
        //                :TCL-IMP-PREST
        //                                       :IND-TCL-IMP-PREST
        //                  ,IMP_INTR_PREST         =
        //                :TCL-IMP-INTR-PREST
        //                                       :IND-TCL-IMP-INTR-PREST
        //                  ,IMP_RAT                =
        //                :TCL-IMP-RAT
        //                                       :IND-TCL-IMP-RAT
        //                  ,IMP_UTI                =
        //                :TCL-IMP-UTI
        //                                       :IND-TCL-IMP-UTI
        //                  ,IMP_RIT_TFR            =
        //                :TCL-IMP-RIT-TFR
        //                                       :IND-TCL-IMP-RIT-TFR
        //                  ,IMP_RIT_ACC            =
        //                :TCL-IMP-RIT-ACC
        //                                       :IND-TCL-IMP-RIT-ACC
        //                  ,IMP_RIT_VIS            =
        //                :TCL-IMP-RIT-VIS
        //                                       :IND-TCL-IMP-RIT-VIS
        //                  ,IMPB_TFR               =
        //                :TCL-IMPB-TFR
        //                                       :IND-TCL-IMPB-TFR
        //                  ,IMPB_ACC               =
        //                :TCL-IMPB-ACC
        //                                       :IND-TCL-IMPB-ACC
        //                  ,IMPB_VIS               =
        //                :TCL-IMPB-VIS
        //                                       :IND-TCL-IMPB-VIS
        //                  ,IMP_RIMB               =
        //                :TCL-IMP-RIMB
        //                                       :IND-TCL-IMP-RIMB
        //                  ,IMP_CORTVO             =
        //                :TCL-IMP-CORTVO
        //                                       :IND-TCL-IMP-CORTVO
        //                  ,IMPB_IMPST_PRVR        =
        //                :TCL-IMPB-IMPST-PRVR
        //                                       :IND-TCL-IMPB-IMPST-PRVR
        //                  ,IMPST_PRVR             =
        //                :TCL-IMPST-PRVR
        //                                       :IND-TCL-IMPST-PRVR
        //                  ,IMPB_IMPST_252         =
        //                :TCL-IMPB-IMPST-252
        //                                       :IND-TCL-IMPB-IMPST-252
        //                  ,IMPST_252              =
        //                :TCL-IMPST-252
        //                                       :IND-TCL-IMPST-252
        //                  ,IMP_IS                 =
        //                :TCL-IMP-IS
        //                                       :IND-TCL-IMP-IS
        //                  ,IMP_DIR_LIQ            =
        //                :TCL-IMP-DIR-LIQ
        //                                       :IND-TCL-IMP-DIR-LIQ
        //                  ,IMP_NET_LIQTO          =
        //                :TCL-IMP-NET-LIQTO
        //                                       :IND-TCL-IMP-NET-LIQTO
        //                  ,IMP_EFFLQ              =
        //                :TCL-IMP-EFFLQ
        //                                       :IND-TCL-IMP-EFFLQ
        //                  ,COD_DVS                =
        //                :TCL-COD-DVS
        //                                       :IND-TCL-COD-DVS
        //                  ,TP_MEZ_PAG_ACCR        =
        //                :TCL-TP-MEZ-PAG-ACCR
        //                                       :IND-TCL-TP-MEZ-PAG-ACCR
        //                  ,ESTR_CNT_CORR_ACCR     =
        //                :TCL-ESTR-CNT-CORR-ACCR
        //                                       :IND-TCL-ESTR-CNT-CORR-ACCR
        //                  ,DS_RIGA                =
        //                :TCL-DS-RIGA
        //                  ,DS_OPER_SQL            =
        //                :TCL-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :TCL-DS-VER
        //                  ,DS_TS_INI_CPTZ         =
        //                :TCL-DS-TS-INI-CPTZ
        //                  ,DS_TS_END_CPTZ         =
        //                :TCL-DS-TS-END-CPTZ
        //                  ,DS_UTENTE              =
        //                :TCL-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :TCL-DS-STATO-ELAB
        //                  ,TP_STAT_TIT            =
        //                :TCL-TP-STAT-TIT
        //                                       :IND-TCL-TP-STAT-TIT
        //                  ,IMPB_VIS_1382011       =
        //                :TCL-IMPB-VIS-1382011
        //                                       :IND-TCL-IMPB-VIS-1382011
        //                  ,IMPST_VIS_1382011      =
        //                :TCL-IMPST-VIS-1382011
        //                                       :IND-TCL-IMPST-VIS-1382011
        //                  ,IMPST_SOST_1382011     =
        //                :TCL-IMPST-SOST-1382011
        //                                       :IND-TCL-IMPST-SOST-1382011
        //                  ,IMP_INTR_RIT_PAG       =
        //                :TCL-IMP-INTR-RIT-PAG
        //                                       :IND-TCL-IMP-INTR-RIT-PAG
        //                  ,IMPB_IS                =
        //                :TCL-IMPB-IS
        //                                       :IND-TCL-IMPB-IS
        //                  ,IMPB_IS_1382011        =
        //                :TCL-IMPB-IS-1382011
        //                                       :IND-TCL-IMPB-IS-1382011
        //                  ,IMPB_VIS_662014        =
        //                :TCL-IMPB-VIS-662014
        //                                       :IND-TCL-IMPB-VIS-662014
        //                  ,IMPST_VIS_662014       =
        //                :TCL-IMPST-VIS-662014
        //                                       :IND-TCL-IMPST-VIS-662014
        //                  ,IMPB_IS_662014         =
        //                :TCL-IMPB-IS-662014
        //                                       :IND-TCL-IMPB-IS-662014
        //                  ,IMPST_SOST_662014      =
        //                :TCL-IMPST-SOST-662014
        //                                       :IND-TCL-IMPST-SOST-662014
        //                WHERE     DS_RIGA = :TCL-DS-RIGA
        //           END-EXEC.
        tcontLiqDao.updateRec(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A240-DELETE-PK<br>*/
    private void a240DeletePk() {
        // COB_CODE: EXEC SQL
        //                DELETE
        //                FROM TCONT_LIQ
        //                WHERE     DS_RIGA = :TCL-DS-RIGA
        //           END-EXEC.
        tcontLiqDao.deleteByTclDsRiga(tcontLiq.getTclDsRiga());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A305-DECLARE-CURSOR-ID-EFF<br>
	 * <pre>----
	 * ----  gestione ID Effetto
	 * ----</pre>*/
    private void a305DeclareCursorIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-ID-UPD-EFF-TCL CURSOR FOR
        //              SELECT
        //                     ID_TCONT_LIQ
        //                    ,ID_PERC_LIQ
        //                    ,ID_BNFICR_LIQ
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,DT_VLT
        //                    ,IMP_LRD_LIQTO
        //                    ,IMP_PREST
        //                    ,IMP_INTR_PREST
        //                    ,IMP_RAT
        //                    ,IMP_UTI
        //                    ,IMP_RIT_TFR
        //                    ,IMP_RIT_ACC
        //                    ,IMP_RIT_VIS
        //                    ,IMPB_TFR
        //                    ,IMPB_ACC
        //                    ,IMPB_VIS
        //                    ,IMP_RIMB
        //                    ,IMP_CORTVO
        //                    ,IMPB_IMPST_PRVR
        //                    ,IMPST_PRVR
        //                    ,IMPB_IMPST_252
        //                    ,IMPST_252
        //                    ,IMP_IS
        //                    ,IMP_DIR_LIQ
        //                    ,IMP_NET_LIQTO
        //                    ,IMP_EFFLQ
        //                    ,COD_DVS
        //                    ,TP_MEZ_PAG_ACCR
        //                    ,ESTR_CNT_CORR_ACCR
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,TP_STAT_TIT
        //                    ,IMPB_VIS_1382011
        //                    ,IMPST_VIS_1382011
        //                    ,IMPST_SOST_1382011
        //                    ,IMP_INTR_RIT_PAG
        //                    ,IMPB_IS
        //                    ,IMPB_IS_1382011
        //                    ,IMPB_VIS_662014
        //                    ,IMPST_VIS_662014
        //                    ,IMPB_IS_662014
        //                    ,IMPST_SOST_662014
        //              FROM TCONT_LIQ
        //              WHERE     ID_TCONT_LIQ = :TCL-ID-TCONT-LIQ
        //                    AND DS_TS_END_CPTZ = :WS-TS-INFINITO
        //                    AND DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //              ORDER BY DT_INI_EFF ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A310-SELECT-ID-EFF<br>*/
    private void a310SelectIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_TCONT_LIQ
        //                ,ID_PERC_LIQ
        //                ,ID_BNFICR_LIQ
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,DT_VLT
        //                ,IMP_LRD_LIQTO
        //                ,IMP_PREST
        //                ,IMP_INTR_PREST
        //                ,IMP_RAT
        //                ,IMP_UTI
        //                ,IMP_RIT_TFR
        //                ,IMP_RIT_ACC
        //                ,IMP_RIT_VIS
        //                ,IMPB_TFR
        //                ,IMPB_ACC
        //                ,IMPB_VIS
        //                ,IMP_RIMB
        //                ,IMP_CORTVO
        //                ,IMPB_IMPST_PRVR
        //                ,IMPST_PRVR
        //                ,IMPB_IMPST_252
        //                ,IMPST_252
        //                ,IMP_IS
        //                ,IMP_DIR_LIQ
        //                ,IMP_NET_LIQTO
        //                ,IMP_EFFLQ
        //                ,COD_DVS
        //                ,TP_MEZ_PAG_ACCR
        //                ,ESTR_CNT_CORR_ACCR
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,TP_STAT_TIT
        //                ,IMPB_VIS_1382011
        //                ,IMPST_VIS_1382011
        //                ,IMPST_SOST_1382011
        //                ,IMP_INTR_RIT_PAG
        //                ,IMPB_IS
        //                ,IMPB_IS_1382011
        //                ,IMPB_VIS_662014
        //                ,IMPST_VIS_662014
        //                ,IMPB_IS_662014
        //                ,IMPST_SOST_662014
        //             INTO
        //                :TCL-ID-TCONT-LIQ
        //               ,:TCL-ID-PERC-LIQ
        //               ,:TCL-ID-BNFICR-LIQ
        //               ,:TCL-ID-MOVI-CRZ
        //               ,:TCL-ID-MOVI-CHIU
        //                :IND-TCL-ID-MOVI-CHIU
        //               ,:TCL-DT-INI-EFF-DB
        //               ,:TCL-DT-END-EFF-DB
        //               ,:TCL-COD-COMP-ANIA
        //               ,:TCL-DT-VLT-DB
        //                :IND-TCL-DT-VLT
        //               ,:TCL-IMP-LRD-LIQTO
        //                :IND-TCL-IMP-LRD-LIQTO
        //               ,:TCL-IMP-PREST
        //                :IND-TCL-IMP-PREST
        //               ,:TCL-IMP-INTR-PREST
        //                :IND-TCL-IMP-INTR-PREST
        //               ,:TCL-IMP-RAT
        //                :IND-TCL-IMP-RAT
        //               ,:TCL-IMP-UTI
        //                :IND-TCL-IMP-UTI
        //               ,:TCL-IMP-RIT-TFR
        //                :IND-TCL-IMP-RIT-TFR
        //               ,:TCL-IMP-RIT-ACC
        //                :IND-TCL-IMP-RIT-ACC
        //               ,:TCL-IMP-RIT-VIS
        //                :IND-TCL-IMP-RIT-VIS
        //               ,:TCL-IMPB-TFR
        //                :IND-TCL-IMPB-TFR
        //               ,:TCL-IMPB-ACC
        //                :IND-TCL-IMPB-ACC
        //               ,:TCL-IMPB-VIS
        //                :IND-TCL-IMPB-VIS
        //               ,:TCL-IMP-RIMB
        //                :IND-TCL-IMP-RIMB
        //               ,:TCL-IMP-CORTVO
        //                :IND-TCL-IMP-CORTVO
        //               ,:TCL-IMPB-IMPST-PRVR
        //                :IND-TCL-IMPB-IMPST-PRVR
        //               ,:TCL-IMPST-PRVR
        //                :IND-TCL-IMPST-PRVR
        //               ,:TCL-IMPB-IMPST-252
        //                :IND-TCL-IMPB-IMPST-252
        //               ,:TCL-IMPST-252
        //                :IND-TCL-IMPST-252
        //               ,:TCL-IMP-IS
        //                :IND-TCL-IMP-IS
        //               ,:TCL-IMP-DIR-LIQ
        //                :IND-TCL-IMP-DIR-LIQ
        //               ,:TCL-IMP-NET-LIQTO
        //                :IND-TCL-IMP-NET-LIQTO
        //               ,:TCL-IMP-EFFLQ
        //                :IND-TCL-IMP-EFFLQ
        //               ,:TCL-COD-DVS
        //                :IND-TCL-COD-DVS
        //               ,:TCL-TP-MEZ-PAG-ACCR
        //                :IND-TCL-TP-MEZ-PAG-ACCR
        //               ,:TCL-ESTR-CNT-CORR-ACCR
        //                :IND-TCL-ESTR-CNT-CORR-ACCR
        //               ,:TCL-DS-RIGA
        //               ,:TCL-DS-OPER-SQL
        //               ,:TCL-DS-VER
        //               ,:TCL-DS-TS-INI-CPTZ
        //               ,:TCL-DS-TS-END-CPTZ
        //               ,:TCL-DS-UTENTE
        //               ,:TCL-DS-STATO-ELAB
        //               ,:TCL-TP-STAT-TIT
        //                :IND-TCL-TP-STAT-TIT
        //               ,:TCL-IMPB-VIS-1382011
        //                :IND-TCL-IMPB-VIS-1382011
        //               ,:TCL-IMPST-VIS-1382011
        //                :IND-TCL-IMPST-VIS-1382011
        //               ,:TCL-IMPST-SOST-1382011
        //                :IND-TCL-IMPST-SOST-1382011
        //               ,:TCL-IMP-INTR-RIT-PAG
        //                :IND-TCL-IMP-INTR-RIT-PAG
        //               ,:TCL-IMPB-IS
        //                :IND-TCL-IMPB-IS
        //               ,:TCL-IMPB-IS-1382011
        //                :IND-TCL-IMPB-IS-1382011
        //               ,:TCL-IMPB-VIS-662014
        //                :IND-TCL-IMPB-VIS-662014
        //               ,:TCL-IMPST-VIS-662014
        //                :IND-TCL-IMPST-VIS-662014
        //               ,:TCL-IMPB-IS-662014
        //                :IND-TCL-IMPB-IS-662014
        //               ,:TCL-IMPST-SOST-662014
        //                :IND-TCL-IMPST-SOST-662014
        //             FROM TCONT_LIQ
        //             WHERE     ID_TCONT_LIQ = :TCL-ID-TCONT-LIQ
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        tcontLiqDao.selectRec(tcontLiq.getTclIdTcontLiq(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A330-UPDATE-ID-EFF<br>*/
    private void a330UpdateIdEff() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE TCONT_LIQ SET
        //                   ID_TCONT_LIQ           =
        //                :TCL-ID-TCONT-LIQ
        //                  ,ID_PERC_LIQ            =
        //                :TCL-ID-PERC-LIQ
        //                  ,ID_BNFICR_LIQ          =
        //                :TCL-ID-BNFICR-LIQ
        //                  ,ID_MOVI_CRZ            =
        //                :TCL-ID-MOVI-CRZ
        //                  ,ID_MOVI_CHIU           =
        //                :TCL-ID-MOVI-CHIU
        //                                       :IND-TCL-ID-MOVI-CHIU
        //                  ,DT_INI_EFF             =
        //           :TCL-DT-INI-EFF-DB
        //                  ,DT_END_EFF             =
        //           :TCL-DT-END-EFF-DB
        //                  ,COD_COMP_ANIA          =
        //                :TCL-COD-COMP-ANIA
        //                  ,DT_VLT                 =
        //           :TCL-DT-VLT-DB
        //                                       :IND-TCL-DT-VLT
        //                  ,IMP_LRD_LIQTO          =
        //                :TCL-IMP-LRD-LIQTO
        //                                       :IND-TCL-IMP-LRD-LIQTO
        //                  ,IMP_PREST              =
        //                :TCL-IMP-PREST
        //                                       :IND-TCL-IMP-PREST
        //                  ,IMP_INTR_PREST         =
        //                :TCL-IMP-INTR-PREST
        //                                       :IND-TCL-IMP-INTR-PREST
        //                  ,IMP_RAT                =
        //                :TCL-IMP-RAT
        //                                       :IND-TCL-IMP-RAT
        //                  ,IMP_UTI                =
        //                :TCL-IMP-UTI
        //                                       :IND-TCL-IMP-UTI
        //                  ,IMP_RIT_TFR            =
        //                :TCL-IMP-RIT-TFR
        //                                       :IND-TCL-IMP-RIT-TFR
        //                  ,IMP_RIT_ACC            =
        //                :TCL-IMP-RIT-ACC
        //                                       :IND-TCL-IMP-RIT-ACC
        //                  ,IMP_RIT_VIS            =
        //                :TCL-IMP-RIT-VIS
        //                                       :IND-TCL-IMP-RIT-VIS
        //                  ,IMPB_TFR               =
        //                :TCL-IMPB-TFR
        //                                       :IND-TCL-IMPB-TFR
        //                  ,IMPB_ACC               =
        //                :TCL-IMPB-ACC
        //                                       :IND-TCL-IMPB-ACC
        //                  ,IMPB_VIS               =
        //                :TCL-IMPB-VIS
        //                                       :IND-TCL-IMPB-VIS
        //                  ,IMP_RIMB               =
        //                :TCL-IMP-RIMB
        //                                       :IND-TCL-IMP-RIMB
        //                  ,IMP_CORTVO             =
        //                :TCL-IMP-CORTVO
        //                                       :IND-TCL-IMP-CORTVO
        //                  ,IMPB_IMPST_PRVR        =
        //                :TCL-IMPB-IMPST-PRVR
        //                                       :IND-TCL-IMPB-IMPST-PRVR
        //                  ,IMPST_PRVR             =
        //                :TCL-IMPST-PRVR
        //                                       :IND-TCL-IMPST-PRVR
        //                  ,IMPB_IMPST_252         =
        //                :TCL-IMPB-IMPST-252
        //                                       :IND-TCL-IMPB-IMPST-252
        //                  ,IMPST_252              =
        //                :TCL-IMPST-252
        //                                       :IND-TCL-IMPST-252
        //                  ,IMP_IS                 =
        //                :TCL-IMP-IS
        //                                       :IND-TCL-IMP-IS
        //                  ,IMP_DIR_LIQ            =
        //                :TCL-IMP-DIR-LIQ
        //                                       :IND-TCL-IMP-DIR-LIQ
        //                  ,IMP_NET_LIQTO          =
        //                :TCL-IMP-NET-LIQTO
        //                                       :IND-TCL-IMP-NET-LIQTO
        //                  ,IMP_EFFLQ              =
        //                :TCL-IMP-EFFLQ
        //                                       :IND-TCL-IMP-EFFLQ
        //                  ,COD_DVS                =
        //                :TCL-COD-DVS
        //                                       :IND-TCL-COD-DVS
        //                  ,TP_MEZ_PAG_ACCR        =
        //                :TCL-TP-MEZ-PAG-ACCR
        //                                       :IND-TCL-TP-MEZ-PAG-ACCR
        //                  ,ESTR_CNT_CORR_ACCR     =
        //                :TCL-ESTR-CNT-CORR-ACCR
        //                                       :IND-TCL-ESTR-CNT-CORR-ACCR
        //                  ,DS_RIGA                =
        //                :TCL-DS-RIGA
        //                  ,DS_OPER_SQL            =
        //                :TCL-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :TCL-DS-VER
        //                  ,DS_TS_INI_CPTZ         =
        //                :TCL-DS-TS-INI-CPTZ
        //                  ,DS_TS_END_CPTZ         =
        //                :TCL-DS-TS-END-CPTZ
        //                  ,DS_UTENTE              =
        //                :TCL-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :TCL-DS-STATO-ELAB
        //                  ,TP_STAT_TIT            =
        //                :TCL-TP-STAT-TIT
        //                                       :IND-TCL-TP-STAT-TIT
        //                  ,IMPB_VIS_1382011       =
        //                :TCL-IMPB-VIS-1382011
        //                                       :IND-TCL-IMPB-VIS-1382011
        //                  ,IMPST_VIS_1382011      =
        //                :TCL-IMPST-VIS-1382011
        //                                       :IND-TCL-IMPST-VIS-1382011
        //                  ,IMPST_SOST_1382011     =
        //                :TCL-IMPST-SOST-1382011
        //                                       :IND-TCL-IMPST-SOST-1382011
        //                  ,IMP_INTR_RIT_PAG       =
        //                :TCL-IMP-INTR-RIT-PAG
        //                                       :IND-TCL-IMP-INTR-RIT-PAG
        //                  ,IMPB_IS                =
        //                :TCL-IMPB-IS
        //                                       :IND-TCL-IMPB-IS
        //                  ,IMPB_IS_1382011        =
        //                :TCL-IMPB-IS-1382011
        //                                       :IND-TCL-IMPB-IS-1382011
        //                  ,IMPB_VIS_662014        =
        //                :TCL-IMPB-VIS-662014
        //                                       :IND-TCL-IMPB-VIS-662014
        //                  ,IMPST_VIS_662014       =
        //                :TCL-IMPST-VIS-662014
        //                                       :IND-TCL-IMPST-VIS-662014
        //                  ,IMPB_IS_662014         =
        //                :TCL-IMPB-IS-662014
        //                                       :IND-TCL-IMPB-IS-662014
        //                  ,IMPST_SOST_662014      =
        //                :TCL-IMPST-SOST-662014
        //                                       :IND-TCL-IMPST-SOST-662014
        //                WHERE     DS_RIGA = :TCL-DS-RIGA
        //                   AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        tcontLiqDao.updateRec1(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A360-OPEN-CURSOR-ID-EFF<br>*/
    private void a360OpenCursorIdEff() {
        // COB_CODE: PERFORM A305-DECLARE-CURSOR-ID-EFF THRU A305-EX.
        a305DeclareCursorIdEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-ID-UPD-EFF-TCL
        //           END-EXEC.
        tcontLiqDao.openCIdUpdEffTcl(tcontLiq.getTclIdTcontLiq(), ws.getIdsv0010().getWsTsInfinito(), ws.getIdsv0010().getWsDataInizioEffettoDb(), idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A370-CLOSE-CURSOR-ID-EFF<br>*/
    private void a370CloseCursorIdEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-ID-UPD-EFF-TCL
        //           END-EXEC.
        tcontLiqDao.closeCIdUpdEffTcl();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A390-FETCH-NEXT-ID-EFF<br>*/
    private void a390FetchNextIdEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-ID-UPD-EFF-TCL
        //           INTO
        //                :TCL-ID-TCONT-LIQ
        //               ,:TCL-ID-PERC-LIQ
        //               ,:TCL-ID-BNFICR-LIQ
        //               ,:TCL-ID-MOVI-CRZ
        //               ,:TCL-ID-MOVI-CHIU
        //                :IND-TCL-ID-MOVI-CHIU
        //               ,:TCL-DT-INI-EFF-DB
        //               ,:TCL-DT-END-EFF-DB
        //               ,:TCL-COD-COMP-ANIA
        //               ,:TCL-DT-VLT-DB
        //                :IND-TCL-DT-VLT
        //               ,:TCL-IMP-LRD-LIQTO
        //                :IND-TCL-IMP-LRD-LIQTO
        //               ,:TCL-IMP-PREST
        //                :IND-TCL-IMP-PREST
        //               ,:TCL-IMP-INTR-PREST
        //                :IND-TCL-IMP-INTR-PREST
        //               ,:TCL-IMP-RAT
        //                :IND-TCL-IMP-RAT
        //               ,:TCL-IMP-UTI
        //                :IND-TCL-IMP-UTI
        //               ,:TCL-IMP-RIT-TFR
        //                :IND-TCL-IMP-RIT-TFR
        //               ,:TCL-IMP-RIT-ACC
        //                :IND-TCL-IMP-RIT-ACC
        //               ,:TCL-IMP-RIT-VIS
        //                :IND-TCL-IMP-RIT-VIS
        //               ,:TCL-IMPB-TFR
        //                :IND-TCL-IMPB-TFR
        //               ,:TCL-IMPB-ACC
        //                :IND-TCL-IMPB-ACC
        //               ,:TCL-IMPB-VIS
        //                :IND-TCL-IMPB-VIS
        //               ,:TCL-IMP-RIMB
        //                :IND-TCL-IMP-RIMB
        //               ,:TCL-IMP-CORTVO
        //                :IND-TCL-IMP-CORTVO
        //               ,:TCL-IMPB-IMPST-PRVR
        //                :IND-TCL-IMPB-IMPST-PRVR
        //               ,:TCL-IMPST-PRVR
        //                :IND-TCL-IMPST-PRVR
        //               ,:TCL-IMPB-IMPST-252
        //                :IND-TCL-IMPB-IMPST-252
        //               ,:TCL-IMPST-252
        //                :IND-TCL-IMPST-252
        //               ,:TCL-IMP-IS
        //                :IND-TCL-IMP-IS
        //               ,:TCL-IMP-DIR-LIQ
        //                :IND-TCL-IMP-DIR-LIQ
        //               ,:TCL-IMP-NET-LIQTO
        //                :IND-TCL-IMP-NET-LIQTO
        //               ,:TCL-IMP-EFFLQ
        //                :IND-TCL-IMP-EFFLQ
        //               ,:TCL-COD-DVS
        //                :IND-TCL-COD-DVS
        //               ,:TCL-TP-MEZ-PAG-ACCR
        //                :IND-TCL-TP-MEZ-PAG-ACCR
        //               ,:TCL-ESTR-CNT-CORR-ACCR
        //                :IND-TCL-ESTR-CNT-CORR-ACCR
        //               ,:TCL-DS-RIGA
        //               ,:TCL-DS-OPER-SQL
        //               ,:TCL-DS-VER
        //               ,:TCL-DS-TS-INI-CPTZ
        //               ,:TCL-DS-TS-END-CPTZ
        //               ,:TCL-DS-UTENTE
        //               ,:TCL-DS-STATO-ELAB
        //               ,:TCL-TP-STAT-TIT
        //                :IND-TCL-TP-STAT-TIT
        //               ,:TCL-IMPB-VIS-1382011
        //                :IND-TCL-IMPB-VIS-1382011
        //               ,:TCL-IMPST-VIS-1382011
        //                :IND-TCL-IMPST-VIS-1382011
        //               ,:TCL-IMPST-SOST-1382011
        //                :IND-TCL-IMPST-SOST-1382011
        //               ,:TCL-IMP-INTR-RIT-PAG
        //                :IND-TCL-IMP-INTR-RIT-PAG
        //               ,:TCL-IMPB-IS
        //                :IND-TCL-IMPB-IS
        //               ,:TCL-IMPB-IS-1382011
        //                :IND-TCL-IMPB-IS-1382011
        //               ,:TCL-IMPB-VIS-662014
        //                :IND-TCL-IMPB-VIS-662014
        //               ,:TCL-IMPST-VIS-662014
        //                :IND-TCL-IMPST-VIS-662014
        //               ,:TCL-IMPB-IS-662014
        //                :IND-TCL-IMPB-IS-662014
        //               ,:TCL-IMPST-SOST-662014
        //                :IND-TCL-IMPST-SOST-662014
        //           END-EXEC.
        tcontLiqDao.fetchCIdUpdEffTcl(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A370-CLOSE-CURSOR-ID-EFF THRU A370-EX
            a370CloseCursorIdEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A405-DECLARE-CURSOR-IDP-EFF<br>
	 * <pre>----
	 * ----  gestione IDP Effetto
	 * ----</pre>*/
    private void a405DeclareCursorIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A410-SELECT-IDP-EFF<br>*/
    private void a410SelectIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A460-OPEN-CURSOR-IDP-EFF<br>*/
    private void a460OpenCursorIdpEff() {
        // COB_CODE: PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.
        a405DeclareCursorIdpEff();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A470-CLOSE-CURSOR-IDP-EFF<br>*/
    private void a470CloseCursorIdpEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A480-FETCH-FIRST-IDP-EFF<br>*/
    private void a480FetchFirstIdpEff() {
        // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.
        a460OpenCursorIdpEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
            a490FetchNextIdpEff();
        }
    }

    /**Original name: A490-FETCH-NEXT-IDP-EFF<br>*/
    private void a490FetchNextIdpEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A505-DECLARE-CURSOR-IBO<br>
	 * <pre>----
	 * ----  gestione IBO Effetto e non
	 * ----</pre>*/
    private void a505DeclareCursorIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A510-SELECT-IBO<br>*/
    private void a510SelectIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A560-OPEN-CURSOR-IBO<br>*/
    private void a560OpenCursorIbo() {
        // COB_CODE: PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.
        a505DeclareCursorIbo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A570-CLOSE-CURSOR-IBO<br>*/
    private void a570CloseCursorIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A580-FETCH-FIRST-IBO<br>*/
    private void a580FetchFirstIbo() {
        // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.
        a560OpenCursorIbo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
            a590FetchNextIbo();
        }
    }

    /**Original name: A590-FETCH-NEXT-IBO<br>*/
    private void a590FetchNextIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A605-DECLARE-CURSOR-IBS<br>
	 * <pre>----
	 * ----  gestione IBS Effetto e non
	 * ----</pre>*/
    private void a605DeclareCursorIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A610-SELECT-IBS<br>*/
    private void a610SelectIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A660-OPEN-CURSOR-IBS<br>*/
    private void a660OpenCursorIbs() {
        // COB_CODE: PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.
        a605DeclareCursorIbs();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A670-CLOSE-CURSOR-IBS<br>*/
    private void a670CloseCursorIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A680-FETCH-FIRST-IBS<br>*/
    private void a680FetchFirstIbs() {
        // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.
        a660OpenCursorIbs();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
            a690FetchNextIbs();
        }
    }

    /**Original name: A690-FETCH-NEXT-IBS<br>*/
    private void a690FetchNextIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A705-DECLARE-CURSOR-IDO<br>
	 * <pre>----
	 * ----  gestione IDO Effetto e non
	 * ----</pre>*/
    private void a705DeclareCursorIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A710-SELECT-IDO<br>*/
    private void a710SelectIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A760-OPEN-CURSOR-IDO<br>*/
    private void a760OpenCursorIdo() {
        // COB_CODE: PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.
        a705DeclareCursorIdo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A770-CLOSE-CURSOR-IDO<br>*/
    private void a770CloseCursorIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A780-FETCH-FIRST-IDO<br>*/
    private void a780FetchFirstIdo() {
        // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.
        a760OpenCursorIdo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
            a790FetchNextIdo();
        }
    }

    /**Original name: A790-FETCH-NEXT-IDO<br>*/
    private void a790FetchNextIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B310-SELECT-ID-CPZ<br>
	 * <pre>----
	 * ----  gestione ID Competenza
	 * ----</pre>*/
    private void b310SelectIdCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_TCONT_LIQ
        //                ,ID_PERC_LIQ
        //                ,ID_BNFICR_LIQ
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,DT_VLT
        //                ,IMP_LRD_LIQTO
        //                ,IMP_PREST
        //                ,IMP_INTR_PREST
        //                ,IMP_RAT
        //                ,IMP_UTI
        //                ,IMP_RIT_TFR
        //                ,IMP_RIT_ACC
        //                ,IMP_RIT_VIS
        //                ,IMPB_TFR
        //                ,IMPB_ACC
        //                ,IMPB_VIS
        //                ,IMP_RIMB
        //                ,IMP_CORTVO
        //                ,IMPB_IMPST_PRVR
        //                ,IMPST_PRVR
        //                ,IMPB_IMPST_252
        //                ,IMPST_252
        //                ,IMP_IS
        //                ,IMP_DIR_LIQ
        //                ,IMP_NET_LIQTO
        //                ,IMP_EFFLQ
        //                ,COD_DVS
        //                ,TP_MEZ_PAG_ACCR
        //                ,ESTR_CNT_CORR_ACCR
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,TP_STAT_TIT
        //                ,IMPB_VIS_1382011
        //                ,IMPST_VIS_1382011
        //                ,IMPST_SOST_1382011
        //                ,IMP_INTR_RIT_PAG
        //                ,IMPB_IS
        //                ,IMPB_IS_1382011
        //                ,IMPB_VIS_662014
        //                ,IMPST_VIS_662014
        //                ,IMPB_IS_662014
        //                ,IMPST_SOST_662014
        //             INTO
        //                :TCL-ID-TCONT-LIQ
        //               ,:TCL-ID-PERC-LIQ
        //               ,:TCL-ID-BNFICR-LIQ
        //               ,:TCL-ID-MOVI-CRZ
        //               ,:TCL-ID-MOVI-CHIU
        //                :IND-TCL-ID-MOVI-CHIU
        //               ,:TCL-DT-INI-EFF-DB
        //               ,:TCL-DT-END-EFF-DB
        //               ,:TCL-COD-COMP-ANIA
        //               ,:TCL-DT-VLT-DB
        //                :IND-TCL-DT-VLT
        //               ,:TCL-IMP-LRD-LIQTO
        //                :IND-TCL-IMP-LRD-LIQTO
        //               ,:TCL-IMP-PREST
        //                :IND-TCL-IMP-PREST
        //               ,:TCL-IMP-INTR-PREST
        //                :IND-TCL-IMP-INTR-PREST
        //               ,:TCL-IMP-RAT
        //                :IND-TCL-IMP-RAT
        //               ,:TCL-IMP-UTI
        //                :IND-TCL-IMP-UTI
        //               ,:TCL-IMP-RIT-TFR
        //                :IND-TCL-IMP-RIT-TFR
        //               ,:TCL-IMP-RIT-ACC
        //                :IND-TCL-IMP-RIT-ACC
        //               ,:TCL-IMP-RIT-VIS
        //                :IND-TCL-IMP-RIT-VIS
        //               ,:TCL-IMPB-TFR
        //                :IND-TCL-IMPB-TFR
        //               ,:TCL-IMPB-ACC
        //                :IND-TCL-IMPB-ACC
        //               ,:TCL-IMPB-VIS
        //                :IND-TCL-IMPB-VIS
        //               ,:TCL-IMP-RIMB
        //                :IND-TCL-IMP-RIMB
        //               ,:TCL-IMP-CORTVO
        //                :IND-TCL-IMP-CORTVO
        //               ,:TCL-IMPB-IMPST-PRVR
        //                :IND-TCL-IMPB-IMPST-PRVR
        //               ,:TCL-IMPST-PRVR
        //                :IND-TCL-IMPST-PRVR
        //               ,:TCL-IMPB-IMPST-252
        //                :IND-TCL-IMPB-IMPST-252
        //               ,:TCL-IMPST-252
        //                :IND-TCL-IMPST-252
        //               ,:TCL-IMP-IS
        //                :IND-TCL-IMP-IS
        //               ,:TCL-IMP-DIR-LIQ
        //                :IND-TCL-IMP-DIR-LIQ
        //               ,:TCL-IMP-NET-LIQTO
        //                :IND-TCL-IMP-NET-LIQTO
        //               ,:TCL-IMP-EFFLQ
        //                :IND-TCL-IMP-EFFLQ
        //               ,:TCL-COD-DVS
        //                :IND-TCL-COD-DVS
        //               ,:TCL-TP-MEZ-PAG-ACCR
        //                :IND-TCL-TP-MEZ-PAG-ACCR
        //               ,:TCL-ESTR-CNT-CORR-ACCR
        //                :IND-TCL-ESTR-CNT-CORR-ACCR
        //               ,:TCL-DS-RIGA
        //               ,:TCL-DS-OPER-SQL
        //               ,:TCL-DS-VER
        //               ,:TCL-DS-TS-INI-CPTZ
        //               ,:TCL-DS-TS-END-CPTZ
        //               ,:TCL-DS-UTENTE
        //               ,:TCL-DS-STATO-ELAB
        //               ,:TCL-TP-STAT-TIT
        //                :IND-TCL-TP-STAT-TIT
        //               ,:TCL-IMPB-VIS-1382011
        //                :IND-TCL-IMPB-VIS-1382011
        //               ,:TCL-IMPST-VIS-1382011
        //                :IND-TCL-IMPST-VIS-1382011
        //               ,:TCL-IMPST-SOST-1382011
        //                :IND-TCL-IMPST-SOST-1382011
        //               ,:TCL-IMP-INTR-RIT-PAG
        //                :IND-TCL-IMP-INTR-RIT-PAG
        //               ,:TCL-IMPB-IS
        //                :IND-TCL-IMPB-IS
        //               ,:TCL-IMPB-IS-1382011
        //                :IND-TCL-IMPB-IS-1382011
        //               ,:TCL-IMPB-VIS-662014
        //                :IND-TCL-IMPB-VIS-662014
        //               ,:TCL-IMPST-VIS-662014
        //                :IND-TCL-IMPST-VIS-662014
        //               ,:TCL-IMPB-IS-662014
        //                :IND-TCL-IMPB-IS-662014
        //               ,:TCL-IMPST-SOST-662014
        //                :IND-TCL-IMPST-SOST-662014
        //             FROM TCONT_LIQ
        //             WHERE     ID_TCONT_LIQ = :TCL-ID-TCONT-LIQ
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        tcontLiqDao.selectRec1(tcontLiq.getTclIdTcontLiq(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B405-DECLARE-CURSOR-IDP-CPZ<br>
	 * <pre>----
	 * ----  gestione IDP Competenza
	 * ----</pre>*/
    private void b405DeclareCursorIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B410-SELECT-IDP-CPZ<br>*/
    private void b410SelectIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B460-OPEN-CURSOR-IDP-CPZ<br>*/
    private void b460OpenCursorIdpCpz() {
        // COB_CODE: PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.
        b405DeclareCursorIdpCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B470-CLOSE-CURSOR-IDP-CPZ<br>*/
    private void b470CloseCursorIdpCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B480-FETCH-FIRST-IDP-CPZ<br>*/
    private void b480FetchFirstIdpCpz() {
        // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.
        b460OpenCursorIdpCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
            b490FetchNextIdpCpz();
        }
    }

    /**Original name: B490-FETCH-NEXT-IDP-CPZ<br>*/
    private void b490FetchNextIdpCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B505-DECLARE-CURSOR-IBO-CPZ<br>
	 * <pre>----
	 * ----  gestione IBO Competenza
	 * ----</pre>*/
    private void b505DeclareCursorIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B510-SELECT-IBO-CPZ<br>*/
    private void b510SelectIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B560-OPEN-CURSOR-IBO-CPZ<br>*/
    private void b560OpenCursorIboCpz() {
        // COB_CODE: PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.
        b505DeclareCursorIboCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B570-CLOSE-CURSOR-IBO-CPZ<br>*/
    private void b570CloseCursorIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B580-FETCH-FIRST-IBO-CPZ<br>*/
    private void b580FetchFirstIboCpz() {
        // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.
        b560OpenCursorIboCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
    }

    /**Original name: B590-FETCH-NEXT-IBO-CPZ<br>*/
    private void b590FetchNextIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B605-DECLARE-CURSOR-IBS-CPZ<br>
	 * <pre>----
	 * ----  gestione IBS Competenza
	 * ----</pre>*/
    private void b605DeclareCursorIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B610-SELECT-IBS-CPZ<br>*/
    private void b610SelectIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B660-OPEN-CURSOR-IBS-CPZ<br>*/
    private void b660OpenCursorIbsCpz() {
        // COB_CODE: PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.
        b605DeclareCursorIbsCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B670-CLOSE-CURSOR-IBS-CPZ<br>*/
    private void b670CloseCursorIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B680-FETCH-FIRST-IBS-CPZ<br>*/
    private void b680FetchFirstIbsCpz() {
        // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.
        b660OpenCursorIbsCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
    }

    /**Original name: B690-FETCH-NEXT-IBS-CPZ<br>*/
    private void b690FetchNextIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B705-DECLARE-CURSOR-IDO-CPZ<br>
	 * <pre>----
	 * ----  gestione IDO Competenza
	 * ----</pre>*/
    private void b705DeclareCursorIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B710-SELECT-IDO-CPZ<br>*/
    private void b710SelectIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B760-OPEN-CURSOR-IDO-CPZ<br>*/
    private void b760OpenCursorIdoCpz() {
        // COB_CODE: PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.
        b705DeclareCursorIdoCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B770-CLOSE-CURSOR-IDO-CPZ<br>*/
    private void b770CloseCursorIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B780-FETCH-FIRST-IDO-CPZ<br>*/
    private void b780FetchFirstIdoCpz() {
        // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.
        b760OpenCursorIdoCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
    }

    /**Original name: B790-FETCH-NEXT-IDO-CPZ<br>*/
    private void b790FetchNextIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-TCL-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO TCL-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndTcontLiq().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TCL-ID-MOVI-CHIU-NULL
            tcontLiq.getTclIdMoviChiu().setTclIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TclIdMoviChiu.Len.TCL_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-TCL-DT-VLT = -1
        //              MOVE HIGH-VALUES TO TCL-DT-VLT-NULL
        //           END-IF
        if (ws.getIndTcontLiq().getDtVlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TCL-DT-VLT-NULL
            tcontLiq.getTclDtVlt().setTclDtVltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TclDtVlt.Len.TCL_DT_VLT_NULL));
        }
        // COB_CODE: IF IND-TCL-IMP-LRD-LIQTO = -1
        //              MOVE HIGH-VALUES TO TCL-IMP-LRD-LIQTO-NULL
        //           END-IF
        if (ws.getIndTcontLiq().getImpLrdLiqto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TCL-IMP-LRD-LIQTO-NULL
            tcontLiq.getTclImpLrdLiqto().setTclImpLrdLiqtoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TclImpLrdLiqto.Len.TCL_IMP_LRD_LIQTO_NULL));
        }
        // COB_CODE: IF IND-TCL-IMP-PREST = -1
        //              MOVE HIGH-VALUES TO TCL-IMP-PREST-NULL
        //           END-IF
        if (ws.getIndTcontLiq().getImpPrest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TCL-IMP-PREST-NULL
            tcontLiq.getTclImpPrest().setTclImpPrestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TclImpPrest.Len.TCL_IMP_PREST_NULL));
        }
        // COB_CODE: IF IND-TCL-IMP-INTR-PREST = -1
        //              MOVE HIGH-VALUES TO TCL-IMP-INTR-PREST-NULL
        //           END-IF
        if (ws.getIndTcontLiq().getImpIntrPrest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TCL-IMP-INTR-PREST-NULL
            tcontLiq.getTclImpIntrPrest().setTclImpIntrPrestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TclImpIntrPrest.Len.TCL_IMP_INTR_PREST_NULL));
        }
        // COB_CODE: IF IND-TCL-IMP-RAT = -1
        //              MOVE HIGH-VALUES TO TCL-IMP-RAT-NULL
        //           END-IF
        if (ws.getIndTcontLiq().getImpRat() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TCL-IMP-RAT-NULL
            tcontLiq.getTclImpRat().setTclImpRatNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TclImpRat.Len.TCL_IMP_RAT_NULL));
        }
        // COB_CODE: IF IND-TCL-IMP-UTI = -1
        //              MOVE HIGH-VALUES TO TCL-IMP-UTI-NULL
        //           END-IF
        if (ws.getIndTcontLiq().getImpUti() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TCL-IMP-UTI-NULL
            tcontLiq.getTclImpUti().setTclImpUtiNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TclImpUti.Len.TCL_IMP_UTI_NULL));
        }
        // COB_CODE: IF IND-TCL-IMP-RIT-TFR = -1
        //              MOVE HIGH-VALUES TO TCL-IMP-RIT-TFR-NULL
        //           END-IF
        if (ws.getIndTcontLiq().getImpRitTfr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TCL-IMP-RIT-TFR-NULL
            tcontLiq.getTclImpRitTfr().setTclImpRitTfrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TclImpRitTfr.Len.TCL_IMP_RIT_TFR_NULL));
        }
        // COB_CODE: IF IND-TCL-IMP-RIT-ACC = -1
        //              MOVE HIGH-VALUES TO TCL-IMP-RIT-ACC-NULL
        //           END-IF
        if (ws.getIndTcontLiq().getImpRitAcc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TCL-IMP-RIT-ACC-NULL
            tcontLiq.getTclImpRitAcc().setTclImpRitAccNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TclImpRitAcc.Len.TCL_IMP_RIT_ACC_NULL));
        }
        // COB_CODE: IF IND-TCL-IMP-RIT-VIS = -1
        //              MOVE HIGH-VALUES TO TCL-IMP-RIT-VIS-NULL
        //           END-IF
        if (ws.getIndTcontLiq().getImpRitVis() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TCL-IMP-RIT-VIS-NULL
            tcontLiq.getTclImpRitVis().setTclImpRitVisNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TclImpRitVis.Len.TCL_IMP_RIT_VIS_NULL));
        }
        // COB_CODE: IF IND-TCL-IMPB-TFR = -1
        //              MOVE HIGH-VALUES TO TCL-IMPB-TFR-NULL
        //           END-IF
        if (ws.getIndTcontLiq().getImpbTfr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TCL-IMPB-TFR-NULL
            tcontLiq.getTclImpbTfr().setTclImpbTfrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TclImpbTfr.Len.TCL_IMPB_TFR_NULL));
        }
        // COB_CODE: IF IND-TCL-IMPB-ACC = -1
        //              MOVE HIGH-VALUES TO TCL-IMPB-ACC-NULL
        //           END-IF
        if (ws.getIndTcontLiq().getImpbAcc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TCL-IMPB-ACC-NULL
            tcontLiq.getTclImpbAcc().setTclImpbAccNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TclImpbAcc.Len.TCL_IMPB_ACC_NULL));
        }
        // COB_CODE: IF IND-TCL-IMPB-VIS = -1
        //              MOVE HIGH-VALUES TO TCL-IMPB-VIS-NULL
        //           END-IF
        if (ws.getIndTcontLiq().getImpbVis() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TCL-IMPB-VIS-NULL
            tcontLiq.getTclImpbVis().setTclImpbVisNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TclImpbVis.Len.TCL_IMPB_VIS_NULL));
        }
        // COB_CODE: IF IND-TCL-IMP-RIMB = -1
        //              MOVE HIGH-VALUES TO TCL-IMP-RIMB-NULL
        //           END-IF
        if (ws.getIndTcontLiq().getImpRimb() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TCL-IMP-RIMB-NULL
            tcontLiq.getTclImpRimb().setTclImpRimbNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TclImpRimb.Len.TCL_IMP_RIMB_NULL));
        }
        // COB_CODE: IF IND-TCL-IMP-CORTVO = -1
        //              MOVE HIGH-VALUES TO TCL-IMP-CORTVO-NULL
        //           END-IF
        if (ws.getIndTcontLiq().getImpCortvo() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TCL-IMP-CORTVO-NULL
            tcontLiq.getTclImpCortvo().setTclImpCortvoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TclImpCortvo.Len.TCL_IMP_CORTVO_NULL));
        }
        // COB_CODE: IF IND-TCL-IMPB-IMPST-PRVR = -1
        //              MOVE HIGH-VALUES TO TCL-IMPB-IMPST-PRVR-NULL
        //           END-IF
        if (ws.getIndTcontLiq().getImpbImpstPrvr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TCL-IMPB-IMPST-PRVR-NULL
            tcontLiq.getTclImpbImpstPrvr().setTclImpbImpstPrvrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TclImpbImpstPrvr.Len.TCL_IMPB_IMPST_PRVR_NULL));
        }
        // COB_CODE: IF IND-TCL-IMPST-PRVR = -1
        //              MOVE HIGH-VALUES TO TCL-IMPST-PRVR-NULL
        //           END-IF
        if (ws.getIndTcontLiq().getImpstPrvr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TCL-IMPST-PRVR-NULL
            tcontLiq.getTclImpstPrvr().setTclImpstPrvrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TclImpstPrvr.Len.TCL_IMPST_PRVR_NULL));
        }
        // COB_CODE: IF IND-TCL-IMPB-IMPST-252 = -1
        //              MOVE HIGH-VALUES TO TCL-IMPB-IMPST-252-NULL
        //           END-IF
        if (ws.getIndTcontLiq().getImpbImpst252() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TCL-IMPB-IMPST-252-NULL
            tcontLiq.getTclImpbImpst252().setTclImpbImpst252Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TclImpbImpst252.Len.TCL_IMPB_IMPST252_NULL));
        }
        // COB_CODE: IF IND-TCL-IMPST-252 = -1
        //              MOVE HIGH-VALUES TO TCL-IMPST-252-NULL
        //           END-IF
        if (ws.getIndTcontLiq().getImpst252() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TCL-IMPST-252-NULL
            tcontLiq.getTclImpst252().setTclImpst252Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TclImpst252.Len.TCL_IMPST252_NULL));
        }
        // COB_CODE: IF IND-TCL-IMP-IS = -1
        //              MOVE HIGH-VALUES TO TCL-IMP-IS-NULL
        //           END-IF
        if (ws.getIndTcontLiq().getImpIs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TCL-IMP-IS-NULL
            tcontLiq.getTclImpIs().setTclImpIsNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TclImpIs.Len.TCL_IMP_IS_NULL));
        }
        // COB_CODE: IF IND-TCL-IMP-DIR-LIQ = -1
        //              MOVE HIGH-VALUES TO TCL-IMP-DIR-LIQ-NULL
        //           END-IF
        if (ws.getIndTcontLiq().getImpDirLiq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TCL-IMP-DIR-LIQ-NULL
            tcontLiq.getTclImpDirLiq().setTclImpDirLiqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TclImpDirLiq.Len.TCL_IMP_DIR_LIQ_NULL));
        }
        // COB_CODE: IF IND-TCL-IMP-NET-LIQTO = -1
        //              MOVE HIGH-VALUES TO TCL-IMP-NET-LIQTO-NULL
        //           END-IF
        if (ws.getIndTcontLiq().getImpNetLiqto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TCL-IMP-NET-LIQTO-NULL
            tcontLiq.getTclImpNetLiqto().setTclImpNetLiqtoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TclImpNetLiqto.Len.TCL_IMP_NET_LIQTO_NULL));
        }
        // COB_CODE: IF IND-TCL-IMP-EFFLQ = -1
        //              MOVE HIGH-VALUES TO TCL-IMP-EFFLQ-NULL
        //           END-IF
        if (ws.getIndTcontLiq().getImpEfflq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TCL-IMP-EFFLQ-NULL
            tcontLiq.getTclImpEfflq().setTclImpEfflqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TclImpEfflq.Len.TCL_IMP_EFFLQ_NULL));
        }
        // COB_CODE: IF IND-TCL-COD-DVS = -1
        //              MOVE HIGH-VALUES TO TCL-COD-DVS-NULL
        //           END-IF
        if (ws.getIndTcontLiq().getCodDvs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TCL-COD-DVS-NULL
            tcontLiq.setTclCodDvs(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TcontLiq.Len.TCL_COD_DVS));
        }
        // COB_CODE: IF IND-TCL-TP-MEZ-PAG-ACCR = -1
        //              MOVE HIGH-VALUES TO TCL-TP-MEZ-PAG-ACCR-NULL
        //           END-IF
        if (ws.getIndTcontLiq().getTpMezPagAccr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TCL-TP-MEZ-PAG-ACCR-NULL
            tcontLiq.setTclTpMezPagAccr(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TcontLiq.Len.TCL_TP_MEZ_PAG_ACCR));
        }
        // COB_CODE: IF IND-TCL-ESTR-CNT-CORR-ACCR = -1
        //              MOVE HIGH-VALUES TO TCL-ESTR-CNT-CORR-ACCR-NULL
        //           END-IF
        if (ws.getIndTcontLiq().getEstrCntCorrAccr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TCL-ESTR-CNT-CORR-ACCR-NULL
            tcontLiq.setTclEstrCntCorrAccr(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TcontLiq.Len.TCL_ESTR_CNT_CORR_ACCR));
        }
        // COB_CODE: IF IND-TCL-TP-STAT-TIT = -1
        //              MOVE HIGH-VALUES TO TCL-TP-STAT-TIT-NULL
        //           END-IF
        if (ws.getIndTcontLiq().getTpStatTit() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TCL-TP-STAT-TIT-NULL
            tcontLiq.setTclTpStatTit(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TcontLiq.Len.TCL_TP_STAT_TIT));
        }
        // COB_CODE: IF IND-TCL-IMPB-VIS-1382011 = -1
        //              MOVE HIGH-VALUES TO TCL-IMPB-VIS-1382011-NULL
        //           END-IF
        if (ws.getIndTcontLiq().getImpbVis1382011() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TCL-IMPB-VIS-1382011-NULL
            tcontLiq.getTclImpbVis1382011().setTclImpbVis1382011Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TclImpbVis1382011.Len.TCL_IMPB_VIS1382011_NULL));
        }
        // COB_CODE: IF IND-TCL-IMPST-VIS-1382011 = -1
        //              MOVE HIGH-VALUES TO TCL-IMPST-VIS-1382011-NULL
        //           END-IF
        if (ws.getIndTcontLiq().getImpstVis1382011() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TCL-IMPST-VIS-1382011-NULL
            tcontLiq.getTclImpstVis1382011().setTclImpstVis1382011Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TclImpstVis1382011.Len.TCL_IMPST_VIS1382011_NULL));
        }
        // COB_CODE: IF IND-TCL-IMPST-SOST-1382011 = -1
        //              MOVE HIGH-VALUES TO TCL-IMPST-SOST-1382011-NULL
        //           END-IF
        if (ws.getIndTcontLiq().getImpstSost1382011() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TCL-IMPST-SOST-1382011-NULL
            tcontLiq.getTclImpstSost1382011().setTclImpstSost1382011Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TclImpstSost1382011.Len.TCL_IMPST_SOST1382011_NULL));
        }
        // COB_CODE: IF IND-TCL-IMP-INTR-RIT-PAG = -1
        //              MOVE HIGH-VALUES TO TCL-IMP-INTR-RIT-PAG-NULL
        //           END-IF
        if (ws.getIndTcontLiq().getImpIntrRitPag() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TCL-IMP-INTR-RIT-PAG-NULL
            tcontLiq.getTclImpIntrRitPag().setTclImpIntrRitPagNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TclImpIntrRitPag.Len.TCL_IMP_INTR_RIT_PAG_NULL));
        }
        // COB_CODE: IF IND-TCL-IMPB-IS = -1
        //              MOVE HIGH-VALUES TO TCL-IMPB-IS-NULL
        //           END-IF
        if (ws.getIndTcontLiq().getImpbIs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TCL-IMPB-IS-NULL
            tcontLiq.getTclImpbIs().setTclImpbIsNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TclImpbIs.Len.TCL_IMPB_IS_NULL));
        }
        // COB_CODE: IF IND-TCL-IMPB-IS-1382011 = -1
        //              MOVE HIGH-VALUES TO TCL-IMPB-IS-1382011-NULL
        //           END-IF
        if (ws.getIndTcontLiq().getImpbIs1382011() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TCL-IMPB-IS-1382011-NULL
            tcontLiq.getTclImpbIs1382011().setTclImpbIs1382011Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TclImpbIs1382011.Len.TCL_IMPB_IS1382011_NULL));
        }
        // COB_CODE: IF IND-TCL-IMPB-VIS-662014 = -1
        //              MOVE HIGH-VALUES TO TCL-IMPB-VIS-662014-NULL
        //           END-IF
        if (ws.getIndTcontLiq().getImpbVis662014() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TCL-IMPB-VIS-662014-NULL
            tcontLiq.getTclImpbVis662014().setTclImpbVis662014Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TclImpbVis662014.Len.TCL_IMPB_VIS662014_NULL));
        }
        // COB_CODE: IF IND-TCL-IMPST-VIS-662014 = -1
        //              MOVE HIGH-VALUES TO TCL-IMPST-VIS-662014-NULL
        //           END-IF
        if (ws.getIndTcontLiq().getImpstVis662014() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TCL-IMPST-VIS-662014-NULL
            tcontLiq.getTclImpstVis662014().setTclImpstVis662014Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TclImpstVis662014.Len.TCL_IMPST_VIS662014_NULL));
        }
        // COB_CODE: IF IND-TCL-IMPB-IS-662014 = -1
        //              MOVE HIGH-VALUES TO TCL-IMPB-IS-662014-NULL
        //           END-IF
        if (ws.getIndTcontLiq().getImpbIs662014() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TCL-IMPB-IS-662014-NULL
            tcontLiq.getTclImpbIs662014().setTclImpbIs662014Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TclImpbIs662014.Len.TCL_IMPB_IS662014_NULL));
        }
        // COB_CODE: IF IND-TCL-IMPST-SOST-662014 = -1
        //              MOVE HIGH-VALUES TO TCL-IMPST-SOST-662014-NULL
        //           END-IF.
        if (ws.getIndTcontLiq().getImpstSost662014() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TCL-IMPST-SOST-662014-NULL
            tcontLiq.getTclImpstSost662014().setTclImpstSost662014Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TclImpstSost662014.Len.TCL_IMPST_SOST662014_NULL));
        }
    }

    /**Original name: Z150-VALORIZZA-DATA-SERVICES-I<br>*/
    private void z150ValorizzaDataServicesI() {
        // COB_CODE: MOVE 'I' TO TCL-DS-OPER-SQL
        tcontLiq.setTclDsOperSqlFormatted("I");
        // COB_CODE: MOVE 0                   TO TCL-DS-VER
        tcontLiq.setTclDsVer(0);
        // COB_CODE: MOVE IDSV0003-USER-NAME TO TCL-DS-UTENTE
        tcontLiq.setTclDsUtente(idsv0003.getUserName());
        // COB_CODE: MOVE '1'                   TO TCL-DS-STATO-ELAB.
        tcontLiq.setTclDsStatoElabFormatted("1");
    }

    /**Original name: Z160-VALORIZZA-DATA-SERVICES-U<br>*/
    private void z160ValorizzaDataServicesU() {
        // COB_CODE: MOVE 'U' TO TCL-DS-OPER-SQL
        tcontLiq.setTclDsOperSqlFormatted("U");
        // COB_CODE: MOVE IDSV0003-USER-NAME TO TCL-DS-UTENTE.
        tcontLiq.setTclDsUtente(idsv0003.getUserName());
    }

    /**Original name: Z200-SET-INDICATORI-NULL<br>*/
    private void z200SetIndicatoriNull() {
        // COB_CODE: IF TCL-ID-MOVI-CHIU-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TCL-ID-MOVI-CHIU
        //           ELSE
        //              MOVE 0 TO IND-TCL-ID-MOVI-CHIU
        //           END-IF
        if (Characters.EQ_HIGH.test(tcontLiq.getTclIdMoviChiu().getTclIdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TCL-ID-MOVI-CHIU
            ws.getIndTcontLiq().setIdMoviChiu(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TCL-ID-MOVI-CHIU
            ws.getIndTcontLiq().setIdMoviChiu(((short)0));
        }
        // COB_CODE: IF TCL-DT-VLT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TCL-DT-VLT
        //           ELSE
        //              MOVE 0 TO IND-TCL-DT-VLT
        //           END-IF
        if (Characters.EQ_HIGH.test(tcontLiq.getTclDtVlt().getTclDtVltNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TCL-DT-VLT
            ws.getIndTcontLiq().setDtVlt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TCL-DT-VLT
            ws.getIndTcontLiq().setDtVlt(((short)0));
        }
        // COB_CODE: IF TCL-IMP-LRD-LIQTO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TCL-IMP-LRD-LIQTO
        //           ELSE
        //              MOVE 0 TO IND-TCL-IMP-LRD-LIQTO
        //           END-IF
        if (Characters.EQ_HIGH.test(tcontLiq.getTclImpLrdLiqto().getTclImpLrdLiqtoNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TCL-IMP-LRD-LIQTO
            ws.getIndTcontLiq().setImpLrdLiqto(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TCL-IMP-LRD-LIQTO
            ws.getIndTcontLiq().setImpLrdLiqto(((short)0));
        }
        // COB_CODE: IF TCL-IMP-PREST-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TCL-IMP-PREST
        //           ELSE
        //              MOVE 0 TO IND-TCL-IMP-PREST
        //           END-IF
        if (Characters.EQ_HIGH.test(tcontLiq.getTclImpPrest().getTclImpPrestNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TCL-IMP-PREST
            ws.getIndTcontLiq().setImpPrest(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TCL-IMP-PREST
            ws.getIndTcontLiq().setImpPrest(((short)0));
        }
        // COB_CODE: IF TCL-IMP-INTR-PREST-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TCL-IMP-INTR-PREST
        //           ELSE
        //              MOVE 0 TO IND-TCL-IMP-INTR-PREST
        //           END-IF
        if (Characters.EQ_HIGH.test(tcontLiq.getTclImpIntrPrest().getTclImpIntrPrestNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TCL-IMP-INTR-PREST
            ws.getIndTcontLiq().setImpIntrPrest(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TCL-IMP-INTR-PREST
            ws.getIndTcontLiq().setImpIntrPrest(((short)0));
        }
        // COB_CODE: IF TCL-IMP-RAT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TCL-IMP-RAT
        //           ELSE
        //              MOVE 0 TO IND-TCL-IMP-RAT
        //           END-IF
        if (Characters.EQ_HIGH.test(tcontLiq.getTclImpRat().getTclImpRatNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TCL-IMP-RAT
            ws.getIndTcontLiq().setImpRat(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TCL-IMP-RAT
            ws.getIndTcontLiq().setImpRat(((short)0));
        }
        // COB_CODE: IF TCL-IMP-UTI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TCL-IMP-UTI
        //           ELSE
        //              MOVE 0 TO IND-TCL-IMP-UTI
        //           END-IF
        if (Characters.EQ_HIGH.test(tcontLiq.getTclImpUti().getTclImpUtiNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TCL-IMP-UTI
            ws.getIndTcontLiq().setImpUti(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TCL-IMP-UTI
            ws.getIndTcontLiq().setImpUti(((short)0));
        }
        // COB_CODE: IF TCL-IMP-RIT-TFR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TCL-IMP-RIT-TFR
        //           ELSE
        //              MOVE 0 TO IND-TCL-IMP-RIT-TFR
        //           END-IF
        if (Characters.EQ_HIGH.test(tcontLiq.getTclImpRitTfr().getTclImpRitTfrNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TCL-IMP-RIT-TFR
            ws.getIndTcontLiq().setImpRitTfr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TCL-IMP-RIT-TFR
            ws.getIndTcontLiq().setImpRitTfr(((short)0));
        }
        // COB_CODE: IF TCL-IMP-RIT-ACC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TCL-IMP-RIT-ACC
        //           ELSE
        //              MOVE 0 TO IND-TCL-IMP-RIT-ACC
        //           END-IF
        if (Characters.EQ_HIGH.test(tcontLiq.getTclImpRitAcc().getTclImpRitAccNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TCL-IMP-RIT-ACC
            ws.getIndTcontLiq().setImpRitAcc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TCL-IMP-RIT-ACC
            ws.getIndTcontLiq().setImpRitAcc(((short)0));
        }
        // COB_CODE: IF TCL-IMP-RIT-VIS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TCL-IMP-RIT-VIS
        //           ELSE
        //              MOVE 0 TO IND-TCL-IMP-RIT-VIS
        //           END-IF
        if (Characters.EQ_HIGH.test(tcontLiq.getTclImpRitVis().getTclImpRitVisNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TCL-IMP-RIT-VIS
            ws.getIndTcontLiq().setImpRitVis(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TCL-IMP-RIT-VIS
            ws.getIndTcontLiq().setImpRitVis(((short)0));
        }
        // COB_CODE: IF TCL-IMPB-TFR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TCL-IMPB-TFR
        //           ELSE
        //              MOVE 0 TO IND-TCL-IMPB-TFR
        //           END-IF
        if (Characters.EQ_HIGH.test(tcontLiq.getTclImpbTfr().getTclImpbTfrNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TCL-IMPB-TFR
            ws.getIndTcontLiq().setImpbTfr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TCL-IMPB-TFR
            ws.getIndTcontLiq().setImpbTfr(((short)0));
        }
        // COB_CODE: IF TCL-IMPB-ACC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TCL-IMPB-ACC
        //           ELSE
        //              MOVE 0 TO IND-TCL-IMPB-ACC
        //           END-IF
        if (Characters.EQ_HIGH.test(tcontLiq.getTclImpbAcc().getTclImpbAccNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TCL-IMPB-ACC
            ws.getIndTcontLiq().setImpbAcc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TCL-IMPB-ACC
            ws.getIndTcontLiq().setImpbAcc(((short)0));
        }
        // COB_CODE: IF TCL-IMPB-VIS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TCL-IMPB-VIS
        //           ELSE
        //              MOVE 0 TO IND-TCL-IMPB-VIS
        //           END-IF
        if (Characters.EQ_HIGH.test(tcontLiq.getTclImpbVis().getTclImpbVisNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TCL-IMPB-VIS
            ws.getIndTcontLiq().setImpbVis(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TCL-IMPB-VIS
            ws.getIndTcontLiq().setImpbVis(((short)0));
        }
        // COB_CODE: IF TCL-IMP-RIMB-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TCL-IMP-RIMB
        //           ELSE
        //              MOVE 0 TO IND-TCL-IMP-RIMB
        //           END-IF
        if (Characters.EQ_HIGH.test(tcontLiq.getTclImpRimb().getTclImpRimbNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TCL-IMP-RIMB
            ws.getIndTcontLiq().setImpRimb(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TCL-IMP-RIMB
            ws.getIndTcontLiq().setImpRimb(((short)0));
        }
        // COB_CODE: IF TCL-IMP-CORTVO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TCL-IMP-CORTVO
        //           ELSE
        //              MOVE 0 TO IND-TCL-IMP-CORTVO
        //           END-IF
        if (Characters.EQ_HIGH.test(tcontLiq.getTclImpCortvo().getTclImpCortvoNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TCL-IMP-CORTVO
            ws.getIndTcontLiq().setImpCortvo(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TCL-IMP-CORTVO
            ws.getIndTcontLiq().setImpCortvo(((short)0));
        }
        // COB_CODE: IF TCL-IMPB-IMPST-PRVR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TCL-IMPB-IMPST-PRVR
        //           ELSE
        //              MOVE 0 TO IND-TCL-IMPB-IMPST-PRVR
        //           END-IF
        if (Characters.EQ_HIGH.test(tcontLiq.getTclImpbImpstPrvr().getTclImpbImpstPrvrNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TCL-IMPB-IMPST-PRVR
            ws.getIndTcontLiq().setImpbImpstPrvr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TCL-IMPB-IMPST-PRVR
            ws.getIndTcontLiq().setImpbImpstPrvr(((short)0));
        }
        // COB_CODE: IF TCL-IMPST-PRVR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TCL-IMPST-PRVR
        //           ELSE
        //              MOVE 0 TO IND-TCL-IMPST-PRVR
        //           END-IF
        if (Characters.EQ_HIGH.test(tcontLiq.getTclImpstPrvr().getTclImpstPrvrNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TCL-IMPST-PRVR
            ws.getIndTcontLiq().setImpstPrvr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TCL-IMPST-PRVR
            ws.getIndTcontLiq().setImpstPrvr(((short)0));
        }
        // COB_CODE: IF TCL-IMPB-IMPST-252-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TCL-IMPB-IMPST-252
        //           ELSE
        //              MOVE 0 TO IND-TCL-IMPB-IMPST-252
        //           END-IF
        if (Characters.EQ_HIGH.test(tcontLiq.getTclImpbImpst252().getTclImpbImpst252NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TCL-IMPB-IMPST-252
            ws.getIndTcontLiq().setImpbImpst252(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TCL-IMPB-IMPST-252
            ws.getIndTcontLiq().setImpbImpst252(((short)0));
        }
        // COB_CODE: IF TCL-IMPST-252-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TCL-IMPST-252
        //           ELSE
        //              MOVE 0 TO IND-TCL-IMPST-252
        //           END-IF
        if (Characters.EQ_HIGH.test(tcontLiq.getTclImpst252().getTclImpst252NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TCL-IMPST-252
            ws.getIndTcontLiq().setImpst252(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TCL-IMPST-252
            ws.getIndTcontLiq().setImpst252(((short)0));
        }
        // COB_CODE: IF TCL-IMP-IS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TCL-IMP-IS
        //           ELSE
        //              MOVE 0 TO IND-TCL-IMP-IS
        //           END-IF
        if (Characters.EQ_HIGH.test(tcontLiq.getTclImpIs().getTclImpIsNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TCL-IMP-IS
            ws.getIndTcontLiq().setImpIs(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TCL-IMP-IS
            ws.getIndTcontLiq().setImpIs(((short)0));
        }
        // COB_CODE: IF TCL-IMP-DIR-LIQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TCL-IMP-DIR-LIQ
        //           ELSE
        //              MOVE 0 TO IND-TCL-IMP-DIR-LIQ
        //           END-IF
        if (Characters.EQ_HIGH.test(tcontLiq.getTclImpDirLiq().getTclImpDirLiqNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TCL-IMP-DIR-LIQ
            ws.getIndTcontLiq().setImpDirLiq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TCL-IMP-DIR-LIQ
            ws.getIndTcontLiq().setImpDirLiq(((short)0));
        }
        // COB_CODE: IF TCL-IMP-NET-LIQTO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TCL-IMP-NET-LIQTO
        //           ELSE
        //              MOVE 0 TO IND-TCL-IMP-NET-LIQTO
        //           END-IF
        if (Characters.EQ_HIGH.test(tcontLiq.getTclImpNetLiqto().getTclImpNetLiqtoNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TCL-IMP-NET-LIQTO
            ws.getIndTcontLiq().setImpNetLiqto(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TCL-IMP-NET-LIQTO
            ws.getIndTcontLiq().setImpNetLiqto(((short)0));
        }
        // COB_CODE: IF TCL-IMP-EFFLQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TCL-IMP-EFFLQ
        //           ELSE
        //              MOVE 0 TO IND-TCL-IMP-EFFLQ
        //           END-IF
        if (Characters.EQ_HIGH.test(tcontLiq.getTclImpEfflq().getTclImpEfflqNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TCL-IMP-EFFLQ
            ws.getIndTcontLiq().setImpEfflq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TCL-IMP-EFFLQ
            ws.getIndTcontLiq().setImpEfflq(((short)0));
        }
        // COB_CODE: IF TCL-COD-DVS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TCL-COD-DVS
        //           ELSE
        //              MOVE 0 TO IND-TCL-COD-DVS
        //           END-IF
        if (Characters.EQ_HIGH.test(tcontLiq.getTclCodDvsFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TCL-COD-DVS
            ws.getIndTcontLiq().setCodDvs(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TCL-COD-DVS
            ws.getIndTcontLiq().setCodDvs(((short)0));
        }
        // COB_CODE: IF TCL-TP-MEZ-PAG-ACCR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TCL-TP-MEZ-PAG-ACCR
        //           ELSE
        //              MOVE 0 TO IND-TCL-TP-MEZ-PAG-ACCR
        //           END-IF
        if (Characters.EQ_HIGH.test(tcontLiq.getTclTpMezPagAccrFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TCL-TP-MEZ-PAG-ACCR
            ws.getIndTcontLiq().setTpMezPagAccr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TCL-TP-MEZ-PAG-ACCR
            ws.getIndTcontLiq().setTpMezPagAccr(((short)0));
        }
        // COB_CODE: IF TCL-ESTR-CNT-CORR-ACCR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TCL-ESTR-CNT-CORR-ACCR
        //           ELSE
        //              MOVE 0 TO IND-TCL-ESTR-CNT-CORR-ACCR
        //           END-IF
        if (Characters.EQ_HIGH.test(tcontLiq.getTclEstrCntCorrAccrFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TCL-ESTR-CNT-CORR-ACCR
            ws.getIndTcontLiq().setEstrCntCorrAccr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TCL-ESTR-CNT-CORR-ACCR
            ws.getIndTcontLiq().setEstrCntCorrAccr(((short)0));
        }
        // COB_CODE: IF TCL-TP-STAT-TIT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TCL-TP-STAT-TIT
        //           ELSE
        //              MOVE 0 TO IND-TCL-TP-STAT-TIT
        //           END-IF
        if (Characters.EQ_HIGH.test(tcontLiq.getTclTpStatTitFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TCL-TP-STAT-TIT
            ws.getIndTcontLiq().setTpStatTit(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TCL-TP-STAT-TIT
            ws.getIndTcontLiq().setTpStatTit(((short)0));
        }
        // COB_CODE: IF TCL-IMPB-VIS-1382011-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TCL-IMPB-VIS-1382011
        //           ELSE
        //              MOVE 0 TO IND-TCL-IMPB-VIS-1382011
        //           END-IF
        if (Characters.EQ_HIGH.test(tcontLiq.getTclImpbVis1382011().getTclImpbVis1382011NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TCL-IMPB-VIS-1382011
            ws.getIndTcontLiq().setImpbVis1382011(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TCL-IMPB-VIS-1382011
            ws.getIndTcontLiq().setImpbVis1382011(((short)0));
        }
        // COB_CODE: IF TCL-IMPST-VIS-1382011-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TCL-IMPST-VIS-1382011
        //           ELSE
        //              MOVE 0 TO IND-TCL-IMPST-VIS-1382011
        //           END-IF
        if (Characters.EQ_HIGH.test(tcontLiq.getTclImpstVis1382011().getTclImpstVis1382011NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TCL-IMPST-VIS-1382011
            ws.getIndTcontLiq().setImpstVis1382011(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TCL-IMPST-VIS-1382011
            ws.getIndTcontLiq().setImpstVis1382011(((short)0));
        }
        // COB_CODE: IF TCL-IMPST-SOST-1382011-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TCL-IMPST-SOST-1382011
        //           ELSE
        //              MOVE 0 TO IND-TCL-IMPST-SOST-1382011
        //           END-IF
        if (Characters.EQ_HIGH.test(tcontLiq.getTclImpstSost1382011().getTclImpstSost1382011NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TCL-IMPST-SOST-1382011
            ws.getIndTcontLiq().setImpstSost1382011(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TCL-IMPST-SOST-1382011
            ws.getIndTcontLiq().setImpstSost1382011(((short)0));
        }
        // COB_CODE: IF TCL-IMP-INTR-RIT-PAG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TCL-IMP-INTR-RIT-PAG
        //           ELSE
        //              MOVE 0 TO IND-TCL-IMP-INTR-RIT-PAG
        //           END-IF
        if (Characters.EQ_HIGH.test(tcontLiq.getTclImpIntrRitPag().getTclImpIntrRitPagNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TCL-IMP-INTR-RIT-PAG
            ws.getIndTcontLiq().setImpIntrRitPag(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TCL-IMP-INTR-RIT-PAG
            ws.getIndTcontLiq().setImpIntrRitPag(((short)0));
        }
        // COB_CODE: IF TCL-IMPB-IS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TCL-IMPB-IS
        //           ELSE
        //              MOVE 0 TO IND-TCL-IMPB-IS
        //           END-IF
        if (Characters.EQ_HIGH.test(tcontLiq.getTclImpbIs().getTclImpbIsNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TCL-IMPB-IS
            ws.getIndTcontLiq().setImpbIs(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TCL-IMPB-IS
            ws.getIndTcontLiq().setImpbIs(((short)0));
        }
        // COB_CODE: IF TCL-IMPB-IS-1382011-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TCL-IMPB-IS-1382011
        //           ELSE
        //              MOVE 0 TO IND-TCL-IMPB-IS-1382011
        //           END-IF
        if (Characters.EQ_HIGH.test(tcontLiq.getTclImpbIs1382011().getTclImpbIs1382011NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TCL-IMPB-IS-1382011
            ws.getIndTcontLiq().setImpbIs1382011(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TCL-IMPB-IS-1382011
            ws.getIndTcontLiq().setImpbIs1382011(((short)0));
        }
        // COB_CODE: IF TCL-IMPB-VIS-662014-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TCL-IMPB-VIS-662014
        //           ELSE
        //              MOVE 0 TO IND-TCL-IMPB-VIS-662014
        //           END-IF
        if (Characters.EQ_HIGH.test(tcontLiq.getTclImpbVis662014().getTclImpbVis662014NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TCL-IMPB-VIS-662014
            ws.getIndTcontLiq().setImpbVis662014(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TCL-IMPB-VIS-662014
            ws.getIndTcontLiq().setImpbVis662014(((short)0));
        }
        // COB_CODE: IF TCL-IMPST-VIS-662014-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TCL-IMPST-VIS-662014
        //           ELSE
        //              MOVE 0 TO IND-TCL-IMPST-VIS-662014
        //           END-IF
        if (Characters.EQ_HIGH.test(tcontLiq.getTclImpstVis662014().getTclImpstVis662014NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TCL-IMPST-VIS-662014
            ws.getIndTcontLiq().setImpstVis662014(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TCL-IMPST-VIS-662014
            ws.getIndTcontLiq().setImpstVis662014(((short)0));
        }
        // COB_CODE: IF TCL-IMPB-IS-662014-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TCL-IMPB-IS-662014
        //           ELSE
        //              MOVE 0 TO IND-TCL-IMPB-IS-662014
        //           END-IF
        if (Characters.EQ_HIGH.test(tcontLiq.getTclImpbIs662014().getTclImpbIs662014NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TCL-IMPB-IS-662014
            ws.getIndTcontLiq().setImpbIs662014(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TCL-IMPB-IS-662014
            ws.getIndTcontLiq().setImpbIs662014(((short)0));
        }
        // COB_CODE: IF TCL-IMPST-SOST-662014-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-TCL-IMPST-SOST-662014
        //           ELSE
        //              MOVE 0 TO IND-TCL-IMPST-SOST-662014
        //           END-IF.
        if (Characters.EQ_HIGH.test(tcontLiq.getTclImpstSost662014().getTclImpstSost662014NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-TCL-IMPST-SOST-662014
            ws.getIndTcontLiq().setImpstSost662014(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-TCL-IMPST-SOST-662014
            ws.getIndTcontLiq().setImpstSost662014(((short)0));
        }
    }

    /**Original name: Z400-SEQ-RIGA<br>*/
    private void z400SeqRiga() {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_RIGA
        //              INTO : TCL-DS-RIGA
        //           END-EXEC.
        //TODO: Implement: valuesStmt;
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: Z500-AGGIORNAMENTO-STORICO<br>*/
    private void z500AggiornamentoStorico() {
        // COB_CODE: MOVE TCONT-LIQ TO WS-BUFFER-TABLE.
        ws.setWsBufferTable(tcontLiq.getTcontLiqFormatted());
        // COB_CODE: MOVE TCL-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.
        ws.setWsIdMoviCrz(TruncAbs.toInt(tcontLiq.getTclIdMoviCrz(), 9));
        // COB_CODE: PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.
        a360OpenCursorIdEff();
        // COB_CODE: PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-SQL
        //              END-IF
        //           END-PERFORM.
        while (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX
            a390FetchNextIdEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              END-IF
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: MOVE WS-ID-MOVI-CRZ   TO TCL-ID-MOVI-CHIU
                tcontLiq.getTclIdMoviChiu().setTclIdMoviChiu(ws.getWsIdMoviCrz());
                // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
                //                                 TO TCL-DS-TS-END-CPTZ
                tcontLiq.setTclDsTsEndCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
                // COB_CODE: PERFORM A330-UPDATE-ID-EFF THRU A330-EX
                a330UpdateIdEff();
                // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
                //                 END-IF
                //           END-IF
                if (idsv0003.getSqlcode().isSuccessfulSql()) {
                    // COB_CODE: MOVE WS-ID-MOVI-CRZ TO TCL-ID-MOVI-CRZ
                    tcontLiq.setTclIdMoviCrz(ws.getWsIdMoviCrz());
                    // COB_CODE: MOVE HIGH-VALUES    TO TCL-ID-MOVI-CHIU-NULL
                    tcontLiq.getTclIdMoviChiu().setTclIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TclIdMoviChiu.Len.TCL_ID_MOVI_CHIU_NULL));
                    // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
                    //                               TO TCL-DT-END-EFF
                    tcontLiq.setTclDtEndEff(idsv0003.getDataInizioEffetto());
                    // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
                    //                               TO TCL-DS-TS-INI-CPTZ
                    tcontLiq.setTclDsTsIniCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
                    // COB_CODE: MOVE WS-TS-INFINITO
                    //                               TO TCL-DS-TS-END-CPTZ
                    tcontLiq.setTclDsTsEndCptz(ws.getIdsv0010().getWsTsInfinito());
                    // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
                    //              PERFORM A220-INSERT-PK THRU A220-EX
                    //           END-IF
                    if (idsv0003.getSqlcode().isSuccessfulSql()) {
                        // COB_CODE: PERFORM A220-INSERT-PK THRU A220-EX
                        a220InsertPk();
                    }
                }
            }
        }
        // COB_CODE: IF IDSV0003-NOT-FOUND
        //              END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF NOT IDSV0003-DELETE-LOGICA
            //              PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX
            //           ELSE
            //              SET IDSV0003-SUCCESSFUL-SQL TO TRUE
            //           END-IF
            if (!idsv0003.getOperazione().isDeleteLogica()) {
                // COB_CODE: PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX
                z600InsertNuovaRigaStorica();
            }
            else {
                // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL TO TRUE
                idsv0003.getSqlcode().setSuccessfulSql();
            }
        }
    }

    /**Original name: Z550-AGG-STORICO-SOLO-INS<br>*/
    private void z550AggStoricoSoloIns() {
        // COB_CODE: MOVE TCONT-LIQ TO WS-BUFFER-TABLE.
        ws.setWsBufferTable(tcontLiq.getTcontLiqFormatted());
        // COB_CODE: MOVE TCL-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.
        ws.setWsIdMoviCrz(TruncAbs.toInt(tcontLiq.getTclIdMoviCrz(), 9));
        // COB_CODE: PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX.
        z600InsertNuovaRigaStorica();
    }

    /**Original name: Z600-INSERT-NUOVA-RIGA-STORICA<br>*/
    private void z600InsertNuovaRigaStorica() {
        // COB_CODE: MOVE WS-BUFFER-TABLE TO TCONT-LIQ.
        tcontLiq.setTcontLiqFormatted(ws.getWsBufferTableFormatted());
        // COB_CODE: MOVE WS-ID-MOVI-CRZ  TO TCL-ID-MOVI-CRZ.
        tcontLiq.setTclIdMoviCrz(ws.getWsIdMoviCrz());
        // COB_CODE: MOVE HIGH-VALUES     TO TCL-ID-MOVI-CHIU-NULL.
        tcontLiq.getTclIdMoviChiu().setTclIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TclIdMoviChiu.Len.TCL_ID_MOVI_CHIU_NULL));
        // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
        //                                TO TCL-DT-INI-EFF.
        tcontLiq.setTclDtIniEff(idsv0003.getDataInizioEffetto());
        // COB_CODE: MOVE WS-DT-INFINITO
        //                                TO TCL-DT-END-EFF.
        tcontLiq.setTclDtEndEff(ws.getIdsv0010().getWsDtInfinito());
        // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
        //                                TO TCL-DS-TS-INI-CPTZ.
        tcontLiq.setTclDsTsIniCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
        // COB_CODE: MOVE WS-TS-INFINITO
        //                                TO TCL-DS-TS-END-CPTZ.
        tcontLiq.setTclDsTsEndCptz(ws.getIdsv0010().getWsTsInfinito());
        // COB_CODE: MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
        //                                TO TCL-COD-COMP-ANIA.
        tcontLiq.setTclCodCompAnia(idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A220-INSERT-PK THRU A220-EX.
        a220InsertPk();
    }

    /**Original name: Z900-CONVERTI-N-TO-X<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da 9(8) comp-3 a date
	 * ----</pre>*/
    private void z900ConvertiNToX() {
        // COB_CODE: MOVE TCL-DT-INI-EFF TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(tcontLiq.getTclDtIniEff(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO TCL-DT-INI-EFF-DB
        ws.getTcontLiqDb().setIniEffDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: MOVE TCL-DT-END-EFF TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(tcontLiq.getTclDtEndEff(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO TCL-DT-END-EFF-DB
        ws.getTcontLiqDb().setEndEffDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: IF IND-TCL-DT-VLT = 0
        //               MOVE WS-DATE-X      TO TCL-DT-VLT-DB
        //           END-IF.
        if (ws.getIndTcontLiq().getDtVlt() == 0) {
            // COB_CODE: MOVE TCL-DT-VLT TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(tcontLiq.getTclDtVlt().getTclDtVlt(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO TCL-DT-VLT-DB
            ws.getTcontLiqDb().setVltDb(ws.getIdsv0010().getWsDateX());
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE TCL-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getTcontLiqDb().getIniEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO TCL-DT-INI-EFF
        tcontLiq.setTclDtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE TCL-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getTcontLiqDb().getEndEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO TCL-DT-END-EFF
        tcontLiq.setTclDtEndEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-TCL-DT-VLT = 0
        //               MOVE WS-DATE-N      TO TCL-DT-VLT
        //           END-IF.
        if (ws.getIndTcontLiq().getDtVlt() == 0) {
            // COB_CODE: MOVE TCL-DT-VLT-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getTcontLiqDb().getVltDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO TCL-DT-VLT
            tcontLiq.getTclDtVlt().setTclDtVlt(ws.getIdsv0010().getWsDateN());
        }
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public int getCodCompAnia() {
        return tcontLiq.getTclCodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.tcontLiq.setTclCodCompAnia(codCompAnia);
    }

    @Override
    public String getCodDvs() {
        return tcontLiq.getTclCodDvs();
    }

    @Override
    public void setCodDvs(String codDvs) {
        this.tcontLiq.setTclCodDvs(codDvs);
    }

    @Override
    public String getCodDvsObj() {
        if (ws.getIndTcontLiq().getCodDvs() >= 0) {
            return getCodDvs();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodDvsObj(String codDvsObj) {
        if (codDvsObj != null) {
            setCodDvs(codDvsObj);
            ws.getIndTcontLiq().setCodDvs(((short)0));
        }
        else {
            ws.getIndTcontLiq().setCodDvs(((short)-1));
        }
    }

    @Override
    public char getDsOperSql() {
        return tcontLiq.getTclDsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.tcontLiq.setTclDsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return tcontLiq.getTclDsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.tcontLiq.setTclDsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsEndCptz() {
        return tcontLiq.getTclDsTsEndCptz();
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.tcontLiq.setTclDsTsEndCptz(dsTsEndCptz);
    }

    @Override
    public long getDsTsIniCptz() {
        return tcontLiq.getTclDsTsIniCptz();
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.tcontLiq.setTclDsTsIniCptz(dsTsIniCptz);
    }

    @Override
    public String getDsUtente() {
        return tcontLiq.getTclDsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.tcontLiq.setTclDsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return tcontLiq.getTclDsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.tcontLiq.setTclDsVer(dsVer);
    }

    @Override
    public String getDtEndEffDb() {
        return ws.getTcontLiqDb().getEndEffDb();
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        this.ws.getTcontLiqDb().setEndEffDb(dtEndEffDb);
    }

    @Override
    public String getDtIniEffDb() {
        return ws.getTcontLiqDb().getIniEffDb();
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        this.ws.getTcontLiqDb().setIniEffDb(dtIniEffDb);
    }

    @Override
    public String getDtVltDb() {
        return ws.getTcontLiqDb().getVltDb();
    }

    @Override
    public void setDtVltDb(String dtVltDb) {
        this.ws.getTcontLiqDb().setVltDb(dtVltDb);
    }

    @Override
    public String getDtVltDbObj() {
        if (ws.getIndTcontLiq().getDtVlt() >= 0) {
            return getDtVltDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtVltDbObj(String dtVltDbObj) {
        if (dtVltDbObj != null) {
            setDtVltDb(dtVltDbObj);
            ws.getIndTcontLiq().setDtVlt(((short)0));
        }
        else {
            ws.getIndTcontLiq().setDtVlt(((short)-1));
        }
    }

    @Override
    public String getEstrCntCorrAccr() {
        return tcontLiq.getTclEstrCntCorrAccr();
    }

    @Override
    public void setEstrCntCorrAccr(String estrCntCorrAccr) {
        this.tcontLiq.setTclEstrCntCorrAccr(estrCntCorrAccr);
    }

    @Override
    public String getEstrCntCorrAccrObj() {
        if (ws.getIndTcontLiq().getEstrCntCorrAccr() >= 0) {
            return getEstrCntCorrAccr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setEstrCntCorrAccrObj(String estrCntCorrAccrObj) {
        if (estrCntCorrAccrObj != null) {
            setEstrCntCorrAccr(estrCntCorrAccrObj);
            ws.getIndTcontLiq().setEstrCntCorrAccr(((short)0));
        }
        else {
            ws.getIndTcontLiq().setEstrCntCorrAccr(((short)-1));
        }
    }

    @Override
    public int getIdBnficrLiq() {
        return tcontLiq.getTclIdBnficrLiq();
    }

    @Override
    public void setIdBnficrLiq(int idBnficrLiq) {
        this.tcontLiq.setTclIdBnficrLiq(idBnficrLiq);
    }

    @Override
    public int getIdMoviChiu() {
        return tcontLiq.getTclIdMoviChiu().getTclIdMoviChiu();
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        this.tcontLiq.getTclIdMoviChiu().setTclIdMoviChiu(idMoviChiu);
    }

    @Override
    public Integer getIdMoviChiuObj() {
        if (ws.getIndTcontLiq().getIdMoviChiu() >= 0) {
            return ((Integer)getIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        if (idMoviChiuObj != null) {
            setIdMoviChiu(((int)idMoviChiuObj));
            ws.getIndTcontLiq().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndTcontLiq().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getIdMoviCrz() {
        return tcontLiq.getTclIdMoviCrz();
    }

    @Override
    public void setIdMoviCrz(int idMoviCrz) {
        this.tcontLiq.setTclIdMoviCrz(idMoviCrz);
    }

    @Override
    public int getIdPercLiq() {
        return tcontLiq.getTclIdPercLiq();
    }

    @Override
    public void setIdPercLiq(int idPercLiq) {
        this.tcontLiq.setTclIdPercLiq(idPercLiq);
    }

    @Override
    public int getIdTcontLiq() {
        return tcontLiq.getTclIdTcontLiq();
    }

    @Override
    public void setIdTcontLiq(int idTcontLiq) {
        this.tcontLiq.setTclIdTcontLiq(idTcontLiq);
    }

    @Override
    public AfDecimal getImpCortvo() {
        return tcontLiq.getTclImpCortvo().getTclImpCortvo();
    }

    @Override
    public void setImpCortvo(AfDecimal impCortvo) {
        this.tcontLiq.getTclImpCortvo().setTclImpCortvo(impCortvo.copy());
    }

    @Override
    public AfDecimal getImpCortvoObj() {
        if (ws.getIndTcontLiq().getImpCortvo() >= 0) {
            return getImpCortvo();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpCortvoObj(AfDecimal impCortvoObj) {
        if (impCortvoObj != null) {
            setImpCortvo(new AfDecimal(impCortvoObj, 15, 3));
            ws.getIndTcontLiq().setImpCortvo(((short)0));
        }
        else {
            ws.getIndTcontLiq().setImpCortvo(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpDirLiq() {
        return tcontLiq.getTclImpDirLiq().getTclImpDirLiq();
    }

    @Override
    public void setImpDirLiq(AfDecimal impDirLiq) {
        this.tcontLiq.getTclImpDirLiq().setTclImpDirLiq(impDirLiq.copy());
    }

    @Override
    public AfDecimal getImpDirLiqObj() {
        if (ws.getIndTcontLiq().getImpDirLiq() >= 0) {
            return getImpDirLiq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpDirLiqObj(AfDecimal impDirLiqObj) {
        if (impDirLiqObj != null) {
            setImpDirLiq(new AfDecimal(impDirLiqObj, 15, 3));
            ws.getIndTcontLiq().setImpDirLiq(((short)0));
        }
        else {
            ws.getIndTcontLiq().setImpDirLiq(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpEfflq() {
        return tcontLiq.getTclImpEfflq().getTclImpEfflq();
    }

    @Override
    public void setImpEfflq(AfDecimal impEfflq) {
        this.tcontLiq.getTclImpEfflq().setTclImpEfflq(impEfflq.copy());
    }

    @Override
    public AfDecimal getImpEfflqObj() {
        if (ws.getIndTcontLiq().getImpEfflq() >= 0) {
            return getImpEfflq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpEfflqObj(AfDecimal impEfflqObj) {
        if (impEfflqObj != null) {
            setImpEfflq(new AfDecimal(impEfflqObj, 15, 3));
            ws.getIndTcontLiq().setImpEfflq(((short)0));
        }
        else {
            ws.getIndTcontLiq().setImpEfflq(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpIntrPrest() {
        return tcontLiq.getTclImpIntrPrest().getTclImpIntrPrest();
    }

    @Override
    public void setImpIntrPrest(AfDecimal impIntrPrest) {
        this.tcontLiq.getTclImpIntrPrest().setTclImpIntrPrest(impIntrPrest.copy());
    }

    @Override
    public AfDecimal getImpIntrPrestObj() {
        if (ws.getIndTcontLiq().getImpIntrPrest() >= 0) {
            return getImpIntrPrest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpIntrPrestObj(AfDecimal impIntrPrestObj) {
        if (impIntrPrestObj != null) {
            setImpIntrPrest(new AfDecimal(impIntrPrestObj, 15, 3));
            ws.getIndTcontLiq().setImpIntrPrest(((short)0));
        }
        else {
            ws.getIndTcontLiq().setImpIntrPrest(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpIntrRitPag() {
        return tcontLiq.getTclImpIntrRitPag().getTclImpIntrRitPag();
    }

    @Override
    public void setImpIntrRitPag(AfDecimal impIntrRitPag) {
        this.tcontLiq.getTclImpIntrRitPag().setTclImpIntrRitPag(impIntrRitPag.copy());
    }

    @Override
    public AfDecimal getImpIntrRitPagObj() {
        if (ws.getIndTcontLiq().getImpIntrRitPag() >= 0) {
            return getImpIntrRitPag();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpIntrRitPagObj(AfDecimal impIntrRitPagObj) {
        if (impIntrRitPagObj != null) {
            setImpIntrRitPag(new AfDecimal(impIntrRitPagObj, 15, 3));
            ws.getIndTcontLiq().setImpIntrRitPag(((short)0));
        }
        else {
            ws.getIndTcontLiq().setImpIntrRitPag(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpIs() {
        return tcontLiq.getTclImpIs().getTclImpIs();
    }

    @Override
    public void setImpIs(AfDecimal impIs) {
        this.tcontLiq.getTclImpIs().setTclImpIs(impIs.copy());
    }

    @Override
    public AfDecimal getImpIsObj() {
        if (ws.getIndTcontLiq().getImpIs() >= 0) {
            return getImpIs();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpIsObj(AfDecimal impIsObj) {
        if (impIsObj != null) {
            setImpIs(new AfDecimal(impIsObj, 15, 3));
            ws.getIndTcontLiq().setImpIs(((short)0));
        }
        else {
            ws.getIndTcontLiq().setImpIs(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpLrdLiqto() {
        return tcontLiq.getTclImpLrdLiqto().getTclImpLrdLiqto();
    }

    @Override
    public void setImpLrdLiqto(AfDecimal impLrdLiqto) {
        this.tcontLiq.getTclImpLrdLiqto().setTclImpLrdLiqto(impLrdLiqto.copy());
    }

    @Override
    public AfDecimal getImpLrdLiqtoObj() {
        if (ws.getIndTcontLiq().getImpLrdLiqto() >= 0) {
            return getImpLrdLiqto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpLrdLiqtoObj(AfDecimal impLrdLiqtoObj) {
        if (impLrdLiqtoObj != null) {
            setImpLrdLiqto(new AfDecimal(impLrdLiqtoObj, 15, 3));
            ws.getIndTcontLiq().setImpLrdLiqto(((short)0));
        }
        else {
            ws.getIndTcontLiq().setImpLrdLiqto(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpNetLiqto() {
        return tcontLiq.getTclImpNetLiqto().getTclImpNetLiqto();
    }

    @Override
    public void setImpNetLiqto(AfDecimal impNetLiqto) {
        this.tcontLiq.getTclImpNetLiqto().setTclImpNetLiqto(impNetLiqto.copy());
    }

    @Override
    public AfDecimal getImpNetLiqtoObj() {
        if (ws.getIndTcontLiq().getImpNetLiqto() >= 0) {
            return getImpNetLiqto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpNetLiqtoObj(AfDecimal impNetLiqtoObj) {
        if (impNetLiqtoObj != null) {
            setImpNetLiqto(new AfDecimal(impNetLiqtoObj, 15, 3));
            ws.getIndTcontLiq().setImpNetLiqto(((short)0));
        }
        else {
            ws.getIndTcontLiq().setImpNetLiqto(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpPrest() {
        return tcontLiq.getTclImpPrest().getTclImpPrest();
    }

    @Override
    public void setImpPrest(AfDecimal impPrest) {
        this.tcontLiq.getTclImpPrest().setTclImpPrest(impPrest.copy());
    }

    @Override
    public AfDecimal getImpPrestObj() {
        if (ws.getIndTcontLiq().getImpPrest() >= 0) {
            return getImpPrest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpPrestObj(AfDecimal impPrestObj) {
        if (impPrestObj != null) {
            setImpPrest(new AfDecimal(impPrestObj, 15, 3));
            ws.getIndTcontLiq().setImpPrest(((short)0));
        }
        else {
            ws.getIndTcontLiq().setImpPrest(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpRat() {
        return tcontLiq.getTclImpRat().getTclImpRat();
    }

    @Override
    public void setImpRat(AfDecimal impRat) {
        this.tcontLiq.getTclImpRat().setTclImpRat(impRat.copy());
    }

    @Override
    public AfDecimal getImpRatObj() {
        if (ws.getIndTcontLiq().getImpRat() >= 0) {
            return getImpRat();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpRatObj(AfDecimal impRatObj) {
        if (impRatObj != null) {
            setImpRat(new AfDecimal(impRatObj, 15, 3));
            ws.getIndTcontLiq().setImpRat(((short)0));
        }
        else {
            ws.getIndTcontLiq().setImpRat(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpRimb() {
        return tcontLiq.getTclImpRimb().getTclImpRimb();
    }

    @Override
    public void setImpRimb(AfDecimal impRimb) {
        this.tcontLiq.getTclImpRimb().setTclImpRimb(impRimb.copy());
    }

    @Override
    public AfDecimal getImpRimbObj() {
        if (ws.getIndTcontLiq().getImpRimb() >= 0) {
            return getImpRimb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpRimbObj(AfDecimal impRimbObj) {
        if (impRimbObj != null) {
            setImpRimb(new AfDecimal(impRimbObj, 15, 3));
            ws.getIndTcontLiq().setImpRimb(((short)0));
        }
        else {
            ws.getIndTcontLiq().setImpRimb(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpRitAcc() {
        return tcontLiq.getTclImpRitAcc().getTclImpRitAcc();
    }

    @Override
    public void setImpRitAcc(AfDecimal impRitAcc) {
        this.tcontLiq.getTclImpRitAcc().setTclImpRitAcc(impRitAcc.copy());
    }

    @Override
    public AfDecimal getImpRitAccObj() {
        if (ws.getIndTcontLiq().getImpRitAcc() >= 0) {
            return getImpRitAcc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpRitAccObj(AfDecimal impRitAccObj) {
        if (impRitAccObj != null) {
            setImpRitAcc(new AfDecimal(impRitAccObj, 15, 3));
            ws.getIndTcontLiq().setImpRitAcc(((short)0));
        }
        else {
            ws.getIndTcontLiq().setImpRitAcc(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpRitTfr() {
        return tcontLiq.getTclImpRitTfr().getTclImpRitTfr();
    }

    @Override
    public void setImpRitTfr(AfDecimal impRitTfr) {
        this.tcontLiq.getTclImpRitTfr().setTclImpRitTfr(impRitTfr.copy());
    }

    @Override
    public AfDecimal getImpRitTfrObj() {
        if (ws.getIndTcontLiq().getImpRitTfr() >= 0) {
            return getImpRitTfr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpRitTfrObj(AfDecimal impRitTfrObj) {
        if (impRitTfrObj != null) {
            setImpRitTfr(new AfDecimal(impRitTfrObj, 15, 3));
            ws.getIndTcontLiq().setImpRitTfr(((short)0));
        }
        else {
            ws.getIndTcontLiq().setImpRitTfr(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpRitVis() {
        return tcontLiq.getTclImpRitVis().getTclImpRitVis();
    }

    @Override
    public void setImpRitVis(AfDecimal impRitVis) {
        this.tcontLiq.getTclImpRitVis().setTclImpRitVis(impRitVis.copy());
    }

    @Override
    public AfDecimal getImpRitVisObj() {
        if (ws.getIndTcontLiq().getImpRitVis() >= 0) {
            return getImpRitVis();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpRitVisObj(AfDecimal impRitVisObj) {
        if (impRitVisObj != null) {
            setImpRitVis(new AfDecimal(impRitVisObj, 15, 3));
            ws.getIndTcontLiq().setImpRitVis(((short)0));
        }
        else {
            ws.getIndTcontLiq().setImpRitVis(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpUti() {
        return tcontLiq.getTclImpUti().getTclImpUti();
    }

    @Override
    public void setImpUti(AfDecimal impUti) {
        this.tcontLiq.getTclImpUti().setTclImpUti(impUti.copy());
    }

    @Override
    public AfDecimal getImpUtiObj() {
        if (ws.getIndTcontLiq().getImpUti() >= 0) {
            return getImpUti();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpUtiObj(AfDecimal impUtiObj) {
        if (impUtiObj != null) {
            setImpUti(new AfDecimal(impUtiObj, 15, 3));
            ws.getIndTcontLiq().setImpUti(((short)0));
        }
        else {
            ws.getIndTcontLiq().setImpUti(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbAcc() {
        return tcontLiq.getTclImpbAcc().getTclImpbAcc();
    }

    @Override
    public void setImpbAcc(AfDecimal impbAcc) {
        this.tcontLiq.getTclImpbAcc().setTclImpbAcc(impbAcc.copy());
    }

    @Override
    public AfDecimal getImpbAccObj() {
        if (ws.getIndTcontLiq().getImpbAcc() >= 0) {
            return getImpbAcc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbAccObj(AfDecimal impbAccObj) {
        if (impbAccObj != null) {
            setImpbAcc(new AfDecimal(impbAccObj, 15, 3));
            ws.getIndTcontLiq().setImpbAcc(((short)0));
        }
        else {
            ws.getIndTcontLiq().setImpbAcc(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbImpst252() {
        return tcontLiq.getTclImpbImpst252().getTclImpbImpst252();
    }

    @Override
    public void setImpbImpst252(AfDecimal impbImpst252) {
        this.tcontLiq.getTclImpbImpst252().setTclImpbImpst252(impbImpst252.copy());
    }

    @Override
    public AfDecimal getImpbImpst252Obj() {
        if (ws.getIndTcontLiq().getImpbImpst252() >= 0) {
            return getImpbImpst252();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbImpst252Obj(AfDecimal impbImpst252Obj) {
        if (impbImpst252Obj != null) {
            setImpbImpst252(new AfDecimal(impbImpst252Obj, 15, 3));
            ws.getIndTcontLiq().setImpbImpst252(((short)0));
        }
        else {
            ws.getIndTcontLiq().setImpbImpst252(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbImpstPrvr() {
        return tcontLiq.getTclImpbImpstPrvr().getTclImpbImpstPrvr();
    }

    @Override
    public void setImpbImpstPrvr(AfDecimal impbImpstPrvr) {
        this.tcontLiq.getTclImpbImpstPrvr().setTclImpbImpstPrvr(impbImpstPrvr.copy());
    }

    @Override
    public AfDecimal getImpbImpstPrvrObj() {
        if (ws.getIndTcontLiq().getImpbImpstPrvr() >= 0) {
            return getImpbImpstPrvr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbImpstPrvrObj(AfDecimal impbImpstPrvrObj) {
        if (impbImpstPrvrObj != null) {
            setImpbImpstPrvr(new AfDecimal(impbImpstPrvrObj, 15, 3));
            ws.getIndTcontLiq().setImpbImpstPrvr(((short)0));
        }
        else {
            ws.getIndTcontLiq().setImpbImpstPrvr(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbIs1382011() {
        return tcontLiq.getTclImpbIs1382011().getTclImpbIs1382011();
    }

    @Override
    public void setImpbIs1382011(AfDecimal impbIs1382011) {
        this.tcontLiq.getTclImpbIs1382011().setTclImpbIs1382011(impbIs1382011.copy());
    }

    @Override
    public AfDecimal getImpbIs1382011Obj() {
        if (ws.getIndTcontLiq().getImpbIs1382011() >= 0) {
            return getImpbIs1382011();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbIs1382011Obj(AfDecimal impbIs1382011Obj) {
        if (impbIs1382011Obj != null) {
            setImpbIs1382011(new AfDecimal(impbIs1382011Obj, 15, 3));
            ws.getIndTcontLiq().setImpbIs1382011(((short)0));
        }
        else {
            ws.getIndTcontLiq().setImpbIs1382011(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbIs662014() {
        return tcontLiq.getTclImpbIs662014().getTclImpbIs662014();
    }

    @Override
    public void setImpbIs662014(AfDecimal impbIs662014) {
        this.tcontLiq.getTclImpbIs662014().setTclImpbIs662014(impbIs662014.copy());
    }

    @Override
    public AfDecimal getImpbIs662014Obj() {
        if (ws.getIndTcontLiq().getImpbIs662014() >= 0) {
            return getImpbIs662014();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbIs662014Obj(AfDecimal impbIs662014Obj) {
        if (impbIs662014Obj != null) {
            setImpbIs662014(new AfDecimal(impbIs662014Obj, 15, 3));
            ws.getIndTcontLiq().setImpbIs662014(((short)0));
        }
        else {
            ws.getIndTcontLiq().setImpbIs662014(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbIs() {
        return tcontLiq.getTclImpbIs().getTclImpbIs();
    }

    @Override
    public void setImpbIs(AfDecimal impbIs) {
        this.tcontLiq.getTclImpbIs().setTclImpbIs(impbIs.copy());
    }

    @Override
    public AfDecimal getImpbIsObj() {
        if (ws.getIndTcontLiq().getImpbIs() >= 0) {
            return getImpbIs();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbIsObj(AfDecimal impbIsObj) {
        if (impbIsObj != null) {
            setImpbIs(new AfDecimal(impbIsObj, 15, 3));
            ws.getIndTcontLiq().setImpbIs(((short)0));
        }
        else {
            ws.getIndTcontLiq().setImpbIs(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbTfr() {
        return tcontLiq.getTclImpbTfr().getTclImpbTfr();
    }

    @Override
    public void setImpbTfr(AfDecimal impbTfr) {
        this.tcontLiq.getTclImpbTfr().setTclImpbTfr(impbTfr.copy());
    }

    @Override
    public AfDecimal getImpbTfrObj() {
        if (ws.getIndTcontLiq().getImpbTfr() >= 0) {
            return getImpbTfr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbTfrObj(AfDecimal impbTfrObj) {
        if (impbTfrObj != null) {
            setImpbTfr(new AfDecimal(impbTfrObj, 15, 3));
            ws.getIndTcontLiq().setImpbTfr(((short)0));
        }
        else {
            ws.getIndTcontLiq().setImpbTfr(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbVis1382011() {
        return tcontLiq.getTclImpbVis1382011().getTclImpbVis1382011();
    }

    @Override
    public void setImpbVis1382011(AfDecimal impbVis1382011) {
        this.tcontLiq.getTclImpbVis1382011().setTclImpbVis1382011(impbVis1382011.copy());
    }

    @Override
    public AfDecimal getImpbVis1382011Obj() {
        if (ws.getIndTcontLiq().getImpbVis1382011() >= 0) {
            return getImpbVis1382011();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbVis1382011Obj(AfDecimal impbVis1382011Obj) {
        if (impbVis1382011Obj != null) {
            setImpbVis1382011(new AfDecimal(impbVis1382011Obj, 15, 3));
            ws.getIndTcontLiq().setImpbVis1382011(((short)0));
        }
        else {
            ws.getIndTcontLiq().setImpbVis1382011(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbVis662014() {
        return tcontLiq.getTclImpbVis662014().getTclImpbVis662014();
    }

    @Override
    public void setImpbVis662014(AfDecimal impbVis662014) {
        this.tcontLiq.getTclImpbVis662014().setTclImpbVis662014(impbVis662014.copy());
    }

    @Override
    public AfDecimal getImpbVis662014Obj() {
        if (ws.getIndTcontLiq().getImpbVis662014() >= 0) {
            return getImpbVis662014();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbVis662014Obj(AfDecimal impbVis662014Obj) {
        if (impbVis662014Obj != null) {
            setImpbVis662014(new AfDecimal(impbVis662014Obj, 15, 3));
            ws.getIndTcontLiq().setImpbVis662014(((short)0));
        }
        else {
            ws.getIndTcontLiq().setImpbVis662014(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbVis() {
        return tcontLiq.getTclImpbVis().getTclImpbVis();
    }

    @Override
    public void setImpbVis(AfDecimal impbVis) {
        this.tcontLiq.getTclImpbVis().setTclImpbVis(impbVis.copy());
    }

    @Override
    public AfDecimal getImpbVisObj() {
        if (ws.getIndTcontLiq().getImpbVis() >= 0) {
            return getImpbVis();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbVisObj(AfDecimal impbVisObj) {
        if (impbVisObj != null) {
            setImpbVis(new AfDecimal(impbVisObj, 15, 3));
            ws.getIndTcontLiq().setImpbVis(((short)0));
        }
        else {
            ws.getIndTcontLiq().setImpbVis(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpst252() {
        return tcontLiq.getTclImpst252().getTclImpst252();
    }

    @Override
    public void setImpst252(AfDecimal impst252) {
        this.tcontLiq.getTclImpst252().setTclImpst252(impst252.copy());
    }

    @Override
    public AfDecimal getImpst252Obj() {
        if (ws.getIndTcontLiq().getImpst252() >= 0) {
            return getImpst252();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpst252Obj(AfDecimal impst252Obj) {
        if (impst252Obj != null) {
            setImpst252(new AfDecimal(impst252Obj, 15, 3));
            ws.getIndTcontLiq().setImpst252(((short)0));
        }
        else {
            ws.getIndTcontLiq().setImpst252(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstPrvr() {
        return tcontLiq.getTclImpstPrvr().getTclImpstPrvr();
    }

    @Override
    public void setImpstPrvr(AfDecimal impstPrvr) {
        this.tcontLiq.getTclImpstPrvr().setTclImpstPrvr(impstPrvr.copy());
    }

    @Override
    public AfDecimal getImpstPrvrObj() {
        if (ws.getIndTcontLiq().getImpstPrvr() >= 0) {
            return getImpstPrvr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstPrvrObj(AfDecimal impstPrvrObj) {
        if (impstPrvrObj != null) {
            setImpstPrvr(new AfDecimal(impstPrvrObj, 15, 3));
            ws.getIndTcontLiq().setImpstPrvr(((short)0));
        }
        else {
            ws.getIndTcontLiq().setImpstPrvr(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstSost1382011() {
        return tcontLiq.getTclImpstSost1382011().getTclImpstSost1382011();
    }

    @Override
    public void setImpstSost1382011(AfDecimal impstSost1382011) {
        this.tcontLiq.getTclImpstSost1382011().setTclImpstSost1382011(impstSost1382011.copy());
    }

    @Override
    public AfDecimal getImpstSost1382011Obj() {
        if (ws.getIndTcontLiq().getImpstSost1382011() >= 0) {
            return getImpstSost1382011();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstSost1382011Obj(AfDecimal impstSost1382011Obj) {
        if (impstSost1382011Obj != null) {
            setImpstSost1382011(new AfDecimal(impstSost1382011Obj, 15, 3));
            ws.getIndTcontLiq().setImpstSost1382011(((short)0));
        }
        else {
            ws.getIndTcontLiq().setImpstSost1382011(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstSost662014() {
        return tcontLiq.getTclImpstSost662014().getTclImpstSost662014();
    }

    @Override
    public void setImpstSost662014(AfDecimal impstSost662014) {
        this.tcontLiq.getTclImpstSost662014().setTclImpstSost662014(impstSost662014.copy());
    }

    @Override
    public AfDecimal getImpstSost662014Obj() {
        if (ws.getIndTcontLiq().getImpstSost662014() >= 0) {
            return getImpstSost662014();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstSost662014Obj(AfDecimal impstSost662014Obj) {
        if (impstSost662014Obj != null) {
            setImpstSost662014(new AfDecimal(impstSost662014Obj, 15, 3));
            ws.getIndTcontLiq().setImpstSost662014(((short)0));
        }
        else {
            ws.getIndTcontLiq().setImpstSost662014(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstVis1382011() {
        return tcontLiq.getTclImpstVis1382011().getTclImpstVis1382011();
    }

    @Override
    public void setImpstVis1382011(AfDecimal impstVis1382011) {
        this.tcontLiq.getTclImpstVis1382011().setTclImpstVis1382011(impstVis1382011.copy());
    }

    @Override
    public AfDecimal getImpstVis1382011Obj() {
        if (ws.getIndTcontLiq().getImpstVis1382011() >= 0) {
            return getImpstVis1382011();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstVis1382011Obj(AfDecimal impstVis1382011Obj) {
        if (impstVis1382011Obj != null) {
            setImpstVis1382011(new AfDecimal(impstVis1382011Obj, 15, 3));
            ws.getIndTcontLiq().setImpstVis1382011(((short)0));
        }
        else {
            ws.getIndTcontLiq().setImpstVis1382011(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstVis662014() {
        return tcontLiq.getTclImpstVis662014().getTclImpstVis662014();
    }

    @Override
    public void setImpstVis662014(AfDecimal impstVis662014) {
        this.tcontLiq.getTclImpstVis662014().setTclImpstVis662014(impstVis662014.copy());
    }

    @Override
    public AfDecimal getImpstVis662014Obj() {
        if (ws.getIndTcontLiq().getImpstVis662014() >= 0) {
            return getImpstVis662014();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstVis662014Obj(AfDecimal impstVis662014Obj) {
        if (impstVis662014Obj != null) {
            setImpstVis662014(new AfDecimal(impstVis662014Obj, 15, 3));
            ws.getIndTcontLiq().setImpstVis662014(((short)0));
        }
        else {
            ws.getIndTcontLiq().setImpstVis662014(((short)-1));
        }
    }

    @Override
    public long getTclDsRiga() {
        return tcontLiq.getTclDsRiga();
    }

    @Override
    public void setTclDsRiga(long tclDsRiga) {
        this.tcontLiq.setTclDsRiga(tclDsRiga);
    }

    @Override
    public String getTpMezPagAccr() {
        return tcontLiq.getTclTpMezPagAccr();
    }

    @Override
    public void setTpMezPagAccr(String tpMezPagAccr) {
        this.tcontLiq.setTclTpMezPagAccr(tpMezPagAccr);
    }

    @Override
    public String getTpMezPagAccrObj() {
        if (ws.getIndTcontLiq().getTpMezPagAccr() >= 0) {
            return getTpMezPagAccr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpMezPagAccrObj(String tpMezPagAccrObj) {
        if (tpMezPagAccrObj != null) {
            setTpMezPagAccr(tpMezPagAccrObj);
            ws.getIndTcontLiq().setTpMezPagAccr(((short)0));
        }
        else {
            ws.getIndTcontLiq().setTpMezPagAccr(((short)-1));
        }
    }

    @Override
    public String getTpStatTit() {
        return tcontLiq.getTclTpStatTit();
    }

    @Override
    public void setTpStatTit(String tpStatTit) {
        this.tcontLiq.setTclTpStatTit(tpStatTit);
    }

    @Override
    public String getTpStatTitObj() {
        if (ws.getIndTcontLiq().getTpStatTit() >= 0) {
            return getTpStatTit();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpStatTitObj(String tpStatTitObj) {
        if (tpStatTitObj != null) {
            setTpStatTit(tpStatTitObj);
            ws.getIndTcontLiq().setTpStatTit(((short)0));
        }
        else {
            ws.getIndTcontLiq().setTpStatTit(((short)-1));
        }
    }
}
