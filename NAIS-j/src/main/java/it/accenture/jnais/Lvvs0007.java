package it.accenture.jnais;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Idsv0003CampiEsito;
import it.accenture.jnais.ws.enums.Idsv0003Sqlcode;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.Lvvs0007Data;
import it.accenture.jnais.ws.TrchDiGar;

/**Original name: LVVS0007<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2007.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *   PROGRAMMA...... LVVS0007
 *   TIPOLOGIA...... SERVIZIO
 *   PROCESSO....... XXX
 *   FUNZIONE....... XXX
 *   DESCRIZIONE.... CONVERSIONE DATA DECORRENZA POLIZZA
 * **------------------------------------------------------------***</pre>*/
public class Lvvs0007 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lvvs0007Data ws = new Lvvs0007Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS0007
    private Ivvc0213 ivvc0213;

    //==== METHODS ====
    /**Original name: PROGRAM_LVVS0007_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: PERFORM S1000-ELABORAZIONE
        //              THRU EX-S1000
        s1000Elaborazione();
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lvvs0007 getInstance() {
        return ((Lvvs0007)Programs.getInstance(Lvvs0007.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                        IX-INDICI
        //                                             IVVC0213-TAB-OUTPUT.
        initIxIndici();
        initTabOutput();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: INITIALIZE LDBV1591.
        initLdbv1591();
        // COB_CODE: INITIALIZE LDBV3021.
        initLdbv3021();
        // COB_CODE: INITIALIZE WS-PRE-SOLO-RSH.
        ws.setWsPreSoloRsh(new AfDecimal(0, 15, 3));
        // COB_CODE: PERFORM S00010-INIZIA-TABELLE
        //              THRU S00010-INIZIA-TABELLE-EX.
        s00010IniziaTabelle();
        //
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE
        //             TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
    }

    /**Original name: S00010-INIZIA-TABELLE<br>
	 * <pre>----------------------------------------------------------------*
	 *  INIZIALIZZAZIONE TABELLE
	 * ----------------------------------------------------------------*
	 *  -->TRANCHE DI GARANZIA</pre>*/
    private void s00010IniziaTabelle() {
        // COB_CODE: PERFORM INIZIA-TOT-TGA
        //              THRU INIZIA-TOT-TGA-EX
        //           VARYING IX-TAB-TGA FROM 1 BY 1
        //             UNTIL IX-TAB-TGA > WK-TGA-MAX-C.
        ws.setIxTabTga(((short)1));
        while (!(ws.getIxTabTga() > ws.getWkTgaMax().getC())) {
            iniziaTotTga();
            ws.setIxTabTga(Trunc.toShort(ws.getIxTabTga() + 1, 4));
        }
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: INITIALIZE WK-DATA-OUTPUT
        //                      WK-DATA-X-12.
        ws.setWkDataOutput(new AfDecimal(0, 11, 7));
        initWkDataX12();
        //--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
        //--> RISPETTIVE AREE DCLGEN IN WORKING
        // COB_CODE: PERFORM S1100-VALORIZZA-DCLGEN
        //              THRU S1100-VALORIZZA-DCLGEN-EX
        //           VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
        //                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //                   SPACES OR LOW-VALUE OR HIGH-VALUE.
        ws.setIxDclgen(((short)1));
        while (!(ws.getIxDclgen() > ivvc0213.getEleInfoMax() || Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias()) || Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()) || Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()))) {
            s1100ValorizzaDclgen();
            ws.setIxDclgen(Trunc.toShort(ws.getIxDclgen() + 1, 4));
        }
        // COB_CODE:      IF  IDSV0003-SUCCESSFUL-RC
        //                AND IDSV0003-SUCCESSFUL-SQL
        //           *  INSERITA NUOVA GESTIONE A LIVELLO DI ADESIONE PER LE POLIZZE
        //           *  COLLETTIVE PARTENDO DALLE SINGOLE TRANCHE DI GARANZIA. PER LE
        //           *  INDIVIDUALI LA GESTIONE RESTA INALTERATA A LIVELLO DI POLIZZA.
        //                 END-IF
        //                END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            //  INSERITA NUOVA GESTIONE A LIVELLO DI ADESIONE PER LE POLIZZE
            //  COLLETTIVE PARTENDO DALLE SINGOLE TRANCHE DI GARANZIA. PER LE
            //  INDIVIDUALI LA GESTIONE RESTA INALTERATA A LIVELLO DI POLIZZA.
            // COB_CODE: IF DPOL-TP-FRM-ASSVA = 'IN'
            //            END-EVALUATE
            //           ELSE
            //            PERFORM S1300-ELABORA-COLL              THRU S1300-EX
            //           END-IF
            if (Conditions.eq(ws.getLccvpol1().getDati().getWpolTpFrmAssva(), "IN")) {
                // COB_CODE: EVALUATE DPOL-TP-LIV-GENZ-TIT
                //             WHEN 'PO'
                //               PERFORM S1250-CALCOLA-DATA1         THRU S1250-EX
                //             WHEN 'AD'
                //               PERFORM S1250-CALCOLA-DATA1         THRU S1250-EX
                //           END-EVALUATE
                switch (ws.getLccvpol1().getDati().getWpolTpLivGenzTit()) {

                    case "PO":// COB_CODE: MOVE IVVC0213-ID-ADESIONE       TO LDBV1591-ID-ADES
                        ws.getLdbv1591().setIdAdes(ivvc0213.getIdAdesione());
                        // COB_CODE: MOVE IVVC0213-ID-POLIZZA        TO LDBV1591-ID-OGG
                        ws.getLdbv1591().setIdOgg(ivvc0213.getIdPolizza());
                        // COB_CODE: MOVE DPOL-DT-DECOR              TO LDBV1591-DT-DECOR
                        ws.getLdbv1591().setDtDecor(ws.getLccvpol1().getDati().getWpolDtDecor());
                        // COB_CODE: MOVE 'PO'                       TO LDBV1591-TP-OGG
                        ws.getLdbv1591().setTpOgg("PO");
                        // COB_CODE: MOVE 'IN'                       TO LDBV1591-TP-STAT-TIT
                        ws.getLdbv1591().setTpStatTit("IN");
                        // COB_CODE: PERFORM S1250-CALCOLA-DATA1         THRU S1250-EX
                        s1250CalcolaData1();
                        break;

                    case "AD":// COB_CODE: MOVE IVVC0213-ID-ADESIONE       TO LDBV1591-ID-ADES
                        ws.getLdbv1591().setIdAdes(ivvc0213.getIdAdesione());
                        // COB_CODE: MOVE IVVC0213-ID-ADESIONE       TO LDBV1591-ID-OGG
                        ws.getLdbv1591().setIdOgg(ivvc0213.getIdAdesione());
                        // COB_CODE: MOVE DPOL-DT-DECOR              TO LDBV1591-DT-DECOR
                        ws.getLdbv1591().setDtDecor(ws.getLccvpol1().getDati().getWpolDtDecor());
                        // COB_CODE: MOVE 'AD'                       TO LDBV1591-TP-OGG
                        ws.getLdbv1591().setTpOgg("AD");
                        // COB_CODE: MOVE 'IN'                       TO LDBV1591-TP-STAT-TIT
                        ws.getLdbv1591().setTpStatTit("IN");
                        // COB_CODE: PERFORM S1250-CALCOLA-DATA1         THRU S1250-EX
                        s1250CalcolaData1();
                        break;

                    default:break;
                }
            }
            else {
                // COB_CODE: PERFORM S1300-ELABORA-COLL              THRU S1300-EX
                s1300ElaboraColl();
            }
        }
    }

    /**Original name: S1100-VALORIZZA-DCLGEN<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 *     RISPETTIVE AREE DCLGEN IN WORKING
	 * ----------------------------------------------------------------*</pre>*/
    private void s1100ValorizzaDclgen() {
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-POLI
        //                TO DPOL-AREA-POLIZZA
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias(), ws.getIvvc0218().getAliasPoli())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO DPOL-AREA-POLIZZA
            ws.setDpolAreaPolizzaFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxDclgen()).getLunghezza() - 1));
        }
    }

    /**Original name: S1250-CALCOLA-DATA1<br>
	 * <pre>----------------------------------------------------------------*
	 *   CALCOLA DATA 1
	 * ----------------------------------------------------------------*</pre>*/
    private void s1250CalcolaData1() {
        Ldbs1590 ldbs1590 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'LDBS1590'                     TO WK-CALL-PGM.
        ws.setWkCallPgm("LDBS1590");
        //
        //
        // COB_CODE:      CALL WK-CALL-PGM  USING  IDSV0003 LDBV1591
        //           *
        //                ON EXCEPTION
        //                     SET IDSV0003-INVALID-OPER  TO TRUE
        //                END-CALL.
        try {
            ldbs1590 = Ldbs1590.getInstance();
            ldbs1590.run(idsv0003, ws.getLdbv1591());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-CALL-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: MOVE 'CALL-LDBS1590 ERRORE CHIAMATA - T0000-TRATTA-MATRICE'
            //              TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBS1590 ERRORE CHIAMATA - T0000-TRATTA-MATRICE");
            // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE:      IF IDSV0003-SUCCESSFUL-SQL
        //           *     IF IVVC0213-ID-POLIZZA = 2071263
        //           *       MOVE ZERO                    TO IVVC0213-VAL-IMP-O
        //           *     ELSE
        //                   MOVE LDBV1591-IMP-TOT        TO IVVC0213-VAL-IMP-O
        //           *     END-IF
        //                ELSE
        //                   END-IF
        //                END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            //     IF IVVC0213-ID-POLIZZA = 2071263
            //       MOVE ZERO                    TO IVVC0213-VAL-IMP-O
            //     ELSE
            // COB_CODE: MOVE LDBV1591-IMP-TOT        TO IVVC0213-VAL-IMP-O
            ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(ws.getLdbv1591().getImpTot(), 18, 7));
            //     END-IF
        }
        else {
            // COB_CODE: MOVE WK-CALL-PGM             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: STRING 'CHIAMATA LDBS1590 ;'
            //                  IDSV0003-RETURN-CODE ';'
            //                  IDSV0003-SQLCODE
            //              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LDBS1590 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
            //           ELSE
            //              SET IDSV0003-INVALID-OPER            TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isNotFound()) {
                // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-OPER            TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
        }
    }

    /**Original name: S1300-ELABORA-COLL<br>
	 * <pre>----------------------------------------------------------------*
	 *   GESTIONE CALCOLO PER COLLETTIVE : LEGGI A LIVELLO DI ADESIONE
	 *   LE TRANCHE IN VIGORE E PER OGNUNO CALCOLA LA SOMMA DEL PREMIO
	 *   DI RISCHIO PER TUTTI I DETTAGLI TITOLI
	 * ----------------------------------------------------------------*
	 *  --> RECUPERA TRANCHE</pre>*/
    private void s1300ElaboraColl() {
        // COB_CODE: PERFORM S1310-LEGGI-TRCH      THRU S1310-EX
        s1310LeggiTrch();
        // --> CALCOLA VALORE DA DETTAGLIO TITOLI
        // COB_CODE: PERFORM VARYING IX-TAB-TGA FROM 1 BY 1
        //              UNTIL IX-TAB-TGA > DTGA-ELE-TRAN-MAX
        //              OR NOT IDSV0003-SUCCESSFUL-RC
        //                    THRU S1320-EX
        //           END-PERFORM
        ws.setIxTabTga(((short)1));
        while (!(ws.getIxTabTga() > ws.getDtgaEleTranMax() || !idsv0003.getReturnCode().isSuccessfulRc())) {
            // COB_CODE: PERFORM S1320-LEGGI-DTC
            //              THRU S1320-EX
            s1320LeggiDtc();
            ws.setIxTabTga(Trunc.toShort(ws.getIxTabTga() + 1, 4));
        }
        // COB_CODE:      IF IDSV0003-SUCCESSFUL-RC
        //           * --> PASSA VALORE CALCOLATO NELLA VARIABILE DI RIFERIMENTO
        //                  MOVE WS-PRE-SOLO-RSH    TO IVVC0213-VAL-IMP-O
        //                ELSE
        //                  INITIALIZE                 IVVC0213-VAL-IMP-O
        //                END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // --> PASSA VALORE CALCOLATO NELLA VARIABILE DI RIFERIMENTO
            // COB_CODE: MOVE WS-PRE-SOLO-RSH    TO IVVC0213-VAL-IMP-O
            ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(ws.getWsPreSoloRsh(), 18, 7));
        }
        else {
            // COB_CODE: INITIALIZE                 IVVC0213-VAL-IMP-O
            ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        }
        // COB_CODE: MOVE WS-PRE-SOLO-RSH       TO WS-SOMMA-DIS.
        ws.setWsSommaDis(TruncAbs.toLong(ws.getWsPreSoloRsh(), 15));
    }

    /**Original name: S1310-LEGGI-TRCH<br>
	 * <pre>----------------------------------------------------------------*
	 *   LETTURA TRANCHE DI GARANZIA
	 * ----------------------------------------------------------------*</pre>*/
    private void s1310LeggiTrch() {
        Ldbs3020 ldbs3020 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE LDBV3021.
        initLdbv3021();
        // COB_CODE: INITIALIZE WS-PRE-SOLO-RSH.
        ws.setWsPreSoloRsh(new AfDecimal(0, 15, 3));
        // COB_CODE: MOVE 0  TO IX-TAB-TGA
        //                      DTGA-ELE-TRAN-MAX.
        ws.setIxTabTga(((short)0));
        ws.setDtgaEleTranMax(((short)0));
        // COB_CODE: MOVE IVVC0213-ID-ADESIONE     TO LDBV3021-ID-OGG.
        ws.getLdbv3021().setIdOgg(ivvc0213.getIdAdesione());
        // COB_CODE: SET TRANCHE                   TO TRUE.
        ws.getWsTpOgg().setTranche();
        // COB_CODE: MOVE WS-TP-OGG                TO LDBV3021-TP-OGG.
        ws.getLdbv3021().setTpOgg(ws.getWsTpOgg().getWsTpOgg());
        // COB_CODE: SET IN-VIGORE                 TO TRUE.
        ws.getWsTpStatBus().setInVigore();
        // COB_CODE: MOVE WS-TP-STAT-BUS           TO LDBV3021-TP-STAT-BUS.
        ws.getLdbv3021().setTpStatBus(ws.getWsTpStatBus().getWsTpStatBus());
        // COB_CODE: SET ATTESA-PERFEZIONAMENTO    TO TRUE.
        ws.getWsTpCaus().setAttesaPerfezionamento();
        // COB_CODE: MOVE WS-TP-CAUS               TO LDBV3021-TP-CAUS-BUS1.
        ws.getLdbv3021().setTpCausBus1(ws.getWsTpCaus().getWsTpCaus());
        // COB_CODE: MOVE LDBV3021                 TO IDSV0003-BUFFER-WHERE-COND.
        idsv0003.setBufferWhereCond(ws.getLdbv3021().getLdbv3021Formatted());
        //--> TIPO OPERAZIONE
        // COB_CODE: SET IDSV0003-FETCH-FIRST         TO TRUE.
        idsv0003.getOperazione().setFetchFirst();
        // COB_CODE: SET IDSV0003-WHERE-CONDITION     TO TRUE
        idsv0003.getLivelloOperazione().setWhereCondition();
        // COB_CODE: SET IDSV0003-TRATT-X-COMPETENZA  TO TRUE
        idsv0003.getTrattamentoStoricita().setTrattXCompetenza();
        //--> INIZIALIZZA CODICE DI RITORNO
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC     TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL    TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: MOVE 'LDBS3020'                TO WK-CALL-PGM.
        ws.setWkCallPgm("LDBS3020");
        //--> LIVELLO OPERAZIONE
        // COB_CODE: PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC
        //                      OR NOT IDSV0003-SUCCESSFUL-SQL
        //              END-IF
        //           END-PERFORM.
        while (!(!idsv0003.getReturnCode().isSuccessfulRc() || !idsv0003.getSqlcode().isSuccessfulSql())) {
            // COB_CODE: CALL WK-CALL-PGM  USING  IDSV0003 TRCH-DI-GAR
            //           ON EXCEPTION
            //              SET IDSV0003-INVALID-OPER TO TRUE
            //           END-CALL
            try {
                ldbs3020 = Ldbs3020.getInstance();
                ldbs3020.run(idsv0003, ws.getTrchDiGar());
            }
            catch (ProgramExecutionException __ex) {
                // COB_CODE: MOVE WK-CALL-PGM
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
                // COB_CODE: MOVE 'ERRORE CALL LDBS3020 FETCH'
                //             TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("ERRORE CALL LDBS3020 FETCH");
                // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
            //
            // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
            //              END-EVALUATE
            //           END-IF
            if (idsv0003.getReturnCode().isSuccessfulRc()) {
                // COB_CODE:            EVALUATE TRUE
                //                          WHEN IDSV0003-NOT-FOUND
                //           *-->    NESSUN DATO IN TABELLA
                //                               CONTINUE
                //                          WHEN IDSV0003-SUCCESSFUL-SQL
                //           *-->    OPERAZIONE ESEGUITA CORRETTAMENTE
                //                               MOVE DTGA-ELE-TRAN-MAX TO WS-IX-TGA-MAX
                //                          WHEN OTHER
                //           *--->   ERRORE DI ACCESSO AL DB
                //                               END-STRING
                //                      END-EVALUATE
                switch (idsv0003.getSqlcode().getSqlcode()) {

                    case Idsv0003Sqlcode.NOT_FOUND://-->    NESSUN DATO IN TABELLA
                    // COB_CODE: CONTINUE
                    //continue
                        break;

                    case Idsv0003Sqlcode.SUCCESSFUL_SQL://-->    OPERAZIONE ESEGUITA CORRETTAMENTE
                        // COB_CODE: ADD 1       TO IX-TAB-TGA
                        ws.setIxTabTga(Trunc.toShort(1 + ws.getIxTabTga(), 4));
                        // COB_CODE: PERFORM VALORIZZA-OUTPUT-TGA
                        //              THRU VALORIZZA-OUTPUT-TGA-EX
                        valorizzaOutputTga();
                        // COB_CODE: MOVE IX-TAB-TGA
                        //             TO DTGA-ELE-TRAN-MAX
                        ws.setDtgaEleTranMax(ws.getIxTabTga());
                        // COB_CODE: SET IDSV0003-FETCH-NEXT TO TRUE
                        idsv0003.getOperazione().setFetchNext();
                        // COB_CODE: MOVE IX-TAB-TGA  TO WS-IX-TGA
                        ws.setWsIxTga(TruncAbs.toInt(ws.getIxTabTga(), 9));
                        // COB_CODE: MOVE DTGA-ELE-TRAN-MAX TO WS-IX-TGA-MAX
                        ws.setWsIxTgaMax(TruncAbs.toInt(ws.getDtgaEleTranMax(), 9));
                        break;

                    default://--->   ERRORE DI ACCESSO AL DB
                        // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
                        idsv0003.getReturnCode().setInvalidOper();
                        // COB_CODE: MOVE WK-PGM
                        //             TO IDSV0003-COD-SERVIZIO-BE
                        idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                        // COB_CODE: STRING 'ERRORE LETTURA TABELLA TRCH/STB ;'
                        //                  IDSV0003-RETURN-CODE ';'
                        //                  IDSV0003-SQLCODE
                        //                  DELIMITED BY SIZE INTO
                        //                  IDSV0003-DESCRIZ-ERR-DB2
                        //           END-STRING
                        concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "ERRORE LETTURA TABELLA TRCH/STB ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                        idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                        break;
                }
            }
        }
    }

    /**Original name: S1320-LEGGI-DTC<br>
	 * <pre>----------------------------------------------------------------*
	 *    RECUPERA DETTAGLIO TITOLI
	 * ----------------------------------------------------------------*</pre>*/
    private void s1320LeggiDtc() {
        Idbsdtc0 idbsdtc0 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE DETT-TIT-CONT.
        initDettTitCont();
        // COB_CODE: MOVE DTGA-ID-TRCH-DI-GAR(IX-TAB-TGA) TO DTC-ID-OGG.
        ws.getDettTitCont().setDtcIdOgg(ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaIdTrchDiGar());
        // COB_CODE: SET TRANCHE                 TO TRUE.
        ws.getWsTpOgg().setTranche();
        // COB_CODE: MOVE WS-TP-OGG              TO DTC-TP-OGG.
        ws.getDettTitCont().setDtcTpOgg(ws.getWsTpOgg().getWsTpOgg());
        //--> TIPO OPERAZIONE
        // COB_CODE: SET IDSV0003-FETCH-FIRST         TO TRUE.
        idsv0003.getOperazione().setFetchFirst();
        // COB_CODE: SET IDSV0003-ID-OGGETTO          TO TRUE.
        idsv0003.getLivelloOperazione().setIdsi0011IdOggetto();
        // COB_CODE: SET IDSV0003-TRATT-X-COMPETENZA  TO TRUE
        idsv0003.getTrattamentoStoricita().setTrattXCompetenza();
        //--> INIZIALIZZA CODICE DI RITORNO
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC     TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL    TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: MOVE 'IDBSDTC0'                TO WK-CALL-PGM.
        ws.setWkCallPgm("IDBSDTC0");
        //--> LIVELLO OPERAZIONE
        // COB_CODE: PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC
        //                      OR NOT IDSV0003-SUCCESSFUL-SQL
        //              END-IF
        //           END-PERFORM.
        while (!(!idsv0003.getReturnCode().isSuccessfulRc() || !idsv0003.getSqlcode().isSuccessfulSql())) {
            // COB_CODE: CALL WK-CALL-PGM  USING  IDSV0003 DETT-TIT-CONT
            //           ON EXCEPTION
            //              SET IDSV0003-INVALID-OPER TO TRUE
            //           END-CALL
            try {
                idbsdtc0 = Idbsdtc0.getInstance();
                idbsdtc0.run(idsv0003, ws.getDettTitCont());
            }
            catch (ProgramExecutionException __ex) {
                // COB_CODE: MOVE WK-CALL-PGM
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
                // COB_CODE: MOVE 'ERRORE CALL IDBSDTC0 FETCH'
                //             TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("ERRORE CALL IDBSDTC0 FETCH");
                // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
            //
            // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
            //              END-EVALUATE
            //           END-IF
            if (idsv0003.getReturnCode().isSuccessfulRc()) {
                // COB_CODE:            EVALUATE TRUE
                //                          WHEN IDSV0003-NOT-FOUND
                //           *-->    NESSUN DATO IN TABELLA
                //                               CONTINUE
                //                          WHEN IDSV0003-SUCCESSFUL-SQL
                //           *-->    OPERAZIONE ESEGUITA CORRETTAMENTE
                //                               SET IDSV0003-FETCH-NEXT TO TRUE
                //                          WHEN OTHER
                //           *--->   ERRORE DI ACCESSO AL DB
                //                               END-STRING
                //                      END-EVALUATE
                switch (idsv0003.getSqlcode().getSqlcode()) {

                    case Idsv0003Sqlcode.NOT_FOUND://-->    NESSUN DATO IN TABELLA
                    // COB_CODE: CONTINUE
                    //continue
                        break;

                    case Idsv0003Sqlcode.SUCCESSFUL_SQL://-->    OPERAZIONE ESEGUITA CORRETTAMENTE
                        // COB_CODE: IF DTC-PRE-SOLO-RSH-NULL NOT = HIGH-VALUES
                        //            MOVE WS-PRE-SOLO-RSH TO WS-SOMMA-DIS
                        //           END-IF
                        if (!Characters.EQ_HIGH.test(ws.getDettTitCont().getDtcPreSoloRsh().getDtcPreSoloRshNullFormatted())) {
                            // COB_CODE: ADD DTC-PRE-SOLO-RSH
                            //            TO WS-PRE-SOLO-RSH
                            ws.setWsPreSoloRsh(Trunc.toDecimal(ws.getDettTitCont().getDtcPreSoloRsh().getDtcPreSoloRsh().add(ws.getWsPreSoloRsh()), 15, 3));
                            // COB_CODE: MOVE WS-PRE-SOLO-RSH TO WS-SOMMA-DIS
                            ws.setWsSommaDis(TruncAbs.toLong(ws.getWsPreSoloRsh(), 15));
                        }
                        // COB_CODE: SET IDSV0003-FETCH-NEXT TO TRUE
                        idsv0003.getOperazione().setFetchNext();
                        break;

                    default://--->   ERRORE DI ACCESSO AL DB
                        // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
                        idsv0003.getReturnCode().setInvalidOper();
                        // COB_CODE: MOVE WK-PGM
                        //             TO IDSV0003-COD-SERVIZIO-BE
                        idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                        // COB_CODE: STRING 'ERRORE LETTURA TABE DETT_TIT_CONT ;'
                        //                  IDSV0003-RETURN-CODE ';'
                        //                  IDSV0003-SQLCODE
                        //                  DELIMITED BY SIZE INTO
                        //                  IDSV0003-DESCRIZ-ERR-DB2
                        //           END-STRING
                        concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "ERRORE LETTURA TABE DETT_TIT_CONT ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                        idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                        break;
                }
            }
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    /**Original name: VALORIZZA-OUTPUT-TGA<br>
	 * <pre>----------------------------------------------------------------*
	 *     COPY DI VALORIZZAZIONE AREE DI OUTPUT
	 * ----------------------------------------------------------------*
	 *  --> TRANCHE DI GARANZIA
	 * ------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    VALORIZZA OUTPUT COPY LCCVTGA3
	 *    ULTIMO AGG. 03 GIU 2019
	 * ------------------------------------------------------------</pre>*/
    private void valorizzaOutputTga() {
        // COB_CODE: MOVE TGA-ID-TRCH-DI-GAR
        //             TO (SF)-ID-PTF(IX-TAB-TGA)
        ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().setIdPtf(ws.getTrchDiGar().getTgaIdTrchDiGar());
        // COB_CODE: MOVE TGA-ID-TRCH-DI-GAR
        //             TO (SF)-ID-TRCH-DI-GAR(IX-TAB-TGA)
        ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().setWtgaIdTrchDiGar(ws.getTrchDiGar().getTgaIdTrchDiGar());
        // COB_CODE: MOVE TGA-ID-GAR
        //             TO (SF)-ID-GAR(IX-TAB-TGA)
        ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().setWtgaIdGar(ws.getTrchDiGar().getTgaIdGar());
        // COB_CODE: MOVE TGA-ID-ADES
        //             TO (SF)-ID-ADES(IX-TAB-TGA)
        ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().setWtgaIdAdes(ws.getTrchDiGar().getTgaIdAdes());
        // COB_CODE: MOVE TGA-ID-POLI
        //             TO (SF)-ID-POLI(IX-TAB-TGA)
        ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().setWtgaIdPoli(ws.getTrchDiGar().getTgaIdPoli());
        // COB_CODE: MOVE TGA-ID-MOVI-CRZ
        //             TO (SF)-ID-MOVI-CRZ(IX-TAB-TGA)
        ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().setWtgaIdMoviCrz(ws.getTrchDiGar().getTgaIdMoviCrz());
        // COB_CODE: IF TGA-ID-MOVI-CHIU-NULL = HIGH-VALUES
        //                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ID-MOVI-CHIU(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaIdMoviChiu().getTgaIdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE TGA-ID-MOVI-CHIU-NULL
            //             TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaIdMoviChiu().setWtgaIdMoviChiuNull(ws.getTrchDiGar().getTgaIdMoviChiu().getTgaIdMoviChiuNull());
        }
        else {
            // COB_CODE: MOVE TGA-ID-MOVI-CHIU
            //             TO (SF)-ID-MOVI-CHIU(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaIdMoviChiu().setWtgaIdMoviChiu(ws.getTrchDiGar().getTgaIdMoviChiu().getTgaIdMoviChiu());
        }
        // COB_CODE: MOVE TGA-DT-INI-EFF
        //             TO (SF)-DT-INI-EFF(IX-TAB-TGA)
        ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().setWtgaDtIniEff(ws.getTrchDiGar().getTgaDtIniEff());
        // COB_CODE: MOVE TGA-DT-END-EFF
        //             TO (SF)-DT-END-EFF(IX-TAB-TGA)
        ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().setWtgaDtEndEff(ws.getTrchDiGar().getTgaDtEndEff());
        // COB_CODE: MOVE TGA-COD-COMP-ANIA
        //             TO (SF)-COD-COMP-ANIA(IX-TAB-TGA)
        ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().setWtgaCodCompAnia(ws.getTrchDiGar().getTgaCodCompAnia());
        // COB_CODE: MOVE TGA-DT-DECOR
        //             TO (SF)-DT-DECOR(IX-TAB-TGA)
        ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().setWtgaDtDecor(ws.getTrchDiGar().getTgaDtDecor());
        // COB_CODE: IF TGA-DT-SCAD-NULL = HIGH-VALUES
        //                TO (SF)-DT-SCAD-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-DT-SCAD(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaDtScad().getTgaDtScadNullFormatted())) {
            // COB_CODE: MOVE TGA-DT-SCAD-NULL
            //             TO (SF)-DT-SCAD-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaDtScad().setWtgaDtScadNull(ws.getTrchDiGar().getTgaDtScad().getTgaDtScadNull());
        }
        else {
            // COB_CODE: MOVE TGA-DT-SCAD
            //             TO (SF)-DT-SCAD(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaDtScad().setWtgaDtScad(ws.getTrchDiGar().getTgaDtScad().getTgaDtScad());
        }
        // COB_CODE: IF TGA-IB-OGG-NULL = HIGH-VALUES
        //                TO (SF)-IB-OGG-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IB-OGG(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaIbOgg(), TrchDiGar.Len.TGA_IB_OGG)) {
            // COB_CODE: MOVE TGA-IB-OGG-NULL
            //             TO (SF)-IB-OGG-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().setWtgaIbOgg(ws.getTrchDiGar().getTgaIbOgg());
        }
        else {
            // COB_CODE: MOVE TGA-IB-OGG
            //             TO (SF)-IB-OGG(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().setWtgaIbOgg(ws.getTrchDiGar().getTgaIbOgg());
        }
        // COB_CODE: MOVE TGA-TP-RGM-FISC
        //             TO (SF)-TP-RGM-FISC(IX-TAB-TGA)
        ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().setWtgaTpRgmFisc(ws.getTrchDiGar().getTgaTpRgmFisc());
        // COB_CODE: IF TGA-DT-EMIS-NULL = HIGH-VALUES
        //                TO (SF)-DT-EMIS-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-DT-EMIS(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaDtEmis().getTgaDtEmisNullFormatted())) {
            // COB_CODE: MOVE TGA-DT-EMIS-NULL
            //             TO (SF)-DT-EMIS-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaDtEmis().setWtgaDtEmisNull(ws.getTrchDiGar().getTgaDtEmis().getTgaDtEmisNull());
        }
        else {
            // COB_CODE: MOVE TGA-DT-EMIS
            //             TO (SF)-DT-EMIS(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaDtEmis().setWtgaDtEmis(ws.getTrchDiGar().getTgaDtEmis().getTgaDtEmis());
        }
        // COB_CODE: MOVE TGA-TP-TRCH
        //             TO (SF)-TP-TRCH(IX-TAB-TGA)
        ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().setWtgaTpTrch(ws.getTrchDiGar().getTgaTpTrch());
        // COB_CODE: IF TGA-DUR-AA-NULL = HIGH-VALUES
        //                TO (SF)-DUR-AA-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-DUR-AA(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaDurAa().getTgaDurAaNullFormatted())) {
            // COB_CODE: MOVE TGA-DUR-AA-NULL
            //             TO (SF)-DUR-AA-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaDurAa().setWtgaDurAaNull(ws.getTrchDiGar().getTgaDurAa().getTgaDurAaNull());
        }
        else {
            // COB_CODE: MOVE TGA-DUR-AA
            //             TO (SF)-DUR-AA(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaDurAa().setWtgaDurAa(ws.getTrchDiGar().getTgaDurAa().getTgaDurAa());
        }
        // COB_CODE: IF TGA-DUR-MM-NULL = HIGH-VALUES
        //                TO (SF)-DUR-MM-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-DUR-MM(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaDurMm().getTgaDurMmNullFormatted())) {
            // COB_CODE: MOVE TGA-DUR-MM-NULL
            //             TO (SF)-DUR-MM-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaDurMm().setWtgaDurMmNull(ws.getTrchDiGar().getTgaDurMm().getTgaDurMmNull());
        }
        else {
            // COB_CODE: MOVE TGA-DUR-MM
            //             TO (SF)-DUR-MM(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaDurMm().setWtgaDurMm(ws.getTrchDiGar().getTgaDurMm().getTgaDurMm());
        }
        // COB_CODE: IF TGA-DUR-GG-NULL = HIGH-VALUES
        //                TO (SF)-DUR-GG-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-DUR-GG(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaDurGg().getTgaDurGgNullFormatted())) {
            // COB_CODE: MOVE TGA-DUR-GG-NULL
            //             TO (SF)-DUR-GG-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaDurGg().setWtgaDurGgNull(ws.getTrchDiGar().getTgaDurGg().getTgaDurGgNull());
        }
        else {
            // COB_CODE: MOVE TGA-DUR-GG
            //             TO (SF)-DUR-GG(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaDurGg().setWtgaDurGg(ws.getTrchDiGar().getTgaDurGg().getTgaDurGg());
        }
        // COB_CODE: IF TGA-PRE-CASO-MOR-NULL = HIGH-VALUES
        //                TO (SF)-PRE-CASO-MOR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-CASO-MOR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPreCasoMor().getTgaPreCasoMorNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-CASO-MOR-NULL
            //             TO (SF)-PRE-CASO-MOR-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaPreCasoMor().setWtgaPreCasoMorNull(ws.getTrchDiGar().getTgaPreCasoMor().getTgaPreCasoMorNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-CASO-MOR
            //             TO (SF)-PRE-CASO-MOR(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaPreCasoMor().setWtgaPreCasoMor(Trunc.toDecimal(ws.getTrchDiGar().getTgaPreCasoMor().getTgaPreCasoMor(), 15, 3));
        }
        // COB_CODE: IF TGA-PC-INTR-RIAT-NULL = HIGH-VALUES
        //                TO (SF)-PC-INTR-RIAT-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PC-INTR-RIAT(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPcIntrRiat().getTgaPcIntrRiatNullFormatted())) {
            // COB_CODE: MOVE TGA-PC-INTR-RIAT-NULL
            //             TO (SF)-PC-INTR-RIAT-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaPcIntrRiat().setWtgaPcIntrRiatNull(ws.getTrchDiGar().getTgaPcIntrRiat().getTgaPcIntrRiatNull());
        }
        else {
            // COB_CODE: MOVE TGA-PC-INTR-RIAT
            //             TO (SF)-PC-INTR-RIAT(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaPcIntrRiat().setWtgaPcIntrRiat(Trunc.toDecimal(ws.getTrchDiGar().getTgaPcIntrRiat().getTgaPcIntrRiat(), 6, 3));
        }
        // COB_CODE: IF TGA-IMP-BNS-ANTIC-NULL = HIGH-VALUES
        //                TO (SF)-IMP-BNS-ANTIC-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-BNS-ANTIC(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpBnsAntic().getTgaImpBnsAnticNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-BNS-ANTIC-NULL
            //             TO (SF)-IMP-BNS-ANTIC-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaImpBnsAntic().setWtgaImpBnsAnticNull(ws.getTrchDiGar().getTgaImpBnsAntic().getTgaImpBnsAnticNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-BNS-ANTIC
            //             TO (SF)-IMP-BNS-ANTIC(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaImpBnsAntic().setWtgaImpBnsAntic(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpBnsAntic().getTgaImpBnsAntic(), 15, 3));
        }
        // COB_CODE: IF TGA-PRE-INI-NET-NULL = HIGH-VALUES
        //                TO (SF)-PRE-INI-NET-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-INI-NET(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPreIniNet().getTgaPreIniNetNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-INI-NET-NULL
            //             TO (SF)-PRE-INI-NET-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaPreIniNet().setWtgaPreIniNetNull(ws.getTrchDiGar().getTgaPreIniNet().getTgaPreIniNetNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-INI-NET
            //             TO (SF)-PRE-INI-NET(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaPreIniNet().setWtgaPreIniNet(Trunc.toDecimal(ws.getTrchDiGar().getTgaPreIniNet().getTgaPreIniNet(), 15, 3));
        }
        // COB_CODE: IF TGA-PRE-PP-INI-NULL = HIGH-VALUES
        //                TO (SF)-PRE-PP-INI-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-PP-INI(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPrePpIni().getTgaPrePpIniNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-PP-INI-NULL
            //             TO (SF)-PRE-PP-INI-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaPrePpIni().setWtgaPrePpIniNull(ws.getTrchDiGar().getTgaPrePpIni().getTgaPrePpIniNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-PP-INI
            //             TO (SF)-PRE-PP-INI(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaPrePpIni().setWtgaPrePpIni(Trunc.toDecimal(ws.getTrchDiGar().getTgaPrePpIni().getTgaPrePpIni(), 15, 3));
        }
        // COB_CODE: IF TGA-PRE-PP-ULT-NULL = HIGH-VALUES
        //                TO (SF)-PRE-PP-ULT-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-PP-ULT(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPrePpUlt().getTgaPrePpUltNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-PP-ULT-NULL
            //             TO (SF)-PRE-PP-ULT-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaPrePpUlt().setWtgaPrePpUltNull(ws.getTrchDiGar().getTgaPrePpUlt().getTgaPrePpUltNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-PP-ULT
            //             TO (SF)-PRE-PP-ULT(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaPrePpUlt().setWtgaPrePpUlt(Trunc.toDecimal(ws.getTrchDiGar().getTgaPrePpUlt().getTgaPrePpUlt(), 15, 3));
        }
        // COB_CODE: IF TGA-PRE-TARI-INI-NULL = HIGH-VALUES
        //                TO (SF)-PRE-TARI-INI-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-TARI-INI(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPreTariIni().getTgaPreTariIniNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-TARI-INI-NULL
            //             TO (SF)-PRE-TARI-INI-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaPreTariIni().setWtgaPreTariIniNull(ws.getTrchDiGar().getTgaPreTariIni().getTgaPreTariIniNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-TARI-INI
            //             TO (SF)-PRE-TARI-INI(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaPreTariIni().setWtgaPreTariIni(Trunc.toDecimal(ws.getTrchDiGar().getTgaPreTariIni().getTgaPreTariIni(), 15, 3));
        }
        // COB_CODE: IF TGA-PRE-TARI-ULT-NULL = HIGH-VALUES
        //                TO (SF)-PRE-TARI-ULT-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-TARI-ULT(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPreTariUlt().getTgaPreTariUltNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-TARI-ULT-NULL
            //             TO (SF)-PRE-TARI-ULT-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaPreTariUlt().setWtgaPreTariUltNull(ws.getTrchDiGar().getTgaPreTariUlt().getTgaPreTariUltNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-TARI-ULT
            //             TO (SF)-PRE-TARI-ULT(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaPreTariUlt().setWtgaPreTariUlt(Trunc.toDecimal(ws.getTrchDiGar().getTgaPreTariUlt().getTgaPreTariUlt(), 15, 3));
        }
        // COB_CODE: IF TGA-PRE-INVRIO-INI-NULL = HIGH-VALUES
        //                TO (SF)-PRE-INVRIO-INI-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-INVRIO-INI(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPreInvrioIni().getTgaPreInvrioIniNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-INVRIO-INI-NULL
            //             TO (SF)-PRE-INVRIO-INI-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaPreInvrioIni().setWtgaPreInvrioIniNull(ws.getTrchDiGar().getTgaPreInvrioIni().getTgaPreInvrioIniNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-INVRIO-INI
            //             TO (SF)-PRE-INVRIO-INI(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaPreInvrioIni().setWtgaPreInvrioIni(Trunc.toDecimal(ws.getTrchDiGar().getTgaPreInvrioIni().getTgaPreInvrioIni(), 15, 3));
        }
        // COB_CODE: IF TGA-PRE-INVRIO-ULT-NULL = HIGH-VALUES
        //                TO (SF)-PRE-INVRIO-ULT-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-INVRIO-ULT(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPreInvrioUlt().getTgaPreInvrioUltNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-INVRIO-ULT-NULL
            //             TO (SF)-PRE-INVRIO-ULT-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaPreInvrioUlt().setWtgaPreInvrioUltNull(ws.getTrchDiGar().getTgaPreInvrioUlt().getTgaPreInvrioUltNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-INVRIO-ULT
            //             TO (SF)-PRE-INVRIO-ULT(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaPreInvrioUlt().setWtgaPreInvrioUlt(Trunc.toDecimal(ws.getTrchDiGar().getTgaPreInvrioUlt().getTgaPreInvrioUlt(), 15, 3));
        }
        // COB_CODE: IF TGA-PRE-RIVTO-NULL = HIGH-VALUES
        //                TO (SF)-PRE-RIVTO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-RIVTO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPreRivto().getTgaPreRivtoNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-RIVTO-NULL
            //             TO (SF)-PRE-RIVTO-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaPreRivto().setWtgaPreRivtoNull(ws.getTrchDiGar().getTgaPreRivto().getTgaPreRivtoNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-RIVTO
            //             TO (SF)-PRE-RIVTO(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaPreRivto().setWtgaPreRivto(Trunc.toDecimal(ws.getTrchDiGar().getTgaPreRivto().getTgaPreRivto(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-SOPR-PROF-NULL = HIGH-VALUES
        //                TO (SF)-IMP-SOPR-PROF-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-SOPR-PROF(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpSoprProf().getTgaImpSoprProfNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-SOPR-PROF-NULL
            //             TO (SF)-IMP-SOPR-PROF-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaImpSoprProf().setWtgaImpSoprProfNull(ws.getTrchDiGar().getTgaImpSoprProf().getTgaImpSoprProfNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-SOPR-PROF
            //             TO (SF)-IMP-SOPR-PROF(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaImpSoprProf().setWtgaImpSoprProf(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpSoprProf().getTgaImpSoprProf(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-SOPR-SAN-NULL = HIGH-VALUES
        //                TO (SF)-IMP-SOPR-SAN-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-SOPR-SAN(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpSoprSan().getTgaImpSoprSanNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-SOPR-SAN-NULL
            //             TO (SF)-IMP-SOPR-SAN-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaImpSoprSan().setWtgaImpSoprSanNull(ws.getTrchDiGar().getTgaImpSoprSan().getTgaImpSoprSanNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-SOPR-SAN
            //             TO (SF)-IMP-SOPR-SAN(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaImpSoprSan().setWtgaImpSoprSan(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpSoprSan().getTgaImpSoprSan(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-SOPR-SPO-NULL = HIGH-VALUES
        //                TO (SF)-IMP-SOPR-SPO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-SOPR-SPO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpSoprSpo().getTgaImpSoprSpoNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-SOPR-SPO-NULL
            //             TO (SF)-IMP-SOPR-SPO-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaImpSoprSpo().setWtgaImpSoprSpoNull(ws.getTrchDiGar().getTgaImpSoprSpo().getTgaImpSoprSpoNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-SOPR-SPO
            //             TO (SF)-IMP-SOPR-SPO(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaImpSoprSpo().setWtgaImpSoprSpo(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpSoprSpo().getTgaImpSoprSpo(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-SOPR-TEC-NULL = HIGH-VALUES
        //                TO (SF)-IMP-SOPR-TEC-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-SOPR-TEC(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpSoprTec().getTgaImpSoprTecNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-SOPR-TEC-NULL
            //             TO (SF)-IMP-SOPR-TEC-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaImpSoprTec().setWtgaImpSoprTecNull(ws.getTrchDiGar().getTgaImpSoprTec().getTgaImpSoprTecNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-SOPR-TEC
            //             TO (SF)-IMP-SOPR-TEC(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaImpSoprTec().setWtgaImpSoprTec(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpSoprTec().getTgaImpSoprTec(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-ALT-SOPR-NULL = HIGH-VALUES
        //                TO (SF)-IMP-ALT-SOPR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-ALT-SOPR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpAltSopr().getTgaImpAltSoprNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-ALT-SOPR-NULL
            //             TO (SF)-IMP-ALT-SOPR-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaImpAltSopr().setWtgaImpAltSoprNull(ws.getTrchDiGar().getTgaImpAltSopr().getTgaImpAltSoprNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-ALT-SOPR
            //             TO (SF)-IMP-ALT-SOPR(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaImpAltSopr().setWtgaImpAltSopr(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpAltSopr().getTgaImpAltSopr(), 15, 3));
        }
        // COB_CODE: IF TGA-PRE-STAB-NULL = HIGH-VALUES
        //                TO (SF)-PRE-STAB-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-STAB(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPreStab().getTgaPreStabNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-STAB-NULL
            //             TO (SF)-PRE-STAB-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaPreStab().setWtgaPreStabNull(ws.getTrchDiGar().getTgaPreStab().getTgaPreStabNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-STAB
            //             TO (SF)-PRE-STAB(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaPreStab().setWtgaPreStab(Trunc.toDecimal(ws.getTrchDiGar().getTgaPreStab().getTgaPreStab(), 15, 3));
        }
        // COB_CODE: IF TGA-DT-EFF-STAB-NULL = HIGH-VALUES
        //                TO (SF)-DT-EFF-STAB-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-DT-EFF-STAB(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaDtEffStab().getTgaDtEffStabNullFormatted())) {
            // COB_CODE: MOVE TGA-DT-EFF-STAB-NULL
            //             TO (SF)-DT-EFF-STAB-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaDtEffStab().setWtgaDtEffStabNull(ws.getTrchDiGar().getTgaDtEffStab().getTgaDtEffStabNull());
        }
        else {
            // COB_CODE: MOVE TGA-DT-EFF-STAB
            //             TO (SF)-DT-EFF-STAB(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaDtEffStab().setWtgaDtEffStab(ws.getTrchDiGar().getTgaDtEffStab().getTgaDtEffStab());
        }
        // COB_CODE: IF TGA-TS-RIVAL-FIS-NULL = HIGH-VALUES
        //                TO (SF)-TS-RIVAL-FIS-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-TS-RIVAL-FIS(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaTsRivalFis().getTgaTsRivalFisNullFormatted())) {
            // COB_CODE: MOVE TGA-TS-RIVAL-FIS-NULL
            //             TO (SF)-TS-RIVAL-FIS-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaTsRivalFis().setWtgaTsRivalFisNull(ws.getTrchDiGar().getTgaTsRivalFis().getTgaTsRivalFisNull());
        }
        else {
            // COB_CODE: MOVE TGA-TS-RIVAL-FIS
            //             TO (SF)-TS-RIVAL-FIS(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaTsRivalFis().setWtgaTsRivalFis(Trunc.toDecimal(ws.getTrchDiGar().getTgaTsRivalFis().getTgaTsRivalFis(), 14, 9));
        }
        // COB_CODE: IF TGA-TS-RIVAL-INDICIZ-NULL = HIGH-VALUES
        //                TO (SF)-TS-RIVAL-INDICIZ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-TS-RIVAL-INDICIZ(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaTsRivalIndiciz().getTgaTsRivalIndicizNullFormatted())) {
            // COB_CODE: MOVE TGA-TS-RIVAL-INDICIZ-NULL
            //             TO (SF)-TS-RIVAL-INDICIZ-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaTsRivalIndiciz().setWtgaTsRivalIndicizNull(ws.getTrchDiGar().getTgaTsRivalIndiciz().getTgaTsRivalIndicizNull());
        }
        else {
            // COB_CODE: MOVE TGA-TS-RIVAL-INDICIZ
            //             TO (SF)-TS-RIVAL-INDICIZ(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaTsRivalIndiciz().setWtgaTsRivalIndiciz(Trunc.toDecimal(ws.getTrchDiGar().getTgaTsRivalIndiciz().getTgaTsRivalIndiciz(), 14, 9));
        }
        // COB_CODE: IF TGA-OLD-TS-TEC-NULL = HIGH-VALUES
        //                TO (SF)-OLD-TS-TEC-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-OLD-TS-TEC(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaOldTsTec().getTgaOldTsTecNullFormatted())) {
            // COB_CODE: MOVE TGA-OLD-TS-TEC-NULL
            //             TO (SF)-OLD-TS-TEC-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaOldTsTec().setWtgaOldTsTecNull(ws.getTrchDiGar().getTgaOldTsTec().getTgaOldTsTecNull());
        }
        else {
            // COB_CODE: MOVE TGA-OLD-TS-TEC
            //             TO (SF)-OLD-TS-TEC(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaOldTsTec().setWtgaOldTsTec(Trunc.toDecimal(ws.getTrchDiGar().getTgaOldTsTec().getTgaOldTsTec(), 14, 9));
        }
        // COB_CODE: IF TGA-RAT-LRD-NULL = HIGH-VALUES
        //                TO (SF)-RAT-LRD-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-RAT-LRD(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaRatLrd().getTgaRatLrdNullFormatted())) {
            // COB_CODE: MOVE TGA-RAT-LRD-NULL
            //             TO (SF)-RAT-LRD-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaRatLrd().setWtgaRatLrdNull(ws.getTrchDiGar().getTgaRatLrd().getTgaRatLrdNull());
        }
        else {
            // COB_CODE: MOVE TGA-RAT-LRD
            //             TO (SF)-RAT-LRD(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaRatLrd().setWtgaRatLrd(Trunc.toDecimal(ws.getTrchDiGar().getTgaRatLrd().getTgaRatLrd(), 15, 3));
        }
        // COB_CODE: IF TGA-PRE-LRD-NULL = HIGH-VALUES
        //                TO (SF)-PRE-LRD-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-LRD(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPreLrd().getTgaPreLrdNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-LRD-NULL
            //             TO (SF)-PRE-LRD-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaPreLrd().setWtgaPreLrdNull(ws.getTrchDiGar().getTgaPreLrd().getTgaPreLrdNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-LRD
            //             TO (SF)-PRE-LRD(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaPreLrd().setWtgaPreLrd(Trunc.toDecimal(ws.getTrchDiGar().getTgaPreLrd().getTgaPreLrd(), 15, 3));
        }
        // COB_CODE: IF TGA-PRSTZ-INI-NULL = HIGH-VALUES
        //                TO (SF)-PRSTZ-INI-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRSTZ-INI(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPrstzIni().getTgaPrstzIniNullFormatted())) {
            // COB_CODE: MOVE TGA-PRSTZ-INI-NULL
            //             TO (SF)-PRSTZ-INI-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaPrstzIni().setWtgaPrstzIniNull(ws.getTrchDiGar().getTgaPrstzIni().getTgaPrstzIniNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRSTZ-INI
            //             TO (SF)-PRSTZ-INI(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaPrstzIni().setWtgaPrstzIni(Trunc.toDecimal(ws.getTrchDiGar().getTgaPrstzIni().getTgaPrstzIni(), 15, 3));
        }
        // COB_CODE: IF TGA-PRSTZ-ULT-NULL = HIGH-VALUES
        //                TO (SF)-PRSTZ-ULT-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRSTZ-ULT(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPrstzUlt().getTgaPrstzUltNullFormatted())) {
            // COB_CODE: MOVE TGA-PRSTZ-ULT-NULL
            //             TO (SF)-PRSTZ-ULT-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaPrstzUlt().setWtgaPrstzUltNull(ws.getTrchDiGar().getTgaPrstzUlt().getTgaPrstzUltNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRSTZ-ULT
            //             TO (SF)-PRSTZ-ULT(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaPrstzUlt().setWtgaPrstzUlt(Trunc.toDecimal(ws.getTrchDiGar().getTgaPrstzUlt().getTgaPrstzUlt(), 15, 3));
        }
        // COB_CODE: IF TGA-CPT-IN-OPZ-RIVTO-NULL = HIGH-VALUES
        //                TO (SF)-CPT-IN-OPZ-RIVTO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-CPT-IN-OPZ-RIVTO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaCptInOpzRivto().getTgaCptInOpzRivtoNullFormatted())) {
            // COB_CODE: MOVE TGA-CPT-IN-OPZ-RIVTO-NULL
            //             TO (SF)-CPT-IN-OPZ-RIVTO-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaCptInOpzRivto().setWtgaCptInOpzRivtoNull(ws.getTrchDiGar().getTgaCptInOpzRivto().getTgaCptInOpzRivtoNull());
        }
        else {
            // COB_CODE: MOVE TGA-CPT-IN-OPZ-RIVTO
            //             TO (SF)-CPT-IN-OPZ-RIVTO(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaCptInOpzRivto().setWtgaCptInOpzRivto(Trunc.toDecimal(ws.getTrchDiGar().getTgaCptInOpzRivto().getTgaCptInOpzRivto(), 15, 3));
        }
        // COB_CODE: IF TGA-PRSTZ-INI-STAB-NULL = HIGH-VALUES
        //                TO (SF)-PRSTZ-INI-STAB-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRSTZ-INI-STAB(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPrstzIniStab().getTgaPrstzIniStabNullFormatted())) {
            // COB_CODE: MOVE TGA-PRSTZ-INI-STAB-NULL
            //             TO (SF)-PRSTZ-INI-STAB-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaPrstzIniStab().setWtgaPrstzIniStabNull(ws.getTrchDiGar().getTgaPrstzIniStab().getTgaPrstzIniStabNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRSTZ-INI-STAB
            //             TO (SF)-PRSTZ-INI-STAB(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaPrstzIniStab().setWtgaPrstzIniStab(Trunc.toDecimal(ws.getTrchDiGar().getTgaPrstzIniStab().getTgaPrstzIniStab(), 15, 3));
        }
        // COB_CODE: IF TGA-CPT-RSH-MOR-NULL = HIGH-VALUES
        //                TO (SF)-CPT-RSH-MOR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-CPT-RSH-MOR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaCptRshMor().getTgaCptRshMorNullFormatted())) {
            // COB_CODE: MOVE TGA-CPT-RSH-MOR-NULL
            //             TO (SF)-CPT-RSH-MOR-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaCptRshMor().setWtgaCptRshMorNull(ws.getTrchDiGar().getTgaCptRshMor().getTgaCptRshMorNull());
        }
        else {
            // COB_CODE: MOVE TGA-CPT-RSH-MOR
            //             TO (SF)-CPT-RSH-MOR(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaCptRshMor().setWtgaCptRshMor(Trunc.toDecimal(ws.getTrchDiGar().getTgaCptRshMor().getTgaCptRshMor(), 15, 3));
        }
        // COB_CODE: IF TGA-PRSTZ-RID-INI-NULL = HIGH-VALUES
        //                TO (SF)-PRSTZ-RID-INI-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRSTZ-RID-INI(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPrstzRidIni().getTgaPrstzRidIniNullFormatted())) {
            // COB_CODE: MOVE TGA-PRSTZ-RID-INI-NULL
            //             TO (SF)-PRSTZ-RID-INI-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaPrstzRidIni().setWtgaPrstzRidIniNull(ws.getTrchDiGar().getTgaPrstzRidIni().getTgaPrstzRidIniNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRSTZ-RID-INI
            //             TO (SF)-PRSTZ-RID-INI(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaPrstzRidIni().setWtgaPrstzRidIni(Trunc.toDecimal(ws.getTrchDiGar().getTgaPrstzRidIni().getTgaPrstzRidIni(), 15, 3));
        }
        // COB_CODE: IF TGA-FL-CAR-CONT-NULL = HIGH-VALUES
        //                TO (SF)-FL-CAR-CONT-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-FL-CAR-CONT(IX-TAB-TGA)
        //           END-IF
        if (Conditions.eq(ws.getTrchDiGar().getTgaFlCarCont(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE TGA-FL-CAR-CONT-NULL
            //             TO (SF)-FL-CAR-CONT-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().setWtgaFlCarCont(ws.getTrchDiGar().getTgaFlCarCont());
        }
        else {
            // COB_CODE: MOVE TGA-FL-CAR-CONT
            //             TO (SF)-FL-CAR-CONT(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().setWtgaFlCarCont(ws.getTrchDiGar().getTgaFlCarCont());
        }
        // COB_CODE: IF TGA-BNS-GIA-LIQTO-NULL = HIGH-VALUES
        //                TO (SF)-BNS-GIA-LIQTO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-BNS-GIA-LIQTO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaBnsGiaLiqto().getTgaBnsGiaLiqtoNullFormatted())) {
            // COB_CODE: MOVE TGA-BNS-GIA-LIQTO-NULL
            //             TO (SF)-BNS-GIA-LIQTO-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaBnsGiaLiqto().setWtgaBnsGiaLiqtoNull(ws.getTrchDiGar().getTgaBnsGiaLiqto().getTgaBnsGiaLiqtoNull());
        }
        else {
            // COB_CODE: MOVE TGA-BNS-GIA-LIQTO
            //             TO (SF)-BNS-GIA-LIQTO(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaBnsGiaLiqto().setWtgaBnsGiaLiqto(Trunc.toDecimal(ws.getTrchDiGar().getTgaBnsGiaLiqto().getTgaBnsGiaLiqto(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-BNS-NULL = HIGH-VALUES
        //                TO (SF)-IMP-BNS-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-BNS(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpBns().getTgaImpBnsNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-BNS-NULL
            //             TO (SF)-IMP-BNS-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaImpBns().setWtgaImpBnsNull(ws.getTrchDiGar().getTgaImpBns().getTgaImpBnsNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-BNS
            //             TO (SF)-IMP-BNS(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaImpBns().setWtgaImpBns(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpBns().getTgaImpBns(), 15, 3));
        }
        // COB_CODE: MOVE TGA-COD-DVS
        //             TO (SF)-COD-DVS(IX-TAB-TGA)
        ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().setWtgaCodDvs(ws.getTrchDiGar().getTgaCodDvs());
        // COB_CODE: IF TGA-PRSTZ-INI-NEWFIS-NULL = HIGH-VALUES
        //                TO (SF)-PRSTZ-INI-NEWFIS-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRSTZ-INI-NEWFIS(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPrstzIniNewfis().getTgaPrstzIniNewfisNullFormatted())) {
            // COB_CODE: MOVE TGA-PRSTZ-INI-NEWFIS-NULL
            //             TO (SF)-PRSTZ-INI-NEWFIS-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaPrstzIniNewfis().setWtgaPrstzIniNewfisNull(ws.getTrchDiGar().getTgaPrstzIniNewfis().getTgaPrstzIniNewfisNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRSTZ-INI-NEWFIS
            //             TO (SF)-PRSTZ-INI-NEWFIS(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaPrstzIniNewfis().setWtgaPrstzIniNewfis(Trunc.toDecimal(ws.getTrchDiGar().getTgaPrstzIniNewfis().getTgaPrstzIniNewfis(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-SCON-NULL = HIGH-VALUES
        //                TO (SF)-IMP-SCON-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-SCON(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpScon().getTgaImpSconNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-SCON-NULL
            //             TO (SF)-IMP-SCON-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaImpScon().setWtgaImpSconNull(ws.getTrchDiGar().getTgaImpScon().getTgaImpSconNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-SCON
            //             TO (SF)-IMP-SCON(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaImpScon().setWtgaImpScon(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpScon().getTgaImpScon(), 15, 3));
        }
        // COB_CODE: IF TGA-ALQ-SCON-NULL = HIGH-VALUES
        //                TO (SF)-ALQ-SCON-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ALQ-SCON(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaAlqScon().getTgaAlqSconNullFormatted())) {
            // COB_CODE: MOVE TGA-ALQ-SCON-NULL
            //             TO (SF)-ALQ-SCON-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaAlqScon().setWtgaAlqSconNull(ws.getTrchDiGar().getTgaAlqScon().getTgaAlqSconNull());
        }
        else {
            // COB_CODE: MOVE TGA-ALQ-SCON
            //             TO (SF)-ALQ-SCON(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaAlqScon().setWtgaAlqScon(Trunc.toDecimal(ws.getTrchDiGar().getTgaAlqScon().getTgaAlqScon(), 6, 3));
        }
        // COB_CODE: IF TGA-IMP-CAR-ACQ-NULL = HIGH-VALUES
        //                TO (SF)-IMP-CAR-ACQ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-CAR-ACQ(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpCarAcq().getTgaImpCarAcqNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-CAR-ACQ-NULL
            //             TO (SF)-IMP-CAR-ACQ-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaImpCarAcq().setWtgaImpCarAcqNull(ws.getTrchDiGar().getTgaImpCarAcq().getTgaImpCarAcqNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-CAR-ACQ
            //             TO (SF)-IMP-CAR-ACQ(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaImpCarAcq().setWtgaImpCarAcq(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpCarAcq().getTgaImpCarAcq(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-CAR-INC-NULL = HIGH-VALUES
        //                TO (SF)-IMP-CAR-INC-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-CAR-INC(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpCarInc().getTgaImpCarIncNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-CAR-INC-NULL
            //             TO (SF)-IMP-CAR-INC-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaImpCarInc().setWtgaImpCarIncNull(ws.getTrchDiGar().getTgaImpCarInc().getTgaImpCarIncNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-CAR-INC
            //             TO (SF)-IMP-CAR-INC(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaImpCarInc().setWtgaImpCarInc(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpCarInc().getTgaImpCarInc(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-CAR-GEST-NULL = HIGH-VALUES
        //                TO (SF)-IMP-CAR-GEST-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-CAR-GEST(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpCarGest().getTgaImpCarGestNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-CAR-GEST-NULL
            //             TO (SF)-IMP-CAR-GEST-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaImpCarGest().setWtgaImpCarGestNull(ws.getTrchDiGar().getTgaImpCarGest().getTgaImpCarGestNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-CAR-GEST
            //             TO (SF)-IMP-CAR-GEST(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaImpCarGest().setWtgaImpCarGest(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpCarGest().getTgaImpCarGest(), 15, 3));
        }
        // COB_CODE: IF TGA-ETA-AA-1O-ASSTO-NULL = HIGH-VALUES
        //                TO (SF)-ETA-AA-1O-ASSTO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ETA-AA-1O-ASSTO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaEtaAa1oAssto().getTgaEtaAa1oAsstoNullFormatted())) {
            // COB_CODE: MOVE TGA-ETA-AA-1O-ASSTO-NULL
            //             TO (SF)-ETA-AA-1O-ASSTO-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaEtaAa1oAssto().setWtgaEtaAa1oAsstoNull(ws.getTrchDiGar().getTgaEtaAa1oAssto().getTgaEtaAa1oAsstoNull());
        }
        else {
            // COB_CODE: MOVE TGA-ETA-AA-1O-ASSTO
            //             TO (SF)-ETA-AA-1O-ASSTO(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaEtaAa1oAssto().setWtgaEtaAa1oAssto(ws.getTrchDiGar().getTgaEtaAa1oAssto().getTgaEtaAa1oAssto());
        }
        // COB_CODE: IF TGA-ETA-MM-1O-ASSTO-NULL = HIGH-VALUES
        //                TO (SF)-ETA-MM-1O-ASSTO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ETA-MM-1O-ASSTO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaEtaMm1oAssto().getTgaEtaMm1oAsstoNullFormatted())) {
            // COB_CODE: MOVE TGA-ETA-MM-1O-ASSTO-NULL
            //             TO (SF)-ETA-MM-1O-ASSTO-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaEtaMm1oAssto().setWtgaEtaMm1oAsstoNull(ws.getTrchDiGar().getTgaEtaMm1oAssto().getTgaEtaMm1oAsstoNull());
        }
        else {
            // COB_CODE: MOVE TGA-ETA-MM-1O-ASSTO
            //             TO (SF)-ETA-MM-1O-ASSTO(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaEtaMm1oAssto().setWtgaEtaMm1oAssto(ws.getTrchDiGar().getTgaEtaMm1oAssto().getTgaEtaMm1oAssto());
        }
        // COB_CODE: IF TGA-ETA-AA-2O-ASSTO-NULL = HIGH-VALUES
        //                TO (SF)-ETA-AA-2O-ASSTO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ETA-AA-2O-ASSTO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaEtaAa2oAssto().getTgaEtaAa2oAsstoNullFormatted())) {
            // COB_CODE: MOVE TGA-ETA-AA-2O-ASSTO-NULL
            //             TO (SF)-ETA-AA-2O-ASSTO-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaEtaAa2oAssto().setWtgaEtaAa2oAsstoNull(ws.getTrchDiGar().getTgaEtaAa2oAssto().getTgaEtaAa2oAsstoNull());
        }
        else {
            // COB_CODE: MOVE TGA-ETA-AA-2O-ASSTO
            //             TO (SF)-ETA-AA-2O-ASSTO(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaEtaAa2oAssto().setWtgaEtaAa2oAssto(ws.getTrchDiGar().getTgaEtaAa2oAssto().getTgaEtaAa2oAssto());
        }
        // COB_CODE: IF TGA-ETA-MM-2O-ASSTO-NULL = HIGH-VALUES
        //                TO (SF)-ETA-MM-2O-ASSTO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ETA-MM-2O-ASSTO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaEtaMm2oAssto().getTgaEtaMm2oAsstoNullFormatted())) {
            // COB_CODE: MOVE TGA-ETA-MM-2O-ASSTO-NULL
            //             TO (SF)-ETA-MM-2O-ASSTO-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaEtaMm2oAssto().setWtgaEtaMm2oAsstoNull(ws.getTrchDiGar().getTgaEtaMm2oAssto().getTgaEtaMm2oAsstoNull());
        }
        else {
            // COB_CODE: MOVE TGA-ETA-MM-2O-ASSTO
            //             TO (SF)-ETA-MM-2O-ASSTO(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaEtaMm2oAssto().setWtgaEtaMm2oAssto(ws.getTrchDiGar().getTgaEtaMm2oAssto().getTgaEtaMm2oAssto());
        }
        // COB_CODE: IF TGA-ETA-AA-3O-ASSTO-NULL = HIGH-VALUES
        //                TO (SF)-ETA-AA-3O-ASSTO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ETA-AA-3O-ASSTO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaEtaAa3oAssto().getTgaEtaAa3oAsstoNullFormatted())) {
            // COB_CODE: MOVE TGA-ETA-AA-3O-ASSTO-NULL
            //             TO (SF)-ETA-AA-3O-ASSTO-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaEtaAa3oAssto().setWtgaEtaAa3oAsstoNull(ws.getTrchDiGar().getTgaEtaAa3oAssto().getTgaEtaAa3oAsstoNull());
        }
        else {
            // COB_CODE: MOVE TGA-ETA-AA-3O-ASSTO
            //             TO (SF)-ETA-AA-3O-ASSTO(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaEtaAa3oAssto().setWtgaEtaAa3oAssto(ws.getTrchDiGar().getTgaEtaAa3oAssto().getTgaEtaAa3oAssto());
        }
        // COB_CODE: IF TGA-ETA-MM-3O-ASSTO-NULL = HIGH-VALUES
        //                TO (SF)-ETA-MM-3O-ASSTO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ETA-MM-3O-ASSTO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaEtaMm3oAssto().getTgaEtaMm3oAsstoNullFormatted())) {
            // COB_CODE: MOVE TGA-ETA-MM-3O-ASSTO-NULL
            //             TO (SF)-ETA-MM-3O-ASSTO-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaEtaMm3oAssto().setWtgaEtaMm3oAsstoNull(ws.getTrchDiGar().getTgaEtaMm3oAssto().getTgaEtaMm3oAsstoNull());
        }
        else {
            // COB_CODE: MOVE TGA-ETA-MM-3O-ASSTO
            //             TO (SF)-ETA-MM-3O-ASSTO(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaEtaMm3oAssto().setWtgaEtaMm3oAssto(ws.getTrchDiGar().getTgaEtaMm3oAssto().getTgaEtaMm3oAssto());
        }
        // COB_CODE: IF TGA-RENDTO-LRD-NULL = HIGH-VALUES
        //                TO (SF)-RENDTO-LRD-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-RENDTO-LRD(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaRendtoLrd().getTgaRendtoLrdNullFormatted())) {
            // COB_CODE: MOVE TGA-RENDTO-LRD-NULL
            //             TO (SF)-RENDTO-LRD-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaRendtoLrd().setWtgaRendtoLrdNull(ws.getTrchDiGar().getTgaRendtoLrd().getTgaRendtoLrdNull());
        }
        else {
            // COB_CODE: MOVE TGA-RENDTO-LRD
            //             TO (SF)-RENDTO-LRD(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaRendtoLrd().setWtgaRendtoLrd(Trunc.toDecimal(ws.getTrchDiGar().getTgaRendtoLrd().getTgaRendtoLrd(), 14, 9));
        }
        // COB_CODE: IF TGA-PC-RETR-NULL = HIGH-VALUES
        //                TO (SF)-PC-RETR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PC-RETR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPcRetr().getTgaPcRetrNullFormatted())) {
            // COB_CODE: MOVE TGA-PC-RETR-NULL
            //             TO (SF)-PC-RETR-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaPcRetr().setWtgaPcRetrNull(ws.getTrchDiGar().getTgaPcRetr().getTgaPcRetrNull());
        }
        else {
            // COB_CODE: MOVE TGA-PC-RETR
            //             TO (SF)-PC-RETR(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaPcRetr().setWtgaPcRetr(Trunc.toDecimal(ws.getTrchDiGar().getTgaPcRetr().getTgaPcRetr(), 6, 3));
        }
        // COB_CODE: IF TGA-RENDTO-RETR-NULL = HIGH-VALUES
        //                TO (SF)-RENDTO-RETR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-RENDTO-RETR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaRendtoRetr().getTgaRendtoRetrNullFormatted())) {
            // COB_CODE: MOVE TGA-RENDTO-RETR-NULL
            //             TO (SF)-RENDTO-RETR-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaRendtoRetr().setWtgaRendtoRetrNull(ws.getTrchDiGar().getTgaRendtoRetr().getTgaRendtoRetrNull());
        }
        else {
            // COB_CODE: MOVE TGA-RENDTO-RETR
            //             TO (SF)-RENDTO-RETR(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaRendtoRetr().setWtgaRendtoRetr(Trunc.toDecimal(ws.getTrchDiGar().getTgaRendtoRetr().getTgaRendtoRetr(), 14, 9));
        }
        // COB_CODE: IF TGA-MIN-GARTO-NULL = HIGH-VALUES
        //                TO (SF)-MIN-GARTO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-MIN-GARTO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaMinGarto().getTgaMinGartoNullFormatted())) {
            // COB_CODE: MOVE TGA-MIN-GARTO-NULL
            //             TO (SF)-MIN-GARTO-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaMinGarto().setWtgaMinGartoNull(ws.getTrchDiGar().getTgaMinGarto().getTgaMinGartoNull());
        }
        else {
            // COB_CODE: MOVE TGA-MIN-GARTO
            //             TO (SF)-MIN-GARTO(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaMinGarto().setWtgaMinGarto(Trunc.toDecimal(ws.getTrchDiGar().getTgaMinGarto().getTgaMinGarto(), 14, 9));
        }
        // COB_CODE: IF TGA-MIN-TRNUT-NULL = HIGH-VALUES
        //                TO (SF)-MIN-TRNUT-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-MIN-TRNUT(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaMinTrnut().getTgaMinTrnutNullFormatted())) {
            // COB_CODE: MOVE TGA-MIN-TRNUT-NULL
            //             TO (SF)-MIN-TRNUT-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaMinTrnut().setWtgaMinTrnutNull(ws.getTrchDiGar().getTgaMinTrnut().getTgaMinTrnutNull());
        }
        else {
            // COB_CODE: MOVE TGA-MIN-TRNUT
            //             TO (SF)-MIN-TRNUT(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaMinTrnut().setWtgaMinTrnut(Trunc.toDecimal(ws.getTrchDiGar().getTgaMinTrnut().getTgaMinTrnut(), 14, 9));
        }
        // COB_CODE: IF TGA-PRE-ATT-DI-TRCH-NULL = HIGH-VALUES
        //                TO (SF)-PRE-ATT-DI-TRCH-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-ATT-DI-TRCH(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPreAttDiTrch().getTgaPreAttDiTrchNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-ATT-DI-TRCH-NULL
            //             TO (SF)-PRE-ATT-DI-TRCH-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaPreAttDiTrch().setWtgaPreAttDiTrchNull(ws.getTrchDiGar().getTgaPreAttDiTrch().getTgaPreAttDiTrchNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-ATT-DI-TRCH
            //             TO (SF)-PRE-ATT-DI-TRCH(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaPreAttDiTrch().setWtgaPreAttDiTrch(Trunc.toDecimal(ws.getTrchDiGar().getTgaPreAttDiTrch().getTgaPreAttDiTrch(), 15, 3));
        }
        // COB_CODE: IF TGA-MATU-END2000-NULL = HIGH-VALUES
        //                TO (SF)-MATU-END2000-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-MATU-END2000(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaMatuEnd2000().getTgaMatuEnd2000NullFormatted())) {
            // COB_CODE: MOVE TGA-MATU-END2000-NULL
            //             TO (SF)-MATU-END2000-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaMatuEnd2000().setWtgaMatuEnd2000Null(ws.getTrchDiGar().getTgaMatuEnd2000().getTgaMatuEnd2000Null());
        }
        else {
            // COB_CODE: MOVE TGA-MATU-END2000
            //             TO (SF)-MATU-END2000(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaMatuEnd2000().setWtgaMatuEnd2000(Trunc.toDecimal(ws.getTrchDiGar().getTgaMatuEnd2000().getTgaMatuEnd2000(), 15, 3));
        }
        // COB_CODE: IF TGA-ABB-TOT-INI-NULL = HIGH-VALUES
        //                TO (SF)-ABB-TOT-INI-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ABB-TOT-INI(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaAbbTotIni().getTgaAbbTotIniNullFormatted())) {
            // COB_CODE: MOVE TGA-ABB-TOT-INI-NULL
            //             TO (SF)-ABB-TOT-INI-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaAbbTotIni().setWtgaAbbTotIniNull(ws.getTrchDiGar().getTgaAbbTotIni().getTgaAbbTotIniNull());
        }
        else {
            // COB_CODE: MOVE TGA-ABB-TOT-INI
            //             TO (SF)-ABB-TOT-INI(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaAbbTotIni().setWtgaAbbTotIni(Trunc.toDecimal(ws.getTrchDiGar().getTgaAbbTotIni().getTgaAbbTotIni(), 15, 3));
        }
        // COB_CODE: IF TGA-ABB-TOT-ULT-NULL = HIGH-VALUES
        //                TO (SF)-ABB-TOT-ULT-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ABB-TOT-ULT(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaAbbTotUlt().getTgaAbbTotUltNullFormatted())) {
            // COB_CODE: MOVE TGA-ABB-TOT-ULT-NULL
            //             TO (SF)-ABB-TOT-ULT-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaAbbTotUlt().setWtgaAbbTotUltNull(ws.getTrchDiGar().getTgaAbbTotUlt().getTgaAbbTotUltNull());
        }
        else {
            // COB_CODE: MOVE TGA-ABB-TOT-ULT
            //             TO (SF)-ABB-TOT-ULT(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaAbbTotUlt().setWtgaAbbTotUlt(Trunc.toDecimal(ws.getTrchDiGar().getTgaAbbTotUlt().getTgaAbbTotUlt(), 15, 3));
        }
        // COB_CODE: IF TGA-ABB-ANNU-ULT-NULL = HIGH-VALUES
        //                TO (SF)-ABB-ANNU-ULT-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ABB-ANNU-ULT(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaAbbAnnuUlt().getTgaAbbAnnuUltNullFormatted())) {
            // COB_CODE: MOVE TGA-ABB-ANNU-ULT-NULL
            //             TO (SF)-ABB-ANNU-ULT-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaAbbAnnuUlt().setWtgaAbbAnnuUltNull(ws.getTrchDiGar().getTgaAbbAnnuUlt().getTgaAbbAnnuUltNull());
        }
        else {
            // COB_CODE: MOVE TGA-ABB-ANNU-ULT
            //             TO (SF)-ABB-ANNU-ULT(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaAbbAnnuUlt().setWtgaAbbAnnuUlt(Trunc.toDecimal(ws.getTrchDiGar().getTgaAbbAnnuUlt().getTgaAbbAnnuUlt(), 15, 3));
        }
        // COB_CODE: IF TGA-DUR-ABB-NULL = HIGH-VALUES
        //                TO (SF)-DUR-ABB-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-DUR-ABB(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaDurAbb().getTgaDurAbbNullFormatted())) {
            // COB_CODE: MOVE TGA-DUR-ABB-NULL
            //             TO (SF)-DUR-ABB-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaDurAbb().setWtgaDurAbbNull(ws.getTrchDiGar().getTgaDurAbb().getTgaDurAbbNull());
        }
        else {
            // COB_CODE: MOVE TGA-DUR-ABB
            //             TO (SF)-DUR-ABB(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaDurAbb().setWtgaDurAbb(ws.getTrchDiGar().getTgaDurAbb().getTgaDurAbb());
        }
        // COB_CODE: IF TGA-TP-ADEG-ABB-NULL = HIGH-VALUES
        //                TO (SF)-TP-ADEG-ABB-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-TP-ADEG-ABB(IX-TAB-TGA)
        //           END-IF
        if (Conditions.eq(ws.getTrchDiGar().getTgaTpAdegAbb(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE TGA-TP-ADEG-ABB-NULL
            //             TO (SF)-TP-ADEG-ABB-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().setWtgaTpAdegAbb(ws.getTrchDiGar().getTgaTpAdegAbb());
        }
        else {
            // COB_CODE: MOVE TGA-TP-ADEG-ABB
            //             TO (SF)-TP-ADEG-ABB(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().setWtgaTpAdegAbb(ws.getTrchDiGar().getTgaTpAdegAbb());
        }
        // COB_CODE: IF TGA-MOD-CALC-NULL = HIGH-VALUES
        //                TO (SF)-MOD-CALC-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-MOD-CALC(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaModCalcFormatted())) {
            // COB_CODE: MOVE TGA-MOD-CALC-NULL
            //             TO (SF)-MOD-CALC-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().setWtgaModCalc(ws.getTrchDiGar().getTgaModCalc());
        }
        else {
            // COB_CODE: MOVE TGA-MOD-CALC
            //             TO (SF)-MOD-CALC(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().setWtgaModCalc(ws.getTrchDiGar().getTgaModCalc());
        }
        // COB_CODE: IF TGA-IMP-AZ-NULL = HIGH-VALUES
        //                TO (SF)-IMP-AZ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-AZ(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpAz().getTgaImpAzNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-AZ-NULL
            //             TO (SF)-IMP-AZ-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaImpAz().setWtgaImpAzNull(ws.getTrchDiGar().getTgaImpAz().getTgaImpAzNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-AZ
            //             TO (SF)-IMP-AZ(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaImpAz().setWtgaImpAz(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpAz().getTgaImpAz(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-ADER-NULL = HIGH-VALUES
        //                TO (SF)-IMP-ADER-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-ADER(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpAder().getTgaImpAderNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-ADER-NULL
            //             TO (SF)-IMP-ADER-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaImpAder().setWtgaImpAderNull(ws.getTrchDiGar().getTgaImpAder().getTgaImpAderNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-ADER
            //             TO (SF)-IMP-ADER(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaImpAder().setWtgaImpAder(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpAder().getTgaImpAder(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-TFR-NULL = HIGH-VALUES
        //                TO (SF)-IMP-TFR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-TFR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpTfr().getTgaImpTfrNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-TFR-NULL
            //             TO (SF)-IMP-TFR-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaImpTfr().setWtgaImpTfrNull(ws.getTrchDiGar().getTgaImpTfr().getTgaImpTfrNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-TFR
            //             TO (SF)-IMP-TFR(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaImpTfr().setWtgaImpTfr(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpTfr().getTgaImpTfr(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-VOLO-NULL = HIGH-VALUES
        //                TO (SF)-IMP-VOLO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-VOLO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpVolo().getTgaImpVoloNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-VOLO-NULL
            //             TO (SF)-IMP-VOLO-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaImpVolo().setWtgaImpVoloNull(ws.getTrchDiGar().getTgaImpVolo().getTgaImpVoloNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-VOLO
            //             TO (SF)-IMP-VOLO(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaImpVolo().setWtgaImpVolo(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpVolo().getTgaImpVolo(), 15, 3));
        }
        // COB_CODE: IF TGA-VIS-END2000-NULL = HIGH-VALUES
        //                TO (SF)-VIS-END2000-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-VIS-END2000(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaVisEnd2000().getTgaVisEnd2000NullFormatted())) {
            // COB_CODE: MOVE TGA-VIS-END2000-NULL
            //             TO (SF)-VIS-END2000-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaVisEnd2000().setWtgaVisEnd2000Null(ws.getTrchDiGar().getTgaVisEnd2000().getTgaVisEnd2000Null());
        }
        else {
            // COB_CODE: MOVE TGA-VIS-END2000
            //             TO (SF)-VIS-END2000(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaVisEnd2000().setWtgaVisEnd2000(Trunc.toDecimal(ws.getTrchDiGar().getTgaVisEnd2000().getTgaVisEnd2000(), 15, 3));
        }
        // COB_CODE: IF TGA-DT-VLDT-PROD-NULL = HIGH-VALUES
        //                TO (SF)-DT-VLDT-PROD-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-DT-VLDT-PROD(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaDtVldtProd().getTgaDtVldtProdNullFormatted())) {
            // COB_CODE: MOVE TGA-DT-VLDT-PROD-NULL
            //             TO (SF)-DT-VLDT-PROD-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaDtVldtProd().setWtgaDtVldtProdNull(ws.getTrchDiGar().getTgaDtVldtProd().getTgaDtVldtProdNull());
        }
        else {
            // COB_CODE: MOVE TGA-DT-VLDT-PROD
            //             TO (SF)-DT-VLDT-PROD(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaDtVldtProd().setWtgaDtVldtProd(ws.getTrchDiGar().getTgaDtVldtProd().getTgaDtVldtProd());
        }
        // COB_CODE: IF TGA-DT-INI-VAL-TAR-NULL = HIGH-VALUES
        //                TO (SF)-DT-INI-VAL-TAR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-DT-INI-VAL-TAR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaDtIniValTar().getTgaDtIniValTarNullFormatted())) {
            // COB_CODE: MOVE TGA-DT-INI-VAL-TAR-NULL
            //             TO (SF)-DT-INI-VAL-TAR-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaDtIniValTar().setWtgaDtIniValTarNull(ws.getTrchDiGar().getTgaDtIniValTar().getTgaDtIniValTarNull());
        }
        else {
            // COB_CODE: MOVE TGA-DT-INI-VAL-TAR
            //             TO (SF)-DT-INI-VAL-TAR(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaDtIniValTar().setWtgaDtIniValTar(ws.getTrchDiGar().getTgaDtIniValTar().getTgaDtIniValTar());
        }
        // COB_CODE: IF TGA-IMPB-VIS-END2000-NULL = HIGH-VALUES
        //                TO (SF)-IMPB-VIS-END2000-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMPB-VIS-END2000(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpbVisEnd2000().getTgaImpbVisEnd2000NullFormatted())) {
            // COB_CODE: MOVE TGA-IMPB-VIS-END2000-NULL
            //             TO (SF)-IMPB-VIS-END2000-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaImpbVisEnd2000().setWtgaImpbVisEnd2000Null(ws.getTrchDiGar().getTgaImpbVisEnd2000().getTgaImpbVisEnd2000Null());
        }
        else {
            // COB_CODE: MOVE TGA-IMPB-VIS-END2000
            //             TO (SF)-IMPB-VIS-END2000(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaImpbVisEnd2000().setWtgaImpbVisEnd2000(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpbVisEnd2000().getTgaImpbVisEnd2000(), 15, 3));
        }
        // COB_CODE: IF TGA-REN-INI-TS-TEC-0-NULL = HIGH-VALUES
        //                TO (SF)-REN-INI-TS-TEC-0-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-REN-INI-TS-TEC-0(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaRenIniTsTec0().getTgaRenIniTsTec0NullFormatted())) {
            // COB_CODE: MOVE TGA-REN-INI-TS-TEC-0-NULL
            //             TO (SF)-REN-INI-TS-TEC-0-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaRenIniTsTec0().setWtgaRenIniTsTec0Null(ws.getTrchDiGar().getTgaRenIniTsTec0().getTgaRenIniTsTec0Null());
        }
        else {
            // COB_CODE: MOVE TGA-REN-INI-TS-TEC-0
            //             TO (SF)-REN-INI-TS-TEC-0(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaRenIniTsTec0().setWtgaRenIniTsTec0(Trunc.toDecimal(ws.getTrchDiGar().getTgaRenIniTsTec0().getTgaRenIniTsTec0(), 15, 3));
        }
        // COB_CODE: IF TGA-PC-RIP-PRE-NULL = HIGH-VALUES
        //                TO (SF)-PC-RIP-PRE-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PC-RIP-PRE(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPcRipPre().getTgaPcRipPreNullFormatted())) {
            // COB_CODE: MOVE TGA-PC-RIP-PRE-NULL
            //             TO (SF)-PC-RIP-PRE-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaPcRipPre().setWtgaPcRipPreNull(ws.getTrchDiGar().getTgaPcRipPre().getTgaPcRipPreNull());
        }
        else {
            // COB_CODE: MOVE TGA-PC-RIP-PRE
            //             TO (SF)-PC-RIP-PRE(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaPcRipPre().setWtgaPcRipPre(Trunc.toDecimal(ws.getTrchDiGar().getTgaPcRipPre().getTgaPcRipPre(), 6, 3));
        }
        // COB_CODE: IF TGA-FL-IMPORTI-FORZ-NULL = HIGH-VALUES
        //                TO (SF)-FL-IMPORTI-FORZ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-FL-IMPORTI-FORZ(IX-TAB-TGA)
        //           END-IF
        if (Conditions.eq(ws.getTrchDiGar().getTgaFlImportiForz(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE TGA-FL-IMPORTI-FORZ-NULL
            //             TO (SF)-FL-IMPORTI-FORZ-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().setWtgaFlImportiForz(ws.getTrchDiGar().getTgaFlImportiForz());
        }
        else {
            // COB_CODE: MOVE TGA-FL-IMPORTI-FORZ
            //             TO (SF)-FL-IMPORTI-FORZ(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().setWtgaFlImportiForz(ws.getTrchDiGar().getTgaFlImportiForz());
        }
        // COB_CODE: IF TGA-PRSTZ-INI-NFORZ-NULL = HIGH-VALUES
        //                TO (SF)-PRSTZ-INI-NFORZ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRSTZ-INI-NFORZ(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPrstzIniNforz().getTgaPrstzIniNforzNullFormatted())) {
            // COB_CODE: MOVE TGA-PRSTZ-INI-NFORZ-NULL
            //             TO (SF)-PRSTZ-INI-NFORZ-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaPrstzIniNforz().setWtgaPrstzIniNforzNull(ws.getTrchDiGar().getTgaPrstzIniNforz().getTgaPrstzIniNforzNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRSTZ-INI-NFORZ
            //             TO (SF)-PRSTZ-INI-NFORZ(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaPrstzIniNforz().setWtgaPrstzIniNforz(Trunc.toDecimal(ws.getTrchDiGar().getTgaPrstzIniNforz().getTgaPrstzIniNforz(), 15, 3));
        }
        // COB_CODE: IF TGA-VIS-END2000-NFORZ-NULL = HIGH-VALUES
        //                TO (SF)-VIS-END2000-NFORZ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-VIS-END2000-NFORZ(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaVisEnd2000Nforz().getTgaVisEnd2000NforzNullFormatted())) {
            // COB_CODE: MOVE TGA-VIS-END2000-NFORZ-NULL
            //             TO (SF)-VIS-END2000-NFORZ-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaVisEnd2000Nforz().setWtgaVisEnd2000NforzNull(ws.getTrchDiGar().getTgaVisEnd2000Nforz().getTgaVisEnd2000NforzNull());
        }
        else {
            // COB_CODE: MOVE TGA-VIS-END2000-NFORZ
            //             TO (SF)-VIS-END2000-NFORZ(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaVisEnd2000Nforz().setWtgaVisEnd2000Nforz(Trunc.toDecimal(ws.getTrchDiGar().getTgaVisEnd2000Nforz().getTgaVisEnd2000Nforz(), 15, 3));
        }
        // COB_CODE: IF TGA-INTR-MORA-NULL = HIGH-VALUES
        //                TO (SF)-INTR-MORA-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-INTR-MORA(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaIntrMora().getTgaIntrMoraNullFormatted())) {
            // COB_CODE: MOVE TGA-INTR-MORA-NULL
            //             TO (SF)-INTR-MORA-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaIntrMora().setWtgaIntrMoraNull(ws.getTrchDiGar().getTgaIntrMora().getTgaIntrMoraNull());
        }
        else {
            // COB_CODE: MOVE TGA-INTR-MORA
            //             TO (SF)-INTR-MORA(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaIntrMora().setWtgaIntrMora(Trunc.toDecimal(ws.getTrchDiGar().getTgaIntrMora().getTgaIntrMora(), 15, 3));
        }
        // COB_CODE: IF TGA-MANFEE-ANTIC-NULL = HIGH-VALUES
        //                TO (SF)-MANFEE-ANTIC-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-MANFEE-ANTIC(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaManfeeAntic().getTgaManfeeAnticNullFormatted())) {
            // COB_CODE: MOVE TGA-MANFEE-ANTIC-NULL
            //             TO (SF)-MANFEE-ANTIC-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaManfeeAntic().setWtgaManfeeAnticNull(ws.getTrchDiGar().getTgaManfeeAntic().getTgaManfeeAnticNull());
        }
        else {
            // COB_CODE: MOVE TGA-MANFEE-ANTIC
            //             TO (SF)-MANFEE-ANTIC(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaManfeeAntic().setWtgaManfeeAntic(Trunc.toDecimal(ws.getTrchDiGar().getTgaManfeeAntic().getTgaManfeeAntic(), 15, 3));
        }
        // COB_CODE: IF TGA-MANFEE-RICOR-NULL = HIGH-VALUES
        //                TO (SF)-MANFEE-RICOR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-MANFEE-RICOR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaManfeeRicor().getTgaManfeeRicorNullFormatted())) {
            // COB_CODE: MOVE TGA-MANFEE-RICOR-NULL
            //             TO (SF)-MANFEE-RICOR-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaManfeeRicor().setWtgaManfeeRicorNull(ws.getTrchDiGar().getTgaManfeeRicor().getTgaManfeeRicorNull());
        }
        else {
            // COB_CODE: MOVE TGA-MANFEE-RICOR
            //             TO (SF)-MANFEE-RICOR(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaManfeeRicor().setWtgaManfeeRicor(Trunc.toDecimal(ws.getTrchDiGar().getTgaManfeeRicor().getTgaManfeeRicor(), 15, 3));
        }
        // COB_CODE: IF TGA-PRE-UNI-RIVTO-NULL = HIGH-VALUES
        //                TO (SF)-PRE-UNI-RIVTO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-UNI-RIVTO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPreUniRivto().getTgaPreUniRivtoNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-UNI-RIVTO-NULL
            //             TO (SF)-PRE-UNI-RIVTO-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaPreUniRivto().setWtgaPreUniRivtoNull(ws.getTrchDiGar().getTgaPreUniRivto().getTgaPreUniRivtoNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-UNI-RIVTO
            //             TO (SF)-PRE-UNI-RIVTO(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaPreUniRivto().setWtgaPreUniRivto(Trunc.toDecimal(ws.getTrchDiGar().getTgaPreUniRivto().getTgaPreUniRivto(), 15, 3));
        }
        // COB_CODE: IF TGA-PROV-1AA-ACQ-NULL = HIGH-VALUES
        //                TO (SF)-PROV-1AA-ACQ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PROV-1AA-ACQ(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaProv1aaAcq().getTgaProv1aaAcqNullFormatted())) {
            // COB_CODE: MOVE TGA-PROV-1AA-ACQ-NULL
            //             TO (SF)-PROV-1AA-ACQ-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaProv1aaAcq().setWtgaProv1aaAcqNull(ws.getTrchDiGar().getTgaProv1aaAcq().getTgaProv1aaAcqNull());
        }
        else {
            // COB_CODE: MOVE TGA-PROV-1AA-ACQ
            //             TO (SF)-PROV-1AA-ACQ(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaProv1aaAcq().setWtgaProv1aaAcq(Trunc.toDecimal(ws.getTrchDiGar().getTgaProv1aaAcq().getTgaProv1aaAcq(), 15, 3));
        }
        // COB_CODE: IF TGA-PROV-2AA-ACQ-NULL = HIGH-VALUES
        //                TO (SF)-PROV-2AA-ACQ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PROV-2AA-ACQ(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaProv2aaAcq().getTgaProv2aaAcqNullFormatted())) {
            // COB_CODE: MOVE TGA-PROV-2AA-ACQ-NULL
            //             TO (SF)-PROV-2AA-ACQ-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaProv2aaAcq().setWtgaProv2aaAcqNull(ws.getTrchDiGar().getTgaProv2aaAcq().getTgaProv2aaAcqNull());
        }
        else {
            // COB_CODE: MOVE TGA-PROV-2AA-ACQ
            //             TO (SF)-PROV-2AA-ACQ(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaProv2aaAcq().setWtgaProv2aaAcq(Trunc.toDecimal(ws.getTrchDiGar().getTgaProv2aaAcq().getTgaProv2aaAcq(), 15, 3));
        }
        // COB_CODE: IF TGA-PROV-RICOR-NULL = HIGH-VALUES
        //                TO (SF)-PROV-RICOR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PROV-RICOR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaProvRicor().getTgaProvRicorNullFormatted())) {
            // COB_CODE: MOVE TGA-PROV-RICOR-NULL
            //             TO (SF)-PROV-RICOR-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaProvRicor().setWtgaProvRicorNull(ws.getTrchDiGar().getTgaProvRicor().getTgaProvRicorNull());
        }
        else {
            // COB_CODE: MOVE TGA-PROV-RICOR
            //             TO (SF)-PROV-RICOR(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaProvRicor().setWtgaProvRicor(Trunc.toDecimal(ws.getTrchDiGar().getTgaProvRicor().getTgaProvRicor(), 15, 3));
        }
        // COB_CODE: IF TGA-PROV-INC-NULL = HIGH-VALUES
        //                TO (SF)-PROV-INC-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PROV-INC(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaProvInc().getTgaProvIncNullFormatted())) {
            // COB_CODE: MOVE TGA-PROV-INC-NULL
            //             TO (SF)-PROV-INC-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaProvInc().setWtgaProvIncNull(ws.getTrchDiGar().getTgaProvInc().getTgaProvIncNull());
        }
        else {
            // COB_CODE: MOVE TGA-PROV-INC
            //             TO (SF)-PROV-INC(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaProvInc().setWtgaProvInc(Trunc.toDecimal(ws.getTrchDiGar().getTgaProvInc().getTgaProvInc(), 15, 3));
        }
        // COB_CODE: IF TGA-ALQ-PROV-ACQ-NULL = HIGH-VALUES
        //                TO (SF)-ALQ-PROV-ACQ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ALQ-PROV-ACQ(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaAlqProvAcq().getTgaAlqProvAcqNullFormatted())) {
            // COB_CODE: MOVE TGA-ALQ-PROV-ACQ-NULL
            //             TO (SF)-ALQ-PROV-ACQ-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaAlqProvAcq().setWtgaAlqProvAcqNull(ws.getTrchDiGar().getTgaAlqProvAcq().getTgaAlqProvAcqNull());
        }
        else {
            // COB_CODE: MOVE TGA-ALQ-PROV-ACQ
            //             TO (SF)-ALQ-PROV-ACQ(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaAlqProvAcq().setWtgaAlqProvAcq(Trunc.toDecimal(ws.getTrchDiGar().getTgaAlqProvAcq().getTgaAlqProvAcq(), 6, 3));
        }
        // COB_CODE: IF TGA-ALQ-PROV-INC-NULL = HIGH-VALUES
        //                TO (SF)-ALQ-PROV-INC-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ALQ-PROV-INC(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaAlqProvInc().getTgaAlqProvIncNullFormatted())) {
            // COB_CODE: MOVE TGA-ALQ-PROV-INC-NULL
            //             TO (SF)-ALQ-PROV-INC-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaAlqProvInc().setWtgaAlqProvIncNull(ws.getTrchDiGar().getTgaAlqProvInc().getTgaAlqProvIncNull());
        }
        else {
            // COB_CODE: MOVE TGA-ALQ-PROV-INC
            //             TO (SF)-ALQ-PROV-INC(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaAlqProvInc().setWtgaAlqProvInc(Trunc.toDecimal(ws.getTrchDiGar().getTgaAlqProvInc().getTgaAlqProvInc(), 6, 3));
        }
        // COB_CODE: IF TGA-ALQ-PROV-RICOR-NULL = HIGH-VALUES
        //                TO (SF)-ALQ-PROV-RICOR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ALQ-PROV-RICOR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaAlqProvRicor().getTgaAlqProvRicorNullFormatted())) {
            // COB_CODE: MOVE TGA-ALQ-PROV-RICOR-NULL
            //             TO (SF)-ALQ-PROV-RICOR-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaAlqProvRicor().setWtgaAlqProvRicorNull(ws.getTrchDiGar().getTgaAlqProvRicor().getTgaAlqProvRicorNull());
        }
        else {
            // COB_CODE: MOVE TGA-ALQ-PROV-RICOR
            //             TO (SF)-ALQ-PROV-RICOR(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaAlqProvRicor().setWtgaAlqProvRicor(Trunc.toDecimal(ws.getTrchDiGar().getTgaAlqProvRicor().getTgaAlqProvRicor(), 6, 3));
        }
        // COB_CODE: IF TGA-IMPB-PROV-ACQ-NULL = HIGH-VALUES
        //                TO (SF)-IMPB-PROV-ACQ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMPB-PROV-ACQ(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpbProvAcq().getTgaImpbProvAcqNullFormatted())) {
            // COB_CODE: MOVE TGA-IMPB-PROV-ACQ-NULL
            //             TO (SF)-IMPB-PROV-ACQ-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaImpbProvAcq().setWtgaImpbProvAcqNull(ws.getTrchDiGar().getTgaImpbProvAcq().getTgaImpbProvAcqNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMPB-PROV-ACQ
            //             TO (SF)-IMPB-PROV-ACQ(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaImpbProvAcq().setWtgaImpbProvAcq(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpbProvAcq().getTgaImpbProvAcq(), 15, 3));
        }
        // COB_CODE: IF TGA-IMPB-PROV-INC-NULL = HIGH-VALUES
        //                TO (SF)-IMPB-PROV-INC-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMPB-PROV-INC(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpbProvInc().getTgaImpbProvIncNullFormatted())) {
            // COB_CODE: MOVE TGA-IMPB-PROV-INC-NULL
            //             TO (SF)-IMPB-PROV-INC-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaImpbProvInc().setWtgaImpbProvIncNull(ws.getTrchDiGar().getTgaImpbProvInc().getTgaImpbProvIncNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMPB-PROV-INC
            //             TO (SF)-IMPB-PROV-INC(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaImpbProvInc().setWtgaImpbProvInc(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpbProvInc().getTgaImpbProvInc(), 15, 3));
        }
        // COB_CODE: IF TGA-IMPB-PROV-RICOR-NULL = HIGH-VALUES
        //                TO (SF)-IMPB-PROV-RICOR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMPB-PROV-RICOR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpbProvRicor().getTgaImpbProvRicorNullFormatted())) {
            // COB_CODE: MOVE TGA-IMPB-PROV-RICOR-NULL
            //             TO (SF)-IMPB-PROV-RICOR-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaImpbProvRicor().setWtgaImpbProvRicorNull(ws.getTrchDiGar().getTgaImpbProvRicor().getTgaImpbProvRicorNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMPB-PROV-RICOR
            //             TO (SF)-IMPB-PROV-RICOR(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaImpbProvRicor().setWtgaImpbProvRicor(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpbProvRicor().getTgaImpbProvRicor(), 15, 3));
        }
        // COB_CODE: IF TGA-FL-PROV-FORZ-NULL = HIGH-VALUES
        //                TO (SF)-FL-PROV-FORZ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-FL-PROV-FORZ(IX-TAB-TGA)
        //           END-IF
        if (Conditions.eq(ws.getTrchDiGar().getTgaFlProvForz(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE TGA-FL-PROV-FORZ-NULL
            //             TO (SF)-FL-PROV-FORZ-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().setWtgaFlProvForz(ws.getTrchDiGar().getTgaFlProvForz());
        }
        else {
            // COB_CODE: MOVE TGA-FL-PROV-FORZ
            //             TO (SF)-FL-PROV-FORZ(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().setWtgaFlProvForz(ws.getTrchDiGar().getTgaFlProvForz());
        }
        // COB_CODE: IF TGA-PRSTZ-AGG-INI-NULL = HIGH-VALUES
        //                TO (SF)-PRSTZ-AGG-INI-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRSTZ-AGG-INI(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPrstzAggIni().getTgaPrstzAggIniNullFormatted())) {
            // COB_CODE: MOVE TGA-PRSTZ-AGG-INI-NULL
            //             TO (SF)-PRSTZ-AGG-INI-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaPrstzAggIni().setWtgaPrstzAggIniNull(ws.getTrchDiGar().getTgaPrstzAggIni().getTgaPrstzAggIniNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRSTZ-AGG-INI
            //             TO (SF)-PRSTZ-AGG-INI(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaPrstzAggIni().setWtgaPrstzAggIni(Trunc.toDecimal(ws.getTrchDiGar().getTgaPrstzAggIni().getTgaPrstzAggIni(), 15, 3));
        }
        // COB_CODE: IF TGA-INCR-PRE-NULL = HIGH-VALUES
        //                TO (SF)-INCR-PRE-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-INCR-PRE(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaIncrPre().getTgaIncrPreNullFormatted())) {
            // COB_CODE: MOVE TGA-INCR-PRE-NULL
            //             TO (SF)-INCR-PRE-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaIncrPre().setWtgaIncrPreNull(ws.getTrchDiGar().getTgaIncrPre().getTgaIncrPreNull());
        }
        else {
            // COB_CODE: MOVE TGA-INCR-PRE
            //             TO (SF)-INCR-PRE(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaIncrPre().setWtgaIncrPre(Trunc.toDecimal(ws.getTrchDiGar().getTgaIncrPre().getTgaIncrPre(), 15, 3));
        }
        // COB_CODE: IF TGA-INCR-PRSTZ-NULL = HIGH-VALUES
        //                TO (SF)-INCR-PRSTZ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-INCR-PRSTZ(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaIncrPrstz().getTgaIncrPrstzNullFormatted())) {
            // COB_CODE: MOVE TGA-INCR-PRSTZ-NULL
            //             TO (SF)-INCR-PRSTZ-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaIncrPrstz().setWtgaIncrPrstzNull(ws.getTrchDiGar().getTgaIncrPrstz().getTgaIncrPrstzNull());
        }
        else {
            // COB_CODE: MOVE TGA-INCR-PRSTZ
            //             TO (SF)-INCR-PRSTZ(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaIncrPrstz().setWtgaIncrPrstz(Trunc.toDecimal(ws.getTrchDiGar().getTgaIncrPrstz().getTgaIncrPrstz(), 15, 3));
        }
        // COB_CODE: IF TGA-DT-ULT-ADEG-PRE-PR-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-ADEG-PRE-PR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-DT-ULT-ADEG-PRE-PR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaDtUltAdegPrePr().getTgaDtUltAdegPrePrNullFormatted())) {
            // COB_CODE: MOVE TGA-DT-ULT-ADEG-PRE-PR-NULL
            //             TO (SF)-DT-ULT-ADEG-PRE-PR-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaDtUltAdegPrePr().setWtgaDtUltAdegPrePrNull(ws.getTrchDiGar().getTgaDtUltAdegPrePr().getTgaDtUltAdegPrePrNull());
        }
        else {
            // COB_CODE: MOVE TGA-DT-ULT-ADEG-PRE-PR
            //             TO (SF)-DT-ULT-ADEG-PRE-PR(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaDtUltAdegPrePr().setWtgaDtUltAdegPrePr(ws.getTrchDiGar().getTgaDtUltAdegPrePr().getTgaDtUltAdegPrePr());
        }
        // COB_CODE: IF TGA-PRSTZ-AGG-ULT-NULL = HIGH-VALUES
        //                TO (SF)-PRSTZ-AGG-ULT-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRSTZ-AGG-ULT(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPrstzAggUlt().getTgaPrstzAggUltNullFormatted())) {
            // COB_CODE: MOVE TGA-PRSTZ-AGG-ULT-NULL
            //             TO (SF)-PRSTZ-AGG-ULT-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaPrstzAggUlt().setWtgaPrstzAggUltNull(ws.getTrchDiGar().getTgaPrstzAggUlt().getTgaPrstzAggUltNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRSTZ-AGG-ULT
            //             TO (SF)-PRSTZ-AGG-ULT(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaPrstzAggUlt().setWtgaPrstzAggUlt(Trunc.toDecimal(ws.getTrchDiGar().getTgaPrstzAggUlt().getTgaPrstzAggUlt(), 15, 3));
        }
        // COB_CODE: IF TGA-TS-RIVAL-NET-NULL = HIGH-VALUES
        //                TO (SF)-TS-RIVAL-NET-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-TS-RIVAL-NET(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaTsRivalNet().getTgaTsRivalNetNullFormatted())) {
            // COB_CODE: MOVE TGA-TS-RIVAL-NET-NULL
            //             TO (SF)-TS-RIVAL-NET-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaTsRivalNet().setWtgaTsRivalNetNull(ws.getTrchDiGar().getTgaTsRivalNet().getTgaTsRivalNetNull());
        }
        else {
            // COB_CODE: MOVE TGA-TS-RIVAL-NET
            //             TO (SF)-TS-RIVAL-NET(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaTsRivalNet().setWtgaTsRivalNet(Trunc.toDecimal(ws.getTrchDiGar().getTgaTsRivalNet().getTgaTsRivalNet(), 14, 9));
        }
        // COB_CODE: IF TGA-PRE-PATTUITO-NULL = HIGH-VALUES
        //                TO (SF)-PRE-PATTUITO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-PATTUITO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPrePattuito().getTgaPrePattuitoNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-PATTUITO-NULL
            //             TO (SF)-PRE-PATTUITO-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaPrePattuito().setWtgaPrePattuitoNull(ws.getTrchDiGar().getTgaPrePattuito().getTgaPrePattuitoNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-PATTUITO
            //             TO (SF)-PRE-PATTUITO(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaPrePattuito().setWtgaPrePattuito(Trunc.toDecimal(ws.getTrchDiGar().getTgaPrePattuito().getTgaPrePattuito(), 15, 3));
        }
        // COB_CODE: IF TGA-TP-RIVAL-NULL = HIGH-VALUES
        //                TO (SF)-TP-RIVAL-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-TP-RIVAL(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaTpRivalFormatted())) {
            // COB_CODE: MOVE TGA-TP-RIVAL-NULL
            //             TO (SF)-TP-RIVAL-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().setWtgaTpRival(ws.getTrchDiGar().getTgaTpRival());
        }
        else {
            // COB_CODE: MOVE TGA-TP-RIVAL
            //             TO (SF)-TP-RIVAL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().setWtgaTpRival(ws.getTrchDiGar().getTgaTpRival());
        }
        // COB_CODE: IF TGA-RIS-MAT-NULL = HIGH-VALUES
        //                TO (SF)-RIS-MAT-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-RIS-MAT(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaRisMat().getTgaRisMatNullFormatted())) {
            // COB_CODE: MOVE TGA-RIS-MAT-NULL
            //             TO (SF)-RIS-MAT-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaRisMat().setWtgaRisMatNull(ws.getTrchDiGar().getTgaRisMat().getTgaRisMatNull());
        }
        else {
            // COB_CODE: MOVE TGA-RIS-MAT
            //             TO (SF)-RIS-MAT(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaRisMat().setWtgaRisMat(Trunc.toDecimal(ws.getTrchDiGar().getTgaRisMat().getTgaRisMat(), 15, 3));
        }
        // COB_CODE: IF TGA-CPT-MIN-SCAD-NULL = HIGH-VALUES
        //                TO (SF)-CPT-MIN-SCAD-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-CPT-MIN-SCAD(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaCptMinScad().getTgaCptMinScadNullFormatted())) {
            // COB_CODE: MOVE TGA-CPT-MIN-SCAD-NULL
            //             TO (SF)-CPT-MIN-SCAD-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaCptMinScad().setWtgaCptMinScadNull(ws.getTrchDiGar().getTgaCptMinScad().getTgaCptMinScadNull());
        }
        else {
            // COB_CODE: MOVE TGA-CPT-MIN-SCAD
            //             TO (SF)-CPT-MIN-SCAD(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaCptMinScad().setWtgaCptMinScad(Trunc.toDecimal(ws.getTrchDiGar().getTgaCptMinScad().getTgaCptMinScad(), 15, 3));
        }
        // COB_CODE: IF TGA-COMMIS-GEST-NULL = HIGH-VALUES
        //                TO (SF)-COMMIS-GEST-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-COMMIS-GEST(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaCommisGest().getTgaCommisGestNullFormatted())) {
            // COB_CODE: MOVE TGA-COMMIS-GEST-NULL
            //             TO (SF)-COMMIS-GEST-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaCommisGest().setWtgaCommisGestNull(ws.getTrchDiGar().getTgaCommisGest().getTgaCommisGestNull());
        }
        else {
            // COB_CODE: MOVE TGA-COMMIS-GEST
            //             TO (SF)-COMMIS-GEST(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaCommisGest().setWtgaCommisGest(Trunc.toDecimal(ws.getTrchDiGar().getTgaCommisGest().getTgaCommisGest(), 15, 3));
        }
        // COB_CODE: IF TGA-TP-MANFEE-APPL-NULL = HIGH-VALUES
        //                TO (SF)-TP-MANFEE-APPL-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-TP-MANFEE-APPL(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaTpManfeeApplFormatted())) {
            // COB_CODE: MOVE TGA-TP-MANFEE-APPL-NULL
            //             TO (SF)-TP-MANFEE-APPL-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().setWtgaTpManfeeAppl(ws.getTrchDiGar().getTgaTpManfeeAppl());
        }
        else {
            // COB_CODE: MOVE TGA-TP-MANFEE-APPL
            //             TO (SF)-TP-MANFEE-APPL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().setWtgaTpManfeeAppl(ws.getTrchDiGar().getTgaTpManfeeAppl());
        }
        // COB_CODE: MOVE TGA-DS-RIGA
        //             TO (SF)-DS-RIGA(IX-TAB-TGA)
        ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().setWtgaDsRiga(ws.getTrchDiGar().getTgaDsRiga());
        // COB_CODE: MOVE TGA-DS-OPER-SQL
        //             TO (SF)-DS-OPER-SQL(IX-TAB-TGA)
        ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().setWtgaDsOperSql(ws.getTrchDiGar().getTgaDsOperSql());
        // COB_CODE: MOVE TGA-DS-VER
        //             TO (SF)-DS-VER(IX-TAB-TGA)
        ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().setWtgaDsVer(ws.getTrchDiGar().getTgaDsVer());
        // COB_CODE: MOVE TGA-DS-TS-INI-CPTZ
        //             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-TGA)
        ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().setWtgaDsTsIniCptz(ws.getTrchDiGar().getTgaDsTsIniCptz());
        // COB_CODE: MOVE TGA-DS-TS-END-CPTZ
        //             TO (SF)-DS-TS-END-CPTZ(IX-TAB-TGA)
        ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().setWtgaDsTsEndCptz(ws.getTrchDiGar().getTgaDsTsEndCptz());
        // COB_CODE: MOVE TGA-DS-UTENTE
        //             TO (SF)-DS-UTENTE(IX-TAB-TGA)
        ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().setWtgaDsUtente(ws.getTrchDiGar().getTgaDsUtente());
        // COB_CODE: MOVE TGA-DS-STATO-ELAB
        //             TO (SF)-DS-STATO-ELAB(IX-TAB-TGA)
        ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().setWtgaDsStatoElab(ws.getTrchDiGar().getTgaDsStatoElab());
        // COB_CODE: IF TGA-PC-COMMIS-GEST-NULL = HIGH-VALUES
        //                TO (SF)-PC-COMMIS-GEST-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PC-COMMIS-GEST(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPcCommisGest().getTgaPcCommisGestNullFormatted())) {
            // COB_CODE: MOVE TGA-PC-COMMIS-GEST-NULL
            //             TO (SF)-PC-COMMIS-GEST-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaPcCommisGest().setWtgaPcCommisGestNull(ws.getTrchDiGar().getTgaPcCommisGest().getTgaPcCommisGestNull());
        }
        else {
            // COB_CODE: MOVE TGA-PC-COMMIS-GEST
            //             TO (SF)-PC-COMMIS-GEST(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaPcCommisGest().setWtgaPcCommisGest(Trunc.toDecimal(ws.getTrchDiGar().getTgaPcCommisGest().getTgaPcCommisGest(), 6, 3));
        }
        // COB_CODE: IF TGA-NUM-GG-RIVAL-NULL = HIGH-VALUES
        //                TO (SF)-NUM-GG-RIVAL-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-NUM-GG-RIVAL(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaNumGgRival().getTgaNumGgRivalNullFormatted())) {
            // COB_CODE: MOVE TGA-NUM-GG-RIVAL-NULL
            //             TO (SF)-NUM-GG-RIVAL-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaNumGgRival().setWtgaNumGgRivalNull(ws.getTrchDiGar().getTgaNumGgRival().getTgaNumGgRivalNull());
        }
        else {
            // COB_CODE: MOVE TGA-NUM-GG-RIVAL
            //             TO (SF)-NUM-GG-RIVAL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaNumGgRival().setWtgaNumGgRival(ws.getTrchDiGar().getTgaNumGgRival().getTgaNumGgRival());
        }
        // COB_CODE: IF TGA-IMP-TRASFE-NULL = HIGH-VALUES
        //                TO (SF)-IMP-TRASFE-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-TRASFE(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpTrasfe().getTgaImpTrasfeNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-TRASFE-NULL
            //             TO (SF)-IMP-TRASFE-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaImpTrasfe().setWtgaImpTrasfeNull(ws.getTrchDiGar().getTgaImpTrasfe().getTgaImpTrasfeNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-TRASFE
            //             TO (SF)-IMP-TRASFE(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaImpTrasfe().setWtgaImpTrasfe(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpTrasfe().getTgaImpTrasfe(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-TFR-STRC-NULL = HIGH-VALUES
        //                TO (SF)-IMP-TFR-STRC-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-TFR-STRC(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpTfrStrc().getTgaImpTfrStrcNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-TFR-STRC-NULL
            //             TO (SF)-IMP-TFR-STRC-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaImpTfrStrc().setWtgaImpTfrStrcNull(ws.getTrchDiGar().getTgaImpTfrStrc().getTgaImpTfrStrcNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-TFR-STRC
            //             TO (SF)-IMP-TFR-STRC(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaImpTfrStrc().setWtgaImpTfrStrc(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpTfrStrc().getTgaImpTfrStrc(), 15, 3));
        }
        // COB_CODE: IF TGA-ACQ-EXP-NULL = HIGH-VALUES
        //                TO (SF)-ACQ-EXP-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ACQ-EXP(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaAcqExp().getTgaAcqExpNullFormatted())) {
            // COB_CODE: MOVE TGA-ACQ-EXP-NULL
            //             TO (SF)-ACQ-EXP-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaAcqExp().setWtgaAcqExpNull(ws.getTrchDiGar().getTgaAcqExp().getTgaAcqExpNull());
        }
        else {
            // COB_CODE: MOVE TGA-ACQ-EXP
            //             TO (SF)-ACQ-EXP(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaAcqExp().setWtgaAcqExp(Trunc.toDecimal(ws.getTrchDiGar().getTgaAcqExp().getTgaAcqExp(), 15, 3));
        }
        // COB_CODE: IF TGA-REMUN-ASS-NULL = HIGH-VALUES
        //                TO (SF)-REMUN-ASS-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-REMUN-ASS(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaRemunAss().getTgaRemunAssNullFormatted())) {
            // COB_CODE: MOVE TGA-REMUN-ASS-NULL
            //             TO (SF)-REMUN-ASS-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaRemunAss().setWtgaRemunAssNull(ws.getTrchDiGar().getTgaRemunAss().getTgaRemunAssNull());
        }
        else {
            // COB_CODE: MOVE TGA-REMUN-ASS
            //             TO (SF)-REMUN-ASS(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaRemunAss().setWtgaRemunAss(Trunc.toDecimal(ws.getTrchDiGar().getTgaRemunAss().getTgaRemunAss(), 15, 3));
        }
        // COB_CODE: IF TGA-COMMIS-INTER-NULL = HIGH-VALUES
        //                TO (SF)-COMMIS-INTER-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-COMMIS-INTER(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaCommisInter().getTgaCommisInterNullFormatted())) {
            // COB_CODE: MOVE TGA-COMMIS-INTER-NULL
            //             TO (SF)-COMMIS-INTER-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaCommisInter().setWtgaCommisInterNull(ws.getTrchDiGar().getTgaCommisInter().getTgaCommisInterNull());
        }
        else {
            // COB_CODE: MOVE TGA-COMMIS-INTER
            //             TO (SF)-COMMIS-INTER(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaCommisInter().setWtgaCommisInter(Trunc.toDecimal(ws.getTrchDiGar().getTgaCommisInter().getTgaCommisInter(), 15, 3));
        }
        // COB_CODE: IF TGA-ALQ-REMUN-ASS-NULL = HIGH-VALUES
        //                TO (SF)-ALQ-REMUN-ASS-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ALQ-REMUN-ASS(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaAlqRemunAss().getTgaAlqRemunAssNullFormatted())) {
            // COB_CODE: MOVE TGA-ALQ-REMUN-ASS-NULL
            //             TO (SF)-ALQ-REMUN-ASS-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaAlqRemunAss().setWtgaAlqRemunAssNull(ws.getTrchDiGar().getTgaAlqRemunAss().getTgaAlqRemunAssNull());
        }
        else {
            // COB_CODE: MOVE TGA-ALQ-REMUN-ASS
            //             TO (SF)-ALQ-REMUN-ASS(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaAlqRemunAss().setWtgaAlqRemunAss(Trunc.toDecimal(ws.getTrchDiGar().getTgaAlqRemunAss().getTgaAlqRemunAss(), 6, 3));
        }
        // COB_CODE: IF TGA-ALQ-COMMIS-INTER-NULL = HIGH-VALUES
        //                TO (SF)-ALQ-COMMIS-INTER-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ALQ-COMMIS-INTER(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaAlqCommisInter().getTgaAlqCommisInterNullFormatted())) {
            // COB_CODE: MOVE TGA-ALQ-COMMIS-INTER-NULL
            //             TO (SF)-ALQ-COMMIS-INTER-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaAlqCommisInter().setWtgaAlqCommisInterNull(ws.getTrchDiGar().getTgaAlqCommisInter().getTgaAlqCommisInterNull());
        }
        else {
            // COB_CODE: MOVE TGA-ALQ-COMMIS-INTER
            //             TO (SF)-ALQ-COMMIS-INTER(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaAlqCommisInter().setWtgaAlqCommisInter(Trunc.toDecimal(ws.getTrchDiGar().getTgaAlqCommisInter().getTgaAlqCommisInter(), 6, 3));
        }
        // COB_CODE: IF TGA-IMPB-REMUN-ASS-NULL = HIGH-VALUES
        //                TO (SF)-IMPB-REMUN-ASS-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMPB-REMUN-ASS(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpbRemunAss().getTgaImpbRemunAssNullFormatted())) {
            // COB_CODE: MOVE TGA-IMPB-REMUN-ASS-NULL
            //             TO (SF)-IMPB-REMUN-ASS-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaImpbRemunAss().setWtgaImpbRemunAssNull(ws.getTrchDiGar().getTgaImpbRemunAss().getTgaImpbRemunAssNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMPB-REMUN-ASS
            //             TO (SF)-IMPB-REMUN-ASS(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaImpbRemunAss().setWtgaImpbRemunAss(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpbRemunAss().getTgaImpbRemunAss(), 15, 3));
        }
        // COB_CODE: IF TGA-IMPB-COMMIS-INTER-NULL = HIGH-VALUES
        //                TO (SF)-IMPB-COMMIS-INTER-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMPB-COMMIS-INTER(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpbCommisInter().getTgaImpbCommisInterNullFormatted())) {
            // COB_CODE: MOVE TGA-IMPB-COMMIS-INTER-NULL
            //             TO (SF)-IMPB-COMMIS-INTER-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaImpbCommisInter().setWtgaImpbCommisInterNull(ws.getTrchDiGar().getTgaImpbCommisInter().getTgaImpbCommisInterNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMPB-COMMIS-INTER
            //             TO (SF)-IMPB-COMMIS-INTER(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaImpbCommisInter().setWtgaImpbCommisInter(Trunc.toDecimal(ws.getTrchDiGar().getTgaImpbCommisInter().getTgaImpbCommisInter(), 15, 3));
        }
        // COB_CODE: IF TGA-COS-RUN-ASSVA-NULL = HIGH-VALUES
        //                TO (SF)-COS-RUN-ASSVA-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-COS-RUN-ASSVA(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaCosRunAssva().getTgaCosRunAssvaNullFormatted())) {
            // COB_CODE: MOVE TGA-COS-RUN-ASSVA-NULL
            //             TO (SF)-COS-RUN-ASSVA-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaCosRunAssva().setWtgaCosRunAssvaNull(ws.getTrchDiGar().getTgaCosRunAssva().getTgaCosRunAssvaNull());
        }
        else {
            // COB_CODE: MOVE TGA-COS-RUN-ASSVA
            //             TO (SF)-COS-RUN-ASSVA(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaCosRunAssva().setWtgaCosRunAssva(Trunc.toDecimal(ws.getTrchDiGar().getTgaCosRunAssva().getTgaCosRunAssva(), 15, 3));
        }
        // COB_CODE: IF TGA-COS-RUN-ASSVA-IDC-NULL = HIGH-VALUES
        //                TO (SF)-COS-RUN-ASSVA-IDC-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-COS-RUN-ASSVA-IDC(IX-TAB-TGA)
        //           END-IF.
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaCosRunAssvaIdc().getTgaCosRunAssvaIdcNullFormatted())) {
            // COB_CODE: MOVE TGA-COS-RUN-ASSVA-IDC-NULL
            //             TO (SF)-COS-RUN-ASSVA-IDC-NULL(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaCosRunAssvaIdc().setWtgaCosRunAssvaIdcNull(ws.getTrchDiGar().getTgaCosRunAssvaIdc().getTgaCosRunAssvaIdcNull());
        }
        else {
            // COB_CODE: MOVE TGA-COS-RUN-ASSVA-IDC
            //             TO (SF)-COS-RUN-ASSVA-IDC(IX-TAB-TGA)
            ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().getWtgaCosRunAssvaIdc().setWtgaCosRunAssvaIdc(Trunc.toDecimal(ws.getTrchDiGar().getTgaCosRunAssvaIdc().getTgaCosRunAssvaIdc(), 15, 3));
        }
    }

    /**Original name: INIZIA-TOT-TGA<br>
	 * <pre>------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    INIZIALIZZAZIONE CAMPI NULL COPY LCCVTGA4
	 *    ULTIMO AGG. 03 GIU 2019
	 * ------------------------------------------------------------</pre>*/
    private void iniziaTotTga() {
        // COB_CODE: PERFORM INIZIA-ZEROES-TGA THRU INIZIA-ZEROES-TGA-EX
        iniziaZeroesTga();
        // COB_CODE: PERFORM INIZIA-SPACES-TGA THRU INIZIA-SPACES-TGA-EX
        //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LCCVTGA4:line=12, because the code is unreachable.
        // COB_CODE: PERFORM INIZIA-NULL-TGA THRU INIZIA-NULL-TGA-EX.
        //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LCCVTGA4:line=14, because the code is unreachable.
    }

    /**Original name: INIZIA-ZEROES-TGA<br>*/
    private void iniziaZeroesTga() {
        // COB_CODE: MOVE 0 TO (SF)-ID-TRCH-DI-GAR(IX-TAB-TGA)
        ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().setWtgaIdTrchDiGar(0);
        // COB_CODE: MOVE 0 TO (SF)-ID-GAR(IX-TAB-TGA)
        ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().setWtgaIdGar(0);
        // COB_CODE: MOVE 0 TO (SF)-ID-ADES(IX-TAB-TGA)
        ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().setWtgaIdAdes(0);
        // COB_CODE: MOVE 0 TO (SF)-ID-POLI(IX-TAB-TGA)
        ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().setWtgaIdPoli(0);
        // COB_CODE: MOVE 0 TO (SF)-ID-MOVI-CRZ(IX-TAB-TGA)
        ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().setWtgaIdMoviCrz(0);
        // COB_CODE: MOVE 0 TO (SF)-DT-INI-EFF(IX-TAB-TGA)
        ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().setWtgaDtIniEff(0);
        // COB_CODE: MOVE 0 TO (SF)-DT-END-EFF(IX-TAB-TGA)
        ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().setWtgaDtEndEff(0);
        // COB_CODE: MOVE 0 TO (SF)-COD-COMP-ANIA(IX-TAB-TGA)
        ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().setWtgaCodCompAnia(0);
        // COB_CODE: MOVE 0 TO (SF)-DT-DECOR(IX-TAB-TGA)
        ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().setWtgaDtDecor(0);
        // COB_CODE: MOVE 0 TO (SF)-DS-RIGA(IX-TAB-TGA)
        ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().setWtgaDsRiga(0);
        // COB_CODE: MOVE 0 TO (SF)-DS-VER(IX-TAB-TGA)
        ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().setWtgaDsVer(0);
        // COB_CODE: MOVE 0 TO (SF)-DS-TS-INI-CPTZ(IX-TAB-TGA)
        ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().setWtgaDsTsIniCptz(0);
        // COB_CODE: MOVE 0 TO (SF)-DS-TS-END-CPTZ(IX-TAB-TGA).
        ws.getDtgaTabTran(ws.getIxTabTga()).getLccvtga1().getDati().setWtgaDsTsEndCptz(0);
    }

    public void initIxIndici() {
        ws.setIxDclgen(((short)0));
        ws.setIxTabTga(((short)0));
    }

    public void initTabOutput() {
        ivvc0213.getTabOutput().setCodVariabileO("");
        ivvc0213.getTabOutput().setTpDatoO(Types.SPACE_CHAR);
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        ivvc0213.getTabOutput().setValPercO(new AfDecimal(0, 14, 9));
        ivvc0213.getTabOutput().setValStrO("");
    }

    public void initLdbv1591() {
        ws.getLdbv1591().setIdAdes(0);
        ws.getLdbv1591().setIdOgg(0);
        ws.getLdbv1591().setTpOgg("");
        ws.getLdbv1591().setDtDecor(0);
        ws.getLdbv1591().setTpStatTit("");
        ws.getLdbv1591().setImpTot(new AfDecimal(0, 15, 3));
    }

    public void initLdbv3021() {
        ws.getLdbv3021().setIdOgg(0);
        ws.getLdbv3021().setTpOgg("");
        ws.getLdbv3021().setTpStatBus("");
        ws.getLdbv3021().setTpCausBus1("");
        ws.getLdbv3021().setTpCausBus2("");
        ws.getLdbv3021().setTpCausBus3("");
        ws.getLdbv3021().setTpCausBus4("");
        ws.getLdbv3021().setTpCausBus5("");
        ws.getLdbv3021().setTpCausBus6("");
        ws.getLdbv3021().setTpCausBus7("");
        ws.getLdbv3021().setTpCausBus8("");
        ws.getLdbv3021().setTpCausBus9("");
        ws.getLdbv3021().setTpCausBus10("");
        ws.getLdbv3021().setTpCausBus11("");
        ws.getLdbv3021().setTpCausBus12("");
        ws.getLdbv3021().setTpCausBus13("");
        ws.getLdbv3021().setTpCausBus14("");
        ws.getLdbv3021().setTpCausBus15("");
        ws.getLdbv3021().setTpCausBus16("");
        ws.getLdbv3021().setTpCausBus17("");
        ws.getLdbv3021().setTpCausBus18("");
        ws.getLdbv3021().setTpCausBus19("");
        ws.getLdbv3021().setTpCausBus20("");
    }

    public void initWkDataX12() {
        ws.getWkDataX12().setxAa("");
        ws.getWkDataX12().setVirogla(Types.SPACE_CHAR);
        ws.getWkDataX12().setxGg("");
    }

    public void initDettTitCont() {
        ws.getDettTitCont().setDtcIdDettTitCont(0);
        ws.getDettTitCont().setDtcIdTitCont(0);
        ws.getDettTitCont().setDtcIdOgg(0);
        ws.getDettTitCont().setDtcTpOgg("");
        ws.getDettTitCont().setDtcIdMoviCrz(0);
        ws.getDettTitCont().getDtcIdMoviChiu().setDtcIdMoviChiu(0);
        ws.getDettTitCont().setDtcDtIniEff(0);
        ws.getDettTitCont().setDtcDtEndEff(0);
        ws.getDettTitCont().setDtcCodCompAnia(0);
        ws.getDettTitCont().getDtcDtIniCop().setDtcDtIniCop(0);
        ws.getDettTitCont().getDtcDtEndCop().setDtcDtEndCop(0);
        ws.getDettTitCont().getDtcPreNet().setDtcPreNet(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcIntrFraz().setDtcIntrFraz(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcIntrMora().setDtcIntrMora(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcIntrRetdt().setDtcIntrRetdt(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcIntrRiat().setDtcIntrRiat(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcDir().setDtcDir(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcSpeMed().setDtcSpeMed(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcTax().setDtcTax(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcSoprSan().setDtcSoprSan(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcSoprSpo().setDtcSoprSpo(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcSoprTec().setDtcSoprTec(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcSoprProf().setDtcSoprProf(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcSoprAlt().setDtcSoprAlt(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcPreTot().setDtcPreTot(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcPrePpIas().setDtcPrePpIas(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcPreSoloRsh().setDtcPreSoloRsh(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcCarAcq().setDtcCarAcq(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcCarGest().setDtcCarGest(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcCarInc().setDtcCarInc(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcProvAcq1aa().setDtcProvAcq1aa(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcProvAcq2aa().setDtcProvAcq2aa(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcProvRicor().setDtcProvRicor(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcProvInc().setDtcProvInc(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcProvDaRec().setDtcProvDaRec(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().setDtcCodDvs("");
        ws.getDettTitCont().getDtcFrqMovi().setDtcFrqMovi(0);
        ws.getDettTitCont().setDtcTpRgmFisc("");
        ws.getDettTitCont().setDtcCodTari("");
        ws.getDettTitCont().setDtcTpStatTit("");
        ws.getDettTitCont().getDtcImpAz().setDtcImpAz(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcImpAder().setDtcImpAder(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcImpTfr().setDtcImpTfr(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcImpVolo().setDtcImpVolo(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcManfeeAntic().setDtcManfeeAntic(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcManfeeRicor().setDtcManfeeRicor(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcManfeeRec().setDtcManfeeRec(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcDtEsiTit().setDtcDtEsiTit(0);
        ws.getDettTitCont().getDtcSpeAge().setDtcSpeAge(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcCarIas().setDtcCarIas(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcTotIntrPrest().setDtcTotIntrPrest(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().setDtcDsRiga(0);
        ws.getDettTitCont().setDtcDsOperSql(Types.SPACE_CHAR);
        ws.getDettTitCont().setDtcDsVer(0);
        ws.getDettTitCont().setDtcDsTsIniCptz(0);
        ws.getDettTitCont().setDtcDsTsEndCptz(0);
        ws.getDettTitCont().setDtcDsUtente("");
        ws.getDettTitCont().setDtcDsStatoElab(Types.SPACE_CHAR);
        ws.getDettTitCont().getDtcImpTrasfe().setDtcImpTrasfe(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcImpTfrStrc().setDtcImpTfrStrc(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcNumGgRitardoPag().setDtcNumGgRitardoPag(0);
        ws.getDettTitCont().getDtcNumGgRival().setDtcNumGgRival(0);
        ws.getDettTitCont().getDtcAcqExp().setDtcAcqExp(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcRemunAss().setDtcRemunAss(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcCommisInter().setDtcCommisInter(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcCnbtAntirac().setDtcCnbtAntirac(new AfDecimal(0, 15, 3));
    }
}
