package it.accenture.jnais;

import com.bphx.ctu.af.core.buffer.BasicBytesClass;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.pointer.IPointerManager;
import com.bphx.ctu.af.core.program.DynamicCall;
import com.bphx.ctu.af.core.program.GenericParam;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.io.file.FileAccessStatus;
import com.bphx.ctu.af.io.file.FileBufferedDAO;
import com.bphx.ctu.af.io.file.FileRecordBuffer;
import com.bphx.ctu.af.io.file.OpenMode;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.lang.types.RoundingMode;
import com.bphx.ctu.af.lang.util.MathUtil;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.inspect.InspectPattern;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.batch.DdCard;
import com.modernsystems.ctu.core.impl.BatchProgram;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.AdesIvvs0216;
import it.accenture.jnais.copy.GarIvvs0216;
import it.accenture.jnais.copy.Idsi0011Area;
import it.accenture.jnais.copy.Ispc0040DatiInput;
import it.accenture.jnais.copy.Ispc0040DatiOutput1;
import it.accenture.jnais.copy.Ispc0040DatiOutput2;
import it.accenture.jnais.copy.Ispc0040OutCpiProtection;
import it.accenture.jnais.copy.Ivvc0211DatiInput;
import it.accenture.jnais.copy.Ivvc0211RestoDati;
import it.accenture.jnais.copy.Movi;
import it.accenture.jnais.copy.Poli;
import it.accenture.jnais.copy.TrchDiGarIvvs0216;
import it.accenture.jnais.copy.WadeDati;
import it.accenture.jnais.copy.WgrzDati;
import it.accenture.jnais.copy.WmovDati;
import it.accenture.jnais.copy.WpolDati;
import it.accenture.jnais.ws.AreaIdsv0001;
import it.accenture.jnais.ws.AreaIoIsps0040;
import it.accenture.jnais.ws.AreaIoLccs0023;
import it.accenture.jnais.ws.AreaMainLoas0310;
import it.accenture.jnais.ws.AreaPassaggio;
import it.accenture.jnais.ws.enums.Idso0011SqlcodeSigned;
import it.accenture.jnais.ws.enums.Idsv0001LivelloDebug;
import it.accenture.jnais.ws.Iabv0006;
import it.accenture.jnais.ws.Ieai9901Area;
import it.accenture.jnais.ws.Lccc0022AreaComunicaz;
import it.accenture.jnais.ws.Loas0310Data;
import it.accenture.jnais.ws.occurs.Ispc0040OperFormaReinvest;
import it.accenture.jnais.ws.occurs.Ispc0211TabErrori;
import it.accenture.jnais.ws.occurs.Ivvv0212CtrlAutOper;
import it.accenture.jnais.ws.occurs.Wk0040Tabella;
import it.accenture.jnais.ws.ptr.Idsv8888StrPerformanceDbg;
import it.accenture.jnais.ws.redefines.Ispc0040TabFondi;
import it.accenture.jnais.ws.redefines.WadeCumCnbtCap;
import it.accenture.jnais.ws.redefines.WadeDtDecor;
import it.accenture.jnais.ws.redefines.WadeDtDecorPrestBan;
import it.accenture.jnais.ws.redefines.WadeDtEffVarzStatT;
import it.accenture.jnais.ws.redefines.WadeDtNovaRgmFisc;
import it.accenture.jnais.ws.redefines.WadeDtPresc;
import it.accenture.jnais.ws.redefines.WadeDtScad;
import it.accenture.jnais.ws.redefines.WadeDtUltConsCnbt;
import it.accenture.jnais.ws.redefines.WadeDtVarzTpIas;
import it.accenture.jnais.ws.redefines.WadeDurAa;
import it.accenture.jnais.ws.redefines.WadeDurGg;
import it.accenture.jnais.ws.redefines.WadeDurMm;
import it.accenture.jnais.ws.redefines.WadeEtaAScad;
import it.accenture.jnais.ws.redefines.WadeIdMoviChiu;
import it.accenture.jnais.ws.redefines.WadeImpAder;
import it.accenture.jnais.ws.redefines.WadeImpAz;
import it.accenture.jnais.ws.redefines.WadeImpbVisDaRec;
import it.accenture.jnais.ws.redefines.WadeImpGarCnbt;
import it.accenture.jnais.ws.redefines.WadeImpRecRitAcc;
import it.accenture.jnais.ws.redefines.WadeImpRecRitVis;
import it.accenture.jnais.ws.redefines.WadeImpTfr;
import it.accenture.jnais.ws.redefines.WadeImpVolo;
import it.accenture.jnais.ws.redefines.WadeNumRatPian;
import it.accenture.jnais.ws.redefines.WadePcAder;
import it.accenture.jnais.ws.redefines.WadePcAz;
import it.accenture.jnais.ws.redefines.WadePcTfr;
import it.accenture.jnais.ws.redefines.WadePcVolo;
import it.accenture.jnais.ws.redefines.WadePreLrdInd;
import it.accenture.jnais.ws.redefines.WadePreNetInd;
import it.accenture.jnais.ws.redefines.WadePrstzIniInd;
import it.accenture.jnais.ws.redefines.WadeRatLrdInd;
import it.accenture.jnais.ws.redefines.WcntTabVar;
import it.accenture.jnais.ws.redefines.WgrzAaPagPreUni;
import it.accenture.jnais.ws.redefines.WgrzAaStab;
import it.accenture.jnais.ws.redefines.WgrzDtDecor;
import it.accenture.jnais.ws.redefines.WgrzDtEndCarz;
import it.accenture.jnais.ws.redefines.WgrzDtIniValTar;
import it.accenture.jnais.ws.redefines.WgrzDtPresc;
import it.accenture.jnais.ws.redefines.WgrzDtScad;
import it.accenture.jnais.ws.redefines.WgrzDtVarzTpIas;
import it.accenture.jnais.ws.redefines.WgrzDurAa;
import it.accenture.jnais.ws.redefines.WgrzDurGg;
import it.accenture.jnais.ws.redefines.WgrzDurMm;
import it.accenture.jnais.ws.redefines.WgrzEtaAa1oAssto;
import it.accenture.jnais.ws.redefines.WgrzEtaAa2oAssto;
import it.accenture.jnais.ws.redefines.WgrzEtaAa3oAssto;
import it.accenture.jnais.ws.redefines.WgrzEtaAScad;
import it.accenture.jnais.ws.redefines.WgrzEtaMm1oAssto;
import it.accenture.jnais.ws.redefines.WgrzEtaMm2oAssto;
import it.accenture.jnais.ws.redefines.WgrzEtaMm3oAssto;
import it.accenture.jnais.ws.redefines.WgrzFrazDecrCpt;
import it.accenture.jnais.ws.redefines.WgrzFrazIniErogRen;
import it.accenture.jnais.ws.redefines.WgrzId1oAssto;
import it.accenture.jnais.ws.redefines.WgrzId2oAssto;
import it.accenture.jnais.ws.redefines.WgrzId3oAssto;
import it.accenture.jnais.ws.redefines.WgrzIdAdes;
import it.accenture.jnais.ws.redefines.WgrzIdMoviChiu;
import it.accenture.jnais.ws.redefines.WgrzMm1oRat;
import it.accenture.jnais.ws.redefines.WgrzMmPagPreUni;
import it.accenture.jnais.ws.redefines.WgrzNumAaPagPre;
import it.accenture.jnais.ws.redefines.WgrzPc1oRat;
import it.accenture.jnais.ws.redefines.WgrzPcOpz;
import it.accenture.jnais.ws.redefines.WgrzPcRevrsb;
import it.accenture.jnais.ws.redefines.WgrzPcRipPre;
import it.accenture.jnais.ws.redefines.WgrzTpGar;
import it.accenture.jnais.ws.redefines.WgrzTpInvst;
import it.accenture.jnais.ws.redefines.WgrzTsStabLimitata;
import it.accenture.jnais.ws.redefines.WkTabVar;
import it.accenture.jnais.ws.redefines.WmovIdMoviAnn;
import it.accenture.jnais.ws.redefines.WmovIdMoviCollg;
import it.accenture.jnais.ws.redefines.WmovIdOgg;
import it.accenture.jnais.ws.redefines.WmovIdRich;
import it.accenture.jnais.ws.redefines.WmovTpMovi;
import it.accenture.jnais.ws.redefines.WpolAaDiffProrDflt;
import it.accenture.jnais.ws.redefines.WpolDir1oVers;
import it.accenture.jnais.ws.redefines.WpolDirEmis;
import it.accenture.jnais.ws.redefines.WpolDirQuiet;
import it.accenture.jnais.ws.redefines.WpolDirVersAgg;
import it.accenture.jnais.ws.redefines.WpolDtApplzConv;
import it.accenture.jnais.ws.redefines.WpolDtIniVldtConv;
import it.accenture.jnais.ws.redefines.WpolDtPresc;
import it.accenture.jnais.ws.redefines.WpolDtProp;
import it.accenture.jnais.ws.redefines.WpolDtScad;
import it.accenture.jnais.ws.redefines.WpolDurAa;
import it.accenture.jnais.ws.redefines.WpolDurGg;
import it.accenture.jnais.ws.redefines.WpolDurMm;
import it.accenture.jnais.ws.redefines.WpolIdAccComm;
import it.accenture.jnais.ws.redefines.WpolIdMoviChiu;
import it.accenture.jnais.ws.redefines.WpolSpeMed;
import it.accenture.jnais.ws.redefines.WskdTabValP;
import it.accenture.jnais.ws.redefines.WskdTabValT;
import it.accenture.jnais.ws.redefines.WtgaTabLoas0310;
import it.accenture.jnais.ws.VtgaAreaTranche;
import it.accenture.jnais.ws.WadeAreaAdesioneLoas0800;
import it.accenture.jnais.ws.WcomIntervalloElab;
import it.accenture.jnais.ws.WkGestioneMsgErr;
import it.accenture.jnais.ws.WmovAreaMovimento;
import it.accenture.jnais.ws.WorkCommarea;
import it.accenture.jnais.ws.WpolAreaPolizzaLccs0005;
import it.accenture.jnais.ws.WskdAreaScheda;
import javax.inject.Inject;
import static java.lang.Math.abs;

/**Original name: LOAS0310<br>
 * <pre>  ============================================================= *
 *                                                                 *
 *         PORTAFOGLIO VITA ITALIA  VER 1.0                        *
 *                                                                 *
 *   ============================================================= *
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       SETTEMBRE 2007.
 * DATE-COMPILED.
 * ----------------------------------------------------------------*
 *      PROGRAMMA ..... LOAS0310                                   *
 *      TIPOLOGIA...... OPERAZIONI AUTOMATICHE                     *
 *      PROCESSO....... ADEGUAMENTO PREMIO PRESTAZIONE             *
 *      FUNZIONE....... BATCH                                      *
 *      DESCRIZIONE.... MODULO DI SECONDO LIVELLO (ELABORAZIONE)   *
 *      PAGINA WEB..... N.A.                                       *
 * ----------------------------------------------------------------*</pre>*/
public class Loas0310 extends BatchProgram {

    //==== PROPERTIES ====
    @Inject
    private IPointerManager pointerManager;
    public FileRecordBuffer outrivaTo = new FileRecordBuffer(Len.OUTRIVA_REC);
    public FileBufferedDAO outrivaDAO = new FileBufferedDAO(new FileAccessStatus(), "OUTRIVA", Len.OUTRIVA_REC);
    //Original name: WORKING-STORAGE
    private Loas0310Data ws = new Loas0310Data();
    //Original name: AREA-IDSV0001
    private AreaIdsv0001 areaIdsv0001;
    //Original name: IABV0006
    private Iabv0006 iabv0006;
    //Original name: AREA-MAIN
    private AreaMainLoas0310 areaMain;
    //Original name: WCOM-INTERVALLO-ELAB
    private WcomIntervalloElab wcomIntervalloElab;
    //Original name: WCOM-IO-STATI
    private AreaPassaggio wcomIoStati;

    //==== METHODS ====
    /**Original name: PROGRAM_LOAS0310_FIRST_SENTENCES<br>*/
    public long execute(AreaIdsv0001 areaIdsv0001, Iabv0006 iabv0006, AreaMainLoas0310 areaMain, WcomIntervalloElab wcomIntervalloElab, AreaPassaggio wcomIoStati) {
        this.areaIdsv0001 = areaIdsv0001;
        this.iabv0006 = iabv0006;
        this.areaMain = areaMain;
        this.wcomIntervalloElab = wcomIntervalloElab;
        this.wcomIoStati = wcomIoStati;
        // COB_CODE: IF IABV0006-ULTIMO-LANCIO
        //                 THRU S90200-CLOSE-OUT-EX
        //           ELSE
        //                 THRU S90000-OPERAZ-FINALI-EX
        //           END-IF.
        if (this.iabv0006.getTipoLancioBus().isIabv0006UltimoLancio()) {
            // COB_CODE: PERFORM S90200-CLOSE-OUT
            //              THRU S90200-CLOSE-OUT-EX
            s90200CloseOut();
        }
        else {
            // COB_CODE: PERFORM S00000-OPERAZ-INIZIALI
            //              THRU S00000-OPERAZ-INIZIALI-EX
            s00000OperazIniziali();
            //
            // COB_CODE:         IF IDSV0001-ESITO-OK
            //                   AND WCOM-WRITE-OK
            //           *
            //                         THRU S10000-ELABORAZIONE-EX
            //           *
            //                   END-IF
            if (this.areaIdsv0001.getEsito().isIdsv0001EsitoOk() && this.wcomIoStati.getLccc0261().getWrite().isWcomWriteOk()) {
                //
                // COB_CODE: PERFORM S10000-ELABORAZIONE
                //              THRU S10000-ELABORAZIONE-EX
                s10000Elaborazione();
                //
            }
            //
            // COB_CODE: PERFORM S90000-OPERAZ-FINALI
            //              THRU S90000-OPERAZ-FINALI-EX
            s90000OperazFinali();
        }
        //
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Loas0310 getInstance() {
        return ((Loas0310)Programs.getInstance(Loas0310.class));
    }

    /**Original name: S00000-OPERAZ-INIZIALI<br>
	 * <pre> ============================================================== *
	 *  -->           O P E R Z I O N I   I N I Z I A L I          <-- *
	 *  ============================================================== *
	 *     DISPLAY IDSV0001-LIVELLO-DEBUG</pre>*/
    private void s00000OperazIniziali() {
        // COB_CODE: MOVE 'S00000-OPERAZ-INIZIALI'   TO WK-LABEL-ERR
        ws.getWkGestioneMsgErr().setLabelErr("S00000-OPERAZ-INIZIALI");
        //
        //    INITIALIZE                         WCNT-AREA-DATI-CONTEST
        //
        //     Inizializzazione area dati contestuali
        //
        // COB_CODE: INITIALIZE                         WCNT-ELE-VAR-CONT-MAX
        //                                              WCNT-TAB-VAR-CONT(1).
        ws.getIvvc0212().setWcntEleVarContMax(((short)0));
        initWcntTabVarCont();
        // COB_CODE: MOVE  WCNT-TAB-VAR              TO WCNT-RESTO-TAB-VAR.
        ws.getIvvc0212().getWcntTabVar().setWcntRestoTabVar(ws.getIvvc0212().getWcntTabVar().getWcntTabVarFormatted());
        //    SET NO-GAR-REND                 TO TRUE.
        //    SET NO-GAR-REND                 TO TRUE
        // COB_CODE: MOVE WPMO-ID-OGG(1)
        //             TO IABV0006-OGG-BUSINESS
        iabv0006.getReport().setOggBusiness(areaMain.getWpmoTabParamMov(1).getLccvpmo1().getDati().getWpmoIdOggFormatted());
        // COB_CODE: MOVE WPMO-TP-OGG(1)
        //             TO IABV0006-TP-OGG-BUSINESS
        iabv0006.getReport().setTpOggBusiness(areaMain.getWpmoTabParamMov(1).getLccvpmo1().getDati().getWpmoTpOgg());
        // COB_CODE: MOVE WPMO-ID-POLI(1)
        //             TO IABV0006-ID-POLI.
        iabv0006.getReport().setIdPoli(areaMain.getWpmoTabParamMov(1).getLccvpmo1().getDati().getWpmoIdPoliFormatted());
        // COB_CODE: MOVE WPMO-ID-ADES(1)
        //             TO IABV0006-ID-ADES.
        iabv0006.getReport().setIdAdes(areaMain.getWpmoTabParamMov(1).getLccvpmo1().getDati().getWpmoIdAdes().getWpmoIdAdesFormatted());
        // COB_CODE: MOVE WPMO-COD-RAMO(1)
        //             TO ISPV0000-COD-RAMO.
        ws.getIspv0000().getCodRamo().setCodRamo(areaMain.getWpmoTabParamMov(1).getLccvpmo1().getDati().getWpmoCodRamo());
        // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
        //             TO IABV0006-DT-EFF-BUSINESS.
        iabv0006.getReport().setDtEffBusiness(areaIdsv0001.getAreaComune().getIdsv0001DataEffettoFormatted());
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // --> Inizializzazione dell'area comune
        //
        // COB_CODE: PERFORM S00090-INIZ-AREA-COMUNE
        //              THRU S00090-INIZ-AREA-COMUNE-EX.
        s00090InizAreaComune();
        // --> Inizializazione dell'area errori di tipo deroga
        //
        // COB_CODE: PERFORM INIZIA-AREA-ERR-DEROGA
        //              THRU INIZIA-AREA-ERR-DEROGA-EX.
        iniziaAreaErrDeroga();
        //
        // --> Inizializazione delle aree di working storage
        //
        // COB_CODE: PERFORM S00200-INIZIA-AREE-WS
        //              THRU S00200-INIZIA-AREE-WS-EX.
        s00200IniziaAreeWs();
        //
        // --> Inizializazione di tutte le tabelle che verranno scritte
        //
        // COB_CODE: PERFORM S00300-INIZ-AREE-TABELLE
        //              THRU S00300-INIZ-AREE-TABELLE-EX.
        s00300InizAreeTabelle();
        //
        // COB_CODE:      IF ALPO-CALL
        //           *
        //                      THRU S00340-TRATTA-MOVI-EX
        //           *
        //                ELSE
        //           *
        //                      THRU S00350-CREA-MOVI-FITTIZIO-EX
        //                END-IF.
        if (wcomIoStati.getLccc0261().getBsCallType().isAlpoCall()) {
            //
            // COB_CODE: PERFORM S00340-TRATTA-MOVI
            //              THRU S00340-TRATTA-MOVI-EX
            s00340TrattaMovi();
            //
        }
        else {
            //
            // COB_CODE: PERFORM S00350-CREA-MOVI-FITTIZIO
            //              THRU S00350-CREA-MOVI-FITTIZIO-EX
            s00350CreaMoviFittizio();
        }
        //
        // --> CONTROLLI
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                      THRU S00400-CONTROLLI-EX
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE: PERFORM S00400-CONTROLLI
            //              THRU S00400-CONTROLLI-EX
            s00400Controlli();
            //
        }
        //
        // COB_CODE:      IF IABV0006-PRIMO-LANCIO
        //           *
        //                   INITIALIZE WK-AREA-DATI-PROD
        //           *
        //                END-IF.
        if (iabv0006.getTipoLancioBus().isIabv0006PrimoLancio()) {
            //
            // COB_CODE: INITIALIZE WK-AREA-DATI-PROD
            initWkAreaDatiProd();
            //
        }
        //
        // --> Determinazione della Data Validit&#x17; Prodotto
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //                AND WCOM-WRITE-OK
        //           *
        //                      THRU S00600-GESTIONE-DATA-PROD-EX
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk() && wcomIoStati.getLccc0261().getWrite().isWcomWriteOk()) {
            //
            // COB_CODE: PERFORM S00600-GESTIONE-DATA-PROD
            //              THRU S00600-GESTIONE-DATA-PROD-EX
            s00600GestioneDataProd();
        }
        //
        // --> Determinazione del tipo di Rivalutazione:
        // --> "Per Incasso" o "Per Emesso"
        //
        // COB_CODE: IF IDSV0001-ESITO-OK
        //           AND WCOM-WRITE-OK
        //                 THRU S00700-DETERMINA-ORIG-RIV-EX
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk() && wcomIoStati.getLccc0261().getWrite().isWcomWriteOk()) {
            // COB_CODE: PERFORM S00700-DETERMINA-ORIG-RIV
            //              THRU S00700-DETERMINA-ORIG-RIV-EX
            s00700DeterminaOrigRiv();
        }
        //
        // --> Apertura del file di output
        //
        // COB_CODE:      IF IABV0006-PRIMO-LANCIO
        //           *
        //                      THRU S00800-OPEN-OUT-EX
        //                END-IF.
        if (iabv0006.getTipoLancioBus().isIabv0006PrimoLancio()) {
            //
            // COB_CODE: PERFORM S00800-OPEN-OUT
            //              THRU S00800-OPEN-OUT-EX
            s00800OpenOut();
        }
        //
        // COB_CODE: PERFORM S00900-VALOR-IB-OGG-MOV
        //              THRU S00900-VALOR-IB-OGG-MOV-EX.
        s00900ValorIbOggMov();
        //
        // COB_CODE: PERFORM DISPLAY-IB-OGG
        //              THRU DISPLAY-IB-OGG-EX.
        displayIbOgg();
        //
        // COB_CODE: MOVE WPMO-TP-FRM-ASSVA(1)
        //             TO WS-TP-FRM-ASSVA.
        ws.getWsTpFrmAssva().setWsTpFrmAssva(areaMain.getWpmoTabParamMov(1).getLccvpmo1().getDati().getWpmoTpFrmAssva());
    }

    /**Original name: S00340-TRATTA-MOVI<br>
	 * <pre>----------------------------------------------------------------*
	 *        LETTURA MOVIMENTO
	 * ----------------------------------------------------------------*</pre>*/
    private void s00340TrattaMovi() {
        // COB_CODE: MOVE 'S00340-TRATTA-MOVI'   TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S00340-TRATTA-MOVI");
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        // COB_CODE: PERFORM S00342-PREPARA-MOVI
        //              THRU S00342-PREPARA-MOVI-EX.
        s00342PreparaMovi();
        // COB_CODE: PERFORM S00344-LEGGI-MOVIMENTO
        //              THRU S00344-LEGGI-MOVIMENTO-EX.
        s00344LeggiMovimento();
    }

    /**Original name: S00342-PREPARA-MOVI<br>
	 * <pre>----------------------------------------------------------------*
	 *  PREPARA AREA LETTURA MOVIMENTO
	 * ----------------------------------------------------------------*</pre>*/
    private void s00342PreparaMovi() {
        // COB_CODE: MOVE 'S00342-PREPARA-MOVI'   TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S00342-PREPARA-MOVI");
        // COB_CODE: PERFORM DISPLAY-LABEL THRU DISPLAY-LABEL-EX.
        displayLabel();
        // COB_CODE: INITIALIZE                       MOVI.
        initMovi();
        // COB_CODE: SET IDSI0011-SELECT           TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC    TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
        // COB_CODE: SET IDSI0011-PRIMARY-KEY      TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setPrimaryKey();
        // COB_CODE: SET IDSI0011-TRATT-SENZA-STOR TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattSenzaStor();
        // COB_CODE: MOVE WCOM-ID-MOVI-CREAZ       TO MOV-ID-MOVI.
        ws.getMovi().setMovIdMovi(wcomIoStati.getLccc0261().getIdMoviCreaz());
        // COB_CODE: MOVE 'MOVI'                   TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("MOVI");
        // COB_CODE: MOVE MOVI                     TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getMovi().getMoviFormatted());
        // COB_CODE: MOVE SPACES                   TO IDSI0011-BUFFER-WHERE-COND.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferWhereCond("");
        // COB_CODE: MOVE ZERO                     TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                            IDSI0011-DATA-FINE-EFFETTO
        //                                            IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
    }

    /**Original name: S00344-LEGGI-MOVIMENTO<br>
	 * <pre>----------------------------------------------------------------*
	 *  LETTURA MOVIMENTO                                              *
	 * ----------------------------------------------------------------*</pre>*/
    private void s00344LeggiMovimento() {
        // COB_CODE: MOVE 'S00344-LEGGI-MOVIMENTO'  TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S00344-LEGGI-MOVIMENTO");
        // COB_CODE: PERFORM DISPLAY-LABEL THRU DISPLAY-LABEL-EX.
        displayLabel();
        // COB_CODE: MOVE 'MOVI'          TO WK-TABELLA.
        ws.setWkTabella("MOVI");
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX.
        callDispatcher();
        // COB_CODE:         IF IDSO0011-SUCCESSFUL-RC
        //                      END-EVALUATE
        //                   ELSE
        //           *-->       GESTIRE ERRORE DISPATCHER
        //                         THRU EX-S0300
        //                   END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            // COB_CODE:           EVALUATE TRUE
            //                          WHEN IDSO0011-SUCCESSFUL-SQL
            //           *-->       OPERAZIONE ESEGUITA CORRETTAMENTE
            //                              TO TRUE
            //                          WHEN IDSO0011-NOT-FOUND
            //           *--->          CAMPO $ NON TROVATO
            //                                   THRU EX-S0300
            //                           WHEN OTHER
            //           *--->          ERRORE DI ACCESSO AL DB
            //                                THRU EX-S0300
            //                      END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://-->       OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: MOVE IDSO0011-BUFFER-DATI
                    //             TO MOVI
                    ws.getMovi().setMoviFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    // COB_CODE: MOVE 1
                    //             TO WMOV-ELE-MOVI-MAX
                    areaMain.getWmovAreaMovimento().setWmovEleMovMax(((short)1));
                    // COB_CODE: PERFORM VALORIZZA-OUTPUT-MOV
                    //              THRU VALORIZZA-OUTPUT-MOV-EX
                    valorizzaOutputMov();
                    // COB_CODE: SET WMOV-ST-INV
                    //            TO TRUE
                    areaMain.getWmovAreaMovimento().getLccvmov1().getStatus().setInv();
                    break;

                case Idso0011SqlcodeSigned.NOT_FOUND://--->          CAMPO $ NON TROVATO
                    // COB_CODE: MOVE WK-PGM
                    //            TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE WK-LABEL-ERR
                    //             TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
                    // COB_CODE: MOVE '005019'
                    //             TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005019");
                    // COB_CODE: MOVE 'ID-MOVI'
                    //             TO IEAI9901-PARAMETRI-ERR
                    ws.getIeai9901Area().setParametriErr("ID-MOVI");
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;

                default://--->          ERRORE DI ACCESSO AL DB
                    // COB_CODE: MOVE WK-PGM
                    //                           TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE WK-LABEL-ERR
                    //                           TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
                    // COB_CODE: MOVE '005015'   TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005015");
                    // COB_CODE: MOVE WK-TABELLA TO IEAI9901-PARAMETRI-ERR
                    ws.getIeai9901Area().setParametriErr(ws.getWkTabella());
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;
            }
        }
        else {
            //-->       GESTIRE ERRORE DISPATCHER
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL-ERR
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
            // COB_CODE: MOVE '005016'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: MOVE SPACES
            //             TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: S00350-CREA-MOVI-FITTIZIO<br>
	 * <pre>----------------------------------------------------------------*
	 *                     CREAZIONE DEL MOVIMENTO FITTIZIO            *
	 * ----------------------------------------------------------------*</pre>*/
    private void s00350CreaMoviFittizio() {
        // COB_CODE: MOVE 'S00350-CREA-MOVI-FITTIZIO'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S00350-CREA-MOVI-FITTIZIO");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // COB_CODE: SET WMOV-ST-ADD
        //             TO TRUE.
        areaMain.getWmovAreaMovimento().getLccvmov1().getStatus().setWcomStAdd();
        //
        // COB_CODE: MOVE 1
        //             TO WMOV-ELE-MOVI-MAX
        areaMain.getWmovAreaMovimento().setWmovEleMovMax(((short)1));
        //
        // COB_CODE: MOVE 1
        //             TO WMOV-ID-MOVI.
        areaMain.getWmovAreaMovimento().getLccvmov1().getDati().setWmovIdMovi(1);
        //
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //             TO WMOV-COD-COMP-ANIA.
        areaMain.getWmovAreaMovimento().getLccvmov1().getDati().setWmovCodCompAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        //
        // COB_CODE:      IF WPMO-TP-FRM-ASSVA(1) = 'IN'
        //           *
        //                     TO WMOV-TP-OGG
        //           *
        //                ELSE
        //           *
        //                     TO WMOV-TP-OGG
        //           *
        //                END-IF.
        if (Conditions.eq(areaMain.getWpmoTabParamMov(1).getLccvpmo1().getDati().getWpmoTpFrmAssva(), "IN")) {
            //
            // COB_CODE: MOVE WPMO-ID-POLI(1)
            //             TO WMOV-ID-OGG
            areaMain.getWmovAreaMovimento().getLccvmov1().getDati().getWmovIdOgg().setWmovIdOgg(areaMain.getWpmoTabParamMov(1).getLccvpmo1().getDati().getWpmoIdPoli());
            // COB_CODE: MOVE 'PO'
            //             TO WMOV-TP-OGG
            areaMain.getWmovAreaMovimento().getLccvmov1().getDati().setWmovTpOgg("PO");
            //
        }
        else {
            //
            // COB_CODE: MOVE WPMO-ID-ADES(1)
            //             TO WMOV-ID-OGG
            areaMain.getWmovAreaMovimento().getLccvmov1().getDati().getWmovIdOgg().setWmovIdOgg(areaMain.getWpmoTabParamMov(1).getLccvpmo1().getDati().getWpmoIdAdes().getWpmoIdAdes());
            // COB_CODE: MOVE 'AD'
            //             TO WMOV-TP-OGG
            areaMain.getWmovAreaMovimento().getLccvmov1().getDati().setWmovTpOgg("AD");
            //
        }
        //
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO
        //             TO WMOV-TP-MOVI.
        areaMain.getWmovAreaMovimento().getLccvmov1().getDati().getWmovTpMovi().setWmovTpMovi(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimento());
        //
        // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
        //             TO WMOV-DT-EFF.
        areaMain.getWmovAreaMovimento().getLccvmov1().getDati().setWmovDtEff(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
        //
        // COB_CODE: IF ISPV0000-IN-CC-ASSICURATIVO
        //              CONTINUE
        //           ELSE
        //                TO WMOV-ID-RICH
        //           END-IF.
        if (ws.getIspv0000().getCodRamo().isInCcAssicurativo()) {
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IABV0006-ID-RICH
            //             TO WMOV-ID-RICH
            areaMain.getWmovAreaMovimento().getLccvmov1().getDati().getWmovIdRich().setWmovIdRich(iabv0006.getIdRich());
        }
    }

    /**Original name: S00090-INIZ-AREA-COMUNE<br>
	 * <pre>----------------------------------------------------------------*
	 *  INIZIALIZZAZIONE AREA COMUNE                                   *
	 * ----------------------------------------------------------------*
	 *  --> Valorizzazione Id-Adesione e Id-Polizza dell'area
	 *  --> passaggio al Main</pre>*/
    private void s00090InizAreaComune() {
        // COB_CODE: MOVE WPMO-ID-POLI(1)
        //             TO WCOM-ID-POLIZZA.
        wcomIoStati.getLccc0261().getDatiPolizza().setIdPolizza(areaMain.getWpmoTabParamMov(1).getLccvpmo1().getDati().getWpmoIdPoli());
        //
        // COB_CODE: MOVE WPMO-ID-ADES(1)
        //             TO WCOM-ID-ADES.
        wcomIoStati.getLccc0261().setIdAdes(areaMain.getWpmoTabParamMov(1).getLccvpmo1().getDati().getWpmoIdAdes().getWpmoIdAdes());
        //
        // COB_CODE: MOVE WPMO-TP-FRM-ASSVA(1)
        //             TO WCOM-TP-FRM-ASSVA.
        wcomIoStati.getLccc0261().getDatiMbs().getWcomTpFrmAssva().setWcomTpFrmAssva(areaMain.getWpmoTabParamMov(1).getLccvpmo1().getDati().getWpmoTpFrmAssva());
        //
        // COB_CODE: MOVE 1
        //             TO WCOM-STEP-ELAB.
        wcomIoStati.getLccc0261().getDatiMbs().setWcomStepElabFormatted("1");
    }

    /**Original name: S00800-OPEN-OUT<br>
	 * <pre>----------------------------------------------------------------*
	 *                APERTURA DEL FILE DI OUTPUT OUTRIVA              *
	 * ----------------------------------------------------------------*</pre>*/
    private void s00800OpenOut() {
        // COB_CODE: OPEN OUTPUT OUTRIVA.
        outrivaDAO.open(OpenMode.WRITE, "Loas0310");
        ws.setFsOut(outrivaDAO.getFileStatus().getStatusCodeFormatted());
        //
        // COB_CODE:      IF FS-OUT NOT = '00'
        //           *
        //                      THRU GESTIONE-ERR-STD-EX
        //           *
        //                END-IF.
        if (!Conditions.eq(ws.getFsOut(), "00")) {
            //
            // COB_CODE: MOVE 'S00800-OPEN-OUT'
            //             TO WK-LABEL-ERR
            ws.getWkGestioneMsgErr().setLabelErr("S00800-OPEN-OUT");
            // COB_CODE: MOVE 'ERRORE APERTURA FILE OUTPUT OUTRIVA'
            //             TO WK-STRING
            ws.getWkGestioneMsgErr().setStringFld("ERRORE APERTURA FILE OUTPUT OUTRIVA");
            // COB_CODE: MOVE '001114'
            //             TO WK-COD-ERR
            ws.getWkGestioneMsgErr().setCodErr("001114");
            // COB_CODE: PERFORM GESTIONE-ERR-STD
            //              THRU GESTIONE-ERR-STD-EX
            gestioneErrStd();
            //
        }
    }

    /**Original name: S00900-VALOR-IB-OGG-MOV<br>
	 * <pre>----------------------------------------------------------------*
	 *      INIZIALIZZAZIONE DELLE AREE DI WORKING STORAGE             *
	 * ----------------------------------------------------------------*</pre>*/
    private void s00900ValorIbOggMov() {
        // COB_CODE:      IF WPMO-TP-FRM-ASSVA(1) = 'IN'
        //           *
        //                   END-IF
        //           *
        //                ELSE
        //           *
        //                   END-IF
        //           *
        //                END-IF.
        if (Conditions.eq(areaMain.getWpmoTabParamMov(1).getLccvpmo1().getDati().getWpmoTpFrmAssva(), "IN")) {
            //
            // COB_CODE:         IF WPOL-ELE-POLI-MAX > 0
            //           *
            //                        TO WMOV-IB-OGG
            //           *
            //                   END-IF
            if (areaMain.getWpolAreaPolizza().getWpolElePoliMax() > 0) {
                //
                // COB_CODE: MOVE WPOL-IB-OGG
                //             TO WMOV-IB-OGG
                areaMain.getWmovAreaMovimento().getLccvmov1().getDati().setWmovIbOgg(areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolIbOgg());
                //
            }
            //
        }
        else {
            //
            // COB_CODE:         IF WADE-ELE-ADES-MAX > 0
            //           *
            //                        TO WMOV-IB-OGG
            //           *
            //                   END-IF
            if (areaMain.getWadeAreaAdesione().getEleAdesMax() > 0) {
                //
                // COB_CODE: MOVE WADE-IB-OGG(1)
                //             TO WMOV-IB-OGG
                areaMain.getWmovAreaMovimento().getLccvmov1().getDati().setWmovIbOgg(areaMain.getWadeAreaAdesione().getTabAde(1).getLccvade1().getDati().getWadeIbOgg());
                //
            }
            //
        }
    }

    /**Original name: S00200-INIZIA-AREE-WS<br>
	 * <pre>----------------------------------------------------------------*
	 *      INIZIALIZZAZIONE DELLE AREE DI WORKING STORAGE             *
	 * ----------------------------------------------------------------*
	 *   Inizializazione di tutti gli indici e le aree di WS
	 *      INITIALIZE WK-VARIABILI.</pre>*/
    private void s00200IniziaAreeWs() {
        // COB_CODE: SET  IDSV8888-BUSINESS-DBG          TO TRUE
        ws.getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
        // COB_CODE: MOVE 'LOAS0310'                 TO IDSV8888-NOME-PGM.
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("LOAS0310");
        // COB_CODE: SET  IDSV8888-INIZIO            TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setInizio();
        // COB_CODE: MOVE SPACES                     TO IDSV8888-DESC-PGM
        //skipped translation for moving SPACES to IDSV8888-DESC-PGM; considered in STRING statement translation below
        // COB_CODE: STRING 'Iniz. aree di working'
        //                   DELIMITED BY SIZE
        //                   INTO IDSV8888-DESC-PGM
        //           END-STRING
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("Iniz. aree di working");
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: INITIALIZE WORK-COMMAREA
        //                      IX-INDICI.
        initWorkCommarea();
        initIxIndici();
        //                AREA-IO-ISPS0040.
        //
        //    INIZIALIZZAZIONE AREA WK-VARIABILI
        //
        // COB_CODE: INITIALIZE WK-ELE-LIVELLO-MAX
        //                      WK-COD-COMP-ANIA(1)
        //                      WK-TP-LIVELLO   (1)
        //                      WK-COD-LIVELLO  (1)
        //                      WK-ID-LIVELLO   (1)
        //                      WK-ELE-VARIABILI-MAX(1)
        //                      WK-NOME-SERVIZIO(1).
        ws.getLccc0211().setWkEleLivelloMax(((short)0));
        ws.getLccc0211().getWkTabVar().setCodCompAniaFormatted(1, "00000");
        ws.getLccc0211().getWkTabVar().setTpLivello(1, Types.SPACE_CHAR);
        ws.getLccc0211().getWkTabVar().setCodLivello(1, "");
        ws.getLccc0211().getWkTabVar().setIdLivelloFormatted(1, "000000000");
        ws.getLccc0211().getWkTabVar().setEleVariabiliMax(1, ((short)0));
        ws.getLccc0211().getWkTabVar().setNomeServizio(1, "");
        // COB_CODE: PERFORM VARYING IX-WK-VAR FROM 1 BY 1
        //             UNTIL IX-WK-VAR > WK-MAX-VAR
        //             INITIALIZE   WK-TAB-VARIABILI(1,IX-WK-VAR)
        //           END-PERFORM.
        ws.getIxIndici().setWkVar(((short)1));
        while (!(ws.getIxIndici().getWkVar() > ws.getWkCostantiTabelle().getMaxVar())) {
            // COB_CODE: INITIALIZE   WK-TAB-VARIABILI(1,IX-WK-VAR)
            initTabVariabili();
            ws.getIxIndici().setWkVar(Trunc.toShort(ws.getIxIndici().getWkVar() + 1, 4));
        }
        // COB_CODE: MOVE WK-TAB-VAR       TO WK-RESTO-TAB-VAR.
        ws.getLccc0211().getWkTabVar().setRestoTabVar(ws.getLccc0211().getWkTabVar().getWkTabVarFormatted());
        //
        //   Arrotolamento copy Servizio Dati Prodotto
        //
        // COB_CODE: INITIALIZE            ISPC0040-DATI-INPUT
        //                                 ISPC0040-DATI-OUTPUT1
        //                                 ISPC0040-DATI-OUTPUT2
        //                                 ISPC0040-AREA-ERRORI
        //                                 ISPC0040-COD-FONDI(1)
        //                                 ISPC0040-DESC-FOND(1).
        initIspc0040DatiInput();
        initIspc0040DatiOutput1();
        initIspc0040DatiOutput2();
        initIspc0040AreaErrori();
        ws.getAreaIoIsps0040().getIspc0040TabFondi().setIspc0040CodFondi(1, "");
        ws.getAreaIoIsps0040().getIspc0040TabFondi().setIspc0040DescFond(1, "");
        // COB_CODE: MOVE ISPC0040-TAB-FONDI  TO ISPC0040-RESTO-TAB-FONDI.
        ws.getAreaIoIsps0040().getIspc0040TabFondi().setIspc0040RestoTabFondi(ws.getAreaIoIsps0040().getIspc0040TabFondi().getIspc0040TabFondiFormatted());
        // COB_CODE: SET  IDSV8888-BUSINESS-DBG          TO TRUE
        ws.getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
        // COB_CODE: MOVE 'LOAS0310'                 TO IDSV8888-NOME-PGM.
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("LOAS0310");
        // COB_CODE: SET  IDSV8888-FINE              TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setFine();
        // COB_CODE: MOVE SPACES                     TO IDSV8888-DESC-PGM
        //skipped translation for moving SPACES to IDSV8888-DESC-PGM; considered in STRING statement translation below
        // COB_CODE: STRING 'Iniz. aree di working'
        //                   DELIMITED BY SIZE
        //                   INTO IDSV8888-DESC-PGM
        //           END-STRING
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("Iniz. aree di working");
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: SET  IDSV0001-ESITO-OK
        //             TO TRUE.
        areaIdsv0001.getEsito().setIdsv0001EsitoOk();
        // COB_CODE: SET  WCOM-WRITE-OK
        //             TO TRUE.
        wcomIoStati.getLccc0261().getWrite().setWcomWriteOk();
        // COB_CODE: MOVE 99991231
        //             TO WK-DT-EFF-BLC
        ws.setWkDtEffBlc(99991231);
        // COB_CODE: MOVE 00000000
        //             TO WK-APPO-OGB-ID-OGG-BLOCCO
        ws.setWkAppoOgbIdOggBlocco(0);
        // COB_CODE: SET WK-BLC-TROVATO-NO
        //             TO TRUE
        ws.getWkBlcTrovato().setNo();
        //
        // COB_CODE: MOVE WPMO-AREA-PARAM-MOVI
        //             TO VPMO-AREA-PARAM-MOVI.
        ws.getWorkCommarea().setAreaParamMoviBytes(areaMain.getWpmoAreaParamMoviBytes());
        //
        // COB_CODE: MOVE WPMO-ELE-PARAM-MOV-MAX
        //             TO VPMO-ELE-PARAM-MOV-MAX.
        ws.getWorkCommarea().setEleParamMovMax(areaMain.getWpmoEleParamMovMax());
    }

    /**Original name: S00300-INIZ-AREE-TABELLE<br>
	 * <pre>----------------------------------------------------------------*
	 *      INIZIALIZZAZIONE DELLE TABELLE CHE VERRANNO SCRITTE        *
	 * ----------------------------------------------------------------*</pre>*/
    private void s00300InizAreeTabelle() {
        // COB_CODE: MOVE ZEROES TO WMOV-ELE-MOVI-MAX
        //                          WPOL-ELE-POLI-MAX
        //                          WADE-ELE-ADES-MAX
        //                          WGRZ-ELE-GAR-MAX
        //                          WTGA-ELE-TRAN-MAX
        //                          VGRZ-ELE-GAR-MAX
        //                          VTGA-ELE-TRAN-MAX.
        areaMain.getWmovAreaMovimento().setWmovEleMovMax(((short)0));
        areaMain.getWpolAreaPolizza().setWpolElePoliMax(((short)0));
        areaMain.getWadeAreaAdesione().setEleAdesMax(((short)0));
        ws.getWgrzAreaGaranzia().setEleGarMax(((short)0));
        ws.getWtgaAreaTranche().setWtgaEleTranMax(((short)0));
        ws.setVgrzEleGarMax(((short)0));
        ws.getVtgaAreaTranche().setVtgaEleTranMax(((short)0));
        //
        // --> Inizializzazione Area Movimento
        //
        // COB_CODE: PERFORM INIZIA-TOT-MOV
        //              THRU INIZIA-TOT-MOV-EX.
        iniziaTotMov();
        //
        // --> Inizializzazione Area Polizza
        //
        // COB_CODE: PERFORM INIZIA-TOT-POL
        //              THRU INIZIA-TOT-POL-EX.
        iniziaTotPol();
        //
        // --> Inizializzazione Area Adesione
        //
        // COB_CODE: PERFORM INIZIA-TOT-ADE
        //              THRU INIZIA-TOT-ADE-EX
        //           VARYING IX-TAB-ADE FROM 1 BY 1
        //             UNTIL IX-TAB-ADE > WK-MAX-ADE
        ws.getIxIndici().setTabAde(((short)1));
        while (!(ws.getIxIndici().getTabAde() > ws.getWkCostantiTabelle().getMaxAde())) {
            iniziaTotAde();
            ws.getIxIndici().setTabAde(Trunc.toShort(ws.getIxIndici().getTabAde() + 1, 4));
        }
        // COB_CODE: SET  IDSV8888-BUSINESS-DBG          TO TRUE
        ws.getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
        // COB_CODE: MOVE 'LOAS0310'                 TO IDSV8888-NOME-PGM.
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("LOAS0310");
        // COB_CODE: SET  IDSV8888-INIZIO            TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setInizio();
        // COB_CODE: MOVE SPACES                     TO IDSV8888-DESC-PGM
        //skipped translation for moving SPACES to IDSV8888-DESC-PGM; considered in STRING statement translation below
        // COB_CODE: STRING 'Iniz. tab. working TGA'
        //                   DELIMITED BY SIZE
        //                   INTO IDSV8888-DESC-PGM
        //           END-STRING
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("Iniz. tab. working TGA");
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //    INITIALIZE             WGRZ-AREA-GARANZIA
        // COB_CODE: PERFORM INIZIA-TOT-GRZ
        //              THRU INIZIA-TOT-GRZ-EX
        //           VARYING IX-TAB-GRZ FROM 1 BY 1
        //             UNTIL IX-TAB-GRZ > WGRZ-ELE-GAR-MAX.
        ws.getIxIndici().setTabGrz(((short)1));
        while (!(ws.getIxIndici().getTabGrz() > ws.getWgrzAreaGaranzia().getEleGarMax())) {
            iniziaTotGrz();
            ws.getIxIndici().setTabGrz(Trunc.toShort(ws.getIxIndici().getTabGrz() + 1, 4));
        }
        // COB_CODE: MOVE WGRZ-AREA-GARANZIA TO VGRZ-AREA-GARANZIA.
        ws.setVgrzAreaGaranziaBytes(ws.getWgrzAreaGaranzia().getWgrzAreaGaranziaBytes());
        //--  INIZIALIZZAZIONE AREA TRANCHE DI GARANZIA
        // COB_CODE: INITIALIZE             WTGA-TAB-TRAN(1).
        initTabTran();
        // COB_CODE: INITIALIZE             WPREC-AREA-TRANCHE.
        initWprecAreaTranche();
        // COB_CODE: MOVE  WTGA-TAB         TO WTGA-RESTO-TAB.
        ws.getWtgaAreaTranche().getWtgaTab().setRestoTab(ws.getWtgaAreaTranche().getWtgaTab().getWtgaTabFormatted());
        // COB_CODE: SET  IDSV8888-BUSINESS-DBG          TO TRUE
        ws.getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
        // COB_CODE: MOVE 'LOAS0310'                 TO IDSV8888-NOME-PGM.
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("LOAS0310");
        // COB_CODE: SET  IDSV8888-FINE              TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setFine();
        // COB_CODE: MOVE SPACES                     TO IDSV8888-DESC-PGM
        //skipped translation for moving SPACES to IDSV8888-DESC-PGM; considered in STRING statement translation below
        // COB_CODE: STRING 'Iniz. tab. working GRZ'
        //                   DELIMITED BY SIZE
        //                   INTO IDSV8888-DESC-PGM
        //           END-STRING
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("Iniz. tab. working GRZ");
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //--  INIZIALIZZAZIONE AREA TRANCHE DI GARANZIA VALORIZZATORE
        // COB_CODE: INITIALIZE             VTGA-TAB-TRAN(1).
        initTabTran1();
        // COB_CODE: MOVE  VTGA-TAB         TO VTGA-RESTO-TAB.
        ws.getVtgaAreaTranche().getVtgaTab().setRestoTab(ws.getVtgaAreaTranche().getVtgaTab().getVtgaTabFormatted());
        //
        // COB_CODE: INITIALIZE IX-INDICI.
        initIxIndici();
        //
        // COB_CODE: MOVE ZEROES
        //             TO IX-X-RIV-INC.
        ws.getIxIndici().setxRivInc(((short)0));
    }

    /**Original name: S00400-CONTROLLI<br>
	 * <pre> ============================================================== *
	 *  -->                     C O N T R O L L I                  <-- *
	 *  ============================================================== *</pre>*/
    private void s00400Controlli() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'S00400-CONTROLLI'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S00400-CONTROLLI");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //           *---> Verifichiamo che il movimento non sia stato gi  eseguito
        //                   END-IF
        //           *
        //                END-IF
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            //---> Verifichiamo che il movimento non sia stato gi&#x17; eseguito
            // COB_CODE: PERFORM S00390-CHIAMA-LDBS2200
            //              THRU S00390-CHIAMA-LDBS2200-EX
            s00390ChiamaLdbs2200();
            // COB_CODE:         IF IDSV0001-ESITO-OK
            //           * --> Se wcom-write-niente, forzatura su idsv0001-esito-ko
            //           * --> per skippare tutta l'elaborazione
            //                      END-IF
            //                   END-IF
            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                // --> Se wcom-write-niente, forzatura su idsv0001-esito-ko
                // --> per skippare tutta l'elaborazione
                // COB_CODE: IF MOVIM-NOT-FOUND
                //              CONTINUE
                //           ELSE
                //                 THRU EX-S0300
                //           END-IF
                if (!ws.isFlMovFound()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET WCOM-WRITE-NIENTE
                    //            TO TRUE
                    wcomIoStati.getLccc0261().getWrite().setWcomWriteNiente();
                    //             SET IDSV0001-ESITO-KO
                    //              TO TRUE
                    // COB_CODE: MOVE WK-PGM
                    //             TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE WK-LABEL-ERR
                    //             TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
                    // COB_CODE: MOVE '005247'
                    //             TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005247");
                    // COB_CODE: MOVE WPMO-ID-POLI(1)
                    //             TO WK-ID-POLI-DISPLAY
                    ws.setWkIdPoliDisplay(TruncAbs.toInt(areaMain.getWpmoTabParamMov(1).getLccvpmo1().getDati().getWpmoIdPoli(), 9));
                    // COB_CODE: STRING 'POLIZZA ' WK-ID-POLI-DISPLAY
                    //                  ' NON ELABORATA PER: '
                    //                  'MOVIMENTO GIA'' ESEGUITO'
                    //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "POLIZZA ", ws.getWkIdPoliDisplayAsString(), " NON ELABORATA PER: ", "MOVIMENTO GIA' ESEGUITO");
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                }
            }
            //
        }
        //
        // COB_CODE:      IF  IDSV0001-ESITO-OK
        //                AND WCOM-WRITE-OK
        //           *
        //                      THRU S00440-LEGGI-POLIZZA-EX
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk() && wcomIoStati.getLccc0261().getWrite().isWcomWriteOk()) {
            //
            // COB_CODE: PERFORM S00440-LEGGI-POLIZZA
            //              THRU S00440-LEGGI-POLIZZA-EX
            s00440LeggiPolizza();
            //
        }
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //                AND WCOM-WRITE-OK
        //           *
        //                      THRU S00410-CTRL-STATO-E-CAUS-EX
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk() && wcomIoStati.getLccc0261().getWrite().isWcomWriteOk()) {
            //
            // COB_CODE: PERFORM S00410-CTRL-STATO-E-CAUS
            //              THRU S00410-CTRL-STATO-E-CAUS-EX
            s00410CtrlStatoECaus();
            //
        }
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //                AND WCOM-WRITE-OK
        //           *
        //                      THRU S00460-LEGGI-ADESIONE-EX
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk() && wcomIoStati.getLccc0261().getWrite().isWcomWriteOk()) {
            //
            // COB_CODE: PERFORM S00460-LEGGI-ADESIONE
            //              THRU S00460-LEGGI-ADESIONE-EX
            s00460LeggiAdesione();
            //
        }
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //                AND WCOM-WRITE-OK
        //           *
        //                     TO IABV0006-IB-OGG-ADES
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk() && wcomIoStati.getLccc0261().getWrite().isWcomWriteOk()) {
            //
            // COB_CODE: MOVE WPOL-IB-OGG
            //             TO IABV0006-IB-OGG-POLI
            iabv0006.getReport().setIbOggPoli(areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolIbOgg());
            // COB_CODE: MOVE WADE-IB-OGG(1)
            //             TO IABV0006-IB-OGG-ADES
            iabv0006.getReport().setIbOggAdes(areaMain.getWadeAreaAdesione().getTabAde(1).getLccvade1().getDati().getWadeIbOgg());
            //
        }
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //                AND WCOM-WRITE-OK
        //           *
        //                      THRU S00570-CTRL-TP-DT-RIVAL-EX
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk() && wcomIoStati.getLccc0261().getWrite().isWcomWriteOk()) {
            //
            // COB_CODE: PERFORM S00570-CTRL-TP-DT-RIVAL
            //              THRU S00570-CTRL-TP-DT-RIVAL-EX
            s00570CtrlTpDtRival();
            //
        }
    }

    /**Original name: S00390-CHIAMA-LDBS2200<br>
	 * <pre>----------------------------------------------------------------*
	 *     ESTRAIAMO DALL'ENTITA' MOVI
	 * ----------------------------------------------------------------*</pre>*/
    private void s00390ChiamaLdbs2200() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'S00390-CHIAMA-LDBS2200'
        //             TO WK-LABEL-ERR
        ws.getWkGestioneMsgErr().setLabelErr("S00390-CHIAMA-LDBS2200");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // COB_CODE: INITIALIZE                       MOVI.
        initMovi();
        //--> DATA EFFETTO
        // COB_CODE: MOVE ZEROES                   TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                            IDSI0011-DATA-FINE-EFFETTO
        //                                            IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        //--> TRATTAMENTO STORICITA'
        // COB_CODE: SET IDSI0011-TRATT-SENZA-STOR TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattSenzaStor();
        //--> MODALITA' DI ACCESSO
        // COB_CODE: SET IDSI0011-WHERE-CONDITION  TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setWhereCondition();
        //--> Valorizziamo DCLGEN TABELLA
        // COB_CODE: MOVE WPMO-ID-ADES(1)          TO MOV-ID-OGG
        ws.getMovi().getMovIdOgg().setMovIdOgg(areaMain.getWpmoTabParamMov(1).getLccvpmo1().getDati().getWpmoIdAdes().getWpmoIdAdes());
        // COB_CODE: MOVE 'AD'                     TO MOV-TP-OGG
        ws.getMovi().setMovTpOgg("AD");
        // COB_CODE: MOVE '6006'                   TO MOV-TP-MOVI
        ws.getMovi().getMovTpMovi().setMovTpMoviFormatted("6006");
        //--> NOME TABELLA FISICA DB
        // COB_CODE: MOVE 'LDBS2200'               TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("LDBS2200");
        //--> DCLGEN TABELLA
        // COB_CODE: MOVE SPACES                   TO IDSI0011-BUFFER-WHERE-COND.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferWhereCond("");
        // COB_CODE: MOVE MOVI                     TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getMovi().getMoviFormatted());
        //--> TIPO OPERAZIONE
        // COB_CODE: SET  IDSI0011-FETCH-FIRST     TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setFetchFirst();
        //--> INIZIALIZZAZIONE FLAGS
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC    TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX
        callDispatcher();
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //                   END-EVALUATE
        //                ELSE
        //           *-->    ERRORE DISPATCHER
        //                      THRU EX-S0300
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            // COB_CODE:         EVALUATE TRUE
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //           *-->        OPERAZIONE ESEGUITA CORRETTAMENTE
            //                             THRU S00395-CLOSE-CUR-LDBS2200-EX
            //                       WHEN IDSO0011-NOT-FOUND
            //           *-->        CHIAVE NON TROVATA
            //                          SET MOVIM-NOT-FOUND    TO TRUE
            //                       WHEN OTHER
            //           *-->        ERRORE DB
            //                             THRU EX-S0300
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://-->        OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: MOVE IDSO0011-BUFFER-DATI
                    //             TO MOVI
                    ws.getMovi().setMoviFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    //-->           Controlliamo DT_EFF
                    // COB_CODE: IF MOV-DT-EFF = IDSV0001-DATA-EFFETTO
                    //              SET MOVIM-FOUND     TO TRUE
                    //           ELSE
                    //              SET MOVIM-NOT-FOUND TO TRUE
                    //           END-IF
                    if (ws.getMovi().getMovDtEff() == areaIdsv0001.getAreaComune().getIdsv0001DataEffetto()) {
                        // COB_CODE: SET MOVIM-FOUND     TO TRUE
                        ws.setFlMovFound(true);
                    }
                    else {
                        // COB_CODE: SET MOVIM-NOT-FOUND TO TRUE
                        ws.setFlMovFound(false);
                    }
                    // COB_CODE: PERFORM S00395-CLOSE-CUR-LDBS2200
                    //              THRU S00395-CLOSE-CUR-LDBS2200-EX
                    s00395CloseCurLdbs2200();
                    break;

                case Idso0011SqlcodeSigned.NOT_FOUND://-->        CHIAVE NON TROVATA
                    // COB_CODE: SET MOVIM-NOT-FOUND    TO TRUE
                    ws.setFlMovFound(false);
                    break;

                default://-->        ERRORE DB
                    // COB_CODE: MOVE WK-PGM
                    //             TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'S00390-CHIAMA-LDBS2200'
                    //             TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("S00390-CHIAMA-LDBS2200");
                    // COB_CODE: MOVE '005016'
                    //             TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005016");
                    // COB_CODE: STRING 'MOVI'             ';'
                    //                IDSO0011-RETURN-CODE ';'
                    //                IDSO0011-SQLCODE
                    //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "MOVI", ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;
            }
        }
        else {
            //-->    ERRORE DISPATCHER
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S00390-CHIAMA-LDBS2200'
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S00390-CHIAMA-LDBS2200");
            // COB_CODE: MOVE '005016'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING 'MOVI'               ';'
            //                  IDSO0011-RETURN-CODE ';'
            //                  IDSO0011-SQLCODE
            //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "MOVI", ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: S00395-CLOSE-CUR-LDBS2200<br>
	 * <pre>----------------------------------------------------------------*
	 *     CHIUSURA DEL CURSORE
	 * ----------------------------------------------------------------*</pre>*/
    private void s00395CloseCurLdbs2200() {
        ConcatUtil concatUtil = null;
        // COB_CODE: SET IDSI0011-CLOSE-CURSOR     TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setIdsv0003CloseCursor();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC    TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
        // COB_CODE: SET IDSI0011-WHERE-CONDITION  TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setWhereCondition();
        // COB_CODE: SET IDSI0011-TRATT-SENZA-STOR TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattSenzaStor();
        // COB_CODE: INITIALIZE                       MOVI.
        initMovi();
        // COB_CODE: MOVE 'LDBS2200'               TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("LDBS2200");
        // COB_CODE: MOVE SPACES                   TO IDSI0011-BUFFER-WHERE-COND.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferWhereCond("");
        // COB_CODE: MOVE SPACES                   TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
        // COB_CODE: MOVE ZEROES                   TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                            IDSI0011-DATA-FINE-EFFETTO
        //                                            IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX.
        callDispatcher();
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //                   END-EVALUATE
        //                ELSE
        //           *-->    ERRORE DISPATCHER
        //                      THRU EX-S0300
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            // COB_CODE:         EVALUATE TRUE
            //                      WHEN IDSO0011-SUCCESSFUL-SQL
            //           *-->       OPERAZIONE ESEGUITA CORRETTAMENTE
            //                         CONTINUE
            //                      WHEN OTHER
            //           *--->      ERRORE DI ACCESSO AL DB
            //                            THRU EX-S0300
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://-->       OPERAZIONE ESEGUITA CORRETTAMENTE
                // COB_CODE: CONTINUE
                //continue
                    break;

                default://--->      ERRORE DI ACCESSO AL DB
                    // COB_CODE: MOVE WK-PGM
                    //             TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'S00395-CLOSE-CUR-LDBS2200'
                    //             TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("S00395-CLOSE-CUR-LDBS2200");
                    // COB_CODE: MOVE '005016'
                    //             TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005016");
                    // COB_CODE: STRING 'MOVI'               ';'
                    //                  IDSO0011-RETURN-CODE ';'
                    //                  IDSO0011-SQLCODE
                    //                  DELIMITED BY SIZE INTO
                    //                  IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "MOVI", ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;
            }
        }
        else {
            //-->    ERRORE DISPATCHER
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S00395-CLOSE-CUR-LDBS2200'
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S00395-CLOSE-CUR-LDBS2200");
            // COB_CODE: MOVE '005016'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING 'MOVI'               ';'
            //                  IDSO0011-RETURN-CODE ';'
            //                  IDSO0011-SQLCODE
            //                  DELIMITED BY SIZE INTO
            //                  IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "MOVI", ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: S00410-CTRL-STATO-E-CAUS<br>
	 * <pre>----------------------------------------------------------------*
	 *         CONTROLLA LO STATO OGGETTO BUSINESS DELLA POLIZZA       *
	 * ----------------------------------------------------------------*</pre>*/
    private void s00410CtrlStatoECaus() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'S00410-CTRL-STATO-E-CAUS'
        //             TO WK-LABEL-ERR
        ws.getWkGestioneMsgErr().setLabelErr("S00410-CTRL-STATO-E-CAUS");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        //  Controllo Stato Contratto
        //
        // COB_CODE: MOVE WPMO-ID-ADES(1)
        //             TO WK-STB-ID-OGG.
        ws.setWkStbIdOgg(areaMain.getWpmoTabParamMov(1).getLccvpmo1().getDati().getWpmoIdAdes().getWpmoIdAdes());
        // COB_CODE: MOVE 'AD'
        //             TO WK-STB-TP-OGG.
        ws.setWkStbTpOgg("AD");
        //
        // COB_CODE: PERFORM S00420-IMPOSTA-STB
        //              THRU S00420-IMPOSTA-STB-EX.
        s00420ImpostaStb();
        //
        // COB_CODE: PERFORM S00430-LEGGI-STB
        //              THRU S00430-LEGGI-STB-EX.
        s00430LeggiStb();
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                   END-IF
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE: MOVE STB-TP-STAT-BUS
            //             TO WS-TP-STAT-BUS
            ws.getWsTpStatBus().setWsTpStatBus(ws.getStatOggBus().getStbTpStatBus());
            // COB_CODE: MOVE STB-TP-CAUS
            //             TO WS-TP-CAUS
            ws.getWsTpCaus().setWsTpCaus(ws.getStatOggBus().getStbTpCaus());
            //
            // --> Se wcom-write-niente, forzatura su idsv0001-esito-ko
            // --> per skippare tutta l'elaborazione
            //
            // COB_CODE:         IF IN-VIGORE
            //           *
            //                      CONTINUE
            //           *
            //                   ELSE
            //           *
            //                         THRU EX-S0300
            //           *
            //                   END-IF
            if (ws.getWsTpStatBus().isInVigore()) {
            //
            // COB_CODE: CONTINUE
            //continue
            //
            }
            else {
                //
                // COB_CODE: SET WCOM-WRITE-NIENTE
                //            TO TRUE
                wcomIoStati.getLccc0261().getWrite().setWcomWriteNiente();
                //          SET IDSV0001-ESITO-KO
                //           TO TRUE
                // COB_CODE: MOVE WK-PGM
                //             TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE WK-LABEL-ERR
                //             TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
                // COB_CODE: MOVE '005247'
                //             TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("005247");
                // COB_CODE: MOVE WPOL-IB-OGG(1:11)
                //             TO WK-IB-POLI-DISPLAY
                ws.setWkIbPoliDisplay(areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolIbOggFormatted().substring((1) - 1, 11));
                // COB_CODE: STRING 'POLIZZA ' WK-IB-POLI-DISPLAY
                //                  ' NON ELABORATA PER: '
                //                  'ADESIONE NON IN VIGORE'
                //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "POLIZZA ", ws.getWkIbPoliDisplayFormatted(), " NON ELABORATA PER: ", "ADESIONE NON IN VIGORE");
                ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
                //
            }
            //
        }
    }

    /**Original name: S00420-IMPOSTA-STB<br>
	 * <pre>----------------------------------------------------------------*
	 *   VALORIZZAZIONI PER LETTURA STATO OGGETTO BUSINESS
	 * ----------------------------------------------------------------*</pre>*/
    private void s00420ImpostaStb() {
        // COB_CODE: INITIALIZE STAT-OGG-BUS.
        initStatOggBus();
        //
        // COB_CODE: MOVE WK-STB-ID-OGG
        //             TO STB-ID-OGG.
        ws.getStatOggBus().setStbIdOgg(ws.getWkStbIdOgg());
        // COB_CODE: MOVE WK-STB-TP-OGG
        //             TO STB-TP-OGG.
        ws.getStatOggBus().setStbTpOgg(ws.getWkStbTpOgg());
        //
        // COB_CODE: SET WK-STB-TROV-NO
        //             TO TRUE.
        ws.getWkStbTrovato().setNo();
        //
        // COB_CODE: MOVE ZEROES
        //             TO IDSI0011-DATA-INIZIO-EFFETTO
        //                IDSI0011-DATA-FINE-EFFETTO
        //                IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        //
        // COB_CODE: SET IDSI0011-TRATT-DEFAULT
        //            TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setIdsi0011TrattDefault();
        //
        // COB_CODE: MOVE 'STAT-OGG-BUS'
        //             TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("STAT-OGG-BUS");
        //
        // COB_CODE: MOVE STAT-OGG-BUS
        //             TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getStatOggBus().getStatOggBusFormatted());
        //
        // COB_CODE: SET IDSI0011-SELECT
        //            TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        // COB_CODE: SET IDSI0011-ID-OGGETTO
        //            TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setIdsi0011IdOggetto();
        //
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC
        //            TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL
        //            TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
    }

    /**Original name: S00430-LEGGI-STB<br>
	 * <pre>----------------------------------------------------------------*
	 *   LETTURA DELLA TABELLA STATO OGGETTO BUSINESS
	 * ----------------------------------------------------------------*</pre>*/
    private void s00430LeggiStb() {
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX.
        callDispatcher();
        //
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //           *
        //                   END-EVALUATE
        //           *
        //                ELSE
        //           *
        //           *  --> Errore Dispatcher
        //           *
        //                      THRU GESTIONE-ERR-STD-EX
        //           *
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            //
            //
            // COB_CODE:         EVALUATE TRUE
            //           *
            //                       WHEN IDSO0011-NOT-FOUND
            //           *
            //           *  --> Chiave non trovata
            //           *
            //                               THRU GESTIONE-ERR-STD-EX
            //           *
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //           *
            //                              TO TRUE
            //           *
            //                       WHEN OTHER
            //           *
            //           *  --> Chiave non trovata
            //           *
            //                               THRU GESTIONE-ERR-STD-EX
            //           *
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.NOT_FOUND://
                    //  --> Chiave non trovata
                    //
                    // COB_CODE: MOVE 'S00430-LEGGI-STB'
                    //             TO WK-LABEL-ERR
                    ws.getWkGestioneMsgErr().setLabelErr("S00430-LEGGI-STB");
                    // COB_CODE: MOVE 'STAT-OGG-BUS NON TROVATA'
                    //             TO WK-STRING
                    ws.getWkGestioneMsgErr().setStringFld("STAT-OGG-BUS NON TROVATA");
                    // COB_CODE: MOVE '005069'
                    //             TO WK-COD-ERR
                    ws.getWkGestioneMsgErr().setCodErr("005069");
                    // COB_CODE: PERFORM GESTIONE-ERR-STD
                    //              THRU GESTIONE-ERR-STD-EX
                    gestioneErrStd();
                    //
                    break;

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://
                    // COB_CODE: MOVE IDSO0011-BUFFER-DATI
                    //             TO STAT-OGG-BUS
                    ws.getStatOggBus().setStatOggBusFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    //
                    // COB_CODE: SET WK-STB-TROV-SI
                    //             TO TRUE
                    ws.getWkStbTrovato().setSi();
                    //
                    break;

                default://
                    //  --> Chiave non trovata
                    //
                    // COB_CODE: MOVE 'S00430-LEGGI-STB'
                    //             TO WK-LABEL-ERR
                    ws.getWkGestioneMsgErr().setLabelErr("S00430-LEGGI-STB");
                    // COB_CODE: MOVE 'ERRORE LETTURA STAT-OGG-BUS'
                    //             TO WK-STRING
                    ws.getWkGestioneMsgErr().setStringFld("ERRORE LETTURA STAT-OGG-BUS");
                    // COB_CODE: MOVE '001114'
                    //             TO WK-COD-ERR
                    ws.getWkGestioneMsgErr().setCodErr("001114");
                    // COB_CODE: PERFORM GESTIONE-ERR-STD
                    //              THRU GESTIONE-ERR-STD-EX
                    gestioneErrStd();
                    //
                    break;
            }
            //
        }
        else {
            //
            //  --> Errore Dispatcher
            //
            // COB_CODE: MOVE 'S00430-LEGGI-STB'
            //             TO WK-LABEL-ERR
            ws.getWkGestioneMsgErr().setLabelErr("S00430-LEGGI-STB");
            // COB_CODE: MOVE 'ERRORE DISPATCHER LETTURA STAT-OGG-BUS'
            //             TO WK-STRING
            ws.getWkGestioneMsgErr().setStringFld("ERRORE DISPATCHER LETTURA STAT-OGG-BUS");
            // COB_CODE: MOVE '001114'
            //             TO WK-COD-ERR
            ws.getWkGestioneMsgErr().setCodErr("001114");
            // COB_CODE: PERFORM GESTIONE-ERR-STD
            //              THRU GESTIONE-ERR-STD-EX
            gestioneErrStd();
            //
        }
    }

    /**Original name: S00440-LEGGI-POLIZZA<br>
	 * <pre> -------------------------------------------------------------- *
	 *     LETTURA DELLA TABELLA POLIZZA                               *
	 *  -------------------------------------------------------------- *</pre>*/
    private void s00440LeggiPolizza() {
        // COB_CODE: MOVE 'S00440-LEGGI-POLIZZA'
        //             TO WK-LABEL-ERR
        ws.getWkGestioneMsgErr().setLabelErr("S00440-LEGGI-POLIZZA");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // COB_CODE: PERFORM S00450-IMPOSTA-POLIZZA
        //              THRU S00450-IMPOSTA-POLIZZA-EX
        s00450ImpostaPolizza();
        //
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX.
        callDispatcher();
        //
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //           *
        //                   END-EVALUATE
        //           *
        //                ELSE
        //           *
        //           *  --> Errore dispatcher
        //           *
        //                      THRU GESTIONE-ERR-STD-EX
        //           *
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            //
            //
            // COB_CODE:         EVALUATE TRUE
            //           *
            //                       WHEN IDSO0011-NOT-FOUND
            //           *
            //           *  --> Chiave non trovata
            //           *
            //                               THRU GESTIONE-ERR-STD-EX
            //           *
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //           *
            //           *  --> Operazione eseguita correttamente
            //           *
            //                              TO WCOM-POL-IB-OGG
            //           *
            //                       WHEN OTHER
            //           *
            //           *  --> Errore di accesso al db
            //           *
            //                               THRU GESTIONE-ERR-STD-EX
            //           *
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.NOT_FOUND://
                    //  --> Chiave non trovata
                    //
                    // COB_CODE: MOVE 'S00440-LEGGI-POLIZZA'
                    //             TO WK-LABEL-ERR
                    ws.getWkGestioneMsgErr().setLabelErr("S00440-LEGGI-POLIZZA");
                    // COB_CODE: MOVE 'POLIZZA NON TROVATA'
                    //             TO WK-STRING
                    ws.getWkGestioneMsgErr().setStringFld("POLIZZA NON TROVATA");
                    // COB_CODE: MOVE '005069'
                    //             TO WK-COD-ERR
                    ws.getWkGestioneMsgErr().setCodErr("005069");
                    // COB_CODE: PERFORM GESTIONE-ERR-STD
                    //              THRU GESTIONE-ERR-STD-EX
                    gestioneErrStd();
                    //
                    break;

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://
                    //  --> Operazione eseguita correttamente
                    //
                    // COB_CODE: MOVE 1
                    //             TO WPOL-ELE-POLI-MAX
                    areaMain.getWpolAreaPolizza().setWpolElePoliMax(((short)1));
                    // COB_CODE: MOVE IDSO0011-BUFFER-DATI
                    //             TO POLI
                    ws.getPoli().setPoliFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    // COB_CODE: PERFORM VALORIZZA-OUTPUT-POL
                    //              THRU VALORIZZA-OUTPUT-POL-EX
                    valorizzaOutputPol();
                    // COB_CODE: SET WPOL-ST-INV
                    //             TO TRUE
                    areaMain.getWpolAreaPolizza().getLccvpol1().getStatus().setInv();
                    // COB_CODE: MOVE WPOL-IB-OGG
                    //             TO WCOM-POL-IB-OGG
                    wcomIoStati.getLccc0261().getDatiPolizza().setPolIbOgg(areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolIbOgg());
                    //
                    break;

                default://
                    //  --> Errore di accesso al db
                    //
                    // COB_CODE: MOVE 'S00440-LEGGI-POLIZZA'
                    //             TO WK-LABEL-ERR
                    ws.getWkGestioneMsgErr().setLabelErr("S00440-LEGGI-POLIZZA");
                    // COB_CODE: MOVE 'ERRORE LETTURA POLIZZA'
                    //             TO WK-STRING
                    ws.getWkGestioneMsgErr().setStringFld("ERRORE LETTURA POLIZZA");
                    // COB_CODE: MOVE '005016'
                    //             TO WK-COD-ERR
                    ws.getWkGestioneMsgErr().setCodErr("005016");
                    // COB_CODE: PERFORM GESTIONE-ERR-STD
                    //              THRU GESTIONE-ERR-STD-EX
                    gestioneErrStd();
                    //
                    break;
            }
            //
        }
        else {
            //
            //  --> Errore dispatcher
            //
            // COB_CODE: MOVE 'S00440-LEGGI-POLIZZA'
            //             TO WK-LABEL-ERR
            ws.getWkGestioneMsgErr().setLabelErr("S00440-LEGGI-POLIZZA");
            // COB_CODE: MOVE 'ERRORE DISPATCHER LETTURA POLIZZA'
            //             TO WK-STRING
            ws.getWkGestioneMsgErr().setStringFld("ERRORE DISPATCHER LETTURA POLIZZA");
            // COB_CODE: MOVE '005016'
            //             TO WK-COD-ERR
            ws.getWkGestioneMsgErr().setCodErr("005016");
            // COB_CODE: PERFORM GESTIONE-ERR-STD
            //              THRU GESTIONE-ERR-STD-EX
            gestioneErrStd();
            //
        }
    }

    /**Original name: S00450-IMPOSTA-POLIZZA<br>
	 * <pre> -------------------------------------------------------------- *
	 *     VALORIZZAZIONE PER LETTURA DELLA TABELLA POLIZZA            *
	 *  -------------------------------------------------------------- *</pre>*/
    private void s00450ImpostaPolizza() {
        // COB_CODE: INITIALIZE POLI.
        initPoli();
        //
        // COB_CODE: MOVE WPMO-ID-POLI(1)
        //             TO POL-ID-POLI.
        ws.getPoli().setPolIdPoli(areaMain.getWpmoTabParamMov(1).getLccvpmo1().getDati().getWpmoIdPoli());
        //
        //  --> La data effetto viene sempre valorizzata
        //
        // COB_CODE: MOVE ZERO
        //             TO IDSI0011-DATA-INIZIO-EFFETTO
        //                IDSI0011-DATA-FINE-EFFETTO
        //                IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        //
        //  --> Nome tabella fisica db
        //
        // COB_CODE: MOVE 'POLI'
        //             TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("POLI");
        //
        //  --> Dclgen tabella
        //
        // COB_CODE: MOVE POLI
        //             TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getPoli().getPoliFormatted());
        //
        //  --> Tipo operazione
        //
        // COB_CODE: SET IDSI0011-SELECT
        //             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        //
        //  --> Tipo livello
        //
        // COB_CODE: SET IDSI0011-ID
        //             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setIdsi0011Id();
        //
        //  --> Modalita di accesso
        //
        // COB_CODE: SET IDSI0011-TRATT-DEFAULT
        //             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setIdsi0011TrattDefault();
        //
        //  --> Inizializzazione Return Code
        //
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC
        //             TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL
        //             TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
    }

    /**Original name: S00460-LEGGI-ADESIONE<br>
	 * <pre> -------------------------------------------------------------- *
	 *     LETTURA DELLA TABELLA ADESIONE                              *
	 *  -------------------------------------------------------------- *</pre>*/
    private void s00460LeggiAdesione() {
        // COB_CODE: MOVE 'S00460-LEGGI-ADESIONE' TO WK-LABEL-ERR
        ws.getWkGestioneMsgErr().setLabelErr("S00460-LEGGI-ADESIONE");
        // COB_CODE: PERFORM DISPLAY-LABEL THRU DISPLAY-LABEL-EX
        displayLabel();
        //
        // COB_CODE: PERFORM S00470-IMPOSTA-ADESIONE
        //              THRU S00470-IMPOSTA-ADESIONE-EX
        s00470ImpostaAdesione();
        //
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX.
        callDispatcher();
        //
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //           *
        //                   END-EVALUATE
        //           *
        //                ELSE
        //           *
        //           *  --> Errore dispatcher
        //           *
        //                      THRU GESTIONE-ERR-STD-EX
        //           *
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            //
            //
            // COB_CODE:         EVALUATE TRUE
            //           *
            //                       WHEN IDSO0011-NOT-FOUND
            //           *
            //           *  --> Chiave non trovata
            //           *
            //                               THRU GESTIONE-ERR-STD-EX
            //           *
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //           *
            //           *  --> Operazione eseguita correttamente
            //           *
            //                              TO WCOM-ADE-IB-OGG
            //           *
            //                       WHEN OTHER
            //           *
            //           *  --> Errore di accesso al db
            //           *
            //                               THRU GESTIONE-ERR-STD-EX
            //           *
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.NOT_FOUND://
                    //  --> Chiave non trovata
                    //
                    // COB_CODE: MOVE 'S00460-LEGGI-ADESIONE'
                    //             TO WK-LABEL-ERR
                    ws.getWkGestioneMsgErr().setLabelErr("S00460-LEGGI-ADESIONE");
                    // COB_CODE: MOVE 'ADESIONE NON TROVATA'
                    //             TO WK-STRING
                    ws.getWkGestioneMsgErr().setStringFld("ADESIONE NON TROVATA");
                    // COB_CODE: MOVE '005069'
                    //             TO WK-COD-ERR
                    ws.getWkGestioneMsgErr().setCodErr("005069");
                    // COB_CODE: PERFORM GESTIONE-ERR-STD
                    //              THRU GESTIONE-ERR-STD-EX
                    gestioneErrStd();
                    //
                    break;

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://
                    //  --> Operazione eseguita correttamente
                    //
                    // COB_CODE: MOVE 1
                    //             TO IX-TAB-ADE
                    //                WADE-ELE-ADES-MAX
                    ws.getIxIndici().setTabAde(((short)1));
                    areaMain.getWadeAreaAdesione().setEleAdesMax(((short)1));
                    // COB_CODE: MOVE IDSO0011-BUFFER-DATI
                    //             TO ADES
                    ws.getAdes().setAdesFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    // COB_CODE: PERFORM VALORIZZA-OUTPUT-ADE
                    //              THRU VALORIZZA-OUTPUT-ADE-EX
                    valorizzaOutputAde();
                    // COB_CODE: SET WADE-ST-INV(1)
                    //             TO TRUE
                    areaMain.getWadeAreaAdesione().getTabAde(1).getLccvade1().getStatus().setInv();
                    // COB_CODE: MOVE WADE-IB-OGG(1)
                    //             TO WCOM-ADE-IB-OGG
                    wcomIoStati.getLccc0261().setAdeIbOgg(areaMain.getWadeAreaAdesione().getTabAde(1).getLccvade1().getDati().getWadeIbOgg());
                    //
                    break;

                default://
                    //  --> Errore di accesso al db
                    //
                    // COB_CODE: MOVE 'S00460-LEGGI-ADESIONE'
                    //             TO WK-LABEL-ERR
                    ws.getWkGestioneMsgErr().setLabelErr("S00460-LEGGI-ADESIONE");
                    // COB_CODE: MOVE 'ERRORE LETTURA ADESIONE'
                    //             TO WK-STRING
                    ws.getWkGestioneMsgErr().setStringFld("ERRORE LETTURA ADESIONE");
                    // COB_CODE: MOVE '005016'
                    //             TO WK-COD-ERR
                    ws.getWkGestioneMsgErr().setCodErr("005016");
                    // COB_CODE: PERFORM GESTIONE-ERR-STD
                    //              THRU GESTIONE-ERR-STD-EX
                    gestioneErrStd();
                    //
                    break;
            }
            //
        }
        else {
            //
            //  --> Errore dispatcher
            //
            // COB_CODE: MOVE 'S00460-LEGGI-ADESIONE'
            //             TO WK-LABEL-ERR
            ws.getWkGestioneMsgErr().setLabelErr("S00460-LEGGI-ADESIONE");
            // COB_CODE: MOVE 'ERRORE DISPATCHER LETTURA ADESIONE'
            //             TO WK-STRING
            ws.getWkGestioneMsgErr().setStringFld("ERRORE DISPATCHER LETTURA ADESIONE");
            // COB_CODE: MOVE '005016'
            //             TO WK-COD-ERR
            ws.getWkGestioneMsgErr().setCodErr("005016");
            // COB_CODE: PERFORM GESTIONE-ERR-STD
            //              THRU GESTIONE-ERR-STD-EX
            gestioneErrStd();
            //
        }
    }

    /**Original name: S00470-IMPOSTA-ADESIONE<br>
	 * <pre> -------------------------------------------------------------- *
	 *     VALORIZZAZIONE PER LETTURA DELLA TABELLA ADESIONE
	 *  -------------------------------------------------------------- *</pre>*/
    private void s00470ImpostaAdesione() {
        // COB_CODE: INITIALIZE ADES.
        initAdes();
        //
        // COB_CODE: MOVE WPMO-ID-ADES(1)
        //             TO ADE-ID-ADES.
        ws.getAdes().setAdeIdAdes(areaMain.getWpmoTabParamMov(1).getLccvpmo1().getDati().getWpmoIdAdes().getWpmoIdAdes());
        //
        //  --> La data effetto viene sempre valorizzata
        //
        // COB_CODE: MOVE ZERO
        //             TO IDSI0011-DATA-INIZIO-EFFETTO
        //                IDSI0011-DATA-FINE-EFFETTO
        //                IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        //
        //  --> Nome tabella fisica db
        //
        // COB_CODE: MOVE 'ADES'
        //             TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("ADES");
        //
        //  --> Dclgen tabella
        //
        // COB_CODE: MOVE ADES
        //             TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getAdes().getAdesFormatted());
        //
        //  --> Tipo operazione
        //
        // COB_CODE: SET IDSI0011-SELECT
        //             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        //
        //  --> Tipo livello
        //
        // COB_CODE: SET IDSI0011-ID
        //             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setIdsi0011Id();
        //
        //  --> Modalita di accesso
        //
        // COB_CODE: SET IDSI0011-TRATT-DEFAULT
        //             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setIdsi0011TrattDefault();
        //
        //  --> Inizializzazione Return Code
        //
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC
        //             TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL
        //             TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
    }

    /**Original name: S00570-CTRL-TP-DT-RIVAL<br>
	 * <pre>----------------------------------------------------------------*
	 *         CONTROLLA IL TIPO DI DATA RIVALUTAZIONE                 *
	 * ----------------------------------------------------------------*</pre>*/
    private void s00570CtrlTpDtRival() {
        // COB_CODE: MOVE 'S00570-CTRL-TP-DT-RIVAL' TO WK-LABEL-ERR
        ws.getWkGestioneMsgErr().setLabelErr("S00570-CTRL-TP-DT-RIVAL");
        // COB_CODE: PERFORM DISPLAY-LABEL THRU DISPLAY-LABEL-EX
        displayLabel();
        // --> Verifica che tutte le occorrenze lette dalla tabella
        // --> guida Parametro Movimento abbiano lo stesso tipo
        // --> oggetto di rivalutazione, se non h cosl viene genarato
        // --> un errore bloccante
        //
        // COB_CODE:      PERFORM VARYING IX-TAB-PMO FROM 1 BY 1
        //                  UNTIL IX-TAB-PMO > WPMO-ELE-PARAM-MOV-MAX
        //           *
        //                     END-IF
        //           *
        //                END-PERFORM.
        ws.getIxIndici().setTabPmo(((short)1));
        while (!(ws.getIxIndici().getTabPmo() > areaMain.getWpmoEleParamMovMax())) {
            //
            // COB_CODE:           IF WPMO-TP-OGG-RIVAL(1) NOT =
            //                        WPMO-TP-OGG-RIVAL(IX-TAB-PMO)
            //           *
            //                           THRU GESTIONE-ERR-STD-EX
            //           *
            //                     END-IF
            if (!Conditions.eq(areaMain.getWpmoTabParamMov(1).getLccvpmo1().getDati().getWpmoTpOggRival(), areaMain.getWpmoTabParamMov(ws.getIxIndici().getTabPmo()).getLccvpmo1().getDati().getWpmoTpOggRival())) {
                //
                // COB_CODE: MOVE 'S00570-CTRL-TP-DT-RIVAL'
                //             TO WK-LABEL-ERR
                ws.getWkGestioneMsgErr().setLabelErr("S00570-CTRL-TP-DT-RIVAL");
                // COB_CODE: MOVE 'TP-OGG-RIVAL DIVERSO NELLA STESSA ADESIONE'
                //             TO WK-STRING
                ws.getWkGestioneMsgErr().setStringFld("TP-OGG-RIVAL DIVERSO NELLA STESSA ADESIONE");
                // COB_CODE: MOVE '001114'
                //             TO WK-COD-ERR
                ws.getWkGestioneMsgErr().setCodErr("001114");
                // COB_CODE: PERFORM GESTIONE-ERR-STD
                //              THRU GESTIONE-ERR-STD-EX
                gestioneErrStd();
                //
            }
            //
            ws.getIxIndici().setTabPmo(Trunc.toShort(ws.getIxIndici().getTabPmo() + 1, 4));
        }
    }

    /**Original name: S00600-GESTIONE-DATA-PROD<br>
	 * <pre> -------------------------------------------------------------- *
	 *        DETERMINAZIONE DELLA DATA INIZIO VALIDITA' PRODOTTO      *
	 *  -------------------------------------------------------------- *</pre>*/
    private void s00600GestioneDataProd() {
        // COB_CODE: MOVE 'S00600-GESTIONE-DATA-PROD' TO WK-LABEL-ERR
        ws.getWkGestioneMsgErr().setLabelErr("S00600-GESTIONE-DATA-PROD");
        // COB_CODE: PERFORM DISPLAY-LABEL            THRU DISPLAY-LABEL-EX
        displayLabel();
        //
        //  --> Chiamata al servizio di prodotto ISPS0040 per il
        //  --> reperimento della data di validit&#x17; prodotto
        //
        // COB_CODE:      IF WCOM-DT-ULT-VERS-PROD = 0
        //           *
        //                   END-IF
        //           *
        //                END-IF.
        if (wcomIoStati.getLccc0001().getWcomDtUltVersProd() == 0) {
            //
            // COB_CODE:         IF WPOL-FL-VER-PROD = 'E'
            //           *
            //                        TO WCOM-DT-ULT-VERS-PROD
            //           *
            //                   ELSE
            //           *
            //           *          PERFORM S00610-PREPARA-ISPS0040
            //           *             THRU S00610-PREPARA-ISPS0040-EX
            //           *
            //           *          PERFORM S00620-CALL-ISPS0040
            //           *             THRU S00620-CALL-ISPS0040-EX
            //           *
            //                         END-IF
            //                   END-IF
            if (Conditions.eq(areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolFlVerProd(), "E")) {
                //
                // COB_CODE: MOVE WPOL-DT-INI-VLDT-PROD
                //             TO WCOM-DT-ULT-VERS-PROD
                wcomIoStati.getLccc0001().setWcomDtUltVersProd(TruncAbs.toInt(areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolDtIniVldtProd(), 8));
                //
            }
            else {
                //
                //          PERFORM S00610-PREPARA-ISPS0040
                //             THRU S00610-PREPARA-ISPS0040-EX
                //
                //          PERFORM S00620-CALL-ISPS0040
                //             THRU S00620-CALL-ISPS0040-EX
                //
                // COB_CODE: SET PROD-KO TO TRUE
                ws.getWkFlagProd().setKo();
                // COB_CODE: PERFORM S00610-PREPARA-ISPS0040
                //              THRU S00610-PREPARA-ISPS0040-EX
                s00610PreparaIsps0040();
                // COB_CODE: PERFORM VERIFICA-PROD
                //              THRU VERIFICA-PROD-EX
                verificaProd();
                // COB_CODE: IF PROD-KO
                //                 END-IF
                //              END-IF
                if (ws.getWkFlagProd().isKo()) {
                    // COB_CODE: PERFORM S00620-CALL-ISPS0040
                    //              THRU S00620-CALL-ISPS0040-EX
                    s00620CallIsps0040();
                    // COB_CODE: IF IDSV0001-ESITO-OK
                    //           AND PROD-KO
                    //           AND WK-0040-ELE-MAX <  WK-0040-ELEMENTI-MAX
                    //                 TO WK-0040-DATA-VERSIONE-PROD(WK-0040-ELE-MAX)
                    //              END-IF
                    if (areaIdsv0001.getEsito().isIdsv0001EsitoOk() && ws.getWkFlagProd().isKo() && ws.getWk0040EleMax() < ws.getWk0040ElementiMax()) {
                        // COB_CODE: ADD 1 TO WK-0040-ELE-MAX
                        ws.setWk0040EleMax(Trunc.toShort(1 + ws.getWk0040EleMax(), 4));
                        // COB_CODE: MOVE WS-0040-COD-COMPAGNIA
                        //             TO WK-0040-COD-COMPAGNIA(WK-0040-ELE-MAX)
                        ws.getWk0040Tabella(ws.getWk0040EleMax()).getDatiInput().setCodCompagniaFormatted(ws.getWs0040DatiInput().getCodCompagniaFormatted());
                        // COB_CODE: MOVE WS-0040-COD-PRODOTTO
                        //             TO WK-0040-COD-PRODOTTO(WK-0040-ELE-MAX)
                        ws.getWk0040Tabella(ws.getWk0040EleMax()).getDatiInput().setCodProdotto(ws.getWs0040DatiInput().getCodProdotto());
                        // COB_CODE: MOVE WS-0040-COD-CONVENZIONE
                        //             TO WK-0040-COD-CONVENZIONE(WK-0040-ELE-MAX)
                        ws.getWk0040Tabella(ws.getWk0040EleMax()).getDatiInput().setCodConvenzione(ws.getWs0040DatiInput().getCodConvenzione());
                        // COB_CODE:   MOVE WS-0040-DATA-INIZ-VALID-CONV
                        //           TO WK-0040-DATA-INIZ-VALID-CONV(WK-0040-ELE-MAX)
                        ws.getWk0040Tabella(ws.getWk0040EleMax()).getDatiInput().setDataInizValidConv(ws.getWs0040DatiInput().getDataInizValidConv());
                        // COB_CODE: MOVE WS-0040-DATA-RIFERIMENTO
                        //           TO WK-0040-DATA-RIFERIMENTO(WK-0040-ELE-MAX)
                        ws.getWk0040Tabella(ws.getWk0040EleMax()).getDatiInput().setDataRiferimento(ws.getWs0040DatiInput().getDataRiferimento());
                        // COB_CODE: MOVE WS-0040-LIVELLO-UTENTE
                        //            TO WK-0040-LIVELLO-UTENTE(WK-0040-ELE-MAX)
                        ws.getWk0040Tabella(ws.getWk0040EleMax()).getDatiInput().setLivelloUtenteFormatted(ws.getWs0040DatiInput().getLivelloUtenteFormatted());
                        // COB_CODE: MOVE WS-0040-SESSION-ID
                        //             TO WK-0040-SESSION-ID(WK-0040-ELE-MAX)
                        ws.getWk0040Tabella(ws.getWk0040EleMax()).getDatiInput().setSessionId(ws.getWs0040DatiInput().getSessionId());
                        // COB_CODE: MOVE WS-0040-FUNZIONALITA
                        //             TO WK-0040-FUNZIONALITA(WK-0040-ELE-MAX)
                        ws.getWk0040Tabella(ws.getWk0040EleMax()).getDatiInput().setFunzionalitaFormatted(ws.getWs0040DatiInput().getFunzionalitaFormatted());
                        // COB_CODE: MOVE WCOM-DT-ULT-VERS-PROD
                        //           TO WK-0040-DATA-VERSIONE-PROD(WK-0040-ELE-MAX)
                        ws.getWk0040Tabella(ws.getWk0040EleMax()).setDataVersioneProd(wcomIoStati.getLccc0001().getWcomDtUltVersProdFormatted());
                    }
                }
            }
            //
        }
    }

    /**Original name: S00610-PREPARA-ISPS0040<br>
	 * <pre>----------------------------------------------------------------*
	 *      PREPAREA AREA SERVIZIO DATI PRODOTTO ISPS0040              *
	 * ----------------------------------------------------------------*
	 *   --> Valorizzazione dell'Area di I/O del servizio chiamato</pre>*/
    private void s00610PreparaIsps0040() {
        // COB_CODE: INITIALIZE AREA-IO-ISPS0040.
        initAreaIoIsps0040();
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //             TO ISPC0040-COD-COMPAGNIA.
        ws.getAreaIoIsps0040().getIspc0040DatiInput().setCodCompagniaFormatted(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAniaFormatted());
        //
        // COB_CODE: MOVE WPOL-COD-PROD
        //             TO ISPC0040-COD-PRODOTTO.
        ws.getAreaIoIsps0040().getIspc0040DatiInput().setCodProdotto(areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolCodProd());
        //
        // COB_CODE:      IF WPOL-COD-CONV-NULL = HIGH-VALUES
        //           *
        //                     TO ISPC0040-COD-CONVENZIONE
        //           *
        //                ELSE
        //           *
        //                     TO ISPC0040-COD-CONVENZIONE
        //           *
        //                END-IF.
        if (Characters.EQ_HIGH.test(areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolCodConvFormatted())) {
            //
            // COB_CODE: MOVE SPACES
            //             TO ISPC0040-COD-CONVENZIONE
            ws.getAreaIoIsps0040().getIspc0040DatiInput().setCodConvenzione("");
            //
        }
        else {
            //
            // COB_CODE: MOVE WPOL-COD-CONV
            //             TO ISPC0040-COD-CONVENZIONE
            ws.getAreaIoIsps0040().getIspc0040DatiInput().setCodConvenzione(areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolCodConv());
            //
        }
        //
        // COB_CODE:      IF WPOL-DT-INI-VLDT-CONV-NULL = HIGH-VALUE
        //           *
        //                     TO ISPC0040-DATA-INIZ-VALID-CONV
        //           *
        //                ELSE
        //           *
        //                     TO ISPC0040-DATA-INIZ-VALID-CONV
        //           *
        //                END-IF.
        if (Characters.EQ_HIGH.test(areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolDtIniVldtConv().getWpolDtIniVldtConvNullFormatted())) {
            //
            // COB_CODE: MOVE SPACES
            //             TO ISPC0040-DATA-INIZ-VALID-CONV
            ws.getAreaIoIsps0040().getIspc0040DatiInput().setDataInizValidConv("");
            //
        }
        else {
            //
            // COB_CODE: MOVE WPOL-DT-INI-VLDT-CONV
            //             TO ISPC0040-DATA-INIZ-VALID-CONV
            ws.getAreaIoIsps0040().getIspc0040DatiInput().setDataInizValidConv(areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolDtIniVldtConv().getWpolDtIniVldtConvFormatted());
            //
        }
        //
        //     MOVE IDSV0001-DATA-EFFETTO
        // COB_CODE: MOVE WPOL-DT-INI-VLDT-PROD
        //            TO ISPC0040-DATA-RIFERIMENTO.
        ws.getAreaIoIsps0040().getIspc0040DatiInput().setDataRiferimento(areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolDtIniVldtProdFormatted());
        //
        // COB_CODE: MOVE WCOM-COD-LIV-AUT-PROFIL
        //             TO ISPC0040-LIVELLO-UTENTE.
        ws.getAreaIoIsps0040().getIspc0040DatiInput().setIspc0040LivelloUtente(TruncAbs.toShort(wcomIoStati.getLccc0001().getDatiDeroghe().getCodLivAutProfil(), 2));
        //
        // COB_CODE: MOVE IDSV0001-SESSIONE
        //             TO ISPC0040-SESSION-ID.
        ws.getAreaIoIsps0040().getIspc0040DatiInput().setSessionId(areaIdsv0001.getAreaComune().getSessione());
        //
        // COB_CODE: MOVE '1001'
        //             TO ISPC0040-FUNZIONALITA.
        ws.getAreaIoIsps0040().getIspc0040DatiInput().setFunzionalitaFormatted("1001");
    }

    /**Original name: S00620-CALL-ISPS0040<br>
	 * <pre>----------------------------------------------------------------*
	 *      CHIAMATA A SERVIZIO DATI PRODOTTO ISPS0040                 *
	 * ----------------------------------------------------------------*</pre>*/
    private void s00620CallIsps0040() {
        Isps0040 isps0040 = null;
        // COB_CODE: SET  IDSV8888-BUSINESS-DBG      TO TRUE
        ws.getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
        // COB_CODE: MOVE 'ISPS0040'                 TO IDSV8888-NOME-PGM.
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("ISPS0040");
        // COB_CODE: SET  IDSV8888-INIZIO            TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setInizio();
        // COB_CODE: MOVE SPACES                     TO IDSV8888-DESC-PGM
        //skipped translation for moving SPACES to IDSV8888-DESC-PGM; considered in STRING statement translation below
        // COB_CODE: STRING 'Servizio dati prodotto'
        //                   DELIMITED BY SIZE
        //                   INTO IDSV8888-DESC-PGM
        //           END-STRING
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("Servizio dati prodotto");
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE:      CALL ISPS0040                 USING AREA-IDSV0001
        //                                                    WCOM-AREA-STATI
        //                                                    AREA-IO-ISPS0040
        //                ON EXCEPTION
        //           *
        //                         THRU GESTIONE-ERR-SIST-EX
        //           *
        //                END-CALL.
        try {
            isps0040 = Isps0040.getInstance();
            isps0040.run(areaIdsv0001, wcomIoStati, ws.getAreaIoIsps0040());
        }
        catch (ProgramExecutionException __ex) {
            //
            // COB_CODE: MOVE 'ISPS0040'
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe("ISPS0040");
            // COB_CODE: MOVE 'SERVIZIO DATI PRODOTTO'
            //             TO CALL-DESC
            ws.getIdsv0002().setCallDesc("SERVIZIO DATI PRODOTTO");
            // COB_CODE: MOVE 'CALL-DATI-PROD'
            //             TO WK-LABEL-ERR
            ws.getWkGestioneMsgErr().setLabelErr("CALL-DATI-PROD");
            //
            // COB_CODE: PERFORM GESTIONE-ERR-SIST
            //              THRU GESTIONE-ERR-SIST-EX
            gestioneErrSist();
            //
        }
        // COB_CODE: SET  IDSV8888-BUSINESS-DBG          TO TRUE
        ws.getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
        // COB_CODE: MOVE 'ISPS0040'                 TO IDSV8888-NOME-PGM.
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("ISPS0040");
        // COB_CODE: SET  IDSV8888-FINE              TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setFine();
        // COB_CODE: MOVE SPACES                     TO IDSV8888-DESC-PGM
        //skipped translation for moving SPACES to IDSV8888-DESC-PGM; considered in STRING statement translation below
        // COB_CODE: STRING 'Servizio dati prodotto'
        //                   DELIMITED BY SIZE
        //                   INTO IDSV8888-DESC-PGM
        //           END-STRING
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("Servizio dati prodotto");
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: IF IDSV0001-ESITO-OK
        //                TO WCOM-DT-ULT-VERS-PROD
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: MOVE ISPC0040-DATA-VERSIONE-PROD
            //             TO WCOM-DT-ULT-VERS-PROD
            wcomIoStati.getLccc0001().setWcomDtUltVersProdFormatted(ws.getAreaIoIsps0040().getIspc0040DatiOutput1().getIspc0040DataVersioneProdFormatted());
        }
    }

    /**Original name: S00700-DETERMINA-ORIG-RIV<br>
	 * <pre>----------------------------------------------------------------*
	 *    DETERMINAZIONE DEL TIPO DI RIVALUTAZIONE EMESSO/INCASSO      *
	 * ----------------------------------------------------------------*</pre>*/
    private void s00700DeterminaOrigRiv() {
        // COB_CODE: MOVE 'S00700-DETERMINA-ORIG-RIV' TO WK-LABEL-ERR
        ws.getWkGestioneMsgErr().setLabelErr("S00700-DETERMINA-ORIG-RIV");
        // COB_CODE: PERFORM DISPLAY-LABEL THRU DISPLAY-LABEL-EX
        displayLabel();
        //
        // --> Accesso alla tabella Parametro Compagnia per
        // --> determinare se la rivalutazione h da effettuarsi
        // --> "Per Incasso" o "Per Emesso"
        //
        // COB_CODE: PERFORM S00710-VERIFICA-INC-EMESSO
        //              THRU S00710-VERIFICA-INC-EMESSO-EX.
        s00710VerificaIncEmesso();
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                   END-IF
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE:         IF PCO-TP-MOD-RIVAL = 'IN'
            //           *
            //                        TO TRUE
            //           *
            //                   ELSE
            //           *
            //                        TO TRUE
            //           *
            //                   END-IF
            if (Conditions.eq(ws.getParamComp().getPcoTpModRival(), "IN")) {
                //
                // COB_CODE: SET WK-ORIG-RIV-IN
                //             TO TRUE
                ws.getWkOrigRiv().setInFld();
                //
            }
            else {
                //
                // COB_CODE: SET WK-ORIG-RIV-EM
                //             TO TRUE
                ws.getWkOrigRiv().setEm();
                //
            }
            //
        }
    }

    /**Original name: S00710-VERIFICA-INC-EMESSO<br>
	 * <pre>----------------------------------------------------------------*
	 *               ACCESSO ALLA TABELLA PARAMETRO COMPAGNIA          *
	 * ----------------------------------------------------------------*</pre>*/
    private void s00710VerificaIncEmesso() {
        // COB_CODE: MOVE 'S00720-VERIFICA-INC-EMESSO' TO WK-LABEL-ERR
        ws.getWkGestioneMsgErr().setLabelErr("S00720-VERIFICA-INC-EMESSO");
        // COB_CODE: PERFORM DISPLAY-LABEL THRU DISPLAY-LABEL-EX
        displayLabel();
        //
        // COB_CODE: PERFORM S00720-IMPOSTA-PARAM-COMP
        //              THRU S00720-IMPOSTA-PARAM-COMP-EX.
        s00720ImpostaParamComp();
        //
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX.
        callDispatcher();
        //
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //           *
        //                   END-EVALUATE
        //           *
        //                ELSE
        //           *
        //           *  --> Errore dispatcher
        //           *
        //                      THRU GESTIONE-ERR-STD-EX
        //           *
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            //
            //
            // COB_CODE:         EVALUATE TRUE
            //           *
            //                       WHEN IDSO0011-NOT-FOUND
            //           *
            //           *  --> Chiave non trovata
            //           *
            //                               THRU GESTIONE-ERR-STD-EX
            //           *
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //           *
            //           *  --> Operazione eseguita correttamente
            //           *
            //                              TO PARAM-COMP
            //           *
            //                       WHEN OTHER
            //           *
            //           *  --> Errore di accesso al db
            //           *
            //                               THRU GESTIONE-ERR-STD-EX
            //           *
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.NOT_FOUND://
                    //  --> Chiave non trovata
                    //
                    // COB_CODE: MOVE 'S00710-VERIFICA-INC-EMESSO'
                    //             TO WK-LABEL-ERR
                    ws.getWkGestioneMsgErr().setLabelErr("S00710-VERIFICA-INC-EMESSO");
                    // COB_CODE: MOVE 'COMPAGNIA NON TROVATA'
                    //             TO WK-STRING
                    ws.getWkGestioneMsgErr().setStringFld("COMPAGNIA NON TROVATA");
                    // COB_CODE: MOVE '005069'
                    //             TO WK-COD-ERR
                    ws.getWkGestioneMsgErr().setCodErr("005069");
                    // COB_CODE: PERFORM GESTIONE-ERR-STD
                    //              THRU GESTIONE-ERR-STD-EX
                    gestioneErrStd();
                    //
                    break;

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://
                    //  --> Operazione eseguita correttamente
                    //
                    // COB_CODE: MOVE IDSO0011-BUFFER-DATI
                    //             TO PARAM-COMP
                    ws.getParamComp().setParamCompFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    //
                    break;

                default://
                    //  --> Errore di accesso al db
                    //
                    // COB_CODE: MOVE 'S00710-VERIFICA-INC-EMESSO'
                    //             TO WK-LABEL-ERR
                    ws.getWkGestioneMsgErr().setLabelErr("S00710-VERIFICA-INC-EMESSO");
                    // COB_CODE: MOVE 'ERRORE LETTURA PARAMETRO COMPAGNIA'
                    //             TO WK-STRING
                    ws.getWkGestioneMsgErr().setStringFld("ERRORE LETTURA PARAMETRO COMPAGNIA");
                    // COB_CODE: MOVE '005016'
                    //             TO WK-COD-ERR
                    ws.getWkGestioneMsgErr().setCodErr("005016");
                    // COB_CODE: PERFORM GESTIONE-ERR-STD
                    //              THRU GESTIONE-ERR-STD-EX
                    gestioneErrStd();
                    //
                    break;
            }
            //
        }
        else {
            //
            //  --> Errore dispatcher
            //
            // COB_CODE: MOVE 'S00710-VERIFICA-INC-EMESSO'
            //             TO WK-LABEL-ERR
            ws.getWkGestioneMsgErr().setLabelErr("S00710-VERIFICA-INC-EMESSO");
            // COB_CODE: MOVE 'ERRORE DISPATCHER LETTURA PARAMETRO COMPAGNIA'
            //             TO WK-STRING
            ws.getWkGestioneMsgErr().setStringFld("ERRORE DISPATCHER LETTURA PARAMETRO COMPAGNIA");
            // COB_CODE: MOVE '005016'
            //             TO WK-COD-ERR
            ws.getWkGestioneMsgErr().setCodErr("005016");
            // COB_CODE: PERFORM GESTIONE-ERR-STD
            //              THRU GESTIONE-ERR-STD-EX
            gestioneErrStd();
            //
        }
    }

    /**Original name: S00720-IMPOSTA-PARAM-COMP<br>
	 * <pre> -------------------------------------------------------------- *
	 *  VALORIZZAZIONE DELLE CHIAVI DELLA TABELLA PARAMETRO COMPAGNIA  *
	 *  -------------------------------------------------------------- *</pre>*/
    private void s00720ImpostaParamComp() {
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //             TO PCO-COD-COMP-ANIA.
        ws.getParamComp().setPcoCodCompAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        //
        //  --> La data effetto viene sempre valorizzata
        //
        // COB_CODE: MOVE ZERO
        //             TO IDSI0011-DATA-INIZIO-EFFETTO
        //                IDSI0011-DATA-FINE-EFFETTO
        //                IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        //
        //  --> Nome tabella fisica db
        //
        // COB_CODE: MOVE 'PARAM-COMP'
        //             TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("PARAM-COMP");
        //
        //  --> Dclgen tabella
        //
        // COB_CODE: MOVE PARAM-COMP
        //             TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getParamComp().getParamCompFormatted());
        //
        //  --> Tipo operazione
        //
        // COB_CODE: SET IDSI0011-SELECT
        //             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        //
        //  --> Tipo livello
        //
        // COB_CODE: SET IDSI0011-PRIMARY-KEY
        //             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setPrimaryKey();
        //
        //  --> Modalita di accesso
        //
        // COB_CODE: SET IDSI0011-TRATT-SENZA-STOR
        //             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattSenzaStor();
        //
        //  --> Inizializzazione Return Code
        //
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC
        //             TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL
        //             TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
    }

    /**Original name: S10000-ELABORAZIONE<br>
	 * <pre> ============================================================== *
	 *  -->               E L A B O R A Z I O N E                  <-- *
	 *  ============================================================== *</pre>*/
    private void s10000Elaborazione() {
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                      THRU S10100-ACQUISIZ-GAR-TRANCHE-EX
        //           *
        //                END-IF
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE: PERFORM S10100-ACQUISIZ-GAR-TRANCHE
            //              THRU S10100-ACQUISIZ-GAR-TRANCHE-EX
            s10100AcquisizGarTranche();
            //
        }
        //
        // COB_CODE: IF WCOM-WRITE-OK
        //           END-IF.
        if (wcomIoStati.getLccc0261().getWrite().isWcomWriteOk()) {
            // COB_CODE: PERFORM VARYING IX-TAB-PMO FROM 1 BY 1
            //             UNTIL IX-TAB-PMO > WPMO-ELE-PARAM-MOV-MAX
            //               END-IF
            //           END-PERFORM
            ws.getIxIndici().setTabPmo(((short)1));
            while (!(ws.getIxIndici().getTabPmo() > areaMain.getWpmoEleParamMovMax())) {
                // COB_CODE: IF WPMO-ST-ADD(IX-TAB-PMO)
                //                TO VPMO-TAB-PARAM-MOV(IX-VPMO)
                //           END-IF
                if (areaMain.getWpmoTabParamMov(ws.getIxIndici().getTabPmo()).getLccvpmo1().getStatus().isAdd()) {
                    // COB_CODE: MOVE VPMO-ELE-PARAM-MOV-MAX
                    //             TO IX-VPMO
                    ws.getIxIndici().setVpmo(ws.getWorkCommarea().getEleParamMovMax());
                    // COB_CODE: ADD 1
                    //             TO IX-VPMO
                    ws.getIxIndici().setVpmo(Trunc.toShort(1 + ws.getIxIndici().getVpmo(), 4));
                    // COB_CODE: MOVE IX-VPMO
                    //             TO VPMO-ELE-PARAM-MOV-MAX
                    ws.getWorkCommarea().setEleParamMovMax(ws.getIxIndici().getVpmo());
                    // COB_CODE: MOVE WPMO-TAB-PARAM-MOV(IX-TAB-PMO)
                    //             TO VPMO-TAB-PARAM-MOV(IX-VPMO)
                    ws.getWorkCommarea().getTabParamMov(ws.getIxIndici().getVpmo()).setTabParamMovBytes(areaMain.getWpmoTabParamMov(ws.getIxIndici().getTabPmo()).getWpmoTabParamMovBytes());
                }
                ws.getIxIndici().setTabPmo(Trunc.toShort(ws.getIxIndici().getTabPmo() + 1, 4));
            }
            //
            // COB_CODE:      IF IDSV0001-ESITO-OK
            //           *
            //                      THRU S10450-GEST-RIC-SUCC-PMO-EX
            //           *
            //                END-IF
            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                //
                // COB_CODE: PERFORM S10450-GEST-RIC-SUCC-PMO
                //              THRU S10450-GEST-RIC-SUCC-PMO-EX
                s10450GestRicSuccPmo();
                //
            }
            //
            // COB_CODE:      IF IDSV0001-ESITO-OK
            //           *
            //                END-IF
            //                END-IF.
            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                //
                // COB_CODE:         IF WK-ORIG-RIV-IN
                //           *
                //                   END-IF
                //           *
                //                END-IF
                if (ws.getWkOrigRiv().isInFld()) {
                    //
                    // COB_CODE:            IF COLLETTIVA
                    //           *
                    //                            THRU S10200-RIVAL-X-INCASSO-COL-EX
                    //           *
                    //                      ELSE
                    //           *
                    //                            THRU S10300-RIVAL-X-INCASSO-IND-EX
                    //           *
                    //                   END-IF
                    if (ws.getWsTpFrmAssva().isCollettiva()) {
                        //
                        // COB_CODE: PERFORM S10200-RIVAL-X-INCASSO-COL
                        //              THRU S10200-RIVAL-X-INCASSO-COL-EX
                        s10200RivalXIncassoCol();
                        //
                    }
                    else {
                        //
                        // COB_CODE: PERFORM S10300-RIVAL-X-INCASSO-IND
                        //              THRU S10300-RIVAL-X-INCASSO-IND-EX
                        s10300RivalXIncassoInd();
                        //
                    }
                    //
                }
                //
                // Controllo Blocco - Movimento Futuro - Presenza Deroga
                //
                // COB_CODE:      IF IDSV0001-ESITO-OK
                //           *
                //                   END-IF
                //                END-IF
                if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                    //
                    // COB_CODE: MOVE WPOL-TP-FRM-ASSVA
                    //             TO WS-TP-FRM-ASSVA
                    ws.getWsTpFrmAssva().setWsTpFrmAssva(areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolTpFrmAssva());
                    // COB_CODE: MOVE WPOL-ID-POLI
                    //             TO LOAP1-ID-POLI
                    ws.setLoap1IdPoli(areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolIdPoli());
                    // COB_CODE: MOVE WADE-ID-ADES(1)
                    //             TO LOAP1-ID-ADES
                    ws.setLoap1IdAdes(areaMain.getWadeAreaAdesione().getTabAde(1).getLccvade1().getDati().getWadeIdAdes());
                    //
                    // COB_CODE: PERFORM LOAP0001-CONTROLLI
                    //              THRU LOAP0001-CONTROLLI-EX
                    loap0001Controlli();
                    //
                    // COB_CODE: IF  IDSV0001-ESITO-KO
                    //           AND LOAC0280-DEROGA-BLOCCANTE-SI
                    //               MOVE ZMOV-DATI   TO MOVI
                    //           END-IF
                    if (areaIdsv0001.getEsito().isIdsv0001EsitoKo() && ws.getAreaIoLoas0280().getGravitaDeroga().isLoac0280DerogaBloccanteSi()) {
                        // COB_CODE: MOVE MOVI        TO ZMOV-DATI
                        ws.getLccvmov1().getDati().setDatiBytes(ws.getMovi().getMoviBytes());
                        // COB_CODE: PERFORM LOAP0002-CONTROLLI
                        //              THRU LOAP0002-CONTROLLI-EX
                        loap0002Controlli();
                        // COB_CODE: MOVE ZMOV-DATI   TO MOVI
                        ws.getMovi().setMoviBytes(ws.getLccvmov1().getDati().getDatiBytes());
                    }
                }
                //
                // COB_CODE:      IF IDSV0001-ESITO-OK
                //           *
                //                      THRU S10500-GEST-VALORIZZATORE-EX
                //           *
                //                END-IF
                if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                    //
                    // COB_CODE: PERFORM S10500-GEST-VALORIZZATORE
                    //              THRU S10500-GEST-VALORIZZATORE-EX
                    s10500GestValorizzatore();
                    //
                }
                //*****************************************************************
                //*****************************************************************
                //  INIZIO CALL MODULO LOAS0660
                //*****************************************************************
                // COB_CODE:      IF IDSV0001-ESITO-OK
                //                      THRU S10600-PREP-CALL-LOAS0660-EX
                //           *
                //                END-IF
                if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                    // COB_CODE: PERFORM S10599-SALVA-PRSTZ-PREC
                    //              THRU S10599-SALVA-PRSTZ-PREC-EX
                    s10599SalvaPrstzPrec();
                    //
                    // COB_CODE: PERFORM S10600-PREP-CALL-LOAS0660
                    //              THRU S10600-PREP-CALL-LOAS0660-EX
                    s10600PrepCallLoas0660();
                    //
                }
                //
                // COB_CODE:      IF IDSV0001-ESITO-OK
                //           *
                //                      THRU S10700-CALL-LOAS0660
                //           *
                //                END-IF
                if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                    //
                    // COB_CODE: PERFORM S10700-CALL-LOAS0660
                    //              THRU S10700-CALL-LOAS0660
                    s10700CallLoas0660();
                    //
                }
                //*****************************************************************
                //  FINE CALL MODULO LOAS0660
                //*****************************************************************
                //*****************************************************************
                // COB_CODE: IF IDSV0001-ESITO-OK
                //              END-IF
                //           END-IF
                if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                    // COB_CODE: IF WPMO-DT-RICOR-PREC-NULL(1) = HIGH-VALUES
                    //              MOVE 0                       TO WK-DT-RICOR-PREC
                    //           ELSE
                    //              MOVE WPMO-DT-RICOR-PREC(1)   TO WK-DT-RICOR-PREC
                    //           END-IF
                    if (Characters.EQ_HIGH.test(areaMain.getWpmoTabParamMov(1).getLccvpmo1().getDati().getWpmoDtRicorPrec().getWpmoDtRicorPrecNullFormatted())) {
                        // COB_CODE: MOVE 0                       TO WK-DT-RICOR-PREC
                        ws.setWkDtRicorPrec(0);
                    }
                    else {
                        // COB_CODE: MOVE WPMO-DT-RICOR-PREC(1)   TO WK-DT-RICOR-PREC
                        ws.setWkDtRicorPrec(TruncAbs.toInt(areaMain.getWpmoTabParamMov(1).getLccvpmo1().getDati().getWpmoDtRicorPrec().getWpmoDtRicorPrec(), 8));
                    }
                    // COB_CODE: IF WPMO-DT-RICOR-SUCC-NULL(1) = HIGH-VALUES
                    //              MOVE 0                       TO WK-DT-RICOR-SUCC
                    //           ELSE
                    //              MOVE WPMO-DT-RICOR-SUCC(1)   TO WK-DT-RICOR-SUCC
                    //           END-IF
                    if (Characters.EQ_HIGH.test(areaMain.getWpmoTabParamMov(1).getLccvpmo1().getDati().getWpmoDtRicorSucc().getWpmoDtRicorSuccNullFormatted())) {
                        // COB_CODE: MOVE 0                       TO WK-DT-RICOR-SUCC
                        ws.setWkDtRicorSucc(0);
                    }
                    else {
                        // COB_CODE: MOVE WPMO-DT-RICOR-SUCC(1)   TO WK-DT-RICOR-SUCC
                        ws.setWkDtRicorSucc(TruncAbs.toInt(areaMain.getWpmoTabParamMov(1).getLccvpmo1().getDati().getWpmoDtRicorSucc().getWpmoDtRicorSucc(), 8));
                    }
                }
                // COB_CODE:      IF IDSV0001-ESITO-OK         AND
                //                   IABV0006-SIMULAZIONE-NO
                //           *
                //                      THRU S10900-GESTIONE-EOC-EX
                //           *
                //                END-IF
                if (areaIdsv0001.getEsito().isIdsv0001EsitoOk() && iabv0006.getFlagSimulazione().isNo()) {
                    //
                    // COB_CODE: PERFORM S10900-GESTIONE-EOC
                    //              THRU S10900-GESTIONE-EOC-EX
                    s10900GestioneEoc();
                    //
                }
                //
                // COB_CODE: IF IDSV0001-ESITO-OK
                //                 THRU S10950-GESTIONE-FILEOUT-EX
                //           END-IF
                if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                    // COB_CODE: PERFORM S10950-GESTIONE-FILEOUT
                    //              THRU S10950-GESTIONE-FILEOUT-EX
                    s10950GestioneFileout();
                }
                //
                // COB_CODE:      IF IDSV0001-ESITO-OK
                //           *
                //                   END-IF
                //           *
                //                END-IF
                if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                    //
                    // COB_CODE:         IF W660-MF-CALCOLATO-SI
                    //           *
                    //                         THRU S11000-GESTIONE-FILE-MF-EX
                    //           *
                    //                   END-IF
                    if (ws.getW660AreaLoas0660().getMfCalcolato().isW660MfCalcolatoSi()) {
                        //
                        // COB_CODE: PERFORM S11000-GESTIONE-FILE-MF
                        //              THRU S11000-GESTIONE-FILE-MF-EX
                        s11000GestioneFileMf();
                        //
                    }
                    //
                }
            }
        }
    }

    /**Original name: S10100-ACQUISIZ-GAR-TRANCHE<br>
	 * <pre>----------------------------------------------------------------*
	 *       ACQUSIZIONE GARANZIE E TRANCHE DI GARANZIA                *
	 * ----------------------------------------------------------------*</pre>*/
    private void s10100AcquisizGarTranche() {
        // COB_CODE: MOVE 'S10100-ACQUISIZ-GAR-TRANCHE'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S10100-ACQUISIZ-GAR-TRANCHE");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        //
        // Ci potrebbero essere piy righe di pmo per una stessa Garanzia.
        // Viene salvato l'id-gar per effettuare le letture sulle
        // tabelle GAR e TRCH_DI_GAR una sola volta per ogni Garanzia
        // COB_CODE: IF WPMO-ELE-PARAM-MOV-MAX > 1
        //               END-PERFORM
        //           END-IF
        if (areaMain.getWpmoEleParamMovMax() > 1) {
            // COB_CODE: MOVE ZERO
            //             TO WK-APPO-ID-GAR
            ws.setWkAppoIdGar(0);
            // COB_CODE: PERFORM VARYING IX-TAB-PMO FROM 1 BY 1
            //             UNTIL IX-TAB-PMO > WPMO-ELE-PARAM-MOV-MAX
            //                 OR IDSV0001-ESITO-KO
            //                 END-PERFORM
            //            END-PERFORM
            ws.getIxIndici().setTabPmo(((short)1));
            while (!(ws.getIxIndici().getTabPmo() > areaMain.getWpmoEleParamMovMax() || areaIdsv0001.getEsito().isIdsv0001EsitoKo())) {
                // COB_CODE: COMPUTE IX-TAB-PMO-2 = IX-TAB-PMO + 1
                ws.getIxIndici().setTabPmo2(Trunc.toShort(ws.getIxIndici().getTabPmo() + 1, 4));
                // COB_CODE: PERFORM UNTIL IX-TAB-PMO-2 > WPMO-ELE-PARAM-MOV-MAX
                //               OR IDSV0001-ESITO-KO
                //             COMPUTE IX-TAB-PMO-2 = IX-TAB-PMO-2 + 1
                //           END-PERFORM
                while (!(ws.getIxIndici().getTabPmo2() > areaMain.getWpmoEleParamMovMax() || areaIdsv0001.getEsito().isIdsv0001EsitoKo())) {
                    // COB_CODE: IF WPMO-ID-OGG(IX-TAB-PMO) =
                    //              WPMO-ID-OGG(IX-TAB-PMO-2)
                    //                 THRU GESTIONE-ERR-STD-EX
                    //           END-IF
                    if (areaMain.getWpmoTabParamMov(ws.getIxIndici().getTabPmo()).getLccvpmo1().getDati().getWpmoIdOgg() == areaMain.getWpmoTabParamMov(ws.getIxIndici().getTabPmo2()).getLccvpmo1().getDati().getWpmoIdOgg()) {
                        // COB_CODE: MOVE 'ERRORE - PRESENTI GARANZIE DUPLICATE'
                        //             TO WK-STRING
                        ws.getWkGestioneMsgErr().setStringFld("ERRORE - PRESENTI GARANZIE DUPLICATE");
                        // COB_CODE: MOVE '001114'
                        //             TO WK-COD-ERR
                        ws.getWkGestioneMsgErr().setCodErr("001114");
                        // COB_CODE: PERFORM GESTIONE-ERR-STD
                        //              THRU GESTIONE-ERR-STD-EX
                        gestioneErrStd();
                    }
                    // COB_CODE: COMPUTE IX-TAB-PMO-2 = IX-TAB-PMO-2 + 1
                    ws.getIxIndici().setTabPmo2(Trunc.toShort(ws.getIxIndici().getTabPmo2() + 1, 4));
                }
                ws.getIxIndici().setTabPmo(Trunc.toShort(ws.getIxIndici().getTabPmo() + 1, 4));
            }
        }
        //
        // COB_CODE:      PERFORM VARYING IX-TAB-PMO FROM 1 BY 1
        //                  UNTIL IX-TAB-PMO > WPMO-ELE-PARAM-MOV-MAX
        //                     OR IDSV0001-ESITO-KO
        //           *
        //                     END-IF
        //                END-PERFORM.
        ws.getIxIndici().setTabPmo(((short)1));
        while (!(ws.getIxIndici().getTabPmo() > areaMain.getWpmoEleParamMovMax() || areaIdsv0001.getEsito().isIdsv0001EsitoKo())) {
            //
            // COB_CODE: PERFORM S10110-LEGGI-GARANZIA
            //              THRU S10110-LEGGI-GARANZIA-EX
            s10110LeggiGaranzia();
            //
            // COB_CODE:              IF IDSV0001-ESITO-OK
            //           *
            //                           END-IF
            //           *
            //                     END-IF
            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                //
                // COB_CODE:                 IF WK-SCARTA-GAR-NO
                //           *
                //                                 THRU S10130-ACQUISIZ-TRANCHE-EX
                //           *
                //                           END-IF
                if (ws.getWkScartaGar().isNo()) {
                    //
                    // COB_CODE: PERFORM S10130-ACQUISIZ-TRANCHE
                    //              THRU S10130-ACQUISIZ-TRANCHE-EX
                    s10130AcquisizTranche();
                    //
                }
                //
            }
            ws.getIxIndici().setTabPmo(Trunc.toShort(ws.getIxIndici().getTabPmo() + 1, 4));
        }
        //
        // COB_CODE: IF IDSV0001-ESITO-OK
        //                 THRU S10170-VERIFICA-GARANZIE-EX
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: PERFORM S10170-VERIFICA-GARANZIE
            //              THRU S10170-VERIFICA-GARANZIE-EX
            s10170VerificaGaranzie();
        }
    }

    /**Original name: S10170-VERIFICA-GARANZIE<br>
	 * <pre>----------------------------------------------------------------*
	 *        VERIFICA LEGAME GARANZIE TRANCHE
	 * ----------------------------------------------------------------*</pre>*/
    private void s10170VerificaGaranzie() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'S10170-VERIFICA-GARANZIE' TO WK-LABEL-ERR
        ws.getWkGestioneMsgErr().setLabelErr("S10170-VERIFICA-GARANZIE");
        // COB_CODE: PERFORM DISPLAY-LABEL THRU DISPLAY-LABEL-EX
        displayLabel();
        //
        // --> Verifica che epr ogni Garanzia sia stata caricata
        // --> almeno una Tranche
        //
        // COB_CODE: IF WGRZ-ELE-GAR-MAX = 0
        //                    THRU EX-S0300
        //           END-IF
        if (ws.getWgrzAreaGaranzia().getEleGarMax() == 0) {
            // COB_CODE: SET WCOM-WRITE-NIENTE TO TRUE
            wcomIoStati.getLccc0261().getWrite().setWcomWriteNiente();
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL-ERR
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
            // COB_CODE: MOVE '005247'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005247");
            // COB_CODE: MOVE WPOL-IB-OGG(1:11)
            //             TO WK-IB-POLI-DISPLAY
            ws.setWkIbPoliDisplay(areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolIbOggFormatted().substring((1) - 1, 11));
            // COB_CODE: STRING 'POLIZZA ' WK-IB-POLI-DISPLAY
            //                  ' NON ELABORATA PER: '
            //                  'GARANZIE NON IN VIGORE'
            //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "POLIZZA ", ws.getWkIbPoliDisplayFormatted(), " NON ELABORATA PER: ", "GARANZIE NON IN VIGORE");
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
        // COB_CODE:      PERFORM VARYING IX-TAB-GRZ FROM 1 BY 1
        //                  UNTIL IX-TAB-GRZ > WGRZ-ELE-GAR-MAX
        //           *
        //                    END-IF
        //           *
        //                END-PERFORM.
        ws.getIxIndici().setTabGrz(((short)1));
        while (!(ws.getIxIndici().getTabGrz() > ws.getWgrzAreaGaranzia().getEleGarMax())) {
            //
            // COB_CODE: SET WK-TROV-NO
            //             TO TRUE
            ws.getWkTrovato().setNo();
            //
            // COB_CODE:          PERFORM VARYING IX-TAB-TGA FROM 1 BY 1
            //                      UNTIL IX-TAB-TGA > WTGA-ELE-TRAN-MAX
            //           *
            //                        END-IF
            //           *
            //                    END-PERFORM
            ws.getIxIndici().setTabTga(((short)1));
            while (!(ws.getIxIndici().getTabTga() > ws.getWtgaAreaTranche().getWtgaEleTranMax())) {
                //
                // COB_CODE:              IF WGRZ-ID-GAR(IX-TAB-GRZ) =
                //                           WTGA-ID-GAR(IX-TAB-TGA)
                //           *
                //                             TO TRUE
                //           *
                //                        END-IF
                if (ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzIdGar() == ws.getWtgaAreaTranche().getWtgaTab().getIdGar(ws.getIxIndici().getTabTga())) {
                    //
                    // COB_CODE: SET WK-TROV-SI
                    //             TO TRUE
                    ws.getWkTrovato().setSi();
                    //
                }
                //
                ws.getIxIndici().setTabTga(Trunc.toShort(ws.getIxIndici().getTabTga() + 1, 4));
            }
            //
            // COB_CODE:          IF WK-TROV-NO
            //           *
            //                          THRU GESTIONE-ERR-STD-EX
            //           *
            //                    END-IF
            if (ws.getWkTrovato().isNo()) {
                //
                // COB_CODE: MOVE 'NESSUNA TRANCHE DA ELAB. PER LA GARANZIA'
                //             TO WK-STRING
                ws.getWkGestioneMsgErr().setStringFld("NESSUNA TRANCHE DA ELAB. PER LA GARANZIA");
                // COB_CODE: MOVE '001114'
                //             TO WK-COD-ERR
                ws.getWkGestioneMsgErr().setCodErr("001114");
                // COB_CODE: PERFORM GESTIONE-ERR-STD
                //              THRU GESTIONE-ERR-STD-EX
                gestioneErrStd();
                //
            }
            //
            ws.getIxIndici().setTabGrz(Trunc.toShort(ws.getIxIndici().getTabGrz() + 1, 4));
        }
    }

    /**Original name: S10180-CARICA-TRANCHE<br>
	 * <pre>----------------------------------------------------------------*
	 *  CARICA TRANCHE IN WORKING
	 * ----------------------------------------------------------------*</pre>*/
    private void s10180CaricaTranche() {
        // COB_CODE: MOVE 'S10180-CARICA-TRANCHE'  TO WK-LABEL-ERR
        ws.getWkGestioneMsgErr().setLabelErr("S10180-CARICA-TRANCHE");
        // COB_CODE: MOVE WTGA-ELE-TRAN-MAX        TO IX-TAB-TGA
        ws.getIxIndici().setTabTga(ws.getWtgaAreaTranche().getWtgaEleTranMax());
        // COB_CODE: ADD 1                         TO IX-TAB-TGA
        ws.getIxIndici().setTabTga(Trunc.toShort(1 + ws.getIxIndici().getTabTga(), 4));
        // COB_CODE: IF IX-TAB-TGA > WK-TGA-MAX-C
        //                THRU GESTIONE-ERR-STD-EX
        //           ELSE
        //             SET WTGA-ST-INV(IX-TAB-TGA) TO TRUE
        //           END-IF.
        if (ws.getIxIndici().getTabTga() > ws.getWkTgaMax().getC()) {
            // COB_CODE: MOVE 'OVERFLOW CARICAMENTO TRANCHE DI GARANZIA'
            //              TO WK-STRING
            ws.getWkGestioneMsgErr().setStringFld("OVERFLOW CARICAMENTO TRANCHE DI GARANZIA");
            // COB_CODE: MOVE '005059'
            //              TO WK-COD-ERR
            ws.getWkGestioneMsgErr().setCodErr("005059");
            // COB_CODE: PERFORM GESTIONE-ERR-STD
            //              THRU GESTIONE-ERR-STD-EX
            gestioneErrStd();
        }
        else {
            // COB_CODE: MOVE IX-TAB-TGA             TO WTGA-ELE-TRAN-MAX
            ws.getWtgaAreaTranche().setWtgaEleTranMax(ws.getIxIndici().getTabTga());
            // COB_CODE: PERFORM VALORIZZA-OUTPUT-TGA
            //              THRU VALORIZZA-OUTPUT-TGA-EX
            valorizzaOutputTga();
            // COB_CODE: SET WTGA-ST-INV(IX-TAB-TGA) TO TRUE
            ws.getWtgaAreaTranche().getWtgaTab().setWtgaStInv(ws.getIxIndici().getTabTga());
        }
    }

    /**Original name: S10110-LEGGI-GARANZIA<br>
	 * <pre>----------------------------------------------------------------*
	 *        LETTURA DELLA TABELLA GAR                                *
	 * ----------------------------------------------------------------*</pre>*/
    private void s10110LeggiGaranzia() {
        // COB_CODE: MOVE 'S10110-LEGGI-GARANZIA' TO WK-LABEL-ERR
        ws.getWkGestioneMsgErr().setLabelErr("S10110-LEGGI-GARANZIA");
        // COB_CODE: PERFORM DISPLAY-LABEL THRU DISPLAY-LABEL-EX
        displayLabel();
        //
        // COB_CODE: PERFORM S10120-IMPOSTA-GARANZIA
        //              THRU S10120-IMPOSTA-GARANZIA-EX.
        s10120ImpostaGaranzia();
        //
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX
        callDispatcher();
        //
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //           *
        //                   END-EVALUATE
        //           *
        //                ELSE
        //           *
        //           *  --> Errore dispatcher
        //           *
        //                      THRU GESTIONE-ERR-STD-EX
        //           *
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            //
            //
            // COB_CODE:         EVALUATE TRUE
            //           *
            //                       WHEN IDSO0011-NOT-FOUND
            //           *
            //           *  --> Chiave non trovata
            //           *
            //                               THRU GESTIONE-ERR-STD-EX
            //           *
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //           *
            //           *  --> Operazione eseguita correttamente
            //           *
            //                            END-IF
            //           *
            //                       WHEN OTHER
            //           *
            //           *  --> Errore di accesso al db
            //           *
            //                               THRU GESTIONE-ERR-STD-EX
            //           *
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.NOT_FOUND://
                    //  --> Chiave non trovata
                    //
                    // COB_CODE: MOVE 'GARANZIA NON TROVATA'
                    //             TO WK-STRING
                    ws.getWkGestioneMsgErr().setStringFld("GARANZIA NON TROVATA");
                    // COB_CODE: MOVE '005069'
                    //             TO WK-COD-ERR
                    ws.getWkGestioneMsgErr().setCodErr("005069");
                    // COB_CODE: PERFORM GESTIONE-ERR-STD
                    //              THRU GESTIONE-ERR-STD-EX
                    gestioneErrStd();
                    //
                    break;

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://
                    //  --> Operazione eseguita correttamente
                    //
                    // COB_CODE: MOVE IDSO0011-BUFFER-DATI
                    //             TO GAR
                    ws.getGar().setGarFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    //
                    // COB_CODE: PERFORM S10125-CTRL-STATO-GAR
                    //              THRU S10125-CTRL-STATO-GAR-EX
                    s10125CtrlStatoGar();
                    //
                    // COB_CODE: IF WK-SCARTA-GAR-NO
                    //              END-IF
                    //           END-IF
                    if (ws.getWkScartaGar().isNo()) {
                        // COB_CODE: ADD 1              TO IX-TAB-GRZ
                        ws.getIxIndici().setTabGrz(Trunc.toShort(1 + ws.getIxIndici().getTabGrz(), 4));
                        // COB_CODE:                     IF IX-TAB-GRZ > WK-GRZ-MAX-B
                        //                                     THRU GESTIONE-ERR-STD-EX
                        //                               ELSE
                        //                                   TO TRUE
                        //           *
                        //                               END-IF
                        if (ws.getIxIndici().getTabGrz() > ws.getLccvgrzz().getMaxB()) {
                            // COB_CODE: MOVE 'OVERFLOW CARICAMENTO GARANZIA'
                            //             TO WK-STRING
                            ws.getWkGestioneMsgErr().setStringFld("OVERFLOW CARICAMENTO GARANZIA");
                            // COB_CODE: MOVE '005059'
                            //             TO WK-COD-ERR
                            ws.getWkGestioneMsgErr().setCodErr("005059");
                            // COB_CODE: PERFORM GESTIONE-ERR-STD
                            //              THRU GESTIONE-ERR-STD-EX
                            gestioneErrStd();
                        }
                        else {
                            // COB_CODE: MOVE IX-TAB-GRZ  TO WGRZ-ELE-GAR-MAX
                            ws.getWgrzAreaGaranzia().setEleGarMax(ws.getIxIndici().getTabGrz());
                            //
                            // COB_CODE: PERFORM VALORIZZA-OUTPUT-GRZ
                            //              THRU VALORIZZA-OUTPUT-GRZ-EX
                            valorizzaOutputGrz();
                            //
                            // COB_CODE: SET WGRZ-ST-INV(IX-TAB-GRZ)
                            //            TO TRUE
                            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getStatus().setInv();
                            //
                        }
                    }
                    //
                    break;

                default://
                    //  --> Errore di accesso al db
                    //
                    // COB_CODE: MOVE 'ERRORE LETTURA GARANZIA'
                    //             TO WK-STRING
                    ws.getWkGestioneMsgErr().setStringFld("ERRORE LETTURA GARANZIA");
                    // COB_CODE: MOVE '005016'
                    //             TO WK-COD-ERR
                    ws.getWkGestioneMsgErr().setCodErr("005016");
                    // COB_CODE: PERFORM GESTIONE-ERR-STD
                    //              THRU GESTIONE-ERR-STD-EX
                    gestioneErrStd();
                    //
                    break;
            }
            //
        }
        else {
            //
            //  --> Errore dispatcher
            //
            // COB_CODE: MOVE 'S10110-LEGGI-GARANZIA'
            //             TO WK-LABEL-ERR
            ws.getWkGestioneMsgErr().setLabelErr("S10110-LEGGI-GARANZIA");
            // COB_CODE: MOVE 'ERRORE DISPATCHER LETTURA GAR'
            //             TO WK-STRING
            ws.getWkGestioneMsgErr().setStringFld("ERRORE DISPATCHER LETTURA GAR");
            // COB_CODE: MOVE '005016'
            //             TO WK-COD-ERR
            ws.getWkGestioneMsgErr().setCodErr("005016");
            // COB_CODE: PERFORM GESTIONE-ERR-STD
            //              THRU GESTIONE-ERR-STD-EX
            gestioneErrStd();
            //
        }
    }

    /**Original name: S10120-IMPOSTA-GARANZIA<br>
	 * <pre> -------------------------------------------------------------- *
	 *        VALORIZZAZIONE DELLE CHIAVI DELLA TABELLA GAR            *
	 *  -------------------------------------------------------------- *</pre>*/
    private void s10120ImpostaGaranzia() {
        // COB_CODE: MOVE WPMO-ID-OGG(IX-TAB-PMO)
        //             TO GRZ-ID-GAR.
        ws.getGar().setGrzIdGar(areaMain.getWpmoTabParamMov(ws.getIxIndici().getTabPmo()).getLccvpmo1().getDati().getWpmoIdOgg());
        //
        //  --> La data effetto viene sempre valorizzata
        //
        // COB_CODE: MOVE ZERO
        //             TO IDSI0011-DATA-INIZIO-EFFETTO
        //                IDSI0011-DATA-FINE-EFFETTO
        //                IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        //
        //  --> Nome tabella fisica db
        //
        // COB_CODE: MOVE 'GAR'
        //             TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("GAR");
        //
        //  --> Dclgen tabella
        //
        // COB_CODE: MOVE GAR
        //             TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getGar().getGarFormatted());
        //
        //  --> Tipo operazione
        //
        // COB_CODE: SET IDSI0011-SELECT
        //             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        //
        //  --> Tipo livello
        //
        // COB_CODE: SET IDSI0011-ID
        //             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setIdsi0011Id();
        //
        //  --> Modalita di accesso
        //
        // COB_CODE: SET IDSI0011-TRATT-DEFAULT
        //             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setIdsi0011TrattDefault();
        //
        //  --> Inizializzazione Return Code
        //
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC
        //             TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL
        //             TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
    }

    /**Original name: S10125-CTRL-STATO-GAR<br>
	 * <pre> -------------------------------------------------------------- *
	 *    CONTROLLO STATO E CASUSALE DELLA GARANZIA                    *
	 *  -------------------------------------------------------------- *</pre>*/
    private void s10125CtrlStatoGar() {
        // COB_CODE: MOVE 'S10125-CTRL-STATO-GAR' TO WK-LABEL-ERR
        ws.getWkGestioneMsgErr().setLabelErr("S10125-CTRL-STATO-GAR");
        // COB_CODE: PERFORM DISPLAY-LABEL THRU DISPLAY-LABEL-EX
        displayLabel();
        //
        //  Controllo Stato Contratto
        //
        // COB_CODE: MOVE GRZ-ID-GAR
        //             TO WK-STB-ID-OGG.
        ws.setWkStbIdOgg(ws.getGar().getGrzIdGar());
        // COB_CODE: MOVE 'GA'
        //             TO WK-STB-TP-OGG.
        ws.setWkStbTpOgg("GA");
        //
        // COB_CODE: SET WK-SCARTA-GAR-NO
        //             TO TRUE.
        ws.getWkScartaGar().setNo();
        //
        // COB_CODE: PERFORM S00420-IMPOSTA-STB
        //              THRU S00420-IMPOSTA-STB-EX.
        s00420ImpostaStb();
        //
        // COB_CODE: PERFORM S00430-LEGGI-STB
        //              THRU S00430-LEGGI-STB-EX.
        s00430LeggiStb();
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                   END-IF
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE: MOVE STB-TP-STAT-BUS
            //             TO WS-TP-STAT-BUS
            ws.getWsTpStatBus().setWsTpStatBus(ws.getStatOggBus().getStbTpStatBus());
            // COB_CODE: MOVE STB-TP-CAUS
            //             TO WS-TP-CAUS
            ws.getWsTpCaus().setWsTpCaus(ws.getStatOggBus().getStbTpCaus());
            //
            // --> Se garanzia h in stato Vigore ed Erogazione Rendita,
            // -->  imposta flag
            //
            // COB_CODE:         IF IN-VIGORE
            //           *
            //           *          IF RENDITA-EROGAZIONE
            //                         CONTINUE
            //           *             SET SI-GAR-REND
            //           *               TO TRUE
            //           *               TO TRUE
            //           *
            //           *          END-IF
            //           *          END-IF
            //                   ELSE
            //           *
            //                       TO TRUE
            //           *
            //                   END-IF
            if (ws.getWsTpStatBus().isInVigore()) {
            //
            //          IF RENDITA-EROGAZIONE
            // COB_CODE: CONTINUE
            //continue
            //             SET SI-GAR-REND
            //               TO TRUE
            //               TO TRUE
            //
            //          END-IF
            //          END-IF
            }
            else {
                //
                // COB_CODE: SET WK-SCARTA-GAR-SI
                //            TO TRUE
                ws.getWkScartaGar().setSi();
                //
            }
            //
        }
    }

    /**Original name: S10130-ACQUISIZ-TRANCHE<br>
	 * <pre> -------------------------------------------------------------- *
	 *    GESTIONE DELL'ACQUISIZIONE DELLE TRANCHE DI GARANZIA         *
	 *  -------------------------------------------------------------- *
	 *  -->> Indicatore di fine fetch</pre>*/
    private void s10130AcquisizTranche() {
        // COB_CODE: SET WK-FINE-FETCH-NO
        //             TO TRUE.
        ws.getWkFineFetch().setNo();
        //
        // COB_CODE: PERFORM S10140-IMPOSTA-TRANCHE
        //              THRU S10140-IMPOSTA-TRANCHE-EX
        s10140ImpostaTranche();
        // COB_CODE: SET IDSI0011-FETCH-FIRST
        //             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setFetchFirst();
        // COB_CODE: PERFORM S10150-LEGGI-TRANCHE
        //              THRU S10150-LEGGI-TRANCHE-EX
        //             UNTIL WK-FINE-FETCH-SI
        //                OR IDSV0001-ESITO-KO.
        while (!(ws.getWkFineFetch().isSi() || areaIdsv0001.getEsito().isIdsv0001EsitoKo())) {
            s10150LeggiTranche();
        }
    }

    /**Original name: S10140-IMPOSTA-TRANCHE<br>
	 * <pre> -------------------------------------------------------------- *
	 *        VALORIZZAZIONE DELLE CHIAVI DELLA TABELLA TRCH_DI_GAR    *
	 *  -------------------------------------------------------------- *</pre>*/
    private void s10140ImpostaTranche() {
        // COB_CODE: INITIALIZE LDBV0011
        initLdbv0011();
        // COB_CODE: MOVE WGRZ-ID-GAR(IX-TAB-GRZ)
        //             TO LDBV0011-ID-GAR.
        ws.getLdbv0011().setGar(ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzIdGar());
        //    MODIFICA SIR CQPrd00019650
        // COB_CODE: MOVE WGRZ-TP-PER-PRE(IX-TAB-GRZ)
        //             TO ISPV0000-PERIODO-PREM
        ws.getIspv0000().getPeriodoPrem().setPeriodoPremFormatted(ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzTpPerPreFormatted());
        //
        //  --> La data effetto viene sempre valorizzata
        //
        // COB_CODE: MOVE ZERO
        //             TO IDSI0011-DATA-INIZIO-EFFETTO
        //                IDSI0011-DATA-FINE-EFFETTO
        //                IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        //
        //  --> Nome tabella fisica db
        //
        // COB_CODE: MOVE 'LDBS0130'
        //             TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("LDBS0130");
        //
        //  --> Dclgen tabella
        //
        // COB_CODE: MOVE SPACES
        //             TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
        // COB_CODE: MOVE LDBV0011
        //             TO IDSI0011-BUFFER-WHERE-COND.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferWhereCond(ws.getLdbv0011().getLdbv0011Formatted());
        //
        //  --> Tipo operazione
        //
        // COB_CODE: SET IDSI0011-WHERE-CONDITION
        //             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setWhereCondition();
        //
        //  --> Modalita di accesso
        //
        // COB_CODE: SET IDSI0011-TRATT-DEFAULT
        //             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setIdsi0011TrattDefault();
        //
        //  --> Inizializzazione Return Code
        //
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC
        //             TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL
        //             TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
    }

    /**Original name: S10150-LEGGI-TRANCHE<br>
	 * <pre>----------------------------------------------------------------*
	 *             LETTURA DELLE TABELLA TRANCHE DI GARANZIA           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s10150LeggiTranche() {
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX
        callDispatcher();
        //
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //           *
        //                   END-EVALUATE
        //           *
        //                ELSE
        //           *
        //           *  --> Errore dispatcher
        //           *
        //                      THRU GESTIONE-ERR-STD-EX
        //           *
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            //
            //
            // COB_CODE:         EVALUATE TRUE
            //           *
            //                       WHEN IDSO0011-NOT-FOUND
            //           *
            //           *  --> Chiave non trovata
            //           *
            //                                 TO TRUE
            //           *
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //           *
            //           *  --> Operazione eseguita correttamente
            //           *
            //                              TO TRUE
            //           *
            //                       WHEN OTHER
            //           *
            //           *  --> Errore di accesso al db
            //           *
            //                               THRU GESTIONE-ERR-STD-EX
            //           *
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.NOT_FOUND://
                    //  --> Chiave non trovata
                    //
                    // COB_CODE:                  IF IDSI0011-FETCH-FIRST
                    //           *
                    //                                  THRU GESTIONE-ERR-STD-EX
                    //           *
                    //                            END-IF
                    if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchFirst()) {
                        //
                        // COB_CODE: MOVE 'S10150-LEGGI-TRANCHE'
                        //             TO WK-LABEL-ERR
                        ws.getWkGestioneMsgErr().setLabelErr("S10150-LEGGI-TRANCHE");
                        // COB_CODE: MOVE 'TRANCHE NON TROVATA'
                        //             TO WK-STRING
                        ws.getWkGestioneMsgErr().setStringFld("TRANCHE NON TROVATA");
                        // COB_CODE: MOVE '005069'
                        //             TO WK-COD-ERR
                        ws.getWkGestioneMsgErr().setCodErr("005069");
                        // COB_CODE: PERFORM GESTIONE-ERR-STD
                        //              THRU GESTIONE-ERR-STD-EX
                        gestioneErrStd();
                        //
                    }
                    //
                    //  --> Fine occorrenze fetch
                    //
                    // COB_CODE: SET WK-FINE-FETCH-SI
                    //             TO TRUE
                    ws.getWkFineFetch().setSi();
                    //
                    break;

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://
                    //  --> Operazione eseguita correttamente
                    //
                    // COB_CODE: MOVE IDSO0011-BUFFER-DATI
                    //             TO TRCH-DI-GAR
                    ws.getTrchDiGar().setTrchDiGarFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    //
                    // COB_CODE: SET WK-SCARTA-TGA-NO
                    //             TO TRUE
                    ws.getWkScartaTga().setNo();
                    //
                    // COB_CODE:                  IF WK-SCARTA-TGA-NO
                    //           *
                    //                                  THRU S10145-CTRL-STATO-TGA-EX
                    //           *
                    //                            END-IF
                    if (ws.getWkScartaTga().isNo()) {
                        //
                        // COB_CODE: PERFORM S10145-CTRL-STATO-TGA
                        //              THRU S10145-CTRL-STATO-TGA-EX
                        s10145CtrlStatoTga();
                        //
                    }
                    //
                    // COB_CODE:                  IF WK-SCARTA-TGA-NO
                    //           *
                    //                                  THRU S10160-GESTIONE-DECOR-EX
                    //           *
                    //                            END-IF
                    if (ws.getWkScartaTga().isNo()) {
                        //
                        // COB_CODE: PERFORM S10160-GESTIONE-DECOR
                        //              THRU S10160-GESTIONE-DECOR-EX
                        s10160GestioneDecor();
                        //
                    }
                    //
                    // COB_CODE:                  IF WK-SCARTA-TGA-NO
                    //           *
                    //                                  THRU S10180-CARICA-TRANCHE-EX
                    //           *
                    //                            END-IF
                    if (ws.getWkScartaTga().isNo()) {
                        //
                        // COB_CODE: PERFORM S10180-CARICA-TRANCHE
                        //              THRU S10180-CARICA-TRANCHE-EX
                        s10180CaricaTranche();
                        //
                    }
                    //
                    // COB_CODE: PERFORM S10140-IMPOSTA-TRANCHE
                    //              THRU S10140-IMPOSTA-TRANCHE-EX
                    s10140ImpostaTranche();
                    //
                    // COB_CODE: SET IDSI0011-FETCH-NEXT
                    //              TO TRUE
                    ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setFetchNext();
                    //
                    break;

                default://
                    //  --> Errore di accesso al db
                    //
                    // COB_CODE: MOVE 'S10150-LEGGI-TRANCHE'
                    //             TO WK-LABEL-ERR
                    ws.getWkGestioneMsgErr().setLabelErr("S10150-LEGGI-TRANCHE");
                    // COB_CODE: MOVE 'ERRORE LETTURA TRANCHE'
                    //             TO WK-STRING
                    ws.getWkGestioneMsgErr().setStringFld("ERRORE LETTURA TRANCHE");
                    // COB_CODE: MOVE '005016'
                    //             TO WK-COD-ERR
                    ws.getWkGestioneMsgErr().setCodErr("005016");
                    // COB_CODE: PERFORM GESTIONE-ERR-STD
                    //              THRU GESTIONE-ERR-STD-EX
                    gestioneErrStd();
                    //
                    break;
            }
            //
        }
        else {
            //
            //  --> Errore dispatcher
            //
            // COB_CODE: MOVE 'S10150-LEGGI-TRANCHE'
            //             TO WK-LABEL-ERR
            ws.getWkGestioneMsgErr().setLabelErr("S10150-LEGGI-TRANCHE");
            // COB_CODE: MOVE 'ERRORE DISPATCHER LETTURA TRANCHE'
            //             TO WK-STRING
            ws.getWkGestioneMsgErr().setStringFld("ERRORE DISPATCHER LETTURA TRANCHE");
            // COB_CODE: MOVE '005016'
            //             TO WK-COD-ERR
            ws.getWkGestioneMsgErr().setCodErr("005016");
            // COB_CODE: PERFORM GESTIONE-ERR-STD
            //              THRU GESTIONE-ERR-STD-EX
            gestioneErrStd();
            //
        }
    }

    /**Original name: S10145-CTRL-STATO-TGA<br>
	 * <pre> -------------------------------------------------------------- *
	 *    CONTROLLO STATO E CASUSALE DELLA TRANCHE DI GARANZIA
	 *  -------------------------------------------------------------- *
	 *   Controllo Stato Contratto</pre>*/
    private void s10145CtrlStatoTga() {
        // COB_CODE: MOVE TGA-ID-TRCH-DI-GAR
        //             TO WK-STB-ID-OGG.
        ws.setWkStbIdOgg(ws.getTrchDiGar().getTgaIdTrchDiGar());
        // COB_CODE: MOVE 'TG'
        //             TO WK-STB-TP-OGG.
        ws.setWkStbTpOgg("TG");
        //
        // COB_CODE: PERFORM S00420-IMPOSTA-STB
        //              THRU S00420-IMPOSTA-STB-EX.
        s00420ImpostaStb();
        //
        // COB_CODE: PERFORM S00430-LEGGI-STB
        //              THRU S00430-LEGGI-STB-EX.
        s00430LeggiStb();
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                   END-IF
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE: MOVE STB-TP-STAT-BUS
            //             TO WS-TP-STAT-BUS
            ws.getWsTpStatBus().setWsTpStatBus(ws.getStatOggBus().getStbTpStatBus());
            // COB_CODE: MOVE STB-TP-CAUS
            //             TO WS-TP-CAUS
            ws.getWsTpCaus().setWsTpCaus(ws.getStatOggBus().getStbTpCaus());
            //
            // --> Se wcom-write-niente, forzatura su idsv0001-esito-ko
            // --> per skippare tutta l'elaborazione
            //
            // COB_CODE:         IF IN-VIGORE
            //           *
            //           *          CONTINUE
            //                      END-IF
            //           *
            //                   ELSE
            //           *
            //                       TO TRUE
            //           *
            //                   END-IF
            if (ws.getWsTpStatBus().isInVigore()) {
                //
                //          CONTINUE
                // COB_CODE: IF WK-ORIG-RIV-IN           AND
                //              ATTESA-PERFEZIONAMENTO   AND
                //             (WGRZ-TP-PER-PRE (1) = 'R' OR 'U')
                //              SET WK-SCARTA-TGA-SI TO TRUE
                //           END-IF
                if (ws.getWkOrigRiv().isInFld() && ws.getWsTpCaus().isAttesaPerfezionamento() && (Conditions.eq(ws.getWgrzAreaGaranzia().getTabGar(1).getLccvgrz1().getDati().getWgrzTpPerPre(), "R") || Conditions.eq(ws.getWgrzAreaGaranzia().getTabGar(1).getLccvgrz1().getDati().getWgrzTpPerPre(), "U"))) {
                    // COB_CODE: SET WK-SCARTA-TGA-SI TO TRUE
                    ws.getWkScartaTga().setSi();
                }
                //
            }
            else {
                //
                // COB_CODE: SET WK-SCARTA-TGA-SI
                //            TO TRUE
                ws.getWkScartaTga().setSi();
                //
            }
            //
        }
    }

    /**Original name: S10160-GESTIONE-DECOR<br>
	 * <pre>----------------------------------------------------------------*
	 *   SCARTO TRANCHE CON DATA DECOR = A DATA DI RICORRENZA          *
	 * ----------------------------------------------------------------*
	 *     IF WPMO-DT-RICOR-SUCC(IX-TAB-PMO) = TGA-DT-DECOR</pre>*/
    private void s10160GestioneDecor() {
        // COB_CODE:      IF WPMO-DT-RICOR-SUCC(IX-TAB-PMO) <= TGA-DT-DECOR
        //                     TO TRUE
        //           *
        //                ELSE
        //           *
        //                   END-IF
        //           *
        //                END-IF.
        if (areaMain.getWpmoTabParamMov(ws.getIxIndici().getTabPmo()).getLccvpmo1().getDati().getWpmoDtRicorSucc().getWpmoDtRicorSucc() <= ws.getTrchDiGar().getTgaDtDecor()) {
            // COB_CODE: SET WK-SCARTA-TGA-SI
            //             TO TRUE
            ws.getWkScartaTga().setSi();
            //
        }
        else {
            //
            // COB_CODE:         IF WPMO-TP-OGG-RIVAL(1) = 'TG'
            //           *
            //                         THRU S10165-DECOR-X-RIVAL-TGA-EX
            //           *
            //                   END-IF
            if (Conditions.eq(areaMain.getWpmoTabParamMov(1).getLccvpmo1().getDati().getWpmoTpOggRival(), "TG")) {
                //
                // COB_CODE: PERFORM S10165-DECOR-X-RIVAL-TGA
                //              THRU S10165-DECOR-X-RIVAL-TGA-EX
                s10165DecorXRivalTga();
                //
            }
            //
        }
    }

    /**Original name: S10165-DECOR-X-RIVAL-TGA<br>
	 * <pre>----------------------------------------------------------------*
	 *   SCARTO TRANCHE CON DATA DECOR (MM-GG) <> DA DATA RICORRENZA   *
	 *          SOLO PER RIVALUTAZIONE A RICORRENZA DI TRANCHE         *
	 * ----------------------------------------------------------------*</pre>*/
    private void s10165DecorXRivalTga() {
        // COB_CODE: MOVE ZEROES                         TO WK-APPO-DT-NUM
        ws.setWkAppoDtNum(0);
        // COB_CODE: MOVE WPMO-DT-RICOR-SUCC(IX-TAB-PMO) TO WK-APPO-DT-NUM
        ws.setWkAppoDtNum(TruncAbs.toInt(areaMain.getWpmoTabParamMov(ws.getIxIndici().getTabPmo()).getLccvpmo1().getDati().getWpmoDtRicorSucc().getWpmoDtRicorSucc(), 8));
        // COB_CODE: MOVE WK-APPO-DT-NUM                 TO WK-APPO-DT
        ws.getWkAppoDt().setWkAppoDtFormatted(ws.getWkAppoDtNumFormatted());
        //
        // COB_CODE: MOVE ZEROES                         TO WK-APPO-DT-NUM
        ws.setWkAppoDtNum(0);
        // COB_CODE: MOVE TGA-DT-DECOR                   TO WK-APPO-DT-NUM
        ws.setWkAppoDtNum(TruncAbs.toInt(ws.getTrchDiGar().getTgaDtDecor(), 8));
        //       TO WK-DATA-AMG.
        // COB_CODE: MOVE WK-APPO-DT-NUM
        //             TO WK-DATA-AMG.
        ws.getWkDataAmg().setWkDataAmgFormatted(ws.getWkAppoDtNumFormatted());
        //
        // COB_CODE:      IF MM-SYS OF WK-DATA-AMG NOT = WK-APPO-DT-MM OR
        //                   GG-SYS OF WK-DATA-AMG NOT = WK-APPO-DT-GG
        //                     TO TRUE
        //           *
        //                END-IF.
        if (!Conditions.eq(ws.getWkDataAmg().getMmSys(), ws.getWkAppoDt().getMmFormatted()) || !Conditions.eq(ws.getWkDataAmg().getGgSys(), ws.getWkAppoDt().getGgFormatted())) {
            // COB_CODE: SET WK-SCARTA-TGA-SI
            //            TO TRUE
            ws.getWkScartaTga().setSi();
            //
        }
    }

    /**Original name: S10450-GEST-RIC-SUCC-PMO<br>
	 * <pre>----------------------------------------------------------------*
	 *  GESTIONE DELLA DATA RICORRENZA SUCCESSIVA DELLA PMO
	 * ----------------------------------------------------------------*</pre>*/
    private void s10450GestRicSuccPmo() {
        Lccs0320 lccs0320 = null;
        // COB_CODE: MOVE 'S10450-GEST-RIC-SUCC-PMO'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S10450-GEST-RIC-SUCC-PMO");
        // COB_CODE: SET  IDSV8888-BUSINESS-DBG          TO TRUE
        ws.getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
        // COB_CODE: MOVE 'LCCS0320'                 TO IDSV8888-NOME-PGM.
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("LCCS0320");
        // COB_CODE: SET  IDSV8888-INIZIO            TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setInizio();
        // COB_CODE: MOVE SPACES                     TO IDSV8888-DESC-PGM
        //skipped translation for moving SPACES to IDSV8888-DESC-PGM; considered in STRING statement translation below
        // COB_CODE: STRING 'DT Ricorr. Succ. PMO'
        //                   DELIMITED BY SIZE
        //                   INTO IDSV8888-DESC-PGM
        //           END-STRING
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("DT Ricorr. Succ. PMO");
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        //
        // COB_CODE:      CALL LCCS0320 USING AREA-IDSV0001
        //                                    VPMO-AREA-PARAM-MOVI
        //                                    WPOL-AREA-POLIZZA
        //                                    WADE-AREA-ADESIONE
        //                                    WGRZ-AREA-GARANZIA
        //                                    WTGA-AREA-TRANCHE
        //           *
        //                ON EXCEPTION
        //           *
        //                        THRU GESTIONE-ERR-SIST-EX
        //           *
        //                END-CALL.
        try {
            lccs0320 = Lccs0320.getInstance();
            lccs0320.run(new Object[] {areaIdsv0001, ws.getWorkCommarea(), areaMain.getWpolAreaPolizza(), areaMain.getWadeAreaAdesione(), ws.getWgrzAreaGaranzia(), ws.getWtgaAreaTranche()});
        }
        catch (ProgramExecutionException __ex) {
            //
            // COB_CODE: MOVE 'LCCS0320'
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe("LCCS0320");
            // COB_CODE: MOVE 'ERRORE CALL MODULO LCCS0320'
            //             TO CALL-DESC
            ws.getIdsv0002().setCallDesc("ERRORE CALL MODULO LCCS0320");
            //
            // COB_CODE: PERFORM GESTIONE-ERR-SIST
            //              THRU GESTIONE-ERR-SIST-EX
            gestioneErrSist();
            //
        }
        // COB_CODE: SET  IDSV8888-BUSINESS-DBG          TO TRUE
        ws.getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
        // COB_CODE: MOVE 'LCCS0320'                 TO IDSV8888-NOME-PGM.
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("LCCS0320");
        // COB_CODE: SET  IDSV8888-FINE            TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setFine();
        // COB_CODE: MOVE SPACES                     TO IDSV8888-DESC-PGM
        //skipped translation for moving SPACES to IDSV8888-DESC-PGM; considered in STRING statement translation below
        // COB_CODE: STRING 'DT RICORR. SUCC. PMO'
        //                   DELIMITED BY SIZE
        //                   INTO IDSV8888-DESC-PGM
        //           END-STRING
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("DT RICORR. SUCC. PMO");
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
    }

    /**Original name: S10200-RIVAL-X-INCASSO-COL<br>
	 * <pre>----------------------------------------------------------------*
	 *               GESTIONE DEL TIPO DI RIVALUTAZIONE                *
	 * ----------------------------------------------------------------*</pre>*/
    private void s10200RivalXIncassoCol() {
        // COB_CODE: MOVE 'S10200-RIVAL-X-INCASSO-COL'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S10200-RIVAL-X-INCASSO-COL");
        //
        // COB_CODE: PERFORM S10210-PREP-CALL-LOAS0670
        //              THRU S10210-PREP-CALL-LOAS0670-EX.
        s10210PrepCallLoas0670();
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                      THRU S10220-CALL-LOAS0670-EX
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE: PERFORM S10220-CALL-LOAS0670
            //              THRU S10220-CALL-LOAS0670-EX
            s10220CallLoas0670();
            //
        }
    }

    /**Original name: S10210-PREP-CALL-LOAS0670<br>
	 * <pre>----------------------------------------------------------------*
	 *   PREPARA AREA DATI PER CALL SERVIZIO LOAS0670                  *
	 * ----------------------------------------------------------------*</pre>*/
    private void s10210PrepCallLoas0670() {
        // COB_CODE: MOVE 'S10210-PREP-CALL-LOAS0670' TO WK-LABEL-ERR
        ws.getWkGestioneMsgErr().setLabelErr("S10210-PREP-CALL-LOAS0670");
        // COB_CODE: PERFORM DISPLAY-LABEL THRU DISPLAY-LABEL-EX
        displayLabel();
        //
        // COB_CODE: SET W670-GGCONTESTO-CONT-NO   TO TRUE
        ws.getW670AreaLoas0670().getDatiGgincassoContesto().setW670GgcontestoContNo();
        // COB_CODE: MOVE ZEROES                   TO W670-GGINCASSO
        //                                            W670-NUM-MAX-ELE.
        ws.getW670AreaLoas0670().setGgincasso(0);
        ws.getW670AreaLoas0670().setNumMaxEle(((short)0));
    }

    /**Original name: S10220-CALL-LOAS0670<br>
	 * <pre>----------------------------------------------------------------*
	 *             CALL SERVIZIO LOAS0670                              *
	 * ----------------------------------------------------------------*</pre>*/
    private void s10220CallLoas0670() {
        Loas0670 loas0670 = null;
        // COB_CODE: MOVE 'S10220-CALL-LOAS0670'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S10220-CALL-LOAS0670");
        // COB_CODE: SET  IDSV8888-BUSINESS-DBG          TO TRUE
        ws.getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
        // COB_CODE: MOVE 'LOAS0670'                 TO IDSV8888-NOME-PGM.
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("LOAS0670");
        // COB_CODE: SET  IDSV8888-INIZIO            TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setInizio();
        // COB_CODE: MOVE SPACES                     TO IDSV8888-DESC-PGM
        //skipped translation for moving SPACES to IDSV8888-DESC-PGM; considered in STRING statement translation below
        // COB_CODE: STRING 'Rivalut x incasso coll.'
        //                   DELIMITED BY SIZE
        //                   INTO IDSV8888-DESC-PGM
        //           END-STRING
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("Rivalut x incasso coll.");
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        //
        // COB_CODE:       CALL LOAS0670        USING AREA-IDSV0001
        //                                            WCOM-AREA-STATI
        //                                            WPMO-AREA-PARAM-MOVI
        //                                            WPOL-AREA-POLIZZA
        //                                            WADE-AREA-ADESIONE
        //                                            WGRZ-AREA-GARANZIA
        //                                            WTGA-AREA-TRANCHE
        //                                            W670-AREA-LOAS0670
        //           *
        //                 ON EXCEPTION
        //           *
        //                         THRU GESTIONE-ERR-SIST-EX
        //           *
        //                 END-CALL.
        try {
            loas0670 = Loas0670.getInstance();
            loas0670.run(new Object[] {areaIdsv0001, wcomIoStati, areaMain, areaMain.getWpolAreaPolizza(), areaMain.getWadeAreaAdesione(), ws.getWgrzAreaGaranzia(), ws.getWtgaAreaTranche(), ws.getW670AreaLoas0670()});
        }
        catch (ProgramExecutionException __ex) {
            //
            // COB_CODE: MOVE 'LOAS0670'
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe("LOAS0670");
            // COB_CODE: MOVE 'ERRORE CALL MODULO LOAS0670'
            //             TO CALL-DESC
            ws.getIdsv0002().setCallDesc("ERRORE CALL MODULO LOAS0670");
            // COB_CODE: MOVE 'S10220-CALL-LOAS0670'
            //             TO WK-LABEL-ERR
            ws.getWkGestioneMsgErr().setLabelErr("S10220-CALL-LOAS0670");
            //
            // COB_CODE: PERFORM GESTIONE-ERR-SIST
            //              THRU GESTIONE-ERR-SIST-EX
            gestioneErrSist();
            //
        }
        // COB_CODE: SET  IDSV8888-BUSINESS-DBG          TO TRUE
        ws.getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
        // COB_CODE: MOVE 'LOAS0670'                 TO IDSV8888-NOME-PGM.
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("LOAS0670");
        // COB_CODE: SET  IDSV8888-FINE              TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setFine();
        // COB_CODE: MOVE SPACES                     TO IDSV8888-DESC-PGM
        //skipped translation for moving SPACES to IDSV8888-DESC-PGM; considered in STRING statement translation below
        // COB_CODE: STRING 'Rivalut x incasso coll.'
        //                   DELIMITED BY SIZE
        //                   INTO IDSV8888-DESC-PGM
        //           END-STRING
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("Rivalut x incasso coll.");
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
    }

    /**Original name: S10300-RIVAL-X-INCASSO-IND<br>
	 * <pre>----------------------------------------------------------------*
	 *               GESTIONE DEL TIPO DI RIVALUTAZIONE                *
	 * ----------------------------------------------------------------*</pre>*/
    private void s10300RivalXIncassoInd() {
        // COB_CODE: MOVE 'S10300-RIVAL-X-INCASSO-IND'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S10300-RIVAL-X-INCASSO-IND");
        //
        // COB_CODE: PERFORM S10310-PREP-CALL-LOAS0870
        //              THRU S10310-PREP-CALL-LOAS0870-EX.
        s10310PrepCallLoas0870();
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                      THRU S10320-CALL-LOAS0870-EX
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE: PERFORM S10320-CALL-LOAS0870
            //              THRU S10320-CALL-LOAS0870-EX
            s10320CallLoas0870();
            //
        }
    }

    /**Original name: S10310-PREP-CALL-LOAS0870<br>
	 * <pre>----------------------------------------------------------------*
	 *   PREPARA AREA DATI PER CALL SERVIZIO LOAS0870                  *
	 * ----------------------------------------------------------------*</pre>*/
    private void s10310PrepCallLoas0870() {
        // COB_CODE: MOVE 'S10310-PREP-CALL-LOAS0870'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S10310-PREP-CALL-LOAS0870");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX
        displayLabel();
        //
        // COB_CODE: SET W870-GGCONTESTO-CONT-NO
        //             TO TRUE
        ws.getW870AreaLoas0870().getW870DatiGgincassoContesto().setW870GgcontestoContNo();
        // COB_CODE: MOVE ZEROES
        //             TO W870-GGINCASSO
        //                W870-TGA-NUM-MAX-ELE.
        ws.getW870AreaLoas0870().setW870Ggincasso(0);
        ws.getW870AreaLoas0870().setW870TgaNumMaxEle(((short)0));
        //
        //   INIZIALIZZAZIONE AREA DATI PER CALL SERVIZIO LOAS0870
        //
        // COB_CODE: INITIALIZE  W870-ID-TRCH-DI-GAR  (1)
        //                       W870-TIT-NUM-MAX-ELE (1).
        ws.getW870AreaLoas0870().getW870TabTit().setIdTrchDiGar(1, 0);
        ws.getW870AreaLoas0870().getW870TabTit().setTitNumMaxEle(1, ((short)0));
        // COB_CODE: PERFORM VARYING IX-TIT-W870 FROM 1 BY 1
        //             UNTIL IX-TIT-W870 > WK-MAX-TIT-W870
        //             INITIALIZE   W870-DATI-TIT(1,IX-TIT-W870)
        //           END-PERFORM.
        ws.getIxIndici().setTitW870(((short)1));
        while (!(ws.getIxIndici().getTitW870() > ws.getWkCostantiTabelle().getMaxTitW870())) {
            // COB_CODE: INITIALIZE   W870-DATI-TIT(1,IX-TIT-W870)
            initW870DatiTit();
            ws.getIxIndici().setTitW870(Trunc.toShort(ws.getIxIndici().getTitW870() + 1, 4));
        }
        // COB_CODE: MOVE  W870-TAB-TIT     TO W870-RESTO-TAB-TIT.
        ws.getW870AreaLoas0870().getW870TabTit().setW870RestoTabTit(ws.getW870AreaLoas0870().getW870TabTit().getW870TabTitFormatted());
    }

    /**Original name: S10320-CALL-LOAS0870<br>
	 * <pre>----------------------------------------------------------------*
	 *             CALL SERVIZIO LOAS0870                              *
	 * ----------------------------------------------------------------*</pre>*/
    private void s10320CallLoas0870() {
        Loas0870 loas0870 = null;
        // COB_CODE: MOVE 'S10320-CALL-LOAS0870'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S10320-CALL-LOAS0870");
        // COB_CODE: SET  IDSV8888-BUSINESS-DBG          TO TRUE
        ws.getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
        // COB_CODE: MOVE 'LOAS0870'                 TO IDSV8888-NOME-PGM.
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("LOAS0870");
        // COB_CODE: SET  IDSV8888-INIZIO            TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setInizio();
        // COB_CODE: MOVE SPACES                     TO IDSV8888-DESC-PGM
        //skipped translation for moving SPACES to IDSV8888-DESC-PGM; considered in STRING statement translation below
        // COB_CODE: STRING 'Rivalut x incasso ind.'
        //                   DELIMITED BY SIZE
        //                   INTO IDSV8888-DESC-PGM
        //           END-STRING
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("Rivalut x incasso ind.");
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        //
        // COB_CODE:       CALL LOAS0870        USING AREA-IDSV0001
        //                                            WCOM-IO-STATI
        //                                            WPMO-AREA-PARAM-MOVI
        //                                            WPOL-AREA-POLIZZA
        //                                            WADE-AREA-ADESIONE
        //                                            WGRZ-AREA-GARANZIA
        //                                            WTGA-AREA-TRANCHE
        //                                            W870-AREA-LOAS0870
        //           *
        //                 ON EXCEPTION
        //           *
        //                         THRU GESTIONE-ERR-SIST-EX
        //           *
        //                END-CALL.
        try {
            loas0870 = Loas0870.getInstance();
            loas0870.run(new Object[] {areaIdsv0001, wcomIoStati, areaMain, areaMain.getWpolAreaPolizza(), areaMain.getWadeAreaAdesione(), ws.getWgrzAreaGaranzia(), ws.getWtgaAreaTranche(), ws.getW870AreaLoas0870()});
        }
        catch (ProgramExecutionException __ex) {
            //
            // COB_CODE: MOVE 'LOAS0870'
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe("LOAS0870");
            // COB_CODE: MOVE 'ERRORE CALL MODULO LOAS0870'
            //             TO CALL-DESC
            ws.getIdsv0002().setCallDesc("ERRORE CALL MODULO LOAS0870");
            // COB_CODE: MOVE 'S10220-CALL-LOAS0870'
            //             TO WK-LABEL-ERR
            ws.getWkGestioneMsgErr().setLabelErr("S10220-CALL-LOAS0870");
            //
            // COB_CODE: PERFORM GESTIONE-ERR-SIST
            //              THRU GESTIONE-ERR-SIST-EX
            gestioneErrSist();
            //
        }
        //
        // COB_CODE: SET  IDSV8888-BUSINESS-DBG      TO TRUE
        ws.getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
        // COB_CODE: MOVE 'LOAS0870'                 TO IDSV8888-NOME-PGM.
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("LOAS0870");
        // COB_CODE: SET  IDSV8888-FINE              TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setFine();
        // COB_CODE: MOVE SPACES                     TO IDSV8888-DESC-PGM
        //skipped translation for moving SPACES to IDSV8888-DESC-PGM; considered in STRING statement translation below
        // COB_CODE: STRING 'Rivalut x incasso ind.'
        //                   DELIMITED BY SIZE
        //                   INTO IDSV8888-DESC-PGM
        //           END-STRING
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("Rivalut x incasso ind.");
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
    }

    /**Original name: S10500-GEST-VALORIZZATORE<br>
	 * <pre>----------------------------------------------------------------*
	 *             GESTIONE CHIAMATA AL VALORIZZATORE VARIABILI        *
	 * ----------------------------------------------------------------*</pre>*/
    private void s10500GestValorizzatore() {
        // COB_CODE: MOVE 'S10500-GEST-VALORIZZATORE'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S10500-GEST-VALORIZZATORE");
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        //     INITIALIZE WSKD-AREA-SCHEDA.
        //
        //--  INIZIALIZZAZIONE AREA SCHEDA VARIABILI
        // COB_CODE: INITIALIZE             WSKD-ELE-LIVELLO-MAX-P
        //                                  WSKD-DEE
        //                                  WSKD-ID-POL-P            (1)
        //                                  WSKD-COD-TIPO-OPZIONE-P  (1)
        //                                  WSKD-TP-LIVELLO-P        (1)
        //                                  WSKD-COD-LIVELLO-P       (1)
        //                                  WSKD-ID-LIVELLO-P        (1)
        //                                  WSKD-DT-INIZ-PROD-P      (1)
        //                                  WSKD-COD-RGM-FISC-P      (1)
        //                                  WSKD-NOME-SERVIZIO-P     (1)
        //                                  WSKD-ELE-VARIABILI-MAX-P (1)
        //                                  WSKD-AREA-VARIABILI-P    (1)
        //                                  WSKD-VAR-AUT-OPER.
        ws.getWskdAreaScheda().setWskdEleLivelloMaxP(((short)0));
        ws.getWskdAreaScheda().setWskdDee("");
        ws.getWskdAreaScheda().getWskdTabValP().setWskdIdPolPFormatted(1, "000000000");
        ws.getWskdAreaScheda().getWskdTabValP().setWskdCodTipoOpzioneP(1, "");
        ws.getWskdAreaScheda().getWskdTabValP().setWskdTpLivelloP(1, Types.SPACE_CHAR);
        ws.getWskdAreaScheda().getWskdTabValP().setWskdCodLivelloP(1, "");
        ws.getWskdAreaScheda().getWskdTabValP().setWskdIdLivelloPFormatted(1, "000000000");
        ws.getWskdAreaScheda().getWskdTabValP().setWskdDtInizProdP(1, "");
        ws.getWskdAreaScheda().getWskdTabValP().setWskdCodRgmFiscP(1, "");
        ws.getWskdAreaScheda().getWskdTabValP().setWskdNomeServizioP(1, "");
        ws.getWskdAreaScheda().getWskdTabValP().setWskdEleVariabiliMaxP(1, ((short)0));
        initWskdAreaVariabiliP();
        initWskdVarAutOper();
        // COB_CODE: MOVE WSKD-TAB-VAL-P TO WSKD-RESTO-TAB-VAL-P.
        ws.getWskdAreaScheda().getWskdTabValP().setWskdRestoTabValP(ws.getWskdAreaScheda().getWskdTabValP().getWskdTabValPFormatted());
        // COB_CODE: INITIALIZE             WSKD-ELE-LIVELLO-MAX-T
        //                                  WSKD-ID-GAR-T            (1)
        //                                  WSKD-COD-TIPO-OPZIONE-T  (1)
        //                                  WSKD-TP-LIVELLO-T        (1)
        //                                  WSKD-COD-LIVELLO-T       (1)
        //                                  WSKD-ID-LIVELLO-T        (1)
        //                                  WSKD-DT-DECOR-TRCH-T     (1)
        //                                  WSKD-DT-INIZ-TARI-T      (1)
        //                                  WSKD-COD-RGM-FISC-T      (1)
        //                                  WSKD-NOME-SERVIZIO-T     (1)
        //                                  WSKD-ELE-VARIABILI-MAX-T (1)
        //                                  WSKD-AREA-VARIABILI-T    (1)
        ws.getWskdAreaScheda().setWskdEleLivelloMaxT(((short)0));
        ws.getWskdAreaScheda().getWskdTabValT().setWskdIdGarTFormatted(1, "000000000");
        ws.getWskdAreaScheda().getWskdTabValT().setWskdCodTipoOpzioneT(1, "");
        ws.getWskdAreaScheda().getWskdTabValT().setWskdTpLivelloT(1, Types.SPACE_CHAR);
        ws.getWskdAreaScheda().getWskdTabValT().setWskdCodLivelloT(1, "");
        ws.getWskdAreaScheda().getWskdTabValT().setWskdIdLivelloTFormatted(1, "000000000");
        ws.getWskdAreaScheda().getWskdTabValT().setWskdDtDecorTrchT(1, "");
        ws.getWskdAreaScheda().getWskdTabValT().setWskdDtInizTariT(1, "");
        ws.getWskdAreaScheda().getWskdTabValT().setWskdCodRgmFiscT(1, "");
        ws.getWskdAreaScheda().getWskdTabValT().setWskdNomeServizioT(1, "");
        ws.getWskdAreaScheda().getWskdTabValT().setWskdEleVariabiliMaxT(1, ((short)0));
        initWskdAreaVariabiliT();
        // COB_CODE: MOVE WSKD-TAB-VAL-T TO WSKD-RESTO-TAB-VAL-T.
        ws.getWskdAreaScheda().getWskdTabValT().setWskdRestoTabValT(ws.getWskdAreaScheda().getWskdTabValT().getWskdTabValTFormatted());
        // COB_CODE: PERFORM S10510-PREPARA-AREA-VAL
        //              THRU S10510-PREPARA-AREA-VAL-EX.
        s10510PreparaAreaVal();
        //
        // COB_CODE: SET  IDSV8888-BUSINESS-DBG          TO TRUE
        ws.getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
        // COB_CODE: MOVE 'IVVS0211'                     TO IDSV8888-NOME-PGM
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("IVVS0211");
        // COB_CODE: MOVE 'Valoriz. variab. step A'      TO IDSV8888-DESC-PGM
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("Valoriz. variab. step A");
        // COB_CODE: SET  IDSV8888-INIZIO                TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setInizio();
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: PERFORM CALL-VALORIZZATORE
        //              THRU CALL-VALORIZZATORE-EX.
        callValorizzatore();
        // COB_CODE: SET  IDSV8888-BUSINESS-DBG          TO TRUE
        ws.getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
        // COB_CODE: MOVE 'IVVS0211'                     TO IDSV8888-NOME-PGM
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("IVVS0211");
        // COB_CODE: MOVE 'Valoriz. variab. step A'      TO IDSV8888-DESC-PGM
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("Valoriz. variab. step A");
        // COB_CODE: SET  IDSV8888-FINE                  TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setFine();
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                     TO WSKD-AREA-SCHEDA
        //           *
        //                ELSE
        //           *
        //                      THRU GESTIONE-ERR-STD-EX
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE: SET  S211-FL-AREA-VAR-EXTRA-NO TO TRUE
            ws.getAreaIoIvvs0211().getIvvc0211RestoDati().getFlAreaVarExtra().setS211FlAreaVarExtraNo();
            // COB_CODE: MOVE S211-BUFFER-DATI
            //             TO WSKD-AREA-SCHEDA
            ws.getWskdAreaScheda().setWskdAreaSchedaFormatted(ws.getAreaIoIvvs0211().getIvvc0211RestoDati().getBufferDatiFormatted());
            //
        }
        else {
            //
            // COB_CODE: MOVE 'S10500-GEST-VALORIZZATORE'
            //             TO WK-LABEL-ERR
            ws.getWkGestioneMsgErr().setLabelErr("S10500-GEST-VALORIZZATORE");
            // COB_CODE: MOVE 'ERRORE CHIAMATA VALORIZZATORE'
            //             TO WK-STRING
            ws.getWkGestioneMsgErr().setStringFld("ERRORE CHIAMATA VALORIZZATORE");
            // COB_CODE: MOVE '001114'
            //             TO WK-COD-ERR
            ws.getWkGestioneMsgErr().setCodErr("001114");
            // COB_CODE: PERFORM GESTIONE-ERR-STD
            //              THRU GESTIONE-ERR-STD-EX
            gestioneErrStd();
            //
        }
    }

    /**Original name: S10510-PREPARA-AREA-VAL<br>
	 * <pre>----------------------------------------------------------------*
	 *              VALORIZZAZIONE DELL'AREA DI INPUT DEL              *
	 *                SERVIZIO VALORIZZATORE VARIABILI                 *
	 * ----------------------------------------------------------------*</pre>*/
    private void s10510PreparaAreaVal() {
        // COB_CODE: MOVE 'S10510-PREPARA-AREA-VAL'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S10510-PREPARA-AREA-VAL");
        //
        // --> Valorizzazione fittizia della dclgen della tabella
        // --> Movimento per consentire l'eventuale calcolo delle
        // --> variabili  DEE e DEEL
        //
        //    Inizializzazione area io valorizzatore variabile
        //
        //     INITIALIZE AREA-IO-IVVS0211.
        // COB_CODE: INITIALIZE S211-DATI-INPUT
        //                      S211-RESTO-DATI
        //                      S211-TAB-INFO(1).
        initS211DatiInput();
        initS211RestoDati();
        initS211TabInfo();
        // COB_CODE: MOVE  S211-TAB-INFO1     TO  S211-RESTO-TAB-INFO1.
        ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211RestoTabInfo1(ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getS211TabInfo1Formatted());
        // COB_CODE: IF WK-ORIG-RIV-IN AND
        //              WPOL-TP-FRM-ASSVA = 'CO'
        //                 THRU S10520-EX
        //           ELSE
        //              END-IF
        //           END-IF
        if (ws.getWkOrigRiv().isInFld() && Conditions.eq(areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolTpFrmAssva(), "CO")) {
            // COB_CODE: PERFORM S10520-CARICA-GRZ-E-TRCH
            //              THRU S10520-EX
            s10520CaricaGrzETrch();
        }
        else if (ws.getWkOrigRiv().isInFld() && Conditions.eq(areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolTpFrmAssva(), "IN")) {
            // COB_CODE: IF  WK-ORIG-RIV-IN AND
            //               WPOL-TP-FRM-ASSVA = 'IN'
            //                  THRU S10520-EX
            //           ELSE
            //                 TO VTGA-AREA-TRANCHE
            //            END-IF
            // COB_CODE: PERFORM S10520-CARICA-GRZ-E-TRCH
            //              THRU S10520-EX
            s10520CaricaGrzETrch();
        }
        else {
            // COB_CODE: MOVE WGRZ-AREA-GARANZIA
            //             TO VGRZ-AREA-GARANZIA
            ws.setVgrzAreaGaranziaBytes(ws.getWgrzAreaGaranzia().getWgrzAreaGaranziaBytes());
            // COB_CODE: MOVE WTGA-AREA-TRANCHE
            //             TO VTGA-AREA-TRANCHE
            ws.getVtgaAreaTranche().setVtgaAreaTrancheBytes(ws.getWtgaAreaTranche().getWtgaAreaTrancheBytes());
        }
        //
        // COB_CODE: MOVE IDSI0011-TRATTAMENTO-STORICITA
        //             TO S211-TRATTAMENTO-STORICITA.
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setTrattamentoStoricita(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().getTrattamentoStoricita());
        //
        // COB_CODE: SET S211-BATCH                      TO TRUE.
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().getModalitaEsecutiva().setS211Batch();
        // COB_CODE: SET S211-AREA-PV                    TO TRUE.
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().getFlgArea().setS211AreaPv();
        //
        // COB_CODE: SET S211-IN-CONV
        //             TO TRUE.
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().getStepElab().setS211InConv();
        //
        // COB_CODE: MOVE WCOM-DT-ULT-VERS-PROD
        //             TO S211-DATA-ULT-VERS-PROD.
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setDataUltVersProd(wcomIoStati.getLccc0001().getWcomDtUltVersProd());
        //
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO
        //             TO S211-TIPO-MOVIMENTO.
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setS211TipoMovimentoFormatted(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimentoFormatted());
        //
        // COB_CODE: MOVE SPACES
        //             TO S211-COD-MAIN-BATCH.
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setCodMainBatch("");
        // COB_CODE: MOVE SPACES
        //             TO S211-COD-SERVIZIO-BE.
        ws.getAreaIoIvvs0211().getIvvc0211RestoDati().getCampiEsito().setCodServizioBe("");
        //
        // COB_CODE: MOVE ZEROES
        //             TO S211-DATA-COMPETENZA.
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setDataCompetenza(0);
        //
        // COB_CODE: MOVE 1
        //             TO WK-APPO-LUNGHEZZA.
        ws.setWkAppoLunghezza(1);
        //
        //  -->  Valorizzazione della struttura di mapping
        //
        // COB_CODE: MOVE 6
        //             TO S211-ELE-INFO-MAX.
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setEleInfoMax(((short)6));
        //
        //  --> Occorrenza 1
        //
        // COB_CODE: MOVE S211-ALIAS-POLI
        //             TO S211-TAB-ALIAS(1).
        ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211TabAlias(1, ws.getIvvc0218().getAliasPoli());
        // COB_CODE: MOVE WK-APPO-LUNGHEZZA
        //             TO S211-POSIZ-INI(1).
        ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211PosizIni(1, ws.getWkAppoLunghezza());
        // COB_CODE: MOVE LENGTH OF WPOL-AREA-POLIZZA
        //             TO S211-LUNGHEZZA(1).
        ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211Lunghezza(1, WpolAreaPolizzaLccs0005.Len.WPOL_AREA_POLIZZA);
        //
        // COB_CODE: COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
        //                                       S211-LUNGHEZZA(1) + 1.
        ws.setWkAppoLunghezza(Trunc.toInt(ws.getWkAppoLunghezza() + ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getLunghezza(1) + 1, 8));
        //
        //  --> Occorrenza 2
        //
        // COB_CODE: MOVE S211-ALIAS-ADES
        //             TO S211-TAB-ALIAS(2).
        ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211TabAlias(2, ws.getIvvc0218().getAliasAdes());
        // COB_CODE: MOVE WK-APPO-LUNGHEZZA
        //             TO S211-POSIZ-INI(2).
        ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211PosizIni(2, ws.getWkAppoLunghezza());
        // COB_CODE: MOVE LENGTH OF WADE-AREA-ADESIONE
        //             TO S211-LUNGHEZZA(2).
        ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211Lunghezza(2, WadeAreaAdesioneLoas0800.Len.WADE_AREA_ADESIONE);
        //
        // COB_CODE: COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
        //                                       S211-LUNGHEZZA(2) + 1
        ws.setWkAppoLunghezza(Trunc.toInt(ws.getWkAppoLunghezza() + ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getLunghezza(2) + 1, 8));
        //
        //  --> Occorrenza 3
        //
        // COB_CODE: MOVE S211-ALIAS-GARANZIA
        //             TO S211-TAB-ALIAS(3)
        ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211TabAlias(3, ws.getIvvc0218().getAliasGaranzia());
        // COB_CODE: MOVE WK-APPO-LUNGHEZZA
        //             TO S211-POSIZ-INI(3)
        ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211PosizIni(3, ws.getWkAppoLunghezza());
        // COB_CODE: MOVE LENGTH OF VGRZ-AREA-GARANZIA
        //             TO S211-LUNGHEZZA(3)
        ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211Lunghezza(3, Loas0310Data.Len.VGRZ_AREA_GARANZIA);
        //
        // COB_CODE: COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
        //                                       S211-LUNGHEZZA(3) + 1
        ws.setWkAppoLunghezza(Trunc.toInt(ws.getWkAppoLunghezza() + ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getLunghezza(3) + 1, 8));
        //
        //  --> Occorrenza 4
        //
        // COB_CODE: MOVE S211-ALIAS-TRCH-GAR
        //             TO S211-TAB-ALIAS(4)
        ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211TabAlias(4, ws.getIvvc0218().getAliasTrchGar());
        // COB_CODE: MOVE WK-APPO-LUNGHEZZA
        //             TO S211-POSIZ-INI(4)
        ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211PosizIni(4, ws.getWkAppoLunghezza());
        // COB_CODE: MOVE LENGTH OF VTGA-AREA-TRANCHE
        //             TO S211-LUNGHEZZA(4)
        ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211Lunghezza(4, VtgaAreaTranche.Len.VTGA_AREA_TRANCHE);
        //
        // COB_CODE: COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
        //                                       S211-LUNGHEZZA(4) + 1
        ws.setWkAppoLunghezza(Trunc.toInt(ws.getWkAppoLunghezza() + ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getLunghezza(4) + 1, 8));
        //
        //  --> Occorrenza 5
        //
        // COB_CODE: MOVE S211-ALIAS-MOVIMENTO
        //             TO S211-TAB-ALIAS(5)
        ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211TabAlias(5, ws.getIvvc0218().getAliasMovimento());
        // COB_CODE: MOVE WK-APPO-LUNGHEZZA
        //             TO S211-POSIZ-INI(5)
        ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211PosizIni(5, ws.getWkAppoLunghezza());
        // COB_CODE: MOVE LENGTH OF WMOV-AREA-MOVIMENTO
        //             TO S211-LUNGHEZZA(5)
        ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211Lunghezza(5, WmovAreaMovimento.Len.WMOV_AREA_MOVIMENTO);
        //
        // COB_CODE: COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
        //                                       S211-LUNGHEZZA(5) + 1
        ws.setWkAppoLunghezza(Trunc.toInt(ws.getWkAppoLunghezza() + ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getLunghezza(5) + 1, 8));
        //
        //  --> Occorrenza 6
        //
        // COB_CODE: MOVE S211-ALIAS-PARAM-MOV
        //             TO S211-TAB-ALIAS(6)
        ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211TabAlias(6, ws.getIvvc0218().getAliasParamMov());
        // COB_CODE: MOVE WK-APPO-LUNGHEZZA
        //             TO S211-POSIZ-INI(6)
        ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211PosizIni(6, ws.getWkAppoLunghezza());
        // COB_CODE: MOVE LENGTH OF WPMO-AREA-PARAM-MOVI
        //             TO S211-LUNGHEZZA(6)
        ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211Lunghezza(6, AreaMainLoas0310.Len.WPMO_AREA_PARAM_MOVI);
        //
        // COB_CODE: COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
        //                                       S211-LUNGHEZZA(6) + 1
        ws.setWkAppoLunghezza(Trunc.toInt(ws.getWkAppoLunghezza() + ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getLunghezza(6) + 1, 8));
        //
        //  -->  Valorizzazione del buffer dati
        //
        // COB_CODE: MOVE WPOL-AREA-POLIZZA
        //             TO S211-BUFFER-DATI(S211-POSIZ-INI(1): S211-LUNGHEZZA(1)).
        ws.getAreaIoIvvs0211().getIvvc0211RestoDati().setS211BufferDatiSubstring(areaMain.getWpolAreaPolizza().getWpolAreaPolizzaFormatted(), ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getPosizIni(1), ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getLunghezza(1));
        //
        // COB_CODE: MOVE WADE-AREA-ADESIONE
        //             TO S211-BUFFER-DATI(S211-POSIZ-INI(2): S211-LUNGHEZZA(2)).
        ws.getAreaIoIvvs0211().getIvvc0211RestoDati().setS211BufferDatiSubstring(areaMain.getWadeAreaAdesione().getWadeAreaAdesioneFormatted(), ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getPosizIni(2), ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getLunghezza(2));
        //
        // COB_CODE: MOVE VGRZ-AREA-GARANZIA
        //             TO S211-BUFFER-DATI(S211-POSIZ-INI(3): S211-LUNGHEZZA(3)).
        ws.getAreaIoIvvs0211().getIvvc0211RestoDati().setS211BufferDatiSubstring(ws.getVgrzAreaGaranziaFormatted(), ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getPosizIni(3), ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getLunghezza(3));
        //
        // COB_CODE: MOVE VTGA-AREA-TRANCHE
        //             TO S211-BUFFER-DATI(S211-POSIZ-INI(4): S211-LUNGHEZZA(4)).
        ws.getAreaIoIvvs0211().getIvvc0211RestoDati().setS211BufferDatiSubstring(ws.getVtgaAreaTranche().getVtgaAreaTrancheFormatted(), ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getPosizIni(4), ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getLunghezza(4));
        //
        // COB_CODE: MOVE WMOV-AREA-MOVIMENTO
        //             TO S211-BUFFER-DATI(S211-POSIZ-INI(5): S211-LUNGHEZZA(5)).
        ws.getAreaIoIvvs0211().getIvvc0211RestoDati().setS211BufferDatiSubstring(areaMain.getWmovAreaMovimento().getWmovAreaMovimentoFormatted(), ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getPosizIni(5), ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getLunghezza(5));
        //
        // COB_CODE: MOVE WPMO-AREA-PARAM-MOVI
        //             TO S211-BUFFER-DATI(S211-POSIZ-INI(6): S211-LUNGHEZZA(6)).
        ws.getAreaIoIvvs0211().getIvvc0211RestoDati().setS211BufferDatiSubstring(areaMain.getWpmoAreaParamMoviFormatted(), ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getPosizIni(6), ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().getLunghezza(6));
        //
        // COB_CODE: SET S211-FL-AREA-VAR-EXTRA-SI   TO TRUE.
        ws.getAreaIoIvvs0211().getIvvc0211RestoDati().getFlAreaVarExtra().setS211FlAreaVarExtraSi();
    }

    /**Original name: S10520-CARICA-GRZ-E-TRCH<br>
	 * <pre>---------------------------------------------------------------
	 *  CARICAMENTO AREA  TRANCHE DI GARANZIA E GARANZIA
	 *  PER VALORIZZATORE
	 * ---------------------------------------------------------------</pre>*/
    private void s10520CaricaGrzETrch() {
        // COB_CODE: MOVE 'S10520-CARICA-GRZ-E-TRCH'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S10520-CARICA-GRZ-E-TRCH");
        // COB_CODE: PERFORM VARYING IX-TAB-GRZ FROM 1 BY 1
        //            UNTIL IX-TAB-GRZ > WGRZ-ELE-GAR-MAX
        //              END-IF
        //           END-PERFORM.
        ws.getIxIndici().setTabGrz(((short)1));
        while (!(ws.getIxIndici().getTabGrz() > ws.getWgrzAreaGaranzia().getEleGarMax())) {
            // COB_CODE: SET WK-TRANCHE-NON-CARICATA TO TRUE
            ws.getWkTrancheDaCaricare().setNonCaricata();
            // COB_CODE: PERFORM VARYING IX-TAB-TGA FROM 1 BY 1
            //              UNTIL IX-TAB-TGA > WTGA-ELE-TRAN-MAX
            //                 END-IF
            //           END-PERFORM
            ws.getIxIndici().setTabTga(((short)1));
            while (!(ws.getIxIndici().getTabTga() > ws.getWtgaAreaTranche().getWtgaEleTranMax())) {
                // COB_CODE: IF WTGA-TP-RIVAL(IX-TAB-TGA) NOT = 'NU'
                //                 AND
                //              WTGA-ID-GAR(IX-TAB-TGA) =
                //              WGRZ-ID-GAR(IX-TAB-GRZ)
                //              SET WK-TRANCHE-CARICATA TO TRUE
                //           END-IF
                if (!Conditions.eq(ws.getWtgaAreaTranche().getWtgaTab().getTpRival(ws.getIxIndici().getTabTga()), "NU") && ws.getWtgaAreaTranche().getWtgaTab().getIdGar(ws.getIxIndici().getTabTga()) == ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzIdGar()) {
                    // COB_CODE: ADD 1 TO VTGA-ELE-TRAN-MAX
                    ws.getVtgaAreaTranche().setVtgaEleTranMax(Trunc.toShort(1 + ws.getVtgaAreaTranche().getVtgaEleTranMax(), 4));
                    // COB_CODE: MOVE WTGA-TAB-TRAN(IX-TAB-TGA)
                    //             TO VTGA-TAB-TRAN(VTGA-ELE-TRAN-MAX)
                    ws.getVtgaAreaTranche().getVtgaTab().setTabTranBytes(ws.getVtgaAreaTranche().getVtgaEleTranMax(), ws.getWtgaAreaTranche().getWtgaTab().getTabTranBytes(ws.getIxIndici().getTabTga()));
                    // COB_CODE: SET WK-TRANCHE-CARICATA TO TRUE
                    ws.getWkTrancheDaCaricare().setCaricata();
                }
                ws.getIxIndici().setTabTga(Trunc.toShort(ws.getIxIndici().getTabTga() + 1, 4));
            }
            // COB_CODE: IF WK-TRANCHE-CARICATA
            //                TO VGRZ-TAB-GAR(VGRZ-ELE-GAR-MAX)
            //           END-IF
            if (ws.getWkTrancheDaCaricare().isCaricata()) {
                // COB_CODE: ADD 1 TO VGRZ-ELE-GAR-MAX
                ws.setVgrzEleGarMax(Trunc.toShort(1 + ws.getVgrzEleGarMax(), 4));
                // COB_CODE: MOVE WGRZ-TAB-GAR(IX-TAB-GRZ)
                //             TO VGRZ-TAB-GAR(VGRZ-ELE-GAR-MAX)
                ws.getVgrzTabGar(ws.getVgrzEleGarMax()).setWgarTabGarBytes(ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getTabGarBytes());
            }
            ws.getIxIndici().setTabGrz(Trunc.toShort(ws.getIxIndici().getTabGrz() + 1, 4));
        }
    }

    /**Original name: S10599-SALVA-PRSTZ-PREC<br>*/
    private void s10599SalvaPrstzPrec() {
        // COB_CODE: MOVE 'S10599-SALVA-PRSTZ-PREC'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S10599-SALVA-PRSTZ-PREC");
        // COB_CODE: PERFORM VARYING IX-TAB-TGA FROM 1 BY 1
        //             UNTIL IX-TAB-TGA > WTGA-ELE-TRAN-MAX
        //                END-IF
        //           END-PERFORM.
        ws.getIxIndici().setTabTga(((short)1));
        while (!(ws.getIxIndici().getTabTga() > ws.getWtgaAreaTranche().getWtgaEleTranMax())) {
            // COB_CODE: MOVE WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA)
            //                 TO WPREC-ID-TRCH-DI-GAR(IX-TAB-TGA)
            ws.getWprecTabTran(ws.getIxIndici().getTabTga()).setIdTrchDiGar(ws.getWtgaAreaTranche().getWtgaTab().getIdTrchDiGar(ws.getIxIndici().getTabTga()));
            // COB_CODE: IF WTGA-PRSTZ-ULT-NULL(IX-TAB-TGA) NOT = HIGH-VALUE
            //                 TO WPREC-PRSTZ-ULT(IX-TAB-TGA)
            //           ELSE
            //                 TO  WPREC-PRSTZ-ULT(IX-TAB-TGA)
            //           END-IF
            if (!Characters.EQ_HIGH.test(ws.getWtgaAreaTranche().getWtgaTab().getPrstzUltNullFormatted(ws.getIxIndici().getTabTga()))) {
                // COB_CODE: MOVE WTGA-PRSTZ-ULT(IX-TAB-TGA)
                //              TO WPREC-PRSTZ-ULT(IX-TAB-TGA)
                ws.getWprecTabTran(ws.getIxIndici().getTabTga()).setPrstzUlt(Trunc.toDecimal(ws.getWtgaAreaTranche().getWtgaTab().getPrstzUlt(ws.getIxIndici().getTabTga()), 15, 3));
            }
            else {
                // COB_CODE: MOVE ZEROES
                //              TO  WPREC-PRSTZ-ULT(IX-TAB-TGA)
                ws.getWprecTabTran(ws.getIxIndici().getTabTga()).setPrstzUlt(new AfDecimal(0, 15, 3));
            }
            ws.getIxIndici().setTabTga(Trunc.toShort(ws.getIxIndici().getTabTga() + 1, 4));
        }
    }

    /**Original name: S10600-PREP-CALL-LOAS0660<br>
	 * <pre> -------------------------------------------------------------- *
	 *  SECTION DI PREPARAZIONE ALLA CALL DEL SERVIZIO DI RIVALUTAZIONE
	 *  -------------------------------------------------------------- *</pre>*/
    private void s10600PrepCallLoas0660() {
        // COB_CODE: MOVE 'S10600-PREP-CALL-LOAS0660'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S10600-PREP-CALL-LOAS0660");
        //
        // COB_CODE: SET W660-MANFEE-CONT-NO
        //             TO TRUE.
        ws.getW660AreaLoas0660().getDatiManfeeContesto().setW660ManfeeContNo();
        //
        // COB_CODE: MOVE ZEROES
        //             TO W660-PERMANFEE
        //                W660-MESIDIFFMFEE
        //                W660-DECADELMFEE.
        ws.getW660AreaLoas0660().setPermanfee(0);
        ws.getW660AreaLoas0660().setMesidiffmfee(0);
        ws.getW660AreaLoas0660().setDecadelmfee(0);
        //
        // COB_CODE: SET W660-MF-CALCOLATO-NO
        //             TO TRUE.
        ws.getW660AreaLoas0660().getMfCalcolato().setW660MfCalcolatoNo();
    }

    /**Original name: S10700-CALL-LOAS0660<br>
	 * <pre> -------------------------------------------------------------- *
	 *  CALL DEL SERVIZIO DI RIVALUTAZIONE
	 *  -------------------------------------------------------------- *</pre>*/
    private void s10700CallLoas0660() {
        Loas0660 loas0660 = null;
        // COB_CODE: MOVE 'S10700-CALL-LOAS0660'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S10700-CALL-LOAS0660");
        // COB_CODE: SET  IDSV8888-BUSINESS-DBG      TO TRUE
        ws.getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
        // COB_CODE: MOVE 'LOAS0660'                 TO IDSV8888-NOME-PGM.
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("LOAS0660");
        // COB_CODE: SET  IDSV8888-INIZIO            TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setInizio();
        // COB_CODE: MOVE SPACES                     TO IDSV8888-DESC-PGM
        //skipped translation for moving SPACES to IDSV8888-DESC-PGM; considered in STRING statement translation below
        // COB_CODE: STRING 'Serv. di rivalutazione'
        //                   DELIMITED BY SIZE
        //                   INTO IDSV8888-DESC-PGM
        //           END-STRING
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("Serv. di rivalutazione");
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        //
        // COB_CODE:      CALL LOAS0660         USING AREA-IDSV0001
        //                                            WCOM-IO-STATI
        //                                            WPMO-AREA-PARAM-MOVI
        //                                            WMOV-AREA-MOVIMENTO
        //                                            WPOL-AREA-POLIZZA
        //                                            WADE-AREA-ADESIONE
        //                                            WGRZ-AREA-GARANZIA
        //                                            WTGA-AREA-TRANCHE
        //                                            WSKD-AREA-SCHEDA
        //                                            W660-AREA-LOAS0660
        //           *
        //                 ON EXCEPTION
        //           *
        //                         THRU GESTIONE-ERR-SIST-EX
        //           *
        //                 END-CALL.
        try {
            loas0660 = Loas0660.getInstance();
            loas0660.run(new Object[] {areaIdsv0001, wcomIoStati, areaMain, areaMain.getWmovAreaMovimento(), areaMain.getWpolAreaPolizza(), areaMain.getWadeAreaAdesione(), ws.getWgrzAreaGaranzia(), ws.getWtgaAreaTranche(), ws.getWskdAreaScheda(), ws.getW660AreaLoas0660()});
        }
        catch (ProgramExecutionException __ex) {
            //
            // COB_CODE: MOVE 'LOAS0660'
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe("LOAS0660");
            // COB_CODE: MOVE 'ERRORE CALL MODULO LOAS0660'
            //             TO CALL-DESC
            ws.getIdsv0002().setCallDesc("ERRORE CALL MODULO LOAS0660");
            // COB_CODE: MOVE 'S10700-CALL-LOAS0660'
            //             TO WK-LABEL-ERR
            ws.getWkGestioneMsgErr().setLabelErr("S10700-CALL-LOAS0660");
            //
            // COB_CODE: PERFORM GESTIONE-ERR-SIST
            //              THRU GESTIONE-ERR-SIST-EX
            gestioneErrSist();
            //
        }
        // COB_CODE: SET  IDSV8888-BUSINESS-DBG      TO TRUE
        ws.getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
        // COB_CODE: MOVE 'LOAS0660'                 TO IDSV8888-NOME-PGM.
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("LOAS0660");
        // COB_CODE: SET  IDSV8888-FINE              TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setFine();
        // COB_CODE: MOVE SPACES                     TO IDSV8888-DESC-PGM
        //skipped translation for moving SPACES to IDSV8888-DESC-PGM; considered in STRING statement translation below
        // COB_CODE: STRING 'Serv. di rivalutazione'
        //                   DELIMITED BY SIZE
        //                   INTO IDSV8888-DESC-PGM
        //           END-STRING
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("Serv. di rivalutazione");
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
    }

    /**Original name: S10900-GESTIONE-EOC<br>
	 * <pre>----------------------------------------------------------------*
	 *            GESTIONE DELLA CHIAMATA AL MODULO DI EOC             *
	 * ----------------------------------------------------------------*
	 *  --> Merge tra l'area di PMO con le date calcolate e
	 *  --> quella con le nuove occorrenze
	 *     CONTROLLO SE E' STATO CREATO UN MOVIMENTO DI MANFEE
	 *     SE E' STATO CREATO ACCODO IL MOVIMENTO 6006 ALL'ARRAY
	 *     ALTRIMENTI VADO A RICOPRIRE L'INTERA AREA
	 *        MOVE VPMO-AREA-PARAM-MOVI
	 *          TO WPMO-AREA-PARAM-MOVI
	 *        MOVE VPMO-TAB-PARAM-MOV(1)
	 *          TO WPMO-TAB-PARAM-MOV(1)</pre>*/
    private void s10900GestioneEoc() {
        Loas0320 loas0320 = null;
        // COB_CODE: PERFORM
        //              VARYING IX-VPMO FROM 1 BY 1
        //                UNTIL IX-VPMO GREATER VPMO-ELE-PARAM-MOV-MAX
        //                  END-IF
        //           END-PERFORM
        ws.getIxIndici().setVpmo(((short)1));
        while (!(ws.getIxIndici().getVpmo() > ws.getWorkCommarea().getEleParamMovMax())) {
            // COB_CODE: IF NOT VPMO-ST-INV(IX-VPMO)
            //                TO WPMO-TAB-PARAM-MOV(IX-VPMO)
            //           END-IF
            if (!ws.getWorkCommarea().getTabParamMov(ws.getIxIndici().getVpmo()).getLccvpmo1().getStatus().isInv()) {
                // COB_CODE: MOVE VPMO-TAB-PARAM-MOV(IX-VPMO)
                //             TO WPMO-TAB-PARAM-MOV(IX-VPMO)
                areaMain.getWpmoTabParamMov(ws.getIxIndici().getVpmo()).setTabParamMovBytes(ws.getWorkCommarea().getTabParamMov(ws.getIxIndici().getVpmo()).getWpmoTabParamMovBytes());
            }
            ws.getIxIndici().setVpmo(Trunc.toShort(ws.getIxIndici().getVpmo() + 1, 4));
        }
        // COB_CODE: SET  IDSV8888-BUSINESS-DBG      TO TRUE
        ws.getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
        // COB_CODE: MOVE 'LOAS0320'                 TO IDSV8888-NOME-PGM.
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("LOAS0320");
        // COB_CODE: SET  IDSV8888-INIZIO            TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setInizio();
        // COB_CODE: MOVE SPACES                     TO IDSV8888-DESC-PGM
        //skipped translation for moving SPACES to IDSV8888-DESC-PGM; considered in STRING statement translation below
        // COB_CODE: STRING 'Servizio di EOC'
        //                   DELIMITED BY SIZE
        //                   INTO IDSV8888-DESC-PGM
        //           END-STRING
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("Servizio di EOC");
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        //
        // COB_CODE:      CALL LOAS0320         USING AREA-IDSV0001
        //                                            WGRZ-AREA-GARANZIA
        //                                            WTGA-AREA-TRANCHE
        //                                            AREA-MAIN
        //                                            WCOM-IO-STATI
        //                                            IABV0006
        //           *
        //                ON EXCEPTION
        //           *
        //                         THRU GESTIONE-ERR-SIST-EX
        //           *
        //                END-CALL.
        try {
            loas0320 = Loas0320.getInstance();
            loas0320.run(new Object[] {areaIdsv0001, ws.getWgrzAreaGaranzia(), ws.getWtgaAreaTranche(), areaMain, wcomIoStati, iabv0006});
        }
        catch (ProgramExecutionException __ex) {
            //
            // COB_CODE: MOVE 'LOAS0320'
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe("LOAS0320");
            // COB_CODE: MOVE 'SERVIZIO LOAS0320'
            //             TO CALL-DESC
            ws.getIdsv0002().setCallDesc("SERVIZIO LOAS0320");
            // COB_CODE: MOVE 'S10940-CALL-LOAS0320'
            //             TO WK-LABEL-ERR
            ws.getWkGestioneMsgErr().setLabelErr("S10940-CALL-LOAS0320");
            //
            // COB_CODE: PERFORM GESTIONE-ERR-SIST
            //              THRU GESTIONE-ERR-SIST-EX
            gestioneErrSist();
            //
        }
        // COB_CODE: SET  IDSV8888-BUSINESS-DBG      TO TRUE
        ws.getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
        // COB_CODE: MOVE 'LOAS0320'                 TO IDSV8888-NOME-PGM.
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("LOAS0320");
        // COB_CODE: SET  IDSV8888-FINE              TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setFine();
        // COB_CODE: MOVE SPACES                     TO IDSV8888-DESC-PGM
        //skipped translation for moving SPACES to IDSV8888-DESC-PGM; considered in STRING statement translation below
        // COB_CODE: STRING 'Servizio di EOC'
        //                   DELIMITED BY SIZE
        //                   INTO IDSV8888-DESC-PGM
        //           END-STRING
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("Servizio di EOC");
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
    }

    /**Original name: S10950-GESTIONE-FILEOUT<br>
	 * <pre>----------------------------------------------------------------*
	 *                  GESTIONE DEL FILE DI OUTPUT                    *
	 * ----------------------------------------------------------------*</pre>*/
    private void s10950GestioneFileout() {
        // COB_CODE: INITIALIZE W-REC-OUTRIVA
        initWRecOutriva();
        //
        // COB_CODE: PERFORM S10960-VALORIZZA-REC-GEN
        //              THRU S10960-VALORIZZA-REC-GEN-EX.
        s10960ValorizzaRecGen();
        //
        // COB_CODE:      PERFORM VARYING IX-TAB-TGA FROM 1 BY 1
        //                  UNTIL IX-TAB-TGA > WTGA-ELE-TRAN-MAX
        //                     OR IDSV0001-ESITO-KO
        //           *
        //                         THRU S10970-VALORIZZA-REC-DET-EX
        //           *
        //                END-PERFORM.
        ws.getIxIndici().setTabTga(((short)1));
        while (!(ws.getIxIndici().getTabTga() > ws.getWtgaAreaTranche().getWtgaEleTranMax() || areaIdsv0001.getEsito().isIdsv0001EsitoKo())) {
            //
            // COB_CODE: INITIALIZE W-REC-OUTRIVA
            initWRecOutriva();
            //
            // COB_CODE: PERFORM S10970-VALORIZZA-REC-DET
            //              THRU S10970-VALORIZZA-REC-DET-EX
            s10970ValorizzaRecDet();
            //
            ws.getIxIndici().setTabTga(Trunc.toShort(ws.getIxIndici().getTabTga() + 1, 4));
        }
    }

    /**Original name: S10960-VALORIZZA-REC-GEN<br>
	 * <pre>----------------------------------------------------------------*
	 *                  GESTIONE DEL FILE DI OUTPUT   - REC GEN -      *
	 * ----------------------------------------------------------------*</pre>*/
    private void s10960ValorizzaRecGen() {
        // COB_CODE: MOVE 'GEN'
        //             TO WREC-TIPO-REC.
        ws.getLoar0171().setWrecTipoRec("GEN");
        // COB_CODE: MOVE 'OA'
        //             TO WREC-MACROFUNZ.
        ws.getLoar0171().getWrecOut().setMacrofunz("OA");
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO
        //             TO WREC-FUNZ.
        ws.getLoar0171().getWrecOut().setFunzFormatted(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimentoFormatted());
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //             TO WREC-COD-COMP.
        ws.getLoar0171().getWrecOut().setCodCompFormatted(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAniaFormatted());
        // COB_CODE: MOVE WCOM-PERIODO-ELAB-DA
        //             TO WREC-DT-RICH-DA.
        ws.getLoar0171().getWrecOut().setDtRichDaFormatted(wcomIntervalloElab.getDaFormatted());
        // COB_CODE: MOVE WCOM-PERIODO-ELAB-A
        //             TO WREC-DT-RICH-A.
        ws.getLoar0171().getWrecOut().setDtRichAFormatted(wcomIntervalloElab.getAFormatted());
        // COB_CODE: IF WPOL-COD-RAMO-NULL = HIGH-VALUES
        //                TO WREC-RAMO
        //           ELSE
        //                TO WREC-RAMO
        //           END-IF
        if (Characters.EQ_HIGH.test(areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolCodRamoFormatted())) {
            // COB_CODE: MOVE SPACES
            //             TO WREC-RAMO
            ws.getLoar0171().getWrecOut().setRamo("");
        }
        else {
            // COB_CODE: MOVE WPOL-COD-RAMO
            //             TO WREC-RAMO
            ws.getLoar0171().getWrecOut().setRamo(areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolCodRamo());
        }
        //
        // COB_CODE:      IF WPOL-TP-FRM-ASSVA = 'IN'
        //           *
        //                   END-IF
        //           *
        //                ELSE
        //           *
        //                   END-IF
        //           *
        //                END-IF.
        if (Conditions.eq(areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolTpFrmAssva(), "IN")) {
            //
            // COB_CODE: IF WPOL-IB-OGG-NULL = HIGH-VALUES
            //                TO WREC-NUM-POL-IND OF WREC-GEN
            //           ELSE
            //                TO WREC-NUM-POL-IND OF WREC-GEN
            //           END-IF
            if (Characters.EQ_HIGH.test(areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolIbOgg(), WpolDati.Len.WPOL_IB_OGG)) {
                // COB_CODE: MOVE SPACES
                //             TO WREC-NUM-POL-IND OF WREC-GEN
                ws.getLoar0171().getWrecOut().setNumPolIndofWrecGen("");
            }
            else {
                // COB_CODE: MOVE WPOL-IB-OGG
                //             TO WREC-NUM-POL-IND OF WREC-GEN
                ws.getLoar0171().getWrecOut().setNumPolIndofWrecGen(areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolIbOgg());
            }
            //
        }
        else {
            //
            // COB_CODE: IF WPOL-IB-OGG-NULL = HIGH-VALUES
            //                TO WREC-NUM-POL-COLL
            //           ELSE
            //                TO WREC-NUM-POL-COLL
            //           END-IF
            if (Characters.EQ_HIGH.test(areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolIbOgg(), WpolDati.Len.WPOL_IB_OGG)) {
                // COB_CODE: MOVE SPACES
                //             TO WREC-NUM-POL-COLL
                ws.getLoar0171().getWrecOut().setNumPolColl("");
            }
            else {
                // COB_CODE: MOVE WPOL-IB-OGG
                //             TO WREC-NUM-POL-COLL
                ws.getLoar0171().getWrecOut().setNumPolColl(areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolIbOgg());
            }
            // COB_CODE: IF WADE-IB-OGG-NULL(1) = HIGH-VALUES
            //                TO WREC-NUM-ADESIONE
            //           ELSE
            //                TO WREC-NUM-ADESIONE
            //           END-IF
            if (Characters.EQ_HIGH.test(areaMain.getWadeAreaAdesione().getTabAde(1).getLccvade1().getDati().getWadeIbOgg(), WadeDati.Len.WADE_IB_OGG)) {
                // COB_CODE: MOVE SPACES
                //             TO WREC-NUM-ADESIONE
                ws.getLoar0171().getWrecOut().setNumAdesione("");
            }
            else {
                // COB_CODE: MOVE WADE-IB-OGG(1)
                //             TO WREC-NUM-ADESIONE
                ws.getLoar0171().getWrecOut().setNumAdesione(areaMain.getWadeAreaAdesione().getTabAde(1).getLccvade1().getDati().getWadeIbOgg());
            }
            //
        }
        //
        // COB_CODE: MOVE IDSV0001-DATA-COMPETENZA(1:8)
        //             TO WREC-DT-COMP-RIVA.
        ws.getLoar0171().getWrecOut().setDtCompRivaFormatted(areaIdsv0001.getAreaComune().getIdsv0001DataCompetenzaFormatted().substring((1) - 1, 8));
        // COB_CODE: INSPECT W-REC-OUTRIVA REPLACING ALL LOW-VALUE BY SPACES
        ws.setwRecOutrivaFormatted(ws.getwRecOutrivaFormatted().replace(String.valueOf(Types.LOW_CHAR_VAL), Types.SPACE_STRING));
        // COB_CODE: INSPECT W-REC-OUTRIVA REPLACING ALL HIGH-VALUE BY SPACES
        ws.setwRecOutrivaFormatted(ws.getwRecOutrivaFormatted().replace(String.valueOf(Types.HIGH_CHAR_VAL), Types.SPACE_STRING));
        //
        // COB_CODE: code not available
        outrivaTo.setVariable(ws.getwRecOutrivaFormatted());
        outrivaDAO.write(outrivaTo);
        ws.setFsOut(outrivaDAO.getFileStatus().getStatusCodeFormatted());
        //
        // COB_CODE:      IF FS-OUT NOT = '00'
        //           *
        //                      THRU GESTIONE-ERR-STD-EX
        //           *
        //                END-IF.
        if (!Conditions.eq(ws.getFsOut(), "00")) {
            //
            // COB_CODE: MOVE 'S10960-VALORIZZA-REC-GEN'
            //             TO WK-LABEL-ERR
            ws.getWkGestioneMsgErr().setLabelErr("S10960-VALORIZZA-REC-GEN");
            // COB_CODE: MOVE 'ERRORE WRITE FILE DI OUTPUT'
            //             TO WK-STRING
            ws.getWkGestioneMsgErr().setStringFld("ERRORE WRITE FILE DI OUTPUT");
            // COB_CODE: MOVE '001114'
            //             TO WK-COD-ERR
            ws.getWkGestioneMsgErr().setCodErr("001114");
            // COB_CODE: PERFORM GESTIONE-ERR-STD
            //              THRU GESTIONE-ERR-STD-EX
            gestioneErrStd();
            //
        }
    }

    /**Original name: S10970-VALORIZZA-REC-DET<br>
	 * <pre>----------------------------------------------------------------*
	 *                  GESTIONE DEL FILE DI OUTPUT   - REC DET -      *
	 * ----------------------------------------------------------------*</pre>*/
    private void s10970ValorizzaRecDet() {
        // COB_CODE: MOVE 'DET'
        //             TO WREC-TIPO-REC.
        ws.getLoar0171().setWrecTipoRec("DET");
        // COB_CODE: IF WPOL-TP-FRM-ASSVA = 'IN' AND
        //              WPOL-IB-OGG-NULL NOT = HIGH-VALUES
        //                   TO WREC-NUM-POL-IND OF WREC-DET
        //           ELSE
        //                   TO WREC-NUM-POL-IND OF WREC-DET
        //           END-IF.
        if (Conditions.eq(areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolTpFrmAssva(), "IN") && !Characters.EQ_HIGH.test(areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolIbOgg(), WpolDati.Len.WPOL_IB_OGG)) {
            // COB_CODE: MOVE WPOL-IB-OGG
            //                TO WREC-NUM-POL-IND OF WREC-DET
            ws.getLoar0171().getWrecOut().setNumPolIndofWrecDet(areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolIbOgg());
        }
        else {
            // COB_CODE: MOVE SPACES
            //                TO WREC-NUM-POL-IND OF WREC-DET
            ws.getLoar0171().getWrecOut().setNumPolIndofWrecDet("");
        }
        // COB_CODE:      PERFORM VARYING IX-TAB-GRZ FROM 1 BY 1
        //                  UNTIL IX-TAB-GRZ > WGRZ-ELE-GAR-MAX
        //           *
        //                     END-IF
        //           *
        //                END-PERFORM.
        ws.getIxIndici().setTabGrz(((short)1));
        while (!(ws.getIxIndici().getTabGrz() > ws.getWgrzAreaGaranzia().getEleGarMax())) {
            //
            // COB_CODE:           IF WGRZ-ID-GAR(IX-TAB-GRZ) =
            //                        WTGA-ID-GAR(IX-TAB-TGA)
            //           *
            //                          TO WREC-COD-GAR-TRCH
            //           *
            //                     END-IF
            if (ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzIdGar() == ws.getWtgaAreaTranche().getWtgaTab().getIdGar(ws.getIxIndici().getTabTga())) {
                //
                // COB_CODE: MOVE WGRZ-COD-TARI(IX-TAB-GRZ)
                //             TO WREC-COD-GAR-TRCH
                ws.getLoar0171().getWrecOut().setCodGarTrch(ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzCodTari());
                //
            }
            //
            ws.getIxIndici().setTabGrz(Trunc.toShort(ws.getIxIndici().getTabGrz() + 1, 4));
        }
        //
        // COB_CODE: IF WTGA-IB-OGG-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              MOVE ALL SPACES              TO WREC-NUM-TRCH
        //           ELSE
        //                TO WREC-NUM-TRCH
        //           END-IF.
        if (Characters.EQ_HIGH.test(ws.getWtgaAreaTranche().getWtgaTab().getIbOggNull(ws.getIxIndici().getTabTga()), WtgaTabLoas0310.Len.IB_OGG_NULL)) {
            // COB_CODE: MOVE ALL SPACES              TO WREC-NUM-TRCH
            ws.getLoar0171().getWrecOut().setNumTrch("");
        }
        else {
            // COB_CODE: MOVE WTGA-IB-OGG(IX-TAB-TGA)
            //             TO WREC-NUM-TRCH
            ws.getLoar0171().getWrecOut().setNumTrch(ws.getWtgaAreaTranche().getWtgaTab().getIbOgg(ws.getIxIndici().getTabTga()));
        }
        // COB_CODE: MOVE WTGA-DT-DECOR(IX-TAB-TGA)
        //             TO WREC-DT-DECOR-TRCH
        ws.getLoar0171().getWrecOut().setDtDecorTrch(TruncAbs.toInt(ws.getWtgaAreaTranche().getWtgaTab().getDtDecor(ws.getIxIndici().getTabTga()), 8));
        //
        // COB_CODE: IF WTGA-DT-ULT-ADEG-PRE-PR-NULL(IX-TAB-TGA) = HIGH-VALUES
        //                TO WREC-DT-ULT-RIVA-TRCH
        //           ELSE
        //                TO WREC-DT-ULT-RIVA-TRCH
        //           END-IF.
        if (Characters.EQ_HIGH.test(ws.getWtgaAreaTranche().getWtgaTab().getDtUltAdegPrePrNullFormatted(ws.getIxIndici().getTabTga()))) {
            // COB_CODE: MOVE 0
            //             TO WREC-DT-ULT-RIVA-TRCH
            ws.getLoar0171().getWrecOut().setDtUltRivaTrch(0);
        }
        else {
            // COB_CODE: MOVE WTGA-DT-ULT-ADEG-PRE-PR(IX-TAB-TGA)
            //             TO WREC-DT-ULT-RIVA-TRCH
            ws.getLoar0171().getWrecOut().setDtUltRivaTrch(TruncAbs.toInt(ws.getWtgaAreaTranche().getWtgaTab().getDtUltAdegPrePr(ws.getIxIndici().getTabTga()), 8));
        }
        //
        // COB_CODE: IF WTGA-PRE-RIVTO-NULL(IX-TAB-TGA) = HIGH-VALUES
        //                TO WREC-PRE-RIVTO
        //           ELSE
        //                TO WREC-PRE-RIVTO
        //           END-IF.
        if (Characters.EQ_HIGH.test(ws.getWtgaAreaTranche().getWtgaTab().getPreRivtoNullFormatted(ws.getIxIndici().getTabTga()))) {
            // COB_CODE: MOVE 0
            //             TO WREC-PRE-RIVTO
            ws.getLoar0171().getWrecOut().setPreRivto(Trunc.toDecimal(0, 15, 3));
        }
        else {
            // COB_CODE: MOVE WTGA-PRE-RIVTO(IX-TAB-TGA)
            //             TO WREC-PRE-RIVTO
            ws.getLoar0171().getWrecOut().setPreRivto(TruncAbs.toDecimal(ws.getWtgaAreaTranche().getWtgaTab().getPreRivto(ws.getIxIndici().getTabTga()), 15, 3));
        }
        // COB_CODE: SET PREC-TROVATO-NO TO TRUE
        ws.getWkPrecTrovato().setNo();
        // COB_CODE: PERFORM VARYING IX-TAB-PREC FROM 1 BY 1 UNTIL
        //                           IX-TAB-PREC > WTGA-ELE-TRAN-MAX OR
        //                           PREC-TROVATO-SI
        //              END-IF
        //           END-PERFORM
        ws.getIxIndici().setTabPrec(((short)1));
        while (!(ws.getIxIndici().getTabPrec() > ws.getWtgaAreaTranche().getWtgaEleTranMax() || ws.getWkPrecTrovato().isSi())) {
            // COB_CODE: IF WPREC-ID-TRCH-DI-GAR(IX-TAB-PREC) =
            //              WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA)
            //                TO TRUE
            //           END-IF
            if (ws.getWprecTabTran(ws.getIxIndici().getTabPrec()).getIdTrchDiGar() == ws.getWtgaAreaTranche().getWtgaTab().getIdTrchDiGar(ws.getIxIndici().getTabTga())) {
                // COB_CODE: MOVE WPREC-PRSTZ-ULT(IX-TAB-PREC)
                //             TO WREC-PRSTZ-PREC
                ws.getLoar0171().getWrecOut().setPrstzPrec(TruncAbs.toDecimal(ws.getWprecTabTran(ws.getIxIndici().getTabPrec()).getPrstzUlt(), 15, 3));
                // COB_CODE: SET PREC-TROVATO-SI
                //             TO TRUE
                ws.getWkPrecTrovato().setSi();
            }
            ws.getIxIndici().setTabPrec(Trunc.toShort(ws.getIxIndici().getTabPrec() + 1, 4));
        }
        //
        // COB_CODE:      IF WTGA-PRSTZ-ULT-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
        //           *
        //                     TO WREC-PRSTZ-ULT
        //           *
        //                ELSE
        //           *
        //                     TO WREC-PRSTZ-ULT
        //           *
        //                END-IF.
        if (!Characters.EQ_HIGH.test(ws.getWtgaAreaTranche().getWtgaTab().getPrstzUltNullFormatted(ws.getIxIndici().getTabTga()))) {
            //
            // COB_CODE: MOVE WTGA-PRSTZ-ULT(IX-TAB-TGA)
            //             TO WREC-PRSTZ-ULT
            ws.getLoar0171().getWrecOut().setPrstzUlt(TruncAbs.toDecimal(ws.getWtgaAreaTranche().getWtgaTab().getPrstzUlt(ws.getIxIndici().getTabTga()), 15, 3));
            //
        }
        else {
            //
            // COB_CODE: MOVE 0
            //             TO WREC-PRSTZ-ULT
            ws.getLoar0171().getWrecOut().setPrstzUlt(Trunc.toDecimal(0, 15, 3));
            //
        }
        //
        // COB_CODE: IF WTGA-PRE-PP-ULT-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
        //                TO WREC-PRE-PP-ULT
        //           ELSE
        //                TO WREC-PRE-PP-ULT
        //           END-IF.
        if (!Characters.EQ_HIGH.test(ws.getWtgaAreaTranche().getWtgaTab().getPrePpUltNullFormatted(ws.getIxIndici().getTabTga()))) {
            // COB_CODE: MOVE WTGA-PRE-PP-ULT(IX-TAB-TGA)
            //             TO WREC-PRE-PP-ULT
            ws.getLoar0171().getWrecOut().setPrePpUlt(TruncAbs.toDecimal(ws.getWtgaAreaTranche().getWtgaTab().getPrePpUlt(ws.getIxIndici().getTabTga()), 15, 3));
        }
        else {
            // COB_CODE: MOVE ZEROES
            //             TO WREC-PRE-PP-ULT
            ws.getLoar0171().getWrecOut().setPrePpUlt(new AfDecimal(0, 15, 3));
        }
        //
        // COB_CODE:      IF WTGA-IMP-SOPR-SAN-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
        //           *
        //                     TO WREC-IMP-SOPR-SAN
        //           *
        //                ELSE
        //           *
        //                     TO WREC-IMP-SOPR-SAN
        //           *
        //                END-IF.
        if (!Characters.EQ_HIGH.test(ws.getWtgaAreaTranche().getWtgaTab().getImpSoprSanNullFormatted(ws.getIxIndici().getTabTga()))) {
            //
            // COB_CODE: MOVE WTGA-IMP-SOPR-SAN(IX-TAB-TGA)
            //             TO WREC-IMP-SOPR-SAN
            ws.getLoar0171().getWrecOut().setImpSoprSan(TruncAbs.toDecimal(ws.getWtgaAreaTranche().getWtgaTab().getImpSoprSan(ws.getIxIndici().getTabTga()), 15, 3));
            //
        }
        else {
            //
            // COB_CODE: MOVE 0
            //             TO WREC-IMP-SOPR-SAN
            ws.getLoar0171().getWrecOut().setImpSoprSan(Trunc.toDecimal(0, 15, 3));
            //
        }
        //
        // COB_CODE:      IF WTGA-IMP-SOPR-PROF-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
        //           *
        //                     TO WREC-IMP-SOPR-PROF
        //           *
        //                ELSE
        //           *
        //                     TO WREC-IMP-SOPR-PROF
        //           *
        //                END-IF.
        if (!Characters.EQ_HIGH.test(ws.getWtgaAreaTranche().getWtgaTab().getImpSoprProfNullFormatted(ws.getIxIndici().getTabTga()))) {
            //
            // COB_CODE: MOVE WTGA-IMP-SOPR-PROF(IX-TAB-TGA)
            //             TO WREC-IMP-SOPR-PROF
            ws.getLoar0171().getWrecOut().setImpSoprProf(TruncAbs.toDecimal(ws.getWtgaAreaTranche().getWtgaTab().getImpSoprProf(ws.getIxIndici().getTabTga()), 15, 3));
            //
        }
        else {
            //
            // COB_CODE: MOVE 0
            //             TO WREC-IMP-SOPR-PROF
            ws.getLoar0171().getWrecOut().setImpSoprProf(Trunc.toDecimal(0, 15, 3));
            //
        }
        //
        // COB_CODE:      IF WTGA-IMP-SOPR-SPO-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
        //           *
        //                     TO WREC-IMP-SOPR-SPO
        //           *
        //                ELSE
        //           *
        //                     TO WREC-IMP-SOPR-SPO
        //           *
        //                END-IF.
        if (!Characters.EQ_HIGH.test(ws.getWtgaAreaTranche().getWtgaTab().getImpSoprSpoNullFormatted(ws.getIxIndici().getTabTga()))) {
            //
            // COB_CODE: MOVE WTGA-IMP-SOPR-SPO(IX-TAB-TGA)
            //             TO WREC-IMP-SOPR-SPO
            ws.getLoar0171().getWrecOut().setImpSoprSpo(TruncAbs.toDecimal(ws.getWtgaAreaTranche().getWtgaTab().getImpSoprSpo(ws.getIxIndici().getTabTga()), 15, 3));
            //
        }
        else {
            //
            // COB_CODE: MOVE 0
            //             TO WREC-IMP-SOPR-SPO
            ws.getLoar0171().getWrecOut().setImpSoprSpo(Trunc.toDecimal(0, 15, 3));
            //
        }
        //
        // COB_CODE:      IF WTGA-IMP-SOPR-TEC-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
        //           *
        //                     TO WREC-IMP-SOPR-TEC
        //           *
        //                ELSE
        //           *
        //                     TO WREC-IMP-SOPR-TEC
        //           *
        //                END-IF.
        if (!Characters.EQ_HIGH.test(ws.getWtgaAreaTranche().getWtgaTab().getImpSoprTecNullFormatted(ws.getIxIndici().getTabTga()))) {
            //
            // COB_CODE: MOVE WTGA-IMP-SOPR-TEC(IX-TAB-TGA)
            //             TO WREC-IMP-SOPR-TEC
            ws.getLoar0171().getWrecOut().setImpSoprTec(TruncAbs.toDecimal(ws.getWtgaAreaTranche().getWtgaTab().getImpSoprTec(ws.getIxIndici().getTabTga()), 15, 3));
            //
        }
        else {
            //
            // COB_CODE: MOVE 0
            //             TO WREC-IMP-SOPR-TEC
            ws.getLoar0171().getWrecOut().setImpSoprTec(Trunc.toDecimal(0, 15, 3));
            //
        }
        //
        // COB_CODE:      IF WTGA-IMP-ALT-SOPR-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
        //           *
        //                     TO WREC-IMP-ALT-SOPR
        //           *
        //                ELSE
        //           *
        //                     TO WREC-IMP-ALT-SOPR
        //           *
        //                END-IF.
        if (!Characters.EQ_HIGH.test(ws.getWtgaAreaTranche().getWtgaTab().getImpAltSoprNullFormatted(ws.getIxIndici().getTabTga()))) {
            //
            // COB_CODE: MOVE WTGA-IMP-ALT-SOPR(IX-TAB-TGA)
            //             TO WREC-IMP-ALT-SOPR
            ws.getLoar0171().getWrecOut().setImpAltSopr(TruncAbs.toDecimal(ws.getWtgaAreaTranche().getWtgaTab().getImpAltSopr(ws.getIxIndici().getTabTga()), 15, 3));
            //
        }
        else {
            //
            // COB_CODE: MOVE 0
            //             TO WREC-IMP-ALT-SOPR
            ws.getLoar0171().getWrecOut().setImpAltSopr(Trunc.toDecimal(0, 15, 3));
            //
        }
        //
        // COB_CODE:      IF WTGA-PRE-UNI-RIVTO-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
        //           *
        //                     TO WREC-PRE-UNI-RIVTO
        //           *
        //                ELSE
        //           *
        //                     TO WREC-PRE-UNI-RIVTO
        //           *
        //                END-IF.
        if (!Characters.EQ_HIGH.test(ws.getWtgaAreaTranche().getWtgaTab().getPreUniRivtoNullFormatted(ws.getIxIndici().getTabTga()))) {
            //
            // COB_CODE: MOVE WTGA-PRE-UNI-RIVTO(IX-TAB-TGA)
            //             TO WREC-PRE-UNI-RIVTO
            ws.getLoar0171().getWrecOut().setPreUniRivto(TruncAbs.toDecimal(ws.getWtgaAreaTranche().getWtgaTab().getPreUniRivto(ws.getIxIndici().getTabTga()), 15, 3));
            //
        }
        else {
            //
            // COB_CODE: MOVE 0
            //             TO WREC-PRE-UNI-RIVTO
            ws.getLoar0171().getWrecOut().setPreUniRivto(Trunc.toDecimal(0, 15, 3));
            //
        }
        //
        // COB_CODE:      IF WTGA-PRE-INVRIO-ULT-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
        //           *
        //                     TO WREC-PRE-INVRIO-ULT
        //           *
        //                ELSE
        //           *
        //                     TO WREC-PRE-INVRIO-ULT
        //           *
        //                END-IF.
        if (!Characters.EQ_HIGH.test(ws.getWtgaAreaTranche().getWtgaTab().getPreInvrioUltNullFormatted(ws.getIxIndici().getTabTga()))) {
            //
            // COB_CODE: MOVE WTGA-PRE-INVRIO-ULT(IX-TAB-TGA)
            //             TO WREC-PRE-INVRIO-ULT
            ws.getLoar0171().getWrecOut().setPreInvrioUlt(TruncAbs.toDecimal(ws.getWtgaAreaTranche().getWtgaTab().getPreInvrioUlt(ws.getIxIndici().getTabTga()), 15, 3));
            //
        }
        else {
            //
            // COB_CODE: MOVE 0
            //             TO WREC-PRE-INVRIO-ULT
            ws.getLoar0171().getWrecOut().setPreInvrioUlt(Trunc.toDecimal(0, 15, 3));
            //
        }
        //
        // COB_CODE:      IF WTGA-RIS-MAT-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
        //           *
        //                      TO WREC-RIS-MAT
        //           *
        //                 ELSE
        //           *
        //                      TO WREC-RIS-MAT
        //           *
        //                 END-IF.
        if (!Characters.EQ_HIGH.test(ws.getWtgaAreaTranche().getWtgaTab().getRisMatNullFormatted(ws.getIxIndici().getTabTga()))) {
            //
            // COB_CODE: MOVE WTGA-RIS-MAT(IX-TAB-TGA)
            //             TO WREC-RIS-MAT
            ws.getLoar0171().getWrecOut().setRisMat(TruncAbs.toDecimal(ws.getWtgaAreaTranche().getWtgaTab().getRisMat(ws.getIxIndici().getTabTga()), 15, 3));
            //
        }
        else {
            //
            // COB_CODE: MOVE 0
            //             TO WREC-RIS-MAT
            ws.getLoar0171().getWrecOut().setRisMat(Trunc.toDecimal(0, 15, 3));
            //
        }
        //
        // COB_CODE:      IF WTGA-CPT-IN-OPZ-RIVTO-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
        //           *
        //                     TO WREC-CPT-IN-OPZ-RIVTO
        //           *
        //                ELSE
        //           *
        //                     TO WREC-CPT-IN-OPZ-RIVTO
        //           *
        //                END-IF.
        if (!Characters.EQ_HIGH.test(ws.getWtgaAreaTranche().getWtgaTab().getCptInOpzRivtoNullFormatted(ws.getIxIndici().getTabTga()))) {
            //
            // COB_CODE: MOVE WTGA-CPT-IN-OPZ-RIVTO(IX-TAB-TGA)
            //             TO WREC-CPT-IN-OPZ-RIVTO
            ws.getLoar0171().getWrecOut().setCptInOpzRivto(TruncAbs.toDecimal(ws.getWtgaAreaTranche().getWtgaTab().getCptInOpzRivto(ws.getIxIndici().getTabTga()), 15, 3));
            //
        }
        else {
            //
            // COB_CODE: MOVE 0
            //             TO WREC-CPT-IN-OPZ-RIVTO
            ws.getLoar0171().getWrecOut().setCptInOpzRivto(Trunc.toDecimal(0, 15, 3));
            //
        }
        //
        // COB_CODE:      IF WTGA-RENDTO-LRD-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
        //           *
        //           *       MOVE WTGA-RENDTO-LRD(IX-TAB-TGA)
        //           *         TO WREC-RENDTO-LRD
        //                       WTGA-RENDTO-LRD(IX-TAB-TGA)
        //           *
        //                ELSE
        //           *
        //                     TO WREC-RENDTO-LRD
        //           *
        //                END-IF.
        if (!Characters.EQ_HIGH.test(ws.getWtgaAreaTranche().getWtgaTab().getRendtoLrdNullFormatted(ws.getIxIndici().getTabTga()))) {
            //
            //       MOVE WTGA-RENDTO-LRD(IX-TAB-TGA)
            //         TO WREC-RENDTO-LRD
            // COB_CODE: COMPUTE WREC-RENDTO-LRD ROUNDED =
            //               WTGA-RENDTO-LRD(IX-TAB-TGA)
            ws.getLoar0171().getWrecOut().setRendtoLrd(Trunc.toDecimal(MathUtil.convertRoundDecimal((ws.getWtgaAreaTranche().getWtgaTab().getRendtoLrd(ws.getIxIndici().getTabTga())), 2, RoundingMode.ROUND_UP, 31, 2), 5, 2));
            //
        }
        else {
            //
            // COB_CODE: MOVE 0
            //             TO WREC-RENDTO-LRD
            ws.getLoar0171().getWrecOut().setRendtoLrd(Trunc.toDecimal(0, 5, 2));
            //
        }
        //
        // COB_CODE:      IF WTGA-PC-RETR-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
        //           *
        //           *       MOVE WTGA-PC-RETR(IX-TAB-TGA)
        //           *         TO WREC-PC-RETR
        //                       WTGA-PC-RETR(IX-TAB-TGA)
        //           *
        //                ELSE
        //           *
        //                     TO WREC-PC-RETR
        //           *
        //                END-IF.
        if (!Characters.EQ_HIGH.test(ws.getWtgaAreaTranche().getWtgaTab().getPcRetrNullFormatted(ws.getIxIndici().getTabTga()))) {
            //
            //       MOVE WTGA-PC-RETR(IX-TAB-TGA)
            //         TO WREC-PC-RETR
            // COB_CODE: COMPUTE WREC-PC-RETR    ROUNDED =
            //               WTGA-PC-RETR(IX-TAB-TGA)
            ws.getLoar0171().getWrecOut().setPcRetr(Trunc.toDecimal(MathUtil.convertRoundDecimal((ws.getWtgaAreaTranche().getWtgaTab().getPcRetr(ws.getIxIndici().getTabTga())), 3, RoundingMode.ROUND_UP, 31, 3), 6, 3));
            //
        }
        else {
            //
            // COB_CODE: MOVE 0
            //             TO WREC-PC-RETR
            ws.getLoar0171().getWrecOut().setPcRetr(Trunc.toDecimal(0, 6, 3));
            //
        }
        //
        // COB_CODE:      IF WTGA-MIN-TRNUT-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
        //           *
        //           *       MOVE WTGA-MIN-TRNUT(IX-TAB-TGA)
        //           *         TO WREC-MIN-TRNUT
        //                       WTGA-MIN-TRNUT(IX-TAB-TGA)
        //           *
        //                ELSE
        //           *
        //                     TO WREC-MIN-TRNUT
        //           *
        //                END-IF.
        if (!Characters.EQ_HIGH.test(ws.getWtgaAreaTranche().getWtgaTab().getMinTrnutNullFormatted(ws.getIxIndici().getTabTga()))) {
            //
            //       MOVE WTGA-MIN-TRNUT(IX-TAB-TGA)
            //         TO WREC-MIN-TRNUT
            // COB_CODE: COMPUTE WREC-MIN-TRNUT  ROUNDED =
            //               WTGA-MIN-TRNUT(IX-TAB-TGA)
            ws.getLoar0171().getWrecOut().setMinTrnut(Trunc.toDecimal(MathUtil.convertRoundDecimal((ws.getWtgaAreaTranche().getWtgaTab().getMinTrnut(ws.getIxIndici().getTabTga())), 3, RoundingMode.ROUND_UP, 31, 3), 6, 3));
            //
        }
        else {
            //
            // COB_CODE: MOVE 0
            //             TO WREC-MIN-TRNUT
            ws.getLoar0171().getWrecOut().setMinTrnut(Trunc.toDecimal(0, 6, 3));
            //
        }
        //
        // COB_CODE:      IF WTGA-MIN-GARTO-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
        //           *
        //           *       MOVE WTGA-MIN-GARTO(IX-TAB-TGA)
        //           *         TO WREC-MIN-GARTO
        //                       WTGA-MIN-GARTO(IX-TAB-TGA)
        //           *
        //                ELSE
        //           *
        //                     TO WREC-MIN-GARTO
        //           *
        //                END-IF.
        if (!Characters.EQ_HIGH.test(ws.getWtgaAreaTranche().getWtgaTab().getMinGartoNullFormatted(ws.getIxIndici().getTabTga()))) {
            //
            //       MOVE WTGA-MIN-GARTO(IX-TAB-TGA)
            //         TO WREC-MIN-GARTO
            // COB_CODE: COMPUTE WREC-MIN-GARTO  ROUNDED =
            //               WTGA-MIN-GARTO(IX-TAB-TGA)
            ws.getLoar0171().getWrecOut().setMinGarto(Trunc.toDecimal(MathUtil.convertRoundDecimal((ws.getWtgaAreaTranche().getWtgaTab().getMinGarto(ws.getIxIndici().getTabTga())), 2, RoundingMode.ROUND_UP, 31, 2), 5, 2));
            //
        }
        else {
            //
            // COB_CODE: MOVE 0
            //             TO WREC-MIN-GARTO
            ws.getLoar0171().getWrecOut().setMinGarto(Trunc.toDecimal(0, 5, 2));
            //
        }
        //
        // COB_CODE:      IF WTGA-RENDTO-RETR-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
        //           *
        //           *       MOVE WTGA-RENDTO-RETR(IX-TAB-TGA)
        //           *         TO WREC-RENDTO-RETR
        //                       WTGA-RENDTO-RETR(IX-TAB-TGA)
        //           *
        //                ELSE
        //           *
        //                     TO WREC-RENDTO-RETR
        //           *
        //                END-IF.
        if (!Characters.EQ_HIGH.test(ws.getWtgaAreaTranche().getWtgaTab().getRendtoRetrNullFormatted(ws.getIxIndici().getTabTga()))) {
            //
            //       MOVE WTGA-RENDTO-RETR(IX-TAB-TGA)
            //         TO WREC-RENDTO-RETR
            // COB_CODE: COMPUTE WREC-RENDTO-RETR ROUNDED =
            //               WTGA-RENDTO-RETR(IX-TAB-TGA)
            ws.getLoar0171().getWrecOut().setRendtoRetr(Trunc.toDecimal(MathUtil.convertRoundDecimal((ws.getWtgaAreaTranche().getWtgaTab().getRendtoRetr(ws.getIxIndici().getTabTga())), 2, RoundingMode.ROUND_UP, 31, 2), 5, 2));
            //
        }
        else {
            //
            // COB_CODE: MOVE 0
            //             TO WREC-RENDTO-RETR
            ws.getLoar0171().getWrecOut().setRendtoRetr(Trunc.toDecimal(0, 5, 2));
            //
        }
        //
        // COB_CODE:      IF WTGA-TS-RIVAL-NET-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
        //           *
        //           *       MOVE WTGA-TS-RIVAL-NET(IX-TAB-TGA)
        //           *         TO WREC-RENDTO-NET
        //                       WTGA-TS-RIVAL-NET(IX-TAB-TGA)
        //           *
        //                ELSE
        //           *
        //                     TO WREC-RENDTO-NET
        //           *
        //                END-IF.
        if (!Characters.EQ_HIGH.test(ws.getWtgaAreaTranche().getWtgaTab().getTsRivalNetNullFormatted(ws.getIxIndici().getTabTga()))) {
            //
            //       MOVE WTGA-TS-RIVAL-NET(IX-TAB-TGA)
            //         TO WREC-RENDTO-NET
            // COB_CODE: COMPUTE WREC-RENDTO-NET ROUNDED =
            //               WTGA-TS-RIVAL-NET(IX-TAB-TGA)
            ws.getLoar0171().getWrecOut().setRendtoNet(Trunc.toDecimal(MathUtil.convertRoundDecimal((ws.getWtgaAreaTranche().getWtgaTab().getTsRivalNet(ws.getIxIndici().getTabTga())), 2, RoundingMode.ROUND_UP, 31, 2), 5, 2));
            //
        }
        else {
            //
            // COB_CODE: MOVE 0
            //             TO WREC-RENDTO-NET
            ws.getLoar0171().getWrecOut().setRendtoNet(Trunc.toDecimal(0, 5, 2));
            //
        }
        //
        // COB_CODE:      IF WTGA-CPT-MIN-SCAD-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
        //           *
        //                     TO WREC-CPT-GARTO-A-SCAD
        //           *
        //                ELSE
        //           *
        //                     TO WREC-CPT-GARTO-A-SCAD
        //           *
        //                END-IF.
        if (!Characters.EQ_HIGH.test(ws.getWtgaAreaTranche().getWtgaTab().getCptMinScadNullFormatted(ws.getIxIndici().getTabTga()))) {
            //
            // COB_CODE: MOVE WTGA-CPT-MIN-SCAD(IX-TAB-TGA)
            //             TO WREC-CPT-GARTO-A-SCAD
            ws.getLoar0171().getWrecOut().setCptGartoAScad(TruncAbs.toDecimal(ws.getWtgaAreaTranche().getWtgaTab().getCptMinScad(ws.getIxIndici().getTabTga()), 15, 3));
            //
        }
        else {
            //
            // COB_CODE: MOVE 0
            //             TO WREC-CPT-GARTO-A-SCAD
            ws.getLoar0171().getWrecOut().setCptGartoAScad(Trunc.toDecimal(0, 15, 3));
            //
        }
        //
        // COB_CODE:      IF WTGA-PRE-CASO-MOR-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
        //           *
        //                     TO WREC-PRE-CASO-MOR
        //           *
        //                ELSE
        //           *
        //                     TO WREC-PRE-CASO-MOR
        //           *
        //                END-IF.
        if (!Characters.EQ_HIGH.test(ws.getWtgaAreaTranche().getWtgaTab().getPreCasoMorNullFormatted(ws.getIxIndici().getTabTga()))) {
            //
            // COB_CODE: MOVE WTGA-PRE-CASO-MOR(IX-TAB-TGA)
            //             TO WREC-PRE-CASO-MOR
            ws.getLoar0171().getWrecOut().setPreCasoMor(TruncAbs.toDecimal(ws.getWtgaAreaTranche().getWtgaTab().getPreCasoMor(ws.getIxIndici().getTabTga()), 15, 3));
            //
        }
        else {
            //
            // COB_CODE: MOVE 0
            //             TO WREC-PRE-CASO-MOR
            ws.getLoar0171().getWrecOut().setPreCasoMor(Trunc.toDecimal(0, 15, 3));
            //
        }
        //
        // COB_CODE:      IF WTGA-ABB-ANNU-ULT-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
        //           *
        //                     TO WREC-ABB-ANNU-ULT
        //           *
        //                ELSE
        //           *
        //                     TO WREC-ABB-ANNU-ULT
        //           *
        //                END-IF.
        if (!Characters.EQ_HIGH.test(ws.getWtgaAreaTranche().getWtgaTab().getAbbAnnuUltNullFormatted(ws.getIxIndici().getTabTga()))) {
            //
            // COB_CODE: MOVE WTGA-ABB-ANNU-ULT(IX-TAB-TGA)
            //             TO WREC-ABB-ANNU-ULT
            ws.getLoar0171().getWrecOut().setAbbAnnuUlt(TruncAbs.toDecimal(ws.getWtgaAreaTranche().getWtgaTab().getAbbAnnuUlt(ws.getIxIndici().getTabTga()), 15, 3));
            //
        }
        else {
            //
            // COB_CODE: MOVE 0
            //             TO WREC-ABB-ANNU-ULT
            ws.getLoar0171().getWrecOut().setAbbAnnuUlt(Trunc.toDecimal(0, 15, 3));
            //
        }
        //
        //
        //    IF WTGA-COMMIS-GEST-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
        // COB_CODE:      IF WTGA-PC-COMMIS-GEST-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
        //           *
        //           *       MOVE WTGA-COMMIS-GEST(IX-TAB-TGA)
        //           *       MOVE WTGA-PC-COMMIS-GEST(IX-TAB-TGA)
        //           *         TO WREC-COMM-GEST
        //                       WTGA-PC-COMMIS-GEST(IX-TAB-TGA)
        //                ELSE
        //           *
        //                     TO WREC-COMM-GEST
        //           *
        //                END-IF.
        if (!Characters.EQ_HIGH.test(ws.getWtgaAreaTranche().getWtgaTab().getPcCommisGestNullFormatted(ws.getIxIndici().getTabTga()))) {
            //
            //       MOVE WTGA-COMMIS-GEST(IX-TAB-TGA)
            //       MOVE WTGA-PC-COMMIS-GEST(IX-TAB-TGA)
            //         TO WREC-COMM-GEST
            // COB_CODE: COMPUTE WREC-COMM-GEST  ROUNDED =
            //               WTGA-PC-COMMIS-GEST(IX-TAB-TGA)
            ws.getLoar0171().getWrecOut().setCommGest(Trunc.toDecimal(MathUtil.convertRoundDecimal((ws.getWtgaAreaTranche().getWtgaTab().getPcCommisGest(ws.getIxIndici().getTabTga())), 3, RoundingMode.ROUND_UP, 31, 3), 15, 3));
        }
        else {
            //
            // COB_CODE: MOVE 0
            //             TO WREC-COMM-GEST
            ws.getLoar0171().getWrecOut().setCommGest(Trunc.toDecimal(0, 15, 3));
            //
        }
        //
        // COB_CODE:      IF WTGA-INCR-PRSTZ-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
        //           *
        //                     TO WREC-INCR-PRSTZ
        //           *
        //                ELSE
        //           *
        //                     TO WREC-INCR-PRSTZ
        //           *
        //                END-IF.
        if (!Characters.EQ_HIGH.test(ws.getWtgaAreaTranche().getWtgaTab().getIncrPrstzNullFormatted(ws.getIxIndici().getTabTga()))) {
            //
            // COB_CODE: MOVE WTGA-INCR-PRSTZ(IX-TAB-TGA)
            //             TO WREC-INCR-PRSTZ
            ws.getLoar0171().getWrecOut().setIncrPrstz(TruncAbs.toDecimal(ws.getWtgaAreaTranche().getWtgaTab().getIncrPrstz(ws.getIxIndici().getTabTga()), 15, 3));
            //
        }
        else {
            //
            // COB_CODE: MOVE 0
            //             TO WREC-INCR-PRSTZ
            ws.getLoar0171().getWrecOut().setIncrPrstz(Trunc.toDecimal(0, 15, 3));
            //
        }
        //
        // COB_CODE:      IF WTGA-INCR-PRE-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
        //           *
        //                     TO WREC-INCR-PRE
        //           *
        //                ELSE
        //           *
        //                     TO WREC-INCR-PRE
        //           *
        //                END-IF.
        if (!Characters.EQ_HIGH.test(ws.getWtgaAreaTranche().getWtgaTab().getIncrPreNullFormatted(ws.getIxIndici().getTabTga()))) {
            //
            // COB_CODE: MOVE WTGA-INCR-PRE(IX-TAB-TGA)
            //             TO WREC-INCR-PRE
            ws.getLoar0171().getWrecOut().setIncrPre(TruncAbs.toDecimal(ws.getWtgaAreaTranche().getWtgaTab().getIncrPre(ws.getIxIndici().getTabTga()), 15, 3));
            //
        }
        else {
            //
            // COB_CODE: MOVE 0
            //             TO WREC-INCR-PRE
            ws.getLoar0171().getWrecOut().setIncrPre(Trunc.toDecimal(0, 15, 3));
            //
        }
        //
        // COB_CODE:      IF WTGA-PRSTZ-AGG-ULT-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
        //           *
        //                     TO WREC-PRSTZ-AGG-ULT
        //           *
        //                ELSE
        //           *
        //                     TO WREC-PRSTZ-AGG-ULT
        //           *
        //                END-IF.
        if (!Characters.EQ_HIGH.test(ws.getWtgaAreaTranche().getWtgaTab().getPrstzAggUltNullFormatted(ws.getIxIndici().getTabTga()))) {
            //
            // COB_CODE: MOVE WTGA-PRSTZ-AGG-ULT(IX-TAB-TGA)
            //             TO WREC-PRSTZ-AGG-ULT
            ws.getLoar0171().getWrecOut().setPrstzAggUlt(TruncAbs.toDecimal(ws.getWtgaAreaTranche().getWtgaTab().getPrstzAggUlt(ws.getIxIndici().getTabTga()), 15, 3));
            //
        }
        else {
            //
            // COB_CODE: MOVE 0
            //             TO WREC-PRSTZ-AGG-ULT
            ws.getLoar0171().getWrecOut().setPrstzAggUlt(Trunc.toDecimal(0, 15, 3));
            //
        }
        //
        // COB_CODE:      IF WTGA-MANFEE-RICOR-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
        //           *
        //                     TO WREC-MANFEE-RICOR
        //           *
        //                ELSE
        //           *
        //                     TO WREC-MANFEE-RICOR
        //           *
        //                END-IF.
        if (!Characters.EQ_HIGH.test(ws.getWtgaAreaTranche().getWtgaTab().getManfeeRicorNullFormatted(ws.getIxIndici().getTabTga()))) {
            //
            // COB_CODE: MOVE WTGA-MANFEE-RICOR(IX-TAB-TGA)
            //             TO WREC-MANFEE-RICOR
            ws.getLoar0171().getWrecOut().setManfeeRicor(TruncAbs.toDecimal(ws.getWtgaAreaTranche().getWtgaTab().getManfeeRicor(ws.getIxIndici().getTabTga()), 15, 3));
            //
        }
        else {
            //
            // COB_CODE: MOVE 0
            //             TO WREC-MANFEE-RICOR
            ws.getLoar0171().getWrecOut().setManfeeRicor(Trunc.toDecimal(0, 15, 3));
            //
        }
        // COB_CODE:      IF WTGA-NUM-GG-RIVAL-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
        //           *
        //                     TO WREC-NUM-GG-RIVAL
        //           *
        //                ELSE
        //           *
        //                     TO WREC-NUM-GG-RIVAL
        //           *
        //                END-IF.
        if (!Characters.EQ_HIGH.test(ws.getWtgaAreaTranche().getWtgaTab().getNumGgRivalNullFormatted(ws.getIxIndici().getTabTga()))) {
            //
            // COB_CODE: MOVE WTGA-NUM-GG-RIVAL(IX-TAB-TGA)
            //             TO WREC-NUM-GG-RIVAL
            ws.getLoar0171().getWrecOut().setNumGgRival(TruncAbs.toInt(ws.getWtgaAreaTranche().getWtgaTab().getNumGgRival(ws.getIxIndici().getTabTga()), 5));
            //
        }
        else {
            //
            // COB_CODE: MOVE 0
            //             TO WREC-NUM-GG-RIVAL
            ws.getLoar0171().getWrecOut().setNumGgRival(0);
            //
        }
        // COB_CODE: IF WTGA-INTR-MORA-NULL(IX-TAB-TGA) = HIGH-VALUES
        //                TO WREC-INT-MORA
        //           ELSE
        //                TO WREC-INT-MORA
        //           END-IF.
        if (Characters.EQ_HIGH.test(ws.getWtgaAreaTranche().getWtgaTab().getIntrMoraNullFormatted(ws.getIxIndici().getTabTga()))) {
            // COB_CODE: MOVE ZERO
            //             TO WREC-INT-MORA
            ws.getLoar0171().getWrecOut().setIntMora(new AfDecimal(0, 15, 3));
        }
        else {
            // COB_CODE: MOVE WTGA-INTR-MORA(IX-TAB-TGA)
            //             TO WREC-INT-MORA
            ws.getLoar0171().getWrecOut().setIntMora(TruncAbs.toDecimal(ws.getWtgaAreaTranche().getWtgaTab().getIntrMora(ws.getIxIndici().getTabTga()), 15, 3));
        }
        // COB_CODE: MOVE ZEROES              TO WREC-DT-VLT-TIT
        ws.getLoar0171().getWrecOut().setDtVltTit(0);
        //    SIR CQPrd00014405: LA DATA VALUTA DEVE ESSERE VALORIZZATA
        //    INDIPENDENTEMENTE DAL VALORE ASSUNTO DAL CAMPO TP_MOD_RIVAL
        //    AND WK-ORIG-RIV-IN
        // COB_CODE: IF COLLETTIVA
        //              END-IF
        //           ELSE
        //              END-IF
        //           END-IF.
        if (ws.getWsTpFrmAssva().isCollettiva()) {
            // COB_CODE:         IF WK-ORIG-RIV-IN
            //           *
            //                      END-PERFORM
            //                   ELSE
            //                      END-IF
            //                   END-IF
            if (ws.getWkOrigRiv().isInFld()) {
                //
                // COB_CODE:            PERFORM VARYING IX-W670 FROM 1 BY 1
                //                                UNTIL IX-W670 > W670-NUM-MAX-ELE
                //           *
                //                        END-IF
                //           *
                //                      END-PERFORM
                ws.getIxIndici().setW670(((short)1));
                while (!(ws.getIxIndici().getW670() > ws.getW670AreaLoas0670().getNumMaxEle())) {
                    //
                    // COB_CODE:              IF WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA) =
                    //                           W670-ID-TRCH-DI-GAR(IX-W670)
                    //           *
                    //                           END-IF
                    //           *
                    //                        END-IF
                    if (ws.getWtgaAreaTranche().getWtgaTab().getIdTrchDiGar(ws.getIxIndici().getTabTga()) == ws.getW670AreaLoas0670().getDatiTit(ws.getIxIndici().getW670()).getW670IdTrchDiGar()) {
                        //
                        // COB_CODE:                 IF W670-DT-VLT-NULL(IX-W670) NOT = HIGH-VALUES
                        //           *
                        //                                TO WREC-DT-VLT-TIT
                        //           *
                        //                           END-IF
                        if (!Characters.EQ_HIGH.test(ws.getW670AreaLoas0670().getDatiTit(ws.getIxIndici().getW670()).getW670DtVlt().getW670DtVltNullFormatted())) {
                            //
                            // COB_CODE: MOVE W670-DT-VLT(IX-W670)
                            //             TO WREC-DT-VLT-TIT
                            ws.getLoar0171().getWrecOut().setDtVltTit(TruncAbs.toInt(ws.getW670AreaLoas0670().getDatiTit(ws.getIxIndici().getW670()).getW670DtVlt().getW670DtVlt(), 8));
                            //
                        }
                        //
                    }
                    //
                    ws.getIxIndici().setW670(Trunc.toShort(ws.getIxIndici().getW670() + 1, 4));
                }
            }
            else {
                // COB_CODE: PERFORM S10980-RICERCA-TITOLO
                //              THRU S10980-RICERCA-TITOLO-EX
                s10980RicercaTitolo();
                //BNL2288
                // COB_CODE: IF IDSV0001-ESITO-OK AND WK-TIT-SI
                //              END-IF
                //           END-IF
                if (areaIdsv0001.getEsito().isIdsv0001EsitoOk() && ws.getWkTitTrovato().isSi()) {
                    // COB_CODE:               IF TIT-DT-VLT-NULL NOT = HIGH-VALUES
                    //           *
                    //                              TO WREC-DT-VLT-TIT
                    //           *
                    //                         END-IF
                    if (!Characters.EQ_HIGH.test(ws.getTitCont().getTitDtVlt().getTitDtVltNullFormatted())) {
                        //
                        // COB_CODE: MOVE TIT-DT-VLT
                        //             TO WREC-DT-VLT-TIT
                        ws.getLoar0171().getWrecOut().setDtVltTit(TruncAbs.toInt(ws.getTitCont().getTitDtVlt().getTitDtVlt(), 8));
                        //
                    }
                }
            }
        }
        else if (ws.getWkOrigRiv().isInFld()) {
            // COB_CODE:         IF WK-ORIG-RIV-IN
            //                      END-PERFORM
            //                   ELSE
            //           * data valuta deve essere valorizzata anche per emesso individ.
            //                      END-IF
            //           *
            //                   END-IF
            // COB_CODE: PERFORM VARYING IX-TGA-W870 FROM 1 BY 1
            //                     UNTIL IX-TGA-W870 > W870-TGA-NUM-MAX-ELE
            //               END-IF
            //           END-PERFORM
            ws.getIxIndici().setTgaW870(((short)1));
            while (!(ws.getIxIndici().getTgaW870() > ws.getW870AreaLoas0870().getW870TgaNumMaxEle())) {
                // COB_CODE: IF WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA) =
                //              W870-ID-TRCH-DI-GAR(IX-TGA-W870)
                //               END-IF
                //           END-IF
                if (ws.getWtgaAreaTranche().getWtgaTab().getIdTrchDiGar(ws.getIxIndici().getTabTga()) == ws.getW870AreaLoas0870().getW870TabTit().getIdTrchDiGar(ws.getIxIndici().getTgaW870())) {
                    // COB_CODE: PERFORM RICERCA-GAR
                    //              THRU RICERCA-GAR-EX
                    ricercaGar();
                    // COB_CODE: IF  WK-GRZ-SI
                    //           AND (ISPV0000-UNICO
                    //            OR ISPV0000-UNICO-RICORRENTE)
                    //              END-IF
                    //           END-IF
                    if (ws.getWkGrzTrovata().isSi() && (ws.getIspv0000().getPeriodoPrem().isUnico() || ws.getIspv0000().getPeriodoPrem().isUnicoRicorrente())) {
                        // COB_CODE: IF W870-DT-VLT-NULL(IX-TGA-W870, 1)
                        //              NOT = HIGH-VALUES
                        //                TO WREC-DT-VLT-TIT
                        //           END-IF
                        if (!Characters.EQ_HIGH.test(ws.getW870AreaLoas0870().getW870TabTit().getDtVltNullFormatted(ws.getIxIndici().getTgaW870(), 1))) {
                            // COB_CODE: MOVE W870-DT-VLT(IX-TGA-W870, 1)
                            //             TO WREC-DT-VLT-TIT
                            ws.getLoar0171().getWrecOut().setDtVltTit(TruncAbs.toInt(ws.getW870AreaLoas0870().getW870TabTit().getDtVlt(ws.getIxIndici().getTgaW870(), 1), 8));
                        }
                    }
                }
                ws.getIxIndici().setTgaW870(Trunc.toShort(ws.getIxIndici().getTgaW870() + 1, 4));
            }
        }
        else {
            // data valuta deve essere valorizzata anche per emesso individ.
            // COB_CODE: PERFORM RICERCA-GAR
            //              THRU RICERCA-GAR-EX
            ricercaGar();
            //
            // COB_CODE:            IF  WK-GRZ-SI
            //                      AND (ISPV0000-UNICO
            //                       OR ISPV0000-UNICO-RICORRENTE)
            //                          END-IF
            //           *
            //                      END-IF
            if (ws.getWkGrzTrovata().isSi() && (ws.getIspv0000().getPeriodoPrem().isUnico() || ws.getIspv0000().getPeriodoPrem().isUnicoRicorrente())) {
                // COB_CODE: PERFORM S10980-RICERCA-TITOLO
                //              THRU S10980-RICERCA-TITOLO-EX
                s10980RicercaTitolo();
                //
                //BNL2288
                // COB_CODE:                IF IDSV0001-ESITO-OK AND WK-TIT-SI
                //           *
                //                             END-IF
                //           *
                //                          END-IF
                if (areaIdsv0001.getEsito().isIdsv0001EsitoOk() && ws.getWkTitTrovato().isSi()) {
                    //
                    // COB_CODE:                   IF TIT-DT-VLT-NULL NOT = HIGH-VALUES
                    //           *
                    //                                  TO WREC-DT-VLT-TIT
                    //           *
                    //                             END-IF
                    if (!Characters.EQ_HIGH.test(ws.getTitCont().getTitDtVlt().getTitDtVltNullFormatted())) {
                        //
                        // COB_CODE: MOVE TIT-DT-VLT
                        //             TO WREC-DT-VLT-TIT
                        ws.getLoar0171().getWrecOut().setDtVltTit(TruncAbs.toInt(ws.getTitCont().getTitDtVlt().getTitDtVlt(), 8));
                        //
                    }
                    //
                }
                //
            }
            //
        }
        //
        // COB_CODE: MOVE ZERO TO WREC-NUM-GG-RIT-PAG
        ws.getLoar0171().getWrecOut().setNumGgRitPag(0);
        // COB_CODE: IF WK-ORIG-RIV-IN
        //              END-IF
        //           END-IF.
        if (ws.getWkOrigRiv().isInFld()) {
            // COB_CODE: IF IDSV0001-ESITO-OK
            //              END-IF
            //           END-IF
            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                // COB_CODE: PERFORM S10990-LEGGE-GG-RIT-PAG
                //              THRU S10990-EX
                s10990LeggeGgRitPag();
                // COB_CODE: IF  IDSV0001-ESITO-OK
                //           AND DETTAGLIO-TROVATO
                //                 TO WREC-NUM-GG-RIT-PAG
                //           END-IF
                if (areaIdsv0001.getEsito().isIdsv0001EsitoOk() && ws.getSwTrovatoDtc().isTrovato()) {
                    // COB_CODE: MOVE DTC-NUM-GG-RITARDO-PAG
                    //             TO WREC-NUM-GG-RIT-PAG
                    ws.getLoar0171().getWrecOut().setNumGgRitPag(TruncAbs.toInt(ws.getDettTitCont().getDtcNumGgRitardoPag().getDtcNumGgRitardoPag(), 5));
                }
            }
        }
        // COB_CODE: INSPECT W-REC-OUTRIVA REPLACING ALL LOW-VALUE BY SPACES.
        ws.setwRecOutrivaFormatted(ws.getwRecOutrivaFormatted().replace(String.valueOf(Types.LOW_CHAR_VAL), Types.SPACE_STRING));
        // COB_CODE: INSPECT W-REC-OUTRIVA REPLACING ALL HIGH-VALUE BY SPACES.
        ws.setwRecOutrivaFormatted(ws.getwRecOutrivaFormatted().replace(String.valueOf(Types.HIGH_CHAR_VAL), Types.SPACE_STRING));
        // COB_CODE: code not available
        outrivaTo.setVariable(ws.getwRecOutrivaFormatted());
        outrivaDAO.write(outrivaTo);
        ws.setFsOut(outrivaDAO.getFileStatus().getStatusCodeFormatted());
        //
        // COB_CODE:      IF FS-OUT NOT = '00'
        //           *
        //                      THRU GESTIONE-ERR-STD-EX
        //           *
        //                END-IF.
        if (!Conditions.eq(ws.getFsOut(), "00")) {
            //
            // COB_CODE: MOVE 'S10970-VALORIZZA-REC-DET'
            //             TO WK-LABEL-ERR
            ws.getWkGestioneMsgErr().setLabelErr("S10970-VALORIZZA-REC-DET");
            // COB_CODE: MOVE 'ERRORE WRITE FILE DI OUTPUT'
            //             TO WK-STRING
            ws.getWkGestioneMsgErr().setStringFld("ERRORE WRITE FILE DI OUTPUT");
            // COB_CODE: MOVE '001114'
            //             TO WK-COD-ERR
            ws.getWkGestioneMsgErr().setCodErr("001114");
            // COB_CODE: PERFORM GESTIONE-ERR-STD
            //              THRU GESTIONE-ERR-STD-EX
            gestioneErrStd();
            //
        }
    }

    /**Original name: S10990-LEGGE-GG-RIT-PAG<br>*/
    private void s10990LeggeGgRitPag() {
        // COB_CODE: MOVE 'S10990-LEGGE-GG-RIT-PAG'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S10990-LEGGE-GG-RIT-PAG");
        // COB_CODE: SET IDSI0011-FETCH-FIRST       TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setFetchFirst();
        // COB_CODE: SET  IDSO0011-SUCCESSFUL-RC    TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET  IDSO0011-SUCCESSFUL-SQL   TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
        // COB_CODE: SET INIZ-CUR-DTC               TO TRUE.
        ws.getSwCurDtc().setInizCurDtc();
        // COB_CODE: SET IDSI0011-TRATT-DEFAULT  TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setIdsi0011TrattDefault();
        // COB_CODE: MOVE WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA) TO DTC-ID-OGG.
        ws.getDettTitCont().setDtcIdOgg(ws.getWtgaAreaTranche().getWtgaTab().getIdTrchDiGar(ws.getIxIndici().getTabTga()));
        // COB_CODE: SET TRANCHE                 TO TRUE.
        ws.getWsTpOgg().setTranche();
        // COB_CODE: MOVE WS-TP-OGG              TO DTC-TP-OGG.
        ws.getDettTitCont().setDtcTpOgg(ws.getWsTpOgg().getWsTpOgg());
        // COB_CODE: SET IDSI0011-ID-OGGETTO     TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setIdsi0011IdOggetto();
        // COB_CODE: MOVE 'DETT-TIT-CONT'        TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("DETT-TIT-CONT");
        // COB_CODE: MOVE DETT-TIT-CONT          TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getDettTitCont().getDettTitContFormatted());
        // COB_CODE: MOVE ZERO TO IDSI0011-DATA-INIZIO-EFFETTO
        //                        IDSI0011-DATA-FINE-EFFETTO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        // COB_CODE: MOVE ZERO TO IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        // COB_CODE: SET DETTAGLIO-NON-TROVATO TO TRUE.
        ws.getSwTrovatoDtc().setNonTrovato();
        // COB_CODE: PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC
        //                      OR IDSV0001-ESITO-KO
        //                      OR FINE-CUR-DTC
        //                      OR DETTAGLIO-TROVATO
        //                   END-IF
        //           END-PERFORM.
        while (!(!ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc() || areaIdsv0001.getEsito().isIdsv0001EsitoKo() || ws.getSwCurDtc().isFineCurDtc() || ws.getSwTrovatoDtc().isTrovato())) {
            // COB_CODE: PERFORM CALL-DISPATCHER
            //              THRU CALL-DISPATCHER-EX
            callDispatcher();
            // COB_CODE:              IF IDSV0001-ESITO-OK
            //                           END-IF
            //                        ELSE
            //           *--> GESTIRE ERRORE
            //                               THRU EX-S0300
            //                        END-IF
            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                // COB_CODE:                 IF IDSO0011-SUCCESSFUL-RC
                //                              END-EVALUATE
                //                           ELSE
                //           *--> GESTIRE ERRORE
                //                                   THRU EX-S0300
                //                           END-IF
                if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
                    // COB_CODE:                    EVALUATE TRUE
                    //                                       WHEN IDSO0011-NOT-FOUND
                    //           *-->    NESSUN DATO ESTRATTO DALLA TABELLA
                    //                                           SET FINE-CUR-DTC TO TRUE
                    //                              WHEN IDSO0011-SUCCESSFUL-SQL
                    //           *-->       OPERAZIONE ESEGUITA CORRETTAMENTE
                    //                                     THRU S10991-EX
                    //                              WHEN OTHER
                    //           *--->   ERRORE DI ACCESSO AL DB
                    //                                     THRU EX-S0300
                    //                              END-EVALUATE
                    switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                        case Idso0011SqlcodeSigned.NOT_FOUND://-->    NESSUN DATO ESTRATTO DALLA TABELLA
                            // COB_CODE: SET FINE-CUR-DTC TO TRUE
                            ws.getSwCurDtc().setFineCurDtc();
                            break;

                        case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://-->       OPERAZIONE ESEGUITA CORRETTAMENTE
                            // COB_CODE: MOVE IDSO0011-BUFFER-DATI TO DETT-TIT-CONT
                            ws.getDettTitCont().setDettTitContFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                            //                      IF DTC-ID-TIT-CONT = TIT-ID-TIT-CONT
                            //                         SET DETTAGLIO-TROVATO TO TRUE
                            //                      END-IF
                            //
                            // N.B.
                            //       ANCHE A FRONTE DI UNA GARANZIA ANNUALE, PER LA QUALE
                            //       E' PLAUSIBILE LA PRESENZA DI PIU' TITOLI,
                            //       VIENE SEMPRE CONSIDERATO IL PRIMO DETTAGLIO TITOLO
                            //       DISPONIBILE
                            // COB_CODE: SET DETTAGLIO-TROVATO TO TRUE
                            ws.getSwTrovatoDtc().setTrovato();
                            // COB_CODE: PERFORM S10991-CHIUDI-CURSORE
                            //              THRU S10991-EX
                            s10991ChiudiCursore();
                            break;

                        default://--->   ERRORE DI ACCESSO AL DB
                            // COB_CODE: MOVE 'IDBSDTC0'
                            //             TO IEAI9901-COD-SERVIZIO-BE
                            ws.getIeai9901Area().setCodServizioBe("IDBSDTC0");
                            // COB_CODE: MOVE 'S10990-LEGGE-GG-RIT-PAG'
                            //             TO IEAI9901-LABEL-ERR
                            ws.getIeai9901Area().setLabelErr("S10990-LEGGE-GG-RIT-PAG");
                            // COB_CODE: MOVE '005166'
                            //             TO IEAI9901-COD-ERRORE
                            ws.getIeai9901Area().setCodErroreFormatted("005166");
                            // COB_CODE: MOVE SPACES TO IEAI9901-PARAMETRI-ERR
                            //skipped translation for moving SPACES to IEAI9901-PARAMETRI-ERR; considered in STRING statement translation below
                            // COB_CODE: STRING IDSI0011-CODICE-STR-DATO ';'
                            //                  IDSO0011-RETURN-CODE     ';'
                            //                  IDSO0011-SQLCODE
                            //                  DELIMITED BY SIZE
                            //                  INTO IEAI9901-PARAMETRI-ERR
                            //           END-STRING
                            ws.getIeai9901Area().setParametriErr(new StringBuffer(256).append(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011CodiceStrDatoFormatted()).append(";").append(ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted()).append(";").append(ws.getDispatcherVariables().getIdso0011Area().getSqlcode()).toString());
                            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                            //              THRU EX-S0300
                            s0300RicercaGravitaErrore();
                            break;
                    }
                }
                else {
                    //--> GESTIRE ERRORE
                    // COB_CODE: MOVE 'IDBSDTC0'
                    //             TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe("IDBSDTC0");
                    // COB_CODE: MOVE 'S10990-LEGGE-GG-RIT-PAG'
                    //             TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("S10990-LEGGE-GG-RIT-PAG");
                    // COB_CODE: MOVE '005166'
                    //             TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005166");
                    // COB_CODE: MOVE SPACES
                    //             TO IEAI9901-PARAMETRI-ERR
                    //skipped translation for moving SPACES to IEAI9901-PARAMETRI-ERR; considered in STRING statement translation below
                    // COB_CODE: STRING IDSI0011-CODICE-STR-DATO ';'
                    //                  IDSO0011-RETURN-CODE     ';'
                    //                  IDSO0011-SQLCODE
                    //                  DELIMITED BY SIZE
                    //                  INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    ws.getIeai9901Area().setParametriErr(new StringBuffer(256).append(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011CodiceStrDatoFormatted()).append(";").append(ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted()).append(";").append(ws.getDispatcherVariables().getIdso0011Area().getSqlcode()).toString());
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                }
            }
            else {
                //--> GESTIRE ERRORE
                // COB_CODE: MOVE 'IDBSDTC0'
                //             TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe("IDBSDTC0");
                // COB_CODE: MOVE 'S10990-LEGGE-GG-RIT-PAG'
                //             TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr("S10990-LEGGE-GG-RIT-PAG");
                // COB_CODE: MOVE '005166'
                //             TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("005166");
                // COB_CODE: MOVE SPACES
                //             TO IEAI9901-PARAMETRI-ERR
                //skipped translation for moving SPACES to IEAI9901-PARAMETRI-ERR; considered in STRING statement translation below
                // COB_CODE: STRING IDSI0011-CODICE-STR-DATO ';'
                //                  IDSO0011-RETURN-CODE     ';'
                //                  IDSO0011-SQLCODE
                //                  DELIMITED BY SIZE
                //                  INTO IEAI9901-PARAMETRI-ERR
                //           END-STRING
                ws.getIeai9901Area().setParametriErr(new StringBuffer(256).append(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011CodiceStrDatoFormatted()).append(";").append(ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted()).append(";").append(ws.getDispatcherVariables().getIdso0011Area().getSqlcode()).toString());
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
            }
        }
    }

    /**Original name: S10991-CHIUDI-CURSORE<br>*/
    private void s10991ChiudiCursore() {
        // COB_CODE: MOVE 'S10991-CHIUDI-CURSORE'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S10991-CHIUDI-CURSORE");
        // COB_CODE: SET IDSI0011-CLOSE-CURSOR  TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setIdsv0003CloseCursor();
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX.
        callDispatcher();
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //                   END-IF
        //                ELSE
        //           *--> GESTIRE ERRORE
        //                      THRU EX-S0300
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE:         IF IDSO0011-SUCCESSFUL-RC
            //                              END-EVALUATE
            //                   ELSE
            //           *--> GESTIRE ERRORE
            //                           THRU EX-S0300
            //                   END-IF
            if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
                // COB_CODE:                    EVALUATE TRUE
                //                              WHEN IDSO0011-SUCCESSFUL-SQL
                //           *-->       OPERAZIONE ESEGUITA CORRETTAMENTE
                //                                   CONTINUE
                //                              WHEN OTHER
                //           *--->   ERRORE DI ACCESSO AL DB
                //                                     THRU EX-S0300
                //                              END-EVALUATE
                switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                    case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://-->       OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: CONTINUE
                    //continue
                        break;

                    default://--->   ERRORE DI ACCESSO AL DB
                        // COB_CODE: MOVE 'IDBSDTC0'
                        //             TO IEAI9901-COD-SERVIZIO-BE
                        ws.getIeai9901Area().setCodServizioBe("IDBSDTC0");
                        // COB_CODE: MOVE 'S10991-CHIUDI-CURSORE'
                        //             TO IEAI9901-LABEL-ERR
                        ws.getIeai9901Area().setLabelErr("S10991-CHIUDI-CURSORE");
                        // COB_CODE: MOVE '005166'
                        //             TO IEAI9901-COD-ERRORE
                        ws.getIeai9901Area().setCodErroreFormatted("005166");
                        // COB_CODE: MOVE SPACES TO IEAI9901-PARAMETRI-ERR
                        //skipped translation for moving SPACES to IEAI9901-PARAMETRI-ERR; considered in STRING statement translation below
                        // COB_CODE: STRING IDSI0011-CODICE-STR-DATO ';'
                        //                  IDSO0011-RETURN-CODE     ';'
                        //                  IDSO0011-SQLCODE
                        //                  DELIMITED BY SIZE
                        //                  INTO IEAI9901-PARAMETRI-ERR
                        //           END-STRING
                        ws.getIeai9901Area().setParametriErr(new StringBuffer(256).append(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011CodiceStrDatoFormatted()).append(";").append(ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted()).append(";").append(ws.getDispatcherVariables().getIdso0011Area().getSqlcode()).toString());
                        // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        //              THRU EX-S0300
                        s0300RicercaGravitaErrore();
                        break;
                }
            }
            else {
                //--> GESTIRE ERRORE
                // COB_CODE: MOVE 'IDBSDTC0'
                //             TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe("IDBSDTC0");
                // COB_CODE: MOVE 'S10991-CHIUDI-CURSORE'
                //             TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr("S10991-CHIUDI-CURSORE");
                // COB_CODE: MOVE '005166'
                //             TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("005166");
                // COB_CODE: MOVE SPACES
                //             TO IEAI9901-PARAMETRI-ERR
                //skipped translation for moving SPACES to IEAI9901-PARAMETRI-ERR; considered in STRING statement translation below
                // COB_CODE: STRING IDSI0011-CODICE-STR-DATO ';'
                //                  IDSO0011-RETURN-CODE     ';'
                //                  IDSO0011-SQLCODE
                //                  DELIMITED BY SIZE
                //                  INTO IEAI9901-PARAMETRI-ERR
                //           END-STRING
                ws.getIeai9901Area().setParametriErr(new StringBuffer(256).append(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011CodiceStrDatoFormatted()).append(";").append(ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted()).append(";").append(ws.getDispatcherVariables().getIdso0011Area().getSqlcode()).toString());
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
            }
        }
        else {
            //--> GESTIRE ERRORE
            // COB_CODE: MOVE 'IDBSDTC0'
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe("IDBSDTC0");
            // COB_CODE: MOVE 'S10991-CHIUDI-CURSORE'
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S10991-CHIUDI-CURSORE");
            // COB_CODE: MOVE '005166'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005166");
            // COB_CODE: MOVE SPACES
            //             TO IEAI9901-PARAMETRI-ERR
            //skipped translation for moving SPACES to IEAI9901-PARAMETRI-ERR; considered in STRING statement translation below
            // COB_CODE: STRING IDSI0011-CODICE-STR-DATO ';'
            //                  IDSO0011-RETURN-CODE     ';'
            //                  IDSO0011-SQLCODE
            //                  DELIMITED BY SIZE
            //                  INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            ws.getIeai9901Area().setParametriErr(new StringBuffer(256).append(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011CodiceStrDatoFormatted()).append(";").append(ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted()).append(";").append(ws.getDispatcherVariables().getIdso0011Area().getSqlcode()).toString());
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: S10980-RICERCA-TITOLO<br>
	 * <pre>----------------------------------------------------------------*
	 *                      RICERCA TITOLI CONTABILI                   *
	 * ----------------------------------------------------------------*
	 *  -->> Indicatore di fine fetch</pre>*/
    private void s10980RicercaTitolo() {
        // COB_CODE: SET WK-FINE-FETCH-NO
        //             TO TRUE.
        ws.getWkFineFetch().setNo();
        //
        // -->> Flag Ricerca Titolo Contabile Incassato
        //
        // COB_CODE: SET WK-TIT-NO
        //             TO TRUE.
        ws.getWkTitTrovato().setNo();
        //
        // COB_CODE: INITIALIZE LDBVD601.
        initLdbvd601();
        // COB_CODE: MOVE 'IN'
        //             TO LDBVD601-TP-STA-TIT.
        ws.getLdbvd601().setTpStaTit("IN");
        //
        // COB_CODE: SET IDSI0011-FETCH-FIRST
        //             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setFetchFirst();
        //
        // COB_CODE: PERFORM S10990-IMPOSTA-TIT-CONT
        //              THRU S10990-IMPOSTA-TIT-CONT-EX
        s10990ImpostaTitCont();
        //
        // COB_CODE: PERFORM S11000-LEGGI-TIT-CONT
        //              THRU S11000-LEGGI-TIT-CONT-EX
        //             UNTIL WK-FINE-FETCH-SI
        //                OR IDSV0001-ESITO-KO
        while (!(ws.getWkFineFetch().isSi() || areaIdsv0001.getEsito().isIdsv0001EsitoKo())) {
            s11000LeggiTitCont();
        }
        //
        // --> Non esistono Titoli Contabili con stato INCASSATO
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE:         IF WK-TIT-NO
            //           *
            //                           OR IDSV0001-ESITO-KO
            //           *
            //                END-IF.
            if (ws.getWkTitTrovato().isNo()) {
                //
                // COB_CODE: SET WK-FINE-FETCH-NO
                //             TO TRUE
                ws.getWkFineFetch().setNo();
                //
                // COB_CODE: MOVE 'EM'
                //             TO LDBVD601-TP-STA-TIT
                ws.getLdbvd601().setTpStaTit("EM");
                //
                // COB_CODE: SET IDSI0011-FETCH-FIRST
                //             TO TRUE
                ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setFetchFirst();
                //
                // COB_CODE: PERFORM S10990-IMPOSTA-TIT-CONT
                //              THRU S10990-IMPOSTA-TIT-CONT-EX
                s10990ImpostaTitCont();
                //
                // COB_CODE: PERFORM S11000-LEGGI-TIT-CONT
                //              THRU S11000-LEGGI-TIT-CONT-EX
                //             UNTIL WK-FINE-FETCH-SI
                //                OR IDSV0001-ESITO-KO
                while (!(ws.getWkFineFetch().isSi() || areaIdsv0001.getEsito().isIdsv0001EsitoKo())) {
                    s11000LeggiTitCont();
                }
                //
            }
        }
    }

    /**Original name: S10990-IMPOSTA-TIT-CONT<br>
	 * <pre>----------------------------------------------------------------*
	 *      VALORIZZAZIONI PER LA LETTURA CON WHERE CONDITION AD HOC   *
	 * ----------------------------------------------------------------*</pre>*/
    private void s10990ImpostaTitCont() {
        // COB_CODE: MOVE WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA)
        //             TO LDBVD601-ID-OGG.
        ws.getLdbvd601().setIdOgg(ws.getWtgaAreaTranche().getWtgaTab().getIdTrchDiGar(ws.getIxIndici().getTabTga()));
        // COB_CODE: MOVE 'TG'
        //             TO LDBVD601-TP-OGG.
        ws.getLdbvd601().setTpOgg("TG");
        // COB_CODE: MOVE 'PR'
        //             TO LDBVD601-TP-TIT.
        ws.getLdbvd601().setTpTit("PR");
        //    SIR CQPrd00020801: LA DATA INCASSO NON VIENE VALORIZZATA
        //    NEL FILE DI OUTPUT
        // COB_CODE: MOVE WS-DT-INFINITO-1-N
        //             TO IDSI0011-DATA-INIZIO-EFFETTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(ws.getIdsv0015().getDtInfinito1N());
        // COB_CODE: MOVE WS-TS-INFINITO-1-N
        //             TO IDSI0011-DATA-COMPETENZA
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(ws.getIdsv0015().getTsInfinito1N());
        // COB_CODE: MOVE ZEROES
        //             TO IDSI0011-DATA-FINE-EFFETTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        //
        //  --> Nome tabella fisica db
        //
        // COB_CODE: MOVE 'LDBSD600'
        //             TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("LDBSD600");
        //
        //  --> Dclgen tabella
        //
        // COB_CODE: MOVE SPACES
        //             TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
        // COB_CODE: MOVE LDBVD601
        //             TO IDSI0011-BUFFER-WHERE-COND.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferWhereCond(ws.getLdbvd601().getLdbvd601Formatted());
        //
        //  --> Tipo operazione
        //
        // COB_CODE: SET IDSI0011-WHERE-CONDITION
        //             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setWhereCondition();
        //
        //  --> Modalita di accesso
        //
        // COB_CODE: SET IDSI0011-TRATT-DEFAULT
        //             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setIdsi0011TrattDefault();
        //
        //  --> Inizializzazione Return Code
        //
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC
        //             TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL
        //             TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
    }

    /**Original name: S11000-LEGGI-TIT-CONT<br>
	 * <pre>----------------------------------------------------------------*
	 *             LETTURA DELLE TABELLA TRANCHE DI GARANZIA           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s11000LeggiTitCont() {
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX.
        callDispatcher();
        //
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //           *
        //                   END-EVALUATE
        //           *
        //                ELSE
        //           *
        //           *  --> Errore dispatcher
        //           *
        //                      THRU GESTIONE-ERR-STD-EX
        //           *
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            //
            //
            // COB_CODE:         EVALUATE TRUE
            //           *
            //                       WHEN IDSO0011-NOT-FOUND
            //           *
            //           *  --> Chiave non trovata, fine occorrenze fetch
            //           *
            //                             TO TRUE
            //           *
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //           *
            //           *  --> Operazione eseguita correttamente
            //           *
            //                              TO TIT-CONT
            //           *
            //                       WHEN OTHER
            //           *
            //           *  --> Errore di accesso al db
            //                               THRU GESTIONE-ERR-STD-EX
            //           *
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.NOT_FOUND://
                    //  --> Chiave non trovata, fine occorrenze fetch
                    //
                    // COB_CODE: SET WK-FINE-FETCH-SI
                    //            TO TRUE
                    ws.getWkFineFetch().setSi();
                    //
                    break;

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://
                    //  --> Operazione eseguita correttamente
                    //
                    // COB_CODE: SET WK-TIT-SI
                    //            TO TRUE
                    ws.getWkTitTrovato().setSi();
                    //
                    // COB_CODE: SET IDSI0011-FETCH-NEXT
                    //            TO TRUE
                    ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setFetchNext();
                    //
                    // COB_CODE: MOVE IDSO0011-BUFFER-DATI
                    //             TO TIT-CONT
                    ws.getTitCont().setTitContFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    //
                    break;

                default://
                    //  --> Errore di accesso al db
                    // COB_CODE: MOVE 'S11000-LEGGI-TIT-CONT'
                    //             TO WK-LABEL-ERR
                    ws.getWkGestioneMsgErr().setLabelErr("S11000-LEGGI-TIT-CONT");
                    // COB_CODE: MOVE 'ERRORE LETTURA TITOLO CONTABILE'
                    //             TO WK-STRING
                    ws.getWkGestioneMsgErr().setStringFld("ERRORE LETTURA TITOLO CONTABILE");
                    // COB_CODE: MOVE '005016'
                    //             TO WK-COD-ERR
                    ws.getWkGestioneMsgErr().setCodErr("005016");
                    // COB_CODE: PERFORM GESTIONE-ERR-STD
                    //              THRU GESTIONE-ERR-STD-EX
                    gestioneErrStd();
                    //
                    break;
            }
            //
        }
        else {
            //
            //  --> Errore dispatcher
            //
            // COB_CODE: MOVE 'S11000-LEGGI-TIT-CONT'
            //             TO WK-LABEL-ERR
            ws.getWkGestioneMsgErr().setLabelErr("S11000-LEGGI-TIT-CONT");
            // COB_CODE: MOVE 'ERRORE LETTURA TITOLO CONTABILE'
            //             TO WK-STRING
            ws.getWkGestioneMsgErr().setStringFld("ERRORE LETTURA TITOLO CONTABILE");
            // COB_CODE: MOVE '005016'
            //             TO WK-COD-ERR
            ws.getWkGestioneMsgErr().setCodErr("005016");
            // COB_CODE: PERFORM GESTIONE-ERR-STD
            //              THRU GESTIONE-ERR-STD-EX
            gestioneErrStd();
            //
        }
    }

    /**Original name: S11000-GESTIONE-FILE-MF<br>
	 * <pre>----------------------------------------------------------------*
	 *                  GESTIONE DEL FILE DI OUTPUT   - MANAG. FEE -   *
	 * ----------------------------------------------------------------*</pre>*/
    private void s11000GestioneFileMf() {
        Loas0820 loas0820 = null;
        // COB_CODE: MOVE 'S11000-GESTIONE-FILE-MF'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S11000-GESTIONE-FILE-MF");
        // COB_CODE: SET  IDSV8888-BUSINESS-DBG      TO TRUE
        ws.getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
        // COB_CODE: MOVE 'LOAS0820'                 TO IDSV8888-NOME-PGM.
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("LOAS0820");
        // COB_CODE: SET  IDSV8888-INIZIO            TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setInizio();
        // COB_CODE: MOVE SPACES                     TO IDSV8888-DESC-PGM
        //skipped translation for moving SPACES to IDSV8888-DESC-PGM; considered in STRING statement translation below
        // COB_CODE: STRING 'Gestione file MF'
        //                   DELIMITED BY SIZE
        //                   INTO IDSV8888-DESC-PGM
        //           END-STRING
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("Gestione file MF");
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        //
        // COB_CODE:      CALL LOAS0820  USING AREA-IDSV0001
        //                                     WCOM-IO-STATI
        //                                     WPMO-AREA-PARAM-MOVI
        //                                     WPOL-AREA-POLIZZA
        //                                     WADE-AREA-ADESIONE
        //                                     WGRZ-AREA-GARANZIA
        //                                     WTGA-AREA-TRANCHE
        //                                     WAPPL-NUM-ELAB
        //           *
        //                 ON EXCEPTION
        //           *
        //                         THRU GESTIONE-ERR-SIST-EX
        //           *
        //                 END-CALL.
        try {
            loas0820 = Loas0820.getInstance();
            loas0820.run(new Object[] {areaIdsv0001, wcomIoStati, areaMain, areaMain.getWpolAreaPolizza(), areaMain.getWadeAreaAdesione(), ws.getWgrzAreaGaranzia(), ws.getWtgaAreaTranche(), ws.getWapplNumElab()});
        }
        catch (ProgramExecutionException __ex) {
            //
            // COB_CODE: MOVE 'LOAS0820'
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe("LOAS0820");
            // COB_CODE: MOVE 'ERRORE CALL MODULO LOAS0820'
            //             TO CALL-DESC
            ws.getIdsv0002().setCallDesc("ERRORE CALL MODULO LOAS0820");
            // COB_CODE: MOVE 'S11000-GESTIONE-FILE-MF'
            //             TO WK-LABEL-ERR
            ws.getWkGestioneMsgErr().setLabelErr("S11000-GESTIONE-FILE-MF");
            //
            // COB_CODE: PERFORM GESTIONE-ERR-SIST
            //              THRU GESTIONE-ERR-SIST-EX
            gestioneErrSist();
            //
        }
        // COB_CODE: SET  IDSV8888-BUSINESS-DBG      TO TRUE
        ws.getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
        // COB_CODE: MOVE 'LOAS0820'                 TO IDSV8888-NOME-PGM.
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("LOAS0820");
        // COB_CODE: SET  IDSV8888-FINE              TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setFine();
        // COB_CODE: MOVE SPACES                     TO IDSV8888-DESC-PGM
        //skipped translation for moving SPACES to IDSV8888-DESC-PGM; considered in STRING statement translation below
        // COB_CODE: STRING 'Gestione file MF'
        //                   DELIMITED BY SIZE
        //                   INTO IDSV8888-DESC-PGM
        //           END-STRING
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("Gestione file MF");
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
    }

    /**Original name: S90000-OPERAZ-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *     GESTIONE CHIAMATA ALLA ROUTINE DEL TRANSITORIO
	 * ----------------------------------------------------------------*
	 * S12000-GEST-TRANSITORIO.
	 *     IF IDSV0001-ESITO-OK
	 * --     SERVIZIO SCRITTURA TRANSITORIO PER GESTIONE RENDITE
	 *        IF SI-GAR-REND
	 *           PERFORM S12100-PREPARA-LRES0002
	 *              THRU S12100-PREPARA-LRES0002-EX
	 *           PERFORM S12200-CALL-LRES0002
	 *              THRU S12200-CALL-LRES0002-EX
	 *       END-IF
	 *     END-IF.
	 * S12000-GEST-TRANSITORIO-EX.
	 *     EXIT.
	 * ----------------------------------------------------------------*
	 *   PREPARA AREA SERVIZIO SCRITTURA TRANSITORIO GESTIONE RENDITE
	 * ----------------------------------------------------------------*
	 * S12100-PREPARA-LRES0002.
	 *     INITIALIZE                        WRAN-AREA-RAPP-ANAG
	 *                                       AREA-IO-LREC0003.
	 *     SET LREC0003-FUNZ-VAR-ANAGRAFE    TO TRUE.
	 *     SET LREC0003-FUNZ-RIVALUTAZIONE   TO TRUE.
	 *     SET LREC0003-FUNZ-RIVALUTAZIONE   TO TRUE.
	 * S12100-PREPARA-LRES0002-EX.
	 *     EXIT.
	 * ----------------------------------------------------------------*
	 *   CALL AL SERVIZIO SCRITTURA TRANSITORIO GESTIONE RENDITE
	 * ----------------------------------------------------------------*
	 *  S12200-CALL-LRES0002.
	 *      MOVE 'LRES0002'                   TO WK-PGM-CALL.
	 *      CALL WK-PGM-CALL USING AREA-IDSV0001
	 *                             WCOM-AREA-STATI
	 *                             WMOV-AREA-MOVIMENTO
	 *                             WPOL-AREA-POLIZZA
	 *                             WADE-AREA-ADESIONE
	 *                             WGRZ-AREA-GARANZIA
	 *                             WRAN-AREA-RAPP-ANAG
	 *                             AREA-IO-LREC0003
	 *      ON EXCEPTION
	 *            MOVE WK-PGM
	 *              TO IEAI9901-COD-SERVIZIO-BE
	 *            MOVE 'CALL SERVIZIO SCRITTURA TRANSITORIO'
	 *              TO CALL-DESC
	 *            MOVE 'S12200-CALL-LRES0002'
	 *              TO IEAI9901-LABEL-ERR
	 *            PERFORM S0290-ERRORE-DI-SISTEMA
	 *               THRU EX-S0290
	 *      END-CALL.
	 *  S12200-CALL-LRES0002-EX.
	 *      EXIT.
	 *  ============================================================== *
	 *  -->         O P E R A Z I O N I   F I N A L I              <-- *
	 *  ============================================================== *</pre>*/
    private void s90000OperazFinali() {
        // COB_CODE: MOVE 'S90000-OPERAZ-FINALI'  TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S90000-OPERAZ-FINALI");
        // COB_CODE: PERFORM DISPLAY-LABEL THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        //    MOVE VPMO-AREA-PARAM-MOVI
        //      TO WPMO-AREA-PARAM-MOVI.
        //
        //    IF WCOM-WRITE-NIENTE
        //
        //       SET IDSV0001-ESITO-OK
        //         TO TRUE
        //
        //    END-IF.
        //
        // COB_CODE:      IF IDSV0001-ESITO-KO
        //           *
        //                   MOVE VPMO-ELE-PARAM-MOV-MAX  TO WPMO-ELE-PARAM-MOV-MAX
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoKo()) {
            //
            // COB_CODE: SET WCOM-WRITE-ERR
            //             TO TRUE
            wcomIoStati.getLccc0261().getWrite().setWcomWriteErr();
            //
            // COB_CODE:         IF WPOL-TP-FRM-ASSVA = 'IN'
            //           *
            //                        TO TRUE
            //           *
            //                   ELSE
            //           *
            //                        TO TRUE
            //           *
            //                   END-IF
            if (Conditions.eq(areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolTpFrmAssva(), "IN")) {
                //
                // COB_CODE: SET WCOM-POLIZZA-MBS
                //             TO TRUE
                wcomIoStati.getLccc0261().getDatiMbs().getWcomTpOggMbs().setWcomPolizzaMbs();
                // COB_CODE: SET WCOM-POLIZZA-L11
                //             TO TRUE
                wcomIoStati.getLccc0261().getDatiBlocco().getTpOggBlocco().setWcomPolizzaL11();
                //
            }
            else {
                //
                // COB_CODE: SET WCOM-ADESIONE-MBS
                //             TO TRUE
                wcomIoStati.getLccc0261().getDatiMbs().getWcomTpOggMbs().setWcomAdesioneMbs();
                // COB_CODE: SET WCOM-ADESIONE-L11
                //             TO TRUE
                wcomIoStati.getLccc0261().getDatiBlocco().getTpOggBlocco().setWcomAdesioneL11();
                //
            }
            //
            // COB_CODE: SET WMOV-ST-ADD
            //            TO TRUE
            areaMain.getWmovAreaMovimento().getLccvmov1().getStatus().setWcomStAdd();
            //
            // COB_CODE: INITIALIZE  WPMO-AREA-PARAM-MOVI
            initWpmoAreaParamMovi();
            // COB_CODE: PERFORM
            //              VARYING IX-VPMO FROM 1 BY 1
            //                UNTIL IX-VPMO GREATER VPMO-ELE-PARAM-MOV-MAX
            //                  END-IF
            //           END-PERFORM
            ws.getIxIndici().setVpmo(((short)1));
            while (!(ws.getIxIndici().getVpmo() > ws.getWorkCommarea().getEleParamMovMax())) {
                // COB_CODE: IF NOT VPMO-ST-INV(IX-VPMO)
                //                TO WPMO-TAB-PARAM-MOV(IX-VPMO)
                //           END-IF
                if (!ws.getWorkCommarea().getTabParamMov(ws.getIxIndici().getVpmo()).getLccvpmo1().getStatus().isInv()) {
                    // COB_CODE: MOVE VPMO-TAB-PARAM-MOV(IX-VPMO)
                    //             TO WPMO-TAB-PARAM-MOV(IX-VPMO)
                    areaMain.getWpmoTabParamMov(ws.getIxIndici().getVpmo()).setTabParamMovBytes(ws.getWorkCommarea().getTabParamMov(ws.getIxIndici().getVpmo()).getWpmoTabParamMovBytes());
                }
                ws.getIxIndici().setVpmo(Trunc.toShort(ws.getIxIndici().getVpmo() + 1, 4));
            }
            // COB_CODE: MOVE VPMO-ELE-PARAM-MOV-MAX  TO WPMO-ELE-PARAM-MOV-MAX
            areaMain.setWpmoEleParamMovMax(ws.getWorkCommarea().getEleParamMovMax());
            //
        }
    }

    /**Original name: S90200-CLOSE-OUT<br>
	 * <pre>----------------------------------------------------------------*
	 *                CHIUSURA DEL FILE DI OUTPUT OUTRIVA              *
	 * ----------------------------------------------------------------*</pre>*/
    private void s90200CloseOut() {
        ConcatUtil concatUtil = null;
        // COB_CODE: CLOSE OUTRIVA
        outrivaDAO.close();
        ws.setFsOut(outrivaDAO.getFileStatus().getStatusCodeFormatted());
        //
        // COB_CODE:      IF FS-OUT NOT = '00'
        //           * changes SIR FCTVI00011410 starts here
        //                      THRU GESTIONE-ERR-STD-EX
        //           *
        //                END-IF.
        if (!Conditions.eq(ws.getFsOut(), "00")) {
            // changes SIR FCTVI00011410 starts here
            // COB_CODE: MOVE FS-OUT TO WS-FS-OUT
            ws.setWsFsOut(ws.getFsOut());
            // changes SIR FCTVI00011410 ends here
            // COB_CODE: MOVE 'S90200-CLOSE-OUT' TO WK-LABEL-ERR
            ws.getWkGestioneMsgErr().setLabelErr("S90200-CLOSE-OUT");
            // changes SIR FCTVI00011410 starts here
            // COB_CODE: STRING 'ERRORE CLOSE FILE DI OUTPUT' ';'  WS-FS-OUT
            //                  DELIMITED BY SIZE INTO WK-STRING
            //           END-STRING
            concatUtil = ConcatUtil.buildString(WkGestioneMsgErr.Len.STRING_FLD, "ERRORE CLOSE FILE DI OUTPUT", ";", ws.getWsFsOutFormatted());
            ws.getWkGestioneMsgErr().setStringFld(concatUtil.replaceInString(ws.getWkGestioneMsgErr().getStringFldFormatted()));
            // changes SIR FCTVI00011410 ends here
            // COB_CODE: MOVE '001114' TO WK-COD-ERR
            ws.getWkGestioneMsgErr().setCodErr("001114");
            // COB_CODE: PERFORM GESTIONE-ERR-STD
            //              THRU GESTIONE-ERR-STD-EX
            gestioneErrStd();
            //
        }
    }

    /**Original name: RICERCA-GAR<br>
	 * <pre>----------------------------------------------------------------*
	 *     RICERCA GARANZIA
	 * ----------------------------------------------------------------*</pre>*/
    private void ricercaGar() {
        // COB_CODE: SET WK-GRZ-NO       TO TRUE
        ws.getWkGrzTrovata().setNo();
        // COB_CODE: PERFORM VARYING IX-GRZ FROM 1 BY 1
        //                     UNTIL IX-GRZ > WGRZ-ELE-GAR-MAX
        //                        OR WK-GRZ-SI
        //              END-IF
        //           END-PERFORM.
        ws.getIxIndici().setGrz(((short)1));
        while (!(ws.getIxIndici().getGrz() > ws.getWgrzAreaGaranzia().getEleGarMax() || ws.getWkGrzTrovata().isSi())) {
            // COB_CODE: IF WGRZ-ID-GAR(IX-GRZ) = WTGA-ID-GAR(IX-TAB-TGA)
            //                TO ISPV0000-PERIODO-PREM
            //           END-IF
            if (ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getGrz()).getLccvgrz1().getDati().getWgrzIdGar() == ws.getWtgaAreaTranche().getWtgaTab().getIdGar(ws.getIxIndici().getTabTga())) {
                // COB_CODE: SET WK-GRZ-SI               TO TRUE
                ws.getWkGrzTrovata().setSi();
                // COB_CODE: MOVE WGRZ-TP-PER-PRE(IX-GRZ)
                //             TO ISPV0000-PERIODO-PREM
                ws.getIspv0000().getPeriodoPrem().setPeriodoPremFormatted(ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getGrz()).getLccvgrz1().getDati().getWgrzTpPerPreFormatted());
            }
            ws.getIxIndici().setGrz(Trunc.toShort(ws.getIxIndici().getGrz() + 1, 4));
        }
    }

    /**Original name: GESTIONE-ERR-STD<br>
	 * <pre>----------------------------------------------------------------*
	 *                 GESTIONE STANDARD DELL'ERRORE                   *
	 * ----------------------------------------------------------------*</pre>*/
    private void gestioneErrStd() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE WK-PGM
        //             TO IEAI9901-COD-SERVIZIO-BE.
        ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
        // COB_CODE: MOVE WK-LABEL-ERR
        //             TO IEAI9901-LABEL-ERR.
        ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
        // COB_CODE: MOVE WK-COD-ERR
        //             TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErroreFormatted(ws.getWkGestioneMsgErr().getCodErrFormatted());
        // COB_CODE: STRING WK-STRING ';'
        //                  IDSO0011-RETURN-CODE ';'
        //                  IDSO0011-SQLCODE
        //                  DELIMITED BY SIZE
        //                  INTO IEAI9901-PARAMETRI-ERR
        //           END-STRING.
        concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkGestioneMsgErr().getStringFldFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
        ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
        //
        // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
        //              THRU EX-S0300.
        s0300RicercaGravitaErrore();
    }

    /**Original name: GESTIONE-ERR-SIST<br>
	 * <pre>----------------------------------------------------------------*
	 *            GESTIONE STANDARD DELL'ERRORE DI SISTEMA             *
	 * ----------------------------------------------------------------*</pre>*/
    private void gestioneErrSist() {
        // COB_CODE: MOVE WK-LABEL-ERR
        //             TO IEAI9901-LABEL-ERR
        ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
        //
        // COB_CODE: PERFORM S0290-ERRORE-DI-SISTEMA
        //              THRU EX-S0290.
        s0290ErroreDiSistema();
    }

    /**Original name: VALORIZZA-OUTPUT-MOV<br>
	 * <pre>----------------------------------------------------------------*
	 *      COPY VALORIZZA OUTPUT TABELLE                              *
	 * ----------------------------------------------------------------*
	 *   --> MOVIMENTO
	 * ------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    VALORIZZA OUTPUT COPY LCCVMOV3
	 *    ULTIMO AGG. 04 SET 2008
	 * ------------------------------------------------------------</pre>*/
    private void valorizzaOutputMov() {
        // COB_CODE: MOVE MOV-ID-MOVI
        //             TO (SF)-ID-PTF
        areaMain.getWmovAreaMovimento().getLccvmov1().setIdPtf(ws.getMovi().getMovIdMovi());
        // COB_CODE: MOVE MOV-ID-MOVI
        //             TO (SF)-ID-MOVI
        areaMain.getWmovAreaMovimento().getLccvmov1().getDati().setWmovIdMovi(ws.getMovi().getMovIdMovi());
        // COB_CODE: MOVE MOV-COD-COMP-ANIA
        //             TO (SF)-COD-COMP-ANIA
        areaMain.getWmovAreaMovimento().getLccvmov1().getDati().setWmovCodCompAnia(ws.getMovi().getMovCodCompAnia());
        // COB_CODE: IF MOV-ID-OGG-NULL = HIGH-VALUES
        //                TO (SF)-ID-OGG-NULL
        //           ELSE
        //                TO (SF)-ID-OGG
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getMovi().getMovIdOgg().getMovIdOggNullFormatted())) {
            // COB_CODE: MOVE MOV-ID-OGG-NULL
            //             TO (SF)-ID-OGG-NULL
            areaMain.getWmovAreaMovimento().getLccvmov1().getDati().getWmovIdOgg().setWmovIdOggNull(ws.getMovi().getMovIdOgg().getMovIdOggNull());
        }
        else {
            // COB_CODE: MOVE MOV-ID-OGG
            //             TO (SF)-ID-OGG
            areaMain.getWmovAreaMovimento().getLccvmov1().getDati().getWmovIdOgg().setWmovIdOgg(ws.getMovi().getMovIdOgg().getMovIdOgg());
        }
        // COB_CODE: IF MOV-IB-OGG-NULL = HIGH-VALUES
        //                TO (SF)-IB-OGG-NULL
        //           ELSE
        //                TO (SF)-IB-OGG
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getMovi().getMovIbOgg(), Movi.Len.MOV_IB_OGG)) {
            // COB_CODE: MOVE MOV-IB-OGG-NULL
            //             TO (SF)-IB-OGG-NULL
            areaMain.getWmovAreaMovimento().getLccvmov1().getDati().setWmovIbOgg(ws.getMovi().getMovIbOgg());
        }
        else {
            // COB_CODE: MOVE MOV-IB-OGG
            //             TO (SF)-IB-OGG
            areaMain.getWmovAreaMovimento().getLccvmov1().getDati().setWmovIbOgg(ws.getMovi().getMovIbOgg());
        }
        // COB_CODE: IF MOV-IB-MOVI-NULL = HIGH-VALUES
        //                TO (SF)-IB-MOVI-NULL
        //           ELSE
        //                TO (SF)-IB-MOVI
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getMovi().getMovIbMovi(), Movi.Len.MOV_IB_MOVI)) {
            // COB_CODE: MOVE MOV-IB-MOVI-NULL
            //             TO (SF)-IB-MOVI-NULL
            areaMain.getWmovAreaMovimento().getLccvmov1().getDati().setWmovIbMovi(ws.getMovi().getMovIbMovi());
        }
        else {
            // COB_CODE: MOVE MOV-IB-MOVI
            //             TO (SF)-IB-MOVI
            areaMain.getWmovAreaMovimento().getLccvmov1().getDati().setWmovIbMovi(ws.getMovi().getMovIbMovi());
        }
        // COB_CODE: IF MOV-TP-OGG-NULL = HIGH-VALUES
        //                TO (SF)-TP-OGG-NULL
        //           ELSE
        //                TO (SF)-TP-OGG
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getMovi().getMovTpOggFormatted())) {
            // COB_CODE: MOVE MOV-TP-OGG-NULL
            //             TO (SF)-TP-OGG-NULL
            areaMain.getWmovAreaMovimento().getLccvmov1().getDati().setWmovTpOgg(ws.getMovi().getMovTpOgg());
        }
        else {
            // COB_CODE: MOVE MOV-TP-OGG
            //             TO (SF)-TP-OGG
            areaMain.getWmovAreaMovimento().getLccvmov1().getDati().setWmovTpOgg(ws.getMovi().getMovTpOgg());
        }
        // COB_CODE: IF MOV-ID-RICH-NULL = HIGH-VALUES
        //                TO (SF)-ID-RICH-NULL
        //           ELSE
        //                TO (SF)-ID-RICH
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getMovi().getMovIdRich().getMovIdRichNullFormatted())) {
            // COB_CODE: MOVE MOV-ID-RICH-NULL
            //             TO (SF)-ID-RICH-NULL
            areaMain.getWmovAreaMovimento().getLccvmov1().getDati().getWmovIdRich().setWmovIdRichNull(ws.getMovi().getMovIdRich().getMovIdRichNull());
        }
        else {
            // COB_CODE: MOVE MOV-ID-RICH
            //             TO (SF)-ID-RICH
            areaMain.getWmovAreaMovimento().getLccvmov1().getDati().getWmovIdRich().setWmovIdRich(ws.getMovi().getMovIdRich().getMovIdRich());
        }
        // COB_CODE: IF MOV-TP-MOVI-NULL = HIGH-VALUES
        //                TO (SF)-TP-MOVI-NULL
        //           ELSE
        //                TO (SF)-TP-MOVI
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getMovi().getMovTpMovi().getMovTpMoviNullFormatted())) {
            // COB_CODE: MOVE MOV-TP-MOVI-NULL
            //             TO (SF)-TP-MOVI-NULL
            areaMain.getWmovAreaMovimento().getLccvmov1().getDati().getWmovTpMovi().setWmovTpMoviNull(ws.getMovi().getMovTpMovi().getMovTpMoviNull());
        }
        else {
            // COB_CODE: MOVE MOV-TP-MOVI
            //             TO (SF)-TP-MOVI
            areaMain.getWmovAreaMovimento().getLccvmov1().getDati().getWmovTpMovi().setWmovTpMovi(ws.getMovi().getMovTpMovi().getMovTpMovi());
        }
        // COB_CODE: MOVE MOV-DT-EFF
        //             TO (SF)-DT-EFF
        areaMain.getWmovAreaMovimento().getLccvmov1().getDati().setWmovDtEff(ws.getMovi().getMovDtEff());
        // COB_CODE: IF MOV-ID-MOVI-ANN-NULL = HIGH-VALUES
        //                TO (SF)-ID-MOVI-ANN-NULL
        //           ELSE
        //                TO (SF)-ID-MOVI-ANN
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getMovi().getMovIdMoviAnn().getMovIdMoviAnnNullFormatted())) {
            // COB_CODE: MOVE MOV-ID-MOVI-ANN-NULL
            //             TO (SF)-ID-MOVI-ANN-NULL
            areaMain.getWmovAreaMovimento().getLccvmov1().getDati().getWmovIdMoviAnn().setWmovIdMoviAnnNull(ws.getMovi().getMovIdMoviAnn().getMovIdMoviAnnNull());
        }
        else {
            // COB_CODE: MOVE MOV-ID-MOVI-ANN
            //             TO (SF)-ID-MOVI-ANN
            areaMain.getWmovAreaMovimento().getLccvmov1().getDati().getWmovIdMoviAnn().setWmovIdMoviAnn(ws.getMovi().getMovIdMoviAnn().getMovIdMoviAnn());
        }
        // COB_CODE: IF MOV-ID-MOVI-COLLG-NULL = HIGH-VALUES
        //                TO (SF)-ID-MOVI-COLLG-NULL
        //           ELSE
        //                TO (SF)-ID-MOVI-COLLG
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getMovi().getMovIdMoviCollg().getMovIdMoviCollgNullFormatted())) {
            // COB_CODE: MOVE MOV-ID-MOVI-COLLG-NULL
            //             TO (SF)-ID-MOVI-COLLG-NULL
            areaMain.getWmovAreaMovimento().getLccvmov1().getDati().getWmovIdMoviCollg().setWmovIdMoviCollgNull(ws.getMovi().getMovIdMoviCollg().getMovIdMoviCollgNull());
        }
        else {
            // COB_CODE: MOVE MOV-ID-MOVI-COLLG
            //             TO (SF)-ID-MOVI-COLLG
            areaMain.getWmovAreaMovimento().getLccvmov1().getDati().getWmovIdMoviCollg().setWmovIdMoviCollg(ws.getMovi().getMovIdMoviCollg().getMovIdMoviCollg());
        }
        // COB_CODE: MOVE MOV-DS-OPER-SQL
        //             TO (SF)-DS-OPER-SQL
        areaMain.getWmovAreaMovimento().getLccvmov1().getDati().setWmovDsOperSql(ws.getMovi().getMovDsOperSql());
        // COB_CODE: MOVE MOV-DS-VER
        //             TO (SF)-DS-VER
        areaMain.getWmovAreaMovimento().getLccvmov1().getDati().setWmovDsVer(ws.getMovi().getMovDsVer());
        // COB_CODE: MOVE MOV-DS-TS-CPTZ
        //             TO (SF)-DS-TS-CPTZ
        areaMain.getWmovAreaMovimento().getLccvmov1().getDati().setWmovDsTsCptz(ws.getMovi().getMovDsTsCptz());
        // COB_CODE: MOVE MOV-DS-UTENTE
        //             TO (SF)-DS-UTENTE
        areaMain.getWmovAreaMovimento().getLccvmov1().getDati().setWmovDsUtente(ws.getMovi().getMovDsUtente());
        // COB_CODE: MOVE MOV-DS-STATO-ELAB
        //             TO (SF)-DS-STATO-ELAB.
        areaMain.getWmovAreaMovimento().getLccvmov1().getDati().setWmovDsStatoElab(ws.getMovi().getMovDsStatoElab());
    }

    /**Original name: VALORIZZA-OUTPUT-POL<br>
	 * <pre>  --> POLIZZA
	 * ------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    VALORIZZA OUTPUT COPY LCCVPOL3
	 *    ULTIMO AGG. 07 DIC 2017
	 * ------------------------------------------------------------</pre>*/
    private void valorizzaOutputPol() {
        // COB_CODE: MOVE POL-ID-POLI
        //             TO (SF)-ID-PTF
        areaMain.getWpolAreaPolizza().getLccvpol1().setIdPtf(ws.getPoli().getPolIdPoli());
        // COB_CODE: MOVE POL-ID-POLI
        //             TO (SF)-ID-POLI
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolIdPoli(ws.getPoli().getPolIdPoli());
        // COB_CODE: MOVE POL-ID-MOVI-CRZ
        //             TO (SF)-ID-MOVI-CRZ
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolIdMoviCrz(ws.getPoli().getPolIdMoviCrz());
        // COB_CODE: IF POL-ID-MOVI-CHIU-NULL = HIGH-VALUES
        //                TO (SF)-ID-MOVI-CHIU-NULL
        //           ELSE
        //                TO (SF)-ID-MOVI-CHIU
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getPoli().getPolIdMoviChiu().getPolIdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE POL-ID-MOVI-CHIU-NULL
            //             TO (SF)-ID-MOVI-CHIU-NULL
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolIdMoviChiu().setWpolIdMoviChiuNull(ws.getPoli().getPolIdMoviChiu().getPolIdMoviChiuNull());
        }
        else {
            // COB_CODE: MOVE POL-ID-MOVI-CHIU
            //             TO (SF)-ID-MOVI-CHIU
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolIdMoviChiu().setWpolIdMoviChiu(ws.getPoli().getPolIdMoviChiu().getPolIdMoviChiu());
        }
        // COB_CODE: IF POL-IB-OGG-NULL = HIGH-VALUES
        //                TO (SF)-IB-OGG-NULL
        //           ELSE
        //                TO (SF)-IB-OGG
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getPoli().getPolIbOgg(), Poli.Len.POL_IB_OGG)) {
            // COB_CODE: MOVE POL-IB-OGG-NULL
            //             TO (SF)-IB-OGG-NULL
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolIbOgg(ws.getPoli().getPolIbOgg());
        }
        else {
            // COB_CODE: MOVE POL-IB-OGG
            //             TO (SF)-IB-OGG
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolIbOgg(ws.getPoli().getPolIbOgg());
        }
        // COB_CODE: MOVE POL-IB-PROP
        //             TO (SF)-IB-PROP
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolIbProp(ws.getPoli().getPolIbProp());
        // COB_CODE: IF POL-DT-PROP-NULL = HIGH-VALUES
        //                TO (SF)-DT-PROP-NULL
        //           ELSE
        //                TO (SF)-DT-PROP
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getPoli().getPolDtProp().getPolDtPropNullFormatted())) {
            // COB_CODE: MOVE POL-DT-PROP-NULL
            //             TO (SF)-DT-PROP-NULL
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolDtProp().setWpolDtPropNull(ws.getPoli().getPolDtProp().getPolDtPropNull());
        }
        else {
            // COB_CODE: MOVE POL-DT-PROP
            //             TO (SF)-DT-PROP
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolDtProp().setWpolDtProp(ws.getPoli().getPolDtProp().getPolDtProp());
        }
        // COB_CODE: MOVE POL-DT-INI-EFF
        //             TO (SF)-DT-INI-EFF
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolDtIniEff(ws.getPoli().getPolDtIniEff());
        // COB_CODE: MOVE POL-DT-END-EFF
        //             TO (SF)-DT-END-EFF
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolDtEndEff(ws.getPoli().getPolDtEndEff());
        // COB_CODE: MOVE POL-COD-COMP-ANIA
        //             TO (SF)-COD-COMP-ANIA
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolCodCompAnia(ws.getPoli().getPolCodCompAnia());
        // COB_CODE: MOVE POL-DT-DECOR
        //             TO (SF)-DT-DECOR
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolDtDecor(ws.getPoli().getPolDtDecor());
        // COB_CODE: MOVE POL-DT-EMIS
        //             TO (SF)-DT-EMIS
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolDtEmis(ws.getPoli().getPolDtEmis());
        // COB_CODE: MOVE POL-TP-POLI
        //             TO (SF)-TP-POLI
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolTpPoli(ws.getPoli().getPolTpPoli());
        // COB_CODE: IF POL-DUR-AA-NULL = HIGH-VALUES
        //                TO (SF)-DUR-AA-NULL
        //           ELSE
        //                TO (SF)-DUR-AA
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getPoli().getPolDurAa().getPolDurAaNullFormatted())) {
            // COB_CODE: MOVE POL-DUR-AA-NULL
            //             TO (SF)-DUR-AA-NULL
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolDurAa().setWpolDurAaNull(ws.getPoli().getPolDurAa().getPolDurAaNull());
        }
        else {
            // COB_CODE: MOVE POL-DUR-AA
            //             TO (SF)-DUR-AA
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolDurAa().setWpolDurAa(ws.getPoli().getPolDurAa().getPolDurAa());
        }
        // COB_CODE: IF POL-DUR-MM-NULL = HIGH-VALUES
        //                TO (SF)-DUR-MM-NULL
        //           ELSE
        //                TO (SF)-DUR-MM
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getPoli().getPolDurMm().getPolDurMmNullFormatted())) {
            // COB_CODE: MOVE POL-DUR-MM-NULL
            //             TO (SF)-DUR-MM-NULL
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolDurMm().setWpolDurMmNull(ws.getPoli().getPolDurMm().getPolDurMmNull());
        }
        else {
            // COB_CODE: MOVE POL-DUR-MM
            //             TO (SF)-DUR-MM
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolDurMm().setWpolDurMm(ws.getPoli().getPolDurMm().getPolDurMm());
        }
        // COB_CODE: IF POL-DT-SCAD-NULL = HIGH-VALUES
        //                TO (SF)-DT-SCAD-NULL
        //           ELSE
        //                TO (SF)-DT-SCAD
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getPoli().getPolDtScad().getPolDtScadNullFormatted())) {
            // COB_CODE: MOVE POL-DT-SCAD-NULL
            //             TO (SF)-DT-SCAD-NULL
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolDtScad().setWpolDtScadNull(ws.getPoli().getPolDtScad().getPolDtScadNull());
        }
        else {
            // COB_CODE: MOVE POL-DT-SCAD
            //             TO (SF)-DT-SCAD
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolDtScad().setWpolDtScad(ws.getPoli().getPolDtScad().getPolDtScad());
        }
        // COB_CODE: MOVE POL-COD-PROD
        //             TO (SF)-COD-PROD
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolCodProd(ws.getPoli().getPolCodProd());
        // COB_CODE: MOVE POL-DT-INI-VLDT-PROD
        //             TO (SF)-DT-INI-VLDT-PROD
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolDtIniVldtProd(ws.getPoli().getPolDtIniVldtProd());
        // COB_CODE: IF POL-COD-CONV-NULL = HIGH-VALUES
        //                TO (SF)-COD-CONV-NULL
        //           ELSE
        //                TO (SF)-COD-CONV
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getPoli().getPolCodConvFormatted())) {
            // COB_CODE: MOVE POL-COD-CONV-NULL
            //             TO (SF)-COD-CONV-NULL
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolCodConv(ws.getPoli().getPolCodConv());
        }
        else {
            // COB_CODE: MOVE POL-COD-CONV
            //             TO (SF)-COD-CONV
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolCodConv(ws.getPoli().getPolCodConv());
        }
        // COB_CODE: IF POL-COD-RAMO-NULL = HIGH-VALUES
        //                TO (SF)-COD-RAMO-NULL
        //           ELSE
        //                TO (SF)-COD-RAMO
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getPoli().getPolCodRamoFormatted())) {
            // COB_CODE: MOVE POL-COD-RAMO-NULL
            //             TO (SF)-COD-RAMO-NULL
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolCodRamo(ws.getPoli().getPolCodRamo());
        }
        else {
            // COB_CODE: MOVE POL-COD-RAMO
            //             TO (SF)-COD-RAMO
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolCodRamo(ws.getPoli().getPolCodRamo());
        }
        // COB_CODE: IF POL-DT-INI-VLDT-CONV-NULL = HIGH-VALUES
        //                TO (SF)-DT-INI-VLDT-CONV-NULL
        //           ELSE
        //                TO (SF)-DT-INI-VLDT-CONV
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getPoli().getPolDtIniVldtConv().getPolDtIniVldtConvNullFormatted())) {
            // COB_CODE: MOVE POL-DT-INI-VLDT-CONV-NULL
            //             TO (SF)-DT-INI-VLDT-CONV-NULL
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolDtIniVldtConv().setWpolDtIniVldtConvNull(ws.getPoli().getPolDtIniVldtConv().getPolDtIniVldtConvNull());
        }
        else {
            // COB_CODE: MOVE POL-DT-INI-VLDT-CONV
            //             TO (SF)-DT-INI-VLDT-CONV
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolDtIniVldtConv().setWpolDtIniVldtConv(ws.getPoli().getPolDtIniVldtConv().getPolDtIniVldtConv());
        }
        // COB_CODE: IF POL-DT-APPLZ-CONV-NULL = HIGH-VALUES
        //                TO (SF)-DT-APPLZ-CONV-NULL
        //           ELSE
        //                TO (SF)-DT-APPLZ-CONV
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getPoli().getPolDtApplzConv().getPolDtApplzConvNullFormatted())) {
            // COB_CODE: MOVE POL-DT-APPLZ-CONV-NULL
            //             TO (SF)-DT-APPLZ-CONV-NULL
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolDtApplzConv().setWpolDtApplzConvNull(ws.getPoli().getPolDtApplzConv().getPolDtApplzConvNull());
        }
        else {
            // COB_CODE: MOVE POL-DT-APPLZ-CONV
            //             TO (SF)-DT-APPLZ-CONV
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolDtApplzConv().setWpolDtApplzConv(ws.getPoli().getPolDtApplzConv().getPolDtApplzConv());
        }
        // COB_CODE: MOVE POL-TP-FRM-ASSVA
        //             TO (SF)-TP-FRM-ASSVA
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolTpFrmAssva(ws.getPoli().getPolTpFrmAssva());
        // COB_CODE: IF POL-TP-RGM-FISC-NULL = HIGH-VALUES
        //                TO (SF)-TP-RGM-FISC-NULL
        //           ELSE
        //                TO (SF)-TP-RGM-FISC
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getPoli().getPolTpRgmFiscFormatted())) {
            // COB_CODE: MOVE POL-TP-RGM-FISC-NULL
            //             TO (SF)-TP-RGM-FISC-NULL
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolTpRgmFisc(ws.getPoli().getPolTpRgmFisc());
        }
        else {
            // COB_CODE: MOVE POL-TP-RGM-FISC
            //             TO (SF)-TP-RGM-FISC
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolTpRgmFisc(ws.getPoli().getPolTpRgmFisc());
        }
        // COB_CODE: IF POL-FL-ESTAS-NULL = HIGH-VALUES
        //                TO (SF)-FL-ESTAS-NULL
        //           ELSE
        //                TO (SF)-FL-ESTAS
        //           END-IF
        if (Conditions.eq(ws.getPoli().getPolFlEstas(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE POL-FL-ESTAS-NULL
            //             TO (SF)-FL-ESTAS-NULL
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlEstas(ws.getPoli().getPolFlEstas());
        }
        else {
            // COB_CODE: MOVE POL-FL-ESTAS
            //             TO (SF)-FL-ESTAS
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlEstas(ws.getPoli().getPolFlEstas());
        }
        // COB_CODE: IF POL-FL-RSH-COMUN-NULL = HIGH-VALUES
        //                TO (SF)-FL-RSH-COMUN-NULL
        //           ELSE
        //                TO (SF)-FL-RSH-COMUN
        //           END-IF
        if (Conditions.eq(ws.getPoli().getPolFlRshComun(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE POL-FL-RSH-COMUN-NULL
            //             TO (SF)-FL-RSH-COMUN-NULL
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlRshComun(ws.getPoli().getPolFlRshComun());
        }
        else {
            // COB_CODE: MOVE POL-FL-RSH-COMUN
            //             TO (SF)-FL-RSH-COMUN
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlRshComun(ws.getPoli().getPolFlRshComun());
        }
        // COB_CODE: IF POL-FL-RSH-COMUN-COND-NULL = HIGH-VALUES
        //                TO (SF)-FL-RSH-COMUN-COND-NULL
        //           ELSE
        //                TO (SF)-FL-RSH-COMUN-COND
        //           END-IF
        if (Conditions.eq(ws.getPoli().getPolFlRshComunCond(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE POL-FL-RSH-COMUN-COND-NULL
            //             TO (SF)-FL-RSH-COMUN-COND-NULL
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlRshComunCond(ws.getPoli().getPolFlRshComunCond());
        }
        else {
            // COB_CODE: MOVE POL-FL-RSH-COMUN-COND
            //             TO (SF)-FL-RSH-COMUN-COND
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlRshComunCond(ws.getPoli().getPolFlRshComunCond());
        }
        // COB_CODE: MOVE POL-TP-LIV-GENZ-TIT
        //             TO (SF)-TP-LIV-GENZ-TIT
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolTpLivGenzTit(ws.getPoli().getPolTpLivGenzTit());
        // COB_CODE: IF POL-FL-COP-FINANZ-NULL = HIGH-VALUES
        //                TO (SF)-FL-COP-FINANZ-NULL
        //           ELSE
        //                TO (SF)-FL-COP-FINANZ
        //           END-IF
        if (Conditions.eq(ws.getPoli().getPolFlCopFinanz(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE POL-FL-COP-FINANZ-NULL
            //             TO (SF)-FL-COP-FINANZ-NULL
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlCopFinanz(ws.getPoli().getPolFlCopFinanz());
        }
        else {
            // COB_CODE: MOVE POL-FL-COP-FINANZ
            //             TO (SF)-FL-COP-FINANZ
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlCopFinanz(ws.getPoli().getPolFlCopFinanz());
        }
        // COB_CODE: IF POL-TP-APPLZ-DIR-NULL = HIGH-VALUES
        //                TO (SF)-TP-APPLZ-DIR-NULL
        //           ELSE
        //                TO (SF)-TP-APPLZ-DIR
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getPoli().getPolTpApplzDirFormatted())) {
            // COB_CODE: MOVE POL-TP-APPLZ-DIR-NULL
            //             TO (SF)-TP-APPLZ-DIR-NULL
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolTpApplzDir(ws.getPoli().getPolTpApplzDir());
        }
        else {
            // COB_CODE: MOVE POL-TP-APPLZ-DIR
            //             TO (SF)-TP-APPLZ-DIR
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolTpApplzDir(ws.getPoli().getPolTpApplzDir());
        }
        // COB_CODE: IF POL-SPE-MED-NULL = HIGH-VALUES
        //                TO (SF)-SPE-MED-NULL
        //           ELSE
        //                TO (SF)-SPE-MED
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getPoli().getPolSpeMed().getPolSpeMedNullFormatted())) {
            // COB_CODE: MOVE POL-SPE-MED-NULL
            //             TO (SF)-SPE-MED-NULL
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolSpeMed().setWpolSpeMedNull(ws.getPoli().getPolSpeMed().getPolSpeMedNull());
        }
        else {
            // COB_CODE: MOVE POL-SPE-MED
            //             TO (SF)-SPE-MED
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolSpeMed().setWpolSpeMed(Trunc.toDecimal(ws.getPoli().getPolSpeMed().getPolSpeMed(), 15, 3));
        }
        // COB_CODE: IF POL-DIR-EMIS-NULL = HIGH-VALUES
        //                TO (SF)-DIR-EMIS-NULL
        //           ELSE
        //                TO (SF)-DIR-EMIS
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getPoli().getPolDirEmis().getPolDirEmisNullFormatted())) {
            // COB_CODE: MOVE POL-DIR-EMIS-NULL
            //             TO (SF)-DIR-EMIS-NULL
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolDirEmis().setWpolDirEmisNull(ws.getPoli().getPolDirEmis().getPolDirEmisNull());
        }
        else {
            // COB_CODE: MOVE POL-DIR-EMIS
            //             TO (SF)-DIR-EMIS
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolDirEmis().setWpolDirEmis(Trunc.toDecimal(ws.getPoli().getPolDirEmis().getPolDirEmis(), 15, 3));
        }
        // COB_CODE: IF POL-DIR-1O-VERS-NULL = HIGH-VALUES
        //                TO (SF)-DIR-1O-VERS-NULL
        //           ELSE
        //                TO (SF)-DIR-1O-VERS
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getPoli().getPolDir1oVers().getPolDir1oVersNullFormatted())) {
            // COB_CODE: MOVE POL-DIR-1O-VERS-NULL
            //             TO (SF)-DIR-1O-VERS-NULL
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolDir1oVers().setWpolDir1oVersNull(ws.getPoli().getPolDir1oVers().getPolDir1oVersNull());
        }
        else {
            // COB_CODE: MOVE POL-DIR-1O-VERS
            //             TO (SF)-DIR-1O-VERS
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolDir1oVers().setWpolDir1oVers(Trunc.toDecimal(ws.getPoli().getPolDir1oVers().getPolDir1oVers(), 15, 3));
        }
        // COB_CODE: IF POL-DIR-VERS-AGG-NULL = HIGH-VALUES
        //                TO (SF)-DIR-VERS-AGG-NULL
        //           ELSE
        //                TO (SF)-DIR-VERS-AGG
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getPoli().getPolDirVersAgg().getPolDirVersAggNullFormatted())) {
            // COB_CODE: MOVE POL-DIR-VERS-AGG-NULL
            //             TO (SF)-DIR-VERS-AGG-NULL
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolDirVersAgg().setWpolDirVersAggNull(ws.getPoli().getPolDirVersAgg().getPolDirVersAggNull());
        }
        else {
            // COB_CODE: MOVE POL-DIR-VERS-AGG
            //             TO (SF)-DIR-VERS-AGG
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolDirVersAgg().setWpolDirVersAgg(Trunc.toDecimal(ws.getPoli().getPolDirVersAgg().getPolDirVersAgg(), 15, 3));
        }
        // COB_CODE: IF POL-COD-DVS-NULL = HIGH-VALUES
        //                TO (SF)-COD-DVS-NULL
        //           ELSE
        //                TO (SF)-COD-DVS
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getPoli().getPolCodDvsFormatted())) {
            // COB_CODE: MOVE POL-COD-DVS-NULL
            //             TO (SF)-COD-DVS-NULL
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolCodDvs(ws.getPoli().getPolCodDvs());
        }
        else {
            // COB_CODE: MOVE POL-COD-DVS
            //             TO (SF)-COD-DVS
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolCodDvs(ws.getPoli().getPolCodDvs());
        }
        // COB_CODE: IF POL-FL-FNT-AZ-NULL = HIGH-VALUES
        //                TO (SF)-FL-FNT-AZ-NULL
        //           ELSE
        //                TO (SF)-FL-FNT-AZ
        //           END-IF
        if (Conditions.eq(ws.getPoli().getPolFlFntAz(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE POL-FL-FNT-AZ-NULL
            //             TO (SF)-FL-FNT-AZ-NULL
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlFntAz(ws.getPoli().getPolFlFntAz());
        }
        else {
            // COB_CODE: MOVE POL-FL-FNT-AZ
            //             TO (SF)-FL-FNT-AZ
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlFntAz(ws.getPoli().getPolFlFntAz());
        }
        // COB_CODE: IF POL-FL-FNT-ADER-NULL = HIGH-VALUES
        //                TO (SF)-FL-FNT-ADER-NULL
        //           ELSE
        //                TO (SF)-FL-FNT-ADER
        //           END-IF
        if (Conditions.eq(ws.getPoli().getPolFlFntAder(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE POL-FL-FNT-ADER-NULL
            //             TO (SF)-FL-FNT-ADER-NULL
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlFntAder(ws.getPoli().getPolFlFntAder());
        }
        else {
            // COB_CODE: MOVE POL-FL-FNT-ADER
            //             TO (SF)-FL-FNT-ADER
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlFntAder(ws.getPoli().getPolFlFntAder());
        }
        // COB_CODE: IF POL-FL-FNT-TFR-NULL = HIGH-VALUES
        //                TO (SF)-FL-FNT-TFR-NULL
        //           ELSE
        //                TO (SF)-FL-FNT-TFR
        //           END-IF
        if (Conditions.eq(ws.getPoli().getPolFlFntTfr(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE POL-FL-FNT-TFR-NULL
            //             TO (SF)-FL-FNT-TFR-NULL
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlFntTfr(ws.getPoli().getPolFlFntTfr());
        }
        else {
            // COB_CODE: MOVE POL-FL-FNT-TFR
            //             TO (SF)-FL-FNT-TFR
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlFntTfr(ws.getPoli().getPolFlFntTfr());
        }
        // COB_CODE: IF POL-FL-FNT-VOLO-NULL = HIGH-VALUES
        //                TO (SF)-FL-FNT-VOLO-NULL
        //           ELSE
        //                TO (SF)-FL-FNT-VOLO
        //           END-IF
        if (Conditions.eq(ws.getPoli().getPolFlFntVolo(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE POL-FL-FNT-VOLO-NULL
            //             TO (SF)-FL-FNT-VOLO-NULL
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlFntVolo(ws.getPoli().getPolFlFntVolo());
        }
        else {
            // COB_CODE: MOVE POL-FL-FNT-VOLO
            //             TO (SF)-FL-FNT-VOLO
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlFntVolo(ws.getPoli().getPolFlFntVolo());
        }
        // COB_CODE: IF POL-TP-OPZ-A-SCAD-NULL = HIGH-VALUES
        //                TO (SF)-TP-OPZ-A-SCAD-NULL
        //           ELSE
        //                TO (SF)-TP-OPZ-A-SCAD
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getPoli().getPolTpOpzAScadFormatted())) {
            // COB_CODE: MOVE POL-TP-OPZ-A-SCAD-NULL
            //             TO (SF)-TP-OPZ-A-SCAD-NULL
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolTpOpzAScad(ws.getPoli().getPolTpOpzAScad());
        }
        else {
            // COB_CODE: MOVE POL-TP-OPZ-A-SCAD
            //             TO (SF)-TP-OPZ-A-SCAD
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolTpOpzAScad(ws.getPoli().getPolTpOpzAScad());
        }
        // COB_CODE: IF POL-AA-DIFF-PROR-DFLT-NULL = HIGH-VALUES
        //                TO (SF)-AA-DIFF-PROR-DFLT-NULL
        //           ELSE
        //                TO (SF)-AA-DIFF-PROR-DFLT
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getPoli().getPolAaDiffProrDflt().getPolAaDiffProrDfltNullFormatted())) {
            // COB_CODE: MOVE POL-AA-DIFF-PROR-DFLT-NULL
            //             TO (SF)-AA-DIFF-PROR-DFLT-NULL
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolAaDiffProrDflt().setWpolAaDiffProrDfltNull(ws.getPoli().getPolAaDiffProrDflt().getPolAaDiffProrDfltNull());
        }
        else {
            // COB_CODE: MOVE POL-AA-DIFF-PROR-DFLT
            //             TO (SF)-AA-DIFF-PROR-DFLT
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolAaDiffProrDflt().setWpolAaDiffProrDflt(ws.getPoli().getPolAaDiffProrDflt().getPolAaDiffProrDflt());
        }
        // COB_CODE: IF POL-FL-VER-PROD-NULL = HIGH-VALUES
        //                TO (SF)-FL-VER-PROD-NULL
        //           ELSE
        //                TO (SF)-FL-VER-PROD
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getPoli().getPolFlVerProdFormatted())) {
            // COB_CODE: MOVE POL-FL-VER-PROD-NULL
            //             TO (SF)-FL-VER-PROD-NULL
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlVerProd(ws.getPoli().getPolFlVerProd());
        }
        else {
            // COB_CODE: MOVE POL-FL-VER-PROD
            //             TO (SF)-FL-VER-PROD
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlVerProd(ws.getPoli().getPolFlVerProd());
        }
        // COB_CODE: IF POL-DUR-GG-NULL = HIGH-VALUES
        //                TO (SF)-DUR-GG-NULL
        //           ELSE
        //                TO (SF)-DUR-GG
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getPoli().getPolDurGg().getPolDurGgNullFormatted())) {
            // COB_CODE: MOVE POL-DUR-GG-NULL
            //             TO (SF)-DUR-GG-NULL
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolDurGg().setWpolDurGgNull(ws.getPoli().getPolDurGg().getPolDurGgNull());
        }
        else {
            // COB_CODE: MOVE POL-DUR-GG
            //             TO (SF)-DUR-GG
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolDurGg().setWpolDurGg(ws.getPoli().getPolDurGg().getPolDurGg());
        }
        // COB_CODE: IF POL-DIR-QUIET-NULL = HIGH-VALUES
        //                TO (SF)-DIR-QUIET-NULL
        //           ELSE
        //                TO (SF)-DIR-QUIET
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getPoli().getPolDirQuiet().getPolDirQuietNullFormatted())) {
            // COB_CODE: MOVE POL-DIR-QUIET-NULL
            //             TO (SF)-DIR-QUIET-NULL
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolDirQuiet().setWpolDirQuietNull(ws.getPoli().getPolDirQuiet().getPolDirQuietNull());
        }
        else {
            // COB_CODE: MOVE POL-DIR-QUIET
            //             TO (SF)-DIR-QUIET
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolDirQuiet().setWpolDirQuiet(Trunc.toDecimal(ws.getPoli().getPolDirQuiet().getPolDirQuiet(), 15, 3));
        }
        // COB_CODE: IF POL-TP-PTF-ESTNO-NULL = HIGH-VALUES
        //                TO (SF)-TP-PTF-ESTNO-NULL
        //           ELSE
        //                TO (SF)-TP-PTF-ESTNO
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getPoli().getPolTpPtfEstnoFormatted())) {
            // COB_CODE: MOVE POL-TP-PTF-ESTNO-NULL
            //             TO (SF)-TP-PTF-ESTNO-NULL
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolTpPtfEstno(ws.getPoli().getPolTpPtfEstno());
        }
        else {
            // COB_CODE: MOVE POL-TP-PTF-ESTNO
            //             TO (SF)-TP-PTF-ESTNO
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolTpPtfEstno(ws.getPoli().getPolTpPtfEstno());
        }
        // COB_CODE: IF POL-FL-CUM-PRE-CNTR-NULL = HIGH-VALUES
        //                TO (SF)-FL-CUM-PRE-CNTR-NULL
        //           ELSE
        //                TO (SF)-FL-CUM-PRE-CNTR
        //           END-IF
        if (Conditions.eq(ws.getPoli().getPolFlCumPreCntr(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE POL-FL-CUM-PRE-CNTR-NULL
            //             TO (SF)-FL-CUM-PRE-CNTR-NULL
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlCumPreCntr(ws.getPoli().getPolFlCumPreCntr());
        }
        else {
            // COB_CODE: MOVE POL-FL-CUM-PRE-CNTR
            //             TO (SF)-FL-CUM-PRE-CNTR
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlCumPreCntr(ws.getPoli().getPolFlCumPreCntr());
        }
        // COB_CODE: IF POL-FL-AMMB-MOVI-NULL = HIGH-VALUES
        //                TO (SF)-FL-AMMB-MOVI-NULL
        //           ELSE
        //                TO (SF)-FL-AMMB-MOVI
        //           END-IF
        if (Conditions.eq(ws.getPoli().getPolFlAmmbMovi(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE POL-FL-AMMB-MOVI-NULL
            //             TO (SF)-FL-AMMB-MOVI-NULL
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlAmmbMovi(ws.getPoli().getPolFlAmmbMovi());
        }
        else {
            // COB_CODE: MOVE POL-FL-AMMB-MOVI
            //             TO (SF)-FL-AMMB-MOVI
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlAmmbMovi(ws.getPoli().getPolFlAmmbMovi());
        }
        // COB_CODE: IF POL-CONV-GECO-NULL = HIGH-VALUES
        //                TO (SF)-CONV-GECO-NULL
        //           ELSE
        //                TO (SF)-CONV-GECO
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getPoli().getPolConvGecoFormatted())) {
            // COB_CODE: MOVE POL-CONV-GECO-NULL
            //             TO (SF)-CONV-GECO-NULL
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolConvGeco(ws.getPoli().getPolConvGeco());
        }
        else {
            // COB_CODE: MOVE POL-CONV-GECO
            //             TO (SF)-CONV-GECO
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolConvGeco(ws.getPoli().getPolConvGeco());
        }
        // COB_CODE: MOVE POL-DS-RIGA
        //             TO (SF)-DS-RIGA
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolDsRiga(ws.getPoli().getPolDsRiga());
        // COB_CODE: MOVE POL-DS-OPER-SQL
        //             TO (SF)-DS-OPER-SQL
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolDsOperSql(ws.getPoli().getPolDsOperSql());
        // COB_CODE: MOVE POL-DS-VER
        //             TO (SF)-DS-VER
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolDsVer(ws.getPoli().getPolDsVer());
        // COB_CODE: MOVE POL-DS-TS-INI-CPTZ
        //             TO (SF)-DS-TS-INI-CPTZ
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolDsTsIniCptz(ws.getPoli().getPolDsTsIniCptz());
        // COB_CODE: MOVE POL-DS-TS-END-CPTZ
        //             TO (SF)-DS-TS-END-CPTZ
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolDsTsEndCptz(ws.getPoli().getPolDsTsEndCptz());
        // COB_CODE: MOVE POL-DS-UTENTE
        //             TO (SF)-DS-UTENTE
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolDsUtente(ws.getPoli().getPolDsUtente());
        // COB_CODE: MOVE POL-DS-STATO-ELAB
        //             TO (SF)-DS-STATO-ELAB
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolDsStatoElab(ws.getPoli().getPolDsStatoElab());
        // COB_CODE: IF POL-FL-SCUDO-FISC-NULL = HIGH-VALUES
        //                TO (SF)-FL-SCUDO-FISC-NULL
        //           ELSE
        //                TO (SF)-FL-SCUDO-FISC
        //           END-IF
        if (Conditions.eq(ws.getPoli().getPolFlScudoFisc(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE POL-FL-SCUDO-FISC-NULL
            //             TO (SF)-FL-SCUDO-FISC-NULL
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlScudoFisc(ws.getPoli().getPolFlScudoFisc());
        }
        else {
            // COB_CODE: MOVE POL-FL-SCUDO-FISC
            //             TO (SF)-FL-SCUDO-FISC
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlScudoFisc(ws.getPoli().getPolFlScudoFisc());
        }
        // COB_CODE: IF POL-FL-TRASFE-NULL = HIGH-VALUES
        //                TO (SF)-FL-TRASFE-NULL
        //           ELSE
        //                TO (SF)-FL-TRASFE
        //           END-IF
        if (Conditions.eq(ws.getPoli().getPolFlTrasfe(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE POL-FL-TRASFE-NULL
            //             TO (SF)-FL-TRASFE-NULL
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlTrasfe(ws.getPoli().getPolFlTrasfe());
        }
        else {
            // COB_CODE: MOVE POL-FL-TRASFE
            //             TO (SF)-FL-TRASFE
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlTrasfe(ws.getPoli().getPolFlTrasfe());
        }
        // COB_CODE: IF POL-FL-TFR-STRC-NULL = HIGH-VALUES
        //                TO (SF)-FL-TFR-STRC-NULL
        //           ELSE
        //                TO (SF)-FL-TFR-STRC
        //           END-IF
        if (Conditions.eq(ws.getPoli().getPolFlTfrStrc(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE POL-FL-TFR-STRC-NULL
            //             TO (SF)-FL-TFR-STRC-NULL
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlTfrStrc(ws.getPoli().getPolFlTfrStrc());
        }
        else {
            // COB_CODE: MOVE POL-FL-TFR-STRC
            //             TO (SF)-FL-TFR-STRC
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlTfrStrc(ws.getPoli().getPolFlTfrStrc());
        }
        // COB_CODE: IF POL-DT-PRESC-NULL = HIGH-VALUES
        //                TO (SF)-DT-PRESC-NULL
        //           ELSE
        //                TO (SF)-DT-PRESC
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getPoli().getPolDtPresc().getPolDtPrescNullFormatted())) {
            // COB_CODE: MOVE POL-DT-PRESC-NULL
            //             TO (SF)-DT-PRESC-NULL
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolDtPresc().setWpolDtPrescNull(ws.getPoli().getPolDtPresc().getPolDtPrescNull());
        }
        else {
            // COB_CODE: MOVE POL-DT-PRESC
            //             TO (SF)-DT-PRESC
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolDtPresc().setWpolDtPresc(ws.getPoli().getPolDtPresc().getPolDtPresc());
        }
        // COB_CODE: IF POL-COD-CONV-AGG-NULL = HIGH-VALUES
        //                TO (SF)-COD-CONV-AGG-NULL
        //           ELSE
        //                TO (SF)-COD-CONV-AGG
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getPoli().getPolCodConvAggFormatted())) {
            // COB_CODE: MOVE POL-COD-CONV-AGG-NULL
            //             TO (SF)-COD-CONV-AGG-NULL
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolCodConvAgg(ws.getPoli().getPolCodConvAgg());
        }
        else {
            // COB_CODE: MOVE POL-COD-CONV-AGG
            //             TO (SF)-COD-CONV-AGG
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolCodConvAgg(ws.getPoli().getPolCodConvAgg());
        }
        // COB_CODE: IF POL-SUBCAT-PROD-NULL = HIGH-VALUES
        //                TO (SF)-SUBCAT-PROD-NULL
        //           ELSE
        //                TO (SF)-SUBCAT-PROD
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getPoli().getPolSubcatProdFormatted())) {
            // COB_CODE: MOVE POL-SUBCAT-PROD-NULL
            //             TO (SF)-SUBCAT-PROD-NULL
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolSubcatProd(ws.getPoli().getPolSubcatProd());
        }
        else {
            // COB_CODE: MOVE POL-SUBCAT-PROD
            //             TO (SF)-SUBCAT-PROD
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolSubcatProd(ws.getPoli().getPolSubcatProd());
        }
        // COB_CODE: IF POL-FL-QUEST-ADEGZ-ASS-NULL = HIGH-VALUES
        //                TO (SF)-FL-QUEST-ADEGZ-ASS-NULL
        //           ELSE
        //                TO (SF)-FL-QUEST-ADEGZ-ASS
        //           END-IF
        if (Conditions.eq(ws.getPoli().getPolFlQuestAdegzAss(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE POL-FL-QUEST-ADEGZ-ASS-NULL
            //             TO (SF)-FL-QUEST-ADEGZ-ASS-NULL
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlQuestAdegzAss(ws.getPoli().getPolFlQuestAdegzAss());
        }
        else {
            // COB_CODE: MOVE POL-FL-QUEST-ADEGZ-ASS
            //             TO (SF)-FL-QUEST-ADEGZ-ASS
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlQuestAdegzAss(ws.getPoli().getPolFlQuestAdegzAss());
        }
        // COB_CODE: IF POL-COD-TPA-NULL = HIGH-VALUES
        //                TO (SF)-COD-TPA-NULL
        //           ELSE
        //                TO (SF)-COD-TPA
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getPoli().getPolCodTpaFormatted())) {
            // COB_CODE: MOVE POL-COD-TPA-NULL
            //             TO (SF)-COD-TPA-NULL
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolCodTpa(ws.getPoli().getPolCodTpa());
        }
        else {
            // COB_CODE: MOVE POL-COD-TPA
            //             TO (SF)-COD-TPA
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolCodTpa(ws.getPoli().getPolCodTpa());
        }
        // COB_CODE: IF POL-ID-ACC-COMM-NULL = HIGH-VALUES
        //                TO (SF)-ID-ACC-COMM-NULL
        //           ELSE
        //                TO (SF)-ID-ACC-COMM
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getPoli().getPolIdAccComm().getPolIdAccCommNullFormatted())) {
            // COB_CODE: MOVE POL-ID-ACC-COMM-NULL
            //             TO (SF)-ID-ACC-COMM-NULL
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolIdAccComm().setWpolIdAccCommNull(ws.getPoli().getPolIdAccComm().getPolIdAccCommNull());
        }
        else {
            // COB_CODE: MOVE POL-ID-ACC-COMM
            //             TO (SF)-ID-ACC-COMM
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolIdAccComm().setWpolIdAccComm(ws.getPoli().getPolIdAccComm().getPolIdAccComm());
        }
        // COB_CODE: IF POL-FL-POLI-CPI-PR-NULL = HIGH-VALUES
        //                TO (SF)-FL-POLI-CPI-PR-NULL
        //           ELSE
        //                TO (SF)-FL-POLI-CPI-PR
        //           END-IF
        if (Conditions.eq(ws.getPoli().getPolFlPoliCpiPr(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE POL-FL-POLI-CPI-PR-NULL
            //             TO (SF)-FL-POLI-CPI-PR-NULL
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlPoliCpiPr(ws.getPoli().getPolFlPoliCpiPr());
        }
        else {
            // COB_CODE: MOVE POL-FL-POLI-CPI-PR
            //             TO (SF)-FL-POLI-CPI-PR
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlPoliCpiPr(ws.getPoli().getPolFlPoliCpiPr());
        }
        // COB_CODE: IF POL-FL-POLI-BUNDLING-NULL = HIGH-VALUES
        //                TO (SF)-FL-POLI-BUNDLING-NULL
        //           ELSE
        //                TO (SF)-FL-POLI-BUNDLING
        //           END-IF
        if (Conditions.eq(ws.getPoli().getPolFlPoliBundling(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE POL-FL-POLI-BUNDLING-NULL
            //             TO (SF)-FL-POLI-BUNDLING-NULL
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlPoliBundling(ws.getPoli().getPolFlPoliBundling());
        }
        else {
            // COB_CODE: MOVE POL-FL-POLI-BUNDLING
            //             TO (SF)-FL-POLI-BUNDLING
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlPoliBundling(ws.getPoli().getPolFlPoliBundling());
        }
        // COB_CODE: IF POL-IND-POLI-PRIN-COLL-NULL = HIGH-VALUES
        //                TO (SF)-IND-POLI-PRIN-COLL-NULL
        //           ELSE
        //                TO (SF)-IND-POLI-PRIN-COLL
        //           END-IF
        if (Conditions.eq(ws.getPoli().getPolIndPoliPrinColl(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE POL-IND-POLI-PRIN-COLL-NULL
            //             TO (SF)-IND-POLI-PRIN-COLL-NULL
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolIndPoliPrinColl(ws.getPoli().getPolIndPoliPrinColl());
        }
        else {
            // COB_CODE: MOVE POL-IND-POLI-PRIN-COLL
            //             TO (SF)-IND-POLI-PRIN-COLL
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolIndPoliPrinColl(ws.getPoli().getPolIndPoliPrinColl());
        }
        // COB_CODE: IF POL-FL-VND-BUNDLE-NULL = HIGH-VALUES
        //                TO (SF)-FL-VND-BUNDLE-NULL
        //           ELSE
        //                TO (SF)-FL-VND-BUNDLE
        //           END-IF
        if (Conditions.eq(ws.getPoli().getPolFlVndBundle(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE POL-FL-VND-BUNDLE-NULL
            //             TO (SF)-FL-VND-BUNDLE-NULL
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlVndBundle(ws.getPoli().getPolFlVndBundle());
        }
        else {
            // COB_CODE: MOVE POL-FL-VND-BUNDLE
            //             TO (SF)-FL-VND-BUNDLE
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlVndBundle(ws.getPoli().getPolFlVndBundle());
        }
        // COB_CODE: IF POL-IB-BS-NULL = HIGH-VALUES
        //                TO (SF)-IB-BS-NULL
        //           ELSE
        //                TO (SF)-IB-BS
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getPoli().getPolIbBs(), Poli.Len.POL_IB_BS)) {
            // COB_CODE: MOVE POL-IB-BS-NULL
            //             TO (SF)-IB-BS-NULL
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolIbBs(ws.getPoli().getPolIbBs());
        }
        else {
            // COB_CODE: MOVE POL-IB-BS
            //             TO (SF)-IB-BS
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolIbBs(ws.getPoli().getPolIbBs());
        }
        // COB_CODE: IF POL-FL-POLI-IFP-NULL = HIGH-VALUES
        //                TO (SF)-FL-POLI-IFP-NULL
        //           ELSE
        //                TO (SF)-FL-POLI-IFP
        //           END-IF.
        if (Conditions.eq(ws.getPoli().getPolFlPoliIfp(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE POL-FL-POLI-IFP-NULL
            //             TO (SF)-FL-POLI-IFP-NULL
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlPoliIfp(ws.getPoli().getPolFlPoliIfp());
        }
        else {
            // COB_CODE: MOVE POL-FL-POLI-IFP
            //             TO (SF)-FL-POLI-IFP
            areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlPoliIfp(ws.getPoli().getPolFlPoliIfp());
        }
    }

    /**Original name: VALORIZZA-OUTPUT-ADE<br>
	 * <pre>  --> ADESIONE
	 * ------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    VALORIZZA OUTPUT COPY LCCVADE3
	 *    ULTIMO AGG. 07 DIC 2010
	 * ------------------------------------------------------------</pre>*/
    private void valorizzaOutputAde() {
        // COB_CODE: MOVE ADE-ID-ADES
        //             TO (SF)-ID-PTF(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().setIdPtf(ws.getAdes().getAdeIdAdes());
        // COB_CODE: MOVE ADE-ID-ADES
        //             TO (SF)-ID-ADES(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeIdAdes(ws.getAdes().getAdeIdAdes());
        // COB_CODE: MOVE ADE-ID-POLI
        //             TO (SF)-ID-POLI(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeIdPoli(ws.getAdes().getAdeIdPoli());
        // COB_CODE: MOVE ADE-ID-MOVI-CRZ
        //             TO (SF)-ID-MOVI-CRZ(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeIdMoviCrz(ws.getAdes().getAdeIdMoviCrz());
        // COB_CODE: IF ADE-ID-MOVI-CHIU-NULL = HIGH-VALUES
        //                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-ADE)
        //           ELSE
        //                TO (SF)-ID-MOVI-CHIU(IX-TAB-ADE)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getAdes().getAdeIdMoviChiu().getAdeIdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE ADE-ID-MOVI-CHIU-NULL
            //             TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeIdMoviChiu().setWadeIdMoviChiuNull(ws.getAdes().getAdeIdMoviChiu().getAdeIdMoviChiuNull());
        }
        else {
            // COB_CODE: MOVE ADE-ID-MOVI-CHIU
            //             TO (SF)-ID-MOVI-CHIU(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeIdMoviChiu().setWadeIdMoviChiu(ws.getAdes().getAdeIdMoviChiu().getAdeIdMoviChiu());
        }
        // COB_CODE: MOVE ADE-DT-INI-EFF
        //             TO (SF)-DT-INI-EFF(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeDtIniEff(ws.getAdes().getAdeDtIniEff());
        // COB_CODE: MOVE ADE-DT-END-EFF
        //             TO (SF)-DT-END-EFF(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeDtEndEff(ws.getAdes().getAdeDtEndEff());
        // COB_CODE: IF ADE-IB-PREV-NULL = HIGH-VALUES
        //                TO (SF)-IB-PREV-NULL(IX-TAB-ADE)
        //           ELSE
        //                TO (SF)-IB-PREV(IX-TAB-ADE)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getAdes().getAdeIbPrev(), AdesIvvs0216.Len.ADE_IB_PREV)) {
            // COB_CODE: MOVE ADE-IB-PREV-NULL
            //             TO (SF)-IB-PREV-NULL(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeIbPrev(ws.getAdes().getAdeIbPrev());
        }
        else {
            // COB_CODE: MOVE ADE-IB-PREV
            //             TO (SF)-IB-PREV(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeIbPrev(ws.getAdes().getAdeIbPrev());
        }
        // COB_CODE: IF ADE-IB-OGG-NULL = HIGH-VALUES
        //                TO (SF)-IB-OGG-NULL(IX-TAB-ADE)
        //           ELSE
        //                TO (SF)-IB-OGG(IX-TAB-ADE)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getAdes().getAdeIbOgg(), AdesIvvs0216.Len.ADE_IB_OGG)) {
            // COB_CODE: MOVE ADE-IB-OGG-NULL
            //             TO (SF)-IB-OGG-NULL(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeIbOgg(ws.getAdes().getAdeIbOgg());
        }
        else {
            // COB_CODE: MOVE ADE-IB-OGG
            //             TO (SF)-IB-OGG(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeIbOgg(ws.getAdes().getAdeIbOgg());
        }
        // COB_CODE: MOVE ADE-COD-COMP-ANIA
        //             TO (SF)-COD-COMP-ANIA(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeCodCompAnia(ws.getAdes().getAdeCodCompAnia());
        // COB_CODE: IF ADE-DT-DECOR-NULL = HIGH-VALUES
        //                TO (SF)-DT-DECOR-NULL(IX-TAB-ADE)
        //           ELSE
        //                TO (SF)-DT-DECOR(IX-TAB-ADE)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getAdes().getAdeDtDecor().getAdeDtDecorNullFormatted())) {
            // COB_CODE: MOVE ADE-DT-DECOR-NULL
            //             TO (SF)-DT-DECOR-NULL(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeDtDecor().setWadeDtDecorNull(ws.getAdes().getAdeDtDecor().getAdeDtDecorNull());
        }
        else {
            // COB_CODE: MOVE ADE-DT-DECOR
            //             TO (SF)-DT-DECOR(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeDtDecor().setWadeDtDecor(ws.getAdes().getAdeDtDecor().getAdeDtDecor());
        }
        // COB_CODE: IF ADE-DT-SCAD-NULL = HIGH-VALUES
        //                TO (SF)-DT-SCAD-NULL(IX-TAB-ADE)
        //           ELSE
        //                TO (SF)-DT-SCAD(IX-TAB-ADE)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getAdes().getAdeDtScad().getAdeDtScadNullFormatted())) {
            // COB_CODE: MOVE ADE-DT-SCAD-NULL
            //             TO (SF)-DT-SCAD-NULL(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeDtScad().setWadeDtScadNull(ws.getAdes().getAdeDtScad().getAdeDtScadNull());
        }
        else {
            // COB_CODE: MOVE ADE-DT-SCAD
            //             TO (SF)-DT-SCAD(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeDtScad().setWadeDtScad(ws.getAdes().getAdeDtScad().getAdeDtScad());
        }
        // COB_CODE: IF ADE-ETA-A-SCAD-NULL = HIGH-VALUES
        //                TO (SF)-ETA-A-SCAD-NULL(IX-TAB-ADE)
        //           ELSE
        //                TO (SF)-ETA-A-SCAD(IX-TAB-ADE)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getAdes().getAdeEtaAScad().getAdeEtaAScadNullFormatted())) {
            // COB_CODE: MOVE ADE-ETA-A-SCAD-NULL
            //             TO (SF)-ETA-A-SCAD-NULL(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeEtaAScad().setWadeEtaAScadNull(ws.getAdes().getAdeEtaAScad().getAdeEtaAScadNull());
        }
        else {
            // COB_CODE: MOVE ADE-ETA-A-SCAD
            //             TO (SF)-ETA-A-SCAD(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeEtaAScad().setWadeEtaAScad(ws.getAdes().getAdeEtaAScad().getAdeEtaAScad());
        }
        // COB_CODE: IF ADE-DUR-AA-NULL = HIGH-VALUES
        //                TO (SF)-DUR-AA-NULL(IX-TAB-ADE)
        //           ELSE
        //                TO (SF)-DUR-AA(IX-TAB-ADE)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getAdes().getAdeDurAa().getAdeDurAaNullFormatted())) {
            // COB_CODE: MOVE ADE-DUR-AA-NULL
            //             TO (SF)-DUR-AA-NULL(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeDurAa().setWadeDurAaNull(ws.getAdes().getAdeDurAa().getAdeDurAaNull());
        }
        else {
            // COB_CODE: MOVE ADE-DUR-AA
            //             TO (SF)-DUR-AA(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeDurAa().setWadeDurAa(ws.getAdes().getAdeDurAa().getAdeDurAa());
        }
        // COB_CODE: IF ADE-DUR-MM-NULL = HIGH-VALUES
        //                TO (SF)-DUR-MM-NULL(IX-TAB-ADE)
        //           ELSE
        //                TO (SF)-DUR-MM(IX-TAB-ADE)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getAdes().getAdeDurMm().getAdeDurMmNullFormatted())) {
            // COB_CODE: MOVE ADE-DUR-MM-NULL
            //             TO (SF)-DUR-MM-NULL(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeDurMm().setWadeDurMmNull(ws.getAdes().getAdeDurMm().getAdeDurMmNull());
        }
        else {
            // COB_CODE: MOVE ADE-DUR-MM
            //             TO (SF)-DUR-MM(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeDurMm().setWadeDurMm(ws.getAdes().getAdeDurMm().getAdeDurMm());
        }
        // COB_CODE: IF ADE-DUR-GG-NULL = HIGH-VALUES
        //                TO (SF)-DUR-GG-NULL(IX-TAB-ADE)
        //           ELSE
        //                TO (SF)-DUR-GG(IX-TAB-ADE)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getAdes().getAdeDurGg().getAdeDurGgNullFormatted())) {
            // COB_CODE: MOVE ADE-DUR-GG-NULL
            //             TO (SF)-DUR-GG-NULL(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeDurGg().setWadeDurGgNull(ws.getAdes().getAdeDurGg().getAdeDurGgNull());
        }
        else {
            // COB_CODE: MOVE ADE-DUR-GG
            //             TO (SF)-DUR-GG(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeDurGg().setWadeDurGg(ws.getAdes().getAdeDurGg().getAdeDurGg());
        }
        // COB_CODE: MOVE ADE-TP-RGM-FISC
        //             TO (SF)-TP-RGM-FISC(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeTpRgmFisc(ws.getAdes().getAdeTpRgmFisc());
        // COB_CODE: IF ADE-TP-RIAT-NULL = HIGH-VALUES
        //                TO (SF)-TP-RIAT-NULL(IX-TAB-ADE)
        //           ELSE
        //                TO (SF)-TP-RIAT(IX-TAB-ADE)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getAdes().getAdeTpRiatFormatted())) {
            // COB_CODE: MOVE ADE-TP-RIAT-NULL
            //             TO (SF)-TP-RIAT-NULL(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeTpRiat(ws.getAdes().getAdeTpRiat());
        }
        else {
            // COB_CODE: MOVE ADE-TP-RIAT
            //             TO (SF)-TP-RIAT(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeTpRiat(ws.getAdes().getAdeTpRiat());
        }
        // COB_CODE: MOVE ADE-TP-MOD-PAG-TIT
        //             TO (SF)-TP-MOD-PAG-TIT(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeTpModPagTit(ws.getAdes().getAdeTpModPagTit());
        // COB_CODE: IF ADE-TP-IAS-NULL = HIGH-VALUES
        //                TO (SF)-TP-IAS-NULL(IX-TAB-ADE)
        //           ELSE
        //                TO (SF)-TP-IAS(IX-TAB-ADE)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getAdes().getAdeTpIasFormatted())) {
            // COB_CODE: MOVE ADE-TP-IAS-NULL
            //             TO (SF)-TP-IAS-NULL(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeTpIas(ws.getAdes().getAdeTpIas());
        }
        else {
            // COB_CODE: MOVE ADE-TP-IAS
            //             TO (SF)-TP-IAS(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeTpIas(ws.getAdes().getAdeTpIas());
        }
        // COB_CODE: IF ADE-DT-VARZ-TP-IAS-NULL = HIGH-VALUES
        //                TO (SF)-DT-VARZ-TP-IAS-NULL(IX-TAB-ADE)
        //           ELSE
        //                TO (SF)-DT-VARZ-TP-IAS(IX-TAB-ADE)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getAdes().getAdeDtVarzTpIas().getAdeDtVarzTpIasNullFormatted())) {
            // COB_CODE: MOVE ADE-DT-VARZ-TP-IAS-NULL
            //             TO (SF)-DT-VARZ-TP-IAS-NULL(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeDtVarzTpIas().setWadeDtVarzTpIasNull(ws.getAdes().getAdeDtVarzTpIas().getAdeDtVarzTpIasNull());
        }
        else {
            // COB_CODE: MOVE ADE-DT-VARZ-TP-IAS
            //             TO (SF)-DT-VARZ-TP-IAS(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeDtVarzTpIas().setWadeDtVarzTpIas(ws.getAdes().getAdeDtVarzTpIas().getAdeDtVarzTpIas());
        }
        // COB_CODE: IF ADE-PRE-NET-IND-NULL = HIGH-VALUES
        //                TO (SF)-PRE-NET-IND-NULL(IX-TAB-ADE)
        //           ELSE
        //                TO (SF)-PRE-NET-IND(IX-TAB-ADE)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getAdes().getAdePreNetInd().getAdePreNetIndNullFormatted())) {
            // COB_CODE: MOVE ADE-PRE-NET-IND-NULL
            //             TO (SF)-PRE-NET-IND-NULL(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadePreNetInd().setWadePreNetIndNull(ws.getAdes().getAdePreNetInd().getAdePreNetIndNull());
        }
        else {
            // COB_CODE: MOVE ADE-PRE-NET-IND
            //             TO (SF)-PRE-NET-IND(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadePreNetInd().setWadePreNetInd(Trunc.toDecimal(ws.getAdes().getAdePreNetInd().getAdePreNetInd(), 15, 3));
        }
        // COB_CODE: IF ADE-PRE-LRD-IND-NULL = HIGH-VALUES
        //                TO (SF)-PRE-LRD-IND-NULL(IX-TAB-ADE)
        //           ELSE
        //                TO (SF)-PRE-LRD-IND(IX-TAB-ADE)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getAdes().getAdePreLrdInd().getAdePreLrdIndNullFormatted())) {
            // COB_CODE: MOVE ADE-PRE-LRD-IND-NULL
            //             TO (SF)-PRE-LRD-IND-NULL(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadePreLrdInd().setWadePreLrdIndNull(ws.getAdes().getAdePreLrdInd().getAdePreLrdIndNull());
        }
        else {
            // COB_CODE: MOVE ADE-PRE-LRD-IND
            //             TO (SF)-PRE-LRD-IND(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadePreLrdInd().setWadePreLrdInd(Trunc.toDecimal(ws.getAdes().getAdePreLrdInd().getAdePreLrdInd(), 15, 3));
        }
        // COB_CODE: IF ADE-RAT-LRD-IND-NULL = HIGH-VALUES
        //                TO (SF)-RAT-LRD-IND-NULL(IX-TAB-ADE)
        //           ELSE
        //                TO (SF)-RAT-LRD-IND(IX-TAB-ADE)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getAdes().getAdeRatLrdInd().getAdeRatLrdIndNullFormatted())) {
            // COB_CODE: MOVE ADE-RAT-LRD-IND-NULL
            //             TO (SF)-RAT-LRD-IND-NULL(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeRatLrdInd().setWadeRatLrdIndNull(ws.getAdes().getAdeRatLrdInd().getAdeRatLrdIndNull());
        }
        else {
            // COB_CODE: MOVE ADE-RAT-LRD-IND
            //             TO (SF)-RAT-LRD-IND(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeRatLrdInd().setWadeRatLrdInd(Trunc.toDecimal(ws.getAdes().getAdeRatLrdInd().getAdeRatLrdInd(), 15, 3));
        }
        // COB_CODE: IF ADE-PRSTZ-INI-IND-NULL = HIGH-VALUES
        //                TO (SF)-PRSTZ-INI-IND-NULL(IX-TAB-ADE)
        //           ELSE
        //                TO (SF)-PRSTZ-INI-IND(IX-TAB-ADE)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getAdes().getAdePrstzIniInd().getAdePrstzIniIndNullFormatted())) {
            // COB_CODE: MOVE ADE-PRSTZ-INI-IND-NULL
            //             TO (SF)-PRSTZ-INI-IND-NULL(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadePrstzIniInd().setWadePrstzIniIndNull(ws.getAdes().getAdePrstzIniInd().getAdePrstzIniIndNull());
        }
        else {
            // COB_CODE: MOVE ADE-PRSTZ-INI-IND
            //             TO (SF)-PRSTZ-INI-IND(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadePrstzIniInd().setWadePrstzIniInd(Trunc.toDecimal(ws.getAdes().getAdePrstzIniInd().getAdePrstzIniInd(), 15, 3));
        }
        // COB_CODE: IF ADE-FL-COINC-ASSTO-NULL = HIGH-VALUES
        //                TO (SF)-FL-COINC-ASSTO-NULL(IX-TAB-ADE)
        //           ELSE
        //                TO (SF)-FL-COINC-ASSTO(IX-TAB-ADE)
        //           END-IF
        if (Conditions.eq(ws.getAdes().getAdeFlCoincAssto(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE ADE-FL-COINC-ASSTO-NULL
            //             TO (SF)-FL-COINC-ASSTO-NULL(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeFlCoincAssto(ws.getAdes().getAdeFlCoincAssto());
        }
        else {
            // COB_CODE: MOVE ADE-FL-COINC-ASSTO
            //             TO (SF)-FL-COINC-ASSTO(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeFlCoincAssto(ws.getAdes().getAdeFlCoincAssto());
        }
        // COB_CODE: IF ADE-IB-DFLT-NULL = HIGH-VALUES
        //                TO (SF)-IB-DFLT-NULL(IX-TAB-ADE)
        //           ELSE
        //                TO (SF)-IB-DFLT(IX-TAB-ADE)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getAdes().getAdeIbDflt(), AdesIvvs0216.Len.ADE_IB_DFLT)) {
            // COB_CODE: MOVE ADE-IB-DFLT-NULL
            //             TO (SF)-IB-DFLT-NULL(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeIbDflt(ws.getAdes().getAdeIbDflt());
        }
        else {
            // COB_CODE: MOVE ADE-IB-DFLT
            //             TO (SF)-IB-DFLT(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeIbDflt(ws.getAdes().getAdeIbDflt());
        }
        // COB_CODE: IF ADE-MOD-CALC-NULL = HIGH-VALUES
        //                TO (SF)-MOD-CALC-NULL(IX-TAB-ADE)
        //           ELSE
        //                TO (SF)-MOD-CALC(IX-TAB-ADE)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getAdes().getAdeModCalcFormatted())) {
            // COB_CODE: MOVE ADE-MOD-CALC-NULL
            //             TO (SF)-MOD-CALC-NULL(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeModCalc(ws.getAdes().getAdeModCalc());
        }
        else {
            // COB_CODE: MOVE ADE-MOD-CALC
            //             TO (SF)-MOD-CALC(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeModCalc(ws.getAdes().getAdeModCalc());
        }
        // COB_CODE: IF ADE-TP-FNT-CNBTVA-NULL = HIGH-VALUES
        //                TO (SF)-TP-FNT-CNBTVA-NULL(IX-TAB-ADE)
        //           ELSE
        //                TO (SF)-TP-FNT-CNBTVA(IX-TAB-ADE)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getAdes().getAdeTpFntCnbtvaFormatted())) {
            // COB_CODE: MOVE ADE-TP-FNT-CNBTVA-NULL
            //             TO (SF)-TP-FNT-CNBTVA-NULL(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeTpFntCnbtva(ws.getAdes().getAdeTpFntCnbtva());
        }
        else {
            // COB_CODE: MOVE ADE-TP-FNT-CNBTVA
            //             TO (SF)-TP-FNT-CNBTVA(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeTpFntCnbtva(ws.getAdes().getAdeTpFntCnbtva());
        }
        // COB_CODE: IF ADE-IMP-AZ-NULL = HIGH-VALUES
        //                TO (SF)-IMP-AZ-NULL(IX-TAB-ADE)
        //           ELSE
        //                TO (SF)-IMP-AZ(IX-TAB-ADE)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getAdes().getAdeImpAz().getAdeImpAzNullFormatted())) {
            // COB_CODE: MOVE ADE-IMP-AZ-NULL
            //             TO (SF)-IMP-AZ-NULL(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeImpAz().setWadeImpAzNull(ws.getAdes().getAdeImpAz().getAdeImpAzNull());
        }
        else {
            // COB_CODE: MOVE ADE-IMP-AZ
            //             TO (SF)-IMP-AZ(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeImpAz().setWadeImpAz(Trunc.toDecimal(ws.getAdes().getAdeImpAz().getAdeImpAz(), 15, 3));
        }
        // COB_CODE: IF ADE-IMP-ADER-NULL = HIGH-VALUES
        //                TO (SF)-IMP-ADER-NULL(IX-TAB-ADE)
        //           ELSE
        //                TO (SF)-IMP-ADER(IX-TAB-ADE)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getAdes().getAdeImpAder().getAdeImpAderNullFormatted())) {
            // COB_CODE: MOVE ADE-IMP-ADER-NULL
            //             TO (SF)-IMP-ADER-NULL(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeImpAder().setWadeImpAderNull(ws.getAdes().getAdeImpAder().getAdeImpAderNull());
        }
        else {
            // COB_CODE: MOVE ADE-IMP-ADER
            //             TO (SF)-IMP-ADER(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeImpAder().setWadeImpAder(Trunc.toDecimal(ws.getAdes().getAdeImpAder().getAdeImpAder(), 15, 3));
        }
        // COB_CODE: IF ADE-IMP-TFR-NULL = HIGH-VALUES
        //                TO (SF)-IMP-TFR-NULL(IX-TAB-ADE)
        //           ELSE
        //                TO (SF)-IMP-TFR(IX-TAB-ADE)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getAdes().getAdeImpTfr().getAdeImpTfrNullFormatted())) {
            // COB_CODE: MOVE ADE-IMP-TFR-NULL
            //             TO (SF)-IMP-TFR-NULL(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeImpTfr().setWadeImpTfrNull(ws.getAdes().getAdeImpTfr().getAdeImpTfrNull());
        }
        else {
            // COB_CODE: MOVE ADE-IMP-TFR
            //             TO (SF)-IMP-TFR(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeImpTfr().setWadeImpTfr(Trunc.toDecimal(ws.getAdes().getAdeImpTfr().getAdeImpTfr(), 15, 3));
        }
        // COB_CODE: IF ADE-IMP-VOLO-NULL = HIGH-VALUES
        //                TO (SF)-IMP-VOLO-NULL(IX-TAB-ADE)
        //           ELSE
        //                TO (SF)-IMP-VOLO(IX-TAB-ADE)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getAdes().getAdeImpVolo().getAdeImpVoloNullFormatted())) {
            // COB_CODE: MOVE ADE-IMP-VOLO-NULL
            //             TO (SF)-IMP-VOLO-NULL(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeImpVolo().setWadeImpVoloNull(ws.getAdes().getAdeImpVolo().getAdeImpVoloNull());
        }
        else {
            // COB_CODE: MOVE ADE-IMP-VOLO
            //             TO (SF)-IMP-VOLO(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeImpVolo().setWadeImpVolo(Trunc.toDecimal(ws.getAdes().getAdeImpVolo().getAdeImpVolo(), 15, 3));
        }
        // COB_CODE: IF ADE-PC-AZ-NULL = HIGH-VALUES
        //                TO (SF)-PC-AZ-NULL(IX-TAB-ADE)
        //           ELSE
        //                TO (SF)-PC-AZ(IX-TAB-ADE)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getAdes().getAdePcAz().getAdePcAzNullFormatted())) {
            // COB_CODE: MOVE ADE-PC-AZ-NULL
            //             TO (SF)-PC-AZ-NULL(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadePcAz().setWadePcAzNull(ws.getAdes().getAdePcAz().getAdePcAzNull());
        }
        else {
            // COB_CODE: MOVE ADE-PC-AZ
            //             TO (SF)-PC-AZ(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadePcAz().setWadePcAz(Trunc.toDecimal(ws.getAdes().getAdePcAz().getAdePcAz(), 6, 3));
        }
        // COB_CODE: IF ADE-PC-ADER-NULL = HIGH-VALUES
        //                TO (SF)-PC-ADER-NULL(IX-TAB-ADE)
        //           ELSE
        //                TO (SF)-PC-ADER(IX-TAB-ADE)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getAdes().getAdePcAder().getAdePcAderNullFormatted())) {
            // COB_CODE: MOVE ADE-PC-ADER-NULL
            //             TO (SF)-PC-ADER-NULL(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadePcAder().setWadePcAderNull(ws.getAdes().getAdePcAder().getAdePcAderNull());
        }
        else {
            // COB_CODE: MOVE ADE-PC-ADER
            //             TO (SF)-PC-ADER(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadePcAder().setWadePcAder(Trunc.toDecimal(ws.getAdes().getAdePcAder().getAdePcAder(), 6, 3));
        }
        // COB_CODE: IF ADE-PC-TFR-NULL = HIGH-VALUES
        //                TO (SF)-PC-TFR-NULL(IX-TAB-ADE)
        //           ELSE
        //                TO (SF)-PC-TFR(IX-TAB-ADE)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getAdes().getAdePcTfr().getAdePcTfrNullFormatted())) {
            // COB_CODE: MOVE ADE-PC-TFR-NULL
            //             TO (SF)-PC-TFR-NULL(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadePcTfr().setWadePcTfrNull(ws.getAdes().getAdePcTfr().getAdePcTfrNull());
        }
        else {
            // COB_CODE: MOVE ADE-PC-TFR
            //             TO (SF)-PC-TFR(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadePcTfr().setWadePcTfr(Trunc.toDecimal(ws.getAdes().getAdePcTfr().getAdePcTfr(), 6, 3));
        }
        // COB_CODE: IF ADE-PC-VOLO-NULL = HIGH-VALUES
        //                TO (SF)-PC-VOLO-NULL(IX-TAB-ADE)
        //           ELSE
        //                TO (SF)-PC-VOLO(IX-TAB-ADE)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getAdes().getAdePcVolo().getAdePcVoloNullFormatted())) {
            // COB_CODE: MOVE ADE-PC-VOLO-NULL
            //             TO (SF)-PC-VOLO-NULL(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadePcVolo().setWadePcVoloNull(ws.getAdes().getAdePcVolo().getAdePcVoloNull());
        }
        else {
            // COB_CODE: MOVE ADE-PC-VOLO
            //             TO (SF)-PC-VOLO(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadePcVolo().setWadePcVolo(Trunc.toDecimal(ws.getAdes().getAdePcVolo().getAdePcVolo(), 6, 3));
        }
        // COB_CODE: IF ADE-DT-NOVA-RGM-FISC-NULL = HIGH-VALUES
        //                TO (SF)-DT-NOVA-RGM-FISC-NULL(IX-TAB-ADE)
        //           ELSE
        //                TO (SF)-DT-NOVA-RGM-FISC(IX-TAB-ADE)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getAdes().getAdeDtNovaRgmFisc().getAdeDtNovaRgmFiscNullFormatted())) {
            // COB_CODE: MOVE ADE-DT-NOVA-RGM-FISC-NULL
            //             TO (SF)-DT-NOVA-RGM-FISC-NULL(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeDtNovaRgmFisc().setWadeDtNovaRgmFiscNull(ws.getAdes().getAdeDtNovaRgmFisc().getAdeDtNovaRgmFiscNull());
        }
        else {
            // COB_CODE: MOVE ADE-DT-NOVA-RGM-FISC
            //             TO (SF)-DT-NOVA-RGM-FISC(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeDtNovaRgmFisc().setWadeDtNovaRgmFisc(ws.getAdes().getAdeDtNovaRgmFisc().getAdeDtNovaRgmFisc());
        }
        // COB_CODE: IF ADE-FL-ATTIV-NULL = HIGH-VALUES
        //                TO (SF)-FL-ATTIV-NULL(IX-TAB-ADE)
        //           ELSE
        //                TO (SF)-FL-ATTIV(IX-TAB-ADE)
        //           END-IF
        if (Conditions.eq(ws.getAdes().getAdeFlAttiv(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE ADE-FL-ATTIV-NULL
            //             TO (SF)-FL-ATTIV-NULL(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeFlAttiv(ws.getAdes().getAdeFlAttiv());
        }
        else {
            // COB_CODE: MOVE ADE-FL-ATTIV
            //             TO (SF)-FL-ATTIV(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeFlAttiv(ws.getAdes().getAdeFlAttiv());
        }
        // COB_CODE: IF ADE-IMP-REC-RIT-VIS-NULL = HIGH-VALUES
        //                TO (SF)-IMP-REC-RIT-VIS-NULL(IX-TAB-ADE)
        //           ELSE
        //                TO (SF)-IMP-REC-RIT-VIS(IX-TAB-ADE)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getAdes().getAdeImpRecRitVis().getAdeImpRecRitVisNullFormatted())) {
            // COB_CODE: MOVE ADE-IMP-REC-RIT-VIS-NULL
            //             TO (SF)-IMP-REC-RIT-VIS-NULL(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeImpRecRitVis().setWadeImpRecRitVisNull(ws.getAdes().getAdeImpRecRitVis().getAdeImpRecRitVisNull());
        }
        else {
            // COB_CODE: MOVE ADE-IMP-REC-RIT-VIS
            //             TO (SF)-IMP-REC-RIT-VIS(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeImpRecRitVis().setWadeImpRecRitVis(Trunc.toDecimal(ws.getAdes().getAdeImpRecRitVis().getAdeImpRecRitVis(), 15, 3));
        }
        // COB_CODE: IF ADE-IMP-REC-RIT-ACC-NULL = HIGH-VALUES
        //                TO (SF)-IMP-REC-RIT-ACC-NULL(IX-TAB-ADE)
        //           ELSE
        //                TO (SF)-IMP-REC-RIT-ACC(IX-TAB-ADE)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getAdes().getAdeImpRecRitAcc().getAdeImpRecRitAccNullFormatted())) {
            // COB_CODE: MOVE ADE-IMP-REC-RIT-ACC-NULL
            //             TO (SF)-IMP-REC-RIT-ACC-NULL(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeImpRecRitAcc().setWadeImpRecRitAccNull(ws.getAdes().getAdeImpRecRitAcc().getAdeImpRecRitAccNull());
        }
        else {
            // COB_CODE: MOVE ADE-IMP-REC-RIT-ACC
            //             TO (SF)-IMP-REC-RIT-ACC(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeImpRecRitAcc().setWadeImpRecRitAcc(Trunc.toDecimal(ws.getAdes().getAdeImpRecRitAcc().getAdeImpRecRitAcc(), 15, 3));
        }
        // COB_CODE: IF ADE-FL-VARZ-STAT-TBGC-NULL = HIGH-VALUES
        //                TO (SF)-FL-VARZ-STAT-TBGC-NULL(IX-TAB-ADE)
        //           ELSE
        //                TO (SF)-FL-VARZ-STAT-TBGC(IX-TAB-ADE)
        //           END-IF
        if (Conditions.eq(ws.getAdes().getAdeFlVarzStatTbgc(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE ADE-FL-VARZ-STAT-TBGC-NULL
            //             TO (SF)-FL-VARZ-STAT-TBGC-NULL(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeFlVarzStatTbgc(ws.getAdes().getAdeFlVarzStatTbgc());
        }
        else {
            // COB_CODE: MOVE ADE-FL-VARZ-STAT-TBGC
            //             TO (SF)-FL-VARZ-STAT-TBGC(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeFlVarzStatTbgc(ws.getAdes().getAdeFlVarzStatTbgc());
        }
        // COB_CODE: IF ADE-FL-PROVZA-MIGRAZ-NULL = HIGH-VALUES
        //                TO (SF)-FL-PROVZA-MIGRAZ-NULL(IX-TAB-ADE)
        //           ELSE
        //                TO (SF)-FL-PROVZA-MIGRAZ(IX-TAB-ADE)
        //           END-IF
        if (Conditions.eq(ws.getAdes().getAdeFlProvzaMigraz(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE ADE-FL-PROVZA-MIGRAZ-NULL
            //             TO (SF)-FL-PROVZA-MIGRAZ-NULL(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeFlProvzaMigraz(ws.getAdes().getAdeFlProvzaMigraz());
        }
        else {
            // COB_CODE: MOVE ADE-FL-PROVZA-MIGRAZ
            //             TO (SF)-FL-PROVZA-MIGRAZ(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeFlProvzaMigraz(ws.getAdes().getAdeFlProvzaMigraz());
        }
        // COB_CODE: IF ADE-IMPB-VIS-DA-REC-NULL = HIGH-VALUES
        //                TO (SF)-IMPB-VIS-DA-REC-NULL(IX-TAB-ADE)
        //           ELSE
        //                TO (SF)-IMPB-VIS-DA-REC(IX-TAB-ADE)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getAdes().getAdeImpbVisDaRec().getAdeImpbVisDaRecNullFormatted())) {
            // COB_CODE: MOVE ADE-IMPB-VIS-DA-REC-NULL
            //             TO (SF)-IMPB-VIS-DA-REC-NULL(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeImpbVisDaRec().setWadeImpbVisDaRecNull(ws.getAdes().getAdeImpbVisDaRec().getAdeImpbVisDaRecNull());
        }
        else {
            // COB_CODE: MOVE ADE-IMPB-VIS-DA-REC
            //             TO (SF)-IMPB-VIS-DA-REC(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeImpbVisDaRec().setWadeImpbVisDaRec(Trunc.toDecimal(ws.getAdes().getAdeImpbVisDaRec().getAdeImpbVisDaRec(), 15, 3));
        }
        // COB_CODE: IF ADE-DT-DECOR-PREST-BAN-NULL = HIGH-VALUES
        //                TO (SF)-DT-DECOR-PREST-BAN-NULL(IX-TAB-ADE)
        //           ELSE
        //                TO (SF)-DT-DECOR-PREST-BAN(IX-TAB-ADE)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getAdes().getAdeDtDecorPrestBan().getAdeDtDecorPrestBanNullFormatted())) {
            // COB_CODE: MOVE ADE-DT-DECOR-PREST-BAN-NULL
            //             TO (SF)-DT-DECOR-PREST-BAN-NULL(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeDtDecorPrestBan().setWadeDtDecorPrestBanNull(ws.getAdes().getAdeDtDecorPrestBan().getAdeDtDecorPrestBanNull());
        }
        else {
            // COB_CODE: MOVE ADE-DT-DECOR-PREST-BAN
            //             TO (SF)-DT-DECOR-PREST-BAN(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeDtDecorPrestBan().setWadeDtDecorPrestBan(ws.getAdes().getAdeDtDecorPrestBan().getAdeDtDecorPrestBan());
        }
        // COB_CODE: IF ADE-DT-EFF-VARZ-STAT-T-NULL = HIGH-VALUES
        //                TO (SF)-DT-EFF-VARZ-STAT-T-NULL(IX-TAB-ADE)
        //           ELSE
        //                TO (SF)-DT-EFF-VARZ-STAT-T(IX-TAB-ADE)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getAdes().getAdeDtEffVarzStatT().getAdeDtEffVarzStatTNullFormatted())) {
            // COB_CODE: MOVE ADE-DT-EFF-VARZ-STAT-T-NULL
            //             TO (SF)-DT-EFF-VARZ-STAT-T-NULL(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeDtEffVarzStatT().setWadeDtEffVarzStatTNull(ws.getAdes().getAdeDtEffVarzStatT().getAdeDtEffVarzStatTNull());
        }
        else {
            // COB_CODE: MOVE ADE-DT-EFF-VARZ-STAT-T
            //             TO (SF)-DT-EFF-VARZ-STAT-T(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeDtEffVarzStatT().setWadeDtEffVarzStatT(ws.getAdes().getAdeDtEffVarzStatT().getAdeDtEffVarzStatT());
        }
        // COB_CODE: MOVE ADE-DS-RIGA
        //             TO (SF)-DS-RIGA(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeDsRiga(ws.getAdes().getAdeDsRiga());
        // COB_CODE: MOVE ADE-DS-OPER-SQL
        //             TO (SF)-DS-OPER-SQL(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeDsOperSql(ws.getAdes().getAdeDsOperSql());
        // COB_CODE: MOVE ADE-DS-VER
        //             TO (SF)-DS-VER(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeDsVer(ws.getAdes().getAdeDsVer());
        // COB_CODE: MOVE ADE-DS-TS-INI-CPTZ
        //             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeDsTsIniCptz(ws.getAdes().getAdeDsTsIniCptz());
        // COB_CODE: MOVE ADE-DS-TS-END-CPTZ
        //             TO (SF)-DS-TS-END-CPTZ(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeDsTsEndCptz(ws.getAdes().getAdeDsTsEndCptz());
        // COB_CODE: MOVE ADE-DS-UTENTE
        //             TO (SF)-DS-UTENTE(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeDsUtente(ws.getAdes().getAdeDsUtente());
        // COB_CODE: MOVE ADE-DS-STATO-ELAB
        //             TO (SF)-DS-STATO-ELAB(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeDsStatoElab(ws.getAdes().getAdeDsStatoElab());
        // COB_CODE: IF ADE-CUM-CNBT-CAP-NULL = HIGH-VALUES
        //                TO (SF)-CUM-CNBT-CAP-NULL(IX-TAB-ADE)
        //           ELSE
        //                TO (SF)-CUM-CNBT-CAP(IX-TAB-ADE)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getAdes().getAdeCumCnbtCap().getAdeCumCnbtCapNullFormatted())) {
            // COB_CODE: MOVE ADE-CUM-CNBT-CAP-NULL
            //             TO (SF)-CUM-CNBT-CAP-NULL(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeCumCnbtCap().setWadeCumCnbtCapNull(ws.getAdes().getAdeCumCnbtCap().getAdeCumCnbtCapNull());
        }
        else {
            // COB_CODE: MOVE ADE-CUM-CNBT-CAP
            //             TO (SF)-CUM-CNBT-CAP(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeCumCnbtCap().setWadeCumCnbtCap(Trunc.toDecimal(ws.getAdes().getAdeCumCnbtCap().getAdeCumCnbtCap(), 15, 3));
        }
        // COB_CODE: IF ADE-IMP-GAR-CNBT-NULL = HIGH-VALUES
        //                TO (SF)-IMP-GAR-CNBT-NULL(IX-TAB-ADE)
        //           ELSE
        //                TO (SF)-IMP-GAR-CNBT(IX-TAB-ADE)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getAdes().getAdeImpGarCnbt().getAdeImpGarCnbtNullFormatted())) {
            // COB_CODE: MOVE ADE-IMP-GAR-CNBT-NULL
            //             TO (SF)-IMP-GAR-CNBT-NULL(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeImpGarCnbt().setWadeImpGarCnbtNull(ws.getAdes().getAdeImpGarCnbt().getAdeImpGarCnbtNull());
        }
        else {
            // COB_CODE: MOVE ADE-IMP-GAR-CNBT
            //             TO (SF)-IMP-GAR-CNBT(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeImpGarCnbt().setWadeImpGarCnbt(Trunc.toDecimal(ws.getAdes().getAdeImpGarCnbt().getAdeImpGarCnbt(), 15, 3));
        }
        // COB_CODE: IF ADE-DT-ULT-CONS-CNBT-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-CONS-CNBT-NULL(IX-TAB-ADE)
        //           ELSE
        //                TO (SF)-DT-ULT-CONS-CNBT(IX-TAB-ADE)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getAdes().getAdeDtUltConsCnbt().getAdeDtUltConsCnbtNullFormatted())) {
            // COB_CODE: MOVE ADE-DT-ULT-CONS-CNBT-NULL
            //             TO (SF)-DT-ULT-CONS-CNBT-NULL(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeDtUltConsCnbt().setWadeDtUltConsCnbtNull(ws.getAdes().getAdeDtUltConsCnbt().getAdeDtUltConsCnbtNull());
        }
        else {
            // COB_CODE: MOVE ADE-DT-ULT-CONS-CNBT
            //             TO (SF)-DT-ULT-CONS-CNBT(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeDtUltConsCnbt().setWadeDtUltConsCnbt(ws.getAdes().getAdeDtUltConsCnbt().getAdeDtUltConsCnbt());
        }
        // COB_CODE: IF ADE-IDEN-ISC-FND-NULL = HIGH-VALUES
        //                TO (SF)-IDEN-ISC-FND-NULL(IX-TAB-ADE)
        //           ELSE
        //                TO (SF)-IDEN-ISC-FND(IX-TAB-ADE)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getAdes().getAdeIdenIscFnd(), AdesIvvs0216.Len.ADE_IDEN_ISC_FND)) {
            // COB_CODE: MOVE ADE-IDEN-ISC-FND-NULL
            //             TO (SF)-IDEN-ISC-FND-NULL(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeIdenIscFnd(ws.getAdes().getAdeIdenIscFnd());
        }
        else {
            // COB_CODE: MOVE ADE-IDEN-ISC-FND
            //             TO (SF)-IDEN-ISC-FND(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeIdenIscFnd(ws.getAdes().getAdeIdenIscFnd());
        }
        // COB_CODE: IF ADE-NUM-RAT-PIAN-NULL = HIGH-VALUES
        //                TO (SF)-NUM-RAT-PIAN-NULL(IX-TAB-ADE)
        //           ELSE
        //                TO (SF)-NUM-RAT-PIAN(IX-TAB-ADE)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getAdes().getAdeNumRatPian().getAdeNumRatPianNullFormatted())) {
            // COB_CODE: MOVE ADE-NUM-RAT-PIAN-NULL
            //             TO (SF)-NUM-RAT-PIAN-NULL(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeNumRatPian().setWadeNumRatPianNull(ws.getAdes().getAdeNumRatPian().getAdeNumRatPianNull());
        }
        else {
            // COB_CODE: MOVE ADE-NUM-RAT-PIAN
            //             TO (SF)-NUM-RAT-PIAN(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeNumRatPian().setWadeNumRatPian(Trunc.toDecimal(ws.getAdes().getAdeNumRatPian().getAdeNumRatPian(), 12, 5));
        }
        // COB_CODE: IF ADE-DT-PRESC-NULL = HIGH-VALUES
        //                TO (SF)-DT-PRESC-NULL(IX-TAB-ADE)
        //           ELSE
        //                TO (SF)-DT-PRESC(IX-TAB-ADE)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getAdes().getAdeDtPresc().getAdeDtPrescNullFormatted())) {
            // COB_CODE: MOVE ADE-DT-PRESC-NULL
            //             TO (SF)-DT-PRESC-NULL(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeDtPresc().setWadeDtPrescNull(ws.getAdes().getAdeDtPresc().getAdeDtPrescNull());
        }
        else {
            // COB_CODE: MOVE ADE-DT-PRESC
            //             TO (SF)-DT-PRESC(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeDtPresc().setWadeDtPresc(ws.getAdes().getAdeDtPresc().getAdeDtPresc());
        }
        // COB_CODE: IF ADE-CONCS-PREST-NULL = HIGH-VALUES
        //                TO (SF)-CONCS-PREST-NULL(IX-TAB-ADE)
        //           ELSE
        //                TO (SF)-CONCS-PREST(IX-TAB-ADE)
        //           END-IF.
        if (Conditions.eq(ws.getAdes().getAdeConcsPrest(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE ADE-CONCS-PREST-NULL
            //             TO (SF)-CONCS-PREST-NULL(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeConcsPrest(ws.getAdes().getAdeConcsPrest());
        }
        else {
            // COB_CODE: MOVE ADE-CONCS-PREST
            //             TO (SF)-CONCS-PREST(IX-TAB-ADE)
            areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeConcsPrest(ws.getAdes().getAdeConcsPrest());
        }
    }

    /**Original name: VALORIZZA-OUTPUT-GRZ<br>
	 * <pre>  --> GARANZIA
	 * ------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    VALORIZZA OUTPUT COPY LCCVGRZ3
	 *    ULTIMO AGG. 31 OTT 2013
	 * ------------------------------------------------------------</pre>*/
    private void valorizzaOutputGrz() {
        // COB_CODE: MOVE GRZ-ID-GAR
        //             TO (SF)-ID-PTF(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().setIdPtf(ws.getGar().getGrzIdGar());
        // COB_CODE: MOVE GRZ-ID-GAR
        //             TO (SF)-ID-GAR(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzIdGar(ws.getGar().getGrzIdGar());
        // COB_CODE: IF GRZ-ID-ADES-NULL = HIGH-VALUES
        //                TO (SF)-ID-ADES-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-ID-ADES(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzIdAdes().getGrzIdAdesNullFormatted())) {
            // COB_CODE: MOVE GRZ-ID-ADES-NULL
            //             TO (SF)-ID-ADES-NULL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzIdAdes().setWgrzIdAdesNull(ws.getGar().getGrzIdAdes().getGrzIdAdesNull());
        }
        else {
            // COB_CODE: MOVE GRZ-ID-ADES
            //             TO (SF)-ID-ADES(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzIdAdes().setWgrzIdAdes(ws.getGar().getGrzIdAdes().getGrzIdAdes());
        }
        // COB_CODE: MOVE GRZ-ID-POLI
        //             TO (SF)-ID-POLI(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzIdPoli(ws.getGar().getGrzIdPoli());
        // COB_CODE: MOVE GRZ-ID-MOVI-CRZ
        //             TO (SF)-ID-MOVI-CRZ(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzIdMoviCrz(ws.getGar().getGrzIdMoviCrz());
        // COB_CODE: IF GRZ-ID-MOVI-CHIU-NULL = HIGH-VALUES
        //                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-ID-MOVI-CHIU(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzIdMoviChiu().getGrzIdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE GRZ-ID-MOVI-CHIU-NULL
            //             TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzIdMoviChiu().setWgrzIdMoviChiuNull(ws.getGar().getGrzIdMoviChiu().getGrzIdMoviChiuNull());
        }
        else {
            // COB_CODE: MOVE GRZ-ID-MOVI-CHIU
            //             TO (SF)-ID-MOVI-CHIU(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzIdMoviChiu().setWgrzIdMoviChiu(ws.getGar().getGrzIdMoviChiu().getGrzIdMoviChiu());
        }
        // COB_CODE: MOVE GRZ-DT-INI-EFF
        //             TO (SF)-DT-INI-EFF(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzDtIniEff(ws.getGar().getGrzDtIniEff());
        // COB_CODE: MOVE GRZ-DT-END-EFF
        //             TO (SF)-DT-END-EFF(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzDtEndEff(ws.getGar().getGrzDtEndEff());
        // COB_CODE: MOVE GRZ-COD-COMP-ANIA
        //             TO (SF)-COD-COMP-ANIA(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzCodCompAnia(ws.getGar().getGrzCodCompAnia());
        // COB_CODE: IF GRZ-IB-OGG-NULL = HIGH-VALUES
        //                TO (SF)-IB-OGG-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-IB-OGG(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzIbOgg(), GarIvvs0216.Len.GRZ_IB_OGG)) {
            // COB_CODE: MOVE GRZ-IB-OGG-NULL
            //             TO (SF)-IB-OGG-NULL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzIbOgg(ws.getGar().getGrzIbOgg());
        }
        else {
            // COB_CODE: MOVE GRZ-IB-OGG
            //             TO (SF)-IB-OGG(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzIbOgg(ws.getGar().getGrzIbOgg());
        }
        // COB_CODE: IF GRZ-DT-DECOR-NULL = HIGH-VALUES
        //                TO (SF)-DT-DECOR-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-DT-DECOR(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzDtDecor().getGrzDtDecorNullFormatted())) {
            // COB_CODE: MOVE GRZ-DT-DECOR-NULL
            //             TO (SF)-DT-DECOR-NULL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzDtDecor().setWgrzDtDecorNull(ws.getGar().getGrzDtDecor().getGrzDtDecorNull());
        }
        else {
            // COB_CODE: MOVE GRZ-DT-DECOR
            //             TO (SF)-DT-DECOR(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzDtDecor().setWgrzDtDecor(ws.getGar().getGrzDtDecor().getGrzDtDecor());
        }
        // COB_CODE: IF GRZ-DT-SCAD-NULL = HIGH-VALUES
        //                TO (SF)-DT-SCAD-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-DT-SCAD(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzDtScad().getGrzDtScadNullFormatted())) {
            // COB_CODE: MOVE GRZ-DT-SCAD-NULL
            //             TO (SF)-DT-SCAD-NULL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzDtScad().setWgrzDtScadNull(ws.getGar().getGrzDtScad().getGrzDtScadNull());
        }
        else {
            // COB_CODE: MOVE GRZ-DT-SCAD
            //             TO (SF)-DT-SCAD(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzDtScad().setWgrzDtScad(ws.getGar().getGrzDtScad().getGrzDtScad());
        }
        // COB_CODE: IF GRZ-COD-SEZ-NULL = HIGH-VALUES
        //                TO (SF)-COD-SEZ-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-COD-SEZ(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzCodSezFormatted())) {
            // COB_CODE: MOVE GRZ-COD-SEZ-NULL
            //             TO (SF)-COD-SEZ-NULL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzCodSez(ws.getGar().getGrzCodSez());
        }
        else {
            // COB_CODE: MOVE GRZ-COD-SEZ
            //             TO (SF)-COD-SEZ(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzCodSez(ws.getGar().getGrzCodSez());
        }
        // COB_CODE: MOVE GRZ-COD-TARI
        //             TO (SF)-COD-TARI(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzCodTari(ws.getGar().getGrzCodTari());
        // COB_CODE: IF GRZ-RAMO-BILA-NULL = HIGH-VALUES
        //                TO (SF)-RAMO-BILA-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-RAMO-BILA(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzRamoBilaFormatted())) {
            // COB_CODE: MOVE GRZ-RAMO-BILA-NULL
            //             TO (SF)-RAMO-BILA-NULL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzRamoBila(ws.getGar().getGrzRamoBila());
        }
        else {
            // COB_CODE: MOVE GRZ-RAMO-BILA
            //             TO (SF)-RAMO-BILA(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzRamoBila(ws.getGar().getGrzRamoBila());
        }
        // COB_CODE: IF GRZ-DT-INI-VAL-TAR-NULL = HIGH-VALUES
        //                TO (SF)-DT-INI-VAL-TAR-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-DT-INI-VAL-TAR(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzDtIniValTar().getGrzDtIniValTarNullFormatted())) {
            // COB_CODE: MOVE GRZ-DT-INI-VAL-TAR-NULL
            //             TO (SF)-DT-INI-VAL-TAR-NULL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzDtIniValTar().setWgrzDtIniValTarNull(ws.getGar().getGrzDtIniValTar().getGrzDtIniValTarNull());
        }
        else {
            // COB_CODE: MOVE GRZ-DT-INI-VAL-TAR
            //             TO (SF)-DT-INI-VAL-TAR(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzDtIniValTar().setWgrzDtIniValTar(ws.getGar().getGrzDtIniValTar().getGrzDtIniValTar());
        }
        // COB_CODE: IF GRZ-ID-1O-ASSTO-NULL = HIGH-VALUES
        //                TO (SF)-ID-1O-ASSTO-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-ID-1O-ASSTO(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzId1oAssto().getGrzId1oAsstoNullFormatted())) {
            // COB_CODE: MOVE GRZ-ID-1O-ASSTO-NULL
            //             TO (SF)-ID-1O-ASSTO-NULL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzId1oAssto().setWgrzId1oAsstoNull(ws.getGar().getGrzId1oAssto().getGrzId1oAsstoNull());
        }
        else {
            // COB_CODE: MOVE GRZ-ID-1O-ASSTO
            //             TO (SF)-ID-1O-ASSTO(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzId1oAssto().setWgrzId1oAssto(ws.getGar().getGrzId1oAssto().getGrzId1oAssto());
        }
        // COB_CODE: IF GRZ-ID-2O-ASSTO-NULL = HIGH-VALUES
        //                TO (SF)-ID-2O-ASSTO-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-ID-2O-ASSTO(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzId2oAssto().getGrzId2oAsstoNullFormatted())) {
            // COB_CODE: MOVE GRZ-ID-2O-ASSTO-NULL
            //             TO (SF)-ID-2O-ASSTO-NULL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzId2oAssto().setWgrzId2oAsstoNull(ws.getGar().getGrzId2oAssto().getGrzId2oAsstoNull());
        }
        else {
            // COB_CODE: MOVE GRZ-ID-2O-ASSTO
            //             TO (SF)-ID-2O-ASSTO(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzId2oAssto().setWgrzId2oAssto(ws.getGar().getGrzId2oAssto().getGrzId2oAssto());
        }
        // COB_CODE: IF GRZ-ID-3O-ASSTO-NULL = HIGH-VALUES
        //                TO (SF)-ID-3O-ASSTO-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-ID-3O-ASSTO(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzId3oAssto().getGrzId3oAsstoNullFormatted())) {
            // COB_CODE: MOVE GRZ-ID-3O-ASSTO-NULL
            //             TO (SF)-ID-3O-ASSTO-NULL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzId3oAssto().setWgrzId3oAsstoNull(ws.getGar().getGrzId3oAssto().getGrzId3oAsstoNull());
        }
        else {
            // COB_CODE: MOVE GRZ-ID-3O-ASSTO
            //             TO (SF)-ID-3O-ASSTO(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzId3oAssto().setWgrzId3oAssto(ws.getGar().getGrzId3oAssto().getGrzId3oAssto());
        }
        // COB_CODE: IF GRZ-TP-GAR-NULL = HIGH-VALUES
        //                TO (SF)-TP-GAR-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-TP-GAR(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzTpGar().getGrzTpGarNullFormatted())) {
            // COB_CODE: MOVE GRZ-TP-GAR-NULL
            //             TO (SF)-TP-GAR-NULL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzTpGar().setWgrzTpGarNull(ws.getGar().getGrzTpGar().getGrzTpGarNull());
        }
        else {
            // COB_CODE: MOVE GRZ-TP-GAR
            //             TO (SF)-TP-GAR(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzTpGar().setWgrzTpGar(ws.getGar().getGrzTpGar().getGrzTpGar());
        }
        // COB_CODE: IF GRZ-TP-RSH-NULL = HIGH-VALUES
        //                TO (SF)-TP-RSH-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-TP-RSH(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzTpRshFormatted())) {
            // COB_CODE: MOVE GRZ-TP-RSH-NULL
            //             TO (SF)-TP-RSH-NULL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzTpRsh(ws.getGar().getGrzTpRsh());
        }
        else {
            // COB_CODE: MOVE GRZ-TP-RSH
            //             TO (SF)-TP-RSH(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzTpRsh(ws.getGar().getGrzTpRsh());
        }
        // COB_CODE: IF GRZ-TP-INVST-NULL = HIGH-VALUES
        //                TO (SF)-TP-INVST-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-TP-INVST(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzTpInvst().getGrzTpInvstNullFormatted())) {
            // COB_CODE: MOVE GRZ-TP-INVST-NULL
            //             TO (SF)-TP-INVST-NULL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzTpInvst().setWgrzTpInvstNull(ws.getGar().getGrzTpInvst().getGrzTpInvstNull());
        }
        else {
            // COB_CODE: MOVE GRZ-TP-INVST
            //             TO (SF)-TP-INVST(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzTpInvst().setWgrzTpInvst(ws.getGar().getGrzTpInvst().getGrzTpInvst());
        }
        // COB_CODE: IF GRZ-MOD-PAG-GARCOL-NULL = HIGH-VALUES
        //                TO (SF)-MOD-PAG-GARCOL-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-MOD-PAG-GARCOL(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzModPagGarcolFormatted())) {
            // COB_CODE: MOVE GRZ-MOD-PAG-GARCOL-NULL
            //             TO (SF)-MOD-PAG-GARCOL-NULL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzModPagGarcol(ws.getGar().getGrzModPagGarcol());
        }
        else {
            // COB_CODE: MOVE GRZ-MOD-PAG-GARCOL
            //             TO (SF)-MOD-PAG-GARCOL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzModPagGarcol(ws.getGar().getGrzModPagGarcol());
        }
        // COB_CODE: IF GRZ-TP-PER-PRE-NULL = HIGH-VALUES
        //                TO (SF)-TP-PER-PRE-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-TP-PER-PRE(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzTpPerPreFormatted())) {
            // COB_CODE: MOVE GRZ-TP-PER-PRE-NULL
            //             TO (SF)-TP-PER-PRE-NULL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzTpPerPre(ws.getGar().getGrzTpPerPre());
        }
        else {
            // COB_CODE: MOVE GRZ-TP-PER-PRE
            //             TO (SF)-TP-PER-PRE(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzTpPerPre(ws.getGar().getGrzTpPerPre());
        }
        // COB_CODE: IF GRZ-ETA-AA-1O-ASSTO-NULL = HIGH-VALUES
        //                TO (SF)-ETA-AA-1O-ASSTO-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-ETA-AA-1O-ASSTO(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzEtaAa1oAssto().getGrzEtaAa1oAsstoNullFormatted())) {
            // COB_CODE: MOVE GRZ-ETA-AA-1O-ASSTO-NULL
            //             TO (SF)-ETA-AA-1O-ASSTO-NULL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzEtaAa1oAssto().setWgrzEtaAa1oAsstoNull(ws.getGar().getGrzEtaAa1oAssto().getGrzEtaAa1oAsstoNull());
        }
        else {
            // COB_CODE: MOVE GRZ-ETA-AA-1O-ASSTO
            //             TO (SF)-ETA-AA-1O-ASSTO(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzEtaAa1oAssto().setWgrzEtaAa1oAssto(ws.getGar().getGrzEtaAa1oAssto().getGrzEtaAa1oAssto());
        }
        // COB_CODE: IF GRZ-ETA-MM-1O-ASSTO-NULL = HIGH-VALUES
        //                TO (SF)-ETA-MM-1O-ASSTO-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-ETA-MM-1O-ASSTO(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzEtaMm1oAssto().getGrzEtaMm1oAsstoNullFormatted())) {
            // COB_CODE: MOVE GRZ-ETA-MM-1O-ASSTO-NULL
            //             TO (SF)-ETA-MM-1O-ASSTO-NULL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzEtaMm1oAssto().setWgrzEtaMm1oAsstoNull(ws.getGar().getGrzEtaMm1oAssto().getGrzEtaMm1oAsstoNull());
        }
        else {
            // COB_CODE: MOVE GRZ-ETA-MM-1O-ASSTO
            //             TO (SF)-ETA-MM-1O-ASSTO(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzEtaMm1oAssto().setWgrzEtaMm1oAssto(ws.getGar().getGrzEtaMm1oAssto().getGrzEtaMm1oAssto());
        }
        // COB_CODE: IF GRZ-ETA-AA-2O-ASSTO-NULL = HIGH-VALUES
        //                TO (SF)-ETA-AA-2O-ASSTO-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-ETA-AA-2O-ASSTO(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzEtaAa2oAssto().getGrzEtaAa2oAsstoNullFormatted())) {
            // COB_CODE: MOVE GRZ-ETA-AA-2O-ASSTO-NULL
            //             TO (SF)-ETA-AA-2O-ASSTO-NULL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzEtaAa2oAssto().setWgrzEtaAa2oAsstoNull(ws.getGar().getGrzEtaAa2oAssto().getGrzEtaAa2oAsstoNull());
        }
        else {
            // COB_CODE: MOVE GRZ-ETA-AA-2O-ASSTO
            //             TO (SF)-ETA-AA-2O-ASSTO(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzEtaAa2oAssto().setWgrzEtaAa2oAssto(ws.getGar().getGrzEtaAa2oAssto().getGrzEtaAa2oAssto());
        }
        // COB_CODE: IF GRZ-ETA-MM-2O-ASSTO-NULL = HIGH-VALUES
        //                TO (SF)-ETA-MM-2O-ASSTO-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-ETA-MM-2O-ASSTO(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzEtaMm2oAssto().getGrzEtaMm2oAsstoNullFormatted())) {
            // COB_CODE: MOVE GRZ-ETA-MM-2O-ASSTO-NULL
            //             TO (SF)-ETA-MM-2O-ASSTO-NULL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzEtaMm2oAssto().setWgrzEtaMm2oAsstoNull(ws.getGar().getGrzEtaMm2oAssto().getGrzEtaMm2oAsstoNull());
        }
        else {
            // COB_CODE: MOVE GRZ-ETA-MM-2O-ASSTO
            //             TO (SF)-ETA-MM-2O-ASSTO(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzEtaMm2oAssto().setWgrzEtaMm2oAssto(ws.getGar().getGrzEtaMm2oAssto().getGrzEtaMm2oAssto());
        }
        // COB_CODE: IF GRZ-ETA-AA-3O-ASSTO-NULL = HIGH-VALUES
        //                TO (SF)-ETA-AA-3O-ASSTO-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-ETA-AA-3O-ASSTO(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzEtaAa3oAssto().getGrzEtaAa3oAsstoNullFormatted())) {
            // COB_CODE: MOVE GRZ-ETA-AA-3O-ASSTO-NULL
            //             TO (SF)-ETA-AA-3O-ASSTO-NULL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzEtaAa3oAssto().setWgrzEtaAa3oAsstoNull(ws.getGar().getGrzEtaAa3oAssto().getGrzEtaAa3oAsstoNull());
        }
        else {
            // COB_CODE: MOVE GRZ-ETA-AA-3O-ASSTO
            //             TO (SF)-ETA-AA-3O-ASSTO(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzEtaAa3oAssto().setWgrzEtaAa3oAssto(ws.getGar().getGrzEtaAa3oAssto().getGrzEtaAa3oAssto());
        }
        // COB_CODE: IF GRZ-ETA-MM-3O-ASSTO-NULL = HIGH-VALUES
        //                TO (SF)-ETA-MM-3O-ASSTO-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-ETA-MM-3O-ASSTO(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzEtaMm3oAssto().getGrzEtaMm3oAsstoNullFormatted())) {
            // COB_CODE: MOVE GRZ-ETA-MM-3O-ASSTO-NULL
            //             TO (SF)-ETA-MM-3O-ASSTO-NULL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzEtaMm3oAssto().setWgrzEtaMm3oAsstoNull(ws.getGar().getGrzEtaMm3oAssto().getGrzEtaMm3oAsstoNull());
        }
        else {
            // COB_CODE: MOVE GRZ-ETA-MM-3O-ASSTO
            //             TO (SF)-ETA-MM-3O-ASSTO(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzEtaMm3oAssto().setWgrzEtaMm3oAssto(ws.getGar().getGrzEtaMm3oAssto().getGrzEtaMm3oAssto());
        }
        // COB_CODE: IF GRZ-TP-EMIS-PUR-NULL = HIGH-VALUES
        //                TO (SF)-TP-EMIS-PUR-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-TP-EMIS-PUR(IX-TAB-GRZ)
        //           END-IF
        if (Conditions.eq(ws.getGar().getGrzTpEmisPur(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE GRZ-TP-EMIS-PUR-NULL
            //             TO (SF)-TP-EMIS-PUR-NULL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzTpEmisPur(ws.getGar().getGrzTpEmisPur());
        }
        else {
            // COB_CODE: MOVE GRZ-TP-EMIS-PUR
            //             TO (SF)-TP-EMIS-PUR(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzTpEmisPur(ws.getGar().getGrzTpEmisPur());
        }
        // COB_CODE: IF GRZ-ETA-A-SCAD-NULL = HIGH-VALUES
        //                TO (SF)-ETA-A-SCAD-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-ETA-A-SCAD(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzEtaAScad().getGrzEtaAScadNullFormatted())) {
            // COB_CODE: MOVE GRZ-ETA-A-SCAD-NULL
            //             TO (SF)-ETA-A-SCAD-NULL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzEtaAScad().setWgrzEtaAScadNull(ws.getGar().getGrzEtaAScad().getGrzEtaAScadNull());
        }
        else {
            // COB_CODE: MOVE GRZ-ETA-A-SCAD
            //             TO (SF)-ETA-A-SCAD(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzEtaAScad().setWgrzEtaAScad(ws.getGar().getGrzEtaAScad().getGrzEtaAScad());
        }
        // COB_CODE: IF GRZ-TP-CALC-PRE-PRSTZ-NULL = HIGH-VALUES
        //                TO (SF)-TP-CALC-PRE-PRSTZ-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-TP-CALC-PRE-PRSTZ(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzTpCalcPrePrstzFormatted())) {
            // COB_CODE: MOVE GRZ-TP-CALC-PRE-PRSTZ-NULL
            //             TO (SF)-TP-CALC-PRE-PRSTZ-NULL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzTpCalcPrePrstz(ws.getGar().getGrzTpCalcPrePrstz());
        }
        else {
            // COB_CODE: MOVE GRZ-TP-CALC-PRE-PRSTZ
            //             TO (SF)-TP-CALC-PRE-PRSTZ(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzTpCalcPrePrstz(ws.getGar().getGrzTpCalcPrePrstz());
        }
        // COB_CODE: IF GRZ-TP-PRE-NULL = HIGH-VALUES
        //                TO (SF)-TP-PRE-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-TP-PRE(IX-TAB-GRZ)
        //           END-IF
        if (Conditions.eq(ws.getGar().getGrzTpPre(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE GRZ-TP-PRE-NULL
            //             TO (SF)-TP-PRE-NULL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzTpPre(ws.getGar().getGrzTpPre());
        }
        else {
            // COB_CODE: MOVE GRZ-TP-PRE
            //             TO (SF)-TP-PRE(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzTpPre(ws.getGar().getGrzTpPre());
        }
        // COB_CODE: IF GRZ-TP-DUR-NULL = HIGH-VALUES
        //                TO (SF)-TP-DUR-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-TP-DUR(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzTpDurFormatted())) {
            // COB_CODE: MOVE GRZ-TP-DUR-NULL
            //             TO (SF)-TP-DUR-NULL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzTpDur(ws.getGar().getGrzTpDur());
        }
        else {
            // COB_CODE: MOVE GRZ-TP-DUR
            //             TO (SF)-TP-DUR(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzTpDur(ws.getGar().getGrzTpDur());
        }
        // COB_CODE: IF GRZ-DUR-AA-NULL = HIGH-VALUES
        //                TO (SF)-DUR-AA-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-DUR-AA(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzDurAa().getGrzDurAaNullFormatted())) {
            // COB_CODE: MOVE GRZ-DUR-AA-NULL
            //             TO (SF)-DUR-AA-NULL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzDurAa().setWgrzDurAaNull(ws.getGar().getGrzDurAa().getGrzDurAaNull());
        }
        else {
            // COB_CODE: MOVE GRZ-DUR-AA
            //             TO (SF)-DUR-AA(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzDurAa().setWgrzDurAa(ws.getGar().getGrzDurAa().getGrzDurAa());
        }
        // COB_CODE: IF GRZ-DUR-MM-NULL = HIGH-VALUES
        //                TO (SF)-DUR-MM-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-DUR-MM(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzDurMm().getGrzDurMmNullFormatted())) {
            // COB_CODE: MOVE GRZ-DUR-MM-NULL
            //             TO (SF)-DUR-MM-NULL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzDurMm().setWgrzDurMmNull(ws.getGar().getGrzDurMm().getGrzDurMmNull());
        }
        else {
            // COB_CODE: MOVE GRZ-DUR-MM
            //             TO (SF)-DUR-MM(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzDurMm().setWgrzDurMm(ws.getGar().getGrzDurMm().getGrzDurMm());
        }
        // COB_CODE: IF GRZ-DUR-GG-NULL = HIGH-VALUES
        //                TO (SF)-DUR-GG-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-DUR-GG(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzDurGg().getGrzDurGgNullFormatted())) {
            // COB_CODE: MOVE GRZ-DUR-GG-NULL
            //             TO (SF)-DUR-GG-NULL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzDurGg().setWgrzDurGgNull(ws.getGar().getGrzDurGg().getGrzDurGgNull());
        }
        else {
            // COB_CODE: MOVE GRZ-DUR-GG
            //             TO (SF)-DUR-GG(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzDurGg().setWgrzDurGg(ws.getGar().getGrzDurGg().getGrzDurGg());
        }
        // COB_CODE: IF GRZ-NUM-AA-PAG-PRE-NULL = HIGH-VALUES
        //                TO (SF)-NUM-AA-PAG-PRE-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-NUM-AA-PAG-PRE(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzNumAaPagPre().getGrzNumAaPagPreNullFormatted())) {
            // COB_CODE: MOVE GRZ-NUM-AA-PAG-PRE-NULL
            //             TO (SF)-NUM-AA-PAG-PRE-NULL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzNumAaPagPre().setWgrzNumAaPagPreNull(ws.getGar().getGrzNumAaPagPre().getGrzNumAaPagPreNull());
        }
        else {
            // COB_CODE: MOVE GRZ-NUM-AA-PAG-PRE
            //             TO (SF)-NUM-AA-PAG-PRE(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzNumAaPagPre().setWgrzNumAaPagPre(ws.getGar().getGrzNumAaPagPre().getGrzNumAaPagPre());
        }
        // COB_CODE: IF GRZ-AA-PAG-PRE-UNI-NULL = HIGH-VALUES
        //                TO (SF)-AA-PAG-PRE-UNI-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-AA-PAG-PRE-UNI(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzAaPagPreUni().getGrzAaPagPreUniNullFormatted())) {
            // COB_CODE: MOVE GRZ-AA-PAG-PRE-UNI-NULL
            //             TO (SF)-AA-PAG-PRE-UNI-NULL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzAaPagPreUni().setWgrzAaPagPreUniNull(ws.getGar().getGrzAaPagPreUni().getGrzAaPagPreUniNull());
        }
        else {
            // COB_CODE: MOVE GRZ-AA-PAG-PRE-UNI
            //             TO (SF)-AA-PAG-PRE-UNI(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzAaPagPreUni().setWgrzAaPagPreUni(ws.getGar().getGrzAaPagPreUni().getGrzAaPagPreUni());
        }
        // COB_CODE: IF GRZ-MM-PAG-PRE-UNI-NULL = HIGH-VALUES
        //                TO (SF)-MM-PAG-PRE-UNI-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-MM-PAG-PRE-UNI(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzMmPagPreUni().getGrzMmPagPreUniNullFormatted())) {
            // COB_CODE: MOVE GRZ-MM-PAG-PRE-UNI-NULL
            //             TO (SF)-MM-PAG-PRE-UNI-NULL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzMmPagPreUni().setWgrzMmPagPreUniNull(ws.getGar().getGrzMmPagPreUni().getGrzMmPagPreUniNull());
        }
        else {
            // COB_CODE: MOVE GRZ-MM-PAG-PRE-UNI
            //             TO (SF)-MM-PAG-PRE-UNI(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzMmPagPreUni().setWgrzMmPagPreUni(ws.getGar().getGrzMmPagPreUni().getGrzMmPagPreUni());
        }
        // COB_CODE: IF GRZ-FRAZ-INI-EROG-REN-NULL = HIGH-VALUES
        //                TO (SF)-FRAZ-INI-EROG-REN-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-FRAZ-INI-EROG-REN(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzFrazIniErogRen().getGrzFrazIniErogRenNullFormatted())) {
            // COB_CODE: MOVE GRZ-FRAZ-INI-EROG-REN-NULL
            //             TO (SF)-FRAZ-INI-EROG-REN-NULL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzFrazIniErogRen().setWgrzFrazIniErogRenNull(ws.getGar().getGrzFrazIniErogRen().getGrzFrazIniErogRenNull());
        }
        else {
            // COB_CODE: MOVE GRZ-FRAZ-INI-EROG-REN
            //             TO (SF)-FRAZ-INI-EROG-REN(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzFrazIniErogRen().setWgrzFrazIniErogRen(ws.getGar().getGrzFrazIniErogRen().getGrzFrazIniErogRen());
        }
        // COB_CODE: IF GRZ-MM-1O-RAT-NULL = HIGH-VALUES
        //                TO (SF)-MM-1O-RAT-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-MM-1O-RAT(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzMm1oRat().getGrzMm1oRatNullFormatted())) {
            // COB_CODE: MOVE GRZ-MM-1O-RAT-NULL
            //             TO (SF)-MM-1O-RAT-NULL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzMm1oRat().setWgrzMm1oRatNull(ws.getGar().getGrzMm1oRat().getGrzMm1oRatNull());
        }
        else {
            // COB_CODE: MOVE GRZ-MM-1O-RAT
            //             TO (SF)-MM-1O-RAT(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzMm1oRat().setWgrzMm1oRat(ws.getGar().getGrzMm1oRat().getGrzMm1oRat());
        }
        // COB_CODE: IF GRZ-PC-1O-RAT-NULL = HIGH-VALUES
        //                TO (SF)-PC-1O-RAT-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-PC-1O-RAT(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzPc1oRat().getGrzPc1oRatNullFormatted())) {
            // COB_CODE: MOVE GRZ-PC-1O-RAT-NULL
            //             TO (SF)-PC-1O-RAT-NULL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzPc1oRat().setWgrzPc1oRatNull(ws.getGar().getGrzPc1oRat().getGrzPc1oRatNull());
        }
        else {
            // COB_CODE: MOVE GRZ-PC-1O-RAT
            //             TO (SF)-PC-1O-RAT(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzPc1oRat().setWgrzPc1oRat(Trunc.toDecimal(ws.getGar().getGrzPc1oRat().getGrzPc1oRat(), 6, 3));
        }
        // COB_CODE: IF GRZ-TP-PRSTZ-ASSTA-NULL = HIGH-VALUES
        //                TO (SF)-TP-PRSTZ-ASSTA-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-TP-PRSTZ-ASSTA(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzTpPrstzAsstaFormatted())) {
            // COB_CODE: MOVE GRZ-TP-PRSTZ-ASSTA-NULL
            //             TO (SF)-TP-PRSTZ-ASSTA-NULL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzTpPrstzAssta(ws.getGar().getGrzTpPrstzAssta());
        }
        else {
            // COB_CODE: MOVE GRZ-TP-PRSTZ-ASSTA
            //             TO (SF)-TP-PRSTZ-ASSTA(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzTpPrstzAssta(ws.getGar().getGrzTpPrstzAssta());
        }
        // COB_CODE: IF GRZ-DT-END-CARZ-NULL = HIGH-VALUES
        //                TO (SF)-DT-END-CARZ-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-DT-END-CARZ(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzDtEndCarz().getGrzDtEndCarzNullFormatted())) {
            // COB_CODE: MOVE GRZ-DT-END-CARZ-NULL
            //             TO (SF)-DT-END-CARZ-NULL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzDtEndCarz().setWgrzDtEndCarzNull(ws.getGar().getGrzDtEndCarz().getGrzDtEndCarzNull());
        }
        else {
            // COB_CODE: MOVE GRZ-DT-END-CARZ
            //             TO (SF)-DT-END-CARZ(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzDtEndCarz().setWgrzDtEndCarz(ws.getGar().getGrzDtEndCarz().getGrzDtEndCarz());
        }
        // COB_CODE: IF GRZ-PC-RIP-PRE-NULL = HIGH-VALUES
        //                TO (SF)-PC-RIP-PRE-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-PC-RIP-PRE(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzPcRipPre().getGrzPcRipPreNullFormatted())) {
            // COB_CODE: MOVE GRZ-PC-RIP-PRE-NULL
            //             TO (SF)-PC-RIP-PRE-NULL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzPcRipPre().setWgrzPcRipPreNull(ws.getGar().getGrzPcRipPre().getGrzPcRipPreNull());
        }
        else {
            // COB_CODE: MOVE GRZ-PC-RIP-PRE
            //             TO (SF)-PC-RIP-PRE(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzPcRipPre().setWgrzPcRipPre(Trunc.toDecimal(ws.getGar().getGrzPcRipPre().getGrzPcRipPre(), 6, 3));
        }
        // COB_CODE: IF GRZ-COD-FND-NULL = HIGH-VALUES
        //                TO (SF)-COD-FND-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-COD-FND(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzCodFndFormatted())) {
            // COB_CODE: MOVE GRZ-COD-FND-NULL
            //             TO (SF)-COD-FND-NULL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzCodFnd(ws.getGar().getGrzCodFnd());
        }
        else {
            // COB_CODE: MOVE GRZ-COD-FND
            //             TO (SF)-COD-FND(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzCodFnd(ws.getGar().getGrzCodFnd());
        }
        // COB_CODE: IF GRZ-AA-REN-CER-NULL = HIGH-VALUES
        //                TO (SF)-AA-REN-CER-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-AA-REN-CER(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzAaRenCerFormatted())) {
            // COB_CODE: MOVE GRZ-AA-REN-CER-NULL
            //             TO (SF)-AA-REN-CER-NULL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzAaRenCer(ws.getGar().getGrzAaRenCer());
        }
        else {
            // COB_CODE: MOVE GRZ-AA-REN-CER
            //             TO (SF)-AA-REN-CER(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzAaRenCer(ws.getGar().getGrzAaRenCer());
        }
        // COB_CODE: IF GRZ-PC-REVRSB-NULL = HIGH-VALUES
        //                TO (SF)-PC-REVRSB-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-PC-REVRSB(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzPcRevrsb().getGrzPcRevrsbNullFormatted())) {
            // COB_CODE: MOVE GRZ-PC-REVRSB-NULL
            //             TO (SF)-PC-REVRSB-NULL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzPcRevrsb().setWgrzPcRevrsbNull(ws.getGar().getGrzPcRevrsb().getGrzPcRevrsbNull());
        }
        else {
            // COB_CODE: MOVE GRZ-PC-REVRSB
            //             TO (SF)-PC-REVRSB(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzPcRevrsb().setWgrzPcRevrsb(Trunc.toDecimal(ws.getGar().getGrzPcRevrsb().getGrzPcRevrsb(), 6, 3));
        }
        // COB_CODE: IF GRZ-TP-PC-RIP-NULL = HIGH-VALUES
        //                TO (SF)-TP-PC-RIP-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-TP-PC-RIP(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzTpPcRipFormatted())) {
            // COB_CODE: MOVE GRZ-TP-PC-RIP-NULL
            //             TO (SF)-TP-PC-RIP-NULL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzTpPcRip(ws.getGar().getGrzTpPcRip());
        }
        else {
            // COB_CODE: MOVE GRZ-TP-PC-RIP
            //             TO (SF)-TP-PC-RIP(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzTpPcRip(ws.getGar().getGrzTpPcRip());
        }
        // COB_CODE: IF GRZ-PC-OPZ-NULL = HIGH-VALUES
        //                TO (SF)-PC-OPZ-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-PC-OPZ(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzPcOpz().getGrzPcOpzNullFormatted())) {
            // COB_CODE: MOVE GRZ-PC-OPZ-NULL
            //             TO (SF)-PC-OPZ-NULL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzPcOpz().setWgrzPcOpzNull(ws.getGar().getGrzPcOpz().getGrzPcOpzNull());
        }
        else {
            // COB_CODE: MOVE GRZ-PC-OPZ
            //             TO (SF)-PC-OPZ(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzPcOpz().setWgrzPcOpz(Trunc.toDecimal(ws.getGar().getGrzPcOpz().getGrzPcOpz(), 6, 3));
        }
        // COB_CODE: IF GRZ-TP-IAS-NULL = HIGH-VALUES
        //                TO (SF)-TP-IAS-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-TP-IAS(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzTpIasFormatted())) {
            // COB_CODE: MOVE GRZ-TP-IAS-NULL
            //             TO (SF)-TP-IAS-NULL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzTpIas(ws.getGar().getGrzTpIas());
        }
        else {
            // COB_CODE: MOVE GRZ-TP-IAS
            //             TO (SF)-TP-IAS(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzTpIas(ws.getGar().getGrzTpIas());
        }
        // COB_CODE: IF GRZ-TP-STAB-NULL = HIGH-VALUES
        //                TO (SF)-TP-STAB-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-TP-STAB(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzTpStabFormatted())) {
            // COB_CODE: MOVE GRZ-TP-STAB-NULL
            //             TO (SF)-TP-STAB-NULL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzTpStab(ws.getGar().getGrzTpStab());
        }
        else {
            // COB_CODE: MOVE GRZ-TP-STAB
            //             TO (SF)-TP-STAB(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzTpStab(ws.getGar().getGrzTpStab());
        }
        // COB_CODE: IF GRZ-TP-ADEG-PRE-NULL = HIGH-VALUES
        //                TO (SF)-TP-ADEG-PRE-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-TP-ADEG-PRE(IX-TAB-GRZ)
        //           END-IF
        if (Conditions.eq(ws.getGar().getGrzTpAdegPre(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE GRZ-TP-ADEG-PRE-NULL
            //             TO (SF)-TP-ADEG-PRE-NULL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzTpAdegPre(ws.getGar().getGrzTpAdegPre());
        }
        else {
            // COB_CODE: MOVE GRZ-TP-ADEG-PRE
            //             TO (SF)-TP-ADEG-PRE(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzTpAdegPre(ws.getGar().getGrzTpAdegPre());
        }
        // COB_CODE: IF GRZ-DT-VARZ-TP-IAS-NULL = HIGH-VALUES
        //                TO (SF)-DT-VARZ-TP-IAS-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-DT-VARZ-TP-IAS(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzDtVarzTpIas().getGrzDtVarzTpIasNullFormatted())) {
            // COB_CODE: MOVE GRZ-DT-VARZ-TP-IAS-NULL
            //             TO (SF)-DT-VARZ-TP-IAS-NULL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzDtVarzTpIas().setWgrzDtVarzTpIasNull(ws.getGar().getGrzDtVarzTpIas().getGrzDtVarzTpIasNull());
        }
        else {
            // COB_CODE: MOVE GRZ-DT-VARZ-TP-IAS
            //             TO (SF)-DT-VARZ-TP-IAS(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzDtVarzTpIas().setWgrzDtVarzTpIas(ws.getGar().getGrzDtVarzTpIas().getGrzDtVarzTpIas());
        }
        // COB_CODE: IF GRZ-FRAZ-DECR-CPT-NULL = HIGH-VALUES
        //                TO (SF)-FRAZ-DECR-CPT-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-FRAZ-DECR-CPT(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzFrazDecrCpt().getGrzFrazDecrCptNullFormatted())) {
            // COB_CODE: MOVE GRZ-FRAZ-DECR-CPT-NULL
            //             TO (SF)-FRAZ-DECR-CPT-NULL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzFrazDecrCpt().setWgrzFrazDecrCptNull(ws.getGar().getGrzFrazDecrCpt().getGrzFrazDecrCptNull());
        }
        else {
            // COB_CODE: MOVE GRZ-FRAZ-DECR-CPT
            //             TO (SF)-FRAZ-DECR-CPT(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzFrazDecrCpt().setWgrzFrazDecrCpt(ws.getGar().getGrzFrazDecrCpt().getGrzFrazDecrCpt());
        }
        // COB_CODE: IF GRZ-COD-TRAT-RIASS-NULL = HIGH-VALUES
        //                TO (SF)-COD-TRAT-RIASS-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-COD-TRAT-RIASS(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzCodTratRiassFormatted())) {
            // COB_CODE: MOVE GRZ-COD-TRAT-RIASS-NULL
            //             TO (SF)-COD-TRAT-RIASS-NULL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzCodTratRiass(ws.getGar().getGrzCodTratRiass());
        }
        else {
            // COB_CODE: MOVE GRZ-COD-TRAT-RIASS
            //             TO (SF)-COD-TRAT-RIASS(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzCodTratRiass(ws.getGar().getGrzCodTratRiass());
        }
        // COB_CODE: IF GRZ-TP-DT-EMIS-RIASS-NULL = HIGH-VALUES
        //                TO (SF)-TP-DT-EMIS-RIASS-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-TP-DT-EMIS-RIASS(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzTpDtEmisRiassFormatted())) {
            // COB_CODE: MOVE GRZ-TP-DT-EMIS-RIASS-NULL
            //             TO (SF)-TP-DT-EMIS-RIASS-NULL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzTpDtEmisRiass(ws.getGar().getGrzTpDtEmisRiass());
        }
        else {
            // COB_CODE: MOVE GRZ-TP-DT-EMIS-RIASS
            //             TO (SF)-TP-DT-EMIS-RIASS(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzTpDtEmisRiass(ws.getGar().getGrzTpDtEmisRiass());
        }
        // COB_CODE: IF GRZ-TP-CESS-RIASS-NULL = HIGH-VALUES
        //                TO (SF)-TP-CESS-RIASS-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-TP-CESS-RIASS(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzTpCessRiassFormatted())) {
            // COB_CODE: MOVE GRZ-TP-CESS-RIASS-NULL
            //             TO (SF)-TP-CESS-RIASS-NULL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzTpCessRiass(ws.getGar().getGrzTpCessRiass());
        }
        else {
            // COB_CODE: MOVE GRZ-TP-CESS-RIASS
            //             TO (SF)-TP-CESS-RIASS(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzTpCessRiass(ws.getGar().getGrzTpCessRiass());
        }
        // COB_CODE: MOVE GRZ-DS-RIGA
        //             TO (SF)-DS-RIGA(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzDsRiga(ws.getGar().getGrzDsRiga());
        // COB_CODE: MOVE GRZ-DS-OPER-SQL
        //             TO (SF)-DS-OPER-SQL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzDsOperSql(ws.getGar().getGrzDsOperSql());
        // COB_CODE: MOVE GRZ-DS-VER
        //             TO (SF)-DS-VER(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzDsVer(ws.getGar().getGrzDsVer());
        // COB_CODE: MOVE GRZ-DS-TS-INI-CPTZ
        //             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzDsTsIniCptz(ws.getGar().getGrzDsTsIniCptz());
        // COB_CODE: MOVE GRZ-DS-TS-END-CPTZ
        //             TO (SF)-DS-TS-END-CPTZ(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzDsTsEndCptz(ws.getGar().getGrzDsTsEndCptz());
        // COB_CODE: MOVE GRZ-DS-UTENTE
        //             TO (SF)-DS-UTENTE(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzDsUtente(ws.getGar().getGrzDsUtente());
        // COB_CODE: MOVE GRZ-DS-STATO-ELAB
        //             TO (SF)-DS-STATO-ELAB(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzDsStatoElab(ws.getGar().getGrzDsStatoElab());
        // COB_CODE: IF GRZ-AA-STAB-NULL = HIGH-VALUES
        //                TO (SF)-AA-STAB-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-AA-STAB(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzAaStab().getGrzAaStabNullFormatted())) {
            // COB_CODE: MOVE GRZ-AA-STAB-NULL
            //             TO (SF)-AA-STAB-NULL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzAaStab().setWgrzAaStabNull(ws.getGar().getGrzAaStab().getGrzAaStabNull());
        }
        else {
            // COB_CODE: MOVE GRZ-AA-STAB
            //             TO (SF)-AA-STAB(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzAaStab().setWgrzAaStab(ws.getGar().getGrzAaStab().getGrzAaStab());
        }
        // COB_CODE: IF GRZ-TS-STAB-LIMITATA-NULL = HIGH-VALUES
        //                TO (SF)-TS-STAB-LIMITATA-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-TS-STAB-LIMITATA(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzTsStabLimitata().getGrzTsStabLimitataNullFormatted())) {
            // COB_CODE: MOVE GRZ-TS-STAB-LIMITATA-NULL
            //             TO (SF)-TS-STAB-LIMITATA-NULL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzTsStabLimitata().setWgrzTsStabLimitataNull(ws.getGar().getGrzTsStabLimitata().getGrzTsStabLimitataNull());
        }
        else {
            // COB_CODE: MOVE GRZ-TS-STAB-LIMITATA
            //             TO (SF)-TS-STAB-LIMITATA(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzTsStabLimitata().setWgrzTsStabLimitata(Trunc.toDecimal(ws.getGar().getGrzTsStabLimitata().getGrzTsStabLimitata(), 14, 9));
        }
        // COB_CODE: IF GRZ-DT-PRESC-NULL = HIGH-VALUES
        //                TO (SF)-DT-PRESC-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-DT-PRESC(IX-TAB-GRZ)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getGar().getGrzDtPresc().getGrzDtPrescNullFormatted())) {
            // COB_CODE: MOVE GRZ-DT-PRESC-NULL
            //             TO (SF)-DT-PRESC-NULL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzDtPresc().setWgrzDtPrescNull(ws.getGar().getGrzDtPresc().getGrzDtPrescNull());
        }
        else {
            // COB_CODE: MOVE GRZ-DT-PRESC
            //             TO (SF)-DT-PRESC(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzDtPresc().setWgrzDtPresc(ws.getGar().getGrzDtPresc().getGrzDtPresc());
        }
        // COB_CODE: IF GRZ-RSH-INVST-NULL = HIGH-VALUES
        //                TO (SF)-RSH-INVST-NULL(IX-TAB-GRZ)
        //           ELSE
        //                TO (SF)-RSH-INVST(IX-TAB-GRZ)
        //           END-IF
        if (Conditions.eq(ws.getGar().getGrzRshInvst(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE GRZ-RSH-INVST-NULL
            //             TO (SF)-RSH-INVST-NULL(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzRshInvst(ws.getGar().getGrzRshInvst());
        }
        else {
            // COB_CODE: MOVE GRZ-RSH-INVST
            //             TO (SF)-RSH-INVST(IX-TAB-GRZ)
            ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzRshInvst(ws.getGar().getGrzRshInvst());
        }
        // COB_CODE: MOVE GRZ-TP-RAMO-BILA
        //             TO (SF)-TP-RAMO-BILA(IX-TAB-GRZ).
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzTpRamoBila(ws.getGar().getGrzTpRamoBila());
    }

    /**Original name: VALORIZZA-OUTPUT-TGA<br>
	 * <pre>  --> TRANCHE DI GARANZIA
	 * ------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    VALORIZZA OUTPUT COPY LCCVTGA3
	 *    ULTIMO AGG. 03 GIU 2019
	 * ------------------------------------------------------------</pre>*/
    private void valorizzaOutputTga() {
        // COB_CODE: MOVE TGA-ID-TRCH-DI-GAR
        //             TO (SF)-ID-PTF(IX-TAB-TGA)
        ws.getWtgaAreaTranche().getWtgaTab().setIdPtf(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaIdTrchDiGar());
        // COB_CODE: MOVE TGA-ID-TRCH-DI-GAR
        //             TO (SF)-ID-TRCH-DI-GAR(IX-TAB-TGA)
        ws.getWtgaAreaTranche().getWtgaTab().setIdTrchDiGar(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaIdTrchDiGar());
        // COB_CODE: MOVE TGA-ID-GAR
        //             TO (SF)-ID-GAR(IX-TAB-TGA)
        ws.getWtgaAreaTranche().getWtgaTab().setIdGar(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaIdGar());
        // COB_CODE: MOVE TGA-ID-ADES
        //             TO (SF)-ID-ADES(IX-TAB-TGA)
        ws.getWtgaAreaTranche().getWtgaTab().setIdAdes(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaIdAdes());
        // COB_CODE: MOVE TGA-ID-POLI
        //             TO (SF)-ID-POLI(IX-TAB-TGA)
        ws.getWtgaAreaTranche().getWtgaTab().setIdPoli(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaIdPoli());
        // COB_CODE: MOVE TGA-ID-MOVI-CRZ
        //             TO (SF)-ID-MOVI-CRZ(IX-TAB-TGA)
        ws.getWtgaAreaTranche().getWtgaTab().setIdMoviCrz(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaIdMoviCrz());
        // COB_CODE: IF TGA-ID-MOVI-CHIU-NULL = HIGH-VALUES
        //                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ID-MOVI-CHIU(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaIdMoviChiu().getTgaIdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE TGA-ID-MOVI-CHIU-NULL
            //             TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setIdMoviChiuNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaIdMoviChiu().getTgaIdMoviChiuNull());
        }
        else {
            // COB_CODE: MOVE TGA-ID-MOVI-CHIU
            //             TO (SF)-ID-MOVI-CHIU(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setIdMoviChiu(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaIdMoviChiu().getTgaIdMoviChiu());
        }
        // COB_CODE: MOVE TGA-DT-INI-EFF
        //             TO (SF)-DT-INI-EFF(IX-TAB-TGA)
        ws.getWtgaAreaTranche().getWtgaTab().setDtIniEff(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaDtIniEff());
        // COB_CODE: MOVE TGA-DT-END-EFF
        //             TO (SF)-DT-END-EFF(IX-TAB-TGA)
        ws.getWtgaAreaTranche().getWtgaTab().setDtEndEff(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaDtEndEff());
        // COB_CODE: MOVE TGA-COD-COMP-ANIA
        //             TO (SF)-COD-COMP-ANIA(IX-TAB-TGA)
        ws.getWtgaAreaTranche().getWtgaTab().setCodCompAnia(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaCodCompAnia());
        // COB_CODE: MOVE TGA-DT-DECOR
        //             TO (SF)-DT-DECOR(IX-TAB-TGA)
        ws.getWtgaAreaTranche().getWtgaTab().setDtDecor(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaDtDecor());
        // COB_CODE: IF TGA-DT-SCAD-NULL = HIGH-VALUES
        //                TO (SF)-DT-SCAD-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-DT-SCAD(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaDtScad().getTgaDtScadNullFormatted())) {
            // COB_CODE: MOVE TGA-DT-SCAD-NULL
            //             TO (SF)-DT-SCAD-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setDtScadNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaDtScad().getTgaDtScadNull());
        }
        else {
            // COB_CODE: MOVE TGA-DT-SCAD
            //             TO (SF)-DT-SCAD(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setDtScad(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaDtScad().getTgaDtScad());
        }
        // COB_CODE: IF TGA-IB-OGG-NULL = HIGH-VALUES
        //                TO (SF)-IB-OGG-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IB-OGG(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaIbOgg(), TrchDiGarIvvs0216.Len.TGA_IB_OGG)) {
            // COB_CODE: MOVE TGA-IB-OGG-NULL
            //             TO (SF)-IB-OGG-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setIbOggNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaIbOgg());
        }
        else {
            // COB_CODE: MOVE TGA-IB-OGG
            //             TO (SF)-IB-OGG(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setIbOgg(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaIbOgg());
        }
        // COB_CODE: MOVE TGA-TP-RGM-FISC
        //             TO (SF)-TP-RGM-FISC(IX-TAB-TGA)
        ws.getWtgaAreaTranche().getWtgaTab().setTpRgmFisc(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaTpRgmFisc());
        // COB_CODE: IF TGA-DT-EMIS-NULL = HIGH-VALUES
        //                TO (SF)-DT-EMIS-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-DT-EMIS(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaDtEmis().getTgaDtEmisNullFormatted())) {
            // COB_CODE: MOVE TGA-DT-EMIS-NULL
            //             TO (SF)-DT-EMIS-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setDtEmisNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaDtEmis().getTgaDtEmisNull());
        }
        else {
            // COB_CODE: MOVE TGA-DT-EMIS
            //             TO (SF)-DT-EMIS(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setDtEmis(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaDtEmis().getTgaDtEmis());
        }
        // COB_CODE: MOVE TGA-TP-TRCH
        //             TO (SF)-TP-TRCH(IX-TAB-TGA)
        ws.getWtgaAreaTranche().getWtgaTab().setTpTrch(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaTpTrch());
        // COB_CODE: IF TGA-DUR-AA-NULL = HIGH-VALUES
        //                TO (SF)-DUR-AA-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-DUR-AA(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaDurAa().getTgaDurAaNullFormatted())) {
            // COB_CODE: MOVE TGA-DUR-AA-NULL
            //             TO (SF)-DUR-AA-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setDurAaNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaDurAa().getTgaDurAaNull());
        }
        else {
            // COB_CODE: MOVE TGA-DUR-AA
            //             TO (SF)-DUR-AA(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setDurAa(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaDurAa().getTgaDurAa());
        }
        // COB_CODE: IF TGA-DUR-MM-NULL = HIGH-VALUES
        //                TO (SF)-DUR-MM-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-DUR-MM(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaDurMm().getTgaDurMmNullFormatted())) {
            // COB_CODE: MOVE TGA-DUR-MM-NULL
            //             TO (SF)-DUR-MM-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setDurMmNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaDurMm().getTgaDurMmNull());
        }
        else {
            // COB_CODE: MOVE TGA-DUR-MM
            //             TO (SF)-DUR-MM(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setDurMm(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaDurMm().getTgaDurMm());
        }
        // COB_CODE: IF TGA-DUR-GG-NULL = HIGH-VALUES
        //                TO (SF)-DUR-GG-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-DUR-GG(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaDurGg().getTgaDurGgNullFormatted())) {
            // COB_CODE: MOVE TGA-DUR-GG-NULL
            //             TO (SF)-DUR-GG-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setDurGgNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaDurGg().getTgaDurGgNull());
        }
        else {
            // COB_CODE: MOVE TGA-DUR-GG
            //             TO (SF)-DUR-GG(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setDurGg(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaDurGg().getTgaDurGg());
        }
        // COB_CODE: IF TGA-PRE-CASO-MOR-NULL = HIGH-VALUES
        //                TO (SF)-PRE-CASO-MOR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-CASO-MOR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPreCasoMor().getTgaPreCasoMorNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-CASO-MOR-NULL
            //             TO (SF)-PRE-CASO-MOR-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setPreCasoMorNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaPreCasoMor().getTgaPreCasoMorNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-CASO-MOR
            //             TO (SF)-PRE-CASO-MOR(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setPreCasoMor(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaPreCasoMor().getTgaPreCasoMor(), 15, 3));
        }
        // COB_CODE: IF TGA-PC-INTR-RIAT-NULL = HIGH-VALUES
        //                TO (SF)-PC-INTR-RIAT-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PC-INTR-RIAT(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPcIntrRiat().getTgaPcIntrRiatNullFormatted())) {
            // COB_CODE: MOVE TGA-PC-INTR-RIAT-NULL
            //             TO (SF)-PC-INTR-RIAT-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setPcIntrRiatNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaPcIntrRiat().getTgaPcIntrRiatNull());
        }
        else {
            // COB_CODE: MOVE TGA-PC-INTR-RIAT
            //             TO (SF)-PC-INTR-RIAT(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setPcIntrRiat(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaPcIntrRiat().getTgaPcIntrRiat(), 6, 3));
        }
        // COB_CODE: IF TGA-IMP-BNS-ANTIC-NULL = HIGH-VALUES
        //                TO (SF)-IMP-BNS-ANTIC-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-BNS-ANTIC(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpBnsAntic().getTgaImpBnsAnticNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-BNS-ANTIC-NULL
            //             TO (SF)-IMP-BNS-ANTIC-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setImpBnsAnticNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaImpBnsAntic().getTgaImpBnsAnticNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-BNS-ANTIC
            //             TO (SF)-IMP-BNS-ANTIC(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setImpBnsAntic(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaImpBnsAntic().getTgaImpBnsAntic(), 15, 3));
        }
        // COB_CODE: IF TGA-PRE-INI-NET-NULL = HIGH-VALUES
        //                TO (SF)-PRE-INI-NET-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-INI-NET(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPreIniNet().getTgaPreIniNetNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-INI-NET-NULL
            //             TO (SF)-PRE-INI-NET-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setPreIniNetNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaPreIniNet().getTgaPreIniNetNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-INI-NET
            //             TO (SF)-PRE-INI-NET(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setPreIniNet(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaPreIniNet().getTgaPreIniNet(), 15, 3));
        }
        // COB_CODE: IF TGA-PRE-PP-INI-NULL = HIGH-VALUES
        //                TO (SF)-PRE-PP-INI-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-PP-INI(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPrePpIni().getTgaPrePpIniNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-PP-INI-NULL
            //             TO (SF)-PRE-PP-INI-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setPrePpIniNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaPrePpIni().getTgaPrePpIniNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-PP-INI
            //             TO (SF)-PRE-PP-INI(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setPrePpIni(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaPrePpIni().getTgaPrePpIni(), 15, 3));
        }
        // COB_CODE: IF TGA-PRE-PP-ULT-NULL = HIGH-VALUES
        //                TO (SF)-PRE-PP-ULT-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-PP-ULT(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPrePpUlt().getTgaPrePpUltNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-PP-ULT-NULL
            //             TO (SF)-PRE-PP-ULT-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setPrePpUltNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaPrePpUlt().getTgaPrePpUltNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-PP-ULT
            //             TO (SF)-PRE-PP-ULT(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setPrePpUlt(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaPrePpUlt().getTgaPrePpUlt(), 15, 3));
        }
        // COB_CODE: IF TGA-PRE-TARI-INI-NULL = HIGH-VALUES
        //                TO (SF)-PRE-TARI-INI-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-TARI-INI(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPreTariIni().getTgaPreTariIniNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-TARI-INI-NULL
            //             TO (SF)-PRE-TARI-INI-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setPreTariIniNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaPreTariIni().getTgaPreTariIniNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-TARI-INI
            //             TO (SF)-PRE-TARI-INI(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setPreTariIni(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaPreTariIni().getTgaPreTariIni(), 15, 3));
        }
        // COB_CODE: IF TGA-PRE-TARI-ULT-NULL = HIGH-VALUES
        //                TO (SF)-PRE-TARI-ULT-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-TARI-ULT(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPreTariUlt().getTgaPreTariUltNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-TARI-ULT-NULL
            //             TO (SF)-PRE-TARI-ULT-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setPreTariUltNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaPreTariUlt().getTgaPreTariUltNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-TARI-ULT
            //             TO (SF)-PRE-TARI-ULT(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setPreTariUlt(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaPreTariUlt().getTgaPreTariUlt(), 15, 3));
        }
        // COB_CODE: IF TGA-PRE-INVRIO-INI-NULL = HIGH-VALUES
        //                TO (SF)-PRE-INVRIO-INI-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-INVRIO-INI(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPreInvrioIni().getTgaPreInvrioIniNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-INVRIO-INI-NULL
            //             TO (SF)-PRE-INVRIO-INI-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setPreInvrioIniNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaPreInvrioIni().getTgaPreInvrioIniNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-INVRIO-INI
            //             TO (SF)-PRE-INVRIO-INI(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setPreInvrioIni(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaPreInvrioIni().getTgaPreInvrioIni(), 15, 3));
        }
        // COB_CODE: IF TGA-PRE-INVRIO-ULT-NULL = HIGH-VALUES
        //                TO (SF)-PRE-INVRIO-ULT-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-INVRIO-ULT(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPreInvrioUlt().getTgaPreInvrioUltNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-INVRIO-ULT-NULL
            //             TO (SF)-PRE-INVRIO-ULT-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setPreInvrioUltNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaPreInvrioUlt().getTgaPreInvrioUltNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-INVRIO-ULT
            //             TO (SF)-PRE-INVRIO-ULT(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setPreInvrioUlt(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaPreInvrioUlt().getTgaPreInvrioUlt(), 15, 3));
        }
        // COB_CODE: IF TGA-PRE-RIVTO-NULL = HIGH-VALUES
        //                TO (SF)-PRE-RIVTO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-RIVTO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPreRivto().getTgaPreRivtoNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-RIVTO-NULL
            //             TO (SF)-PRE-RIVTO-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setPreRivtoNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaPreRivto().getTgaPreRivtoNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-RIVTO
            //             TO (SF)-PRE-RIVTO(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setPreRivto(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaPreRivto().getTgaPreRivto(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-SOPR-PROF-NULL = HIGH-VALUES
        //                TO (SF)-IMP-SOPR-PROF-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-SOPR-PROF(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpSoprProf().getTgaImpSoprProfNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-SOPR-PROF-NULL
            //             TO (SF)-IMP-SOPR-PROF-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setImpSoprProfNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaImpSoprProf().getTgaImpSoprProfNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-SOPR-PROF
            //             TO (SF)-IMP-SOPR-PROF(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setImpSoprProf(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaImpSoprProf().getTgaImpSoprProf(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-SOPR-SAN-NULL = HIGH-VALUES
        //                TO (SF)-IMP-SOPR-SAN-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-SOPR-SAN(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpSoprSan().getTgaImpSoprSanNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-SOPR-SAN-NULL
            //             TO (SF)-IMP-SOPR-SAN-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setImpSoprSanNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaImpSoprSan().getTgaImpSoprSanNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-SOPR-SAN
            //             TO (SF)-IMP-SOPR-SAN(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setImpSoprSan(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaImpSoprSan().getTgaImpSoprSan(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-SOPR-SPO-NULL = HIGH-VALUES
        //                TO (SF)-IMP-SOPR-SPO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-SOPR-SPO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpSoprSpo().getTgaImpSoprSpoNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-SOPR-SPO-NULL
            //             TO (SF)-IMP-SOPR-SPO-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setImpSoprSpoNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaImpSoprSpo().getTgaImpSoprSpoNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-SOPR-SPO
            //             TO (SF)-IMP-SOPR-SPO(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setImpSoprSpo(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaImpSoprSpo().getTgaImpSoprSpo(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-SOPR-TEC-NULL = HIGH-VALUES
        //                TO (SF)-IMP-SOPR-TEC-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-SOPR-TEC(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpSoprTec().getTgaImpSoprTecNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-SOPR-TEC-NULL
            //             TO (SF)-IMP-SOPR-TEC-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setImpSoprTecNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaImpSoprTec().getTgaImpSoprTecNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-SOPR-TEC
            //             TO (SF)-IMP-SOPR-TEC(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setImpSoprTec(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaImpSoprTec().getTgaImpSoprTec(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-ALT-SOPR-NULL = HIGH-VALUES
        //                TO (SF)-IMP-ALT-SOPR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-ALT-SOPR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpAltSopr().getTgaImpAltSoprNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-ALT-SOPR-NULL
            //             TO (SF)-IMP-ALT-SOPR-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setImpAltSoprNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaImpAltSopr().getTgaImpAltSoprNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-ALT-SOPR
            //             TO (SF)-IMP-ALT-SOPR(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setImpAltSopr(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaImpAltSopr().getTgaImpAltSopr(), 15, 3));
        }
        // COB_CODE: IF TGA-PRE-STAB-NULL = HIGH-VALUES
        //                TO (SF)-PRE-STAB-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-STAB(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPreStab().getTgaPreStabNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-STAB-NULL
            //             TO (SF)-PRE-STAB-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setPreStabNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaPreStab().getTgaPreStabNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-STAB
            //             TO (SF)-PRE-STAB(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setPreStab(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaPreStab().getTgaPreStab(), 15, 3));
        }
        // COB_CODE: IF TGA-DT-EFF-STAB-NULL = HIGH-VALUES
        //                TO (SF)-DT-EFF-STAB-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-DT-EFF-STAB(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaDtEffStab().getTgaDtEffStabNullFormatted())) {
            // COB_CODE: MOVE TGA-DT-EFF-STAB-NULL
            //             TO (SF)-DT-EFF-STAB-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setDtEffStabNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaDtEffStab().getTgaDtEffStabNull());
        }
        else {
            // COB_CODE: MOVE TGA-DT-EFF-STAB
            //             TO (SF)-DT-EFF-STAB(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setDtEffStab(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaDtEffStab().getTgaDtEffStab());
        }
        // COB_CODE: IF TGA-TS-RIVAL-FIS-NULL = HIGH-VALUES
        //                TO (SF)-TS-RIVAL-FIS-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-TS-RIVAL-FIS(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaTsRivalFis().getTgaTsRivalFisNullFormatted())) {
            // COB_CODE: MOVE TGA-TS-RIVAL-FIS-NULL
            //             TO (SF)-TS-RIVAL-FIS-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setTsRivalFisNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaTsRivalFis().getTgaTsRivalFisNull());
        }
        else {
            // COB_CODE: MOVE TGA-TS-RIVAL-FIS
            //             TO (SF)-TS-RIVAL-FIS(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setTsRivalFis(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaTsRivalFis().getTgaTsRivalFis(), 14, 9));
        }
        // COB_CODE: IF TGA-TS-RIVAL-INDICIZ-NULL = HIGH-VALUES
        //                TO (SF)-TS-RIVAL-INDICIZ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-TS-RIVAL-INDICIZ(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaTsRivalIndiciz().getTgaTsRivalIndicizNullFormatted())) {
            // COB_CODE: MOVE TGA-TS-RIVAL-INDICIZ-NULL
            //             TO (SF)-TS-RIVAL-INDICIZ-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setTsRivalIndicizNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaTsRivalIndiciz().getTgaTsRivalIndicizNull());
        }
        else {
            // COB_CODE: MOVE TGA-TS-RIVAL-INDICIZ
            //             TO (SF)-TS-RIVAL-INDICIZ(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setTsRivalIndiciz(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaTsRivalIndiciz().getTgaTsRivalIndiciz(), 14, 9));
        }
        // COB_CODE: IF TGA-OLD-TS-TEC-NULL = HIGH-VALUES
        //                TO (SF)-OLD-TS-TEC-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-OLD-TS-TEC(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaOldTsTec().getTgaOldTsTecNullFormatted())) {
            // COB_CODE: MOVE TGA-OLD-TS-TEC-NULL
            //             TO (SF)-OLD-TS-TEC-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setOldTsTecNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaOldTsTec().getTgaOldTsTecNull());
        }
        else {
            // COB_CODE: MOVE TGA-OLD-TS-TEC
            //             TO (SF)-OLD-TS-TEC(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setOldTsTec(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaOldTsTec().getTgaOldTsTec(), 14, 9));
        }
        // COB_CODE: IF TGA-RAT-LRD-NULL = HIGH-VALUES
        //                TO (SF)-RAT-LRD-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-RAT-LRD(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaRatLrd().getTgaRatLrdNullFormatted())) {
            // COB_CODE: MOVE TGA-RAT-LRD-NULL
            //             TO (SF)-RAT-LRD-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setRatLrdNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaRatLrd().getTgaRatLrdNull());
        }
        else {
            // COB_CODE: MOVE TGA-RAT-LRD
            //             TO (SF)-RAT-LRD(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setRatLrd(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaRatLrd().getTgaRatLrd(), 15, 3));
        }
        // COB_CODE: IF TGA-PRE-LRD-NULL = HIGH-VALUES
        //                TO (SF)-PRE-LRD-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-LRD(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPreLrd().getTgaPreLrdNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-LRD-NULL
            //             TO (SF)-PRE-LRD-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setPreLrdNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaPreLrd().getTgaPreLrdNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-LRD
            //             TO (SF)-PRE-LRD(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setPreLrd(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaPreLrd().getTgaPreLrd(), 15, 3));
        }
        // COB_CODE: IF TGA-PRSTZ-INI-NULL = HIGH-VALUES
        //                TO (SF)-PRSTZ-INI-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRSTZ-INI(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPrstzIni().getTgaPrstzIniNullFormatted())) {
            // COB_CODE: MOVE TGA-PRSTZ-INI-NULL
            //             TO (SF)-PRSTZ-INI-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setPrstzIniNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaPrstzIni().getTgaPrstzIniNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRSTZ-INI
            //             TO (SF)-PRSTZ-INI(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setPrstzIni(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaPrstzIni().getTgaPrstzIni(), 15, 3));
        }
        // COB_CODE: IF TGA-PRSTZ-ULT-NULL = HIGH-VALUES
        //                TO (SF)-PRSTZ-ULT-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRSTZ-ULT(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPrstzUlt().getTgaPrstzUltNullFormatted())) {
            // COB_CODE: MOVE TGA-PRSTZ-ULT-NULL
            //             TO (SF)-PRSTZ-ULT-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setPrstzUltNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaPrstzUlt().getTgaPrstzUltNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRSTZ-ULT
            //             TO (SF)-PRSTZ-ULT(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setPrstzUlt(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaPrstzUlt().getTgaPrstzUlt(), 15, 3));
        }
        // COB_CODE: IF TGA-CPT-IN-OPZ-RIVTO-NULL = HIGH-VALUES
        //                TO (SF)-CPT-IN-OPZ-RIVTO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-CPT-IN-OPZ-RIVTO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaCptInOpzRivto().getTgaCptInOpzRivtoNullFormatted())) {
            // COB_CODE: MOVE TGA-CPT-IN-OPZ-RIVTO-NULL
            //             TO (SF)-CPT-IN-OPZ-RIVTO-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setCptInOpzRivtoNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaCptInOpzRivto().getTgaCptInOpzRivtoNull());
        }
        else {
            // COB_CODE: MOVE TGA-CPT-IN-OPZ-RIVTO
            //             TO (SF)-CPT-IN-OPZ-RIVTO(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setCptInOpzRivto(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaCptInOpzRivto().getTgaCptInOpzRivto(), 15, 3));
        }
        // COB_CODE: IF TGA-PRSTZ-INI-STAB-NULL = HIGH-VALUES
        //                TO (SF)-PRSTZ-INI-STAB-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRSTZ-INI-STAB(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPrstzIniStab().getTgaPrstzIniStabNullFormatted())) {
            // COB_CODE: MOVE TGA-PRSTZ-INI-STAB-NULL
            //             TO (SF)-PRSTZ-INI-STAB-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setPrstzIniStabNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaPrstzIniStab().getTgaPrstzIniStabNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRSTZ-INI-STAB
            //             TO (SF)-PRSTZ-INI-STAB(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setPrstzIniStab(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaPrstzIniStab().getTgaPrstzIniStab(), 15, 3));
        }
        // COB_CODE: IF TGA-CPT-RSH-MOR-NULL = HIGH-VALUES
        //                TO (SF)-CPT-RSH-MOR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-CPT-RSH-MOR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaCptRshMor().getTgaCptRshMorNullFormatted())) {
            // COB_CODE: MOVE TGA-CPT-RSH-MOR-NULL
            //             TO (SF)-CPT-RSH-MOR-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setCptRshMorNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaCptRshMor().getTgaCptRshMorNull());
        }
        else {
            // COB_CODE: MOVE TGA-CPT-RSH-MOR
            //             TO (SF)-CPT-RSH-MOR(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setCptRshMor(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaCptRshMor().getTgaCptRshMor(), 15, 3));
        }
        // COB_CODE: IF TGA-PRSTZ-RID-INI-NULL = HIGH-VALUES
        //                TO (SF)-PRSTZ-RID-INI-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRSTZ-RID-INI(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPrstzRidIni().getTgaPrstzRidIniNullFormatted())) {
            // COB_CODE: MOVE TGA-PRSTZ-RID-INI-NULL
            //             TO (SF)-PRSTZ-RID-INI-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setPrstzRidIniNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaPrstzRidIni().getTgaPrstzRidIniNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRSTZ-RID-INI
            //             TO (SF)-PRSTZ-RID-INI(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setPrstzRidIni(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaPrstzRidIni().getTgaPrstzRidIni(), 15, 3));
        }
        // COB_CODE: IF TGA-FL-CAR-CONT-NULL = HIGH-VALUES
        //                TO (SF)-FL-CAR-CONT-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-FL-CAR-CONT(IX-TAB-TGA)
        //           END-IF
        if (Conditions.eq(ws.getTrchDiGar().getTgaFlCarCont(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE TGA-FL-CAR-CONT-NULL
            //             TO (SF)-FL-CAR-CONT-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setFlCarContNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaFlCarCont());
        }
        else {
            // COB_CODE: MOVE TGA-FL-CAR-CONT
            //             TO (SF)-FL-CAR-CONT(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setFlCarCont(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaFlCarCont());
        }
        // COB_CODE: IF TGA-BNS-GIA-LIQTO-NULL = HIGH-VALUES
        //                TO (SF)-BNS-GIA-LIQTO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-BNS-GIA-LIQTO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaBnsGiaLiqto().getTgaBnsGiaLiqtoNullFormatted())) {
            // COB_CODE: MOVE TGA-BNS-GIA-LIQTO-NULL
            //             TO (SF)-BNS-GIA-LIQTO-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setBnsGiaLiqtoNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaBnsGiaLiqto().getTgaBnsGiaLiqtoNull());
        }
        else {
            // COB_CODE: MOVE TGA-BNS-GIA-LIQTO
            //             TO (SF)-BNS-GIA-LIQTO(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setBnsGiaLiqto(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaBnsGiaLiqto().getTgaBnsGiaLiqto(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-BNS-NULL = HIGH-VALUES
        //                TO (SF)-IMP-BNS-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-BNS(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpBns().getTgaImpBnsNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-BNS-NULL
            //             TO (SF)-IMP-BNS-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setImpBnsNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaImpBns().getTgaImpBnsNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-BNS
            //             TO (SF)-IMP-BNS(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setImpBns(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaImpBns().getTgaImpBns(), 15, 3));
        }
        // COB_CODE: MOVE TGA-COD-DVS
        //             TO (SF)-COD-DVS(IX-TAB-TGA)
        ws.getWtgaAreaTranche().getWtgaTab().setCodDvs(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaCodDvs());
        // COB_CODE: IF TGA-PRSTZ-INI-NEWFIS-NULL = HIGH-VALUES
        //                TO (SF)-PRSTZ-INI-NEWFIS-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRSTZ-INI-NEWFIS(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPrstzIniNewfis().getTgaPrstzIniNewfisNullFormatted())) {
            // COB_CODE: MOVE TGA-PRSTZ-INI-NEWFIS-NULL
            //             TO (SF)-PRSTZ-INI-NEWFIS-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setPrstzIniNewfisNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaPrstzIniNewfis().getTgaPrstzIniNewfisNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRSTZ-INI-NEWFIS
            //             TO (SF)-PRSTZ-INI-NEWFIS(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setPrstzIniNewfis(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaPrstzIniNewfis().getTgaPrstzIniNewfis(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-SCON-NULL = HIGH-VALUES
        //                TO (SF)-IMP-SCON-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-SCON(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpScon().getTgaImpSconNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-SCON-NULL
            //             TO (SF)-IMP-SCON-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setImpSconNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaImpScon().getTgaImpSconNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-SCON
            //             TO (SF)-IMP-SCON(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setImpScon(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaImpScon().getTgaImpScon(), 15, 3));
        }
        // COB_CODE: IF TGA-ALQ-SCON-NULL = HIGH-VALUES
        //                TO (SF)-ALQ-SCON-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ALQ-SCON(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaAlqScon().getTgaAlqSconNullFormatted())) {
            // COB_CODE: MOVE TGA-ALQ-SCON-NULL
            //             TO (SF)-ALQ-SCON-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setAlqSconNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaAlqScon().getTgaAlqSconNull());
        }
        else {
            // COB_CODE: MOVE TGA-ALQ-SCON
            //             TO (SF)-ALQ-SCON(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setAlqScon(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaAlqScon().getTgaAlqScon(), 6, 3));
        }
        // COB_CODE: IF TGA-IMP-CAR-ACQ-NULL = HIGH-VALUES
        //                TO (SF)-IMP-CAR-ACQ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-CAR-ACQ(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpCarAcq().getTgaImpCarAcqNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-CAR-ACQ-NULL
            //             TO (SF)-IMP-CAR-ACQ-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setImpCarAcqNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaImpCarAcq().getTgaImpCarAcqNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-CAR-ACQ
            //             TO (SF)-IMP-CAR-ACQ(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setImpCarAcq(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaImpCarAcq().getTgaImpCarAcq(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-CAR-INC-NULL = HIGH-VALUES
        //                TO (SF)-IMP-CAR-INC-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-CAR-INC(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpCarInc().getTgaImpCarIncNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-CAR-INC-NULL
            //             TO (SF)-IMP-CAR-INC-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setImpCarIncNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaImpCarInc().getTgaImpCarIncNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-CAR-INC
            //             TO (SF)-IMP-CAR-INC(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setImpCarInc(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaImpCarInc().getTgaImpCarInc(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-CAR-GEST-NULL = HIGH-VALUES
        //                TO (SF)-IMP-CAR-GEST-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-CAR-GEST(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpCarGest().getTgaImpCarGestNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-CAR-GEST-NULL
            //             TO (SF)-IMP-CAR-GEST-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setImpCarGestNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaImpCarGest().getTgaImpCarGestNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-CAR-GEST
            //             TO (SF)-IMP-CAR-GEST(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setImpCarGest(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaImpCarGest().getTgaImpCarGest(), 15, 3));
        }
        // COB_CODE: IF TGA-ETA-AA-1O-ASSTO-NULL = HIGH-VALUES
        //                TO (SF)-ETA-AA-1O-ASSTO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ETA-AA-1O-ASSTO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaEtaAa1oAssto().getTgaEtaAa1oAsstoNullFormatted())) {
            // COB_CODE: MOVE TGA-ETA-AA-1O-ASSTO-NULL
            //             TO (SF)-ETA-AA-1O-ASSTO-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setEtaAa1oAsstoNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaEtaAa1oAssto().getTgaEtaAa1oAsstoNull());
        }
        else {
            // COB_CODE: MOVE TGA-ETA-AA-1O-ASSTO
            //             TO (SF)-ETA-AA-1O-ASSTO(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setEtaAa1oAssto(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaEtaAa1oAssto().getTgaEtaAa1oAssto());
        }
        // COB_CODE: IF TGA-ETA-MM-1O-ASSTO-NULL = HIGH-VALUES
        //                TO (SF)-ETA-MM-1O-ASSTO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ETA-MM-1O-ASSTO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaEtaMm1oAssto().getTgaEtaMm1oAsstoNullFormatted())) {
            // COB_CODE: MOVE TGA-ETA-MM-1O-ASSTO-NULL
            //             TO (SF)-ETA-MM-1O-ASSTO-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setEtaMm1oAsstoNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaEtaMm1oAssto().getTgaEtaMm1oAsstoNull());
        }
        else {
            // COB_CODE: MOVE TGA-ETA-MM-1O-ASSTO
            //             TO (SF)-ETA-MM-1O-ASSTO(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setEtaMm1oAssto(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaEtaMm1oAssto().getTgaEtaMm1oAssto());
        }
        // COB_CODE: IF TGA-ETA-AA-2O-ASSTO-NULL = HIGH-VALUES
        //                TO (SF)-ETA-AA-2O-ASSTO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ETA-AA-2O-ASSTO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaEtaAa2oAssto().getTgaEtaAa2oAsstoNullFormatted())) {
            // COB_CODE: MOVE TGA-ETA-AA-2O-ASSTO-NULL
            //             TO (SF)-ETA-AA-2O-ASSTO-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setEtaAa2oAsstoNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaEtaAa2oAssto().getTgaEtaAa2oAsstoNull());
        }
        else {
            // COB_CODE: MOVE TGA-ETA-AA-2O-ASSTO
            //             TO (SF)-ETA-AA-2O-ASSTO(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setEtaAa2oAssto(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaEtaAa2oAssto().getTgaEtaAa2oAssto());
        }
        // COB_CODE: IF TGA-ETA-MM-2O-ASSTO-NULL = HIGH-VALUES
        //                TO (SF)-ETA-MM-2O-ASSTO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ETA-MM-2O-ASSTO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaEtaMm2oAssto().getTgaEtaMm2oAsstoNullFormatted())) {
            // COB_CODE: MOVE TGA-ETA-MM-2O-ASSTO-NULL
            //             TO (SF)-ETA-MM-2O-ASSTO-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setEtaMm2oAsstoNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaEtaMm2oAssto().getTgaEtaMm2oAsstoNull());
        }
        else {
            // COB_CODE: MOVE TGA-ETA-MM-2O-ASSTO
            //             TO (SF)-ETA-MM-2O-ASSTO(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setEtaMm2oAssto(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaEtaMm2oAssto().getTgaEtaMm2oAssto());
        }
        // COB_CODE: IF TGA-ETA-AA-3O-ASSTO-NULL = HIGH-VALUES
        //                TO (SF)-ETA-AA-3O-ASSTO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ETA-AA-3O-ASSTO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaEtaAa3oAssto().getTgaEtaAa3oAsstoNullFormatted())) {
            // COB_CODE: MOVE TGA-ETA-AA-3O-ASSTO-NULL
            //             TO (SF)-ETA-AA-3O-ASSTO-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setEtaAa3oAsstoNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaEtaAa3oAssto().getTgaEtaAa3oAsstoNull());
        }
        else {
            // COB_CODE: MOVE TGA-ETA-AA-3O-ASSTO
            //             TO (SF)-ETA-AA-3O-ASSTO(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setEtaAa3oAssto(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaEtaAa3oAssto().getTgaEtaAa3oAssto());
        }
        // COB_CODE: IF TGA-ETA-MM-3O-ASSTO-NULL = HIGH-VALUES
        //                TO (SF)-ETA-MM-3O-ASSTO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ETA-MM-3O-ASSTO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaEtaMm3oAssto().getTgaEtaMm3oAsstoNullFormatted())) {
            // COB_CODE: MOVE TGA-ETA-MM-3O-ASSTO-NULL
            //             TO (SF)-ETA-MM-3O-ASSTO-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setEtaMm3oAsstoNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaEtaMm3oAssto().getTgaEtaMm3oAsstoNull());
        }
        else {
            // COB_CODE: MOVE TGA-ETA-MM-3O-ASSTO
            //             TO (SF)-ETA-MM-3O-ASSTO(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setEtaMm3oAssto(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaEtaMm3oAssto().getTgaEtaMm3oAssto());
        }
        // COB_CODE: IF TGA-RENDTO-LRD-NULL = HIGH-VALUES
        //                TO (SF)-RENDTO-LRD-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-RENDTO-LRD(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaRendtoLrd().getTgaRendtoLrdNullFormatted())) {
            // COB_CODE: MOVE TGA-RENDTO-LRD-NULL
            //             TO (SF)-RENDTO-LRD-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setRendtoLrdNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaRendtoLrd().getTgaRendtoLrdNull());
        }
        else {
            // COB_CODE: MOVE TGA-RENDTO-LRD
            //             TO (SF)-RENDTO-LRD(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setRendtoLrd(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaRendtoLrd().getTgaRendtoLrd(), 14, 9));
        }
        // COB_CODE: IF TGA-PC-RETR-NULL = HIGH-VALUES
        //                TO (SF)-PC-RETR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PC-RETR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPcRetr().getTgaPcRetrNullFormatted())) {
            // COB_CODE: MOVE TGA-PC-RETR-NULL
            //             TO (SF)-PC-RETR-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setPcRetrNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaPcRetr().getTgaPcRetrNull());
        }
        else {
            // COB_CODE: MOVE TGA-PC-RETR
            //             TO (SF)-PC-RETR(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setPcRetr(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaPcRetr().getTgaPcRetr(), 6, 3));
        }
        // COB_CODE: IF TGA-RENDTO-RETR-NULL = HIGH-VALUES
        //                TO (SF)-RENDTO-RETR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-RENDTO-RETR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaRendtoRetr().getTgaRendtoRetrNullFormatted())) {
            // COB_CODE: MOVE TGA-RENDTO-RETR-NULL
            //             TO (SF)-RENDTO-RETR-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setRendtoRetrNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaRendtoRetr().getTgaRendtoRetrNull());
        }
        else {
            // COB_CODE: MOVE TGA-RENDTO-RETR
            //             TO (SF)-RENDTO-RETR(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setRendtoRetr(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaRendtoRetr().getTgaRendtoRetr(), 14, 9));
        }
        // COB_CODE: IF TGA-MIN-GARTO-NULL = HIGH-VALUES
        //                TO (SF)-MIN-GARTO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-MIN-GARTO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaMinGarto().getTgaMinGartoNullFormatted())) {
            // COB_CODE: MOVE TGA-MIN-GARTO-NULL
            //             TO (SF)-MIN-GARTO-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setMinGartoNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaMinGarto().getTgaMinGartoNull());
        }
        else {
            // COB_CODE: MOVE TGA-MIN-GARTO
            //             TO (SF)-MIN-GARTO(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setMinGarto(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaMinGarto().getTgaMinGarto(), 14, 9));
        }
        // COB_CODE: IF TGA-MIN-TRNUT-NULL = HIGH-VALUES
        //                TO (SF)-MIN-TRNUT-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-MIN-TRNUT(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaMinTrnut().getTgaMinTrnutNullFormatted())) {
            // COB_CODE: MOVE TGA-MIN-TRNUT-NULL
            //             TO (SF)-MIN-TRNUT-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setMinTrnutNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaMinTrnut().getTgaMinTrnutNull());
        }
        else {
            // COB_CODE: MOVE TGA-MIN-TRNUT
            //             TO (SF)-MIN-TRNUT(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setMinTrnut(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaMinTrnut().getTgaMinTrnut(), 14, 9));
        }
        // COB_CODE: IF TGA-PRE-ATT-DI-TRCH-NULL = HIGH-VALUES
        //                TO (SF)-PRE-ATT-DI-TRCH-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-ATT-DI-TRCH(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPreAttDiTrch().getTgaPreAttDiTrchNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-ATT-DI-TRCH-NULL
            //             TO (SF)-PRE-ATT-DI-TRCH-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setPreAttDiTrchNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaPreAttDiTrch().getTgaPreAttDiTrchNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-ATT-DI-TRCH
            //             TO (SF)-PRE-ATT-DI-TRCH(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setPreAttDiTrch(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaPreAttDiTrch().getTgaPreAttDiTrch(), 15, 3));
        }
        // COB_CODE: IF TGA-MATU-END2000-NULL = HIGH-VALUES
        //                TO (SF)-MATU-END2000-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-MATU-END2000(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaMatuEnd2000().getTgaMatuEnd2000NullFormatted())) {
            // COB_CODE: MOVE TGA-MATU-END2000-NULL
            //             TO (SF)-MATU-END2000-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setMatuEnd2000Null(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaMatuEnd2000().getTgaMatuEnd2000Null());
        }
        else {
            // COB_CODE: MOVE TGA-MATU-END2000
            //             TO (SF)-MATU-END2000(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setMatuEnd2000(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaMatuEnd2000().getTgaMatuEnd2000(), 15, 3));
        }
        // COB_CODE: IF TGA-ABB-TOT-INI-NULL = HIGH-VALUES
        //                TO (SF)-ABB-TOT-INI-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ABB-TOT-INI(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaAbbTotIni().getTgaAbbTotIniNullFormatted())) {
            // COB_CODE: MOVE TGA-ABB-TOT-INI-NULL
            //             TO (SF)-ABB-TOT-INI-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setAbbTotIniNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaAbbTotIni().getTgaAbbTotIniNull());
        }
        else {
            // COB_CODE: MOVE TGA-ABB-TOT-INI
            //             TO (SF)-ABB-TOT-INI(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setAbbTotIni(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaAbbTotIni().getTgaAbbTotIni(), 15, 3));
        }
        // COB_CODE: IF TGA-ABB-TOT-ULT-NULL = HIGH-VALUES
        //                TO (SF)-ABB-TOT-ULT-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ABB-TOT-ULT(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaAbbTotUlt().getTgaAbbTotUltNullFormatted())) {
            // COB_CODE: MOVE TGA-ABB-TOT-ULT-NULL
            //             TO (SF)-ABB-TOT-ULT-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setAbbTotUltNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaAbbTotUlt().getTgaAbbTotUltNull());
        }
        else {
            // COB_CODE: MOVE TGA-ABB-TOT-ULT
            //             TO (SF)-ABB-TOT-ULT(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setAbbTotUlt(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaAbbTotUlt().getTgaAbbTotUlt(), 15, 3));
        }
        // COB_CODE: IF TGA-ABB-ANNU-ULT-NULL = HIGH-VALUES
        //                TO (SF)-ABB-ANNU-ULT-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ABB-ANNU-ULT(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaAbbAnnuUlt().getTgaAbbAnnuUltNullFormatted())) {
            // COB_CODE: MOVE TGA-ABB-ANNU-ULT-NULL
            //             TO (SF)-ABB-ANNU-ULT-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setAbbAnnuUltNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaAbbAnnuUlt().getTgaAbbAnnuUltNull());
        }
        else {
            // COB_CODE: MOVE TGA-ABB-ANNU-ULT
            //             TO (SF)-ABB-ANNU-ULT(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setAbbAnnuUlt(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaAbbAnnuUlt().getTgaAbbAnnuUlt(), 15, 3));
        }
        // COB_CODE: IF TGA-DUR-ABB-NULL = HIGH-VALUES
        //                TO (SF)-DUR-ABB-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-DUR-ABB(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaDurAbb().getTgaDurAbbNullFormatted())) {
            // COB_CODE: MOVE TGA-DUR-ABB-NULL
            //             TO (SF)-DUR-ABB-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setDurAbbNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaDurAbb().getTgaDurAbbNull());
        }
        else {
            // COB_CODE: MOVE TGA-DUR-ABB
            //             TO (SF)-DUR-ABB(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setDurAbb(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaDurAbb().getTgaDurAbb());
        }
        // COB_CODE: IF TGA-TP-ADEG-ABB-NULL = HIGH-VALUES
        //                TO (SF)-TP-ADEG-ABB-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-TP-ADEG-ABB(IX-TAB-TGA)
        //           END-IF
        if (Conditions.eq(ws.getTrchDiGar().getTgaTpAdegAbb(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE TGA-TP-ADEG-ABB-NULL
            //             TO (SF)-TP-ADEG-ABB-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setTpAdegAbbNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaTpAdegAbb());
        }
        else {
            // COB_CODE: MOVE TGA-TP-ADEG-ABB
            //             TO (SF)-TP-ADEG-ABB(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setTpAdegAbb(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaTpAdegAbb());
        }
        // COB_CODE: IF TGA-MOD-CALC-NULL = HIGH-VALUES
        //                TO (SF)-MOD-CALC-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-MOD-CALC(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaModCalcFormatted())) {
            // COB_CODE: MOVE TGA-MOD-CALC-NULL
            //             TO (SF)-MOD-CALC-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setModCalcNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaModCalc());
        }
        else {
            // COB_CODE: MOVE TGA-MOD-CALC
            //             TO (SF)-MOD-CALC(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setModCalc(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaModCalc());
        }
        // COB_CODE: IF TGA-IMP-AZ-NULL = HIGH-VALUES
        //                TO (SF)-IMP-AZ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-AZ(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpAz().getTgaImpAzNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-AZ-NULL
            //             TO (SF)-IMP-AZ-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setImpAzNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaImpAz().getTgaImpAzNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-AZ
            //             TO (SF)-IMP-AZ(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setImpAz(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaImpAz().getTgaImpAz(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-ADER-NULL = HIGH-VALUES
        //                TO (SF)-IMP-ADER-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-ADER(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpAder().getTgaImpAderNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-ADER-NULL
            //             TO (SF)-IMP-ADER-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setImpAderNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaImpAder().getTgaImpAderNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-ADER
            //             TO (SF)-IMP-ADER(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setImpAder(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaImpAder().getTgaImpAder(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-TFR-NULL = HIGH-VALUES
        //                TO (SF)-IMP-TFR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-TFR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpTfr().getTgaImpTfrNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-TFR-NULL
            //             TO (SF)-IMP-TFR-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setImpTfrNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaImpTfr().getTgaImpTfrNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-TFR
            //             TO (SF)-IMP-TFR(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setImpTfr(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaImpTfr().getTgaImpTfr(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-VOLO-NULL = HIGH-VALUES
        //                TO (SF)-IMP-VOLO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-VOLO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpVolo().getTgaImpVoloNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-VOLO-NULL
            //             TO (SF)-IMP-VOLO-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setImpVoloNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaImpVolo().getTgaImpVoloNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-VOLO
            //             TO (SF)-IMP-VOLO(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setImpVolo(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaImpVolo().getTgaImpVolo(), 15, 3));
        }
        // COB_CODE: IF TGA-VIS-END2000-NULL = HIGH-VALUES
        //                TO (SF)-VIS-END2000-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-VIS-END2000(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaVisEnd2000().getTgaVisEnd2000NullFormatted())) {
            // COB_CODE: MOVE TGA-VIS-END2000-NULL
            //             TO (SF)-VIS-END2000-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setVisEnd2000Null(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaVisEnd2000().getTgaVisEnd2000Null());
        }
        else {
            // COB_CODE: MOVE TGA-VIS-END2000
            //             TO (SF)-VIS-END2000(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setVisEnd2000(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaVisEnd2000().getTgaVisEnd2000(), 15, 3));
        }
        // COB_CODE: IF TGA-DT-VLDT-PROD-NULL = HIGH-VALUES
        //                TO (SF)-DT-VLDT-PROD-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-DT-VLDT-PROD(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaDtVldtProd().getTgaDtVldtProdNullFormatted())) {
            // COB_CODE: MOVE TGA-DT-VLDT-PROD-NULL
            //             TO (SF)-DT-VLDT-PROD-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setDtVldtProdNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaDtVldtProd().getTgaDtVldtProdNull());
        }
        else {
            // COB_CODE: MOVE TGA-DT-VLDT-PROD
            //             TO (SF)-DT-VLDT-PROD(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setDtVldtProd(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaDtVldtProd().getTgaDtVldtProd());
        }
        // COB_CODE: IF TGA-DT-INI-VAL-TAR-NULL = HIGH-VALUES
        //                TO (SF)-DT-INI-VAL-TAR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-DT-INI-VAL-TAR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaDtIniValTar().getTgaDtIniValTarNullFormatted())) {
            // COB_CODE: MOVE TGA-DT-INI-VAL-TAR-NULL
            //             TO (SF)-DT-INI-VAL-TAR-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setDtIniValTarNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaDtIniValTar().getTgaDtIniValTarNull());
        }
        else {
            // COB_CODE: MOVE TGA-DT-INI-VAL-TAR
            //             TO (SF)-DT-INI-VAL-TAR(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setDtIniValTar(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaDtIniValTar().getTgaDtIniValTar());
        }
        // COB_CODE: IF TGA-IMPB-VIS-END2000-NULL = HIGH-VALUES
        //                TO (SF)-IMPB-VIS-END2000-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMPB-VIS-END2000(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpbVisEnd2000().getTgaImpbVisEnd2000NullFormatted())) {
            // COB_CODE: MOVE TGA-IMPB-VIS-END2000-NULL
            //             TO (SF)-IMPB-VIS-END2000-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setImpbVisEnd2000Null(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaImpbVisEnd2000().getTgaImpbVisEnd2000Null());
        }
        else {
            // COB_CODE: MOVE TGA-IMPB-VIS-END2000
            //             TO (SF)-IMPB-VIS-END2000(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setImpbVisEnd2000(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaImpbVisEnd2000().getTgaImpbVisEnd2000(), 15, 3));
        }
        // COB_CODE: IF TGA-REN-INI-TS-TEC-0-NULL = HIGH-VALUES
        //                TO (SF)-REN-INI-TS-TEC-0-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-REN-INI-TS-TEC-0(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaRenIniTsTec0().getTgaRenIniTsTec0NullFormatted())) {
            // COB_CODE: MOVE TGA-REN-INI-TS-TEC-0-NULL
            //             TO (SF)-REN-INI-TS-TEC-0-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setRenIniTsTec0Null(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaRenIniTsTec0().getTgaRenIniTsTec0Null());
        }
        else {
            // COB_CODE: MOVE TGA-REN-INI-TS-TEC-0
            //             TO (SF)-REN-INI-TS-TEC-0(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setRenIniTsTec0(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaRenIniTsTec0().getTgaRenIniTsTec0(), 15, 3));
        }
        // COB_CODE: IF TGA-PC-RIP-PRE-NULL = HIGH-VALUES
        //                TO (SF)-PC-RIP-PRE-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PC-RIP-PRE(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPcRipPre().getTgaPcRipPreNullFormatted())) {
            // COB_CODE: MOVE TGA-PC-RIP-PRE-NULL
            //             TO (SF)-PC-RIP-PRE-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setPcRipPreNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaPcRipPre().getTgaPcRipPreNull());
        }
        else {
            // COB_CODE: MOVE TGA-PC-RIP-PRE
            //             TO (SF)-PC-RIP-PRE(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setPcRipPre(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaPcRipPre().getTgaPcRipPre(), 6, 3));
        }
        // COB_CODE: IF TGA-FL-IMPORTI-FORZ-NULL = HIGH-VALUES
        //                TO (SF)-FL-IMPORTI-FORZ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-FL-IMPORTI-FORZ(IX-TAB-TGA)
        //           END-IF
        if (Conditions.eq(ws.getTrchDiGar().getTgaFlImportiForz(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE TGA-FL-IMPORTI-FORZ-NULL
            //             TO (SF)-FL-IMPORTI-FORZ-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setFlImportiForzNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaFlImportiForz());
        }
        else {
            // COB_CODE: MOVE TGA-FL-IMPORTI-FORZ
            //             TO (SF)-FL-IMPORTI-FORZ(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setFlImportiForz(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaFlImportiForz());
        }
        // COB_CODE: IF TGA-PRSTZ-INI-NFORZ-NULL = HIGH-VALUES
        //                TO (SF)-PRSTZ-INI-NFORZ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRSTZ-INI-NFORZ(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPrstzIniNforz().getTgaPrstzIniNforzNullFormatted())) {
            // COB_CODE: MOVE TGA-PRSTZ-INI-NFORZ-NULL
            //             TO (SF)-PRSTZ-INI-NFORZ-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setPrstzIniNforzNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaPrstzIniNforz().getTgaPrstzIniNforzNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRSTZ-INI-NFORZ
            //             TO (SF)-PRSTZ-INI-NFORZ(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setPrstzIniNforz(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaPrstzIniNforz().getTgaPrstzIniNforz(), 15, 3));
        }
        // COB_CODE: IF TGA-VIS-END2000-NFORZ-NULL = HIGH-VALUES
        //                TO (SF)-VIS-END2000-NFORZ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-VIS-END2000-NFORZ(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaVisEnd2000Nforz().getTgaVisEnd2000NforzNullFormatted())) {
            // COB_CODE: MOVE TGA-VIS-END2000-NFORZ-NULL
            //             TO (SF)-VIS-END2000-NFORZ-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setVisEnd2000NforzNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaVisEnd2000Nforz().getTgaVisEnd2000NforzNull());
        }
        else {
            // COB_CODE: MOVE TGA-VIS-END2000-NFORZ
            //             TO (SF)-VIS-END2000-NFORZ(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setVisEnd2000Nforz(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaVisEnd2000Nforz().getTgaVisEnd2000Nforz(), 15, 3));
        }
        // COB_CODE: IF TGA-INTR-MORA-NULL = HIGH-VALUES
        //                TO (SF)-INTR-MORA-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-INTR-MORA(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaIntrMora().getTgaIntrMoraNullFormatted())) {
            // COB_CODE: MOVE TGA-INTR-MORA-NULL
            //             TO (SF)-INTR-MORA-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setIntrMoraNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaIntrMora().getTgaIntrMoraNull());
        }
        else {
            // COB_CODE: MOVE TGA-INTR-MORA
            //             TO (SF)-INTR-MORA(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setIntrMora(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaIntrMora().getTgaIntrMora(), 15, 3));
        }
        // COB_CODE: IF TGA-MANFEE-ANTIC-NULL = HIGH-VALUES
        //                TO (SF)-MANFEE-ANTIC-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-MANFEE-ANTIC(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaManfeeAntic().getTgaManfeeAnticNullFormatted())) {
            // COB_CODE: MOVE TGA-MANFEE-ANTIC-NULL
            //             TO (SF)-MANFEE-ANTIC-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setManfeeAnticNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaManfeeAntic().getTgaManfeeAnticNull());
        }
        else {
            // COB_CODE: MOVE TGA-MANFEE-ANTIC
            //             TO (SF)-MANFEE-ANTIC(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setManfeeAntic(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaManfeeAntic().getTgaManfeeAntic(), 15, 3));
        }
        // COB_CODE: IF TGA-MANFEE-RICOR-NULL = HIGH-VALUES
        //                TO (SF)-MANFEE-RICOR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-MANFEE-RICOR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaManfeeRicor().getTgaManfeeRicorNullFormatted())) {
            // COB_CODE: MOVE TGA-MANFEE-RICOR-NULL
            //             TO (SF)-MANFEE-RICOR-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setManfeeRicorNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaManfeeRicor().getTgaManfeeRicorNull());
        }
        else {
            // COB_CODE: MOVE TGA-MANFEE-RICOR
            //             TO (SF)-MANFEE-RICOR(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setManfeeRicor(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaManfeeRicor().getTgaManfeeRicor(), 15, 3));
        }
        // COB_CODE: IF TGA-PRE-UNI-RIVTO-NULL = HIGH-VALUES
        //                TO (SF)-PRE-UNI-RIVTO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-UNI-RIVTO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPreUniRivto().getTgaPreUniRivtoNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-UNI-RIVTO-NULL
            //             TO (SF)-PRE-UNI-RIVTO-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setPreUniRivtoNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaPreUniRivto().getTgaPreUniRivtoNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-UNI-RIVTO
            //             TO (SF)-PRE-UNI-RIVTO(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setPreUniRivto(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaPreUniRivto().getTgaPreUniRivto(), 15, 3));
        }
        // COB_CODE: IF TGA-PROV-1AA-ACQ-NULL = HIGH-VALUES
        //                TO (SF)-PROV-1AA-ACQ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PROV-1AA-ACQ(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaProv1aaAcq().getTgaProv1aaAcqNullFormatted())) {
            // COB_CODE: MOVE TGA-PROV-1AA-ACQ-NULL
            //             TO (SF)-PROV-1AA-ACQ-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setProv1aaAcqNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaProv1aaAcq().getTgaProv1aaAcqNull());
        }
        else {
            // COB_CODE: MOVE TGA-PROV-1AA-ACQ
            //             TO (SF)-PROV-1AA-ACQ(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setProv1aaAcq(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaProv1aaAcq().getTgaProv1aaAcq(), 15, 3));
        }
        // COB_CODE: IF TGA-PROV-2AA-ACQ-NULL = HIGH-VALUES
        //                TO (SF)-PROV-2AA-ACQ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PROV-2AA-ACQ(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaProv2aaAcq().getTgaProv2aaAcqNullFormatted())) {
            // COB_CODE: MOVE TGA-PROV-2AA-ACQ-NULL
            //             TO (SF)-PROV-2AA-ACQ-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setProv2aaAcqNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaProv2aaAcq().getTgaProv2aaAcqNull());
        }
        else {
            // COB_CODE: MOVE TGA-PROV-2AA-ACQ
            //             TO (SF)-PROV-2AA-ACQ(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setProv2aaAcq(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaProv2aaAcq().getTgaProv2aaAcq(), 15, 3));
        }
        // COB_CODE: IF TGA-PROV-RICOR-NULL = HIGH-VALUES
        //                TO (SF)-PROV-RICOR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PROV-RICOR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaProvRicor().getTgaProvRicorNullFormatted())) {
            // COB_CODE: MOVE TGA-PROV-RICOR-NULL
            //             TO (SF)-PROV-RICOR-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setProvRicorNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaProvRicor().getTgaProvRicorNull());
        }
        else {
            // COB_CODE: MOVE TGA-PROV-RICOR
            //             TO (SF)-PROV-RICOR(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setProvRicor(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaProvRicor().getTgaProvRicor(), 15, 3));
        }
        // COB_CODE: IF TGA-PROV-INC-NULL = HIGH-VALUES
        //                TO (SF)-PROV-INC-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PROV-INC(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaProvInc().getTgaProvIncNullFormatted())) {
            // COB_CODE: MOVE TGA-PROV-INC-NULL
            //             TO (SF)-PROV-INC-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setProvIncNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaProvInc().getTgaProvIncNull());
        }
        else {
            // COB_CODE: MOVE TGA-PROV-INC
            //             TO (SF)-PROV-INC(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setProvInc(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaProvInc().getTgaProvInc(), 15, 3));
        }
        // COB_CODE: IF TGA-ALQ-PROV-ACQ-NULL = HIGH-VALUES
        //                TO (SF)-ALQ-PROV-ACQ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ALQ-PROV-ACQ(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaAlqProvAcq().getTgaAlqProvAcqNullFormatted())) {
            // COB_CODE: MOVE TGA-ALQ-PROV-ACQ-NULL
            //             TO (SF)-ALQ-PROV-ACQ-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setAlqProvAcqNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaAlqProvAcq().getTgaAlqProvAcqNull());
        }
        else {
            // COB_CODE: MOVE TGA-ALQ-PROV-ACQ
            //             TO (SF)-ALQ-PROV-ACQ(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setAlqProvAcq(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaAlqProvAcq().getTgaAlqProvAcq(), 6, 3));
        }
        // COB_CODE: IF TGA-ALQ-PROV-INC-NULL = HIGH-VALUES
        //                TO (SF)-ALQ-PROV-INC-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ALQ-PROV-INC(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaAlqProvInc().getTgaAlqProvIncNullFormatted())) {
            // COB_CODE: MOVE TGA-ALQ-PROV-INC-NULL
            //             TO (SF)-ALQ-PROV-INC-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setAlqProvIncNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaAlqProvInc().getTgaAlqProvIncNull());
        }
        else {
            // COB_CODE: MOVE TGA-ALQ-PROV-INC
            //             TO (SF)-ALQ-PROV-INC(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setAlqProvInc(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaAlqProvInc().getTgaAlqProvInc(), 6, 3));
        }
        // COB_CODE: IF TGA-ALQ-PROV-RICOR-NULL = HIGH-VALUES
        //                TO (SF)-ALQ-PROV-RICOR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ALQ-PROV-RICOR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaAlqProvRicor().getTgaAlqProvRicorNullFormatted())) {
            // COB_CODE: MOVE TGA-ALQ-PROV-RICOR-NULL
            //             TO (SF)-ALQ-PROV-RICOR-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setAlqProvRicorNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaAlqProvRicor().getTgaAlqProvRicorNull());
        }
        else {
            // COB_CODE: MOVE TGA-ALQ-PROV-RICOR
            //             TO (SF)-ALQ-PROV-RICOR(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setAlqProvRicor(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaAlqProvRicor().getTgaAlqProvRicor(), 6, 3));
        }
        // COB_CODE: IF TGA-IMPB-PROV-ACQ-NULL = HIGH-VALUES
        //                TO (SF)-IMPB-PROV-ACQ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMPB-PROV-ACQ(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpbProvAcq().getTgaImpbProvAcqNullFormatted())) {
            // COB_CODE: MOVE TGA-IMPB-PROV-ACQ-NULL
            //             TO (SF)-IMPB-PROV-ACQ-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setImpbProvAcqNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaImpbProvAcq().getTgaImpbProvAcqNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMPB-PROV-ACQ
            //             TO (SF)-IMPB-PROV-ACQ(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setImpbProvAcq(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaImpbProvAcq().getTgaImpbProvAcq(), 15, 3));
        }
        // COB_CODE: IF TGA-IMPB-PROV-INC-NULL = HIGH-VALUES
        //                TO (SF)-IMPB-PROV-INC-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMPB-PROV-INC(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpbProvInc().getTgaImpbProvIncNullFormatted())) {
            // COB_CODE: MOVE TGA-IMPB-PROV-INC-NULL
            //             TO (SF)-IMPB-PROV-INC-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setImpbProvIncNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaImpbProvInc().getTgaImpbProvIncNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMPB-PROV-INC
            //             TO (SF)-IMPB-PROV-INC(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setImpbProvInc(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaImpbProvInc().getTgaImpbProvInc(), 15, 3));
        }
        // COB_CODE: IF TGA-IMPB-PROV-RICOR-NULL = HIGH-VALUES
        //                TO (SF)-IMPB-PROV-RICOR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMPB-PROV-RICOR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpbProvRicor().getTgaImpbProvRicorNullFormatted())) {
            // COB_CODE: MOVE TGA-IMPB-PROV-RICOR-NULL
            //             TO (SF)-IMPB-PROV-RICOR-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setImpbProvRicorNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaImpbProvRicor().getTgaImpbProvRicorNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMPB-PROV-RICOR
            //             TO (SF)-IMPB-PROV-RICOR(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setImpbProvRicor(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaImpbProvRicor().getTgaImpbProvRicor(), 15, 3));
        }
        // COB_CODE: IF TGA-FL-PROV-FORZ-NULL = HIGH-VALUES
        //                TO (SF)-FL-PROV-FORZ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-FL-PROV-FORZ(IX-TAB-TGA)
        //           END-IF
        if (Conditions.eq(ws.getTrchDiGar().getTgaFlProvForz(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE TGA-FL-PROV-FORZ-NULL
            //             TO (SF)-FL-PROV-FORZ-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setFlProvForzNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaFlProvForz());
        }
        else {
            // COB_CODE: MOVE TGA-FL-PROV-FORZ
            //             TO (SF)-FL-PROV-FORZ(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setFlProvForz(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaFlProvForz());
        }
        // COB_CODE: IF TGA-PRSTZ-AGG-INI-NULL = HIGH-VALUES
        //                TO (SF)-PRSTZ-AGG-INI-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRSTZ-AGG-INI(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPrstzAggIni().getTgaPrstzAggIniNullFormatted())) {
            // COB_CODE: MOVE TGA-PRSTZ-AGG-INI-NULL
            //             TO (SF)-PRSTZ-AGG-INI-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setPrstzAggIniNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaPrstzAggIni().getTgaPrstzAggIniNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRSTZ-AGG-INI
            //             TO (SF)-PRSTZ-AGG-INI(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setPrstzAggIni(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaPrstzAggIni().getTgaPrstzAggIni(), 15, 3));
        }
        // COB_CODE: IF TGA-INCR-PRE-NULL = HIGH-VALUES
        //                TO (SF)-INCR-PRE-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-INCR-PRE(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaIncrPre().getTgaIncrPreNullFormatted())) {
            // COB_CODE: MOVE TGA-INCR-PRE-NULL
            //             TO (SF)-INCR-PRE-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setIncrPreNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaIncrPre().getTgaIncrPreNull());
        }
        else {
            // COB_CODE: MOVE TGA-INCR-PRE
            //             TO (SF)-INCR-PRE(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setIncrPre(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaIncrPre().getTgaIncrPre(), 15, 3));
        }
        // COB_CODE: IF TGA-INCR-PRSTZ-NULL = HIGH-VALUES
        //                TO (SF)-INCR-PRSTZ-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-INCR-PRSTZ(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaIncrPrstz().getTgaIncrPrstzNullFormatted())) {
            // COB_CODE: MOVE TGA-INCR-PRSTZ-NULL
            //             TO (SF)-INCR-PRSTZ-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setIncrPrstzNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaIncrPrstz().getTgaIncrPrstzNull());
        }
        else {
            // COB_CODE: MOVE TGA-INCR-PRSTZ
            //             TO (SF)-INCR-PRSTZ(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setIncrPrstz(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaIncrPrstz().getTgaIncrPrstz(), 15, 3));
        }
        // COB_CODE: IF TGA-DT-ULT-ADEG-PRE-PR-NULL = HIGH-VALUES
        //                TO (SF)-DT-ULT-ADEG-PRE-PR-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-DT-ULT-ADEG-PRE-PR(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaDtUltAdegPrePr().getTgaDtUltAdegPrePrNullFormatted())) {
            // COB_CODE: MOVE TGA-DT-ULT-ADEG-PRE-PR-NULL
            //             TO (SF)-DT-ULT-ADEG-PRE-PR-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setDtUltAdegPrePrNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaDtUltAdegPrePr().getTgaDtUltAdegPrePrNull());
        }
        else {
            // COB_CODE: MOVE TGA-DT-ULT-ADEG-PRE-PR
            //             TO (SF)-DT-ULT-ADEG-PRE-PR(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setDtUltAdegPrePr(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaDtUltAdegPrePr().getTgaDtUltAdegPrePr());
        }
        // COB_CODE: IF TGA-PRSTZ-AGG-ULT-NULL = HIGH-VALUES
        //                TO (SF)-PRSTZ-AGG-ULT-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRSTZ-AGG-ULT(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPrstzAggUlt().getTgaPrstzAggUltNullFormatted())) {
            // COB_CODE: MOVE TGA-PRSTZ-AGG-ULT-NULL
            //             TO (SF)-PRSTZ-AGG-ULT-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setPrstzAggUltNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaPrstzAggUlt().getTgaPrstzAggUltNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRSTZ-AGG-ULT
            //             TO (SF)-PRSTZ-AGG-ULT(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setPrstzAggUlt(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaPrstzAggUlt().getTgaPrstzAggUlt(), 15, 3));
        }
        // COB_CODE: IF TGA-TS-RIVAL-NET-NULL = HIGH-VALUES
        //                TO (SF)-TS-RIVAL-NET-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-TS-RIVAL-NET(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaTsRivalNet().getTgaTsRivalNetNullFormatted())) {
            // COB_CODE: MOVE TGA-TS-RIVAL-NET-NULL
            //             TO (SF)-TS-RIVAL-NET-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setTsRivalNetNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaTsRivalNet().getTgaTsRivalNetNull());
        }
        else {
            // COB_CODE: MOVE TGA-TS-RIVAL-NET
            //             TO (SF)-TS-RIVAL-NET(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setTsRivalNet(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaTsRivalNet().getTgaTsRivalNet(), 14, 9));
        }
        // COB_CODE: IF TGA-PRE-PATTUITO-NULL = HIGH-VALUES
        //                TO (SF)-PRE-PATTUITO-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PRE-PATTUITO(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPrePattuito().getTgaPrePattuitoNullFormatted())) {
            // COB_CODE: MOVE TGA-PRE-PATTUITO-NULL
            //             TO (SF)-PRE-PATTUITO-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setPrePattuitoNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaPrePattuito().getTgaPrePattuitoNull());
        }
        else {
            // COB_CODE: MOVE TGA-PRE-PATTUITO
            //             TO (SF)-PRE-PATTUITO(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setPrePattuito(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaPrePattuito().getTgaPrePattuito(), 15, 3));
        }
        // COB_CODE: IF TGA-TP-RIVAL-NULL = HIGH-VALUES
        //                TO (SF)-TP-RIVAL-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-TP-RIVAL(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaTpRivalFormatted())) {
            // COB_CODE: MOVE TGA-TP-RIVAL-NULL
            //             TO (SF)-TP-RIVAL-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setTpRivalNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaTpRival());
        }
        else {
            // COB_CODE: MOVE TGA-TP-RIVAL
            //             TO (SF)-TP-RIVAL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setTpRival(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaTpRival());
        }
        // COB_CODE: IF TGA-RIS-MAT-NULL = HIGH-VALUES
        //                TO (SF)-RIS-MAT-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-RIS-MAT(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaRisMat().getTgaRisMatNullFormatted())) {
            // COB_CODE: MOVE TGA-RIS-MAT-NULL
            //             TO (SF)-RIS-MAT-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setRisMatNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaRisMat().getTgaRisMatNull());
        }
        else {
            // COB_CODE: MOVE TGA-RIS-MAT
            //             TO (SF)-RIS-MAT(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setRisMat(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaRisMat().getTgaRisMat(), 15, 3));
        }
        // COB_CODE: IF TGA-CPT-MIN-SCAD-NULL = HIGH-VALUES
        //                TO (SF)-CPT-MIN-SCAD-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-CPT-MIN-SCAD(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaCptMinScad().getTgaCptMinScadNullFormatted())) {
            // COB_CODE: MOVE TGA-CPT-MIN-SCAD-NULL
            //             TO (SF)-CPT-MIN-SCAD-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setCptMinScadNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaCptMinScad().getTgaCptMinScadNull());
        }
        else {
            // COB_CODE: MOVE TGA-CPT-MIN-SCAD
            //             TO (SF)-CPT-MIN-SCAD(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setCptMinScad(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaCptMinScad().getTgaCptMinScad(), 15, 3));
        }
        // COB_CODE: IF TGA-COMMIS-GEST-NULL = HIGH-VALUES
        //                TO (SF)-COMMIS-GEST-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-COMMIS-GEST(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaCommisGest().getTgaCommisGestNullFormatted())) {
            // COB_CODE: MOVE TGA-COMMIS-GEST-NULL
            //             TO (SF)-COMMIS-GEST-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setCommisGestNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaCommisGest().getTgaCommisGestNull());
        }
        else {
            // COB_CODE: MOVE TGA-COMMIS-GEST
            //             TO (SF)-COMMIS-GEST(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setCommisGest(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaCommisGest().getTgaCommisGest(), 15, 3));
        }
        // COB_CODE: IF TGA-TP-MANFEE-APPL-NULL = HIGH-VALUES
        //                TO (SF)-TP-MANFEE-APPL-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-TP-MANFEE-APPL(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaTpManfeeApplFormatted())) {
            // COB_CODE: MOVE TGA-TP-MANFEE-APPL-NULL
            //             TO (SF)-TP-MANFEE-APPL-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setTpManfeeApplNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaTpManfeeAppl());
        }
        else {
            // COB_CODE: MOVE TGA-TP-MANFEE-APPL
            //             TO (SF)-TP-MANFEE-APPL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setTpManfeeAppl(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaTpManfeeAppl());
        }
        // COB_CODE: MOVE TGA-DS-RIGA
        //             TO (SF)-DS-RIGA(IX-TAB-TGA)
        ws.getWtgaAreaTranche().getWtgaTab().setDsRiga(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaDsRiga());
        // COB_CODE: MOVE TGA-DS-OPER-SQL
        //             TO (SF)-DS-OPER-SQL(IX-TAB-TGA)
        ws.getWtgaAreaTranche().getWtgaTab().setDsOperSql(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaDsOperSql());
        // COB_CODE: MOVE TGA-DS-VER
        //             TO (SF)-DS-VER(IX-TAB-TGA)
        ws.getWtgaAreaTranche().getWtgaTab().setDsVer(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaDsVer());
        // COB_CODE: MOVE TGA-DS-TS-INI-CPTZ
        //             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-TGA)
        ws.getWtgaAreaTranche().getWtgaTab().setDsTsIniCptz(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaDsTsIniCptz());
        // COB_CODE: MOVE TGA-DS-TS-END-CPTZ
        //             TO (SF)-DS-TS-END-CPTZ(IX-TAB-TGA)
        ws.getWtgaAreaTranche().getWtgaTab().setDsTsEndCptz(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaDsTsEndCptz());
        // COB_CODE: MOVE TGA-DS-UTENTE
        //             TO (SF)-DS-UTENTE(IX-TAB-TGA)
        ws.getWtgaAreaTranche().getWtgaTab().setDsUtente(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaDsUtente());
        // COB_CODE: MOVE TGA-DS-STATO-ELAB
        //             TO (SF)-DS-STATO-ELAB(IX-TAB-TGA)
        ws.getWtgaAreaTranche().getWtgaTab().setDsStatoElab(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaDsStatoElab());
        // COB_CODE: IF TGA-PC-COMMIS-GEST-NULL = HIGH-VALUES
        //                TO (SF)-PC-COMMIS-GEST-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-PC-COMMIS-GEST(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPcCommisGest().getTgaPcCommisGestNullFormatted())) {
            // COB_CODE: MOVE TGA-PC-COMMIS-GEST-NULL
            //             TO (SF)-PC-COMMIS-GEST-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setPcCommisGestNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaPcCommisGest().getTgaPcCommisGestNull());
        }
        else {
            // COB_CODE: MOVE TGA-PC-COMMIS-GEST
            //             TO (SF)-PC-COMMIS-GEST(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setPcCommisGest(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaPcCommisGest().getTgaPcCommisGest(), 6, 3));
        }
        // COB_CODE: IF TGA-NUM-GG-RIVAL-NULL = HIGH-VALUES
        //                TO (SF)-NUM-GG-RIVAL-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-NUM-GG-RIVAL(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaNumGgRival().getTgaNumGgRivalNullFormatted())) {
            // COB_CODE: MOVE TGA-NUM-GG-RIVAL-NULL
            //             TO (SF)-NUM-GG-RIVAL-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setNumGgRivalNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaNumGgRival().getTgaNumGgRivalNull());
        }
        else {
            // COB_CODE: MOVE TGA-NUM-GG-RIVAL
            //             TO (SF)-NUM-GG-RIVAL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setNumGgRival(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaNumGgRival().getTgaNumGgRival());
        }
        // COB_CODE: IF TGA-IMP-TRASFE-NULL = HIGH-VALUES
        //                TO (SF)-IMP-TRASFE-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-TRASFE(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpTrasfe().getTgaImpTrasfeNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-TRASFE-NULL
            //             TO (SF)-IMP-TRASFE-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setImpTrasfeNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaImpTrasfe().getTgaImpTrasfeNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-TRASFE
            //             TO (SF)-IMP-TRASFE(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setImpTrasfe(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaImpTrasfe().getTgaImpTrasfe(), 15, 3));
        }
        // COB_CODE: IF TGA-IMP-TFR-STRC-NULL = HIGH-VALUES
        //                TO (SF)-IMP-TFR-STRC-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMP-TFR-STRC(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpTfrStrc().getTgaImpTfrStrcNullFormatted())) {
            // COB_CODE: MOVE TGA-IMP-TFR-STRC-NULL
            //             TO (SF)-IMP-TFR-STRC-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setImpTfrStrcNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaImpTfrStrc().getTgaImpTfrStrcNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMP-TFR-STRC
            //             TO (SF)-IMP-TFR-STRC(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setImpTfrStrc(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaImpTfrStrc().getTgaImpTfrStrc(), 15, 3));
        }
        // COB_CODE: IF TGA-ACQ-EXP-NULL = HIGH-VALUES
        //                TO (SF)-ACQ-EXP-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ACQ-EXP(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaAcqExp().getTgaAcqExpNullFormatted())) {
            // COB_CODE: MOVE TGA-ACQ-EXP-NULL
            //             TO (SF)-ACQ-EXP-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setAcqExpNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaAcqExp().getTgaAcqExpNull());
        }
        else {
            // COB_CODE: MOVE TGA-ACQ-EXP
            //             TO (SF)-ACQ-EXP(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setAcqExp(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaAcqExp().getTgaAcqExp(), 15, 3));
        }
        // COB_CODE: IF TGA-REMUN-ASS-NULL = HIGH-VALUES
        //                TO (SF)-REMUN-ASS-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-REMUN-ASS(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaRemunAss().getTgaRemunAssNullFormatted())) {
            // COB_CODE: MOVE TGA-REMUN-ASS-NULL
            //             TO (SF)-REMUN-ASS-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setRemunAssNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaRemunAss().getTgaRemunAssNull());
        }
        else {
            // COB_CODE: MOVE TGA-REMUN-ASS
            //             TO (SF)-REMUN-ASS(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setRemunAss(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaRemunAss().getTgaRemunAss(), 15, 3));
        }
        // COB_CODE: IF TGA-COMMIS-INTER-NULL = HIGH-VALUES
        //                TO (SF)-COMMIS-INTER-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-COMMIS-INTER(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaCommisInter().getTgaCommisInterNullFormatted())) {
            // COB_CODE: MOVE TGA-COMMIS-INTER-NULL
            //             TO (SF)-COMMIS-INTER-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setCommisInterNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaCommisInter().getTgaCommisInterNull());
        }
        else {
            // COB_CODE: MOVE TGA-COMMIS-INTER
            //             TO (SF)-COMMIS-INTER(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setCommisInter(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaCommisInter().getTgaCommisInter(), 15, 3));
        }
        // COB_CODE: IF TGA-ALQ-REMUN-ASS-NULL = HIGH-VALUES
        //                TO (SF)-ALQ-REMUN-ASS-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ALQ-REMUN-ASS(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaAlqRemunAss().getTgaAlqRemunAssNullFormatted())) {
            // COB_CODE: MOVE TGA-ALQ-REMUN-ASS-NULL
            //             TO (SF)-ALQ-REMUN-ASS-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setAlqRemunAssNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaAlqRemunAss().getTgaAlqRemunAssNull());
        }
        else {
            // COB_CODE: MOVE TGA-ALQ-REMUN-ASS
            //             TO (SF)-ALQ-REMUN-ASS(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setAlqRemunAss(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaAlqRemunAss().getTgaAlqRemunAss(), 6, 3));
        }
        // COB_CODE: IF TGA-ALQ-COMMIS-INTER-NULL = HIGH-VALUES
        //                TO (SF)-ALQ-COMMIS-INTER-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-ALQ-COMMIS-INTER(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaAlqCommisInter().getTgaAlqCommisInterNullFormatted())) {
            // COB_CODE: MOVE TGA-ALQ-COMMIS-INTER-NULL
            //             TO (SF)-ALQ-COMMIS-INTER-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setAlqCommisInterNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaAlqCommisInter().getTgaAlqCommisInterNull());
        }
        else {
            // COB_CODE: MOVE TGA-ALQ-COMMIS-INTER
            //             TO (SF)-ALQ-COMMIS-INTER(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setAlqCommisInter(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaAlqCommisInter().getTgaAlqCommisInter(), 6, 3));
        }
        // COB_CODE: IF TGA-IMPB-REMUN-ASS-NULL = HIGH-VALUES
        //                TO (SF)-IMPB-REMUN-ASS-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMPB-REMUN-ASS(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpbRemunAss().getTgaImpbRemunAssNullFormatted())) {
            // COB_CODE: MOVE TGA-IMPB-REMUN-ASS-NULL
            //             TO (SF)-IMPB-REMUN-ASS-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setImpbRemunAssNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaImpbRemunAss().getTgaImpbRemunAssNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMPB-REMUN-ASS
            //             TO (SF)-IMPB-REMUN-ASS(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setImpbRemunAss(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaImpbRemunAss().getTgaImpbRemunAss(), 15, 3));
        }
        // COB_CODE: IF TGA-IMPB-COMMIS-INTER-NULL = HIGH-VALUES
        //                TO (SF)-IMPB-COMMIS-INTER-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-IMPB-COMMIS-INTER(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaImpbCommisInter().getTgaImpbCommisInterNullFormatted())) {
            // COB_CODE: MOVE TGA-IMPB-COMMIS-INTER-NULL
            //             TO (SF)-IMPB-COMMIS-INTER-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setImpbCommisInterNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaImpbCommisInter().getTgaImpbCommisInterNull());
        }
        else {
            // COB_CODE: MOVE TGA-IMPB-COMMIS-INTER
            //             TO (SF)-IMPB-COMMIS-INTER(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setImpbCommisInter(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaImpbCommisInter().getTgaImpbCommisInter(), 15, 3));
        }
        // COB_CODE: IF TGA-COS-RUN-ASSVA-NULL = HIGH-VALUES
        //                TO (SF)-COS-RUN-ASSVA-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-COS-RUN-ASSVA(IX-TAB-TGA)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaCosRunAssva().getTgaCosRunAssvaNullFormatted())) {
            // COB_CODE: MOVE TGA-COS-RUN-ASSVA-NULL
            //             TO (SF)-COS-RUN-ASSVA-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setCosRunAssvaNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaCosRunAssva().getTgaCosRunAssvaNull());
        }
        else {
            // COB_CODE: MOVE TGA-COS-RUN-ASSVA
            //             TO (SF)-COS-RUN-ASSVA(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setCosRunAssva(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaCosRunAssva().getTgaCosRunAssva(), 15, 3));
        }
        // COB_CODE: IF TGA-COS-RUN-ASSVA-IDC-NULL = HIGH-VALUES
        //                TO (SF)-COS-RUN-ASSVA-IDC-NULL(IX-TAB-TGA)
        //           ELSE
        //                TO (SF)-COS-RUN-ASSVA-IDC(IX-TAB-TGA)
        //           END-IF.
        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaCosRunAssvaIdc().getTgaCosRunAssvaIdcNullFormatted())) {
            // COB_CODE: MOVE TGA-COS-RUN-ASSVA-IDC-NULL
            //             TO (SF)-COS-RUN-ASSVA-IDC-NULL(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setCosRunAssvaIdcNull(ws.getIxIndici().getTabTga(), ws.getTrchDiGar().getTgaCosRunAssvaIdc().getTgaCosRunAssvaIdcNull());
        }
        else {
            // COB_CODE: MOVE TGA-COS-RUN-ASSVA-IDC
            //             TO (SF)-COS-RUN-ASSVA-IDC(IX-TAB-TGA)
            ws.getWtgaAreaTranche().getWtgaTab().setCosRunAssvaIdc(ws.getIxIndici().getTabTga(), Trunc.toDecimal(ws.getTrchDiGar().getTgaCosRunAssvaIdc().getTgaCosRunAssvaIdc(), 15, 3));
        }
    }

    /**Original name: INIZIA-TOT-POL<br>
	 * <pre>----------------------------------------------------------------*
	 *     COPY INIZIALIZZAZIONE TABELLE
	 * ----------------------------------------------------------------*
	 *  --> POLIZZA
	 * ------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    INIZIALIZZAZIONE CAMPI NULL COPY LCCVPOL4
	 *    ULTIMO AGG. 07 DIC 2017
	 * ------------------------------------------------------------</pre>*/
    private void iniziaTotPol() {
        // COB_CODE: PERFORM INIZIA-ZEROES-POL THRU INIZIA-ZEROES-POL-EX
        iniziaZeroesPol();
        // COB_CODE: PERFORM INIZIA-SPACES-POL THRU INIZIA-SPACES-POL-EX
        iniziaSpacesPol();
        // COB_CODE: PERFORM INIZIA-NULL-POL THRU INIZIA-NULL-POL-EX.
        iniziaNullPol();
    }

    /**Original name: INIZIA-NULL-POL<br>*/
    private void iniziaNullPol() {
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-ID-MOVI-CHIU-NULL
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolIdMoviChiu().setWpolIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpolIdMoviChiu.Len.WPOL_ID_MOVI_CHIU_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IB-OGG-NULL
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolIbOgg(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpolDati.Len.WPOL_IB_OGG));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-PROP-NULL
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolDtProp().setWpolDtPropNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpolDtProp.Len.WPOL_DT_PROP_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DUR-AA-NULL
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolDurAa().setWpolDurAaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpolDurAa.Len.WPOL_DUR_AA_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DUR-MM-NULL
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolDurMm().setWpolDurMmNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpolDurMm.Len.WPOL_DUR_MM_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-SCAD-NULL
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolDtScad().setWpolDtScadNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpolDtScad.Len.WPOL_DT_SCAD_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-COD-CONV-NULL
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolCodConv(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpolDati.Len.WPOL_COD_CONV));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-COD-RAMO-NULL
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolCodRamo(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpolDati.Len.WPOL_COD_RAMO));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-INI-VLDT-CONV-NULL
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolDtIniVldtConv().setWpolDtIniVldtConvNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpolDtIniVldtConv.Len.WPOL_DT_INI_VLDT_CONV_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-APPLZ-CONV-NULL
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolDtApplzConv().setWpolDtApplzConvNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpolDtApplzConv.Len.WPOL_DT_APPLZ_CONV_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TP-RGM-FISC-NULL
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolTpRgmFisc(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpolDati.Len.WPOL_TP_RGM_FISC));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-FL-ESTAS-NULL
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlEstas(Types.HIGH_CHAR_VAL);
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-FL-RSH-COMUN-NULL
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlRshComun(Types.HIGH_CHAR_VAL);
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-FL-RSH-COMUN-COND-NULL
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlRshComunCond(Types.HIGH_CHAR_VAL);
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-FL-COP-FINANZ-NULL
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlCopFinanz(Types.HIGH_CHAR_VAL);
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TP-APPLZ-DIR-NULL
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolTpApplzDir(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpolDati.Len.WPOL_TP_APPLZ_DIR));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-SPE-MED-NULL
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolSpeMed().setWpolSpeMedNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpolSpeMed.Len.WPOL_SPE_MED_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DIR-EMIS-NULL
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolDirEmis().setWpolDirEmisNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpolDirEmis.Len.WPOL_DIR_EMIS_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DIR-1O-VERS-NULL
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolDir1oVers().setWpolDir1oVersNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpolDir1oVers.Len.WPOL_DIR1O_VERS_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DIR-VERS-AGG-NULL
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolDirVersAgg().setWpolDirVersAggNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpolDirVersAgg.Len.WPOL_DIR_VERS_AGG_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-COD-DVS-NULL
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolCodDvs(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpolDati.Len.WPOL_COD_DVS));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-FL-FNT-AZ-NULL
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlFntAz(Types.HIGH_CHAR_VAL);
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-FL-FNT-ADER-NULL
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlFntAder(Types.HIGH_CHAR_VAL);
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-FL-FNT-TFR-NULL
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlFntTfr(Types.HIGH_CHAR_VAL);
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-FL-FNT-VOLO-NULL
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlFntVolo(Types.HIGH_CHAR_VAL);
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TP-OPZ-A-SCAD-NULL
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolTpOpzAScad(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpolDati.Len.WPOL_TP_OPZ_A_SCAD));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-AA-DIFF-PROR-DFLT-NULL
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolAaDiffProrDflt().setWpolAaDiffProrDfltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpolAaDiffProrDflt.Len.WPOL_AA_DIFF_PROR_DFLT_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-FL-VER-PROD-NULL
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlVerProd(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpolDati.Len.WPOL_FL_VER_PROD));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DUR-GG-NULL
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolDurGg().setWpolDurGgNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpolDurGg.Len.WPOL_DUR_GG_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DIR-QUIET-NULL
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolDirQuiet().setWpolDirQuietNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpolDirQuiet.Len.WPOL_DIR_QUIET_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TP-PTF-ESTNO-NULL
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolTpPtfEstno(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpolDati.Len.WPOL_TP_PTF_ESTNO));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-FL-CUM-PRE-CNTR-NULL
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlCumPreCntr(Types.HIGH_CHAR_VAL);
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-FL-AMMB-MOVI-NULL
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlAmmbMovi(Types.HIGH_CHAR_VAL);
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-CONV-GECO-NULL
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolConvGeco(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpolDati.Len.WPOL_CONV_GECO));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-FL-SCUDO-FISC-NULL
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlScudoFisc(Types.HIGH_CHAR_VAL);
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-FL-TRASFE-NULL
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlTrasfe(Types.HIGH_CHAR_VAL);
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-FL-TFR-STRC-NULL
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlTfrStrc(Types.HIGH_CHAR_VAL);
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-PRESC-NULL
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolDtPresc().setWpolDtPrescNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpolDtPresc.Len.WPOL_DT_PRESC_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-COD-CONV-AGG-NULL
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolCodConvAgg(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpolDati.Len.WPOL_COD_CONV_AGG));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-SUBCAT-PROD-NULL
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolSubcatProd(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpolDati.Len.WPOL_SUBCAT_PROD));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-FL-QUEST-ADEGZ-ASS-NULL
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlQuestAdegzAss(Types.HIGH_CHAR_VAL);
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-COD-TPA-NULL
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolCodTpa(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpolDati.Len.WPOL_COD_TPA));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-ID-ACC-COMM-NULL
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolIdAccComm().setWpolIdAccCommNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpolIdAccComm.Len.WPOL_ID_ACC_COMM_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-FL-POLI-CPI-PR-NULL
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlPoliCpiPr(Types.HIGH_CHAR_VAL);
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-FL-POLI-BUNDLING-NULL
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlPoliBundling(Types.HIGH_CHAR_VAL);
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IND-POLI-PRIN-COLL-NULL
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolIndPoliPrinColl(Types.HIGH_CHAR_VAL);
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-FL-VND-BUNDLE-NULL
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlVndBundle(Types.HIGH_CHAR_VAL);
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IB-BS-NULL
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolIbBs(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpolDati.Len.WPOL_IB_BS));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-FL-POLI-IFP-NULL.
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolFlPoliIfp(Types.HIGH_CHAR_VAL);
    }

    /**Original name: INIZIA-ZEROES-POL<br>*/
    private void iniziaZeroesPol() {
        // COB_CODE: MOVE 0 TO (SF)-ID-POLI
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolIdPoli(0);
        // COB_CODE: MOVE 0 TO (SF)-ID-MOVI-CRZ
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolIdMoviCrz(0);
        // COB_CODE: MOVE 0 TO (SF)-DT-INI-EFF
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolDtIniEff(0);
        // COB_CODE: MOVE 0 TO (SF)-DT-END-EFF
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolDtEndEff(0);
        // COB_CODE: MOVE 0 TO (SF)-COD-COMP-ANIA
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolCodCompAnia(0);
        // COB_CODE: MOVE 0 TO (SF)-DT-DECOR
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolDtDecor(0);
        // COB_CODE: MOVE 0 TO (SF)-DT-EMIS
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolDtEmis(0);
        // COB_CODE: MOVE 0 TO (SF)-DT-INI-VLDT-PROD
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolDtIniVldtProd(0);
        // COB_CODE: MOVE 0 TO (SF)-DS-RIGA
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolDsRiga(0);
        // COB_CODE: MOVE 0 TO (SF)-DS-VER
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolDsVer(0);
        // COB_CODE: MOVE 0 TO (SF)-DS-TS-INI-CPTZ
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolDsTsIniCptz(0);
        // COB_CODE: MOVE 0 TO (SF)-DS-TS-END-CPTZ.
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolDsTsEndCptz(0);
    }

    /**Original name: INIZIA-SPACES-POL<br>*/
    private void iniziaSpacesPol() {
        // COB_CODE: MOVE SPACES TO (SF)-IB-PROP
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolIbProp("");
        // COB_CODE: MOVE SPACES TO (SF)-TP-POLI
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolTpPoli("");
        // COB_CODE: MOVE SPACES TO (SF)-COD-PROD
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolCodProd("");
        // COB_CODE: MOVE SPACES TO (SF)-TP-FRM-ASSVA
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolTpFrmAssva("");
        // COB_CODE: MOVE SPACES TO (SF)-TP-LIV-GENZ-TIT
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolTpLivGenzTit("");
        // COB_CODE: MOVE SPACES TO (SF)-DS-OPER-SQL
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolDsOperSql(Types.SPACE_CHAR);
        // COB_CODE: MOVE SPACES TO (SF)-DS-UTENTE
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolDsUtente("");
        // COB_CODE: MOVE SPACES TO (SF)-DS-STATO-ELAB.
        areaMain.getWpolAreaPolizza().getLccvpol1().getDati().setWpolDsStatoElab(Types.SPACE_CHAR);
    }

    /**Original name: INIZIA-TOT-ADE<br>
	 * <pre> --> ADESIONE
	 * ------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    INIZIALIZZAZIONE CAMPI NULL COPY LCCVADE4
	 *    ULTIMO AGG. 07 DIC 2010
	 * ------------------------------------------------------------</pre>*/
    private void iniziaTotAde() {
        // COB_CODE: PERFORM INIZIA-ZEROES-ADE THRU INIZIA-ZEROES-ADE-EX
        iniziaZeroesAde();
        // COB_CODE: PERFORM INIZIA-SPACES-ADE THRU INIZIA-SPACES-ADE-EX
        iniziaSpacesAde();
        // COB_CODE: PERFORM INIZIA-NULL-ADE THRU INIZIA-NULL-ADE-EX.
        iniziaNullAde();
    }

    /**Original name: INIZIA-NULL-ADE<br>*/
    private void iniziaNullAde() {
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeIdMoviChiu().setWadeIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WadeIdMoviChiu.Len.WADE_ID_MOVI_CHIU_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IB-PREV-NULL(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeIbPrev(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WadeDati.Len.WADE_IB_PREV));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IB-OGG-NULL(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeIbOgg(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WadeDati.Len.WADE_IB_OGG));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-DECOR-NULL(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeDtDecor().setWadeDtDecorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WadeDtDecor.Len.WADE_DT_DECOR_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-SCAD-NULL(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeDtScad().setWadeDtScadNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WadeDtScad.Len.WADE_DT_SCAD_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-ETA-A-SCAD-NULL(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeEtaAScad().setWadeEtaAScadNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WadeEtaAScad.Len.WADE_ETA_A_SCAD_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DUR-AA-NULL(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeDurAa().setWadeDurAaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WadeDurAa.Len.WADE_DUR_AA_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DUR-MM-NULL(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeDurMm().setWadeDurMmNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WadeDurMm.Len.WADE_DUR_MM_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DUR-GG-NULL(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeDurGg().setWadeDurGgNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WadeDurGg.Len.WADE_DUR_GG_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TP-RIAT-NULL(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeTpRiat(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WadeDati.Len.WADE_TP_RIAT));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TP-IAS-NULL(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeTpIas(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WadeDati.Len.WADE_TP_IAS));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-VARZ-TP-IAS-NULL(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeDtVarzTpIas().setWadeDtVarzTpIasNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WadeDtVarzTpIas.Len.WADE_DT_VARZ_TP_IAS_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PRE-NET-IND-NULL(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadePreNetInd().setWadePreNetIndNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WadePreNetInd.Len.WADE_PRE_NET_IND_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PRE-LRD-IND-NULL(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadePreLrdInd().setWadePreLrdIndNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WadePreLrdInd.Len.WADE_PRE_LRD_IND_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-RAT-LRD-IND-NULL(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeRatLrdInd().setWadeRatLrdIndNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WadeRatLrdInd.Len.WADE_RAT_LRD_IND_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PRSTZ-INI-IND-NULL(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadePrstzIniInd().setWadePrstzIniIndNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WadePrstzIniInd.Len.WADE_PRSTZ_INI_IND_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-FL-COINC-ASSTO-NULL(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeFlCoincAssto(Types.HIGH_CHAR_VAL);
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IB-DFLT-NULL(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeIbDflt(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WadeDati.Len.WADE_IB_DFLT));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-MOD-CALC-NULL(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeModCalc(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WadeDati.Len.WADE_MOD_CALC));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TP-FNT-CNBTVA-NULL(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeTpFntCnbtva(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WadeDati.Len.WADE_TP_FNT_CNBTVA));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMP-AZ-NULL(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeImpAz().setWadeImpAzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WadeImpAz.Len.WADE_IMP_AZ_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMP-ADER-NULL(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeImpAder().setWadeImpAderNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WadeImpAder.Len.WADE_IMP_ADER_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMP-TFR-NULL(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeImpTfr().setWadeImpTfrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WadeImpTfr.Len.WADE_IMP_TFR_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMP-VOLO-NULL(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeImpVolo().setWadeImpVoloNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WadeImpVolo.Len.WADE_IMP_VOLO_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PC-AZ-NULL(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadePcAz().setWadePcAzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WadePcAz.Len.WADE_PC_AZ_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PC-ADER-NULL(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadePcAder().setWadePcAderNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WadePcAder.Len.WADE_PC_ADER_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PC-TFR-NULL(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadePcTfr().setWadePcTfrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WadePcTfr.Len.WADE_PC_TFR_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PC-VOLO-NULL(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadePcVolo().setWadePcVoloNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WadePcVolo.Len.WADE_PC_VOLO_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-NOVA-RGM-FISC-NULL(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeDtNovaRgmFisc().setWadeDtNovaRgmFiscNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WadeDtNovaRgmFisc.Len.WADE_DT_NOVA_RGM_FISC_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-FL-ATTIV-NULL(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeFlAttiv(Types.HIGH_CHAR_VAL);
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMP-REC-RIT-VIS-NULL(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeImpRecRitVis().setWadeImpRecRitVisNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WadeImpRecRitVis.Len.WADE_IMP_REC_RIT_VIS_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMP-REC-RIT-ACC-NULL(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeImpRecRitAcc().setWadeImpRecRitAccNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WadeImpRecRitAcc.Len.WADE_IMP_REC_RIT_ACC_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-FL-VARZ-STAT-TBGC-NULL(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeFlVarzStatTbgc(Types.HIGH_CHAR_VAL);
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-FL-PROVZA-MIGRAZ-NULL(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeFlProvzaMigraz(Types.HIGH_CHAR_VAL);
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMPB-VIS-DA-REC-NULL(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeImpbVisDaRec().setWadeImpbVisDaRecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WadeImpbVisDaRec.Len.WADE_IMPB_VIS_DA_REC_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-DECOR-PREST-BAN-NULL(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeDtDecorPrestBan().setWadeDtDecorPrestBanNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WadeDtDecorPrestBan.Len.WADE_DT_DECOR_PREST_BAN_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-EFF-VARZ-STAT-T-NULL(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeDtEffVarzStatT().setWadeDtEffVarzStatTNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WadeDtEffVarzStatT.Len.WADE_DT_EFF_VARZ_STAT_T_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-CUM-CNBT-CAP-NULL(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeCumCnbtCap().setWadeCumCnbtCapNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WadeCumCnbtCap.Len.WADE_CUM_CNBT_CAP_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMP-GAR-CNBT-NULL(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeImpGarCnbt().setWadeImpGarCnbtNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WadeImpGarCnbt.Len.WADE_IMP_GAR_CNBT_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ULT-CONS-CNBT-NULL(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeDtUltConsCnbt().setWadeDtUltConsCnbtNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WadeDtUltConsCnbt.Len.WADE_DT_ULT_CONS_CNBT_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IDEN-ISC-FND-NULL(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeIdenIscFnd(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WadeDati.Len.WADE_IDEN_ISC_FND));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-NUM-RAT-PIAN-NULL(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeNumRatPian().setWadeNumRatPianNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WadeNumRatPian.Len.WADE_NUM_RAT_PIAN_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-PRESC-NULL(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().getWadeDtPresc().setWadeDtPrescNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WadeDtPresc.Len.WADE_DT_PRESC_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-CONCS-PREST-NULL(IX-TAB-ADE).
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeConcsPrest(Types.HIGH_CHAR_VAL);
    }

    /**Original name: INIZIA-ZEROES-ADE<br>*/
    private void iniziaZeroesAde() {
        // COB_CODE: MOVE 0 TO (SF)-ID-ADES(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeIdAdes(0);
        // COB_CODE: MOVE 0 TO (SF)-ID-POLI(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeIdPoli(0);
        // COB_CODE: MOVE 0 TO (SF)-ID-MOVI-CRZ(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeIdMoviCrz(0);
        // COB_CODE: MOVE 0 TO (SF)-DT-INI-EFF(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeDtIniEff(0);
        // COB_CODE: MOVE 0 TO (SF)-DT-END-EFF(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeDtEndEff(0);
        // COB_CODE: MOVE 0 TO (SF)-COD-COMP-ANIA(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeCodCompAnia(0);
        // COB_CODE: MOVE 0 TO (SF)-DS-RIGA(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeDsRiga(0);
        // COB_CODE: MOVE 0 TO (SF)-DS-VER(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeDsVer(0);
        // COB_CODE: MOVE 0 TO (SF)-DS-TS-INI-CPTZ(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeDsTsIniCptz(0);
        // COB_CODE: MOVE 0 TO (SF)-DS-TS-END-CPTZ(IX-TAB-ADE).
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeDsTsEndCptz(0);
    }

    /**Original name: INIZIA-SPACES-ADE<br>*/
    private void iniziaSpacesAde() {
        // COB_CODE: MOVE SPACES TO (SF)-TP-RGM-FISC(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeTpRgmFisc("");
        // COB_CODE: MOVE SPACES TO (SF)-TP-MOD-PAG-TIT(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeTpModPagTit("");
        // COB_CODE: MOVE SPACES TO (SF)-DS-OPER-SQL(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeDsOperSql(Types.SPACE_CHAR);
        // COB_CODE: MOVE SPACES TO (SF)-DS-UTENTE(IX-TAB-ADE)
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeDsUtente("");
        // COB_CODE: MOVE SPACES TO (SF)-DS-STATO-ELAB(IX-TAB-ADE).
        areaMain.getWadeAreaAdesione().getTabAde(ws.getIxIndici().getTabAde()).getLccvade1().getDati().setWadeDsStatoElab(Types.SPACE_CHAR);
    }

    /**Original name: INIZIA-TOT-MOV<br>
	 * <pre>  --> MOVIMENTO
	 * ------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    INIZIALIZZAZIONE CAMPI NULL COPY LCCVMOV4
	 *    ULTIMO AGG. 02 SET 2008
	 * ------------------------------------------------------------</pre>*/
    private void iniziaTotMov() {
        // COB_CODE: PERFORM INIZIA-ZEROES-MOV THRU INIZIA-ZEROES-MOV-EX
        iniziaZeroesMov();
        // COB_CODE: PERFORM INIZIA-SPACES-MOV THRU INIZIA-SPACES-MOV-EX
        iniziaSpacesMov();
        // COB_CODE: PERFORM INIZIA-NULL-MOV THRU INIZIA-NULL-MOV-EX.
        iniziaNullMov();
    }

    /**Original name: INIZIA-NULL-MOV<br>*/
    private void iniziaNullMov() {
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-ID-OGG-NULL
        areaMain.getWmovAreaMovimento().getLccvmov1().getDati().getWmovIdOgg().setWmovIdOggNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WmovIdOgg.Len.WMOV_ID_OGG_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IB-OGG-NULL
        areaMain.getWmovAreaMovimento().getLccvmov1().getDati().setWmovIbOgg(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WmovDati.Len.WMOV_IB_OGG));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IB-MOVI-NULL
        areaMain.getWmovAreaMovimento().getLccvmov1().getDati().setWmovIbMovi(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WmovDati.Len.WMOV_IB_MOVI));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TP-OGG-NULL
        areaMain.getWmovAreaMovimento().getLccvmov1().getDati().setWmovTpOgg(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WmovDati.Len.WMOV_TP_OGG));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-ID-RICH-NULL
        areaMain.getWmovAreaMovimento().getLccvmov1().getDati().getWmovIdRich().setWmovIdRichNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WmovIdRich.Len.WMOV_ID_RICH_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TP-MOVI-NULL
        areaMain.getWmovAreaMovimento().getLccvmov1().getDati().getWmovTpMovi().setWmovTpMoviNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WmovTpMovi.Len.WMOV_TP_MOVI_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-ID-MOVI-ANN-NULL
        areaMain.getWmovAreaMovimento().getLccvmov1().getDati().getWmovIdMoviAnn().setWmovIdMoviAnnNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WmovIdMoviAnn.Len.WMOV_ID_MOVI_ANN_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-ID-MOVI-COLLG-NULL.
        areaMain.getWmovAreaMovimento().getLccvmov1().getDati().getWmovIdMoviCollg().setWmovIdMoviCollgNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WmovIdMoviCollg.Len.WMOV_ID_MOVI_COLLG_NULL));
    }

    /**Original name: INIZIA-ZEROES-MOV<br>*/
    private void iniziaZeroesMov() {
        // COB_CODE: MOVE 0 TO (SF)-ID-MOVI
        areaMain.getWmovAreaMovimento().getLccvmov1().getDati().setWmovIdMovi(0);
        // COB_CODE: MOVE 0 TO (SF)-COD-COMP-ANIA
        areaMain.getWmovAreaMovimento().getLccvmov1().getDati().setWmovCodCompAnia(0);
        // COB_CODE: MOVE 0 TO (SF)-DT-EFF
        areaMain.getWmovAreaMovimento().getLccvmov1().getDati().setWmovDtEff(0);
        // COB_CODE: MOVE 0 TO (SF)-DS-VER
        areaMain.getWmovAreaMovimento().getLccvmov1().getDati().setWmovDsVer(0);
        // COB_CODE: MOVE 0 TO (SF)-DS-TS-CPTZ.
        areaMain.getWmovAreaMovimento().getLccvmov1().getDati().setWmovDsTsCptz(0);
    }

    /**Original name: INIZIA-SPACES-MOV<br>*/
    private void iniziaSpacesMov() {
        // COB_CODE: MOVE SPACES TO (SF)-DS-OPER-SQL
        areaMain.getWmovAreaMovimento().getLccvmov1().getDati().setWmovDsOperSql(Types.SPACE_CHAR);
        // COB_CODE: MOVE SPACES TO (SF)-DS-UTENTE
        areaMain.getWmovAreaMovimento().getLccvmov1().getDati().setWmovDsUtente("");
        // COB_CODE: MOVE SPACES TO (SF)-DS-STATO-ELAB.
        areaMain.getWmovAreaMovimento().getLccvmov1().getDati().setWmovDsStatoElab(Types.SPACE_CHAR);
    }

    /**Original name: INIZIA-TOT-GRZ<br>
	 * <pre> --> GARANZIA
	 * ------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    INIZIALIZZAZIONE CAMPI NULL COPY LCCVGRZ4
	 *    ULTIMO AGG. 31 OTT 2013
	 * ------------------------------------------------------------</pre>*/
    private void iniziaTotGrz() {
        // COB_CODE: PERFORM INIZIA-ZEROES-GRZ THRU INIZIA-ZEROES-GRZ-EX
        iniziaZeroesGrz();
        // COB_CODE: PERFORM INIZIA-SPACES-GRZ THRU INIZIA-SPACES-GRZ-EX
        iniziaSpacesGrz();
        // COB_CODE: PERFORM INIZIA-NULL-GRZ THRU INIZIA-NULL-GRZ-EX.
        iniziaNullGrz();
    }

    /**Original name: INIZIA-NULL-GRZ<br>*/
    private void iniziaNullGrz() {
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-ID-ADES-NULL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzIdAdes().setWgrzIdAdesNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WgrzIdAdes.Len.WGRZ_ID_ADES_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzIdMoviChiu().setWgrzIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WgrzIdMoviChiu.Len.WGRZ_ID_MOVI_CHIU_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IB-OGG-NULL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzIbOgg(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WgrzDati.Len.WGRZ_IB_OGG));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-DECOR-NULL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzDtDecor().setWgrzDtDecorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WgrzDtDecor.Len.WGRZ_DT_DECOR_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-SCAD-NULL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzDtScad().setWgrzDtScadNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WgrzDtScad.Len.WGRZ_DT_SCAD_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-COD-SEZ-NULL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzCodSez(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WgrzDati.Len.WGRZ_COD_SEZ));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-RAMO-BILA-NULL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzRamoBila(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WgrzDati.Len.WGRZ_RAMO_BILA));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-INI-VAL-TAR-NULL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzDtIniValTar().setWgrzDtIniValTarNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WgrzDtIniValTar.Len.WGRZ_DT_INI_VAL_TAR_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-ID-1O-ASSTO-NULL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzId1oAssto().setWgrzId1oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WgrzId1oAssto.Len.WGRZ_ID1O_ASSTO_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-ID-2O-ASSTO-NULL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzId2oAssto().setWgrzId2oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WgrzId2oAssto.Len.WGRZ_ID2O_ASSTO_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-ID-3O-ASSTO-NULL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzId3oAssto().setWgrzId3oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WgrzId3oAssto.Len.WGRZ_ID3O_ASSTO_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TP-GAR-NULL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzTpGar().setWgrzTpGarNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WgrzTpGar.Len.WGRZ_TP_GAR_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TP-RSH-NULL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzTpRsh(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WgrzDati.Len.WGRZ_TP_RSH));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TP-INVST-NULL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzTpInvst().setWgrzTpInvstNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WgrzTpInvst.Len.WGRZ_TP_INVST_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-MOD-PAG-GARCOL-NULL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzModPagGarcol(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WgrzDati.Len.WGRZ_MOD_PAG_GARCOL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TP-PER-PRE-NULL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzTpPerPre(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WgrzDati.Len.WGRZ_TP_PER_PRE));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-ETA-AA-1O-ASSTO-NULL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzEtaAa1oAssto().setWgrzEtaAa1oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WgrzEtaAa1oAssto.Len.WGRZ_ETA_AA1O_ASSTO_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-ETA-MM-1O-ASSTO-NULL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzEtaMm1oAssto().setWgrzEtaMm1oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WgrzEtaMm1oAssto.Len.WGRZ_ETA_MM1O_ASSTO_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-ETA-AA-2O-ASSTO-NULL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzEtaAa2oAssto().setWgrzEtaAa2oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WgrzEtaAa2oAssto.Len.WGRZ_ETA_AA2O_ASSTO_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-ETA-MM-2O-ASSTO-NULL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzEtaMm2oAssto().setWgrzEtaMm2oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WgrzEtaMm2oAssto.Len.WGRZ_ETA_MM2O_ASSTO_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-ETA-AA-3O-ASSTO-NULL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzEtaAa3oAssto().setWgrzEtaAa3oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WgrzEtaAa3oAssto.Len.WGRZ_ETA_AA3O_ASSTO_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-ETA-MM-3O-ASSTO-NULL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzEtaMm3oAssto().setWgrzEtaMm3oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WgrzEtaMm3oAssto.Len.WGRZ_ETA_MM3O_ASSTO_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TP-EMIS-PUR-NULL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzTpEmisPur(Types.HIGH_CHAR_VAL);
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-ETA-A-SCAD-NULL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzEtaAScad().setWgrzEtaAScadNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WgrzEtaAScad.Len.WGRZ_ETA_A_SCAD_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TP-CALC-PRE-PRSTZ-NULL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzTpCalcPrePrstz(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WgrzDati.Len.WGRZ_TP_CALC_PRE_PRSTZ));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TP-PRE-NULL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzTpPre(Types.HIGH_CHAR_VAL);
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TP-DUR-NULL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzTpDur(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WgrzDati.Len.WGRZ_TP_DUR));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DUR-AA-NULL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzDurAa().setWgrzDurAaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WgrzDurAa.Len.WGRZ_DUR_AA_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DUR-MM-NULL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzDurMm().setWgrzDurMmNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WgrzDurMm.Len.WGRZ_DUR_MM_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DUR-GG-NULL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzDurGg().setWgrzDurGgNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WgrzDurGg.Len.WGRZ_DUR_GG_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-NUM-AA-PAG-PRE-NULL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzNumAaPagPre().setWgrzNumAaPagPreNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WgrzNumAaPagPre.Len.WGRZ_NUM_AA_PAG_PRE_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-AA-PAG-PRE-UNI-NULL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzAaPagPreUni().setWgrzAaPagPreUniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WgrzAaPagPreUni.Len.WGRZ_AA_PAG_PRE_UNI_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-MM-PAG-PRE-UNI-NULL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzMmPagPreUni().setWgrzMmPagPreUniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WgrzMmPagPreUni.Len.WGRZ_MM_PAG_PRE_UNI_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-FRAZ-INI-EROG-REN-NULL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzFrazIniErogRen().setWgrzFrazIniErogRenNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WgrzFrazIniErogRen.Len.WGRZ_FRAZ_INI_EROG_REN_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-MM-1O-RAT-NULL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzMm1oRat().setWgrzMm1oRatNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WgrzMm1oRat.Len.WGRZ_MM1O_RAT_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PC-1O-RAT-NULL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzPc1oRat().setWgrzPc1oRatNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WgrzPc1oRat.Len.WGRZ_PC1O_RAT_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TP-PRSTZ-ASSTA-NULL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzTpPrstzAssta(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WgrzDati.Len.WGRZ_TP_PRSTZ_ASSTA));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-END-CARZ-NULL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzDtEndCarz().setWgrzDtEndCarzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WgrzDtEndCarz.Len.WGRZ_DT_END_CARZ_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PC-RIP-PRE-NULL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzPcRipPre().setWgrzPcRipPreNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WgrzPcRipPre.Len.WGRZ_PC_RIP_PRE_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-COD-FND-NULL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzCodFnd(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WgrzDati.Len.WGRZ_COD_FND));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-AA-REN-CER-NULL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzAaRenCer(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WgrzDati.Len.WGRZ_AA_REN_CER));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PC-REVRSB-NULL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzPcRevrsb().setWgrzPcRevrsbNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WgrzPcRevrsb.Len.WGRZ_PC_REVRSB_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TP-PC-RIP-NULL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzTpPcRip(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WgrzDati.Len.WGRZ_TP_PC_RIP));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PC-OPZ-NULL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzPcOpz().setWgrzPcOpzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WgrzPcOpz.Len.WGRZ_PC_OPZ_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TP-IAS-NULL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzTpIas(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WgrzDati.Len.WGRZ_TP_IAS));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TP-STAB-NULL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzTpStab(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WgrzDati.Len.WGRZ_TP_STAB));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TP-ADEG-PRE-NULL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzTpAdegPre(Types.HIGH_CHAR_VAL);
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-VARZ-TP-IAS-NULL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzDtVarzTpIas().setWgrzDtVarzTpIasNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WgrzDtVarzTpIas.Len.WGRZ_DT_VARZ_TP_IAS_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-FRAZ-DECR-CPT-NULL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzFrazDecrCpt().setWgrzFrazDecrCptNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WgrzFrazDecrCpt.Len.WGRZ_FRAZ_DECR_CPT_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-COD-TRAT-RIASS-NULL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzCodTratRiass(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WgrzDati.Len.WGRZ_COD_TRAT_RIASS));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TP-DT-EMIS-RIASS-NULL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzTpDtEmisRiass(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WgrzDati.Len.WGRZ_TP_DT_EMIS_RIASS));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TP-CESS-RIASS-NULL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzTpCessRiass(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WgrzDati.Len.WGRZ_TP_CESS_RIASS));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-AA-STAB-NULL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzAaStab().setWgrzAaStabNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WgrzAaStab.Len.WGRZ_AA_STAB_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TS-STAB-LIMITATA-NULL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzTsStabLimitata().setWgrzTsStabLimitataNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WgrzTsStabLimitata.Len.WGRZ_TS_STAB_LIMITATA_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-PRESC-NULL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzDtPresc().setWgrzDtPrescNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WgrzDtPresc.Len.WGRZ_DT_PRESC_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-RSH-INVST-NULL(IX-TAB-GRZ).
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzRshInvst(Types.HIGH_CHAR_VAL);
    }

    /**Original name: INIZIA-ZEROES-GRZ<br>*/
    private void iniziaZeroesGrz() {
        // COB_CODE: MOVE 0 TO (SF)-ID-GAR(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzIdGar(0);
        // COB_CODE: MOVE 0 TO (SF)-ID-POLI(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzIdPoli(0);
        // COB_CODE: MOVE 0 TO (SF)-ID-MOVI-CRZ(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzIdMoviCrz(0);
        // COB_CODE: MOVE 0 TO (SF)-DT-INI-EFF(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzDtIniEff(0);
        // COB_CODE: MOVE 0 TO (SF)-DT-END-EFF(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzDtEndEff(0);
        // COB_CODE: MOVE 0 TO (SF)-COD-COMP-ANIA(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzCodCompAnia(0);
        // COB_CODE: MOVE 0 TO (SF)-DS-RIGA(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzDsRiga(0);
        // COB_CODE: MOVE 0 TO (SF)-DS-VER(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzDsVer(0);
        // COB_CODE: MOVE 0 TO (SF)-DS-TS-INI-CPTZ(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzDsTsIniCptz(0);
        // COB_CODE: MOVE 0 TO (SF)-DS-TS-END-CPTZ(IX-TAB-GRZ).
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzDsTsEndCptz(0);
    }

    /**Original name: INIZIA-SPACES-GRZ<br>*/
    private void iniziaSpacesGrz() {
        // COB_CODE: MOVE SPACES TO (SF)-COD-TARI(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzCodTari("");
        // COB_CODE: MOVE SPACES TO (SF)-DS-OPER-SQL(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzDsOperSql(Types.SPACE_CHAR);
        // COB_CODE: MOVE SPACES TO (SF)-DS-UTENTE(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzDsUtente("");
        // COB_CODE: MOVE SPACES TO (SF)-DS-STATO-ELAB(IX-TAB-GRZ)
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzDsStatoElab(Types.SPACE_CHAR);
        // COB_CODE: MOVE SPACES TO (SF)-TP-RAMO-BILA(IX-TAB-GRZ).
        ws.getWgrzAreaGaranzia().getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().setWgrzTpRamoBila("");
    }

    /**Original name: CALL-DISPATCHER<br>
	 * <pre>----------------------------------------------------------------*
	 *      ROUTINES CALL DISPATCHER                                   *
	 * ----------------------------------------------------------------*
	 * *****************************************************************
	 *    CALL DISPATCHER
	 * *****************************************************************</pre>*/
    private void callDispatcher() {
        Idss0010 idss0010 = null;
        // COB_CODE: MOVE IDSV0001-MODALITA-ESECUTIVA
        //                              TO IDSI0011-MODALITA-ESECUTIVA
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011ModalitaEsecutiva().setIdsi0011ModalitaEsecutiva(areaIdsv0001.getAreaComune().getModalitaEsecutiva().getModalitaEsecutiva());
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //                              TO IDSI0011-CODICE-COMPAGNIA-ANIA
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceCompagniaAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: MOVE IDSV0001-COD-MAIN-BATCH
        //                              TO IDSI0011-COD-MAIN-BATCH
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodMainBatch(areaIdsv0001.getAreaComune().getCodMainBatch());
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO
        //                              TO IDSI0011-TIPO-MOVIMENTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011TipoMovimentoFormatted(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimentoFormatted());
        // COB_CODE: MOVE IDSV0001-SESSIONE
        //                              TO IDSI0011-SESSIONE
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011Sessione(areaIdsv0001.getAreaComune().getSessione());
        // COB_CODE: MOVE IDSV0001-USER-NAME
        //                              TO IDSI0011-USER-NAME
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011UserName(areaIdsv0001.getAreaComune().getUserName());
        // COB_CODE: IF IDSI0011-DATA-INIZIO-EFFETTO = 0
        //              END-IF
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataInizioEffetto() == 0) {
            // COB_CODE: IF IDSV0001-LETT-ULT-IMMAGINE-SI
            //              END-IF
            //           ELSE
            //                TO IDSI0011-DATA-INIZIO-EFFETTO
            //           END-IF
            if (areaIdsv0001.getAreaComune().getLettUltImmagine().isIdsv0001LettUltImmagineSi()) {
                // COB_CODE: IF IDSI0011-SELECT
                //           OR IDSI0011-FETCH-FIRST
                //           OR IDSI0011-FETCH-NEXT
                //                TO IDSI0011-DATA-COMPETENZA
                //           ELSE
                //                TO IDSI0011-DATA-INIZIO-EFFETTO
                //           END-IF
                if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isSelect() || ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchFirst() || ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchNext()) {
                    // COB_CODE: MOVE 99991230
                    //             TO IDSI0011-DATA-INIZIO-EFFETTO
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(99991230);
                    // COB_CODE: MOVE 999912304023595999
                    //             TO IDSI0011-DATA-COMPETENZA
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(999912304023595999L);
                }
                else {
                    // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
                    //             TO IDSI0011-DATA-INIZIO-EFFETTO
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
                }
            }
            else {
                // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
                //             TO IDSI0011-DATA-INIZIO-EFFETTO
                ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
            }
        }
        // COB_CODE: IF IDSI0011-DATA-FINE-EFFETTO = 0
        //              MOVE 99991231   TO IDSI0011-DATA-FINE-EFFETTO
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataFineEffetto() == 0) {
            // COB_CODE: MOVE 99991231   TO IDSI0011-DATA-FINE-EFFETTO
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(99991231);
        }
        // COB_CODE: IF IDSI0011-DATA-COMPETENZA = 0
        //                              TO IDSI0011-DATA-COMPETENZA
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataCompetenza() == 0) {
            // COB_CODE: MOVE IDSV0001-DATA-COMPETENZA
            //                           TO IDSI0011-DATA-COMPETENZA
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(areaIdsv0001.getAreaComune().getIdsv0001DataCompetenza());
        }
        // COB_CODE: MOVE IDSV0001-DATA-COMP-AGG-STOR
        //                              TO IDSI0011-DATA-COMP-AGG-STOR
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompAggStor(areaIdsv0001.getAreaComune().getIdsv0001DataCompAggStor());
        // COB_CODE: IF IDSI0011-TRATT-DEFAULT
        //                              TO IDSI0011-TRATTAMENTO-STORICITA
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().isTrattDefault()) {
            // COB_CODE: MOVE IDSV0001-TRATTAMENTO-STORICITA
            //                           TO IDSI0011-TRATTAMENTO-STORICITA
            ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattamentoStoricita(areaIdsv0001.getAreaComune().getTrattamentoStoricita().getTrattamentoStoricita());
        }
        // COB_CODE: MOVE IDSV0001-FORMATO-DATA-DB
        //                              TO IDSI0011-FORMATO-DATA-DB
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011FormatoDataDb().setIdsi0011FormatoDataDb(areaIdsv0001.getAreaComune().getFormatoDataDb().getFormatoDataDb());
        // COB_CODE: MOVE IDSV0001-LIVELLO-DEBUG
        //                              TO IDSI0011-LIVELLO-DEBUG
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloDebug().setIdsi0011LivelloDebugFormatted(areaIdsv0001.getAreaComune().getLivelloDebug().getIdsi0011LivelloDebugFormatted());
        // COB_CODE: MOVE 0             TO IDSI0011-ID-MOVI-ANNULLATO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011IdMoviAnnullato(0);
        // COB_CODE: SET IDSI0011-PTF-NEWLIFE          TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011IdentitaChiamante().setPtfNewlife();
        // COB_CODE: SET IDSV0012-STATUS-SHARED-MEM-KO TO TRUE
        ws.getIdsv00122().getStatusSharedMemory().setKo();
        // COB_CODE: MOVE 'IDSS0010'             TO IDSI0011-PGM
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011Pgm("IDSS0010");
        //     SET WS-ADDRESS-DIS TO ADDRESS OF IN-OUT-IDSS0010.
        // COB_CODE: CALL IDSI0011-PGM           USING IDSI0011-AREA
        //                                             IDSO0011-AREA.
        idss0010 = Idss0010.getInstance();
        idss0010.run(ws.getDispatcherVariables(), ws.getDispatcherVariables().getIdso0011Area());
        // COB_CODE: MOVE IDSO0011-SQLCODE-SIGNED
        //                                  TO IDSO0011-SQLCODE.
        ws.getDispatcherVariables().getIdso0011Area().setSqlcode(ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned());
    }

    /**Original name: S0300-RICERCA-GRAVITA-ERRORE<br>
	 * <pre>----------------------------------------------------------------*
	 *   ROUTINES GESTIONE ERRORI                                      *
	 * ----------------------------------------------------------------*
	 * MUOVO L'AREA DEL SERVIZIO NELL'AREA DATI DELL'AREA DI CONTESTO.
	 * ----->VALORIZZO I CAMPI DELLA IDSV0001</pre>*/
    private void s0300RicercaGravitaErrore() {
        Ieas9900 ieas9900 = null;
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR       TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE  'IEAS9900'              TO CALL-PGM
        ws.getIdsv0002().setCallPgm("IEAS9900");
        // COB_CODE: CALL CALL-PGM USING IDSV0001-AREA-CONTESTO
        //                               IEAI9901-AREA
        //                               IEAO9901-AREA
        //             ON EXCEPTION
        //                   THRU EX-S0310-ERRORE-FATALE
        //           END-CALL.
        try {
            ieas9900 = Ieas9900.getInstance();
            ieas9900.run(areaIdsv0001, ws.getIeai9901Area(), ws.getIeao9901Area());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
            areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
            // COB_CODE: PERFORM S0310-ERRORE-FATALE
            //              THRU EX-S0310-ERRORE-FATALE
            s0310ErroreFatale();
        }
        //SE LA CHIAMATA AL SERVIZIO HA ESITO POSITIVO (NESSUN ERRORE DI
        //SISTEMA, VALORIZZO L'AREA DI OUTPUT.
        //INCREMENTO L'INDICE DEGLI ERRORI
        // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
        areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
        //SE L'INDICE E' MINORE DI 10 ...
        // COB_CODE:      IF IDSV0001-MAX-ELE-ERRORI NOT GREATER 10
        //           *SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
        //                   END-IF
        //                ELSE
        //           *IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        //                       TO IDSV0001-DESC-ERRORE-ESTESA
        //                END-IF.
        if (areaIdsv0001.getMaxEleErrori() <= 10) {
            //SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
            // COB_CODE: IF IEAO9901-COD-ERRORE-990 = ZEROES
            //                      EX-S0300-IMPOSTA-ERRORE
            //           ELSE
            //                   IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
            //           END-IF
            if (Characters.EQ_ZERO.test(ws.getIeao9901Area().getCodErrore990Formatted())) {
                // COB_CODE: PERFORM S0300-IMPOSTA-ERRORE THRU
                //                   EX-S0300-IMPOSTA-ERRORE
                s0300ImpostaErrore();
            }
            else {
                // COB_CODE: MOVE 'IEAS9900'
                //             TO IDSV0001-COD-SERVIZIO-BE
                areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
                // COB_CODE: MOVE IEAO9901-LABEL-ERR-990
                //             TO IDSV0001-LABEL-ERR
                areaIdsv0001.getLogErrore().setLabelErr(ws.getIeao9901Area().getLabelErr990());
                // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
                //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
                // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
                areaIdsv0001.getEsito().setIdsv0001EsitoKo();
                // IMPOSTO IL CODICE ERRORE GESTITO ALL'INTERNO DEL PROGRAMMA
                // COB_CODE: MOVE IEAO9901-COD-ERRORE-990
                //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeao9901Area().getCodErrore990Formatted());
                // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
                //             TO IDSV0001-DESC-ERRORE-ESTESA
                //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
            }
        }
        else {
            //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
            // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
            //             TO IDSV0001-LABEL-ERR
            areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
            // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 'IEAS9900'
            //             TO IDSV0001-COD-SERVIZIO-BE
            areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
            // COB_CODE: MOVE 'OVERFLOW IMPAGINAZIONE ERRORI'
            //             TO IDSV0001-DESC-ERRORE-ESTESA
            areaIdsv0001.getLogErrore().setDescErroreEstesa("OVERFLOW IMPAGINAZIONE ERRORI");
        }
    }

    /**Original name: S0300-IMPOSTA-ERRORE<br>
	 * <pre>  se la gravit— di tutti gli errori dell'elaborazione
	 *   h minore di zero ( ad esempio -3) vuol dire che il return-code
	 *   dell'elaborazione complessiva deve essere abbassato da '08' a
	 *   '04'. Per fare cir utilizzo il flag IDSV0001-FORZ-RC-04
	 * IMPOSTO LA GRAVITA'</pre>*/
    private void s0300ImpostaErrore() {
        // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
        // COB_CODE: EVALUATE TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = -3
        //                       IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        //             WHEN  IEAO9901-LIV-GRAVITA = 0 OR 1 OR 2
        //                   SET IDSV0001-FORZ-RC-04-NO  TO TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = 3 OR 4
        //                   SET IDSV0001-ESITO-KO       TO TRUE
        //           END-EVALUATE
        if (ws.getIeao9901Area().getLivGravita() == -3) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-YES TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04Yes();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 2  TO
            //               IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
            areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)2));
        }
        else if (ws.getIeao9901Area().getLivGravita() == 0 || ws.getIeao9901Area().getLivGravita() == 1 || ws.getIeao9901Area().getLivGravita() == 2) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
        }
        else if (ws.getIeao9901Area().getLivGravita() == 3 || ws.getIeao9901Area().getLivGravita() == 4) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        }
        // IMPOSTO IL CODICE ERRORE
        // COB_CODE: MOVE IEAI9901-COD-ERRORE
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeai9901Area().getCodErroreFormatted());
        // SE IL LIVELLO DI GRAVITA' RESTITUITO DALLO IAS9900 E' MAGGIORE
        // DI 2 (ERRORE BLOCCANTE)...IMPOSTO L'ESITO E I DATI DI CONTESTO
        //RELATIVI ALL'ERRORE BLOCCANTE
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE
        //             TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR
        //             TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
        //             TO IDSV0001-DESC-ERRORE-ESTESA
        //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
        // COB_CODE: MOVE SPACES          TO IEAI9901-COD-SERVIZIO-BE
        //                                  IEAI9901-LABEL-ERR
        //                                   IEAI9901-PARAMETRI-ERR.
        ws.getIeai9901Area().setCodServizioBe("");
        ws.getIeai9901Area().setLabelErr("");
        ws.getIeai9901Area().setParametriErr("");
        // COB_CODE: MOVE ZEROES          TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErrore(0);
    }

    /**Original name: S0310-ERRORE-FATALE<br>
	 * <pre>IMPOSTO LA GRAVITA' ERRORE</pre>*/
    private void s0310ErroreFatale() {
        // COB_CODE: MOVE  3
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)3));
        // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
        areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        //IMPOSTO IL CODICE ERRORE (ERRORE FISSO)
        // COB_CODE: MOVE 900001
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErrore(900001);
        //IMPOSTO NOME DEL MODULO
        // COB_CODE: MOVE 'IEAS9900'               TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
        //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
        //              TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
        // COB_CODE: MOVE  'ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900'
        //              TO IDSV0001-DESC-ERRORE-ESTESA
        //                 IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
    }

    /**Original name: S0290-ERRORE-DI-SISTEMA<br>
	 * <pre>**-------------------------------------------------------------**
	 *     PROGRAMMA ..... IERP9902
	 *     TIPOLOGIA...... COPY
	 *     DESCRIZIONE.... ROUTINE GENERALIZZATA GESTIONE ERRORI CALL
	 * **-------------------------------------------------------------**</pre>*/
    private void s0290ErroreDiSistema() {
        // COB_CODE: MOVE '005006'           TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErroreFormatted("005006");
        // COB_CODE: MOVE CALL-DESC          TO IEAI9901-PARAMETRI-ERR.
        ws.getIeai9901Area().setParametriErr(ws.getIdsv0002().getCallDesc());
        // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
        //              THRU EX-S0300.
        s0300RicercaGravitaErrore();
    }

    /**Original name: CALL-VALORIZZATORE<br>
	 * <pre>  ---------------------------------------------------------------
	 *   ROUTINES CALL AL VALORIZZATORE VARIABILI
	 *   ---------------------------------------------------------------
	 * *****************************************************************
	 *     CALL VALORIZZATORE VARIABILI
	 * *****************************************************************</pre>*/
    private void callValorizzatore() {
        Ivvs0211 ivvs0211 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE IDSV0001-MODALITA-ESECUTIVA
        //                TO (SF)-MODALITA-ESECUTIVA
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().getModalitaEsecutiva().setModalitaEsecutiva(areaIdsv0001.getAreaComune().getModalitaEsecutiva().getModalitaEsecutiva());
        // COB_CODE: IF (SF)-COD-COMPAGNIA-ANIA = 0
        //                TO (SF)-COD-COMPAGNIA-ANIA
        //           END-IF
        if (ws.getAreaIoIvvs0211().getIvvc0211DatiInput().getCodCompagniaAnia() == 0) {
            // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
            //             TO (SF)-COD-COMPAGNIA-ANIA
            ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setS211CodCompagniaAniaFormatted(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAniaFormatted());
        }
        // COB_CODE: IF (SF)-TIPO-MOVIMENTO = 0
        //                TO (SF)-TIPO-MOVIMENTO
        //           END-IF
        if (ws.getAreaIoIvvs0211().getIvvc0211DatiInput().getTipoMovimento() == 0) {
            // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO
            //             TO (SF)-TIPO-MOVIMENTO
            ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setS211TipoMovimentoFormatted(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimentoFormatted());
        }
        // COB_CODE: IF (SF)-COD-MAIN-BATCH = SPACES
        //                TO (SF)-COD-MAIN-BATCH
        //           END-IF
        if (Characters.EQ_SPACE.test(ws.getAreaIoIvvs0211().getIvvc0211DatiInput().getCodMainBatch())) {
            // COB_CODE: MOVE IDSV0001-COD-MAIN-BATCH
            //             TO (SF)-COD-MAIN-BATCH
            ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setCodMainBatch(areaIdsv0001.getAreaComune().getCodMainBatch());
        }
        // COB_CODE: IF (SF)-COD-SERVIZIO-BE = SPACES
        //                TO (SF)-COD-SERVIZIO-BE
        //           END-IF
        if (Characters.EQ_SPACE.test(ws.getAreaIoIvvs0211().getIvvc0211RestoDati().getCampiEsito().getCodServizioBe())) {
            // COB_CODE: MOVE IDSV0001-COD-SERVIZIO-BE
            //             TO (SF)-COD-SERVIZIO-BE
            ws.getAreaIoIvvs0211().getIvvc0211RestoDati().getCampiEsito().setCodServizioBe(areaIdsv0001.getLogErrore().getCodServizioBe());
        }
        // COB_CODE: IF (SF)-DATA-EFFETTO = 0
        //                TO (SF)-DATA-EFFETTO
        //           END-IF
        if (ws.getAreaIoIvvs0211().getIvvc0211DatiInput().getDataEffetto() == 0) {
            // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
            //             TO (SF)-DATA-EFFETTO
            ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setDataEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
        }
        // COB_CODE: IF (SF)-DATA-COMPETENZA = 0
        //                TO (SF)-DATA-COMPETENZA
        //           END-IF
        if (ws.getAreaIoIvvs0211().getIvvc0211DatiInput().getDataCompetenza() == 0) {
            // COB_CODE: MOVE IDSV0001-DATA-COMPETENZA
            //             TO (SF)-DATA-COMPETENZA
            ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setDataCompetenza(areaIdsv0001.getAreaComune().getIdsv0001DataCompetenza());
        }
        // COB_CODE: MOVE IDSV0001-DATA-COMP-AGG-STOR
        //                TO (SF)-DATA-COMP-AGG-STOR
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setDataCompAggStor(areaIdsv0001.getAreaComune().getIdsv0001DataCompAggStor());
        // COB_CODE: MOVE IDSV0001-TRATTAMENTO-STORICITA
        //                TO (SF)-TRATTAMENTO-STORICITA
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setTrattamentoStoricita(areaIdsv0001.getAreaComune().getTrattamentoStoricita().getTrattamentoStoricita());
        // COB_CODE: MOVE IDSV0001-FORMATO-DATA-DB
        //                TO (SF)-FORMATO-DATA-DB
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().getFormatoDataDb().setFormatoDataDb(areaIdsv0001.getAreaComune().getFormatoDataDb().getFormatoDataDb());
        // COB_CODE: MOVE IDSV0001-AREA-ADDRESSES
        //                TO (SF)-AREA-ADDRESSES
        ws.getAreaIoIvvs0211().getIvvc0211RestoDati().setS211AreaAddressesBytes(areaIdsv0001.getAreaComune().getIdsv0001AreaAddressesBytes());
        // COB_CODE: MOVE 'IVVS0211' TO (SF)-PGM
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setPgm("IVVS0211");
        // COB_CODE: CALL (SF)-PGM USING AREA-IO-IVVS0211
        //           ON EXCEPTION
        //                      THRU EX-S0290
        //           END-CALL.
        try {
            ivvs0211 = Ivvs0211.getInstance();
            ivvs0211.run(ws.getAreaIoIvvs0211());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'CALL VALORIZZATORE'
            //             TO CALL-DESC
            ws.getIdsv0002().setCallDesc("CALL VALORIZZATORE");
            // COB_CODE: MOVE 'CALL-VALORIZZATORE'
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("CALL-VALORIZZATORE");
            // COB_CODE: PERFORM S0290-ERRORE-DI-SISTEMA
            //              THRU EX-S0290
            s0290ErroreDiSistema();
        }
        // COB_CODE: IF NOT (SF)-SUCCESSFUL-RC
        //           END-IF.
        if (!ws.getAreaIoIvvs0211().getIvvc0211RestoDati().getReturnCode().isSuccessfulRc()) {
            // COB_CODE:         IF (SF)-CALCOLO-NON-COMPLETO
            //           *
            //                      END-IF
            //                   ELSE
            //                   END-IF
            //                END-IF.
            if (ws.getAreaIoIvvs0211().getIvvc0211RestoDati().getReturnCode().isCalcoloNonCompleto()) {
                //
                // COB_CODE: IF (SF)-ELE-MAX-NOT-FOUND GREATER ZEROES
                //                 THRU EX-S0300
                //           END-IF
                if (ws.getAreaIoIvvs0211().getIvvc0211RestoDati().getEleMaxNotFound() > 0) {
                    // COB_CODE: MOVE (SF)-PGM
                    //             TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getAreaIoIvvs0211().getIvvc0211DatiInput().getPgm());
                    // COB_CODE: MOVE 'CALL-VALORIZZATORE'
                    //             TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("CALL-VALORIZZATORE");
                    // COB_CODE: MOVE (SF)-AREA-VAR-NOT-FOUND
                    //             TO IEAI9901-PARAMETRI-ERR
                    ws.getIeai9901Area().setParametriErr(ws.getAreaIoIvvs0211().getIvvc0211RestoDati().getS211AreaVarNotFoundFormatted());
                    // COB_CODE: MOVE '005236'
                    //             TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005236");
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                }
                //
                // COB_CODE: IF (SF)-ELE-MAX-CALC-KO GREATER ZEROES
                //                 THRU EX-S0300
                //           END-IF
                if (ws.getAreaIoIvvs0211().getIvvc0211RestoDati().getEleMaxCalcKo() > 0) {
                    // COB_CODE: MOVE (SF)-PGM
                    //             TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getAreaIoIvvs0211().getIvvc0211DatiInput().getPgm());
                    // COB_CODE: MOVE 'CALL-VALORIZZATORE'
                    //             TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("CALL-VALORIZZATORE");
                    // COB_CODE: MOVE (SF)-AREA-CALC-KO
                    //             TO IEAI9901-PARAMETRI-ERR
                    ws.getIeai9901Area().setParametriErr(ws.getAreaIoIvvs0211().getIvvc0211RestoDati().getS211AreaCalcKoFormatted());
                    // COB_CODE: MOVE '005237'
                    //             TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005237");
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                }
            }
            else if (ws.getAreaIoIvvs0211().getIvvc0211RestoDati().getReturnCode().isCachePiena()) {
                // COB_CODE:   IF (SF)-CACHE-PIENA
                //                 THRU EX-S0300
                //             ELSE
                //             END-IF
                //           END-IF
                // COB_CODE: MOVE (SF)-PGM
                //             TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getAreaIoIvvs0211().getIvvc0211DatiInput().getPgm());
                // COB_CODE: MOVE 'CALL-VALORIZZATORE'
                //             TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr("CALL-VALORIZZATORE");
                // COB_CODE: MOVE SPACES
                //             TO IEAI9901-PARAMETRI-ERR
                ws.getIeai9901Area().setParametriErr("");
                // COB_CODE: MOVE '005283'
                //             TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("005283");
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
            }
            else if (ws.getAreaIoIvvs0211().getIvvc0211RestoDati().getReturnCode().isS211GlovarlistVuota() || ws.getAreaIoIvvs0211().getIvvc0211RestoDati().getReturnCode().isS211SkipCallActuator()) {
            // COB_CODE:  IF (SF)-GLOVARLIST-VUOTA
            //            OR (SF)-SKIP-CALL-ACTUATOR
            //               CONTINUE
            //            ELSE
            //                 THRU EX-S0300
            //           END-IF
            // COB_CODE: CONTINUE
            //continue
            }
            else {
                // COB_CODE: MOVE (SF)-COD-SERVIZIO-BE
                //                                     TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getAreaIoIvvs0211().getIvvc0211RestoDati().getCampiEsito().getCodServizioBe());
                // COB_CODE: MOVE 'CALL-VALORIZZATORE' TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr("CALL-VALORIZZATORE");
                // COB_CODE: MOVE '001114'             TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("001114");
                // COB_CODE: STRING (SF)-DESCRIZ-ERR  DELIMITED BY '     '
                //                   ' - '
                //                  (SF)-NOME-TABELLA DELIMITED BY SIZE
                //           INTO IEAI9901-PARAMETRI-ERR
                concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, Functions.substringBefore(ws.getAreaIoIvvs0211().getIvvc0211RestoDati().getCampiEsito().getDescrizErrFormatted(), "     "), " - ", ws.getAreaIoIvvs0211().getIvvc0211RestoDati().getCampiEsito().getS211NomeTabellaFormatted());
                ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
            }
        }
    }

    /**Original name: INIZIA-AREA-ERR-DEROGA<br>
	 * <pre>  ---------------------------------------------------------------
	 *      INIZIALIZZAZIONE CAMPI ERRORE DEROGA
	 *   ---------------------------------------------------------------
	 * ------------------------------------------------------------
	 * ------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    INIZIALIZZAZIONE CAMPI ERRORE DEROGA
	 *    CREAZIONE 31-GEN-2008
	 * ------------------------------------------------------------</pre>*/
    private void iniziaAreaErrDeroga() {
        // COB_CODE: PERFORM VARYING (SF)-NUM-ELE-MOT-DEROGA FROM 1 BY 1
        //                     UNTIL (SF)-NUM-ELE-MOT-DEROGA > 20
        //                TO (SF)-MOT-DER-STEP-ELAB((SF)-NUM-ELE-MOT-DEROGA)
        //           END-PERFORM.
        wcomIoStati.getLccc0001().getDatiDeroghe().setNumEleMotDeroga(((short)1));
        while (!(wcomIoStati.getLccc0001().getDatiDeroghe().getNumEleMotDeroga() > 20)) {
            // COB_CODE: MOVE ZEROES
            //             TO (SF)-ID-MOT-DEROGA ((SF)-NUM-ELE-MOT-DEROGA)
            wcomIoStati.getLccc0001().getDatiDeroghe().getTabMotDeroga(wcomIoStati.getLccc0001().getDatiDeroghe().getNumEleMotDeroga()).setIdMotDeroga(0);
            // COB_CODE: MOVE SPACES
            //             TO (SF)-COD-ERR ((SF)-NUM-ELE-MOT-DEROGA)
            wcomIoStati.getLccc0001().getDatiDeroghe().getTabMotDeroga(wcomIoStati.getLccc0001().getDatiDeroghe().getNumEleMotDeroga()).setCodErr("");
            // COB_CODE: MOVE SPACES
            //             TO (SF)-DESCRIZIONE-ERR ((SF)-NUM-ELE-MOT-DEROGA)
            wcomIoStati.getLccc0001().getDatiDeroghe().getTabMotDeroga(wcomIoStati.getLccc0001().getDatiDeroghe().getNumEleMotDeroga()).setDescrizioneErr("");
            // COB_CODE: MOVE SPACES
            //             TO (SF)-TP-ERR ((SF)-NUM-ELE-MOT-DEROGA)
            wcomIoStati.getLccc0001().getDatiDeroghe().getTabMotDeroga(wcomIoStati.getLccc0001().getDatiDeroghe().getNumEleMotDeroga()).setTpErr("");
            // COB_CODE: MOVE SPACES
            //             TO (SF)-TP-MOT-DEROGA ((SF)-NUM-ELE-MOT-DEROGA)
            wcomIoStati.getLccc0001().getDatiDeroghe().getTabMotDeroga(wcomIoStati.getLccc0001().getDatiDeroghe().getNumEleMotDeroga()).setTpMotDeroga("");
            // COB_CODE: MOVE ZEROES
            //             TO (SF)-COD-LIV-AUTORIZZATIVO((SF)-NUM-ELE-MOT-DEROGA)
            wcomIoStati.getLccc0001().getDatiDeroghe().getTabMotDeroga(wcomIoStati.getLccc0001().getDatiDeroghe().getNumEleMotDeroga()).setCodLivAutorizzativo(0);
            // COB_CODE: MOVE SPACES
            //             TO (SF)-IDC-FORZABILITA ((SF)-NUM-ELE-MOT-DEROGA)
            wcomIoStati.getLccc0001().getDatiDeroghe().getTabMotDeroga(wcomIoStati.getLccc0001().getDatiDeroghe().getNumEleMotDeroga()).setIdcForzabilita(Types.SPACE_CHAR);
            // COB_CODE: MOVE ZEROES
            //             TO (SF)-MOT-DER-DT-EFFETTO ((SF)-NUM-ELE-MOT-DEROGA)
            wcomIoStati.getLccc0001().getDatiDeroghe().getTabMotDeroga(wcomIoStati.getLccc0001().getDatiDeroghe().getNumEleMotDeroga()).setMotDerDtEffetto(0);
            // COB_CODE: MOVE ZEROES
            //             TO (SF)-MOT-DER-DT-COMPETENZA ((SF)-NUM-ELE-MOT-DEROGA)
            wcomIoStati.getLccc0001().getDatiDeroghe().getTabMotDeroga(wcomIoStati.getLccc0001().getDatiDeroghe().getNumEleMotDeroga()).setMotDerDtCompetenza(0);
            // COB_CODE: MOVE ZEROES
            //             TO (SF)-MOT-DER-DS-VER ((SF)-NUM-ELE-MOT-DEROGA)
            wcomIoStati.getLccc0001().getDatiDeroghe().getTabMotDeroga(wcomIoStati.getLccc0001().getDatiDeroghe().getNumEleMotDeroga()).setMotDerDsVer(0);
            // COB_CODE: MOVE ZEROES
            //             TO (SF)-MOT-DER-STEP-ELAB((SF)-NUM-ELE-MOT-DEROGA)
            wcomIoStati.getLccc0001().getDatiDeroghe().getTabMotDeroga(wcomIoStati.getLccc0001().getDatiDeroghe().getNumEleMotDeroga()).setMotDerStepElab('0');
            wcomIoStati.getLccc0001().getDatiDeroghe().setNumEleMotDeroga(Trunc.toShort(wcomIoStati.getLccc0001().getDatiDeroghe().getNumEleMotDeroga() + 1, 3));
        }
        // COB_CODE: MOVE ZEROES TO (SF)-NUM-ELE-MOT-DEROGA.
        wcomIoStati.getLccc0001().getDatiDeroghe().setNumEleMotDeroga(((short)0));
    }

    /**Original name: LOAP0001-CONTROLLI<br>
	 * <pre>----------------------------------------------------------------*
	 *  GESTIONE DEI CONTROLLI SU PRESENZA DI :
	 *       BLOCCHI (TABELLA OGGETTO BLOCCO) ->  LCCS0022
	 *       MOVIMENTI FUTURI BLOCCANTI       ->  LCCS0023
	 *       DEROGHE BLOCCANTI                ->  LOAS0280
	 * ----------------------------------------------------------------*</pre>*/
    private void loap0001Controlli() {
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                      THRU LOAP1-S100-CTRL-BLOCCHI-EX
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE: PERFORM LOAP1-S100-CTRL-BLOCCHI
            //              THRU LOAP1-S100-CTRL-BLOCCHI-EX
            loap1S100CtrlBlocchi();
            //
        }
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                      THRU LOAP1-S200-CTRL-MOV-FUT-EX
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE: PERFORM LOAP1-S200-CTRL-MOV-FUT
            //              THRU LOAP1-S200-CTRL-MOV-FUT-EX
            loap1S200CtrlMovFut();
            //
        }
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                      THRU LOAP1-S300-CTRL-DEROGHE-EX
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE: PERFORM LOAP1-S300-CTRL-DEROGHE
            //              THRU LOAP1-S300-CTRL-DEROGHE-EX
            loap1S300CtrlDeroghe();
            //
        }
    }

    /**Original name: LOAP1-S100-CTRL-BLOCCHI<br>
	 * <pre>----------------------------------------------------------------*
	 *  VERIFICA PRESENZA DI BLOCCHI (TABELLA OGGETTO BLOCCO)
	 *                          LCCS0022
	 * ----------------------------------------------------------------*</pre>*/
    private void loap1S100CtrlBlocchi() {
        // COB_CODE: PERFORM LOAP1-S110-INPUT-LCCS0022
        //              THRU LOAP1-S110-INPUT-LCCS0022-EX.
        loap1S110InputLccs0022();
        //
        // COB_CODE: PERFORM LOAP1-S120-CALL-LCCS0022
        //              THRU LOAP1-S120-CALL-LCCS0022-EX.
        loap1S120CallLccs0022();
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                   END-IF
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE:         IF ALPO-CALL
            //           *
            //                         THRU LOAP1-S130-OUT-LCCS0022-A-EX
            //           *
            //                   ELSE
            //           *
            //                         THRU LOAP1-S140-OUT-LCCS0022-S-EX
            //           *
            //                   END-IF
            if (wcomIoStati.getLccc0261().getBsCallType().isAlpoCall()) {
                //
                // COB_CODE: PERFORM LOAP1-S130-OUT-LCCS0022-A
                //              THRU LOAP1-S130-OUT-LCCS0022-A-EX
                loap1S130OutLccs0022A();
                //
            }
            else {
                //
                // COB_CODE: PERFORM LOAP1-S140-OUT-LCCS0022-S
                //              THRU LOAP1-S140-OUT-LCCS0022-S-EX
                loap1S140OutLccs0022S();
                //
            }
            //
        }
    }

    /**Original name: LOAP1-S110-INPUT-LCCS0022<br>
	 * <pre>----------------------------------------------------------------*
	 *  VERIFICA PRESENZA DI BLOCCHI (TABELLA OGGETTO BLOCCO)
	 *  VALORIZZAZIONE INPUT SERVIZIO LCCS0022
	 * ----------------------------------------------------------------*</pre>*/
    private void loap1S110InputLccs0022() {
        // COB_CODE: INITIALIZE AREA-IO-LCCS0022.
        initAreaIoLccs0022();
        //
        // COB_CODE: SET LCCC0022-BLOCCO-ATTIVO
        //             TO TRUE.
        ws.getAreaIoLccs0022().getTpStatoBlocco().setLccc0022BloccoAttivo();
        //
        // COB_CODE:      IF INDIVIDUALE
        //           *
        //                     TO WK-OGGETTO-9
        //           *
        //                ELSE
        //           *
        //                     TO WK-OGGETTO-9
        //           *
        //                END-IF.
        if (ws.getWsTpFrmAssva().isIndividuale()) {
            //
            // COB_CODE: SET POLIZZA
            //             TO TRUE
            ws.getWsTpOgg().setPolizza();
            // COB_CODE: MOVE WS-TP-OGG
            //             TO LCCC0022-TP-OGG
            ws.getAreaIoLccs0022().setTpOgg(ws.getWsTpOgg().getWsTpOgg());
            // COB_CODE: MOVE LOAP1-ID-POLI
            //             TO WK-OGGETTO-9
            ws.setWkOggetto9(TruncAbs.toInt(ws.getLoap1IdPoli(), 9));
            //
        }
        else {
            //
            // COB_CODE: SET ADESIONE
            //             TO TRUE
            ws.getWsTpOgg().setAdesione();
            // COB_CODE: MOVE WS-TP-OGG
            //             TO LCCC0022-TP-OGG
            ws.getAreaIoLccs0022().setTpOgg(ws.getWsTpOgg().getWsTpOgg());
            // COB_CODE: MOVE LOAP1-ID-ADES
            //             TO WK-OGGETTO-9
            ws.setWkOggetto9(TruncAbs.toInt(ws.getLoap1IdAdes(), 9));
            //
        }
        //
        // COB_CODE: MOVE LOAP1-ID-POLI
        //             TO LCCC0022-ID-POLI.
        ws.getAreaIoLccs0022().setIdPoli(ws.getLoap1IdPoli());
        // COB_CODE: MOVE LOAP1-ID-ADES
        //             TO LCCC0022-ID-ADES.
        ws.getAreaIoLccs0022().setIdAdes(ws.getLoap1IdAdes());
        // COB_CODE: MOVE WK-OGGETTO-9
        //             TO WK-OGGETTO-X.
        ws.setWkOggettoX(ws.getWkOggetto9Formatted());
    }

    /**Original name: LOAP1-S120-CALL-LCCS0022<br>
	 * <pre>----------------------------------------------------------------*
	 *  VERIFICA PRESENZA DI BLOCCHI (TABELLA OGGETTO BLOCCO)
	 *  CHIAMATA AL SERVIZIO LCCS0022
	 * ----------------------------------------------------------------*</pre>*/
    private void loap1S120CallLccs0022() {
        Lccs0022 lccs0022 = null;
        // COB_CODE:      CALL LCCS0022  USING IDSV0001-AREA-CONTESTO
        //                                     WCOM-AREA-STATI
        //                                     AREA-IO-LCCS0022
        //           *
        //                ON EXCEPTION
        //           *
        //                       THRU EX-S0290
        //           *
        //                END-CALL.
        try {
            lccs0022 = Lccs0022.getInstance();
            lccs0022.run(areaIdsv0001, wcomIoStati, ws.getAreaIoLccs0022());
        }
        catch (ProgramExecutionException __ex) {
            //
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'SERVIZIO VERIFICA BLOCCHI - LCCS0022'
            //             TO CALL-DESC
            ws.getIdsv0002().setCallDesc("SERVIZIO VERIFICA BLOCCHI - LCCS0022");
            // COB_CODE: MOVE 'LOAP1-S120-CALL-LCCS0022'
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("LOAP1-S120-CALL-LCCS0022");
            // COB_CODE: PERFORM S0290-ERRORE-DI-SISTEMA
            //              THRU EX-S0290
            s0290ErroreDiSistema();
            //
        }
    }

    /**Original name: LOAP1-S130-OUT-LCCS0022-A<br>
	 * <pre>----------------------------------------------------------------*
	 *  VERIFICA PRESENZA DI BLOCCHI (TABELLA OGGETTO BLOCCO)
	 *  VERIFICA OUTPUT SERVIZIO LCCS0022 - CHIAMATA DA ALPO -
	 * ----------------------------------------------------------------*</pre>*/
    private void loap1S130OutLccs0022A() {
        // COB_CODE:      IF LCCC0022-FL-PRES-GRAV-BLOC = 'S'
        //           *
        //                   END-PERFORM
        //           *
        //                END-IF.
        if (ws.getAreaIoLccs0022().getFlPresGravBloc() == 'S') {
            //
            // COB_CODE:         PERFORM VARYING IX-TAB-BLC FROM 1 BY 1
            //                     UNTIL IX-TAB-BLC > LCCC0022-ELE-MAX-OGG-BLOC
            //           *
            //                       END-IF
            //           *
            //                   END-PERFORM
            ws.getIxIndici().setTabBlc(((short)1));
            while (!(ws.getIxIndici().getTabBlc() > ws.getAreaIoLccs0022().getEleMaxOggBloc())) {
                //
                // COB_CODE:             IF LCCC0022-OGB-DT-EFFETTO(IX-TAB-BLC)  <
                //                          WK-DT-EFF-BLC
                //           *
                //                          END-IF
                //           *
                //                       END-IF
                if (ws.getAreaIoLccs0022().getTabOggBlocco(ws.getIxIndici().getTabBlc()).getDtEffetto() < ws.getWkDtEffBlc()) {
                    //
                    // COB_CODE:                IF LCCC0022-OGB-GRAVITA (IX-TAB-BLC) = 'B'
                    //           *
                    //                               TO WK-APPO-OGB-ID-OGG-BLOCCO
                    //           *
                    //                          END-IF
                    if (ws.getAreaIoLccs0022().getTabOggBlocco(ws.getIxIndici().getTabBlc()).getGravita() == 'B') {
                        //
                        // COB_CODE: SET WK-BLC-TROVATO-SI
                        //             TO TRUE
                        ws.getWkBlcTrovato().setSi();
                        // COB_CODE: MOVE LCCC0022-OGB-DT-EFFETTO (IX-TAB-BLC)
                        //             TO WK-DT-EFF-BLC
                        ws.setWkDtEffBlc(ws.getAreaIoLccs0022().getTabOggBlocco(ws.getIxIndici().getTabBlc()).getDtEffetto());
                        // COB_CODE: MOVE LCCC0022-OGB-ID-OGG-BLOCCO (IX-TAB-BLC)
                        //             TO WK-APPO-OGB-ID-OGG-BLOCCO
                        ws.setWkAppoOgbIdOggBlocco(ws.getAreaIoLccs0022().getTabOggBlocco(ws.getIxIndici().getTabBlc()).getIdOggBlocco());
                        //
                    }
                    //
                }
                //
                ws.getIxIndici().setTabBlc(Trunc.toShort(ws.getIxIndici().getTabBlc() + 1, 4));
            }
            //
        }
        //
        // COB_CODE:      IF WK-BLC-TROVATO-SI
        //           *
        //                   END-IF
        //           *
        //                END-IF.
        if (ws.getWkBlcTrovato().isSi()) {
            //
            // COB_CODE:         IF WK-APPO-OGB-ID-OGG-BLOCCO NOT = WCOM-ID-BLOCCO-CRZ
            //           *
            //                       TO TRUE
            //           *
            //                   END-IF
            if (ws.getWkAppoOgbIdOggBlocco() != wcomIoStati.getLccc0261().getDatiBlocco().getIdBloccoCrz()) {
                //
                // COB_CODE: SET IDSV0001-ESITO-KO
                //            TO TRUE
                areaIdsv0001.getEsito().setIdsv0001EsitoKo();
                //
            }
            //
        }
    }

    /**Original name: LOAP1-S140-OUT-LCCS0022-S<br>
	 * <pre>----------------------------------------------------------------*
	 *  VERIFICA PRESENZA DI BLOCCHI (TABELLA OGGETTO BLOCCO)
	 *  VERIFICA OUTPUT SERVIZIO LCCS0022 - CHIAMATA STANDARD -
	 * ----------------------------------------------------------------*</pre>*/
    private void loap1S140OutLccs0022S() {
        ConcatUtil concatUtil = null;
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                   END-IF
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE:         IF LCCC0022-FL-PRES-GRAV-BLOC = 'S'
            //           *
            //                         THRU EX-S0300
            //           *
            //                   END-IF
            if (ws.getAreaIoLccs0022().getFlPresGravBloc() == 'S') {
                //
                // COB_CODE: MOVE WK-PGM
                //             TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE 'LOAP1-S140-LCCS0022-STD'
                //             TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr("LOAP1-S140-LCCS0022-STD");
                // COB_CODE: MOVE '005204'
                //             TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("005204");
                // COB_CODE: STRING WK-OGGETTO-X   ';'
                //                  LCCC0022-OGB-COD-BLOCCO(1)
                //           DELIMITED BY SIZE
                //           INTO IEAI9901-PARAMETRI-ERR
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkOggettoXFormatted(), ";", ws.getAreaIoLccs0022().getTabOggBlocco(1).getLccc0022OgbCodBloccoFormatted());
                ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
                //
            }
            //
        }
    }

    /**Original name: LOAP1-S200-CTRL-MOV-FUT<br>
	 * <pre>----------------------------------------------------------------*
	 *  VERIFICA PRESENZA DI MOVIMENTI FUTURI BLOCCANTI
	 *                          LCCS0023
	 * ----------------------------------------------------------------*</pre>*/
    private void loap1S200CtrlMovFut() {
        // COB_CODE: PERFORM LOAP1-S210-INPUT-LCCS0023
        //              THRU LOAP1-S210-INPUT-LCCS0023-EX.
        loap1S210InputLccs0023();
        //
        // COB_CODE: PERFORM LOAP1-S220-CALL-LCCS0023
        //              THRU LOAP1-S220-CALL-LCCS0023-EX.
        loap1S220CallLccs0023();
        //
        // COB_CODE: PERFORM LOAP1-S230-OUT-LCCS0023
        //              THRU LOAP1-S230-OUT-LCCS0023-EX.
        loap1S230OutLccs0023();
    }

    /**Original name: LOAP1-S210-INPUT-LCCS0023<br>
	 * <pre>----------------------------------------------------------------*
	 *  VERIFICA PRESENZA DI MOVIMENTI FUTURI BLOCCANTI
	 *  VALORIZZAZIONE INPUT SERVIZIO LCCS0023
	 * ----------------------------------------------------------------*</pre>*/
    private void loap1S210InputLccs0023() {
        // COB_CODE: INITIALIZE AREA-IO-LCCS0023.
        initAreaIoLccs0023();
        //
        // COB_CODE:      IF INDIVIDUALE
        //           *
        //                        WK-OGGETTO-9
        //           *
        //                ELSE
        //           *
        //                        WK-OGGETTO-9
        //           *
        //                END-IF.
        if (ws.getWsTpFrmAssva().isIndividuale()) {
            //
            // COB_CODE: SET POLIZZA
            //             TO TRUE
            ws.getWsTpOgg().setPolizza();
            // COB_CODE: MOVE WS-TP-OGG
            //             TO LCCC0023-TP-OGG-PTF
            ws.getAreaIoLccs0023().setTpOggPtf(ws.getWsTpOgg().getWsTpOgg());
            // COB_CODE: MOVE LOAP1-ID-POLI
            //             TO LCCC0023-ID-OGG-PTF
            //                WK-OGGETTO-9
            ws.getAreaIoLccs0023().setIdOggPtf(ws.getLoap1IdPoli());
            ws.setWkOggetto9(TruncAbs.toInt(ws.getLoap1IdPoli(), 9));
            //
        }
        else {
            //
            // COB_CODE: SET ADESIONE
            //             TO TRUE
            ws.getWsTpOgg().setAdesione();
            // COB_CODE: MOVE WS-TP-OGG
            //             TO LCCC0023-TP-OGG-PTF
            ws.getAreaIoLccs0023().setTpOggPtf(ws.getWsTpOgg().getWsTpOgg());
            // COB_CODE: MOVE LOAP1-ID-ADES
            //             TO LCCC0023-ID-OGG-PTF
            //                WK-OGGETTO-9
            ws.getAreaIoLccs0023().setIdOggPtf(ws.getLoap1IdAdes());
            ws.setWkOggetto9(TruncAbs.toInt(ws.getLoap1IdAdes(), 9));
            //
        }
        //
        // COB_CODE: MOVE WK-OGGETTO-9
        //             TO WK-OGGETTO-X.
        ws.setWkOggettoX(ws.getWkOggetto9Formatted());
    }

    /**Original name: LOAP1-S220-CALL-LCCS0023<br>
	 * <pre>----------------------------------------------------------------*
	 *  VERIFICA PRESENZA DI MOVIMENTI FUTURI BLOCCANTI
	 *  CHIAMATA AL SERVIZIO LCCS0023
	 * ----------------------------------------------------------------*</pre>*/
    private void loap1S220CallLccs0023() {
        Lccs0023 lccs0023 = null;
        // COB_CODE:      CALL LCCS0023  USING IDSV0001-AREA-CONTESTO
        //                                     WCOM-AREA-STATI
        //                                     AREA-IO-LCCS0023
        //           *
        //                ON EXCEPTION
        //           *
        //                       THRU EX-S0290
        //           *
        //                END-CALL.
        try {
            lccs0023 = Lccs0023.getInstance();
            lccs0023.run(areaIdsv0001, wcomIoStati, ws.getAreaIoLccs0023());
        }
        catch (ProgramExecutionException __ex) {
            //
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'SERVIZIO VERIFICA MOV. FUTURI - LCCS0023'
            //             TO CALL-DESC
            ws.getIdsv0002().setCallDesc("SERVIZIO VERIFICA MOV. FUTURI - LCCS0023");
            // COB_CODE: MOVE 'LOAP1-S220-CALL-LCCS0023'
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("LOAP1-S220-CALL-LCCS0023");
            // COB_CODE: PERFORM S0290-ERRORE-DI-SISTEMA
            //              THRU EX-S0290
            s0290ErroreDiSistema();
            //
        }
    }

    /**Original name: LOAP1-S230-OUT-LCCS0023<br>
	 * <pre>----------------------------------------------------------------*
	 *  VERIFICA PRESENZA DI MOVIMENTI FUTURI BLOCCANTI
	 *  VERIFICA OUTPUT SERVIZIO LCCS0023
	 * ----------------------------------------------------------------*</pre>*/
    private void loap1S230OutLccs0023() {
        ConcatUtil concatUtil = null;
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                   END-IF
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE:         IF LCCC0023-PRE-GRAV-BLOCCANTE = 'S'
            //           *
            //                         THRU EX-S0300
            //           *
            //                   END-IF
            if (ws.getAreaIoLccs0023().getPreGravBloccante() == 'S') {
                //
                // COB_CODE: MOVE WK-PGM
                //             TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE 'LOAP1-S230-OUT-LCCS0023'
                //             TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr("LOAP1-S230-OUT-LCCS0023");
                // COB_CODE: MOVE '005205'
                //             TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("005205");
                // COB_CODE: STRING WK-OGGETTO-X   ';'
                //                  'MOVIMENTO FUTURO BLOCCANTE'
                //           DELIMITED BY SIZE
                //           INTO IEAI9901-PARAMETRI-ERR
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkOggettoXFormatted(), ";", "MOVIMENTO FUTURO BLOCCANTE");
                ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
                //
            }
            //
        }
    }

    /**Original name: LOAP1-S300-CTRL-DEROGHE<br>
	 * <pre>----------------------------------------------------------------*
	 *  VERIFICA PRESENZA DI DEROGHE BLOCCANTI
	 *                          LOAS0280
	 * ----------------------------------------------------------------*</pre>*/
    private void loap1S300CtrlDeroghe() {
        // COB_CODE: PERFORM LOAP1-S310-INPUT-LOAS0280
        //              THRU LOAP1-S310-INPUT-LOAS0280-EX.
        loap1S310InputLoas0280();
        //
        // COB_CODE: PERFORM LOAP1-S320-CALL-LOAS0280
        //              THRU LOAP1-S320-CALL-LOAS0280-EX.
        loap1S320CallLoas0280();
        //
        // COB_CODE: PERFORM LOAP1-S330-OUT-LOAS0280
        //              THRU LOAP1-S330-OUT-LOAS0280-EX.
        loap1S330OutLoas0280();
    }

    /**Original name: LOAP1-S310-INPUT-LOAS0280<br>
	 * <pre>----------------------------------------------------------------*
	 *  VERIFICA PRESENZA DI DEROGHE BLOCCANTI
	 *  VALORIZZAZIONE INPUT SERVIZIO LOAS0280
	 * ----------------------------------------------------------------*</pre>*/
    private void loap1S310InputLoas0280() {
        // COB_CODE: INITIALIZE AREA-IO-LOAS0280.
        initAreaIoLoas0280();
        //
        // COB_CODE: MOVE LOAP1-ID-POLI
        //             TO LOAC0280-ID-POLI
        //                WK-OGGETTO-9.
        ws.getAreaIoLoas0280().setIdPoli(ws.getLoap1IdPoli());
        ws.setWkOggetto9(TruncAbs.toInt(ws.getLoap1IdPoli(), 9));
        //
        // COB_CODE:      IF COLLETTIVA
        //           *
        //                        WK-OGGETTO-9
        //           *
        //                ELSE
        //           *
        //                     TO LOAC0280-TP-OGG
        //           *
        //                END-IF.
        if (ws.getWsTpFrmAssva().isCollettiva()) {
            //
            // COB_CODE: SET ADESIONE
            //             TO TRUE
            ws.getWsTpOgg().setAdesione();
            // COB_CODE: MOVE WS-TP-OGG
            //             TO LOAC0280-TP-OGG
            ws.getAreaIoLoas0280().setTpOgg(ws.getWsTpOgg().getWsTpOgg());
            // COB_CODE: MOVE LOAP1-ID-ADES
            //             TO LOAC0280-ID-ADES
            //                WK-OGGETTO-9
            ws.getAreaIoLoas0280().setIdAdes(ws.getLoap1IdAdes());
            ws.setWkOggetto9(TruncAbs.toInt(ws.getLoap1IdAdes(), 9));
            //
        }
        else {
            //
            // COB_CODE: SET POLIZZA
            //             TO TRUE
            ws.getWsTpOgg().setPolizza();
            // COB_CODE: MOVE WS-TP-OGG
            //             TO LOAC0280-TP-OGG
            ws.getAreaIoLoas0280().setTpOgg(ws.getWsTpOgg().getWsTpOgg());
            //
        }
        //
        // COB_CODE: MOVE WK-OGGETTO-9
        //             TO WK-OGGETTO-X.
        ws.setWkOggettoX(ws.getWkOggetto9Formatted());
    }

    /**Original name: LOAP1-S320-CALL-LOAS0280<br>
	 * <pre>----------------------------------------------------------------*
	 *  VERIFICA PRESENZA DI DEROGHE BLOCCANTI
	 *  CHIAMATA AL SERVIZIO LOAS0280
	 * ----------------------------------------------------------------*</pre>*/
    private void loap1S320CallLoas0280() {
        Loas0280 loas0280 = null;
        // COB_CODE:      CALL LOAS0280  USING AREA-IDSV0001
        //                                     WCOM-AREA-STATI
        //                                     AREA-IO-LOAS0280
        //           *
        //                ON EXCEPTION
        //           *
        //                       THRU EX-S0290
        //           *
        //                END-CALL.
        try {
            loas0280 = Loas0280.getInstance();
            loas0280.run(areaIdsv0001, wcomIoStati, ws.getAreaIoLoas0280());
        }
        catch (ProgramExecutionException __ex) {
            //
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'SERVIZIO VERIFICA PRES. DEROGA - LOAS0280'
            //             TO CALL-DESC
            ws.getIdsv0002().setCallDesc("SERVIZIO VERIFICA PRES. DEROGA - LOAS0280");
            // COB_CODE: MOVE 'LOAP1-S320-CALL-LOAS0280'
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("LOAP1-S320-CALL-LOAS0280");
            // COB_CODE: PERFORM S0290-ERRORE-DI-SISTEMA
            //              THRU EX-S0290
            s0290ErroreDiSistema();
            //
        }
    }

    /**Original name: LOAP1-S330-OUT-LOAS0280<br>
	 * <pre>----------------------------------------------------------------*
	 *  VERIFICA PRESENZA DI DEROGHE BLOCCANTI
	 *  VERIFICA OUTPUT SERVIZIO LOAS0280
	 * ----------------------------------------------------------------*</pre>*/
    private void loap1S330OutLoas0280() {
        ConcatUtil concatUtil = null;
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                   END-IF
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE:         IF LOAC0280-DEROGA-BLOCCANTE-SI
            //           *
            //                         THRU EX-S0300
            //           *
            //                   END-IF
            if (ws.getAreaIoLoas0280().getGravitaDeroga().isLoac0280DerogaBloccanteSi()) {
                //
                // COB_CODE: MOVE LOAC0280-LIVELLO-DEROGA
                //             TO WK-LIV-DEROGA
                ws.setWkLivDeroga(ws.getAreaIoLoas0280().getLivelloDeroga().getLivelloDeroga());
                //
                // COB_CODE:            IF LOAC0280-LIV-POLI
                //           *
                //                           TO WK-OGGETTO-9
                //           *
                //                      ELSE
                //           *
                //                           TO WK-OGGETTO-9
                //           *
                //                      END-IF
                if (ws.getAreaIoLoas0280().getLivelloDeroga().isLoac0280LivPoli()) {
                    //
                    // COB_CODE: MOVE LOAP1-ID-POLI
                    //             TO WK-OGGETTO-9
                    ws.setWkOggetto9(TruncAbs.toInt(ws.getLoap1IdPoli(), 9));
                    //
                }
                else {
                    //
                    // COB_CODE: MOVE LOAP1-ID-ADES
                    //             TO WK-OGGETTO-9
                    ws.setWkOggetto9(TruncAbs.toInt(ws.getLoap1IdAdes(), 9));
                    //
                }
                //
                // COB_CODE: MOVE WK-OGGETTO-9
                //             TO WK-OGGETTO-X
                ws.setWkOggettoX(ws.getWkOggetto9Formatted());
                //
                // COB_CODE: MOVE WK-PGM
                //             TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE 'LOAP1-S320-CALL-LOAS0280'
                //             TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr("LOAP1-S320-CALL-LOAS0280");
                // COB_CODE: MOVE '005210'
                //             TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("005210");
                // COB_CODE: STRING WK-LIV-DEROGA ';'
                //                  WK-OGGETTO-X ';'
                //                 'BLOCCANTE'
                //           DELIMITED BY SIZE
                //           INTO IEAI9901-PARAMETRI-ERR
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkLivDerogaFormatted(), ";", ws.getWkOggettoXFormatted(), ";", "BLOCCANTE");
                ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
                //
            }
            //
        }
    }

    /**Original name: DISPLAY-LABEL<br>
	 * <pre>-----------------------------------------------------------------</pre>*/
    private void displayLabel() {
        // COB_CODE:       IF IDSV0001-DEBUG-BASSO OR
        //                    IDSV0001-DEBUG-ESASPERATO
        //           *        DISPLAY WK-LABEL-ERR
        //                    CONTINUE
        //                 END-IF.
        if (areaIdsv0001.getAreaComune().getLivelloDebug().isIdsv0001DebugBasso() || areaIdsv0001.getAreaComune().getLivelloDebug().isIdsv0001DebugEsasperato()) {
        //        DISPLAY WK-LABEL-ERR
        // COB_CODE: CONTINUE
        //continue
        }
    }

    /**Original name: DISPLAY-IB-OGG<br>*/
    private void displayIbOgg() {
        // COB_CODE: IF IDSV0001-DEBUG-ESASPERATO OR
        //              IDSV0001-DEBUG-MEDIO
        //               PERFORM DISPLAY-LABEL THRU DISPLAY-LABEL-EX
        //           END-IF.
        if (areaIdsv0001.getAreaComune().getLivelloDebug().isIdsv0001DebugEsasperato() || areaIdsv0001.getAreaComune().getLivelloDebug().isIdsv0001DebugMedio()) {
            // COB_CODE: MOVE WADE-IB-OGG(1)  TO WK-LABEL-ERR
            ws.getWkGestioneMsgErr().setLabelErr(areaMain.getWadeAreaAdesione().getTabAde(1).getLccvade1().getDati().getWadeIbOgg());
            // COB_CODE: PERFORM DISPLAY-LABEL THRU DISPLAY-LABEL-EX
            displayLabel();
            // COB_CODE: MOVE WPOL-IB-OGG     TO WK-LABEL-ERR
            ws.getWkGestioneMsgErr().setLabelErr(areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolIbOgg());
            // COB_CODE: PERFORM DISPLAY-LABEL THRU DISPLAY-LABEL-EX
            displayLabel();
        }
    }

    /**Original name: VERIFICA-PROD<br>
	 * <pre>----------------------------------------------------------------*
	 *      VERIFICA PRESENZA PRODOTTO IN AREA DI CACHE                *
	 * ----------------------------------------------------------------*</pre>*/
    private void verificaProd() {
        // COB_CODE: PERFORM VARYING IX-WK-0040 FROM 1 BY 1
        //             UNTIL IX-WK-0040 > WK-0040-ELE-MAX
        //                OR PROD-OK
        //                END-IF
        //           END-PERFORM.
        ws.getIxIndici().setWk0040(((short)1));
        while (!(ws.getIxIndici().getWk0040() > ws.getWk0040EleMax() || ws.getWkFlagProd().isOk())) {
            // COB_CODE: IF   WK-0040-COD-COMPAGNIA(IX-WK-0040) =
            //                ISPC0040-COD-COMPAGNIA
            //           AND  WK-0040-COD-PRODOTTO(IX-WK-0040) =
            //                ISPC0040-COD-PRODOTTO
            //           AND  WK-0040-COD-CONVENZIONE(IX-WK-0040) =
            //                ISPC0040-COD-CONVENZIONE
            //           AND  WK-0040-DATA-INIZ-VALID-CONV(IX-WK-0040) =
            //                ISPC0040-DATA-INIZ-VALID-CONV
            //           AND  WK-0040-DATA-RIFERIMENTO(IX-WK-0040) =
            //                ISPC0040-DATA-RIFERIMENTO
            //           AND  WK-0040-LIVELLO-UTENTE(IX-WK-0040) =
            //                ISPC0040-LIVELLO-UTENTE
            //           AND  WK-0040-SESSION-ID(IX-WK-0040) =
            //                ISPC0040-SESSION-ID
            //           AND  WK-0040-FUNZIONALITA(IX-WK-0040) =
            //                ISPC0040-FUNZIONALITA
            //                  TO WCOM-DT-ULT-VERS-PROD
            //           END-IF
            if (ws.getWk0040Tabella(ws.getIxIndici().getWk0040()).getDatiInput().getCodCompagnia() == ws.getAreaIoIsps0040().getIspc0040DatiInput().getCodCompagnia() && Conditions.eq(ws.getWk0040Tabella(ws.getIxIndici().getWk0040()).getDatiInput().getCodProdotto(), ws.getAreaIoIsps0040().getIspc0040DatiInput().getCodProdotto()) && Conditions.eq(ws.getWk0040Tabella(ws.getIxIndici().getWk0040()).getDatiInput().getCodConvenzione(), ws.getAreaIoIsps0040().getIspc0040DatiInput().getCodConvenzione()) && Conditions.eq(ws.getWk0040Tabella(ws.getIxIndici().getWk0040()).getDatiInput().getDataInizValidConv(), ws.getAreaIoIsps0040().getIspc0040DatiInput().getDataInizValidConv()) && Conditions.eq(ws.getWk0040Tabella(ws.getIxIndici().getWk0040()).getDatiInput().getDataRiferimento(), ws.getAreaIoIsps0040().getIspc0040DatiInput().getDataRiferimento()) && ws.getWk0040Tabella(ws.getIxIndici().getWk0040()).getDatiInput().getLivelloUtente() == ws.getAreaIoIsps0040().getIspc0040DatiInput().getLivelloUtente() && Conditions.eq(ws.getWk0040Tabella(ws.getIxIndici().getWk0040()).getDatiInput().getSessionId(), ws.getAreaIoIsps0040().getIspc0040DatiInput().getSessionId()) && ws.getWk0040Tabella(ws.getIxIndici().getWk0040()).getDatiInput().getFunzionalita() == ws.getAreaIoIsps0040().getIspc0040DatiInput().getFunzionalita()) {
                // COB_CODE: SET PROD-OK TO TRUE
                ws.getWkFlagProd().setOk();
                // COB_CODE: MOVE WK-0040-DATA-VERSIONE-PROD(IX-WK-0040)
                //             TO WCOM-DT-ULT-VERS-PROD
                wcomIoStati.getLccc0001().setWcomDtUltVersProdFormatted(ws.getWk0040Tabella(ws.getIxIndici().getWk0040()).getDataVersioneProdFormatted());
            }
            ws.getIxIndici().setWk0040(Trunc.toShort(ws.getIxIndici().getWk0040() + 1, 4));
        }
        //
        // COB_CODE: IF PROD-KO
        //                TO WS-0040-FUNZIONALITA
        //           END-IF.
        if (ws.getWkFlagProd().isKo()) {
            // COB_CODE: INITIALIZE WS-0040-DATI-INPUT
            initWs0040DatiInput();
            // COB_CODE: MOVE ISPC0040-COD-COMPAGNIA
            //             TO WS-0040-COD-COMPAGNIA
            ws.getWs0040DatiInput().setCodCompagniaFormatted(ws.getAreaIoIsps0040().getIspc0040DatiInput().getCodCompagniaFormatted());
            // COB_CODE: MOVE ISPC0040-COD-PRODOTTO
            //             TO WS-0040-COD-PRODOTTO
            ws.getWs0040DatiInput().setCodProdotto(ws.getAreaIoIsps0040().getIspc0040DatiInput().getCodProdotto());
            // COB_CODE: MOVE ISPC0040-COD-CONVENZIONE
            //             TO WS-0040-COD-CONVENZIONE
            ws.getWs0040DatiInput().setCodConvenzione(ws.getAreaIoIsps0040().getIspc0040DatiInput().getCodConvenzione());
            // COB_CODE: MOVE ISPC0040-DATA-INIZ-VALID-CONV
            //             TO WS-0040-DATA-INIZ-VALID-CONV
            ws.getWs0040DatiInput().setDataInizValidConv(ws.getAreaIoIsps0040().getIspc0040DatiInput().getDataInizValidConv());
            // COB_CODE: MOVE ISPC0040-DATA-RIFERIMENTO
            //             TO WS-0040-DATA-RIFERIMENTO
            ws.getWs0040DatiInput().setDataRiferimento(ws.getAreaIoIsps0040().getIspc0040DatiInput().getDataRiferimento());
            // COB_CODE: MOVE ISPC0040-LIVELLO-UTENTE
            //            TO WS-0040-LIVELLO-UTENTE
            ws.getWs0040DatiInput().setLivelloUtenteFormatted(ws.getAreaIoIsps0040().getIspc0040DatiInput().getLivelloUtenteFormatted());
            // COB_CODE: MOVE ISPC0040-SESSION-ID
            //             TO WS-0040-SESSION-ID
            ws.getWs0040DatiInput().setSessionId(ws.getAreaIoIsps0040().getIspc0040DatiInput().getSessionId());
            // COB_CODE: MOVE ISPC0040-FUNZIONALITA
            //             TO WS-0040-FUNZIONALITA
            ws.getWs0040DatiInput().setFunzionalitaFormatted(ws.getAreaIoIsps0040().getIspc0040DatiInput().getFunzionalitaFormatted());
        }
    }

    /**Original name: LOAP0002-CONTROLLI<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE  DI GESTIONE DEROGHE BLOCCANTI
	 * ----------------------------------------------------------------*
	 * ----------------------------------------------------------------*
	 *   GESTIONE DELLE DEROGHE ATTIVE PER LE FUNZIONALITA' SEGUENTI :
	 *   ADEGUAMENTO PREMIO PRESTAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void loap0002Controlli() {
        // COB_CODE: PERFORM LOAP2-GESTIONE-DEROGA
        //              THRU LOAP2-GESTIONE-DEROGA-EX.
        loap2GestioneDeroga();
    }

    /**Original name: LOAP2-GESTIONE-DEROGA<br>
	 * <pre>----------------------------------------------------------------*
	 *  LETTURA E GESTIONE DELL'ULTIMA IMMAGINE DELLA DEROGA
	 * ----------------------------------------------------------------*</pre>*/
    private void loap2GestioneDeroga() {
        ConcatUtil concatUtil = null;
        // COB_CODE: SET IDSV0001-ESITO-OK  TO TRUE.
        areaIdsv0001.getEsito().setIdsv0001EsitoOk();
        // COB_CODE: SUBTRACT 1 FROM IDSV0001-MAX-ELE-ERRORI.
        areaIdsv0001.setMaxEleErrori(Trunc.toShort(areaIdsv0001.getMaxEleErrori() - 1, 4));
        // COB_CODE: PERFORM LOAP2-LEGGI-ULT-IMMAG-DER
        //              THRU LOAP2-LEGGI-ULT-IMMAG-DER-EX.
        loap2LeggiUltImmagDer();
        // COB_CODE: IF IDSV0001-ESITO-OK
        //              END-IF
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: PERFORM LOAP2-LEGGI-MOVI-DEROGA
            //              THRU LOAP2-LEGGI-MOVI-DEROGA-EX
            loap2LeggiMoviDeroga();
            //
            // COB_CODE: IF  IDSV0001-ESITO-OK
            //               END-IF
            //           END-IF
            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                // COB_CODE: MOVE MOV-TP-MOVI  TO LOAC0002-TP-MOVI
                ws.getLoac0002AreaGestDeroghe().setLoac0002TpMovi(TruncAbs.toInt(ws.getMovi().getMovTpMovi().getMovTpMovi(), 5));
                // COB_CODE: SET LOAC0002-DEROGA-NON-TROVATA  TO TRUE
                ws.getLoac0002AreaGestDeroghe().getLoac0002FlDeroga().setNonTrovata();
                // COB_CODE: PERFORM VARYING LOAC0002-IND-R FROM 1 BY 1
                //              UNTIL LOAC0002-IND-R > LOAC0002-ELE-MAX-FUNZ
                //                 OR LOAC0002-DEROGA-TROVATA
                //              END-IF
                //           END-PERFORM
                ws.getLoac0002AreaGestDeroghe().setLoac0002IndR(((short)1));
                while (!(ws.getLoac0002AreaGestDeroghe().getLoac0002IndR() > ws.getLoac0002AreaGestDeroghe().getLoac0002EleMaxFunz() || ws.getLoac0002AreaGestDeroghe().getLoac0002FlDeroga().isTrovata())) {
                    // COB_CODE: IF  LOAC0002-FUNZ-ESEC(LOAC0002-IND-R) =
                    //               IDSV0001-TIPO-MOVIMENTO
                    //               END-PERFORM
                    //           END-IF
                    if (ws.getLoac0002AreaGestDeroghe().getLoac0002TabFunzDeroga().getFunzEsec(ws.getLoac0002AreaGestDeroghe().getLoac0002IndR()) == areaIdsv0001.getAreaComune().getIdsv0001TipoMovimento()) {
                        //                    UNTIL LOAC0002-IND-C > 30
                        // COB_CODE:                    PERFORM VARYING LOAC0002-IND-C FROM 1 BY 1
                        //           *                    UNTIL LOAC0002-IND-C > 30
                        //                                UNTIL LOAC0002-IND-C > 43
                        //                                   OR LOAC0002-DEROGA-TROVATA
                        //                                END-IF
                        //                              END-PERFORM
                        ws.getLoac0002AreaGestDeroghe().setLoac0002IndC(((short)1));
                        while (!(ws.getLoac0002AreaGestDeroghe().getLoac0002IndC() > 43 || ws.getLoac0002AreaGestDeroghe().getLoac0002FlDeroga().isTrovata())) {
                            // COB_CODE: IF LOAC0002-FUNZ-DEROG
                            //              (LOAC0002-IND-R , LOAC0002-IND-C) =
                            //               LOAC0002-TP-MOVI
                            //              SET LOAC0002-DEROGA-TROVATA TO TRUE
                            //           END-IF
                            if (ws.getLoac0002AreaGestDeroghe().getLoac0002TabFunzDeroga().getFunzDerog(ws.getLoac0002AreaGestDeroghe().getLoac0002IndR(), ws.getLoac0002AreaGestDeroghe().getLoac0002IndC()) == ws.getLoac0002AreaGestDeroghe().getLoac0002TpMovi()) {
                                // COB_CODE: SET LOAC0002-DEROGA-TROVATA TO TRUE
                                ws.getLoac0002AreaGestDeroghe().getLoac0002FlDeroga().setTrovata();
                            }
                            ws.getLoac0002AreaGestDeroghe().setLoac0002IndC(Trunc.toShort(ws.getLoac0002AreaGestDeroghe().getLoac0002IndC() + 1, 2));
                        }
                    }
                    ws.getLoac0002AreaGestDeroghe().setLoac0002IndR(Trunc.toShort(ws.getLoac0002AreaGestDeroghe().getLoac0002IndR() + 1, 2));
                }
                // COB_CODE: IF  LOAC0002-DEROGA-TROVATA
                //             END-IF
                //           END-IF
                if (ws.getLoac0002AreaGestDeroghe().getLoac0002FlDeroga().isTrovata()) {
                    // COB_CODE: code not available
                    ws.setIntRegister1(((short)1));
                    ws.getLoac0002AreaGestDeroghe().setLoac0002IndR(Trunc.toShort(abs(ws.getLoac0002AreaGestDeroghe().getLoac0002IndR() - ws.getIntRegister1()), 2));
                    ws.getLoac0002AreaGestDeroghe().setLoac0002IndC(Trunc.toShort(abs(ws.getLoac0002AreaGestDeroghe().getLoac0002IndC() - ws.getIntRegister1()), 2));
                    // COB_CODE: IF LOAC0002-TIPO-DEROGA
                    //           (LOAC0002-IND-R , LOAC0002-IND-C) = 'B'
                    //               THRU EX-S0300
                    //           END-IF
                    if (ws.getLoac0002AreaGestDeroghe().getLoac0002TabFunzDeroga().getTipoDeroga(ws.getLoac0002AreaGestDeroghe().getLoac0002IndR(), ws.getLoac0002AreaGestDeroghe().getLoac0002IndC()) == 'B') {
                        // COB_CODE: MOVE LOAC0280-LIVELLO-DEROGA
                        //             TO WK-LIV-DEROGA
                        ws.setWkLivDeroga(ws.getAreaIoLoas0280().getLivelloDeroga().getLivelloDeroga());
                        // COB_CODE: IF POLIZZA
                        //                TO WK-OGGETTO-9
                        //           ELSE
                        //                TO WK-OGGETTO-9
                        //           END-IF
                        if (ws.getWsTpOgg().isPolizza()) {
                            // COB_CODE: MOVE WPOL-ID-POLI
                            //             TO WK-OGGETTO-9
                            ws.setWkOggetto9(TruncAbs.toInt(areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolIdPoli(), 9));
                        }
                        else {
                            // COB_CODE: MOVE WADE-ID-ADES(1)
                            //             TO WK-OGGETTO-9
                            ws.setWkOggetto9(TruncAbs.toInt(areaMain.getWadeAreaAdesione().getTabAde(1).getLccvade1().getDati().getWadeIdAdes(), 9));
                        }
                        // COB_CODE: MOVE WK-OGGETTO-9
                        //             TO WK-OGGETTO-X
                        ws.setWkOggettoX(ws.getWkOggetto9Formatted());
                        // COB_CODE: MOVE WK-PGM
                        //             TO IEAI9901-COD-SERVIZIO-BE
                        ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                        // COB_CODE: MOVE 'LOAP1-S320-CALL-LOAS0280'
                        //             TO IEAI9901-LABEL-ERR
                        ws.getIeai9901Area().setLabelErr("LOAP1-S320-CALL-LOAS0280");
                        // COB_CODE: MOVE '005210'
                        //             TO IEAI9901-COD-ERRORE
                        ws.getIeai9901Area().setCodErroreFormatted("005210");
                        // COB_CODE: STRING WK-LIV-DEROGA ';'
                        //                  WK-OGGETTO-X ';'
                        //                 'BLOCCANTE'
                        //           DELIMITED BY SIZE
                        //           INTO IEAI9901-PARAMETRI-ERR
                        //           END-STRING
                        concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkLivDerogaFormatted(), ";", ws.getWkOggettoXFormatted(), ";", "BLOCCANTE");
                        ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                        // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        //              THRU EX-S0300
                        s0300RicercaGravitaErrore();
                    }
                }
            }
        }
    }

    /**Original name: LOAP2-LEGGI-ULT-IMMAG-DER<br>
	 * <pre>----------------------------------------------------------------*
	 *  LETTURA DELL'ULTIMA IMMAGINE DELLO STATO DELLA DEROGA          *
	 * ----------------------------------------------------------------*</pre>*/
    private void loap2LeggiUltImmagDer() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'LOAP2-LEGGI-ULT-IMMAG-DER' TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("LOAP2-LEGGI-ULT-IMMAG-DER");
        // COB_CODE: INITIALIZE IDSI0011-AREA.
        initIdsi0011Area();
        // COB_CODE: IF INDIVIDUALE
        //                TO LDBV3361-ID-OGG
        //           ELSE
        //               TO LDBV3361-ID-OGG
        //           END-IF.
        if (ws.getWsTpFrmAssva().isIndividuale()) {
            // COB_CODE: SET POLIZZA
            //             TO TRUE
            ws.getWsTpOgg().setPolizza();
            // COB_CODE: MOVE WS-TP-OGG
            //             TO LDBV3361-TP-OGG
            ws.getLdbv3361().setLdbv3361TpOgg(ws.getWsTpOgg().getWsTpOgg());
            // COB_CODE: MOVE WPOL-ID-POLI
            //             TO LDBV3361-ID-OGG
            ws.getLdbv3361().setLdbv3361IdOgg(areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolIdPoli());
        }
        else {
            // COB_CODE: SET ADESIONE
            //             TO TRUE
            ws.getWsTpOgg().setAdesione();
            // COB_CODE: MOVE WS-TP-OGG
            //             TO LDBV3361-TP-OGG
            ws.getLdbv3361().setLdbv3361TpOgg(ws.getWsTpOgg().getWsTpOgg());
            // COB_CODE: MOVE WADE-ID-ADES(1)
            //             TO LDBV3361-ID-OGG
            ws.getLdbv3361().setLdbv3361IdOgg(areaMain.getWadeAreaAdesione().getTabAde(1).getLccvade1().getDati().getWadeIdAdes());
        }
        // COB_CODE: MOVE WS-DT-INFINITO-1-N TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                      IDSI0011-DATA-FINE-EFFETTO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(ws.getIdsv0015().getDtInfinito1N());
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(ws.getIdsv0015().getDtInfinito1N());
        // COB_CODE: MOVE WS-TS-INFINITO-1-N TO IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(ws.getIdsv0015().getTsInfinito1N());
        // COB_CODE: SET  IDSI0011-TRATT-DEFAULT
        //             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setIdsi0011TrattDefault();
        // COB_CODE: MOVE LDBS3360
        //             TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato(ws.getLdbs3360());
        // COB_CODE: MOVE LDBV3361
        //             TO IDSI0011-BUFFER-WHERE-COND
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferWhereCond(ws.getLdbv3361().getLdbv3361Formatted());
        // COB_CODE: SET IDSI0011-WHERE-CONDITION
        //             TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setWhereCondition();
        // COB_CODE: SET IDSI0011-SELECT
        //             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC
        //             TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL
        //             TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX.
        callDispatcher();
        //
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //                   END-EVALUATE
        //                ELSE
        //           *--> ERRORE DISPATCHER
        //                      THRU EX-S0300
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            // COB_CODE:         EVALUATE TRUE
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //           *--> OPERAZIONE ESEGUITA CORRETTAMENTE
            //                            MOVE IDSO0011-BUFFER-DATI TO OGG-DEROGA
            //                       WHEN OTHER
            //           *--> ERRORE DI ACCESSO AL DB
            //                               THRU EX-S0300
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://--> OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: MOVE IDSO0011-BUFFER-DATI TO OGG-DEROGA
                    ws.getOggDeroga().setOggDerogaFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    break;

                default://--> ERRORE DI ACCESSO AL DB
                    // COB_CODE: MOVE WK-PGM
                    //             TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE WK-LABEL-ERR
                    //             TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
                    // COB_CODE: MOVE '005016'
                    //             TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005016");
                    // COB_CODE: STRING LDBS3360  ';'
                    //                  IDSO0011-RETURN-CODE ';'
                    //                  IDSO0011-SQLCODE
                    //             DELIMITED BY SIZE
                    //             INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getLdbs3360Formatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;
            }
        }
        else {
            //--> ERRORE DISPATCHER
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL-ERR
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
            // COB_CODE: MOVE '005056'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005056");
            // COB_CODE: STRING LDBS3360  ';'
            //                  IDSO0011-RETURN-CODE ';'
            //                  IDSO0011-SQLCODE
            //             DELIMITED BY SIZE
            //             INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getLdbs3360Formatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: LOAP2-LEGGI-MOVI-DEROGA<br>
	 * <pre>----------------------------------------------------------------*
	 *  LETTURA DEL MOVIMENTO CHE HA CREATO LA DEROGA                  *
	 * ----------------------------------------------------------------*</pre>*/
    private void loap2LeggiMoviDeroga() {
        // COB_CODE: MOVE 'LOAP2-LEGGI-MOVI-DEROGA' TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("LOAP2-LEGGI-MOVI-DEROGA");
        // COB_CODE: PERFORM DISPLAY-LABEL THRU DISPLAY-LABEL-EX.
        displayLabel();
        // COB_CODE: MOVE 'MOVI'          TO WK-TABELLA.
        ws.setWkTabella("MOVI");
        // COB_CODE: INITIALIZE                       MOVI.
        initMovi();
        // COB_CODE: SET IDSI0011-SELECT           TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC    TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
        // COB_CODE: SET IDSI0011-PRIMARY-KEY      TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setPrimaryKey();
        // COB_CODE: SET IDSI0011-TRATT-SENZA-STOR TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattSenzaStor();
        // COB_CODE: MOVE ODE-ID-MOVI-CRZ          TO MOV-ID-MOVI.
        ws.getMovi().setMovIdMovi(ws.getOggDeroga().getOdeIdMoviCrz());
        // COB_CODE: MOVE 'MOVI'                   TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("MOVI");
        // COB_CODE: MOVE MOVI                     TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getMovi().getMoviFormatted());
        // COB_CODE: MOVE SPACES                   TO IDSI0011-BUFFER-WHERE-COND.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferWhereCond("");
        // COB_CODE: MOVE ZERO                     TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                            IDSI0011-DATA-FINE-EFFETTO
        //                                            IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX.
        callDispatcher();
        // COB_CODE:         IF IDSO0011-SUCCESSFUL-RC
        //                      END-EVALUATE
        //                   ELSE
        //           *-->       GESTIRE ERRORE DISPATCHER
        //                         THRU EX-S0300
        //                   END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            // COB_CODE:           EVALUATE TRUE
            //                          WHEN IDSO0011-SUCCESSFUL-SQL
            //           *-->       OPERAZIONE ESEGUITA CORRETTAMENTE
            //                               TO MOVI
            //                          WHEN IDSO0011-NOT-FOUND
            //           *--->          CAMPO $ NON TROVATO
            //                                   THRU EX-S0300
            //                           WHEN OTHER
            //           *--->          ERRORE DI ACCESSO AL DB
            //                                THRU EX-S0300
            //                      END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://-->       OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: MOVE IDSO0011-BUFFER-DATI
                    //             TO MOVI
                    ws.getMovi().setMoviFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    break;

                case Idso0011SqlcodeSigned.NOT_FOUND://--->          CAMPO $ NON TROVATO
                    // COB_CODE: MOVE WK-PGM
                    //            TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE WK-LABEL-ERR
                    //             TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
                    // COB_CODE: MOVE '005019'
                    //             TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005019");
                    // COB_CODE: MOVE 'ID-MOVI'
                    //             TO IEAI9901-PARAMETRI-ERR
                    ws.getIeai9901Area().setParametriErr("ID-MOVI");
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;

                default://--->          ERRORE DI ACCESSO AL DB
                    // COB_CODE: MOVE WK-PGM
                    //                           TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE WK-LABEL-ERR
                    //                           TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
                    // COB_CODE: MOVE '005015'   TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005015");
                    // COB_CODE: MOVE WK-TABELLA TO IEAI9901-PARAMETRI-ERR
                    ws.getIeai9901Area().setParametriErr(ws.getWkTabella());
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;
            }
        }
        else {
            //-->       GESTIRE ERRORE DISPATCHER
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL-ERR
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
            // COB_CODE: MOVE '005016'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: MOVE SPACES
            //             TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: ESEGUI-DISPLAY<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI GESTIONE DISPLAY
	 * ----------------------------------------------------------------*</pre>*/
    private void eseguiDisplay() {
        Idss8880 idss8880 = null;
        GenericParam idsv8888DisplayAddress = null;
        // COB_CODE: IF IDSV0001-ANY-TUNING-DBG AND
        //              IDSV8888-ANY-TUNING-DBG
        //              END-IF
        //           ELSE
        //              END-IF
        //           END-IF.
        if (areaIdsv0001.getAreaComune().getLivelloDebug().isAnyTuningDbg() && ws.getIdsv8888().getLivelloDebug().isAnyTuningDbg()) {
            // COB_CODE: IF IDSV0001-TOT-TUNING-DBG
            //              INITIALIZE IDSV8888-STR-PERFORMANCE-DBG
            //           ELSE
            //              END-IF
            //           END-IF
            if (areaIdsv0001.getAreaComune().getLivelloDebug().isTotTuningDbg()) {
                // COB_CODE: IF IDSV8888-STRESS-TEST-DBG
                //              CONTINUE
                //           ELSE
                //              END-IF
                //           END-IF
                if (ws.getIdsv8888().getLivelloDebug().isStressTestDbg()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else if (ws.getIdsv8888().getLivelloDebug().isComCobJavDbg()) {
                    // COB_CODE: IF IDSV8888-COM-COB-JAV-DBG
                    //              SET IDSV8888-COM-COB-JAV   TO TRUE
                    //           ELSE
                    //              END-IF
                    //           END-IF
                    // COB_CODE: SET IDSV8888-COM-COB-JAV   TO TRUE
                    ws.getIdsv8888().getStrPerformanceDbg().setComCobJav();
                }
                else if (ws.getIdsv8888().getLivelloDebug().isBusinessDbg()) {
                    // COB_CODE: IF IDSV8888-BUSINESS-DBG
                    //              SET IDSV8888-BUSINESS   TO TRUE
                    //           ELSE
                    //              END-IF
                    //           END-IF
                    // COB_CODE: SET IDSV8888-BUSINESS   TO TRUE
                    ws.getIdsv8888().getStrPerformanceDbg().setBusiness();
                }
                else if (ws.getIdsv8888().getLivelloDebug().isArchBatchDbg()) {
                    // COB_CODE: IF IDSV8888-ARCH-BATCH-DBG
                    //              SET IDSV8888-ARCH-BATCH TO TRUE
                    //           ELSE
                    //              MOVE 'ERRO'   TO IDSV8888-FASE
                    //           END-IF
                    // COB_CODE: SET IDSV8888-ARCH-BATCH TO TRUE
                    ws.getIdsv8888().getStrPerformanceDbg().setArchBatch();
                }
                else {
                    // COB_CODE: MOVE 'ERRO'   TO IDSV8888-FASE
                    ws.getIdsv8888().getStrPerformanceDbg().setFase("ERRO");
                }
                // COB_CODE: CALL IDSV8888-CALL-TIMESTAMP
                //                USING IDSV8888-TIMESTAMP
                DynamicCall.invoke(ws.getIdsv8888().getCallTimestamp(), new BasicBytesClass(ws.getIdsv8888().getStrPerformanceDbg().getArray(), Idsv8888StrPerformanceDbg.Pos.TIMESTAMP_FLD - 1));
                // COB_CODE: SET IDSV8888-DISPLAY-ADDRESS
                //                TO ADDRESS OF IDSV8888-STR-PERFORMANCE-DBG
                ws.getIdsv8888().setDisplayAddress(pointerManager.addressOf(ws.getIdsv8888().getStrPerformanceDbg()));
                // COB_CODE: CALL IDSV8888-CALL-DISPLAY
                //                USING IDSV8888-DISPLAY-ADDRESS
                idss8880 = Idss8880.getInstance();
                idsv8888DisplayAddress = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getIdsv8888().getDisplayAddress()));
                idss8880.run(idsv8888DisplayAddress);
                ws.getIdsv8888().setDisplayAddressFromBuffer(idsv8888DisplayAddress.getByteData());
                // COB_CODE: INITIALIZE IDSV8888-STR-PERFORMANCE-DBG
                initStrPerformanceDbg();
            }
            else if (ws.getIdsv8888().getLivelloDebug().getLivelloDebug() == areaIdsv0001.getAreaComune().getLivelloDebug().getIdsi0011LivelloDebug()) {
                // COB_CODE: IF IDSV8888-LIVELLO-DEBUG =
                //              IDSV0001-LIVELLO-DEBUG
                //              INITIALIZE IDSV8888-STR-PERFORMANCE-DBG
                //           END-IF
                // COB_CODE: IF IDSV8888-STRESS-TEST-DBG
                //              CONTINUE
                //           ELSE
                //              END-IF
                //           END-IF
                if (ws.getIdsv8888().getLivelloDebug().isStressTestDbg()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else if (ws.getIdsv8888().getLivelloDebug().isComCobJavDbg()) {
                    // COB_CODE: IF IDSV8888-COM-COB-JAV-DBG
                    //              SET IDSV8888-COM-COB-JAV   TO TRUE
                    //           ELSE
                    //              END-IF
                    //           END-IF
                    // COB_CODE: SET IDSV8888-COM-COB-JAV   TO TRUE
                    ws.getIdsv8888().getStrPerformanceDbg().setComCobJav();
                }
                else if (ws.getIdsv8888().getLivelloDebug().isBusinessDbg()) {
                    // COB_CODE: IF IDSV8888-BUSINESS-DBG
                    //              SET IDSV8888-BUSINESS   TO TRUE
                    //           ELSE
                    //              SET IDSV8888-ARCH-BATCH TO TRUE
                    //           END-IF
                    // COB_CODE: SET IDSV8888-BUSINESS   TO TRUE
                    ws.getIdsv8888().getStrPerformanceDbg().setBusiness();
                }
                else {
                    // COB_CODE: SET IDSV8888-ARCH-BATCH TO TRUE
                    ws.getIdsv8888().getStrPerformanceDbg().setArchBatch();
                }
                // COB_CODE: CALL IDSV8888-CALL-TIMESTAMP
                //                USING IDSV8888-TIMESTAMP
                DynamicCall.invoke(ws.getIdsv8888().getCallTimestamp(), new BasicBytesClass(ws.getIdsv8888().getStrPerformanceDbg().getArray(), Idsv8888StrPerformanceDbg.Pos.TIMESTAMP_FLD - 1));
                // COB_CODE: SET IDSV8888-DISPLAY-ADDRESS
                //               TO ADDRESS OF IDSV8888-STR-PERFORMANCE-DBG
                ws.getIdsv8888().setDisplayAddress(pointerManager.addressOf(ws.getIdsv8888().getStrPerformanceDbg()));
                // COB_CODE: CALL IDSV8888-CALL-DISPLAY
                //                USING IDSV8888-DISPLAY-ADDRESS
                idss8880 = Idss8880.getInstance();
                idsv8888DisplayAddress = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getIdsv8888().getDisplayAddress()));
                idss8880.run(idsv8888DisplayAddress);
                ws.getIdsv8888().setDisplayAddressFromBuffer(idsv8888DisplayAddress.getByteData());
                // COB_CODE: INITIALIZE IDSV8888-STR-PERFORMANCE-DBG
                initStrPerformanceDbg();
            }
        }
        else if (areaIdsv0001.getAreaComune().getLivelloDebug().isAnyApplDbg() && ws.getIdsv8888().getLivelloDebug().isAnyApplDbg()) {
            // COB_CODE: IF IDSV0001-ANY-APPL-DBG AND
            //              IDSV8888-ANY-APPL-DBG
            //               END-IF
            //           END-IF
            // COB_CODE: IF IDSV8888-LIVELLO-DEBUG <=
            //              IDSV0001-LIVELLO-DEBUG
            //              MOVE SPACES TO IDSV8888-AREA-DISPLAY
            //           END-IF
            if (ws.getIdsv8888().getLivelloDebug().getLivelloDebug() <= areaIdsv0001.getAreaComune().getLivelloDebug().getIdsi0011LivelloDebug()) {
                // COB_CODE: SET IDSV8888-DISPLAY-ADDRESS
                //               TO ADDRESS OF IDSV8888-AREA-DISPLAY
                ws.getIdsv8888().setDisplayAddress(pointerManager.addressOf(ws.getIdsv8888().getAreaDisplay()));
                // COB_CODE: CALL IDSV8888-CALL-DISPLAY
                //                USING IDSV8888-DISPLAY-ADDRESS
                idss8880 = Idss8880.getInstance();
                idsv8888DisplayAddress = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getIdsv8888().getDisplayAddress()));
                idss8880.run(idsv8888DisplayAddress);
                ws.getIdsv8888().setDisplayAddressFromBuffer(idsv8888DisplayAddress.getByteData());
                // COB_CODE: MOVE SPACES TO IDSV8888-AREA-DISPLAY
                ws.getIdsv8888().getAreaDisplay().setAreaDisplay("");
            }
        }
        // COB_CODE: SET IDSV8888-NO-DEBUG              TO TRUE.
        ws.getIdsv8888().getLivelloDebug().setNoDebug();
    }

    @Override
    public DdCard[] getDefaultDdCards() {
        return new DdCard[] {new DdCard("OUTRIVA", 500)};
    }

    public void initWcntTabVarCont() {
        ws.getIvvc0212().getWcntTabVar().setWcntCodVarCont(1, "");
        ws.getIvvc0212().getWcntTabVar().setWcntTpDatoCont(1, Types.SPACE_CHAR);
        ws.getIvvc0212().getWcntTabVar().setWcntValImpCont(1, new AfDecimal(0, 18, 7));
        ws.getIvvc0212().getWcntTabVar().setWcntValPercCont(1, new AfDecimal(0, 14, 9));
        ws.getIvvc0212().getWcntTabVar().setWcntValStrCont(1, "");
        ws.getIvvc0212().getWcntTabVar().setWcntTpLivello(1, Types.SPACE_CHAR);
        ws.getIvvc0212().getWcntTabVar().setWcntCodLivello(1, "");
        ws.getIvvc0212().getWcntTabVar().setWcntIdLivelloFormatted(1, "000000000");
    }

    public void initWkAreaDatiProd() {
        ws.setWk0040EleMax(((short)0));
        ws.getWk0040TabellaObj().fill(new Wk0040Tabella().initWk0040Tabella());
    }

    public void initMovi() {
        ws.getMovi().setMovIdMovi(0);
        ws.getMovi().setMovCodCompAnia(0);
        ws.getMovi().getMovIdOgg().setMovIdOgg(0);
        ws.getMovi().setMovIbOgg("");
        ws.getMovi().setMovIbMovi("");
        ws.getMovi().setMovTpOgg("");
        ws.getMovi().getMovIdRich().setMovIdRich(0);
        ws.getMovi().getMovTpMovi().setMovTpMovi(0);
        ws.getMovi().setMovDtEff(0);
        ws.getMovi().getMovIdMoviAnn().setMovIdMoviAnn(0);
        ws.getMovi().getMovIdMoviCollg().setMovIdMoviCollg(0);
        ws.getMovi().setMovDsOperSql(Types.SPACE_CHAR);
        ws.getMovi().setMovDsVer(0);
        ws.getMovi().setMovDsTsCptz(0);
        ws.getMovi().setMovDsUtente("");
        ws.getMovi().setMovDsStatoElab(Types.SPACE_CHAR);
    }

    public void initWorkCommarea() {
        ws.getWorkCommarea().setEleParamMovMax(((short)0));
        for (int idx0 = 1; idx0 <= WorkCommarea.TAB_PARAM_MOV_MAXOCCURS; idx0++) {
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().setIdPtf(0);
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().setWpmoIdParamMovi(0);
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().setWpmoIdOgg(0);
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().setWpmoTpOgg("");
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().setWpmoIdMoviCrz(0);
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().getWpmoIdMoviChiu().setWpmoIdMoviChiu(0);
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().setWpmoDtIniEff(0);
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().setWpmoDtEndEff(0);
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().setWpmoCodCompAnia(0);
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().getWpmoTpMovi().setWpmoTpMovi(0);
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().getWpmoFrqMovi().setWpmoFrqMovi(0);
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().getWpmoDurAa().setWpmoDurAa(0);
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().getWpmoDurMm().setWpmoDurMm(0);
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().getWpmoDurGg().setWpmoDurGg(0);
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().getWpmoDtRicorPrec().setWpmoDtRicorPrec(0);
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().getWpmoDtRicorSucc().setWpmoDtRicorSucc(0);
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().getWpmoPcIntrFraz().setWpmoPcIntrFraz(new AfDecimal(0, 6, 3));
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().getWpmoImpBnsDaScoTot().setWpmoImpBnsDaScoTot(new AfDecimal(0, 15, 3));
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().getWpmoImpBnsDaSco().setWpmoImpBnsDaSco(new AfDecimal(0, 15, 3));
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().getWpmoPcAnticBns().setWpmoPcAnticBns(new AfDecimal(0, 6, 3));
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().setWpmoTpRinnColl("");
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().setWpmoTpRivalPre("");
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().setWpmoTpRivalPrstz("");
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().setWpmoFlEvidRival(Types.SPACE_CHAR);
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().getWpmoUltPcPerd().setWpmoUltPcPerd(new AfDecimal(0, 6, 3));
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().getWpmoTotAaGiaPror().setWpmoTotAaGiaPror(0);
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().setWpmoTpOpz("");
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().getWpmoAaRenCer().setWpmoAaRenCer(0);
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().getWpmoPcRevrsb().setWpmoPcRevrsb(new AfDecimal(0, 6, 3));
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().getWpmoImpRiscParzPrgt().setWpmoImpRiscParzPrgt(new AfDecimal(0, 15, 3));
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().getWpmoImpLrdDiRat().setWpmoImpLrdDiRat(new AfDecimal(0, 15, 3));
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().setWpmoIbOgg("");
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().getWpmoCosOner().setWpmoCosOner(new AfDecimal(0, 15, 3));
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().getWpmoSpePc().setWpmoSpePc(new AfDecimal(0, 6, 3));
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().setWpmoFlAttivGar(Types.SPACE_CHAR);
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().setWpmoCambioVerProd(Types.SPACE_CHAR);
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().getWpmoMmDiff().setWpmoMmDiff(((short)0));
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().getWpmoImpRatManfee().setWpmoImpRatManfee(new AfDecimal(0, 15, 3));
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().getWpmoDtUltErogManfee().setWpmoDtUltErogManfee(0);
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().setWpmoTpOggRival("");
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().getWpmoSomAsstaGarac().setWpmoSomAsstaGarac(new AfDecimal(0, 15, 3));
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().getWpmoPcApplzOpz().setWpmoPcApplzOpz(new AfDecimal(0, 6, 3));
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().getWpmoIdAdes().setWpmoIdAdes(0);
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().setWpmoIdPoli(0);
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().setWpmoTpFrmAssva("");
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().setWpmoDsRiga(0);
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().setWpmoDsOperSql(Types.SPACE_CHAR);
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().setWpmoDsVer(0);
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().setWpmoDsTsIniCptz(0);
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().setWpmoDsTsEndCptz(0);
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().setWpmoDsUtente("");
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().setWpmoDsStatoElab(Types.SPACE_CHAR);
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().setWpmoTpEstrCnt("");
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().setWpmoCodRamo("");
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().setWpmoGenDaSin(Types.SPACE_CHAR);
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().setWpmoCodTari("");
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().getWpmoNumRatPagPre().setWpmoNumRatPagPre(0);
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().getWpmoPcServVal().setWpmoPcServVal(new AfDecimal(0, 6, 3));
            ws.getWorkCommarea().getTabParamMov(idx0).getLccvpmo1().getDati().getWpmoEtaAaSoglBnficr().setWpmoEtaAaSoglBnficr(((short)0));
        }
    }

    public void initIxIndici() {
        ws.getIxIndici().setTabPmo(((short)0));
        ws.getIxIndici().setTabPmo2(((short)0));
        ws.getIxIndici().setTabBlc(((short)0));
        ws.getIxIndici().setTabMov(((short)0));
        ws.getIxIndici().setTabAde(((short)0));
        ws.getIxIndici().setTabGrz(((short)0));
        ws.getIxIndici().setTabTga(((short)0));
        ws.getIxIndici().setTabPog(((short)0));
        ws.getIxIndici().setTabRre(((short)0));
        ws.getIxIndici().setTabRan(((short)0));
        ws.getIxIndici().setGrz(((short)0));
        ws.getIxIndici().setTabErr(((short)0));
        ws.getIxIndici().setAreaScheda(((short)0));
        ws.getIxIndici().setTabVar(((short)0));
        ws.getIxIndici().setxRivInc(((short)0));
        ws.getIxIndici().setVpmo(((short)0));
        ws.getIxIndici().setW670(((short)0));
        ws.getIxIndici().setTgaW870(((short)0));
        ws.getIxIndici().setTitW870(((short)0));
        ws.getIxIndici().setWkVar(((short)0));
        ws.getIxIndici().setTabPrec(((short)0));
        ws.getIxIndici().setWk0040(((short)0));
    }

    public void initTabVariabili() {
        ws.getLccc0211().getWkTabVar().setCodVariabile(1, ws.getIxIndici().getWkVar(), "");
        ws.getLccc0211().getWkTabVar().setTpDato(1, ws.getIxIndici().getWkVar(), Types.SPACE_CHAR);
        ws.getLccc0211().getWkTabVar().setValImp(1, ws.getIxIndici().getWkVar(), new AfDecimal(0, 18, 7));
        ws.getLccc0211().getWkTabVar().setValPerc(1, ws.getIxIndici().getWkVar(), new AfDecimal(0, 14, 9));
        ws.getLccc0211().getWkTabVar().setValStr(1, ws.getIxIndici().getWkVar(), "");
    }

    public void initIspc0040DatiInput() {
        ws.getAreaIoIsps0040().getIspc0040DatiInput().setCodCompagniaFormatted("00000");
        ws.getAreaIoIsps0040().getIspc0040DatiInput().setCodProdotto("");
        ws.getAreaIoIsps0040().getIspc0040DatiInput().setCodConvenzione("");
        ws.getAreaIoIsps0040().getIspc0040DatiInput().setDataInizValidConv("");
        ws.getAreaIoIsps0040().getIspc0040DatiInput().setDataRiferimento("");
        ws.getAreaIoIsps0040().getIspc0040DatiInput().setDataEmissione("");
        ws.getAreaIoIsps0040().getIspc0040DatiInput().setDataProposta("");
        ws.getAreaIoIsps0040().getIspc0040DatiInput().setLivelloUtenteFormatted("00");
        ws.getAreaIoIsps0040().getIspc0040DatiInput().setFunzionalitaFormatted("00000");
        ws.getAreaIoIsps0040().getIspc0040DatiInput().setSessionId("");
    }

    public void initIspc0040DatiOutput1() {
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setProdotto("");
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setDataVersioneProd("");
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setTipoVersioneProd("");
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setCodiceDivisa("");
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setDescrDivisa("");
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().getTipoPolizza().setTipoPolizza("");
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setDescPolizza("");
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setIspc0040TpTrasfNumEleFormatted("000");
        for (int idx0 = 1; idx0 <= Ispc0040DatiOutput1.TIPO_TRASFORMAZIONE_MAXOCCURS; idx0++) {
            ws.getAreaIoIsps0040().getIspc0040DatiOutput1().getTipoTrasformazione(idx0).getCodTpTrasf().setCodTpTrasf("");
            ws.getAreaIoIsps0040().getIspc0040DatiOutput1().getTipoTrasformazione(idx0).setDescrTpTrasf("");
        }
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setIspc0040TpAdeNumEleFormatted("000");
        for (int idx0 = 1; idx0 <= Ispc0040DatiOutput1.TIPO_ADESIONE_MAXOCCURS; idx0++) {
            ws.getAreaIoIsps0040().getIspc0040DatiOutput1().getTipoAdesione(idx0).getCodiceAdesione().setCodiceAdesione("");
            ws.getAreaIoIsps0040().getIspc0040DatiOutput1().getTipoAdesione(idx0).setDescrAdesione("");
        }
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setIspc0040NumEleModCalDurFormatted("000");
        for (int idx0 = 1; idx0 <= Ispc0040DatiOutput1.MOD_CALC_DUR_MAXOCCURS; idx0++) {
            ws.getAreaIoIsps0040().getIspc0040DatiOutput1().getModCalcDur(idx0).getCodiceMod().setCodiceMod(Types.SPACE_CHAR);
            ws.getAreaIoIsps0040().getIspc0040DatiOutput1().getModCalcDur(idx0).setDescrMod("");
        }
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().getFlagRischioComune().setFlagRischioComune(Types.SPACE_CHAR);
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setIspc0040NumEleDiffProFormatted("000");
        for (int idx0 = 1; idx0 <= Ispc0040DatiOutput1.DIFFERIMENTO_PROROGA_MAXOCCURS; idx0++) {
            ws.getAreaIoIsps0040().getIspc0040DatiOutput1().getDifferimentoProroga(idx0).getCodDiffProg().setCodDiffProg("");
            ws.getAreaIoIsps0040().getIspc0040DatiOutput1().getDifferimentoProroga(idx0).setDescDiffProg("");
        }
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setIspc0040AnniDifferProrFormatted("00");
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setTrattAaDiffPror(Types.SPACE_CHAR);
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setIspc0040NumEleModDurFormatted("000");
        for (int idx0 = 1; idx0 <= Ispc0040DatiOutput1.MODALITA_DURATA_MAXOCCURS; idx0++) {
            ws.getAreaIoIsps0040().getIspc0040DatiOutput1().getModalitaDurata(idx0).getCodDurata().setCodDurata("");
            ws.getAreaIoIsps0040().getIspc0040DatiOutput1().getModalitaDurata(idx0).setDescrDurata("");
        }
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setIspc0040EtaScadMaschiDfltFormatted("00000");
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setTrattEtaScadMaschi(Types.SPACE_CHAR);
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setIspc0040EtaScadFemmineDfltFormatted("00000");
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setTrattEtaScadFemm(Types.SPACE_CHAR);
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setDataScadenzaDflt("");
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setTrattDataScadFemm(Types.SPACE_CHAR);
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setIspc0040DurataAnniDefaultFormatted("00000");
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setTrattDurataAnni(Types.SPACE_CHAR);
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setIspc0040DurataMesiDefaultFormatted("00000");
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setTrattDurataMesi(Types.SPACE_CHAR);
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setIspc0040DurataGiorniDefaultFormatted("00000");
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setTrattDurataGiorni(Types.SPACE_CHAR);
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setIspc0040IndMaxFrazFormatted("000");
        for (int idx0 = 1; idx0 <= Ispc0040DatiOutput1.FRAZIONAMENTO_MAXOCCURS; idx0++) {
            ws.getAreaIoIsps0040().getIspc0040DatiOutput1().getFrazionamento(idx0).setCodFraz("");
            ws.getAreaIoIsps0040().getIspc0040DatiOutput1().getFrazionamento(idx0).setDescFraz("");
        }
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setTrattamentoFraz(Types.SPACE_CHAR);
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setIspc0040IndMaxImpScoFormatted("000");
        for (int idx0 = 1; idx0 <= Ispc0040DatiOutput1.IMP_SCONTO_MAXOCCURS; idx0++) {
            ws.getAreaIoIsps0040().getIspc0040DatiOutput1().getImpSconto(idx0).setCodImpSco("");
            ws.getAreaIoIsps0040().getIspc0040DatiOutput1().getImpSconto(idx0).setDescImpSco("");
        }
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setTrattamentoImpSco(Types.SPACE_CHAR);
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setIspc0040IndMaxFondiFormatted("000");
    }

    public void initIspc0040DatiOutput2() {
        ws.getAreaIoIsps0040().getIspc0040DatiOutput2().setTrattamentoFondo(Types.SPACE_CHAR);
        ws.getAreaIoIsps0040().getIspc0040DatiOutput2().getFlagCoperturaFin().setFlagCoperturaFin(Types.SPACE_CHAR);
        ws.getAreaIoIsps0040().getIspc0040DatiOutput2().getAdesSenzaAssic().setAdesSenzaAssic(Types.SPACE_CHAR);
        ws.getAreaIoIsps0040().getIspc0040DatiOutput2().setIspc0040FlagCumuloContrFormatted("0");
        ws.getAreaIoIsps0040().getIspc0040DatiOutput2().setFlagTrattDirEmis(Types.SPACE_CHAR);
        ws.getAreaIoIsps0040().getIspc0040DatiOutput2().setImpDirEmis(new AfDecimal(0, 18, 3));
        ws.getAreaIoIsps0040().getIspc0040DatiOutput2().setFlagTrattDirQuie(Types.SPACE_CHAR);
        ws.getAreaIoIsps0040().getIspc0040DatiOutput2().setImpDirQuie(new AfDecimal(0, 18, 3));
        ws.getAreaIoIsps0040().getIspc0040DatiOutput2().setFlagTrattDir1Ver(Types.SPACE_CHAR);
        ws.getAreaIoIsps0040().getIspc0040DatiOutput2().setImpDir1Ver(new AfDecimal(0, 18, 3));
        ws.getAreaIoIsps0040().getIspc0040DatiOutput2().setFlagTrattVersAgg(Types.SPACE_CHAR);
        ws.getAreaIoIsps0040().getIspc0040DatiOutput2().setImpDirVersAgg(new AfDecimal(0, 18, 3));
        ws.getAreaIoIsps0040().getIspc0040DatiOutput2().setFlagSpeseMediche(Types.SPACE_CHAR);
        ws.getAreaIoIsps0040().getIspc0040DatiOutput2().setImpSpeseMediche(new AfDecimal(0, 18, 3));
        ws.getAreaIoIsps0040().getIspc0040DatiOutput2().setIspc0040TpPremioNumEFormatted("000");
        for (int idx0 = 1; idx0 <= Ispc0040DatiOutput2.TIPO_PREMIO_MAXOCCURS; idx0++) {
            ws.getAreaIoIsps0040().getIspc0040DatiOutput2().getTipoPremio(idx0).getCodTpPremio().setCodTpPremio(Types.SPACE_CHAR);
            ws.getAreaIoIsps0040().getIspc0040DatiOutput2().getTipoPremio(idx0).setDescrTpPremio("");
        }
        ws.getAreaIoIsps0040().getIspc0040DatiOutput2().setTrattTipoPremio(Types.SPACE_CHAR);
        ws.getAreaIoIsps0040().getIspc0040DatiOutput2().getDistintaContr().setDistintaContr(Types.SPACE_CHAR);
        ws.getAreaIoIsps0040().getIspc0040DatiOutput2().setDataDecor("");
        ws.getAreaIoIsps0040().getIspc0040DatiOutput2().setDataDecorTratt(Types.SPACE_CHAR);
        ws.getAreaIoIsps0040().getIspc0040DatiOutput2().setIspc0040NumGgReinvestFormatted("00000");
        ws.getAreaIoIsps0040().getIspc0040DatiOutput2().setIspc0040MaxOperFormInvestFormatted("000");
        for (int idx0 = 1; idx0 <= Ispc0040DatiOutput2.OPER_FORMA_REINVEST_MAXOCCURS; idx0++) {
            ws.getAreaIoIsps0040().getIspc0040DatiOutput2().getOperFormaReinvest(idx0).setIspc0040CodOperReinvFormatted("0000");
            ws.getAreaIoIsps0040().getIspc0040DatiOutput2().getOperFormaReinvest(idx0).setIspc0040CodFormaReinvFormatted("0000");
            ws.getAreaIoIsps0040().getIspc0040DatiOutput2().getOperFormaReinvest(idx0).setPercReinv(new AfDecimal(0, 14, 9));
        }
        ws.getAreaIoIsps0040().getIspc0040DatiOutput2().setSottocategoria("");
    }

    public void initIspc0040AreaErrori() {
        ws.getAreaIoIsps0040().setIspc0040EsitoFormatted("00");
        ws.getAreaIoIsps0040().setIspc0040ErrNumEleFormatted("000");
        for (int idx0 = 1; idx0 <= AreaIoIsps0040.ISPC0040_TAB_ERRORI_MAXOCCURS; idx0++) {
            ws.getAreaIoIsps0040().getIspc0040TabErrori(idx0).setCodErr("");
            ws.getAreaIoIsps0040().getIspc0040TabErrori(idx0).setDescrizioneErr("");
            ws.getAreaIoIsps0040().getIspc0040TabErrori(idx0).setIspc0211GravitaErrFormatted("00");
            ws.getAreaIoIsps0040().getIspc0040TabErrori(idx0).setIspc0211LivelloDerogaFormatted("00");
        }
    }

    public void initTabTran() {
        ws.getWtgaAreaTranche().getWtgaTab().setStatus(1, Types.SPACE_CHAR);
        ws.getWtgaAreaTranche().getWtgaTab().setIdPtf(1, 0);
        ws.getWtgaAreaTranche().getWtgaTab().setIdTrchDiGar(1, 0);
        ws.getWtgaAreaTranche().getWtgaTab().setIdGar(1, 0);
        ws.getWtgaAreaTranche().getWtgaTab().setIdAdes(1, 0);
        ws.getWtgaAreaTranche().getWtgaTab().setIdPoli(1, 0);
        ws.getWtgaAreaTranche().getWtgaTab().setIdMoviCrz(1, 0);
        ws.getWtgaAreaTranche().getWtgaTab().setIdMoviChiu(1, 0);
        ws.getWtgaAreaTranche().getWtgaTab().setDtIniEff(1, 0);
        ws.getWtgaAreaTranche().getWtgaTab().setDtEndEff(1, 0);
        ws.getWtgaAreaTranche().getWtgaTab().setCodCompAnia(1, 0);
        ws.getWtgaAreaTranche().getWtgaTab().setDtDecor(1, 0);
        ws.getWtgaAreaTranche().getWtgaTab().setDtScad(1, 0);
        ws.getWtgaAreaTranche().getWtgaTab().setIbOgg(1, "");
        ws.getWtgaAreaTranche().getWtgaTab().setTpRgmFisc(1, "");
        ws.getWtgaAreaTranche().getWtgaTab().setDtEmis(1, 0);
        ws.getWtgaAreaTranche().getWtgaTab().setTpTrch(1, "");
        ws.getWtgaAreaTranche().getWtgaTab().setDurAa(1, 0);
        ws.getWtgaAreaTranche().getWtgaTab().setDurMm(1, 0);
        ws.getWtgaAreaTranche().getWtgaTab().setDurGg(1, 0);
        ws.getWtgaAreaTranche().getWtgaTab().setPreCasoMor(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setPcIntrRiat(1, new AfDecimal(0, 6, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setImpBnsAntic(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setPreIniNet(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setPrePpIni(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setPrePpUlt(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setPreTariIni(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setPreTariUlt(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setPreInvrioIni(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setPreInvrioUlt(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setPreRivto(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setImpSoprProf(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setImpSoprSan(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setImpSoprSpo(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setImpSoprTec(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setImpAltSopr(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setPreStab(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setDtEffStab(1, 0);
        ws.getWtgaAreaTranche().getWtgaTab().setTsRivalFis(1, new AfDecimal(0, 14, 9));
        ws.getWtgaAreaTranche().getWtgaTab().setTsRivalIndiciz(1, new AfDecimal(0, 14, 9));
        ws.getWtgaAreaTranche().getWtgaTab().setOldTsTec(1, new AfDecimal(0, 14, 9));
        ws.getWtgaAreaTranche().getWtgaTab().setRatLrd(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setPreLrd(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setPrstzIni(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setPrstzUlt(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setCptInOpzRivto(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setPrstzIniStab(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setCptRshMor(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setPrstzRidIni(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setFlCarCont(1, Types.SPACE_CHAR);
        ws.getWtgaAreaTranche().getWtgaTab().setBnsGiaLiqto(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setImpBns(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setCodDvs(1, "");
        ws.getWtgaAreaTranche().getWtgaTab().setPrstzIniNewfis(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setImpScon(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setAlqScon(1, new AfDecimal(0, 6, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setImpCarAcq(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setImpCarInc(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setImpCarGest(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setEtaAa1oAssto(1, ((short)0));
        ws.getWtgaAreaTranche().getWtgaTab().setEtaMm1oAssto(1, ((short)0));
        ws.getWtgaAreaTranche().getWtgaTab().setEtaAa2oAssto(1, ((short)0));
        ws.getWtgaAreaTranche().getWtgaTab().setEtaMm2oAssto(1, ((short)0));
        ws.getWtgaAreaTranche().getWtgaTab().setEtaAa3oAssto(1, ((short)0));
        ws.getWtgaAreaTranche().getWtgaTab().setEtaMm3oAssto(1, ((short)0));
        ws.getWtgaAreaTranche().getWtgaTab().setRendtoLrd(1, new AfDecimal(0, 14, 9));
        ws.getWtgaAreaTranche().getWtgaTab().setPcRetr(1, new AfDecimal(0, 6, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setRendtoRetr(1, new AfDecimal(0, 14, 9));
        ws.getWtgaAreaTranche().getWtgaTab().setMinGarto(1, new AfDecimal(0, 14, 9));
        ws.getWtgaAreaTranche().getWtgaTab().setMinTrnut(1, new AfDecimal(0, 14, 9));
        ws.getWtgaAreaTranche().getWtgaTab().setPreAttDiTrch(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setMatuEnd2000(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setAbbTotIni(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setAbbTotUlt(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setAbbAnnuUlt(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setDurAbb(1, 0);
        ws.getWtgaAreaTranche().getWtgaTab().setTpAdegAbb(1, Types.SPACE_CHAR);
        ws.getWtgaAreaTranche().getWtgaTab().setModCalc(1, "");
        ws.getWtgaAreaTranche().getWtgaTab().setImpAz(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setImpAder(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setImpTfr(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setImpVolo(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setVisEnd2000(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setDtVldtProd(1, 0);
        ws.getWtgaAreaTranche().getWtgaTab().setDtIniValTar(1, 0);
        ws.getWtgaAreaTranche().getWtgaTab().setImpbVisEnd2000(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setRenIniTsTec0(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setPcRipPre(1, new AfDecimal(0, 6, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setFlImportiForz(1, Types.SPACE_CHAR);
        ws.getWtgaAreaTranche().getWtgaTab().setPrstzIniNforz(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setVisEnd2000Nforz(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setIntrMora(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setManfeeAntic(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setManfeeRicor(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setPreUniRivto(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setProv1aaAcq(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setProv2aaAcq(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setProvRicor(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setProvInc(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setAlqProvAcq(1, new AfDecimal(0, 6, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setAlqProvInc(1, new AfDecimal(0, 6, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setAlqProvRicor(1, new AfDecimal(0, 6, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setImpbProvAcq(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setImpbProvInc(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setImpbProvRicor(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setFlProvForz(1, Types.SPACE_CHAR);
        ws.getWtgaAreaTranche().getWtgaTab().setPrstzAggIni(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setIncrPre(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setIncrPrstz(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setDtUltAdegPrePr(1, 0);
        ws.getWtgaAreaTranche().getWtgaTab().setPrstzAggUlt(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setTsRivalNet(1, new AfDecimal(0, 14, 9));
        ws.getWtgaAreaTranche().getWtgaTab().setPrePattuito(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setTpRival(1, "");
        ws.getWtgaAreaTranche().getWtgaTab().setRisMat(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setCptMinScad(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setCommisGest(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setTpManfeeAppl(1, "");
        ws.getWtgaAreaTranche().getWtgaTab().setDsRiga(1, 0);
        ws.getWtgaAreaTranche().getWtgaTab().setDsOperSql(1, Types.SPACE_CHAR);
        ws.getWtgaAreaTranche().getWtgaTab().setDsVer(1, 0);
        ws.getWtgaAreaTranche().getWtgaTab().setDsTsIniCptz(1, 0);
        ws.getWtgaAreaTranche().getWtgaTab().setDsTsEndCptz(1, 0);
        ws.getWtgaAreaTranche().getWtgaTab().setDsUtente(1, "");
        ws.getWtgaAreaTranche().getWtgaTab().setDsStatoElab(1, Types.SPACE_CHAR);
        ws.getWtgaAreaTranche().getWtgaTab().setPcCommisGest(1, new AfDecimal(0, 6, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setNumGgRival(1, 0);
        ws.getWtgaAreaTranche().getWtgaTab().setImpTrasfe(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setImpTfrStrc(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setAcqExp(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setRemunAss(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setCommisInter(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setAlqRemunAss(1, new AfDecimal(0, 6, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setAlqCommisInter(1, new AfDecimal(0, 6, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setImpbRemunAss(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setImpbCommisInter(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setCosRunAssva(1, new AfDecimal(0, 15, 3));
        ws.getWtgaAreaTranche().getWtgaTab().setCosRunAssvaIdc(1, new AfDecimal(0, 15, 3));
    }

    public void initWprecAreaTranche() {
        for (int idx0 = 1; idx0 <= Loas0310Data.WPREC_TAB_TRAN_MAXOCCURS; idx0++) {
            ws.getWprecTabTran(idx0).setIdTrchDiGar(0);
            ws.getWprecTabTran(idx0).setPrstzUlt(new AfDecimal(0, 15, 3));
        }
    }

    public void initTabTran1() {
        ws.getVtgaAreaTranche().getVtgaTab().setStatus(1, Types.SPACE_CHAR);
        ws.getVtgaAreaTranche().getVtgaTab().setIdPtf(1, 0);
        ws.getVtgaAreaTranche().getVtgaTab().setIdTrchDiGar(1, 0);
        ws.getVtgaAreaTranche().getVtgaTab().setIdGar(1, 0);
        ws.getVtgaAreaTranche().getVtgaTab().setIdAdes(1, 0);
        ws.getVtgaAreaTranche().getVtgaTab().setIdPoli(1, 0);
        ws.getVtgaAreaTranche().getVtgaTab().setIdMoviCrz(1, 0);
        ws.getVtgaAreaTranche().getVtgaTab().setIdMoviChiu(1, 0);
        ws.getVtgaAreaTranche().getVtgaTab().setDtIniEff(1, 0);
        ws.getVtgaAreaTranche().getVtgaTab().setDtEndEff(1, 0);
        ws.getVtgaAreaTranche().getVtgaTab().setCodCompAnia(1, 0);
        ws.getVtgaAreaTranche().getVtgaTab().setDtDecor(1, 0);
        ws.getVtgaAreaTranche().getVtgaTab().setDtScad(1, 0);
        ws.getVtgaAreaTranche().getVtgaTab().setIbOgg(1, "");
        ws.getVtgaAreaTranche().getVtgaTab().setTpRgmFisc(1, "");
        ws.getVtgaAreaTranche().getVtgaTab().setDtEmis(1, 0);
        ws.getVtgaAreaTranche().getVtgaTab().setTpTrch(1, "");
        ws.getVtgaAreaTranche().getVtgaTab().setDurAa(1, 0);
        ws.getVtgaAreaTranche().getVtgaTab().setDurMm(1, 0);
        ws.getVtgaAreaTranche().getVtgaTab().setDurGg(1, 0);
        ws.getVtgaAreaTranche().getVtgaTab().setPreCasoMor(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setPcIntrRiat(1, new AfDecimal(0, 6, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setImpBnsAntic(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setPreIniNet(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setPrePpIni(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setPrePpUlt(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setPreTariIni(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setPreTariUlt(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setPreInvrioIni(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setPreInvrioUlt(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setPreRivto(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setImpSoprProf(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setImpSoprSan(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setImpSoprSpo(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setImpSoprTec(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setImpAltSopr(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setPreStab(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setDtEffStab(1, 0);
        ws.getVtgaAreaTranche().getVtgaTab().setTsRivalFis(1, new AfDecimal(0, 14, 9));
        ws.getVtgaAreaTranche().getVtgaTab().setTsRivalIndiciz(1, new AfDecimal(0, 14, 9));
        ws.getVtgaAreaTranche().getVtgaTab().setOldTsTec(1, new AfDecimal(0, 14, 9));
        ws.getVtgaAreaTranche().getVtgaTab().setRatLrd(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setPreLrd(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setPrstzIni(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setPrstzUlt(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setCptInOpzRivto(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setPrstzIniStab(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setCptRshMor(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setPrstzRidIni(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setFlCarCont(1, Types.SPACE_CHAR);
        ws.getVtgaAreaTranche().getVtgaTab().setBnsGiaLiqto(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setImpBns(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setCodDvs(1, "");
        ws.getVtgaAreaTranche().getVtgaTab().setPrstzIniNewfis(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setImpScon(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setAlqScon(1, new AfDecimal(0, 6, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setImpCarAcq(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setImpCarInc(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setImpCarGest(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setEtaAa1oAssto(1, ((short)0));
        ws.getVtgaAreaTranche().getVtgaTab().setEtaMm1oAssto(1, ((short)0));
        ws.getVtgaAreaTranche().getVtgaTab().setEtaAa2oAssto(1, ((short)0));
        ws.getVtgaAreaTranche().getVtgaTab().setEtaMm2oAssto(1, ((short)0));
        ws.getVtgaAreaTranche().getVtgaTab().setEtaAa3oAssto(1, ((short)0));
        ws.getVtgaAreaTranche().getVtgaTab().setEtaMm3oAssto(1, ((short)0));
        ws.getVtgaAreaTranche().getVtgaTab().setRendtoLrd(1, new AfDecimal(0, 14, 9));
        ws.getVtgaAreaTranche().getVtgaTab().setPcRetr(1, new AfDecimal(0, 6, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setRendtoRetr(1, new AfDecimal(0, 14, 9));
        ws.getVtgaAreaTranche().getVtgaTab().setMinGarto(1, new AfDecimal(0, 14, 9));
        ws.getVtgaAreaTranche().getVtgaTab().setMinTrnut(1, new AfDecimal(0, 14, 9));
        ws.getVtgaAreaTranche().getVtgaTab().setPreAttDiTrch(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setMatuEnd2000(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setAbbTotIni(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setAbbTotUlt(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setAbbAnnuUlt(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setDurAbb(1, 0);
        ws.getVtgaAreaTranche().getVtgaTab().setTpAdegAbb(1, Types.SPACE_CHAR);
        ws.getVtgaAreaTranche().getVtgaTab().setModCalc(1, "");
        ws.getVtgaAreaTranche().getVtgaTab().setImpAz(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setImpAder(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setImpTfr(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setImpVolo(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setVisEnd2000(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setDtVldtProd(1, 0);
        ws.getVtgaAreaTranche().getVtgaTab().setDtIniValTar(1, 0);
        ws.getVtgaAreaTranche().getVtgaTab().setImpbVisEnd2000(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setRenIniTsTec0(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setPcRipPre(1, new AfDecimal(0, 6, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setFlImportiForz(1, Types.SPACE_CHAR);
        ws.getVtgaAreaTranche().getVtgaTab().setPrstzIniNforz(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setVisEnd2000Nforz(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setIntrMora(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setManfeeAntic(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setManfeeRicor(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setPreUniRivto(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setProv1aaAcq(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setProv2aaAcq(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setProvRicor(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setProvInc(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setAlqProvAcq(1, new AfDecimal(0, 6, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setAlqProvInc(1, new AfDecimal(0, 6, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setAlqProvRicor(1, new AfDecimal(0, 6, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setImpbProvAcq(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setImpbProvInc(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setImpbProvRicor(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setFlProvForz(1, Types.SPACE_CHAR);
        ws.getVtgaAreaTranche().getVtgaTab().setPrstzAggIni(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setIncrPre(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setIncrPrstz(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setDtUltAdegPrePr(1, 0);
        ws.getVtgaAreaTranche().getVtgaTab().setPrstzAggUlt(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setTsRivalNet(1, new AfDecimal(0, 14, 9));
        ws.getVtgaAreaTranche().getVtgaTab().setPrePattuito(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setTpRival(1, "");
        ws.getVtgaAreaTranche().getVtgaTab().setRisMat(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setCptMinScad(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setCommisGest(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setTpManfeeAppl(1, "");
        ws.getVtgaAreaTranche().getVtgaTab().setDsRiga(1, 0);
        ws.getVtgaAreaTranche().getVtgaTab().setDsOperSql(1, Types.SPACE_CHAR);
        ws.getVtgaAreaTranche().getVtgaTab().setDsVer(1, 0);
        ws.getVtgaAreaTranche().getVtgaTab().setDsTsIniCptz(1, 0);
        ws.getVtgaAreaTranche().getVtgaTab().setDsTsEndCptz(1, 0);
        ws.getVtgaAreaTranche().getVtgaTab().setDsUtente(1, "");
        ws.getVtgaAreaTranche().getVtgaTab().setDsStatoElab(1, Types.SPACE_CHAR);
        ws.getVtgaAreaTranche().getVtgaTab().setPcCommisGest(1, new AfDecimal(0, 6, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setNumGgRival(1, 0);
        ws.getVtgaAreaTranche().getVtgaTab().setImpTrasfe(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setImpTfrStrc(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setAcqExp(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setRemunAss(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setCommisInter(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setAlqRemunAss(1, new AfDecimal(0, 6, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setAlqCommisInter(1, new AfDecimal(0, 6, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setImpbRemunAss(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setImpbCommisInter(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setCosRunAssva(1, new AfDecimal(0, 15, 3));
        ws.getVtgaAreaTranche().getVtgaTab().setCosRunAssvaIdc(1, new AfDecimal(0, 15, 3));
    }

    public void initStatOggBus() {
        ws.getStatOggBus().setStbIdStatOggBus(0);
        ws.getStatOggBus().setStbIdOgg(0);
        ws.getStatOggBus().setStbTpOgg("");
        ws.getStatOggBus().setStbIdMoviCrz(0);
        ws.getStatOggBus().getStbIdMoviChiu().setStbIdMoviChiu(0);
        ws.getStatOggBus().setStbDtIniEff(0);
        ws.getStatOggBus().setStbDtEndEff(0);
        ws.getStatOggBus().setStbCodCompAnia(0);
        ws.getStatOggBus().setStbTpStatBus("");
        ws.getStatOggBus().setStbTpCaus("");
        ws.getStatOggBus().setStbDsRiga(0);
        ws.getStatOggBus().setStbDsOperSql(Types.SPACE_CHAR);
        ws.getStatOggBus().setStbDsVer(0);
        ws.getStatOggBus().setStbDsTsIniCptz(0);
        ws.getStatOggBus().setStbDsTsEndCptz(0);
        ws.getStatOggBus().setStbDsUtente("");
        ws.getStatOggBus().setStbDsStatoElab(Types.SPACE_CHAR);
    }

    public void initPoli() {
        ws.getPoli().setPolIdPoli(0);
        ws.getPoli().setPolIdMoviCrz(0);
        ws.getPoli().getPolIdMoviChiu().setPolIdMoviChiu(0);
        ws.getPoli().setPolIbOgg("");
        ws.getPoli().setPolIbProp("");
        ws.getPoli().getPolDtProp().setPolDtProp(0);
        ws.getPoli().setPolDtIniEff(0);
        ws.getPoli().setPolDtEndEff(0);
        ws.getPoli().setPolCodCompAnia(0);
        ws.getPoli().setPolDtDecor(0);
        ws.getPoli().setPolDtEmis(0);
        ws.getPoli().setPolTpPoli("");
        ws.getPoli().getPolDurAa().setPolDurAa(0);
        ws.getPoli().getPolDurMm().setPolDurMm(0);
        ws.getPoli().getPolDtScad().setPolDtScad(0);
        ws.getPoli().setPolCodProd("");
        ws.getPoli().setPolDtIniVldtProd(0);
        ws.getPoli().setPolCodConv("");
        ws.getPoli().setPolCodRamo("");
        ws.getPoli().getPolDtIniVldtConv().setPolDtIniVldtConv(0);
        ws.getPoli().getPolDtApplzConv().setPolDtApplzConv(0);
        ws.getPoli().setPolTpFrmAssva("");
        ws.getPoli().setPolTpRgmFisc("");
        ws.getPoli().setPolFlEstas(Types.SPACE_CHAR);
        ws.getPoli().setPolFlRshComun(Types.SPACE_CHAR);
        ws.getPoli().setPolFlRshComunCond(Types.SPACE_CHAR);
        ws.getPoli().setPolTpLivGenzTit("");
        ws.getPoli().setPolFlCopFinanz(Types.SPACE_CHAR);
        ws.getPoli().setPolTpApplzDir("");
        ws.getPoli().getPolSpeMed().setPolSpeMed(new AfDecimal(0, 15, 3));
        ws.getPoli().getPolDirEmis().setPolDirEmis(new AfDecimal(0, 15, 3));
        ws.getPoli().getPolDir1oVers().setPolDir1oVers(new AfDecimal(0, 15, 3));
        ws.getPoli().getPolDirVersAgg().setPolDirVersAgg(new AfDecimal(0, 15, 3));
        ws.getPoli().setPolCodDvs("");
        ws.getPoli().setPolFlFntAz(Types.SPACE_CHAR);
        ws.getPoli().setPolFlFntAder(Types.SPACE_CHAR);
        ws.getPoli().setPolFlFntTfr(Types.SPACE_CHAR);
        ws.getPoli().setPolFlFntVolo(Types.SPACE_CHAR);
        ws.getPoli().setPolTpOpzAScad("");
        ws.getPoli().getPolAaDiffProrDflt().setPolAaDiffProrDflt(0);
        ws.getPoli().setPolFlVerProd("");
        ws.getPoli().getPolDurGg().setPolDurGg(0);
        ws.getPoli().getPolDirQuiet().setPolDirQuiet(new AfDecimal(0, 15, 3));
        ws.getPoli().setPolTpPtfEstno("");
        ws.getPoli().setPolFlCumPreCntr(Types.SPACE_CHAR);
        ws.getPoli().setPolFlAmmbMovi(Types.SPACE_CHAR);
        ws.getPoli().setPolConvGeco("");
        ws.getPoli().setPolDsRiga(0);
        ws.getPoli().setPolDsOperSql(Types.SPACE_CHAR);
        ws.getPoli().setPolDsVer(0);
        ws.getPoli().setPolDsTsIniCptz(0);
        ws.getPoli().setPolDsTsEndCptz(0);
        ws.getPoli().setPolDsUtente("");
        ws.getPoli().setPolDsStatoElab(Types.SPACE_CHAR);
        ws.getPoli().setPolFlScudoFisc(Types.SPACE_CHAR);
        ws.getPoli().setPolFlTrasfe(Types.SPACE_CHAR);
        ws.getPoli().setPolFlTfrStrc(Types.SPACE_CHAR);
        ws.getPoli().getPolDtPresc().setPolDtPresc(0);
        ws.getPoli().setPolCodConvAgg("");
        ws.getPoli().setPolSubcatProd("");
        ws.getPoli().setPolFlQuestAdegzAss(Types.SPACE_CHAR);
        ws.getPoli().setPolCodTpa("");
        ws.getPoli().getPolIdAccComm().setPolIdAccComm(0);
        ws.getPoli().setPolFlPoliCpiPr(Types.SPACE_CHAR);
        ws.getPoli().setPolFlPoliBundling(Types.SPACE_CHAR);
        ws.getPoli().setPolIndPoliPrinColl(Types.SPACE_CHAR);
        ws.getPoli().setPolFlVndBundle(Types.SPACE_CHAR);
        ws.getPoli().setPolIbBs("");
        ws.getPoli().setPolFlPoliIfp(Types.SPACE_CHAR);
    }

    public void initAdes() {
        ws.getAdes().setAdeIdAdes(0);
        ws.getAdes().setAdeIdPoli(0);
        ws.getAdes().setAdeIdMoviCrz(0);
        ws.getAdes().getAdeIdMoviChiu().setAdeIdMoviChiu(0);
        ws.getAdes().setAdeDtIniEff(0);
        ws.getAdes().setAdeDtEndEff(0);
        ws.getAdes().setAdeIbPrev("");
        ws.getAdes().setAdeIbOgg("");
        ws.getAdes().setAdeCodCompAnia(0);
        ws.getAdes().getAdeDtDecor().setAdeDtDecor(0);
        ws.getAdes().getAdeDtScad().setAdeDtScad(0);
        ws.getAdes().getAdeEtaAScad().setAdeEtaAScad(0);
        ws.getAdes().getAdeDurAa().setAdeDurAa(0);
        ws.getAdes().getAdeDurMm().setAdeDurMm(0);
        ws.getAdes().getAdeDurGg().setAdeDurGg(0);
        ws.getAdes().setAdeTpRgmFisc("");
        ws.getAdes().setAdeTpRiat("");
        ws.getAdes().setAdeTpModPagTit("");
        ws.getAdes().setAdeTpIas("");
        ws.getAdes().getAdeDtVarzTpIas().setAdeDtVarzTpIas(0);
        ws.getAdes().getAdePreNetInd().setAdePreNetInd(new AfDecimal(0, 15, 3));
        ws.getAdes().getAdePreLrdInd().setAdePreLrdInd(new AfDecimal(0, 15, 3));
        ws.getAdes().getAdeRatLrdInd().setAdeRatLrdInd(new AfDecimal(0, 15, 3));
        ws.getAdes().getAdePrstzIniInd().setAdePrstzIniInd(new AfDecimal(0, 15, 3));
        ws.getAdes().setAdeFlCoincAssto(Types.SPACE_CHAR);
        ws.getAdes().setAdeIbDflt("");
        ws.getAdes().setAdeModCalc("");
        ws.getAdes().setAdeTpFntCnbtva("");
        ws.getAdes().getAdeImpAz().setAdeImpAz(new AfDecimal(0, 15, 3));
        ws.getAdes().getAdeImpAder().setAdeImpAder(new AfDecimal(0, 15, 3));
        ws.getAdes().getAdeImpTfr().setAdeImpTfr(new AfDecimal(0, 15, 3));
        ws.getAdes().getAdeImpVolo().setAdeImpVolo(new AfDecimal(0, 15, 3));
        ws.getAdes().getAdePcAz().setAdePcAz(new AfDecimal(0, 6, 3));
        ws.getAdes().getAdePcAder().setAdePcAder(new AfDecimal(0, 6, 3));
        ws.getAdes().getAdePcTfr().setAdePcTfr(new AfDecimal(0, 6, 3));
        ws.getAdes().getAdePcVolo().setAdePcVolo(new AfDecimal(0, 6, 3));
        ws.getAdes().getAdeDtNovaRgmFisc().setAdeDtNovaRgmFisc(0);
        ws.getAdes().setAdeFlAttiv(Types.SPACE_CHAR);
        ws.getAdes().getAdeImpRecRitVis().setAdeImpRecRitVis(new AfDecimal(0, 15, 3));
        ws.getAdes().getAdeImpRecRitAcc().setAdeImpRecRitAcc(new AfDecimal(0, 15, 3));
        ws.getAdes().setAdeFlVarzStatTbgc(Types.SPACE_CHAR);
        ws.getAdes().setAdeFlProvzaMigraz(Types.SPACE_CHAR);
        ws.getAdes().getAdeImpbVisDaRec().setAdeImpbVisDaRec(new AfDecimal(0, 15, 3));
        ws.getAdes().getAdeDtDecorPrestBan().setAdeDtDecorPrestBan(0);
        ws.getAdes().getAdeDtEffVarzStatT().setAdeDtEffVarzStatT(0);
        ws.getAdes().setAdeDsRiga(0);
        ws.getAdes().setAdeDsOperSql(Types.SPACE_CHAR);
        ws.getAdes().setAdeDsVer(0);
        ws.getAdes().setAdeDsTsIniCptz(0);
        ws.getAdes().setAdeDsTsEndCptz(0);
        ws.getAdes().setAdeDsUtente("");
        ws.getAdes().setAdeDsStatoElab(Types.SPACE_CHAR);
        ws.getAdes().getAdeCumCnbtCap().setAdeCumCnbtCap(new AfDecimal(0, 15, 3));
        ws.getAdes().getAdeImpGarCnbt().setAdeImpGarCnbt(new AfDecimal(0, 15, 3));
        ws.getAdes().getAdeDtUltConsCnbt().setAdeDtUltConsCnbt(0);
        ws.getAdes().setAdeIdenIscFnd("");
        ws.getAdes().getAdeNumRatPian().setAdeNumRatPian(new AfDecimal(0, 12, 5));
        ws.getAdes().getAdeDtPresc().setAdeDtPresc(0);
        ws.getAdes().setAdeConcsPrest(Types.SPACE_CHAR);
    }

    public void initAreaIoIsps0040() {
        ws.getAreaIoIsps0040().getIspc0040DatiInput().setCodCompagniaFormatted("00000");
        ws.getAreaIoIsps0040().getIspc0040DatiInput().setCodProdotto("");
        ws.getAreaIoIsps0040().getIspc0040DatiInput().setCodConvenzione("");
        ws.getAreaIoIsps0040().getIspc0040DatiInput().setDataInizValidConv("");
        ws.getAreaIoIsps0040().getIspc0040DatiInput().setDataRiferimento("");
        ws.getAreaIoIsps0040().getIspc0040DatiInput().setDataEmissione("");
        ws.getAreaIoIsps0040().getIspc0040DatiInput().setDataProposta("");
        ws.getAreaIoIsps0040().getIspc0040DatiInput().setLivelloUtenteFormatted("00");
        ws.getAreaIoIsps0040().getIspc0040DatiInput().setFunzionalitaFormatted("00000");
        ws.getAreaIoIsps0040().getIspc0040DatiInput().setSessionId("");
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setProdotto("");
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setDataVersioneProd("");
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setTipoVersioneProd("");
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setCodiceDivisa("");
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setDescrDivisa("");
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().getTipoPolizza().setTipoPolizza("");
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setDescPolizza("");
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setIspc0040TpTrasfNumEleFormatted("000");
        for (int idx0 = 1; idx0 <= Ispc0040DatiOutput1.TIPO_TRASFORMAZIONE_MAXOCCURS; idx0++) {
            ws.getAreaIoIsps0040().getIspc0040DatiOutput1().getTipoTrasformazione(idx0).getCodTpTrasf().setCodTpTrasf("");
            ws.getAreaIoIsps0040().getIspc0040DatiOutput1().getTipoTrasformazione(idx0).setDescrTpTrasf("");
        }
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setIspc0040TpAdeNumEleFormatted("000");
        for (int idx0 = 1; idx0 <= Ispc0040DatiOutput1.TIPO_ADESIONE_MAXOCCURS; idx0++) {
            ws.getAreaIoIsps0040().getIspc0040DatiOutput1().getTipoAdesione(idx0).getCodiceAdesione().setCodiceAdesione("");
            ws.getAreaIoIsps0040().getIspc0040DatiOutput1().getTipoAdesione(idx0).setDescrAdesione("");
        }
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setIspc0040NumEleModCalDurFormatted("000");
        for (int idx0 = 1; idx0 <= Ispc0040DatiOutput1.MOD_CALC_DUR_MAXOCCURS; idx0++) {
            ws.getAreaIoIsps0040().getIspc0040DatiOutput1().getModCalcDur(idx0).getCodiceMod().setCodiceMod(Types.SPACE_CHAR);
            ws.getAreaIoIsps0040().getIspc0040DatiOutput1().getModCalcDur(idx0).setDescrMod("");
        }
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().getFlagRischioComune().setFlagRischioComune(Types.SPACE_CHAR);
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setIspc0040NumEleDiffProFormatted("000");
        for (int idx0 = 1; idx0 <= Ispc0040DatiOutput1.DIFFERIMENTO_PROROGA_MAXOCCURS; idx0++) {
            ws.getAreaIoIsps0040().getIspc0040DatiOutput1().getDifferimentoProroga(idx0).getCodDiffProg().setCodDiffProg("");
            ws.getAreaIoIsps0040().getIspc0040DatiOutput1().getDifferimentoProroga(idx0).setDescDiffProg("");
        }
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setIspc0040AnniDifferProrFormatted("00");
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setTrattAaDiffPror(Types.SPACE_CHAR);
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setIspc0040NumEleModDurFormatted("000");
        for (int idx0 = 1; idx0 <= Ispc0040DatiOutput1.MODALITA_DURATA_MAXOCCURS; idx0++) {
            ws.getAreaIoIsps0040().getIspc0040DatiOutput1().getModalitaDurata(idx0).getCodDurata().setCodDurata("");
            ws.getAreaIoIsps0040().getIspc0040DatiOutput1().getModalitaDurata(idx0).setDescrDurata("");
        }
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setIspc0040EtaScadMaschiDfltFormatted("00000");
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setTrattEtaScadMaschi(Types.SPACE_CHAR);
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setIspc0040EtaScadFemmineDfltFormatted("00000");
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setTrattEtaScadFemm(Types.SPACE_CHAR);
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setDataScadenzaDflt("");
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setTrattDataScadFemm(Types.SPACE_CHAR);
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setIspc0040DurataAnniDefaultFormatted("00000");
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setTrattDurataAnni(Types.SPACE_CHAR);
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setIspc0040DurataMesiDefaultFormatted("00000");
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setTrattDurataMesi(Types.SPACE_CHAR);
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setIspc0040DurataGiorniDefaultFormatted("00000");
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setTrattDurataGiorni(Types.SPACE_CHAR);
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setIspc0040IndMaxFrazFormatted("000");
        for (int idx0 = 1; idx0 <= Ispc0040DatiOutput1.FRAZIONAMENTO_MAXOCCURS; idx0++) {
            ws.getAreaIoIsps0040().getIspc0040DatiOutput1().getFrazionamento(idx0).setCodFraz("");
            ws.getAreaIoIsps0040().getIspc0040DatiOutput1().getFrazionamento(idx0).setDescFraz("");
        }
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setTrattamentoFraz(Types.SPACE_CHAR);
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setIspc0040IndMaxImpScoFormatted("000");
        for (int idx0 = 1; idx0 <= Ispc0040DatiOutput1.IMP_SCONTO_MAXOCCURS; idx0++) {
            ws.getAreaIoIsps0040().getIspc0040DatiOutput1().getImpSconto(idx0).setCodImpSco("");
            ws.getAreaIoIsps0040().getIspc0040DatiOutput1().getImpSconto(idx0).setDescImpSco("");
        }
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setTrattamentoImpSco(Types.SPACE_CHAR);
        ws.getAreaIoIsps0040().getIspc0040DatiOutput1().setIspc0040IndMaxFondiFormatted("000");
        for (int idx0 = 1; idx0 <= Ispc0040TabFondi.FONDI_MAXOCCURS; idx0++) {
            ws.getAreaIoIsps0040().getIspc0040TabFondi().setIspc0040CodFondi(idx0, "");
            ws.getAreaIoIsps0040().getIspc0040TabFondi().setIspc0040DescFond(idx0, "");
        }
        ws.getAreaIoIsps0040().getIspc0040DatiOutput2().setTrattamentoFondo(Types.SPACE_CHAR);
        ws.getAreaIoIsps0040().getIspc0040DatiOutput2().getFlagCoperturaFin().setFlagCoperturaFin(Types.SPACE_CHAR);
        ws.getAreaIoIsps0040().getIspc0040DatiOutput2().getAdesSenzaAssic().setAdesSenzaAssic(Types.SPACE_CHAR);
        ws.getAreaIoIsps0040().getIspc0040DatiOutput2().setIspc0040FlagCumuloContrFormatted("0");
        ws.getAreaIoIsps0040().getIspc0040DatiOutput2().setFlagTrattDirEmis(Types.SPACE_CHAR);
        ws.getAreaIoIsps0040().getIspc0040DatiOutput2().setImpDirEmis(new AfDecimal(0, 18, 3));
        ws.getAreaIoIsps0040().getIspc0040DatiOutput2().setFlagTrattDirQuie(Types.SPACE_CHAR);
        ws.getAreaIoIsps0040().getIspc0040DatiOutput2().setImpDirQuie(new AfDecimal(0, 18, 3));
        ws.getAreaIoIsps0040().getIspc0040DatiOutput2().setFlagTrattDir1Ver(Types.SPACE_CHAR);
        ws.getAreaIoIsps0040().getIspc0040DatiOutput2().setImpDir1Ver(new AfDecimal(0, 18, 3));
        ws.getAreaIoIsps0040().getIspc0040DatiOutput2().setFlagTrattVersAgg(Types.SPACE_CHAR);
        ws.getAreaIoIsps0040().getIspc0040DatiOutput2().setImpDirVersAgg(new AfDecimal(0, 18, 3));
        ws.getAreaIoIsps0040().getIspc0040DatiOutput2().setFlagSpeseMediche(Types.SPACE_CHAR);
        ws.getAreaIoIsps0040().getIspc0040DatiOutput2().setImpSpeseMediche(new AfDecimal(0, 18, 3));
        ws.getAreaIoIsps0040().getIspc0040DatiOutput2().setIspc0040TpPremioNumEFormatted("000");
        for (int idx0 = 1; idx0 <= Ispc0040DatiOutput2.TIPO_PREMIO_MAXOCCURS; idx0++) {
            ws.getAreaIoIsps0040().getIspc0040DatiOutput2().getTipoPremio(idx0).getCodTpPremio().setCodTpPremio(Types.SPACE_CHAR);
            ws.getAreaIoIsps0040().getIspc0040DatiOutput2().getTipoPremio(idx0).setDescrTpPremio("");
        }
        ws.getAreaIoIsps0040().getIspc0040DatiOutput2().setTrattTipoPremio(Types.SPACE_CHAR);
        ws.getAreaIoIsps0040().getIspc0040DatiOutput2().getDistintaContr().setDistintaContr(Types.SPACE_CHAR);
        ws.getAreaIoIsps0040().getIspc0040DatiOutput2().setDataDecor("");
        ws.getAreaIoIsps0040().getIspc0040DatiOutput2().setDataDecorTratt(Types.SPACE_CHAR);
        ws.getAreaIoIsps0040().getIspc0040DatiOutput2().setIspc0040NumGgReinvestFormatted("00000");
        ws.getAreaIoIsps0040().getIspc0040DatiOutput2().setIspc0040MaxOperFormInvestFormatted("000");
        for (int idx0 = 1; idx0 <= Ispc0040DatiOutput2.OPER_FORMA_REINVEST_MAXOCCURS; idx0++) {
            ws.getAreaIoIsps0040().getIspc0040DatiOutput2().getOperFormaReinvest(idx0).setIspc0040CodOperReinvFormatted("0000");
            ws.getAreaIoIsps0040().getIspc0040DatiOutput2().getOperFormaReinvest(idx0).setIspc0040CodFormaReinvFormatted("0000");
            ws.getAreaIoIsps0040().getIspc0040DatiOutput2().getOperFormaReinvest(idx0).setPercReinv(new AfDecimal(0, 14, 9));
        }
        ws.getAreaIoIsps0040().getIspc0040DatiOutput2().setSottocategoria("");
        ws.getAreaIoIsps0040().getIspc0040OutCpiProtection().setAmmContrResEst(Types.SPACE_CHAR);
        ws.getAreaIoIsps0040().getIspc0040OutCpiProtection().setFlCpiProtection(Types.SPACE_CHAR);
        ws.getAreaIoIsps0040().getIspc0040OutCpiProtection().setFlBundling(Types.SPACE_CHAR);
        ws.getAreaIoIsps0040().getIspc0040OutCpiProtection().setFlBundlingTratt(Types.SPACE_CHAR);
        ws.getAreaIoIsps0040().getIspc0040OutCpiProtection().setFlPrincColl(Types.SPACE_CHAR);
        ws.getAreaIoIsps0040().getIspc0040OutCpiProtection().setTrattPacchetto(Types.SPACE_CHAR);
        ws.getAreaIoIsps0040().getIspc0040OutCpiProtection().setIspc0040EleMaxPacchettoFormatted("000");
        for (int idx0 = 1; idx0 <= Ispc0040OutCpiProtection.DATI_PACCHETTO_MAXOCCURS; idx0++) {
            ws.getAreaIoIsps0040().getIspc0040OutCpiProtection().getDatiPacchetto(idx0).setCodPacchetto("");
            ws.getAreaIoIsps0040().getIspc0040OutCpiProtection().getDatiPacchetto(idx0).setDescPacchetto("");
        }
        ws.getAreaIoIsps0040().getIspc0040OutCpiProtection().setFlContCoincAsst(Types.SPACE_CHAR);
        ws.getAreaIoIsps0040().setIspc0040EsitoFormatted("00");
        ws.getAreaIoIsps0040().setIspc0040ErrNumEleFormatted("000");
        for (int idx0 = 1; idx0 <= AreaIoIsps0040.ISPC0040_TAB_ERRORI_MAXOCCURS; idx0++) {
            ws.getAreaIoIsps0040().getIspc0040TabErrori(idx0).setCodErr("");
            ws.getAreaIoIsps0040().getIspc0040TabErrori(idx0).setDescrizioneErr("");
            ws.getAreaIoIsps0040().getIspc0040TabErrori(idx0).setIspc0211GravitaErrFormatted("00");
            ws.getAreaIoIsps0040().getIspc0040TabErrori(idx0).setIspc0211LivelloDerogaFormatted("00");
        }
    }

    public void initLdbv0011() {
        ws.getLdbv0011().setTitCont(0);
        ws.getLdbv0011().setDettTitCont(0);
        ws.getLdbv0011().setPoli(0);
        ws.getLdbv0011().setAdes(0);
        ws.getLdbv0011().setTrchDiGar(0);
        ws.getLdbv0011().setGar(0);
    }

    public void initW870DatiTit() {
        ws.getW870AreaLoas0870().getW870TabTit().setIdTitCont(1, ws.getIxIndici().getTitW870(), 0);
        ws.getW870AreaLoas0870().getW870TabTit().setDtVlt(1, ws.getIxIndici().getTitW870(), 0);
        ws.getW870AreaLoas0870().getW870TabTit().setDtIniEff(1, ws.getIxIndici().getTitW870(), 0);
        ws.getW870AreaLoas0870().getW870TabTit().setTpStatTit(1, ws.getIxIndici().getTitW870(), "");
    }

    public void initWskdAreaVariabiliP() {
        ws.getWskdAreaScheda().getWskdTabValP().setWskdEleVariabiliMaxP(1, ((short)0));
        for (int idx0 = 1; idx0 <= WskdTabValP.TAB_VARIABILI_P_MAXOCCURS; idx0++) {
            ws.getWskdAreaScheda().getWskdTabValP().setWskdCodVariabileP(1, idx0, "");
            ws.getWskdAreaScheda().getWskdTabValP().setWskdTpDatoP(1, idx0, Types.SPACE_CHAR);
            ws.getWskdAreaScheda().getWskdTabValP().setWskdValGenericoP(1, idx0, "");
        }
    }

    public void initWskdVarAutOper() {
        ws.getWskdAreaScheda().setWskdEleCtrlAutOperMax(((short)0));
        for (int idx0 = 1; idx0 <= WskdAreaScheda.WSKD_CTRL_AUT_OPER_MAXOCCURS; idx0++) {
            ws.getWskdAreaScheda().getWskdCtrlAutOper(idx0).setCodErrore(0);
            ws.getWskdAreaScheda().getWskdCtrlAutOper(idx0).setCodLivAut(0);
            ws.getWskdAreaScheda().getWskdCtrlAutOper(idx0).setTpMotDeroga("");
            ws.getWskdAreaScheda().getWskdCtrlAutOper(idx0).setModVerifica("");
            ws.getWskdAreaScheda().getWskdCtrlAutOper(idx0).setCodiceCondizione("");
            ws.getWskdAreaScheda().getWskdCtrlAutOper(idx0).setProgressCondition(((short)0));
            ws.getWskdAreaScheda().getWskdCtrlAutOper(idx0).setRisultatoCondizione(Types.SPACE_CHAR);
            ws.getWskdAreaScheda().getWskdCtrlAutOper(idx0).setEleParamMax(((short)0));
            for (int idx1 = 1; idx1 <= Ivvv0212CtrlAutOper.TAB_PARAM_MAXOCCURS; idx1++) {
                ws.getWskdAreaScheda().getWskdCtrlAutOper(idx0).getTabParam(idx1).setCodParam("");
                ws.getWskdAreaScheda().getWskdCtrlAutOper(idx0).getTabParam(idx1).setValParam("");
            }
        }
    }

    public void initWskdAreaVariabiliT() {
        ws.getWskdAreaScheda().getWskdTabValT().setWskdEleVariabiliMaxT(1, ((short)0));
        for (int idx0 = 1; idx0 <= WskdTabValT.TAB_VARIABILI_T_MAXOCCURS; idx0++) {
            ws.getWskdAreaScheda().getWskdTabValT().setWskdCodVariabileT(1, idx0, "");
            ws.getWskdAreaScheda().getWskdTabValT().setWskdTpDatoT(1, idx0, Types.SPACE_CHAR);
            ws.getWskdAreaScheda().getWskdTabValT().setWskdValGenericoT(1, idx0, "");
        }
    }

    public void initS211DatiInput() {
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setPgm("");
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().getFlgArea().setFlgArea("");
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().getModalitaEsecutiva().setModalitaEsecutiva(Types.SPACE_CHAR);
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setS211CodCompagniaAniaFormatted("00000");
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setS211TipoMovimentoFormatted("00000");
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setS211TipoMoviOrigFormatted("00000");
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setCodMainBatch("");
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setDataEffetto(0);
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setDataCompetenza(0);
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setDataCompAggStor(0);
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setDataUltVersProd(0);
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setDataUltTitInc(0);
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setTrattamentoStoricita("");
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().getFormatoDataDb().setFormatoDataDb("");
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().getStepElab().setStepElab(Types.SPACE_CHAR);
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setKeyAutOper1("");
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setKeyAutOper2("");
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setKeyAutOper3("");
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setKeyAutOper4("");
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setKeyAutOper5("");
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().getFlagGarOpzione().setFlagGarOpzione(Types.SPACE_CHAR);
        ws.getAreaIoIvvs0211().getIvvc0211DatiInput().setEleInfoMax(((short)0));
    }

    public void initS211RestoDati() {
        ws.getAreaIoIvvs0211().getIvvc0211RestoDati().setBufferDati("");
        ws.getAreaIoIvvs0211().getIvvc0211RestoDati().setIdTabTemp(0);
        ws.getAreaIoIvvs0211().getIvvc0211RestoDati().getReturnCode().setReturnCode("");
        ws.getAreaIoIvvs0211().getIvvc0211RestoDati().getCampiEsito().setDescrizErr("");
        ws.getAreaIoIvvs0211().getIvvc0211RestoDati().getCampiEsito().setCodServizioBe("");
        ws.getAreaIoIvvs0211().getIvvc0211RestoDati().getCampiEsito().setNomeTabella("");
        ws.getAreaIoIvvs0211().getIvvc0211RestoDati().getCampiEsito().setKeyTabella("");
        ws.getAreaIoIvvs0211().getIvvc0211RestoDati().setEleMaxNotFound(((short)0));
        for (int idx0 = 1; idx0 <= Ivvc0211RestoDati.TAB_VAR_NOT_FOUND_MAXOCCURS; idx0++) {
            ws.getAreaIoIvvs0211().getIvvc0211RestoDati().getTabVarNotFound(idx0).setFound("");
            ws.getAreaIoIvvs0211().getIvvc0211RestoDati().getTabVarNotFound(idx0).setFoundSp(Types.SPACE_CHAR);
        }
        ws.getAreaIoIvvs0211().getIvvc0211RestoDati().setEleMaxCalcKo(((short)0));
        for (int idx0 = 1; idx0 <= Ivvc0211RestoDati.TAB_CALCOLO_KO_MAXOCCURS; idx0++) {
            ws.getAreaIoIvvs0211().getIvvc0211RestoDati().getTabCalcoloKo(idx0).setCalcoloKo("");
            ws.getAreaIoIvvs0211().getIvvc0211RestoDati().getTabCalcoloKo(idx0).setNotFoundSp(Types.SPACE_CHAR);
        }
        for (int idx0 = 1; idx0 <= Ivvc0211RestoDati.ADDRESSES_MAXOCCURS; idx0++) {
            ws.getAreaIoIvvs0211().getIvvc0211RestoDati().getAddresses(idx0).setType(Types.SPACE_CHAR);
        }
        ws.getAreaIoIvvs0211().getIvvc0211RestoDati().setCodiceIniziativa("");
        ws.getAreaIoIvvs0211().getIvvc0211RestoDati().setCodiceTrattato("");
        ws.getAreaIoIvvs0211().getIvvc0211RestoDati().getFlAreaVarExtra().setFlAreaVarExtra(Types.SPACE_CHAR);
    }

    public void initS211TabInfo() {
        ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211TabAlias(1, "");
        ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211NumEleTabAlias(1, 0);
        ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211PosizIni(1, 0);
        ws.getAreaIoIvvs0211().getIvvc0211TabInfo1().setS211Lunghezza(1, 0);
    }

    public void initWRecOutriva() {
        ws.getLoar0171().setWrecTipoRec("");
        ws.getLoar0171().getWrecOut().setWrecOut("");
    }

    public void initLdbvd601() {
        ws.getLdbvd601().setIdOgg(0);
        ws.getLdbvd601().setTpOgg("");
        ws.getLdbvd601().setTpStaTit("");
        ws.getLdbvd601().setTpTit("");
    }

    public void initWpmoAreaParamMovi() {
        areaMain.setWpmoEleParamMovMax(((short)0));
        for (int idx0 = 1; idx0 <= AreaMainLoas0310.WPMO_TAB_PARAM_MOV_MAXOCCURS; idx0++) {
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getStatus().setStatus(Types.SPACE_CHAR);
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().setIdPtf(0);
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoIdParamMovi(0);
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoIdOgg(0);
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoTpOgg("");
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoIdMoviCrz(0);
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoIdMoviChiu().setWpmoIdMoviChiu(0);
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoDtIniEff(0);
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoDtEndEff(0);
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoCodCompAnia(0);
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoTpMovi().setWpmoTpMovi(0);
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoFrqMovi().setWpmoFrqMovi(0);
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoDurAa().setWpmoDurAa(0);
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoDurMm().setWpmoDurMm(0);
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoDurGg().setWpmoDurGg(0);
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoDtRicorPrec().setWpmoDtRicorPrec(0);
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoDtRicorSucc().setWpmoDtRicorSucc(0);
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoPcIntrFraz().setWpmoPcIntrFraz(new AfDecimal(0, 6, 3));
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoImpBnsDaScoTot().setWpmoImpBnsDaScoTot(new AfDecimal(0, 15, 3));
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoImpBnsDaSco().setWpmoImpBnsDaSco(new AfDecimal(0, 15, 3));
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoPcAnticBns().setWpmoPcAnticBns(new AfDecimal(0, 6, 3));
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoTpRinnColl("");
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoTpRivalPre("");
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoTpRivalPrstz("");
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoFlEvidRival(Types.SPACE_CHAR);
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoUltPcPerd().setWpmoUltPcPerd(new AfDecimal(0, 6, 3));
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoTotAaGiaPror().setWpmoTotAaGiaPror(0);
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoTpOpz("");
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoAaRenCer().setWpmoAaRenCer(0);
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoPcRevrsb().setWpmoPcRevrsb(new AfDecimal(0, 6, 3));
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoImpRiscParzPrgt().setWpmoImpRiscParzPrgt(new AfDecimal(0, 15, 3));
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoImpLrdDiRat().setWpmoImpLrdDiRat(new AfDecimal(0, 15, 3));
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoIbOgg("");
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoCosOner().setWpmoCosOner(new AfDecimal(0, 15, 3));
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoSpePc().setWpmoSpePc(new AfDecimal(0, 6, 3));
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoFlAttivGar(Types.SPACE_CHAR);
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoCambioVerProd(Types.SPACE_CHAR);
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoMmDiff().setWpmoMmDiff(((short)0));
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoImpRatManfee().setWpmoImpRatManfee(new AfDecimal(0, 15, 3));
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoDtUltErogManfee().setWpmoDtUltErogManfee(0);
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoTpOggRival("");
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoSomAsstaGarac().setWpmoSomAsstaGarac(new AfDecimal(0, 15, 3));
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoPcApplzOpz().setWpmoPcApplzOpz(new AfDecimal(0, 6, 3));
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoIdAdes().setWpmoIdAdes(0);
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoIdPoli(0);
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoTpFrmAssva("");
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoDsRiga(0);
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoDsOperSql(Types.SPACE_CHAR);
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoDsVer(0);
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoDsTsIniCptz(0);
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoDsTsEndCptz(0);
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoDsUtente("");
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoDsStatoElab(Types.SPACE_CHAR);
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoTpEstrCnt("");
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoCodRamo("");
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoGenDaSin(Types.SPACE_CHAR);
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoCodTari("");
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoNumRatPagPre().setWpmoNumRatPagPre(0);
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoPcServVal().setWpmoPcServVal(new AfDecimal(0, 6, 3));
            areaMain.getWpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoEtaAaSoglBnficr().setWpmoEtaAaSoglBnficr(((short)0));
        }
    }

    public void initAreaIoLccs0022() {
        ws.getAreaIoLccs0022().setIdPoli(0);
        ws.getAreaIoLccs0022().setIdAdes(0);
        ws.getAreaIoLccs0022().setTpOgg("");
        ws.getAreaIoLccs0022().getTpStatoBlocco().setTpStatoBlocco("");
        ws.getAreaIoLccs0022().setFlPresGravBloc(Types.SPACE_CHAR);
        ws.getAreaIoLccs0022().setEleMaxOggBloc(((short)0));
        for (int idx0 = 1; idx0 <= Lccc0022AreaComunicaz.TAB_OGG_BLOCCO_MAXOCCURS; idx0++) {
            ws.getAreaIoLccs0022().getTabOggBlocco(idx0).setIdOggBlocco(0);
            ws.getAreaIoLccs0022().getTabOggBlocco(idx0).setIdOgg1rio(0);
            ws.getAreaIoLccs0022().getTabOggBlocco(idx0).setTpOgg1rio("");
            ws.getAreaIoLccs0022().getTabOggBlocco(idx0).setCodCompAnia(0);
            ws.getAreaIoLccs0022().getTabOggBlocco(idx0).setTpMovi(0);
            ws.getAreaIoLccs0022().getTabOggBlocco(idx0).setTpOgg("");
            ws.getAreaIoLccs0022().getTabOggBlocco(idx0).setIdOgg(0);
            ws.getAreaIoLccs0022().getTabOggBlocco(idx0).setDtEffetto(0);
            ws.getAreaIoLccs0022().getTabOggBlocco(idx0).setTpStatBlocco("");
            ws.getAreaIoLccs0022().getTabOggBlocco(idx0).setCodBlocco("");
            ws.getAreaIoLccs0022().getTabOggBlocco(idx0).setDescrizione("");
            ws.getAreaIoLccs0022().getTabOggBlocco(idx0).setGravita(Types.SPACE_CHAR);
        }
    }

    public void initAreaIoLccs0023() {
        ws.getAreaIoLccs0023().setIdOggPtf(0);
        ws.getAreaIoLccs0023().setTpOggPtf("");
        ws.getAreaIoLccs0023().setPreGravBloccante(Types.SPACE_CHAR);
        ws.getAreaIoLccs0023().setPreGravAnnullabile(Types.SPACE_CHAR);
        ws.getAreaIoLccs0023().setEleMovFuturiMax(((short)0));
        for (int idx0 = 1; idx0 <= AreaIoLccs0023.TAB_MOV_FUTURI_MAXOCCURS; idx0++) {
            ws.getAreaIoLccs0023().getTabMovFuturi(idx0).setLccc0023IdMovi(0);
            ws.getAreaIoLccs0023().getTabMovFuturi(idx0).getLccc0023IdOgg().setLccc0023IdOgg(0);
            ws.getAreaIoLccs0023().getTabMovFuturi(idx0).setLccc0023IbOgg("");
            ws.getAreaIoLccs0023().getTabMovFuturi(idx0).setLccc0023IbMovi("");
            ws.getAreaIoLccs0023().getTabMovFuturi(idx0).setLccc0023TpOgg("");
            ws.getAreaIoLccs0023().getTabMovFuturi(idx0).getLccc0023IdRich().setLccc0023IdRich(0);
            ws.getAreaIoLccs0023().getTabMovFuturi(idx0).getLccc0023TpMovi().setLccc0023TpMovi(0);
            ws.getAreaIoLccs0023().getTabMovFuturi(idx0).setLccc0023DtEff(0);
            ws.getAreaIoLccs0023().getTabMovFuturi(idx0).getLccc0023IdMoviAnn().setLccc0023IdMoviAnn(0);
            ws.getAreaIoLccs0023().getTabMovFuturi(idx0).setLccc0023IdMoviPrenotaz(0);
            ws.getAreaIoLccs0023().getTabMovFuturi(idx0).setLccc0023TpGravitaMovimento("");
            ws.getAreaIoLccs0023().getTabMovFuturi(idx0).setLccc0023DsTsCptz(0);
        }
    }

    public void initAreaIoLoas0280() {
        ws.getAreaIoLoas0280().setIdPoli(0);
        ws.getAreaIoLoas0280().setIdAdes(0);
        ws.getAreaIoLoas0280().setTpOgg("");
        ws.getAreaIoLoas0280().getFlPresDeroga().setFlPresDeroga(Types.SPACE_CHAR);
        ws.getAreaIoLoas0280().getGravitaDeroga().setGravitaDeroga(Types.SPACE_CHAR);
        ws.getAreaIoLoas0280().getLivelloDeroga().setLivelloDeroga("");
    }

    public void initWs0040DatiInput() {
        ws.getWs0040DatiInput().setCodCompagniaFormatted("00000");
        ws.getWs0040DatiInput().setCodProdotto("");
        ws.getWs0040DatiInput().setCodConvenzione("");
        ws.getWs0040DatiInput().setDataInizValidConv("");
        ws.getWs0040DatiInput().setDataRiferimento("");
        ws.getWs0040DatiInput().setDataEmissione("");
        ws.getWs0040DatiInput().setDataProposta("");
        ws.getWs0040DatiInput().setLivelloUtenteFormatted("00");
        ws.getWs0040DatiInput().setFunzionalitaFormatted("00000");
        ws.getWs0040DatiInput().setSessionId("");
    }

    public void initIdsi0011Area() {
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011Pgm("");
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011ModalitaEsecutiva().setIdsi0011ModalitaEsecutiva(Types.SPACE_CHAR);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceCompagniaAnia(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodMainBatch("");
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011TipoMovimentoFormatted("00000");
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011Sessione("");
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011UserName("");
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompAggStor(0);
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattamentoStoricita("");
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011FormatoDataDb().setIdsi0011FormatoDataDb("");
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011IdMoviAnnullatoFormatted("000000000");
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011IdentitaChiamante().setIdsi0011IdentitaChiamante("");
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setLivelloOperazione("");
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setOperazione("");
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011FlagCodaTs().setIdsi0011FlagCodaTs(Types.SPACE_CHAR);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("");
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferWhereCond("");
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloDebug().setIdsi0011LivelloDebugFormatted("0");
    }

    public void initStrPerformanceDbg() {
        ws.getIdsv8888().getStrPerformanceDbg().setFase("");
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("");
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("");
        ws.getIdsv8888().getStrPerformanceDbg().setTimestampFld("");
        ws.getIdsv8888().getStrPerformanceDbg().setStato("");
        ws.getIdsv8888().getStrPerformanceDbg().setModalitaEsecutiva(Types.SPACE_CHAR);
        ws.getIdsv8888().getStrPerformanceDbg().setUserName("");
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int OUTRIVA_REC = 500;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
