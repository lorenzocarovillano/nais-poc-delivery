package it.accenture.jnais;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Idsv0003CampiEsito;
import it.accenture.jnais.ws.enums.Idsv0003Sqlcode;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.Lvvs0058Data;

/**Original name: LVVS0058<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2007.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *   PROGRAMMA...... LVVS0058
 *   TIPOLOGIA...... SERVIZIO
 *   PROCESSO....... XXX
 *   FUNZIONE....... XXX
 *   DESCRIZIONE.... CONTROLLO SALTO RATA
 * **------------------------------------------------------------***</pre>*/
public class Lvvs0058 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lvvs0058Data ws = new Lvvs0058Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS0058
    private Ivvc0213 ivvc0213;

    //==== METHODS ====
    /**Original name: PROGRAM_LVVS0058_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: PERFORM S1000-ELABORAZIONE
        //              THRU EX-S1000
        s1000Elaborazione();
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lvvs0058 getInstance() {
        return ((Lvvs0058)Programs.getInstance(Lvvs0058.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                        IX-INDICI
        //                                             IVVC0213-TAB-OUTPUT.
        initIxIndici();
        initTabOutput();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: INITIALIZE LDBV2131.
        initLdbv2131();
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE
        //             TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: INITIALIZE WK-DATA-OUTPUT
        //                      WK-DATA-X-12.
        ws.setWkDataOutput(new AfDecimal(0, 11, 7));
        initWkDataX12();
        //--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
        //--> RISPETTIVE AREE DCLGEN IN WORKING
        // COB_CODE: PERFORM S1100-VALORIZZA-DCLGEN
        //              THRU S1100-VALORIZZA-DCLGEN-EX
        //           VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
        //                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //                   SPACES OR LOW-VALUE OR HIGH-VALUE.
        ws.setIxDclgen(((short)1));
        while (!(ws.getIxDclgen() > ivvc0213.getEleInfoMax() || Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias()) || Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()) || Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()))) {
            //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformVaryingStmtImpl @source=LVVS0058.cbl:line=128, because the code is unreachable.
            ws.setIxDclgen(Trunc.toShort(ws.getIxDclgen() + 1, 4));
        }
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //               MOVE 'IN'                     TO LDBV2131-TP-STAT-TIT-01
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: MOVE IVVC0213-ID-TRANCHE      TO LDBV2131-ID-OGG
            ws.getLdbv2131().setIdOgg(ivvc0213.getIdTranche());
            // COB_CODE: MOVE 'TG'                     TO LDBV2131-TP-OGG
            ws.getLdbv2131().setTpOgg("TG");
            // COB_CODE: MOVE 'IN'                     TO LDBV2131-TP-STAT-TIT-01
            ws.getLdbv2131().setTpStatTit01("IN");
        }
        // COB_CODE: PERFORM S1250-CALCOLA-DATA1       THRU S1250-EX.
        s1250CalcolaData1();
    }

    /**Original name: S1250-CALCOLA-DATA1<br>
	 * <pre>----------------------------------------------------------------*
	 *   CALCOLA DATA 1
	 * ----------------------------------------------------------------*</pre>*/
    private void s1250CalcolaData1() {
        Ldbs2130 ldbs2130 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE ZERO                      TO IVVC0213-VAL-IMP-O.
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        //--> TIPO OPERAZIONE
        // COB_CODE: SET IDSV0003-FETCH-FIRST       TO TRUE
        idsv0003.getOperazione().setFetchFirst();
        //----------------------------------------------------------------*
        // COB_CODE:      PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC OR
        //                              NOT IDSV0003-SUCCESSFUL-SQL
        //           *--> LIVELLO OPERAZIONE
        //                   END-IF
        //                END-PERFORM.
        while (!(!idsv0003.getReturnCode().isSuccessfulRc() || !idsv0003.getSqlcode().isSuccessfulSql())) {
            //--> LIVELLO OPERAZIONE
            // COB_CODE: SET IDSV0003-WHERE-CONDITION     TO TRUE
            idsv0003.getLivelloOperazione().setWhereCondition();
            // COB_CODE: SET IDSV0003-TRATT-X-EFFETTO     TO TRUE
            idsv0003.getTrattamentoStoricita().setIdsi0011TrattXEffetto();
            // COB_CODE: MOVE 'LDBS2130'                     TO WK-CALL-PGM
            ws.setWkCallPgm("LDBS2130");
            //
            // COB_CODE: CALL WK-CALL-PGM  USING  IDSV0003 LDBV2131
            //           ON EXCEPTION
            //                SET IDSV0003-INVALID-OPER  TO TRUE
            //           END-CALL
            try {
                ldbs2130 = Ldbs2130.getInstance();
                ldbs2130.run(idsv0003, ws.getLdbv2131());
            }
            catch (ProgramExecutionException __ex) {
                // COB_CODE: MOVE WK-CALL-PGM
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
                // COB_CODE: MOVE 'CALL-LDBS2130 ERRORE CHIAMATA - T0000-TRATTA-MATRICE'
                //                 TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBS2130 ERRORE CHIAMATA - T0000-TRATTA-MATRICE");
                // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
            //
            // COB_CODE:         IF IDSV0003-SUCCESSFUL-RC
            //                      END-EVALUATE
            //                   ELSE
            //           *--> GESTIRE ERRORE
            //                      SET IDSV0003-INVALID-OPER            TO TRUE
            //                   END-IF
            if (idsv0003.getReturnCode().isSuccessfulRc()) {
                // COB_CODE:            EVALUATE TRUE
                //                          WHEN IDSV0003-SUCCESSFUL-SQL
                //           *-->    OPERAZIONE ESEGUITA CORRETTAMENTE
                //                              SET IDSV0003-FETCH-NEXT      TO TRUE
                //                          WHEN IDSV0003-NOT-FOUND
                //                                               TO IDSV0003-DESCRIZ-ERR-DB2
                //                          WHEN OTHER
                //           *--->   ERRORE DI ACCESSO AL DB
                //                              SET IDSV0003-INVALID-OPER            TO TRUE
                //                      END-EVALUATE
                switch (idsv0003.getSqlcode().getSqlcode()) {

                    case Idsv0003Sqlcode.SUCCESSFUL_SQL://-->    OPERAZIONE ESEGUITA CORRETTAMENTE
                        // COB_CODE: ADD LDBV2131-TOT-PREMI TO IVVC0213-VAL-IMP-O
                        ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(ws.getLdbv2131().getTotPremi().add(ivvc0213.getTabOutput().getValImpO()), 18, 7));
                        // COB_CODE: SET IDSV0003-FETCH-NEXT      TO TRUE
                        idsv0003.getOperazione().setFetchNext();
                        break;

                    case Idsv0003Sqlcode.NOT_FOUND:// COB_CODE: MOVE SPACES
                        //                           TO IDSV0003-DESCRIZ-ERR-DB2
                        idsv0003.getCampiEsito().setDescrizErrDb2("");
                        break;

                    default://--->   ERRORE DI ACCESSO AL DB
                        // COB_CODE: MOVE WK-CALL-PGM    TO IDSV0003-COD-SERVIZIO-BE
                        idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
                        // COB_CODE: STRING 'CHIAMATA LDBS2130 ;'
                        //               IDSV0003-RETURN-CODE ';'
                        //               IDSV0003-SQLCODE
                        //           DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                        //           END-STRING
                        concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LDBS2130 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                        idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                        // COB_CODE: SET IDSV0003-INVALID-OPER            TO TRUE
                        idsv0003.getReturnCode().setInvalidOper();
                        break;
                }
            }
            else {
                //--> GESTIRE ERRORE
                // COB_CODE: MOVE WK-CALL-PGM         TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
                // COB_CODE: STRING 'CHIAMATA LDBS2130 ;'
                //               IDSV0003-RETURN-CODE ';'
                //               IDSV0003-SQLCODE
                //           DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LDBS2130 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                // COB_CODE: SET IDSV0003-INVALID-OPER            TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
        }
        // COB_CODE: IF IDSV0003-NOT-FOUND
        //              SET IDSV0003-SUCCESSFUL-RC   TO TRUE
        //           END-IF.
        if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL  TO TRUE
            idsv0003.getSqlcode().setSuccessfulSql();
            // COB_CODE: SET IDSV0003-SUCCESSFUL-RC   TO TRUE
            idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    public void initIxIndici() {
        ws.setIxDclgen(((short)0));
    }

    public void initTabOutput() {
        ivvc0213.getTabOutput().setCodVariabileO("");
        ivvc0213.getTabOutput().setTpDatoO(Types.SPACE_CHAR);
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        ivvc0213.getTabOutput().setValPercO(new AfDecimal(0, 14, 9));
        ivvc0213.getTabOutput().setValStrO("");
    }

    public void initLdbv2131() {
        ws.getLdbv2131().setIdOgg(0);
        ws.getLdbv2131().setTpOgg("");
        ws.getLdbv2131().setTpStatTit01("");
        ws.getLdbv2131().setTpStatTit02("");
        ws.getLdbv2131().setTpStatTit03("");
        ws.getLdbv2131().setTpStatTit04("");
        ws.getLdbv2131().setTpStatTit05("");
        ws.getLdbv2131().setTotPremi(new AfDecimal(0, 18, 3));
    }

    public void initWkDataX12() {
        ws.getWkDataX12().setxAa("");
        ws.getWkDataX12().setVirogla(Types.SPACE_CHAR);
        ws.getWkDataX12().setxGg("");
    }
}
