package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.jdbc.FieldNotMappedException;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.ImpstSostDao;
import it.accenture.jnais.commons.data.dao.TitContDao;
import it.accenture.jnais.commons.data.to.IImpstSost;
import it.accenture.jnais.copy.ImpstSost;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ldbs1590Data;
import it.accenture.jnais.ws.Ldbv1591;
import it.accenture.jnais.ws.redefines.IsoAlqIs;
import it.accenture.jnais.ws.redefines.IsoCumPreVers;
import it.accenture.jnais.ws.redefines.IsoDtEndPer;
import it.accenture.jnais.ws.redefines.IsoDtIniPer;
import it.accenture.jnais.ws.redefines.IsoIdMoviChiu;
import it.accenture.jnais.ws.redefines.IsoIdOgg;
import it.accenture.jnais.ws.redefines.IsoImpbIs;
import it.accenture.jnais.ws.redefines.IsoImpstSost;
import it.accenture.jnais.ws.redefines.IsoPrstzLrdAnteIs;
import it.accenture.jnais.ws.redefines.IsoPrstzNet;
import it.accenture.jnais.ws.redefines.IsoPrstzPrec;
import it.accenture.jnais.ws.redefines.IsoRisMatAnteTax;
import it.accenture.jnais.ws.redefines.IsoRisMatNetPrec;
import it.accenture.jnais.ws.redefines.IsoRisMatPostTax;

/**Original name: LDBS1590<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  12 LUG 2010.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO AD HOC PER ACCESSO RISORSE DB        *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Ldbs1590 extends Program implements IImpstSost {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private TitContLdbs1590 titContLdbs1590 = new TitContLdbs1590(this);
    private ImpstSostDao impstSostDao = new ImpstSostDao(dbAccessStatus);
    private TitContDao titContDao = new TitContDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Ldbs1590Data ws = new Ldbs1590Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: LDBV1591
    private Ldbv1591 ldbv1591;

    //==== METHODS ====
    /**Original name: PROGRAM_LDBS1590_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, Ldbv1591 ldbv1591) {
        this.idsv0003 = idsv0003;
        this.ldbv1591 = ldbv1591;
        // COB_CODE: PERFORM A000-INIZIO              THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                        a200ElaboraWcEff();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                        b200ElaboraWcCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //               SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                        c200ElaboraWcNst();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Ldbs1590 getInstance() {
        return ((Ldbs1590)Programs.getInstance(Ldbs1590.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'LDBS1590'              TO   IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("LDBS1590");
        // COB_CODE: MOVE 'LDBV1591' TO   IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("LDBV1591");
        // COB_CODE: MOVE '00'                      TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                    TO   IDSV0003-SQLCODE
        //                                               IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                    TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                               IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-WC-EFF<br>*/
    private void a200ElaboraWcEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-WC-EFF          THRU A210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-WC-EFF          THRU A210-EX
            a210SelectWcEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
            a260OpenCursorWcEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
            a270CloseCursorWcEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
            a280FetchFirstWcEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
            a290FetchNextWcEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B200-ELABORA-WC-CPZ<br>*/
    private void b200ElaboraWcCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
            b210SelectWcCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
            b260OpenCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
            b270CloseCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
            b280FetchFirstWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
            b290FetchNextWcCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: C200-ELABORA-WC-NST<br>*/
    private void c200ElaboraWcNst() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM C210-SELECT-WC-NST          THRU C210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM C210-SELECT-WC-NST          THRU C210-EX
            c210SelectWcNst();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
            c260OpenCursorWcNst();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
            c270CloseCursorWcNst();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
            c280FetchFirstWcNst();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
            c290FetchNextWcNst();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A205-DECLARE-CURSOR-WC-EFF<br>
	 * <pre>----
	 * ----  gestione WC Effetto
	 * ----</pre>*/
    private void a205DeclareCursorWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A210-SELECT-WC-EFF<br>*/
    private void a210SelectWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //            SELECT MAX(DT_INI_PER)
        //            INTO  :ISO-DT-INI-PER-DB
        //                  :IND-ISO-DT-INI-PER
        //                FROM IMPST_SOST
        //               WHERE ID_OGG = :LDBV1591-ID-ADES
        //                 AND TP_OGG = 'AD'
        //                 AND DT_INI_PER <=
        //                     :WS-DATA-INIZIO-EFFETTO-DB
        //                 AND DT_INI_EFF <=
        //                     :WS-DT-INFINITO-1
        //                 AND DT_END_EFF >
        //                     :WS-DT-INFINITO-1
        //                 AND DS_TS_INI_CPTZ <=
        //                     :WS-TS-INFINITO-1
        //                 AND DS_TS_END_CPTZ >
        //                     :WS-TS-INFINITO-1
        //                 AND COD_COMP_ANIA =
        //                     :IDSV0003-CODICE-COMPAGNIA-ANIA
        //           END-EXEC.
        impstSostDao.selectRec10(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LDBS1590.cbl:line=247, because the code is unreachable.
            // COB_CODE: IF IND-ISO-DT-INI-PER = -1
            //              PERFORM Z900-CONVERTI-N-TO-X  THRU Z900-EX
            //           END-IF
            if (ws.getIndImpstSost().getDtIniPer() == -1) {
                // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X  THRU Z900-EX
                z900ConvertiNToX();
            }
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: A260-OPEN-CURSOR-WC-EFF<br>*/
    private void a260OpenCursorWcEff() {
        // COB_CODE: PERFORM A205-DECLARE-CURSOR-WC-EFF THRU A205-EX.
        a205DeclareCursorWcEff();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A270-CLOSE-CURSOR-WC-EFF<br>*/
    private void a270CloseCursorWcEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A280-FETCH-FIRST-WC-EFF<br>*/
    private void a280FetchFirstWcEff() {
        // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF    THRU A260-EX.
        a260OpenCursorWcEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
            a290FetchNextWcEff();
        }
    }

    /**Original name: A290-FETCH-NEXT-WC-EFF<br>*/
    private void a290FetchNextWcEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B205-DECLARE-CURSOR-WC-CPZ<br>
	 * <pre>----
	 * ----  gestione WC Competenza
	 * ----</pre>*/
    private void b205DeclareCursorWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B210-SELECT-WC-CPZ<br>*/
    private void b210SelectWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //            SELECT MAX(DT_INI_PER)
        //            INTO  :ISO-DT-INI-PER-DB
        //                  :IND-ISO-DT-INI-PER
        //                FROM IMPST_SOST
        //               WHERE ID_OGG = :LDBV1591-ID-ADES
        //                 AND TP_OGG = 'AD'
        //                 AND DT_INI_PER <=
        //                     :WS-DATA-INIZIO-EFFETTO-DB
        //                 AND DT_INI_EFF <=
        //                     :WS-DT-INFINITO-1
        //                 AND DT_END_EFF >
        //                     :WS-DT-INFINITO-1
        //                 AND DS_TS_INI_CPTZ <=
        //                     :WS-TS-INFINITO-1
        //                 AND DS_TS_END_CPTZ >
        //                     :WS-TS-INFINITO-1
        //                 AND COD_COMP_ANIA =
        //                     :IDSV0003-CODICE-COMPAGNIA-ANIA
        //           END-EXEC.
        impstSostDao.selectRec10(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LDBS1590.cbl:line=330, because the code is unreachable.
            // COB_CODE: IF IND-ISO-DT-INI-PER = -1
            //              PERFORM Z900-CONVERTI-N-TO-X  THRU Z900-EX
            //           END-IF
            if (ws.getIndImpstSost().getDtIniPer() == -1) {
                // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X  THRU Z900-EX
                z900ConvertiNToX();
            }
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: B260-OPEN-CURSOR-WC-CPZ<br>*/
    private void b260OpenCursorWcCpz() {
        // COB_CODE: PERFORM B205-DECLARE-CURSOR-WC-CPZ THRU B205-EX.
        b205DeclareCursorWcCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B270-CLOSE-CURSOR-WC-CPZ<br>*/
    private void b270CloseCursorWcCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B280-FETCH-FIRST-WC-CPZ<br>*/
    private void b280FetchFirstWcCpz() {
        // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ    THRU B260-EX.
        b260OpenCursorWcCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
            b290FetchNextWcCpz();
        }
    }

    /**Original name: B290-FETCH-NEXT-WC-CPZ<br>*/
    private void b290FetchNextWcCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C205-DECLARE-CURSOR-WC-NST<br>
	 * <pre>----
	 * ----  gestione WC Senza Storicit—
	 * ----</pre>*/
    private void c205DeclareCursorWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C210-SELECT-WC-NST<br>*/
    private void c210SelectWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C260-OPEN-CURSOR-WC-NST<br>*/
    private void c260OpenCursorWcNst() {
        // COB_CODE: PERFORM C205-DECLARE-CURSOR-WC-NST THRU C205-EX.
        c205DeclareCursorWcNst();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C270-CLOSE-CURSOR-WC-NST<br>*/
    private void c270CloseCursorWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C280-FETCH-FIRST-WC-NST<br>*/
    private void c280FetchFirstWcNst() {
        // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST    THRU C260-EX.
        c260OpenCursorWcNst();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
            c290FetchNextWcNst();
        }
    }

    /**Original name: C290-FETCH-NEXT-WC-NST<br>*/
    private void c290FetchNextWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>----
	 * ----  utilità comuni a tutti i livelli operazione
	 * ----</pre>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-ISO-ID-OGG = -1
        //              MOVE HIGH-VALUES TO ISO-ID-OGG-NULL
        //           END-IF
        if (ws.getIndImpstSost().getIdOgg() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ISO-ID-OGG-NULL
            ws.getImpstSost().getIsoIdOgg().setIsoIdOggNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, IsoIdOgg.Len.ISO_ID_OGG_NULL));
        }
        // COB_CODE: IF IND-ISO-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO ISO-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndImpstSost().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ISO-ID-MOVI-CHIU-NULL
            ws.getImpstSost().getIsoIdMoviChiu().setIsoIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, IsoIdMoviChiu.Len.ISO_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-ISO-DT-INI-PER = -1
        //              MOVE HIGH-VALUES TO ISO-DT-INI-PER-NULL
        //           END-IF
        if (ws.getIndImpstSost().getDtIniPer() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ISO-DT-INI-PER-NULL
            ws.getImpstSost().getIsoDtIniPer().setIsoDtIniPerNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, IsoDtIniPer.Len.ISO_DT_INI_PER_NULL));
        }
        // COB_CODE: IF IND-ISO-DT-END-PER = -1
        //              MOVE HIGH-VALUES TO ISO-DT-END-PER-NULL
        //           END-IF
        if (ws.getIndImpstSost().getDtEndPer() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ISO-DT-END-PER-NULL
            ws.getImpstSost().getIsoDtEndPer().setIsoDtEndPerNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, IsoDtEndPer.Len.ISO_DT_END_PER_NULL));
        }
        // COB_CODE: IF IND-ISO-IMPST-SOST = -1
        //              MOVE HIGH-VALUES TO ISO-IMPST-SOST-NULL
        //           END-IF
        if (ws.getIndImpstSost().getImpstSost() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ISO-IMPST-SOST-NULL
            ws.getImpstSost().getIsoImpstSost().setIsoImpstSostNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, IsoImpstSost.Len.ISO_IMPST_SOST_NULL));
        }
        // COB_CODE: IF IND-ISO-IMPB-IS = -1
        //              MOVE HIGH-VALUES TO ISO-IMPB-IS-NULL
        //           END-IF
        if (ws.getIndImpstSost().getImpbIs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ISO-IMPB-IS-NULL
            ws.getImpstSost().getIsoImpbIs().setIsoImpbIsNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, IsoImpbIs.Len.ISO_IMPB_IS_NULL));
        }
        // COB_CODE: IF IND-ISO-ALQ-IS = -1
        //              MOVE HIGH-VALUES TO ISO-ALQ-IS-NULL
        //           END-IF
        if (ws.getIndImpstSost().getAlqIs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ISO-ALQ-IS-NULL
            ws.getImpstSost().getIsoAlqIs().setIsoAlqIsNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, IsoAlqIs.Len.ISO_ALQ_IS_NULL));
        }
        // COB_CODE: IF IND-ISO-COD-TRB = -1
        //              MOVE HIGH-VALUES TO ISO-COD-TRB-NULL
        //           END-IF
        if (ws.getIndImpstSost().getCodTrb() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ISO-COD-TRB-NULL
            ws.getImpstSost().setIsoCodTrb(LiteralGenerator.create(Types.HIGH_CHAR_VAL, ImpstSost.Len.ISO_COD_TRB));
        }
        // COB_CODE: IF IND-ISO-PRSTZ-LRD-ANTE-IS = -1
        //              MOVE HIGH-VALUES TO ISO-PRSTZ-LRD-ANTE-IS-NULL
        //           END-IF
        if (ws.getIndImpstSost().getPrstzLrdAnteIs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ISO-PRSTZ-LRD-ANTE-IS-NULL
            ws.getImpstSost().getIsoPrstzLrdAnteIs().setIsoPrstzLrdAnteIsNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, IsoPrstzLrdAnteIs.Len.ISO_PRSTZ_LRD_ANTE_IS_NULL));
        }
        // COB_CODE: IF IND-ISO-RIS-MAT-NET-PREC = -1
        //              MOVE HIGH-VALUES TO ISO-RIS-MAT-NET-PREC-NULL
        //           END-IF
        if (ws.getIndImpstSost().getRisMatNetPrec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ISO-RIS-MAT-NET-PREC-NULL
            ws.getImpstSost().getIsoRisMatNetPrec().setIsoRisMatNetPrecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, IsoRisMatNetPrec.Len.ISO_RIS_MAT_NET_PREC_NULL));
        }
        // COB_CODE: IF IND-ISO-RIS-MAT-ANTE-TAX = -1
        //              MOVE HIGH-VALUES TO ISO-RIS-MAT-ANTE-TAX-NULL
        //           END-IF
        if (ws.getIndImpstSost().getRisMatAnteTax() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ISO-RIS-MAT-ANTE-TAX-NULL
            ws.getImpstSost().getIsoRisMatAnteTax().setIsoRisMatAnteTaxNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, IsoRisMatAnteTax.Len.ISO_RIS_MAT_ANTE_TAX_NULL));
        }
        // COB_CODE: IF IND-ISO-RIS-MAT-POST-TAX = -1
        //              MOVE HIGH-VALUES TO ISO-RIS-MAT-POST-TAX-NULL
        //           END-IF
        if (ws.getIndImpstSost().getRisMatPostTax() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ISO-RIS-MAT-POST-TAX-NULL
            ws.getImpstSost().getIsoRisMatPostTax().setIsoRisMatPostTaxNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, IsoRisMatPostTax.Len.ISO_RIS_MAT_POST_TAX_NULL));
        }
        // COB_CODE: IF IND-ISO-PRSTZ-NET = -1
        //              MOVE HIGH-VALUES TO ISO-PRSTZ-NET-NULL
        //           END-IF
        if (ws.getIndImpstSost().getPrstzNet() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ISO-PRSTZ-NET-NULL
            ws.getImpstSost().getIsoPrstzNet().setIsoPrstzNetNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, IsoPrstzNet.Len.ISO_PRSTZ_NET_NULL));
        }
        // COB_CODE: IF IND-ISO-PRSTZ-PREC = -1
        //              MOVE HIGH-VALUES TO ISO-PRSTZ-PREC-NULL
        //           END-IF
        if (ws.getIndImpstSost().getPrstzPrec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ISO-PRSTZ-PREC-NULL
            ws.getImpstSost().getIsoPrstzPrec().setIsoPrstzPrecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, IsoPrstzPrec.Len.ISO_PRSTZ_PREC_NULL));
        }
        // COB_CODE: IF IND-ISO-CUM-PRE-VERS = -1
        //              MOVE HIGH-VALUES TO ISO-CUM-PRE-VERS-NULL
        //           END-IF.
        if (ws.getIndImpstSost().getCumPreVers() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ISO-CUM-PRE-VERS-NULL
            ws.getImpstSost().getIsoCumPreVers().setIsoCumPreVersNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, IsoCumPreVers.Len.ISO_CUM_PRE_VERS_NULL));
        }
    }

    /**Original name: Z900-CONVERTI-N-TO-X<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da 9(8) comp-3 a date
	 * ----</pre>*/
    private void z900ConvertiNToX() {
        // COB_CODE: MOVE LDBV1591-DT-DECOR  TO WS-DATE-N.
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(ldbv1591.getDtDecor(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX.
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO ISO-DT-INI-PER-DB.
        ws.getImpstSostDb().setPervRichDb(ws.getIdsv0010().getWsDateX());
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z970-CODICE-ADHOC-PRE<br>
	 * <pre>----
	 * ----  prevede statements AD HOC PRE Query
	 * ----</pre>*/
    private void z970CodiceAdhocPre() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z980-CODICE-ADHOC-POST<br>
	 * <pre>----
	 * ----  prevede statements AD HOC POST Query
	 * ----</pre>*/
    private void z980CodiceAdhocPost() {
        // COB_CODE: EXEC SQL
        //            SELECT VALUE (SUM(TOT_PRE_SOLO_RSH) , 0)
        //              INTO :LDBV1591-IMP-TOT
        //            FROM TIT_CONT
        //             WHERE ID_OGG = :LDBV1591-ID-OGG
        //               AND TP_OGG = :LDBV1591-TP-OGG
        //               AND DT_INI_COP BETWEEN :ISO-DT-INI-PER-DB
        //               AND :WS-DATA-INIZIO-EFFETTO-DB
        //               AND COD_COMP_ANIA =
        //                   :IDSV0003-CODICE-COMPAGNIA-ANIA
        //               AND DT_INI_EFF <=
        //                   :WS-DATA-INIZIO-EFFETTO-DB
        //               AND DT_END_EFF >
        //                   :WS-DATA-INIZIO-EFFETTO-DB
        //               AND DS_TS_INI_CPTZ <=
        //                   :WS-TS-COMPETENZA
        //               AND DS_TS_END_CPTZ >
        //                   :WS-TS-COMPETENZA
        //           END-EXEC.
        ldbv1591.setImpTot(titContDao.selectRec6(this.getTitContLdbs1590(), ldbv1591.getImpTot()));
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    @Override
    public AfDecimal getAlqIs() {
        throw new FieldNotMappedException("alqIs");
    }

    @Override
    public void setAlqIs(AfDecimal alqIs) {
        throw new FieldNotMappedException("alqIs");
    }

    @Override
    public AfDecimal getAlqIsObj() {
        return getAlqIs();
    }

    @Override
    public void setAlqIsObj(AfDecimal alqIsObj) {
        setAlqIs(new AfDecimal(alqIsObj, 6, 3));
    }

    @Override
    public int getCodCompAnia() {
        throw new FieldNotMappedException("codCompAnia");
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        throw new FieldNotMappedException("codCompAnia");
    }

    @Override
    public String getCodTrb() {
        throw new FieldNotMappedException("codTrb");
    }

    @Override
    public void setCodTrb(String codTrb) {
        throw new FieldNotMappedException("codTrb");
    }

    @Override
    public String getCodTrbObj() {
        return getCodTrb();
    }

    @Override
    public void setCodTrbObj(String codTrbObj) {
        setCodTrb(codTrbObj);
    }

    @Override
    public AfDecimal getCumPreVers() {
        throw new FieldNotMappedException("cumPreVers");
    }

    @Override
    public void setCumPreVers(AfDecimal cumPreVers) {
        throw new FieldNotMappedException("cumPreVers");
    }

    @Override
    public AfDecimal getCumPreVersObj() {
        return getCumPreVers();
    }

    @Override
    public void setCumPreVersObj(AfDecimal cumPreVersObj) {
        setCumPreVers(new AfDecimal(cumPreVersObj, 15, 3));
    }

    @Override
    public char getDsOperSql() {
        throw new FieldNotMappedException("dsOperSql");
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        throw new FieldNotMappedException("dsOperSql");
    }

    @Override
    public char getDsStatoElab() {
        throw new FieldNotMappedException("dsStatoElab");
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        throw new FieldNotMappedException("dsStatoElab");
    }

    @Override
    public long getDsTsEndCptz() {
        throw new FieldNotMappedException("dsTsEndCptz");
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        throw new FieldNotMappedException("dsTsEndCptz");
    }

    @Override
    public long getDsTsIniCptz() {
        throw new FieldNotMappedException("dsTsIniCptz");
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        throw new FieldNotMappedException("dsTsIniCptz");
    }

    @Override
    public String getDsUtente() {
        throw new FieldNotMappedException("dsUtente");
    }

    @Override
    public void setDsUtente(String dsUtente) {
        throw new FieldNotMappedException("dsUtente");
    }

    @Override
    public int getDsVer() {
        throw new FieldNotMappedException("dsVer");
    }

    @Override
    public void setDsVer(int dsVer) {
        throw new FieldNotMappedException("dsVer");
    }

    @Override
    public String getDtEndEffDb() {
        throw new FieldNotMappedException("dtEndEffDb");
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        throw new FieldNotMappedException("dtEndEffDb");
    }

    @Override
    public String getDtEndPerDb() {
        throw new FieldNotMappedException("dtEndPerDb");
    }

    @Override
    public void setDtEndPerDb(String dtEndPerDb) {
        throw new FieldNotMappedException("dtEndPerDb");
    }

    @Override
    public String getDtEndPerDbObj() {
        return getDtEndPerDb();
    }

    @Override
    public void setDtEndPerDbObj(String dtEndPerDbObj) {
        setDtEndPerDb(dtEndPerDbObj);
    }

    @Override
    public String getDtIniEffDb() {
        throw new FieldNotMappedException("dtIniEffDb");
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        throw new FieldNotMappedException("dtIniEffDb");
    }

    @Override
    public String getDtIniPerDb() {
        return ws.getImpstSostDb().getPervRichDb();
    }

    @Override
    public void setDtIniPerDb(String dtIniPerDb) {
        this.ws.getImpstSostDb().setPervRichDb(dtIniPerDb);
    }

    @Override
    public String getDtIniPerDbObj() {
        if (ws.getIndImpstSost().getDtIniPer() >= 0) {
            return getDtIniPerDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtIniPerDbObj(String dtIniPerDbObj) {
        if (dtIniPerDbObj != null) {
            setDtIniPerDb(dtIniPerDbObj);
            ws.getIndImpstSost().setDtIniPer(((short)0));
        }
        else {
            ws.getIndImpstSost().setDtIniPer(((short)-1));
        }
    }

    @Override
    public int getIdImpstSost() {
        throw new FieldNotMappedException("idImpstSost");
    }

    @Override
    public void setIdImpstSost(int idImpstSost) {
        throw new FieldNotMappedException("idImpstSost");
    }

    @Override
    public int getIdMoviChiu() {
        throw new FieldNotMappedException("idMoviChiu");
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        throw new FieldNotMappedException("idMoviChiu");
    }

    @Override
    public Integer getIdMoviChiuObj() {
        return ((Integer)getIdMoviChiu());
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        setIdMoviChiu(((int)idMoviChiuObj));
    }

    @Override
    public int getIdMoviCrz() {
        throw new FieldNotMappedException("idMoviCrz");
    }

    @Override
    public void setIdMoviCrz(int idMoviCrz) {
        throw new FieldNotMappedException("idMoviCrz");
    }

    public Idsv0003 getIdsv0003() {
        return idsv0003;
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        return idsv0003.getCodiceCompagniaAnia();
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        this.idsv0003.setCodiceCompagniaAnia(idsv0003CodiceCompagniaAnia);
    }

    @Override
    public AfDecimal getImpGiaTassato() {
        throw new FieldNotMappedException("impGiaTassato");
    }

    @Override
    public void setImpGiaTassato(AfDecimal impGiaTassato) {
        throw new FieldNotMappedException("impGiaTassato");
    }

    @Override
    public AfDecimal getImpbIs() {
        throw new FieldNotMappedException("impbIs");
    }

    @Override
    public void setImpbIs(AfDecimal impbIs) {
        throw new FieldNotMappedException("impbIs");
    }

    @Override
    public AfDecimal getImpbIsObj() {
        return getImpbIs();
    }

    @Override
    public void setImpbIsObj(AfDecimal impbIsObj) {
        setImpbIs(new AfDecimal(impbIsObj, 15, 3));
    }

    @Override
    public AfDecimal getImpstSost() {
        throw new FieldNotMappedException("impstSost");
    }

    @Override
    public void setImpstSost(AfDecimal impstSost) {
        throw new FieldNotMappedException("impstSost");
    }

    @Override
    public AfDecimal getImpstSostObj() {
        return getImpstSost();
    }

    @Override
    public void setImpstSostObj(AfDecimal impstSostObj) {
        setImpstSost(new AfDecimal(impstSostObj, 15, 3));
    }

    @Override
    public long getIsoDsRiga() {
        throw new FieldNotMappedException("isoDsRiga");
    }

    @Override
    public void setIsoDsRiga(long isoDsRiga) {
        throw new FieldNotMappedException("isoDsRiga");
    }

    @Override
    public int getIsoIdOgg() {
        throw new FieldNotMappedException("isoIdOgg");
    }

    @Override
    public void setIsoIdOgg(int isoIdOgg) {
        throw new FieldNotMappedException("isoIdOgg");
    }

    @Override
    public Integer getIsoIdOggObj() {
        return ((Integer)getIsoIdOgg());
    }

    @Override
    public void setIsoIdOggObj(Integer isoIdOggObj) {
        setIsoIdOgg(((int)isoIdOggObj));
    }

    @Override
    public String getIsoTpOgg() {
        throw new FieldNotMappedException("isoTpOgg");
    }

    @Override
    public void setIsoTpOgg(String isoTpOgg) {
        throw new FieldNotMappedException("isoTpOgg");
    }

    public Ldbv1591 getLdbv1591() {
        return ldbv1591;
    }

    @Override
    public int getLdbv1591IdAdes() {
        return ldbv1591.getIdAdes();
    }

    @Override
    public void setLdbv1591IdAdes(int ldbv1591IdAdes) {
        this.ldbv1591.setIdAdes(ldbv1591IdAdes);
    }

    @Override
    public int getLdbv2901IdAdes() {
        throw new FieldNotMappedException("ldbv2901IdAdes");
    }

    @Override
    public void setLdbv2901IdAdes(int ldbv2901IdAdes) {
        throw new FieldNotMappedException("ldbv2901IdAdes");
    }

    @Override
    public String getLdbv6191DtInfDb() {
        throw new FieldNotMappedException("ldbv6191DtInfDb");
    }

    @Override
    public void setLdbv6191DtInfDb(String ldbv6191DtInfDb) {
        throw new FieldNotMappedException("ldbv6191DtInfDb");
    }

    @Override
    public String getLdbv6191DtSupDb() {
        throw new FieldNotMappedException("ldbv6191DtSupDb");
    }

    @Override
    public void setLdbv6191DtSupDb(String ldbv6191DtSupDb) {
        throw new FieldNotMappedException("ldbv6191DtSupDb");
    }

    @Override
    public AfDecimal getMpbIs() {
        throw new FieldNotMappedException("mpbIs");
    }

    @Override
    public void setMpbIs(AfDecimal mpbIs) {
        throw new FieldNotMappedException("mpbIs");
    }

    @Override
    public AfDecimal getPrstzLrdAnteIs() {
        throw new FieldNotMappedException("prstzLrdAnteIs");
    }

    @Override
    public void setPrstzLrdAnteIs(AfDecimal prstzLrdAnteIs) {
        throw new FieldNotMappedException("prstzLrdAnteIs");
    }

    @Override
    public AfDecimal getPrstzLrdAnteIsObj() {
        return getPrstzLrdAnteIs();
    }

    @Override
    public void setPrstzLrdAnteIsObj(AfDecimal prstzLrdAnteIsObj) {
        setPrstzLrdAnteIs(new AfDecimal(prstzLrdAnteIsObj, 15, 3));
    }

    @Override
    public AfDecimal getPrstzNet() {
        throw new FieldNotMappedException("prstzNet");
    }

    @Override
    public void setPrstzNet(AfDecimal prstzNet) {
        throw new FieldNotMappedException("prstzNet");
    }

    @Override
    public AfDecimal getPrstzNetObj() {
        return getPrstzNet();
    }

    @Override
    public void setPrstzNetObj(AfDecimal prstzNetObj) {
        setPrstzNet(new AfDecimal(prstzNetObj, 15, 3));
    }

    @Override
    public AfDecimal getPrstzPrec() {
        throw new FieldNotMappedException("prstzPrec");
    }

    @Override
    public void setPrstzPrec(AfDecimal prstzPrec) {
        throw new FieldNotMappedException("prstzPrec");
    }

    @Override
    public AfDecimal getPrstzPrecObj() {
        return getPrstzPrec();
    }

    @Override
    public void setPrstzPrecObj(AfDecimal prstzPrecObj) {
        setPrstzPrec(new AfDecimal(prstzPrecObj, 15, 3));
    }

    @Override
    public AfDecimal getRisMatAnteTax() {
        throw new FieldNotMappedException("risMatAnteTax");
    }

    @Override
    public void setRisMatAnteTax(AfDecimal risMatAnteTax) {
        throw new FieldNotMappedException("risMatAnteTax");
    }

    @Override
    public AfDecimal getRisMatAnteTaxObj() {
        return getRisMatAnteTax();
    }

    @Override
    public void setRisMatAnteTaxObj(AfDecimal risMatAnteTaxObj) {
        setRisMatAnteTax(new AfDecimal(risMatAnteTaxObj, 15, 3));
    }

    @Override
    public AfDecimal getRisMatNetPrec() {
        throw new FieldNotMappedException("risMatNetPrec");
    }

    @Override
    public void setRisMatNetPrec(AfDecimal risMatNetPrec) {
        throw new FieldNotMappedException("risMatNetPrec");
    }

    @Override
    public AfDecimal getRisMatNetPrecObj() {
        return getRisMatNetPrec();
    }

    @Override
    public void setRisMatNetPrecObj(AfDecimal risMatNetPrecObj) {
        setRisMatNetPrec(new AfDecimal(risMatNetPrecObj, 15, 3));
    }

    @Override
    public AfDecimal getRisMatPostTax() {
        throw new FieldNotMappedException("risMatPostTax");
    }

    @Override
    public void setRisMatPostTax(AfDecimal risMatPostTax) {
        throw new FieldNotMappedException("risMatPostTax");
    }

    @Override
    public AfDecimal getRisMatPostTaxObj() {
        return getRisMatPostTax();
    }

    @Override
    public void setRisMatPostTaxObj(AfDecimal risMatPostTaxObj) {
        setRisMatPostTax(new AfDecimal(risMatPostTaxObj, 15, 3));
    }

    public TitContLdbs1590 getTitContLdbs1590() {
        return titContLdbs1590;
    }

    @Override
    public String getTpCalcImpst() {
        throw new FieldNotMappedException("tpCalcImpst");
    }

    @Override
    public void setTpCalcImpst(String tpCalcImpst) {
        throw new FieldNotMappedException("tpCalcImpst");
    }

    public Ldbs1590Data getWs() {
        return ws;
    }

    @Override
    public String getWsDataInizioEffettoDb() {
        return ws.getIdsv0010().getWsDataInizioEffettoDb();
    }

    @Override
    public void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb) {
        this.ws.getIdsv0010().setWsDataInizioEffettoDb(wsDataInizioEffettoDb);
    }

    @Override
    public String getWsDtInfinito1() {
        return ws.getIdsv0010().getWsDtInfinito1();
    }

    @Override
    public void setWsDtInfinito1(String wsDtInfinito1) {
        this.ws.getIdsv0010().setWsDtInfinito1(wsDtInfinito1);
    }

    @Override
    public long getWsTsCompetenza() {
        throw new FieldNotMappedException("wsTsCompetenza");
    }

    @Override
    public void setWsTsCompetenza(long wsTsCompetenza) {
        throw new FieldNotMappedException("wsTsCompetenza");
    }

    @Override
    public long getWsTsInfinito1() {
        return ws.getIdsv0010().getWsTsInfinito1();
    }

    @Override
    public void setWsTsInfinito1(long wsTsInfinito1) {
        this.ws.getIdsv0010().setWsTsInfinito1(wsTsInfinito1);
    }
}
