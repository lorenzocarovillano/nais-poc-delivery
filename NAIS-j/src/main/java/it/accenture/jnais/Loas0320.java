package it.accenture.jnais;

import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.WdtrDati;
import it.accenture.jnais.copy.WtdrDati;
import it.accenture.jnais.copy.WtgaDati;
import it.accenture.jnais.ws.AreaIdsv0001;
import it.accenture.jnais.ws.AreaMain;
import it.accenture.jnais.ws.AreaPassaggio;
import it.accenture.jnais.ws.enums.Idso0011SqlcodeSigned;
import it.accenture.jnais.ws.enums.WpolStatus;
import it.accenture.jnais.ws.Iabv0006;
import it.accenture.jnais.ws.Ieai9901Area;
import it.accenture.jnais.ws.Loas0320Data;
import it.accenture.jnais.ws.redefines.DtrDtEndCop;
import it.accenture.jnais.ws.redefines.DtrDtIniCop;
import it.accenture.jnais.ws.redefines.TdrDtApplzMora;
import it.accenture.jnais.ws.redefines.TdrDtEndCop;
import it.accenture.jnais.ws.redefines.TdrDtEsiTit;
import it.accenture.jnais.ws.redefines.TdrDtIniCop;
import it.accenture.jnais.ws.redefines.TgaDtEffStab;
import it.accenture.jnais.ws.redefines.TgaDtEmis;
import it.accenture.jnais.ws.redefines.TgaDtIniValTar;
import it.accenture.jnais.ws.redefines.TgaDtScad;
import it.accenture.jnais.ws.redefines.TgaDtUltAdegPrePr;
import it.accenture.jnais.ws.redefines.TgaDtVldtProd;
import it.accenture.jnais.ws.redefines.WdtrAcqExp;
import it.accenture.jnais.ws.redefines.WdtrCarAcq;
import it.accenture.jnais.ws.redefines.WdtrCarGest;
import it.accenture.jnais.ws.redefines.WdtrCarIas;
import it.accenture.jnais.ws.redefines.WdtrCarInc;
import it.accenture.jnais.ws.redefines.WdtrCnbtAntirac;
import it.accenture.jnais.ws.redefines.WdtrCommisInter;
import it.accenture.jnais.ws.redefines.WdtrDir;
import it.accenture.jnais.ws.redefines.WdtrDtEndCop;
import it.accenture.jnais.ws.redefines.WdtrDtIniCop;
import it.accenture.jnais.ws.redefines.WdtrFrqMovi;
import it.accenture.jnais.ws.redefines.WdtrIdMoviChiu;
import it.accenture.jnais.ws.redefines.WdtrImpAder;
import it.accenture.jnais.ws.redefines.WdtrImpAz;
import it.accenture.jnais.ws.redefines.WdtrImpTfr;
import it.accenture.jnais.ws.redefines.WdtrImpTfrStrc;
import it.accenture.jnais.ws.redefines.WdtrImpTrasfe;
import it.accenture.jnais.ws.redefines.WdtrImpVolo;
import it.accenture.jnais.ws.redefines.WdtrIntrFraz;
import it.accenture.jnais.ws.redefines.WdtrIntrMora;
import it.accenture.jnais.ws.redefines.WdtrIntrRetdt;
import it.accenture.jnais.ws.redefines.WdtrIntrRiat;
import it.accenture.jnais.ws.redefines.WdtrManfeeAntic;
import it.accenture.jnais.ws.redefines.WdtrManfeeRicor;
import it.accenture.jnais.ws.redefines.WdtrPreNet;
import it.accenture.jnais.ws.redefines.WdtrPrePpIas;
import it.accenture.jnais.ws.redefines.WdtrPreSoloRsh;
import it.accenture.jnais.ws.redefines.WdtrPreTot;
import it.accenture.jnais.ws.redefines.WdtrProvAcq1aa;
import it.accenture.jnais.ws.redefines.WdtrProvAcq2aa;
import it.accenture.jnais.ws.redefines.WdtrProvDaRec;
import it.accenture.jnais.ws.redefines.WdtrProvInc;
import it.accenture.jnais.ws.redefines.WdtrProvRicor;
import it.accenture.jnais.ws.redefines.WdtrRemunAss;
import it.accenture.jnais.ws.redefines.WdtrSoprAlt;
import it.accenture.jnais.ws.redefines.WdtrSoprProf;
import it.accenture.jnais.ws.redefines.WdtrSoprSan;
import it.accenture.jnais.ws.redefines.WdtrSoprSpo;
import it.accenture.jnais.ws.redefines.WdtrSoprTec;
import it.accenture.jnais.ws.redefines.WdtrSpeAge;
import it.accenture.jnais.ws.redefines.WdtrSpeMed;
import it.accenture.jnais.ws.redefines.WdtrTax;
import it.accenture.jnais.ws.redefines.WdtrTotIntrPrest;
import it.accenture.jnais.ws.redefines.WtdrDtApplzMora;
import it.accenture.jnais.ws.redefines.WtdrDtEndCop;
import it.accenture.jnais.ws.redefines.WtdrDtEsiTit;
import it.accenture.jnais.ws.redefines.WtdrDtIniCop;
import it.accenture.jnais.ws.redefines.WtdrFraz;
import it.accenture.jnais.ws.redefines.WtdrIdMoviChiu;
import it.accenture.jnais.ws.redefines.WtdrIdRappAna;
import it.accenture.jnais.ws.redefines.WtdrIdRappRete;
import it.accenture.jnais.ws.redefines.WtdrImpAder;
import it.accenture.jnais.ws.redefines.WtdrImpAz;
import it.accenture.jnais.ws.redefines.WtdrImpPag;
import it.accenture.jnais.ws.redefines.WtdrImpTfr;
import it.accenture.jnais.ws.redefines.WtdrImpTfrStrc;
import it.accenture.jnais.ws.redefines.WtdrImpTrasfe;
import it.accenture.jnais.ws.redefines.WtdrImpVolo;
import it.accenture.jnais.ws.redefines.WtdrProgTit;
import it.accenture.jnais.ws.redefines.WtdrTotAcqExp;
import it.accenture.jnais.ws.redefines.WtdrTotCarAcq;
import it.accenture.jnais.ws.redefines.WtdrTotCarGest;
import it.accenture.jnais.ws.redefines.WtdrTotCarIas;
import it.accenture.jnais.ws.redefines.WtdrTotCarInc;
import it.accenture.jnais.ws.redefines.WtdrTotCnbtAntirac;
import it.accenture.jnais.ws.redefines.WtdrTotCommisInter;
import it.accenture.jnais.ws.redefines.WtdrTotDir;
import it.accenture.jnais.ws.redefines.WtdrTotIntrFraz;
import it.accenture.jnais.ws.redefines.WtdrTotIntrMora;
import it.accenture.jnais.ws.redefines.WtdrTotIntrPrest;
import it.accenture.jnais.ws.redefines.WtdrTotIntrRetdt;
import it.accenture.jnais.ws.redefines.WtdrTotIntrRiat;
import it.accenture.jnais.ws.redefines.WtdrTotManfeeAntic;
import it.accenture.jnais.ws.redefines.WtdrTotManfeeRec;
import it.accenture.jnais.ws.redefines.WtdrTotManfeeRicor;
import it.accenture.jnais.ws.redefines.WtdrTotPreNet;
import it.accenture.jnais.ws.redefines.WtdrTotPrePpIas;
import it.accenture.jnais.ws.redefines.WtdrTotPreSoloRsh;
import it.accenture.jnais.ws.redefines.WtdrTotPreTot;
import it.accenture.jnais.ws.redefines.WtdrTotProvAcq1aa;
import it.accenture.jnais.ws.redefines.WtdrTotProvAcq2aa;
import it.accenture.jnais.ws.redefines.WtdrTotProvDaRec;
import it.accenture.jnais.ws.redefines.WtdrTotProvInc;
import it.accenture.jnais.ws.redefines.WtdrTotProvRicor;
import it.accenture.jnais.ws.redefines.WtdrTotRemunAss;
import it.accenture.jnais.ws.redefines.WtdrTotSoprAlt;
import it.accenture.jnais.ws.redefines.WtdrTotSoprProf;
import it.accenture.jnais.ws.redefines.WtdrTotSoprSan;
import it.accenture.jnais.ws.redefines.WtdrTotSoprSpo;
import it.accenture.jnais.ws.redefines.WtdrTotSoprTec;
import it.accenture.jnais.ws.redefines.WtdrTotSpeAge;
import it.accenture.jnais.ws.redefines.WtdrTotSpeMed;
import it.accenture.jnais.ws.redefines.WtdrTotTax;
import it.accenture.jnais.ws.WgrzAreaGaranziaLccs0005;
import it.accenture.jnais.ws.WtgaAreaTrancheLoas0800;

/**Original name: LOAS0320<br>
 * <pre>  ============================================================= *
 *                                                                 *
 *         PORTAFOGLIO VITA ITALIA  VER 1.0                        *
 *                                                                 *
 *   ============================================================= *
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2007.
 * DATE-COMPILED.
 * ----------------------------------------------------------------*
 * ----------------------------------------------------------------*
 *      PROGRAMMA ..... LOAS0320                                   *
 *      TIPOLOGIA...... DRIVER EOC                                 *
 *      PROCESSO....... XXX                                        *
 *      FUNZIONE....... XXX                                        *
 *      DESCRIZIONE.... RIVALUTAZIONI - EOC                        *
 * ----------------------------------------------------------------*</pre>*/
public class Loas0320 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Loas0320Data ws = new Loas0320Data();
    //Original name: AREA-IDSV0001
    private AreaIdsv0001 areaIdsv0001;
    //Original name: WGRZ-AREA-GARANZIA
    private WgrzAreaGaranziaLccs0005 wgrzAreaGaranzia;
    //Original name: WTGA-AREA-TRANCHE
    private WtgaAreaTrancheLoas0800 wtgaAreaTranche;
    //Original name: AREA-MAIN
    private AreaMain areaMain;
    //Original name: WCOM-IO-STATI
    private AreaPassaggio wcomIoStati;
    //Original name: IABV0006
    private Iabv0006 iabv0006;

    //==== METHODS ====
    /**Original name: PROGRAM_LOAS0320_FIRST_SENTENCES<br>*/
    public long execute(AreaIdsv0001 areaIdsv0001, WgrzAreaGaranziaLccs0005 wgrzAreaGaranzia, WtgaAreaTrancheLoas0800 wtgaAreaTranche, AreaMain areaMain, AreaPassaggio wcomIoStati, Iabv0006 iabv0006) {
        this.areaIdsv0001 = areaIdsv0001;
        this.wgrzAreaGaranzia = wgrzAreaGaranzia;
        this.wtgaAreaTranche = wtgaAreaTranche;
        this.areaMain = areaMain;
        this.wcomIoStati = wcomIoStati;
        this.iabv0006 = iabv0006;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU S0000-OPERAZIONI-INIZIALI-EX.
        s0000OperazioniIniziali();
        //
        // COB_CODE: PERFORM S1000-ELABORAZIONE
        //              THRU S1000-ELABORAZIONE-EX.
        s1000Elaborazione();
        //
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU S9000-OPERAZIONI-FINALI-EX.
        //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LOAS0320.cbl:line=254, because the code is unreachable.
        //
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Loas0320 getInstance() {
        return ((Loas0320)Programs.getInstance(Loas0320.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *                       OPERAZIONI INIZIALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: MOVE 'S0000-OPERAZIONI-INIZIALI'
        //             TO WK-LABEL-ERR.
        ws.setWkLabelErr("S0000-OPERAZIONI-INIZIALI");
        //
        // --> Inizializzazione Area E Indice Titolo Di Rata
        //
        // COB_CODE: PERFORM S0100-INIZIA-AREA-TDR
        //              THRU S0100-INIZIA-AREA-TDR-EX.
        s0100IniziaAreaTdr();
        //
        // --> Inizializzazione Area E Indice Dettaglio Titolo Di Rata
        //
        // COB_CODE: PERFORM S0200-INIZIA-AREA-DTR
        //              THRU S0200-INIZIA-AREA-DTR-EX.
        s0200IniziaAreaDtr();
        //
        // COB_CODE: MOVE ZEROES
        //             TO WK-ID-TIT-RAT.
        ws.setWkIdTitRat(0);
    }

    /**Original name: S0100-INIZIA-AREA-TDR<br>
	 * <pre>----------------------------------------------------------------*
	 *       INIZIALIZZAZIONE AREA E INDICE TITOLO DI RATA
	 * ----------------------------------------------------------------*
	 *  --> Inizializzazione Titolo Di Rata</pre>*/
    private void s0100IniziaAreaTdr() {
        // COB_CODE: PERFORM INIZIA-TOT-TDR
        //              THRU INIZIA-TOT-TDR-EX
        //           VARYING IX-TAB-TDR FROM 1 BY 1
        //             UNTIL IX-TAB-TDR > WK-TDR-MAX-A
        ws.getIxIndici().setTdr(((short)1));
        while (!(ws.getIxIndici().getTdr() > ws.getWkTdrMaxA())) {
            iniziaTotTdr();
            ws.getIxIndici().setTdr(Trunc.toShort(ws.getIxIndici().getTdr() + 1, 4));
        }
        //
        // COB_CODE: MOVE ZEROES
        //             TO WTDR-ELE-TDR-MAX
        //                IX-TAB-TDR.
        ws.setWtdrEleTdrMax(((short)0));
        ws.getIxIndici().setTdr(((short)0));
    }

    /**Original name: S0200-INIZIA-AREA-DTR<br>
	 * <pre>----------------------------------------------------------------*
	 *       INIZIALIZZAZIONE AREA E INDICE DETTAGLIO TITOLO DI RATA
	 * ----------------------------------------------------------------*
	 *  --> Inizializzazione Titolo Di Rata</pre>*/
    private void s0200IniziaAreaDtr() {
        // COB_CODE: PERFORM INIZIA-TOT-DTR
        //              THRU INIZIA-TOT-DTR-EX
        //           VARYING IX-TAB-DTR FROM 1 BY 1
        //             UNTIL IX-TAB-DTR > WK-DTR-MAX-A
        ws.getIxIndici().setDtr(((short)1));
        while (!(ws.getIxIndici().getDtr() > ws.getWkDtrMaxA())) {
            iniziaTotDtr();
            ws.getIxIndici().setDtr(Trunc.toShort(ws.getIxIndici().getDtr() + 1, 4));
        }
        //
        // COB_CODE: MOVE ZEROES
        //             TO WDTR-ELE-DTR-MAX
        //                IX-TAB-DTR.
        ws.setWdtrEleDtrMax(((short)0));
        ws.getIxIndici().setDtr(((short)0));
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *                          ELABORAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                      THRU GESTIONE-EOC-COMUNE-EX
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE: PERFORM GESTIONE-EOC-COMUNE
            //              THRU GESTIONE-EOC-COMUNE-EX
            gestioneEocComune();
            //
        }
        //
        // COB_CODE: PERFORM AGGIORNA-TRCH-DI-GAR
        //              THRU AGGIORNA-TRCH-DI-GAR-EX
        //           VARYING IX-TAB-TGA FROM 1 BY 1
        //             UNTIL IX-TAB-TGA > WTGA-ELE-TRAN-MAX
        //                OR IDSV0001-ESITO-KO.
        ws.getIxIndici().setTga(((short)1));
        while (!(ws.getIxIndici().getTga() > wtgaAreaTranche.getEleTranMax() || areaIdsv0001.getEsito().isIdsv0001EsitoKo())) {
            aggiornaTrchDiGar();
            ws.getIxIndici().setTga(Trunc.toShort(ws.getIxIndici().getTga() + 1, 4));
        }
        //
        // COB_CODE: PERFORM S1100-INVALIDA-DTR
        //              THRU S1100-INVALIDA-DTR-EX
        //           VARYING IX-TAB-TGA FROM 1 BY 1
        //             UNTIL IX-TAB-TGA > WTGA-ELE-TRAN-MAX
        //                OR IDSV0001-ESITO-KO.
        ws.getIxIndici().setTga(((short)1));
        while (!(ws.getIxIndici().getTga() > wtgaAreaTranche.getEleTranMax() || areaIdsv0001.getEsito().isIdsv0001EsitoKo())) {
            s1100InvalidaDtr();
            ws.getIxIndici().setTga(Trunc.toShort(ws.getIxIndici().getTga() + 1, 4));
        }
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                   END-IF
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE:         IF WK-ID-TIT-RAT NOT = ZEROES
            //           *
            //                      END-IF
            //           *
            //                   END-IF
            if (ws.getWkIdTitRat() != 0) {
                //
                // COB_CODE:            IF  WPOL-TP-FRM-ASSVA = 'IN'       OR
                //                         (WPOL-TP-FRM-ASSVA = 'CO'       AND
                //                          WPOL-TP-LIV-GENZ-TIT = 'AD')
                //           *
                //                             THRU S1200-INVALIDA-TDR-EX
                //           *
                //                      END-IF
                if (Conditions.eq(areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolTpFrmAssva(), "IN") || Conditions.eq(areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolTpFrmAssva(), "CO") && Conditions.eq(areaMain.getWpolAreaPolizza().getLccvpol1().getDati().getWpolTpLivGenzTit(), "AD")) {
                    //
                    // COB_CODE: PERFORM S1200-INVALIDA-TDR
                    //              THRU S1200-INVALIDA-TDR-EX
                    s1200InvalidaTdr();
                    //
                }
                //
            }
            //
        }
    }

    /**Original name: S1100-INVALIDA-DTR<br>
	 * <pre>----------------------------------------------------------------*
	 *    INVALIDAZIONE DETTAGLI TITOLI DI RATA
	 * ----------------------------------------------------------------*</pre>*/
    private void s1100InvalidaDtr() {
        // COB_CODE: PERFORM S1110-IMPOSTA-DTR
        //              THRU S1110-IMPOSTA-DTR-EX.
        s1110ImpostaDtr();
        //
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX.
        callDispatcher();
        //
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //           *
        //                   END-EVALUATE
        //           *
        //                ELSE
        //           *
        //           *  --> Errore dispatcher
        //           *
        //                      THRU EX-S0300
        //           *
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            //
            //
            // COB_CODE:         EVALUATE TRUE
            //           *
            //                       WHEN IDSO0011-NOT-FOUND
            //           *
            //           *  --> Chiave non trovata
            //           *
            //                            CONTINUE
            //           *
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //           *
            //           *  --> Operazione eseguita correttamente
            //           *
            //                               THRU AGGIORNA-DETT-TIT-DI-RAT-EX
            //           *
            //                       WHEN OTHER
            //           *
            //           *  --> Errore di accesso al db
            //           *
            //                               THRU EX-S0300
            //           *
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.NOT_FOUND://
                //  --> Chiave non trovata
                //
                // COB_CODE: CONTINUE
                //continue
                //
                    break;

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://
                    //  --> Operazione eseguita correttamente
                    //
                    // COB_CODE: MOVE 1
                    //             TO IX-TAB-DTR
                    //                WDTR-ELE-DTR-MAX
                    ws.getIxIndici().setDtr(((short)1));
                    ws.setWdtrEleDtrMax(((short)1));
                    // COB_CODE: MOVE IDSO0011-BUFFER-DATI
                    //             TO DETT-TIT-DI-RAT
                    ws.getDettTitDiRat().setDettTitDiRatFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    // COB_CODE: PERFORM VALORIZZA-OUTPUT-DTR
                    //              THRU VALORIZZA-OUTPUT-DTR-EX
                    valorizzaOutputDtr();
                    // COB_CODE: SET WDTR-ST-MOD(1)
                    //             TO TRUE
                    ws.getWdtrTabDettTira(1).getLccvdtr1().getStatus().setMod();
                    //
                    // COB_CODE: MOVE 'N'
                    //             TO WDTR-FL-VLDT-TIT(1)
                    ws.getWdtrTabDettTira(1).getLccvdtr1().getDati().setWdtrFlVldtTitFormatted("N");
                    //
                    // COB_CODE: MOVE WDTR-ID-TIT-RAT(1)
                    //             TO WK-ID-TIT-RAT
                    ws.setWkIdTitRat(ws.getWdtrTabDettTira(1).getLccvdtr1().getDati().getWdtrIdTitRat());
                    //
                    // COB_CODE: PERFORM AGGIORNA-DETT-TIT-DI-RAT
                    //              THRU AGGIORNA-DETT-TIT-DI-RAT-EX
                    aggiornaDettTitDiRat();
                    //
                    break;

                default://
                    //  --> Errore di accesso al db
                    //
                    // COB_CODE: MOVE WK-PGM
                    //             TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'S1100-INVALIDA-DTR'
                    //             TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("S1100-INVALIDA-DTR");
                    // COB_CODE: MOVE '001114'
                    //             TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("001114");
                    // COB_CODE: MOVE 'ERRORE LETTURA DTR'
                    //             TO IEAI9901-PARAMETRI-ERR
                    ws.getIeai9901Area().setParametriErr("ERRORE LETTURA DTR");
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    //
                    break;
            }
            //
        }
        else {
            //
            //  --> Errore dispatcher
            //
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S1100-INVALIDA-DTR'
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S1100-INVALIDA-DTR");
            // COB_CODE: MOVE '005016'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: MOVE 'ERRORE DISPATCHER LETTURA DTR'
            //             TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("ERRORE DISPATCHER LETTURA DTR");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
            //
        }
        //
        // COB_CODE: PERFORM S0200-INIZIA-AREA-DTR
        //              THRU S0200-INIZIA-AREA-DTR-EX.
        s0200IniziaAreaDtr();
        //
        // COB_CODE: PERFORM S1110-IMPOSTA-DTR
        //              THRU S1110-IMPOSTA-DTR-EX.
        s1110ImpostaDtr();
    }

    /**Original name: S1110-IMPOSTA-DTR<br>
	 * <pre>----------------------------------------------------------------*
	 *  VALORIZZAZIONE PER LETTURA DETTAGLIO TITOLO DI RATA
	 * ----------------------------------------------------------------*</pre>*/
    private void s1110ImpostaDtr() {
        // COB_CODE: INITIALIZE DETT-TIT-DI-RAT.
        initDettTitDiRat();
        //
        // COB_CODE: MOVE WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA)
        //             TO DTR-ID-OGG.
        ws.getDettTitDiRat().setDtrIdOgg(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaIdTrchDiGar());
        //
        // COB_CODE: MOVE 'TG'
        //             TO DTR-TP-OGG.
        ws.getDettTitDiRat().setDtrTpOgg("TG");
        //
        //  --> La data effetto viene sempre valorizzata
        //
        // COB_CODE: MOVE ZERO
        //             TO IDSI0011-DATA-INIZIO-EFFETTO
        //                IDSI0011-DATA-FINE-EFFETTO
        //                IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        //
        //  --> Nome tabella fisica db
        //
        // COB_CODE: MOVE 'DETT-TIT-DI-RAT'
        //             TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("DETT-TIT-DI-RAT");
        //
        //  --> Dclgen tabella
        //
        // COB_CODE: MOVE DETT-TIT-DI-RAT
        //             TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getDettTitDiRat().getDettTitDiRatFormatted());
        //
        //  --> Tipo operazione
        //
        // COB_CODE: SET IDSI0011-SELECT
        //             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        //
        //  --> Tipo livello
        //
        // COB_CODE: SET IDSI0011-ID-OGGETTO
        //             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setIdsi0011IdOggetto();
        //
        //  --> Modalita di accesso
        //
        // COB_CODE: SET IDSI0011-TRATT-DEFAULT
        //             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setIdsi0011TrattDefault();
        //
        //  --> Inizializzazione Return Code
        //
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC
        //             TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL
        //             TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
    }

    /**Original name: S1200-INVALIDA-TDR<br>
	 * <pre>----------------------------------------------------------------*
	 *    INVALIDAZIONE TITOLO DI RATA
	 * ----------------------------------------------------------------*</pre>*/
    private void s1200InvalidaTdr() {
        // COB_CODE: PERFORM S1210-IMPOSTA-TDR
        //              THRU S1210-IMPOSTA-TDR-EX.
        s1210ImpostaTdr();
        //
        // COB_CODE: PERFORM S1220-DISPATCHER-TDR
        //              THRU S1220-DISPATCHER-TDR-EX.
        s1220DispatcherTdr();
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                        OR IDSV0001-ESITO-KO
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE: PERFORM S1230-IMPOSTA-LDBV3461
            //              THRU S1230-IMPOSTA-LDBV3461-EX
            s1230ImpostaLdbv3461();
            //
            // COB_CODE: SET WK-FINE-DTR-NO
            //             TO TRUE
            ws.getWkFineDtr().setNo();
            // COB_CODE: SET IDSI0011-FETCH-FIRST
            //             TO TRUE
            ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setFetchFirst();
            //
            // COB_CODE: PERFORM S1240-AGGIORNA-DTR-COLLEG
            //              THRU S1240-AGGIORNA-DTR-COLLEG-EX
            //             UNTIL WK-FINE-DTR-SI
            //                OR IDSV0001-ESITO-KO
            while (!(ws.getWkFineDtr().isSi() || areaIdsv0001.getEsito().isIdsv0001EsitoKo())) {
                s1240AggiornaDtrColleg();
            }
            //
        }
    }

    /**Original name: S1220-DISPATCHER-TDR<br>
	 * <pre>----------------------------------------------------------------*
	 *  LETTURA/AGGIORNAMENTO TITOLO DI RATA
	 * ----------------------------------------------------------------*</pre>*/
    private void s1220DispatcherTdr() {
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX.
        callDispatcher();
        //
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //           *
        //                   END-EVALUATE
        //           *
        //                ELSE
        //           *
        //           *  --> Errore dispatcher
        //           *
        //                      THRU EX-S0300
        //           *
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            //
            //
            // COB_CODE:         EVALUATE TRUE
            //           *
            //                       WHEN IDSO0011-NOT-FOUND
            //           *
            //           *  --> Chiave non trovata
            //           *
            //                            CONTINUE
            //           *
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //           *
            //           *  --> Operazione eseguita correttamente
            //           *
            //                               THRU AGGIORNA-TIT-RAT-EX
            //           *
            //                       WHEN OTHER
            //           *
            //           *  --> Errore di accesso al db
            //           *
            //                               THRU EX-S0300
            //           *
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.NOT_FOUND://
                //  --> Chiave non trovata
                //
                // COB_CODE: CONTINUE
                //continue
                //
                    break;

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://
                    //  --> Operazione eseguita correttamente
                    //
                    // COB_CODE: MOVE 1
                    //             TO IX-TAB-TDR
                    //                WTDR-ELE-TDR-MAX
                    ws.getIxIndici().setTdr(((short)1));
                    ws.setWtdrEleTdrMax(((short)1));
                    // COB_CODE: MOVE IDSO0011-BUFFER-DATI
                    //             TO TIT-RAT
                    ws.getTitRat().setTitRatFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    // COB_CODE: PERFORM VALORIZZA-OUTPUT-TDR
                    //              THRU VALORIZZA-OUTPUT-TDR-EX
                    valorizzaOutputTdr();
                    // COB_CODE: SET WTDR-ST-MOD(1)
                    //             TO TRUE
                    ws.getWtdrTabTitRat(1).getLccvtdr1().getStatus().setMod();
                    //
                    // COB_CODE: MOVE 'N'
                    //             TO WTDR-FL-VLDT-TIT(1)
                    ws.getWtdrTabTitRat(1).getLccvtdr1().getDati().setWtdrFlVldtTitFormatted("N");
                    //
                    // COB_CODE: PERFORM AGGIORNA-TIT-RAT
                    //              THRU AGGIORNA-TIT-RAT-EX
                    aggiornaTitRat();
                    //
                    break;

                default://
                    //  --> Errore di accesso al db
                    //
                    // COB_CODE: MOVE WK-PGM
                    //             TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'S1200-INVALIDA-TDR'
                    //             TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("S1200-INVALIDA-TDR");
                    // COB_CODE: MOVE '001114'
                    //             TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("001114");
                    // COB_CODE: MOVE 'ERRORE LETTURA TDR'
                    //             TO IEAI9901-PARAMETRI-ERR
                    ws.getIeai9901Area().setParametriErr("ERRORE LETTURA TDR");
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    //
                    break;
            }
            //
        }
        else {
            //
            //  --> Errore dispatcher
            //
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S1200-INVALIDA-TDR'
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S1200-INVALIDA-TDR");
            // COB_CODE: MOVE '005016'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: MOVE 'ERRORE DISPATCHER LETTURA TDR'
            //             TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("ERRORE DISPATCHER LETTURA TDR");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
            //
        }
        //
        // COB_CODE: PERFORM S0100-INIZIA-AREA-TDR
        //              THRU S0100-INIZIA-AREA-TDR-EX.
        s0100IniziaAreaTdr();
    }

    /**Original name: S1240-AGGIORNA-DTR-COLLEG<br>
	 * <pre>----------------------------------------------------------------*
	 *  AGGIORNAMENTO DI EVENTUALI ALTRI DETTAGLI LEGATI AL TITOLO
	 * ----------------------------------------------------------------*</pre>*/
    private void s1240AggiornaDtrColleg() {
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX.
        callDispatcher();
        //
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //           *
        //                   END-EVALUATE
        //           *
        //                ELSE
        //           *
        //           *  --> Errore dispatcher
        //           *
        //                      THRU EX-S0300
        //           *
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            //
            //
            // COB_CODE:         EVALUATE TRUE
            //           *
            //                       WHEN IDSO0011-NOT-FOUND
            //           *
            //           *  --> Chiave non trovata
            //           *
            //                              TO TRUE
            //           *
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //           *
            //           *  --> Operazione eseguita correttamente
            //           *
            //                              TO TRUE
            //           *
            //                       WHEN OTHER
            //           *
            //           *  --> Errore di accesso al db
            //           *
            //                               THRU EX-S0300
            //           *
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.NOT_FOUND://
                    //  --> Chiave non trovata
                    //
                    // COB_CODE: SET WK-FINE-DTR-SI
                    //             TO TRUE
                    ws.getWkFineDtr().setSi();
                    //
                    break;

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://
                    //  --> Operazione eseguita correttamente
                    //
                    // COB_CODE: MOVE 1
                    //             TO IX-TAB-DTR
                    //                WDTR-ELE-DTR-MAX
                    ws.getIxIndici().setDtr(((short)1));
                    ws.setWdtrEleDtrMax(((short)1));
                    // COB_CODE: MOVE IDSO0011-BUFFER-DATI
                    //             TO DETT-TIT-DI-RAT
                    ws.getDettTitDiRat().setDettTitDiRatFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    // COB_CODE: PERFORM VALORIZZA-OUTPUT-DTR
                    //              THRU VALORIZZA-OUTPUT-DTR-EX
                    valorizzaOutputDtr();
                    // COB_CODE: SET WDTR-ST-MOD(1)
                    //             TO TRUE
                    ws.getWdtrTabDettTira(1).getLccvdtr1().getStatus().setMod();
                    //
                    // COB_CODE: MOVE 'N'
                    //             TO WDTR-FL-VLDT-TIT(1)
                    ws.getWdtrTabDettTira(1).getLccvdtr1().getDati().setWdtrFlVldtTitFormatted("N");
                    //
                    // COB_CODE: MOVE WDTR-ID-TIT-RAT(1)
                    //             TO WK-ID-TIT-RAT
                    ws.setWkIdTitRat(ws.getWdtrTabDettTira(1).getLccvdtr1().getDati().getWdtrIdTitRat());
                    //
                    // COB_CODE: PERFORM AGGIORNA-DETT-TIT-DI-RAT
                    //              THRU AGGIORNA-DETT-TIT-DI-RAT-EX
                    aggiornaDettTitDiRat();
                    //
                    // COB_CODE: SET IDSI0011-FETCH-NEXT
                    //             TO TRUE
                    ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setFetchNext();
                    //
                    break;

                default://
                    //  --> Errore di accesso al db
                    //
                    // COB_CODE: MOVE WK-PGM
                    //             TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'S1240-AGGIORNA-DTR-COLLEG'
                    //             TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("S1240-AGGIORNA-DTR-COLLEG");
                    // COB_CODE: MOVE '001114'
                    //             TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("001114");
                    // COB_CODE: MOVE 'ERRORE LETTURA DTR'
                    //             TO IEAI9901-PARAMETRI-ERR
                    ws.getIeai9901Area().setParametriErr("ERRORE LETTURA DTR");
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    //
                    break;
            }
            //
        }
        else {
            //
            //  --> Errore dispatcher
            //
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S1240-AGGIORNA-DTR-COLLEG'
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S1240-AGGIORNA-DTR-COLLEG");
            // COB_CODE: MOVE '005016'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: MOVE 'ERRORE DISPATCHER LETTURA DTR'
            //             TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("ERRORE DISPATCHER LETTURA DTR");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
            //
        }
        //
        // COB_CODE: PERFORM S0200-INIZIA-AREA-DTR
        //              THRU S0200-INIZIA-AREA-DTR-EX.
        s0200IniziaAreaDtr();
        //
        // COB_CODE: PERFORM S1230-IMPOSTA-LDBV3461
        //              THRU S1230-IMPOSTA-LDBV3461-EX.
        s1230ImpostaLdbv3461();
    }

    /**Original name: S1230-IMPOSTA-LDBV3461<br>
	 * <pre>----------------------------------------------------------------*
	 *  VALORIZZAZIONE PER LETTURA DETTAGLIO TITOLO DI RATA (AD HOC)
	 * ----------------------------------------------------------------*</pre>*/
    private void s1230ImpostaLdbv3461() {
        // COB_CODE: MOVE WK-ID-TIT-RAT
        //             TO DTR-ID-TIT-RAT.
        ws.getDettTitDiRat().setDtrIdTitRat(ws.getWkIdTitRat());
        //      TO LDBV3461-ID-TIT-RAT.
        //
        // COB_CODE: MOVE 'S'
        //             TO DTR-FL-VLDT-TIT.
        ws.getDettTitDiRat().setDtrFlVldtTitFormatted("S");
        //      TO LDBV3461-FL-VLDT-TIT
        //
        //  --> La data effetto viene sempre valorizzata
        //
        // COB_CODE: MOVE ZERO
        //            TO IDSI0011-DATA-INIZIO-EFFETTO
        //               IDSI0011-DATA-FINE-EFFETTO
        //               IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        //
        // COB_CODE: SET IDSI0011-TRATT-DEFAULT  TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setIdsi0011TrattDefault();
        //
        //  --> Nome tabella fisica db
        //
        // COB_CODE: MOVE 'LDBS3460'
        //             TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("LDBS3460");
        //
        //  --> Dclgen tabella
        //
        // COB_CODE: MOVE SPACES
        //             TO IDSI0011-BUFFER-WHERE-COND.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferWhereCond("");
        // COB_CODE: MOVE DETT-TIT-DI-RAT
        //             TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getDettTitDiRat().getDettTitDiRatFormatted());
        //
        //  --> Tipo operazione
        //
        // COB_CODE: SET IDSI0011-TRATT-DEFAULT
        //             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setIdsi0011TrattDefault();
        // COB_CODE: SET IDSI0011-WHERE-CONDITION
        //             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setWhereCondition();
        //
        //  --> Modalita di accesso
        //
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC
        //             TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL
        //             TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
    }

    /**Original name: S1210-IMPOSTA-TDR<br>
	 * <pre>----------------------------------------------------------------*
	 *  VALORIZZAZIONE PER LETTURA TITOLO DI RATA
	 * ----------------------------------------------------------------*</pre>*/
    private void s1210ImpostaTdr() {
        // COB_CODE: INITIALIZE DETT-TIT-DI-RAT.
        initDettTitDiRat();
        //
        // COB_CODE: MOVE WK-ID-TIT-RAT
        //             TO TDR-ID-TIT-RAT.
        ws.getTitRat().setTdrIdTitRat(ws.getWkIdTitRat());
        //
        //  --> La data effetto viene sempre valorizzata
        //
        // COB_CODE: MOVE ZERO
        //             TO IDSI0011-DATA-INIZIO-EFFETTO
        //                IDSI0011-DATA-FINE-EFFETTO
        //                IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        //
        //  --> Nome tabella fisica db
        //
        // COB_CODE: MOVE 'TIT-RAT'
        //             TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("TIT-RAT");
        //
        //  --> Dclgen tabella
        //
        // COB_CODE: MOVE TIT-RAT
        //             TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getTitRat().getTitRatFormatted());
        //
        //  --> Tipo operazione
        //
        // COB_CODE: SET IDSI0011-SELECT
        //             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        //
        //  --> Tipo livello
        //
        // COB_CODE: SET IDSI0011-ID
        //             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setIdsi0011Id();
        //
        //  --> Modalita di accesso
        //
        // COB_CODE: SET IDSI0011-TRATT-DEFAULT
        //             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setIdsi0011TrattDefault();
        //
        //  --> Inizializzazione Return Code
        //
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC
        //             TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL
        //             TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
    }

    /**Original name: GESTIONE-EOC-COMUNE<br>
	 * <pre>----------------------------------------------------------------*
	 *         GESTIONE DELLA CHIAMATA AL SERVIZIO DI EOC COMUNE
	 * ----------------------------------------------------------------*</pre>*/
    private void gestioneEocComune() {
        // COB_CODE: PERFORM PREPARA-AREA-LOAS0110
        //              THRU PREPARA-AREA-LOAS0110-EX.
        preparaAreaLoas0110();
        //
        // COB_CODE: PERFORM CALL-EOC-COMUNE
        //              THRU CALL-EOC-COMUNE-EX.
        callEocComune();
    }

    /**Original name: PREPARA-AREA-LOAS0110<br>
	 * <pre>----------------------------------------------------------------*
	 *          VALORIZZAZIONE INPUT DEL SERVIZIO DI EOC COMUNE
	 * ----------------------------------------------------------------*</pre>*/
    private void preparaAreaLoas0110() {
        // COB_CODE: MOVE 1                          TO WCOM-STEP-ELAB.
        wcomIoStati.getLccc0261().getDatiMbs().setWcomStepElabFormatted("1");
    }

    /**Original name: CALL-EOC-COMUNE<br>
	 * <pre>----------------------------------------------------------------*
	 *                   CALL AL SERVIZIO DI EOC COMUNE
	 * ----------------------------------------------------------------*</pre>*/
    private void callEocComune() {
        Loas0110 loas0110 = null;
        // COB_CODE:      CALL LOAS0110    USING AREA-IDSV0001
        //                                       WADE-AREA-ADESIONE
        //                                       WGRZ-AREA-GARANZIA
        //                                       WMOV-AREA-MOVIMENTO
        //                                       WPMO-AREA-PARAM-MOV
        //                                       WPOL-AREA-POLIZZA
        //                                       WRIC-AREA-RICHIESTA
        //                                       WTGA-AREA-TRANCHE
        //                                       WCOM-IO-STATI
        //                                       IABV0006
        //           *
        //                ON EXCEPTION
        //           *
        //                         THRU GESTIONE-ERR-SIST-EX
        //           *
        //                END-CALL.
        try {
            loas0110 = Loas0110.getInstance();
            loas0110.run(new Object[] {areaIdsv0001, areaMain.getWadeAreaAdesione(), wgrzAreaGaranzia, areaMain.getWmovAreaMovimento(), areaMain, areaMain.getWpolAreaPolizza(), ws.getWricAreaRichiesta(), wtgaAreaTranche, wcomIoStati, iabv0006});
        }
        catch (ProgramExecutionException __ex) {
            //
            // COB_CODE: MOVE 'LOAS0110'             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe("LOAS0110");
            // COB_CODE: MOVE 'SERVIZIO EOC COMUNE'  TO CALL-DESC
            ws.getIdsv0002().setCallDesc("SERVIZIO EOC COMUNE");
            // COB_CODE: MOVE 'CALL-EOC-COMUNE'      TO WK-LABEL-ERR
            ws.setWkLabelErr("CALL-EOC-COMUNE");
            //
            // COB_CODE: PERFORM GESTIONE-ERR-SIST
            //              THRU GESTIONE-ERR-SIST-EX
            gestioneErrSist();
            //
        }
    }

    /**Original name: CALL-LCCS0234<br>
	 * <pre>----------------------------------------------------------------*
	 *           CALL SERVIZIO ESTRAZIONE OGGETTO PTF LCCS0234
	 * ----------------------------------------------------------------*</pre>*/
    private void callLccs0234() {
        Lccs0234 lccs0234 = null;
        // COB_CODE:      CALL LCCS0234  USING AREA-IDSV0001
        //                                     WPOL-AREA-POLIZZA
        //                                     WADE-AREA-ADESIONE
        //                                     WGRZ-AREA-GARANZIA
        //                                     WTGA-AREA-TRANCHE
        //                                     S234-DATI-INPUT
        //                                     S234-DATI-OUTPUT
        //                ON EXCEPTION
        //           *
        //                          THRU GESTIONE-ERR-SIST-EX
        //           *
        //                END-CALL.
        try {
            lccs0234 = Lccs0234.getInstance();
            lccs0234.run(new Object[] {areaIdsv0001, areaMain.getWpolAreaPolizza(), areaMain.getWadeAreaAdesione(), wgrzAreaGaranzia, wtgaAreaTranche, ws.getS234DatiInput(), ws.getS234DatiOutput()});
        }
        catch (ProgramExecutionException __ex) {
            //
            // COB_CODE: MOVE 'LCCS0234'
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe("LCCS0234");
            // COB_CODE: MOVE 'IL REPERIMENTO ID PORTAFOGLIO'
            //             TO CALL-DESC
            ws.getIdsv0002().setCallDesc("IL REPERIMENTO ID PORTAFOGLIO");
            // COB_CODE: MOVE 'CALL-LCCS0234'
            //             TO WK-LABEL-ERR
            ws.setWkLabelErr("CALL-LCCS0234");
            //
            // COB_CODE: PERFORM GESTIONE-ERR-SIST
            //              THRU GESTIONE-ERR-SIST-EX
            gestioneErrSist();
            //
        }
    }

    /**Original name: GESTIONE-ERR-SIST<br>
	 * <pre>----------------------------------------------------------------*
	 *            GESTIONE STANDARD DELL'ERRORE DI SISTEMA
	 * ----------------------------------------------------------------*</pre>*/
    private void gestioneErrSist() {
        // COB_CODE: SET WCOM-WRITE-ERR          TO TRUE.
        wcomIoStati.getLccc0261().getWrite().setWcomWriteErr();
        //
        // COB_CODE: MOVE WK-LABEL-ERR           TO IEAI9901-LABEL-ERR
        ws.getIeai9901Area().setLabelErr(ws.getWkLabelErr());
        //
        // COB_CODE: PERFORM S0290-ERRORE-DI-SISTEMA
        //              THRU EX-S0290.
        s0290ErroreDiSistema();
    }

    /**Original name: VALORIZZA-OUTPUT-TDR<br>
	 * <pre>----------------------------------------------------------------*
	 *      VALORIZZAZION TABELLE
	 * ----------------------------------------------------------------*
	 * ------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    VALORIZZA OUTPUT COPY LCCVTDR3
	 *    ULTIMO AGG. 05 DIC 2014
	 * ------------------------------------------------------------</pre>*/
    private void valorizzaOutputTdr() {
        // COB_CODE: MOVE TDR-ID-TIT-RAT
        //             TO (SF)-ID-PTF(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().setIdPtf(ws.getTitRat().getTdrIdTitRat());
        // COB_CODE: MOVE TDR-ID-TIT-RAT
        //             TO (SF)-ID-TIT-RAT(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().setWtdrIdTitRat(ws.getTitRat().getTdrIdTitRat());
        // COB_CODE: MOVE TDR-ID-OGG
        //             TO (SF)-ID-OGG(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().setWtdrIdOgg(ws.getTitRat().getTdrIdOgg());
        // COB_CODE: MOVE TDR-TP-OGG
        //             TO (SF)-TP-OGG(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().setWtdrTpOgg(ws.getTitRat().getTdrTpOgg());
        // COB_CODE: MOVE TDR-ID-MOVI-CRZ
        //             TO (SF)-ID-MOVI-CRZ(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().setWtdrIdMoviCrz(ws.getTitRat().getTdrIdMoviCrz());
        // COB_CODE: IF TDR-ID-MOVI-CHIU-NULL = HIGH-VALUES
        //                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-TDR)
        //           ELSE
        //                TO (SF)-ID-MOVI-CHIU(IX-TAB-TDR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitRat().getTdrIdMoviChiu().getTdrIdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE TDR-ID-MOVI-CHIU-NULL
            //             TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrIdMoviChiu().setWtdrIdMoviChiuNull(ws.getTitRat().getTdrIdMoviChiu().getTdrIdMoviChiuNull());
        }
        else {
            // COB_CODE: MOVE TDR-ID-MOVI-CHIU
            //             TO (SF)-ID-MOVI-CHIU(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrIdMoviChiu().setWtdrIdMoviChiu(ws.getTitRat().getTdrIdMoviChiu().getTdrIdMoviChiu());
        }
        // COB_CODE: MOVE TDR-DT-INI-EFF
        //             TO (SF)-DT-INI-EFF(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().setWtdrDtIniEff(ws.getTitRat().getTdrDtIniEff());
        // COB_CODE: MOVE TDR-DT-END-EFF
        //             TO (SF)-DT-END-EFF(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().setWtdrDtEndEff(ws.getTitRat().getTdrDtEndEff());
        // COB_CODE: MOVE TDR-COD-COMP-ANIA
        //             TO (SF)-COD-COMP-ANIA(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().setWtdrCodCompAnia(ws.getTitRat().getTdrCodCompAnia());
        // COB_CODE: MOVE TDR-TP-TIT
        //             TO (SF)-TP-TIT(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().setWtdrTpTit(ws.getTitRat().getTdrTpTit());
        // COB_CODE: IF TDR-PROG-TIT-NULL = HIGH-VALUES
        //                TO (SF)-PROG-TIT-NULL(IX-TAB-TDR)
        //           ELSE
        //                TO (SF)-PROG-TIT(IX-TAB-TDR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitRat().getTdrProgTit().getTdrProgTitNullFormatted())) {
            // COB_CODE: MOVE TDR-PROG-TIT-NULL
            //             TO (SF)-PROG-TIT-NULL(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrProgTit().setWtdrProgTitNull(ws.getTitRat().getTdrProgTit().getTdrProgTitNull());
        }
        else {
            // COB_CODE: MOVE TDR-PROG-TIT
            //             TO (SF)-PROG-TIT(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrProgTit().setWtdrProgTit(ws.getTitRat().getTdrProgTit().getTdrProgTit());
        }
        // COB_CODE: MOVE TDR-TP-PRE-TIT
        //             TO (SF)-TP-PRE-TIT(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().setWtdrTpPreTit(ws.getTitRat().getTdrTpPreTit());
        // COB_CODE: MOVE TDR-TP-STAT-TIT
        //             TO (SF)-TP-STAT-TIT(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().setWtdrTpStatTit(ws.getTitRat().getTdrTpStatTit());
        // COB_CODE: IF TDR-DT-INI-COP-NULL = HIGH-VALUES
        //                TO (SF)-DT-INI-COP-NULL(IX-TAB-TDR)
        //           ELSE
        //                TO (SF)-DT-INI-COP(IX-TAB-TDR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitRat().getTdrDtIniCop().getTdrDtIniCopNullFormatted())) {
            // COB_CODE: MOVE TDR-DT-INI-COP-NULL
            //             TO (SF)-DT-INI-COP-NULL(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrDtIniCop().setWtdrDtIniCopNull(ws.getTitRat().getTdrDtIniCop().getTdrDtIniCopNull());
        }
        else {
            // COB_CODE: MOVE TDR-DT-INI-COP
            //             TO (SF)-DT-INI-COP(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrDtIniCop().setWtdrDtIniCop(ws.getTitRat().getTdrDtIniCop().getTdrDtIniCop());
        }
        // COB_CODE: IF TDR-DT-END-COP-NULL = HIGH-VALUES
        //                TO (SF)-DT-END-COP-NULL(IX-TAB-TDR)
        //           ELSE
        //                TO (SF)-DT-END-COP(IX-TAB-TDR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitRat().getTdrDtEndCop().getTdrDtEndCopNullFormatted())) {
            // COB_CODE: MOVE TDR-DT-END-COP-NULL
            //             TO (SF)-DT-END-COP-NULL(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrDtEndCop().setWtdrDtEndCopNull(ws.getTitRat().getTdrDtEndCop().getTdrDtEndCopNull());
        }
        else {
            // COB_CODE: MOVE TDR-DT-END-COP
            //             TO (SF)-DT-END-COP(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrDtEndCop().setWtdrDtEndCop(ws.getTitRat().getTdrDtEndCop().getTdrDtEndCop());
        }
        // COB_CODE: IF TDR-IMP-PAG-NULL = HIGH-VALUES
        //                TO (SF)-IMP-PAG-NULL(IX-TAB-TDR)
        //           ELSE
        //                TO (SF)-IMP-PAG(IX-TAB-TDR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitRat().getTdrImpPag().getTdrImpPagNullFormatted())) {
            // COB_CODE: MOVE TDR-IMP-PAG-NULL
            //             TO (SF)-IMP-PAG-NULL(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrImpPag().setWtdrImpPagNull(ws.getTitRat().getTdrImpPag().getTdrImpPagNull());
        }
        else {
            // COB_CODE: MOVE TDR-IMP-PAG
            //             TO (SF)-IMP-PAG(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrImpPag().setWtdrImpPag(Trunc.toDecimal(ws.getTitRat().getTdrImpPag().getTdrImpPag(), 15, 3));
        }
        // COB_CODE: IF TDR-FL-SOLL-NULL = HIGH-VALUES
        //                TO (SF)-FL-SOLL-NULL(IX-TAB-TDR)
        //           ELSE
        //                TO (SF)-FL-SOLL(IX-TAB-TDR)
        //           END-IF
        if (Conditions.eq(ws.getTitRat().getTdrFlSoll(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE TDR-FL-SOLL-NULL
            //             TO (SF)-FL-SOLL-NULL(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().setWtdrFlSoll(ws.getTitRat().getTdrFlSoll());
        }
        else {
            // COB_CODE: MOVE TDR-FL-SOLL
            //             TO (SF)-FL-SOLL(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().setWtdrFlSoll(ws.getTitRat().getTdrFlSoll());
        }
        // COB_CODE: IF TDR-FRAZ-NULL = HIGH-VALUES
        //                TO (SF)-FRAZ-NULL(IX-TAB-TDR)
        //           ELSE
        //                TO (SF)-FRAZ(IX-TAB-TDR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitRat().getTdrFraz().getTdrFrazNullFormatted())) {
            // COB_CODE: MOVE TDR-FRAZ-NULL
            //             TO (SF)-FRAZ-NULL(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrFraz().setWtdrFrazNull(ws.getTitRat().getTdrFraz().getTdrFrazNull());
        }
        else {
            // COB_CODE: MOVE TDR-FRAZ
            //             TO (SF)-FRAZ(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrFraz().setWtdrFraz(ws.getTitRat().getTdrFraz().getTdrFraz());
        }
        // COB_CODE: IF TDR-DT-APPLZ-MORA-NULL = HIGH-VALUES
        //                TO (SF)-DT-APPLZ-MORA-NULL(IX-TAB-TDR)
        //           ELSE
        //                TO (SF)-DT-APPLZ-MORA(IX-TAB-TDR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitRat().getTdrDtApplzMora().getTdrDtApplzMoraNullFormatted())) {
            // COB_CODE: MOVE TDR-DT-APPLZ-MORA-NULL
            //             TO (SF)-DT-APPLZ-MORA-NULL(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrDtApplzMora().setWtdrDtApplzMoraNull(ws.getTitRat().getTdrDtApplzMora().getTdrDtApplzMoraNull());
        }
        else {
            // COB_CODE: MOVE TDR-DT-APPLZ-MORA
            //             TO (SF)-DT-APPLZ-MORA(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrDtApplzMora().setWtdrDtApplzMora(ws.getTitRat().getTdrDtApplzMora().getTdrDtApplzMora());
        }
        // COB_CODE: IF TDR-FL-MORA-NULL = HIGH-VALUES
        //                TO (SF)-FL-MORA-NULL(IX-TAB-TDR)
        //           ELSE
        //                TO (SF)-FL-MORA(IX-TAB-TDR)
        //           END-IF
        if (Conditions.eq(ws.getTitRat().getTdrFlMora(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE TDR-FL-MORA-NULL
            //             TO (SF)-FL-MORA-NULL(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().setWtdrFlMora(ws.getTitRat().getTdrFlMora());
        }
        else {
            // COB_CODE: MOVE TDR-FL-MORA
            //             TO (SF)-FL-MORA(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().setWtdrFlMora(ws.getTitRat().getTdrFlMora());
        }
        // COB_CODE: IF TDR-ID-RAPP-RETE-NULL = HIGH-VALUES
        //                TO (SF)-ID-RAPP-RETE-NULL(IX-TAB-TDR)
        //           ELSE
        //                TO (SF)-ID-RAPP-RETE(IX-TAB-TDR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitRat().getTdrIdRappRete().getTdrIdRappReteNullFormatted())) {
            // COB_CODE: MOVE TDR-ID-RAPP-RETE-NULL
            //             TO (SF)-ID-RAPP-RETE-NULL(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrIdRappRete().setWtdrIdRappReteNull(ws.getTitRat().getTdrIdRappRete().getTdrIdRappReteNull());
        }
        else {
            // COB_CODE: MOVE TDR-ID-RAPP-RETE
            //             TO (SF)-ID-RAPP-RETE(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrIdRappRete().setWtdrIdRappRete(ws.getTitRat().getTdrIdRappRete().getTdrIdRappRete());
        }
        // COB_CODE: IF TDR-ID-RAPP-ANA-NULL = HIGH-VALUES
        //                TO (SF)-ID-RAPP-ANA-NULL(IX-TAB-TDR)
        //           ELSE
        //                TO (SF)-ID-RAPP-ANA(IX-TAB-TDR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitRat().getTdrIdRappAna().getTdrIdRappAnaNullFormatted())) {
            // COB_CODE: MOVE TDR-ID-RAPP-ANA-NULL
            //             TO (SF)-ID-RAPP-ANA-NULL(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrIdRappAna().setWtdrIdRappAnaNull(ws.getTitRat().getTdrIdRappAna().getTdrIdRappAnaNull());
        }
        else {
            // COB_CODE: MOVE TDR-ID-RAPP-ANA
            //             TO (SF)-ID-RAPP-ANA(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrIdRappAna().setWtdrIdRappAna(ws.getTitRat().getTdrIdRappAna().getTdrIdRappAna());
        }
        // COB_CODE: IF TDR-COD-DVS-NULL = HIGH-VALUES
        //                TO (SF)-COD-DVS-NULL(IX-TAB-TDR)
        //           ELSE
        //                TO (SF)-COD-DVS(IX-TAB-TDR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitRat().getTdrCodDvsFormatted())) {
            // COB_CODE: MOVE TDR-COD-DVS-NULL
            //             TO (SF)-COD-DVS-NULL(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().setWtdrCodDvs(ws.getTitRat().getTdrCodDvs());
        }
        else {
            // COB_CODE: MOVE TDR-COD-DVS
            //             TO (SF)-COD-DVS(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().setWtdrCodDvs(ws.getTitRat().getTdrCodDvs());
        }
        // COB_CODE: MOVE TDR-DT-EMIS-TIT
        //             TO (SF)-DT-EMIS-TIT(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().setWtdrDtEmisTit(ws.getTitRat().getTdrDtEmisTit());
        // COB_CODE: IF TDR-DT-ESI-TIT-NULL = HIGH-VALUES
        //                TO (SF)-DT-ESI-TIT-NULL(IX-TAB-TDR)
        //           ELSE
        //                TO (SF)-DT-ESI-TIT(IX-TAB-TDR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitRat().getTdrDtEsiTit().getTdrDtEsiTitNullFormatted())) {
            // COB_CODE: MOVE TDR-DT-ESI-TIT-NULL
            //             TO (SF)-DT-ESI-TIT-NULL(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrDtEsiTit().setWtdrDtEsiTitNull(ws.getTitRat().getTdrDtEsiTit().getTdrDtEsiTitNull());
        }
        else {
            // COB_CODE: MOVE TDR-DT-ESI-TIT
            //             TO (SF)-DT-ESI-TIT(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrDtEsiTit().setWtdrDtEsiTit(ws.getTitRat().getTdrDtEsiTit().getTdrDtEsiTit());
        }
        // COB_CODE: IF TDR-TOT-PRE-NET-NULL = HIGH-VALUES
        //                TO (SF)-TOT-PRE-NET-NULL(IX-TAB-TDR)
        //           ELSE
        //                TO (SF)-TOT-PRE-NET(IX-TAB-TDR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitRat().getTdrTotPreNet().getTdrTotPreNetNullFormatted())) {
            // COB_CODE: MOVE TDR-TOT-PRE-NET-NULL
            //             TO (SF)-TOT-PRE-NET-NULL(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotPreNet().setWtdrTotPreNetNull(ws.getTitRat().getTdrTotPreNet().getTdrTotPreNetNull());
        }
        else {
            // COB_CODE: MOVE TDR-TOT-PRE-NET
            //             TO (SF)-TOT-PRE-NET(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotPreNet().setWtdrTotPreNet(Trunc.toDecimal(ws.getTitRat().getTdrTotPreNet().getTdrTotPreNet(), 15, 3));
        }
        // COB_CODE: IF TDR-TOT-INTR-FRAZ-NULL = HIGH-VALUES
        //                TO (SF)-TOT-INTR-FRAZ-NULL(IX-TAB-TDR)
        //           ELSE
        //                TO (SF)-TOT-INTR-FRAZ(IX-TAB-TDR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitRat().getTdrTotIntrFraz().getTdrTotIntrFrazNullFormatted())) {
            // COB_CODE: MOVE TDR-TOT-INTR-FRAZ-NULL
            //             TO (SF)-TOT-INTR-FRAZ-NULL(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotIntrFraz().setWtdrTotIntrFrazNull(ws.getTitRat().getTdrTotIntrFraz().getTdrTotIntrFrazNull());
        }
        else {
            // COB_CODE: MOVE TDR-TOT-INTR-FRAZ
            //             TO (SF)-TOT-INTR-FRAZ(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotIntrFraz().setWtdrTotIntrFraz(Trunc.toDecimal(ws.getTitRat().getTdrTotIntrFraz().getTdrTotIntrFraz(), 15, 3));
        }
        // COB_CODE: IF TDR-TOT-INTR-MORA-NULL = HIGH-VALUES
        //                TO (SF)-TOT-INTR-MORA-NULL(IX-TAB-TDR)
        //           ELSE
        //                TO (SF)-TOT-INTR-MORA(IX-TAB-TDR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitRat().getTdrTotIntrMora().getTdrTotIntrMoraNullFormatted())) {
            // COB_CODE: MOVE TDR-TOT-INTR-MORA-NULL
            //             TO (SF)-TOT-INTR-MORA-NULL(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotIntrMora().setWtdrTotIntrMoraNull(ws.getTitRat().getTdrTotIntrMora().getTdrTotIntrMoraNull());
        }
        else {
            // COB_CODE: MOVE TDR-TOT-INTR-MORA
            //             TO (SF)-TOT-INTR-MORA(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotIntrMora().setWtdrTotIntrMora(Trunc.toDecimal(ws.getTitRat().getTdrTotIntrMora().getTdrTotIntrMora(), 15, 3));
        }
        // COB_CODE: IF TDR-TOT-INTR-PREST-NULL = HIGH-VALUES
        //                TO (SF)-TOT-INTR-PREST-NULL(IX-TAB-TDR)
        //           ELSE
        //                TO (SF)-TOT-INTR-PREST(IX-TAB-TDR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitRat().getTdrTotIntrPrest().getTdrTotIntrPrestNullFormatted())) {
            // COB_CODE: MOVE TDR-TOT-INTR-PREST-NULL
            //             TO (SF)-TOT-INTR-PREST-NULL(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotIntrPrest().setWtdrTotIntrPrestNull(ws.getTitRat().getTdrTotIntrPrest().getTdrTotIntrPrestNull());
        }
        else {
            // COB_CODE: MOVE TDR-TOT-INTR-PREST
            //             TO (SF)-TOT-INTR-PREST(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotIntrPrest().setWtdrTotIntrPrest(Trunc.toDecimal(ws.getTitRat().getTdrTotIntrPrest().getTdrTotIntrPrest(), 15, 3));
        }
        // COB_CODE: IF TDR-TOT-INTR-RETDT-NULL = HIGH-VALUES
        //                TO (SF)-TOT-INTR-RETDT-NULL(IX-TAB-TDR)
        //           ELSE
        //                TO (SF)-TOT-INTR-RETDT(IX-TAB-TDR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitRat().getTdrTotIntrRetdt().getTdrTotIntrRetdtNullFormatted())) {
            // COB_CODE: MOVE TDR-TOT-INTR-RETDT-NULL
            //             TO (SF)-TOT-INTR-RETDT-NULL(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotIntrRetdt().setWtdrTotIntrRetdtNull(ws.getTitRat().getTdrTotIntrRetdt().getTdrTotIntrRetdtNull());
        }
        else {
            // COB_CODE: MOVE TDR-TOT-INTR-RETDT
            //             TO (SF)-TOT-INTR-RETDT(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotIntrRetdt().setWtdrTotIntrRetdt(Trunc.toDecimal(ws.getTitRat().getTdrTotIntrRetdt().getTdrTotIntrRetdt(), 15, 3));
        }
        // COB_CODE: IF TDR-TOT-INTR-RIAT-NULL = HIGH-VALUES
        //                TO (SF)-TOT-INTR-RIAT-NULL(IX-TAB-TDR)
        //           ELSE
        //                TO (SF)-TOT-INTR-RIAT(IX-TAB-TDR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitRat().getTdrTotIntrRiat().getTdrTotIntrRiatNullFormatted())) {
            // COB_CODE: MOVE TDR-TOT-INTR-RIAT-NULL
            //             TO (SF)-TOT-INTR-RIAT-NULL(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotIntrRiat().setWtdrTotIntrRiatNull(ws.getTitRat().getTdrTotIntrRiat().getTdrTotIntrRiatNull());
        }
        else {
            // COB_CODE: MOVE TDR-TOT-INTR-RIAT
            //             TO (SF)-TOT-INTR-RIAT(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotIntrRiat().setWtdrTotIntrRiat(Trunc.toDecimal(ws.getTitRat().getTdrTotIntrRiat().getTdrTotIntrRiat(), 15, 3));
        }
        // COB_CODE: IF TDR-TOT-DIR-NULL = HIGH-VALUES
        //                TO (SF)-TOT-DIR-NULL(IX-TAB-TDR)
        //           ELSE
        //                TO (SF)-TOT-DIR(IX-TAB-TDR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitRat().getTdrTotDir().getTdrTotDirNullFormatted())) {
            // COB_CODE: MOVE TDR-TOT-DIR-NULL
            //             TO (SF)-TOT-DIR-NULL(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotDir().setWtdrTotDirNull(ws.getTitRat().getTdrTotDir().getTdrTotDirNull());
        }
        else {
            // COB_CODE: MOVE TDR-TOT-DIR
            //             TO (SF)-TOT-DIR(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotDir().setWtdrTotDir(Trunc.toDecimal(ws.getTitRat().getTdrTotDir().getTdrTotDir(), 15, 3));
        }
        // COB_CODE: IF TDR-TOT-SPE-MED-NULL = HIGH-VALUES
        //                TO (SF)-TOT-SPE-MED-NULL(IX-TAB-TDR)
        //           ELSE
        //                TO (SF)-TOT-SPE-MED(IX-TAB-TDR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitRat().getTdrTotSpeMed().getTdrTotSpeMedNullFormatted())) {
            // COB_CODE: MOVE TDR-TOT-SPE-MED-NULL
            //             TO (SF)-TOT-SPE-MED-NULL(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotSpeMed().setWtdrTotSpeMedNull(ws.getTitRat().getTdrTotSpeMed().getTdrTotSpeMedNull());
        }
        else {
            // COB_CODE: MOVE TDR-TOT-SPE-MED
            //             TO (SF)-TOT-SPE-MED(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotSpeMed().setWtdrTotSpeMed(Trunc.toDecimal(ws.getTitRat().getTdrTotSpeMed().getTdrTotSpeMed(), 15, 3));
        }
        // COB_CODE: IF TDR-TOT-SPE-AGE-NULL = HIGH-VALUES
        //                TO (SF)-TOT-SPE-AGE-NULL(IX-TAB-TDR)
        //           ELSE
        //                TO (SF)-TOT-SPE-AGE(IX-TAB-TDR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitRat().getTdrTotSpeAge().getTdrTotSpeAgeNullFormatted())) {
            // COB_CODE: MOVE TDR-TOT-SPE-AGE-NULL
            //             TO (SF)-TOT-SPE-AGE-NULL(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotSpeAge().setWtdrTotSpeAgeNull(ws.getTitRat().getTdrTotSpeAge().getTdrTotSpeAgeNull());
        }
        else {
            // COB_CODE: MOVE TDR-TOT-SPE-AGE
            //             TO (SF)-TOT-SPE-AGE(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotSpeAge().setWtdrTotSpeAge(Trunc.toDecimal(ws.getTitRat().getTdrTotSpeAge().getTdrTotSpeAge(), 15, 3));
        }
        // COB_CODE: IF TDR-TOT-TAX-NULL = HIGH-VALUES
        //                TO (SF)-TOT-TAX-NULL(IX-TAB-TDR)
        //           ELSE
        //                TO (SF)-TOT-TAX(IX-TAB-TDR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitRat().getTdrTotTax().getTdrTotTaxNullFormatted())) {
            // COB_CODE: MOVE TDR-TOT-TAX-NULL
            //             TO (SF)-TOT-TAX-NULL(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotTax().setWtdrTotTaxNull(ws.getTitRat().getTdrTotTax().getTdrTotTaxNull());
        }
        else {
            // COB_CODE: MOVE TDR-TOT-TAX
            //             TO (SF)-TOT-TAX(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotTax().setWtdrTotTax(Trunc.toDecimal(ws.getTitRat().getTdrTotTax().getTdrTotTax(), 15, 3));
        }
        // COB_CODE: IF TDR-TOT-SOPR-SAN-NULL = HIGH-VALUES
        //                TO (SF)-TOT-SOPR-SAN-NULL(IX-TAB-TDR)
        //           ELSE
        //                TO (SF)-TOT-SOPR-SAN(IX-TAB-TDR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitRat().getTdrTotSoprSan().getTdrTotSoprSanNullFormatted())) {
            // COB_CODE: MOVE TDR-TOT-SOPR-SAN-NULL
            //             TO (SF)-TOT-SOPR-SAN-NULL(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotSoprSan().setWtdrTotSoprSanNull(ws.getTitRat().getTdrTotSoprSan().getTdrTotSoprSanNull());
        }
        else {
            // COB_CODE: MOVE TDR-TOT-SOPR-SAN
            //             TO (SF)-TOT-SOPR-SAN(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotSoprSan().setWtdrTotSoprSan(Trunc.toDecimal(ws.getTitRat().getTdrTotSoprSan().getTdrTotSoprSan(), 15, 3));
        }
        // COB_CODE: IF TDR-TOT-SOPR-TEC-NULL = HIGH-VALUES
        //                TO (SF)-TOT-SOPR-TEC-NULL(IX-TAB-TDR)
        //           ELSE
        //                TO (SF)-TOT-SOPR-TEC(IX-TAB-TDR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitRat().getTdrTotSoprTec().getTdrTotSoprTecNullFormatted())) {
            // COB_CODE: MOVE TDR-TOT-SOPR-TEC-NULL
            //             TO (SF)-TOT-SOPR-TEC-NULL(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotSoprTec().setWtdrTotSoprTecNull(ws.getTitRat().getTdrTotSoprTec().getTdrTotSoprTecNull());
        }
        else {
            // COB_CODE: MOVE TDR-TOT-SOPR-TEC
            //             TO (SF)-TOT-SOPR-TEC(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotSoprTec().setWtdrTotSoprTec(Trunc.toDecimal(ws.getTitRat().getTdrTotSoprTec().getTdrTotSoprTec(), 15, 3));
        }
        // COB_CODE: IF TDR-TOT-SOPR-SPO-NULL = HIGH-VALUES
        //                TO (SF)-TOT-SOPR-SPO-NULL(IX-TAB-TDR)
        //           ELSE
        //                TO (SF)-TOT-SOPR-SPO(IX-TAB-TDR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitRat().getTdrTotSoprSpo().getTdrTotSoprSpoNullFormatted())) {
            // COB_CODE: MOVE TDR-TOT-SOPR-SPO-NULL
            //             TO (SF)-TOT-SOPR-SPO-NULL(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotSoprSpo().setWtdrTotSoprSpoNull(ws.getTitRat().getTdrTotSoprSpo().getTdrTotSoprSpoNull());
        }
        else {
            // COB_CODE: MOVE TDR-TOT-SOPR-SPO
            //             TO (SF)-TOT-SOPR-SPO(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotSoprSpo().setWtdrTotSoprSpo(Trunc.toDecimal(ws.getTitRat().getTdrTotSoprSpo().getTdrTotSoprSpo(), 15, 3));
        }
        // COB_CODE: IF TDR-TOT-SOPR-PROF-NULL = HIGH-VALUES
        //                TO (SF)-TOT-SOPR-PROF-NULL(IX-TAB-TDR)
        //           ELSE
        //                TO (SF)-TOT-SOPR-PROF(IX-TAB-TDR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitRat().getTdrTotSoprProf().getTdrTotSoprProfNullFormatted())) {
            // COB_CODE: MOVE TDR-TOT-SOPR-PROF-NULL
            //             TO (SF)-TOT-SOPR-PROF-NULL(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotSoprProf().setWtdrTotSoprProfNull(ws.getTitRat().getTdrTotSoprProf().getTdrTotSoprProfNull());
        }
        else {
            // COB_CODE: MOVE TDR-TOT-SOPR-PROF
            //             TO (SF)-TOT-SOPR-PROF(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotSoprProf().setWtdrTotSoprProf(Trunc.toDecimal(ws.getTitRat().getTdrTotSoprProf().getTdrTotSoprProf(), 15, 3));
        }
        // COB_CODE: IF TDR-TOT-SOPR-ALT-NULL = HIGH-VALUES
        //                TO (SF)-TOT-SOPR-ALT-NULL(IX-TAB-TDR)
        //           ELSE
        //                TO (SF)-TOT-SOPR-ALT(IX-TAB-TDR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitRat().getTdrTotSoprAlt().getTdrTotSoprAltNullFormatted())) {
            // COB_CODE: MOVE TDR-TOT-SOPR-ALT-NULL
            //             TO (SF)-TOT-SOPR-ALT-NULL(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotSoprAlt().setWtdrTotSoprAltNull(ws.getTitRat().getTdrTotSoprAlt().getTdrTotSoprAltNull());
        }
        else {
            // COB_CODE: MOVE TDR-TOT-SOPR-ALT
            //             TO (SF)-TOT-SOPR-ALT(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotSoprAlt().setWtdrTotSoprAlt(Trunc.toDecimal(ws.getTitRat().getTdrTotSoprAlt().getTdrTotSoprAlt(), 15, 3));
        }
        // COB_CODE: IF TDR-TOT-PRE-TOT-NULL = HIGH-VALUES
        //                TO (SF)-TOT-PRE-TOT-NULL(IX-TAB-TDR)
        //           ELSE
        //                TO (SF)-TOT-PRE-TOT(IX-TAB-TDR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitRat().getTdrTotPreTot().getTdrTotPreTotNullFormatted())) {
            // COB_CODE: MOVE TDR-TOT-PRE-TOT-NULL
            //             TO (SF)-TOT-PRE-TOT-NULL(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotPreTot().setWtdrTotPreTotNull(ws.getTitRat().getTdrTotPreTot().getTdrTotPreTotNull());
        }
        else {
            // COB_CODE: MOVE TDR-TOT-PRE-TOT
            //             TO (SF)-TOT-PRE-TOT(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotPreTot().setWtdrTotPreTot(Trunc.toDecimal(ws.getTitRat().getTdrTotPreTot().getTdrTotPreTot(), 15, 3));
        }
        // COB_CODE: IF TDR-TOT-PRE-PP-IAS-NULL = HIGH-VALUES
        //                TO (SF)-TOT-PRE-PP-IAS-NULL(IX-TAB-TDR)
        //           ELSE
        //                TO (SF)-TOT-PRE-PP-IAS(IX-TAB-TDR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitRat().getTdrTotPrePpIas().getTdrTotPrePpIasNullFormatted())) {
            // COB_CODE: MOVE TDR-TOT-PRE-PP-IAS-NULL
            //             TO (SF)-TOT-PRE-PP-IAS-NULL(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotPrePpIas().setWtdrTotPrePpIasNull(ws.getTitRat().getTdrTotPrePpIas().getTdrTotPrePpIasNull());
        }
        else {
            // COB_CODE: MOVE TDR-TOT-PRE-PP-IAS
            //             TO (SF)-TOT-PRE-PP-IAS(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotPrePpIas().setWtdrTotPrePpIas(Trunc.toDecimal(ws.getTitRat().getTdrTotPrePpIas().getTdrTotPrePpIas(), 15, 3));
        }
        // COB_CODE: IF TDR-TOT-CAR-IAS-NULL = HIGH-VALUES
        //                TO (SF)-TOT-CAR-IAS-NULL(IX-TAB-TDR)
        //           ELSE
        //                TO (SF)-TOT-CAR-IAS(IX-TAB-TDR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitRat().getTdrTotCarIas().getTdrTotCarIasNullFormatted())) {
            // COB_CODE: MOVE TDR-TOT-CAR-IAS-NULL
            //             TO (SF)-TOT-CAR-IAS-NULL(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotCarIas().setWtdrTotCarIasNull(ws.getTitRat().getTdrTotCarIas().getTdrTotCarIasNull());
        }
        else {
            // COB_CODE: MOVE TDR-TOT-CAR-IAS
            //             TO (SF)-TOT-CAR-IAS(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotCarIas().setWtdrTotCarIas(Trunc.toDecimal(ws.getTitRat().getTdrTotCarIas().getTdrTotCarIas(), 15, 3));
        }
        // COB_CODE: IF TDR-TOT-PRE-SOLO-RSH-NULL = HIGH-VALUES
        //                TO (SF)-TOT-PRE-SOLO-RSH-NULL(IX-TAB-TDR)
        //           ELSE
        //                TO (SF)-TOT-PRE-SOLO-RSH(IX-TAB-TDR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitRat().getTdrTotPreSoloRsh().getTdrTotPreSoloRshNullFormatted())) {
            // COB_CODE: MOVE TDR-TOT-PRE-SOLO-RSH-NULL
            //             TO (SF)-TOT-PRE-SOLO-RSH-NULL(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotPreSoloRsh().setWtdrTotPreSoloRshNull(ws.getTitRat().getTdrTotPreSoloRsh().getTdrTotPreSoloRshNull());
        }
        else {
            // COB_CODE: MOVE TDR-TOT-PRE-SOLO-RSH
            //             TO (SF)-TOT-PRE-SOLO-RSH(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotPreSoloRsh().setWtdrTotPreSoloRsh(Trunc.toDecimal(ws.getTitRat().getTdrTotPreSoloRsh().getTdrTotPreSoloRsh(), 15, 3));
        }
        // COB_CODE: IF TDR-TOT-PROV-ACQ-1AA-NULL = HIGH-VALUES
        //                TO (SF)-TOT-PROV-ACQ-1AA-NULL(IX-TAB-TDR)
        //           ELSE
        //                TO (SF)-TOT-PROV-ACQ-1AA(IX-TAB-TDR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitRat().getTdrTotProvAcq1aa().getTdrTotProvAcq1aaNullFormatted())) {
            // COB_CODE: MOVE TDR-TOT-PROV-ACQ-1AA-NULL
            //             TO (SF)-TOT-PROV-ACQ-1AA-NULL(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotProvAcq1aa().setWtdrTotProvAcq1aaNull(ws.getTitRat().getTdrTotProvAcq1aa().getTdrTotProvAcq1aaNull());
        }
        else {
            // COB_CODE: MOVE TDR-TOT-PROV-ACQ-1AA
            //             TO (SF)-TOT-PROV-ACQ-1AA(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotProvAcq1aa().setWtdrTotProvAcq1aa(Trunc.toDecimal(ws.getTitRat().getTdrTotProvAcq1aa().getTdrTotProvAcq1aa(), 15, 3));
        }
        // COB_CODE: IF TDR-TOT-PROV-ACQ-2AA-NULL = HIGH-VALUES
        //                TO (SF)-TOT-PROV-ACQ-2AA-NULL(IX-TAB-TDR)
        //           ELSE
        //                TO (SF)-TOT-PROV-ACQ-2AA(IX-TAB-TDR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitRat().getTdrTotProvAcq2aa().getTdrTotProvAcq2aaNullFormatted())) {
            // COB_CODE: MOVE TDR-TOT-PROV-ACQ-2AA-NULL
            //             TO (SF)-TOT-PROV-ACQ-2AA-NULL(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotProvAcq2aa().setWtdrTotProvAcq2aaNull(ws.getTitRat().getTdrTotProvAcq2aa().getTdrTotProvAcq2aaNull());
        }
        else {
            // COB_CODE: MOVE TDR-TOT-PROV-ACQ-2AA
            //             TO (SF)-TOT-PROV-ACQ-2AA(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotProvAcq2aa().setWtdrTotProvAcq2aa(Trunc.toDecimal(ws.getTitRat().getTdrTotProvAcq2aa().getTdrTotProvAcq2aa(), 15, 3));
        }
        // COB_CODE: IF TDR-TOT-PROV-RICOR-NULL = HIGH-VALUES
        //                TO (SF)-TOT-PROV-RICOR-NULL(IX-TAB-TDR)
        //           ELSE
        //                TO (SF)-TOT-PROV-RICOR(IX-TAB-TDR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitRat().getTdrTotProvRicor().getTdrTotProvRicorNullFormatted())) {
            // COB_CODE: MOVE TDR-TOT-PROV-RICOR-NULL
            //             TO (SF)-TOT-PROV-RICOR-NULL(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotProvRicor().setWtdrTotProvRicorNull(ws.getTitRat().getTdrTotProvRicor().getTdrTotProvRicorNull());
        }
        else {
            // COB_CODE: MOVE TDR-TOT-PROV-RICOR
            //             TO (SF)-TOT-PROV-RICOR(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotProvRicor().setWtdrTotProvRicor(Trunc.toDecimal(ws.getTitRat().getTdrTotProvRicor().getTdrTotProvRicor(), 15, 3));
        }
        // COB_CODE: IF TDR-TOT-PROV-INC-NULL = HIGH-VALUES
        //                TO (SF)-TOT-PROV-INC-NULL(IX-TAB-TDR)
        //           ELSE
        //                TO (SF)-TOT-PROV-INC(IX-TAB-TDR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitRat().getTdrTotProvInc().getTdrTotProvIncNullFormatted())) {
            // COB_CODE: MOVE TDR-TOT-PROV-INC-NULL
            //             TO (SF)-TOT-PROV-INC-NULL(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotProvInc().setWtdrTotProvIncNull(ws.getTitRat().getTdrTotProvInc().getTdrTotProvIncNull());
        }
        else {
            // COB_CODE: MOVE TDR-TOT-PROV-INC
            //             TO (SF)-TOT-PROV-INC(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotProvInc().setWtdrTotProvInc(Trunc.toDecimal(ws.getTitRat().getTdrTotProvInc().getTdrTotProvInc(), 15, 3));
        }
        // COB_CODE: IF TDR-TOT-PROV-DA-REC-NULL = HIGH-VALUES
        //                TO (SF)-TOT-PROV-DA-REC-NULL(IX-TAB-TDR)
        //           ELSE
        //                TO (SF)-TOT-PROV-DA-REC(IX-TAB-TDR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitRat().getTdrTotProvDaRec().getTdrTotProvDaRecNullFormatted())) {
            // COB_CODE: MOVE TDR-TOT-PROV-DA-REC-NULL
            //             TO (SF)-TOT-PROV-DA-REC-NULL(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotProvDaRec().setWtdrTotProvDaRecNull(ws.getTitRat().getTdrTotProvDaRec().getTdrTotProvDaRecNull());
        }
        else {
            // COB_CODE: MOVE TDR-TOT-PROV-DA-REC
            //             TO (SF)-TOT-PROV-DA-REC(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotProvDaRec().setWtdrTotProvDaRec(Trunc.toDecimal(ws.getTitRat().getTdrTotProvDaRec().getTdrTotProvDaRec(), 15, 3));
        }
        // COB_CODE: IF TDR-IMP-AZ-NULL = HIGH-VALUES
        //                TO (SF)-IMP-AZ-NULL(IX-TAB-TDR)
        //           ELSE
        //                TO (SF)-IMP-AZ(IX-TAB-TDR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitRat().getTdrImpAz().getTdrImpAzNullFormatted())) {
            // COB_CODE: MOVE TDR-IMP-AZ-NULL
            //             TO (SF)-IMP-AZ-NULL(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrImpAz().setWtdrImpAzNull(ws.getTitRat().getTdrImpAz().getTdrImpAzNull());
        }
        else {
            // COB_CODE: MOVE TDR-IMP-AZ
            //             TO (SF)-IMP-AZ(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrImpAz().setWtdrImpAz(Trunc.toDecimal(ws.getTitRat().getTdrImpAz().getTdrImpAz(), 15, 3));
        }
        // COB_CODE: IF TDR-IMP-ADER-NULL = HIGH-VALUES
        //                TO (SF)-IMP-ADER-NULL(IX-TAB-TDR)
        //           ELSE
        //                TO (SF)-IMP-ADER(IX-TAB-TDR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitRat().getTdrImpAder().getTdrImpAderNullFormatted())) {
            // COB_CODE: MOVE TDR-IMP-ADER-NULL
            //             TO (SF)-IMP-ADER-NULL(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrImpAder().setWtdrImpAderNull(ws.getTitRat().getTdrImpAder().getTdrImpAderNull());
        }
        else {
            // COB_CODE: MOVE TDR-IMP-ADER
            //             TO (SF)-IMP-ADER(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrImpAder().setWtdrImpAder(Trunc.toDecimal(ws.getTitRat().getTdrImpAder().getTdrImpAder(), 15, 3));
        }
        // COB_CODE: IF TDR-IMP-TFR-NULL = HIGH-VALUES
        //                TO (SF)-IMP-TFR-NULL(IX-TAB-TDR)
        //           ELSE
        //                TO (SF)-IMP-TFR(IX-TAB-TDR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitRat().getTdrImpTfr().getTdrImpTfrNullFormatted())) {
            // COB_CODE: MOVE TDR-IMP-TFR-NULL
            //             TO (SF)-IMP-TFR-NULL(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrImpTfr().setWtdrImpTfrNull(ws.getTitRat().getTdrImpTfr().getTdrImpTfrNull());
        }
        else {
            // COB_CODE: MOVE TDR-IMP-TFR
            //             TO (SF)-IMP-TFR(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrImpTfr().setWtdrImpTfr(Trunc.toDecimal(ws.getTitRat().getTdrImpTfr().getTdrImpTfr(), 15, 3));
        }
        // COB_CODE: IF TDR-IMP-VOLO-NULL = HIGH-VALUES
        //                TO (SF)-IMP-VOLO-NULL(IX-TAB-TDR)
        //           ELSE
        //                TO (SF)-IMP-VOLO(IX-TAB-TDR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitRat().getTdrImpVolo().getTdrImpVoloNullFormatted())) {
            // COB_CODE: MOVE TDR-IMP-VOLO-NULL
            //             TO (SF)-IMP-VOLO-NULL(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrImpVolo().setWtdrImpVoloNull(ws.getTitRat().getTdrImpVolo().getTdrImpVoloNull());
        }
        else {
            // COB_CODE: MOVE TDR-IMP-VOLO
            //             TO (SF)-IMP-VOLO(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrImpVolo().setWtdrImpVolo(Trunc.toDecimal(ws.getTitRat().getTdrImpVolo().getTdrImpVolo(), 15, 3));
        }
        // COB_CODE: IF TDR-FL-VLDT-TIT-NULL = HIGH-VALUES
        //                TO (SF)-FL-VLDT-TIT-NULL(IX-TAB-TDR)
        //           ELSE
        //                TO (SF)-FL-VLDT-TIT(IX-TAB-TDR)
        //           END-IF
        if (Conditions.eq(ws.getTitRat().getTdrFlVldtTit(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE TDR-FL-VLDT-TIT-NULL
            //             TO (SF)-FL-VLDT-TIT-NULL(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().setWtdrFlVldtTit(ws.getTitRat().getTdrFlVldtTit());
        }
        else {
            // COB_CODE: MOVE TDR-FL-VLDT-TIT
            //             TO (SF)-FL-VLDT-TIT(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().setWtdrFlVldtTit(ws.getTitRat().getTdrFlVldtTit());
        }
        // COB_CODE: IF TDR-TOT-CAR-ACQ-NULL = HIGH-VALUES
        //                TO (SF)-TOT-CAR-ACQ-NULL(IX-TAB-TDR)
        //           ELSE
        //                TO (SF)-TOT-CAR-ACQ(IX-TAB-TDR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitRat().getTdrTotCarAcq().getTdrTotCarAcqNullFormatted())) {
            // COB_CODE: MOVE TDR-TOT-CAR-ACQ-NULL
            //             TO (SF)-TOT-CAR-ACQ-NULL(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotCarAcq().setWtdrTotCarAcqNull(ws.getTitRat().getTdrTotCarAcq().getTdrTotCarAcqNull());
        }
        else {
            // COB_CODE: MOVE TDR-TOT-CAR-ACQ
            //             TO (SF)-TOT-CAR-ACQ(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotCarAcq().setWtdrTotCarAcq(Trunc.toDecimal(ws.getTitRat().getTdrTotCarAcq().getTdrTotCarAcq(), 15, 3));
        }
        // COB_CODE: IF TDR-TOT-CAR-GEST-NULL = HIGH-VALUES
        //                TO (SF)-TOT-CAR-GEST-NULL(IX-TAB-TDR)
        //           ELSE
        //                TO (SF)-TOT-CAR-GEST(IX-TAB-TDR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitRat().getTdrTotCarGest().getTdrTotCarGestNullFormatted())) {
            // COB_CODE: MOVE TDR-TOT-CAR-GEST-NULL
            //             TO (SF)-TOT-CAR-GEST-NULL(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotCarGest().setWtdrTotCarGestNull(ws.getTitRat().getTdrTotCarGest().getTdrTotCarGestNull());
        }
        else {
            // COB_CODE: MOVE TDR-TOT-CAR-GEST
            //             TO (SF)-TOT-CAR-GEST(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotCarGest().setWtdrTotCarGest(Trunc.toDecimal(ws.getTitRat().getTdrTotCarGest().getTdrTotCarGest(), 15, 3));
        }
        // COB_CODE: IF TDR-TOT-CAR-INC-NULL = HIGH-VALUES
        //                TO (SF)-TOT-CAR-INC-NULL(IX-TAB-TDR)
        //           ELSE
        //                TO (SF)-TOT-CAR-INC(IX-TAB-TDR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitRat().getTdrTotCarInc().getTdrTotCarIncNullFormatted())) {
            // COB_CODE: MOVE TDR-TOT-CAR-INC-NULL
            //             TO (SF)-TOT-CAR-INC-NULL(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotCarInc().setWtdrTotCarIncNull(ws.getTitRat().getTdrTotCarInc().getTdrTotCarIncNull());
        }
        else {
            // COB_CODE: MOVE TDR-TOT-CAR-INC
            //             TO (SF)-TOT-CAR-INC(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotCarInc().setWtdrTotCarInc(Trunc.toDecimal(ws.getTitRat().getTdrTotCarInc().getTdrTotCarInc(), 15, 3));
        }
        // COB_CODE: IF TDR-TOT-MANFEE-ANTIC-NULL = HIGH-VALUES
        //                TO (SF)-TOT-MANFEE-ANTIC-NULL(IX-TAB-TDR)
        //           ELSE
        //                TO (SF)-TOT-MANFEE-ANTIC(IX-TAB-TDR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitRat().getTdrTotManfeeAntic().getTdrTotManfeeAnticNullFormatted())) {
            // COB_CODE: MOVE TDR-TOT-MANFEE-ANTIC-NULL
            //             TO (SF)-TOT-MANFEE-ANTIC-NULL(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotManfeeAntic().setWtdrTotManfeeAnticNull(ws.getTitRat().getTdrTotManfeeAntic().getTdrTotManfeeAnticNull());
        }
        else {
            // COB_CODE: MOVE TDR-TOT-MANFEE-ANTIC
            //             TO (SF)-TOT-MANFEE-ANTIC(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotManfeeAntic().setWtdrTotManfeeAntic(Trunc.toDecimal(ws.getTitRat().getTdrTotManfeeAntic().getTdrTotManfeeAntic(), 15, 3));
        }
        // COB_CODE: IF TDR-TOT-MANFEE-RICOR-NULL = HIGH-VALUES
        //                TO (SF)-TOT-MANFEE-RICOR-NULL(IX-TAB-TDR)
        //           ELSE
        //                TO (SF)-TOT-MANFEE-RICOR(IX-TAB-TDR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitRat().getTdrTotManfeeRicor().getTdrTotManfeeRicorNullFormatted())) {
            // COB_CODE: MOVE TDR-TOT-MANFEE-RICOR-NULL
            //             TO (SF)-TOT-MANFEE-RICOR-NULL(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotManfeeRicor().setWtdrTotManfeeRicorNull(ws.getTitRat().getTdrTotManfeeRicor().getTdrTotManfeeRicorNull());
        }
        else {
            // COB_CODE: MOVE TDR-TOT-MANFEE-RICOR
            //             TO (SF)-TOT-MANFEE-RICOR(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotManfeeRicor().setWtdrTotManfeeRicor(Trunc.toDecimal(ws.getTitRat().getTdrTotManfeeRicor().getTdrTotManfeeRicor(), 15, 3));
        }
        // COB_CODE: IF TDR-TOT-MANFEE-REC-NULL = HIGH-VALUES
        //                TO (SF)-TOT-MANFEE-REC-NULL(IX-TAB-TDR)
        //           ELSE
        //                TO (SF)-TOT-MANFEE-REC(IX-TAB-TDR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitRat().getTdrTotManfeeRec().getTdrTotManfeeRecNullFormatted())) {
            // COB_CODE: MOVE TDR-TOT-MANFEE-REC-NULL
            //             TO (SF)-TOT-MANFEE-REC-NULL(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotManfeeRec().setWtdrTotManfeeRecNull(ws.getTitRat().getTdrTotManfeeRec().getTdrTotManfeeRecNull());
        }
        else {
            // COB_CODE: MOVE TDR-TOT-MANFEE-REC
            //             TO (SF)-TOT-MANFEE-REC(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotManfeeRec().setWtdrTotManfeeRec(Trunc.toDecimal(ws.getTitRat().getTdrTotManfeeRec().getTdrTotManfeeRec(), 15, 3));
        }
        // COB_CODE: MOVE TDR-DS-RIGA
        //             TO (SF)-DS-RIGA(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().setWtdrDsRiga(ws.getTitRat().getTdrDsRiga());
        // COB_CODE: MOVE TDR-DS-OPER-SQL
        //             TO (SF)-DS-OPER-SQL(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().setWtdrDsOperSql(ws.getTitRat().getTdrDsOperSql());
        // COB_CODE: MOVE TDR-DS-VER
        //             TO (SF)-DS-VER(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().setWtdrDsVer(ws.getTitRat().getTdrDsVer());
        // COB_CODE: MOVE TDR-DS-TS-INI-CPTZ
        //             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().setWtdrDsTsIniCptz(ws.getTitRat().getTdrDsTsIniCptz());
        // COB_CODE: MOVE TDR-DS-TS-END-CPTZ
        //             TO (SF)-DS-TS-END-CPTZ(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().setWtdrDsTsEndCptz(ws.getTitRat().getTdrDsTsEndCptz());
        // COB_CODE: MOVE TDR-DS-UTENTE
        //             TO (SF)-DS-UTENTE(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().setWtdrDsUtente(ws.getTitRat().getTdrDsUtente());
        // COB_CODE: MOVE TDR-DS-STATO-ELAB
        //             TO (SF)-DS-STATO-ELAB(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().setWtdrDsStatoElab(ws.getTitRat().getTdrDsStatoElab());
        // COB_CODE: IF TDR-IMP-TRASFE-NULL = HIGH-VALUES
        //                TO (SF)-IMP-TRASFE-NULL(IX-TAB-TDR)
        //           ELSE
        //                TO (SF)-IMP-TRASFE(IX-TAB-TDR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitRat().getTdrImpTrasfe().getTdrImpTrasfeNullFormatted())) {
            // COB_CODE: MOVE TDR-IMP-TRASFE-NULL
            //             TO (SF)-IMP-TRASFE-NULL(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrImpTrasfe().setWtdrImpTrasfeNull(ws.getTitRat().getTdrImpTrasfe().getTdrImpTrasfeNull());
        }
        else {
            // COB_CODE: MOVE TDR-IMP-TRASFE
            //             TO (SF)-IMP-TRASFE(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrImpTrasfe().setWtdrImpTrasfe(Trunc.toDecimal(ws.getTitRat().getTdrImpTrasfe().getTdrImpTrasfe(), 15, 3));
        }
        // COB_CODE: IF TDR-IMP-TFR-STRC-NULL = HIGH-VALUES
        //                TO (SF)-IMP-TFR-STRC-NULL(IX-TAB-TDR)
        //           ELSE
        //                TO (SF)-IMP-TFR-STRC(IX-TAB-TDR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitRat().getTdrImpTfrStrc().getTdrImpTfrStrcNullFormatted())) {
            // COB_CODE: MOVE TDR-IMP-TFR-STRC-NULL
            //             TO (SF)-IMP-TFR-STRC-NULL(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrImpTfrStrc().setWtdrImpTfrStrcNull(ws.getTitRat().getTdrImpTfrStrc().getTdrImpTfrStrcNull());
        }
        else {
            // COB_CODE: MOVE TDR-IMP-TFR-STRC
            //             TO (SF)-IMP-TFR-STRC(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrImpTfrStrc().setWtdrImpTfrStrc(Trunc.toDecimal(ws.getTitRat().getTdrImpTfrStrc().getTdrImpTfrStrc(), 15, 3));
        }
        // COB_CODE: IF TDR-TOT-ACQ-EXP-NULL = HIGH-VALUES
        //                TO (SF)-TOT-ACQ-EXP-NULL(IX-TAB-TDR)
        //           ELSE
        //                TO (SF)-TOT-ACQ-EXP(IX-TAB-TDR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitRat().getTdrTotAcqExp().getTdrTotAcqExpNullFormatted())) {
            // COB_CODE: MOVE TDR-TOT-ACQ-EXP-NULL
            //             TO (SF)-TOT-ACQ-EXP-NULL(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotAcqExp().setWtdrTotAcqExpNull(ws.getTitRat().getTdrTotAcqExp().getTdrTotAcqExpNull());
        }
        else {
            // COB_CODE: MOVE TDR-TOT-ACQ-EXP
            //             TO (SF)-TOT-ACQ-EXP(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotAcqExp().setWtdrTotAcqExp(Trunc.toDecimal(ws.getTitRat().getTdrTotAcqExp().getTdrTotAcqExp(), 15, 3));
        }
        // COB_CODE: IF TDR-TOT-REMUN-ASS-NULL = HIGH-VALUES
        //                TO (SF)-TOT-REMUN-ASS-NULL(IX-TAB-TDR)
        //           ELSE
        //                TO (SF)-TOT-REMUN-ASS(IX-TAB-TDR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitRat().getTdrTotRemunAss().getTdrTotRemunAssNullFormatted())) {
            // COB_CODE: MOVE TDR-TOT-REMUN-ASS-NULL
            //             TO (SF)-TOT-REMUN-ASS-NULL(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotRemunAss().setWtdrTotRemunAssNull(ws.getTitRat().getTdrTotRemunAss().getTdrTotRemunAssNull());
        }
        else {
            // COB_CODE: MOVE TDR-TOT-REMUN-ASS
            //             TO (SF)-TOT-REMUN-ASS(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotRemunAss().setWtdrTotRemunAss(Trunc.toDecimal(ws.getTitRat().getTdrTotRemunAss().getTdrTotRemunAss(), 15, 3));
        }
        // COB_CODE: IF TDR-TOT-COMMIS-INTER-NULL = HIGH-VALUES
        //                TO (SF)-TOT-COMMIS-INTER-NULL(IX-TAB-TDR)
        //           ELSE
        //                TO (SF)-TOT-COMMIS-INTER(IX-TAB-TDR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitRat().getTdrTotCommisInter().getTdrTotCommisInterNullFormatted())) {
            // COB_CODE: MOVE TDR-TOT-COMMIS-INTER-NULL
            //             TO (SF)-TOT-COMMIS-INTER-NULL(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotCommisInter().setWtdrTotCommisInterNull(ws.getTitRat().getTdrTotCommisInter().getTdrTotCommisInterNull());
        }
        else {
            // COB_CODE: MOVE TDR-TOT-COMMIS-INTER
            //             TO (SF)-TOT-COMMIS-INTER(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotCommisInter().setWtdrTotCommisInter(Trunc.toDecimal(ws.getTitRat().getTdrTotCommisInter().getTdrTotCommisInter(), 15, 3));
        }
        // COB_CODE: IF TDR-TOT-CNBT-ANTIRAC-NULL = HIGH-VALUES
        //                TO (SF)-TOT-CNBT-ANTIRAC-NULL(IX-TAB-TDR)
        //           ELSE
        //                TO (SF)-TOT-CNBT-ANTIRAC(IX-TAB-TDR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitRat().getTdrTotCnbtAntirac().getTdrTotCnbtAntiracNullFormatted())) {
            // COB_CODE: MOVE TDR-TOT-CNBT-ANTIRAC-NULL
            //             TO (SF)-TOT-CNBT-ANTIRAC-NULL(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotCnbtAntirac().setWtdrTotCnbtAntiracNull(ws.getTitRat().getTdrTotCnbtAntirac().getTdrTotCnbtAntiracNull());
        }
        else {
            // COB_CODE: MOVE TDR-TOT-CNBT-ANTIRAC
            //             TO (SF)-TOT-CNBT-ANTIRAC(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotCnbtAntirac().setWtdrTotCnbtAntirac(Trunc.toDecimal(ws.getTitRat().getTdrTotCnbtAntirac().getTdrTotCnbtAntirac(), 15, 3));
        }
        // COB_CODE: IF TDR-FL-INC-AUTOGEN-NULL = HIGH-VALUES
        //                TO (SF)-FL-INC-AUTOGEN-NULL(IX-TAB-TDR)
        //           ELSE
        //                TO (SF)-FL-INC-AUTOGEN(IX-TAB-TDR)
        //           END-IF.
        if (Conditions.eq(ws.getTitRat().getTdrFlIncAutogen(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE TDR-FL-INC-AUTOGEN-NULL
            //             TO (SF)-FL-INC-AUTOGEN-NULL(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().setWtdrFlIncAutogen(ws.getTitRat().getTdrFlIncAutogen());
        }
        else {
            // COB_CODE: MOVE TDR-FL-INC-AUTOGEN
            //             TO (SF)-FL-INC-AUTOGEN(IX-TAB-TDR)
            ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().setWtdrFlIncAutogen(ws.getTitRat().getTdrFlIncAutogen());
        }
    }

    /**Original name: INIZIA-TOT-TDR<br>
	 * <pre>------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    INIZIALIZZAZIONE CAMPI NULL COPY LCCVTDR4
	 *    ULTIMO AGG. 05 DIC 2014
	 * ------------------------------------------------------------</pre>*/
    private void iniziaTotTdr() {
        // COB_CODE: PERFORM INIZIA-ZEROES-TDR THRU INIZIA-ZEROES-TDR-EX
        iniziaZeroesTdr();
        // COB_CODE: PERFORM INIZIA-SPACES-TDR THRU INIZIA-SPACES-TDR-EX
        iniziaSpacesTdr();
        // COB_CODE: PERFORM INIZIA-NULL-TDR THRU INIZIA-NULL-TDR-EX.
        iniziaNullTdr();
    }

    /**Original name: INIZIA-NULL-TDR<br>*/
    private void iniziaNullTdr() {
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrIdMoviChiu().setWtdrIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtdrIdMoviChiu.Len.WTDR_ID_MOVI_CHIU_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PROG-TIT-NULL(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrProgTit().setWtdrProgTitNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtdrProgTit.Len.WTDR_PROG_TIT_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-INI-COP-NULL(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrDtIniCop().setWtdrDtIniCopNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtdrDtIniCop.Len.WTDR_DT_INI_COP_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-END-COP-NULL(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrDtEndCop().setWtdrDtEndCopNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtdrDtEndCop.Len.WTDR_DT_END_COP_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMP-PAG-NULL(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrImpPag().setWtdrImpPagNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtdrImpPag.Len.WTDR_IMP_PAG_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-FL-SOLL-NULL(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().setWtdrFlSoll(Types.HIGH_CHAR_VAL);
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-FRAZ-NULL(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrFraz().setWtdrFrazNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtdrFraz.Len.WTDR_FRAZ_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-APPLZ-MORA-NULL(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrDtApplzMora().setWtdrDtApplzMoraNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtdrDtApplzMora.Len.WTDR_DT_APPLZ_MORA_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-FL-MORA-NULL(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().setWtdrFlMora(Types.HIGH_CHAR_VAL);
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-ID-RAPP-RETE-NULL(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrIdRappRete().setWtdrIdRappReteNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtdrIdRappRete.Len.WTDR_ID_RAPP_RETE_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-ID-RAPP-ANA-NULL(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrIdRappAna().setWtdrIdRappAnaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtdrIdRappAna.Len.WTDR_ID_RAPP_ANA_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-COD-DVS-NULL(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().setWtdrCodDvs(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtdrDati.Len.WTDR_COD_DVS));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ESI-TIT-NULL(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrDtEsiTit().setWtdrDtEsiTitNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtdrDtEsiTit.Len.WTDR_DT_ESI_TIT_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-PRE-NET-NULL(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotPreNet().setWtdrTotPreNetNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtdrTotPreNet.Len.WTDR_TOT_PRE_NET_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-INTR-FRAZ-NULL(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotIntrFraz().setWtdrTotIntrFrazNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtdrTotIntrFraz.Len.WTDR_TOT_INTR_FRAZ_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-INTR-MORA-NULL(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotIntrMora().setWtdrTotIntrMoraNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtdrTotIntrMora.Len.WTDR_TOT_INTR_MORA_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-INTR-PREST-NULL(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotIntrPrest().setWtdrTotIntrPrestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtdrTotIntrPrest.Len.WTDR_TOT_INTR_PREST_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-INTR-RETDT-NULL(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotIntrRetdt().setWtdrTotIntrRetdtNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtdrTotIntrRetdt.Len.WTDR_TOT_INTR_RETDT_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-INTR-RIAT-NULL(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotIntrRiat().setWtdrTotIntrRiatNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtdrTotIntrRiat.Len.WTDR_TOT_INTR_RIAT_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-DIR-NULL(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotDir().setWtdrTotDirNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtdrTotDir.Len.WTDR_TOT_DIR_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-SPE-MED-NULL(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotSpeMed().setWtdrTotSpeMedNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtdrTotSpeMed.Len.WTDR_TOT_SPE_MED_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-SPE-AGE-NULL(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotSpeAge().setWtdrTotSpeAgeNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtdrTotSpeAge.Len.WTDR_TOT_SPE_AGE_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-TAX-NULL(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotTax().setWtdrTotTaxNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtdrTotTax.Len.WTDR_TOT_TAX_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-SOPR-SAN-NULL(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotSoprSan().setWtdrTotSoprSanNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtdrTotSoprSan.Len.WTDR_TOT_SOPR_SAN_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-SOPR-TEC-NULL(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotSoprTec().setWtdrTotSoprTecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtdrTotSoprTec.Len.WTDR_TOT_SOPR_TEC_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-SOPR-SPO-NULL(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotSoprSpo().setWtdrTotSoprSpoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtdrTotSoprSpo.Len.WTDR_TOT_SOPR_SPO_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-SOPR-PROF-NULL(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotSoprProf().setWtdrTotSoprProfNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtdrTotSoprProf.Len.WTDR_TOT_SOPR_PROF_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-SOPR-ALT-NULL(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotSoprAlt().setWtdrTotSoprAltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtdrTotSoprAlt.Len.WTDR_TOT_SOPR_ALT_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-PRE-TOT-NULL(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotPreTot().setWtdrTotPreTotNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtdrTotPreTot.Len.WTDR_TOT_PRE_TOT_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-PRE-PP-IAS-NULL(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotPrePpIas().setWtdrTotPrePpIasNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtdrTotPrePpIas.Len.WTDR_TOT_PRE_PP_IAS_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-CAR-IAS-NULL(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotCarIas().setWtdrTotCarIasNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtdrTotCarIas.Len.WTDR_TOT_CAR_IAS_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-PRE-SOLO-RSH-NULL(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotPreSoloRsh().setWtdrTotPreSoloRshNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtdrTotPreSoloRsh.Len.WTDR_TOT_PRE_SOLO_RSH_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-PROV-ACQ-1AA-NULL(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotProvAcq1aa().setWtdrTotProvAcq1aaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtdrTotProvAcq1aa.Len.WTDR_TOT_PROV_ACQ1AA_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-PROV-ACQ-2AA-NULL(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotProvAcq2aa().setWtdrTotProvAcq2aaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtdrTotProvAcq2aa.Len.WTDR_TOT_PROV_ACQ2AA_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-PROV-RICOR-NULL(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotProvRicor().setWtdrTotProvRicorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtdrTotProvRicor.Len.WTDR_TOT_PROV_RICOR_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-PROV-INC-NULL(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotProvInc().setWtdrTotProvIncNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtdrTotProvInc.Len.WTDR_TOT_PROV_INC_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-PROV-DA-REC-NULL(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotProvDaRec().setWtdrTotProvDaRecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtdrTotProvDaRec.Len.WTDR_TOT_PROV_DA_REC_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMP-AZ-NULL(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrImpAz().setWtdrImpAzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtdrImpAz.Len.WTDR_IMP_AZ_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMP-ADER-NULL(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrImpAder().setWtdrImpAderNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtdrImpAder.Len.WTDR_IMP_ADER_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMP-TFR-NULL(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrImpTfr().setWtdrImpTfrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtdrImpTfr.Len.WTDR_IMP_TFR_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMP-VOLO-NULL(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrImpVolo().setWtdrImpVoloNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtdrImpVolo.Len.WTDR_IMP_VOLO_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-FL-VLDT-TIT-NULL(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().setWtdrFlVldtTit(Types.HIGH_CHAR_VAL);
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-CAR-ACQ-NULL(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotCarAcq().setWtdrTotCarAcqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtdrTotCarAcq.Len.WTDR_TOT_CAR_ACQ_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-CAR-GEST-NULL(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotCarGest().setWtdrTotCarGestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtdrTotCarGest.Len.WTDR_TOT_CAR_GEST_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-CAR-INC-NULL(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotCarInc().setWtdrTotCarIncNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtdrTotCarInc.Len.WTDR_TOT_CAR_INC_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-MANFEE-ANTIC-NULL(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotManfeeAntic().setWtdrTotManfeeAnticNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtdrTotManfeeAntic.Len.WTDR_TOT_MANFEE_ANTIC_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-MANFEE-RICOR-NULL(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotManfeeRicor().setWtdrTotManfeeRicorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtdrTotManfeeRicor.Len.WTDR_TOT_MANFEE_RICOR_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-MANFEE-REC-NULL(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotManfeeRec().setWtdrTotManfeeRecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtdrTotManfeeRec.Len.WTDR_TOT_MANFEE_REC_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMP-TRASFE-NULL(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrImpTrasfe().setWtdrImpTrasfeNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtdrImpTrasfe.Len.WTDR_IMP_TRASFE_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMP-TFR-STRC-NULL(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrImpTfrStrc().setWtdrImpTfrStrcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtdrImpTfrStrc.Len.WTDR_IMP_TFR_STRC_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-ACQ-EXP-NULL(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotAcqExp().setWtdrTotAcqExpNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtdrTotAcqExp.Len.WTDR_TOT_ACQ_EXP_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-REMUN-ASS-NULL(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotRemunAss().setWtdrTotRemunAssNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtdrTotRemunAss.Len.WTDR_TOT_REMUN_ASS_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-COMMIS-INTER-NULL(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotCommisInter().setWtdrTotCommisInterNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtdrTotCommisInter.Len.WTDR_TOT_COMMIS_INTER_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-CNBT-ANTIRAC-NULL(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotCnbtAntirac().setWtdrTotCnbtAntiracNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtdrTotCnbtAntirac.Len.WTDR_TOT_CNBT_ANTIRAC_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-FL-INC-AUTOGEN-NULL(IX-TAB-TDR).
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().setWtdrFlIncAutogen(Types.HIGH_CHAR_VAL);
    }

    /**Original name: INIZIA-ZEROES-TDR<br>*/
    private void iniziaZeroesTdr() {
        // COB_CODE: MOVE 0 TO (SF)-ID-TIT-RAT(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().setWtdrIdTitRat(0);
        // COB_CODE: MOVE 0 TO (SF)-ID-OGG(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().setWtdrIdOgg(0);
        // COB_CODE: MOVE 0 TO (SF)-ID-MOVI-CRZ(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().setWtdrIdMoviCrz(0);
        // COB_CODE: MOVE 0 TO (SF)-DT-INI-EFF(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().setWtdrDtIniEff(0);
        // COB_CODE: MOVE 0 TO (SF)-DT-END-EFF(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().setWtdrDtEndEff(0);
        // COB_CODE: MOVE 0 TO (SF)-COD-COMP-ANIA(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().setWtdrCodCompAnia(0);
        // COB_CODE: MOVE 0 TO (SF)-DT-EMIS-TIT(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().setWtdrDtEmisTit(0);
        // COB_CODE: MOVE 0 TO (SF)-DS-RIGA(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().setWtdrDsRiga(0);
        // COB_CODE: MOVE 0 TO (SF)-DS-VER(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().setWtdrDsVer(0);
        // COB_CODE: MOVE 0 TO (SF)-DS-TS-INI-CPTZ(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().setWtdrDsTsIniCptz(0);
        // COB_CODE: MOVE 0 TO (SF)-DS-TS-END-CPTZ(IX-TAB-TDR).
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().setWtdrDsTsEndCptz(0);
    }

    /**Original name: INIZIA-SPACES-TDR<br>*/
    private void iniziaSpacesTdr() {
        // COB_CODE: MOVE SPACES TO (SF)-TP-OGG(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().setWtdrTpOgg("");
        // COB_CODE: MOVE SPACES TO (SF)-TP-TIT(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().setWtdrTpTit("");
        // COB_CODE: MOVE SPACES TO (SF)-TP-PRE-TIT(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().setWtdrTpPreTit("");
        // COB_CODE: MOVE SPACES TO (SF)-TP-STAT-TIT(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().setWtdrTpStatTit("");
        // COB_CODE: MOVE SPACES TO (SF)-DS-OPER-SQL(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().setWtdrDsOperSql(Types.SPACE_CHAR);
        // COB_CODE: MOVE SPACES TO (SF)-DS-UTENTE(IX-TAB-TDR)
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().setWtdrDsUtente("");
        // COB_CODE: MOVE SPACES TO (SF)-DS-STATO-ELAB(IX-TAB-TDR).
        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().setWtdrDsStatoElab(Types.SPACE_CHAR);
    }

    /**Original name: VALORIZZA-OUTPUT-DTR<br>
	 * <pre>------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    VALORIZZA OUTPUT COPY LCCVDTR3
	 *    ULTIMO AGG. 28 NOV 2014
	 * ------------------------------------------------------------</pre>*/
    private void valorizzaOutputDtr() {
        // COB_CODE: MOVE DTR-ID-DETT-TIT-DI-RAT
        //             TO (SF)-ID-PTF(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().setIdPtf(ws.getDettTitDiRat().getDtrIdDettTitDiRat());
        // COB_CODE: MOVE DTR-ID-DETT-TIT-DI-RAT
        //             TO (SF)-ID-DETT-TIT-DI-RAT(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().setWdtrIdDettTitDiRat(ws.getDettTitDiRat().getDtrIdDettTitDiRat());
        // COB_CODE: MOVE DTR-ID-TIT-RAT
        //             TO (SF)-ID-TIT-RAT(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().setWdtrIdTitRat(ws.getDettTitDiRat().getDtrIdTitRat());
        // COB_CODE: MOVE DTR-ID-OGG
        //             TO (SF)-ID-OGG(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().setWdtrIdOgg(ws.getDettTitDiRat().getDtrIdOgg());
        // COB_CODE: MOVE DTR-TP-OGG
        //             TO (SF)-TP-OGG(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().setWdtrTpOgg(ws.getDettTitDiRat().getDtrTpOgg());
        // COB_CODE: MOVE DTR-ID-MOVI-CRZ
        //             TO (SF)-ID-MOVI-CRZ(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().setWdtrIdMoviCrz(ws.getDettTitDiRat().getDtrIdMoviCrz());
        // COB_CODE: IF DTR-ID-MOVI-CHIU-NULL = HIGH-VALUES
        //                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-DTR)
        //           ELSE
        //                TO (SF)-ID-MOVI-CHIU(IX-TAB-DTR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getDettTitDiRat().getDtrIdMoviChiu().getDtrIdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE DTR-ID-MOVI-CHIU-NULL
            //             TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrIdMoviChiu().setWdtrIdMoviChiuNull(ws.getDettTitDiRat().getDtrIdMoviChiu().getDtrIdMoviChiuNull());
        }
        else {
            // COB_CODE: MOVE DTR-ID-MOVI-CHIU
            //             TO (SF)-ID-MOVI-CHIU(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrIdMoviChiu().setWdtrIdMoviChiu(ws.getDettTitDiRat().getDtrIdMoviChiu().getDtrIdMoviChiu());
        }
        // COB_CODE: MOVE DTR-DT-INI-EFF
        //             TO (SF)-DT-INI-EFF(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().setWdtrDtIniEff(ws.getDettTitDiRat().getDtrDtIniEff());
        // COB_CODE: MOVE DTR-DT-END-EFF
        //             TO (SF)-DT-END-EFF(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().setWdtrDtEndEff(ws.getDettTitDiRat().getDtrDtEndEff());
        // COB_CODE: MOVE DTR-COD-COMP-ANIA
        //             TO (SF)-COD-COMP-ANIA(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().setWdtrCodCompAnia(ws.getDettTitDiRat().getDtrCodCompAnia());
        // COB_CODE: IF DTR-DT-INI-COP-NULL = HIGH-VALUES
        //                TO (SF)-DT-INI-COP-NULL(IX-TAB-DTR)
        //           ELSE
        //                TO (SF)-DT-INI-COP(IX-TAB-DTR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getDettTitDiRat().getDtrDtIniCop().getDtrDtIniCopNullFormatted())) {
            // COB_CODE: MOVE DTR-DT-INI-COP-NULL
            //             TO (SF)-DT-INI-COP-NULL(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrDtIniCop().setWdtrDtIniCopNull(ws.getDettTitDiRat().getDtrDtIniCop().getDtrDtIniCopNull());
        }
        else {
            // COB_CODE: MOVE DTR-DT-INI-COP
            //             TO (SF)-DT-INI-COP(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrDtIniCop().setWdtrDtIniCop(ws.getDettTitDiRat().getDtrDtIniCop().getDtrDtIniCop());
        }
        // COB_CODE: IF DTR-DT-END-COP-NULL = HIGH-VALUES
        //                TO (SF)-DT-END-COP-NULL(IX-TAB-DTR)
        //           ELSE
        //                TO (SF)-DT-END-COP(IX-TAB-DTR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getDettTitDiRat().getDtrDtEndCop().getDtrDtEndCopNullFormatted())) {
            // COB_CODE: MOVE DTR-DT-END-COP-NULL
            //             TO (SF)-DT-END-COP-NULL(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrDtEndCop().setWdtrDtEndCopNull(ws.getDettTitDiRat().getDtrDtEndCop().getDtrDtEndCopNull());
        }
        else {
            // COB_CODE: MOVE DTR-DT-END-COP
            //             TO (SF)-DT-END-COP(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrDtEndCop().setWdtrDtEndCop(ws.getDettTitDiRat().getDtrDtEndCop().getDtrDtEndCop());
        }
        // COB_CODE: IF DTR-PRE-NET-NULL = HIGH-VALUES
        //                TO (SF)-PRE-NET-NULL(IX-TAB-DTR)
        //           ELSE
        //                TO (SF)-PRE-NET(IX-TAB-DTR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getDettTitDiRat().getDtrPreNet().getDtrPreNetNullFormatted())) {
            // COB_CODE: MOVE DTR-PRE-NET-NULL
            //             TO (SF)-PRE-NET-NULL(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrPreNet().setWdtrPreNetNull(ws.getDettTitDiRat().getDtrPreNet().getDtrPreNetNull());
        }
        else {
            // COB_CODE: MOVE DTR-PRE-NET
            //             TO (SF)-PRE-NET(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrPreNet().setWdtrPreNet(Trunc.toDecimal(ws.getDettTitDiRat().getDtrPreNet().getDtrPreNet(), 15, 3));
        }
        // COB_CODE: IF DTR-INTR-FRAZ-NULL = HIGH-VALUES
        //                TO (SF)-INTR-FRAZ-NULL(IX-TAB-DTR)
        //           ELSE
        //                TO (SF)-INTR-FRAZ(IX-TAB-DTR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getDettTitDiRat().getDtrIntrFraz().getDtrIntrFrazNullFormatted())) {
            // COB_CODE: MOVE DTR-INTR-FRAZ-NULL
            //             TO (SF)-INTR-FRAZ-NULL(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrIntrFraz().setWdtrIntrFrazNull(ws.getDettTitDiRat().getDtrIntrFraz().getDtrIntrFrazNull());
        }
        else {
            // COB_CODE: MOVE DTR-INTR-FRAZ
            //             TO (SF)-INTR-FRAZ(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrIntrFraz().setWdtrIntrFraz(Trunc.toDecimal(ws.getDettTitDiRat().getDtrIntrFraz().getDtrIntrFraz(), 15, 3));
        }
        // COB_CODE: IF DTR-INTR-MORA-NULL = HIGH-VALUES
        //                TO (SF)-INTR-MORA-NULL(IX-TAB-DTR)
        //           ELSE
        //                TO (SF)-INTR-MORA(IX-TAB-DTR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getDettTitDiRat().getDtrIntrMora().getDtrIntrMoraNullFormatted())) {
            // COB_CODE: MOVE DTR-INTR-MORA-NULL
            //             TO (SF)-INTR-MORA-NULL(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrIntrMora().setWdtrIntrMoraNull(ws.getDettTitDiRat().getDtrIntrMora().getDtrIntrMoraNull());
        }
        else {
            // COB_CODE: MOVE DTR-INTR-MORA
            //             TO (SF)-INTR-MORA(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrIntrMora().setWdtrIntrMora(Trunc.toDecimal(ws.getDettTitDiRat().getDtrIntrMora().getDtrIntrMora(), 15, 3));
        }
        // COB_CODE: IF DTR-INTR-RETDT-NULL = HIGH-VALUES
        //                TO (SF)-INTR-RETDT-NULL(IX-TAB-DTR)
        //           ELSE
        //                TO (SF)-INTR-RETDT(IX-TAB-DTR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getDettTitDiRat().getDtrIntrRetdt().getDtrIntrRetdtNullFormatted())) {
            // COB_CODE: MOVE DTR-INTR-RETDT-NULL
            //             TO (SF)-INTR-RETDT-NULL(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrIntrRetdt().setWdtrIntrRetdtNull(ws.getDettTitDiRat().getDtrIntrRetdt().getDtrIntrRetdtNull());
        }
        else {
            // COB_CODE: MOVE DTR-INTR-RETDT
            //             TO (SF)-INTR-RETDT(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrIntrRetdt().setWdtrIntrRetdt(Trunc.toDecimal(ws.getDettTitDiRat().getDtrIntrRetdt().getDtrIntrRetdt(), 15, 3));
        }
        // COB_CODE: IF DTR-INTR-RIAT-NULL = HIGH-VALUES
        //                TO (SF)-INTR-RIAT-NULL(IX-TAB-DTR)
        //           ELSE
        //                TO (SF)-INTR-RIAT(IX-TAB-DTR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getDettTitDiRat().getDtrIntrRiat().getDtrIntrRiatNullFormatted())) {
            // COB_CODE: MOVE DTR-INTR-RIAT-NULL
            //             TO (SF)-INTR-RIAT-NULL(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrIntrRiat().setWdtrIntrRiatNull(ws.getDettTitDiRat().getDtrIntrRiat().getDtrIntrRiatNull());
        }
        else {
            // COB_CODE: MOVE DTR-INTR-RIAT
            //             TO (SF)-INTR-RIAT(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrIntrRiat().setWdtrIntrRiat(Trunc.toDecimal(ws.getDettTitDiRat().getDtrIntrRiat().getDtrIntrRiat(), 15, 3));
        }
        // COB_CODE: IF DTR-DIR-NULL = HIGH-VALUES
        //                TO (SF)-DIR-NULL(IX-TAB-DTR)
        //           ELSE
        //                TO (SF)-DIR(IX-TAB-DTR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getDettTitDiRat().getDtrDir().getDtrDirNullFormatted())) {
            // COB_CODE: MOVE DTR-DIR-NULL
            //             TO (SF)-DIR-NULL(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrDir().setWdtrDirNull(ws.getDettTitDiRat().getDtrDir().getDtrDirNull());
        }
        else {
            // COB_CODE: MOVE DTR-DIR
            //             TO (SF)-DIR(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrDir().setWdtrDir(Trunc.toDecimal(ws.getDettTitDiRat().getDtrDir().getDtrDir(), 15, 3));
        }
        // COB_CODE: IF DTR-SPE-MED-NULL = HIGH-VALUES
        //                TO (SF)-SPE-MED-NULL(IX-TAB-DTR)
        //           ELSE
        //                TO (SF)-SPE-MED(IX-TAB-DTR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getDettTitDiRat().getDtrSpeMed().getDtrSpeMedNullFormatted())) {
            // COB_CODE: MOVE DTR-SPE-MED-NULL
            //             TO (SF)-SPE-MED-NULL(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrSpeMed().setWdtrSpeMedNull(ws.getDettTitDiRat().getDtrSpeMed().getDtrSpeMedNull());
        }
        else {
            // COB_CODE: MOVE DTR-SPE-MED
            //             TO (SF)-SPE-MED(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrSpeMed().setWdtrSpeMed(Trunc.toDecimal(ws.getDettTitDiRat().getDtrSpeMed().getDtrSpeMed(), 15, 3));
        }
        // COB_CODE: IF DTR-SPE-AGE-NULL = HIGH-VALUES
        //                TO (SF)-SPE-AGE-NULL(IX-TAB-DTR)
        //           ELSE
        //                TO (SF)-SPE-AGE(IX-TAB-DTR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getDettTitDiRat().getDtrSpeAge().getDtrSpeAgeNullFormatted())) {
            // COB_CODE: MOVE DTR-SPE-AGE-NULL
            //             TO (SF)-SPE-AGE-NULL(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrSpeAge().setWdtrSpeAgeNull(ws.getDettTitDiRat().getDtrSpeAge().getDtrSpeAgeNull());
        }
        else {
            // COB_CODE: MOVE DTR-SPE-AGE
            //             TO (SF)-SPE-AGE(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrSpeAge().setWdtrSpeAge(Trunc.toDecimal(ws.getDettTitDiRat().getDtrSpeAge().getDtrSpeAge(), 15, 3));
        }
        // COB_CODE: IF DTR-TAX-NULL = HIGH-VALUES
        //                TO (SF)-TAX-NULL(IX-TAB-DTR)
        //           ELSE
        //                TO (SF)-TAX(IX-TAB-DTR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getDettTitDiRat().getDtrTax().getDtrTaxNullFormatted())) {
            // COB_CODE: MOVE DTR-TAX-NULL
            //             TO (SF)-TAX-NULL(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrTax().setWdtrTaxNull(ws.getDettTitDiRat().getDtrTax().getDtrTaxNull());
        }
        else {
            // COB_CODE: MOVE DTR-TAX
            //             TO (SF)-TAX(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrTax().setWdtrTax(Trunc.toDecimal(ws.getDettTitDiRat().getDtrTax().getDtrTax(), 15, 3));
        }
        // COB_CODE: IF DTR-SOPR-SAN-NULL = HIGH-VALUES
        //                TO (SF)-SOPR-SAN-NULL(IX-TAB-DTR)
        //           ELSE
        //                TO (SF)-SOPR-SAN(IX-TAB-DTR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getDettTitDiRat().getDtrSoprSan().getDtrSoprSanNullFormatted())) {
            // COB_CODE: MOVE DTR-SOPR-SAN-NULL
            //             TO (SF)-SOPR-SAN-NULL(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrSoprSan().setWdtrSoprSanNull(ws.getDettTitDiRat().getDtrSoprSan().getDtrSoprSanNull());
        }
        else {
            // COB_CODE: MOVE DTR-SOPR-SAN
            //             TO (SF)-SOPR-SAN(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrSoprSan().setWdtrSoprSan(Trunc.toDecimal(ws.getDettTitDiRat().getDtrSoprSan().getDtrSoprSan(), 15, 3));
        }
        // COB_CODE: IF DTR-SOPR-SPO-NULL = HIGH-VALUES
        //                TO (SF)-SOPR-SPO-NULL(IX-TAB-DTR)
        //           ELSE
        //                TO (SF)-SOPR-SPO(IX-TAB-DTR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getDettTitDiRat().getDtrSoprSpo().getDtrSoprSpoNullFormatted())) {
            // COB_CODE: MOVE DTR-SOPR-SPO-NULL
            //             TO (SF)-SOPR-SPO-NULL(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrSoprSpo().setWdtrSoprSpoNull(ws.getDettTitDiRat().getDtrSoprSpo().getDtrSoprSpoNull());
        }
        else {
            // COB_CODE: MOVE DTR-SOPR-SPO
            //             TO (SF)-SOPR-SPO(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrSoprSpo().setWdtrSoprSpo(Trunc.toDecimal(ws.getDettTitDiRat().getDtrSoprSpo().getDtrSoprSpo(), 15, 3));
        }
        // COB_CODE: IF DTR-SOPR-TEC-NULL = HIGH-VALUES
        //                TO (SF)-SOPR-TEC-NULL(IX-TAB-DTR)
        //           ELSE
        //                TO (SF)-SOPR-TEC(IX-TAB-DTR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getDettTitDiRat().getDtrSoprTec().getDtrSoprTecNullFormatted())) {
            // COB_CODE: MOVE DTR-SOPR-TEC-NULL
            //             TO (SF)-SOPR-TEC-NULL(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrSoprTec().setWdtrSoprTecNull(ws.getDettTitDiRat().getDtrSoprTec().getDtrSoprTecNull());
        }
        else {
            // COB_CODE: MOVE DTR-SOPR-TEC
            //             TO (SF)-SOPR-TEC(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrSoprTec().setWdtrSoprTec(Trunc.toDecimal(ws.getDettTitDiRat().getDtrSoprTec().getDtrSoprTec(), 15, 3));
        }
        // COB_CODE: IF DTR-SOPR-PROF-NULL = HIGH-VALUES
        //                TO (SF)-SOPR-PROF-NULL(IX-TAB-DTR)
        //           ELSE
        //                TO (SF)-SOPR-PROF(IX-TAB-DTR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getDettTitDiRat().getDtrSoprProf().getDtrSoprProfNullFormatted())) {
            // COB_CODE: MOVE DTR-SOPR-PROF-NULL
            //             TO (SF)-SOPR-PROF-NULL(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrSoprProf().setWdtrSoprProfNull(ws.getDettTitDiRat().getDtrSoprProf().getDtrSoprProfNull());
        }
        else {
            // COB_CODE: MOVE DTR-SOPR-PROF
            //             TO (SF)-SOPR-PROF(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrSoprProf().setWdtrSoprProf(Trunc.toDecimal(ws.getDettTitDiRat().getDtrSoprProf().getDtrSoprProf(), 15, 3));
        }
        // COB_CODE: IF DTR-SOPR-ALT-NULL = HIGH-VALUES
        //                TO (SF)-SOPR-ALT-NULL(IX-TAB-DTR)
        //           ELSE
        //                TO (SF)-SOPR-ALT(IX-TAB-DTR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getDettTitDiRat().getDtrSoprAlt().getDtrSoprAltNullFormatted())) {
            // COB_CODE: MOVE DTR-SOPR-ALT-NULL
            //             TO (SF)-SOPR-ALT-NULL(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrSoprAlt().setWdtrSoprAltNull(ws.getDettTitDiRat().getDtrSoprAlt().getDtrSoprAltNull());
        }
        else {
            // COB_CODE: MOVE DTR-SOPR-ALT
            //             TO (SF)-SOPR-ALT(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrSoprAlt().setWdtrSoprAlt(Trunc.toDecimal(ws.getDettTitDiRat().getDtrSoprAlt().getDtrSoprAlt(), 15, 3));
        }
        // COB_CODE: IF DTR-PRE-TOT-NULL = HIGH-VALUES
        //                TO (SF)-PRE-TOT-NULL(IX-TAB-DTR)
        //           ELSE
        //                TO (SF)-PRE-TOT(IX-TAB-DTR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getDettTitDiRat().getDtrPreTot().getDtrPreTotNullFormatted())) {
            // COB_CODE: MOVE DTR-PRE-TOT-NULL
            //             TO (SF)-PRE-TOT-NULL(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrPreTot().setWdtrPreTotNull(ws.getDettTitDiRat().getDtrPreTot().getDtrPreTotNull());
        }
        else {
            // COB_CODE: MOVE DTR-PRE-TOT
            //             TO (SF)-PRE-TOT(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrPreTot().setWdtrPreTot(Trunc.toDecimal(ws.getDettTitDiRat().getDtrPreTot().getDtrPreTot(), 15, 3));
        }
        // COB_CODE: IF DTR-PRE-PP-IAS-NULL = HIGH-VALUES
        //                TO (SF)-PRE-PP-IAS-NULL(IX-TAB-DTR)
        //           ELSE
        //                TO (SF)-PRE-PP-IAS(IX-TAB-DTR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getDettTitDiRat().getDtrPrePpIas().getDtrPrePpIasNullFormatted())) {
            // COB_CODE: MOVE DTR-PRE-PP-IAS-NULL
            //             TO (SF)-PRE-PP-IAS-NULL(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrPrePpIas().setWdtrPrePpIasNull(ws.getDettTitDiRat().getDtrPrePpIas().getDtrPrePpIasNull());
        }
        else {
            // COB_CODE: MOVE DTR-PRE-PP-IAS
            //             TO (SF)-PRE-PP-IAS(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrPrePpIas().setWdtrPrePpIas(Trunc.toDecimal(ws.getDettTitDiRat().getDtrPrePpIas().getDtrPrePpIas(), 15, 3));
        }
        // COB_CODE: IF DTR-PRE-SOLO-RSH-NULL = HIGH-VALUES
        //                TO (SF)-PRE-SOLO-RSH-NULL(IX-TAB-DTR)
        //           ELSE
        //                TO (SF)-PRE-SOLO-RSH(IX-TAB-DTR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getDettTitDiRat().getDtrPreSoloRsh().getDtrPreSoloRshNullFormatted())) {
            // COB_CODE: MOVE DTR-PRE-SOLO-RSH-NULL
            //             TO (SF)-PRE-SOLO-RSH-NULL(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrPreSoloRsh().setWdtrPreSoloRshNull(ws.getDettTitDiRat().getDtrPreSoloRsh().getDtrPreSoloRshNull());
        }
        else {
            // COB_CODE: MOVE DTR-PRE-SOLO-RSH
            //             TO (SF)-PRE-SOLO-RSH(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrPreSoloRsh().setWdtrPreSoloRsh(Trunc.toDecimal(ws.getDettTitDiRat().getDtrPreSoloRsh().getDtrPreSoloRsh(), 15, 3));
        }
        // COB_CODE: IF DTR-CAR-IAS-NULL = HIGH-VALUES
        //                TO (SF)-CAR-IAS-NULL(IX-TAB-DTR)
        //           ELSE
        //                TO (SF)-CAR-IAS(IX-TAB-DTR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getDettTitDiRat().getDtrCarIas().getDtrCarIasNullFormatted())) {
            // COB_CODE: MOVE DTR-CAR-IAS-NULL
            //             TO (SF)-CAR-IAS-NULL(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrCarIas().setWdtrCarIasNull(ws.getDettTitDiRat().getDtrCarIas().getDtrCarIasNull());
        }
        else {
            // COB_CODE: MOVE DTR-CAR-IAS
            //             TO (SF)-CAR-IAS(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrCarIas().setWdtrCarIas(Trunc.toDecimal(ws.getDettTitDiRat().getDtrCarIas().getDtrCarIas(), 15, 3));
        }
        // COB_CODE: IF DTR-PROV-ACQ-1AA-NULL = HIGH-VALUES
        //                TO (SF)-PROV-ACQ-1AA-NULL(IX-TAB-DTR)
        //           ELSE
        //                TO (SF)-PROV-ACQ-1AA(IX-TAB-DTR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getDettTitDiRat().getDtrProvAcq1aa().getDtrProvAcq1aaNullFormatted())) {
            // COB_CODE: MOVE DTR-PROV-ACQ-1AA-NULL
            //             TO (SF)-PROV-ACQ-1AA-NULL(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrProvAcq1aa().setWdtrProvAcq1aaNull(ws.getDettTitDiRat().getDtrProvAcq1aa().getDtrProvAcq1aaNull());
        }
        else {
            // COB_CODE: MOVE DTR-PROV-ACQ-1AA
            //             TO (SF)-PROV-ACQ-1AA(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrProvAcq1aa().setWdtrProvAcq1aa(Trunc.toDecimal(ws.getDettTitDiRat().getDtrProvAcq1aa().getDtrProvAcq1aa(), 15, 3));
        }
        // COB_CODE: IF DTR-PROV-ACQ-2AA-NULL = HIGH-VALUES
        //                TO (SF)-PROV-ACQ-2AA-NULL(IX-TAB-DTR)
        //           ELSE
        //                TO (SF)-PROV-ACQ-2AA(IX-TAB-DTR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getDettTitDiRat().getDtrProvAcq2aa().getDtrProvAcq2aaNullFormatted())) {
            // COB_CODE: MOVE DTR-PROV-ACQ-2AA-NULL
            //             TO (SF)-PROV-ACQ-2AA-NULL(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrProvAcq2aa().setWdtrProvAcq2aaNull(ws.getDettTitDiRat().getDtrProvAcq2aa().getDtrProvAcq2aaNull());
        }
        else {
            // COB_CODE: MOVE DTR-PROV-ACQ-2AA
            //             TO (SF)-PROV-ACQ-2AA(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrProvAcq2aa().setWdtrProvAcq2aa(Trunc.toDecimal(ws.getDettTitDiRat().getDtrProvAcq2aa().getDtrProvAcq2aa(), 15, 3));
        }
        // COB_CODE: IF DTR-PROV-RICOR-NULL = HIGH-VALUES
        //                TO (SF)-PROV-RICOR-NULL(IX-TAB-DTR)
        //           ELSE
        //                TO (SF)-PROV-RICOR(IX-TAB-DTR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getDettTitDiRat().getDtrProvRicor().getDtrProvRicorNullFormatted())) {
            // COB_CODE: MOVE DTR-PROV-RICOR-NULL
            //             TO (SF)-PROV-RICOR-NULL(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrProvRicor().setWdtrProvRicorNull(ws.getDettTitDiRat().getDtrProvRicor().getDtrProvRicorNull());
        }
        else {
            // COB_CODE: MOVE DTR-PROV-RICOR
            //             TO (SF)-PROV-RICOR(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrProvRicor().setWdtrProvRicor(Trunc.toDecimal(ws.getDettTitDiRat().getDtrProvRicor().getDtrProvRicor(), 15, 3));
        }
        // COB_CODE: IF DTR-PROV-INC-NULL = HIGH-VALUES
        //                TO (SF)-PROV-INC-NULL(IX-TAB-DTR)
        //           ELSE
        //                TO (SF)-PROV-INC(IX-TAB-DTR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getDettTitDiRat().getDtrProvInc().getDtrProvIncNullFormatted())) {
            // COB_CODE: MOVE DTR-PROV-INC-NULL
            //             TO (SF)-PROV-INC-NULL(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrProvInc().setWdtrProvIncNull(ws.getDettTitDiRat().getDtrProvInc().getDtrProvIncNull());
        }
        else {
            // COB_CODE: MOVE DTR-PROV-INC
            //             TO (SF)-PROV-INC(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrProvInc().setWdtrProvInc(Trunc.toDecimal(ws.getDettTitDiRat().getDtrProvInc().getDtrProvInc(), 15, 3));
        }
        // COB_CODE: IF DTR-PROV-DA-REC-NULL = HIGH-VALUES
        //                TO (SF)-PROV-DA-REC-NULL(IX-TAB-DTR)
        //           ELSE
        //                TO (SF)-PROV-DA-REC(IX-TAB-DTR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getDettTitDiRat().getDtrProvDaRec().getDtrProvDaRecNullFormatted())) {
            // COB_CODE: MOVE DTR-PROV-DA-REC-NULL
            //             TO (SF)-PROV-DA-REC-NULL(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrProvDaRec().setWdtrProvDaRecNull(ws.getDettTitDiRat().getDtrProvDaRec().getDtrProvDaRecNull());
        }
        else {
            // COB_CODE: MOVE DTR-PROV-DA-REC
            //             TO (SF)-PROV-DA-REC(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrProvDaRec().setWdtrProvDaRec(Trunc.toDecimal(ws.getDettTitDiRat().getDtrProvDaRec().getDtrProvDaRec(), 15, 3));
        }
        // COB_CODE: IF DTR-COD-DVS-NULL = HIGH-VALUES
        //                TO (SF)-COD-DVS-NULL(IX-TAB-DTR)
        //           ELSE
        //                TO (SF)-COD-DVS(IX-TAB-DTR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getDettTitDiRat().getDtrCodDvsFormatted())) {
            // COB_CODE: MOVE DTR-COD-DVS-NULL
            //             TO (SF)-COD-DVS-NULL(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().setWdtrCodDvs(ws.getDettTitDiRat().getDtrCodDvs());
        }
        else {
            // COB_CODE: MOVE DTR-COD-DVS
            //             TO (SF)-COD-DVS(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().setWdtrCodDvs(ws.getDettTitDiRat().getDtrCodDvs());
        }
        // COB_CODE: IF DTR-FRQ-MOVI-NULL = HIGH-VALUES
        //                TO (SF)-FRQ-MOVI-NULL(IX-TAB-DTR)
        //           ELSE
        //                TO (SF)-FRQ-MOVI(IX-TAB-DTR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getDettTitDiRat().getDtrFrqMovi().getDtrFrqMoviNullFormatted())) {
            // COB_CODE: MOVE DTR-FRQ-MOVI-NULL
            //             TO (SF)-FRQ-MOVI-NULL(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrFrqMovi().setWdtrFrqMoviNull(ws.getDettTitDiRat().getDtrFrqMovi().getDtrFrqMoviNull());
        }
        else {
            // COB_CODE: MOVE DTR-FRQ-MOVI
            //             TO (SF)-FRQ-MOVI(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrFrqMovi().setWdtrFrqMovi(ws.getDettTitDiRat().getDtrFrqMovi().getDtrFrqMovi());
        }
        // COB_CODE: MOVE DTR-TP-RGM-FISC
        //             TO (SF)-TP-RGM-FISC(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().setWdtrTpRgmFisc(ws.getDettTitDiRat().getDtrTpRgmFisc());
        // COB_CODE: IF DTR-COD-TARI-NULL = HIGH-VALUES
        //                TO (SF)-COD-TARI-NULL(IX-TAB-DTR)
        //           ELSE
        //                TO (SF)-COD-TARI(IX-TAB-DTR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getDettTitDiRat().getDtrCodTariFormatted())) {
            // COB_CODE: MOVE DTR-COD-TARI-NULL
            //             TO (SF)-COD-TARI-NULL(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().setWdtrCodTari(ws.getDettTitDiRat().getDtrCodTari());
        }
        else {
            // COB_CODE: MOVE DTR-COD-TARI
            //             TO (SF)-COD-TARI(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().setWdtrCodTari(ws.getDettTitDiRat().getDtrCodTari());
        }
        // COB_CODE: MOVE DTR-TP-STAT-TIT
        //             TO (SF)-TP-STAT-TIT(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().setWdtrTpStatTit(ws.getDettTitDiRat().getDtrTpStatTit());
        // COB_CODE: IF DTR-IMP-AZ-NULL = HIGH-VALUES
        //                TO (SF)-IMP-AZ-NULL(IX-TAB-DTR)
        //           ELSE
        //                TO (SF)-IMP-AZ(IX-TAB-DTR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getDettTitDiRat().getDtrImpAz().getDtrImpAzNullFormatted())) {
            // COB_CODE: MOVE DTR-IMP-AZ-NULL
            //             TO (SF)-IMP-AZ-NULL(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrImpAz().setWdtrImpAzNull(ws.getDettTitDiRat().getDtrImpAz().getDtrImpAzNull());
        }
        else {
            // COB_CODE: MOVE DTR-IMP-AZ
            //             TO (SF)-IMP-AZ(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrImpAz().setWdtrImpAz(Trunc.toDecimal(ws.getDettTitDiRat().getDtrImpAz().getDtrImpAz(), 15, 3));
        }
        // COB_CODE: IF DTR-IMP-ADER-NULL = HIGH-VALUES
        //                TO (SF)-IMP-ADER-NULL(IX-TAB-DTR)
        //           ELSE
        //                TO (SF)-IMP-ADER(IX-TAB-DTR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getDettTitDiRat().getDtrImpAder().getDtrImpAderNullFormatted())) {
            // COB_CODE: MOVE DTR-IMP-ADER-NULL
            //             TO (SF)-IMP-ADER-NULL(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrImpAder().setWdtrImpAderNull(ws.getDettTitDiRat().getDtrImpAder().getDtrImpAderNull());
        }
        else {
            // COB_CODE: MOVE DTR-IMP-ADER
            //             TO (SF)-IMP-ADER(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrImpAder().setWdtrImpAder(Trunc.toDecimal(ws.getDettTitDiRat().getDtrImpAder().getDtrImpAder(), 15, 3));
        }
        // COB_CODE: IF DTR-IMP-TFR-NULL = HIGH-VALUES
        //                TO (SF)-IMP-TFR-NULL(IX-TAB-DTR)
        //           ELSE
        //                TO (SF)-IMP-TFR(IX-TAB-DTR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getDettTitDiRat().getDtrImpTfr().getDtrImpTfrNullFormatted())) {
            // COB_CODE: MOVE DTR-IMP-TFR-NULL
            //             TO (SF)-IMP-TFR-NULL(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrImpTfr().setWdtrImpTfrNull(ws.getDettTitDiRat().getDtrImpTfr().getDtrImpTfrNull());
        }
        else {
            // COB_CODE: MOVE DTR-IMP-TFR
            //             TO (SF)-IMP-TFR(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrImpTfr().setWdtrImpTfr(Trunc.toDecimal(ws.getDettTitDiRat().getDtrImpTfr().getDtrImpTfr(), 15, 3));
        }
        // COB_CODE: IF DTR-IMP-VOLO-NULL = HIGH-VALUES
        //                TO (SF)-IMP-VOLO-NULL(IX-TAB-DTR)
        //           ELSE
        //                TO (SF)-IMP-VOLO(IX-TAB-DTR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getDettTitDiRat().getDtrImpVolo().getDtrImpVoloNullFormatted())) {
            // COB_CODE: MOVE DTR-IMP-VOLO-NULL
            //             TO (SF)-IMP-VOLO-NULL(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrImpVolo().setWdtrImpVoloNull(ws.getDettTitDiRat().getDtrImpVolo().getDtrImpVoloNull());
        }
        else {
            // COB_CODE: MOVE DTR-IMP-VOLO
            //             TO (SF)-IMP-VOLO(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrImpVolo().setWdtrImpVolo(Trunc.toDecimal(ws.getDettTitDiRat().getDtrImpVolo().getDtrImpVolo(), 15, 3));
        }
        // COB_CODE: IF DTR-FL-VLDT-TIT-NULL = HIGH-VALUES
        //                TO (SF)-FL-VLDT-TIT-NULL(IX-TAB-DTR)
        //           ELSE
        //                TO (SF)-FL-VLDT-TIT(IX-TAB-DTR)
        //           END-IF
        if (Conditions.eq(ws.getDettTitDiRat().getDtrFlVldtTit(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE DTR-FL-VLDT-TIT-NULL
            //             TO (SF)-FL-VLDT-TIT-NULL(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().setWdtrFlVldtTit(ws.getDettTitDiRat().getDtrFlVldtTit());
        }
        else {
            // COB_CODE: MOVE DTR-FL-VLDT-TIT
            //             TO (SF)-FL-VLDT-TIT(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().setWdtrFlVldtTit(ws.getDettTitDiRat().getDtrFlVldtTit());
        }
        // COB_CODE: IF DTR-CAR-ACQ-NULL = HIGH-VALUES
        //                TO (SF)-CAR-ACQ-NULL(IX-TAB-DTR)
        //           ELSE
        //                TO (SF)-CAR-ACQ(IX-TAB-DTR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getDettTitDiRat().getDtrCarAcq().getDtrCarAcqNullFormatted())) {
            // COB_CODE: MOVE DTR-CAR-ACQ-NULL
            //             TO (SF)-CAR-ACQ-NULL(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrCarAcq().setWdtrCarAcqNull(ws.getDettTitDiRat().getDtrCarAcq().getDtrCarAcqNull());
        }
        else {
            // COB_CODE: MOVE DTR-CAR-ACQ
            //             TO (SF)-CAR-ACQ(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrCarAcq().setWdtrCarAcq(Trunc.toDecimal(ws.getDettTitDiRat().getDtrCarAcq().getDtrCarAcq(), 15, 3));
        }
        // COB_CODE: IF DTR-CAR-GEST-NULL = HIGH-VALUES
        //                TO (SF)-CAR-GEST-NULL(IX-TAB-DTR)
        //           ELSE
        //                TO (SF)-CAR-GEST(IX-TAB-DTR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getDettTitDiRat().getDtrCarGest().getDtrCarGestNullFormatted())) {
            // COB_CODE: MOVE DTR-CAR-GEST-NULL
            //             TO (SF)-CAR-GEST-NULL(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrCarGest().setWdtrCarGestNull(ws.getDettTitDiRat().getDtrCarGest().getDtrCarGestNull());
        }
        else {
            // COB_CODE: MOVE DTR-CAR-GEST
            //             TO (SF)-CAR-GEST(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrCarGest().setWdtrCarGest(Trunc.toDecimal(ws.getDettTitDiRat().getDtrCarGest().getDtrCarGest(), 15, 3));
        }
        // COB_CODE: IF DTR-CAR-INC-NULL = HIGH-VALUES
        //                TO (SF)-CAR-INC-NULL(IX-TAB-DTR)
        //           ELSE
        //                TO (SF)-CAR-INC(IX-TAB-DTR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getDettTitDiRat().getDtrCarInc().getDtrCarIncNullFormatted())) {
            // COB_CODE: MOVE DTR-CAR-INC-NULL
            //             TO (SF)-CAR-INC-NULL(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrCarInc().setWdtrCarIncNull(ws.getDettTitDiRat().getDtrCarInc().getDtrCarIncNull());
        }
        else {
            // COB_CODE: MOVE DTR-CAR-INC
            //             TO (SF)-CAR-INC(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrCarInc().setWdtrCarInc(Trunc.toDecimal(ws.getDettTitDiRat().getDtrCarInc().getDtrCarInc(), 15, 3));
        }
        // COB_CODE: IF DTR-MANFEE-ANTIC-NULL = HIGH-VALUES
        //                TO (SF)-MANFEE-ANTIC-NULL(IX-TAB-DTR)
        //           ELSE
        //                TO (SF)-MANFEE-ANTIC(IX-TAB-DTR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getDettTitDiRat().getDtrManfeeAntic().getDtrManfeeAnticNullFormatted())) {
            // COB_CODE: MOVE DTR-MANFEE-ANTIC-NULL
            //             TO (SF)-MANFEE-ANTIC-NULL(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrManfeeAntic().setWdtrManfeeAnticNull(ws.getDettTitDiRat().getDtrManfeeAntic().getDtrManfeeAnticNull());
        }
        else {
            // COB_CODE: MOVE DTR-MANFEE-ANTIC
            //             TO (SF)-MANFEE-ANTIC(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrManfeeAntic().setWdtrManfeeAntic(Trunc.toDecimal(ws.getDettTitDiRat().getDtrManfeeAntic().getDtrManfeeAntic(), 15, 3));
        }
        // COB_CODE: IF DTR-MANFEE-RICOR-NULL = HIGH-VALUES
        //                TO (SF)-MANFEE-RICOR-NULL(IX-TAB-DTR)
        //           ELSE
        //                TO (SF)-MANFEE-RICOR(IX-TAB-DTR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getDettTitDiRat().getDtrManfeeRicor().getDtrManfeeRicorNullFormatted())) {
            // COB_CODE: MOVE DTR-MANFEE-RICOR-NULL
            //             TO (SF)-MANFEE-RICOR-NULL(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrManfeeRicor().setWdtrManfeeRicorNull(ws.getDettTitDiRat().getDtrManfeeRicor().getDtrManfeeRicorNull());
        }
        else {
            // COB_CODE: MOVE DTR-MANFEE-RICOR
            //             TO (SF)-MANFEE-RICOR(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrManfeeRicor().setWdtrManfeeRicor(Trunc.toDecimal(ws.getDettTitDiRat().getDtrManfeeRicor().getDtrManfeeRicor(), 15, 3));
        }
        // COB_CODE: IF DTR-MANFEE-REC-NULL = HIGH-VALUES
        //                TO (SF)-MANFEE-REC-NULL(IX-TAB-DTR)
        //           ELSE
        //                TO (SF)-MANFEE-REC(IX-TAB-DTR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getDettTitDiRat().getDtrManfeeRecFormatted())) {
            // COB_CODE: MOVE DTR-MANFEE-REC-NULL
            //             TO (SF)-MANFEE-REC-NULL(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().setWdtrManfeeRec(ws.getDettTitDiRat().getDtrManfeeRec());
        }
        else {
            // COB_CODE: MOVE DTR-MANFEE-REC
            //             TO (SF)-MANFEE-REC(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().setWdtrManfeeRec(ws.getDettTitDiRat().getDtrManfeeRec());
        }
        // COB_CODE: IF DTR-TOT-INTR-PREST-NULL = HIGH-VALUES
        //                TO (SF)-TOT-INTR-PREST-NULL(IX-TAB-DTR)
        //           ELSE
        //                TO (SF)-TOT-INTR-PREST(IX-TAB-DTR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getDettTitDiRat().getDtrTotIntrPrest().getDtrTotIntrPrestNullFormatted())) {
            // COB_CODE: MOVE DTR-TOT-INTR-PREST-NULL
            //             TO (SF)-TOT-INTR-PREST-NULL(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrTotIntrPrest().setWdtrTotIntrPrestNull(ws.getDettTitDiRat().getDtrTotIntrPrest().getDtrTotIntrPrestNull());
        }
        else {
            // COB_CODE: MOVE DTR-TOT-INTR-PREST
            //             TO (SF)-TOT-INTR-PREST(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrTotIntrPrest().setWdtrTotIntrPrest(Trunc.toDecimal(ws.getDettTitDiRat().getDtrTotIntrPrest().getDtrTotIntrPrest(), 15, 3));
        }
        // COB_CODE: MOVE DTR-DS-RIGA
        //             TO (SF)-DS-RIGA(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().setWdtrDsRiga(ws.getDettTitDiRat().getDtrDsRiga());
        // COB_CODE: MOVE DTR-DS-OPER-SQL
        //             TO (SF)-DS-OPER-SQL(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().setWdtrDsOperSql(ws.getDettTitDiRat().getDtrDsOperSql());
        // COB_CODE: MOVE DTR-DS-VER
        //             TO (SF)-DS-VER(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().setWdtrDsVer(ws.getDettTitDiRat().getDtrDsVer());
        // COB_CODE: MOVE DTR-DS-TS-INI-CPTZ
        //             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().setWdtrDsTsIniCptz(ws.getDettTitDiRat().getDtrDsTsIniCptz());
        // COB_CODE: MOVE DTR-DS-TS-END-CPTZ
        //             TO (SF)-DS-TS-END-CPTZ(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().setWdtrDsTsEndCptz(ws.getDettTitDiRat().getDtrDsTsEndCptz());
        // COB_CODE: MOVE DTR-DS-UTENTE
        //             TO (SF)-DS-UTENTE(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().setWdtrDsUtente(ws.getDettTitDiRat().getDtrDsUtente());
        // COB_CODE: MOVE DTR-DS-STATO-ELAB
        //             TO (SF)-DS-STATO-ELAB(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().setWdtrDsStatoElab(ws.getDettTitDiRat().getDtrDsStatoElab());
        // COB_CODE: IF DTR-IMP-TRASFE-NULL = HIGH-VALUES
        //                TO (SF)-IMP-TRASFE-NULL(IX-TAB-DTR)
        //           ELSE
        //                TO (SF)-IMP-TRASFE(IX-TAB-DTR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getDettTitDiRat().getDtrImpTrasfe().getDtrImpTrasfeNullFormatted())) {
            // COB_CODE: MOVE DTR-IMP-TRASFE-NULL
            //             TO (SF)-IMP-TRASFE-NULL(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrImpTrasfe().setWdtrImpTrasfeNull(ws.getDettTitDiRat().getDtrImpTrasfe().getDtrImpTrasfeNull());
        }
        else {
            // COB_CODE: MOVE DTR-IMP-TRASFE
            //             TO (SF)-IMP-TRASFE(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrImpTrasfe().setWdtrImpTrasfe(Trunc.toDecimal(ws.getDettTitDiRat().getDtrImpTrasfe().getDtrImpTrasfe(), 15, 3));
        }
        // COB_CODE: IF DTR-IMP-TFR-STRC-NULL = HIGH-VALUES
        //                TO (SF)-IMP-TFR-STRC-NULL(IX-TAB-DTR)
        //           ELSE
        //                TO (SF)-IMP-TFR-STRC(IX-TAB-DTR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getDettTitDiRat().getDtrImpTfrStrc().getDtrImpTfrStrcNullFormatted())) {
            // COB_CODE: MOVE DTR-IMP-TFR-STRC-NULL
            //             TO (SF)-IMP-TFR-STRC-NULL(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrImpTfrStrc().setWdtrImpTfrStrcNull(ws.getDettTitDiRat().getDtrImpTfrStrc().getDtrImpTfrStrcNull());
        }
        else {
            // COB_CODE: MOVE DTR-IMP-TFR-STRC
            //             TO (SF)-IMP-TFR-STRC(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrImpTfrStrc().setWdtrImpTfrStrc(Trunc.toDecimal(ws.getDettTitDiRat().getDtrImpTfrStrc().getDtrImpTfrStrc(), 15, 3));
        }
        // COB_CODE: IF DTR-ACQ-EXP-NULL = HIGH-VALUES
        //                TO (SF)-ACQ-EXP-NULL(IX-TAB-DTR)
        //           ELSE
        //                TO (SF)-ACQ-EXP(IX-TAB-DTR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getDettTitDiRat().getDtrAcqExp().getDtrAcqExpNullFormatted())) {
            // COB_CODE: MOVE DTR-ACQ-EXP-NULL
            //             TO (SF)-ACQ-EXP-NULL(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrAcqExp().setWdtrAcqExpNull(ws.getDettTitDiRat().getDtrAcqExp().getDtrAcqExpNull());
        }
        else {
            // COB_CODE: MOVE DTR-ACQ-EXP
            //             TO (SF)-ACQ-EXP(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrAcqExp().setWdtrAcqExp(Trunc.toDecimal(ws.getDettTitDiRat().getDtrAcqExp().getDtrAcqExp(), 15, 3));
        }
        // COB_CODE: IF DTR-REMUN-ASS-NULL = HIGH-VALUES
        //                TO (SF)-REMUN-ASS-NULL(IX-TAB-DTR)
        //           ELSE
        //                TO (SF)-REMUN-ASS(IX-TAB-DTR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getDettTitDiRat().getDtrRemunAss().getDtrRemunAssNullFormatted())) {
            // COB_CODE: MOVE DTR-REMUN-ASS-NULL
            //             TO (SF)-REMUN-ASS-NULL(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrRemunAss().setWdtrRemunAssNull(ws.getDettTitDiRat().getDtrRemunAss().getDtrRemunAssNull());
        }
        else {
            // COB_CODE: MOVE DTR-REMUN-ASS
            //             TO (SF)-REMUN-ASS(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrRemunAss().setWdtrRemunAss(Trunc.toDecimal(ws.getDettTitDiRat().getDtrRemunAss().getDtrRemunAss(), 15, 3));
        }
        // COB_CODE: IF DTR-COMMIS-INTER-NULL = HIGH-VALUES
        //                TO (SF)-COMMIS-INTER-NULL(IX-TAB-DTR)
        //           ELSE
        //                TO (SF)-COMMIS-INTER(IX-TAB-DTR)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getDettTitDiRat().getDtrCommisInter().getDtrCommisInterNullFormatted())) {
            // COB_CODE: MOVE DTR-COMMIS-INTER-NULL
            //             TO (SF)-COMMIS-INTER-NULL(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrCommisInter().setWdtrCommisInterNull(ws.getDettTitDiRat().getDtrCommisInter().getDtrCommisInterNull());
        }
        else {
            // COB_CODE: MOVE DTR-COMMIS-INTER
            //             TO (SF)-COMMIS-INTER(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrCommisInter().setWdtrCommisInter(Trunc.toDecimal(ws.getDettTitDiRat().getDtrCommisInter().getDtrCommisInter(), 15, 3));
        }
        // COB_CODE: IF DTR-CNBT-ANTIRAC-NULL = HIGH-VALUES
        //                TO (SF)-CNBT-ANTIRAC-NULL(IX-TAB-DTR)
        //           ELSE
        //                TO (SF)-CNBT-ANTIRAC(IX-TAB-DTR)
        //           END-IF.
        if (Characters.EQ_HIGH.test(ws.getDettTitDiRat().getDtrCnbtAntirac().getDtrCnbtAntiracNullFormatted())) {
            // COB_CODE: MOVE DTR-CNBT-ANTIRAC-NULL
            //             TO (SF)-CNBT-ANTIRAC-NULL(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrCnbtAntirac().setWdtrCnbtAntiracNull(ws.getDettTitDiRat().getDtrCnbtAntirac().getDtrCnbtAntiracNull());
        }
        else {
            // COB_CODE: MOVE DTR-CNBT-ANTIRAC
            //             TO (SF)-CNBT-ANTIRAC(IX-TAB-DTR)
            ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrCnbtAntirac().setWdtrCnbtAntirac(Trunc.toDecimal(ws.getDettTitDiRat().getDtrCnbtAntirac().getDtrCnbtAntirac(), 15, 3));
        }
    }

    /**Original name: INIZIA-TOT-DTR<br>
	 * <pre>------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    INIZIALIZZAZIONE CAMPI NULL COPY LCCVDTR4
	 *    ULTIMO AGG. 28 NOV 2014
	 * ------------------------------------------------------------</pre>*/
    private void iniziaTotDtr() {
        // COB_CODE: PERFORM INIZIA-ZEROES-DTR THRU INIZIA-ZEROES-DTR-EX
        iniziaZeroesDtr();
        // COB_CODE: PERFORM INIZIA-SPACES-DTR THRU INIZIA-SPACES-DTR-EX
        iniziaSpacesDtr();
        // COB_CODE: PERFORM INIZIA-NULL-DTR THRU INIZIA-NULL-DTR-EX.
        iniziaNullDtr();
    }

    /**Original name: INIZIA-NULL-DTR<br>*/
    private void iniziaNullDtr() {
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrIdMoviChiu().setWdtrIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WdtrIdMoviChiu.Len.WDTR_ID_MOVI_CHIU_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-INI-COP-NULL(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrDtIniCop().setWdtrDtIniCopNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WdtrDtIniCop.Len.WDTR_DT_INI_COP_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-END-COP-NULL(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrDtEndCop().setWdtrDtEndCopNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WdtrDtEndCop.Len.WDTR_DT_END_COP_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PRE-NET-NULL(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrPreNet().setWdtrPreNetNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WdtrPreNet.Len.WDTR_PRE_NET_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-INTR-FRAZ-NULL(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrIntrFraz().setWdtrIntrFrazNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WdtrIntrFraz.Len.WDTR_INTR_FRAZ_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-INTR-MORA-NULL(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrIntrMora().setWdtrIntrMoraNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WdtrIntrMora.Len.WDTR_INTR_MORA_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-INTR-RETDT-NULL(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrIntrRetdt().setWdtrIntrRetdtNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WdtrIntrRetdt.Len.WDTR_INTR_RETDT_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-INTR-RIAT-NULL(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrIntrRiat().setWdtrIntrRiatNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WdtrIntrRiat.Len.WDTR_INTR_RIAT_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DIR-NULL(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrDir().setWdtrDirNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WdtrDir.Len.WDTR_DIR_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-SPE-MED-NULL(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrSpeMed().setWdtrSpeMedNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WdtrSpeMed.Len.WDTR_SPE_MED_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-SPE-AGE-NULL(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrSpeAge().setWdtrSpeAgeNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WdtrSpeAge.Len.WDTR_SPE_AGE_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TAX-NULL(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrTax().setWdtrTaxNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WdtrTax.Len.WDTR_TAX_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-SOPR-SAN-NULL(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrSoprSan().setWdtrSoprSanNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WdtrSoprSan.Len.WDTR_SOPR_SAN_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-SOPR-SPO-NULL(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrSoprSpo().setWdtrSoprSpoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WdtrSoprSpo.Len.WDTR_SOPR_SPO_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-SOPR-TEC-NULL(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrSoprTec().setWdtrSoprTecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WdtrSoprTec.Len.WDTR_SOPR_TEC_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-SOPR-PROF-NULL(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrSoprProf().setWdtrSoprProfNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WdtrSoprProf.Len.WDTR_SOPR_PROF_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-SOPR-ALT-NULL(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrSoprAlt().setWdtrSoprAltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WdtrSoprAlt.Len.WDTR_SOPR_ALT_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PRE-TOT-NULL(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrPreTot().setWdtrPreTotNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WdtrPreTot.Len.WDTR_PRE_TOT_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PRE-PP-IAS-NULL(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrPrePpIas().setWdtrPrePpIasNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WdtrPrePpIas.Len.WDTR_PRE_PP_IAS_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PRE-SOLO-RSH-NULL(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrPreSoloRsh().setWdtrPreSoloRshNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WdtrPreSoloRsh.Len.WDTR_PRE_SOLO_RSH_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-CAR-IAS-NULL(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrCarIas().setWdtrCarIasNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WdtrCarIas.Len.WDTR_CAR_IAS_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PROV-ACQ-1AA-NULL(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrProvAcq1aa().setWdtrProvAcq1aaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WdtrProvAcq1aa.Len.WDTR_PROV_ACQ1AA_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PROV-ACQ-2AA-NULL(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrProvAcq2aa().setWdtrProvAcq2aaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WdtrProvAcq2aa.Len.WDTR_PROV_ACQ2AA_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PROV-RICOR-NULL(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrProvRicor().setWdtrProvRicorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WdtrProvRicor.Len.WDTR_PROV_RICOR_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PROV-INC-NULL(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrProvInc().setWdtrProvIncNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WdtrProvInc.Len.WDTR_PROV_INC_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PROV-DA-REC-NULL(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrProvDaRec().setWdtrProvDaRecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WdtrProvDaRec.Len.WDTR_PROV_DA_REC_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-COD-DVS-NULL(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().setWdtrCodDvs(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WdtrDati.Len.WDTR_COD_DVS));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-FRQ-MOVI-NULL(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrFrqMovi().setWdtrFrqMoviNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WdtrFrqMovi.Len.WDTR_FRQ_MOVI_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-COD-TARI-NULL(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().setWdtrCodTari(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WdtrDati.Len.WDTR_COD_TARI));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMP-AZ-NULL(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrImpAz().setWdtrImpAzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WdtrImpAz.Len.WDTR_IMP_AZ_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMP-ADER-NULL(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrImpAder().setWdtrImpAderNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WdtrImpAder.Len.WDTR_IMP_ADER_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMP-TFR-NULL(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrImpTfr().setWdtrImpTfrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WdtrImpTfr.Len.WDTR_IMP_TFR_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMP-VOLO-NULL(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrImpVolo().setWdtrImpVoloNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WdtrImpVolo.Len.WDTR_IMP_VOLO_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-FL-VLDT-TIT-NULL(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().setWdtrFlVldtTit(Types.HIGH_CHAR_VAL);
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-CAR-ACQ-NULL(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrCarAcq().setWdtrCarAcqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WdtrCarAcq.Len.WDTR_CAR_ACQ_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-CAR-GEST-NULL(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrCarGest().setWdtrCarGestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WdtrCarGest.Len.WDTR_CAR_GEST_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-CAR-INC-NULL(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrCarInc().setWdtrCarIncNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WdtrCarInc.Len.WDTR_CAR_INC_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-MANFEE-ANTIC-NULL(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrManfeeAntic().setWdtrManfeeAnticNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WdtrManfeeAntic.Len.WDTR_MANFEE_ANTIC_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-MANFEE-RICOR-NULL(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrManfeeRicor().setWdtrManfeeRicorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WdtrManfeeRicor.Len.WDTR_MANFEE_RICOR_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-MANFEE-REC-NULL(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().setWdtrManfeeRec(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WdtrDati.Len.WDTR_MANFEE_REC));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-INTR-PREST-NULL(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrTotIntrPrest().setWdtrTotIntrPrestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WdtrTotIntrPrest.Len.WDTR_TOT_INTR_PREST_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMP-TRASFE-NULL(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrImpTrasfe().setWdtrImpTrasfeNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WdtrImpTrasfe.Len.WDTR_IMP_TRASFE_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMP-TFR-STRC-NULL(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrImpTfrStrc().setWdtrImpTfrStrcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WdtrImpTfrStrc.Len.WDTR_IMP_TFR_STRC_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-ACQ-EXP-NULL(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrAcqExp().setWdtrAcqExpNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WdtrAcqExp.Len.WDTR_ACQ_EXP_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-REMUN-ASS-NULL(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrRemunAss().setWdtrRemunAssNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WdtrRemunAss.Len.WDTR_REMUN_ASS_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-COMMIS-INTER-NULL(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrCommisInter().setWdtrCommisInterNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WdtrCommisInter.Len.WDTR_COMMIS_INTER_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-CNBT-ANTIRAC-NULL(IX-TAB-DTR).
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrCnbtAntirac().setWdtrCnbtAntiracNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WdtrCnbtAntirac.Len.WDTR_CNBT_ANTIRAC_NULL));
    }

    /**Original name: INIZIA-ZEROES-DTR<br>*/
    private void iniziaZeroesDtr() {
        // COB_CODE: MOVE 0 TO (SF)-ID-DETT-TIT-DI-RAT(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().setWdtrIdDettTitDiRat(0);
        // COB_CODE: MOVE 0 TO (SF)-ID-TIT-RAT(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().setWdtrIdTitRat(0);
        // COB_CODE: MOVE 0 TO (SF)-ID-OGG(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().setWdtrIdOgg(0);
        // COB_CODE: MOVE 0 TO (SF)-ID-MOVI-CRZ(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().setWdtrIdMoviCrz(0);
        // COB_CODE: MOVE 0 TO (SF)-DT-INI-EFF(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().setWdtrDtIniEff(0);
        // COB_CODE: MOVE 0 TO (SF)-DT-END-EFF(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().setWdtrDtEndEff(0);
        // COB_CODE: MOVE 0 TO (SF)-COD-COMP-ANIA(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().setWdtrCodCompAnia(0);
        // COB_CODE: MOVE 0 TO (SF)-DS-RIGA(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().setWdtrDsRiga(0);
        // COB_CODE: MOVE 0 TO (SF)-DS-VER(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().setWdtrDsVer(0);
        // COB_CODE: MOVE 0 TO (SF)-DS-TS-INI-CPTZ(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().setWdtrDsTsIniCptz(0);
        // COB_CODE: MOVE 0 TO (SF)-DS-TS-END-CPTZ(IX-TAB-DTR).
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().setWdtrDsTsEndCptz(0);
    }

    /**Original name: INIZIA-SPACES-DTR<br>*/
    private void iniziaSpacesDtr() {
        // COB_CODE: MOVE SPACES TO (SF)-TP-OGG(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().setWdtrTpOgg("");
        // COB_CODE: MOVE SPACES TO (SF)-TP-RGM-FISC(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().setWdtrTpRgmFisc("");
        // COB_CODE: MOVE SPACES TO (SF)-TP-STAT-TIT(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().setWdtrTpStatTit("");
        // COB_CODE: MOVE SPACES TO (SF)-DS-OPER-SQL(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().setWdtrDsOperSql(Types.SPACE_CHAR);
        // COB_CODE: MOVE SPACES TO (SF)-DS-UTENTE(IX-TAB-DTR)
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().setWdtrDsUtente("");
        // COB_CODE: MOVE SPACES TO (SF)-DS-STATO-ELAB(IX-TAB-DTR).
        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().setWdtrDsStatoElab(Types.SPACE_CHAR);
    }

    /**Original name: VAL-DCLGEN-TGA<br>
	 * <pre>----------------------------------------------------------------*
	 *      CONTIENE STATEMENTS PER LA FASE DI EOC
	 * ----------------------------------------------------------------*
	 * ------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    COPY LCCVTGA5
	 *    ULTIMO AGG. 03 GIU 2019
	 * ------------------------------------------------------------</pre>*/
    private void valDclgenTga() {
        // COB_CODE: IF (SF)-ID-MOVI-CHIU-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-ID-MOVI-CHIU-NULL
        //           ELSE
        //              TO TGA-ID-MOVI-CHIU
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaIdMoviChiu().getWtgaIdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE (SF)-ID-MOVI-CHIU-NULL(IX-TAB-TGA)
            //           TO TGA-ID-MOVI-CHIU-NULL
            ws.getTrchDiGar().getTgaIdMoviChiu().setTgaIdMoviChiuNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaIdMoviChiu().getWtgaIdMoviChiuNull());
        }
        else {
            // COB_CODE: MOVE (SF)-ID-MOVI-CHIU(IX-TAB-TGA)
            //           TO TGA-ID-MOVI-CHIU
            ws.getTrchDiGar().getTgaIdMoviChiu().setTgaIdMoviChiu(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaIdMoviChiu().getWtgaIdMoviChiu());
        }
        // COB_CODE: IF (SF)-DT-INI-EFF(IX-TAB-TGA) NOT NUMERIC
        //              MOVE 0 TO TGA-DT-INI-EFF
        //           ELSE
        //              TO TGA-DT-INI-EFF
        //           END-IF
        if (!Functions.isNumber(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaDtIniEff())) {
            // COB_CODE: MOVE 0 TO TGA-DT-INI-EFF
            ws.getTrchDiGar().setTgaDtIniEff(0);
        }
        else {
            // COB_CODE: MOVE (SF)-DT-INI-EFF(IX-TAB-TGA)
            //           TO TGA-DT-INI-EFF
            ws.getTrchDiGar().setTgaDtIniEff(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaDtIniEff());
        }
        // COB_CODE: IF (SF)-DT-END-EFF(IX-TAB-TGA) NOT NUMERIC
        //              MOVE 0 TO TGA-DT-END-EFF
        //           ELSE
        //              TO TGA-DT-END-EFF
        //           END-IF
        if (!Functions.isNumber(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaDtEndEff())) {
            // COB_CODE: MOVE 0 TO TGA-DT-END-EFF
            ws.getTrchDiGar().setTgaDtEndEff(0);
        }
        else {
            // COB_CODE: MOVE (SF)-DT-END-EFF(IX-TAB-TGA)
            //           TO TGA-DT-END-EFF
            ws.getTrchDiGar().setTgaDtEndEff(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaDtEndEff());
        }
        // COB_CODE: MOVE (SF)-COD-COMP-ANIA(IX-TAB-TGA)
        //              TO TGA-COD-COMP-ANIA
        ws.getTrchDiGar().setTgaCodCompAnia(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaCodCompAnia());
        // COB_CODE: MOVE (SF)-DT-DECOR(IX-TAB-TGA)
        //              TO TGA-DT-DECOR
        ws.getTrchDiGar().setTgaDtDecor(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaDtDecor());
        // COB_CODE: IF (SF)-DT-SCAD-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-DT-SCAD-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaDtScad().getWtgaDtScadNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-SCAD-NULL(IX-TAB-TGA)
            //           TO TGA-DT-SCAD-NULL
            ws.getTrchDiGar().getTgaDtScad().setTgaDtScadNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaDtScad().getWtgaDtScadNull());
        }
        else if (wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaDtScad().getWtgaDtScad() == 0) {
            // COB_CODE: IF (SF)-DT-SCAD(IX-TAB-TGA) = ZERO
            //              TO TGA-DT-SCAD-NULL
            //           ELSE
            //            TO TGA-DT-SCAD
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO TGA-DT-SCAD-NULL
            ws.getTrchDiGar().getTgaDtScad().setTgaDtScadNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaDtScad.Len.TGA_DT_SCAD_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-SCAD(IX-TAB-TGA)
            //           TO TGA-DT-SCAD
            ws.getTrchDiGar().getTgaDtScad().setTgaDtScad(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaDtScad().getWtgaDtScad());
        }
        // COB_CODE: IF (SF)-IB-OGG-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-IB-OGG-NULL
        //           ELSE
        //              TO TGA-IB-OGG
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaIbOgg(), WtgaDati.Len.WTGA_IB_OGG)) {
            // COB_CODE: MOVE (SF)-IB-OGG-NULL(IX-TAB-TGA)
            //           TO TGA-IB-OGG-NULL
            ws.getTrchDiGar().setTgaIbOgg(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaIbOgg());
        }
        else {
            // COB_CODE: MOVE (SF)-IB-OGG(IX-TAB-TGA)
            //           TO TGA-IB-OGG
            ws.getTrchDiGar().setTgaIbOgg(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaIbOgg());
        }
        // COB_CODE: MOVE (SF)-TP-RGM-FISC(IX-TAB-TGA)
        //              TO TGA-TP-RGM-FISC
        ws.getTrchDiGar().setTgaTpRgmFisc(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaTpRgmFisc());
        // COB_CODE: IF (SF)-DT-EMIS-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-DT-EMIS-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaDtEmis().getWtgaDtEmisNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-EMIS-NULL(IX-TAB-TGA)
            //           TO TGA-DT-EMIS-NULL
            ws.getTrchDiGar().getTgaDtEmis().setTgaDtEmisNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaDtEmis().getWtgaDtEmisNull());
        }
        else if (wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaDtEmis().getWtgaDtEmis() == 0) {
            // COB_CODE: IF (SF)-DT-EMIS(IX-TAB-TGA) = ZERO
            //              TO TGA-DT-EMIS-NULL
            //           ELSE
            //            TO TGA-DT-EMIS
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO TGA-DT-EMIS-NULL
            ws.getTrchDiGar().getTgaDtEmis().setTgaDtEmisNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaDtEmis.Len.TGA_DT_EMIS_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-EMIS(IX-TAB-TGA)
            //           TO TGA-DT-EMIS
            ws.getTrchDiGar().getTgaDtEmis().setTgaDtEmis(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaDtEmis().getWtgaDtEmis());
        }
        // COB_CODE: MOVE (SF)-TP-TRCH(IX-TAB-TGA)
        //              TO TGA-TP-TRCH
        ws.getTrchDiGar().setTgaTpTrch(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaTpTrch());
        // COB_CODE: IF (SF)-DUR-AA-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-DUR-AA-NULL
        //           ELSE
        //              TO TGA-DUR-AA
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaDurAa().getWtgaDurAaNullFormatted())) {
            // COB_CODE: MOVE (SF)-DUR-AA-NULL(IX-TAB-TGA)
            //           TO TGA-DUR-AA-NULL
            ws.getTrchDiGar().getTgaDurAa().setTgaDurAaNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaDurAa().getWtgaDurAaNull());
        }
        else {
            // COB_CODE: MOVE (SF)-DUR-AA(IX-TAB-TGA)
            //           TO TGA-DUR-AA
            ws.getTrchDiGar().getTgaDurAa().setTgaDurAa(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaDurAa().getWtgaDurAa());
        }
        // COB_CODE: IF (SF)-DUR-MM-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-DUR-MM-NULL
        //           ELSE
        //              TO TGA-DUR-MM
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaDurMm().getWtgaDurMmNullFormatted())) {
            // COB_CODE: MOVE (SF)-DUR-MM-NULL(IX-TAB-TGA)
            //           TO TGA-DUR-MM-NULL
            ws.getTrchDiGar().getTgaDurMm().setTgaDurMmNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaDurMm().getWtgaDurMmNull());
        }
        else {
            // COB_CODE: MOVE (SF)-DUR-MM(IX-TAB-TGA)
            //           TO TGA-DUR-MM
            ws.getTrchDiGar().getTgaDurMm().setTgaDurMm(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaDurMm().getWtgaDurMm());
        }
        // COB_CODE: IF (SF)-DUR-GG-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-DUR-GG-NULL
        //           ELSE
        //              TO TGA-DUR-GG
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaDurGg().getWtgaDurGgNullFormatted())) {
            // COB_CODE: MOVE (SF)-DUR-GG-NULL(IX-TAB-TGA)
            //           TO TGA-DUR-GG-NULL
            ws.getTrchDiGar().getTgaDurGg().setTgaDurGgNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaDurGg().getWtgaDurGgNull());
        }
        else {
            // COB_CODE: MOVE (SF)-DUR-GG(IX-TAB-TGA)
            //           TO TGA-DUR-GG
            ws.getTrchDiGar().getTgaDurGg().setTgaDurGg(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaDurGg().getWtgaDurGg());
        }
        // COB_CODE: IF (SF)-PRE-CASO-MOR-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-PRE-CASO-MOR-NULL
        //           ELSE
        //              TO TGA-PRE-CASO-MOR
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPreCasoMor().getWtgaPreCasoMorNullFormatted())) {
            // COB_CODE: MOVE (SF)-PRE-CASO-MOR-NULL(IX-TAB-TGA)
            //           TO TGA-PRE-CASO-MOR-NULL
            ws.getTrchDiGar().getTgaPreCasoMor().setTgaPreCasoMorNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPreCasoMor().getWtgaPreCasoMorNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PRE-CASO-MOR(IX-TAB-TGA)
            //           TO TGA-PRE-CASO-MOR
            ws.getTrchDiGar().getTgaPreCasoMor().setTgaPreCasoMor(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPreCasoMor().getWtgaPreCasoMor(), 15, 3));
        }
        // COB_CODE: IF (SF)-PC-INTR-RIAT-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-PC-INTR-RIAT-NULL
        //           ELSE
        //              TO TGA-PC-INTR-RIAT
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPcIntrRiat().getWtgaPcIntrRiatNullFormatted())) {
            // COB_CODE: MOVE (SF)-PC-INTR-RIAT-NULL(IX-TAB-TGA)
            //           TO TGA-PC-INTR-RIAT-NULL
            ws.getTrchDiGar().getTgaPcIntrRiat().setTgaPcIntrRiatNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPcIntrRiat().getWtgaPcIntrRiatNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PC-INTR-RIAT(IX-TAB-TGA)
            //           TO TGA-PC-INTR-RIAT
            ws.getTrchDiGar().getTgaPcIntrRiat().setTgaPcIntrRiat(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPcIntrRiat().getWtgaPcIntrRiat(), 6, 3));
        }
        // COB_CODE: IF (SF)-IMP-BNS-ANTIC-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-IMP-BNS-ANTIC-NULL
        //           ELSE
        //              TO TGA-IMP-BNS-ANTIC
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpBnsAntic().getWtgaImpBnsAnticNullFormatted())) {
            // COB_CODE: MOVE (SF)-IMP-BNS-ANTIC-NULL(IX-TAB-TGA)
            //           TO TGA-IMP-BNS-ANTIC-NULL
            ws.getTrchDiGar().getTgaImpBnsAntic().setTgaImpBnsAnticNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpBnsAntic().getWtgaImpBnsAnticNull());
        }
        else {
            // COB_CODE: MOVE (SF)-IMP-BNS-ANTIC(IX-TAB-TGA)
            //           TO TGA-IMP-BNS-ANTIC
            ws.getTrchDiGar().getTgaImpBnsAntic().setTgaImpBnsAntic(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpBnsAntic().getWtgaImpBnsAntic(), 15, 3));
        }
        // COB_CODE: IF (SF)-PRE-INI-NET-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-PRE-INI-NET-NULL
        //           ELSE
        //              TO TGA-PRE-INI-NET
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPreIniNet().getWtgaPreIniNetNullFormatted())) {
            // COB_CODE: MOVE (SF)-PRE-INI-NET-NULL(IX-TAB-TGA)
            //           TO TGA-PRE-INI-NET-NULL
            ws.getTrchDiGar().getTgaPreIniNet().setTgaPreIniNetNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPreIniNet().getWtgaPreIniNetNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PRE-INI-NET(IX-TAB-TGA)
            //           TO TGA-PRE-INI-NET
            ws.getTrchDiGar().getTgaPreIniNet().setTgaPreIniNet(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPreIniNet().getWtgaPreIniNet(), 15, 3));
        }
        // COB_CODE: IF (SF)-PRE-PP-INI-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-PRE-PP-INI-NULL
        //           ELSE
        //              TO TGA-PRE-PP-INI
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPrePpIni().getWtgaPrePpIniNullFormatted())) {
            // COB_CODE: MOVE (SF)-PRE-PP-INI-NULL(IX-TAB-TGA)
            //           TO TGA-PRE-PP-INI-NULL
            ws.getTrchDiGar().getTgaPrePpIni().setTgaPrePpIniNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPrePpIni().getWtgaPrePpIniNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PRE-PP-INI(IX-TAB-TGA)
            //           TO TGA-PRE-PP-INI
            ws.getTrchDiGar().getTgaPrePpIni().setTgaPrePpIni(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPrePpIni().getWtgaPrePpIni(), 15, 3));
        }
        // COB_CODE: IF (SF)-PRE-PP-ULT-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-PRE-PP-ULT-NULL
        //           ELSE
        //              TO TGA-PRE-PP-ULT
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPrePpUlt().getWtgaPrePpUltNullFormatted())) {
            // COB_CODE: MOVE (SF)-PRE-PP-ULT-NULL(IX-TAB-TGA)
            //           TO TGA-PRE-PP-ULT-NULL
            ws.getTrchDiGar().getTgaPrePpUlt().setTgaPrePpUltNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPrePpUlt().getWtgaPrePpUltNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PRE-PP-ULT(IX-TAB-TGA)
            //           TO TGA-PRE-PP-ULT
            ws.getTrchDiGar().getTgaPrePpUlt().setTgaPrePpUlt(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPrePpUlt().getWtgaPrePpUlt(), 15, 3));
        }
        // COB_CODE: IF (SF)-PRE-TARI-INI-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-PRE-TARI-INI-NULL
        //           ELSE
        //              TO TGA-PRE-TARI-INI
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPreTariIni().getWtgaPreTariIniNullFormatted())) {
            // COB_CODE: MOVE (SF)-PRE-TARI-INI-NULL(IX-TAB-TGA)
            //           TO TGA-PRE-TARI-INI-NULL
            ws.getTrchDiGar().getTgaPreTariIni().setTgaPreTariIniNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPreTariIni().getWtgaPreTariIniNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PRE-TARI-INI(IX-TAB-TGA)
            //           TO TGA-PRE-TARI-INI
            ws.getTrchDiGar().getTgaPreTariIni().setTgaPreTariIni(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPreTariIni().getWtgaPreTariIni(), 15, 3));
        }
        // COB_CODE: IF (SF)-PRE-TARI-ULT-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-PRE-TARI-ULT-NULL
        //           ELSE
        //              TO TGA-PRE-TARI-ULT
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPreTariUlt().getWtgaPreTariUltNullFormatted())) {
            // COB_CODE: MOVE (SF)-PRE-TARI-ULT-NULL(IX-TAB-TGA)
            //           TO TGA-PRE-TARI-ULT-NULL
            ws.getTrchDiGar().getTgaPreTariUlt().setTgaPreTariUltNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPreTariUlt().getWtgaPreTariUltNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PRE-TARI-ULT(IX-TAB-TGA)
            //           TO TGA-PRE-TARI-ULT
            ws.getTrchDiGar().getTgaPreTariUlt().setTgaPreTariUlt(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPreTariUlt().getWtgaPreTariUlt(), 15, 3));
        }
        // COB_CODE: IF (SF)-PRE-INVRIO-INI-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-PRE-INVRIO-INI-NULL
        //           ELSE
        //              TO TGA-PRE-INVRIO-INI
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPreInvrioIni().getWtgaPreInvrioIniNullFormatted())) {
            // COB_CODE: MOVE (SF)-PRE-INVRIO-INI-NULL(IX-TAB-TGA)
            //           TO TGA-PRE-INVRIO-INI-NULL
            ws.getTrchDiGar().getTgaPreInvrioIni().setTgaPreInvrioIniNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPreInvrioIni().getWtgaPreInvrioIniNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PRE-INVRIO-INI(IX-TAB-TGA)
            //           TO TGA-PRE-INVRIO-INI
            ws.getTrchDiGar().getTgaPreInvrioIni().setTgaPreInvrioIni(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPreInvrioIni().getWtgaPreInvrioIni(), 15, 3));
        }
        // COB_CODE: IF (SF)-PRE-INVRIO-ULT-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-PRE-INVRIO-ULT-NULL
        //           ELSE
        //              TO TGA-PRE-INVRIO-ULT
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPreInvrioUlt().getWtgaPreInvrioUltNullFormatted())) {
            // COB_CODE: MOVE (SF)-PRE-INVRIO-ULT-NULL(IX-TAB-TGA)
            //           TO TGA-PRE-INVRIO-ULT-NULL
            ws.getTrchDiGar().getTgaPreInvrioUlt().setTgaPreInvrioUltNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPreInvrioUlt().getWtgaPreInvrioUltNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PRE-INVRIO-ULT(IX-TAB-TGA)
            //           TO TGA-PRE-INVRIO-ULT
            ws.getTrchDiGar().getTgaPreInvrioUlt().setTgaPreInvrioUlt(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPreInvrioUlt().getWtgaPreInvrioUlt(), 15, 3));
        }
        // COB_CODE: IF (SF)-PRE-RIVTO-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-PRE-RIVTO-NULL
        //           ELSE
        //              TO TGA-PRE-RIVTO
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPreRivto().getWtgaPreRivtoNullFormatted())) {
            // COB_CODE: MOVE (SF)-PRE-RIVTO-NULL(IX-TAB-TGA)
            //           TO TGA-PRE-RIVTO-NULL
            ws.getTrchDiGar().getTgaPreRivto().setTgaPreRivtoNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPreRivto().getWtgaPreRivtoNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PRE-RIVTO(IX-TAB-TGA)
            //           TO TGA-PRE-RIVTO
            ws.getTrchDiGar().getTgaPreRivto().setTgaPreRivto(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPreRivto().getWtgaPreRivto(), 15, 3));
        }
        // COB_CODE: IF (SF)-IMP-SOPR-PROF-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-IMP-SOPR-PROF-NULL
        //           ELSE
        //              TO TGA-IMP-SOPR-PROF
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpSoprProf().getWtgaImpSoprProfNullFormatted())) {
            // COB_CODE: MOVE (SF)-IMP-SOPR-PROF-NULL(IX-TAB-TGA)
            //           TO TGA-IMP-SOPR-PROF-NULL
            ws.getTrchDiGar().getTgaImpSoprProf().setTgaImpSoprProfNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpSoprProf().getWtgaImpSoprProfNull());
        }
        else {
            // COB_CODE: MOVE (SF)-IMP-SOPR-PROF(IX-TAB-TGA)
            //           TO TGA-IMP-SOPR-PROF
            ws.getTrchDiGar().getTgaImpSoprProf().setTgaImpSoprProf(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpSoprProf().getWtgaImpSoprProf(), 15, 3));
        }
        // COB_CODE: IF (SF)-IMP-SOPR-SAN-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-IMP-SOPR-SAN-NULL
        //           ELSE
        //              TO TGA-IMP-SOPR-SAN
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpSoprSan().getWtgaImpSoprSanNullFormatted())) {
            // COB_CODE: MOVE (SF)-IMP-SOPR-SAN-NULL(IX-TAB-TGA)
            //           TO TGA-IMP-SOPR-SAN-NULL
            ws.getTrchDiGar().getTgaImpSoprSan().setTgaImpSoprSanNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpSoprSan().getWtgaImpSoprSanNull());
        }
        else {
            // COB_CODE: MOVE (SF)-IMP-SOPR-SAN(IX-TAB-TGA)
            //           TO TGA-IMP-SOPR-SAN
            ws.getTrchDiGar().getTgaImpSoprSan().setTgaImpSoprSan(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpSoprSan().getWtgaImpSoprSan(), 15, 3));
        }
        // COB_CODE: IF (SF)-IMP-SOPR-SPO-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-IMP-SOPR-SPO-NULL
        //           ELSE
        //              TO TGA-IMP-SOPR-SPO
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpSoprSpo().getWtgaImpSoprSpoNullFormatted())) {
            // COB_CODE: MOVE (SF)-IMP-SOPR-SPO-NULL(IX-TAB-TGA)
            //           TO TGA-IMP-SOPR-SPO-NULL
            ws.getTrchDiGar().getTgaImpSoprSpo().setTgaImpSoprSpoNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpSoprSpo().getWtgaImpSoprSpoNull());
        }
        else {
            // COB_CODE: MOVE (SF)-IMP-SOPR-SPO(IX-TAB-TGA)
            //           TO TGA-IMP-SOPR-SPO
            ws.getTrchDiGar().getTgaImpSoprSpo().setTgaImpSoprSpo(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpSoprSpo().getWtgaImpSoprSpo(), 15, 3));
        }
        // COB_CODE: IF (SF)-IMP-SOPR-TEC-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-IMP-SOPR-TEC-NULL
        //           ELSE
        //              TO TGA-IMP-SOPR-TEC
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpSoprTec().getWtgaImpSoprTecNullFormatted())) {
            // COB_CODE: MOVE (SF)-IMP-SOPR-TEC-NULL(IX-TAB-TGA)
            //           TO TGA-IMP-SOPR-TEC-NULL
            ws.getTrchDiGar().getTgaImpSoprTec().setTgaImpSoprTecNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpSoprTec().getWtgaImpSoprTecNull());
        }
        else {
            // COB_CODE: MOVE (SF)-IMP-SOPR-TEC(IX-TAB-TGA)
            //           TO TGA-IMP-SOPR-TEC
            ws.getTrchDiGar().getTgaImpSoprTec().setTgaImpSoprTec(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpSoprTec().getWtgaImpSoprTec(), 15, 3));
        }
        // COB_CODE: IF (SF)-IMP-ALT-SOPR-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-IMP-ALT-SOPR-NULL
        //           ELSE
        //              TO TGA-IMP-ALT-SOPR
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpAltSopr().getWtgaImpAltSoprNullFormatted())) {
            // COB_CODE: MOVE (SF)-IMP-ALT-SOPR-NULL(IX-TAB-TGA)
            //           TO TGA-IMP-ALT-SOPR-NULL
            ws.getTrchDiGar().getTgaImpAltSopr().setTgaImpAltSoprNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpAltSopr().getWtgaImpAltSoprNull());
        }
        else {
            // COB_CODE: MOVE (SF)-IMP-ALT-SOPR(IX-TAB-TGA)
            //           TO TGA-IMP-ALT-SOPR
            ws.getTrchDiGar().getTgaImpAltSopr().setTgaImpAltSopr(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpAltSopr().getWtgaImpAltSopr(), 15, 3));
        }
        // COB_CODE: IF (SF)-PRE-STAB-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-PRE-STAB-NULL
        //           ELSE
        //              TO TGA-PRE-STAB
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPreStab().getWtgaPreStabNullFormatted())) {
            // COB_CODE: MOVE (SF)-PRE-STAB-NULL(IX-TAB-TGA)
            //           TO TGA-PRE-STAB-NULL
            ws.getTrchDiGar().getTgaPreStab().setTgaPreStabNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPreStab().getWtgaPreStabNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PRE-STAB(IX-TAB-TGA)
            //           TO TGA-PRE-STAB
            ws.getTrchDiGar().getTgaPreStab().setTgaPreStab(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPreStab().getWtgaPreStab(), 15, 3));
        }
        // COB_CODE: IF (SF)-DT-EFF-STAB-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-DT-EFF-STAB-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaDtEffStab().getWtgaDtEffStabNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-EFF-STAB-NULL(IX-TAB-TGA)
            //           TO TGA-DT-EFF-STAB-NULL
            ws.getTrchDiGar().getTgaDtEffStab().setTgaDtEffStabNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaDtEffStab().getWtgaDtEffStabNull());
        }
        else if (wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaDtEffStab().getWtgaDtEffStab() == 0) {
            // COB_CODE: IF (SF)-DT-EFF-STAB(IX-TAB-TGA) = ZERO
            //              TO TGA-DT-EFF-STAB-NULL
            //           ELSE
            //            TO TGA-DT-EFF-STAB
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO TGA-DT-EFF-STAB-NULL
            ws.getTrchDiGar().getTgaDtEffStab().setTgaDtEffStabNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaDtEffStab.Len.TGA_DT_EFF_STAB_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-EFF-STAB(IX-TAB-TGA)
            //           TO TGA-DT-EFF-STAB
            ws.getTrchDiGar().getTgaDtEffStab().setTgaDtEffStab(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaDtEffStab().getWtgaDtEffStab());
        }
        // COB_CODE: IF (SF)-TS-RIVAL-FIS-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-TS-RIVAL-FIS-NULL
        //           ELSE
        //              TO TGA-TS-RIVAL-FIS
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaTsRivalFis().getWtgaTsRivalFisNullFormatted())) {
            // COB_CODE: MOVE (SF)-TS-RIVAL-FIS-NULL(IX-TAB-TGA)
            //           TO TGA-TS-RIVAL-FIS-NULL
            ws.getTrchDiGar().getTgaTsRivalFis().setTgaTsRivalFisNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaTsRivalFis().getWtgaTsRivalFisNull());
        }
        else {
            // COB_CODE: MOVE (SF)-TS-RIVAL-FIS(IX-TAB-TGA)
            //           TO TGA-TS-RIVAL-FIS
            ws.getTrchDiGar().getTgaTsRivalFis().setTgaTsRivalFis(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaTsRivalFis().getWtgaTsRivalFis(), 14, 9));
        }
        // COB_CODE: IF (SF)-TS-RIVAL-INDICIZ-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-TS-RIVAL-INDICIZ-NULL
        //           ELSE
        //              TO TGA-TS-RIVAL-INDICIZ
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaTsRivalIndiciz().getWtgaTsRivalIndicizNullFormatted())) {
            // COB_CODE: MOVE (SF)-TS-RIVAL-INDICIZ-NULL(IX-TAB-TGA)
            //           TO TGA-TS-RIVAL-INDICIZ-NULL
            ws.getTrchDiGar().getTgaTsRivalIndiciz().setTgaTsRivalIndicizNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaTsRivalIndiciz().getWtgaTsRivalIndicizNull());
        }
        else {
            // COB_CODE: MOVE (SF)-TS-RIVAL-INDICIZ(IX-TAB-TGA)
            //           TO TGA-TS-RIVAL-INDICIZ
            ws.getTrchDiGar().getTgaTsRivalIndiciz().setTgaTsRivalIndiciz(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaTsRivalIndiciz().getWtgaTsRivalIndiciz(), 14, 9));
        }
        // COB_CODE: IF (SF)-OLD-TS-TEC-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-OLD-TS-TEC-NULL
        //           ELSE
        //              TO TGA-OLD-TS-TEC
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaOldTsTec().getWtgaOldTsTecNullFormatted())) {
            // COB_CODE: MOVE (SF)-OLD-TS-TEC-NULL(IX-TAB-TGA)
            //           TO TGA-OLD-TS-TEC-NULL
            ws.getTrchDiGar().getTgaOldTsTec().setTgaOldTsTecNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaOldTsTec().getWtgaOldTsTecNull());
        }
        else {
            // COB_CODE: MOVE (SF)-OLD-TS-TEC(IX-TAB-TGA)
            //           TO TGA-OLD-TS-TEC
            ws.getTrchDiGar().getTgaOldTsTec().setTgaOldTsTec(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaOldTsTec().getWtgaOldTsTec(), 14, 9));
        }
        // COB_CODE: IF (SF)-RAT-LRD-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-RAT-LRD-NULL
        //           ELSE
        //              TO TGA-RAT-LRD
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaRatLrd().getWtgaRatLrdNullFormatted())) {
            // COB_CODE: MOVE (SF)-RAT-LRD-NULL(IX-TAB-TGA)
            //           TO TGA-RAT-LRD-NULL
            ws.getTrchDiGar().getTgaRatLrd().setTgaRatLrdNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaRatLrd().getWtgaRatLrdNull());
        }
        else {
            // COB_CODE: MOVE (SF)-RAT-LRD(IX-TAB-TGA)
            //           TO TGA-RAT-LRD
            ws.getTrchDiGar().getTgaRatLrd().setTgaRatLrd(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaRatLrd().getWtgaRatLrd(), 15, 3));
        }
        // COB_CODE: IF (SF)-PRE-LRD-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-PRE-LRD-NULL
        //           ELSE
        //              TO TGA-PRE-LRD
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPreLrd().getWtgaPreLrdNullFormatted())) {
            // COB_CODE: MOVE (SF)-PRE-LRD-NULL(IX-TAB-TGA)
            //           TO TGA-PRE-LRD-NULL
            ws.getTrchDiGar().getTgaPreLrd().setTgaPreLrdNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPreLrd().getWtgaPreLrdNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PRE-LRD(IX-TAB-TGA)
            //           TO TGA-PRE-LRD
            ws.getTrchDiGar().getTgaPreLrd().setTgaPreLrd(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPreLrd().getWtgaPreLrd(), 15, 3));
        }
        // COB_CODE: IF (SF)-PRSTZ-INI-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-PRSTZ-INI-NULL
        //           ELSE
        //              TO TGA-PRSTZ-INI
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPrstzIni().getWtgaPrstzIniNullFormatted())) {
            // COB_CODE: MOVE (SF)-PRSTZ-INI-NULL(IX-TAB-TGA)
            //           TO TGA-PRSTZ-INI-NULL
            ws.getTrchDiGar().getTgaPrstzIni().setTgaPrstzIniNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPrstzIni().getWtgaPrstzIniNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PRSTZ-INI(IX-TAB-TGA)
            //           TO TGA-PRSTZ-INI
            ws.getTrchDiGar().getTgaPrstzIni().setTgaPrstzIni(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPrstzIni().getWtgaPrstzIni(), 15, 3));
        }
        // COB_CODE: IF (SF)-PRSTZ-ULT-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-PRSTZ-ULT-NULL
        //           ELSE
        //              TO TGA-PRSTZ-ULT
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPrstzUlt().getWtgaPrstzUltNullFormatted())) {
            // COB_CODE: MOVE (SF)-PRSTZ-ULT-NULL(IX-TAB-TGA)
            //           TO TGA-PRSTZ-ULT-NULL
            ws.getTrchDiGar().getTgaPrstzUlt().setTgaPrstzUltNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPrstzUlt().getWtgaPrstzUltNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PRSTZ-ULT(IX-TAB-TGA)
            //           TO TGA-PRSTZ-ULT
            ws.getTrchDiGar().getTgaPrstzUlt().setTgaPrstzUlt(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPrstzUlt().getWtgaPrstzUlt(), 15, 3));
        }
        // COB_CODE: IF (SF)-CPT-IN-OPZ-RIVTO-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-CPT-IN-OPZ-RIVTO-NULL
        //           ELSE
        //              TO TGA-CPT-IN-OPZ-RIVTO
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaCptInOpzRivto().getWtgaCptInOpzRivtoNullFormatted())) {
            // COB_CODE: MOVE (SF)-CPT-IN-OPZ-RIVTO-NULL(IX-TAB-TGA)
            //           TO TGA-CPT-IN-OPZ-RIVTO-NULL
            ws.getTrchDiGar().getTgaCptInOpzRivto().setTgaCptInOpzRivtoNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaCptInOpzRivto().getWtgaCptInOpzRivtoNull());
        }
        else {
            // COB_CODE: MOVE (SF)-CPT-IN-OPZ-RIVTO(IX-TAB-TGA)
            //           TO TGA-CPT-IN-OPZ-RIVTO
            ws.getTrchDiGar().getTgaCptInOpzRivto().setTgaCptInOpzRivto(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaCptInOpzRivto().getWtgaCptInOpzRivto(), 15, 3));
        }
        // COB_CODE: IF (SF)-PRSTZ-INI-STAB-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-PRSTZ-INI-STAB-NULL
        //           ELSE
        //              TO TGA-PRSTZ-INI-STAB
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPrstzIniStab().getWtgaPrstzIniStabNullFormatted())) {
            // COB_CODE: MOVE (SF)-PRSTZ-INI-STAB-NULL(IX-TAB-TGA)
            //           TO TGA-PRSTZ-INI-STAB-NULL
            ws.getTrchDiGar().getTgaPrstzIniStab().setTgaPrstzIniStabNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPrstzIniStab().getWtgaPrstzIniStabNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PRSTZ-INI-STAB(IX-TAB-TGA)
            //           TO TGA-PRSTZ-INI-STAB
            ws.getTrchDiGar().getTgaPrstzIniStab().setTgaPrstzIniStab(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPrstzIniStab().getWtgaPrstzIniStab(), 15, 3));
        }
        // COB_CODE: IF (SF)-CPT-RSH-MOR-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-CPT-RSH-MOR-NULL
        //           ELSE
        //              TO TGA-CPT-RSH-MOR
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaCptRshMor().getWtgaCptRshMorNullFormatted())) {
            // COB_CODE: MOVE (SF)-CPT-RSH-MOR-NULL(IX-TAB-TGA)
            //           TO TGA-CPT-RSH-MOR-NULL
            ws.getTrchDiGar().getTgaCptRshMor().setTgaCptRshMorNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaCptRshMor().getWtgaCptRshMorNull());
        }
        else {
            // COB_CODE: MOVE (SF)-CPT-RSH-MOR(IX-TAB-TGA)
            //           TO TGA-CPT-RSH-MOR
            ws.getTrchDiGar().getTgaCptRshMor().setTgaCptRshMor(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaCptRshMor().getWtgaCptRshMor(), 15, 3));
        }
        // COB_CODE: IF (SF)-PRSTZ-RID-INI-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-PRSTZ-RID-INI-NULL
        //           ELSE
        //              TO TGA-PRSTZ-RID-INI
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPrstzRidIni().getWtgaPrstzRidIniNullFormatted())) {
            // COB_CODE: MOVE (SF)-PRSTZ-RID-INI-NULL(IX-TAB-TGA)
            //           TO TGA-PRSTZ-RID-INI-NULL
            ws.getTrchDiGar().getTgaPrstzRidIni().setTgaPrstzRidIniNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPrstzRidIni().getWtgaPrstzRidIniNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PRSTZ-RID-INI(IX-TAB-TGA)
            //           TO TGA-PRSTZ-RID-INI
            ws.getTrchDiGar().getTgaPrstzRidIni().setTgaPrstzRidIni(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPrstzRidIni().getWtgaPrstzRidIni(), 15, 3));
        }
        // COB_CODE: IF (SF)-FL-CAR-CONT-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-FL-CAR-CONT-NULL
        //           ELSE
        //              TO TGA-FL-CAR-CONT
        //           END-IF
        if (Conditions.eq(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaFlCarCont(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE (SF)-FL-CAR-CONT-NULL(IX-TAB-TGA)
            //           TO TGA-FL-CAR-CONT-NULL
            ws.getTrchDiGar().setTgaFlCarCont(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaFlCarCont());
        }
        else {
            // COB_CODE: MOVE (SF)-FL-CAR-CONT(IX-TAB-TGA)
            //           TO TGA-FL-CAR-CONT
            ws.getTrchDiGar().setTgaFlCarCont(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaFlCarCont());
        }
        // COB_CODE: IF (SF)-BNS-GIA-LIQTO-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-BNS-GIA-LIQTO-NULL
        //           ELSE
        //              TO TGA-BNS-GIA-LIQTO
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaBnsGiaLiqto().getWtgaBnsGiaLiqtoNullFormatted())) {
            // COB_CODE: MOVE (SF)-BNS-GIA-LIQTO-NULL(IX-TAB-TGA)
            //           TO TGA-BNS-GIA-LIQTO-NULL
            ws.getTrchDiGar().getTgaBnsGiaLiqto().setTgaBnsGiaLiqtoNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaBnsGiaLiqto().getWtgaBnsGiaLiqtoNull());
        }
        else {
            // COB_CODE: MOVE (SF)-BNS-GIA-LIQTO(IX-TAB-TGA)
            //           TO TGA-BNS-GIA-LIQTO
            ws.getTrchDiGar().getTgaBnsGiaLiqto().setTgaBnsGiaLiqto(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaBnsGiaLiqto().getWtgaBnsGiaLiqto(), 15, 3));
        }
        // COB_CODE: IF (SF)-IMP-BNS-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-IMP-BNS-NULL
        //           ELSE
        //              TO TGA-IMP-BNS
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpBns().getWtgaImpBnsNullFormatted())) {
            // COB_CODE: MOVE (SF)-IMP-BNS-NULL(IX-TAB-TGA)
            //           TO TGA-IMP-BNS-NULL
            ws.getTrchDiGar().getTgaImpBns().setTgaImpBnsNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpBns().getWtgaImpBnsNull());
        }
        else {
            // COB_CODE: MOVE (SF)-IMP-BNS(IX-TAB-TGA)
            //           TO TGA-IMP-BNS
            ws.getTrchDiGar().getTgaImpBns().setTgaImpBns(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpBns().getWtgaImpBns(), 15, 3));
        }
        // COB_CODE: MOVE (SF)-COD-DVS(IX-TAB-TGA)
        //              TO TGA-COD-DVS
        ws.getTrchDiGar().setTgaCodDvs(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaCodDvs());
        // COB_CODE: IF (SF)-PRSTZ-INI-NEWFIS-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-PRSTZ-INI-NEWFIS-NULL
        //           ELSE
        //              TO TGA-PRSTZ-INI-NEWFIS
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPrstzIniNewfis().getWtgaPrstzIniNewfisNullFormatted())) {
            // COB_CODE: MOVE (SF)-PRSTZ-INI-NEWFIS-NULL(IX-TAB-TGA)
            //           TO TGA-PRSTZ-INI-NEWFIS-NULL
            ws.getTrchDiGar().getTgaPrstzIniNewfis().setTgaPrstzIniNewfisNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPrstzIniNewfis().getWtgaPrstzIniNewfisNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PRSTZ-INI-NEWFIS(IX-TAB-TGA)
            //           TO TGA-PRSTZ-INI-NEWFIS
            ws.getTrchDiGar().getTgaPrstzIniNewfis().setTgaPrstzIniNewfis(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPrstzIniNewfis().getWtgaPrstzIniNewfis(), 15, 3));
        }
        // COB_CODE: IF (SF)-IMP-SCON-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-IMP-SCON-NULL
        //           ELSE
        //              TO TGA-IMP-SCON
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpScon().getWtgaImpSconNullFormatted())) {
            // COB_CODE: MOVE (SF)-IMP-SCON-NULL(IX-TAB-TGA)
            //           TO TGA-IMP-SCON-NULL
            ws.getTrchDiGar().getTgaImpScon().setTgaImpSconNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpScon().getWtgaImpSconNull());
        }
        else {
            // COB_CODE: MOVE (SF)-IMP-SCON(IX-TAB-TGA)
            //           TO TGA-IMP-SCON
            ws.getTrchDiGar().getTgaImpScon().setTgaImpScon(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpScon().getWtgaImpScon(), 15, 3));
        }
        // COB_CODE: IF (SF)-ALQ-SCON-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-ALQ-SCON-NULL
        //           ELSE
        //              TO TGA-ALQ-SCON
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaAlqScon().getWtgaAlqSconNullFormatted())) {
            // COB_CODE: MOVE (SF)-ALQ-SCON-NULL(IX-TAB-TGA)
            //           TO TGA-ALQ-SCON-NULL
            ws.getTrchDiGar().getTgaAlqScon().setTgaAlqSconNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaAlqScon().getWtgaAlqSconNull());
        }
        else {
            // COB_CODE: MOVE (SF)-ALQ-SCON(IX-TAB-TGA)
            //           TO TGA-ALQ-SCON
            ws.getTrchDiGar().getTgaAlqScon().setTgaAlqScon(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaAlqScon().getWtgaAlqScon(), 6, 3));
        }
        // COB_CODE: IF (SF)-IMP-CAR-ACQ-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-IMP-CAR-ACQ-NULL
        //           ELSE
        //              TO TGA-IMP-CAR-ACQ
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpCarAcq().getWtgaImpCarAcqNullFormatted())) {
            // COB_CODE: MOVE (SF)-IMP-CAR-ACQ-NULL(IX-TAB-TGA)
            //           TO TGA-IMP-CAR-ACQ-NULL
            ws.getTrchDiGar().getTgaImpCarAcq().setTgaImpCarAcqNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpCarAcq().getWtgaImpCarAcqNull());
        }
        else {
            // COB_CODE: MOVE (SF)-IMP-CAR-ACQ(IX-TAB-TGA)
            //           TO TGA-IMP-CAR-ACQ
            ws.getTrchDiGar().getTgaImpCarAcq().setTgaImpCarAcq(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpCarAcq().getWtgaImpCarAcq(), 15, 3));
        }
        // COB_CODE: IF (SF)-IMP-CAR-INC-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-IMP-CAR-INC-NULL
        //           ELSE
        //              TO TGA-IMP-CAR-INC
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpCarInc().getWtgaImpCarIncNullFormatted())) {
            // COB_CODE: MOVE (SF)-IMP-CAR-INC-NULL(IX-TAB-TGA)
            //           TO TGA-IMP-CAR-INC-NULL
            ws.getTrchDiGar().getTgaImpCarInc().setTgaImpCarIncNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpCarInc().getWtgaImpCarIncNull());
        }
        else {
            // COB_CODE: MOVE (SF)-IMP-CAR-INC(IX-TAB-TGA)
            //           TO TGA-IMP-CAR-INC
            ws.getTrchDiGar().getTgaImpCarInc().setTgaImpCarInc(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpCarInc().getWtgaImpCarInc(), 15, 3));
        }
        // COB_CODE: IF (SF)-IMP-CAR-GEST-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-IMP-CAR-GEST-NULL
        //           ELSE
        //              TO TGA-IMP-CAR-GEST
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpCarGest().getWtgaImpCarGestNullFormatted())) {
            // COB_CODE: MOVE (SF)-IMP-CAR-GEST-NULL(IX-TAB-TGA)
            //           TO TGA-IMP-CAR-GEST-NULL
            ws.getTrchDiGar().getTgaImpCarGest().setTgaImpCarGestNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpCarGest().getWtgaImpCarGestNull());
        }
        else {
            // COB_CODE: MOVE (SF)-IMP-CAR-GEST(IX-TAB-TGA)
            //           TO TGA-IMP-CAR-GEST
            ws.getTrchDiGar().getTgaImpCarGest().setTgaImpCarGest(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpCarGest().getWtgaImpCarGest(), 15, 3));
        }
        // COB_CODE: IF (SF)-ETA-AA-1O-ASSTO-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-ETA-AA-1O-ASSTO-NULL
        //           ELSE
        //              TO TGA-ETA-AA-1O-ASSTO
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaEtaAa1oAssto().getWtgaEtaAa1oAsstoNullFormatted())) {
            // COB_CODE: MOVE (SF)-ETA-AA-1O-ASSTO-NULL(IX-TAB-TGA)
            //           TO TGA-ETA-AA-1O-ASSTO-NULL
            ws.getTrchDiGar().getTgaEtaAa1oAssto().setTgaEtaAa1oAsstoNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaEtaAa1oAssto().getWtgaEtaAa1oAsstoNull());
        }
        else {
            // COB_CODE: MOVE (SF)-ETA-AA-1O-ASSTO(IX-TAB-TGA)
            //           TO TGA-ETA-AA-1O-ASSTO
            ws.getTrchDiGar().getTgaEtaAa1oAssto().setTgaEtaAa1oAssto(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaEtaAa1oAssto().getWtgaEtaAa1oAssto());
        }
        // COB_CODE: IF (SF)-ETA-MM-1O-ASSTO-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-ETA-MM-1O-ASSTO-NULL
        //           ELSE
        //              TO TGA-ETA-MM-1O-ASSTO
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaEtaMm1oAssto().getWtgaEtaMm1oAsstoNullFormatted())) {
            // COB_CODE: MOVE (SF)-ETA-MM-1O-ASSTO-NULL(IX-TAB-TGA)
            //           TO TGA-ETA-MM-1O-ASSTO-NULL
            ws.getTrchDiGar().getTgaEtaMm1oAssto().setTgaEtaMm1oAsstoNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaEtaMm1oAssto().getWtgaEtaMm1oAsstoNull());
        }
        else {
            // COB_CODE: MOVE (SF)-ETA-MM-1O-ASSTO(IX-TAB-TGA)
            //           TO TGA-ETA-MM-1O-ASSTO
            ws.getTrchDiGar().getTgaEtaMm1oAssto().setTgaEtaMm1oAssto(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaEtaMm1oAssto().getWtgaEtaMm1oAssto());
        }
        // COB_CODE: IF (SF)-ETA-AA-2O-ASSTO-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-ETA-AA-2O-ASSTO-NULL
        //           ELSE
        //              TO TGA-ETA-AA-2O-ASSTO
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaEtaAa2oAssto().getWtgaEtaAa2oAsstoNullFormatted())) {
            // COB_CODE: MOVE (SF)-ETA-AA-2O-ASSTO-NULL(IX-TAB-TGA)
            //           TO TGA-ETA-AA-2O-ASSTO-NULL
            ws.getTrchDiGar().getTgaEtaAa2oAssto().setTgaEtaAa2oAsstoNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaEtaAa2oAssto().getWtgaEtaAa2oAsstoNull());
        }
        else {
            // COB_CODE: MOVE (SF)-ETA-AA-2O-ASSTO(IX-TAB-TGA)
            //           TO TGA-ETA-AA-2O-ASSTO
            ws.getTrchDiGar().getTgaEtaAa2oAssto().setTgaEtaAa2oAssto(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaEtaAa2oAssto().getWtgaEtaAa2oAssto());
        }
        // COB_CODE: IF (SF)-ETA-MM-2O-ASSTO-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-ETA-MM-2O-ASSTO-NULL
        //           ELSE
        //              TO TGA-ETA-MM-2O-ASSTO
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaEtaMm2oAssto().getWtgaEtaMm2oAsstoNullFormatted())) {
            // COB_CODE: MOVE (SF)-ETA-MM-2O-ASSTO-NULL(IX-TAB-TGA)
            //           TO TGA-ETA-MM-2O-ASSTO-NULL
            ws.getTrchDiGar().getTgaEtaMm2oAssto().setTgaEtaMm2oAsstoNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaEtaMm2oAssto().getWtgaEtaMm2oAsstoNull());
        }
        else {
            // COB_CODE: MOVE (SF)-ETA-MM-2O-ASSTO(IX-TAB-TGA)
            //           TO TGA-ETA-MM-2O-ASSTO
            ws.getTrchDiGar().getTgaEtaMm2oAssto().setTgaEtaMm2oAssto(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaEtaMm2oAssto().getWtgaEtaMm2oAssto());
        }
        // COB_CODE: IF (SF)-ETA-AA-3O-ASSTO-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-ETA-AA-3O-ASSTO-NULL
        //           ELSE
        //              TO TGA-ETA-AA-3O-ASSTO
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaEtaAa3oAssto().getWtgaEtaAa3oAsstoNullFormatted())) {
            // COB_CODE: MOVE (SF)-ETA-AA-3O-ASSTO-NULL(IX-TAB-TGA)
            //           TO TGA-ETA-AA-3O-ASSTO-NULL
            ws.getTrchDiGar().getTgaEtaAa3oAssto().setTgaEtaAa3oAsstoNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaEtaAa3oAssto().getWtgaEtaAa3oAsstoNull());
        }
        else {
            // COB_CODE: MOVE (SF)-ETA-AA-3O-ASSTO(IX-TAB-TGA)
            //           TO TGA-ETA-AA-3O-ASSTO
            ws.getTrchDiGar().getTgaEtaAa3oAssto().setTgaEtaAa3oAssto(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaEtaAa3oAssto().getWtgaEtaAa3oAssto());
        }
        // COB_CODE: IF (SF)-ETA-MM-3O-ASSTO-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-ETA-MM-3O-ASSTO-NULL
        //           ELSE
        //              TO TGA-ETA-MM-3O-ASSTO
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaEtaMm3oAssto().getWtgaEtaMm3oAsstoNullFormatted())) {
            // COB_CODE: MOVE (SF)-ETA-MM-3O-ASSTO-NULL(IX-TAB-TGA)
            //           TO TGA-ETA-MM-3O-ASSTO-NULL
            ws.getTrchDiGar().getTgaEtaMm3oAssto().setTgaEtaMm3oAsstoNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaEtaMm3oAssto().getWtgaEtaMm3oAsstoNull());
        }
        else {
            // COB_CODE: MOVE (SF)-ETA-MM-3O-ASSTO(IX-TAB-TGA)
            //           TO TGA-ETA-MM-3O-ASSTO
            ws.getTrchDiGar().getTgaEtaMm3oAssto().setTgaEtaMm3oAssto(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaEtaMm3oAssto().getWtgaEtaMm3oAssto());
        }
        // COB_CODE: IF (SF)-RENDTO-LRD-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-RENDTO-LRD-NULL
        //           ELSE
        //              TO TGA-RENDTO-LRD
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaRendtoLrd().getWtgaRendtoLrdNullFormatted())) {
            // COB_CODE: MOVE (SF)-RENDTO-LRD-NULL(IX-TAB-TGA)
            //           TO TGA-RENDTO-LRD-NULL
            ws.getTrchDiGar().getTgaRendtoLrd().setTgaRendtoLrdNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaRendtoLrd().getWtgaRendtoLrdNull());
        }
        else {
            // COB_CODE: MOVE (SF)-RENDTO-LRD(IX-TAB-TGA)
            //           TO TGA-RENDTO-LRD
            ws.getTrchDiGar().getTgaRendtoLrd().setTgaRendtoLrd(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaRendtoLrd().getWtgaRendtoLrd(), 14, 9));
        }
        // COB_CODE: IF (SF)-PC-RETR-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-PC-RETR-NULL
        //           ELSE
        //              TO TGA-PC-RETR
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPcRetr().getWtgaPcRetrNullFormatted())) {
            // COB_CODE: MOVE (SF)-PC-RETR-NULL(IX-TAB-TGA)
            //           TO TGA-PC-RETR-NULL
            ws.getTrchDiGar().getTgaPcRetr().setTgaPcRetrNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPcRetr().getWtgaPcRetrNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PC-RETR(IX-TAB-TGA)
            //           TO TGA-PC-RETR
            ws.getTrchDiGar().getTgaPcRetr().setTgaPcRetr(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPcRetr().getWtgaPcRetr(), 6, 3));
        }
        // COB_CODE: IF (SF)-RENDTO-RETR-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-RENDTO-RETR-NULL
        //           ELSE
        //              TO TGA-RENDTO-RETR
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaRendtoRetr().getWtgaRendtoRetrNullFormatted())) {
            // COB_CODE: MOVE (SF)-RENDTO-RETR-NULL(IX-TAB-TGA)
            //           TO TGA-RENDTO-RETR-NULL
            ws.getTrchDiGar().getTgaRendtoRetr().setTgaRendtoRetrNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaRendtoRetr().getWtgaRendtoRetrNull());
        }
        else {
            // COB_CODE: MOVE (SF)-RENDTO-RETR(IX-TAB-TGA)
            //           TO TGA-RENDTO-RETR
            ws.getTrchDiGar().getTgaRendtoRetr().setTgaRendtoRetr(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaRendtoRetr().getWtgaRendtoRetr(), 14, 9));
        }
        // COB_CODE: IF (SF)-MIN-GARTO-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-MIN-GARTO-NULL
        //           ELSE
        //              TO TGA-MIN-GARTO
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaMinGarto().getWtgaMinGartoNullFormatted())) {
            // COB_CODE: MOVE (SF)-MIN-GARTO-NULL(IX-TAB-TGA)
            //           TO TGA-MIN-GARTO-NULL
            ws.getTrchDiGar().getTgaMinGarto().setTgaMinGartoNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaMinGarto().getWtgaMinGartoNull());
        }
        else {
            // COB_CODE: MOVE (SF)-MIN-GARTO(IX-TAB-TGA)
            //           TO TGA-MIN-GARTO
            ws.getTrchDiGar().getTgaMinGarto().setTgaMinGarto(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaMinGarto().getWtgaMinGarto(), 14, 9));
        }
        // COB_CODE: IF (SF)-MIN-TRNUT-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-MIN-TRNUT-NULL
        //           ELSE
        //              TO TGA-MIN-TRNUT
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaMinTrnut().getWtgaMinTrnutNullFormatted())) {
            // COB_CODE: MOVE (SF)-MIN-TRNUT-NULL(IX-TAB-TGA)
            //           TO TGA-MIN-TRNUT-NULL
            ws.getTrchDiGar().getTgaMinTrnut().setTgaMinTrnutNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaMinTrnut().getWtgaMinTrnutNull());
        }
        else {
            // COB_CODE: MOVE (SF)-MIN-TRNUT(IX-TAB-TGA)
            //           TO TGA-MIN-TRNUT
            ws.getTrchDiGar().getTgaMinTrnut().setTgaMinTrnut(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaMinTrnut().getWtgaMinTrnut(), 14, 9));
        }
        // COB_CODE: IF (SF)-PRE-ATT-DI-TRCH-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-PRE-ATT-DI-TRCH-NULL
        //           ELSE
        //              TO TGA-PRE-ATT-DI-TRCH
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPreAttDiTrch().getWtgaPreAttDiTrchNullFormatted())) {
            // COB_CODE: MOVE (SF)-PRE-ATT-DI-TRCH-NULL(IX-TAB-TGA)
            //           TO TGA-PRE-ATT-DI-TRCH-NULL
            ws.getTrchDiGar().getTgaPreAttDiTrch().setTgaPreAttDiTrchNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPreAttDiTrch().getWtgaPreAttDiTrchNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PRE-ATT-DI-TRCH(IX-TAB-TGA)
            //           TO TGA-PRE-ATT-DI-TRCH
            ws.getTrchDiGar().getTgaPreAttDiTrch().setTgaPreAttDiTrch(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPreAttDiTrch().getWtgaPreAttDiTrch(), 15, 3));
        }
        // COB_CODE: IF (SF)-MATU-END2000-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-MATU-END2000-NULL
        //           ELSE
        //              TO TGA-MATU-END2000
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaMatuEnd2000().getWtgaMatuEnd2000NullFormatted())) {
            // COB_CODE: MOVE (SF)-MATU-END2000-NULL(IX-TAB-TGA)
            //           TO TGA-MATU-END2000-NULL
            ws.getTrchDiGar().getTgaMatuEnd2000().setTgaMatuEnd2000Null(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaMatuEnd2000().getWtgaMatuEnd2000Null());
        }
        else {
            // COB_CODE: MOVE (SF)-MATU-END2000(IX-TAB-TGA)
            //           TO TGA-MATU-END2000
            ws.getTrchDiGar().getTgaMatuEnd2000().setTgaMatuEnd2000(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaMatuEnd2000().getWtgaMatuEnd2000(), 15, 3));
        }
        // COB_CODE: IF (SF)-ABB-TOT-INI-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-ABB-TOT-INI-NULL
        //           ELSE
        //              TO TGA-ABB-TOT-INI
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaAbbTotIni().getWtgaAbbTotIniNullFormatted())) {
            // COB_CODE: MOVE (SF)-ABB-TOT-INI-NULL(IX-TAB-TGA)
            //           TO TGA-ABB-TOT-INI-NULL
            ws.getTrchDiGar().getTgaAbbTotIni().setTgaAbbTotIniNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaAbbTotIni().getWtgaAbbTotIniNull());
        }
        else {
            // COB_CODE: MOVE (SF)-ABB-TOT-INI(IX-TAB-TGA)
            //           TO TGA-ABB-TOT-INI
            ws.getTrchDiGar().getTgaAbbTotIni().setTgaAbbTotIni(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaAbbTotIni().getWtgaAbbTotIni(), 15, 3));
        }
        // COB_CODE: IF (SF)-ABB-TOT-ULT-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-ABB-TOT-ULT-NULL
        //           ELSE
        //              TO TGA-ABB-TOT-ULT
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaAbbTotUlt().getWtgaAbbTotUltNullFormatted())) {
            // COB_CODE: MOVE (SF)-ABB-TOT-ULT-NULL(IX-TAB-TGA)
            //           TO TGA-ABB-TOT-ULT-NULL
            ws.getTrchDiGar().getTgaAbbTotUlt().setTgaAbbTotUltNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaAbbTotUlt().getWtgaAbbTotUltNull());
        }
        else {
            // COB_CODE: MOVE (SF)-ABB-TOT-ULT(IX-TAB-TGA)
            //           TO TGA-ABB-TOT-ULT
            ws.getTrchDiGar().getTgaAbbTotUlt().setTgaAbbTotUlt(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaAbbTotUlt().getWtgaAbbTotUlt(), 15, 3));
        }
        // COB_CODE: IF (SF)-ABB-ANNU-ULT-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-ABB-ANNU-ULT-NULL
        //           ELSE
        //              TO TGA-ABB-ANNU-ULT
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaAbbAnnuUlt().getWtgaAbbAnnuUltNullFormatted())) {
            // COB_CODE: MOVE (SF)-ABB-ANNU-ULT-NULL(IX-TAB-TGA)
            //           TO TGA-ABB-ANNU-ULT-NULL
            ws.getTrchDiGar().getTgaAbbAnnuUlt().setTgaAbbAnnuUltNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaAbbAnnuUlt().getWtgaAbbAnnuUltNull());
        }
        else {
            // COB_CODE: MOVE (SF)-ABB-ANNU-ULT(IX-TAB-TGA)
            //           TO TGA-ABB-ANNU-ULT
            ws.getTrchDiGar().getTgaAbbAnnuUlt().setTgaAbbAnnuUlt(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaAbbAnnuUlt().getWtgaAbbAnnuUlt(), 15, 3));
        }
        // COB_CODE: IF (SF)-DUR-ABB-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-DUR-ABB-NULL
        //           ELSE
        //              TO TGA-DUR-ABB
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaDurAbb().getWtgaDurAbbNullFormatted())) {
            // COB_CODE: MOVE (SF)-DUR-ABB-NULL(IX-TAB-TGA)
            //           TO TGA-DUR-ABB-NULL
            ws.getTrchDiGar().getTgaDurAbb().setTgaDurAbbNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaDurAbb().getWtgaDurAbbNull());
        }
        else {
            // COB_CODE: MOVE (SF)-DUR-ABB(IX-TAB-TGA)
            //           TO TGA-DUR-ABB
            ws.getTrchDiGar().getTgaDurAbb().setTgaDurAbb(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaDurAbb().getWtgaDurAbb());
        }
        // COB_CODE: IF (SF)-TP-ADEG-ABB-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-TP-ADEG-ABB-NULL
        //           ELSE
        //              TO TGA-TP-ADEG-ABB
        //           END-IF
        if (Conditions.eq(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaTpAdegAbb(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE (SF)-TP-ADEG-ABB-NULL(IX-TAB-TGA)
            //           TO TGA-TP-ADEG-ABB-NULL
            ws.getTrchDiGar().setTgaTpAdegAbb(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaTpAdegAbb());
        }
        else {
            // COB_CODE: MOVE (SF)-TP-ADEG-ABB(IX-TAB-TGA)
            //           TO TGA-TP-ADEG-ABB
            ws.getTrchDiGar().setTgaTpAdegAbb(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaTpAdegAbb());
        }
        // COB_CODE: IF (SF)-MOD-CALC-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-MOD-CALC-NULL
        //           ELSE
        //              TO TGA-MOD-CALC
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaModCalcFormatted())) {
            // COB_CODE: MOVE (SF)-MOD-CALC-NULL(IX-TAB-TGA)
            //           TO TGA-MOD-CALC-NULL
            ws.getTrchDiGar().setTgaModCalc(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaModCalc());
        }
        else {
            // COB_CODE: MOVE (SF)-MOD-CALC(IX-TAB-TGA)
            //           TO TGA-MOD-CALC
            ws.getTrchDiGar().setTgaModCalc(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaModCalc());
        }
        // COB_CODE: IF (SF)-IMP-AZ-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-IMP-AZ-NULL
        //           ELSE
        //              TO TGA-IMP-AZ
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpAz().getWtgaImpAzNullFormatted())) {
            // COB_CODE: MOVE (SF)-IMP-AZ-NULL(IX-TAB-TGA)
            //           TO TGA-IMP-AZ-NULL
            ws.getTrchDiGar().getTgaImpAz().setTgaImpAzNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpAz().getWtgaImpAzNull());
        }
        else {
            // COB_CODE: MOVE (SF)-IMP-AZ(IX-TAB-TGA)
            //           TO TGA-IMP-AZ
            ws.getTrchDiGar().getTgaImpAz().setTgaImpAz(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpAz().getWtgaImpAz(), 15, 3));
        }
        // COB_CODE: IF (SF)-IMP-ADER-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-IMP-ADER-NULL
        //           ELSE
        //              TO TGA-IMP-ADER
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpAder().getWtgaImpAderNullFormatted())) {
            // COB_CODE: MOVE (SF)-IMP-ADER-NULL(IX-TAB-TGA)
            //           TO TGA-IMP-ADER-NULL
            ws.getTrchDiGar().getTgaImpAder().setTgaImpAderNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpAder().getWtgaImpAderNull());
        }
        else {
            // COB_CODE: MOVE (SF)-IMP-ADER(IX-TAB-TGA)
            //           TO TGA-IMP-ADER
            ws.getTrchDiGar().getTgaImpAder().setTgaImpAder(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpAder().getWtgaImpAder(), 15, 3));
        }
        // COB_CODE: IF (SF)-IMP-TFR-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-IMP-TFR-NULL
        //           ELSE
        //              TO TGA-IMP-TFR
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpTfr().getWtgaImpTfrNullFormatted())) {
            // COB_CODE: MOVE (SF)-IMP-TFR-NULL(IX-TAB-TGA)
            //           TO TGA-IMP-TFR-NULL
            ws.getTrchDiGar().getTgaImpTfr().setTgaImpTfrNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpTfr().getWtgaImpTfrNull());
        }
        else {
            // COB_CODE: MOVE (SF)-IMP-TFR(IX-TAB-TGA)
            //           TO TGA-IMP-TFR
            ws.getTrchDiGar().getTgaImpTfr().setTgaImpTfr(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpTfr().getWtgaImpTfr(), 15, 3));
        }
        // COB_CODE: IF (SF)-IMP-VOLO-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-IMP-VOLO-NULL
        //           ELSE
        //              TO TGA-IMP-VOLO
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpVolo().getWtgaImpVoloNullFormatted())) {
            // COB_CODE: MOVE (SF)-IMP-VOLO-NULL(IX-TAB-TGA)
            //           TO TGA-IMP-VOLO-NULL
            ws.getTrchDiGar().getTgaImpVolo().setTgaImpVoloNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpVolo().getWtgaImpVoloNull());
        }
        else {
            // COB_CODE: MOVE (SF)-IMP-VOLO(IX-TAB-TGA)
            //           TO TGA-IMP-VOLO
            ws.getTrchDiGar().getTgaImpVolo().setTgaImpVolo(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpVolo().getWtgaImpVolo(), 15, 3));
        }
        // COB_CODE: IF (SF)-VIS-END2000-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-VIS-END2000-NULL
        //           ELSE
        //              TO TGA-VIS-END2000
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaVisEnd2000().getWtgaVisEnd2000NullFormatted())) {
            // COB_CODE: MOVE (SF)-VIS-END2000-NULL(IX-TAB-TGA)
            //           TO TGA-VIS-END2000-NULL
            ws.getTrchDiGar().getTgaVisEnd2000().setTgaVisEnd2000Null(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaVisEnd2000().getWtgaVisEnd2000Null());
        }
        else {
            // COB_CODE: MOVE (SF)-VIS-END2000(IX-TAB-TGA)
            //           TO TGA-VIS-END2000
            ws.getTrchDiGar().getTgaVisEnd2000().setTgaVisEnd2000(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaVisEnd2000().getWtgaVisEnd2000(), 15, 3));
        }
        // COB_CODE: IF (SF)-DT-VLDT-PROD-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-DT-VLDT-PROD-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaDtVldtProd().getWtgaDtVldtProdNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-VLDT-PROD-NULL(IX-TAB-TGA)
            //           TO TGA-DT-VLDT-PROD-NULL
            ws.getTrchDiGar().getTgaDtVldtProd().setTgaDtVldtProdNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaDtVldtProd().getWtgaDtVldtProdNull());
        }
        else if (wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaDtVldtProd().getWtgaDtVldtProd() == 0) {
            // COB_CODE: IF (SF)-DT-VLDT-PROD(IX-TAB-TGA) = ZERO
            //              TO TGA-DT-VLDT-PROD-NULL
            //           ELSE
            //            TO TGA-DT-VLDT-PROD
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO TGA-DT-VLDT-PROD-NULL
            ws.getTrchDiGar().getTgaDtVldtProd().setTgaDtVldtProdNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaDtVldtProd.Len.TGA_DT_VLDT_PROD_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-VLDT-PROD(IX-TAB-TGA)
            //           TO TGA-DT-VLDT-PROD
            ws.getTrchDiGar().getTgaDtVldtProd().setTgaDtVldtProd(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaDtVldtProd().getWtgaDtVldtProd());
        }
        // COB_CODE: IF (SF)-DT-INI-VAL-TAR-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-DT-INI-VAL-TAR-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaDtIniValTar().getWtgaDtIniValTarNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-INI-VAL-TAR-NULL(IX-TAB-TGA)
            //           TO TGA-DT-INI-VAL-TAR-NULL
            ws.getTrchDiGar().getTgaDtIniValTar().setTgaDtIniValTarNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaDtIniValTar().getWtgaDtIniValTarNull());
        }
        else if (wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaDtIniValTar().getWtgaDtIniValTar() == 0) {
            // COB_CODE: IF (SF)-DT-INI-VAL-TAR(IX-TAB-TGA) = ZERO
            //              TO TGA-DT-INI-VAL-TAR-NULL
            //           ELSE
            //            TO TGA-DT-INI-VAL-TAR
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO TGA-DT-INI-VAL-TAR-NULL
            ws.getTrchDiGar().getTgaDtIniValTar().setTgaDtIniValTarNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaDtIniValTar.Len.TGA_DT_INI_VAL_TAR_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-INI-VAL-TAR(IX-TAB-TGA)
            //           TO TGA-DT-INI-VAL-TAR
            ws.getTrchDiGar().getTgaDtIniValTar().setTgaDtIniValTar(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaDtIniValTar().getWtgaDtIniValTar());
        }
        // COB_CODE: IF (SF)-IMPB-VIS-END2000-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-IMPB-VIS-END2000-NULL
        //           ELSE
        //              TO TGA-IMPB-VIS-END2000
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpbVisEnd2000().getWtgaImpbVisEnd2000NullFormatted())) {
            // COB_CODE: MOVE (SF)-IMPB-VIS-END2000-NULL(IX-TAB-TGA)
            //           TO TGA-IMPB-VIS-END2000-NULL
            ws.getTrchDiGar().getTgaImpbVisEnd2000().setTgaImpbVisEnd2000Null(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpbVisEnd2000().getWtgaImpbVisEnd2000Null());
        }
        else {
            // COB_CODE: MOVE (SF)-IMPB-VIS-END2000(IX-TAB-TGA)
            //           TO TGA-IMPB-VIS-END2000
            ws.getTrchDiGar().getTgaImpbVisEnd2000().setTgaImpbVisEnd2000(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpbVisEnd2000().getWtgaImpbVisEnd2000(), 15, 3));
        }
        // COB_CODE: IF (SF)-REN-INI-TS-TEC-0-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-REN-INI-TS-TEC-0-NULL
        //           ELSE
        //              TO TGA-REN-INI-TS-TEC-0
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaRenIniTsTec0().getWtgaRenIniTsTec0NullFormatted())) {
            // COB_CODE: MOVE (SF)-REN-INI-TS-TEC-0-NULL(IX-TAB-TGA)
            //           TO TGA-REN-INI-TS-TEC-0-NULL
            ws.getTrchDiGar().getTgaRenIniTsTec0().setTgaRenIniTsTec0Null(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaRenIniTsTec0().getWtgaRenIniTsTec0Null());
        }
        else {
            // COB_CODE: MOVE (SF)-REN-INI-TS-TEC-0(IX-TAB-TGA)
            //           TO TGA-REN-INI-TS-TEC-0
            ws.getTrchDiGar().getTgaRenIniTsTec0().setTgaRenIniTsTec0(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaRenIniTsTec0().getWtgaRenIniTsTec0(), 15, 3));
        }
        // COB_CODE: IF (SF)-PC-RIP-PRE-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-PC-RIP-PRE-NULL
        //           ELSE
        //              TO TGA-PC-RIP-PRE
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPcRipPre().getWtgaPcRipPreNullFormatted())) {
            // COB_CODE: MOVE (SF)-PC-RIP-PRE-NULL(IX-TAB-TGA)
            //           TO TGA-PC-RIP-PRE-NULL
            ws.getTrchDiGar().getTgaPcRipPre().setTgaPcRipPreNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPcRipPre().getWtgaPcRipPreNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PC-RIP-PRE(IX-TAB-TGA)
            //           TO TGA-PC-RIP-PRE
            ws.getTrchDiGar().getTgaPcRipPre().setTgaPcRipPre(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPcRipPre().getWtgaPcRipPre(), 6, 3));
        }
        // COB_CODE: IF (SF)-FL-IMPORTI-FORZ-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-FL-IMPORTI-FORZ-NULL
        //           ELSE
        //              TO TGA-FL-IMPORTI-FORZ
        //           END-IF
        if (Conditions.eq(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaFlImportiForz(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE (SF)-FL-IMPORTI-FORZ-NULL(IX-TAB-TGA)
            //           TO TGA-FL-IMPORTI-FORZ-NULL
            ws.getTrchDiGar().setTgaFlImportiForz(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaFlImportiForz());
        }
        else {
            // COB_CODE: MOVE (SF)-FL-IMPORTI-FORZ(IX-TAB-TGA)
            //           TO TGA-FL-IMPORTI-FORZ
            ws.getTrchDiGar().setTgaFlImportiForz(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaFlImportiForz());
        }
        // COB_CODE: IF (SF)-PRSTZ-INI-NFORZ-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-PRSTZ-INI-NFORZ-NULL
        //           ELSE
        //              TO TGA-PRSTZ-INI-NFORZ
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPrstzIniNforz().getWtgaPrstzIniNforzNullFormatted())) {
            // COB_CODE: MOVE (SF)-PRSTZ-INI-NFORZ-NULL(IX-TAB-TGA)
            //           TO TGA-PRSTZ-INI-NFORZ-NULL
            ws.getTrchDiGar().getTgaPrstzIniNforz().setTgaPrstzIniNforzNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPrstzIniNforz().getWtgaPrstzIniNforzNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PRSTZ-INI-NFORZ(IX-TAB-TGA)
            //           TO TGA-PRSTZ-INI-NFORZ
            ws.getTrchDiGar().getTgaPrstzIniNforz().setTgaPrstzIniNforz(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPrstzIniNforz().getWtgaPrstzIniNforz(), 15, 3));
        }
        // COB_CODE: IF (SF)-VIS-END2000-NFORZ-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-VIS-END2000-NFORZ-NULL
        //           ELSE
        //              TO TGA-VIS-END2000-NFORZ
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaVisEnd2000Nforz().getWtgaVisEnd2000NforzNullFormatted())) {
            // COB_CODE: MOVE (SF)-VIS-END2000-NFORZ-NULL(IX-TAB-TGA)
            //           TO TGA-VIS-END2000-NFORZ-NULL
            ws.getTrchDiGar().getTgaVisEnd2000Nforz().setTgaVisEnd2000NforzNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaVisEnd2000Nforz().getWtgaVisEnd2000NforzNull());
        }
        else {
            // COB_CODE: MOVE (SF)-VIS-END2000-NFORZ(IX-TAB-TGA)
            //           TO TGA-VIS-END2000-NFORZ
            ws.getTrchDiGar().getTgaVisEnd2000Nforz().setTgaVisEnd2000Nforz(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaVisEnd2000Nforz().getWtgaVisEnd2000Nforz(), 15, 3));
        }
        // COB_CODE: IF (SF)-INTR-MORA-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-INTR-MORA-NULL
        //           ELSE
        //              TO TGA-INTR-MORA
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaIntrMora().getWtgaIntrMoraNullFormatted())) {
            // COB_CODE: MOVE (SF)-INTR-MORA-NULL(IX-TAB-TGA)
            //           TO TGA-INTR-MORA-NULL
            ws.getTrchDiGar().getTgaIntrMora().setTgaIntrMoraNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaIntrMora().getWtgaIntrMoraNull());
        }
        else {
            // COB_CODE: MOVE (SF)-INTR-MORA(IX-TAB-TGA)
            //           TO TGA-INTR-MORA
            ws.getTrchDiGar().getTgaIntrMora().setTgaIntrMora(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaIntrMora().getWtgaIntrMora(), 15, 3));
        }
        // COB_CODE: IF (SF)-MANFEE-ANTIC-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-MANFEE-ANTIC-NULL
        //           ELSE
        //              TO TGA-MANFEE-ANTIC
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaManfeeAntic().getWtgaManfeeAnticNullFormatted())) {
            // COB_CODE: MOVE (SF)-MANFEE-ANTIC-NULL(IX-TAB-TGA)
            //           TO TGA-MANFEE-ANTIC-NULL
            ws.getTrchDiGar().getTgaManfeeAntic().setTgaManfeeAnticNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaManfeeAntic().getWtgaManfeeAnticNull());
        }
        else {
            // COB_CODE: MOVE (SF)-MANFEE-ANTIC(IX-TAB-TGA)
            //           TO TGA-MANFEE-ANTIC
            ws.getTrchDiGar().getTgaManfeeAntic().setTgaManfeeAntic(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaManfeeAntic().getWtgaManfeeAntic(), 15, 3));
        }
        // COB_CODE: IF (SF)-MANFEE-RICOR-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-MANFEE-RICOR-NULL
        //           ELSE
        //              TO TGA-MANFEE-RICOR
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaManfeeRicor().getWtgaManfeeRicorNullFormatted())) {
            // COB_CODE: MOVE (SF)-MANFEE-RICOR-NULL(IX-TAB-TGA)
            //           TO TGA-MANFEE-RICOR-NULL
            ws.getTrchDiGar().getTgaManfeeRicor().setTgaManfeeRicorNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaManfeeRicor().getWtgaManfeeRicorNull());
        }
        else {
            // COB_CODE: MOVE (SF)-MANFEE-RICOR(IX-TAB-TGA)
            //           TO TGA-MANFEE-RICOR
            ws.getTrchDiGar().getTgaManfeeRicor().setTgaManfeeRicor(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaManfeeRicor().getWtgaManfeeRicor(), 15, 3));
        }
        // COB_CODE: IF (SF)-PRE-UNI-RIVTO-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-PRE-UNI-RIVTO-NULL
        //           ELSE
        //              TO TGA-PRE-UNI-RIVTO
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPreUniRivto().getWtgaPreUniRivtoNullFormatted())) {
            // COB_CODE: MOVE (SF)-PRE-UNI-RIVTO-NULL(IX-TAB-TGA)
            //           TO TGA-PRE-UNI-RIVTO-NULL
            ws.getTrchDiGar().getTgaPreUniRivto().setTgaPreUniRivtoNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPreUniRivto().getWtgaPreUniRivtoNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PRE-UNI-RIVTO(IX-TAB-TGA)
            //           TO TGA-PRE-UNI-RIVTO
            ws.getTrchDiGar().getTgaPreUniRivto().setTgaPreUniRivto(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPreUniRivto().getWtgaPreUniRivto(), 15, 3));
        }
        // COB_CODE: IF (SF)-PROV-1AA-ACQ-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-PROV-1AA-ACQ-NULL
        //           ELSE
        //              TO TGA-PROV-1AA-ACQ
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaProv1aaAcq().getWtgaProv1aaAcqNullFormatted())) {
            // COB_CODE: MOVE (SF)-PROV-1AA-ACQ-NULL(IX-TAB-TGA)
            //           TO TGA-PROV-1AA-ACQ-NULL
            ws.getTrchDiGar().getTgaProv1aaAcq().setTgaProv1aaAcqNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaProv1aaAcq().getWtgaProv1aaAcqNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PROV-1AA-ACQ(IX-TAB-TGA)
            //           TO TGA-PROV-1AA-ACQ
            ws.getTrchDiGar().getTgaProv1aaAcq().setTgaProv1aaAcq(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaProv1aaAcq().getWtgaProv1aaAcq(), 15, 3));
        }
        // COB_CODE: IF (SF)-PROV-2AA-ACQ-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-PROV-2AA-ACQ-NULL
        //           ELSE
        //              TO TGA-PROV-2AA-ACQ
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaProv2aaAcq().getWtgaProv2aaAcqNullFormatted())) {
            // COB_CODE: MOVE (SF)-PROV-2AA-ACQ-NULL(IX-TAB-TGA)
            //           TO TGA-PROV-2AA-ACQ-NULL
            ws.getTrchDiGar().getTgaProv2aaAcq().setTgaProv2aaAcqNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaProv2aaAcq().getWtgaProv2aaAcqNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PROV-2AA-ACQ(IX-TAB-TGA)
            //           TO TGA-PROV-2AA-ACQ
            ws.getTrchDiGar().getTgaProv2aaAcq().setTgaProv2aaAcq(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaProv2aaAcq().getWtgaProv2aaAcq(), 15, 3));
        }
        // COB_CODE: IF (SF)-PROV-RICOR-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-PROV-RICOR-NULL
        //           ELSE
        //              TO TGA-PROV-RICOR
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaProvRicor().getWtgaProvRicorNullFormatted())) {
            // COB_CODE: MOVE (SF)-PROV-RICOR-NULL(IX-TAB-TGA)
            //           TO TGA-PROV-RICOR-NULL
            ws.getTrchDiGar().getTgaProvRicor().setTgaProvRicorNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaProvRicor().getWtgaProvRicorNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PROV-RICOR(IX-TAB-TGA)
            //           TO TGA-PROV-RICOR
            ws.getTrchDiGar().getTgaProvRicor().setTgaProvRicor(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaProvRicor().getWtgaProvRicor(), 15, 3));
        }
        // COB_CODE: IF (SF)-PROV-INC-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-PROV-INC-NULL
        //           ELSE
        //              TO TGA-PROV-INC
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaProvInc().getWtgaProvIncNullFormatted())) {
            // COB_CODE: MOVE (SF)-PROV-INC-NULL(IX-TAB-TGA)
            //           TO TGA-PROV-INC-NULL
            ws.getTrchDiGar().getTgaProvInc().setTgaProvIncNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaProvInc().getWtgaProvIncNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PROV-INC(IX-TAB-TGA)
            //           TO TGA-PROV-INC
            ws.getTrchDiGar().getTgaProvInc().setTgaProvInc(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaProvInc().getWtgaProvInc(), 15, 3));
        }
        // COB_CODE: IF (SF)-ALQ-PROV-ACQ-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-ALQ-PROV-ACQ-NULL
        //           ELSE
        //              TO TGA-ALQ-PROV-ACQ
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaAlqProvAcq().getWtgaAlqProvAcqNullFormatted())) {
            // COB_CODE: MOVE (SF)-ALQ-PROV-ACQ-NULL(IX-TAB-TGA)
            //           TO TGA-ALQ-PROV-ACQ-NULL
            ws.getTrchDiGar().getTgaAlqProvAcq().setTgaAlqProvAcqNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaAlqProvAcq().getWtgaAlqProvAcqNull());
        }
        else {
            // COB_CODE: MOVE (SF)-ALQ-PROV-ACQ(IX-TAB-TGA)
            //           TO TGA-ALQ-PROV-ACQ
            ws.getTrchDiGar().getTgaAlqProvAcq().setTgaAlqProvAcq(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaAlqProvAcq().getWtgaAlqProvAcq(), 6, 3));
        }
        // COB_CODE: IF (SF)-ALQ-PROV-INC-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-ALQ-PROV-INC-NULL
        //           ELSE
        //              TO TGA-ALQ-PROV-INC
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaAlqProvInc().getWtgaAlqProvIncNullFormatted())) {
            // COB_CODE: MOVE (SF)-ALQ-PROV-INC-NULL(IX-TAB-TGA)
            //           TO TGA-ALQ-PROV-INC-NULL
            ws.getTrchDiGar().getTgaAlqProvInc().setTgaAlqProvIncNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaAlqProvInc().getWtgaAlqProvIncNull());
        }
        else {
            // COB_CODE: MOVE (SF)-ALQ-PROV-INC(IX-TAB-TGA)
            //           TO TGA-ALQ-PROV-INC
            ws.getTrchDiGar().getTgaAlqProvInc().setTgaAlqProvInc(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaAlqProvInc().getWtgaAlqProvInc(), 6, 3));
        }
        // COB_CODE: IF (SF)-ALQ-PROV-RICOR-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-ALQ-PROV-RICOR-NULL
        //           ELSE
        //              TO TGA-ALQ-PROV-RICOR
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaAlqProvRicor().getWtgaAlqProvRicorNullFormatted())) {
            // COB_CODE: MOVE (SF)-ALQ-PROV-RICOR-NULL(IX-TAB-TGA)
            //           TO TGA-ALQ-PROV-RICOR-NULL
            ws.getTrchDiGar().getTgaAlqProvRicor().setTgaAlqProvRicorNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaAlqProvRicor().getWtgaAlqProvRicorNull());
        }
        else {
            // COB_CODE: MOVE (SF)-ALQ-PROV-RICOR(IX-TAB-TGA)
            //           TO TGA-ALQ-PROV-RICOR
            ws.getTrchDiGar().getTgaAlqProvRicor().setTgaAlqProvRicor(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaAlqProvRicor().getWtgaAlqProvRicor(), 6, 3));
        }
        // COB_CODE: IF (SF)-IMPB-PROV-ACQ-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-IMPB-PROV-ACQ-NULL
        //           ELSE
        //              TO TGA-IMPB-PROV-ACQ
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpbProvAcq().getWtgaImpbProvAcqNullFormatted())) {
            // COB_CODE: MOVE (SF)-IMPB-PROV-ACQ-NULL(IX-TAB-TGA)
            //           TO TGA-IMPB-PROV-ACQ-NULL
            ws.getTrchDiGar().getTgaImpbProvAcq().setTgaImpbProvAcqNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpbProvAcq().getWtgaImpbProvAcqNull());
        }
        else {
            // COB_CODE: MOVE (SF)-IMPB-PROV-ACQ(IX-TAB-TGA)
            //           TO TGA-IMPB-PROV-ACQ
            ws.getTrchDiGar().getTgaImpbProvAcq().setTgaImpbProvAcq(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpbProvAcq().getWtgaImpbProvAcq(), 15, 3));
        }
        // COB_CODE: IF (SF)-IMPB-PROV-INC-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-IMPB-PROV-INC-NULL
        //           ELSE
        //              TO TGA-IMPB-PROV-INC
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpbProvInc().getWtgaImpbProvIncNullFormatted())) {
            // COB_CODE: MOVE (SF)-IMPB-PROV-INC-NULL(IX-TAB-TGA)
            //           TO TGA-IMPB-PROV-INC-NULL
            ws.getTrchDiGar().getTgaImpbProvInc().setTgaImpbProvIncNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpbProvInc().getWtgaImpbProvIncNull());
        }
        else {
            // COB_CODE: MOVE (SF)-IMPB-PROV-INC(IX-TAB-TGA)
            //           TO TGA-IMPB-PROV-INC
            ws.getTrchDiGar().getTgaImpbProvInc().setTgaImpbProvInc(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpbProvInc().getWtgaImpbProvInc(), 15, 3));
        }
        // COB_CODE: IF (SF)-IMPB-PROV-RICOR-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-IMPB-PROV-RICOR-NULL
        //           ELSE
        //              TO TGA-IMPB-PROV-RICOR
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpbProvRicor().getWtgaImpbProvRicorNullFormatted())) {
            // COB_CODE: MOVE (SF)-IMPB-PROV-RICOR-NULL(IX-TAB-TGA)
            //           TO TGA-IMPB-PROV-RICOR-NULL
            ws.getTrchDiGar().getTgaImpbProvRicor().setTgaImpbProvRicorNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpbProvRicor().getWtgaImpbProvRicorNull());
        }
        else {
            // COB_CODE: MOVE (SF)-IMPB-PROV-RICOR(IX-TAB-TGA)
            //           TO TGA-IMPB-PROV-RICOR
            ws.getTrchDiGar().getTgaImpbProvRicor().setTgaImpbProvRicor(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpbProvRicor().getWtgaImpbProvRicor(), 15, 3));
        }
        // COB_CODE: IF (SF)-FL-PROV-FORZ-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-FL-PROV-FORZ-NULL
        //           ELSE
        //              TO TGA-FL-PROV-FORZ
        //           END-IF
        if (Conditions.eq(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaFlProvForz(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE (SF)-FL-PROV-FORZ-NULL(IX-TAB-TGA)
            //           TO TGA-FL-PROV-FORZ-NULL
            ws.getTrchDiGar().setTgaFlProvForz(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaFlProvForz());
        }
        else {
            // COB_CODE: MOVE (SF)-FL-PROV-FORZ(IX-TAB-TGA)
            //           TO TGA-FL-PROV-FORZ
            ws.getTrchDiGar().setTgaFlProvForz(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaFlProvForz());
        }
        // COB_CODE: IF (SF)-PRSTZ-AGG-INI-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-PRSTZ-AGG-INI-NULL
        //           ELSE
        //              TO TGA-PRSTZ-AGG-INI
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPrstzAggIni().getWtgaPrstzAggIniNullFormatted())) {
            // COB_CODE: MOVE (SF)-PRSTZ-AGG-INI-NULL(IX-TAB-TGA)
            //           TO TGA-PRSTZ-AGG-INI-NULL
            ws.getTrchDiGar().getTgaPrstzAggIni().setTgaPrstzAggIniNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPrstzAggIni().getWtgaPrstzAggIniNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PRSTZ-AGG-INI(IX-TAB-TGA)
            //           TO TGA-PRSTZ-AGG-INI
            ws.getTrchDiGar().getTgaPrstzAggIni().setTgaPrstzAggIni(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPrstzAggIni().getWtgaPrstzAggIni(), 15, 3));
        }
        // COB_CODE: IF (SF)-INCR-PRE-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-INCR-PRE-NULL
        //           ELSE
        //              TO TGA-INCR-PRE
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaIncrPre().getWtgaIncrPreNullFormatted())) {
            // COB_CODE: MOVE (SF)-INCR-PRE-NULL(IX-TAB-TGA)
            //           TO TGA-INCR-PRE-NULL
            ws.getTrchDiGar().getTgaIncrPre().setTgaIncrPreNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaIncrPre().getWtgaIncrPreNull());
        }
        else {
            // COB_CODE: MOVE (SF)-INCR-PRE(IX-TAB-TGA)
            //           TO TGA-INCR-PRE
            ws.getTrchDiGar().getTgaIncrPre().setTgaIncrPre(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaIncrPre().getWtgaIncrPre(), 15, 3));
        }
        // COB_CODE: IF (SF)-INCR-PRSTZ-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-INCR-PRSTZ-NULL
        //           ELSE
        //              TO TGA-INCR-PRSTZ
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaIncrPrstz().getWtgaIncrPrstzNullFormatted())) {
            // COB_CODE: MOVE (SF)-INCR-PRSTZ-NULL(IX-TAB-TGA)
            //           TO TGA-INCR-PRSTZ-NULL
            ws.getTrchDiGar().getTgaIncrPrstz().setTgaIncrPrstzNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaIncrPrstz().getWtgaIncrPrstzNull());
        }
        else {
            // COB_CODE: MOVE (SF)-INCR-PRSTZ(IX-TAB-TGA)
            //           TO TGA-INCR-PRSTZ
            ws.getTrchDiGar().getTgaIncrPrstz().setTgaIncrPrstz(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaIncrPrstz().getWtgaIncrPrstz(), 15, 3));
        }
        // COB_CODE: IF (SF)-DT-ULT-ADEG-PRE-PR-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-DT-ULT-ADEG-PRE-PR-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaDtUltAdegPrePr().getWtgaDtUltAdegPrePrNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-ADEG-PRE-PR-NULL(IX-TAB-TGA)
            //           TO TGA-DT-ULT-ADEG-PRE-PR-NULL
            ws.getTrchDiGar().getTgaDtUltAdegPrePr().setTgaDtUltAdegPrePrNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaDtUltAdegPrePr().getWtgaDtUltAdegPrePrNull());
        }
        else if (wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaDtUltAdegPrePr().getWtgaDtUltAdegPrePr() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-ADEG-PRE-PR(IX-TAB-TGA) = ZERO
            //              TO TGA-DT-ULT-ADEG-PRE-PR-NULL
            //           ELSE
            //            TO TGA-DT-ULT-ADEG-PRE-PR
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO TGA-DT-ULT-ADEG-PRE-PR-NULL
            ws.getTrchDiGar().getTgaDtUltAdegPrePr().setTgaDtUltAdegPrePrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaDtUltAdegPrePr.Len.TGA_DT_ULT_ADEG_PRE_PR_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-ADEG-PRE-PR(IX-TAB-TGA)
            //           TO TGA-DT-ULT-ADEG-PRE-PR
            ws.getTrchDiGar().getTgaDtUltAdegPrePr().setTgaDtUltAdegPrePr(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaDtUltAdegPrePr().getWtgaDtUltAdegPrePr());
        }
        // COB_CODE: IF (SF)-PRSTZ-AGG-ULT-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-PRSTZ-AGG-ULT-NULL
        //           ELSE
        //              TO TGA-PRSTZ-AGG-ULT
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPrstzAggUlt().getWtgaPrstzAggUltNullFormatted())) {
            // COB_CODE: MOVE (SF)-PRSTZ-AGG-ULT-NULL(IX-TAB-TGA)
            //           TO TGA-PRSTZ-AGG-ULT-NULL
            ws.getTrchDiGar().getTgaPrstzAggUlt().setTgaPrstzAggUltNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPrstzAggUlt().getWtgaPrstzAggUltNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PRSTZ-AGG-ULT(IX-TAB-TGA)
            //           TO TGA-PRSTZ-AGG-ULT
            ws.getTrchDiGar().getTgaPrstzAggUlt().setTgaPrstzAggUlt(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPrstzAggUlt().getWtgaPrstzAggUlt(), 15, 3));
        }
        // COB_CODE: IF (SF)-TS-RIVAL-NET-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-TS-RIVAL-NET-NULL
        //           ELSE
        //              TO TGA-TS-RIVAL-NET
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaTsRivalNet().getWtgaTsRivalNetNullFormatted())) {
            // COB_CODE: MOVE (SF)-TS-RIVAL-NET-NULL(IX-TAB-TGA)
            //           TO TGA-TS-RIVAL-NET-NULL
            ws.getTrchDiGar().getTgaTsRivalNet().setTgaTsRivalNetNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaTsRivalNet().getWtgaTsRivalNetNull());
        }
        else {
            // COB_CODE: MOVE (SF)-TS-RIVAL-NET(IX-TAB-TGA)
            //           TO TGA-TS-RIVAL-NET
            ws.getTrchDiGar().getTgaTsRivalNet().setTgaTsRivalNet(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaTsRivalNet().getWtgaTsRivalNet(), 14, 9));
        }
        // COB_CODE: IF (SF)-PRE-PATTUITO-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-PRE-PATTUITO-NULL
        //           ELSE
        //              TO TGA-PRE-PATTUITO
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPrePattuito().getWtgaPrePattuitoNullFormatted())) {
            // COB_CODE: MOVE (SF)-PRE-PATTUITO-NULL(IX-TAB-TGA)
            //           TO TGA-PRE-PATTUITO-NULL
            ws.getTrchDiGar().getTgaPrePattuito().setTgaPrePattuitoNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPrePattuito().getWtgaPrePattuitoNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PRE-PATTUITO(IX-TAB-TGA)
            //           TO TGA-PRE-PATTUITO
            ws.getTrchDiGar().getTgaPrePattuito().setTgaPrePattuito(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPrePattuito().getWtgaPrePattuito(), 15, 3));
        }
        // COB_CODE: IF (SF)-TP-RIVAL-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-TP-RIVAL-NULL
        //           ELSE
        //              TO TGA-TP-RIVAL
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaTpRivalFormatted())) {
            // COB_CODE: MOVE (SF)-TP-RIVAL-NULL(IX-TAB-TGA)
            //           TO TGA-TP-RIVAL-NULL
            ws.getTrchDiGar().setTgaTpRival(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaTpRival());
        }
        else {
            // COB_CODE: MOVE (SF)-TP-RIVAL(IX-TAB-TGA)
            //           TO TGA-TP-RIVAL
            ws.getTrchDiGar().setTgaTpRival(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaTpRival());
        }
        // COB_CODE: IF (SF)-RIS-MAT-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-RIS-MAT-NULL
        //           ELSE
        //              TO TGA-RIS-MAT
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaRisMat().getWtgaRisMatNullFormatted())) {
            // COB_CODE: MOVE (SF)-RIS-MAT-NULL(IX-TAB-TGA)
            //           TO TGA-RIS-MAT-NULL
            ws.getTrchDiGar().getTgaRisMat().setTgaRisMatNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaRisMat().getWtgaRisMatNull());
        }
        else {
            // COB_CODE: MOVE (SF)-RIS-MAT(IX-TAB-TGA)
            //           TO TGA-RIS-MAT
            ws.getTrchDiGar().getTgaRisMat().setTgaRisMat(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaRisMat().getWtgaRisMat(), 15, 3));
        }
        // COB_CODE: IF (SF)-CPT-MIN-SCAD-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-CPT-MIN-SCAD-NULL
        //           ELSE
        //              TO TGA-CPT-MIN-SCAD
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaCptMinScad().getWtgaCptMinScadNullFormatted())) {
            // COB_CODE: MOVE (SF)-CPT-MIN-SCAD-NULL(IX-TAB-TGA)
            //           TO TGA-CPT-MIN-SCAD-NULL
            ws.getTrchDiGar().getTgaCptMinScad().setTgaCptMinScadNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaCptMinScad().getWtgaCptMinScadNull());
        }
        else {
            // COB_CODE: MOVE (SF)-CPT-MIN-SCAD(IX-TAB-TGA)
            //           TO TGA-CPT-MIN-SCAD
            ws.getTrchDiGar().getTgaCptMinScad().setTgaCptMinScad(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaCptMinScad().getWtgaCptMinScad(), 15, 3));
        }
        // COB_CODE: IF (SF)-COMMIS-GEST-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-COMMIS-GEST-NULL
        //           ELSE
        //              TO TGA-COMMIS-GEST
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaCommisGest().getWtgaCommisGestNullFormatted())) {
            // COB_CODE: MOVE (SF)-COMMIS-GEST-NULL(IX-TAB-TGA)
            //           TO TGA-COMMIS-GEST-NULL
            ws.getTrchDiGar().getTgaCommisGest().setTgaCommisGestNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaCommisGest().getWtgaCommisGestNull());
        }
        else {
            // COB_CODE: MOVE (SF)-COMMIS-GEST(IX-TAB-TGA)
            //           TO TGA-COMMIS-GEST
            ws.getTrchDiGar().getTgaCommisGest().setTgaCommisGest(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaCommisGest().getWtgaCommisGest(), 15, 3));
        }
        // COB_CODE: IF (SF)-TP-MANFEE-APPL-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-TP-MANFEE-APPL-NULL
        //           ELSE
        //              TO TGA-TP-MANFEE-APPL
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaTpManfeeApplFormatted())) {
            // COB_CODE: MOVE (SF)-TP-MANFEE-APPL-NULL(IX-TAB-TGA)
            //           TO TGA-TP-MANFEE-APPL-NULL
            ws.getTrchDiGar().setTgaTpManfeeAppl(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaTpManfeeAppl());
        }
        else {
            // COB_CODE: MOVE (SF)-TP-MANFEE-APPL(IX-TAB-TGA)
            //           TO TGA-TP-MANFEE-APPL
            ws.getTrchDiGar().setTgaTpManfeeAppl(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaTpManfeeAppl());
        }
        // COB_CODE: IF (SF)-DS-RIGA(IX-TAB-TGA) NOT NUMERIC
        //              MOVE 0 TO TGA-DS-RIGA
        //           ELSE
        //              TO TGA-DS-RIGA
        //           END-IF
        if (!Functions.isNumber(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaDsRiga())) {
            // COB_CODE: MOVE 0 TO TGA-DS-RIGA
            ws.getTrchDiGar().setTgaDsRiga(0);
        }
        else {
            // COB_CODE: MOVE (SF)-DS-RIGA(IX-TAB-TGA)
            //           TO TGA-DS-RIGA
            ws.getTrchDiGar().setTgaDsRiga(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaDsRiga());
        }
        // COB_CODE: MOVE (SF)-DS-OPER-SQL(IX-TAB-TGA)
        //              TO TGA-DS-OPER-SQL
        ws.getTrchDiGar().setTgaDsOperSql(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaDsOperSql());
        // COB_CODE: IF (SF)-DS-VER(IX-TAB-TGA) NOT NUMERIC
        //              MOVE 0 TO TGA-DS-VER
        //           ELSE
        //              TO TGA-DS-VER
        //           END-IF
        if (!Functions.isNumber(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaDsVer())) {
            // COB_CODE: MOVE 0 TO TGA-DS-VER
            ws.getTrchDiGar().setTgaDsVer(0);
        }
        else {
            // COB_CODE: MOVE (SF)-DS-VER(IX-TAB-TGA)
            //           TO TGA-DS-VER
            ws.getTrchDiGar().setTgaDsVer(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaDsVer());
        }
        // COB_CODE: IF (SF)-DS-TS-INI-CPTZ(IX-TAB-TGA) NOT NUMERIC
        //              MOVE 0 TO TGA-DS-TS-INI-CPTZ
        //           ELSE
        //              TO TGA-DS-TS-INI-CPTZ
        //           END-IF
        if (!Functions.isNumber(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaDsTsIniCptz())) {
            // COB_CODE: MOVE 0 TO TGA-DS-TS-INI-CPTZ
            ws.getTrchDiGar().setTgaDsTsIniCptz(0);
        }
        else {
            // COB_CODE: MOVE (SF)-DS-TS-INI-CPTZ(IX-TAB-TGA)
            //           TO TGA-DS-TS-INI-CPTZ
            ws.getTrchDiGar().setTgaDsTsIniCptz(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaDsTsIniCptz());
        }
        // COB_CODE: IF (SF)-DS-TS-END-CPTZ(IX-TAB-TGA) NOT NUMERIC
        //              MOVE 0 TO TGA-DS-TS-END-CPTZ
        //           ELSE
        //              TO TGA-DS-TS-END-CPTZ
        //           END-IF
        if (!Functions.isNumber(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaDsTsEndCptz())) {
            // COB_CODE: MOVE 0 TO TGA-DS-TS-END-CPTZ
            ws.getTrchDiGar().setTgaDsTsEndCptz(0);
        }
        else {
            // COB_CODE: MOVE (SF)-DS-TS-END-CPTZ(IX-TAB-TGA)
            //           TO TGA-DS-TS-END-CPTZ
            ws.getTrchDiGar().setTgaDsTsEndCptz(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaDsTsEndCptz());
        }
        // COB_CODE: MOVE (SF)-DS-UTENTE(IX-TAB-TGA)
        //              TO TGA-DS-UTENTE
        ws.getTrchDiGar().setTgaDsUtente(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaDsUtente());
        // COB_CODE: MOVE (SF)-DS-STATO-ELAB(IX-TAB-TGA)
        //              TO TGA-DS-STATO-ELAB
        ws.getTrchDiGar().setTgaDsStatoElab(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaDsStatoElab());
        // COB_CODE: IF (SF)-PC-COMMIS-GEST-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-PC-COMMIS-GEST-NULL
        //           ELSE
        //              TO TGA-PC-COMMIS-GEST
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPcCommisGest().getWtgaPcCommisGestNullFormatted())) {
            // COB_CODE: MOVE (SF)-PC-COMMIS-GEST-NULL(IX-TAB-TGA)
            //           TO TGA-PC-COMMIS-GEST-NULL
            ws.getTrchDiGar().getTgaPcCommisGest().setTgaPcCommisGestNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPcCommisGest().getWtgaPcCommisGestNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PC-COMMIS-GEST(IX-TAB-TGA)
            //           TO TGA-PC-COMMIS-GEST
            ws.getTrchDiGar().getTgaPcCommisGest().setTgaPcCommisGest(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaPcCommisGest().getWtgaPcCommisGest(), 6, 3));
        }
        // COB_CODE: IF (SF)-NUM-GG-RIVAL-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-NUM-GG-RIVAL-NULL
        //           ELSE
        //              TO TGA-NUM-GG-RIVAL
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaNumGgRival().getWtgaNumGgRivalNullFormatted())) {
            // COB_CODE: MOVE (SF)-NUM-GG-RIVAL-NULL(IX-TAB-TGA)
            //           TO TGA-NUM-GG-RIVAL-NULL
            ws.getTrchDiGar().getTgaNumGgRival().setTgaNumGgRivalNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaNumGgRival().getWtgaNumGgRivalNull());
        }
        else {
            // COB_CODE: MOVE (SF)-NUM-GG-RIVAL(IX-TAB-TGA)
            //           TO TGA-NUM-GG-RIVAL
            ws.getTrchDiGar().getTgaNumGgRival().setTgaNumGgRival(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaNumGgRival().getWtgaNumGgRival());
        }
        // COB_CODE: IF (SF)-IMP-TRASFE-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-IMP-TRASFE-NULL
        //           ELSE
        //              TO TGA-IMP-TRASFE
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpTrasfe().getWtgaImpTrasfeNullFormatted())) {
            // COB_CODE: MOVE (SF)-IMP-TRASFE-NULL(IX-TAB-TGA)
            //           TO TGA-IMP-TRASFE-NULL
            ws.getTrchDiGar().getTgaImpTrasfe().setTgaImpTrasfeNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpTrasfe().getWtgaImpTrasfeNull());
        }
        else {
            // COB_CODE: MOVE (SF)-IMP-TRASFE(IX-TAB-TGA)
            //           TO TGA-IMP-TRASFE
            ws.getTrchDiGar().getTgaImpTrasfe().setTgaImpTrasfe(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpTrasfe().getWtgaImpTrasfe(), 15, 3));
        }
        // COB_CODE: IF (SF)-IMP-TFR-STRC-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-IMP-TFR-STRC-NULL
        //           ELSE
        //              TO TGA-IMP-TFR-STRC
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpTfrStrc().getWtgaImpTfrStrcNullFormatted())) {
            // COB_CODE: MOVE (SF)-IMP-TFR-STRC-NULL(IX-TAB-TGA)
            //           TO TGA-IMP-TFR-STRC-NULL
            ws.getTrchDiGar().getTgaImpTfrStrc().setTgaImpTfrStrcNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpTfrStrc().getWtgaImpTfrStrcNull());
        }
        else {
            // COB_CODE: MOVE (SF)-IMP-TFR-STRC(IX-TAB-TGA)
            //           TO TGA-IMP-TFR-STRC
            ws.getTrchDiGar().getTgaImpTfrStrc().setTgaImpTfrStrc(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpTfrStrc().getWtgaImpTfrStrc(), 15, 3));
        }
        // COB_CODE: IF (SF)-ACQ-EXP-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-ACQ-EXP-NULL
        //           ELSE
        //              TO TGA-ACQ-EXP
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaAcqExp().getWtgaAcqExpNullFormatted())) {
            // COB_CODE: MOVE (SF)-ACQ-EXP-NULL(IX-TAB-TGA)
            //           TO TGA-ACQ-EXP-NULL
            ws.getTrchDiGar().getTgaAcqExp().setTgaAcqExpNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaAcqExp().getWtgaAcqExpNull());
        }
        else {
            // COB_CODE: MOVE (SF)-ACQ-EXP(IX-TAB-TGA)
            //           TO TGA-ACQ-EXP
            ws.getTrchDiGar().getTgaAcqExp().setTgaAcqExp(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaAcqExp().getWtgaAcqExp(), 15, 3));
        }
        // COB_CODE: IF (SF)-REMUN-ASS-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-REMUN-ASS-NULL
        //           ELSE
        //              TO TGA-REMUN-ASS
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaRemunAss().getWtgaRemunAssNullFormatted())) {
            // COB_CODE: MOVE (SF)-REMUN-ASS-NULL(IX-TAB-TGA)
            //           TO TGA-REMUN-ASS-NULL
            ws.getTrchDiGar().getTgaRemunAss().setTgaRemunAssNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaRemunAss().getWtgaRemunAssNull());
        }
        else {
            // COB_CODE: MOVE (SF)-REMUN-ASS(IX-TAB-TGA)
            //           TO TGA-REMUN-ASS
            ws.getTrchDiGar().getTgaRemunAss().setTgaRemunAss(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaRemunAss().getWtgaRemunAss(), 15, 3));
        }
        // COB_CODE: IF (SF)-COMMIS-INTER-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-COMMIS-INTER-NULL
        //           ELSE
        //              TO TGA-COMMIS-INTER
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaCommisInter().getWtgaCommisInterNullFormatted())) {
            // COB_CODE: MOVE (SF)-COMMIS-INTER-NULL(IX-TAB-TGA)
            //           TO TGA-COMMIS-INTER-NULL
            ws.getTrchDiGar().getTgaCommisInter().setTgaCommisInterNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaCommisInter().getWtgaCommisInterNull());
        }
        else {
            // COB_CODE: MOVE (SF)-COMMIS-INTER(IX-TAB-TGA)
            //           TO TGA-COMMIS-INTER
            ws.getTrchDiGar().getTgaCommisInter().setTgaCommisInter(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaCommisInter().getWtgaCommisInter(), 15, 3));
        }
        // COB_CODE: IF (SF)-ALQ-REMUN-ASS-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-ALQ-REMUN-ASS-NULL
        //           ELSE
        //              TO TGA-ALQ-REMUN-ASS
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaAlqRemunAss().getWtgaAlqRemunAssNullFormatted())) {
            // COB_CODE: MOVE (SF)-ALQ-REMUN-ASS-NULL(IX-TAB-TGA)
            //           TO TGA-ALQ-REMUN-ASS-NULL
            ws.getTrchDiGar().getTgaAlqRemunAss().setTgaAlqRemunAssNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaAlqRemunAss().getWtgaAlqRemunAssNull());
        }
        else {
            // COB_CODE: MOVE (SF)-ALQ-REMUN-ASS(IX-TAB-TGA)
            //           TO TGA-ALQ-REMUN-ASS
            ws.getTrchDiGar().getTgaAlqRemunAss().setTgaAlqRemunAss(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaAlqRemunAss().getWtgaAlqRemunAss(), 6, 3));
        }
        // COB_CODE: IF (SF)-ALQ-COMMIS-INTER-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-ALQ-COMMIS-INTER-NULL
        //           ELSE
        //              TO TGA-ALQ-COMMIS-INTER
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaAlqCommisInter().getWtgaAlqCommisInterNullFormatted())) {
            // COB_CODE: MOVE (SF)-ALQ-COMMIS-INTER-NULL(IX-TAB-TGA)
            //           TO TGA-ALQ-COMMIS-INTER-NULL
            ws.getTrchDiGar().getTgaAlqCommisInter().setTgaAlqCommisInterNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaAlqCommisInter().getWtgaAlqCommisInterNull());
        }
        else {
            // COB_CODE: MOVE (SF)-ALQ-COMMIS-INTER(IX-TAB-TGA)
            //           TO TGA-ALQ-COMMIS-INTER
            ws.getTrchDiGar().getTgaAlqCommisInter().setTgaAlqCommisInter(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaAlqCommisInter().getWtgaAlqCommisInter(), 6, 3));
        }
        // COB_CODE: IF (SF)-IMPB-REMUN-ASS-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-IMPB-REMUN-ASS-NULL
        //           ELSE
        //              TO TGA-IMPB-REMUN-ASS
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpbRemunAss().getWtgaImpbRemunAssNullFormatted())) {
            // COB_CODE: MOVE (SF)-IMPB-REMUN-ASS-NULL(IX-TAB-TGA)
            //           TO TGA-IMPB-REMUN-ASS-NULL
            ws.getTrchDiGar().getTgaImpbRemunAss().setTgaImpbRemunAssNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpbRemunAss().getWtgaImpbRemunAssNull());
        }
        else {
            // COB_CODE: MOVE (SF)-IMPB-REMUN-ASS(IX-TAB-TGA)
            //           TO TGA-IMPB-REMUN-ASS
            ws.getTrchDiGar().getTgaImpbRemunAss().setTgaImpbRemunAss(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpbRemunAss().getWtgaImpbRemunAss(), 15, 3));
        }
        // COB_CODE: IF (SF)-IMPB-COMMIS-INTER-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-IMPB-COMMIS-INTER-NULL
        //           ELSE
        //              TO TGA-IMPB-COMMIS-INTER
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpbCommisInter().getWtgaImpbCommisInterNullFormatted())) {
            // COB_CODE: MOVE (SF)-IMPB-COMMIS-INTER-NULL(IX-TAB-TGA)
            //           TO TGA-IMPB-COMMIS-INTER-NULL
            ws.getTrchDiGar().getTgaImpbCommisInter().setTgaImpbCommisInterNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpbCommisInter().getWtgaImpbCommisInterNull());
        }
        else {
            // COB_CODE: MOVE (SF)-IMPB-COMMIS-INTER(IX-TAB-TGA)
            //           TO TGA-IMPB-COMMIS-INTER
            ws.getTrchDiGar().getTgaImpbCommisInter().setTgaImpbCommisInter(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaImpbCommisInter().getWtgaImpbCommisInter(), 15, 3));
        }
        // COB_CODE: IF (SF)-COS-RUN-ASSVA-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-COS-RUN-ASSVA-NULL
        //           ELSE
        //              TO TGA-COS-RUN-ASSVA
        //           END-IF
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaCosRunAssva().getWtgaCosRunAssvaNullFormatted())) {
            // COB_CODE: MOVE (SF)-COS-RUN-ASSVA-NULL(IX-TAB-TGA)
            //           TO TGA-COS-RUN-ASSVA-NULL
            ws.getTrchDiGar().getTgaCosRunAssva().setTgaCosRunAssvaNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaCosRunAssva().getWtgaCosRunAssvaNull());
        }
        else {
            // COB_CODE: MOVE (SF)-COS-RUN-ASSVA(IX-TAB-TGA)
            //           TO TGA-COS-RUN-ASSVA
            ws.getTrchDiGar().getTgaCosRunAssva().setTgaCosRunAssva(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaCosRunAssva().getWtgaCosRunAssva(), 15, 3));
        }
        // COB_CODE: IF (SF)-COS-RUN-ASSVA-IDC-NULL(IX-TAB-TGA) = HIGH-VALUES
        //              TO TGA-COS-RUN-ASSVA-IDC-NULL
        //           ELSE
        //              TO TGA-COS-RUN-ASSVA-IDC
        //           END-IF.
        if (Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaCosRunAssvaIdc().getWtgaCosRunAssvaIdcNullFormatted())) {
            // COB_CODE: MOVE (SF)-COS-RUN-ASSVA-IDC-NULL(IX-TAB-TGA)
            //           TO TGA-COS-RUN-ASSVA-IDC-NULL
            ws.getTrchDiGar().getTgaCosRunAssvaIdc().setTgaCosRunAssvaIdcNull(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaCosRunAssvaIdc().getWtgaCosRunAssvaIdcNull());
        }
        else {
            // COB_CODE: MOVE (SF)-COS-RUN-ASSVA-IDC(IX-TAB-TGA)
            //           TO TGA-COS-RUN-ASSVA-IDC
            ws.getTrchDiGar().getTgaCosRunAssvaIdc().setTgaCosRunAssvaIdc(Trunc.toDecimal(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaCosRunAssvaIdc().getWtgaCosRunAssvaIdc(), 15, 3));
        }
    }

    /**Original name: AGGIORNA-TRCH-DI-GAR<br>
	 * <pre>----------------------------------------------------------------*
	 *     COPY      ..... LCCVTGA6
	 *     TIPOLOGIA...... SERVIZIO DI EOC
	 *     DESCRIZIONE.... AGGIORNAMENTO TRANCHE DI GARANZIA
	 * ----------------------------------------------------------------*
	 *     N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
	 *     ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
	 *   - COPY LCCV*5 (VALORIZZAZIONE DCLGEN)
	 *   - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
	 *   - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
	 *   - DICHIARAZIONE DCLGEN (LCCV*1)
	 *   - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
	 * ----------------------------------------------------------------*
	 * --  TABELLA STORICA</pre>*/
    private void aggiornaTrchDiGar() {
        // COB_CODE: INITIALIZE TRCH-DI-GAR.
        initTrchDiGar();
        //--> NOME TABELLA FISICA DB
        // COB_CODE: MOVE 'TRCH-DI-GAR'               TO WK-TABELLA.
        ws.setWkTabella("TRCH-DI-GAR");
        //--> SE LO STATUS NON E' "INVARIATO" O "CONVERSAZIONE"
        // COB_CODE: IF  NOT WTGA-ST-INV(IX-TAB-TGA)
        //           AND NOT WTGA-ST-CON(IX-TAB-TGA)
        //           AND WTGA-ELE-TRAN-MAX NOT = 0
        //              END-IF
        //           END-IF.
        if (!wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getStatus().isInv() && !wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getStatus().isWpmoStCon() && wtgaAreaTranche.getEleTranMax() != 0) {
            // COB_CODE: MOVE WMOV-ID-PTF              TO TGA-ID-MOVI-CRZ
            ws.getTrchDiGar().setTgaIdMoviCrz(areaMain.getWmovAreaMovimento().getLccvmov1().getIdPtf());
            //-->    INSERT
            // COB_CODE:         EVALUATE TRUE
            //           *-->    INSERT
            //                     WHEN WTGA-ST-ADD(IX-TAB-TGA)
            //           *-->         RICERCA DELL'ID-GAR NELLA TABELLA PADRE(GARANZIA)
            //                        END-IF
            //           *-->    DELETE
            //                     WHEN WTGA-ST-DEL(IX-TAB-TGA)
            //                        SET  IDSI0011-DELETE-LOGICA    TO TRUE
            //           *-->    UPDATE
            //                     WHEN WTGA-ST-MOD(IX-TAB-TGA)
            //                        SET  IDSI0011-AGGIORNAMENTO-STORICO   TO TRUE
            //                   END-EVALUATE
            switch (wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getStatus().getStatus()) {

                case WpolStatus.ADD://-->         RICERCA DELL'ID-GAR NELLA TABELLA PADRE(GARANZIA)
                    // COB_CODE: MOVE WTGA-ID-GAR(IX-TAB-TGA)  TO WS-ID-GARANZIA
                    ws.setWsIdGaranzia(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaIdGar());
                    // COB_CODE: PERFORM RICERCA-ID-GAR
                    //              THRU RICERCA-ID-GAR-EX
                    ricercaIdGar();
                    //-->         ESTRAZIONE E VALORIZZAZIONE SEQUENCE
                    // COB_CODE: PERFORM ESTR-SEQUENCE
                    //              THRU ESTR-SEQUENCE-EX
                    estrSequence();
                    // COB_CODE: IF IDSV0001-ESITO-OK
                    //              SET  IDSI0011-AGGIORNAMENTO-STORICO   TO TRUE
                    //           END-IF
                    if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                        // COB_CODE: MOVE S090-SEQ-TABELLA   TO WTGA-ID-PTF(IX-TAB-TGA)
                        //                                      TGA-ID-TRCH-DI-GAR
                        wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().setIdPtf(ws.getAreaIoLccs0090().getSeqTabella());
                        ws.getTrchDiGar().setTgaIdTrchDiGar(ws.getAreaIoLccs0090().getSeqTabella());
                        // COB_CODE: MOVE WPOL-ID-PTF        TO TGA-ID-POLI
                        ws.getTrchDiGar().setTgaIdPoli(areaMain.getWpolAreaPolizza().getLccvpol1().getIdPtf());
                        // COB_CODE: MOVE WADE-ID-PTF        TO TGA-ID-ADES
                        ws.getTrchDiGar().setTgaIdAdes(areaMain.getWadeAreaAdesione().getLccvade1().getIdPtf());
                        // COB_CODE: MOVE WS-ID-PTF-GAR      TO TGA-ID-GAR
                        ws.getTrchDiGar().setTgaIdGar(ws.getWsIdPtfGar());
                        //-->            TIPO OPERAZIONE DISPATCHER
                        // COB_CODE: SET  IDSI0011-AGGIORNAMENTO-STORICO   TO TRUE
                        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setIdsi0011AggiornamentoStorico();
                    }
                    //-->    DELETE
                    break;

                case WpolStatus.DEL:// COB_CODE: MOVE WTGA-ID-PTF(IX-TAB-TGA)
                    //                                 TO TGA-ID-TRCH-DI-GAR
                    ws.getTrchDiGar().setTgaIdTrchDiGar(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getIdPtf());
                    // COB_CODE: MOVE WPOL-ID-PTF      TO TGA-ID-POLI
                    ws.getTrchDiGar().setTgaIdPoli(areaMain.getWpolAreaPolizza().getLccvpol1().getIdPtf());
                    // COB_CODE: MOVE WADE-ID-PTF      TO TGA-ID-ADES
                    ws.getTrchDiGar().setTgaIdAdes(areaMain.getWadeAreaAdesione().getLccvade1().getIdPtf());
                    // COB_CODE: MOVE WTGA-ID-GAR(IX-TAB-TGA)
                    //             TO TGA-ID-GAR
                    ws.getTrchDiGar().setTgaIdGar(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaIdGar());
                    //-->         TIPO OPERAZIONE DISPATCHER
                    // COB_CODE: SET  IDSI0011-DELETE-LOGICA    TO TRUE
                    ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setIdsi0011DeleteLogica();
                    //-->    UPDATE
                    break;

                case WpolStatus.MOD:// COB_CODE: MOVE WTGA-ID-PTF(IX-TAB-TGA)
                    //                                 TO TGA-ID-TRCH-DI-GAR
                    ws.getTrchDiGar().setTgaIdTrchDiGar(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getIdPtf());
                    // COB_CODE: MOVE WPOL-ID-PTF      TO TGA-ID-POLI
                    ws.getTrchDiGar().setTgaIdPoli(areaMain.getWpolAreaPolizza().getLccvpol1().getIdPtf());
                    // COB_CODE: MOVE WADE-ID-PTF      TO TGA-ID-ADES
                    ws.getTrchDiGar().setTgaIdAdes(areaMain.getWadeAreaAdesione().getLccvade1().getIdPtf());
                    // COB_CODE: MOVE WTGA-ID-GAR(IX-TAB-TGA)
                    //             TO TGA-ID-GAR
                    ws.getTrchDiGar().setTgaIdGar(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTga()).getLccvtga1().getDati().getWtgaIdGar());
                    //-->         TIPO OPERAZIONE DISPATCHER
                    // COB_CODE: SET  IDSI0011-AGGIORNAMENTO-STORICO   TO TRUE
                    ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setIdsi0011AggiornamentoStorico();
                    break;

                default:break;
            }
            // COB_CODE:         IF IDSV0001-ESITO-OK
            //           *-->       VALORIZZA DCLGEN TRANCHE DI GARANZIA
            //                         THRU AGGIORNA-TABELLA-EX
            //                   END-IF
            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                //-->       VALORIZZA DCLGEN TRANCHE DI GARANZIA
                // COB_CODE: PERFORM VAL-DCLGEN-TGA
                //              THRU VAL-DCLGEN-TGA-EX
                valDclgenTga();
                //-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                // COB_CODE: PERFORM VALORIZZA-AREA-DSH-TGA
                //              THRU VALORIZZA-AREA-DSH-TGA-EX
                valorizzaAreaDshTga();
                //-->       CALL DISPATCHER PER AGGIORNAMENTO
                // COB_CODE: PERFORM AGGIORNA-TABELLA
                //              THRU AGGIORNA-TABELLA-EX
                aggiornaTabella();
            }
        }
    }

    /**Original name: RICERCA-ID-GAR<br>
	 * <pre>----------------------------------------------------------------*
	 *     RICERCA DELL'ID-GAR DELLA COPY LCCVTGA1 NELLA TAB. GARANZIA
	 * ----------------------------------------------------------------*</pre>*/
    private void ricercaIdGar() {
        // COB_CODE: SET NON-TROVATO        TO TRUE.
        ws.getFlag().setNonTrovato();
        // COB_CODE: PERFORM VARYING IX-TAB-GRZ FROM 1 BY 1
        //             UNTIL IX-TAB-GRZ > WGRZ-ELE-GAR-MAX
        //                OR TROVATO
        //                END-IF
        //           END-PERFORM.
        ws.getIxIndici().setGrz(((short)1));
        while (!(ws.getIxIndici().getGrz() > wgrzAreaGaranzia.getEleGarMax() || ws.getFlag().isTrovato())) {
            // COB_CODE: IF WS-ID-GARANZIA = WGRZ-ID-GAR(IX-TAB-GRZ)
            //              SET  TROVATO   TO TRUE
            //           END-IF
            if (ws.getWsIdGaranzia() == wgrzAreaGaranzia.getTabGar(ws.getIxIndici().getGrz()).getLccvgrz1().getDati().getWgrzIdGar()) {
                // COB_CODE: MOVE WGRZ-ID-PTF(IX-TAB-GRZ)
                //                          TO WS-ID-PTF-GAR
                ws.setWsIdPtfGar(wgrzAreaGaranzia.getTabGar(ws.getIxIndici().getGrz()).getLccvgrz1().getIdPtf());
                // COB_CODE: SET  TROVATO   TO TRUE
                ws.getFlag().setTrovato();
            }
            ws.getIxIndici().setGrz(Trunc.toShort(ws.getIxIndici().getGrz() + 1, 4));
        }
    }

    /**Original name: VALORIZZA-AREA-DSH-TGA<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZA AREA COMUNE DISPATCHER
	 * ----------------------------------------------------------------*
	 * --> DCLGEN TABELLA</pre>*/
    private void valorizzaAreaDshTga() {
        // COB_CODE: MOVE TRCH-DI-GAR               TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getTrchDiGar().getTrchDiGarFormatted());
        //--> MODALITA DI ACCESSO
        // COB_CODE: SET  IDSI0011-ID               TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setIdsi0011Id();
        //--> TIPO TABELLA (STORICA/NON STORICA)
        // COB_CODE: SET  IDSI0011-TRATT-DEFAULT  TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setIdsi0011TrattDefault();
        //--> VALORIZZAZIONE DATE EFFETTO
        // COB_CODE: MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaMain.getWmovAreaMovimento().getLccvmov1().getDati().getWmovDtEff());
        // COB_CODE: MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        // COB_CODE: MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
    }

    /**Original name: VAL-DCLGEN-TDR<br>
	 * <pre>----------------------------------------------------------------*
	 *     COPY VALORIZZAZIONE DCLGEN
	 * ----------------------------------------------------------------*
	 *      COPY LCCVPMO5.
	 * ------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    COPY LCCVTDR5
	 *    ULTIMO AGG. 05 DIC 2014
	 * ------------------------------------------------------------</pre>*/
    private void valDclgenTdr() {
        // COB_CODE: MOVE (SF)-TP-OGG(IX-TAB-TDR)
        //              TO TDR-TP-OGG
        ws.getTitRat().setTdrTpOgg(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTpOgg());
        // COB_CODE: IF (SF)-ID-MOVI-CHIU-NULL(IX-TAB-TDR) = HIGH-VALUES
        //              TO TDR-ID-MOVI-CHIU-NULL
        //           ELSE
        //              TO TDR-ID-MOVI-CHIU
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrIdMoviChiu().getWtdrIdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE (SF)-ID-MOVI-CHIU-NULL(IX-TAB-TDR)
            //           TO TDR-ID-MOVI-CHIU-NULL
            ws.getTitRat().getTdrIdMoviChiu().setTdrIdMoviChiuNull(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrIdMoviChiu().getWtdrIdMoviChiuNull());
        }
        else {
            // COB_CODE: MOVE (SF)-ID-MOVI-CHIU(IX-TAB-TDR)
            //           TO TDR-ID-MOVI-CHIU
            ws.getTitRat().getTdrIdMoviChiu().setTdrIdMoviChiu(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrIdMoviChiu().getWtdrIdMoviChiu());
        }
        // COB_CODE: IF (SF)-DT-INI-EFF(IX-TAB-TDR) NOT NUMERIC
        //              MOVE 0 TO TDR-DT-INI-EFF
        //           ELSE
        //              TO TDR-DT-INI-EFF
        //           END-IF
        if (!Functions.isNumber(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrDtIniEff())) {
            // COB_CODE: MOVE 0 TO TDR-DT-INI-EFF
            ws.getTitRat().setTdrDtIniEff(0);
        }
        else {
            // COB_CODE: MOVE (SF)-DT-INI-EFF(IX-TAB-TDR)
            //           TO TDR-DT-INI-EFF
            ws.getTitRat().setTdrDtIniEff(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrDtIniEff());
        }
        // COB_CODE: IF (SF)-DT-END-EFF(IX-TAB-TDR) NOT NUMERIC
        //              MOVE 0 TO TDR-DT-END-EFF
        //           ELSE
        //              TO TDR-DT-END-EFF
        //           END-IF
        if (!Functions.isNumber(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrDtEndEff())) {
            // COB_CODE: MOVE 0 TO TDR-DT-END-EFF
            ws.getTitRat().setTdrDtEndEff(0);
        }
        else {
            // COB_CODE: MOVE (SF)-DT-END-EFF(IX-TAB-TDR)
            //           TO TDR-DT-END-EFF
            ws.getTitRat().setTdrDtEndEff(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrDtEndEff());
        }
        // COB_CODE: MOVE (SF)-COD-COMP-ANIA(IX-TAB-TDR)
        //              TO TDR-COD-COMP-ANIA
        ws.getTitRat().setTdrCodCompAnia(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrCodCompAnia());
        // COB_CODE: MOVE (SF)-TP-TIT(IX-TAB-TDR)
        //              TO TDR-TP-TIT
        ws.getTitRat().setTdrTpTit(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTpTit());
        // COB_CODE: IF (SF)-PROG-TIT-NULL(IX-TAB-TDR) = HIGH-VALUES
        //              TO TDR-PROG-TIT-NULL
        //           ELSE
        //              TO TDR-PROG-TIT
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrProgTit().getWtdrProgTitNullFormatted())) {
            // COB_CODE: MOVE (SF)-PROG-TIT-NULL(IX-TAB-TDR)
            //           TO TDR-PROG-TIT-NULL
            ws.getTitRat().getTdrProgTit().setTdrProgTitNull(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrProgTit().getWtdrProgTitNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PROG-TIT(IX-TAB-TDR)
            //           TO TDR-PROG-TIT
            ws.getTitRat().getTdrProgTit().setTdrProgTit(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrProgTit().getWtdrProgTit());
        }
        // COB_CODE: MOVE (SF)-TP-PRE-TIT(IX-TAB-TDR)
        //              TO TDR-TP-PRE-TIT
        ws.getTitRat().setTdrTpPreTit(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTpPreTit());
        // COB_CODE: MOVE (SF)-TP-STAT-TIT(IX-TAB-TDR)
        //              TO TDR-TP-STAT-TIT
        ws.getTitRat().setTdrTpStatTit(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTpStatTit());
        // COB_CODE: IF (SF)-DT-INI-COP-NULL(IX-TAB-TDR) = HIGH-VALUES
        //              TO TDR-DT-INI-COP-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrDtIniCop().getWtdrDtIniCopNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-INI-COP-NULL(IX-TAB-TDR)
            //           TO TDR-DT-INI-COP-NULL
            ws.getTitRat().getTdrDtIniCop().setTdrDtIniCopNull(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrDtIniCop().getWtdrDtIniCopNull());
        }
        else if (ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrDtIniCop().getWtdrDtIniCop() == 0) {
            // COB_CODE: IF (SF)-DT-INI-COP(IX-TAB-TDR) = ZERO
            //              TO TDR-DT-INI-COP-NULL
            //           ELSE
            //            TO TDR-DT-INI-COP
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO TDR-DT-INI-COP-NULL
            ws.getTitRat().getTdrDtIniCop().setTdrDtIniCopNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TdrDtIniCop.Len.TDR_DT_INI_COP_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-INI-COP(IX-TAB-TDR)
            //           TO TDR-DT-INI-COP
            ws.getTitRat().getTdrDtIniCop().setTdrDtIniCop(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrDtIniCop().getWtdrDtIniCop());
        }
        // COB_CODE: IF (SF)-DT-END-COP-NULL(IX-TAB-TDR) = HIGH-VALUES
        //              TO TDR-DT-END-COP-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrDtEndCop().getWtdrDtEndCopNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-END-COP-NULL(IX-TAB-TDR)
            //           TO TDR-DT-END-COP-NULL
            ws.getTitRat().getTdrDtEndCop().setTdrDtEndCopNull(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrDtEndCop().getWtdrDtEndCopNull());
        }
        else if (ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrDtEndCop().getWtdrDtEndCop() == 0) {
            // COB_CODE: IF (SF)-DT-END-COP(IX-TAB-TDR) = ZERO
            //              TO TDR-DT-END-COP-NULL
            //           ELSE
            //            TO TDR-DT-END-COP
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO TDR-DT-END-COP-NULL
            ws.getTitRat().getTdrDtEndCop().setTdrDtEndCopNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TdrDtEndCop.Len.TDR_DT_END_COP_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-END-COP(IX-TAB-TDR)
            //           TO TDR-DT-END-COP
            ws.getTitRat().getTdrDtEndCop().setTdrDtEndCop(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrDtEndCop().getWtdrDtEndCop());
        }
        // COB_CODE: IF (SF)-IMP-PAG-NULL(IX-TAB-TDR) = HIGH-VALUES
        //              TO TDR-IMP-PAG-NULL
        //           ELSE
        //              TO TDR-IMP-PAG
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrImpPag().getWtdrImpPagNullFormatted())) {
            // COB_CODE: MOVE (SF)-IMP-PAG-NULL(IX-TAB-TDR)
            //           TO TDR-IMP-PAG-NULL
            ws.getTitRat().getTdrImpPag().setTdrImpPagNull(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrImpPag().getWtdrImpPagNull());
        }
        else {
            // COB_CODE: MOVE (SF)-IMP-PAG(IX-TAB-TDR)
            //           TO TDR-IMP-PAG
            ws.getTitRat().getTdrImpPag().setTdrImpPag(Trunc.toDecimal(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrImpPag().getWtdrImpPag(), 15, 3));
        }
        // COB_CODE: IF (SF)-FL-SOLL-NULL(IX-TAB-TDR) = HIGH-VALUES
        //              TO TDR-FL-SOLL-NULL
        //           ELSE
        //              TO TDR-FL-SOLL
        //           END-IF
        if (Conditions.eq(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrFlSoll(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE (SF)-FL-SOLL-NULL(IX-TAB-TDR)
            //           TO TDR-FL-SOLL-NULL
            ws.getTitRat().setTdrFlSoll(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrFlSoll());
        }
        else {
            // COB_CODE: MOVE (SF)-FL-SOLL(IX-TAB-TDR)
            //           TO TDR-FL-SOLL
            ws.getTitRat().setTdrFlSoll(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrFlSoll());
        }
        // COB_CODE: IF (SF)-FRAZ-NULL(IX-TAB-TDR) = HIGH-VALUES
        //              TO TDR-FRAZ-NULL
        //           ELSE
        //              TO TDR-FRAZ
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrFraz().getWtdrFrazNullFormatted())) {
            // COB_CODE: MOVE (SF)-FRAZ-NULL(IX-TAB-TDR)
            //           TO TDR-FRAZ-NULL
            ws.getTitRat().getTdrFraz().setTdrFrazNull(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrFraz().getWtdrFrazNull());
        }
        else {
            // COB_CODE: MOVE (SF)-FRAZ(IX-TAB-TDR)
            //           TO TDR-FRAZ
            ws.getTitRat().getTdrFraz().setTdrFraz(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrFraz().getWtdrFraz());
        }
        // COB_CODE: IF (SF)-DT-APPLZ-MORA-NULL(IX-TAB-TDR) = HIGH-VALUES
        //              TO TDR-DT-APPLZ-MORA-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrDtApplzMora().getWtdrDtApplzMoraNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-APPLZ-MORA-NULL(IX-TAB-TDR)
            //           TO TDR-DT-APPLZ-MORA-NULL
            ws.getTitRat().getTdrDtApplzMora().setTdrDtApplzMoraNull(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrDtApplzMora().getWtdrDtApplzMoraNull());
        }
        else if (ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrDtApplzMora().getWtdrDtApplzMora() == 0) {
            // COB_CODE: IF (SF)-DT-APPLZ-MORA(IX-TAB-TDR) = ZERO
            //              TO TDR-DT-APPLZ-MORA-NULL
            //           ELSE
            //            TO TDR-DT-APPLZ-MORA
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO TDR-DT-APPLZ-MORA-NULL
            ws.getTitRat().getTdrDtApplzMora().setTdrDtApplzMoraNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TdrDtApplzMora.Len.TDR_DT_APPLZ_MORA_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-APPLZ-MORA(IX-TAB-TDR)
            //           TO TDR-DT-APPLZ-MORA
            ws.getTitRat().getTdrDtApplzMora().setTdrDtApplzMora(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrDtApplzMora().getWtdrDtApplzMora());
        }
        // COB_CODE: IF (SF)-FL-MORA-NULL(IX-TAB-TDR) = HIGH-VALUES
        //              TO TDR-FL-MORA-NULL
        //           ELSE
        //              TO TDR-FL-MORA
        //           END-IF
        if (Conditions.eq(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrFlMora(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE (SF)-FL-MORA-NULL(IX-TAB-TDR)
            //           TO TDR-FL-MORA-NULL
            ws.getTitRat().setTdrFlMora(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrFlMora());
        }
        else {
            // COB_CODE: MOVE (SF)-FL-MORA(IX-TAB-TDR)
            //           TO TDR-FL-MORA
            ws.getTitRat().setTdrFlMora(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrFlMora());
        }
        // COB_CODE: IF (SF)-ID-RAPP-RETE-NULL(IX-TAB-TDR) = HIGH-VALUES
        //              TO TDR-ID-RAPP-RETE-NULL
        //           ELSE
        //              TO TDR-ID-RAPP-RETE
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrIdRappRete().getWtdrIdRappReteNullFormatted())) {
            // COB_CODE: MOVE (SF)-ID-RAPP-RETE-NULL(IX-TAB-TDR)
            //           TO TDR-ID-RAPP-RETE-NULL
            ws.getTitRat().getTdrIdRappRete().setTdrIdRappReteNull(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrIdRappRete().getWtdrIdRappReteNull());
        }
        else {
            // COB_CODE: MOVE (SF)-ID-RAPP-RETE(IX-TAB-TDR)
            //           TO TDR-ID-RAPP-RETE
            ws.getTitRat().getTdrIdRappRete().setTdrIdRappRete(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrIdRappRete().getWtdrIdRappRete());
        }
        // COB_CODE: IF (SF)-ID-RAPP-ANA-NULL(IX-TAB-TDR) = HIGH-VALUES
        //              TO TDR-ID-RAPP-ANA-NULL
        //           ELSE
        //              TO TDR-ID-RAPP-ANA
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrIdRappAna().getWtdrIdRappAnaNullFormatted())) {
            // COB_CODE: MOVE (SF)-ID-RAPP-ANA-NULL(IX-TAB-TDR)
            //           TO TDR-ID-RAPP-ANA-NULL
            ws.getTitRat().getTdrIdRappAna().setTdrIdRappAnaNull(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrIdRappAna().getWtdrIdRappAnaNull());
        }
        else {
            // COB_CODE: MOVE (SF)-ID-RAPP-ANA(IX-TAB-TDR)
            //           TO TDR-ID-RAPP-ANA
            ws.getTitRat().getTdrIdRappAna().setTdrIdRappAna(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrIdRappAna().getWtdrIdRappAna());
        }
        // COB_CODE: IF (SF)-COD-DVS-NULL(IX-TAB-TDR) = HIGH-VALUES
        //              TO TDR-COD-DVS-NULL
        //           ELSE
        //              TO TDR-COD-DVS
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrCodDvsFormatted())) {
            // COB_CODE: MOVE (SF)-COD-DVS-NULL(IX-TAB-TDR)
            //           TO TDR-COD-DVS-NULL
            ws.getTitRat().setTdrCodDvs(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrCodDvs());
        }
        else {
            // COB_CODE: MOVE (SF)-COD-DVS(IX-TAB-TDR)
            //           TO TDR-COD-DVS
            ws.getTitRat().setTdrCodDvs(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrCodDvs());
        }
        // COB_CODE: MOVE (SF)-DT-EMIS-TIT(IX-TAB-TDR)
        //              TO TDR-DT-EMIS-TIT
        ws.getTitRat().setTdrDtEmisTit(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrDtEmisTit());
        // COB_CODE: IF (SF)-DT-ESI-TIT-NULL(IX-TAB-TDR) = HIGH-VALUES
        //              TO TDR-DT-ESI-TIT-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrDtEsiTit().getWtdrDtEsiTitNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ESI-TIT-NULL(IX-TAB-TDR)
            //           TO TDR-DT-ESI-TIT-NULL
            ws.getTitRat().getTdrDtEsiTit().setTdrDtEsiTitNull(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrDtEsiTit().getWtdrDtEsiTitNull());
        }
        else if (ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrDtEsiTit().getWtdrDtEsiTit() == 0) {
            // COB_CODE: IF (SF)-DT-ESI-TIT(IX-TAB-TDR) = ZERO
            //              TO TDR-DT-ESI-TIT-NULL
            //           ELSE
            //            TO TDR-DT-ESI-TIT
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO TDR-DT-ESI-TIT-NULL
            ws.getTitRat().getTdrDtEsiTit().setTdrDtEsiTitNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TdrDtEsiTit.Len.TDR_DT_ESI_TIT_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ESI-TIT(IX-TAB-TDR)
            //           TO TDR-DT-ESI-TIT
            ws.getTitRat().getTdrDtEsiTit().setTdrDtEsiTit(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrDtEsiTit().getWtdrDtEsiTit());
        }
        // COB_CODE: IF (SF)-TOT-PRE-NET-NULL(IX-TAB-TDR) = HIGH-VALUES
        //              TO TDR-TOT-PRE-NET-NULL
        //           ELSE
        //              TO TDR-TOT-PRE-NET
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotPreNet().getWtdrTotPreNetNullFormatted())) {
            // COB_CODE: MOVE (SF)-TOT-PRE-NET-NULL(IX-TAB-TDR)
            //           TO TDR-TOT-PRE-NET-NULL
            ws.getTitRat().getTdrTotPreNet().setTdrTotPreNetNull(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotPreNet().getWtdrTotPreNetNull());
        }
        else {
            // COB_CODE: MOVE (SF)-TOT-PRE-NET(IX-TAB-TDR)
            //           TO TDR-TOT-PRE-NET
            ws.getTitRat().getTdrTotPreNet().setTdrTotPreNet(Trunc.toDecimal(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotPreNet().getWtdrTotPreNet(), 15, 3));
        }
        // COB_CODE: IF (SF)-TOT-INTR-FRAZ-NULL(IX-TAB-TDR) = HIGH-VALUES
        //              TO TDR-TOT-INTR-FRAZ-NULL
        //           ELSE
        //              TO TDR-TOT-INTR-FRAZ
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotIntrFraz().getWtdrTotIntrFrazNullFormatted())) {
            // COB_CODE: MOVE (SF)-TOT-INTR-FRAZ-NULL(IX-TAB-TDR)
            //           TO TDR-TOT-INTR-FRAZ-NULL
            ws.getTitRat().getTdrTotIntrFraz().setTdrTotIntrFrazNull(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotIntrFraz().getWtdrTotIntrFrazNull());
        }
        else {
            // COB_CODE: MOVE (SF)-TOT-INTR-FRAZ(IX-TAB-TDR)
            //           TO TDR-TOT-INTR-FRAZ
            ws.getTitRat().getTdrTotIntrFraz().setTdrTotIntrFraz(Trunc.toDecimal(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotIntrFraz().getWtdrTotIntrFraz(), 15, 3));
        }
        // COB_CODE: IF (SF)-TOT-INTR-MORA-NULL(IX-TAB-TDR) = HIGH-VALUES
        //              TO TDR-TOT-INTR-MORA-NULL
        //           ELSE
        //              TO TDR-TOT-INTR-MORA
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotIntrMora().getWtdrTotIntrMoraNullFormatted())) {
            // COB_CODE: MOVE (SF)-TOT-INTR-MORA-NULL(IX-TAB-TDR)
            //           TO TDR-TOT-INTR-MORA-NULL
            ws.getTitRat().getTdrTotIntrMora().setTdrTotIntrMoraNull(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotIntrMora().getWtdrTotIntrMoraNull());
        }
        else {
            // COB_CODE: MOVE (SF)-TOT-INTR-MORA(IX-TAB-TDR)
            //           TO TDR-TOT-INTR-MORA
            ws.getTitRat().getTdrTotIntrMora().setTdrTotIntrMora(Trunc.toDecimal(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotIntrMora().getWtdrTotIntrMora(), 15, 3));
        }
        // COB_CODE: IF (SF)-TOT-INTR-PREST-NULL(IX-TAB-TDR) = HIGH-VALUES
        //              TO TDR-TOT-INTR-PREST-NULL
        //           ELSE
        //              TO TDR-TOT-INTR-PREST
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotIntrPrest().getWtdrTotIntrPrestNullFormatted())) {
            // COB_CODE: MOVE (SF)-TOT-INTR-PREST-NULL(IX-TAB-TDR)
            //           TO TDR-TOT-INTR-PREST-NULL
            ws.getTitRat().getTdrTotIntrPrest().setTdrTotIntrPrestNull(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotIntrPrest().getWtdrTotIntrPrestNull());
        }
        else {
            // COB_CODE: MOVE (SF)-TOT-INTR-PREST(IX-TAB-TDR)
            //           TO TDR-TOT-INTR-PREST
            ws.getTitRat().getTdrTotIntrPrest().setTdrTotIntrPrest(Trunc.toDecimal(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotIntrPrest().getWtdrTotIntrPrest(), 15, 3));
        }
        // COB_CODE: IF (SF)-TOT-INTR-RETDT-NULL(IX-TAB-TDR) = HIGH-VALUES
        //              TO TDR-TOT-INTR-RETDT-NULL
        //           ELSE
        //              TO TDR-TOT-INTR-RETDT
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotIntrRetdt().getWtdrTotIntrRetdtNullFormatted())) {
            // COB_CODE: MOVE (SF)-TOT-INTR-RETDT-NULL(IX-TAB-TDR)
            //           TO TDR-TOT-INTR-RETDT-NULL
            ws.getTitRat().getTdrTotIntrRetdt().setTdrTotIntrRetdtNull(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotIntrRetdt().getWtdrTotIntrRetdtNull());
        }
        else {
            // COB_CODE: MOVE (SF)-TOT-INTR-RETDT(IX-TAB-TDR)
            //           TO TDR-TOT-INTR-RETDT
            ws.getTitRat().getTdrTotIntrRetdt().setTdrTotIntrRetdt(Trunc.toDecimal(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotIntrRetdt().getWtdrTotIntrRetdt(), 15, 3));
        }
        // COB_CODE: IF (SF)-TOT-INTR-RIAT-NULL(IX-TAB-TDR) = HIGH-VALUES
        //              TO TDR-TOT-INTR-RIAT-NULL
        //           ELSE
        //              TO TDR-TOT-INTR-RIAT
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotIntrRiat().getWtdrTotIntrRiatNullFormatted())) {
            // COB_CODE: MOVE (SF)-TOT-INTR-RIAT-NULL(IX-TAB-TDR)
            //           TO TDR-TOT-INTR-RIAT-NULL
            ws.getTitRat().getTdrTotIntrRiat().setTdrTotIntrRiatNull(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotIntrRiat().getWtdrTotIntrRiatNull());
        }
        else {
            // COB_CODE: MOVE (SF)-TOT-INTR-RIAT(IX-TAB-TDR)
            //           TO TDR-TOT-INTR-RIAT
            ws.getTitRat().getTdrTotIntrRiat().setTdrTotIntrRiat(Trunc.toDecimal(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotIntrRiat().getWtdrTotIntrRiat(), 15, 3));
        }
        // COB_CODE: IF (SF)-TOT-DIR-NULL(IX-TAB-TDR) = HIGH-VALUES
        //              TO TDR-TOT-DIR-NULL
        //           ELSE
        //              TO TDR-TOT-DIR
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotDir().getWtdrTotDirNullFormatted())) {
            // COB_CODE: MOVE (SF)-TOT-DIR-NULL(IX-TAB-TDR)
            //           TO TDR-TOT-DIR-NULL
            ws.getTitRat().getTdrTotDir().setTdrTotDirNull(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotDir().getWtdrTotDirNull());
        }
        else {
            // COB_CODE: MOVE (SF)-TOT-DIR(IX-TAB-TDR)
            //           TO TDR-TOT-DIR
            ws.getTitRat().getTdrTotDir().setTdrTotDir(Trunc.toDecimal(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotDir().getWtdrTotDir(), 15, 3));
        }
        // COB_CODE: IF (SF)-TOT-SPE-MED-NULL(IX-TAB-TDR) = HIGH-VALUES
        //              TO TDR-TOT-SPE-MED-NULL
        //           ELSE
        //              TO TDR-TOT-SPE-MED
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotSpeMed().getWtdrTotSpeMedNullFormatted())) {
            // COB_CODE: MOVE (SF)-TOT-SPE-MED-NULL(IX-TAB-TDR)
            //           TO TDR-TOT-SPE-MED-NULL
            ws.getTitRat().getTdrTotSpeMed().setTdrTotSpeMedNull(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotSpeMed().getWtdrTotSpeMedNull());
        }
        else {
            // COB_CODE: MOVE (SF)-TOT-SPE-MED(IX-TAB-TDR)
            //           TO TDR-TOT-SPE-MED
            ws.getTitRat().getTdrTotSpeMed().setTdrTotSpeMed(Trunc.toDecimal(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotSpeMed().getWtdrTotSpeMed(), 15, 3));
        }
        // COB_CODE: IF (SF)-TOT-SPE-AGE-NULL(IX-TAB-TDR) = HIGH-VALUES
        //              TO TDR-TOT-SPE-AGE-NULL
        //           ELSE
        //              TO TDR-TOT-SPE-AGE
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotSpeAge().getWtdrTotSpeAgeNullFormatted())) {
            // COB_CODE: MOVE (SF)-TOT-SPE-AGE-NULL(IX-TAB-TDR)
            //           TO TDR-TOT-SPE-AGE-NULL
            ws.getTitRat().getTdrTotSpeAge().setTdrTotSpeAgeNull(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotSpeAge().getWtdrTotSpeAgeNull());
        }
        else {
            // COB_CODE: MOVE (SF)-TOT-SPE-AGE(IX-TAB-TDR)
            //           TO TDR-TOT-SPE-AGE
            ws.getTitRat().getTdrTotSpeAge().setTdrTotSpeAge(Trunc.toDecimal(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotSpeAge().getWtdrTotSpeAge(), 15, 3));
        }
        // COB_CODE: IF (SF)-TOT-TAX-NULL(IX-TAB-TDR) = HIGH-VALUES
        //              TO TDR-TOT-TAX-NULL
        //           ELSE
        //              TO TDR-TOT-TAX
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotTax().getWtdrTotTaxNullFormatted())) {
            // COB_CODE: MOVE (SF)-TOT-TAX-NULL(IX-TAB-TDR)
            //           TO TDR-TOT-TAX-NULL
            ws.getTitRat().getTdrTotTax().setTdrTotTaxNull(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotTax().getWtdrTotTaxNull());
        }
        else {
            // COB_CODE: MOVE (SF)-TOT-TAX(IX-TAB-TDR)
            //           TO TDR-TOT-TAX
            ws.getTitRat().getTdrTotTax().setTdrTotTax(Trunc.toDecimal(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotTax().getWtdrTotTax(), 15, 3));
        }
        // COB_CODE: IF (SF)-TOT-SOPR-SAN-NULL(IX-TAB-TDR) = HIGH-VALUES
        //              TO TDR-TOT-SOPR-SAN-NULL
        //           ELSE
        //              TO TDR-TOT-SOPR-SAN
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotSoprSan().getWtdrTotSoprSanNullFormatted())) {
            // COB_CODE: MOVE (SF)-TOT-SOPR-SAN-NULL(IX-TAB-TDR)
            //           TO TDR-TOT-SOPR-SAN-NULL
            ws.getTitRat().getTdrTotSoprSan().setTdrTotSoprSanNull(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotSoprSan().getWtdrTotSoprSanNull());
        }
        else {
            // COB_CODE: MOVE (SF)-TOT-SOPR-SAN(IX-TAB-TDR)
            //           TO TDR-TOT-SOPR-SAN
            ws.getTitRat().getTdrTotSoprSan().setTdrTotSoprSan(Trunc.toDecimal(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotSoprSan().getWtdrTotSoprSan(), 15, 3));
        }
        // COB_CODE: IF (SF)-TOT-SOPR-TEC-NULL(IX-TAB-TDR) = HIGH-VALUES
        //              TO TDR-TOT-SOPR-TEC-NULL
        //           ELSE
        //              TO TDR-TOT-SOPR-TEC
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotSoprTec().getWtdrTotSoprTecNullFormatted())) {
            // COB_CODE: MOVE (SF)-TOT-SOPR-TEC-NULL(IX-TAB-TDR)
            //           TO TDR-TOT-SOPR-TEC-NULL
            ws.getTitRat().getTdrTotSoprTec().setTdrTotSoprTecNull(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotSoprTec().getWtdrTotSoprTecNull());
        }
        else {
            // COB_CODE: MOVE (SF)-TOT-SOPR-TEC(IX-TAB-TDR)
            //           TO TDR-TOT-SOPR-TEC
            ws.getTitRat().getTdrTotSoprTec().setTdrTotSoprTec(Trunc.toDecimal(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotSoprTec().getWtdrTotSoprTec(), 15, 3));
        }
        // COB_CODE: IF (SF)-TOT-SOPR-SPO-NULL(IX-TAB-TDR) = HIGH-VALUES
        //              TO TDR-TOT-SOPR-SPO-NULL
        //           ELSE
        //              TO TDR-TOT-SOPR-SPO
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotSoprSpo().getWtdrTotSoprSpoNullFormatted())) {
            // COB_CODE: MOVE (SF)-TOT-SOPR-SPO-NULL(IX-TAB-TDR)
            //           TO TDR-TOT-SOPR-SPO-NULL
            ws.getTitRat().getTdrTotSoprSpo().setTdrTotSoprSpoNull(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotSoprSpo().getWtdrTotSoprSpoNull());
        }
        else {
            // COB_CODE: MOVE (SF)-TOT-SOPR-SPO(IX-TAB-TDR)
            //           TO TDR-TOT-SOPR-SPO
            ws.getTitRat().getTdrTotSoprSpo().setTdrTotSoprSpo(Trunc.toDecimal(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotSoprSpo().getWtdrTotSoprSpo(), 15, 3));
        }
        // COB_CODE: IF (SF)-TOT-SOPR-PROF-NULL(IX-TAB-TDR) = HIGH-VALUES
        //              TO TDR-TOT-SOPR-PROF-NULL
        //           ELSE
        //              TO TDR-TOT-SOPR-PROF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotSoprProf().getWtdrTotSoprProfNullFormatted())) {
            // COB_CODE: MOVE (SF)-TOT-SOPR-PROF-NULL(IX-TAB-TDR)
            //           TO TDR-TOT-SOPR-PROF-NULL
            ws.getTitRat().getTdrTotSoprProf().setTdrTotSoprProfNull(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotSoprProf().getWtdrTotSoprProfNull());
        }
        else {
            // COB_CODE: MOVE (SF)-TOT-SOPR-PROF(IX-TAB-TDR)
            //           TO TDR-TOT-SOPR-PROF
            ws.getTitRat().getTdrTotSoprProf().setTdrTotSoprProf(Trunc.toDecimal(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotSoprProf().getWtdrTotSoprProf(), 15, 3));
        }
        // COB_CODE: IF (SF)-TOT-SOPR-ALT-NULL(IX-TAB-TDR) = HIGH-VALUES
        //              TO TDR-TOT-SOPR-ALT-NULL
        //           ELSE
        //              TO TDR-TOT-SOPR-ALT
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotSoprAlt().getWtdrTotSoprAltNullFormatted())) {
            // COB_CODE: MOVE (SF)-TOT-SOPR-ALT-NULL(IX-TAB-TDR)
            //           TO TDR-TOT-SOPR-ALT-NULL
            ws.getTitRat().getTdrTotSoprAlt().setTdrTotSoprAltNull(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotSoprAlt().getWtdrTotSoprAltNull());
        }
        else {
            // COB_CODE: MOVE (SF)-TOT-SOPR-ALT(IX-TAB-TDR)
            //           TO TDR-TOT-SOPR-ALT
            ws.getTitRat().getTdrTotSoprAlt().setTdrTotSoprAlt(Trunc.toDecimal(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotSoprAlt().getWtdrTotSoprAlt(), 15, 3));
        }
        // COB_CODE: IF (SF)-TOT-PRE-TOT-NULL(IX-TAB-TDR) = HIGH-VALUES
        //              TO TDR-TOT-PRE-TOT-NULL
        //           ELSE
        //              TO TDR-TOT-PRE-TOT
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotPreTot().getWtdrTotPreTotNullFormatted())) {
            // COB_CODE: MOVE (SF)-TOT-PRE-TOT-NULL(IX-TAB-TDR)
            //           TO TDR-TOT-PRE-TOT-NULL
            ws.getTitRat().getTdrTotPreTot().setTdrTotPreTotNull(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotPreTot().getWtdrTotPreTotNull());
        }
        else {
            // COB_CODE: MOVE (SF)-TOT-PRE-TOT(IX-TAB-TDR)
            //           TO TDR-TOT-PRE-TOT
            ws.getTitRat().getTdrTotPreTot().setTdrTotPreTot(Trunc.toDecimal(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotPreTot().getWtdrTotPreTot(), 15, 3));
        }
        // COB_CODE: IF (SF)-TOT-PRE-PP-IAS-NULL(IX-TAB-TDR) = HIGH-VALUES
        //              TO TDR-TOT-PRE-PP-IAS-NULL
        //           ELSE
        //              TO TDR-TOT-PRE-PP-IAS
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotPrePpIas().getWtdrTotPrePpIasNullFormatted())) {
            // COB_CODE: MOVE (SF)-TOT-PRE-PP-IAS-NULL(IX-TAB-TDR)
            //           TO TDR-TOT-PRE-PP-IAS-NULL
            ws.getTitRat().getTdrTotPrePpIas().setTdrTotPrePpIasNull(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotPrePpIas().getWtdrTotPrePpIasNull());
        }
        else {
            // COB_CODE: MOVE (SF)-TOT-PRE-PP-IAS(IX-TAB-TDR)
            //           TO TDR-TOT-PRE-PP-IAS
            ws.getTitRat().getTdrTotPrePpIas().setTdrTotPrePpIas(Trunc.toDecimal(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotPrePpIas().getWtdrTotPrePpIas(), 15, 3));
        }
        // COB_CODE: IF (SF)-TOT-CAR-IAS-NULL(IX-TAB-TDR) = HIGH-VALUES
        //              TO TDR-TOT-CAR-IAS-NULL
        //           ELSE
        //              TO TDR-TOT-CAR-IAS
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotCarIas().getWtdrTotCarIasNullFormatted())) {
            // COB_CODE: MOVE (SF)-TOT-CAR-IAS-NULL(IX-TAB-TDR)
            //           TO TDR-TOT-CAR-IAS-NULL
            ws.getTitRat().getTdrTotCarIas().setTdrTotCarIasNull(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotCarIas().getWtdrTotCarIasNull());
        }
        else {
            // COB_CODE: MOVE (SF)-TOT-CAR-IAS(IX-TAB-TDR)
            //           TO TDR-TOT-CAR-IAS
            ws.getTitRat().getTdrTotCarIas().setTdrTotCarIas(Trunc.toDecimal(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotCarIas().getWtdrTotCarIas(), 15, 3));
        }
        // COB_CODE: IF (SF)-TOT-PRE-SOLO-RSH-NULL(IX-TAB-TDR) = HIGH-VALUES
        //              TO TDR-TOT-PRE-SOLO-RSH-NULL
        //           ELSE
        //              TO TDR-TOT-PRE-SOLO-RSH
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotPreSoloRsh().getWtdrTotPreSoloRshNullFormatted())) {
            // COB_CODE: MOVE (SF)-TOT-PRE-SOLO-RSH-NULL(IX-TAB-TDR)
            //           TO TDR-TOT-PRE-SOLO-RSH-NULL
            ws.getTitRat().getTdrTotPreSoloRsh().setTdrTotPreSoloRshNull(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotPreSoloRsh().getWtdrTotPreSoloRshNull());
        }
        else {
            // COB_CODE: MOVE (SF)-TOT-PRE-SOLO-RSH(IX-TAB-TDR)
            //           TO TDR-TOT-PRE-SOLO-RSH
            ws.getTitRat().getTdrTotPreSoloRsh().setTdrTotPreSoloRsh(Trunc.toDecimal(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotPreSoloRsh().getWtdrTotPreSoloRsh(), 15, 3));
        }
        // COB_CODE: IF (SF)-TOT-PROV-ACQ-1AA-NULL(IX-TAB-TDR) = HIGH-VALUES
        //              TO TDR-TOT-PROV-ACQ-1AA-NULL
        //           ELSE
        //              TO TDR-TOT-PROV-ACQ-1AA
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotProvAcq1aa().getWtdrTotProvAcq1aaNullFormatted())) {
            // COB_CODE: MOVE (SF)-TOT-PROV-ACQ-1AA-NULL(IX-TAB-TDR)
            //           TO TDR-TOT-PROV-ACQ-1AA-NULL
            ws.getTitRat().getTdrTotProvAcq1aa().setTdrTotProvAcq1aaNull(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotProvAcq1aa().getWtdrTotProvAcq1aaNull());
        }
        else {
            // COB_CODE: MOVE (SF)-TOT-PROV-ACQ-1AA(IX-TAB-TDR)
            //           TO TDR-TOT-PROV-ACQ-1AA
            ws.getTitRat().getTdrTotProvAcq1aa().setTdrTotProvAcq1aa(Trunc.toDecimal(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotProvAcq1aa().getWtdrTotProvAcq1aa(), 15, 3));
        }
        // COB_CODE: IF (SF)-TOT-PROV-ACQ-2AA-NULL(IX-TAB-TDR) = HIGH-VALUES
        //              TO TDR-TOT-PROV-ACQ-2AA-NULL
        //           ELSE
        //              TO TDR-TOT-PROV-ACQ-2AA
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotProvAcq2aa().getWtdrTotProvAcq2aaNullFormatted())) {
            // COB_CODE: MOVE (SF)-TOT-PROV-ACQ-2AA-NULL(IX-TAB-TDR)
            //           TO TDR-TOT-PROV-ACQ-2AA-NULL
            ws.getTitRat().getTdrTotProvAcq2aa().setTdrTotProvAcq2aaNull(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotProvAcq2aa().getWtdrTotProvAcq2aaNull());
        }
        else {
            // COB_CODE: MOVE (SF)-TOT-PROV-ACQ-2AA(IX-TAB-TDR)
            //           TO TDR-TOT-PROV-ACQ-2AA
            ws.getTitRat().getTdrTotProvAcq2aa().setTdrTotProvAcq2aa(Trunc.toDecimal(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotProvAcq2aa().getWtdrTotProvAcq2aa(), 15, 3));
        }
        // COB_CODE: IF (SF)-TOT-PROV-RICOR-NULL(IX-TAB-TDR) = HIGH-VALUES
        //              TO TDR-TOT-PROV-RICOR-NULL
        //           ELSE
        //              TO TDR-TOT-PROV-RICOR
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotProvRicor().getWtdrTotProvRicorNullFormatted())) {
            // COB_CODE: MOVE (SF)-TOT-PROV-RICOR-NULL(IX-TAB-TDR)
            //           TO TDR-TOT-PROV-RICOR-NULL
            ws.getTitRat().getTdrTotProvRicor().setTdrTotProvRicorNull(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotProvRicor().getWtdrTotProvRicorNull());
        }
        else {
            // COB_CODE: MOVE (SF)-TOT-PROV-RICOR(IX-TAB-TDR)
            //           TO TDR-TOT-PROV-RICOR
            ws.getTitRat().getTdrTotProvRicor().setTdrTotProvRicor(Trunc.toDecimal(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotProvRicor().getWtdrTotProvRicor(), 15, 3));
        }
        // COB_CODE: IF (SF)-TOT-PROV-INC-NULL(IX-TAB-TDR) = HIGH-VALUES
        //              TO TDR-TOT-PROV-INC-NULL
        //           ELSE
        //              TO TDR-TOT-PROV-INC
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotProvInc().getWtdrTotProvIncNullFormatted())) {
            // COB_CODE: MOVE (SF)-TOT-PROV-INC-NULL(IX-TAB-TDR)
            //           TO TDR-TOT-PROV-INC-NULL
            ws.getTitRat().getTdrTotProvInc().setTdrTotProvIncNull(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotProvInc().getWtdrTotProvIncNull());
        }
        else {
            // COB_CODE: MOVE (SF)-TOT-PROV-INC(IX-TAB-TDR)
            //           TO TDR-TOT-PROV-INC
            ws.getTitRat().getTdrTotProvInc().setTdrTotProvInc(Trunc.toDecimal(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotProvInc().getWtdrTotProvInc(), 15, 3));
        }
        // COB_CODE: IF (SF)-TOT-PROV-DA-REC-NULL(IX-TAB-TDR) = HIGH-VALUES
        //              TO TDR-TOT-PROV-DA-REC-NULL
        //           ELSE
        //              TO TDR-TOT-PROV-DA-REC
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotProvDaRec().getWtdrTotProvDaRecNullFormatted())) {
            // COB_CODE: MOVE (SF)-TOT-PROV-DA-REC-NULL(IX-TAB-TDR)
            //           TO TDR-TOT-PROV-DA-REC-NULL
            ws.getTitRat().getTdrTotProvDaRec().setTdrTotProvDaRecNull(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotProvDaRec().getWtdrTotProvDaRecNull());
        }
        else {
            // COB_CODE: MOVE (SF)-TOT-PROV-DA-REC(IX-TAB-TDR)
            //           TO TDR-TOT-PROV-DA-REC
            ws.getTitRat().getTdrTotProvDaRec().setTdrTotProvDaRec(Trunc.toDecimal(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotProvDaRec().getWtdrTotProvDaRec(), 15, 3));
        }
        // COB_CODE: IF (SF)-IMP-AZ-NULL(IX-TAB-TDR) = HIGH-VALUES
        //              TO TDR-IMP-AZ-NULL
        //           ELSE
        //              TO TDR-IMP-AZ
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrImpAz().getWtdrImpAzNullFormatted())) {
            // COB_CODE: MOVE (SF)-IMP-AZ-NULL(IX-TAB-TDR)
            //           TO TDR-IMP-AZ-NULL
            ws.getTitRat().getTdrImpAz().setTdrImpAzNull(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrImpAz().getWtdrImpAzNull());
        }
        else {
            // COB_CODE: MOVE (SF)-IMP-AZ(IX-TAB-TDR)
            //           TO TDR-IMP-AZ
            ws.getTitRat().getTdrImpAz().setTdrImpAz(Trunc.toDecimal(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrImpAz().getWtdrImpAz(), 15, 3));
        }
        // COB_CODE: IF (SF)-IMP-ADER-NULL(IX-TAB-TDR) = HIGH-VALUES
        //              TO TDR-IMP-ADER-NULL
        //           ELSE
        //              TO TDR-IMP-ADER
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrImpAder().getWtdrImpAderNullFormatted())) {
            // COB_CODE: MOVE (SF)-IMP-ADER-NULL(IX-TAB-TDR)
            //           TO TDR-IMP-ADER-NULL
            ws.getTitRat().getTdrImpAder().setTdrImpAderNull(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrImpAder().getWtdrImpAderNull());
        }
        else {
            // COB_CODE: MOVE (SF)-IMP-ADER(IX-TAB-TDR)
            //           TO TDR-IMP-ADER
            ws.getTitRat().getTdrImpAder().setTdrImpAder(Trunc.toDecimal(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrImpAder().getWtdrImpAder(), 15, 3));
        }
        // COB_CODE: IF (SF)-IMP-TFR-NULL(IX-TAB-TDR) = HIGH-VALUES
        //              TO TDR-IMP-TFR-NULL
        //           ELSE
        //              TO TDR-IMP-TFR
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrImpTfr().getWtdrImpTfrNullFormatted())) {
            // COB_CODE: MOVE (SF)-IMP-TFR-NULL(IX-TAB-TDR)
            //           TO TDR-IMP-TFR-NULL
            ws.getTitRat().getTdrImpTfr().setTdrImpTfrNull(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrImpTfr().getWtdrImpTfrNull());
        }
        else {
            // COB_CODE: MOVE (SF)-IMP-TFR(IX-TAB-TDR)
            //           TO TDR-IMP-TFR
            ws.getTitRat().getTdrImpTfr().setTdrImpTfr(Trunc.toDecimal(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrImpTfr().getWtdrImpTfr(), 15, 3));
        }
        // COB_CODE: IF (SF)-IMP-VOLO-NULL(IX-TAB-TDR) = HIGH-VALUES
        //              TO TDR-IMP-VOLO-NULL
        //           ELSE
        //              TO TDR-IMP-VOLO
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrImpVolo().getWtdrImpVoloNullFormatted())) {
            // COB_CODE: MOVE (SF)-IMP-VOLO-NULL(IX-TAB-TDR)
            //           TO TDR-IMP-VOLO-NULL
            ws.getTitRat().getTdrImpVolo().setTdrImpVoloNull(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrImpVolo().getWtdrImpVoloNull());
        }
        else {
            // COB_CODE: MOVE (SF)-IMP-VOLO(IX-TAB-TDR)
            //           TO TDR-IMP-VOLO
            ws.getTitRat().getTdrImpVolo().setTdrImpVolo(Trunc.toDecimal(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrImpVolo().getWtdrImpVolo(), 15, 3));
        }
        // COB_CODE: IF (SF)-FL-VLDT-TIT-NULL(IX-TAB-TDR) = HIGH-VALUES
        //              TO TDR-FL-VLDT-TIT-NULL
        //           ELSE
        //              TO TDR-FL-VLDT-TIT
        //           END-IF
        if (Conditions.eq(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrFlVldtTit(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE (SF)-FL-VLDT-TIT-NULL(IX-TAB-TDR)
            //           TO TDR-FL-VLDT-TIT-NULL
            ws.getTitRat().setTdrFlVldtTit(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrFlVldtTit());
        }
        else {
            // COB_CODE: MOVE (SF)-FL-VLDT-TIT(IX-TAB-TDR)
            //           TO TDR-FL-VLDT-TIT
            ws.getTitRat().setTdrFlVldtTit(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrFlVldtTit());
        }
        // COB_CODE: IF (SF)-TOT-CAR-ACQ-NULL(IX-TAB-TDR) = HIGH-VALUES
        //              TO TDR-TOT-CAR-ACQ-NULL
        //           ELSE
        //              TO TDR-TOT-CAR-ACQ
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotCarAcq().getWtdrTotCarAcqNullFormatted())) {
            // COB_CODE: MOVE (SF)-TOT-CAR-ACQ-NULL(IX-TAB-TDR)
            //           TO TDR-TOT-CAR-ACQ-NULL
            ws.getTitRat().getTdrTotCarAcq().setTdrTotCarAcqNull(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotCarAcq().getWtdrTotCarAcqNull());
        }
        else {
            // COB_CODE: MOVE (SF)-TOT-CAR-ACQ(IX-TAB-TDR)
            //           TO TDR-TOT-CAR-ACQ
            ws.getTitRat().getTdrTotCarAcq().setTdrTotCarAcq(Trunc.toDecimal(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotCarAcq().getWtdrTotCarAcq(), 15, 3));
        }
        // COB_CODE: IF (SF)-TOT-CAR-GEST-NULL(IX-TAB-TDR) = HIGH-VALUES
        //              TO TDR-TOT-CAR-GEST-NULL
        //           ELSE
        //              TO TDR-TOT-CAR-GEST
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotCarGest().getWtdrTotCarGestNullFormatted())) {
            // COB_CODE: MOVE (SF)-TOT-CAR-GEST-NULL(IX-TAB-TDR)
            //           TO TDR-TOT-CAR-GEST-NULL
            ws.getTitRat().getTdrTotCarGest().setTdrTotCarGestNull(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotCarGest().getWtdrTotCarGestNull());
        }
        else {
            // COB_CODE: MOVE (SF)-TOT-CAR-GEST(IX-TAB-TDR)
            //           TO TDR-TOT-CAR-GEST
            ws.getTitRat().getTdrTotCarGest().setTdrTotCarGest(Trunc.toDecimal(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotCarGest().getWtdrTotCarGest(), 15, 3));
        }
        // COB_CODE: IF (SF)-TOT-CAR-INC-NULL(IX-TAB-TDR) = HIGH-VALUES
        //              TO TDR-TOT-CAR-INC-NULL
        //           ELSE
        //              TO TDR-TOT-CAR-INC
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotCarInc().getWtdrTotCarIncNullFormatted())) {
            // COB_CODE: MOVE (SF)-TOT-CAR-INC-NULL(IX-TAB-TDR)
            //           TO TDR-TOT-CAR-INC-NULL
            ws.getTitRat().getTdrTotCarInc().setTdrTotCarIncNull(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotCarInc().getWtdrTotCarIncNull());
        }
        else {
            // COB_CODE: MOVE (SF)-TOT-CAR-INC(IX-TAB-TDR)
            //           TO TDR-TOT-CAR-INC
            ws.getTitRat().getTdrTotCarInc().setTdrTotCarInc(Trunc.toDecimal(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotCarInc().getWtdrTotCarInc(), 15, 3));
        }
        // COB_CODE: IF (SF)-TOT-MANFEE-ANTIC-NULL(IX-TAB-TDR) = HIGH-VALUES
        //              TO TDR-TOT-MANFEE-ANTIC-NULL
        //           ELSE
        //              TO TDR-TOT-MANFEE-ANTIC
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotManfeeAntic().getWtdrTotManfeeAnticNullFormatted())) {
            // COB_CODE: MOVE (SF)-TOT-MANFEE-ANTIC-NULL(IX-TAB-TDR)
            //           TO TDR-TOT-MANFEE-ANTIC-NULL
            ws.getTitRat().getTdrTotManfeeAntic().setTdrTotManfeeAnticNull(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotManfeeAntic().getWtdrTotManfeeAnticNull());
        }
        else {
            // COB_CODE: MOVE (SF)-TOT-MANFEE-ANTIC(IX-TAB-TDR)
            //           TO TDR-TOT-MANFEE-ANTIC
            ws.getTitRat().getTdrTotManfeeAntic().setTdrTotManfeeAntic(Trunc.toDecimal(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotManfeeAntic().getWtdrTotManfeeAntic(), 15, 3));
        }
        // COB_CODE: IF (SF)-TOT-MANFEE-RICOR-NULL(IX-TAB-TDR) = HIGH-VALUES
        //              TO TDR-TOT-MANFEE-RICOR-NULL
        //           ELSE
        //              TO TDR-TOT-MANFEE-RICOR
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotManfeeRicor().getWtdrTotManfeeRicorNullFormatted())) {
            // COB_CODE: MOVE (SF)-TOT-MANFEE-RICOR-NULL(IX-TAB-TDR)
            //           TO TDR-TOT-MANFEE-RICOR-NULL
            ws.getTitRat().getTdrTotManfeeRicor().setTdrTotManfeeRicorNull(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotManfeeRicor().getWtdrTotManfeeRicorNull());
        }
        else {
            // COB_CODE: MOVE (SF)-TOT-MANFEE-RICOR(IX-TAB-TDR)
            //           TO TDR-TOT-MANFEE-RICOR
            ws.getTitRat().getTdrTotManfeeRicor().setTdrTotManfeeRicor(Trunc.toDecimal(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotManfeeRicor().getWtdrTotManfeeRicor(), 15, 3));
        }
        // COB_CODE: IF (SF)-TOT-MANFEE-REC-NULL(IX-TAB-TDR) = HIGH-VALUES
        //              TO TDR-TOT-MANFEE-REC-NULL
        //           ELSE
        //              TO TDR-TOT-MANFEE-REC
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotManfeeRec().getWtdrTotManfeeRecNullFormatted())) {
            // COB_CODE: MOVE (SF)-TOT-MANFEE-REC-NULL(IX-TAB-TDR)
            //           TO TDR-TOT-MANFEE-REC-NULL
            ws.getTitRat().getTdrTotManfeeRec().setTdrTotManfeeRecNull(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotManfeeRec().getWtdrTotManfeeRecNull());
        }
        else {
            // COB_CODE: MOVE (SF)-TOT-MANFEE-REC(IX-TAB-TDR)
            //           TO TDR-TOT-MANFEE-REC
            ws.getTitRat().getTdrTotManfeeRec().setTdrTotManfeeRec(Trunc.toDecimal(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotManfeeRec().getWtdrTotManfeeRec(), 15, 3));
        }
        // COB_CODE: IF (SF)-DS-RIGA(IX-TAB-TDR) NOT NUMERIC
        //              MOVE 0 TO TDR-DS-RIGA
        //           ELSE
        //              TO TDR-DS-RIGA
        //           END-IF
        if (!Functions.isNumber(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrDsRiga())) {
            // COB_CODE: MOVE 0 TO TDR-DS-RIGA
            ws.getTitRat().setTdrDsRiga(0);
        }
        else {
            // COB_CODE: MOVE (SF)-DS-RIGA(IX-TAB-TDR)
            //           TO TDR-DS-RIGA
            ws.getTitRat().setTdrDsRiga(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrDsRiga());
        }
        // COB_CODE: MOVE (SF)-DS-OPER-SQL(IX-TAB-TDR)
        //              TO TDR-DS-OPER-SQL
        ws.getTitRat().setTdrDsOperSql(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrDsOperSql());
        // COB_CODE: IF (SF)-DS-VER(IX-TAB-TDR) NOT NUMERIC
        //              MOVE 0 TO TDR-DS-VER
        //           ELSE
        //              TO TDR-DS-VER
        //           END-IF
        if (!Functions.isNumber(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrDsVer())) {
            // COB_CODE: MOVE 0 TO TDR-DS-VER
            ws.getTitRat().setTdrDsVer(0);
        }
        else {
            // COB_CODE: MOVE (SF)-DS-VER(IX-TAB-TDR)
            //           TO TDR-DS-VER
            ws.getTitRat().setTdrDsVer(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrDsVer());
        }
        // COB_CODE: IF (SF)-DS-TS-INI-CPTZ(IX-TAB-TDR) NOT NUMERIC
        //              MOVE 0 TO TDR-DS-TS-INI-CPTZ
        //           ELSE
        //              TO TDR-DS-TS-INI-CPTZ
        //           END-IF
        if (!Functions.isNumber(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrDsTsIniCptz())) {
            // COB_CODE: MOVE 0 TO TDR-DS-TS-INI-CPTZ
            ws.getTitRat().setTdrDsTsIniCptz(0);
        }
        else {
            // COB_CODE: MOVE (SF)-DS-TS-INI-CPTZ(IX-TAB-TDR)
            //           TO TDR-DS-TS-INI-CPTZ
            ws.getTitRat().setTdrDsTsIniCptz(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrDsTsIniCptz());
        }
        // COB_CODE: IF (SF)-DS-TS-END-CPTZ(IX-TAB-TDR) NOT NUMERIC
        //              MOVE 0 TO TDR-DS-TS-END-CPTZ
        //           ELSE
        //              TO TDR-DS-TS-END-CPTZ
        //           END-IF
        if (!Functions.isNumber(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrDsTsEndCptz())) {
            // COB_CODE: MOVE 0 TO TDR-DS-TS-END-CPTZ
            ws.getTitRat().setTdrDsTsEndCptz(0);
        }
        else {
            // COB_CODE: MOVE (SF)-DS-TS-END-CPTZ(IX-TAB-TDR)
            //           TO TDR-DS-TS-END-CPTZ
            ws.getTitRat().setTdrDsTsEndCptz(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrDsTsEndCptz());
        }
        // COB_CODE: MOVE (SF)-DS-UTENTE(IX-TAB-TDR)
        //              TO TDR-DS-UTENTE
        ws.getTitRat().setTdrDsUtente(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrDsUtente());
        // COB_CODE: MOVE (SF)-DS-STATO-ELAB(IX-TAB-TDR)
        //              TO TDR-DS-STATO-ELAB
        ws.getTitRat().setTdrDsStatoElab(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrDsStatoElab());
        // COB_CODE: IF (SF)-IMP-TRASFE-NULL(IX-TAB-TDR) = HIGH-VALUES
        //              TO TDR-IMP-TRASFE-NULL
        //           ELSE
        //              TO TDR-IMP-TRASFE
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrImpTrasfe().getWtdrImpTrasfeNullFormatted())) {
            // COB_CODE: MOVE (SF)-IMP-TRASFE-NULL(IX-TAB-TDR)
            //           TO TDR-IMP-TRASFE-NULL
            ws.getTitRat().getTdrImpTrasfe().setTdrImpTrasfeNull(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrImpTrasfe().getWtdrImpTrasfeNull());
        }
        else {
            // COB_CODE: MOVE (SF)-IMP-TRASFE(IX-TAB-TDR)
            //           TO TDR-IMP-TRASFE
            ws.getTitRat().getTdrImpTrasfe().setTdrImpTrasfe(Trunc.toDecimal(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrImpTrasfe().getWtdrImpTrasfe(), 15, 3));
        }
        // COB_CODE: IF (SF)-IMP-TFR-STRC-NULL(IX-TAB-TDR) = HIGH-VALUES
        //              TO TDR-IMP-TFR-STRC-NULL
        //           ELSE
        //              TO TDR-IMP-TFR-STRC
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrImpTfrStrc().getWtdrImpTfrStrcNullFormatted())) {
            // COB_CODE: MOVE (SF)-IMP-TFR-STRC-NULL(IX-TAB-TDR)
            //           TO TDR-IMP-TFR-STRC-NULL
            ws.getTitRat().getTdrImpTfrStrc().setTdrImpTfrStrcNull(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrImpTfrStrc().getWtdrImpTfrStrcNull());
        }
        else {
            // COB_CODE: MOVE (SF)-IMP-TFR-STRC(IX-TAB-TDR)
            //           TO TDR-IMP-TFR-STRC
            ws.getTitRat().getTdrImpTfrStrc().setTdrImpTfrStrc(Trunc.toDecimal(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrImpTfrStrc().getWtdrImpTfrStrc(), 15, 3));
        }
        // COB_CODE: IF (SF)-TOT-ACQ-EXP-NULL(IX-TAB-TDR) = HIGH-VALUES
        //              TO TDR-TOT-ACQ-EXP-NULL
        //           ELSE
        //              TO TDR-TOT-ACQ-EXP
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotAcqExp().getWtdrTotAcqExpNullFormatted())) {
            // COB_CODE: MOVE (SF)-TOT-ACQ-EXP-NULL(IX-TAB-TDR)
            //           TO TDR-TOT-ACQ-EXP-NULL
            ws.getTitRat().getTdrTotAcqExp().setTdrTotAcqExpNull(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotAcqExp().getWtdrTotAcqExpNull());
        }
        else {
            // COB_CODE: MOVE (SF)-TOT-ACQ-EXP(IX-TAB-TDR)
            //           TO TDR-TOT-ACQ-EXP
            ws.getTitRat().getTdrTotAcqExp().setTdrTotAcqExp(Trunc.toDecimal(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotAcqExp().getWtdrTotAcqExp(), 15, 3));
        }
        // COB_CODE: IF (SF)-TOT-REMUN-ASS-NULL(IX-TAB-TDR) = HIGH-VALUES
        //              TO TDR-TOT-REMUN-ASS-NULL
        //           ELSE
        //              TO TDR-TOT-REMUN-ASS
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotRemunAss().getWtdrTotRemunAssNullFormatted())) {
            // COB_CODE: MOVE (SF)-TOT-REMUN-ASS-NULL(IX-TAB-TDR)
            //           TO TDR-TOT-REMUN-ASS-NULL
            ws.getTitRat().getTdrTotRemunAss().setTdrTotRemunAssNull(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotRemunAss().getWtdrTotRemunAssNull());
        }
        else {
            // COB_CODE: MOVE (SF)-TOT-REMUN-ASS(IX-TAB-TDR)
            //           TO TDR-TOT-REMUN-ASS
            ws.getTitRat().getTdrTotRemunAss().setTdrTotRemunAss(Trunc.toDecimal(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotRemunAss().getWtdrTotRemunAss(), 15, 3));
        }
        // COB_CODE: IF (SF)-TOT-COMMIS-INTER-NULL(IX-TAB-TDR) = HIGH-VALUES
        //              TO TDR-TOT-COMMIS-INTER-NULL
        //           ELSE
        //              TO TDR-TOT-COMMIS-INTER
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotCommisInter().getWtdrTotCommisInterNullFormatted())) {
            // COB_CODE: MOVE (SF)-TOT-COMMIS-INTER-NULL(IX-TAB-TDR)
            //           TO TDR-TOT-COMMIS-INTER-NULL
            ws.getTitRat().getTdrTotCommisInter().setTdrTotCommisInterNull(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotCommisInter().getWtdrTotCommisInterNull());
        }
        else {
            // COB_CODE: MOVE (SF)-TOT-COMMIS-INTER(IX-TAB-TDR)
            //           TO TDR-TOT-COMMIS-INTER
            ws.getTitRat().getTdrTotCommisInter().setTdrTotCommisInter(Trunc.toDecimal(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotCommisInter().getWtdrTotCommisInter(), 15, 3));
        }
        // COB_CODE: IF (SF)-TOT-CNBT-ANTIRAC-NULL(IX-TAB-TDR) = HIGH-VALUES
        //              TO TDR-TOT-CNBT-ANTIRAC-NULL
        //           ELSE
        //              TO TDR-TOT-CNBT-ANTIRAC
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotCnbtAntirac().getWtdrTotCnbtAntiracNullFormatted())) {
            // COB_CODE: MOVE (SF)-TOT-CNBT-ANTIRAC-NULL(IX-TAB-TDR)
            //           TO TDR-TOT-CNBT-ANTIRAC-NULL
            ws.getTitRat().getTdrTotCnbtAntirac().setTdrTotCnbtAntiracNull(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotCnbtAntirac().getWtdrTotCnbtAntiracNull());
        }
        else {
            // COB_CODE: MOVE (SF)-TOT-CNBT-ANTIRAC(IX-TAB-TDR)
            //           TO TDR-TOT-CNBT-ANTIRAC
            ws.getTitRat().getTdrTotCnbtAntirac().setTdrTotCnbtAntirac(Trunc.toDecimal(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTotCnbtAntirac().getWtdrTotCnbtAntirac(), 15, 3));
        }
        // COB_CODE: IF (SF)-FL-INC-AUTOGEN-NULL(IX-TAB-TDR) = HIGH-VALUES
        //              TO TDR-FL-INC-AUTOGEN-NULL
        //           ELSE
        //              TO TDR-FL-INC-AUTOGEN
        //           END-IF.
        if (Conditions.eq(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrFlIncAutogen(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE (SF)-FL-INC-AUTOGEN-NULL(IX-TAB-TDR)
            //           TO TDR-FL-INC-AUTOGEN-NULL
            ws.getTitRat().setTdrFlIncAutogen(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrFlIncAutogen());
        }
        else {
            // COB_CODE: MOVE (SF)-FL-INC-AUTOGEN(IX-TAB-TDR)
            //           TO TDR-FL-INC-AUTOGEN
            ws.getTitRat().setTdrFlIncAutogen(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrFlIncAutogen());
        }
    }

    /**Original name: AGGIORNA-TIT-RAT<br>
	 * <pre>----------------------------------------------------------------*
	 *     COPY      ..... LCCVTDR6
	 *     TIPOLOGIA...... SERVIZIO DI EOC
	 *     DESCRIZIONE.... AGGIORNAMENTO TITOLO DI RATA
	 * ----------------------------------------------------------------*
	 *     N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
	 *     ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
	 *   - COPY LCCV*5 (VALORIZZAZIONE DCLGEN)
	 *   - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
	 *   - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
	 *   - DICHIARAZIONE DCLGEN (LCCV*1)
	 *   - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
	 * ----------------------------------------------------------------*
	 * --> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI</pre>*/
    private void aggiornaTitRat() {
        // COB_CODE: INITIALIZE TIT-RAT
        initTitRat();
        //--> NOME TABELLA FISICA DB
        // COB_CODE: MOVE 'TIT-RAT'                  TO WK-TABELLA.
        ws.setWkTabella("TIT-RAT");
        //--> SE LO STATUS NON E' "INVARIATO"
        // COB_CODE:      IF  NOT WTDR-ST-INV(IX-TAB-TDR)
        //                AND NOT WTDR-ST-CON(IX-TAB-TDR)
        //                AND WTDR-ELE-TDR-MAX  NOT  = 0
        //           *--->   Impostare ID Tabella TITOLO DI RATA
        //                   END-IF
        //                END-IF.
        if (!ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getStatus().isInv() && !ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getStatus().isWpmoStCon() && ws.getWtdrEleTdrMax() != 0) {
            //--->   Impostare ID Tabella TITOLO DI RATA
            // COB_CODE: MOVE WMOV-ID-PTF     TO TDR-ID-MOVI-CRZ
            ws.getTitRat().setTdrIdMoviCrz(areaMain.getWmovAreaMovimento().getLccvmov1().getIdPtf());
            //-->        INSERIMENTO
            // COB_CODE:         EVALUATE TRUE
            //           *-->        INSERIMENTO
            //                       WHEN WTDR-ST-ADD(IX-TAB-TDR)
            //                            SET  IDSI0011-AGGIORNAMENTO-STORICO   TO TRUE
            //           *-->        CANCELLAZIONE
            //                       WHEN WTDR-ST-DEL(IX-TAB-TDR)
            //                            SET  IDSI0011-DELETE-LOGICA    TO TRUE
            //           *-->        MODIFICA
            //                       WHEN WTDR-ST-MOD(IX-TAB-TDR)
            //                            SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE
            //                   END-EVALUATE
            switch (ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getStatus().getStatus()) {

                case WpolStatus.ADD:// COB_CODE: PERFORM ESTR-SEQUENCE
                    //              THRU ESTR-SEQUENCE-EX
                    estrSequence();
                    // COB_CODE: IF IDSV0001-ESITO-OK
                    //              END-IF
                    //           END-IF
                    if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                        // COB_CODE: MOVE S090-SEQ-TABELLA
                        //             TO WTDR-ID-PTF(IX-TAB-TDR)
                        //                TDR-ID-TIT-RAT
                        ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().setIdPtf(ws.getAreaIoLccs0090().getSeqTabella());
                        ws.getTitRat().setTdrIdTitRat(ws.getAreaIoLccs0090().getSeqTabella());
                        //-->                Estrazione Oggetto
                        // COB_CODE: PERFORM PREPARA-AREA-LCCS0234-TDR
                        //              THRU PREPARA-AREA-LCCS0234-TDR-EX
                        preparaAreaLccs0234Tdr();
                        // COB_CODE: PERFORM CALL-LCCS0234
                        //              THRU CALL-LCCS0234-EX
                        callLccs0234();
                        // COB_CODE: IF IDSV0001-ESITO-OK
                        //              MOVE S234-ID-OGG-PTF-EOC     TO TDR-ID-OGG
                        //           END-IF
                        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                            // COB_CODE: MOVE S234-ID-OGG-PTF-EOC     TO TDR-ID-OGG
                            ws.getTitRat().setTdrIdOgg(ws.getS234DatiOutput().getIdOggPtfEoc());
                        }
                    }
                    //-->             TIPO OPERAZIONE DISPATCHER
                    // COB_CODE: SET  IDSI0011-AGGIORNAMENTO-STORICO   TO TRUE
                    ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setIdsi0011AggiornamentoStorico();
                    //-->        CANCELLAZIONE
                    break;

                case WpolStatus.DEL:// COB_CODE: MOVE WTDR-ID-PTF(IX-TAB-TDR)   TO TDR-ID-TIT-RAT
                    ws.getTitRat().setTdrIdTitRat(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getIdPtf());
                    // COB_CODE: MOVE WTDR-ID-OGG(IX-TAB-TDR)   TO TDR-ID-OGG
                    ws.getTitRat().setTdrIdOgg(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrIdOgg());
                    //-->             TIPO OPERAZIONE DISPATCHER
                    // COB_CODE: SET  IDSI0011-DELETE-LOGICA    TO TRUE
                    ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setIdsi0011DeleteLogica();
                    //-->        MODIFICA
                    break;

                case WpolStatus.MOD:// COB_CODE: MOVE WTDR-ID-PTF(IX-TAB-TDR)   TO TDR-ID-TIT-RAT
                    ws.getTitRat().setTdrIdTitRat(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getIdPtf());
                    // COB_CODE: MOVE WTDR-ID-OGG(IX-TAB-TDR)   TO TDR-ID-OGG
                    ws.getTitRat().setTdrIdOgg(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrIdOgg());
                    //-->             TIPO OPERAZIONE DISPATCHER
                    // COB_CODE: SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE
                    ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setIdsi0011AggiornamentoStorico();
                    break;

                default:break;
            }
            // COB_CODE:         IF IDSV0001-ESITO-OK
            //           *-->       VALORIZZA DCLGEN TITOLO DI RATA
            //                         THRU AGGIORNA-TABELLA-EX
            //                   END-IF
            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                //-->       VALORIZZA DCLGEN TITOLO DI RATA
                // COB_CODE: PERFORM VAL-DCLGEN-TDR
                //              THRU VAL-DCLGEN-TDR-EX
                valDclgenTdr();
                //-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                // COB_CODE: PERFORM VALORIZZA-AREA-DSH-TDR
                //              THRU VALORIZZA-AREA-DSH-TDR-EX
                valorizzaAreaDshTdr();
                //-->       CALL DISPATCHER PER AGGIORNAMENTO
                // COB_CODE: PERFORM AGGIORNA-TABELLA
                //              THRU AGGIORNA-TABELLA-EX
                aggiornaTabella();
            }
        }
    }

    /**Original name: VALORIZZA-AREA-DSH-TDR<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZA AREA COMUNE DISPATCHER
	 * ----------------------------------------------------------------*
	 * --> DCLGEN TABELLA</pre>*/
    private void valorizzaAreaDshTdr() {
        // COB_CODE: MOVE TIT-RAT                 TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getTitRat().getTitRatFormatted());
        //--> MODALITA DI ACCESSO
        // COB_CODE: SET  IDSI0011-ID             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setIdsi0011Id();
        //--> TIPO TABELLA (STORICA/NON STORICA)
        // COB_CODE: SET  IDSI0011-TRATT-DEFAULT   TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setIdsi0011TrattDefault();
        //--> VALORIZZAZIONE DATE EFFETTO
        // COB_CODE: MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaMain.getWmovAreaMovimento().getLccvmov1().getDati().getWmovDtEff());
        // COB_CODE: MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        // COB_CODE: MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
    }

    /**Original name: PREPARA-AREA-LCCS0234-TDR<br>
	 * <pre>----------------------------------------------------------------*
	 *     PREPARA AREA PER CALL LCCS0234
	 * ----------------------------------------------------------------*</pre>*/
    private void preparaAreaLccs0234Tdr() {
        // COB_CODE: MOVE WTDR-ID-OGG(IX-TAB-TDR)    TO S234-ID-OGG-EOC.
        ws.getS234DatiInput().setIdOggEoc(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrIdOgg());
        // COB_CODE: MOVE WTDR-TP-OGG(IX-TAB-TDR)    TO S234-TIPO-OGG-EOC.
        ws.getS234DatiInput().setTipoOggEoc(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrTpOgg());
    }

    /**Original name: VAL-DCLGEN-DTR<br>
	 * <pre>----------------------------------------------------------------*
	 *     COPY VALORIZZAZIONE DCLGEN
	 * ----------------------------------------------------------------*
	 *      COPY LCCVTDR5.
	 * ------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    COPY LCCVDTR5
	 *    ULTIMO AGG. 28 NOV 2014
	 * ------------------------------------------------------------</pre>*/
    private void valDclgenDtr() {
        // COB_CODE: MOVE (SF)-TP-OGG(IX-TAB-DTR)
        //              TO DTR-TP-OGG
        ws.getDettTitDiRat().setDtrTpOgg(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrTpOgg());
        // COB_CODE: IF (SF)-ID-MOVI-CHIU-NULL(IX-TAB-DTR) = HIGH-VALUES
        //              TO DTR-ID-MOVI-CHIU-NULL
        //           ELSE
        //              TO DTR-ID-MOVI-CHIU
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrIdMoviChiu().getWdtrIdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE (SF)-ID-MOVI-CHIU-NULL(IX-TAB-DTR)
            //           TO DTR-ID-MOVI-CHIU-NULL
            ws.getDettTitDiRat().getDtrIdMoviChiu().setDtrIdMoviChiuNull(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrIdMoviChiu().getWdtrIdMoviChiuNull());
        }
        else {
            // COB_CODE: MOVE (SF)-ID-MOVI-CHIU(IX-TAB-DTR)
            //           TO DTR-ID-MOVI-CHIU
            ws.getDettTitDiRat().getDtrIdMoviChiu().setDtrIdMoviChiu(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrIdMoviChiu().getWdtrIdMoviChiu());
        }
        // COB_CODE: IF (SF)-DT-INI-EFF(IX-TAB-DTR) NOT NUMERIC
        //              MOVE 0 TO DTR-DT-INI-EFF
        //           ELSE
        //              TO DTR-DT-INI-EFF
        //           END-IF
        if (!Functions.isNumber(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrDtIniEff())) {
            // COB_CODE: MOVE 0 TO DTR-DT-INI-EFF
            ws.getDettTitDiRat().setDtrDtIniEff(0);
        }
        else {
            // COB_CODE: MOVE (SF)-DT-INI-EFF(IX-TAB-DTR)
            //           TO DTR-DT-INI-EFF
            ws.getDettTitDiRat().setDtrDtIniEff(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrDtIniEff());
        }
        // COB_CODE: IF (SF)-DT-END-EFF(IX-TAB-DTR) NOT NUMERIC
        //              MOVE 0 TO DTR-DT-END-EFF
        //           ELSE
        //              TO DTR-DT-END-EFF
        //           END-IF
        if (!Functions.isNumber(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrDtEndEff())) {
            // COB_CODE: MOVE 0 TO DTR-DT-END-EFF
            ws.getDettTitDiRat().setDtrDtEndEff(0);
        }
        else {
            // COB_CODE: MOVE (SF)-DT-END-EFF(IX-TAB-DTR)
            //           TO DTR-DT-END-EFF
            ws.getDettTitDiRat().setDtrDtEndEff(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrDtEndEff());
        }
        // COB_CODE: MOVE (SF)-COD-COMP-ANIA(IX-TAB-DTR)
        //              TO DTR-COD-COMP-ANIA
        ws.getDettTitDiRat().setDtrCodCompAnia(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrCodCompAnia());
        // COB_CODE: IF (SF)-DT-INI-COP-NULL(IX-TAB-DTR) = HIGH-VALUES
        //              TO DTR-DT-INI-COP-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrDtIniCop().getWdtrDtIniCopNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-INI-COP-NULL(IX-TAB-DTR)
            //           TO DTR-DT-INI-COP-NULL
            ws.getDettTitDiRat().getDtrDtIniCop().setDtrDtIniCopNull(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrDtIniCop().getWdtrDtIniCopNull());
        }
        else if (ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrDtIniCop().getWdtrDtIniCop() == 0) {
            // COB_CODE: IF (SF)-DT-INI-COP(IX-TAB-DTR) = ZERO
            //              TO DTR-DT-INI-COP-NULL
            //           ELSE
            //            TO DTR-DT-INI-COP
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO DTR-DT-INI-COP-NULL
            ws.getDettTitDiRat().getDtrDtIniCop().setDtrDtIniCopNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrDtIniCop.Len.DTR_DT_INI_COP_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-INI-COP(IX-TAB-DTR)
            //           TO DTR-DT-INI-COP
            ws.getDettTitDiRat().getDtrDtIniCop().setDtrDtIniCop(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrDtIniCop().getWdtrDtIniCop());
        }
        // COB_CODE: IF (SF)-DT-END-COP-NULL(IX-TAB-DTR) = HIGH-VALUES
        //              TO DTR-DT-END-COP-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrDtEndCop().getWdtrDtEndCopNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-END-COP-NULL(IX-TAB-DTR)
            //           TO DTR-DT-END-COP-NULL
            ws.getDettTitDiRat().getDtrDtEndCop().setDtrDtEndCopNull(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrDtEndCop().getWdtrDtEndCopNull());
        }
        else if (ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrDtEndCop().getWdtrDtEndCop() == 0) {
            // COB_CODE: IF (SF)-DT-END-COP(IX-TAB-DTR) = ZERO
            //              TO DTR-DT-END-COP-NULL
            //           ELSE
            //            TO DTR-DT-END-COP
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO DTR-DT-END-COP-NULL
            ws.getDettTitDiRat().getDtrDtEndCop().setDtrDtEndCopNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrDtEndCop.Len.DTR_DT_END_COP_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-END-COP(IX-TAB-DTR)
            //           TO DTR-DT-END-COP
            ws.getDettTitDiRat().getDtrDtEndCop().setDtrDtEndCop(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrDtEndCop().getWdtrDtEndCop());
        }
        // COB_CODE: IF (SF)-PRE-NET-NULL(IX-TAB-DTR) = HIGH-VALUES
        //              TO DTR-PRE-NET-NULL
        //           ELSE
        //              TO DTR-PRE-NET
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrPreNet().getWdtrPreNetNullFormatted())) {
            // COB_CODE: MOVE (SF)-PRE-NET-NULL(IX-TAB-DTR)
            //           TO DTR-PRE-NET-NULL
            ws.getDettTitDiRat().getDtrPreNet().setDtrPreNetNull(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrPreNet().getWdtrPreNetNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PRE-NET(IX-TAB-DTR)
            //           TO DTR-PRE-NET
            ws.getDettTitDiRat().getDtrPreNet().setDtrPreNet(Trunc.toDecimal(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrPreNet().getWdtrPreNet(), 15, 3));
        }
        // COB_CODE: IF (SF)-INTR-FRAZ-NULL(IX-TAB-DTR) = HIGH-VALUES
        //              TO DTR-INTR-FRAZ-NULL
        //           ELSE
        //              TO DTR-INTR-FRAZ
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrIntrFraz().getWdtrIntrFrazNullFormatted())) {
            // COB_CODE: MOVE (SF)-INTR-FRAZ-NULL(IX-TAB-DTR)
            //           TO DTR-INTR-FRAZ-NULL
            ws.getDettTitDiRat().getDtrIntrFraz().setDtrIntrFrazNull(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrIntrFraz().getWdtrIntrFrazNull());
        }
        else {
            // COB_CODE: MOVE (SF)-INTR-FRAZ(IX-TAB-DTR)
            //           TO DTR-INTR-FRAZ
            ws.getDettTitDiRat().getDtrIntrFraz().setDtrIntrFraz(Trunc.toDecimal(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrIntrFraz().getWdtrIntrFraz(), 15, 3));
        }
        // COB_CODE: IF (SF)-INTR-MORA-NULL(IX-TAB-DTR) = HIGH-VALUES
        //              TO DTR-INTR-MORA-NULL
        //           ELSE
        //              TO DTR-INTR-MORA
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrIntrMora().getWdtrIntrMoraNullFormatted())) {
            // COB_CODE: MOVE (SF)-INTR-MORA-NULL(IX-TAB-DTR)
            //           TO DTR-INTR-MORA-NULL
            ws.getDettTitDiRat().getDtrIntrMora().setDtrIntrMoraNull(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrIntrMora().getWdtrIntrMoraNull());
        }
        else {
            // COB_CODE: MOVE (SF)-INTR-MORA(IX-TAB-DTR)
            //           TO DTR-INTR-MORA
            ws.getDettTitDiRat().getDtrIntrMora().setDtrIntrMora(Trunc.toDecimal(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrIntrMora().getWdtrIntrMora(), 15, 3));
        }
        // COB_CODE: IF (SF)-INTR-RETDT-NULL(IX-TAB-DTR) = HIGH-VALUES
        //              TO DTR-INTR-RETDT-NULL
        //           ELSE
        //              TO DTR-INTR-RETDT
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrIntrRetdt().getWdtrIntrRetdtNullFormatted())) {
            // COB_CODE: MOVE (SF)-INTR-RETDT-NULL(IX-TAB-DTR)
            //           TO DTR-INTR-RETDT-NULL
            ws.getDettTitDiRat().getDtrIntrRetdt().setDtrIntrRetdtNull(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrIntrRetdt().getWdtrIntrRetdtNull());
        }
        else {
            // COB_CODE: MOVE (SF)-INTR-RETDT(IX-TAB-DTR)
            //           TO DTR-INTR-RETDT
            ws.getDettTitDiRat().getDtrIntrRetdt().setDtrIntrRetdt(Trunc.toDecimal(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrIntrRetdt().getWdtrIntrRetdt(), 15, 3));
        }
        // COB_CODE: IF (SF)-INTR-RIAT-NULL(IX-TAB-DTR) = HIGH-VALUES
        //              TO DTR-INTR-RIAT-NULL
        //           ELSE
        //              TO DTR-INTR-RIAT
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrIntrRiat().getWdtrIntrRiatNullFormatted())) {
            // COB_CODE: MOVE (SF)-INTR-RIAT-NULL(IX-TAB-DTR)
            //           TO DTR-INTR-RIAT-NULL
            ws.getDettTitDiRat().getDtrIntrRiat().setDtrIntrRiatNull(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrIntrRiat().getWdtrIntrRiatNull());
        }
        else {
            // COB_CODE: MOVE (SF)-INTR-RIAT(IX-TAB-DTR)
            //           TO DTR-INTR-RIAT
            ws.getDettTitDiRat().getDtrIntrRiat().setDtrIntrRiat(Trunc.toDecimal(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrIntrRiat().getWdtrIntrRiat(), 15, 3));
        }
        // COB_CODE: IF (SF)-DIR-NULL(IX-TAB-DTR) = HIGH-VALUES
        //              TO DTR-DIR-NULL
        //           ELSE
        //              TO DTR-DIR
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrDir().getWdtrDirNullFormatted())) {
            // COB_CODE: MOVE (SF)-DIR-NULL(IX-TAB-DTR)
            //           TO DTR-DIR-NULL
            ws.getDettTitDiRat().getDtrDir().setDtrDirNull(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrDir().getWdtrDirNull());
        }
        else {
            // COB_CODE: MOVE (SF)-DIR(IX-TAB-DTR)
            //           TO DTR-DIR
            ws.getDettTitDiRat().getDtrDir().setDtrDir(Trunc.toDecimal(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrDir().getWdtrDir(), 15, 3));
        }
        // COB_CODE: IF (SF)-SPE-MED-NULL(IX-TAB-DTR) = HIGH-VALUES
        //              TO DTR-SPE-MED-NULL
        //           ELSE
        //              TO DTR-SPE-MED
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrSpeMed().getWdtrSpeMedNullFormatted())) {
            // COB_CODE: MOVE (SF)-SPE-MED-NULL(IX-TAB-DTR)
            //           TO DTR-SPE-MED-NULL
            ws.getDettTitDiRat().getDtrSpeMed().setDtrSpeMedNull(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrSpeMed().getWdtrSpeMedNull());
        }
        else {
            // COB_CODE: MOVE (SF)-SPE-MED(IX-TAB-DTR)
            //           TO DTR-SPE-MED
            ws.getDettTitDiRat().getDtrSpeMed().setDtrSpeMed(Trunc.toDecimal(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrSpeMed().getWdtrSpeMed(), 15, 3));
        }
        // COB_CODE: IF (SF)-SPE-AGE-NULL(IX-TAB-DTR) = HIGH-VALUES
        //              TO DTR-SPE-AGE-NULL
        //           ELSE
        //              TO DTR-SPE-AGE
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrSpeAge().getWdtrSpeAgeNullFormatted())) {
            // COB_CODE: MOVE (SF)-SPE-AGE-NULL(IX-TAB-DTR)
            //           TO DTR-SPE-AGE-NULL
            ws.getDettTitDiRat().getDtrSpeAge().setDtrSpeAgeNull(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrSpeAge().getWdtrSpeAgeNull());
        }
        else {
            // COB_CODE: MOVE (SF)-SPE-AGE(IX-TAB-DTR)
            //           TO DTR-SPE-AGE
            ws.getDettTitDiRat().getDtrSpeAge().setDtrSpeAge(Trunc.toDecimal(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrSpeAge().getWdtrSpeAge(), 15, 3));
        }
        // COB_CODE: IF (SF)-TAX-NULL(IX-TAB-DTR) = HIGH-VALUES
        //              TO DTR-TAX-NULL
        //           ELSE
        //              TO DTR-TAX
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrTax().getWdtrTaxNullFormatted())) {
            // COB_CODE: MOVE (SF)-TAX-NULL(IX-TAB-DTR)
            //           TO DTR-TAX-NULL
            ws.getDettTitDiRat().getDtrTax().setDtrTaxNull(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrTax().getWdtrTaxNull());
        }
        else {
            // COB_CODE: MOVE (SF)-TAX(IX-TAB-DTR)
            //           TO DTR-TAX
            ws.getDettTitDiRat().getDtrTax().setDtrTax(Trunc.toDecimal(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrTax().getWdtrTax(), 15, 3));
        }
        // COB_CODE: IF (SF)-SOPR-SAN-NULL(IX-TAB-DTR) = HIGH-VALUES
        //              TO DTR-SOPR-SAN-NULL
        //           ELSE
        //              TO DTR-SOPR-SAN
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrSoprSan().getWdtrSoprSanNullFormatted())) {
            // COB_CODE: MOVE (SF)-SOPR-SAN-NULL(IX-TAB-DTR)
            //           TO DTR-SOPR-SAN-NULL
            ws.getDettTitDiRat().getDtrSoprSan().setDtrSoprSanNull(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrSoprSan().getWdtrSoprSanNull());
        }
        else {
            // COB_CODE: MOVE (SF)-SOPR-SAN(IX-TAB-DTR)
            //           TO DTR-SOPR-SAN
            ws.getDettTitDiRat().getDtrSoprSan().setDtrSoprSan(Trunc.toDecimal(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrSoprSan().getWdtrSoprSan(), 15, 3));
        }
        // COB_CODE: IF (SF)-SOPR-SPO-NULL(IX-TAB-DTR) = HIGH-VALUES
        //              TO DTR-SOPR-SPO-NULL
        //           ELSE
        //              TO DTR-SOPR-SPO
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrSoprSpo().getWdtrSoprSpoNullFormatted())) {
            // COB_CODE: MOVE (SF)-SOPR-SPO-NULL(IX-TAB-DTR)
            //           TO DTR-SOPR-SPO-NULL
            ws.getDettTitDiRat().getDtrSoprSpo().setDtrSoprSpoNull(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrSoprSpo().getWdtrSoprSpoNull());
        }
        else {
            // COB_CODE: MOVE (SF)-SOPR-SPO(IX-TAB-DTR)
            //           TO DTR-SOPR-SPO
            ws.getDettTitDiRat().getDtrSoprSpo().setDtrSoprSpo(Trunc.toDecimal(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrSoprSpo().getWdtrSoprSpo(), 15, 3));
        }
        // COB_CODE: IF (SF)-SOPR-TEC-NULL(IX-TAB-DTR) = HIGH-VALUES
        //              TO DTR-SOPR-TEC-NULL
        //           ELSE
        //              TO DTR-SOPR-TEC
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrSoprTec().getWdtrSoprTecNullFormatted())) {
            // COB_CODE: MOVE (SF)-SOPR-TEC-NULL(IX-TAB-DTR)
            //           TO DTR-SOPR-TEC-NULL
            ws.getDettTitDiRat().getDtrSoprTec().setDtrSoprTecNull(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrSoprTec().getWdtrSoprTecNull());
        }
        else {
            // COB_CODE: MOVE (SF)-SOPR-TEC(IX-TAB-DTR)
            //           TO DTR-SOPR-TEC
            ws.getDettTitDiRat().getDtrSoprTec().setDtrSoprTec(Trunc.toDecimal(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrSoprTec().getWdtrSoprTec(), 15, 3));
        }
        // COB_CODE: IF (SF)-SOPR-PROF-NULL(IX-TAB-DTR) = HIGH-VALUES
        //              TO DTR-SOPR-PROF-NULL
        //           ELSE
        //              TO DTR-SOPR-PROF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrSoprProf().getWdtrSoprProfNullFormatted())) {
            // COB_CODE: MOVE (SF)-SOPR-PROF-NULL(IX-TAB-DTR)
            //           TO DTR-SOPR-PROF-NULL
            ws.getDettTitDiRat().getDtrSoprProf().setDtrSoprProfNull(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrSoprProf().getWdtrSoprProfNull());
        }
        else {
            // COB_CODE: MOVE (SF)-SOPR-PROF(IX-TAB-DTR)
            //           TO DTR-SOPR-PROF
            ws.getDettTitDiRat().getDtrSoprProf().setDtrSoprProf(Trunc.toDecimal(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrSoprProf().getWdtrSoprProf(), 15, 3));
        }
        // COB_CODE: IF (SF)-SOPR-ALT-NULL(IX-TAB-DTR) = HIGH-VALUES
        //              TO DTR-SOPR-ALT-NULL
        //           ELSE
        //              TO DTR-SOPR-ALT
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrSoprAlt().getWdtrSoprAltNullFormatted())) {
            // COB_CODE: MOVE (SF)-SOPR-ALT-NULL(IX-TAB-DTR)
            //           TO DTR-SOPR-ALT-NULL
            ws.getDettTitDiRat().getDtrSoprAlt().setDtrSoprAltNull(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrSoprAlt().getWdtrSoprAltNull());
        }
        else {
            // COB_CODE: MOVE (SF)-SOPR-ALT(IX-TAB-DTR)
            //           TO DTR-SOPR-ALT
            ws.getDettTitDiRat().getDtrSoprAlt().setDtrSoprAlt(Trunc.toDecimal(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrSoprAlt().getWdtrSoprAlt(), 15, 3));
        }
        // COB_CODE: IF (SF)-PRE-TOT-NULL(IX-TAB-DTR) = HIGH-VALUES
        //              TO DTR-PRE-TOT-NULL
        //           ELSE
        //              TO DTR-PRE-TOT
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrPreTot().getWdtrPreTotNullFormatted())) {
            // COB_CODE: MOVE (SF)-PRE-TOT-NULL(IX-TAB-DTR)
            //           TO DTR-PRE-TOT-NULL
            ws.getDettTitDiRat().getDtrPreTot().setDtrPreTotNull(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrPreTot().getWdtrPreTotNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PRE-TOT(IX-TAB-DTR)
            //           TO DTR-PRE-TOT
            ws.getDettTitDiRat().getDtrPreTot().setDtrPreTot(Trunc.toDecimal(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrPreTot().getWdtrPreTot(), 15, 3));
        }
        // COB_CODE: IF (SF)-PRE-PP-IAS-NULL(IX-TAB-DTR) = HIGH-VALUES
        //              TO DTR-PRE-PP-IAS-NULL
        //           ELSE
        //              TO DTR-PRE-PP-IAS
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrPrePpIas().getWdtrPrePpIasNullFormatted())) {
            // COB_CODE: MOVE (SF)-PRE-PP-IAS-NULL(IX-TAB-DTR)
            //           TO DTR-PRE-PP-IAS-NULL
            ws.getDettTitDiRat().getDtrPrePpIas().setDtrPrePpIasNull(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrPrePpIas().getWdtrPrePpIasNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PRE-PP-IAS(IX-TAB-DTR)
            //           TO DTR-PRE-PP-IAS
            ws.getDettTitDiRat().getDtrPrePpIas().setDtrPrePpIas(Trunc.toDecimal(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrPrePpIas().getWdtrPrePpIas(), 15, 3));
        }
        // COB_CODE: IF (SF)-PRE-SOLO-RSH-NULL(IX-TAB-DTR) = HIGH-VALUES
        //              TO DTR-PRE-SOLO-RSH-NULL
        //           ELSE
        //              TO DTR-PRE-SOLO-RSH
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrPreSoloRsh().getWdtrPreSoloRshNullFormatted())) {
            // COB_CODE: MOVE (SF)-PRE-SOLO-RSH-NULL(IX-TAB-DTR)
            //           TO DTR-PRE-SOLO-RSH-NULL
            ws.getDettTitDiRat().getDtrPreSoloRsh().setDtrPreSoloRshNull(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrPreSoloRsh().getWdtrPreSoloRshNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PRE-SOLO-RSH(IX-TAB-DTR)
            //           TO DTR-PRE-SOLO-RSH
            ws.getDettTitDiRat().getDtrPreSoloRsh().setDtrPreSoloRsh(Trunc.toDecimal(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrPreSoloRsh().getWdtrPreSoloRsh(), 15, 3));
        }
        // COB_CODE: IF (SF)-CAR-IAS-NULL(IX-TAB-DTR) = HIGH-VALUES
        //              TO DTR-CAR-IAS-NULL
        //           ELSE
        //              TO DTR-CAR-IAS
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrCarIas().getWdtrCarIasNullFormatted())) {
            // COB_CODE: MOVE (SF)-CAR-IAS-NULL(IX-TAB-DTR)
            //           TO DTR-CAR-IAS-NULL
            ws.getDettTitDiRat().getDtrCarIas().setDtrCarIasNull(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrCarIas().getWdtrCarIasNull());
        }
        else {
            // COB_CODE: MOVE (SF)-CAR-IAS(IX-TAB-DTR)
            //           TO DTR-CAR-IAS
            ws.getDettTitDiRat().getDtrCarIas().setDtrCarIas(Trunc.toDecimal(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrCarIas().getWdtrCarIas(), 15, 3));
        }
        // COB_CODE: IF (SF)-PROV-ACQ-1AA-NULL(IX-TAB-DTR) = HIGH-VALUES
        //              TO DTR-PROV-ACQ-1AA-NULL
        //           ELSE
        //              TO DTR-PROV-ACQ-1AA
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrProvAcq1aa().getWdtrProvAcq1aaNullFormatted())) {
            // COB_CODE: MOVE (SF)-PROV-ACQ-1AA-NULL(IX-TAB-DTR)
            //           TO DTR-PROV-ACQ-1AA-NULL
            ws.getDettTitDiRat().getDtrProvAcq1aa().setDtrProvAcq1aaNull(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrProvAcq1aa().getWdtrProvAcq1aaNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PROV-ACQ-1AA(IX-TAB-DTR)
            //           TO DTR-PROV-ACQ-1AA
            ws.getDettTitDiRat().getDtrProvAcq1aa().setDtrProvAcq1aa(Trunc.toDecimal(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrProvAcq1aa().getWdtrProvAcq1aa(), 15, 3));
        }
        // COB_CODE: IF (SF)-PROV-ACQ-2AA-NULL(IX-TAB-DTR) = HIGH-VALUES
        //              TO DTR-PROV-ACQ-2AA-NULL
        //           ELSE
        //              TO DTR-PROV-ACQ-2AA
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrProvAcq2aa().getWdtrProvAcq2aaNullFormatted())) {
            // COB_CODE: MOVE (SF)-PROV-ACQ-2AA-NULL(IX-TAB-DTR)
            //           TO DTR-PROV-ACQ-2AA-NULL
            ws.getDettTitDiRat().getDtrProvAcq2aa().setDtrProvAcq2aaNull(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrProvAcq2aa().getWdtrProvAcq2aaNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PROV-ACQ-2AA(IX-TAB-DTR)
            //           TO DTR-PROV-ACQ-2AA
            ws.getDettTitDiRat().getDtrProvAcq2aa().setDtrProvAcq2aa(Trunc.toDecimal(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrProvAcq2aa().getWdtrProvAcq2aa(), 15, 3));
        }
        // COB_CODE: IF (SF)-PROV-RICOR-NULL(IX-TAB-DTR) = HIGH-VALUES
        //              TO DTR-PROV-RICOR-NULL
        //           ELSE
        //              TO DTR-PROV-RICOR
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrProvRicor().getWdtrProvRicorNullFormatted())) {
            // COB_CODE: MOVE (SF)-PROV-RICOR-NULL(IX-TAB-DTR)
            //           TO DTR-PROV-RICOR-NULL
            ws.getDettTitDiRat().getDtrProvRicor().setDtrProvRicorNull(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrProvRicor().getWdtrProvRicorNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PROV-RICOR(IX-TAB-DTR)
            //           TO DTR-PROV-RICOR
            ws.getDettTitDiRat().getDtrProvRicor().setDtrProvRicor(Trunc.toDecimal(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrProvRicor().getWdtrProvRicor(), 15, 3));
        }
        // COB_CODE: IF (SF)-PROV-INC-NULL(IX-TAB-DTR) = HIGH-VALUES
        //              TO DTR-PROV-INC-NULL
        //           ELSE
        //              TO DTR-PROV-INC
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrProvInc().getWdtrProvIncNullFormatted())) {
            // COB_CODE: MOVE (SF)-PROV-INC-NULL(IX-TAB-DTR)
            //           TO DTR-PROV-INC-NULL
            ws.getDettTitDiRat().getDtrProvInc().setDtrProvIncNull(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrProvInc().getWdtrProvIncNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PROV-INC(IX-TAB-DTR)
            //           TO DTR-PROV-INC
            ws.getDettTitDiRat().getDtrProvInc().setDtrProvInc(Trunc.toDecimal(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrProvInc().getWdtrProvInc(), 15, 3));
        }
        // COB_CODE: IF (SF)-PROV-DA-REC-NULL(IX-TAB-DTR) = HIGH-VALUES
        //              TO DTR-PROV-DA-REC-NULL
        //           ELSE
        //              TO DTR-PROV-DA-REC
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrProvDaRec().getWdtrProvDaRecNullFormatted())) {
            // COB_CODE: MOVE (SF)-PROV-DA-REC-NULL(IX-TAB-DTR)
            //           TO DTR-PROV-DA-REC-NULL
            ws.getDettTitDiRat().getDtrProvDaRec().setDtrProvDaRecNull(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrProvDaRec().getWdtrProvDaRecNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PROV-DA-REC(IX-TAB-DTR)
            //           TO DTR-PROV-DA-REC
            ws.getDettTitDiRat().getDtrProvDaRec().setDtrProvDaRec(Trunc.toDecimal(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrProvDaRec().getWdtrProvDaRec(), 15, 3));
        }
        // COB_CODE: IF (SF)-COD-DVS-NULL(IX-TAB-DTR) = HIGH-VALUES
        //              TO DTR-COD-DVS-NULL
        //           ELSE
        //              TO DTR-COD-DVS
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrCodDvsFormatted())) {
            // COB_CODE: MOVE (SF)-COD-DVS-NULL(IX-TAB-DTR)
            //           TO DTR-COD-DVS-NULL
            ws.getDettTitDiRat().setDtrCodDvs(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrCodDvs());
        }
        else {
            // COB_CODE: MOVE (SF)-COD-DVS(IX-TAB-DTR)
            //           TO DTR-COD-DVS
            ws.getDettTitDiRat().setDtrCodDvs(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrCodDvs());
        }
        // COB_CODE: IF (SF)-FRQ-MOVI-NULL(IX-TAB-DTR) = HIGH-VALUES
        //              TO DTR-FRQ-MOVI-NULL
        //           ELSE
        //              TO DTR-FRQ-MOVI
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrFrqMovi().getWdtrFrqMoviNullFormatted())) {
            // COB_CODE: MOVE (SF)-FRQ-MOVI-NULL(IX-TAB-DTR)
            //           TO DTR-FRQ-MOVI-NULL
            ws.getDettTitDiRat().getDtrFrqMovi().setDtrFrqMoviNull(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrFrqMovi().getWdtrFrqMoviNull());
        }
        else {
            // COB_CODE: MOVE (SF)-FRQ-MOVI(IX-TAB-DTR)
            //           TO DTR-FRQ-MOVI
            ws.getDettTitDiRat().getDtrFrqMovi().setDtrFrqMovi(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrFrqMovi().getWdtrFrqMovi());
        }
        // COB_CODE: MOVE (SF)-TP-RGM-FISC(IX-TAB-DTR)
        //              TO DTR-TP-RGM-FISC
        ws.getDettTitDiRat().setDtrTpRgmFisc(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrTpRgmFisc());
        // COB_CODE: IF (SF)-COD-TARI-NULL(IX-TAB-DTR) = HIGH-VALUES
        //              TO DTR-COD-TARI-NULL
        //           ELSE
        //              TO DTR-COD-TARI
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrCodTariFormatted())) {
            // COB_CODE: MOVE (SF)-COD-TARI-NULL(IX-TAB-DTR)
            //           TO DTR-COD-TARI-NULL
            ws.getDettTitDiRat().setDtrCodTari(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrCodTari());
        }
        else {
            // COB_CODE: MOVE (SF)-COD-TARI(IX-TAB-DTR)
            //           TO DTR-COD-TARI
            ws.getDettTitDiRat().setDtrCodTari(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrCodTari());
        }
        // COB_CODE: MOVE (SF)-TP-STAT-TIT(IX-TAB-DTR)
        //              TO DTR-TP-STAT-TIT
        ws.getDettTitDiRat().setDtrTpStatTit(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrTpStatTit());
        // COB_CODE: IF (SF)-IMP-AZ-NULL(IX-TAB-DTR) = HIGH-VALUES
        //              TO DTR-IMP-AZ-NULL
        //           ELSE
        //              TO DTR-IMP-AZ
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrImpAz().getWdtrImpAzNullFormatted())) {
            // COB_CODE: MOVE (SF)-IMP-AZ-NULL(IX-TAB-DTR)
            //           TO DTR-IMP-AZ-NULL
            ws.getDettTitDiRat().getDtrImpAz().setDtrImpAzNull(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrImpAz().getWdtrImpAzNull());
        }
        else {
            // COB_CODE: MOVE (SF)-IMP-AZ(IX-TAB-DTR)
            //           TO DTR-IMP-AZ
            ws.getDettTitDiRat().getDtrImpAz().setDtrImpAz(Trunc.toDecimal(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrImpAz().getWdtrImpAz(), 15, 3));
        }
        // COB_CODE: IF (SF)-IMP-ADER-NULL(IX-TAB-DTR) = HIGH-VALUES
        //              TO DTR-IMP-ADER-NULL
        //           ELSE
        //              TO DTR-IMP-ADER
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrImpAder().getWdtrImpAderNullFormatted())) {
            // COB_CODE: MOVE (SF)-IMP-ADER-NULL(IX-TAB-DTR)
            //           TO DTR-IMP-ADER-NULL
            ws.getDettTitDiRat().getDtrImpAder().setDtrImpAderNull(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrImpAder().getWdtrImpAderNull());
        }
        else {
            // COB_CODE: MOVE (SF)-IMP-ADER(IX-TAB-DTR)
            //           TO DTR-IMP-ADER
            ws.getDettTitDiRat().getDtrImpAder().setDtrImpAder(Trunc.toDecimal(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrImpAder().getWdtrImpAder(), 15, 3));
        }
        // COB_CODE: IF (SF)-IMP-TFR-NULL(IX-TAB-DTR) = HIGH-VALUES
        //              TO DTR-IMP-TFR-NULL
        //           ELSE
        //              TO DTR-IMP-TFR
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrImpTfr().getWdtrImpTfrNullFormatted())) {
            // COB_CODE: MOVE (SF)-IMP-TFR-NULL(IX-TAB-DTR)
            //           TO DTR-IMP-TFR-NULL
            ws.getDettTitDiRat().getDtrImpTfr().setDtrImpTfrNull(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrImpTfr().getWdtrImpTfrNull());
        }
        else {
            // COB_CODE: MOVE (SF)-IMP-TFR(IX-TAB-DTR)
            //           TO DTR-IMP-TFR
            ws.getDettTitDiRat().getDtrImpTfr().setDtrImpTfr(Trunc.toDecimal(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrImpTfr().getWdtrImpTfr(), 15, 3));
        }
        // COB_CODE: IF (SF)-IMP-VOLO-NULL(IX-TAB-DTR) = HIGH-VALUES
        //              TO DTR-IMP-VOLO-NULL
        //           ELSE
        //              TO DTR-IMP-VOLO
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrImpVolo().getWdtrImpVoloNullFormatted())) {
            // COB_CODE: MOVE (SF)-IMP-VOLO-NULL(IX-TAB-DTR)
            //           TO DTR-IMP-VOLO-NULL
            ws.getDettTitDiRat().getDtrImpVolo().setDtrImpVoloNull(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrImpVolo().getWdtrImpVoloNull());
        }
        else {
            // COB_CODE: MOVE (SF)-IMP-VOLO(IX-TAB-DTR)
            //           TO DTR-IMP-VOLO
            ws.getDettTitDiRat().getDtrImpVolo().setDtrImpVolo(Trunc.toDecimal(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrImpVolo().getWdtrImpVolo(), 15, 3));
        }
        // COB_CODE: IF (SF)-FL-VLDT-TIT-NULL(IX-TAB-DTR) = HIGH-VALUES
        //              TO DTR-FL-VLDT-TIT-NULL
        //           ELSE
        //              TO DTR-FL-VLDT-TIT
        //           END-IF
        if (Conditions.eq(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrFlVldtTit(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE (SF)-FL-VLDT-TIT-NULL(IX-TAB-DTR)
            //           TO DTR-FL-VLDT-TIT-NULL
            ws.getDettTitDiRat().setDtrFlVldtTit(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrFlVldtTit());
        }
        else {
            // COB_CODE: MOVE (SF)-FL-VLDT-TIT(IX-TAB-DTR)
            //           TO DTR-FL-VLDT-TIT
            ws.getDettTitDiRat().setDtrFlVldtTit(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrFlVldtTit());
        }
        // COB_CODE: IF (SF)-CAR-ACQ-NULL(IX-TAB-DTR) = HIGH-VALUES
        //              TO DTR-CAR-ACQ-NULL
        //           ELSE
        //              TO DTR-CAR-ACQ
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrCarAcq().getWdtrCarAcqNullFormatted())) {
            // COB_CODE: MOVE (SF)-CAR-ACQ-NULL(IX-TAB-DTR)
            //           TO DTR-CAR-ACQ-NULL
            ws.getDettTitDiRat().getDtrCarAcq().setDtrCarAcqNull(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrCarAcq().getWdtrCarAcqNull());
        }
        else {
            // COB_CODE: MOVE (SF)-CAR-ACQ(IX-TAB-DTR)
            //           TO DTR-CAR-ACQ
            ws.getDettTitDiRat().getDtrCarAcq().setDtrCarAcq(Trunc.toDecimal(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrCarAcq().getWdtrCarAcq(), 15, 3));
        }
        // COB_CODE: IF (SF)-CAR-GEST-NULL(IX-TAB-DTR) = HIGH-VALUES
        //              TO DTR-CAR-GEST-NULL
        //           ELSE
        //              TO DTR-CAR-GEST
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrCarGest().getWdtrCarGestNullFormatted())) {
            // COB_CODE: MOVE (SF)-CAR-GEST-NULL(IX-TAB-DTR)
            //           TO DTR-CAR-GEST-NULL
            ws.getDettTitDiRat().getDtrCarGest().setDtrCarGestNull(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrCarGest().getWdtrCarGestNull());
        }
        else {
            // COB_CODE: MOVE (SF)-CAR-GEST(IX-TAB-DTR)
            //           TO DTR-CAR-GEST
            ws.getDettTitDiRat().getDtrCarGest().setDtrCarGest(Trunc.toDecimal(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrCarGest().getWdtrCarGest(), 15, 3));
        }
        // COB_CODE: IF (SF)-CAR-INC-NULL(IX-TAB-DTR) = HIGH-VALUES
        //              TO DTR-CAR-INC-NULL
        //           ELSE
        //              TO DTR-CAR-INC
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrCarInc().getWdtrCarIncNullFormatted())) {
            // COB_CODE: MOVE (SF)-CAR-INC-NULL(IX-TAB-DTR)
            //           TO DTR-CAR-INC-NULL
            ws.getDettTitDiRat().getDtrCarInc().setDtrCarIncNull(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrCarInc().getWdtrCarIncNull());
        }
        else {
            // COB_CODE: MOVE (SF)-CAR-INC(IX-TAB-DTR)
            //           TO DTR-CAR-INC
            ws.getDettTitDiRat().getDtrCarInc().setDtrCarInc(Trunc.toDecimal(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrCarInc().getWdtrCarInc(), 15, 3));
        }
        // COB_CODE: IF (SF)-MANFEE-ANTIC-NULL(IX-TAB-DTR) = HIGH-VALUES
        //              TO DTR-MANFEE-ANTIC-NULL
        //           ELSE
        //              TO DTR-MANFEE-ANTIC
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrManfeeAntic().getWdtrManfeeAnticNullFormatted())) {
            // COB_CODE: MOVE (SF)-MANFEE-ANTIC-NULL(IX-TAB-DTR)
            //           TO DTR-MANFEE-ANTIC-NULL
            ws.getDettTitDiRat().getDtrManfeeAntic().setDtrManfeeAnticNull(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrManfeeAntic().getWdtrManfeeAnticNull());
        }
        else {
            // COB_CODE: MOVE (SF)-MANFEE-ANTIC(IX-TAB-DTR)
            //           TO DTR-MANFEE-ANTIC
            ws.getDettTitDiRat().getDtrManfeeAntic().setDtrManfeeAntic(Trunc.toDecimal(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrManfeeAntic().getWdtrManfeeAntic(), 15, 3));
        }
        // COB_CODE: IF (SF)-MANFEE-RICOR-NULL(IX-TAB-DTR) = HIGH-VALUES
        //              TO DTR-MANFEE-RICOR-NULL
        //           ELSE
        //              TO DTR-MANFEE-RICOR
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrManfeeRicor().getWdtrManfeeRicorNullFormatted())) {
            // COB_CODE: MOVE (SF)-MANFEE-RICOR-NULL(IX-TAB-DTR)
            //           TO DTR-MANFEE-RICOR-NULL
            ws.getDettTitDiRat().getDtrManfeeRicor().setDtrManfeeRicorNull(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrManfeeRicor().getWdtrManfeeRicorNull());
        }
        else {
            // COB_CODE: MOVE (SF)-MANFEE-RICOR(IX-TAB-DTR)
            //           TO DTR-MANFEE-RICOR
            ws.getDettTitDiRat().getDtrManfeeRicor().setDtrManfeeRicor(Trunc.toDecimal(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrManfeeRicor().getWdtrManfeeRicor(), 15, 3));
        }
        // COB_CODE: IF (SF)-MANFEE-REC-NULL(IX-TAB-DTR) = HIGH-VALUES
        //              TO DTR-MANFEE-REC-NULL
        //           ELSE
        //              TO DTR-MANFEE-REC
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrManfeeRecFormatted())) {
            // COB_CODE: MOVE (SF)-MANFEE-REC-NULL(IX-TAB-DTR)
            //           TO DTR-MANFEE-REC-NULL
            ws.getDettTitDiRat().setDtrManfeeRec(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrManfeeRec());
        }
        else {
            // COB_CODE: MOVE (SF)-MANFEE-REC(IX-TAB-DTR)
            //           TO DTR-MANFEE-REC
            ws.getDettTitDiRat().setDtrManfeeRec(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrManfeeRec());
        }
        // COB_CODE: IF (SF)-TOT-INTR-PREST-NULL(IX-TAB-DTR) = HIGH-VALUES
        //              TO DTR-TOT-INTR-PREST-NULL
        //           ELSE
        //              TO DTR-TOT-INTR-PREST
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrTotIntrPrest().getWdtrTotIntrPrestNullFormatted())) {
            // COB_CODE: MOVE (SF)-TOT-INTR-PREST-NULL(IX-TAB-DTR)
            //           TO DTR-TOT-INTR-PREST-NULL
            ws.getDettTitDiRat().getDtrTotIntrPrest().setDtrTotIntrPrestNull(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrTotIntrPrest().getWdtrTotIntrPrestNull());
        }
        else {
            // COB_CODE: MOVE (SF)-TOT-INTR-PREST(IX-TAB-DTR)
            //           TO DTR-TOT-INTR-PREST
            ws.getDettTitDiRat().getDtrTotIntrPrest().setDtrTotIntrPrest(Trunc.toDecimal(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrTotIntrPrest().getWdtrTotIntrPrest(), 15, 3));
        }
        // COB_CODE: IF (SF)-DS-RIGA(IX-TAB-DTR) NOT NUMERIC
        //              MOVE 0 TO DTR-DS-RIGA
        //           ELSE
        //              TO DTR-DS-RIGA
        //           END-IF
        if (!Functions.isNumber(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrDsRiga())) {
            // COB_CODE: MOVE 0 TO DTR-DS-RIGA
            ws.getDettTitDiRat().setDtrDsRiga(0);
        }
        else {
            // COB_CODE: MOVE (SF)-DS-RIGA(IX-TAB-DTR)
            //           TO DTR-DS-RIGA
            ws.getDettTitDiRat().setDtrDsRiga(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrDsRiga());
        }
        // COB_CODE: MOVE (SF)-DS-OPER-SQL(IX-TAB-DTR)
        //              TO DTR-DS-OPER-SQL
        ws.getDettTitDiRat().setDtrDsOperSql(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrDsOperSql());
        // COB_CODE: IF (SF)-DS-VER(IX-TAB-DTR) NOT NUMERIC
        //              MOVE 0 TO DTR-DS-VER
        //           ELSE
        //              TO DTR-DS-VER
        //           END-IF
        if (!Functions.isNumber(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrDsVer())) {
            // COB_CODE: MOVE 0 TO DTR-DS-VER
            ws.getDettTitDiRat().setDtrDsVer(0);
        }
        else {
            // COB_CODE: MOVE (SF)-DS-VER(IX-TAB-DTR)
            //           TO DTR-DS-VER
            ws.getDettTitDiRat().setDtrDsVer(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrDsVer());
        }
        // COB_CODE: IF (SF)-DS-TS-INI-CPTZ(IX-TAB-DTR) NOT NUMERIC
        //              MOVE 0 TO DTR-DS-TS-INI-CPTZ
        //           ELSE
        //              TO DTR-DS-TS-INI-CPTZ
        //           END-IF
        if (!Functions.isNumber(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrDsTsIniCptz())) {
            // COB_CODE: MOVE 0 TO DTR-DS-TS-INI-CPTZ
            ws.getDettTitDiRat().setDtrDsTsIniCptz(0);
        }
        else {
            // COB_CODE: MOVE (SF)-DS-TS-INI-CPTZ(IX-TAB-DTR)
            //           TO DTR-DS-TS-INI-CPTZ
            ws.getDettTitDiRat().setDtrDsTsIniCptz(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrDsTsIniCptz());
        }
        // COB_CODE: IF (SF)-DS-TS-END-CPTZ(IX-TAB-DTR) NOT NUMERIC
        //              MOVE 0 TO DTR-DS-TS-END-CPTZ
        //           ELSE
        //              TO DTR-DS-TS-END-CPTZ
        //           END-IF
        if (!Functions.isNumber(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrDsTsEndCptz())) {
            // COB_CODE: MOVE 0 TO DTR-DS-TS-END-CPTZ
            ws.getDettTitDiRat().setDtrDsTsEndCptz(0);
        }
        else {
            // COB_CODE: MOVE (SF)-DS-TS-END-CPTZ(IX-TAB-DTR)
            //           TO DTR-DS-TS-END-CPTZ
            ws.getDettTitDiRat().setDtrDsTsEndCptz(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrDsTsEndCptz());
        }
        // COB_CODE: MOVE (SF)-DS-UTENTE(IX-TAB-DTR)
        //              TO DTR-DS-UTENTE
        ws.getDettTitDiRat().setDtrDsUtente(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrDsUtente());
        // COB_CODE: MOVE (SF)-DS-STATO-ELAB(IX-TAB-DTR)
        //              TO DTR-DS-STATO-ELAB
        ws.getDettTitDiRat().setDtrDsStatoElab(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrDsStatoElab());
        // COB_CODE: IF (SF)-IMP-TRASFE-NULL(IX-TAB-DTR) = HIGH-VALUES
        //              TO DTR-IMP-TRASFE-NULL
        //           ELSE
        //              TO DTR-IMP-TRASFE
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrImpTrasfe().getWdtrImpTrasfeNullFormatted())) {
            // COB_CODE: MOVE (SF)-IMP-TRASFE-NULL(IX-TAB-DTR)
            //           TO DTR-IMP-TRASFE-NULL
            ws.getDettTitDiRat().getDtrImpTrasfe().setDtrImpTrasfeNull(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrImpTrasfe().getWdtrImpTrasfeNull());
        }
        else {
            // COB_CODE: MOVE (SF)-IMP-TRASFE(IX-TAB-DTR)
            //           TO DTR-IMP-TRASFE
            ws.getDettTitDiRat().getDtrImpTrasfe().setDtrImpTrasfe(Trunc.toDecimal(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrImpTrasfe().getWdtrImpTrasfe(), 15, 3));
        }
        // COB_CODE: IF (SF)-IMP-TFR-STRC-NULL(IX-TAB-DTR) = HIGH-VALUES
        //              TO DTR-IMP-TFR-STRC-NULL
        //           ELSE
        //              TO DTR-IMP-TFR-STRC
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrImpTfrStrc().getWdtrImpTfrStrcNullFormatted())) {
            // COB_CODE: MOVE (SF)-IMP-TFR-STRC-NULL(IX-TAB-DTR)
            //           TO DTR-IMP-TFR-STRC-NULL
            ws.getDettTitDiRat().getDtrImpTfrStrc().setDtrImpTfrStrcNull(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrImpTfrStrc().getWdtrImpTfrStrcNull());
        }
        else {
            // COB_CODE: MOVE (SF)-IMP-TFR-STRC(IX-TAB-DTR)
            //           TO DTR-IMP-TFR-STRC
            ws.getDettTitDiRat().getDtrImpTfrStrc().setDtrImpTfrStrc(Trunc.toDecimal(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrImpTfrStrc().getWdtrImpTfrStrc(), 15, 3));
        }
        // COB_CODE: IF (SF)-ACQ-EXP-NULL(IX-TAB-DTR) = HIGH-VALUES
        //              TO DTR-ACQ-EXP-NULL
        //           ELSE
        //              TO DTR-ACQ-EXP
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrAcqExp().getWdtrAcqExpNullFormatted())) {
            // COB_CODE: MOVE (SF)-ACQ-EXP-NULL(IX-TAB-DTR)
            //           TO DTR-ACQ-EXP-NULL
            ws.getDettTitDiRat().getDtrAcqExp().setDtrAcqExpNull(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrAcqExp().getWdtrAcqExpNull());
        }
        else {
            // COB_CODE: MOVE (SF)-ACQ-EXP(IX-TAB-DTR)
            //           TO DTR-ACQ-EXP
            ws.getDettTitDiRat().getDtrAcqExp().setDtrAcqExp(Trunc.toDecimal(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrAcqExp().getWdtrAcqExp(), 15, 3));
        }
        // COB_CODE: IF (SF)-REMUN-ASS-NULL(IX-TAB-DTR) = HIGH-VALUES
        //              TO DTR-REMUN-ASS-NULL
        //           ELSE
        //              TO DTR-REMUN-ASS
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrRemunAss().getWdtrRemunAssNullFormatted())) {
            // COB_CODE: MOVE (SF)-REMUN-ASS-NULL(IX-TAB-DTR)
            //           TO DTR-REMUN-ASS-NULL
            ws.getDettTitDiRat().getDtrRemunAss().setDtrRemunAssNull(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrRemunAss().getWdtrRemunAssNull());
        }
        else {
            // COB_CODE: MOVE (SF)-REMUN-ASS(IX-TAB-DTR)
            //           TO DTR-REMUN-ASS
            ws.getDettTitDiRat().getDtrRemunAss().setDtrRemunAss(Trunc.toDecimal(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrRemunAss().getWdtrRemunAss(), 15, 3));
        }
        // COB_CODE: IF (SF)-COMMIS-INTER-NULL(IX-TAB-DTR) = HIGH-VALUES
        //              TO DTR-COMMIS-INTER-NULL
        //           ELSE
        //              TO DTR-COMMIS-INTER
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrCommisInter().getWdtrCommisInterNullFormatted())) {
            // COB_CODE: MOVE (SF)-COMMIS-INTER-NULL(IX-TAB-DTR)
            //           TO DTR-COMMIS-INTER-NULL
            ws.getDettTitDiRat().getDtrCommisInter().setDtrCommisInterNull(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrCommisInter().getWdtrCommisInterNull());
        }
        else {
            // COB_CODE: MOVE (SF)-COMMIS-INTER(IX-TAB-DTR)
            //           TO DTR-COMMIS-INTER
            ws.getDettTitDiRat().getDtrCommisInter().setDtrCommisInter(Trunc.toDecimal(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrCommisInter().getWdtrCommisInter(), 15, 3));
        }
        // COB_CODE: IF (SF)-CNBT-ANTIRAC-NULL(IX-TAB-DTR) = HIGH-VALUES
        //              TO DTR-CNBT-ANTIRAC-NULL
        //           ELSE
        //              TO DTR-CNBT-ANTIRAC
        //           END-IF.
        if (Characters.EQ_HIGH.test(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrCnbtAntirac().getWdtrCnbtAntiracNullFormatted())) {
            // COB_CODE: MOVE (SF)-CNBT-ANTIRAC-NULL(IX-TAB-DTR)
            //           TO DTR-CNBT-ANTIRAC-NULL
            ws.getDettTitDiRat().getDtrCnbtAntirac().setDtrCnbtAntiracNull(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrCnbtAntirac().getWdtrCnbtAntiracNull());
        }
        else {
            // COB_CODE: MOVE (SF)-CNBT-ANTIRAC(IX-TAB-DTR)
            //           TO DTR-CNBT-ANTIRAC
            ws.getDettTitDiRat().getDtrCnbtAntirac().setDtrCnbtAntirac(Trunc.toDecimal(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrCnbtAntirac().getWdtrCnbtAntirac(), 15, 3));
        }
    }

    /**Original name: AGGIORNA-DETT-TIT-DI-RAT<br>
	 * <pre>----------------------------------------------------------------*
	 *     COPY      ..... LCCVDTR6
	 *     TIPOLOGIA...... SERVIZIO DI EOC
	 *     DESCRIZIONE.... AGGIORNAMENTO DETTAGLIO TITOLO DI RATA
	 * ----------------------------------------------------------------*
	 *     N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
	 *     ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
	 *   - COPY LCCV*5 (VALORIZZAZIONE DCLGEN)
	 *   - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
	 *   - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
	 *   - DICHIARAZIONE DCLGEN (LCCV*1)
	 *   - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
	 * ----------------------------------------------------------------*
	 * --> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI</pre>*/
    private void aggiornaDettTitDiRat() {
        // COB_CODE: INITIALIZE DETT-TIT-DI-RAT.
        initDettTitDiRat();
        //--> NOME TABELLA FISICA DB
        // COB_CODE: MOVE 'DETT-TIT-DI-RAT'                  TO WK-TABELLA.
        ws.setWkTabella("DETT-TIT-DI-RAT");
        //--> SE LO STATUS NON E' "INVARIATO"
        // COB_CODE:      IF NOT WDTR-ST-INV(IX-TAB-DTR)
        //                   AND WDTR-ELE-DTR-MAX NOT = 0
        //           *--->   Impostare ID Tabella DETTAGLIO TITOLO DI RATA
        //                   END-IF
        //                END-IF.
        if (!ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getStatus().isInv() && ws.getWdtrEleDtrMax() != 0) {
            //--->   Impostare ID Tabella DETTAGLIO TITOLO DI RATA
            // COB_CODE: MOVE WMOV-ID-PTF
            //             TO DTR-ID-MOVI-CRZ
            ws.getDettTitDiRat().setDtrIdMoviCrz(areaMain.getWmovAreaMovimento().getLccvmov1().getIdPtf());
            // COB_CODE: MOVE WDTR-ID-TIT-RAT(IX-TAB-DTR)
            //             TO WS-ID-TIT-RAT
            ws.setWsIdTitRat(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrIdTitRat());
            //-->    RICERCA DELL'ID-TIT-DI-RATA NELLA TABELLA PADRE
            // COB_CODE: PERFORM RICERCA-TIT-RAT-PTF
            //              THRU RICERCA-TIT-RAT-PTF-EX
            ricercaTitRatPtf();
            //-->        INSERIMENTO
            // COB_CODE:         EVALUATE TRUE
            //           *-->        INSERIMENTO
            //                       WHEN WDTR-ST-ADD(IX-TAB-DTR)
            //                            SET  IDSI0011-AGGIORNAMENTO-STORICO   TO TRUE
            //           *-->        CANCELLAZIONE
            //                       WHEN WDTR-ST-DEL(IX-TAB-DTR)
            //                          SET  IDSI0011-DELETE-LOGICA    TO TRUE
            //           *-->        MODIFICA
            //                       WHEN WDTR-ST-MOD(IX-TAB-DTR)
            //                          SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE
            //                   END-EVALUATE
            switch (ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getStatus().getStatus()) {

                case WpolStatus.ADD:// COB_CODE: PERFORM ESTR-SEQUENCE
                    //              THRU ESTR-SEQUENCE-EX
                    estrSequence();
                    // COB_CODE: IF IDSV0001-ESITO-OK
                    //              END-IF
                    //           END-IF
                    if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                        // COB_CODE: MOVE S090-SEQ-TABELLA
                        //             TO WDTR-ID-PTF(IX-TAB-DTR)
                        //                DTR-ID-DETT-TIT-DI-RAT
                        ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().setIdPtf(ws.getAreaIoLccs0090().getSeqTabella());
                        ws.getDettTitDiRat().setDtrIdDettTitDiRat(ws.getAreaIoLccs0090().getSeqTabella());
                        // COB_CODE: MOVE WS-ID-PTF-TIT-RAT
                        //             TO DTR-ID-TIT-RAT
                        ws.getDettTitDiRat().setDtrIdTitRat(ws.getWsIdPtfTitRat());
                        //-->       ESTRAZIONE OGGETTO PTF
                        // COB_CODE: PERFORM PREPARA-AREA-LCCS0234-DTR
                        //              THRU PREPARA-AREA-LCCS0234-DTR-EX
                        preparaAreaLccs0234Dtr();
                        // COB_CODE: PERFORM CALL-LCCS0234
                        //              THRU CALL-LCCS0234-EX
                        callLccs0234();
                        // COB_CODE: IF IDSV0001-ESITO-OK
                        //                TO DTR-ID-OGG
                        //           END-IF
                        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                            // COB_CODE: MOVE S234-ID-OGG-PTF-EOC
                            //             TO DTR-ID-OGG
                            ws.getDettTitDiRat().setDtrIdOgg(ws.getS234DatiOutput().getIdOggPtfEoc());
                        }
                    }
                    //-->             TIPO OPERAZIONE DISPATCHER
                    // COB_CODE: SET  IDSI0011-AGGIORNAMENTO-STORICO   TO TRUE
                    ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setIdsi0011AggiornamentoStorico();
                    //-->        CANCELLAZIONE
                    break;

                case WpolStatus.DEL:// COB_CODE: MOVE WDTR-ID-PTF(IX-TAB-DTR)
                    //             TO DTR-ID-DETT-TIT-DI-RAT
                    ws.getDettTitDiRat().setDtrIdDettTitDiRat(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getIdPtf());
                    // COB_CODE: MOVE WDTR-ID-TIT-RAT(IX-TAB-DTR)
                    //             TO DTR-ID-TIT-RAT
                    ws.getDettTitDiRat().setDtrIdTitRat(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrIdTitRat());
                    // COB_CODE: MOVE WDTR-ID-OGG(IX-TAB-DTR)
                    //             TO DTR-ID-OGG
                    ws.getDettTitDiRat().setDtrIdOgg(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrIdOgg());
                    //-->           TIPO OPERAZIONE DISPATCHER
                    // COB_CODE: SET  IDSI0011-DELETE-LOGICA    TO TRUE
                    ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setIdsi0011DeleteLogica();
                    //-->        MODIFICA
                    break;

                case WpolStatus.MOD:// COB_CODE: MOVE WDTR-ID-PTF(IX-TAB-DTR)
                    //             TO DTR-ID-DETT-TIT-DI-RAT
                    ws.getDettTitDiRat().setDtrIdDettTitDiRat(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getIdPtf());
                    // COB_CODE: MOVE WDTR-ID-TIT-RAT(IX-TAB-DTR)
                    //             TO DTR-ID-TIT-RAT
                    ws.getDettTitDiRat().setDtrIdTitRat(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrIdTitRat());
                    // COB_CODE: MOVE WDTR-ID-OGG(IX-TAB-DTR)
                    //             TO DTR-ID-OGG
                    ws.getDettTitDiRat().setDtrIdOgg(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrIdOgg());
                    //-->           TIPO OPERAZIONE DISPATCHER
                    // COB_CODE: SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE
                    ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setIdsi0011AggiornamentoStorico();
                    break;

                default:break;
            }
            // COB_CODE:         IF IDSV0001-ESITO-OK
            //           *-->       VALORIZZA DCLGEN DETTAGLIO TITOLO DI RATA
            //                         THRU AGGIORNA-TABELLA-EX
            //                   END-IF
            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                //-->       VALORIZZA DCLGEN DETTAGLIO TITOLO DI RATA
                // COB_CODE: PERFORM VAL-DCLGEN-DTR
                //              THRU VAL-DCLGEN-DTR-EX
                valDclgenDtr();
                //-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                // COB_CODE: PERFORM VALORIZZA-AREA-DSH-DTR
                //              THRU VALORIZZA-AREA-DSH-DTR-EX
                valorizzaAreaDshDtr();
                //-->       CALL DISPATCHER PER AGGIORNAMENTO
                // COB_CODE: PERFORM AGGIORNA-TABELLA
                //              THRU AGGIORNA-TABELLA-EX
                aggiornaTabella();
            }
        }
    }

    /**Original name: RICERCA-TIT-RAT-PTF<br>
	 * <pre>----------------------------------------------------------------*
	 *      RICERCA DELL' ID-TIT-RAT-PTF DELLA TABELLA DETT. TIT.RATA
	 *      NELLA TABELLA TITOLO DI RATA
	 * ----------------------------------------------------------------*</pre>*/
    private void ricercaTitRatPtf() {
        // COB_CODE: SET NON-TROVATO TO TRUE.
        ws.getFlag().setNonTrovato();
        // COB_CODE: PERFORM VARYING IX-TAB-TDR FROM 1 BY 1
        //             UNTIL IX-TAB-TDR > WTDR-ELE-TDR-MAX
        //                OR TROVATO
        //             END-IF
        //           END-PERFORM.
        ws.getIxIndici().setTdr(((short)1));
        while (!(ws.getIxIndici().getTdr() > ws.getWtdrEleTdrMax() || ws.getFlag().isTrovato())) {
            // COB_CODE: IF WS-ID-TIT-RAT = WTDR-ID-TIT-RAT(IX-TAB-TDR)
            //              SET  TROVATO                  TO TRUE
            //           END-IF
            if (ws.getWsIdTitRat() == ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getDati().getWtdrIdTitRat()) {
                // COB_CODE: MOVE WTDR-ID-PTF(IX-TAB-TDR)  TO WS-ID-PTF-TIT-RAT
                ws.setWsIdPtfTitRat(ws.getWtdrTabTitRat(ws.getIxIndici().getTdr()).getLccvtdr1().getIdPtf());
                // COB_CODE: SET  TROVATO                  TO TRUE
                ws.getFlag().setTrovato();
            }
            ws.getIxIndici().setTdr(Trunc.toShort(ws.getIxIndici().getTdr() + 1, 4));
        }
    }

    /**Original name: PREPARA-AREA-LCCS0234-DTR<br>
	 * <pre>----------------------------------------------------------------*
	 *    PREPARA PER LA CALL ALL'LCCS0234 ESTRATTORE OGGETTO PTF
	 * ----------------------------------------------------------------*</pre>*/
    private void preparaAreaLccs0234Dtr() {
        // COB_CODE: MOVE WDTR-ID-OGG(IX-TAB-DTR)     TO S234-ID-OGG-EOC
        ws.getS234DatiInput().setIdOggEoc(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrIdOgg());
        // COB_CODE: MOVE WDTR-TP-OGG(IX-TAB-DTR)     TO S234-TIPO-OGG-EOC.
        ws.getS234DatiInput().setTipoOggEoc(ws.getWdtrTabDettTira(ws.getIxIndici().getDtr()).getLccvdtr1().getDati().getWdtrTpOgg());
    }

    /**Original name: VALORIZZA-AREA-DSH-DTR<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZA AREA COMUNE DISPATCHER
	 * ----------------------------------------------------------------*
	 * --> DCLGEN TABELLA</pre>*/
    private void valorizzaAreaDshDtr() {
        // COB_CODE: MOVE DETT-TIT-DI-RAT         TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getDettTitDiRat().getDettTitDiRatFormatted());
        //--> MODALITA DI ACCESSO
        // COB_CODE: SET  IDSI0011-ID             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setIdsi0011Id();
        //--> TIPO TABELLA (STORICA/NON STORICA)
        // COB_CODE: SET  IDSI0011-TRATT-DEFAULT           TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setIdsi0011TrattDefault();
        //--> VALORIZZAZIONE DATE EFFETTO
        // COB_CODE: MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaMain.getWmovAreaMovimento().getLccvmov1().getDati().getWmovDtEff());
        // COB_CODE: MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        // COB_CODE: MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
    }

    /**Original name: AGGIORNA-TABELLA<br>
	 * <pre>----------------------------------------------------------------*
	 *     COPY VALORIZZAZIONE DCLGEN
	 * ----------------------------------------------------------------*
	 *      COPY LCCVDTR5.
	 * ----------------------------------------------------------------*
	 *     COPY      ..... LCCP0001
	 *     TIPOLOGIA...... COPY PROCEDURE (COMPONENTI COMUNI)
	 *     DESCRIZIONE.... AGGIORNAMENTO TABELLA
	 * ----------------------------------------------------------------*
	 * ----------------------------------------------------------------*
	 *     AGGIORNAMENTO TABELLA
	 * ----------------------------------------------------------------*
	 * --> NOME TABELLA FISICA DB</pre>*/
    private void aggiornaTabella() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE WK-TABELLA               TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato(ws.getWkTabella());
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX.
        callDispatcher();
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //                   END-EVALUATE
        //                ELSE
        //           *-->ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $  RC=$  SQLCODE=$
        //                      THRU EX-S0300
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            //-->      OPERAZIONE ESEGUITA CORRETTAMENTE
            // COB_CODE:         EVALUATE TRUE
            //           *-->      OPERAZIONE ESEGUITA CORRETTAMENTE
            //                     WHEN IDSO0011-SUCCESSFUL-SQL
            //                        CONTINUE
            //           *-->ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $  RC=$  SQLCODE=$
            //                     WHEN OTHER
            //                           THRU EX-S0300
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL:// COB_CODE: CONTINUE
                //continue
                //-->ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $  RC=$  SQLCODE=$
                    break;

                default:// COB_CODE: MOVE WK-PGM           TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'AGGIORNA-TABELLA'
                    //                                 TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("AGGIORNA-TABELLA");
                    // COB_CODE: MOVE '005016'         TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005016");
                    // COB_CODE: STRING WK-TABELLA            ';'
                    //                  IDSO0011-RETURN-CODE  ';'
                    //                  IDSO0011-SQLCODE
                    //           DELIMITED BY SIZE   INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;
            }
        }
        else {
            //-->ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $  RC=$  SQLCODE=$
            // COB_CODE: MOVE WK-PGM                TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'AGGIORNA-TABELLA'
            //                                      TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("AGGIORNA-TABELLA");
            // COB_CODE: MOVE '005016'              TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING WK-TABELLA            ';'
            //                  IDSO0011-RETURN-CODE  ';'
            //                  IDSO0011-SQLCODE
            //           DELIMITED BY SIZE        INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: ESTR-SEQUENCE<br>
	 * <pre>----------------------------------------------------------------*
	 *     COPY      ..... LCCP0002
	 *     TIPOLOGIA...... COPY PROCEDURE (COMPONENTI COMUNI)
	 *     DESCRIZIONE.... ESTRAZIONE SEQUENCE
	 * ----------------------------------------------------------------*
	 * ----------------------------------------------------------------*
	 *     ESTRAZIONE SEQUENCE
	 * ----------------------------------------------------------------*</pre>*/
    private void estrSequence() {
        Lccs0090 lccs0090 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE WK-TABELLA TO S090-NOME-TABELLA.
        ws.getAreaIoLccs0090().setNomeTabella(ws.getWkTabella());
        // COB_CODE: CALL LCCS0090 USING AREA-IO-LCCS0090
        //           ON EXCEPTION
        //                 THRU EX-S0290
        //           END-CALL.
        try {
            lccs0090 = Lccs0090.getInstance();
            lccs0090.run(ws.getAreaIoLccs0090());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'SERVIZIO ESTRAZIONE SEQUENCE'
            //             TO CALL-DESC
            ws.getIdsv0002().setCallDesc("SERVIZIO ESTRAZIONE SEQUENCE");
            // COB_CODE: MOVE 'ESTR-SEQUENCE'
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("ESTR-SEQUENCE");
            // COB_CODE: PERFORM S0290-ERRORE-DI-SISTEMA
            //              THRU EX-S0290
            s0290ErroreDiSistema();
        }
        // COB_CODE: IF IDSV0001-ESITO-OK
        //              END-EVALUATE
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE:         EVALUATE S090-RETURN-CODE
            //                       WHEN '00'
            //                            CONTINUE
            //                       WHEN 'S1'
            //           *-->ERRORE ESTRAZIONE SEQUENCE SULLA TABELLA $ - RC=$ - SQLCODE=$
            //                             THRU EX-S0300
            //                       WHEN 'D3'
            //           *-->ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $  RC=$  SQLCODE=$
            //                             THRU EX-S0300
            //                   END-EVALUATE
            switch (ws.getAreaIoLccs0090().getReturnCode().getReturnCode()) {

                case "00":// COB_CODE: CONTINUE
                //continue
                    break;

                case "S1"://-->ERRORE ESTRAZIONE SEQUENCE SULLA TABELLA $ - RC=$ - SQLCODE=$
                    // COB_CODE: MOVE WK-PGM            TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'S1900-CALL-LCCS0090'
                    //                                  TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("S1900-CALL-LCCS0090");
                    // COB_CODE: MOVE '005015'          TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005015");
                    // COB_CODE: STRING WK-TABELLA        ';'
                    //                  S090-RETURN-CODE  ';'
                    //                  S090-SQLCODE
                    //           DELIMITED BY SIZE    INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getAreaIoLccs0090().getReturnCode().getReturnCodeFormatted(), ";", ws.getAreaIoLccs0090().getSqlcode().getIdso0021SqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;

                case "D3"://-->ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $  RC=$  SQLCODE=$
                    // COB_CODE: MOVE WK-PGM            TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'S1900-CALL-LCCS0090'
                    //                                  TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("S1900-CALL-LCCS0090");
                    // COB_CODE: MOVE '005016'          TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005016");
                    // COB_CODE: STRING WK-TABELLA        ';'
                    //                  S090-RETURN-CODE  ';'
                    //                  S090-SQLCODE
                    //           DELIMITED BY SIZE    INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getAreaIoLccs0090().getReturnCode().getReturnCodeFormatted(), ";", ws.getAreaIoLccs0090().getSqlcode().getIdso0021SqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;

                default:break;
            }
        }
    }

    /**Original name: S0290-ERRORE-DI-SISTEMA<br>
	 * <pre>----------------------------------------------------------------*
	 *   ROUTINES GESTIONE ERRORI
	 * ----------------------------------------------------------------*
	 * **-------------------------------------------------------------**
	 *     PROGRAMMA ..... IERP9902
	 *     TIPOLOGIA...... COPY
	 *     DESCRIZIONE.... ROUTINE GENERALIZZATA GESTIONE ERRORI CALL
	 * **-------------------------------------------------------------**</pre>*/
    private void s0290ErroreDiSistema() {
        // COB_CODE: MOVE '005006'           TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErroreFormatted("005006");
        // COB_CODE: MOVE CALL-DESC          TO IEAI9901-PARAMETRI-ERR.
        ws.getIeai9901Area().setParametriErr(ws.getIdsv0002().getCallDesc());
        // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
        //              THRU EX-S0300.
        s0300RicercaGravitaErrore();
    }

    /**Original name: S0300-RICERCA-GRAVITA-ERRORE<br>
	 * <pre>MUOVO L'AREA DEL SERVIZIO NELL'AREA DATI DELL'AREA DI CONTESTO.
	 * ----->VALORIZZO I CAMPI DELLA IDSV0001</pre>*/
    private void s0300RicercaGravitaErrore() {
        Ieas9900 ieas9900 = null;
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR       TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE  'IEAS9900'              TO CALL-PGM
        ws.getIdsv0002().setCallPgm("IEAS9900");
        // COB_CODE: CALL CALL-PGM USING IDSV0001-AREA-CONTESTO
        //                               IEAI9901-AREA
        //                               IEAO9901-AREA
        //             ON EXCEPTION
        //                   THRU EX-S0310-ERRORE-FATALE
        //           END-CALL.
        try {
            ieas9900 = Ieas9900.getInstance();
            ieas9900.run(areaIdsv0001, ws.getIeai9901Area(), ws.getIeao9901Area());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
            areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
            // COB_CODE: PERFORM S0310-ERRORE-FATALE
            //              THRU EX-S0310-ERRORE-FATALE
            s0310ErroreFatale();
        }
        //SE LA CHIAMATA AL SERVIZIO HA ESITO POSITIVO (NESSUN ERRORE DI
        //SISTEMA, VALORIZZO L'AREA DI OUTPUT.
        //INCREMENTO L'INDICE DEGLI ERRORI
        // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
        areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
        //SE L'INDICE E' MINORE DI 10 ...
        // COB_CODE:      IF IDSV0001-MAX-ELE-ERRORI NOT GREATER 10
        //           *SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
        //                   END-IF
        //                ELSE
        //           *IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        //                       TO IDSV0001-DESC-ERRORE-ESTESA
        //                END-IF.
        if (areaIdsv0001.getMaxEleErrori() <= 10) {
            //SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
            // COB_CODE: IF IEAO9901-COD-ERRORE-990 = ZEROES
            //                      EX-S0300-IMPOSTA-ERRORE
            //           ELSE
            //                   IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
            //           END-IF
            if (Characters.EQ_ZERO.test(ws.getIeao9901Area().getCodErrore990Formatted())) {
                // COB_CODE: PERFORM S0300-IMPOSTA-ERRORE THRU
                //                   EX-S0300-IMPOSTA-ERRORE
                s0300ImpostaErrore();
            }
            else {
                // COB_CODE: MOVE 'IEAS9900'
                //             TO IDSV0001-COD-SERVIZIO-BE
                areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
                // COB_CODE: MOVE IEAO9901-LABEL-ERR-990
                //             TO IDSV0001-LABEL-ERR
                areaIdsv0001.getLogErrore().setLabelErr(ws.getIeao9901Area().getLabelErr990());
                // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
                //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
                // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
                areaIdsv0001.getEsito().setIdsv0001EsitoKo();
                // IMPOSTO IL CODICE ERRORE GESTITO ALL'INTERNO DEL PROGRAMMA
                // COB_CODE: MOVE IEAO9901-COD-ERRORE-990
                //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeao9901Area().getCodErrore990Formatted());
                // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
                //             TO IDSV0001-DESC-ERRORE-ESTESA
                //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
            }
        }
        else {
            //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
            // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
            //             TO IDSV0001-LABEL-ERR
            areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
            // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 'IEAS9900'
            //             TO IDSV0001-COD-SERVIZIO-BE
            areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
            // COB_CODE: MOVE 'OVERFLOW IMPAGINAZIONE ERRORI'
            //             TO IDSV0001-DESC-ERRORE-ESTESA
            areaIdsv0001.getLogErrore().setDescErroreEstesa("OVERFLOW IMPAGINAZIONE ERRORI");
        }
    }

    /**Original name: S0300-IMPOSTA-ERRORE<br>
	 * <pre>  se la gravit— di tutti gli errori dell'elaborazione
	 *   h minore di zero ( ad esempio -3) vuol dire che il return-code
	 *   dell'elaborazione complessiva deve essere abbassato da '08' a
	 *   '04'. Per fare cir utilizzo il flag IDSV0001-FORZ-RC-04
	 * IMPOSTO LA GRAVITA'</pre>*/
    private void s0300ImpostaErrore() {
        // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
        // COB_CODE: EVALUATE TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = -3
        //                       IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        //             WHEN  IEAO9901-LIV-GRAVITA = 0 OR 1 OR 2
        //                   SET IDSV0001-FORZ-RC-04-NO  TO TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = 3 OR 4
        //                   SET IDSV0001-ESITO-KO       TO TRUE
        //           END-EVALUATE
        if (ws.getIeao9901Area().getLivGravita() == -3) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-YES TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04Yes();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 2  TO
            //               IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
            areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)2));
        }
        else if (ws.getIeao9901Area().getLivGravita() == 0 || ws.getIeao9901Area().getLivGravita() == 1 || ws.getIeao9901Area().getLivGravita() == 2) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
        }
        else if (ws.getIeao9901Area().getLivGravita() == 3 || ws.getIeao9901Area().getLivGravita() == 4) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        }
        // IMPOSTO IL CODICE ERRORE
        // COB_CODE: MOVE IEAI9901-COD-ERRORE
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeai9901Area().getCodErroreFormatted());
        // SE IL LIVELLO DI GRAVITA' RESTITUITO DALLO IAS9900 E' MAGGIORE
        // DI 2 (ERRORE BLOCCANTE)...IMPOSTO L'ESITO E I DATI DI CONTESTO
        //RELATIVI ALL'ERRORE BLOCCANTE
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE
        //             TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR
        //             TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
        //             TO IDSV0001-DESC-ERRORE-ESTESA
        //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
        // COB_CODE: MOVE SPACES          TO IEAI9901-COD-SERVIZIO-BE
        //                                  IEAI9901-LABEL-ERR
        //                                   IEAI9901-PARAMETRI-ERR.
        ws.getIeai9901Area().setCodServizioBe("");
        ws.getIeai9901Area().setLabelErr("");
        ws.getIeai9901Area().setParametriErr("");
        // COB_CODE: MOVE ZEROES          TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErrore(0);
    }

    /**Original name: S0310-ERRORE-FATALE<br>
	 * <pre>IMPOSTO LA GRAVITA' ERRORE</pre>*/
    private void s0310ErroreFatale() {
        // COB_CODE: MOVE  3
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)3));
        // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
        areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        //IMPOSTO IL CODICE ERRORE (ERRORE FISSO)
        // COB_CODE: MOVE 900001
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErrore(900001);
        //IMPOSTO NOME DEL MODULO
        // COB_CODE: MOVE 'IEAS9900'               TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
        //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
        //              TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
        // COB_CODE: MOVE  'ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900'
        //              TO IDSV0001-DESC-ERRORE-ESTESA
        //                 IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
    }

    /**Original name: CALL-DISPATCHER<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DISPATCHER
	 * ----------------------------------------------------------------*
	 * *****************************************************************
	 *    CALL DISPATCHER
	 * *****************************************************************</pre>*/
    private void callDispatcher() {
        Idss0010 idss0010 = null;
        // COB_CODE: MOVE IDSV0001-MODALITA-ESECUTIVA
        //                              TO IDSI0011-MODALITA-ESECUTIVA
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011ModalitaEsecutiva().setIdsi0011ModalitaEsecutiva(areaIdsv0001.getAreaComune().getModalitaEsecutiva().getModalitaEsecutiva());
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //                              TO IDSI0011-CODICE-COMPAGNIA-ANIA
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceCompagniaAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: MOVE IDSV0001-COD-MAIN-BATCH
        //                              TO IDSI0011-COD-MAIN-BATCH
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodMainBatch(areaIdsv0001.getAreaComune().getCodMainBatch());
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO
        //                              TO IDSI0011-TIPO-MOVIMENTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011TipoMovimentoFormatted(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimentoFormatted());
        // COB_CODE: MOVE IDSV0001-SESSIONE
        //                              TO IDSI0011-SESSIONE
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011Sessione(areaIdsv0001.getAreaComune().getSessione());
        // COB_CODE: MOVE IDSV0001-USER-NAME
        //                              TO IDSI0011-USER-NAME
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011UserName(areaIdsv0001.getAreaComune().getUserName());
        // COB_CODE: IF IDSI0011-DATA-INIZIO-EFFETTO = 0
        //              END-IF
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataInizioEffetto() == 0) {
            // COB_CODE: IF IDSV0001-LETT-ULT-IMMAGINE-SI
            //              END-IF
            //           ELSE
            //                TO IDSI0011-DATA-INIZIO-EFFETTO
            //           END-IF
            if (areaIdsv0001.getAreaComune().getLettUltImmagine().isIdsv0001LettUltImmagineSi()) {
                // COB_CODE: IF IDSI0011-SELECT
                //           OR IDSI0011-FETCH-FIRST
                //           OR IDSI0011-FETCH-NEXT
                //                TO IDSI0011-DATA-COMPETENZA
                //           ELSE
                //                TO IDSI0011-DATA-INIZIO-EFFETTO
                //           END-IF
                if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isSelect() || ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchFirst() || ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchNext()) {
                    // COB_CODE: MOVE 99991230
                    //             TO IDSI0011-DATA-INIZIO-EFFETTO
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(99991230);
                    // COB_CODE: MOVE 999912304023595999
                    //             TO IDSI0011-DATA-COMPETENZA
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(999912304023595999L);
                }
                else {
                    // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
                    //             TO IDSI0011-DATA-INIZIO-EFFETTO
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
                }
            }
            else {
                // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
                //             TO IDSI0011-DATA-INIZIO-EFFETTO
                ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
            }
        }
        // COB_CODE: IF IDSI0011-DATA-FINE-EFFETTO = 0
        //              MOVE 99991231   TO IDSI0011-DATA-FINE-EFFETTO
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataFineEffetto() == 0) {
            // COB_CODE: MOVE 99991231   TO IDSI0011-DATA-FINE-EFFETTO
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(99991231);
        }
        // COB_CODE: IF IDSI0011-DATA-COMPETENZA = 0
        //                              TO IDSI0011-DATA-COMPETENZA
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataCompetenza() == 0) {
            // COB_CODE: MOVE IDSV0001-DATA-COMPETENZA
            //                           TO IDSI0011-DATA-COMPETENZA
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(areaIdsv0001.getAreaComune().getIdsv0001DataCompetenza());
        }
        // COB_CODE: MOVE IDSV0001-DATA-COMP-AGG-STOR
        //                              TO IDSI0011-DATA-COMP-AGG-STOR
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompAggStor(areaIdsv0001.getAreaComune().getIdsv0001DataCompAggStor());
        // COB_CODE: IF IDSI0011-TRATT-DEFAULT
        //                              TO IDSI0011-TRATTAMENTO-STORICITA
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().isTrattDefault()) {
            // COB_CODE: MOVE IDSV0001-TRATTAMENTO-STORICITA
            //                           TO IDSI0011-TRATTAMENTO-STORICITA
            ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattamentoStoricita(areaIdsv0001.getAreaComune().getTrattamentoStoricita().getTrattamentoStoricita());
        }
        // COB_CODE: MOVE IDSV0001-FORMATO-DATA-DB
        //                              TO IDSI0011-FORMATO-DATA-DB
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011FormatoDataDb().setIdsi0011FormatoDataDb(areaIdsv0001.getAreaComune().getFormatoDataDb().getFormatoDataDb());
        // COB_CODE: MOVE IDSV0001-LIVELLO-DEBUG
        //                              TO IDSI0011-LIVELLO-DEBUG
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloDebug().setIdsi0011LivelloDebugFormatted(areaIdsv0001.getAreaComune().getLivelloDebug().getIdsi0011LivelloDebugFormatted());
        // COB_CODE: MOVE 0             TO IDSI0011-ID-MOVI-ANNULLATO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011IdMoviAnnullato(0);
        // COB_CODE: SET IDSI0011-PTF-NEWLIFE          TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011IdentitaChiamante().setPtfNewlife();
        // COB_CODE: SET IDSV0012-STATUS-SHARED-MEM-KO TO TRUE
        ws.getIdsv00122().getStatusSharedMemory().setKo();
        // COB_CODE: MOVE 'IDSS0010'             TO IDSI0011-PGM
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011Pgm("IDSS0010");
        //     SET WS-ADDRESS-DIS TO ADDRESS OF IN-OUT-IDSS0010.
        // COB_CODE: CALL IDSI0011-PGM           USING IDSI0011-AREA
        //                                             IDSO0011-AREA.
        idss0010 = Idss0010.getInstance();
        idss0010.run(ws.getDispatcherVariables(), ws.getDispatcherVariables().getIdso0011Area());
        // COB_CODE: MOVE IDSO0011-SQLCODE-SIGNED
        //                                  TO IDSO0011-SQLCODE.
        ws.getDispatcherVariables().getIdso0011Area().setSqlcode(ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned());
    }

    public void initDettTitDiRat() {
        ws.getDettTitDiRat().setDtrIdDettTitDiRat(0);
        ws.getDettTitDiRat().setDtrIdTitRat(0);
        ws.getDettTitDiRat().setDtrIdOgg(0);
        ws.getDettTitDiRat().setDtrTpOgg("");
        ws.getDettTitDiRat().setDtrIdMoviCrz(0);
        ws.getDettTitDiRat().getDtrIdMoviChiu().setDtrIdMoviChiu(0);
        ws.getDettTitDiRat().setDtrDtIniEff(0);
        ws.getDettTitDiRat().setDtrDtEndEff(0);
        ws.getDettTitDiRat().setDtrCodCompAnia(0);
        ws.getDettTitDiRat().getDtrDtIniCop().setDtrDtIniCop(0);
        ws.getDettTitDiRat().getDtrDtEndCop().setDtrDtEndCop(0);
        ws.getDettTitDiRat().getDtrPreNet().setDtrPreNet(new AfDecimal(0, 15, 3));
        ws.getDettTitDiRat().getDtrIntrFraz().setDtrIntrFraz(new AfDecimal(0, 15, 3));
        ws.getDettTitDiRat().getDtrIntrMora().setDtrIntrMora(new AfDecimal(0, 15, 3));
        ws.getDettTitDiRat().getDtrIntrRetdt().setDtrIntrRetdt(new AfDecimal(0, 15, 3));
        ws.getDettTitDiRat().getDtrIntrRiat().setDtrIntrRiat(new AfDecimal(0, 15, 3));
        ws.getDettTitDiRat().getDtrDir().setDtrDir(new AfDecimal(0, 15, 3));
        ws.getDettTitDiRat().getDtrSpeMed().setDtrSpeMed(new AfDecimal(0, 15, 3));
        ws.getDettTitDiRat().getDtrSpeAge().setDtrSpeAge(new AfDecimal(0, 15, 3));
        ws.getDettTitDiRat().getDtrTax().setDtrTax(new AfDecimal(0, 15, 3));
        ws.getDettTitDiRat().getDtrSoprSan().setDtrSoprSan(new AfDecimal(0, 15, 3));
        ws.getDettTitDiRat().getDtrSoprSpo().setDtrSoprSpo(new AfDecimal(0, 15, 3));
        ws.getDettTitDiRat().getDtrSoprTec().setDtrSoprTec(new AfDecimal(0, 15, 3));
        ws.getDettTitDiRat().getDtrSoprProf().setDtrSoprProf(new AfDecimal(0, 15, 3));
        ws.getDettTitDiRat().getDtrSoprAlt().setDtrSoprAlt(new AfDecimal(0, 15, 3));
        ws.getDettTitDiRat().getDtrPreTot().setDtrPreTot(new AfDecimal(0, 15, 3));
        ws.getDettTitDiRat().getDtrPrePpIas().setDtrPrePpIas(new AfDecimal(0, 15, 3));
        ws.getDettTitDiRat().getDtrPreSoloRsh().setDtrPreSoloRsh(new AfDecimal(0, 15, 3));
        ws.getDettTitDiRat().getDtrCarIas().setDtrCarIas(new AfDecimal(0, 15, 3));
        ws.getDettTitDiRat().getDtrProvAcq1aa().setDtrProvAcq1aa(new AfDecimal(0, 15, 3));
        ws.getDettTitDiRat().getDtrProvAcq2aa().setDtrProvAcq2aa(new AfDecimal(0, 15, 3));
        ws.getDettTitDiRat().getDtrProvRicor().setDtrProvRicor(new AfDecimal(0, 15, 3));
        ws.getDettTitDiRat().getDtrProvInc().setDtrProvInc(new AfDecimal(0, 15, 3));
        ws.getDettTitDiRat().getDtrProvDaRec().setDtrProvDaRec(new AfDecimal(0, 15, 3));
        ws.getDettTitDiRat().setDtrCodDvs("");
        ws.getDettTitDiRat().getDtrFrqMovi().setDtrFrqMovi(0);
        ws.getDettTitDiRat().setDtrTpRgmFisc("");
        ws.getDettTitDiRat().setDtrCodTari("");
        ws.getDettTitDiRat().setDtrTpStatTit("");
        ws.getDettTitDiRat().getDtrImpAz().setDtrImpAz(new AfDecimal(0, 15, 3));
        ws.getDettTitDiRat().getDtrImpAder().setDtrImpAder(new AfDecimal(0, 15, 3));
        ws.getDettTitDiRat().getDtrImpTfr().setDtrImpTfr(new AfDecimal(0, 15, 3));
        ws.getDettTitDiRat().getDtrImpVolo().setDtrImpVolo(new AfDecimal(0, 15, 3));
        ws.getDettTitDiRat().setDtrFlVldtTit(Types.SPACE_CHAR);
        ws.getDettTitDiRat().getDtrCarAcq().setDtrCarAcq(new AfDecimal(0, 15, 3));
        ws.getDettTitDiRat().getDtrCarGest().setDtrCarGest(new AfDecimal(0, 15, 3));
        ws.getDettTitDiRat().getDtrCarInc().setDtrCarInc(new AfDecimal(0, 15, 3));
        ws.getDettTitDiRat().getDtrManfeeAntic().setDtrManfeeAntic(new AfDecimal(0, 15, 3));
        ws.getDettTitDiRat().getDtrManfeeRicor().setDtrManfeeRicor(new AfDecimal(0, 15, 3));
        ws.getDettTitDiRat().setDtrManfeeRec("");
        ws.getDettTitDiRat().getDtrTotIntrPrest().setDtrTotIntrPrest(new AfDecimal(0, 15, 3));
        ws.getDettTitDiRat().setDtrDsRiga(0);
        ws.getDettTitDiRat().setDtrDsOperSql(Types.SPACE_CHAR);
        ws.getDettTitDiRat().setDtrDsVer(0);
        ws.getDettTitDiRat().setDtrDsTsIniCptz(0);
        ws.getDettTitDiRat().setDtrDsTsEndCptz(0);
        ws.getDettTitDiRat().setDtrDsUtente("");
        ws.getDettTitDiRat().setDtrDsStatoElab(Types.SPACE_CHAR);
        ws.getDettTitDiRat().getDtrImpTrasfe().setDtrImpTrasfe(new AfDecimal(0, 15, 3));
        ws.getDettTitDiRat().getDtrImpTfrStrc().setDtrImpTfrStrc(new AfDecimal(0, 15, 3));
        ws.getDettTitDiRat().getDtrAcqExp().setDtrAcqExp(new AfDecimal(0, 15, 3));
        ws.getDettTitDiRat().getDtrRemunAss().setDtrRemunAss(new AfDecimal(0, 15, 3));
        ws.getDettTitDiRat().getDtrCommisInter().setDtrCommisInter(new AfDecimal(0, 15, 3));
        ws.getDettTitDiRat().getDtrCnbtAntirac().setDtrCnbtAntirac(new AfDecimal(0, 15, 3));
    }

    public void initTrchDiGar() {
        ws.getTrchDiGar().setTgaIdTrchDiGar(0);
        ws.getTrchDiGar().setTgaIdGar(0);
        ws.getTrchDiGar().setTgaIdAdes(0);
        ws.getTrchDiGar().setTgaIdPoli(0);
        ws.getTrchDiGar().setTgaIdMoviCrz(0);
        ws.getTrchDiGar().getTgaIdMoviChiu().setTgaIdMoviChiu(0);
        ws.getTrchDiGar().setTgaDtIniEff(0);
        ws.getTrchDiGar().setTgaDtEndEff(0);
        ws.getTrchDiGar().setTgaCodCompAnia(0);
        ws.getTrchDiGar().setTgaDtDecor(0);
        ws.getTrchDiGar().getTgaDtScad().setTgaDtScad(0);
        ws.getTrchDiGar().setTgaIbOgg("");
        ws.getTrchDiGar().setTgaTpRgmFisc("");
        ws.getTrchDiGar().getTgaDtEmis().setTgaDtEmis(0);
        ws.getTrchDiGar().setTgaTpTrch("");
        ws.getTrchDiGar().getTgaDurAa().setTgaDurAa(0);
        ws.getTrchDiGar().getTgaDurMm().setTgaDurMm(0);
        ws.getTrchDiGar().getTgaDurGg().setTgaDurGg(0);
        ws.getTrchDiGar().getTgaPreCasoMor().setTgaPreCasoMor(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaPcIntrRiat().setTgaPcIntrRiat(new AfDecimal(0, 6, 3));
        ws.getTrchDiGar().getTgaImpBnsAntic().setTgaImpBnsAntic(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaPreIniNet().setTgaPreIniNet(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaPrePpIni().setTgaPrePpIni(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaPrePpUlt().setTgaPrePpUlt(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaPreTariIni().setTgaPreTariIni(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaPreTariUlt().setTgaPreTariUlt(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaPreInvrioIni().setTgaPreInvrioIni(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaPreInvrioUlt().setTgaPreInvrioUlt(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaPreRivto().setTgaPreRivto(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaImpSoprProf().setTgaImpSoprProf(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaImpSoprSan().setTgaImpSoprSan(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaImpSoprSpo().setTgaImpSoprSpo(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaImpSoprTec().setTgaImpSoprTec(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaImpAltSopr().setTgaImpAltSopr(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaPreStab().setTgaPreStab(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaDtEffStab().setTgaDtEffStab(0);
        ws.getTrchDiGar().getTgaTsRivalFis().setTgaTsRivalFis(new AfDecimal(0, 14, 9));
        ws.getTrchDiGar().getTgaTsRivalIndiciz().setTgaTsRivalIndiciz(new AfDecimal(0, 14, 9));
        ws.getTrchDiGar().getTgaOldTsTec().setTgaOldTsTec(new AfDecimal(0, 14, 9));
        ws.getTrchDiGar().getTgaRatLrd().setTgaRatLrd(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaPreLrd().setTgaPreLrd(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaPrstzIni().setTgaPrstzIni(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaPrstzUlt().setTgaPrstzUlt(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaCptInOpzRivto().setTgaCptInOpzRivto(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaPrstzIniStab().setTgaPrstzIniStab(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaCptRshMor().setTgaCptRshMor(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaPrstzRidIni().setTgaPrstzRidIni(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().setTgaFlCarCont(Types.SPACE_CHAR);
        ws.getTrchDiGar().getTgaBnsGiaLiqto().setTgaBnsGiaLiqto(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaImpBns().setTgaImpBns(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().setTgaCodDvs("");
        ws.getTrchDiGar().getTgaPrstzIniNewfis().setTgaPrstzIniNewfis(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaImpScon().setTgaImpScon(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaAlqScon().setTgaAlqScon(new AfDecimal(0, 6, 3));
        ws.getTrchDiGar().getTgaImpCarAcq().setTgaImpCarAcq(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaImpCarInc().setTgaImpCarInc(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaImpCarGest().setTgaImpCarGest(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaEtaAa1oAssto().setTgaEtaAa1oAssto(((short)0));
        ws.getTrchDiGar().getTgaEtaMm1oAssto().setTgaEtaMm1oAssto(((short)0));
        ws.getTrchDiGar().getTgaEtaAa2oAssto().setTgaEtaAa2oAssto(((short)0));
        ws.getTrchDiGar().getTgaEtaMm2oAssto().setTgaEtaMm2oAssto(((short)0));
        ws.getTrchDiGar().getTgaEtaAa3oAssto().setTgaEtaAa3oAssto(((short)0));
        ws.getTrchDiGar().getTgaEtaMm3oAssto().setTgaEtaMm3oAssto(((short)0));
        ws.getTrchDiGar().getTgaRendtoLrd().setTgaRendtoLrd(new AfDecimal(0, 14, 9));
        ws.getTrchDiGar().getTgaPcRetr().setTgaPcRetr(new AfDecimal(0, 6, 3));
        ws.getTrchDiGar().getTgaRendtoRetr().setTgaRendtoRetr(new AfDecimal(0, 14, 9));
        ws.getTrchDiGar().getTgaMinGarto().setTgaMinGarto(new AfDecimal(0, 14, 9));
        ws.getTrchDiGar().getTgaMinTrnut().setTgaMinTrnut(new AfDecimal(0, 14, 9));
        ws.getTrchDiGar().getTgaPreAttDiTrch().setTgaPreAttDiTrch(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaMatuEnd2000().setTgaMatuEnd2000(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaAbbTotIni().setTgaAbbTotIni(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaAbbTotUlt().setTgaAbbTotUlt(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaAbbAnnuUlt().setTgaAbbAnnuUlt(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaDurAbb().setTgaDurAbb(0);
        ws.getTrchDiGar().setTgaTpAdegAbb(Types.SPACE_CHAR);
        ws.getTrchDiGar().setTgaModCalc("");
        ws.getTrchDiGar().getTgaImpAz().setTgaImpAz(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaImpAder().setTgaImpAder(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaImpTfr().setTgaImpTfr(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaImpVolo().setTgaImpVolo(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaVisEnd2000().setTgaVisEnd2000(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaDtVldtProd().setTgaDtVldtProd(0);
        ws.getTrchDiGar().getTgaDtIniValTar().setTgaDtIniValTar(0);
        ws.getTrchDiGar().getTgaImpbVisEnd2000().setTgaImpbVisEnd2000(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaRenIniTsTec0().setTgaRenIniTsTec0(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaPcRipPre().setTgaPcRipPre(new AfDecimal(0, 6, 3));
        ws.getTrchDiGar().setTgaFlImportiForz(Types.SPACE_CHAR);
        ws.getTrchDiGar().getTgaPrstzIniNforz().setTgaPrstzIniNforz(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaVisEnd2000Nforz().setTgaVisEnd2000Nforz(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaIntrMora().setTgaIntrMora(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaManfeeAntic().setTgaManfeeAntic(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaManfeeRicor().setTgaManfeeRicor(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaPreUniRivto().setTgaPreUniRivto(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaProv1aaAcq().setTgaProv1aaAcq(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaProv2aaAcq().setTgaProv2aaAcq(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaProvRicor().setTgaProvRicor(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaProvInc().setTgaProvInc(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaAlqProvAcq().setTgaAlqProvAcq(new AfDecimal(0, 6, 3));
        ws.getTrchDiGar().getTgaAlqProvInc().setTgaAlqProvInc(new AfDecimal(0, 6, 3));
        ws.getTrchDiGar().getTgaAlqProvRicor().setTgaAlqProvRicor(new AfDecimal(0, 6, 3));
        ws.getTrchDiGar().getTgaImpbProvAcq().setTgaImpbProvAcq(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaImpbProvInc().setTgaImpbProvInc(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaImpbProvRicor().setTgaImpbProvRicor(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().setTgaFlProvForz(Types.SPACE_CHAR);
        ws.getTrchDiGar().getTgaPrstzAggIni().setTgaPrstzAggIni(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaIncrPre().setTgaIncrPre(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaIncrPrstz().setTgaIncrPrstz(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaDtUltAdegPrePr().setTgaDtUltAdegPrePr(0);
        ws.getTrchDiGar().getTgaPrstzAggUlt().setTgaPrstzAggUlt(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaTsRivalNet().setTgaTsRivalNet(new AfDecimal(0, 14, 9));
        ws.getTrchDiGar().getTgaPrePattuito().setTgaPrePattuito(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().setTgaTpRival("");
        ws.getTrchDiGar().getTgaRisMat().setTgaRisMat(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaCptMinScad().setTgaCptMinScad(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaCommisGest().setTgaCommisGest(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().setTgaTpManfeeAppl("");
        ws.getTrchDiGar().setTgaDsRiga(0);
        ws.getTrchDiGar().setTgaDsOperSql(Types.SPACE_CHAR);
        ws.getTrchDiGar().setTgaDsVer(0);
        ws.getTrchDiGar().setTgaDsTsIniCptz(0);
        ws.getTrchDiGar().setTgaDsTsEndCptz(0);
        ws.getTrchDiGar().setTgaDsUtente("");
        ws.getTrchDiGar().setTgaDsStatoElab(Types.SPACE_CHAR);
        ws.getTrchDiGar().getTgaPcCommisGest().setTgaPcCommisGest(new AfDecimal(0, 6, 3));
        ws.getTrchDiGar().getTgaNumGgRival().setTgaNumGgRival(0);
        ws.getTrchDiGar().getTgaImpTrasfe().setTgaImpTrasfe(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaImpTfrStrc().setTgaImpTfrStrc(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaAcqExp().setTgaAcqExp(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaRemunAss().setTgaRemunAss(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaCommisInter().setTgaCommisInter(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaAlqRemunAss().setTgaAlqRemunAss(new AfDecimal(0, 6, 3));
        ws.getTrchDiGar().getTgaAlqCommisInter().setTgaAlqCommisInter(new AfDecimal(0, 6, 3));
        ws.getTrchDiGar().getTgaImpbRemunAss().setTgaImpbRemunAss(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaImpbCommisInter().setTgaImpbCommisInter(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaCosRunAssva().setTgaCosRunAssva(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaCosRunAssvaIdc().setTgaCosRunAssvaIdc(new AfDecimal(0, 15, 3));
    }

    public void initTitRat() {
        ws.getTitRat().setTdrIdTitRat(0);
        ws.getTitRat().setTdrIdOgg(0);
        ws.getTitRat().setTdrTpOgg("");
        ws.getTitRat().setTdrIdMoviCrz(0);
        ws.getTitRat().getTdrIdMoviChiu().setTdrIdMoviChiu(0);
        ws.getTitRat().setTdrDtIniEff(0);
        ws.getTitRat().setTdrDtEndEff(0);
        ws.getTitRat().setTdrCodCompAnia(0);
        ws.getTitRat().setTdrTpTit("");
        ws.getTitRat().getTdrProgTit().setTdrProgTit(0);
        ws.getTitRat().setTdrTpPreTit("");
        ws.getTitRat().setTdrTpStatTit("");
        ws.getTitRat().getTdrDtIniCop().setTdrDtIniCop(0);
        ws.getTitRat().getTdrDtEndCop().setTdrDtEndCop(0);
        ws.getTitRat().getTdrImpPag().setTdrImpPag(new AfDecimal(0, 15, 3));
        ws.getTitRat().setTdrFlSoll(Types.SPACE_CHAR);
        ws.getTitRat().getTdrFraz().setTdrFraz(0);
        ws.getTitRat().getTdrDtApplzMora().setTdrDtApplzMora(0);
        ws.getTitRat().setTdrFlMora(Types.SPACE_CHAR);
        ws.getTitRat().getTdrIdRappRete().setTdrIdRappRete(0);
        ws.getTitRat().getTdrIdRappAna().setTdrIdRappAna(0);
        ws.getTitRat().setTdrCodDvs("");
        ws.getTitRat().setTdrDtEmisTit(0);
        ws.getTitRat().getTdrDtEsiTit().setTdrDtEsiTit(0);
        ws.getTitRat().getTdrTotPreNet().setTdrTotPreNet(new AfDecimal(0, 15, 3));
        ws.getTitRat().getTdrTotIntrFraz().setTdrTotIntrFraz(new AfDecimal(0, 15, 3));
        ws.getTitRat().getTdrTotIntrMora().setTdrTotIntrMora(new AfDecimal(0, 15, 3));
        ws.getTitRat().getTdrTotIntrPrest().setTdrTotIntrPrest(new AfDecimal(0, 15, 3));
        ws.getTitRat().getTdrTotIntrRetdt().setTdrTotIntrRetdt(new AfDecimal(0, 15, 3));
        ws.getTitRat().getTdrTotIntrRiat().setTdrTotIntrRiat(new AfDecimal(0, 15, 3));
        ws.getTitRat().getTdrTotDir().setTdrTotDir(new AfDecimal(0, 15, 3));
        ws.getTitRat().getTdrTotSpeMed().setTdrTotSpeMed(new AfDecimal(0, 15, 3));
        ws.getTitRat().getTdrTotSpeAge().setTdrTotSpeAge(new AfDecimal(0, 15, 3));
        ws.getTitRat().getTdrTotTax().setTdrTotTax(new AfDecimal(0, 15, 3));
        ws.getTitRat().getTdrTotSoprSan().setTdrTotSoprSan(new AfDecimal(0, 15, 3));
        ws.getTitRat().getTdrTotSoprTec().setTdrTotSoprTec(new AfDecimal(0, 15, 3));
        ws.getTitRat().getTdrTotSoprSpo().setTdrTotSoprSpo(new AfDecimal(0, 15, 3));
        ws.getTitRat().getTdrTotSoprProf().setTdrTotSoprProf(new AfDecimal(0, 15, 3));
        ws.getTitRat().getTdrTotSoprAlt().setTdrTotSoprAlt(new AfDecimal(0, 15, 3));
        ws.getTitRat().getTdrTotPreTot().setTdrTotPreTot(new AfDecimal(0, 15, 3));
        ws.getTitRat().getTdrTotPrePpIas().setTdrTotPrePpIas(new AfDecimal(0, 15, 3));
        ws.getTitRat().getTdrTotCarIas().setTdrTotCarIas(new AfDecimal(0, 15, 3));
        ws.getTitRat().getTdrTotPreSoloRsh().setTdrTotPreSoloRsh(new AfDecimal(0, 15, 3));
        ws.getTitRat().getTdrTotProvAcq1aa().setTdrTotProvAcq1aa(new AfDecimal(0, 15, 3));
        ws.getTitRat().getTdrTotProvAcq2aa().setTdrTotProvAcq2aa(new AfDecimal(0, 15, 3));
        ws.getTitRat().getTdrTotProvRicor().setTdrTotProvRicor(new AfDecimal(0, 15, 3));
        ws.getTitRat().getTdrTotProvInc().setTdrTotProvInc(new AfDecimal(0, 15, 3));
        ws.getTitRat().getTdrTotProvDaRec().setTdrTotProvDaRec(new AfDecimal(0, 15, 3));
        ws.getTitRat().getTdrImpAz().setTdrImpAz(new AfDecimal(0, 15, 3));
        ws.getTitRat().getTdrImpAder().setTdrImpAder(new AfDecimal(0, 15, 3));
        ws.getTitRat().getTdrImpTfr().setTdrImpTfr(new AfDecimal(0, 15, 3));
        ws.getTitRat().getTdrImpVolo().setTdrImpVolo(new AfDecimal(0, 15, 3));
        ws.getTitRat().setTdrFlVldtTit(Types.SPACE_CHAR);
        ws.getTitRat().getTdrTotCarAcq().setTdrTotCarAcq(new AfDecimal(0, 15, 3));
        ws.getTitRat().getTdrTotCarGest().setTdrTotCarGest(new AfDecimal(0, 15, 3));
        ws.getTitRat().getTdrTotCarInc().setTdrTotCarInc(new AfDecimal(0, 15, 3));
        ws.getTitRat().getTdrTotManfeeAntic().setTdrTotManfeeAntic(new AfDecimal(0, 15, 3));
        ws.getTitRat().getTdrTotManfeeRicor().setTdrTotManfeeRicor(new AfDecimal(0, 15, 3));
        ws.getTitRat().getTdrTotManfeeRec().setTdrTotManfeeRec(new AfDecimal(0, 15, 3));
        ws.getTitRat().setTdrDsRiga(0);
        ws.getTitRat().setTdrDsOperSql(Types.SPACE_CHAR);
        ws.getTitRat().setTdrDsVer(0);
        ws.getTitRat().setTdrDsTsIniCptz(0);
        ws.getTitRat().setTdrDsTsEndCptz(0);
        ws.getTitRat().setTdrDsUtente("");
        ws.getTitRat().setTdrDsStatoElab(Types.SPACE_CHAR);
        ws.getTitRat().getTdrImpTrasfe().setTdrImpTrasfe(new AfDecimal(0, 15, 3));
        ws.getTitRat().getTdrImpTfrStrc().setTdrImpTfrStrc(new AfDecimal(0, 15, 3));
        ws.getTitRat().getTdrTotAcqExp().setTdrTotAcqExp(new AfDecimal(0, 15, 3));
        ws.getTitRat().getTdrTotRemunAss().setTdrTotRemunAss(new AfDecimal(0, 15, 3));
        ws.getTitRat().getTdrTotCommisInter().setTdrTotCommisInter(new AfDecimal(0, 15, 3));
        ws.getTitRat().getTdrTotCnbtAntirac().setTdrTotCnbtAntirac(new AfDecimal(0, 15, 3));
        ws.getTitRat().setTdrFlIncAutogen(Types.SPACE_CHAR);
    }
}
