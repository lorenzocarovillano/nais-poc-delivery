package it.accenture.jnais;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.FieldNotMappedException;
import it.accenture.jnais.commons.data.to.IAdesPoli;

/**Original name: AdesPoliLdbm0250<br>*/
public class AdesPoliLdbm0250 implements IAdesPoli {

    //==== PROPERTIES ====
    private Ldbm0250 ldbm0250;

    //==== CONSTRUCTORS ====
    public AdesPoliLdbm0250(Ldbm0250 ldbm0250) {
        this.ldbm0250 = ldbm0250;
    }

    //==== METHODS ====
    @Override
    public int getAdeCodCompAnia() {
        return ldbm0250.getAdes().getAdeCodCompAnia();
    }

    @Override
    public void setAdeCodCompAnia(int adeCodCompAnia) {
        ldbm0250.getAdes().setAdeCodCompAnia(adeCodCompAnia);
    }

    @Override
    public char getAdeConcsPrest() {
        return ldbm0250.getAdes().getAdeConcsPrest();
    }

    @Override
    public void setAdeConcsPrest(char adeConcsPrest) {
        ldbm0250.getAdes().setAdeConcsPrest(adeConcsPrest);
    }

    @Override
    public Character getAdeConcsPrestObj() {
        if (ldbm0250.getWs().getIndAdes().getConcsPrest() >= 0) {
            return ((Character)getAdeConcsPrest());
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeConcsPrestObj(Character adeConcsPrestObj) {
        if (adeConcsPrestObj != null) {
            setAdeConcsPrest(((char)adeConcsPrestObj));
            ldbm0250.getWs().getIndAdes().setConcsPrest(((short)0));
        }
        else {
            ldbm0250.getWs().getIndAdes().setConcsPrest(((short)-1));
        }
    }

    @Override
    public AfDecimal getAdeCumCnbtCap() {
        return ldbm0250.getAdes().getAdeCumCnbtCap().getAdeCumCnbtCap();
    }

    @Override
    public void setAdeCumCnbtCap(AfDecimal adeCumCnbtCap) {
        ldbm0250.getAdes().getAdeCumCnbtCap().setAdeCumCnbtCap(adeCumCnbtCap.copy());
    }

    @Override
    public AfDecimal getAdeCumCnbtCapObj() {
        if (ldbm0250.getWs().getIndAdes().getCumCnbtCap() >= 0) {
            return getAdeCumCnbtCap();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeCumCnbtCapObj(AfDecimal adeCumCnbtCapObj) {
        if (adeCumCnbtCapObj != null) {
            setAdeCumCnbtCap(new AfDecimal(adeCumCnbtCapObj, 15, 3));
            ldbm0250.getWs().getIndAdes().setCumCnbtCap(((short)0));
        }
        else {
            ldbm0250.getWs().getIndAdes().setCumCnbtCap(((short)-1));
        }
    }

    @Override
    public char getAdeDsOperSql() {
        return ldbm0250.getAdes().getAdeDsOperSql();
    }

    @Override
    public void setAdeDsOperSql(char adeDsOperSql) {
        ldbm0250.getAdes().setAdeDsOperSql(adeDsOperSql);
    }

    @Override
    public long getAdeDsRiga() {
        return ldbm0250.getAdes().getAdeDsRiga();
    }

    @Override
    public void setAdeDsRiga(long adeDsRiga) {
        ldbm0250.getAdes().setAdeDsRiga(adeDsRiga);
    }

    @Override
    public char getAdeDsStatoElab() {
        return ldbm0250.getAdes().getAdeDsStatoElab();
    }

    @Override
    public void setAdeDsStatoElab(char adeDsStatoElab) {
        ldbm0250.getAdes().setAdeDsStatoElab(adeDsStatoElab);
    }

    @Override
    public long getAdeDsTsEndCptz() {
        return ldbm0250.getAdes().getAdeDsTsEndCptz();
    }

    @Override
    public void setAdeDsTsEndCptz(long adeDsTsEndCptz) {
        ldbm0250.getAdes().setAdeDsTsEndCptz(adeDsTsEndCptz);
    }

    @Override
    public long getAdeDsTsIniCptz() {
        return ldbm0250.getAdes().getAdeDsTsIniCptz();
    }

    @Override
    public void setAdeDsTsIniCptz(long adeDsTsIniCptz) {
        ldbm0250.getAdes().setAdeDsTsIniCptz(adeDsTsIniCptz);
    }

    @Override
    public String getAdeDsUtente() {
        return ldbm0250.getAdes().getAdeDsUtente();
    }

    @Override
    public void setAdeDsUtente(String adeDsUtente) {
        ldbm0250.getAdes().setAdeDsUtente(adeDsUtente);
    }

    @Override
    public int getAdeDsVer() {
        return ldbm0250.getAdes().getAdeDsVer();
    }

    @Override
    public void setAdeDsVer(int adeDsVer) {
        ldbm0250.getAdes().setAdeDsVer(adeDsVer);
    }

    @Override
    public String getAdeDtDecorDb() {
        return ldbm0250.getWs().getAdesDb().getDecorDb();
    }

    @Override
    public void setAdeDtDecorDb(String adeDtDecorDb) {
        ldbm0250.getWs().getAdesDb().setDecorDb(adeDtDecorDb);
    }

    @Override
    public String getAdeDtDecorDbObj() {
        if (ldbm0250.getWs().getIndAdes().getDtDecor() >= 0) {
            return getAdeDtDecorDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeDtDecorDbObj(String adeDtDecorDbObj) {
        if (adeDtDecorDbObj != null) {
            setAdeDtDecorDb(adeDtDecorDbObj);
            ldbm0250.getWs().getIndAdes().setDtDecor(((short)0));
        }
        else {
            ldbm0250.getWs().getIndAdes().setDtDecor(((short)-1));
        }
    }

    @Override
    public String getAdeDtDecorPrestBanDb() {
        return ldbm0250.getWs().getAdesDb().getDecorPrestBanDb();
    }

    @Override
    public void setAdeDtDecorPrestBanDb(String adeDtDecorPrestBanDb) {
        ldbm0250.getWs().getAdesDb().setDecorPrestBanDb(adeDtDecorPrestBanDb);
    }

    @Override
    public String getAdeDtDecorPrestBanDbObj() {
        if (ldbm0250.getWs().getIndAdes().getDtDecorPrestBan() >= 0) {
            return getAdeDtDecorPrestBanDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeDtDecorPrestBanDbObj(String adeDtDecorPrestBanDbObj) {
        if (adeDtDecorPrestBanDbObj != null) {
            setAdeDtDecorPrestBanDb(adeDtDecorPrestBanDbObj);
            ldbm0250.getWs().getIndAdes().setDtDecorPrestBan(((short)0));
        }
        else {
            ldbm0250.getWs().getIndAdes().setDtDecorPrestBan(((short)-1));
        }
    }

    @Override
    public String getAdeDtEffVarzStatTDb() {
        return ldbm0250.getWs().getAdesDb().getEffVarzStatTDb();
    }

    @Override
    public void setAdeDtEffVarzStatTDb(String adeDtEffVarzStatTDb) {
        ldbm0250.getWs().getAdesDb().setEffVarzStatTDb(adeDtEffVarzStatTDb);
    }

    @Override
    public String getAdeDtEffVarzStatTDbObj() {
        if (ldbm0250.getWs().getIndAdes().getDtEffVarzStatT() >= 0) {
            return getAdeDtEffVarzStatTDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeDtEffVarzStatTDbObj(String adeDtEffVarzStatTDbObj) {
        if (adeDtEffVarzStatTDbObj != null) {
            setAdeDtEffVarzStatTDb(adeDtEffVarzStatTDbObj);
            ldbm0250.getWs().getIndAdes().setDtEffVarzStatT(((short)0));
        }
        else {
            ldbm0250.getWs().getIndAdes().setDtEffVarzStatT(((short)-1));
        }
    }

    @Override
    public String getAdeDtEndEffDb() {
        return ldbm0250.getWs().getAdesDb().getEndEffDb();
    }

    @Override
    public void setAdeDtEndEffDb(String adeDtEndEffDb) {
        ldbm0250.getWs().getAdesDb().setEndEffDb(adeDtEndEffDb);
    }

    @Override
    public String getAdeDtIniEffDb() {
        return ldbm0250.getWs().getAdesDb().getIniEffDb();
    }

    @Override
    public void setAdeDtIniEffDb(String adeDtIniEffDb) {
        ldbm0250.getWs().getAdesDb().setIniEffDb(adeDtIniEffDb);
    }

    @Override
    public String getAdeDtNovaRgmFiscDb() {
        return ldbm0250.getWs().getAdesDb().getNovaRgmFiscDb();
    }

    @Override
    public void setAdeDtNovaRgmFiscDb(String adeDtNovaRgmFiscDb) {
        ldbm0250.getWs().getAdesDb().setNovaRgmFiscDb(adeDtNovaRgmFiscDb);
    }

    @Override
    public String getAdeDtNovaRgmFiscDbObj() {
        if (ldbm0250.getWs().getIndAdes().getDtNovaRgmFisc() >= 0) {
            return getAdeDtNovaRgmFiscDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeDtNovaRgmFiscDbObj(String adeDtNovaRgmFiscDbObj) {
        if (adeDtNovaRgmFiscDbObj != null) {
            setAdeDtNovaRgmFiscDb(adeDtNovaRgmFiscDbObj);
            ldbm0250.getWs().getIndAdes().setDtNovaRgmFisc(((short)0));
        }
        else {
            ldbm0250.getWs().getIndAdes().setDtNovaRgmFisc(((short)-1));
        }
    }

    @Override
    public String getAdeDtPrescDb() {
        return ldbm0250.getWs().getAdesDb().getPrescDb();
    }

    @Override
    public void setAdeDtPrescDb(String adeDtPrescDb) {
        ldbm0250.getWs().getAdesDb().setPrescDb(adeDtPrescDb);
    }

    @Override
    public String getAdeDtPrescDbObj() {
        if (ldbm0250.getWs().getIndAdes().getDtPresc() >= 0) {
            return getAdeDtPrescDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeDtPrescDbObj(String adeDtPrescDbObj) {
        if (adeDtPrescDbObj != null) {
            setAdeDtPrescDb(adeDtPrescDbObj);
            ldbm0250.getWs().getIndAdes().setDtPresc(((short)0));
        }
        else {
            ldbm0250.getWs().getIndAdes().setDtPresc(((short)-1));
        }
    }

    @Override
    public String getAdeDtScadDb() {
        return ldbm0250.getWs().getAdesDb().getScadDb();
    }

    @Override
    public void setAdeDtScadDb(String adeDtScadDb) {
        ldbm0250.getWs().getAdesDb().setScadDb(adeDtScadDb);
    }

    @Override
    public String getAdeDtScadDbObj() {
        if (ldbm0250.getWs().getIndAdes().getDtScad() >= 0) {
            return getAdeDtScadDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeDtScadDbObj(String adeDtScadDbObj) {
        if (adeDtScadDbObj != null) {
            setAdeDtScadDb(adeDtScadDbObj);
            ldbm0250.getWs().getIndAdes().setDtScad(((short)0));
        }
        else {
            ldbm0250.getWs().getIndAdes().setDtScad(((short)-1));
        }
    }

    @Override
    public String getAdeDtUltConsCnbtDb() {
        return ldbm0250.getWs().getAdesDb().getUltConsCnbtDb();
    }

    @Override
    public void setAdeDtUltConsCnbtDb(String adeDtUltConsCnbtDb) {
        ldbm0250.getWs().getAdesDb().setUltConsCnbtDb(adeDtUltConsCnbtDb);
    }

    @Override
    public String getAdeDtUltConsCnbtDbObj() {
        if (ldbm0250.getWs().getIndAdes().getDtUltConsCnbt() >= 0) {
            return getAdeDtUltConsCnbtDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeDtUltConsCnbtDbObj(String adeDtUltConsCnbtDbObj) {
        if (adeDtUltConsCnbtDbObj != null) {
            setAdeDtUltConsCnbtDb(adeDtUltConsCnbtDbObj);
            ldbm0250.getWs().getIndAdes().setDtUltConsCnbt(((short)0));
        }
        else {
            ldbm0250.getWs().getIndAdes().setDtUltConsCnbt(((short)-1));
        }
    }

    @Override
    public String getAdeDtVarzTpIasDb() {
        return ldbm0250.getWs().getAdesDb().getVarzTpIasDb();
    }

    @Override
    public void setAdeDtVarzTpIasDb(String adeDtVarzTpIasDb) {
        ldbm0250.getWs().getAdesDb().setVarzTpIasDb(adeDtVarzTpIasDb);
    }

    @Override
    public String getAdeDtVarzTpIasDbObj() {
        if (ldbm0250.getWs().getIndAdes().getDtVarzTpIas() >= 0) {
            return getAdeDtVarzTpIasDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeDtVarzTpIasDbObj(String adeDtVarzTpIasDbObj) {
        if (adeDtVarzTpIasDbObj != null) {
            setAdeDtVarzTpIasDb(adeDtVarzTpIasDbObj);
            ldbm0250.getWs().getIndAdes().setDtVarzTpIas(((short)0));
        }
        else {
            ldbm0250.getWs().getIndAdes().setDtVarzTpIas(((short)-1));
        }
    }

    @Override
    public int getAdeDurAa() {
        return ldbm0250.getAdes().getAdeDurAa().getAdeDurAa();
    }

    @Override
    public void setAdeDurAa(int adeDurAa) {
        ldbm0250.getAdes().getAdeDurAa().setAdeDurAa(adeDurAa);
    }

    @Override
    public Integer getAdeDurAaObj() {
        if (ldbm0250.getWs().getIndAdes().getDurAa() >= 0) {
            return ((Integer)getAdeDurAa());
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeDurAaObj(Integer adeDurAaObj) {
        if (adeDurAaObj != null) {
            setAdeDurAa(((int)adeDurAaObj));
            ldbm0250.getWs().getIndAdes().setDurAa(((short)0));
        }
        else {
            ldbm0250.getWs().getIndAdes().setDurAa(((short)-1));
        }
    }

    @Override
    public int getAdeDurGg() {
        return ldbm0250.getAdes().getAdeDurGg().getAdeDurGg();
    }

    @Override
    public void setAdeDurGg(int adeDurGg) {
        ldbm0250.getAdes().getAdeDurGg().setAdeDurGg(adeDurGg);
    }

    @Override
    public Integer getAdeDurGgObj() {
        if (ldbm0250.getWs().getIndAdes().getDurGg() >= 0) {
            return ((Integer)getAdeDurGg());
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeDurGgObj(Integer adeDurGgObj) {
        if (adeDurGgObj != null) {
            setAdeDurGg(((int)adeDurGgObj));
            ldbm0250.getWs().getIndAdes().setDurGg(((short)0));
        }
        else {
            ldbm0250.getWs().getIndAdes().setDurGg(((short)-1));
        }
    }

    @Override
    public int getAdeDurMm() {
        return ldbm0250.getAdes().getAdeDurMm().getAdeDurMm();
    }

    @Override
    public void setAdeDurMm(int adeDurMm) {
        ldbm0250.getAdes().getAdeDurMm().setAdeDurMm(adeDurMm);
    }

    @Override
    public Integer getAdeDurMmObj() {
        if (ldbm0250.getWs().getIndAdes().getDurMm() >= 0) {
            return ((Integer)getAdeDurMm());
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeDurMmObj(Integer adeDurMmObj) {
        if (adeDurMmObj != null) {
            setAdeDurMm(((int)adeDurMmObj));
            ldbm0250.getWs().getIndAdes().setDurMm(((short)0));
        }
        else {
            ldbm0250.getWs().getIndAdes().setDurMm(((short)-1));
        }
    }

    @Override
    public int getAdeEtaAScad() {
        return ldbm0250.getAdes().getAdeEtaAScad().getAdeEtaAScad();
    }

    @Override
    public void setAdeEtaAScad(int adeEtaAScad) {
        ldbm0250.getAdes().getAdeEtaAScad().setAdeEtaAScad(adeEtaAScad);
    }

    @Override
    public Integer getAdeEtaAScadObj() {
        if (ldbm0250.getWs().getIndAdes().getEtaAScad() >= 0) {
            return ((Integer)getAdeEtaAScad());
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeEtaAScadObj(Integer adeEtaAScadObj) {
        if (adeEtaAScadObj != null) {
            setAdeEtaAScad(((int)adeEtaAScadObj));
            ldbm0250.getWs().getIndAdes().setEtaAScad(((short)0));
        }
        else {
            ldbm0250.getWs().getIndAdes().setEtaAScad(((short)-1));
        }
    }

    @Override
    public char getAdeFlAttiv() {
        return ldbm0250.getAdes().getAdeFlAttiv();
    }

    @Override
    public void setAdeFlAttiv(char adeFlAttiv) {
        ldbm0250.getAdes().setAdeFlAttiv(adeFlAttiv);
    }

    @Override
    public Character getAdeFlAttivObj() {
        if (ldbm0250.getWs().getIndAdes().getFlAttiv() >= 0) {
            return ((Character)getAdeFlAttiv());
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeFlAttivObj(Character adeFlAttivObj) {
        if (adeFlAttivObj != null) {
            setAdeFlAttiv(((char)adeFlAttivObj));
            ldbm0250.getWs().getIndAdes().setFlAttiv(((short)0));
        }
        else {
            ldbm0250.getWs().getIndAdes().setFlAttiv(((short)-1));
        }
    }

    @Override
    public char getAdeFlCoincAssto() {
        return ldbm0250.getAdes().getAdeFlCoincAssto();
    }

    @Override
    public void setAdeFlCoincAssto(char adeFlCoincAssto) {
        ldbm0250.getAdes().setAdeFlCoincAssto(adeFlCoincAssto);
    }

    @Override
    public Character getAdeFlCoincAsstoObj() {
        if (ldbm0250.getWs().getIndAdes().getFlCoincAssto() >= 0) {
            return ((Character)getAdeFlCoincAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeFlCoincAsstoObj(Character adeFlCoincAsstoObj) {
        if (adeFlCoincAsstoObj != null) {
            setAdeFlCoincAssto(((char)adeFlCoincAsstoObj));
            ldbm0250.getWs().getIndAdes().setFlCoincAssto(((short)0));
        }
        else {
            ldbm0250.getWs().getIndAdes().setFlCoincAssto(((short)-1));
        }
    }

    @Override
    public char getAdeFlProvzaMigraz() {
        return ldbm0250.getAdes().getAdeFlProvzaMigraz();
    }

    @Override
    public void setAdeFlProvzaMigraz(char adeFlProvzaMigraz) {
        ldbm0250.getAdes().setAdeFlProvzaMigraz(adeFlProvzaMigraz);
    }

    @Override
    public Character getAdeFlProvzaMigrazObj() {
        if (ldbm0250.getWs().getIndAdes().getFlProvzaMigraz() >= 0) {
            return ((Character)getAdeFlProvzaMigraz());
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeFlProvzaMigrazObj(Character adeFlProvzaMigrazObj) {
        if (adeFlProvzaMigrazObj != null) {
            setAdeFlProvzaMigraz(((char)adeFlProvzaMigrazObj));
            ldbm0250.getWs().getIndAdes().setFlProvzaMigraz(((short)0));
        }
        else {
            ldbm0250.getWs().getIndAdes().setFlProvzaMigraz(((short)-1));
        }
    }

    @Override
    public char getAdeFlVarzStatTbgc() {
        return ldbm0250.getAdes().getAdeFlVarzStatTbgc();
    }

    @Override
    public void setAdeFlVarzStatTbgc(char adeFlVarzStatTbgc) {
        ldbm0250.getAdes().setAdeFlVarzStatTbgc(adeFlVarzStatTbgc);
    }

    @Override
    public Character getAdeFlVarzStatTbgcObj() {
        if (ldbm0250.getWs().getIndAdes().getFlVarzStatTbgc() >= 0) {
            return ((Character)getAdeFlVarzStatTbgc());
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeFlVarzStatTbgcObj(Character adeFlVarzStatTbgcObj) {
        if (adeFlVarzStatTbgcObj != null) {
            setAdeFlVarzStatTbgc(((char)adeFlVarzStatTbgcObj));
            ldbm0250.getWs().getIndAdes().setFlVarzStatTbgc(((short)0));
        }
        else {
            ldbm0250.getWs().getIndAdes().setFlVarzStatTbgc(((short)-1));
        }
    }

    @Override
    public String getAdeIbDflt() {
        return ldbm0250.getAdes().getAdeIbDflt();
    }

    @Override
    public void setAdeIbDflt(String adeIbDflt) {
        ldbm0250.getAdes().setAdeIbDflt(adeIbDflt);
    }

    @Override
    public String getAdeIbDfltObj() {
        if (ldbm0250.getWs().getIndAdes().getIbDflt() >= 0) {
            return getAdeIbDflt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeIbDfltObj(String adeIbDfltObj) {
        if (adeIbDfltObj != null) {
            setAdeIbDflt(adeIbDfltObj);
            ldbm0250.getWs().getIndAdes().setIbDflt(((short)0));
        }
        else {
            ldbm0250.getWs().getIndAdes().setIbDflt(((short)-1));
        }
    }

    @Override
    public String getAdeIbOgg() {
        return ldbm0250.getAdes().getAdeIbOgg();
    }

    @Override
    public void setAdeIbOgg(String adeIbOgg) {
        ldbm0250.getAdes().setAdeIbOgg(adeIbOgg);
    }

    @Override
    public String getAdeIbOggObj() {
        if (ldbm0250.getWs().getIndAdes().getIbOgg() >= 0) {
            return getAdeIbOgg();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeIbOggObj(String adeIbOggObj) {
        if (adeIbOggObj != null) {
            setAdeIbOgg(adeIbOggObj);
            ldbm0250.getWs().getIndAdes().setIbOgg(((short)0));
        }
        else {
            ldbm0250.getWs().getIndAdes().setIbOgg(((short)-1));
        }
    }

    @Override
    public String getAdeIbPrev() {
        return ldbm0250.getAdes().getAdeIbPrev();
    }

    @Override
    public void setAdeIbPrev(String adeIbPrev) {
        ldbm0250.getAdes().setAdeIbPrev(adeIbPrev);
    }

    @Override
    public String getAdeIbPrevObj() {
        if (ldbm0250.getWs().getIndAdes().getIbPrev() >= 0) {
            return getAdeIbPrev();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeIbPrevObj(String adeIbPrevObj) {
        if (adeIbPrevObj != null) {
            setAdeIbPrev(adeIbPrevObj);
            ldbm0250.getWs().getIndAdes().setIbPrev(((short)0));
        }
        else {
            ldbm0250.getWs().getIndAdes().setIbPrev(((short)-1));
        }
    }

    @Override
    public int getAdeIdAdes() {
        return ldbm0250.getAdes().getAdeIdAdes();
    }

    @Override
    public void setAdeIdAdes(int adeIdAdes) {
        ldbm0250.getAdes().setAdeIdAdes(adeIdAdes);
    }

    @Override
    public int getAdeIdMoviChiu() {
        return ldbm0250.getAdes().getAdeIdMoviChiu().getAdeIdMoviChiu();
    }

    @Override
    public void setAdeIdMoviChiu(int adeIdMoviChiu) {
        ldbm0250.getAdes().getAdeIdMoviChiu().setAdeIdMoviChiu(adeIdMoviChiu);
    }

    @Override
    public Integer getAdeIdMoviChiuObj() {
        if (ldbm0250.getWs().getIndAdes().getIdMoviChiu() >= 0) {
            return ((Integer)getAdeIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeIdMoviChiuObj(Integer adeIdMoviChiuObj) {
        if (adeIdMoviChiuObj != null) {
            setAdeIdMoviChiu(((int)adeIdMoviChiuObj));
            ldbm0250.getWs().getIndAdes().setIdMoviChiu(((short)0));
        }
        else {
            ldbm0250.getWs().getIndAdes().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getAdeIdMoviCrz() {
        return ldbm0250.getAdes().getAdeIdMoviCrz();
    }

    @Override
    public void setAdeIdMoviCrz(int adeIdMoviCrz) {
        ldbm0250.getAdes().setAdeIdMoviCrz(adeIdMoviCrz);
    }

    @Override
    public int getAdeIdPoli() {
        return ldbm0250.getAdes().getAdeIdPoli();
    }

    @Override
    public void setAdeIdPoli(int adeIdPoli) {
        ldbm0250.getAdes().setAdeIdPoli(adeIdPoli);
    }

    @Override
    public String getAdeIdenIscFnd() {
        return ldbm0250.getAdes().getAdeIdenIscFnd();
    }

    @Override
    public void setAdeIdenIscFnd(String adeIdenIscFnd) {
        ldbm0250.getAdes().setAdeIdenIscFnd(adeIdenIscFnd);
    }

    @Override
    public String getAdeIdenIscFndObj() {
        if (ldbm0250.getWs().getIndAdes().getIdenIscFnd() >= 0) {
            return getAdeIdenIscFnd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeIdenIscFndObj(String adeIdenIscFndObj) {
        if (adeIdenIscFndObj != null) {
            setAdeIdenIscFnd(adeIdenIscFndObj);
            ldbm0250.getWs().getIndAdes().setIdenIscFnd(((short)0));
        }
        else {
            ldbm0250.getWs().getIndAdes().setIdenIscFnd(((short)-1));
        }
    }

    @Override
    public AfDecimal getAdeImpAder() {
        return ldbm0250.getAdes().getAdeImpAder().getAdeImpAder();
    }

    @Override
    public void setAdeImpAder(AfDecimal adeImpAder) {
        ldbm0250.getAdes().getAdeImpAder().setAdeImpAder(adeImpAder.copy());
    }

    @Override
    public AfDecimal getAdeImpAderObj() {
        if (ldbm0250.getWs().getIndAdes().getImpAder() >= 0) {
            return getAdeImpAder();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeImpAderObj(AfDecimal adeImpAderObj) {
        if (adeImpAderObj != null) {
            setAdeImpAder(new AfDecimal(adeImpAderObj, 15, 3));
            ldbm0250.getWs().getIndAdes().setImpAder(((short)0));
        }
        else {
            ldbm0250.getWs().getIndAdes().setImpAder(((short)-1));
        }
    }

    @Override
    public AfDecimal getAdeImpAz() {
        return ldbm0250.getAdes().getAdeImpAz().getAdeImpAz();
    }

    @Override
    public void setAdeImpAz(AfDecimal adeImpAz) {
        ldbm0250.getAdes().getAdeImpAz().setAdeImpAz(adeImpAz.copy());
    }

    @Override
    public AfDecimal getAdeImpAzObj() {
        if (ldbm0250.getWs().getIndAdes().getImpAz() >= 0) {
            return getAdeImpAz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeImpAzObj(AfDecimal adeImpAzObj) {
        if (adeImpAzObj != null) {
            setAdeImpAz(new AfDecimal(adeImpAzObj, 15, 3));
            ldbm0250.getWs().getIndAdes().setImpAz(((short)0));
        }
        else {
            ldbm0250.getWs().getIndAdes().setImpAz(((short)-1));
        }
    }

    @Override
    public AfDecimal getAdeImpGarCnbt() {
        return ldbm0250.getAdes().getAdeImpGarCnbt().getAdeImpGarCnbt();
    }

    @Override
    public void setAdeImpGarCnbt(AfDecimal adeImpGarCnbt) {
        ldbm0250.getAdes().getAdeImpGarCnbt().setAdeImpGarCnbt(adeImpGarCnbt.copy());
    }

    @Override
    public AfDecimal getAdeImpGarCnbtObj() {
        if (ldbm0250.getWs().getIndAdes().getImpGarCnbt() >= 0) {
            return getAdeImpGarCnbt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeImpGarCnbtObj(AfDecimal adeImpGarCnbtObj) {
        if (adeImpGarCnbtObj != null) {
            setAdeImpGarCnbt(new AfDecimal(adeImpGarCnbtObj, 15, 3));
            ldbm0250.getWs().getIndAdes().setImpGarCnbt(((short)0));
        }
        else {
            ldbm0250.getWs().getIndAdes().setImpGarCnbt(((short)-1));
        }
    }

    @Override
    public AfDecimal getAdeImpRecRitAcc() {
        return ldbm0250.getAdes().getAdeImpRecRitAcc().getAdeImpRecRitAcc();
    }

    @Override
    public void setAdeImpRecRitAcc(AfDecimal adeImpRecRitAcc) {
        ldbm0250.getAdes().getAdeImpRecRitAcc().setAdeImpRecRitAcc(adeImpRecRitAcc.copy());
    }

    @Override
    public AfDecimal getAdeImpRecRitAccObj() {
        if (ldbm0250.getWs().getIndAdes().getImpRecRitAcc() >= 0) {
            return getAdeImpRecRitAcc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeImpRecRitAccObj(AfDecimal adeImpRecRitAccObj) {
        if (adeImpRecRitAccObj != null) {
            setAdeImpRecRitAcc(new AfDecimal(adeImpRecRitAccObj, 15, 3));
            ldbm0250.getWs().getIndAdes().setImpRecRitAcc(((short)0));
        }
        else {
            ldbm0250.getWs().getIndAdes().setImpRecRitAcc(((short)-1));
        }
    }

    @Override
    public AfDecimal getAdeImpRecRitVis() {
        return ldbm0250.getAdes().getAdeImpRecRitVis().getAdeImpRecRitVis();
    }

    @Override
    public void setAdeImpRecRitVis(AfDecimal adeImpRecRitVis) {
        ldbm0250.getAdes().getAdeImpRecRitVis().setAdeImpRecRitVis(adeImpRecRitVis.copy());
    }

    @Override
    public AfDecimal getAdeImpRecRitVisObj() {
        if (ldbm0250.getWs().getIndAdes().getImpRecRitVis() >= 0) {
            return getAdeImpRecRitVis();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeImpRecRitVisObj(AfDecimal adeImpRecRitVisObj) {
        if (adeImpRecRitVisObj != null) {
            setAdeImpRecRitVis(new AfDecimal(adeImpRecRitVisObj, 15, 3));
            ldbm0250.getWs().getIndAdes().setImpRecRitVis(((short)0));
        }
        else {
            ldbm0250.getWs().getIndAdes().setImpRecRitVis(((short)-1));
        }
    }

    @Override
    public AfDecimal getAdeImpTfr() {
        return ldbm0250.getAdes().getAdeImpTfr().getAdeImpTfr();
    }

    @Override
    public void setAdeImpTfr(AfDecimal adeImpTfr) {
        ldbm0250.getAdes().getAdeImpTfr().setAdeImpTfr(adeImpTfr.copy());
    }

    @Override
    public AfDecimal getAdeImpTfrObj() {
        if (ldbm0250.getWs().getIndAdes().getImpTfr() >= 0) {
            return getAdeImpTfr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeImpTfrObj(AfDecimal adeImpTfrObj) {
        if (adeImpTfrObj != null) {
            setAdeImpTfr(new AfDecimal(adeImpTfrObj, 15, 3));
            ldbm0250.getWs().getIndAdes().setImpTfr(((short)0));
        }
        else {
            ldbm0250.getWs().getIndAdes().setImpTfr(((short)-1));
        }
    }

    @Override
    public AfDecimal getAdeImpVolo() {
        return ldbm0250.getAdes().getAdeImpVolo().getAdeImpVolo();
    }

    @Override
    public void setAdeImpVolo(AfDecimal adeImpVolo) {
        ldbm0250.getAdes().getAdeImpVolo().setAdeImpVolo(adeImpVolo.copy());
    }

    @Override
    public AfDecimal getAdeImpVoloObj() {
        if (ldbm0250.getWs().getIndAdes().getImpVolo() >= 0) {
            return getAdeImpVolo();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeImpVoloObj(AfDecimal adeImpVoloObj) {
        if (adeImpVoloObj != null) {
            setAdeImpVolo(new AfDecimal(adeImpVoloObj, 15, 3));
            ldbm0250.getWs().getIndAdes().setImpVolo(((short)0));
        }
        else {
            ldbm0250.getWs().getIndAdes().setImpVolo(((short)-1));
        }
    }

    @Override
    public AfDecimal getAdeImpbVisDaRec() {
        return ldbm0250.getAdes().getAdeImpbVisDaRec().getAdeImpbVisDaRec();
    }

    @Override
    public void setAdeImpbVisDaRec(AfDecimal adeImpbVisDaRec) {
        ldbm0250.getAdes().getAdeImpbVisDaRec().setAdeImpbVisDaRec(adeImpbVisDaRec.copy());
    }

    @Override
    public AfDecimal getAdeImpbVisDaRecObj() {
        if (ldbm0250.getWs().getIndAdes().getImpbVisDaRec() >= 0) {
            return getAdeImpbVisDaRec();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeImpbVisDaRecObj(AfDecimal adeImpbVisDaRecObj) {
        if (adeImpbVisDaRecObj != null) {
            setAdeImpbVisDaRec(new AfDecimal(adeImpbVisDaRecObj, 15, 3));
            ldbm0250.getWs().getIndAdes().setImpbVisDaRec(((short)0));
        }
        else {
            ldbm0250.getWs().getIndAdes().setImpbVisDaRec(((short)-1));
        }
    }

    @Override
    public String getAdeModCalc() {
        return ldbm0250.getAdes().getAdeModCalc();
    }

    @Override
    public void setAdeModCalc(String adeModCalc) {
        ldbm0250.getAdes().setAdeModCalc(adeModCalc);
    }

    @Override
    public String getAdeModCalcObj() {
        if (ldbm0250.getWs().getIndAdes().getModCalc() >= 0) {
            return getAdeModCalc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeModCalcObj(String adeModCalcObj) {
        if (adeModCalcObj != null) {
            setAdeModCalc(adeModCalcObj);
            ldbm0250.getWs().getIndAdes().setModCalc(((short)0));
        }
        else {
            ldbm0250.getWs().getIndAdes().setModCalc(((short)-1));
        }
    }

    @Override
    public AfDecimal getAdeNumRatPian() {
        return ldbm0250.getAdes().getAdeNumRatPian().getAdeNumRatPian();
    }

    @Override
    public void setAdeNumRatPian(AfDecimal adeNumRatPian) {
        ldbm0250.getAdes().getAdeNumRatPian().setAdeNumRatPian(adeNumRatPian.copy());
    }

    @Override
    public AfDecimal getAdeNumRatPianObj() {
        if (ldbm0250.getWs().getIndAdes().getNumRatPian() >= 0) {
            return getAdeNumRatPian();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeNumRatPianObj(AfDecimal adeNumRatPianObj) {
        if (adeNumRatPianObj != null) {
            setAdeNumRatPian(new AfDecimal(adeNumRatPianObj, 12, 5));
            ldbm0250.getWs().getIndAdes().setNumRatPian(((short)0));
        }
        else {
            ldbm0250.getWs().getIndAdes().setNumRatPian(((short)-1));
        }
    }

    @Override
    public AfDecimal getAdePcAder() {
        return ldbm0250.getAdes().getAdePcAder().getAdePcAder();
    }

    @Override
    public void setAdePcAder(AfDecimal adePcAder) {
        ldbm0250.getAdes().getAdePcAder().setAdePcAder(adePcAder.copy());
    }

    @Override
    public AfDecimal getAdePcAderObj() {
        if (ldbm0250.getWs().getIndAdes().getPcAder() >= 0) {
            return getAdePcAder();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdePcAderObj(AfDecimal adePcAderObj) {
        if (adePcAderObj != null) {
            setAdePcAder(new AfDecimal(adePcAderObj, 6, 3));
            ldbm0250.getWs().getIndAdes().setPcAder(((short)0));
        }
        else {
            ldbm0250.getWs().getIndAdes().setPcAder(((short)-1));
        }
    }

    @Override
    public AfDecimal getAdePcAz() {
        return ldbm0250.getAdes().getAdePcAz().getAdePcAz();
    }

    @Override
    public void setAdePcAz(AfDecimal adePcAz) {
        ldbm0250.getAdes().getAdePcAz().setAdePcAz(adePcAz.copy());
    }

    @Override
    public AfDecimal getAdePcAzObj() {
        if (ldbm0250.getWs().getIndAdes().getPcAz() >= 0) {
            return getAdePcAz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdePcAzObj(AfDecimal adePcAzObj) {
        if (adePcAzObj != null) {
            setAdePcAz(new AfDecimal(adePcAzObj, 6, 3));
            ldbm0250.getWs().getIndAdes().setPcAz(((short)0));
        }
        else {
            ldbm0250.getWs().getIndAdes().setPcAz(((short)-1));
        }
    }

    @Override
    public AfDecimal getAdePcTfr() {
        return ldbm0250.getAdes().getAdePcTfr().getAdePcTfr();
    }

    @Override
    public void setAdePcTfr(AfDecimal adePcTfr) {
        ldbm0250.getAdes().getAdePcTfr().setAdePcTfr(adePcTfr.copy());
    }

    @Override
    public AfDecimal getAdePcTfrObj() {
        if (ldbm0250.getWs().getIndAdes().getPcTfr() >= 0) {
            return getAdePcTfr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdePcTfrObj(AfDecimal adePcTfrObj) {
        if (adePcTfrObj != null) {
            setAdePcTfr(new AfDecimal(adePcTfrObj, 6, 3));
            ldbm0250.getWs().getIndAdes().setPcTfr(((short)0));
        }
        else {
            ldbm0250.getWs().getIndAdes().setPcTfr(((short)-1));
        }
    }

    @Override
    public AfDecimal getAdePcVolo() {
        return ldbm0250.getAdes().getAdePcVolo().getAdePcVolo();
    }

    @Override
    public void setAdePcVolo(AfDecimal adePcVolo) {
        ldbm0250.getAdes().getAdePcVolo().setAdePcVolo(adePcVolo.copy());
    }

    @Override
    public AfDecimal getAdePcVoloObj() {
        if (ldbm0250.getWs().getIndAdes().getPcVolo() >= 0) {
            return getAdePcVolo();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdePcVoloObj(AfDecimal adePcVoloObj) {
        if (adePcVoloObj != null) {
            setAdePcVolo(new AfDecimal(adePcVoloObj, 6, 3));
            ldbm0250.getWs().getIndAdes().setPcVolo(((short)0));
        }
        else {
            ldbm0250.getWs().getIndAdes().setPcVolo(((short)-1));
        }
    }

    @Override
    public AfDecimal getAdePreLrdInd() {
        return ldbm0250.getAdes().getAdePreLrdInd().getAdePreLrdInd();
    }

    @Override
    public void setAdePreLrdInd(AfDecimal adePreLrdInd) {
        ldbm0250.getAdes().getAdePreLrdInd().setAdePreLrdInd(adePreLrdInd.copy());
    }

    @Override
    public AfDecimal getAdePreLrdIndObj() {
        if (ldbm0250.getWs().getIndAdes().getPreLrdInd() >= 0) {
            return getAdePreLrdInd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdePreLrdIndObj(AfDecimal adePreLrdIndObj) {
        if (adePreLrdIndObj != null) {
            setAdePreLrdInd(new AfDecimal(adePreLrdIndObj, 15, 3));
            ldbm0250.getWs().getIndAdes().setPreLrdInd(((short)0));
        }
        else {
            ldbm0250.getWs().getIndAdes().setPreLrdInd(((short)-1));
        }
    }

    @Override
    public AfDecimal getAdePreNetInd() {
        return ldbm0250.getAdes().getAdePreNetInd().getAdePreNetInd();
    }

    @Override
    public void setAdePreNetInd(AfDecimal adePreNetInd) {
        ldbm0250.getAdes().getAdePreNetInd().setAdePreNetInd(adePreNetInd.copy());
    }

    @Override
    public AfDecimal getAdePreNetIndObj() {
        if (ldbm0250.getWs().getIndAdes().getPreNetInd() >= 0) {
            return getAdePreNetInd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdePreNetIndObj(AfDecimal adePreNetIndObj) {
        if (adePreNetIndObj != null) {
            setAdePreNetInd(new AfDecimal(adePreNetIndObj, 15, 3));
            ldbm0250.getWs().getIndAdes().setPreNetInd(((short)0));
        }
        else {
            ldbm0250.getWs().getIndAdes().setPreNetInd(((short)-1));
        }
    }

    @Override
    public AfDecimal getAdePrstzIniInd() {
        return ldbm0250.getAdes().getAdePrstzIniInd().getAdePrstzIniInd();
    }

    @Override
    public void setAdePrstzIniInd(AfDecimal adePrstzIniInd) {
        ldbm0250.getAdes().getAdePrstzIniInd().setAdePrstzIniInd(adePrstzIniInd.copy());
    }

    @Override
    public AfDecimal getAdePrstzIniIndObj() {
        if (ldbm0250.getWs().getIndAdes().getPrstzIniInd() >= 0) {
            return getAdePrstzIniInd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdePrstzIniIndObj(AfDecimal adePrstzIniIndObj) {
        if (adePrstzIniIndObj != null) {
            setAdePrstzIniInd(new AfDecimal(adePrstzIniIndObj, 15, 3));
            ldbm0250.getWs().getIndAdes().setPrstzIniInd(((short)0));
        }
        else {
            ldbm0250.getWs().getIndAdes().setPrstzIniInd(((short)-1));
        }
    }

    @Override
    public AfDecimal getAdeRatLrdInd() {
        return ldbm0250.getAdes().getAdeRatLrdInd().getAdeRatLrdInd();
    }

    @Override
    public void setAdeRatLrdInd(AfDecimal adeRatLrdInd) {
        ldbm0250.getAdes().getAdeRatLrdInd().setAdeRatLrdInd(adeRatLrdInd.copy());
    }

    @Override
    public AfDecimal getAdeRatLrdIndObj() {
        if (ldbm0250.getWs().getIndAdes().getRatLrdInd() >= 0) {
            return getAdeRatLrdInd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeRatLrdIndObj(AfDecimal adeRatLrdIndObj) {
        if (adeRatLrdIndObj != null) {
            setAdeRatLrdInd(new AfDecimal(adeRatLrdIndObj, 15, 3));
            ldbm0250.getWs().getIndAdes().setRatLrdInd(((short)0));
        }
        else {
            ldbm0250.getWs().getIndAdes().setRatLrdInd(((short)-1));
        }
    }

    @Override
    public String getAdeTpFntCnbtva() {
        return ldbm0250.getAdes().getAdeTpFntCnbtva();
    }

    @Override
    public void setAdeTpFntCnbtva(String adeTpFntCnbtva) {
        ldbm0250.getAdes().setAdeTpFntCnbtva(adeTpFntCnbtva);
    }

    @Override
    public String getAdeTpFntCnbtvaObj() {
        if (ldbm0250.getWs().getIndAdes().getTpFntCnbtva() >= 0) {
            return getAdeTpFntCnbtva();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeTpFntCnbtvaObj(String adeTpFntCnbtvaObj) {
        if (adeTpFntCnbtvaObj != null) {
            setAdeTpFntCnbtva(adeTpFntCnbtvaObj);
            ldbm0250.getWs().getIndAdes().setTpFntCnbtva(((short)0));
        }
        else {
            ldbm0250.getWs().getIndAdes().setTpFntCnbtva(((short)-1));
        }
    }

    @Override
    public String getAdeTpIas() {
        return ldbm0250.getAdes().getAdeTpIas();
    }

    @Override
    public void setAdeTpIas(String adeTpIas) {
        ldbm0250.getAdes().setAdeTpIas(adeTpIas);
    }

    @Override
    public String getAdeTpIasObj() {
        if (ldbm0250.getWs().getIndAdes().getTpIas() >= 0) {
            return getAdeTpIas();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeTpIasObj(String adeTpIasObj) {
        if (adeTpIasObj != null) {
            setAdeTpIas(adeTpIasObj);
            ldbm0250.getWs().getIndAdes().setTpIas(((short)0));
        }
        else {
            ldbm0250.getWs().getIndAdes().setTpIas(((short)-1));
        }
    }

    @Override
    public String getAdeTpModPagTit() {
        return ldbm0250.getAdes().getAdeTpModPagTit();
    }

    @Override
    public void setAdeTpModPagTit(String adeTpModPagTit) {
        ldbm0250.getAdes().setAdeTpModPagTit(adeTpModPagTit);
    }

    @Override
    public String getAdeTpRgmFisc() {
        return ldbm0250.getAdes().getAdeTpRgmFisc();
    }

    @Override
    public void setAdeTpRgmFisc(String adeTpRgmFisc) {
        ldbm0250.getAdes().setAdeTpRgmFisc(adeTpRgmFisc);
    }

    @Override
    public String getAdeTpRiat() {
        return ldbm0250.getAdes().getAdeTpRiat();
    }

    @Override
    public void setAdeTpRiat(String adeTpRiat) {
        ldbm0250.getAdes().setAdeTpRiat(adeTpRiat);
    }

    @Override
    public String getAdeTpRiatObj() {
        if (ldbm0250.getWs().getIndAdes().getTpRiat() >= 0) {
            return getAdeTpRiat();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeTpRiatObj(String adeTpRiatObj) {
        if (adeTpRiatObj != null) {
            setAdeTpRiat(adeTpRiatObj);
            ldbm0250.getWs().getIndAdes().setTpRiat(((short)0));
        }
        else {
            ldbm0250.getWs().getIndAdes().setTpRiat(((short)-1));
        }
    }

    @Override
    public char getIabv0002State01() {
        throw new FieldNotMappedException("iabv0002State01");
    }

    @Override
    public void setIabv0002State01(char iabv0002State01) {
        throw new FieldNotMappedException("iabv0002State01");
    }

    @Override
    public char getIabv0002State02() {
        throw new FieldNotMappedException("iabv0002State02");
    }

    @Override
    public void setIabv0002State02(char iabv0002State02) {
        throw new FieldNotMappedException("iabv0002State02");
    }

    @Override
    public char getIabv0002State03() {
        throw new FieldNotMappedException("iabv0002State03");
    }

    @Override
    public void setIabv0002State03(char iabv0002State03) {
        throw new FieldNotMappedException("iabv0002State03");
    }

    @Override
    public char getIabv0002State04() {
        throw new FieldNotMappedException("iabv0002State04");
    }

    @Override
    public void setIabv0002State04(char iabv0002State04) {
        throw new FieldNotMappedException("iabv0002State04");
    }

    @Override
    public char getIabv0002State05() {
        throw new FieldNotMappedException("iabv0002State05");
    }

    @Override
    public void setIabv0002State05(char iabv0002State05) {
        throw new FieldNotMappedException("iabv0002State05");
    }

    @Override
    public char getIabv0002State06() {
        throw new FieldNotMappedException("iabv0002State06");
    }

    @Override
    public void setIabv0002State06(char iabv0002State06) {
        throw new FieldNotMappedException("iabv0002State06");
    }

    @Override
    public char getIabv0002State07() {
        throw new FieldNotMappedException("iabv0002State07");
    }

    @Override
    public void setIabv0002State07(char iabv0002State07) {
        throw new FieldNotMappedException("iabv0002State07");
    }

    @Override
    public char getIabv0002State08() {
        throw new FieldNotMappedException("iabv0002State08");
    }

    @Override
    public void setIabv0002State08(char iabv0002State08) {
        throw new FieldNotMappedException("iabv0002State08");
    }

    @Override
    public char getIabv0002State09() {
        throw new FieldNotMappedException("iabv0002State09");
    }

    @Override
    public void setIabv0002State09(char iabv0002State09) {
        throw new FieldNotMappedException("iabv0002State09");
    }

    @Override
    public char getIabv0002State10() {
        throw new FieldNotMappedException("iabv0002State10");
    }

    @Override
    public void setIabv0002State10(char iabv0002State10) {
        throw new FieldNotMappedException("iabv0002State10");
    }

    @Override
    public int getIabv0009IdOggA() {
        return ldbm0250.getIabv0002().getIabv0009GestGuideService().getIdOggA();
    }

    @Override
    public void setIabv0009IdOggA(int iabv0009IdOggA) {
        ldbm0250.getIabv0002().getIabv0009GestGuideService().setIdOggA(iabv0009IdOggA);
    }

    @Override
    public int getIabv0009IdOggDa() {
        return ldbm0250.getIabv0002().getIabv0009GestGuideService().getIdOggDa();
    }

    @Override
    public void setIabv0009IdOggDa(int iabv0009IdOggDa) {
        ldbm0250.getIabv0002().getIabv0009GestGuideService().setIdOggDa(iabv0009IdOggDa);
    }

    @Override
    public int getIabv0009Versioning() {
        throw new FieldNotMappedException("iabv0009Versioning");
    }

    @Override
    public void setIabv0009Versioning(int iabv0009Versioning) {
        throw new FieldNotMappedException("iabv0009Versioning");
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        return ldbm0250.getIdsv0003().getCodiceCompagniaAnia();
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        ldbm0250.getIdsv0003().setCodiceCompagniaAnia(idsv0003CodiceCompagniaAnia);
    }

    @Override
    public int getPolAaDiffProrDflt() {
        return ldbm0250.getPoli().getPolAaDiffProrDflt().getPolAaDiffProrDflt();
    }

    @Override
    public void setPolAaDiffProrDflt(int polAaDiffProrDflt) {
        ldbm0250.getPoli().getPolAaDiffProrDflt().setPolAaDiffProrDflt(polAaDiffProrDflt);
    }

    @Override
    public Integer getPolAaDiffProrDfltObj() {
        if (ldbm0250.getWs().getIndPoli().getProvDaRec() >= 0) {
            return ((Integer)getPolAaDiffProrDflt());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolAaDiffProrDfltObj(Integer polAaDiffProrDfltObj) {
        if (polAaDiffProrDfltObj != null) {
            setPolAaDiffProrDflt(((int)polAaDiffProrDfltObj));
            ldbm0250.getWs().getIndPoli().setProvDaRec(((short)0));
        }
        else {
            ldbm0250.getWs().getIndPoli().setProvDaRec(((short)-1));
        }
    }

    @Override
    public int getPolCodCompAnia() {
        return ldbm0250.getPoli().getPolCodCompAnia();
    }

    @Override
    public void setPolCodCompAnia(int polCodCompAnia) {
        ldbm0250.getPoli().setPolCodCompAnia(polCodCompAnia);
    }

    @Override
    public String getPolCodConv() {
        return ldbm0250.getPoli().getPolCodConv();
    }

    @Override
    public String getPolCodConvAgg() {
        return ldbm0250.getPoli().getPolCodConvAgg();
    }

    @Override
    public void setPolCodConvAgg(String polCodConvAgg) {
        ldbm0250.getPoli().setPolCodConvAgg(polCodConvAgg);
    }

    @Override
    public String getPolCodConvAggObj() {
        if (ldbm0250.getWs().getIndPoli().getSpeAge() >= 0) {
            return getPolCodConvAgg();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolCodConvAggObj(String polCodConvAggObj) {
        if (polCodConvAggObj != null) {
            setPolCodConvAgg(polCodConvAggObj);
            ldbm0250.getWs().getIndPoli().setSpeAge(((short)0));
        }
        else {
            ldbm0250.getWs().getIndPoli().setSpeAge(((short)-1));
        }
    }

    @Override
    public void setPolCodConv(String polCodConv) {
        ldbm0250.getPoli().setPolCodConv(polCodConv);
    }

    @Override
    public String getPolCodConvObj() {
        if (ldbm0250.getWs().getIndPoli().getIntrRetdt() >= 0) {
            return getPolCodConv();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolCodConvObj(String polCodConvObj) {
        if (polCodConvObj != null) {
            setPolCodConv(polCodConvObj);
            ldbm0250.getWs().getIndPoli().setIntrRetdt(((short)0));
        }
        else {
            ldbm0250.getWs().getIndPoli().setIntrRetdt(((short)-1));
        }
    }

    @Override
    public String getPolCodDvs() {
        return ldbm0250.getPoli().getPolCodDvs();
    }

    @Override
    public void setPolCodDvs(String polCodDvs) {
        ldbm0250.getPoli().setPolCodDvs(polCodDvs);
    }

    @Override
    public String getPolCodDvsObj() {
        if (ldbm0250.getWs().getIndPoli().getCarGest() >= 0) {
            return getPolCodDvs();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolCodDvsObj(String polCodDvsObj) {
        if (polCodDvsObj != null) {
            setPolCodDvs(polCodDvsObj);
            ldbm0250.getWs().getIndPoli().setCarGest(((short)0));
        }
        else {
            ldbm0250.getWs().getIndPoli().setCarGest(((short)-1));
        }
    }

    @Override
    public String getPolCodProd() {
        return ldbm0250.getPoli().getPolCodProd();
    }

    @Override
    public void setPolCodProd(String polCodProd) {
        ldbm0250.getPoli().setPolCodProd(polCodProd);
    }

    @Override
    public String getPolCodRamo() {
        return ldbm0250.getPoli().getPolCodRamo();
    }

    @Override
    public void setPolCodRamo(String polCodRamo) {
        ldbm0250.getPoli().setPolCodRamo(polCodRamo);
    }

    @Override
    public String getPolCodRamoObj() {
        if (ldbm0250.getWs().getIndPoli().getIntrRiat() >= 0) {
            return getPolCodRamo();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolCodRamoObj(String polCodRamoObj) {
        if (polCodRamoObj != null) {
            setPolCodRamo(polCodRamoObj);
            ldbm0250.getWs().getIndPoli().setIntrRiat(((short)0));
        }
        else {
            ldbm0250.getWs().getIndPoli().setIntrRiat(((short)-1));
        }
    }

    @Override
    public String getPolCodTpa() {
        return ldbm0250.getPoli().getPolCodTpa();
    }

    @Override
    public void setPolCodTpa(String polCodTpa) {
        ldbm0250.getPoli().setPolCodTpa(polCodTpa);
    }

    @Override
    public String getPolCodTpaObj() {
        if (ldbm0250.getWs().getIndPoli().getImpTrasfe() >= 0) {
            return getPolCodTpa();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolCodTpaObj(String polCodTpaObj) {
        if (polCodTpaObj != null) {
            setPolCodTpa(polCodTpaObj);
            ldbm0250.getWs().getIndPoli().setImpTrasfe(((short)0));
        }
        else {
            ldbm0250.getWs().getIndPoli().setImpTrasfe(((short)-1));
        }
    }

    @Override
    public String getPolConvGeco() {
        return ldbm0250.getPoli().getPolConvGeco();
    }

    @Override
    public void setPolConvGeco(String polConvGeco) {
        ldbm0250.getPoli().setPolConvGeco(polConvGeco);
    }

    @Override
    public String getPolConvGecoObj() {
        if (ldbm0250.getWs().getIndPoli().getImpVolo() >= 0) {
            return getPolConvGeco();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolConvGecoObj(String polConvGecoObj) {
        if (polConvGecoObj != null) {
            setPolConvGeco(polConvGecoObj);
            ldbm0250.getWs().getIndPoli().setImpVolo(((short)0));
        }
        else {
            ldbm0250.getWs().getIndPoli().setImpVolo(((short)-1));
        }
    }

    @Override
    public AfDecimal getPolDir1oVers() {
        return ldbm0250.getPoli().getPolDir1oVers().getPolDir1oVers();
    }

    @Override
    public void setPolDir1oVers(AfDecimal polDir1oVers) {
        ldbm0250.getPoli().getPolDir1oVers().setPolDir1oVers(polDir1oVers.copy());
    }

    @Override
    public AfDecimal getPolDir1oVersObj() {
        if (ldbm0250.getWs().getIndPoli().getPreSoloRsh() >= 0) {
            return getPolDir1oVers();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolDir1oVersObj(AfDecimal polDir1oVersObj) {
        if (polDir1oVersObj != null) {
            setPolDir1oVers(new AfDecimal(polDir1oVersObj, 15, 3));
            ldbm0250.getWs().getIndPoli().setPreSoloRsh(((short)0));
        }
        else {
            ldbm0250.getWs().getIndPoli().setPreSoloRsh(((short)-1));
        }
    }

    @Override
    public AfDecimal getPolDirEmis() {
        return ldbm0250.getPoli().getPolDirEmis().getPolDirEmis();
    }

    @Override
    public void setPolDirEmis(AfDecimal polDirEmis) {
        ldbm0250.getPoli().getPolDirEmis().setPolDirEmis(polDirEmis.copy());
    }

    @Override
    public AfDecimal getPolDirEmisObj() {
        if (ldbm0250.getWs().getIndPoli().getPrePpIas() >= 0) {
            return getPolDirEmis();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolDirEmisObj(AfDecimal polDirEmisObj) {
        if (polDirEmisObj != null) {
            setPolDirEmis(new AfDecimal(polDirEmisObj, 15, 3));
            ldbm0250.getWs().getIndPoli().setPrePpIas(((short)0));
        }
        else {
            ldbm0250.getWs().getIndPoli().setPrePpIas(((short)-1));
        }
    }

    @Override
    public AfDecimal getPolDirQuiet() {
        return ldbm0250.getPoli().getPolDirQuiet().getPolDirQuiet();
    }

    @Override
    public void setPolDirQuiet(AfDecimal polDirQuiet) {
        ldbm0250.getPoli().getPolDirQuiet().setPolDirQuiet(polDirQuiet.copy());
    }

    @Override
    public AfDecimal getPolDirQuietObj() {
        if (ldbm0250.getWs().getIndPoli().getCodTari() >= 0) {
            return getPolDirQuiet();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolDirQuietObj(AfDecimal polDirQuietObj) {
        if (polDirQuietObj != null) {
            setPolDirQuiet(new AfDecimal(polDirQuietObj, 15, 3));
            ldbm0250.getWs().getIndPoli().setCodTari(((short)0));
        }
        else {
            ldbm0250.getWs().getIndPoli().setCodTari(((short)-1));
        }
    }

    @Override
    public AfDecimal getPolDirVersAgg() {
        return ldbm0250.getPoli().getPolDirVersAgg().getPolDirVersAgg();
    }

    @Override
    public void setPolDirVersAgg(AfDecimal polDirVersAgg) {
        ldbm0250.getPoli().getPolDirVersAgg().setPolDirVersAgg(polDirVersAgg.copy());
    }

    @Override
    public AfDecimal getPolDirVersAggObj() {
        if (ldbm0250.getWs().getIndPoli().getCarAcq() >= 0) {
            return getPolDirVersAgg();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolDirVersAggObj(AfDecimal polDirVersAggObj) {
        if (polDirVersAggObj != null) {
            setPolDirVersAgg(new AfDecimal(polDirVersAggObj, 15, 3));
            ldbm0250.getWs().getIndPoli().setCarAcq(((short)0));
        }
        else {
            ldbm0250.getWs().getIndPoli().setCarAcq(((short)-1));
        }
    }

    @Override
    public char getPolDsOperSql() {
        return ldbm0250.getPoli().getPolDsOperSql();
    }

    @Override
    public void setPolDsOperSql(char polDsOperSql) {
        ldbm0250.getPoli().setPolDsOperSql(polDsOperSql);
    }

    @Override
    public long getPolDsRiga() {
        return ldbm0250.getPoli().getPolDsRiga();
    }

    @Override
    public void setPolDsRiga(long polDsRiga) {
        ldbm0250.getPoli().setPolDsRiga(polDsRiga);
    }

    @Override
    public char getPolDsStatoElab() {
        return ldbm0250.getPoli().getPolDsStatoElab();
    }

    @Override
    public void setPolDsStatoElab(char polDsStatoElab) {
        ldbm0250.getPoli().setPolDsStatoElab(polDsStatoElab);
    }

    @Override
    public long getPolDsTsEndCptz() {
        return ldbm0250.getPoli().getPolDsTsEndCptz();
    }

    @Override
    public void setPolDsTsEndCptz(long polDsTsEndCptz) {
        ldbm0250.getPoli().setPolDsTsEndCptz(polDsTsEndCptz);
    }

    @Override
    public long getPolDsTsIniCptz() {
        return ldbm0250.getPoli().getPolDsTsIniCptz();
    }

    @Override
    public void setPolDsTsIniCptz(long polDsTsIniCptz) {
        ldbm0250.getPoli().setPolDsTsIniCptz(polDsTsIniCptz);
    }

    @Override
    public String getPolDsUtente() {
        return ldbm0250.getPoli().getPolDsUtente();
    }

    @Override
    public void setPolDsUtente(String polDsUtente) {
        ldbm0250.getPoli().setPolDsUtente(polDsUtente);
    }

    @Override
    public int getPolDsVer() {
        return ldbm0250.getPoli().getPolDsVer();
    }

    @Override
    public void setPolDsVer(int polDsVer) {
        ldbm0250.getPoli().setPolDsVer(polDsVer);
    }

    @Override
    public String getPolDtApplzConvDb() {
        return ldbm0250.getWs().getPoliDb().getUltConsCnbtDb();
    }

    @Override
    public void setPolDtApplzConvDb(String polDtApplzConvDb) {
        ldbm0250.getWs().getPoliDb().setUltConsCnbtDb(polDtApplzConvDb);
    }

    @Override
    public String getPolDtApplzConvDbObj() {
        if (ldbm0250.getWs().getIndPoli().getSpeMed() >= 0) {
            return getPolDtApplzConvDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolDtApplzConvDbObj(String polDtApplzConvDbObj) {
        if (polDtApplzConvDbObj != null) {
            setPolDtApplzConvDb(polDtApplzConvDbObj);
            ldbm0250.getWs().getIndPoli().setSpeMed(((short)0));
        }
        else {
            ldbm0250.getWs().getIndPoli().setSpeMed(((short)-1));
        }
    }

    @Override
    public String getPolDtDecorDb() {
        return ldbm0250.getWs().getPoliDb().getScadDb();
    }

    @Override
    public void setPolDtDecorDb(String polDtDecorDb) {
        ldbm0250.getWs().getPoliDb().setScadDb(polDtDecorDb);
    }

    @Override
    public String getPolDtEmisDb() {
        return ldbm0250.getWs().getPoliDb().getVarzTpIasDb();
    }

    @Override
    public void setPolDtEmisDb(String polDtEmisDb) {
        ldbm0250.getWs().getPoliDb().setVarzTpIasDb(polDtEmisDb);
    }

    @Override
    public String getPolDtEndEffDb() {
        return ldbm0250.getWs().getPoliDb().getDecorDb();
    }

    @Override
    public void setPolDtEndEffDb(String polDtEndEffDb) {
        ldbm0250.getWs().getPoliDb().setDecorDb(polDtEndEffDb);
    }

    @Override
    public String getPolDtIniEffDb() {
        return ldbm0250.getWs().getPoliDb().getEndEffDb();
    }

    @Override
    public void setPolDtIniEffDb(String polDtIniEffDb) {
        ldbm0250.getWs().getPoliDb().setEndEffDb(polDtIniEffDb);
    }

    @Override
    public String getPolDtIniVldtConvDb() {
        return ldbm0250.getWs().getPoliDb().getEffVarzStatTDb();
    }

    @Override
    public void setPolDtIniVldtConvDb(String polDtIniVldtConvDb) {
        ldbm0250.getWs().getPoliDb().setEffVarzStatTDb(polDtIniVldtConvDb);
    }

    @Override
    public String getPolDtIniVldtConvDbObj() {
        if (ldbm0250.getWs().getIndPoli().getDir() >= 0) {
            return getPolDtIniVldtConvDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolDtIniVldtConvDbObj(String polDtIniVldtConvDbObj) {
        if (polDtIniVldtConvDbObj != null) {
            setPolDtIniVldtConvDb(polDtIniVldtConvDbObj);
            ldbm0250.getWs().getIndPoli().setDir(((short)0));
        }
        else {
            ldbm0250.getWs().getIndPoli().setDir(((short)-1));
        }
    }

    @Override
    public String getPolDtIniVldtProdDb() {
        return ldbm0250.getWs().getPoliDb().getDecorPrestBanDb();
    }

    @Override
    public void setPolDtIniVldtProdDb(String polDtIniVldtProdDb) {
        ldbm0250.getWs().getPoliDb().setDecorPrestBanDb(polDtIniVldtProdDb);
    }

    @Override
    public String getPolDtPrescDb() {
        return ldbm0250.getWs().getPoliDb().getPrescDb();
    }

    @Override
    public void setPolDtPrescDb(String polDtPrescDb) {
        ldbm0250.getWs().getPoliDb().setPrescDb(polDtPrescDb);
    }

    @Override
    public String getPolDtPrescDbObj() {
        if (ldbm0250.getWs().getIndPoli().getDtEsiTit() >= 0) {
            return getPolDtPrescDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolDtPrescDbObj(String polDtPrescDbObj) {
        if (polDtPrescDbObj != null) {
            setPolDtPrescDb(polDtPrescDbObj);
            ldbm0250.getWs().getIndPoli().setDtEsiTit(((short)0));
        }
        else {
            ldbm0250.getWs().getIndPoli().setDtEsiTit(((short)-1));
        }
    }

    @Override
    public String getPolDtPropDb() {
        return ldbm0250.getWs().getPoliDb().getIniEffDb();
    }

    @Override
    public void setPolDtPropDb(String polDtPropDb) {
        ldbm0250.getWs().getPoliDb().setIniEffDb(polDtPropDb);
    }

    @Override
    public String getPolDtPropDbObj() {
        if (ldbm0250.getWs().getIndPoli().getDtEndCop() >= 0) {
            return getPolDtPropDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolDtPropDbObj(String polDtPropDbObj) {
        if (polDtPropDbObj != null) {
            setPolDtPropDb(polDtPropDbObj);
            ldbm0250.getWs().getIndPoli().setDtEndCop(((short)0));
        }
        else {
            ldbm0250.getWs().getIndPoli().setDtEndCop(((short)-1));
        }
    }

    @Override
    public String getPolDtScadDb() {
        return ldbm0250.getWs().getPoliDb().getNovaRgmFiscDb();
    }

    @Override
    public void setPolDtScadDb(String polDtScadDb) {
        ldbm0250.getWs().getPoliDb().setNovaRgmFiscDb(polDtScadDb);
    }

    @Override
    public String getPolDtScadDbObj() {
        if (ldbm0250.getWs().getIndPoli().getIntrMora() >= 0) {
            return getPolDtScadDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolDtScadDbObj(String polDtScadDbObj) {
        if (polDtScadDbObj != null) {
            setPolDtScadDb(polDtScadDbObj);
            ldbm0250.getWs().getIndPoli().setIntrMora(((short)0));
        }
        else {
            ldbm0250.getWs().getIndPoli().setIntrMora(((short)-1));
        }
    }

    @Override
    public int getPolDurAa() {
        return ldbm0250.getPoli().getPolDurAa().getPolDurAa();
    }

    @Override
    public void setPolDurAa(int polDurAa) {
        ldbm0250.getPoli().getPolDurAa().setPolDurAa(polDurAa);
    }

    @Override
    public Integer getPolDurAaObj() {
        if (ldbm0250.getWs().getIndPoli().getPreNet() >= 0) {
            return ((Integer)getPolDurAa());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolDurAaObj(Integer polDurAaObj) {
        if (polDurAaObj != null) {
            setPolDurAa(((int)polDurAaObj));
            ldbm0250.getWs().getIndPoli().setPreNet(((short)0));
        }
        else {
            ldbm0250.getWs().getIndPoli().setPreNet(((short)-1));
        }
    }

    @Override
    public int getPolDurGg() {
        return ldbm0250.getPoli().getPolDurGg().getPolDurGg();
    }

    @Override
    public void setPolDurGg(int polDurGg) {
        ldbm0250.getPoli().getPolDurGg().setPolDurGg(polDurGg);
    }

    @Override
    public Integer getPolDurGgObj() {
        if (ldbm0250.getWs().getIndPoli().getFrqMovi() >= 0) {
            return ((Integer)getPolDurGg());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolDurGgObj(Integer polDurGgObj) {
        if (polDurGgObj != null) {
            setPolDurGg(((int)polDurGgObj));
            ldbm0250.getWs().getIndPoli().setFrqMovi(((short)0));
        }
        else {
            ldbm0250.getWs().getIndPoli().setFrqMovi(((short)-1));
        }
    }

    @Override
    public int getPolDurMm() {
        return ldbm0250.getPoli().getPolDurMm().getPolDurMm();
    }

    @Override
    public void setPolDurMm(int polDurMm) {
        ldbm0250.getPoli().getPolDurMm().setPolDurMm(polDurMm);
    }

    @Override
    public Integer getPolDurMmObj() {
        if (ldbm0250.getWs().getIndPoli().getIntrFraz() >= 0) {
            return ((Integer)getPolDurMm());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolDurMmObj(Integer polDurMmObj) {
        if (polDurMmObj != null) {
            setPolDurMm(((int)polDurMmObj));
            ldbm0250.getWs().getIndPoli().setIntrFraz(((short)0));
        }
        else {
            ldbm0250.getWs().getIndPoli().setIntrFraz(((short)-1));
        }
    }

    @Override
    public char getPolFlAmmbMovi() {
        return ldbm0250.getPoli().getPolFlAmmbMovi();
    }

    @Override
    public void setPolFlAmmbMovi(char polFlAmmbMovi) {
        ldbm0250.getPoli().setPolFlAmmbMovi(polFlAmmbMovi);
    }

    @Override
    public Character getPolFlAmmbMoviObj() {
        if (ldbm0250.getWs().getIndPoli().getImpTfr() >= 0) {
            return ((Character)getPolFlAmmbMovi());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlAmmbMoviObj(Character polFlAmmbMoviObj) {
        if (polFlAmmbMoviObj != null) {
            setPolFlAmmbMovi(((char)polFlAmmbMoviObj));
            ldbm0250.getWs().getIndPoli().setImpTfr(((short)0));
        }
        else {
            ldbm0250.getWs().getIndPoli().setImpTfr(((short)-1));
        }
    }

    @Override
    public char getPolFlCopFinanz() {
        return ldbm0250.getPoli().getPolFlCopFinanz();
    }

    @Override
    public void setPolFlCopFinanz(char polFlCopFinanz) {
        ldbm0250.getPoli().setPolFlCopFinanz(polFlCopFinanz);
    }

    @Override
    public Character getPolFlCopFinanzObj() {
        if (ldbm0250.getWs().getIndPoli().getSoprProf() >= 0) {
            return ((Character)getPolFlCopFinanz());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlCopFinanzObj(Character polFlCopFinanzObj) {
        if (polFlCopFinanzObj != null) {
            setPolFlCopFinanz(((char)polFlCopFinanzObj));
            ldbm0250.getWs().getIndPoli().setSoprProf(((short)0));
        }
        else {
            ldbm0250.getWs().getIndPoli().setSoprProf(((short)-1));
        }
    }

    @Override
    public char getPolFlCumPreCntr() {
        return ldbm0250.getPoli().getPolFlCumPreCntr();
    }

    @Override
    public void setPolFlCumPreCntr(char polFlCumPreCntr) {
        ldbm0250.getPoli().setPolFlCumPreCntr(polFlCumPreCntr);
    }

    @Override
    public Character getPolFlCumPreCntrObj() {
        if (ldbm0250.getWs().getIndPoli().getImpAder() >= 0) {
            return ((Character)getPolFlCumPreCntr());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlCumPreCntrObj(Character polFlCumPreCntrObj) {
        if (polFlCumPreCntrObj != null) {
            setPolFlCumPreCntr(((char)polFlCumPreCntrObj));
            ldbm0250.getWs().getIndPoli().setImpAder(((short)0));
        }
        else {
            ldbm0250.getWs().getIndPoli().setImpAder(((short)-1));
        }
    }

    @Override
    public char getPolFlEstas() {
        return ldbm0250.getPoli().getPolFlEstas();
    }

    @Override
    public void setPolFlEstas(char polFlEstas) {
        ldbm0250.getPoli().setPolFlEstas(polFlEstas);
    }

    @Override
    public Character getPolFlEstasObj() {
        if (ldbm0250.getWs().getIndPoli().getSoprSan() >= 0) {
            return ((Character)getPolFlEstas());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlEstasObj(Character polFlEstasObj) {
        if (polFlEstasObj != null) {
            setPolFlEstas(((char)polFlEstasObj));
            ldbm0250.getWs().getIndPoli().setSoprSan(((short)0));
        }
        else {
            ldbm0250.getWs().getIndPoli().setSoprSan(((short)-1));
        }
    }

    @Override
    public char getPolFlFntAder() {
        return ldbm0250.getPoli().getPolFlFntAder();
    }

    @Override
    public void setPolFlFntAder(char polFlFntAder) {
        ldbm0250.getPoli().setPolFlFntAder(polFlFntAder);
    }

    @Override
    public Character getPolFlFntAderObj() {
        if (ldbm0250.getWs().getIndPoli().getProvAcq1aa() >= 0) {
            return ((Character)getPolFlFntAder());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlFntAderObj(Character polFlFntAderObj) {
        if (polFlFntAderObj != null) {
            setPolFlFntAder(((char)polFlFntAderObj));
            ldbm0250.getWs().getIndPoli().setProvAcq1aa(((short)0));
        }
        else {
            ldbm0250.getWs().getIndPoli().setProvAcq1aa(((short)-1));
        }
    }

    @Override
    public char getPolFlFntAz() {
        return ldbm0250.getPoli().getPolFlFntAz();
    }

    @Override
    public void setPolFlFntAz(char polFlFntAz) {
        ldbm0250.getPoli().setPolFlFntAz(polFlFntAz);
    }

    @Override
    public Character getPolFlFntAzObj() {
        if (ldbm0250.getWs().getIndPoli().getCarInc() >= 0) {
            return ((Character)getPolFlFntAz());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlFntAzObj(Character polFlFntAzObj) {
        if (polFlFntAzObj != null) {
            setPolFlFntAz(((char)polFlFntAzObj));
            ldbm0250.getWs().getIndPoli().setCarInc(((short)0));
        }
        else {
            ldbm0250.getWs().getIndPoli().setCarInc(((short)-1));
        }
    }

    @Override
    public char getPolFlFntTfr() {
        return ldbm0250.getPoli().getPolFlFntTfr();
    }

    @Override
    public void setPolFlFntTfr(char polFlFntTfr) {
        ldbm0250.getPoli().setPolFlFntTfr(polFlFntTfr);
    }

    @Override
    public Character getPolFlFntTfrObj() {
        if (ldbm0250.getWs().getIndPoli().getProvAcq2aa() >= 0) {
            return ((Character)getPolFlFntTfr());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlFntTfrObj(Character polFlFntTfrObj) {
        if (polFlFntTfrObj != null) {
            setPolFlFntTfr(((char)polFlFntTfrObj));
            ldbm0250.getWs().getIndPoli().setProvAcq2aa(((short)0));
        }
        else {
            ldbm0250.getWs().getIndPoli().setProvAcq2aa(((short)-1));
        }
    }

    @Override
    public char getPolFlFntVolo() {
        return ldbm0250.getPoli().getPolFlFntVolo();
    }

    @Override
    public void setPolFlFntVolo(char polFlFntVolo) {
        ldbm0250.getPoli().setPolFlFntVolo(polFlFntVolo);
    }

    @Override
    public Character getPolFlFntVoloObj() {
        if (ldbm0250.getWs().getIndPoli().getProvRicor() >= 0) {
            return ((Character)getPolFlFntVolo());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlFntVoloObj(Character polFlFntVoloObj) {
        if (polFlFntVoloObj != null) {
            setPolFlFntVolo(((char)polFlFntVoloObj));
            ldbm0250.getWs().getIndPoli().setProvRicor(((short)0));
        }
        else {
            ldbm0250.getWs().getIndPoli().setProvRicor(((short)-1));
        }
    }

    @Override
    public char getPolFlPoliBundling() {
        return ldbm0250.getPoli().getPolFlPoliBundling();
    }

    @Override
    public void setPolFlPoliBundling(char polFlPoliBundling) {
        ldbm0250.getPoli().setPolFlPoliBundling(polFlPoliBundling);
    }

    @Override
    public Character getPolFlPoliBundlingObj() {
        if (ldbm0250.getWs().getIndPoli().getNumGgRival() >= 0) {
            return ((Character)getPolFlPoliBundling());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlPoliBundlingObj(Character polFlPoliBundlingObj) {
        if (polFlPoliBundlingObj != null) {
            setPolFlPoliBundling(((char)polFlPoliBundlingObj));
            ldbm0250.getWs().getIndPoli().setNumGgRival(((short)0));
        }
        else {
            ldbm0250.getWs().getIndPoli().setNumGgRival(((short)-1));
        }
    }

    @Override
    public char getPolFlPoliCpiPr() {
        return ldbm0250.getPoli().getPolFlPoliCpiPr();
    }

    @Override
    public void setPolFlPoliCpiPr(char polFlPoliCpiPr) {
        ldbm0250.getPoli().setPolFlPoliCpiPr(polFlPoliCpiPr);
    }

    @Override
    public Character getPolFlPoliCpiPrObj() {
        if (ldbm0250.getWs().getIndPoli().getNumGgRitardoPag() >= 0) {
            return ((Character)getPolFlPoliCpiPr());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlPoliCpiPrObj(Character polFlPoliCpiPrObj) {
        if (polFlPoliCpiPrObj != null) {
            setPolFlPoliCpiPr(((char)polFlPoliCpiPrObj));
            ldbm0250.getWs().getIndPoli().setNumGgRitardoPag(((short)0));
        }
        else {
            ldbm0250.getWs().getIndPoli().setNumGgRitardoPag(((short)-1));
        }
    }

    @Override
    public char getPolFlPoliIfp() {
        return ldbm0250.getPoli().getPolFlPoliIfp();
    }

    @Override
    public void setPolFlPoliIfp(char polFlPoliIfp) {
        ldbm0250.getPoli().setPolFlPoliIfp(polFlPoliIfp);
    }

    @Override
    public Character getPolFlPoliIfpObj() {
        if (ldbm0250.getWs().getIndPoli().getCnbtAntirac() >= 0) {
            return ((Character)getPolFlPoliIfp());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlPoliIfpObj(Character polFlPoliIfpObj) {
        if (polFlPoliIfpObj != null) {
            setPolFlPoliIfp(((char)polFlPoliIfpObj));
            ldbm0250.getWs().getIndPoli().setCnbtAntirac(((short)0));
        }
        else {
            ldbm0250.getWs().getIndPoli().setCnbtAntirac(((short)-1));
        }
    }

    @Override
    public char getPolFlQuestAdegzAss() {
        return ldbm0250.getPoli().getPolFlQuestAdegzAss();
    }

    @Override
    public void setPolFlQuestAdegzAss(char polFlQuestAdegzAss) {
        ldbm0250.getPoli().setPolFlQuestAdegzAss(polFlQuestAdegzAss);
    }

    @Override
    public Character getPolFlQuestAdegzAssObj() {
        if (ldbm0250.getWs().getIndPoli().getTotIntrPrest() >= 0) {
            return ((Character)getPolFlQuestAdegzAss());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlQuestAdegzAssObj(Character polFlQuestAdegzAssObj) {
        if (polFlQuestAdegzAssObj != null) {
            setPolFlQuestAdegzAss(((char)polFlQuestAdegzAssObj));
            ldbm0250.getWs().getIndPoli().setTotIntrPrest(((short)0));
        }
        else {
            ldbm0250.getWs().getIndPoli().setTotIntrPrest(((short)-1));
        }
    }

    @Override
    public char getPolFlRshComun() {
        return ldbm0250.getPoli().getPolFlRshComun();
    }

    @Override
    public void setPolFlRshComun(char polFlRshComun) {
        ldbm0250.getPoli().setPolFlRshComun(polFlRshComun);
    }

    @Override
    public char getPolFlRshComunCond() {
        return ldbm0250.getPoli().getPolFlRshComunCond();
    }

    @Override
    public void setPolFlRshComunCond(char polFlRshComunCond) {
        ldbm0250.getPoli().setPolFlRshComunCond(polFlRshComunCond);
    }

    @Override
    public Character getPolFlRshComunCondObj() {
        if (ldbm0250.getWs().getIndPoli().getSoprTec() >= 0) {
            return ((Character)getPolFlRshComunCond());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlRshComunCondObj(Character polFlRshComunCondObj) {
        if (polFlRshComunCondObj != null) {
            setPolFlRshComunCond(((char)polFlRshComunCondObj));
            ldbm0250.getWs().getIndPoli().setSoprTec(((short)0));
        }
        else {
            ldbm0250.getWs().getIndPoli().setSoprTec(((short)-1));
        }
    }

    @Override
    public Character getPolFlRshComunObj() {
        if (ldbm0250.getWs().getIndPoli().getSoprSpo() >= 0) {
            return ((Character)getPolFlRshComun());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlRshComunObj(Character polFlRshComunObj) {
        if (polFlRshComunObj != null) {
            setPolFlRshComun(((char)polFlRshComunObj));
            ldbm0250.getWs().getIndPoli().setSoprSpo(((short)0));
        }
        else {
            ldbm0250.getWs().getIndPoli().setSoprSpo(((short)-1));
        }
    }

    @Override
    public char getPolFlScudoFisc() {
        return ldbm0250.getPoli().getPolFlScudoFisc();
    }

    @Override
    public void setPolFlScudoFisc(char polFlScudoFisc) {
        ldbm0250.getPoli().setPolFlScudoFisc(polFlScudoFisc);
    }

    @Override
    public Character getPolFlScudoFiscObj() {
        if (ldbm0250.getWs().getIndPoli().getManfeeAntic() >= 0) {
            return ((Character)getPolFlScudoFisc());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlScudoFiscObj(Character polFlScudoFiscObj) {
        if (polFlScudoFiscObj != null) {
            setPolFlScudoFisc(((char)polFlScudoFiscObj));
            ldbm0250.getWs().getIndPoli().setManfeeAntic(((short)0));
        }
        else {
            ldbm0250.getWs().getIndPoli().setManfeeAntic(((short)-1));
        }
    }

    @Override
    public char getPolFlTfrStrc() {
        return ldbm0250.getPoli().getPolFlTfrStrc();
    }

    @Override
    public void setPolFlTfrStrc(char polFlTfrStrc) {
        ldbm0250.getPoli().setPolFlTfrStrc(polFlTfrStrc);
    }

    @Override
    public Character getPolFlTfrStrcObj() {
        if (ldbm0250.getWs().getIndPoli().getManfeeRec() >= 0) {
            return ((Character)getPolFlTfrStrc());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlTfrStrcObj(Character polFlTfrStrcObj) {
        if (polFlTfrStrcObj != null) {
            setPolFlTfrStrc(((char)polFlTfrStrcObj));
            ldbm0250.getWs().getIndPoli().setManfeeRec(((short)0));
        }
        else {
            ldbm0250.getWs().getIndPoli().setManfeeRec(((short)-1));
        }
    }

    @Override
    public char getPolFlTrasfe() {
        return ldbm0250.getPoli().getPolFlTrasfe();
    }

    @Override
    public void setPolFlTrasfe(char polFlTrasfe) {
        ldbm0250.getPoli().setPolFlTrasfe(polFlTrasfe);
    }

    @Override
    public Character getPolFlTrasfeObj() {
        if (ldbm0250.getWs().getIndPoli().getManfeeRicor() >= 0) {
            return ((Character)getPolFlTrasfe());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlTrasfeObj(Character polFlTrasfeObj) {
        if (polFlTrasfeObj != null) {
            setPolFlTrasfe(((char)polFlTrasfeObj));
            ldbm0250.getWs().getIndPoli().setManfeeRicor(((short)0));
        }
        else {
            ldbm0250.getWs().getIndPoli().setManfeeRicor(((short)-1));
        }
    }

    @Override
    public String getPolFlVerProd() {
        return ldbm0250.getPoli().getPolFlVerProd();
    }

    @Override
    public void setPolFlVerProd(String polFlVerProd) {
        ldbm0250.getPoli().setPolFlVerProd(polFlVerProd);
    }

    @Override
    public String getPolFlVerProdObj() {
        if (ldbm0250.getWs().getIndPoli().getCodDvs() >= 0) {
            return getPolFlVerProd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlVerProdObj(String polFlVerProdObj) {
        if (polFlVerProdObj != null) {
            setPolFlVerProd(polFlVerProdObj);
            ldbm0250.getWs().getIndPoli().setCodDvs(((short)0));
        }
        else {
            ldbm0250.getWs().getIndPoli().setCodDvs(((short)-1));
        }
    }

    @Override
    public char getPolFlVndBundle() {
        return ldbm0250.getPoli().getPolFlVndBundle();
    }

    @Override
    public void setPolFlVndBundle(char polFlVndBundle) {
        ldbm0250.getPoli().setPolFlVndBundle(polFlVndBundle);
    }

    @Override
    public Character getPolFlVndBundleObj() {
        if (ldbm0250.getWs().getIndPoli().getRemunAss() >= 0) {
            return ((Character)getPolFlVndBundle());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlVndBundleObj(Character polFlVndBundleObj) {
        if (polFlVndBundleObj != null) {
            setPolFlVndBundle(((char)polFlVndBundleObj));
            ldbm0250.getWs().getIndPoli().setRemunAss(((short)0));
        }
        else {
            ldbm0250.getWs().getIndPoli().setRemunAss(((short)-1));
        }
    }

    @Override
    public String getPolIbBs() {
        return ldbm0250.getPoli().getPolIbBs();
    }

    @Override
    public void setPolIbBs(String polIbBs) {
        ldbm0250.getPoli().setPolIbBs(polIbBs);
    }

    @Override
    public String getPolIbBsObj() {
        if (ldbm0250.getWs().getIndPoli().getCommisInter() >= 0) {
            return getPolIbBs();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolIbBsObj(String polIbBsObj) {
        if (polIbBsObj != null) {
            setPolIbBs(polIbBsObj);
            ldbm0250.getWs().getIndPoli().setCommisInter(((short)0));
        }
        else {
            ldbm0250.getWs().getIndPoli().setCommisInter(((short)-1));
        }
    }

    @Override
    public String getPolIbOgg() {
        return ldbm0250.getPoli().getPolIbOgg();
    }

    @Override
    public void setPolIbOgg(String polIbOgg) {
        ldbm0250.getPoli().setPolIbOgg(polIbOgg);
    }

    @Override
    public String getPolIbOggObj() {
        if (ldbm0250.getWs().getIndPoli().getDtIniCop() >= 0) {
            return getPolIbOgg();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolIbOggObj(String polIbOggObj) {
        if (polIbOggObj != null) {
            setPolIbOgg(polIbOggObj);
            ldbm0250.getWs().getIndPoli().setDtIniCop(((short)0));
        }
        else {
            ldbm0250.getWs().getIndPoli().setDtIniCop(((short)-1));
        }
    }

    @Override
    public String getPolIbProp() {
        return ldbm0250.getPoli().getPolIbProp();
    }

    @Override
    public void setPolIbProp(String polIbProp) {
        ldbm0250.getPoli().setPolIbProp(polIbProp);
    }

    @Override
    public int getPolIdAccComm() {
        return ldbm0250.getPoli().getPolIdAccComm().getPolIdAccComm();
    }

    @Override
    public void setPolIdAccComm(int polIdAccComm) {
        ldbm0250.getPoli().getPolIdAccComm().setPolIdAccComm(polIdAccComm);
    }

    @Override
    public Integer getPolIdAccCommObj() {
        if (ldbm0250.getWs().getIndPoli().getImpTfrStrc() >= 0) {
            return ((Integer)getPolIdAccComm());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolIdAccCommObj(Integer polIdAccCommObj) {
        if (polIdAccCommObj != null) {
            setPolIdAccComm(((int)polIdAccCommObj));
            ldbm0250.getWs().getIndPoli().setImpTfrStrc(((short)0));
        }
        else {
            ldbm0250.getWs().getIndPoli().setImpTfrStrc(((short)-1));
        }
    }

    @Override
    public int getPolIdMoviChiu() {
        return ldbm0250.getPoli().getPolIdMoviChiu().getPolIdMoviChiu();
    }

    @Override
    public void setPolIdMoviChiu(int polIdMoviChiu) {
        ldbm0250.getPoli().getPolIdMoviChiu().setPolIdMoviChiu(polIdMoviChiu);
    }

    @Override
    public Integer getPolIdMoviChiuObj() {
        if (ldbm0250.getWs().getIndPoli().getIdMoviChiu() >= 0) {
            return ((Integer)getPolIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolIdMoviChiuObj(Integer polIdMoviChiuObj) {
        if (polIdMoviChiuObj != null) {
            setPolIdMoviChiu(((int)polIdMoviChiuObj));
            ldbm0250.getWs().getIndPoli().setIdMoviChiu(((short)0));
        }
        else {
            ldbm0250.getWs().getIndPoli().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getPolIdMoviCrz() {
        return ldbm0250.getPoli().getPolIdMoviCrz();
    }

    @Override
    public void setPolIdMoviCrz(int polIdMoviCrz) {
        ldbm0250.getPoli().setPolIdMoviCrz(polIdMoviCrz);
    }

    @Override
    public int getPolIdPoli() {
        return ldbm0250.getPoli().getPolIdPoli();
    }

    @Override
    public void setPolIdPoli(int polIdPoli) {
        ldbm0250.getPoli().setPolIdPoli(polIdPoli);
    }

    @Override
    public char getPolIndPoliPrinColl() {
        return ldbm0250.getPoli().getPolIndPoliPrinColl();
    }

    @Override
    public void setPolIndPoliPrinColl(char polIndPoliPrinColl) {
        ldbm0250.getPoli().setPolIndPoliPrinColl(polIndPoliPrinColl);
    }

    @Override
    public Character getPolIndPoliPrinCollObj() {
        if (ldbm0250.getWs().getIndPoli().getAcqExp() >= 0) {
            return ((Character)getPolIndPoliPrinColl());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolIndPoliPrinCollObj(Character polIndPoliPrinCollObj) {
        if (polIndPoliPrinCollObj != null) {
            setPolIndPoliPrinColl(((char)polIndPoliPrinCollObj));
            ldbm0250.getWs().getIndPoli().setAcqExp(((short)0));
        }
        else {
            ldbm0250.getWs().getIndPoli().setAcqExp(((short)-1));
        }
    }

    @Override
    public AfDecimal getPolSpeMed() {
        return ldbm0250.getPoli().getPolSpeMed().getPolSpeMed();
    }

    @Override
    public void setPolSpeMed(AfDecimal polSpeMed) {
        ldbm0250.getPoli().getPolSpeMed().setPolSpeMed(polSpeMed.copy());
    }

    @Override
    public AfDecimal getPolSpeMedObj() {
        if (ldbm0250.getWs().getIndPoli().getPreTot() >= 0) {
            return getPolSpeMed();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolSpeMedObj(AfDecimal polSpeMedObj) {
        if (polSpeMedObj != null) {
            setPolSpeMed(new AfDecimal(polSpeMedObj, 15, 3));
            ldbm0250.getWs().getIndPoli().setPreTot(((short)0));
        }
        else {
            ldbm0250.getWs().getIndPoli().setPreTot(((short)-1));
        }
    }

    @Override
    public String getPolSubcatProd() {
        return ldbm0250.getPoli().getPolSubcatProd();
    }

    @Override
    public void setPolSubcatProd(String polSubcatProd) {
        ldbm0250.getPoli().setPolSubcatProd(polSubcatProd);
    }

    @Override
    public String getPolSubcatProdObj() {
        if (ldbm0250.getWs().getIndPoli().getCarIas() >= 0) {
            return getPolSubcatProd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolSubcatProdObj(String polSubcatProdObj) {
        if (polSubcatProdObj != null) {
            setPolSubcatProd(polSubcatProdObj);
            ldbm0250.getWs().getIndPoli().setCarIas(((short)0));
        }
        else {
            ldbm0250.getWs().getIndPoli().setCarIas(((short)-1));
        }
    }

    @Override
    public String getPolTpApplzDir() {
        return ldbm0250.getPoli().getPolTpApplzDir();
    }

    @Override
    public void setPolTpApplzDir(String polTpApplzDir) {
        ldbm0250.getPoli().setPolTpApplzDir(polTpApplzDir);
    }

    @Override
    public String getPolTpApplzDirObj() {
        if (ldbm0250.getWs().getIndPoli().getSoprAlt() >= 0) {
            return getPolTpApplzDir();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolTpApplzDirObj(String polTpApplzDirObj) {
        if (polTpApplzDirObj != null) {
            setPolTpApplzDir(polTpApplzDirObj);
            ldbm0250.getWs().getIndPoli().setSoprAlt(((short)0));
        }
        else {
            ldbm0250.getWs().getIndPoli().setSoprAlt(((short)-1));
        }
    }

    @Override
    public String getPolTpFrmAssva() {
        return ldbm0250.getPoli().getPolTpFrmAssva();
    }

    @Override
    public void setPolTpFrmAssva(String polTpFrmAssva) {
        ldbm0250.getPoli().setPolTpFrmAssva(polTpFrmAssva);
    }

    @Override
    public String getPolTpLivGenzTit() {
        return ldbm0250.getPoli().getPolTpLivGenzTit();
    }

    @Override
    public void setPolTpLivGenzTit(String polTpLivGenzTit) {
        ldbm0250.getPoli().setPolTpLivGenzTit(polTpLivGenzTit);
    }

    @Override
    public String getPolTpOpzAScad() {
        return ldbm0250.getPoli().getPolTpOpzAScad();
    }

    @Override
    public void setPolTpOpzAScad(String polTpOpzAScad) {
        ldbm0250.getPoli().setPolTpOpzAScad(polTpOpzAScad);
    }

    @Override
    public String getPolTpOpzAScadObj() {
        if (ldbm0250.getWs().getIndPoli().getProvInc() >= 0) {
            return getPolTpOpzAScad();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolTpOpzAScadObj(String polTpOpzAScadObj) {
        if (polTpOpzAScadObj != null) {
            setPolTpOpzAScad(polTpOpzAScadObj);
            ldbm0250.getWs().getIndPoli().setProvInc(((short)0));
        }
        else {
            ldbm0250.getWs().getIndPoli().setProvInc(((short)-1));
        }
    }

    @Override
    public String getPolTpPoli() {
        return ldbm0250.getPoli().getPolTpPoli();
    }

    @Override
    public void setPolTpPoli(String polTpPoli) {
        ldbm0250.getPoli().setPolTpPoli(polTpPoli);
    }

    @Override
    public String getPolTpPtfEstno() {
        return ldbm0250.getPoli().getPolTpPtfEstno();
    }

    @Override
    public void setPolTpPtfEstno(String polTpPtfEstno) {
        ldbm0250.getPoli().setPolTpPtfEstno(polTpPtfEstno);
    }

    @Override
    public String getPolTpPtfEstnoObj() {
        if (ldbm0250.getWs().getIndPoli().getImpAz() >= 0) {
            return getPolTpPtfEstno();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolTpPtfEstnoObj(String polTpPtfEstnoObj) {
        if (polTpPtfEstnoObj != null) {
            setPolTpPtfEstno(polTpPtfEstnoObj);
            ldbm0250.getWs().getIndPoli().setImpAz(((short)0));
        }
        else {
            ldbm0250.getWs().getIndPoli().setImpAz(((short)-1));
        }
    }

    @Override
    public String getPolTpRgmFisc() {
        return ldbm0250.getPoli().getPolTpRgmFisc();
    }

    @Override
    public void setPolTpRgmFisc(String polTpRgmFisc) {
        ldbm0250.getPoli().setPolTpRgmFisc(polTpRgmFisc);
    }

    @Override
    public String getPolTpRgmFiscObj() {
        if (ldbm0250.getWs().getIndPoli().getTax() >= 0) {
            return getPolTpRgmFisc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolTpRgmFiscObj(String polTpRgmFiscObj) {
        if (polTpRgmFiscObj != null) {
            setPolTpRgmFisc(polTpRgmFiscObj);
            ldbm0250.getWs().getIndPoli().setTax(((short)0));
        }
        else {
            ldbm0250.getWs().getIndPoli().setTax(((short)-1));
        }
    }

    @Override
    public int getStbCodCompAnia() {
        return ldbm0250.getStatOggBus().getStbCodCompAnia();
    }

    @Override
    public void setStbCodCompAnia(int stbCodCompAnia) {
        ldbm0250.getStatOggBus().setStbCodCompAnia(stbCodCompAnia);
    }

    @Override
    public char getStbDsOperSql() {
        return ldbm0250.getStatOggBus().getStbDsOperSql();
    }

    @Override
    public void setStbDsOperSql(char stbDsOperSql) {
        ldbm0250.getStatOggBus().setStbDsOperSql(stbDsOperSql);
    }

    @Override
    public long getStbDsRiga() {
        return ldbm0250.getStatOggBus().getStbDsRiga();
    }

    @Override
    public void setStbDsRiga(long stbDsRiga) {
        ldbm0250.getStatOggBus().setStbDsRiga(stbDsRiga);
    }

    @Override
    public char getStbDsStatoElab() {
        return ldbm0250.getStatOggBus().getStbDsStatoElab();
    }

    @Override
    public void setStbDsStatoElab(char stbDsStatoElab) {
        ldbm0250.getStatOggBus().setStbDsStatoElab(stbDsStatoElab);
    }

    @Override
    public long getStbDsTsEndCptz() {
        return ldbm0250.getStatOggBus().getStbDsTsEndCptz();
    }

    @Override
    public void setStbDsTsEndCptz(long stbDsTsEndCptz) {
        ldbm0250.getStatOggBus().setStbDsTsEndCptz(stbDsTsEndCptz);
    }

    @Override
    public long getStbDsTsIniCptz() {
        return ldbm0250.getStatOggBus().getStbDsTsIniCptz();
    }

    @Override
    public void setStbDsTsIniCptz(long stbDsTsIniCptz) {
        ldbm0250.getStatOggBus().setStbDsTsIniCptz(stbDsTsIniCptz);
    }

    @Override
    public String getStbDsUtente() {
        return ldbm0250.getStatOggBus().getStbDsUtente();
    }

    @Override
    public void setStbDsUtente(String stbDsUtente) {
        ldbm0250.getStatOggBus().setStbDsUtente(stbDsUtente);
    }

    @Override
    public int getStbDsVer() {
        return ldbm0250.getStatOggBus().getStbDsVer();
    }

    @Override
    public void setStbDsVer(int stbDsVer) {
        ldbm0250.getStatOggBus().setStbDsVer(stbDsVer);
    }

    @Override
    public String getStbDtEndEffDb() {
        return ldbm0250.getWs().getIdbvstb3().getStbDtEndEffDb();
    }

    @Override
    public void setStbDtEndEffDb(String stbDtEndEffDb) {
        ldbm0250.getWs().getIdbvstb3().setStbDtEndEffDb(stbDtEndEffDb);
    }

    @Override
    public String getStbDtIniEffDb() {
        return ldbm0250.getWs().getIdbvstb3().getStbDtIniEffDb();
    }

    @Override
    public void setStbDtIniEffDb(String stbDtIniEffDb) {
        ldbm0250.getWs().getIdbvstb3().setStbDtIniEffDb(stbDtIniEffDb);
    }

    @Override
    public int getStbIdMoviChiu() {
        return ldbm0250.getStatOggBus().getStbIdMoviChiu().getStbIdMoviChiu();
    }

    @Override
    public void setStbIdMoviChiu(int stbIdMoviChiu) {
        ldbm0250.getStatOggBus().getStbIdMoviChiu().setStbIdMoviChiu(stbIdMoviChiu);
    }

    @Override
    public Integer getStbIdMoviChiuObj() {
        if (ldbm0250.getWs().getIndStbIdMoviChiu() >= 0) {
            return ((Integer)getStbIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setStbIdMoviChiuObj(Integer stbIdMoviChiuObj) {
        if (stbIdMoviChiuObj != null) {
            setStbIdMoviChiu(((int)stbIdMoviChiuObj));
            ldbm0250.getWs().setIndStbIdMoviChiu(((short)0));
        }
        else {
            ldbm0250.getWs().setIndStbIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getStbIdMoviCrz() {
        return ldbm0250.getStatOggBus().getStbIdMoviCrz();
    }

    @Override
    public void setStbIdMoviCrz(int stbIdMoviCrz) {
        ldbm0250.getStatOggBus().setStbIdMoviCrz(stbIdMoviCrz);
    }

    @Override
    public int getStbIdOgg() {
        return ldbm0250.getStatOggBus().getStbIdOgg();
    }

    @Override
    public void setStbIdOgg(int stbIdOgg) {
        ldbm0250.getStatOggBus().setStbIdOgg(stbIdOgg);
    }

    @Override
    public int getStbIdStatOggBus() {
        return ldbm0250.getStatOggBus().getStbIdStatOggBus();
    }

    @Override
    public void setStbIdStatOggBus(int stbIdStatOggBus) {
        ldbm0250.getStatOggBus().setStbIdStatOggBus(stbIdStatOggBus);
    }

    @Override
    public String getStbTpCaus() {
        return ldbm0250.getStatOggBus().getStbTpCaus();
    }

    @Override
    public void setStbTpCaus(String stbTpCaus) {
        ldbm0250.getStatOggBus().setStbTpCaus(stbTpCaus);
    }

    @Override
    public String getStbTpOgg() {
        return ldbm0250.getStatOggBus().getStbTpOgg();
    }

    @Override
    public void setStbTpOgg(String stbTpOgg) {
        ldbm0250.getStatOggBus().setStbTpOgg(stbTpOgg);
    }

    @Override
    public String getStbTpStatBus() {
        return ldbm0250.getStatOggBus().getStbTpStatBus();
    }

    @Override
    public void setStbTpStatBus(String stbTpStatBus) {
        ldbm0250.getStatOggBus().setStbTpStatBus(stbTpStatBus);
    }

    @Override
    public String getWlbCodProd() {
        throw new FieldNotMappedException("wlbCodProd");
    }

    @Override
    public void setWlbCodProd(String wlbCodProd) {
        throw new FieldNotMappedException("wlbCodProd");
    }

    @Override
    public String getWlbIbAdeFirst() {
        throw new FieldNotMappedException("wlbIbAdeFirst");
    }

    @Override
    public void setWlbIbAdeFirst(String wlbIbAdeFirst) {
        throw new FieldNotMappedException("wlbIbAdeFirst");
    }

    @Override
    public String getWlbIbAdeLast() {
        throw new FieldNotMappedException("wlbIbAdeLast");
    }

    @Override
    public void setWlbIbAdeLast(String wlbIbAdeLast) {
        throw new FieldNotMappedException("wlbIbAdeLast");
    }

    @Override
    public String getWlbIbPoliFirst() {
        throw new FieldNotMappedException("wlbIbPoliFirst");
    }

    @Override
    public void setWlbIbPoliFirst(String wlbIbPoliFirst) {
        throw new FieldNotMappedException("wlbIbPoliFirst");
    }

    @Override
    public String getWlbIbPoliLast() {
        throw new FieldNotMappedException("wlbIbPoliLast");
    }

    @Override
    public void setWlbIbPoliLast(String wlbIbPoliLast) {
        throw new FieldNotMappedException("wlbIbPoliLast");
    }

    @Override
    public String getWsDtPtfX() {
        return ldbm0250.getWs().getWsDtPtfX();
    }

    @Override
    public void setWsDtPtfX(String wsDtPtfX) {
        ldbm0250.getWs().setWsDtPtfX(wsDtPtfX);
    }

    @Override
    public long getWsDtTsPtf() {
        return ldbm0250.getWs().getWsDtTsPtf();
    }

    @Override
    public void setWsDtTsPtf(long wsDtTsPtf) {
        ldbm0250.getWs().setWsDtTsPtf(wsDtTsPtf);
    }

    @Override
    public long getWsDtTsPtfPre() {
        return ldbm0250.getWs().getWsDtTsPtfPre();
    }

    @Override
    public void setWsDtTsPtfPre(long wsDtTsPtfPre) {
        ldbm0250.getWs().setWsDtTsPtfPre(wsDtTsPtfPre);
    }

    @Override
    public String getWsTpFrmAssva1() {
        return ldbm0250.getWs().getWsTpFrmAssva1();
    }

    @Override
    public void setWsTpFrmAssva1(String wsTpFrmAssva1) {
        ldbm0250.getWs().setWsTpFrmAssva1(wsTpFrmAssva1);
    }

    @Override
    public String getWsTpFrmAssva2() {
        return ldbm0250.getWs().getWsTpFrmAssva2();
    }

    @Override
    public void setWsTpFrmAssva2(String wsTpFrmAssva2) {
        ldbm0250.getWs().setWsTpFrmAssva2(wsTpFrmAssva2);
    }
}
