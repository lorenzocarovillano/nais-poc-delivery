package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.GruArzDao;
import it.accenture.jnais.commons.data.to.IGruArz;
import it.accenture.jnais.copy.GruArzDb;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.GruArz;
import it.accenture.jnais.ws.Idsv0003;

/**Original name: IDES0020<br>
 * <pre>AUTHOR.        ATS NAPOLI.
 * DATE-WRITTEN.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE
 *  F A S E         : GESTIONE TABELLA GRU_ARZ
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Ides0020 extends Program implements IGruArz {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private GruArzDao gruArzDao = new GruArzDao(dbAccessStatus);
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: GRU-ARZ-DB
    private GruArzDb gruArzDb = new GruArzDb();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: GRU-ARZ
    private GruArz gruArz;

    //==== METHODS ====
    /**Original name: PROGRAM_IDES0020_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, GruArz gruArz) {
        this.idsv0003 = idsv0003;
        this.gruArz = gruArz;
        // COB_CODE: PERFORM A000-INIZIO              THRU A000-EX.
        a000Inizio();
        // COB_CODE: PERFORM A300-ELABORA             THRU A300-EX
        a300Elabora();
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Ides0020 getInstance() {
        return ((Ides0020)Programs.getInstance(Ides0020.class));
    }

    /**Original name: A000-INIZIO<br>
	 * <pre>*****************************************************************</pre>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'IDES0020'              TO   IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("IDES0020");
        // COB_CODE: MOVE 'GRU_ARZ'               TO   IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("GRU_ARZ");
        // COB_CODE: MOVE '00'                    TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                  TO   IDSV0003-SQLCODE
        //                                             IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                  TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                             IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>
	 * <pre>*****************************************************************</pre>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(descrizErrDb2);
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A300-ELABORA<br>
	 * <pre>*****************************************************************</pre>*/
    private void a300Elabora() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A310-SELECT        THRU A310-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER  TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A310-SELECT        THRU A310-EX
            a310Select();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A310-SELECT<br>
	 * <pre>*****************************************************************</pre>*/
    private void a310Select() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX
        //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IDES0020.cbl:line=112, because the code is unreachable.
        // COB_CODE: EXEC SQL
        //                SELECT
        //                COD_GRU_ARZ
        //                ,COD_COMP_ANIA
        //                ,COD_LIV_OGZ
        //                ,DSC_GRU_ARZ
        //                ,DAT_INI_GRU_ARZ
        //                ,DAT_FINE_GRU_ARZ
        //                ,DEN_RESP_GRU_ARZ
        //                ,IND_TLM_RESP
        //                ,COD_UTE_INS
        //                ,TMST_INS_RIG
        //                ,COD_UTE_AGR
        //                ,TMST_AGR_RIG
        //             INTO
        //                :GRU-COD-GRU-ARZ
        //               ,:GRU-COD-COMP-ANIA
        //               ,:GRU-COD-LIV-OGZ
        //               ,:GRU-DSC-GRU-ARZ-VCHAR
        //               ,:GRU-DAT-INI-GRU-ARZ-DB
        //               ,:GRU-DAT-FINE-GRU-ARZ-DB
        //               ,:GRU-DEN-RESP-GRU-ARZ-VCHAR
        //               ,:GRU-IND-TLM-RESP-VCHAR
        //               ,:GRU-COD-UTE-INS
        //               ,:GRU-TMST-INS-RIG-DB
        //               ,:GRU-COD-UTE-AGR
        //               ,:GRU-TMST-AGR-RIG-DB
        //               FROM GRU_ARZ
        //             WHERE COD_GRU_ARZ   = :GRU-COD-GRU-ARZ
        //           END-EXEC.
        gruArzDao.selectByGruCodGruArz(gruArz.getCodGruArz(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>*****************************************************************</pre>*/
    private void z100SetColonneNull() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE GRU-DAT-INI-GRU-ARZ-DB TO WS-DATE-X
        idsv0010.setWsDateX(gruArzDb.getDatIniGruArzDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO GRU-DAT-INI-GRU-ARZ
        gruArz.setDatIniGruArz(idsv0010.getWsDateN());
        // COB_CODE: MOVE GRU-DAT-FINE-GRU-ARZ-DB TO WS-DATE-X
        idsv0010.setWsDateX(gruArzDb.getDatFineGruArzDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO GRU-DAT-FINE-GRU-ARZ
        gruArz.setDatFineGruArz(idsv0010.getWsDateN());
        // COB_CODE: MOVE GRU-TMST-INS-RIG-DB TO WS-TIMESTAMP-X
        idsv0010.setWsTimestampX(gruArzDb.getTmstInsRigDb());
        // COB_CODE: PERFORM Z801-TS-X-TO-N     THRU Z801-EX
        z801TsXToN();
        // COB_CODE: MOVE WS-TIMESTAMP-N      TO GRU-TMST-INS-RIG
        gruArz.setTmstInsRig(idsv0010.getWsTimestampN());
        // COB_CODE: MOVE GRU-TMST-AGR-RIG-DB TO WS-TIMESTAMP-X
        idsv0010.setWsTimestampX(gruArzDb.getTmstAgrRigDb());
        // COB_CODE: PERFORM Z801-TS-X-TO-N     THRU Z801-EX
        z801TsXToN();
        // COB_CODE: MOVE WS-TIMESTAMP-N      TO GRU-TMST-AGR-RIG.
        gruArz.setTmstAgrRig(idsv0010.getWsTimestampN());
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        idsv0010.setWsStrDateNFormatted(Functions.setSubstring(idsv0010.getWsStrDateNFormatted(), idsv0010.getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        idsv0010.setWsStrDateNFormatted(Functions.setSubstring(idsv0010.getWsStrDateNFormatted(), idsv0010.getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        idsv0010.setWsStrDateNFormatted(Functions.setSubstring(idsv0010.getWsStrDateNFormatted(), idsv0010.getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        idsv0010.setWsStrDateNFormatted(Functions.setSubstring(idsv0010.getWsStrDateNFormatted(), idsv0010.getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        idsv0010.setWsStrDateNFormatted(Functions.setSubstring(idsv0010.getWsStrDateNFormatted(), idsv0010.getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        idsv0010.setWsStrDateNFormatted(Functions.setSubstring(idsv0010.getWsStrDateNFormatted(), idsv0010.getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    /**Original name: Z801-TS-X-TO-N<br>*/
    private void z801TsXToN() {
        // COB_CODE: MOVE WS-TIMESTAMP-X(1:4)
        //                TO WS-STR-TIMESTAMP-N(1:4)
        idsv0010.setWsStrTimestampNFormatted(Functions.setSubstring(idsv0010.getWsStrTimestampNFormatted(), idsv0010.getWsTimestampXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-TIMESTAMP-X(6:2)
        //                TO WS-STR-TIMESTAMP-N(5:2)
        idsv0010.setWsStrTimestampNFormatted(Functions.setSubstring(idsv0010.getWsStrTimestampNFormatted(), idsv0010.getWsTimestampXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-TIMESTAMP-X(9:2)
        //                TO WS-STR-TIMESTAMP-N(7:2)
        idsv0010.setWsStrTimestampNFormatted(Functions.setSubstring(idsv0010.getWsStrTimestampNFormatted(), idsv0010.getWsTimestampXFormatted().substring((9) - 1, 10), 7, 2));
        // COB_CODE: MOVE WS-TIMESTAMP-X(12:2)
        //                TO WS-STR-TIMESTAMP-N(9:2)
        idsv0010.setWsStrTimestampNFormatted(Functions.setSubstring(idsv0010.getWsStrTimestampNFormatted(), idsv0010.getWsTimestampXFormatted().substring((12) - 1, 13), 9, 2));
        // COB_CODE: MOVE WS-TIMESTAMP-X(15:2)
        //                TO WS-STR-TIMESTAMP-N(11:2)
        idsv0010.setWsStrTimestampNFormatted(Functions.setSubstring(idsv0010.getWsStrTimestampNFormatted(), idsv0010.getWsTimestampXFormatted().substring((15) - 1, 16), 11, 2));
        // COB_CODE: MOVE WS-TIMESTAMP-X(18:2)
        //                TO WS-STR-TIMESTAMP-N(13:2)
        idsv0010.setWsStrTimestampNFormatted(Functions.setSubstring(idsv0010.getWsStrTimestampNFormatted(), idsv0010.getWsTimestampXFormatted().substring((18) - 1, 19), 13, 2));
        // COB_CODE: MOVE WS-TIMESTAMP-X(21:4)
        //                TO WS-STR-TIMESTAMP-N(15:4).
        idsv0010.setWsStrTimestampNFormatted(Functions.setSubstring(idsv0010.getWsStrTimestampNFormatted(), idsv0010.getWsTimestampXFormatted().substring((21) - 1, 24), 15, 4));
    }

    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    @Override
    public int getCodCompAnia() {
        return gruArz.getCodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.gruArz.setCodCompAnia(codCompAnia);
    }

    @Override
    public int getCodLivOgz() {
        return gruArz.getCodLivOgz();
    }

    @Override
    public void setCodLivOgz(int codLivOgz) {
        this.gruArz.setCodLivOgz(codLivOgz);
    }

    @Override
    public String getCodUteAgr() {
        return gruArz.getCodUteAgr();
    }

    @Override
    public void setCodUteAgr(String codUteAgr) {
        this.gruArz.setCodUteAgr(codUteAgr);
    }

    @Override
    public String getCodUteIns() {
        return gruArz.getCodUteIns();
    }

    @Override
    public void setCodUteIns(String codUteIns) {
        this.gruArz.setCodUteIns(codUteIns);
    }

    @Override
    public String getDatFineGruArzDb() {
        return gruArzDb.getDatFineGruArzDb();
    }

    @Override
    public void setDatFineGruArzDb(String datFineGruArzDb) {
        this.gruArzDb.setDatFineGruArzDb(datFineGruArzDb);
    }

    @Override
    public String getDatIniGruArzDb() {
        return gruArzDb.getDatIniGruArzDb();
    }

    @Override
    public void setDatIniGruArzDb(String datIniGruArzDb) {
        this.gruArzDb.setDatIniGruArzDb(datIniGruArzDb);
    }

    @Override
    public String getDenRespGruArzVchar() {
        return gruArz.getDenRespGruArzVcharFormatted();
    }

    @Override
    public void setDenRespGruArzVchar(String denRespGruArzVchar) {
        this.gruArz.setDenRespGruArzVcharFormatted(denRespGruArzVchar);
    }

    @Override
    public String getDscGruArzVchar() {
        return gruArz.getDscGruArzVcharFormatted();
    }

    @Override
    public void setDscGruArzVchar(String dscGruArzVchar) {
        this.gruArz.setDscGruArzVcharFormatted(dscGruArzVchar);
    }

    @Override
    public long getGruCodGruArz() {
        return gruArz.getCodGruArz();
    }

    @Override
    public void setGruCodGruArz(long gruCodGruArz) {
        this.gruArz.setCodGruArz(gruCodGruArz);
    }

    @Override
    public String getIndTlmRespVchar() {
        return gruArz.getIndTlmRespVcharFormatted();
    }

    @Override
    public void setIndTlmRespVchar(String indTlmRespVchar) {
        this.gruArz.setIndTlmRespVcharFormatted(indTlmRespVchar);
    }

    @Override
    public String getTmstAgrRigDb() {
        return gruArzDb.getTmstAgrRigDb();
    }

    @Override
    public void setTmstAgrRigDb(String tmstAgrRigDb) {
        this.gruArzDb.setTmstAgrRigDb(tmstAgrRigDb);
    }

    @Override
    public String getTmstInsRigDb() {
        return gruArzDb.getTmstInsRigDb();
    }

    @Override
    public void setTmstInsRigDb(String tmstInsRigDb) {
        this.gruArzDb.setTmstInsRigDb(tmstInsRigDb);
    }
}
