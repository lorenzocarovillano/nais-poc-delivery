package it.accenture.jnais;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.Lvvs0112Data;

/**Original name: LVVS0112<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2007.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *   PROGRAMMA...... LVVS0112
 *   TIPOLOGIA...... SERVIZIO
 *   PROCESSO....... XXX
 *   FUNZIONE....... XXX
 *   DESCRIZIONE.... CONVERSIONE DATA DECORRENZA POLIZZA
 * **------------------------------------------------------------***</pre>*/
public class Lvvs0112 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lvvs0112Data ws = new Lvvs0112Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS0112
    private Ivvc0213 ivvc0213;

    //==== METHODS ====
    /**Original name: PROGRAM_LVVS0112_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: PERFORM S1000-ELABORAZIONE
        //              THRU EX-S1000
        s1000Elaborazione();
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lvvs0112 getInstance() {
        return ((Lvvs0112)Programs.getInstance(Lvvs0112.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                        IX-INDICI
        //                                             IVVC0213-TAB-OUTPUT.
        initIxIndici();
        initTabOutput();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE
        //             TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: INITIALIZE DPCO-IO-PCO.
        initDpcoIoPco();
        //
        //--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
        //--> RISPETTIVE AREE DCLGEN IN WORKING
        // COB_CODE: PERFORM S1100-VALORIZZA-DCLGEN
        //              THRU S1100-VALORIZZA-DCLGEN-EX
        //           VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
        //                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //                   SPACES OR LOW-VALUE OR HIGH-VALUE.
        ws.setIxDclgen(((short)1));
        while (!(ws.getIxDclgen() > ivvc0213.getEleInfoMax() || Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias()) || Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()) || Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()))) {
            s1100ValorizzaDclgen();
            ws.setIxDclgen(Trunc.toShort(ws.getIxDclgen() + 1, 4));
        }
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //               PERFORM S1260-CALCOLA-VAL          THRU S1260-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM S1260-CALCOLA-VAL          THRU S1260-EX
            s1260CalcolaVal();
        }
    }

    /**Original name: S1100-VALORIZZA-DCLGEN<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 *     RISPETTIVE AREE DCLGEN IN WORKING
	 * ----------------------------------------------------------------*</pre>*/
    private void s1100ValorizzaDclgen() {
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-PARAM-COMP
        //                TO DPCO-AREA-PCO
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias(), ws.getIvvc0218().getAliasParamComp())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO DPCO-AREA-PCO
            ws.setDpcoAreaPcoFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxDclgen()).getLunghezza() - 1));
        }
    }

    /**Original name: S1260-CALCOLA-VAL<br>
	 * <pre>----------------------------------------------------------------*
	 *   CALCOLA DATA 1
	 * ----------------------------------------------------------------*</pre>*/
    private void s1260CalcolaVal() {
        // COB_CODE: IF DPCO-TP-MOD-RIVAL NOT EQUAL SPACES
        //              END-IF
        //           ELSE
        //              SET IDSV0003-FIELD-NOT-VALUED     TO TRUE
        //           END-IF.
        if (!Characters.EQ_SPACE.test(ws.getLccvpco1().getDati().getWpcoTpModRival())) {
            // COB_CODE: IF DPCO-TP-MOD-RIVAL = 'IN'
            //              MOVE 1                        TO IVVC0213-VAL-IMP-O
            //           ELSE
            //              MOVE 0                        TO IVVC0213-VAL-IMP-O
            //           END-IF
            if (Conditions.eq(ws.getLccvpco1().getDati().getWpcoTpModRival(), "IN")) {
                // COB_CODE: MOVE 1                        TO IVVC0213-VAL-IMP-O
                ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(1, 18, 7));
            }
            else {
                // COB_CODE: MOVE 0                        TO IVVC0213-VAL-IMP-O
                ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(0, 18, 7));
            }
        }
        else {
            // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED     TO TRUE
            idsv0003.getReturnCode().setFieldNotValued();
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    public void initIxIndici() {
        ws.setIxDclgen(((short)0));
        ws.setIxTabRan(((short)0));
    }

    public void initTabOutput() {
        ivvc0213.getTabOutput().setCodVariabileO("");
        ivvc0213.getTabOutput().setTpDatoO(Types.SPACE_CHAR);
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        ivvc0213.getTabOutput().setValPercO(new AfDecimal(0, 14, 9));
        ivvc0213.getTabOutput().setValStrO("");
    }

    public void initDpcoIoPco() {
        ws.setDpcoElePcoMax(((short)0));
        ws.getLccvpco1().getStatus().setStatus(Types.SPACE_CHAR);
        ws.getLccvpco1().getDati().setWpcoCodCompAnia(0);
        ws.getLccvpco1().getDati().setWpcoCodTratCirt("");
        ws.getLccvpco1().getDati().getWpcoLimVltr().setWpcoLimVltr(new AfDecimal(0, 15, 3));
        ws.getLccvpco1().getDati().setWpcoTpRatPerf(Types.SPACE_CHAR);
        ws.getLccvpco1().getDati().setWpcoTpLivGenzTit("");
        ws.getLccvpco1().getDati().getWpcoArrotPre().setWpcoArrotPre(new AfDecimal(0, 15, 3));
        ws.getLccvpco1().getDati().getWpcoDtCont().setWpcoDtCont(0);
        ws.getLccvpco1().getDati().getWpcoDtUltRivalIn().setWpcoDtUltRivalIn(0);
        ws.getLccvpco1().getDati().getWpcoDtUltQtzoIn().setWpcoDtUltQtzoIn(0);
        ws.getLccvpco1().getDati().getWpcoDtUltRiclRiass().setWpcoDtUltRiclRiass(0);
        ws.getLccvpco1().getDati().getWpcoDtUltTabulRiass().setWpcoDtUltTabulRiass(0);
        ws.getLccvpco1().getDati().getWpcoDtUltBollEmes().setWpcoDtUltBollEmes(0);
        ws.getLccvpco1().getDati().getWpcoDtUltBollStor().setWpcoDtUltBollStor(0);
        ws.getLccvpco1().getDati().getWpcoDtUltBollLiq().setWpcoDtUltBollLiq(0);
        ws.getLccvpco1().getDati().getWpcoDtUltBollRiat().setWpcoDtUltBollRiat(0);
        ws.getLccvpco1().getDati().getWpcoDtUltelriscparPr().setWpcoDtUltelriscparPr(0);
        ws.getLccvpco1().getDati().getWpcoDtUltcIsIn().setWpcoDtUltcIsIn(0);
        ws.getLccvpco1().getDati().getWpcoDtUltRiclPre().setWpcoDtUltRiclPre(0);
        ws.getLccvpco1().getDati().getWpcoDtUltcMarsol().setWpcoDtUltcMarsol(0);
        ws.getLccvpco1().getDati().getWpcoDtUltcRbIn().setWpcoDtUltcRbIn(0);
        ws.getLccvpco1().getDati().getWpcoPcProv1aaAcq().setWpcoPcProv1aaAcq(new AfDecimal(0, 6, 3));
        ws.getLccvpco1().getDati().setWpcoModIntrPrest("");
        ws.getLccvpco1().getDati().getWpcoGgMaxRecProv().setWpcoGgMaxRecProv(((short)0));
        ws.getLccvpco1().getDati().getWpcoDtUltgzTrchEIn().setWpcoDtUltgzTrchEIn(0);
        ws.getLccvpco1().getDati().getWpcoDtUltBollSnden().setWpcoDtUltBollSnden(0);
        ws.getLccvpco1().getDati().getWpcoDtUltBollSndnlq().setWpcoDtUltBollSndnlq(0);
        ws.getLccvpco1().getDati().getWpcoDtUltscElabIn().setWpcoDtUltscElabIn(0);
        ws.getLccvpco1().getDati().getWpcoDtUltscOpzIn().setWpcoDtUltscOpzIn(0);
        ws.getLccvpco1().getDati().getWpcoDtUltcBnsricIn().setWpcoDtUltcBnsricIn(0);
        ws.getLccvpco1().getDati().getWpcoDtUltcBnsfdtIn().setWpcoDtUltcBnsfdtIn(0);
        ws.getLccvpco1().getDati().getWpcoDtUltRinnGarac().setWpcoDtUltRinnGarac(0);
        ws.getLccvpco1().getDati().getWpcoDtUltgzCed().setWpcoDtUltgzCed(0);
        ws.getLccvpco1().getDati().getWpcoDtUltElabPrlcos().setWpcoDtUltElabPrlcos(0);
        ws.getLccvpco1().getDati().getWpcoDtUltRinnColl().setWpcoDtUltRinnColl(0);
        ws.getLccvpco1().getDati().setWpcoFlRvcPerf(Types.SPACE_CHAR);
        ws.getLccvpco1().getDati().setWpcoFlRcsPoliNoperf(Types.SPACE_CHAR);
        ws.getLccvpco1().getDati().setWpcoFlGestPlusv(Types.SPACE_CHAR);
        ws.getLccvpco1().getDati().getWpcoDtUltRivalCl().setWpcoDtUltRivalCl(0);
        ws.getLccvpco1().getDati().getWpcoDtUltQtzoCl().setWpcoDtUltQtzoCl(0);
        ws.getLccvpco1().getDati().getWpcoDtUltcBnsricCl().setWpcoDtUltcBnsricCl(0);
        ws.getLccvpco1().getDati().getWpcoDtUltcBnsfdtCl().setWpcoDtUltcBnsfdtCl(0);
        ws.getLccvpco1().getDati().getWpcoDtUltcIsCl().setWpcoDtUltcIsCl(0);
        ws.getLccvpco1().getDati().getWpcoDtUltcRbCl().setWpcoDtUltcRbCl(0);
        ws.getLccvpco1().getDati().getWpcoDtUltgzTrchECl().setWpcoDtUltgzTrchECl(0);
        ws.getLccvpco1().getDati().getWpcoDtUltscElabCl().setWpcoDtUltscElabCl(0);
        ws.getLccvpco1().getDati().getWpcoDtUltscOpzCl().setWpcoDtUltscOpzCl(0);
        ws.getLccvpco1().getDati().getWpcoStstXRegione().setWpcoStstXRegione(0);
        ws.getLccvpco1().getDati().getWpcoDtUltgzCedColl().setWpcoDtUltgzCedColl(0);
        ws.getLccvpco1().getDati().setWpcoTpModRival("");
        ws.getLccvpco1().getDati().getWpcoNumMmCalcMora().setWpcoNumMmCalcMora(0);
        ws.getLccvpco1().getDati().getWpcoDtUltEcRivColl().setWpcoDtUltEcRivColl(0);
        ws.getLccvpco1().getDati().getWpcoDtUltEcRivInd().setWpcoDtUltEcRivInd(0);
        ws.getLccvpco1().getDati().getWpcoDtUltEcIlColl().setWpcoDtUltEcIlColl(0);
        ws.getLccvpco1().getDati().getWpcoDtUltEcIlInd().setWpcoDtUltEcIlInd(0);
        ws.getLccvpco1().getDati().getWpcoDtUltEcUlColl().setWpcoDtUltEcUlColl(0);
        ws.getLccvpco1().getDati().getWpcoDtUltEcUlInd().setWpcoDtUltEcUlInd(0);
        ws.getLccvpco1().getDati().getWpcoAaUti().setWpcoAaUti(((short)0));
        ws.getLccvpco1().getDati().setWpcoCalcRshComun(Types.SPACE_CHAR);
        ws.getLccvpco1().getDati().setWpcoFlLivDebug(((short)0));
        ws.getLccvpco1().getDati().getWpcoDtUltBollPerfC().setWpcoDtUltBollPerfC(0);
        ws.getLccvpco1().getDati().getWpcoDtUltBollRspIn().setWpcoDtUltBollRspIn(0);
        ws.getLccvpco1().getDati().getWpcoDtUltBollRspCl().setWpcoDtUltBollRspCl(0);
        ws.getLccvpco1().getDati().getWpcoDtUltBollEmesI().setWpcoDtUltBollEmesI(0);
        ws.getLccvpco1().getDati().getWpcoDtUltBollStorI().setWpcoDtUltBollStorI(0);
        ws.getLccvpco1().getDati().getWpcoDtUltBollRiatI().setWpcoDtUltBollRiatI(0);
        ws.getLccvpco1().getDati().getWpcoDtUltBollSdI().setWpcoDtUltBollSdI(0);
        ws.getLccvpco1().getDati().getWpcoDtUltBollSdnlI().setWpcoDtUltBollSdnlI(0);
        ws.getLccvpco1().getDati().getWpcoDtUltBollPerfI().setWpcoDtUltBollPerfI(0);
        ws.getLccvpco1().getDati().getWpcoDtRiclRiriasCom().setWpcoDtRiclRiriasCom(0);
        ws.getLccvpco1().getDati().getWpcoDtUltElabAt92C().setWpcoDtUltElabAt92C(0);
        ws.getLccvpco1().getDati().getWpcoDtUltElabAt92I().setWpcoDtUltElabAt92I(0);
        ws.getLccvpco1().getDati().getWpcoDtUltElabAt93C().setWpcoDtUltElabAt93C(0);
        ws.getLccvpco1().getDati().getWpcoDtUltElabAt93I().setWpcoDtUltElabAt93I(0);
        ws.getLccvpco1().getDati().getWpcoDtUltElabSpeIn().setWpcoDtUltElabSpeIn(0);
        ws.getLccvpco1().getDati().getWpcoDtUltElabPrCon().setWpcoDtUltElabPrCon(0);
        ws.getLccvpco1().getDati().getWpcoDtUltBollRpCl().setWpcoDtUltBollRpCl(0);
        ws.getLccvpco1().getDati().getWpcoDtUltBollRpIn().setWpcoDtUltBollRpIn(0);
        ws.getLccvpco1().getDati().getWpcoDtUltBollPreI().setWpcoDtUltBollPreI(0);
        ws.getLccvpco1().getDati().getWpcoDtUltBollPreC().setWpcoDtUltBollPreC(0);
        ws.getLccvpco1().getDati().getWpcoDtUltcPildiMmC().setWpcoDtUltcPildiMmC(0);
        ws.getLccvpco1().getDati().getWpcoDtUltcPildiAaC().setWpcoDtUltcPildiAaC(0);
        ws.getLccvpco1().getDati().getWpcoDtUltcPildiMmI().setWpcoDtUltcPildiMmI(0);
        ws.getLccvpco1().getDati().getWpcoDtUltcPildiTrI().setWpcoDtUltcPildiTrI(0);
        ws.getLccvpco1().getDati().getWpcoDtUltcPildiAaI().setWpcoDtUltcPildiAaI(0);
        ws.getLccvpco1().getDati().getWpcoDtUltBollQuieC().setWpcoDtUltBollQuieC(0);
        ws.getLccvpco1().getDati().getWpcoDtUltBollQuieI().setWpcoDtUltBollQuieI(0);
        ws.getLccvpco1().getDati().getWpcoDtUltBollCotrI().setWpcoDtUltBollCotrI(0);
        ws.getLccvpco1().getDati().getWpcoDtUltBollCotrC().setWpcoDtUltBollCotrC(0);
        ws.getLccvpco1().getDati().getWpcoDtUltBollCoriC().setWpcoDtUltBollCoriC(0);
        ws.getLccvpco1().getDati().getWpcoDtUltBollCoriI().setWpcoDtUltBollCoriI(0);
        ws.getLccvpco1().getDati().setWpcoDsOperSql(Types.SPACE_CHAR);
        ws.getLccvpco1().getDati().setWpcoDsVer(0);
        ws.getLccvpco1().getDati().setWpcoDsTsCptz(0);
        ws.getLccvpco1().getDati().setWpcoDsUtente("");
        ws.getLccvpco1().getDati().setWpcoDsStatoElab(Types.SPACE_CHAR);
        ws.getLccvpco1().getDati().setWpcoTpValzzDtVlt("");
        ws.getLccvpco1().getDati().setWpcoFlFrazProvAcq(Types.SPACE_CHAR);
        ws.getLccvpco1().getDati().getWpcoDtUltAggErogRe().setWpcoDtUltAggErogRe(0);
        ws.getLccvpco1().getDati().getWpcoPcRmMarsol().setWpcoPcRmMarsol(new AfDecimal(0, 6, 3));
        ws.getLccvpco1().getDati().getWpcoPcCSubrshMarsol().setWpcoPcCSubrshMarsol(new AfDecimal(0, 6, 3));
        ws.getLccvpco1().getDati().setWpcoCodCompIsvap("");
        ws.getLccvpco1().getDati().getWpcoLmRisConInt().setWpcoLmRisConInt(new AfDecimal(0, 6, 3));
        ws.getLccvpco1().getDati().getWpcoLmCSubrshConIn().setWpcoLmCSubrshConIn(new AfDecimal(0, 6, 3));
        ws.getLccvpco1().getDati().getWpcoPcGarNoriskMars().setWpcoPcGarNoriskMars(new AfDecimal(0, 6, 3));
        ws.getLccvpco1().getDati().setWpcoCrz1aRatIntrPr(Types.SPACE_CHAR);
        ws.getLccvpco1().getDati().getWpcoNumGgArrIntrPr().setWpcoNumGgArrIntrPr(0);
        ws.getLccvpco1().getDati().setWpcoFlVisualVinpg(Types.SPACE_CHAR);
        ws.getLccvpco1().getDati().getWpcoDtUltEstrazFug().setWpcoDtUltEstrazFug(0);
        ws.getLccvpco1().getDati().getWpcoDtUltElabPrAut().setWpcoDtUltElabPrAut(0);
        ws.getLccvpco1().getDati().getWpcoDtUltElabCommef().setWpcoDtUltElabCommef(0);
        ws.getLccvpco1().getDati().getWpcoDtUltElabLiqmef().setWpcoDtUltElabLiqmef(0);
        ws.getLccvpco1().getDati().setWpcoCodFiscMef("");
        ws.getLccvpco1().getDati().getWpcoImpAssSociale().setWpcoImpAssSociale(new AfDecimal(0, 15, 3));
        ws.getLccvpco1().getDati().setWpcoModComnzInvstSw(Types.SPACE_CHAR);
        ws.getLccvpco1().getDati().getWpcoDtRiatRiassRsh().setWpcoDtRiatRiassRsh(0);
        ws.getLccvpco1().getDati().getWpcoDtRiatRiassComm().setWpcoDtRiatRiassComm(0);
        ws.getLccvpco1().getDati().getWpcoGgIntrRitPag().setWpcoGgIntrRitPag(0);
        ws.getLccvpco1().getDati().getWpcoDtUltRinnTac().setWpcoDtUltRinnTac(0);
        ws.getLccvpco1().getDati().setWpcoDescCompLen(((short)0));
        ws.getLccvpco1().getDati().setWpcoDescComp("");
        ws.getLccvpco1().getDati().getWpcoDtUltEcTcmInd().setWpcoDtUltEcTcmInd(0);
        ws.getLccvpco1().getDati().getWpcoDtUltEcTcmColl().setWpcoDtUltEcTcmColl(0);
        ws.getLccvpco1().getDati().getWpcoDtUltEcMrmInd().setWpcoDtUltEcMrmInd(0);
        ws.getLccvpco1().getDati().getWpcoDtUltEcMrmColl().setWpcoDtUltEcMrmColl(0);
        ws.getLccvpco1().getDati().setWpcoCodCompLdap("");
        ws.getLccvpco1().getDati().getWpcoPcRidImp1382011().setWpcoPcRidImp1382011(new AfDecimal(0, 6, 3));
        ws.getLccvpco1().getDati().getWpcoPcRidImp662014().setWpcoPcRidImp662014(new AfDecimal(0, 6, 3));
        ws.getLccvpco1().getDati().getWpcoSoglAmlPreUni().setWpcoSoglAmlPreUni(new AfDecimal(0, 15, 3));
        ws.getLccvpco1().getDati().getWpcoSoglAmlPrePer().setWpcoSoglAmlPrePer(new AfDecimal(0, 15, 3));
        ws.getLccvpco1().getDati().setWpcoCodSoggFtzAssto("");
        ws.getLccvpco1().getDati().getWpcoDtUltElabRedpro().setWpcoDtUltElabRedpro(0);
        ws.getLccvpco1().getDati().getWpcoDtUltElabTakeP().setWpcoDtUltElabTakeP(0);
        ws.getLccvpco1().getDati().getWpcoDtUltElabPaspas().setWpcoDtUltElabPaspas(0);
        ws.getLccvpco1().getDati().getWpcoSoglAmlPreSavR().setWpcoSoglAmlPreSavR(new AfDecimal(0, 15, 3));
        ws.getLccvpco1().getDati().getWpcoDtUltEstrDecCo().setWpcoDtUltEstrDecCo(0);
        ws.getLccvpco1().getDati().getWpcoDtUltElabCosAt().setWpcoDtUltElabCosAt(0);
        ws.getLccvpco1().getDati().getWpcoFrqCostiAtt().setWpcoFrqCostiAtt(0);
        ws.getLccvpco1().getDati().getWpcoDtUltElabCosSt().setWpcoDtUltElabCosSt(0);
        ws.getLccvpco1().getDati().getWpcoFrqCostiStornati().setWpcoFrqCostiStornati(0);
        ws.getLccvpco1().getDati().getWpcoDtEstrAssMin70a().setWpcoDtEstrAssMin70a(0);
        ws.getLccvpco1().getDati().getWpcoDtEstrAssMag70a().setWpcoDtEstrAssMag70a(0);
    }
}
