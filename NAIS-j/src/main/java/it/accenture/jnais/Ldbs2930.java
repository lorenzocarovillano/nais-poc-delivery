package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.jdbc.FieldNotMappedException;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.BilaTrchEstrDao;
import it.accenture.jnais.commons.data.to.IBilaTrchEstr;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.BilaTrchEstrIdbsb030;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ldbs2930Data;
import it.accenture.jnais.ws.redefines.B03AaRenCer;
import it.accenture.jnais.ws.redefines.B03Abb;
import it.accenture.jnais.ws.redefines.B03AcqExp;
import it.accenture.jnais.ws.redefines.B03AlqMargCSubrsh;
import it.accenture.jnais.ws.redefines.B03AlqMargRis;
import it.accenture.jnais.ws.redefines.B03AlqRetrT;
import it.accenture.jnais.ws.redefines.B03AntidurCalc365;
import it.accenture.jnais.ws.redefines.B03AntidurDtCalc;
import it.accenture.jnais.ws.redefines.B03AntidurRicorPrec;
import it.accenture.jnais.ws.redefines.B03CarAcqNonScon;
import it.accenture.jnais.ws.redefines.B03CarAcqPrecontato;
import it.accenture.jnais.ws.redefines.B03CarGest;
import it.accenture.jnais.ws.redefines.B03CarGestNonScon;
import it.accenture.jnais.ws.redefines.B03CarInc;
import it.accenture.jnais.ws.redefines.B03CarIncNonScon;
import it.accenture.jnais.ws.redefines.B03Carz;
import it.accenture.jnais.ws.redefines.B03CodAge;
import it.accenture.jnais.ws.redefines.B03CodCan;
import it.accenture.jnais.ws.redefines.B03CodPrdt;
import it.accenture.jnais.ws.redefines.B03CodSubage;
import it.accenture.jnais.ws.redefines.B03CoeffOpzCpt;
import it.accenture.jnais.ws.redefines.B03CoeffOpzRen;
import it.accenture.jnais.ws.redefines.B03CoeffRis1T;
import it.accenture.jnais.ws.redefines.B03CoeffRis2T;
import it.accenture.jnais.ws.redefines.B03CommisInter;
import it.accenture.jnais.ws.redefines.B03CptAsstoIniMor;
import it.accenture.jnais.ws.redefines.B03CptDtRidz;
import it.accenture.jnais.ws.redefines.B03CptDtStab;
import it.accenture.jnais.ws.redefines.B03CptRiasto;
import it.accenture.jnais.ws.redefines.B03CptRiastoEcc;
import it.accenture.jnais.ws.redefines.B03CptRshMor;
import it.accenture.jnais.ws.redefines.B03CSubrshT;
import it.accenture.jnais.ws.redefines.B03CumRiscpar;
import it.accenture.jnais.ws.redefines.B03Dir;
import it.accenture.jnais.ws.redefines.B03DirEmis;
import it.accenture.jnais.ws.redefines.B03DtDecorAdes;
import it.accenture.jnais.ws.redefines.B03DtEffCambStat;
import it.accenture.jnais.ws.redefines.B03DtEffRidz;
import it.accenture.jnais.ws.redefines.B03DtEffStab;
import it.accenture.jnais.ws.redefines.B03DtEmisCambStat;
import it.accenture.jnais.ws.redefines.B03DtEmisRidz;
import it.accenture.jnais.ws.redefines.B03DtEmisTrch;
import it.accenture.jnais.ws.redefines.B03DtIncUltPre;
import it.accenture.jnais.ws.redefines.B03DtIniValTar;
import it.accenture.jnais.ws.redefines.B03DtNasc1oAssto;
import it.accenture.jnais.ws.redefines.B03DtQtzEmis;
import it.accenture.jnais.ws.redefines.B03DtScadIntmd;
import it.accenture.jnais.ws.redefines.B03DtScadPagPre;
import it.accenture.jnais.ws.redefines.B03DtScadTrch;
import it.accenture.jnais.ws.redefines.B03DtUltPrePag;
import it.accenture.jnais.ws.redefines.B03DtUltRival;
import it.accenture.jnais.ws.redefines.B03Dur1oPerAa;
import it.accenture.jnais.ws.redefines.B03Dur1oPerGg;
import it.accenture.jnais.ws.redefines.B03Dur1oPerMm;
import it.accenture.jnais.ws.redefines.B03DurAa;
import it.accenture.jnais.ws.redefines.B03DurGarAa;
import it.accenture.jnais.ws.redefines.B03DurGarGg;
import it.accenture.jnais.ws.redefines.B03DurGarMm;
import it.accenture.jnais.ws.redefines.B03DurGg;
import it.accenture.jnais.ws.redefines.B03DurMm;
import it.accenture.jnais.ws.redefines.B03DurPagPre;
import it.accenture.jnais.ws.redefines.B03DurPagRen;
import it.accenture.jnais.ws.redefines.B03DurResDtCalc;
import it.accenture.jnais.ws.redefines.B03EtaAa1oAssto;
import it.accenture.jnais.ws.redefines.B03EtaMm1oAssto;
import it.accenture.jnais.ws.redefines.B03EtaRaggnDtCalc;
import it.accenture.jnais.ws.redefines.B03Fraz;
import it.accenture.jnais.ws.redefines.B03FrazDecrCpt;
import it.accenture.jnais.ws.redefines.B03FrazIniErogRen;
import it.accenture.jnais.ws.redefines.B03IdRichEstrazAgg;
import it.accenture.jnais.ws.redefines.B03ImpCarCasoMor;
import it.accenture.jnais.ws.redefines.B03IntrTecn;
import it.accenture.jnais.ws.redefines.B03MinGartoT;
import it.accenture.jnais.ws.redefines.B03MinTrnutT;
import it.accenture.jnais.ws.redefines.B03NsQuo;
import it.accenture.jnais.ws.redefines.B03NumPrePatt;
import it.accenture.jnais.ws.redefines.B03OverComm;
import it.accenture.jnais.ws.redefines.B03PcCarAcq;
import it.accenture.jnais.ws.redefines.B03PcCarGest;
import it.accenture.jnais.ws.redefines.B03PcCarMor;
import it.accenture.jnais.ws.redefines.B03PreAnnualizRicor;
import it.accenture.jnais.ws.redefines.B03PreCont;
import it.accenture.jnais.ws.redefines.B03PreDovIni;
import it.accenture.jnais.ws.redefines.B03PreDovRivtoT;
import it.accenture.jnais.ws.redefines.B03PrePattuitoIni;
import it.accenture.jnais.ws.redefines.B03PrePpIni;
import it.accenture.jnais.ws.redefines.B03PrePpUlt;
import it.accenture.jnais.ws.redefines.B03PreRiasto;
import it.accenture.jnais.ws.redefines.B03PreRiastoEcc;
import it.accenture.jnais.ws.redefines.B03PreRshT;
import it.accenture.jnais.ws.redefines.B03ProvAcq;
import it.accenture.jnais.ws.redefines.B03ProvAcqRicor;
import it.accenture.jnais.ws.redefines.B03ProvInc;
import it.accenture.jnais.ws.redefines.B03PrstzAggIni;
import it.accenture.jnais.ws.redefines.B03PrstzAggUlt;
import it.accenture.jnais.ws.redefines.B03PrstzIni;
import it.accenture.jnais.ws.redefines.B03PrstzT;
import it.accenture.jnais.ws.redefines.B03QtzSpZCoupDtC;
import it.accenture.jnais.ws.redefines.B03QtzSpZCoupEmis;
import it.accenture.jnais.ws.redefines.B03QtzSpZOpzDtCa;
import it.accenture.jnais.ws.redefines.B03QtzSpZOpzEmis;
import it.accenture.jnais.ws.redefines.B03QtzTotDtCalc;
import it.accenture.jnais.ws.redefines.B03QtzTotDtUltBil;
import it.accenture.jnais.ws.redefines.B03QtzTotEmis;
import it.accenture.jnais.ws.redefines.B03Rappel;
import it.accenture.jnais.ws.redefines.B03RatRen;
import it.accenture.jnais.ws.redefines.B03RemunAss;
import it.accenture.jnais.ws.redefines.B03RisAcqT;
import it.accenture.jnais.ws.redefines.B03Riscpar;
import it.accenture.jnais.ws.redefines.B03RisMatChiuPrec;
import it.accenture.jnais.ws.redefines.B03RisPuraT;
import it.accenture.jnais.ws.redefines.B03RisRiasta;
import it.accenture.jnais.ws.redefines.B03RisRiastaEcc;
import it.accenture.jnais.ws.redefines.B03RisRistorniCap;
import it.accenture.jnais.ws.redefines.B03RisSpeT;
import it.accenture.jnais.ws.redefines.B03RisZilT;
import it.accenture.jnais.ws.redefines.B03TsMedio;
import it.accenture.jnais.ws.redefines.B03TsNetT;
import it.accenture.jnais.ws.redefines.B03TsPp;
import it.accenture.jnais.ws.redefines.B03TsRendtoSppr;
import it.accenture.jnais.ws.redefines.B03TsRendtoT;
import it.accenture.jnais.ws.redefines.B03TsStabPre;
import it.accenture.jnais.ws.redefines.B03TsTariDov;
import it.accenture.jnais.ws.redefines.B03TsTariScon;
import it.accenture.jnais.ws.redefines.B03UltRm;

/**Original name: LDBS2930<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  27 APR 2010.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO AD HOC PER ACCESSO RISORSE DB        *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Ldbs2930 extends Program implements IBilaTrchEstr {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private BilaTrchEstrDao bilaTrchEstrDao = new BilaTrchEstrDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Ldbs2930Data ws = new Ldbs2930Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: BILA-TRCH-ESTR
    private BilaTrchEstrIdbsb030 bilaTrchEstr;

    //==== METHODS ====
    /**Original name: PROGRAM_LDBS2930_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, BilaTrchEstrIdbsb030 bilaTrchEstr) {
        this.idsv0003 = idsv0003;
        this.bilaTrchEstr = bilaTrchEstr;
        // COB_CODE: PERFORM A000-INIZIO              THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                        a200ElaboraWcEff();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                        b200ElaboraWcCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //               SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                        c200ElaboraWcNst();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Ldbs2930 getInstance() {
        return ((Ldbs2930)Programs.getInstance(Ldbs2930.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'LDBS2930'              TO   IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("LDBS2930");
        // COB_CODE: MOVE 'BILA-TRCH-ESTR' TO   IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("BILA-TRCH-ESTR");
        // COB_CODE: MOVE '00'                      TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                    TO   IDSV0003-SQLCODE
        //                                               IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                    TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                               IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-WC-EFF<br>*/
    private void a200ElaboraWcEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-WC-EFF          THRU A210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-WC-EFF          THRU A210-EX
            a210SelectWcEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
            a260OpenCursorWcEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
            a270CloseCursorWcEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
            a280FetchFirstWcEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
            a290FetchNextWcEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B200-ELABORA-WC-CPZ<br>*/
    private void b200ElaboraWcCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
            b210SelectWcCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
            b260OpenCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
            b270CloseCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
            b280FetchFirstWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
            b290FetchNextWcCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: C200-ELABORA-WC-NST<br>*/
    private void c200ElaboraWcNst() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM C210-SELECT-WC-NST          THRU C210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM C210-SELECT-WC-NST          THRU C210-EX
            c210SelectWcNst();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
            c260OpenCursorWcNst();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
            c270CloseCursorWcNst();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
            c280FetchFirstWcNst();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
            c290FetchNextWcNst();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A205-DECLARE-CURSOR-WC-EFF<br>
	 * <pre>----
	 * ----  gestione WC Effetto
	 * ----</pre>*/
    private void a205DeclareCursorWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A210-SELECT-WC-EFF<br>*/
    private void a210SelectWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A260-OPEN-CURSOR-WC-EFF<br>*/
    private void a260OpenCursorWcEff() {
        // COB_CODE: PERFORM A205-DECLARE-CURSOR-WC-EFF THRU A205-EX.
        a205DeclareCursorWcEff();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A270-CLOSE-CURSOR-WC-EFF<br>*/
    private void a270CloseCursorWcEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A280-FETCH-FIRST-WC-EFF<br>*/
    private void a280FetchFirstWcEff() {
        // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF    THRU A260-EX.
        a260OpenCursorWcEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
            a290FetchNextWcEff();
        }
    }

    /**Original name: A290-FETCH-NEXT-WC-EFF<br>*/
    private void a290FetchNextWcEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B205-DECLARE-CURSOR-WC-CPZ<br>
	 * <pre>----
	 * ----  gestione WC Competenza
	 * ----</pre>*/
    private void b205DeclareCursorWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B210-SELECT-WC-CPZ<br>*/
    private void b210SelectWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B260-OPEN-CURSOR-WC-CPZ<br>*/
    private void b260OpenCursorWcCpz() {
        // COB_CODE: PERFORM B205-DECLARE-CURSOR-WC-CPZ THRU B205-EX.
        b205DeclareCursorWcCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B270-CLOSE-CURSOR-WC-CPZ<br>*/
    private void b270CloseCursorWcCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B280-FETCH-FIRST-WC-CPZ<br>*/
    private void b280FetchFirstWcCpz() {
        // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ    THRU B260-EX.
        b260OpenCursorWcCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
            b290FetchNextWcCpz();
        }
    }

    /**Original name: B290-FETCH-NEXT-WC-CPZ<br>*/
    private void b290FetchNextWcCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C205-DECLARE-CURSOR-WC-NST<br>
	 * <pre>----
	 * ----  gestione WC Senza Storicità
	 * ----</pre>*/
    private void c205DeclareCursorWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C210-SELECT-WC-NST<br>*/
    private void c210SelectWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //            UPDATE BILA_TRCH_ESTR
        //            SET  TP_STAT_BUS_TRCH   = :B03-TP-STAT-BUS-TRCH
        //                ,TP_CAUS_TRCH       = :B03-TP-CAUS-TRCH
        //            WHERE  ID_RICH_ESTRAZ_MAS = :B03-ID-RICH-ESTRAZ-MAS
        //            AND    ID_ADES            = :B03-ID-ADES
        //            AND    ID_POLI            = :B03-ID-POLI
        //            AND    ID_GAR             = :B03-ID-GAR
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //           END-EXEC.
        bilaTrchEstrDao.updateRec3(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: C260-OPEN-CURSOR-WC-NST<br>*/
    private void c260OpenCursorWcNst() {
        // COB_CODE: PERFORM C205-DECLARE-CURSOR-WC-NST THRU C205-EX.
        c205DeclareCursorWcNst();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C270-CLOSE-CURSOR-WC-NST<br>*/
    private void c270CloseCursorWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C280-FETCH-FIRST-WC-NST<br>*/
    private void c280FetchFirstWcNst() {
        // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST    THRU C260-EX.
        c260OpenCursorWcNst();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
            c290FetchNextWcNst();
        }
    }

    /**Original name: C290-FETCH-NEXT-WC-NST<br>*/
    private void c290FetchNextWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>----
	 * ----  utilità comuni a tutti i livelli operazione
	 * ----</pre>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-B03-ID-RICH-ESTRAZ-AGG = -1
        //              MOVE HIGH-VALUES TO B03-ID-RICH-ESTRAZ-AGG-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getIdRichEstrazAgg() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-ID-RICH-ESTRAZ-AGG-NULL
            bilaTrchEstr.getB03IdRichEstrazAgg().setB03IdRichEstrazAggNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03IdRichEstrazAgg.Len.B03_ID_RICH_ESTRAZ_AGG_NULL));
        }
        // COB_CODE: IF IND-B03-FL-SIMULAZIONE = -1
        //              MOVE HIGH-VALUES TO B03-FL-SIMULAZIONE-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getFlSimulazione() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-FL-SIMULAZIONE-NULL
            bilaTrchEstr.setB03FlSimulazione(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-B03-DT-INI-VAL-TAR = -1
        //              MOVE HIGH-VALUES TO B03-DT-INI-VAL-TAR-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtIniValTar() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DT-INI-VAL-TAR-NULL
            bilaTrchEstr.getB03DtIniValTar().setB03DtIniValTarNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DtIniValTar.Len.B03_DT_INI_VAL_TAR_NULL));
        }
        // COB_CODE: IF IND-B03-COD-PROD = -1
        //              MOVE HIGH-VALUES TO B03-COD-PROD-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCodProd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-COD-PROD-NULL
            bilaTrchEstr.setB03CodProd(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_COD_PROD));
        }
        // COB_CODE: IF IND-B03-COD-TARI-ORGN = -1
        //              MOVE HIGH-VALUES TO B03-COD-TARI-ORGN-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCodTariOrgn() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-COD-TARI-ORGN-NULL
            bilaTrchEstr.setB03CodTariOrgn(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_COD_TARI_ORGN));
        }
        // COB_CODE: IF IND-B03-MIN-GARTO-T = -1
        //              MOVE HIGH-VALUES TO B03-MIN-GARTO-T-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getMinGartoT() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-MIN-GARTO-T-NULL
            bilaTrchEstr.getB03MinGartoT().setB03MinGartoTNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03MinGartoT.Len.B03_MIN_GARTO_T_NULL));
        }
        // COB_CODE: IF IND-B03-TP-TARI = -1
        //              MOVE HIGH-VALUES TO B03-TP-TARI-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getTpTari() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-TP-TARI-NULL
            bilaTrchEstr.setB03TpTari(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_TP_TARI));
        }
        // COB_CODE: IF IND-B03-TP-PRE = -1
        //              MOVE HIGH-VALUES TO B03-TP-PRE-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getTpPre() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-TP-PRE-NULL
            bilaTrchEstr.setB03TpPre(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-B03-TP-ADEG-PRE = -1
        //              MOVE HIGH-VALUES TO B03-TP-ADEG-PRE-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getTpAdegPre() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-TP-ADEG-PRE-NULL
            bilaTrchEstr.setB03TpAdegPre(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-B03-TP-RIVAL = -1
        //              MOVE HIGH-VALUES TO B03-TP-RIVAL-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getTpRival() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-TP-RIVAL-NULL
            bilaTrchEstr.setB03TpRival(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_TP_RIVAL));
        }
        // COB_CODE: IF IND-B03-FL-DA-TRASF = -1
        //              MOVE HIGH-VALUES TO B03-FL-DA-TRASF-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getFlDaTrasf() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-FL-DA-TRASF-NULL
            bilaTrchEstr.setB03FlDaTrasf(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-B03-FL-CAR-CONT = -1
        //              MOVE HIGH-VALUES TO B03-FL-CAR-CONT-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getFlCarCont() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-FL-CAR-CONT-NULL
            bilaTrchEstr.setB03FlCarCont(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-B03-FL-PRE-DA-RIS = -1
        //              MOVE HIGH-VALUES TO B03-FL-PRE-DA-RIS-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getFlPreDaRis() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-FL-PRE-DA-RIS-NULL
            bilaTrchEstr.setB03FlPreDaRis(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-B03-FL-PRE-AGG = -1
        //              MOVE HIGH-VALUES TO B03-FL-PRE-AGG-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getFlPreAgg() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-FL-PRE-AGG-NULL
            bilaTrchEstr.setB03FlPreAgg(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-B03-TP-TRCH = -1
        //              MOVE HIGH-VALUES TO B03-TP-TRCH-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getTpTrch() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-TP-TRCH-NULL
            bilaTrchEstr.setB03TpTrch(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_TP_TRCH));
        }
        // COB_CODE: IF IND-B03-TP-TST = -1
        //              MOVE HIGH-VALUES TO B03-TP-TST-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getTpTst() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-TP-TST-NULL
            bilaTrchEstr.setB03TpTst(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_TP_TST));
        }
        // COB_CODE: IF IND-B03-COD-CONV = -1
        //              MOVE HIGH-VALUES TO B03-COD-CONV-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCodConv() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-COD-CONV-NULL
            bilaTrchEstr.setB03CodConv(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_COD_CONV));
        }
        // COB_CODE: IF IND-B03-DT-DECOR-ADES = -1
        //              MOVE HIGH-VALUES TO B03-DT-DECOR-ADES-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtDecorAdes() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DT-DECOR-ADES-NULL
            bilaTrchEstr.getB03DtDecorAdes().setB03DtDecorAdesNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DtDecorAdes.Len.B03_DT_DECOR_ADES_NULL));
        }
        // COB_CODE: IF IND-B03-DT-EMIS-TRCH = -1
        //              MOVE HIGH-VALUES TO B03-DT-EMIS-TRCH-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtEmisTrch() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DT-EMIS-TRCH-NULL
            bilaTrchEstr.getB03DtEmisTrch().setB03DtEmisTrchNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DtEmisTrch.Len.B03_DT_EMIS_TRCH_NULL));
        }
        // COB_CODE: IF IND-B03-DT-SCAD-TRCH = -1
        //              MOVE HIGH-VALUES TO B03-DT-SCAD-TRCH-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtScadTrch() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DT-SCAD-TRCH-NULL
            bilaTrchEstr.getB03DtScadTrch().setB03DtScadTrchNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DtScadTrch.Len.B03_DT_SCAD_TRCH_NULL));
        }
        // COB_CODE: IF IND-B03-DT-SCAD-INTMD = -1
        //              MOVE HIGH-VALUES TO B03-DT-SCAD-INTMD-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtScadIntmd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DT-SCAD-INTMD-NULL
            bilaTrchEstr.getB03DtScadIntmd().setB03DtScadIntmdNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DtScadIntmd.Len.B03_DT_SCAD_INTMD_NULL));
        }
        // COB_CODE: IF IND-B03-DT-SCAD-PAG-PRE = -1
        //              MOVE HIGH-VALUES TO B03-DT-SCAD-PAG-PRE-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtScadPagPre() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DT-SCAD-PAG-PRE-NULL
            bilaTrchEstr.getB03DtScadPagPre().setB03DtScadPagPreNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DtScadPagPre.Len.B03_DT_SCAD_PAG_PRE_NULL));
        }
        // COB_CODE: IF IND-B03-DT-ULT-PRE-PAG = -1
        //              MOVE HIGH-VALUES TO B03-DT-ULT-PRE-PAG-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtUltPrePag() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DT-ULT-PRE-PAG-NULL
            bilaTrchEstr.getB03DtUltPrePag().setB03DtUltPrePagNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DtUltPrePag.Len.B03_DT_ULT_PRE_PAG_NULL));
        }
        // COB_CODE: IF IND-B03-DT-NASC-1O-ASSTO = -1
        //              MOVE HIGH-VALUES TO B03-DT-NASC-1O-ASSTO-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtNasc1oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DT-NASC-1O-ASSTO-NULL
            bilaTrchEstr.getB03DtNasc1oAssto().setB03DtNasc1oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DtNasc1oAssto.Len.B03_DT_NASC1O_ASSTO_NULL));
        }
        // COB_CODE: IF IND-B03-SEX-1O-ASSTO = -1
        //              MOVE HIGH-VALUES TO B03-SEX-1O-ASSTO-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getSex1oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-SEX-1O-ASSTO-NULL
            bilaTrchEstr.setB03Sex1oAssto(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-B03-ETA-AA-1O-ASSTO = -1
        //              MOVE HIGH-VALUES TO B03-ETA-AA-1O-ASSTO-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getEtaAa1oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-ETA-AA-1O-ASSTO-NULL
            bilaTrchEstr.getB03EtaAa1oAssto().setB03EtaAa1oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03EtaAa1oAssto.Len.B03_ETA_AA1O_ASSTO_NULL));
        }
        // COB_CODE: IF IND-B03-ETA-MM-1O-ASSTO = -1
        //              MOVE HIGH-VALUES TO B03-ETA-MM-1O-ASSTO-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getEtaMm1oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-ETA-MM-1O-ASSTO-NULL
            bilaTrchEstr.getB03EtaMm1oAssto().setB03EtaMm1oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03EtaMm1oAssto.Len.B03_ETA_MM1O_ASSTO_NULL));
        }
        // COB_CODE: IF IND-B03-ETA-RAGGN-DT-CALC = -1
        //              MOVE HIGH-VALUES TO B03-ETA-RAGGN-DT-CALC-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getEtaRaggnDtCalc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-ETA-RAGGN-DT-CALC-NULL
            bilaTrchEstr.getB03EtaRaggnDtCalc().setB03EtaRaggnDtCalcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03EtaRaggnDtCalc.Len.B03_ETA_RAGGN_DT_CALC_NULL));
        }
        // COB_CODE: IF IND-B03-DUR-AA = -1
        //              MOVE HIGH-VALUES TO B03-DUR-AA-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDurAa() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DUR-AA-NULL
            bilaTrchEstr.getB03DurAa().setB03DurAaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DurAa.Len.B03_DUR_AA_NULL));
        }
        // COB_CODE: IF IND-B03-DUR-MM = -1
        //              MOVE HIGH-VALUES TO B03-DUR-MM-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDurMm() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DUR-MM-NULL
            bilaTrchEstr.getB03DurMm().setB03DurMmNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DurMm.Len.B03_DUR_MM_NULL));
        }
        // COB_CODE: IF IND-B03-DUR-GG = -1
        //              MOVE HIGH-VALUES TO B03-DUR-GG-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDurGg() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DUR-GG-NULL
            bilaTrchEstr.getB03DurGg().setB03DurGgNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DurGg.Len.B03_DUR_GG_NULL));
        }
        // COB_CODE: IF IND-B03-DUR-1O-PER-AA = -1
        //              MOVE HIGH-VALUES TO B03-DUR-1O-PER-AA-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDur1oPerAa() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DUR-1O-PER-AA-NULL
            bilaTrchEstr.getB03Dur1oPerAa().setB03Dur1oPerAaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03Dur1oPerAa.Len.B03_DUR1O_PER_AA_NULL));
        }
        // COB_CODE: IF IND-B03-DUR-1O-PER-MM = -1
        //              MOVE HIGH-VALUES TO B03-DUR-1O-PER-MM-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDur1oPerMm() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DUR-1O-PER-MM-NULL
            bilaTrchEstr.getB03Dur1oPerMm().setB03Dur1oPerMmNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03Dur1oPerMm.Len.B03_DUR1O_PER_MM_NULL));
        }
        // COB_CODE: IF IND-B03-DUR-1O-PER-GG = -1
        //              MOVE HIGH-VALUES TO B03-DUR-1O-PER-GG-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDur1oPerGg() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DUR-1O-PER-GG-NULL
            bilaTrchEstr.getB03Dur1oPerGg().setB03Dur1oPerGgNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03Dur1oPerGg.Len.B03_DUR1O_PER_GG_NULL));
        }
        // COB_CODE: IF IND-B03-ANTIDUR-RICOR-PREC = -1
        //              MOVE HIGH-VALUES TO B03-ANTIDUR-RICOR-PREC-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getAntidurRicorPrec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-ANTIDUR-RICOR-PREC-NULL
            bilaTrchEstr.getB03AntidurRicorPrec().setB03AntidurRicorPrecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03AntidurRicorPrec.Len.B03_ANTIDUR_RICOR_PREC_NULL));
        }
        // COB_CODE: IF IND-B03-ANTIDUR-DT-CALC = -1
        //              MOVE HIGH-VALUES TO B03-ANTIDUR-DT-CALC-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getAntidurDtCalc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-ANTIDUR-DT-CALC-NULL
            bilaTrchEstr.getB03AntidurDtCalc().setB03AntidurDtCalcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03AntidurDtCalc.Len.B03_ANTIDUR_DT_CALC_NULL));
        }
        // COB_CODE: IF IND-B03-DUR-RES-DT-CALC = -1
        //              MOVE HIGH-VALUES TO B03-DUR-RES-DT-CALC-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDurResDtCalc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DUR-RES-DT-CALC-NULL
            bilaTrchEstr.getB03DurResDtCalc().setB03DurResDtCalcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DurResDtCalc.Len.B03_DUR_RES_DT_CALC_NULL));
        }
        // COB_CODE: IF IND-B03-DT-EFF-CAMB-STAT = -1
        //              MOVE HIGH-VALUES TO B03-DT-EFF-CAMB-STAT-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtEffCambStat() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DT-EFF-CAMB-STAT-NULL
            bilaTrchEstr.getB03DtEffCambStat().setB03DtEffCambStatNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DtEffCambStat.Len.B03_DT_EFF_CAMB_STAT_NULL));
        }
        // COB_CODE: IF IND-B03-DT-EMIS-CAMB-STAT = -1
        //              MOVE HIGH-VALUES TO B03-DT-EMIS-CAMB-STAT-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtEmisCambStat() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DT-EMIS-CAMB-STAT-NULL
            bilaTrchEstr.getB03DtEmisCambStat().setB03DtEmisCambStatNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DtEmisCambStat.Len.B03_DT_EMIS_CAMB_STAT_NULL));
        }
        // COB_CODE: IF IND-B03-DT-EFF-STAB = -1
        //              MOVE HIGH-VALUES TO B03-DT-EFF-STAB-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtEffStab() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DT-EFF-STAB-NULL
            bilaTrchEstr.getB03DtEffStab().setB03DtEffStabNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DtEffStab.Len.B03_DT_EFF_STAB_NULL));
        }
        // COB_CODE: IF IND-B03-CPT-DT-STAB = -1
        //              MOVE HIGH-VALUES TO B03-CPT-DT-STAB-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCptDtStab() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-CPT-DT-STAB-NULL
            bilaTrchEstr.getB03CptDtStab().setB03CptDtStabNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03CptDtStab.Len.B03_CPT_DT_STAB_NULL));
        }
        // COB_CODE: IF IND-B03-DT-EFF-RIDZ = -1
        //              MOVE HIGH-VALUES TO B03-DT-EFF-RIDZ-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtEffRidz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DT-EFF-RIDZ-NULL
            bilaTrchEstr.getB03DtEffRidz().setB03DtEffRidzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DtEffRidz.Len.B03_DT_EFF_RIDZ_NULL));
        }
        // COB_CODE: IF IND-B03-DT-EMIS-RIDZ = -1
        //              MOVE HIGH-VALUES TO B03-DT-EMIS-RIDZ-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtEmisRidz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DT-EMIS-RIDZ-NULL
            bilaTrchEstr.getB03DtEmisRidz().setB03DtEmisRidzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DtEmisRidz.Len.B03_DT_EMIS_RIDZ_NULL));
        }
        // COB_CODE: IF IND-B03-CPT-DT-RIDZ = -1
        //              MOVE HIGH-VALUES TO B03-CPT-DT-RIDZ-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCptDtRidz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-CPT-DT-RIDZ-NULL
            bilaTrchEstr.getB03CptDtRidz().setB03CptDtRidzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03CptDtRidz.Len.B03_CPT_DT_RIDZ_NULL));
        }
        // COB_CODE: IF IND-B03-FRAZ = -1
        //              MOVE HIGH-VALUES TO B03-FRAZ-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getFraz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-FRAZ-NULL
            bilaTrchEstr.getB03Fraz().setB03FrazNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03Fraz.Len.B03_FRAZ_NULL));
        }
        // COB_CODE: IF IND-B03-DUR-PAG-PRE = -1
        //              MOVE HIGH-VALUES TO B03-DUR-PAG-PRE-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDurPagPre() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DUR-PAG-PRE-NULL
            bilaTrchEstr.getB03DurPagPre().setB03DurPagPreNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DurPagPre.Len.B03_DUR_PAG_PRE_NULL));
        }
        // COB_CODE: IF IND-B03-NUM-PRE-PATT = -1
        //              MOVE HIGH-VALUES TO B03-NUM-PRE-PATT-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getNumPrePatt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-NUM-PRE-PATT-NULL
            bilaTrchEstr.getB03NumPrePatt().setB03NumPrePattNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03NumPrePatt.Len.B03_NUM_PRE_PATT_NULL));
        }
        // COB_CODE: IF IND-B03-FRAZ-INI-EROG-REN = -1
        //              MOVE HIGH-VALUES TO B03-FRAZ-INI-EROG-REN-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getFrazIniErogRen() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-FRAZ-INI-EROG-REN-NULL
            bilaTrchEstr.getB03FrazIniErogRen().setB03FrazIniErogRenNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03FrazIniErogRen.Len.B03_FRAZ_INI_EROG_REN_NULL));
        }
        // COB_CODE: IF IND-B03-AA-REN-CER = -1
        //              MOVE HIGH-VALUES TO B03-AA-REN-CER-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getAaRenCer() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-AA-REN-CER-NULL
            bilaTrchEstr.getB03AaRenCer().setB03AaRenCerNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03AaRenCer.Len.B03_AA_REN_CER_NULL));
        }
        // COB_CODE: IF IND-B03-RAT-REN = -1
        //              MOVE HIGH-VALUES TO B03-RAT-REN-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getRatRen() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-RAT-REN-NULL
            bilaTrchEstr.getB03RatRen().setB03RatRenNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03RatRen.Len.B03_RAT_REN_NULL));
        }
        // COB_CODE: IF IND-B03-COD-DIV = -1
        //              MOVE HIGH-VALUES TO B03-COD-DIV-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCodDiv() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-COD-DIV-NULL
            bilaTrchEstr.setB03CodDiv(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_COD_DIV));
        }
        // COB_CODE: IF IND-B03-RISCPAR = -1
        //              MOVE HIGH-VALUES TO B03-RISCPAR-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getRiscpar() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-RISCPAR-NULL
            bilaTrchEstr.getB03Riscpar().setB03RiscparNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03Riscpar.Len.B03_RISCPAR_NULL));
        }
        // COB_CODE: IF IND-B03-CUM-RISCPAR = -1
        //              MOVE HIGH-VALUES TO B03-CUM-RISCPAR-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCumRiscpar() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-CUM-RISCPAR-NULL
            bilaTrchEstr.getB03CumRiscpar().setB03CumRiscparNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03CumRiscpar.Len.B03_CUM_RISCPAR_NULL));
        }
        // COB_CODE: IF IND-B03-ULT-RM = -1
        //              MOVE HIGH-VALUES TO B03-ULT-RM-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getUltRm() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-ULT-RM-NULL
            bilaTrchEstr.getB03UltRm().setB03UltRmNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03UltRm.Len.B03_ULT_RM_NULL));
        }
        // COB_CODE: IF IND-B03-TS-RENDTO-T = -1
        //              MOVE HIGH-VALUES TO B03-TS-RENDTO-T-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getTsRendtoT() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-TS-RENDTO-T-NULL
            bilaTrchEstr.getB03TsRendtoT().setB03TsRendtoTNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03TsRendtoT.Len.B03_TS_RENDTO_T_NULL));
        }
        // COB_CODE: IF IND-B03-ALQ-RETR-T = -1
        //              MOVE HIGH-VALUES TO B03-ALQ-RETR-T-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getAlqRetrT() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-ALQ-RETR-T-NULL
            bilaTrchEstr.getB03AlqRetrT().setB03AlqRetrTNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03AlqRetrT.Len.B03_ALQ_RETR_T_NULL));
        }
        // COB_CODE: IF IND-B03-MIN-TRNUT-T = -1
        //              MOVE HIGH-VALUES TO B03-MIN-TRNUT-T-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getMinTrnutT() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-MIN-TRNUT-T-NULL
            bilaTrchEstr.getB03MinTrnutT().setB03MinTrnutTNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03MinTrnutT.Len.B03_MIN_TRNUT_T_NULL));
        }
        // COB_CODE: IF IND-B03-TS-NET-T = -1
        //              MOVE HIGH-VALUES TO B03-TS-NET-T-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getTsNetT() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-TS-NET-T-NULL
            bilaTrchEstr.getB03TsNetT().setB03TsNetTNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03TsNetT.Len.B03_TS_NET_T_NULL));
        }
        // COB_CODE: IF IND-B03-DT-ULT-RIVAL = -1
        //              MOVE HIGH-VALUES TO B03-DT-ULT-RIVAL-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtUltRival() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DT-ULT-RIVAL-NULL
            bilaTrchEstr.getB03DtUltRival().setB03DtUltRivalNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DtUltRival.Len.B03_DT_ULT_RIVAL_NULL));
        }
        // COB_CODE: IF IND-B03-PRSTZ-INI = -1
        //              MOVE HIGH-VALUES TO B03-PRSTZ-INI-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getPrstzIni() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-PRSTZ-INI-NULL
            bilaTrchEstr.getB03PrstzIni().setB03PrstzIniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03PrstzIni.Len.B03_PRSTZ_INI_NULL));
        }
        // COB_CODE: IF IND-B03-PRSTZ-AGG-INI = -1
        //              MOVE HIGH-VALUES TO B03-PRSTZ-AGG-INI-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getPrstzAggIni() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-PRSTZ-AGG-INI-NULL
            bilaTrchEstr.getB03PrstzAggIni().setB03PrstzAggIniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03PrstzAggIni.Len.B03_PRSTZ_AGG_INI_NULL));
        }
        // COB_CODE: IF IND-B03-PRSTZ-AGG-ULT = -1
        //              MOVE HIGH-VALUES TO B03-PRSTZ-AGG-ULT-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getPrstzAggUlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-PRSTZ-AGG-ULT-NULL
            bilaTrchEstr.getB03PrstzAggUlt().setB03PrstzAggUltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03PrstzAggUlt.Len.B03_PRSTZ_AGG_ULT_NULL));
        }
        // COB_CODE: IF IND-B03-RAPPEL = -1
        //              MOVE HIGH-VALUES TO B03-RAPPEL-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getRappel() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-RAPPEL-NULL
            bilaTrchEstr.getB03Rappel().setB03RappelNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03Rappel.Len.B03_RAPPEL_NULL));
        }
        // COB_CODE: IF IND-B03-PRE-PATTUITO-INI = -1
        //              MOVE HIGH-VALUES TO B03-PRE-PATTUITO-INI-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getPrePattuitoIni() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-PRE-PATTUITO-INI-NULL
            bilaTrchEstr.getB03PrePattuitoIni().setB03PrePattuitoIniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03PrePattuitoIni.Len.B03_PRE_PATTUITO_INI_NULL));
        }
        // COB_CODE: IF IND-B03-PRE-DOV-INI = -1
        //              MOVE HIGH-VALUES TO B03-PRE-DOV-INI-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getPreDovIni() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-PRE-DOV-INI-NULL
            bilaTrchEstr.getB03PreDovIni().setB03PreDovIniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03PreDovIni.Len.B03_PRE_DOV_INI_NULL));
        }
        // COB_CODE: IF IND-B03-PRE-DOV-RIVTO-T = -1
        //              MOVE HIGH-VALUES TO B03-PRE-DOV-RIVTO-T-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getPreDovRivtoT() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-PRE-DOV-RIVTO-T-NULL
            bilaTrchEstr.getB03PreDovRivtoT().setB03PreDovRivtoTNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03PreDovRivtoT.Len.B03_PRE_DOV_RIVTO_T_NULL));
        }
        // COB_CODE: IF IND-B03-PRE-ANNUALIZ-RICOR = -1
        //              MOVE HIGH-VALUES TO B03-PRE-ANNUALIZ-RICOR-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getPreAnnualizRicor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-PRE-ANNUALIZ-RICOR-NULL
            bilaTrchEstr.getB03PreAnnualizRicor().setB03PreAnnualizRicorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03PreAnnualizRicor.Len.B03_PRE_ANNUALIZ_RICOR_NULL));
        }
        // COB_CODE: IF IND-B03-PRE-CONT = -1
        //              MOVE HIGH-VALUES TO B03-PRE-CONT-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getPreCont() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-PRE-CONT-NULL
            bilaTrchEstr.getB03PreCont().setB03PreContNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03PreCont.Len.B03_PRE_CONT_NULL));
        }
        // COB_CODE: IF IND-B03-PRE-PP-INI = -1
        //              MOVE HIGH-VALUES TO B03-PRE-PP-INI-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getPrePpIni() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-PRE-PP-INI-NULL
            bilaTrchEstr.getB03PrePpIni().setB03PrePpIniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03PrePpIni.Len.B03_PRE_PP_INI_NULL));
        }
        // COB_CODE: IF IND-B03-RIS-PURA-T = -1
        //              MOVE HIGH-VALUES TO B03-RIS-PURA-T-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getRisPuraT() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-RIS-PURA-T-NULL
            bilaTrchEstr.getB03RisPuraT().setB03RisPuraTNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03RisPuraT.Len.B03_RIS_PURA_T_NULL));
        }
        // COB_CODE: IF IND-B03-PROV-ACQ = -1
        //              MOVE HIGH-VALUES TO B03-PROV-ACQ-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getProvAcq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-PROV-ACQ-NULL
            bilaTrchEstr.getB03ProvAcq().setB03ProvAcqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03ProvAcq.Len.B03_PROV_ACQ_NULL));
        }
        // COB_CODE: IF IND-B03-PROV-ACQ-RICOR = -1
        //              MOVE HIGH-VALUES TO B03-PROV-ACQ-RICOR-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getProvAcqRicor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-PROV-ACQ-RICOR-NULL
            bilaTrchEstr.getB03ProvAcqRicor().setB03ProvAcqRicorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03ProvAcqRicor.Len.B03_PROV_ACQ_RICOR_NULL));
        }
        // COB_CODE: IF IND-B03-PROV-INC = -1
        //              MOVE HIGH-VALUES TO B03-PROV-INC-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getProvInc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-PROV-INC-NULL
            bilaTrchEstr.getB03ProvInc().setB03ProvIncNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03ProvInc.Len.B03_PROV_INC_NULL));
        }
        // COB_CODE: IF IND-B03-CAR-ACQ-NON-SCON = -1
        //              MOVE HIGH-VALUES TO B03-CAR-ACQ-NON-SCON-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCarAcqNonScon() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-CAR-ACQ-NON-SCON-NULL
            bilaTrchEstr.getB03CarAcqNonScon().setB03CarAcqNonSconNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03CarAcqNonScon.Len.B03_CAR_ACQ_NON_SCON_NULL));
        }
        // COB_CODE: IF IND-B03-OVER-COMM = -1
        //              MOVE HIGH-VALUES TO B03-OVER-COMM-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getOverComm() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-OVER-COMM-NULL
            bilaTrchEstr.getB03OverComm().setB03OverCommNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03OverComm.Len.B03_OVER_COMM_NULL));
        }
        // COB_CODE: IF IND-B03-CAR-ACQ-PRECONTATO = -1
        //              MOVE HIGH-VALUES TO B03-CAR-ACQ-PRECONTATO-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCarAcqPrecontato() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-CAR-ACQ-PRECONTATO-NULL
            bilaTrchEstr.getB03CarAcqPrecontato().setB03CarAcqPrecontatoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03CarAcqPrecontato.Len.B03_CAR_ACQ_PRECONTATO_NULL));
        }
        // COB_CODE: IF IND-B03-RIS-ACQ-T = -1
        //              MOVE HIGH-VALUES TO B03-RIS-ACQ-T-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getRisAcqT() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-RIS-ACQ-T-NULL
            bilaTrchEstr.getB03RisAcqT().setB03RisAcqTNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03RisAcqT.Len.B03_RIS_ACQ_T_NULL));
        }
        // COB_CODE: IF IND-B03-RIS-ZIL-T = -1
        //              MOVE HIGH-VALUES TO B03-RIS-ZIL-T-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getRisZilT() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-RIS-ZIL-T-NULL
            bilaTrchEstr.getB03RisZilT().setB03RisZilTNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03RisZilT.Len.B03_RIS_ZIL_T_NULL));
        }
        // COB_CODE: IF IND-B03-CAR-GEST-NON-SCON = -1
        //              MOVE HIGH-VALUES TO B03-CAR-GEST-NON-SCON-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCarGestNonScon() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-CAR-GEST-NON-SCON-NULL
            bilaTrchEstr.getB03CarGestNonScon().setB03CarGestNonSconNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03CarGestNonScon.Len.B03_CAR_GEST_NON_SCON_NULL));
        }
        // COB_CODE: IF IND-B03-CAR-GEST = -1
        //              MOVE HIGH-VALUES TO B03-CAR-GEST-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCarGest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-CAR-GEST-NULL
            bilaTrchEstr.getB03CarGest().setB03CarGestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03CarGest.Len.B03_CAR_GEST_NULL));
        }
        // COB_CODE: IF IND-B03-RIS-SPE-T = -1
        //              MOVE HIGH-VALUES TO B03-RIS-SPE-T-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getRisSpeT() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-RIS-SPE-T-NULL
            bilaTrchEstr.getB03RisSpeT().setB03RisSpeTNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03RisSpeT.Len.B03_RIS_SPE_T_NULL));
        }
        // COB_CODE: IF IND-B03-CAR-INC-NON-SCON = -1
        //              MOVE HIGH-VALUES TO B03-CAR-INC-NON-SCON-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCarIncNonScon() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-CAR-INC-NON-SCON-NULL
            bilaTrchEstr.getB03CarIncNonScon().setB03CarIncNonSconNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03CarIncNonScon.Len.B03_CAR_INC_NON_SCON_NULL));
        }
        // COB_CODE: IF IND-B03-CAR-INC = -1
        //              MOVE HIGH-VALUES TO B03-CAR-INC-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCarInc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-CAR-INC-NULL
            bilaTrchEstr.getB03CarInc().setB03CarIncNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03CarInc.Len.B03_CAR_INC_NULL));
        }
        // COB_CODE: IF IND-B03-RIS-RISTORNI-CAP = -1
        //              MOVE HIGH-VALUES TO B03-RIS-RISTORNI-CAP-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getRisRistorniCap() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-RIS-RISTORNI-CAP-NULL
            bilaTrchEstr.getB03RisRistorniCap().setB03RisRistorniCapNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03RisRistorniCap.Len.B03_RIS_RISTORNI_CAP_NULL));
        }
        // COB_CODE: IF IND-B03-INTR-TECN = -1
        //              MOVE HIGH-VALUES TO B03-INTR-TECN-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getIntrTecn() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-INTR-TECN-NULL
            bilaTrchEstr.getB03IntrTecn().setB03IntrTecnNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03IntrTecn.Len.B03_INTR_TECN_NULL));
        }
        // COB_CODE: IF IND-B03-CPT-RSH-MOR = -1
        //              MOVE HIGH-VALUES TO B03-CPT-RSH-MOR-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCptRshMor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-CPT-RSH-MOR-NULL
            bilaTrchEstr.getB03CptRshMor().setB03CptRshMorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03CptRshMor.Len.B03_CPT_RSH_MOR_NULL));
        }
        // COB_CODE: IF IND-B03-C-SUBRSH-T = -1
        //              MOVE HIGH-VALUES TO B03-C-SUBRSH-T-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getcSubrshT() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-C-SUBRSH-T-NULL
            bilaTrchEstr.getB03CSubrshT().setB03CSubrshTNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03CSubrshT.Len.B03_C_SUBRSH_T_NULL));
        }
        // COB_CODE: IF IND-B03-PRE-RSH-T = -1
        //              MOVE HIGH-VALUES TO B03-PRE-RSH-T-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getPreRshT() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-PRE-RSH-T-NULL
            bilaTrchEstr.getB03PreRshT().setB03PreRshTNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03PreRshT.Len.B03_PRE_RSH_T_NULL));
        }
        // COB_CODE: IF IND-B03-ALQ-MARG-RIS = -1
        //              MOVE HIGH-VALUES TO B03-ALQ-MARG-RIS-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getAlqMargRis() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-ALQ-MARG-RIS-NULL
            bilaTrchEstr.getB03AlqMargRis().setB03AlqMargRisNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03AlqMargRis.Len.B03_ALQ_MARG_RIS_NULL));
        }
        // COB_CODE: IF IND-B03-ALQ-MARG-C-SUBRSH = -1
        //              MOVE HIGH-VALUES TO B03-ALQ-MARG-C-SUBRSH-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getAlqMargCSubrsh() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-ALQ-MARG-C-SUBRSH-NULL
            bilaTrchEstr.getB03AlqMargCSubrsh().setB03AlqMargCSubrshNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03AlqMargCSubrsh.Len.B03_ALQ_MARG_C_SUBRSH_NULL));
        }
        // COB_CODE: IF IND-B03-TS-RENDTO-SPPR = -1
        //              MOVE HIGH-VALUES TO B03-TS-RENDTO-SPPR-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getTsRendtoSppr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-TS-RENDTO-SPPR-NULL
            bilaTrchEstr.getB03TsRendtoSppr().setB03TsRendtoSpprNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03TsRendtoSppr.Len.B03_TS_RENDTO_SPPR_NULL));
        }
        // COB_CODE: IF IND-B03-TP-IAS = -1
        //              MOVE HIGH-VALUES TO B03-TP-IAS-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getTpIas() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-TP-IAS-NULL
            bilaTrchEstr.setB03TpIas(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_TP_IAS));
        }
        // COB_CODE: IF IND-B03-NS-QUO = -1
        //              MOVE HIGH-VALUES TO B03-NS-QUO-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getNsQuo() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-NS-QUO-NULL
            bilaTrchEstr.getB03NsQuo().setB03NsQuoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03NsQuo.Len.B03_NS_QUO_NULL));
        }
        // COB_CODE: IF IND-B03-TS-MEDIO = -1
        //              MOVE HIGH-VALUES TO B03-TS-MEDIO-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getTsMedio() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-TS-MEDIO-NULL
            bilaTrchEstr.getB03TsMedio().setB03TsMedioNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03TsMedio.Len.B03_TS_MEDIO_NULL));
        }
        // COB_CODE: IF IND-B03-CPT-RIASTO = -1
        //              MOVE HIGH-VALUES TO B03-CPT-RIASTO-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCptRiasto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-CPT-RIASTO-NULL
            bilaTrchEstr.getB03CptRiasto().setB03CptRiastoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03CptRiasto.Len.B03_CPT_RIASTO_NULL));
        }
        // COB_CODE: IF IND-B03-PRE-RIASTO = -1
        //              MOVE HIGH-VALUES TO B03-PRE-RIASTO-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getPreRiasto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-PRE-RIASTO-NULL
            bilaTrchEstr.getB03PreRiasto().setB03PreRiastoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03PreRiasto.Len.B03_PRE_RIASTO_NULL));
        }
        // COB_CODE: IF IND-B03-RIS-RIASTA = -1
        //              MOVE HIGH-VALUES TO B03-RIS-RIASTA-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getRisRiasta() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-RIS-RIASTA-NULL
            bilaTrchEstr.getB03RisRiasta().setB03RisRiastaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03RisRiasta.Len.B03_RIS_RIASTA_NULL));
        }
        // COB_CODE: IF IND-B03-CPT-RIASTO-ECC = -1
        //              MOVE HIGH-VALUES TO B03-CPT-RIASTO-ECC-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCptRiastoEcc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-CPT-RIASTO-ECC-NULL
            bilaTrchEstr.getB03CptRiastoEcc().setB03CptRiastoEccNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03CptRiastoEcc.Len.B03_CPT_RIASTO_ECC_NULL));
        }
        // COB_CODE: IF IND-B03-PRE-RIASTO-ECC = -1
        //              MOVE HIGH-VALUES TO B03-PRE-RIASTO-ECC-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getPreRiastoEcc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-PRE-RIASTO-ECC-NULL
            bilaTrchEstr.getB03PreRiastoEcc().setB03PreRiastoEccNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03PreRiastoEcc.Len.B03_PRE_RIASTO_ECC_NULL));
        }
        // COB_CODE: IF IND-B03-RIS-RIASTA-ECC = -1
        //              MOVE HIGH-VALUES TO B03-RIS-RIASTA-ECC-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getRisRiastaEcc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-RIS-RIASTA-ECC-NULL
            bilaTrchEstr.getB03RisRiastaEcc().setB03RisRiastaEccNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03RisRiastaEcc.Len.B03_RIS_RIASTA_ECC_NULL));
        }
        // COB_CODE: IF IND-B03-COD-AGE = -1
        //              MOVE HIGH-VALUES TO B03-COD-AGE-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCodAge() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-COD-AGE-NULL
            bilaTrchEstr.getB03CodAge().setB03CodAgeNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03CodAge.Len.B03_COD_AGE_NULL));
        }
        // COB_CODE: IF IND-B03-COD-SUBAGE = -1
        //              MOVE HIGH-VALUES TO B03-COD-SUBAGE-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCodSubage() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-COD-SUBAGE-NULL
            bilaTrchEstr.getB03CodSubage().setB03CodSubageNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03CodSubage.Len.B03_COD_SUBAGE_NULL));
        }
        // COB_CODE: IF IND-B03-COD-CAN = -1
        //              MOVE HIGH-VALUES TO B03-COD-CAN-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCodCan() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-COD-CAN-NULL
            bilaTrchEstr.getB03CodCan().setB03CodCanNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03CodCan.Len.B03_COD_CAN_NULL));
        }
        // COB_CODE: IF IND-B03-IB-POLI = -1
        //              MOVE HIGH-VALUES TO B03-IB-POLI-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getIbPoli() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-IB-POLI-NULL
            bilaTrchEstr.setB03IbPoli(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_IB_POLI));
        }
        // COB_CODE: IF IND-B03-IB-ADES = -1
        //              MOVE HIGH-VALUES TO B03-IB-ADES-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getIbAdes() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-IB-ADES-NULL
            bilaTrchEstr.setB03IbAdes(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_IB_ADES));
        }
        // COB_CODE: IF IND-B03-IB-TRCH-DI-GAR = -1
        //              MOVE HIGH-VALUES TO B03-IB-TRCH-DI-GAR-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getIbTrchDiGar() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-IB-TRCH-DI-GAR-NULL
            bilaTrchEstr.setB03IbTrchDiGar(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_IB_TRCH_DI_GAR));
        }
        // COB_CODE: IF IND-B03-TP-PRSTZ = -1
        //              MOVE HIGH-VALUES TO B03-TP-PRSTZ-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getTpPrstz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-TP-PRSTZ-NULL
            bilaTrchEstr.setB03TpPrstz(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_TP_PRSTZ));
        }
        // COB_CODE: IF IND-B03-TP-TRASF = -1
        //              MOVE HIGH-VALUES TO B03-TP-TRASF-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getTpTrasf() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-TP-TRASF-NULL
            bilaTrchEstr.setB03TpTrasf(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_TP_TRASF));
        }
        // COB_CODE: IF IND-B03-PP-INVRIO-TARI = -1
        //              MOVE HIGH-VALUES TO B03-PP-INVRIO-TARI-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getPpInvrioTari() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-PP-INVRIO-TARI-NULL
            bilaTrchEstr.setB03PpInvrioTari(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-B03-COEFF-OPZ-REN = -1
        //              MOVE HIGH-VALUES TO B03-COEFF-OPZ-REN-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCoeffOpzRen() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-COEFF-OPZ-REN-NULL
            bilaTrchEstr.getB03CoeffOpzRen().setB03CoeffOpzRenNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03CoeffOpzRen.Len.B03_COEFF_OPZ_REN_NULL));
        }
        // COB_CODE: IF IND-B03-COEFF-OPZ-CPT = -1
        //              MOVE HIGH-VALUES TO B03-COEFF-OPZ-CPT-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCoeffOpzCpt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-COEFF-OPZ-CPT-NULL
            bilaTrchEstr.getB03CoeffOpzCpt().setB03CoeffOpzCptNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03CoeffOpzCpt.Len.B03_COEFF_OPZ_CPT_NULL));
        }
        // COB_CODE: IF IND-B03-DUR-PAG-REN = -1
        //              MOVE HIGH-VALUES TO B03-DUR-PAG-REN-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDurPagRen() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DUR-PAG-REN-NULL
            bilaTrchEstr.getB03DurPagRen().setB03DurPagRenNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DurPagRen.Len.B03_DUR_PAG_REN_NULL));
        }
        // COB_CODE: IF IND-B03-VLT = -1
        //              MOVE HIGH-VALUES TO B03-VLT-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getVlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-VLT-NULL
            bilaTrchEstr.setB03Vlt(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_VLT));
        }
        // COB_CODE: IF IND-B03-RIS-MAT-CHIU-PREC = -1
        //              MOVE HIGH-VALUES TO B03-RIS-MAT-CHIU-PREC-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getRisMatChiuPrec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-RIS-MAT-CHIU-PREC-NULL
            bilaTrchEstr.getB03RisMatChiuPrec().setB03RisMatChiuPrecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03RisMatChiuPrec.Len.B03_RIS_MAT_CHIU_PREC_NULL));
        }
        // COB_CODE: IF IND-B03-COD-FND = -1
        //              MOVE HIGH-VALUES TO B03-COD-FND-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCodFnd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-COD-FND-NULL
            bilaTrchEstr.setB03CodFnd(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_COD_FND));
        }
        // COB_CODE: IF IND-B03-PRSTZ-T = -1
        //              MOVE HIGH-VALUES TO B03-PRSTZ-T-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getPrstzT() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-PRSTZ-T-NULL
            bilaTrchEstr.getB03PrstzT().setB03PrstzTNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03PrstzT.Len.B03_PRSTZ_T_NULL));
        }
        // COB_CODE: IF IND-B03-TS-TARI-DOV = -1
        //              MOVE HIGH-VALUES TO B03-TS-TARI-DOV-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getTsTariDov() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-TS-TARI-DOV-NULL
            bilaTrchEstr.getB03TsTariDov().setB03TsTariDovNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03TsTariDov.Len.B03_TS_TARI_DOV_NULL));
        }
        // COB_CODE: IF IND-B03-TS-TARI-SCON = -1
        //              MOVE HIGH-VALUES TO B03-TS-TARI-SCON-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getTsTariScon() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-TS-TARI-SCON-NULL
            bilaTrchEstr.getB03TsTariScon().setB03TsTariSconNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03TsTariScon.Len.B03_TS_TARI_SCON_NULL));
        }
        // COB_CODE: IF IND-B03-TS-PP = -1
        //              MOVE HIGH-VALUES TO B03-TS-PP-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getTsPp() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-TS-PP-NULL
            bilaTrchEstr.getB03TsPp().setB03TsPpNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03TsPp.Len.B03_TS_PP_NULL));
        }
        // COB_CODE: IF IND-B03-COEFF-RIS-1-T = -1
        //              MOVE HIGH-VALUES TO B03-COEFF-RIS-1-T-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCoeffRis1T() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-COEFF-RIS-1-T-NULL
            bilaTrchEstr.getB03CoeffRis1T().setB03CoeffRis1TNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03CoeffRis1T.Len.B03_COEFF_RIS1_T_NULL));
        }
        // COB_CODE: IF IND-B03-COEFF-RIS-2-T = -1
        //              MOVE HIGH-VALUES TO B03-COEFF-RIS-2-T-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCoeffRis2T() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-COEFF-RIS-2-T-NULL
            bilaTrchEstr.getB03CoeffRis2T().setB03CoeffRis2TNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03CoeffRis2T.Len.B03_COEFF_RIS2_T_NULL));
        }
        // COB_CODE: IF IND-B03-ABB = -1
        //              MOVE HIGH-VALUES TO B03-ABB-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getAbb() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-ABB-NULL
            bilaTrchEstr.getB03Abb().setB03AbbNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03Abb.Len.B03_ABB_NULL));
        }
        // COB_CODE: IF IND-B03-TP-COASS = -1
        //              MOVE HIGH-VALUES TO B03-TP-COASS-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getTpCoass() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-TP-COASS-NULL
            bilaTrchEstr.setB03TpCoass(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_TP_COASS));
        }
        // COB_CODE: IF IND-B03-TRAT-RIASS = -1
        //              MOVE HIGH-VALUES TO B03-TRAT-RIASS-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getTratRiass() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-TRAT-RIASS-NULL
            bilaTrchEstr.setB03TratRiass(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_TRAT_RIASS));
        }
        // COB_CODE: IF IND-B03-TRAT-RIASS-ECC = -1
        //              MOVE HIGH-VALUES TO B03-TRAT-RIASS-ECC-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getTratRiassEcc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-TRAT-RIASS-ECC-NULL
            bilaTrchEstr.setB03TratRiassEcc(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_TRAT_RIASS_ECC));
        }
        // COB_CODE: IF IND-B03-TP-RGM-FISC = -1
        //              MOVE HIGH-VALUES TO B03-TP-RGM-FISC-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getTpRgmFisc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-TP-RGM-FISC-NULL
            bilaTrchEstr.setB03TpRgmFisc(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_TP_RGM_FISC));
        }
        // COB_CODE: IF IND-B03-DUR-GAR-AA = -1
        //              MOVE HIGH-VALUES TO B03-DUR-GAR-AA-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDurGarAa() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DUR-GAR-AA-NULL
            bilaTrchEstr.getB03DurGarAa().setB03DurGarAaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DurGarAa.Len.B03_DUR_GAR_AA_NULL));
        }
        // COB_CODE: IF IND-B03-DUR-GAR-MM = -1
        //              MOVE HIGH-VALUES TO B03-DUR-GAR-MM-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDurGarMm() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DUR-GAR-MM-NULL
            bilaTrchEstr.getB03DurGarMm().setB03DurGarMmNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DurGarMm.Len.B03_DUR_GAR_MM_NULL));
        }
        // COB_CODE: IF IND-B03-DUR-GAR-GG = -1
        //              MOVE HIGH-VALUES TO B03-DUR-GAR-GG-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDurGarGg() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DUR-GAR-GG-NULL
            bilaTrchEstr.getB03DurGarGg().setB03DurGarGgNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DurGarGg.Len.B03_DUR_GAR_GG_NULL));
        }
        // COB_CODE: IF IND-B03-ANTIDUR-CALC-365 = -1
        //              MOVE HIGH-VALUES TO B03-ANTIDUR-CALC-365-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getAntidurCalc365() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-ANTIDUR-CALC-365-NULL
            bilaTrchEstr.getB03AntidurCalc365().setB03AntidurCalc365Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03AntidurCalc365.Len.B03_ANTIDUR_CALC365_NULL));
        }
        // COB_CODE: IF IND-B03-COD-FISC-CNTR = -1
        //              MOVE HIGH-VALUES TO B03-COD-FISC-CNTR-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCodFiscCntr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-COD-FISC-CNTR-NULL
            bilaTrchEstr.setB03CodFiscCntr(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_COD_FISC_CNTR));
        }
        // COB_CODE: IF IND-B03-COD-FISC-ASSTO1 = -1
        //              MOVE HIGH-VALUES TO B03-COD-FISC-ASSTO1-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCodFiscAssto1() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-COD-FISC-ASSTO1-NULL
            bilaTrchEstr.setB03CodFiscAssto1(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_COD_FISC_ASSTO1));
        }
        // COB_CODE: IF IND-B03-COD-FISC-ASSTO2 = -1
        //              MOVE HIGH-VALUES TO B03-COD-FISC-ASSTO2-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCodFiscAssto2() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-COD-FISC-ASSTO2-NULL
            bilaTrchEstr.setB03CodFiscAssto2(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_COD_FISC_ASSTO2));
        }
        // COB_CODE: IF IND-B03-COD-FISC-ASSTO3 = -1
        //              MOVE HIGH-VALUES TO B03-COD-FISC-ASSTO3-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCodFiscAssto3() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-COD-FISC-ASSTO3-NULL
            bilaTrchEstr.setB03CodFiscAssto3(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_COD_FISC_ASSTO3));
        }
        // COB_CODE: IF IND-B03-CAUS-SCON = -1
        //              MOVE HIGH-VALUES TO B03-CAUS-SCON
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCausScon() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-CAUS-SCON
            bilaTrchEstr.setB03CausScon(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_CAUS_SCON));
        }
        // COB_CODE: IF IND-B03-EMIT-TIT-OPZ = -1
        //              MOVE HIGH-VALUES TO B03-EMIT-TIT-OPZ
        //           END-IF
        if (ws.getIndBilaTrchEstr().getEmitTitOpz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-EMIT-TIT-OPZ
            bilaTrchEstr.setB03EmitTitOpz(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_EMIT_TIT_OPZ));
        }
        // COB_CODE: IF IND-B03-QTZ-SP-Z-COUP-EMIS = -1
        //              MOVE HIGH-VALUES TO B03-QTZ-SP-Z-COUP-EMIS-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getQtzSpZCoupEmis() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-QTZ-SP-Z-COUP-EMIS-NULL
            bilaTrchEstr.getB03QtzSpZCoupEmis().setB03QtzSpZCoupEmisNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03QtzSpZCoupEmis.Len.B03_QTZ_SP_Z_COUP_EMIS_NULL));
        }
        // COB_CODE: IF IND-B03-QTZ-SP-Z-OPZ-EMIS = -1
        //              MOVE HIGH-VALUES TO B03-QTZ-SP-Z-OPZ-EMIS-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getQtzSpZOpzEmis() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-QTZ-SP-Z-OPZ-EMIS-NULL
            bilaTrchEstr.getB03QtzSpZOpzEmis().setB03QtzSpZOpzEmisNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03QtzSpZOpzEmis.Len.B03_QTZ_SP_Z_OPZ_EMIS_NULL));
        }
        // COB_CODE: IF IND-B03-QTZ-SP-Z-COUP-DT-C = -1
        //              MOVE HIGH-VALUES TO B03-QTZ-SP-Z-COUP-DT-C-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getQtzSpZCoupDtC() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-QTZ-SP-Z-COUP-DT-C-NULL
            bilaTrchEstr.getB03QtzSpZCoupDtC().setB03QtzSpZCoupDtCNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03QtzSpZCoupDtC.Len.B03_QTZ_SP_Z_COUP_DT_C_NULL));
        }
        // COB_CODE: IF IND-B03-QTZ-SP-Z-OPZ-DT-CA = -1
        //              MOVE HIGH-VALUES TO B03-QTZ-SP-Z-OPZ-DT-CA-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getQtzSpZOpzDtCa() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-QTZ-SP-Z-OPZ-DT-CA-NULL
            bilaTrchEstr.getB03QtzSpZOpzDtCa().setB03QtzSpZOpzDtCaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03QtzSpZOpzDtCa.Len.B03_QTZ_SP_Z_OPZ_DT_CA_NULL));
        }
        // COB_CODE: IF IND-B03-QTZ-TOT-EMIS = -1
        //              MOVE HIGH-VALUES TO B03-QTZ-TOT-EMIS-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getQtzTotEmis() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-QTZ-TOT-EMIS-NULL
            bilaTrchEstr.getB03QtzTotEmis().setB03QtzTotEmisNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03QtzTotEmis.Len.B03_QTZ_TOT_EMIS_NULL));
        }
        // COB_CODE: IF IND-B03-QTZ-TOT-DT-CALC = -1
        //              MOVE HIGH-VALUES TO B03-QTZ-TOT-DT-CALC-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getQtzTotDtCalc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-QTZ-TOT-DT-CALC-NULL
            bilaTrchEstr.getB03QtzTotDtCalc().setB03QtzTotDtCalcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03QtzTotDtCalc.Len.B03_QTZ_TOT_DT_CALC_NULL));
        }
        // COB_CODE: IF IND-B03-QTZ-TOT-DT-ULT-BIL = -1
        //              MOVE HIGH-VALUES TO B03-QTZ-TOT-DT-ULT-BIL-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getQtzTotDtUltBil() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-QTZ-TOT-DT-ULT-BIL-NULL
            bilaTrchEstr.getB03QtzTotDtUltBil().setB03QtzTotDtUltBilNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03QtzTotDtUltBil.Len.B03_QTZ_TOT_DT_ULT_BIL_NULL));
        }
        // COB_CODE: IF IND-B03-DT-QTZ-EMIS = -1
        //              MOVE HIGH-VALUES TO B03-DT-QTZ-EMIS-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtQtzEmis() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DT-QTZ-EMIS-NULL
            bilaTrchEstr.getB03DtQtzEmis().setB03DtQtzEmisNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DtQtzEmis.Len.B03_DT_QTZ_EMIS_NULL));
        }
        // COB_CODE: IF IND-B03-PC-CAR-GEST = -1
        //              MOVE HIGH-VALUES TO B03-PC-CAR-GEST-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getPcCarGest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-PC-CAR-GEST-NULL
            bilaTrchEstr.getB03PcCarGest().setB03PcCarGestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03PcCarGest.Len.B03_PC_CAR_GEST_NULL));
        }
        // COB_CODE: IF IND-B03-PC-CAR-ACQ = -1
        //              MOVE HIGH-VALUES TO B03-PC-CAR-ACQ-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getPcCarAcq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-PC-CAR-ACQ-NULL
            bilaTrchEstr.getB03PcCarAcq().setB03PcCarAcqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03PcCarAcq.Len.B03_PC_CAR_ACQ_NULL));
        }
        // COB_CODE: IF IND-B03-IMP-CAR-CASO-MOR = -1
        //              MOVE HIGH-VALUES TO B03-IMP-CAR-CASO-MOR-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getImpCarCasoMor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-IMP-CAR-CASO-MOR-NULL
            bilaTrchEstr.getB03ImpCarCasoMor().setB03ImpCarCasoMorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03ImpCarCasoMor.Len.B03_IMP_CAR_CASO_MOR_NULL));
        }
        // COB_CODE: IF IND-B03-PC-CAR-MOR = -1
        //              MOVE HIGH-VALUES TO B03-PC-CAR-MOR-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getPcCarMor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-PC-CAR-MOR-NULL
            bilaTrchEstr.getB03PcCarMor().setB03PcCarMorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03PcCarMor.Len.B03_PC_CAR_MOR_NULL));
        }
        // COB_CODE: IF IND-B03-TP-VERS = -1
        //              MOVE HIGH-VALUES TO B03-TP-VERS-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getTpVers() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-TP-VERS-NULL
            bilaTrchEstr.setB03TpVers(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-B03-FL-SWITCH = -1
        //              MOVE HIGH-VALUES TO B03-FL-SWITCH-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getFlSwitch() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-FL-SWITCH-NULL
            bilaTrchEstr.setB03FlSwitch(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-B03-FL-IAS = -1
        //              MOVE HIGH-VALUES TO B03-FL-IAS-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getFlIas() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-FL-IAS-NULL
            bilaTrchEstr.setB03FlIas(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-B03-DIR = -1
        //              MOVE HIGH-VALUES TO B03-DIR-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDir() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DIR-NULL
            bilaTrchEstr.getB03Dir().setB03DirNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03Dir.Len.B03_DIR_NULL));
        }
        // COB_CODE: IF IND-B03-TP-COP-CASO-MOR = -1
        //              MOVE HIGH-VALUES TO B03-TP-COP-CASO-MOR-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getTpCopCasoMor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-TP-COP-CASO-MOR-NULL
            bilaTrchEstr.setB03TpCopCasoMor(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_TP_COP_CASO_MOR));
        }
        // COB_CODE: IF IND-B03-MET-RISC-SPCL = -1
        //              MOVE HIGH-VALUES TO B03-MET-RISC-SPCL-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getMetRiscSpcl() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-MET-RISC-SPCL-NULL
            bilaTrchEstr.getB03MetRiscSpcl().setB03MetRiscSpclNull(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-B03-TP-STAT-INVST = -1
        //              MOVE HIGH-VALUES TO B03-TP-STAT-INVST-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getTpStatInvst() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-TP-STAT-INVST-NULL
            bilaTrchEstr.setB03TpStatInvst(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_TP_STAT_INVST));
        }
        // COB_CODE: IF IND-B03-COD-PRDT = -1
        //              MOVE HIGH-VALUES TO B03-COD-PRDT-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCodPrdt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-COD-PRDT-NULL
            bilaTrchEstr.getB03CodPrdt().setB03CodPrdtNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03CodPrdt.Len.B03_COD_PRDT_NULL));
        }
        // COB_CODE: IF IND-B03-STAT-ASSTO-1 = -1
        //              MOVE HIGH-VALUES TO B03-STAT-ASSTO-1-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getStatAssto1() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-STAT-ASSTO-1-NULL
            bilaTrchEstr.setB03StatAssto1(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-B03-STAT-ASSTO-2 = -1
        //              MOVE HIGH-VALUES TO B03-STAT-ASSTO-2-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getStatAssto2() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-STAT-ASSTO-2-NULL
            bilaTrchEstr.setB03StatAssto2(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-B03-STAT-ASSTO-3 = -1
        //              MOVE HIGH-VALUES TO B03-STAT-ASSTO-3-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getStatAssto3() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-STAT-ASSTO-3-NULL
            bilaTrchEstr.setB03StatAssto3(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-B03-CPT-ASSTO-INI-MOR = -1
        //              MOVE HIGH-VALUES TO B03-CPT-ASSTO-INI-MOR-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCptAsstoIniMor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-CPT-ASSTO-INI-MOR-NULL
            bilaTrchEstr.getB03CptAsstoIniMor().setB03CptAsstoIniMorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03CptAsstoIniMor.Len.B03_CPT_ASSTO_INI_MOR_NULL));
        }
        // COB_CODE: IF IND-B03-TS-STAB-PRE = -1
        //              MOVE HIGH-VALUES TO B03-TS-STAB-PRE-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getTsStabPre() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-TS-STAB-PRE-NULL
            bilaTrchEstr.getB03TsStabPre().setB03TsStabPreNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03TsStabPre.Len.B03_TS_STAB_PRE_NULL));
        }
        // COB_CODE: IF IND-B03-DIR-EMIS = -1
        //              MOVE HIGH-VALUES TO B03-DIR-EMIS-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDirEmis() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DIR-EMIS-NULL
            bilaTrchEstr.getB03DirEmis().setB03DirEmisNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DirEmis.Len.B03_DIR_EMIS_NULL));
        }
        // COB_CODE: IF IND-B03-DT-INC-ULT-PRE = -1
        //              MOVE HIGH-VALUES TO B03-DT-INC-ULT-PRE-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtIncUltPre() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DT-INC-ULT-PRE-NULL
            bilaTrchEstr.getB03DtIncUltPre().setB03DtIncUltPreNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DtIncUltPre.Len.B03_DT_INC_ULT_PRE_NULL));
        }
        // COB_CODE: IF IND-B03-STAT-TBGC-ASSTO-1 = -1
        //              MOVE HIGH-VALUES TO B03-STAT-TBGC-ASSTO-1-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getStatTbgcAssto1() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-STAT-TBGC-ASSTO-1-NULL
            bilaTrchEstr.setB03StatTbgcAssto1(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-B03-STAT-TBGC-ASSTO-2 = -1
        //              MOVE HIGH-VALUES TO B03-STAT-TBGC-ASSTO-2-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getStatTbgcAssto2() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-STAT-TBGC-ASSTO-2-NULL
            bilaTrchEstr.setB03StatTbgcAssto2(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-B03-STAT-TBGC-ASSTO-3 = -1
        //              MOVE HIGH-VALUES TO B03-STAT-TBGC-ASSTO-3-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getStatTbgcAssto3() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-STAT-TBGC-ASSTO-3-NULL
            bilaTrchEstr.setB03StatTbgcAssto3(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-B03-FRAZ-DECR-CPT = -1
        //              MOVE HIGH-VALUES TO B03-FRAZ-DECR-CPT-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getFrazDecrCpt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-FRAZ-DECR-CPT-NULL
            bilaTrchEstr.getB03FrazDecrCpt().setB03FrazDecrCptNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03FrazDecrCpt.Len.B03_FRAZ_DECR_CPT_NULL));
        }
        // COB_CODE: IF IND-B03-PRE-PP-ULT = -1
        //              MOVE HIGH-VALUES TO B03-PRE-PP-ULT-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getPrePpUlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-PRE-PP-ULT-NULL
            bilaTrchEstr.getB03PrePpUlt().setB03PrePpUltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03PrePpUlt.Len.B03_PRE_PP_ULT_NULL));
        }
        // COB_CODE: IF IND-B03-ACQ-EXP = -1
        //              MOVE HIGH-VALUES TO B03-ACQ-EXP-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getAcqExp() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-ACQ-EXP-NULL
            bilaTrchEstr.getB03AcqExp().setB03AcqExpNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03AcqExp.Len.B03_ACQ_EXP_NULL));
        }
        // COB_CODE: IF IND-B03-REMUN-ASS = -1
        //              MOVE HIGH-VALUES TO B03-REMUN-ASS-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getRemunAss() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-REMUN-ASS-NULL
            bilaTrchEstr.getB03RemunAss().setB03RemunAssNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03RemunAss.Len.B03_REMUN_ASS_NULL));
        }
        // COB_CODE: IF IND-B03-COMMIS-INTER = -1
        //              MOVE HIGH-VALUES TO B03-COMMIS-INTER-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCommisInter() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-COMMIS-INTER-NULL
            bilaTrchEstr.getB03CommisInter().setB03CommisInterNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03CommisInter.Len.B03_COMMIS_INTER_NULL));
        }
        // COB_CODE: IF IND-B03-NUM-FINANZ = -1
        //              MOVE HIGH-VALUES TO B03-NUM-FINANZ-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getNumFinanz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-NUM-FINANZ-NULL
            bilaTrchEstr.setB03NumFinanz(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_NUM_FINANZ));
        }
        // COB_CODE: IF IND-B03-TP-ACC-COMM = -1
        //              MOVE HIGH-VALUES TO B03-TP-ACC-COMM-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getTpAccComm() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-TP-ACC-COMM-NULL
            bilaTrchEstr.setB03TpAccComm(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_TP_ACC_COMM));
        }
        // COB_CODE: IF IND-B03-IB-ACC-COMM = -1
        //              MOVE HIGH-VALUES TO B03-IB-ACC-COMM-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getIbAccComm() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-IB-ACC-COMM-NULL
            bilaTrchEstr.setB03IbAccComm(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_IB_ACC_COMM));
        }
        // COB_CODE: IF IND-B03-CARZ = -1
        //              MOVE HIGH-VALUES TO B03-CARZ-NULL
        //           END-IF.
        if (ws.getIndBilaTrchEstr().getCarz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-CARZ-NULL
            bilaTrchEstr.getB03Carz().setB03CarzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03Carz.Len.B03_CARZ_NULL));
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE B03-DT-RIS-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getBilaTrchEstrDb().getRisDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO B03-DT-RIS
        bilaTrchEstr.setB03DtRis(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE B03-DT-PRODUZIONE-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getBilaTrchEstrDb().getProduzioneDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO B03-DT-PRODUZIONE
        bilaTrchEstr.setB03DtProduzione(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-B03-DT-INI-VAL-TAR = 0
        //               MOVE WS-DATE-N      TO B03-DT-INI-VAL-TAR
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtIniValTar() == 0) {
            // COB_CODE: MOVE B03-DT-INI-VAL-TAR-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getBilaTrchEstrDb().getIniValTarDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO B03-DT-INI-VAL-TAR
            bilaTrchEstr.getB03DtIniValTar().setB03DtIniValTar(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: MOVE B03-DT-INI-VLDT-PROD-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getBilaTrchEstrDb().getIniVldtProdDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO B03-DT-INI-VLDT-PROD
        bilaTrchEstr.setB03DtIniVldtProd(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE B03-DT-DECOR-POLI-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getBilaTrchEstrDb().getDecorPoliDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO B03-DT-DECOR-POLI
        bilaTrchEstr.setB03DtDecorPoli(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-B03-DT-DECOR-ADES = 0
        //               MOVE WS-DATE-N      TO B03-DT-DECOR-ADES
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtDecorAdes() == 0) {
            // COB_CODE: MOVE B03-DT-DECOR-ADES-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getBilaTrchEstrDb().getDecorAdesDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO B03-DT-DECOR-ADES
            bilaTrchEstr.getB03DtDecorAdes().setB03DtDecorAdes(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: MOVE B03-DT-DECOR-TRCH-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getBilaTrchEstrDb().getDecorTrchDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO B03-DT-DECOR-TRCH
        bilaTrchEstr.setB03DtDecorTrch(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE B03-DT-EMIS-POLI-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getBilaTrchEstrDb().getEmisPoliDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO B03-DT-EMIS-POLI
        bilaTrchEstr.setB03DtEmisPoli(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-B03-DT-EMIS-TRCH = 0
        //               MOVE WS-DATE-N      TO B03-DT-EMIS-TRCH
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtEmisTrch() == 0) {
            // COB_CODE: MOVE B03-DT-EMIS-TRCH-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getBilaTrchEstrDb().getEmisTrchDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO B03-DT-EMIS-TRCH
            bilaTrchEstr.getB03DtEmisTrch().setB03DtEmisTrch(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-B03-DT-SCAD-TRCH = 0
        //               MOVE WS-DATE-N      TO B03-DT-SCAD-TRCH
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtScadTrch() == 0) {
            // COB_CODE: MOVE B03-DT-SCAD-TRCH-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getBilaTrchEstrDb().getScadTrchDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO B03-DT-SCAD-TRCH
            bilaTrchEstr.getB03DtScadTrch().setB03DtScadTrch(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-B03-DT-SCAD-INTMD = 0
        //               MOVE WS-DATE-N      TO B03-DT-SCAD-INTMD
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtScadIntmd() == 0) {
            // COB_CODE: MOVE B03-DT-SCAD-INTMD-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getBilaTrchEstrDb().getScadIntmdDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO B03-DT-SCAD-INTMD
            bilaTrchEstr.getB03DtScadIntmd().setB03DtScadIntmd(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-B03-DT-SCAD-PAG-PRE = 0
        //               MOVE WS-DATE-N      TO B03-DT-SCAD-PAG-PRE
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtScadPagPre() == 0) {
            // COB_CODE: MOVE B03-DT-SCAD-PAG-PRE-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getBilaTrchEstrDb().getScadPagPreDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO B03-DT-SCAD-PAG-PRE
            bilaTrchEstr.getB03DtScadPagPre().setB03DtScadPagPre(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-B03-DT-ULT-PRE-PAG = 0
        //               MOVE WS-DATE-N      TO B03-DT-ULT-PRE-PAG
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtUltPrePag() == 0) {
            // COB_CODE: MOVE B03-DT-ULT-PRE-PAG-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getBilaTrchEstrDb().getUltPrePagDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO B03-DT-ULT-PRE-PAG
            bilaTrchEstr.getB03DtUltPrePag().setB03DtUltPrePag(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-B03-DT-NASC-1O-ASSTO = 0
        //               MOVE WS-DATE-N      TO B03-DT-NASC-1O-ASSTO
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtNasc1oAssto() == 0) {
            // COB_CODE: MOVE B03-DT-NASC-1O-ASSTO-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getBilaTrchEstrDb().getNasc1oAsstoDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO B03-DT-NASC-1O-ASSTO
            bilaTrchEstr.getB03DtNasc1oAssto().setB03DtNasc1oAssto(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-B03-DT-EFF-CAMB-STAT = 0
        //               MOVE WS-DATE-N      TO B03-DT-EFF-CAMB-STAT
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtEffCambStat() == 0) {
            // COB_CODE: MOVE B03-DT-EFF-CAMB-STAT-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getBilaTrchEstrDb().getEffCambStatDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO B03-DT-EFF-CAMB-STAT
            bilaTrchEstr.getB03DtEffCambStat().setB03DtEffCambStat(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-B03-DT-EMIS-CAMB-STAT = 0
        //               MOVE WS-DATE-N      TO B03-DT-EMIS-CAMB-STAT
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtEmisCambStat() == 0) {
            // COB_CODE: MOVE B03-DT-EMIS-CAMB-STAT-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getBilaTrchEstrDb().getEmisCambStatDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO B03-DT-EMIS-CAMB-STAT
            bilaTrchEstr.getB03DtEmisCambStat().setB03DtEmisCambStat(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-B03-DT-EFF-STAB = 0
        //               MOVE WS-DATE-N      TO B03-DT-EFF-STAB
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtEffStab() == 0) {
            // COB_CODE: MOVE B03-DT-EFF-STAB-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getBilaTrchEstrDb().getEffStabDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO B03-DT-EFF-STAB
            bilaTrchEstr.getB03DtEffStab().setB03DtEffStab(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-B03-DT-EFF-RIDZ = 0
        //               MOVE WS-DATE-N      TO B03-DT-EFF-RIDZ
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtEffRidz() == 0) {
            // COB_CODE: MOVE B03-DT-EFF-RIDZ-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getBilaTrchEstrDb().getEffRidzDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO B03-DT-EFF-RIDZ
            bilaTrchEstr.getB03DtEffRidz().setB03DtEffRidz(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-B03-DT-EMIS-RIDZ = 0
        //               MOVE WS-DATE-N      TO B03-DT-EMIS-RIDZ
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtEmisRidz() == 0) {
            // COB_CODE: MOVE B03-DT-EMIS-RIDZ-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getBilaTrchEstrDb().getEmisRidzDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO B03-DT-EMIS-RIDZ
            bilaTrchEstr.getB03DtEmisRidz().setB03DtEmisRidz(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-B03-DT-ULT-RIVAL = 0
        //               MOVE WS-DATE-N      TO B03-DT-ULT-RIVAL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtUltRival() == 0) {
            // COB_CODE: MOVE B03-DT-ULT-RIVAL-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getBilaTrchEstrDb().getUltRivalDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO B03-DT-ULT-RIVAL
            bilaTrchEstr.getB03DtUltRival().setB03DtUltRival(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-B03-DT-QTZ-EMIS = 0
        //               MOVE WS-DATE-N      TO B03-DT-QTZ-EMIS
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtQtzEmis() == 0) {
            // COB_CODE: MOVE B03-DT-QTZ-EMIS-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getBilaTrchEstrDb().getQtzEmisDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO B03-DT-QTZ-EMIS
            bilaTrchEstr.getB03DtQtzEmis().setB03DtQtzEmis(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-B03-DT-INC-ULT-PRE = 0
        //               MOVE WS-DATE-N      TO B03-DT-INC-ULT-PRE
        //           END-IF.
        if (ws.getIndBilaTrchEstr().getDtIncUltPre() == 0) {
            // COB_CODE: MOVE B03-DT-INC-ULT-PRE-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getBilaTrchEstrDb().getIncUltPreDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO B03-DT-INC-ULT-PRE
            bilaTrchEstr.getB03DtIncUltPre().setB03DtIncUltPre(ws.getIdsv0010().getWsDateN());
        }
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
        // COB_CODE: MOVE LENGTH OF B03-EMIT-TIT-OPZ
        //                       TO B03-EMIT-TIT-OPZ-LEN.
        bilaTrchEstr.setB03EmitTitOpzLen(((short)BilaTrchEstrIdbsb030.Len.B03_EMIT_TIT_OPZ));
    }

    /**Original name: Z970-CODICE-ADHOC-PRE<br>
	 * <pre>----
	 * ----  prevede statements AD HOC PRE Query
	 * ----</pre>*/
    private void z970CodiceAdhocPre() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z980-CODICE-ADHOC-POST<br>
	 * <pre>----
	 * ----  prevede statements AD HOC POST Query
	 * ----</pre>*/
    private void z980CodiceAdhocPost() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IDSP0003:line=25, because the code is unreachable.
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IDSP0003:line=33, because the code is unreachable.
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public int getAaRenCer() {
        throw new FieldNotMappedException("aaRenCer");
    }

    @Override
    public void setAaRenCer(int aaRenCer) {
        throw new FieldNotMappedException("aaRenCer");
    }

    @Override
    public Integer getAaRenCerObj() {
        return ((Integer)getAaRenCer());
    }

    @Override
    public void setAaRenCerObj(Integer aaRenCerObj) {
        setAaRenCer(((int)aaRenCerObj));
    }

    @Override
    public AfDecimal getAbb() {
        throw new FieldNotMappedException("abb");
    }

    @Override
    public void setAbb(AfDecimal abb) {
        throw new FieldNotMappedException("abb");
    }

    @Override
    public AfDecimal getAbbObj() {
        return getAbb();
    }

    @Override
    public void setAbbObj(AfDecimal abbObj) {
        setAbb(new AfDecimal(abbObj, 15, 3));
    }

    @Override
    public AfDecimal getAcqExp() {
        throw new FieldNotMappedException("acqExp");
    }

    @Override
    public void setAcqExp(AfDecimal acqExp) {
        throw new FieldNotMappedException("acqExp");
    }

    @Override
    public AfDecimal getAcqExpObj() {
        return getAcqExp();
    }

    @Override
    public void setAcqExpObj(AfDecimal acqExpObj) {
        setAcqExp(new AfDecimal(acqExpObj, 15, 3));
    }

    @Override
    public AfDecimal getAlqMargCSubrsh() {
        throw new FieldNotMappedException("alqMargCSubrsh");
    }

    @Override
    public void setAlqMargCSubrsh(AfDecimal alqMargCSubrsh) {
        throw new FieldNotMappedException("alqMargCSubrsh");
    }

    @Override
    public AfDecimal getAlqMargCSubrshObj() {
        return getAlqMargCSubrsh();
    }

    @Override
    public void setAlqMargCSubrshObj(AfDecimal alqMargCSubrshObj) {
        setAlqMargCSubrsh(new AfDecimal(alqMargCSubrshObj, 6, 3));
    }

    @Override
    public AfDecimal getAlqMargRis() {
        throw new FieldNotMappedException("alqMargRis");
    }

    @Override
    public void setAlqMargRis(AfDecimal alqMargRis) {
        throw new FieldNotMappedException("alqMargRis");
    }

    @Override
    public AfDecimal getAlqMargRisObj() {
        return getAlqMargRis();
    }

    @Override
    public void setAlqMargRisObj(AfDecimal alqMargRisObj) {
        setAlqMargRis(new AfDecimal(alqMargRisObj, 6, 3));
    }

    @Override
    public AfDecimal getAlqRetrT() {
        throw new FieldNotMappedException("alqRetrT");
    }

    @Override
    public void setAlqRetrT(AfDecimal alqRetrT) {
        throw new FieldNotMappedException("alqRetrT");
    }

    @Override
    public AfDecimal getAlqRetrTObj() {
        return getAlqRetrT();
    }

    @Override
    public void setAlqRetrTObj(AfDecimal alqRetrTObj) {
        setAlqRetrT(new AfDecimal(alqRetrTObj, 6, 3));
    }

    @Override
    public AfDecimal getAntidurCalc365() {
        throw new FieldNotMappedException("antidurCalc365");
    }

    @Override
    public void setAntidurCalc365(AfDecimal antidurCalc365) {
        throw new FieldNotMappedException("antidurCalc365");
    }

    @Override
    public AfDecimal getAntidurCalc365Obj() {
        return getAntidurCalc365();
    }

    @Override
    public void setAntidurCalc365Obj(AfDecimal antidurCalc365Obj) {
        setAntidurCalc365(new AfDecimal(antidurCalc365Obj, 11, 7));
    }

    @Override
    public AfDecimal getAntidurDtCalc() {
        throw new FieldNotMappedException("antidurDtCalc");
    }

    @Override
    public void setAntidurDtCalc(AfDecimal antidurDtCalc) {
        throw new FieldNotMappedException("antidurDtCalc");
    }

    @Override
    public AfDecimal getAntidurDtCalcObj() {
        return getAntidurDtCalc();
    }

    @Override
    public void setAntidurDtCalcObj(AfDecimal antidurDtCalcObj) {
        setAntidurDtCalc(new AfDecimal(antidurDtCalcObj, 11, 7));
    }

    @Override
    public int getAntidurRicorPrec() {
        throw new FieldNotMappedException("antidurRicorPrec");
    }

    @Override
    public void setAntidurRicorPrec(int antidurRicorPrec) {
        throw new FieldNotMappedException("antidurRicorPrec");
    }

    @Override
    public Integer getAntidurRicorPrecObj() {
        return ((Integer)getAntidurRicorPrec());
    }

    @Override
    public void setAntidurRicorPrecObj(Integer antidurRicorPrecObj) {
        setAntidurRicorPrec(((int)antidurRicorPrecObj));
    }

    @Override
    public int getB03IdAdes() {
        return bilaTrchEstr.getB03IdAdes();
    }

    @Override
    public void setB03IdAdes(int b03IdAdes) {
        this.bilaTrchEstr.setB03IdAdes(b03IdAdes);
    }

    @Override
    public int getB03IdBilaTrchEstr() {
        throw new FieldNotMappedException("b03IdBilaTrchEstr");
    }

    @Override
    public void setB03IdBilaTrchEstr(int b03IdBilaTrchEstr) {
        throw new FieldNotMappedException("b03IdBilaTrchEstr");
    }

    @Override
    public int getB03IdPoli() {
        return bilaTrchEstr.getB03IdPoli();
    }

    @Override
    public void setB03IdPoli(int b03IdPoli) {
        this.bilaTrchEstr.setB03IdPoli(b03IdPoli);
    }

    @Override
    public int getB03IdRichEstrazMas() {
        return bilaTrchEstr.getB03IdRichEstrazMas();
    }

    @Override
    public void setB03IdRichEstrazMas(int b03IdRichEstrazMas) {
        this.bilaTrchEstr.setB03IdRichEstrazMas(b03IdRichEstrazMas);
    }

    @Override
    public int getB03IdTrchDiGar() {
        throw new FieldNotMappedException("b03IdTrchDiGar");
    }

    @Override
    public void setB03IdTrchDiGar(int b03IdTrchDiGar) {
        throw new FieldNotMappedException("b03IdTrchDiGar");
    }

    @Override
    public AfDecimal getCarAcqNonScon() {
        throw new FieldNotMappedException("carAcqNonScon");
    }

    @Override
    public void setCarAcqNonScon(AfDecimal carAcqNonScon) {
        throw new FieldNotMappedException("carAcqNonScon");
    }

    @Override
    public AfDecimal getCarAcqNonSconObj() {
        return getCarAcqNonScon();
    }

    @Override
    public void setCarAcqNonSconObj(AfDecimal carAcqNonSconObj) {
        setCarAcqNonScon(new AfDecimal(carAcqNonSconObj, 15, 3));
    }

    @Override
    public AfDecimal getCarAcqPrecontato() {
        throw new FieldNotMappedException("carAcqPrecontato");
    }

    @Override
    public void setCarAcqPrecontato(AfDecimal carAcqPrecontato) {
        throw new FieldNotMappedException("carAcqPrecontato");
    }

    @Override
    public AfDecimal getCarAcqPrecontatoObj() {
        return getCarAcqPrecontato();
    }

    @Override
    public void setCarAcqPrecontatoObj(AfDecimal carAcqPrecontatoObj) {
        setCarAcqPrecontato(new AfDecimal(carAcqPrecontatoObj, 15, 3));
    }

    @Override
    public AfDecimal getCarGest() {
        throw new FieldNotMappedException("carGest");
    }

    @Override
    public void setCarGest(AfDecimal carGest) {
        throw new FieldNotMappedException("carGest");
    }

    @Override
    public AfDecimal getCarGestNonScon() {
        throw new FieldNotMappedException("carGestNonScon");
    }

    @Override
    public void setCarGestNonScon(AfDecimal carGestNonScon) {
        throw new FieldNotMappedException("carGestNonScon");
    }

    @Override
    public AfDecimal getCarGestNonSconObj() {
        return getCarGestNonScon();
    }

    @Override
    public void setCarGestNonSconObj(AfDecimal carGestNonSconObj) {
        setCarGestNonScon(new AfDecimal(carGestNonSconObj, 15, 3));
    }

    @Override
    public AfDecimal getCarGestObj() {
        return getCarGest();
    }

    @Override
    public void setCarGestObj(AfDecimal carGestObj) {
        setCarGest(new AfDecimal(carGestObj, 15, 3));
    }

    @Override
    public AfDecimal getCarInc() {
        throw new FieldNotMappedException("carInc");
    }

    @Override
    public void setCarInc(AfDecimal carInc) {
        throw new FieldNotMappedException("carInc");
    }

    @Override
    public AfDecimal getCarIncNonScon() {
        throw new FieldNotMappedException("carIncNonScon");
    }

    @Override
    public void setCarIncNonScon(AfDecimal carIncNonScon) {
        throw new FieldNotMappedException("carIncNonScon");
    }

    @Override
    public AfDecimal getCarIncNonSconObj() {
        return getCarIncNonScon();
    }

    @Override
    public void setCarIncNonSconObj(AfDecimal carIncNonSconObj) {
        setCarIncNonScon(new AfDecimal(carIncNonSconObj, 15, 3));
    }

    @Override
    public AfDecimal getCarIncObj() {
        return getCarInc();
    }

    @Override
    public void setCarIncObj(AfDecimal carIncObj) {
        setCarInc(new AfDecimal(carIncObj, 15, 3));
    }

    @Override
    public int getCarz() {
        throw new FieldNotMappedException("carz");
    }

    @Override
    public void setCarz(int carz) {
        throw new FieldNotMappedException("carz");
    }

    @Override
    public Integer getCarzObj() {
        return ((Integer)getCarz());
    }

    @Override
    public void setCarzObj(Integer carzObj) {
        setCarz(((int)carzObj));
    }

    @Override
    public String getCausSconVchar() {
        throw new FieldNotMappedException("causSconVchar");
    }

    @Override
    public void setCausSconVchar(String causSconVchar) {
        throw new FieldNotMappedException("causSconVchar");
    }

    @Override
    public String getCausSconVcharObj() {
        return getCausSconVchar();
    }

    @Override
    public void setCausSconVcharObj(String causSconVcharObj) {
        setCausSconVchar(causSconVcharObj);
    }

    @Override
    public int getCodAge() {
        throw new FieldNotMappedException("codAge");
    }

    @Override
    public void setCodAge(int codAge) {
        throw new FieldNotMappedException("codAge");
    }

    @Override
    public Integer getCodAgeObj() {
        return ((Integer)getCodAge());
    }

    @Override
    public void setCodAgeObj(Integer codAgeObj) {
        setCodAge(((int)codAgeObj));
    }

    @Override
    public int getCodCan() {
        throw new FieldNotMappedException("codCan");
    }

    @Override
    public void setCodCan(int codCan) {
        throw new FieldNotMappedException("codCan");
    }

    @Override
    public Integer getCodCanObj() {
        return ((Integer)getCodCan());
    }

    @Override
    public void setCodCanObj(Integer codCanObj) {
        setCodCan(((int)codCanObj));
    }

    @Override
    public int getCodCompAnia() {
        throw new FieldNotMappedException("codCompAnia");
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        throw new FieldNotMappedException("codCompAnia");
    }

    @Override
    public String getCodConv() {
        throw new FieldNotMappedException("codConv");
    }

    @Override
    public void setCodConv(String codConv) {
        throw new FieldNotMappedException("codConv");
    }

    @Override
    public String getCodConvObj() {
        return getCodConv();
    }

    @Override
    public void setCodConvObj(String codConvObj) {
        setCodConv(codConvObj);
    }

    @Override
    public String getCodDiv() {
        throw new FieldNotMappedException("codDiv");
    }

    @Override
    public void setCodDiv(String codDiv) {
        throw new FieldNotMappedException("codDiv");
    }

    @Override
    public String getCodDivObj() {
        return getCodDiv();
    }

    @Override
    public void setCodDivObj(String codDivObj) {
        setCodDiv(codDivObj);
    }

    @Override
    public String getCodFiscAssto1() {
        throw new FieldNotMappedException("codFiscAssto1");
    }

    @Override
    public void setCodFiscAssto1(String codFiscAssto1) {
        throw new FieldNotMappedException("codFiscAssto1");
    }

    @Override
    public String getCodFiscAssto1Obj() {
        return getCodFiscAssto1();
    }

    @Override
    public void setCodFiscAssto1Obj(String codFiscAssto1Obj) {
        setCodFiscAssto1(codFiscAssto1Obj);
    }

    @Override
    public String getCodFiscAssto2() {
        throw new FieldNotMappedException("codFiscAssto2");
    }

    @Override
    public void setCodFiscAssto2(String codFiscAssto2) {
        throw new FieldNotMappedException("codFiscAssto2");
    }

    @Override
    public String getCodFiscAssto2Obj() {
        return getCodFiscAssto2();
    }

    @Override
    public void setCodFiscAssto2Obj(String codFiscAssto2Obj) {
        setCodFiscAssto2(codFiscAssto2Obj);
    }

    @Override
    public String getCodFiscAssto3() {
        throw new FieldNotMappedException("codFiscAssto3");
    }

    @Override
    public void setCodFiscAssto3(String codFiscAssto3) {
        throw new FieldNotMappedException("codFiscAssto3");
    }

    @Override
    public String getCodFiscAssto3Obj() {
        return getCodFiscAssto3();
    }

    @Override
    public void setCodFiscAssto3Obj(String codFiscAssto3Obj) {
        setCodFiscAssto3(codFiscAssto3Obj);
    }

    @Override
    public String getCodFiscCntr() {
        throw new FieldNotMappedException("codFiscCntr");
    }

    @Override
    public void setCodFiscCntr(String codFiscCntr) {
        throw new FieldNotMappedException("codFiscCntr");
    }

    @Override
    public String getCodFiscCntrObj() {
        return getCodFiscCntr();
    }

    @Override
    public void setCodFiscCntrObj(String codFiscCntrObj) {
        setCodFiscCntr(codFiscCntrObj);
    }

    @Override
    public String getCodFnd() {
        throw new FieldNotMappedException("codFnd");
    }

    @Override
    public void setCodFnd(String codFnd) {
        throw new FieldNotMappedException("codFnd");
    }

    @Override
    public String getCodFndObj() {
        return getCodFnd();
    }

    @Override
    public void setCodFndObj(String codFndObj) {
        setCodFnd(codFndObj);
    }

    @Override
    public int getCodPrdt() {
        throw new FieldNotMappedException("codPrdt");
    }

    @Override
    public void setCodPrdt(int codPrdt) {
        throw new FieldNotMappedException("codPrdt");
    }

    @Override
    public Integer getCodPrdtObj() {
        return ((Integer)getCodPrdt());
    }

    @Override
    public void setCodPrdtObj(Integer codPrdtObj) {
        setCodPrdt(((int)codPrdtObj));
    }

    @Override
    public String getCodProd() {
        throw new FieldNotMappedException("codProd");
    }

    @Override
    public void setCodProd(String codProd) {
        throw new FieldNotMappedException("codProd");
    }

    @Override
    public String getCodProdObj() {
        return getCodProd();
    }

    @Override
    public void setCodProdObj(String codProdObj) {
        setCodProd(codProdObj);
    }

    @Override
    public String getCodRamo() {
        throw new FieldNotMappedException("codRamo");
    }

    @Override
    public void setCodRamo(String codRamo) {
        throw new FieldNotMappedException("codRamo");
    }

    @Override
    public int getCodSubage() {
        throw new FieldNotMappedException("codSubage");
    }

    @Override
    public void setCodSubage(int codSubage) {
        throw new FieldNotMappedException("codSubage");
    }

    @Override
    public Integer getCodSubageObj() {
        return ((Integer)getCodSubage());
    }

    @Override
    public void setCodSubageObj(Integer codSubageObj) {
        setCodSubage(((int)codSubageObj));
    }

    @Override
    public String getCodTari() {
        throw new FieldNotMappedException("codTari");
    }

    @Override
    public void setCodTari(String codTari) {
        throw new FieldNotMappedException("codTari");
    }

    @Override
    public String getCodTariOrgn() {
        throw new FieldNotMappedException("codTariOrgn");
    }

    @Override
    public void setCodTariOrgn(String codTariOrgn) {
        throw new FieldNotMappedException("codTariOrgn");
    }

    @Override
    public String getCodTariOrgnObj() {
        return getCodTariOrgn();
    }

    @Override
    public void setCodTariOrgnObj(String codTariOrgnObj) {
        setCodTariOrgn(codTariOrgnObj);
    }

    @Override
    public AfDecimal getCoeffOpzCpt() {
        throw new FieldNotMappedException("coeffOpzCpt");
    }

    @Override
    public void setCoeffOpzCpt(AfDecimal coeffOpzCpt) {
        throw new FieldNotMappedException("coeffOpzCpt");
    }

    @Override
    public AfDecimal getCoeffOpzCptObj() {
        return getCoeffOpzCpt();
    }

    @Override
    public void setCoeffOpzCptObj(AfDecimal coeffOpzCptObj) {
        setCoeffOpzCpt(new AfDecimal(coeffOpzCptObj, 6, 3));
    }

    @Override
    public AfDecimal getCoeffOpzRen() {
        throw new FieldNotMappedException("coeffOpzRen");
    }

    @Override
    public void setCoeffOpzRen(AfDecimal coeffOpzRen) {
        throw new FieldNotMappedException("coeffOpzRen");
    }

    @Override
    public AfDecimal getCoeffOpzRenObj() {
        return getCoeffOpzRen();
    }

    @Override
    public void setCoeffOpzRenObj(AfDecimal coeffOpzRenObj) {
        setCoeffOpzRen(new AfDecimal(coeffOpzRenObj, 6, 3));
    }

    @Override
    public AfDecimal getCoeffRis1T() {
        throw new FieldNotMappedException("coeffRis1T");
    }

    @Override
    public void setCoeffRis1T(AfDecimal coeffRis1T) {
        throw new FieldNotMappedException("coeffRis1T");
    }

    @Override
    public AfDecimal getCoeffRis1TObj() {
        return getCoeffRis1T();
    }

    @Override
    public void setCoeffRis1TObj(AfDecimal coeffRis1TObj) {
        setCoeffRis1T(new AfDecimal(coeffRis1TObj, 14, 9));
    }

    @Override
    public AfDecimal getCoeffRis2T() {
        throw new FieldNotMappedException("coeffRis2T");
    }

    @Override
    public void setCoeffRis2T(AfDecimal coeffRis2T) {
        throw new FieldNotMappedException("coeffRis2T");
    }

    @Override
    public AfDecimal getCoeffRis2TObj() {
        return getCoeffRis2T();
    }

    @Override
    public void setCoeffRis2TObj(AfDecimal coeffRis2TObj) {
        setCoeffRis2T(new AfDecimal(coeffRis2TObj, 14, 9));
    }

    @Override
    public AfDecimal getCommisInter() {
        throw new FieldNotMappedException("commisInter");
    }

    @Override
    public void setCommisInter(AfDecimal commisInter) {
        throw new FieldNotMappedException("commisInter");
    }

    @Override
    public AfDecimal getCommisInterObj() {
        return getCommisInter();
    }

    @Override
    public void setCommisInterObj(AfDecimal commisInterObj) {
        setCommisInter(new AfDecimal(commisInterObj, 15, 3));
    }

    @Override
    public AfDecimal getCptAsstoIniMor() {
        throw new FieldNotMappedException("cptAsstoIniMor");
    }

    @Override
    public void setCptAsstoIniMor(AfDecimal cptAsstoIniMor) {
        throw new FieldNotMappedException("cptAsstoIniMor");
    }

    @Override
    public AfDecimal getCptAsstoIniMorObj() {
        return getCptAsstoIniMor();
    }

    @Override
    public void setCptAsstoIniMorObj(AfDecimal cptAsstoIniMorObj) {
        setCptAsstoIniMor(new AfDecimal(cptAsstoIniMorObj, 15, 3));
    }

    @Override
    public AfDecimal getCptDtRidz() {
        throw new FieldNotMappedException("cptDtRidz");
    }

    @Override
    public void setCptDtRidz(AfDecimal cptDtRidz) {
        throw new FieldNotMappedException("cptDtRidz");
    }

    @Override
    public AfDecimal getCptDtRidzObj() {
        return getCptDtRidz();
    }

    @Override
    public void setCptDtRidzObj(AfDecimal cptDtRidzObj) {
        setCptDtRidz(new AfDecimal(cptDtRidzObj, 15, 3));
    }

    @Override
    public AfDecimal getCptDtStab() {
        throw new FieldNotMappedException("cptDtStab");
    }

    @Override
    public void setCptDtStab(AfDecimal cptDtStab) {
        throw new FieldNotMappedException("cptDtStab");
    }

    @Override
    public AfDecimal getCptDtStabObj() {
        return getCptDtStab();
    }

    @Override
    public void setCptDtStabObj(AfDecimal cptDtStabObj) {
        setCptDtStab(new AfDecimal(cptDtStabObj, 15, 3));
    }

    @Override
    public AfDecimal getCptRiasto() {
        throw new FieldNotMappedException("cptRiasto");
    }

    @Override
    public void setCptRiasto(AfDecimal cptRiasto) {
        throw new FieldNotMappedException("cptRiasto");
    }

    @Override
    public AfDecimal getCptRiastoEcc() {
        throw new FieldNotMappedException("cptRiastoEcc");
    }

    @Override
    public void setCptRiastoEcc(AfDecimal cptRiastoEcc) {
        throw new FieldNotMappedException("cptRiastoEcc");
    }

    @Override
    public AfDecimal getCptRiastoEccObj() {
        return getCptRiastoEcc();
    }

    @Override
    public void setCptRiastoEccObj(AfDecimal cptRiastoEccObj) {
        setCptRiastoEcc(new AfDecimal(cptRiastoEccObj, 15, 3));
    }

    @Override
    public AfDecimal getCptRiastoObj() {
        return getCptRiasto();
    }

    @Override
    public void setCptRiastoObj(AfDecimal cptRiastoObj) {
        setCptRiasto(new AfDecimal(cptRiastoObj, 15, 3));
    }

    @Override
    public AfDecimal getCptRshMor() {
        throw new FieldNotMappedException("cptRshMor");
    }

    @Override
    public void setCptRshMor(AfDecimal cptRshMor) {
        throw new FieldNotMappedException("cptRshMor");
    }

    @Override
    public AfDecimal getCptRshMorObj() {
        return getCptRshMor();
    }

    @Override
    public void setCptRshMorObj(AfDecimal cptRshMorObj) {
        setCptRshMor(new AfDecimal(cptRshMorObj, 15, 3));
    }

    @Override
    public AfDecimal getCumRiscpar() {
        throw new FieldNotMappedException("cumRiscpar");
    }

    @Override
    public void setCumRiscpar(AfDecimal cumRiscpar) {
        throw new FieldNotMappedException("cumRiscpar");
    }

    @Override
    public AfDecimal getCumRiscparObj() {
        return getCumRiscpar();
    }

    @Override
    public void setCumRiscparObj(AfDecimal cumRiscparObj) {
        setCumRiscpar(new AfDecimal(cumRiscparObj, 15, 3));
    }

    @Override
    public AfDecimal getDir() {
        throw new FieldNotMappedException("dir");
    }

    @Override
    public void setDir(AfDecimal dir) {
        throw new FieldNotMappedException("dir");
    }

    @Override
    public AfDecimal getDirEmis() {
        throw new FieldNotMappedException("dirEmis");
    }

    @Override
    public void setDirEmis(AfDecimal dirEmis) {
        throw new FieldNotMappedException("dirEmis");
    }

    @Override
    public AfDecimal getDirEmisObj() {
        return getDirEmis();
    }

    @Override
    public void setDirEmisObj(AfDecimal dirEmisObj) {
        setDirEmis(new AfDecimal(dirEmisObj, 15, 3));
    }

    @Override
    public AfDecimal getDirObj() {
        return getDir();
    }

    @Override
    public void setDirObj(AfDecimal dirObj) {
        setDir(new AfDecimal(dirObj, 15, 3));
    }

    @Override
    public char getDsOperSql() {
        throw new FieldNotMappedException("dsOperSql");
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        throw new FieldNotMappedException("dsOperSql");
    }

    @Override
    public char getDsStatoElab() {
        throw new FieldNotMappedException("dsStatoElab");
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        throw new FieldNotMappedException("dsStatoElab");
    }

    @Override
    public long getDsTsCptz() {
        throw new FieldNotMappedException("dsTsCptz");
    }

    @Override
    public void setDsTsCptz(long dsTsCptz) {
        throw new FieldNotMappedException("dsTsCptz");
    }

    @Override
    public String getDsUtente() {
        throw new FieldNotMappedException("dsUtente");
    }

    @Override
    public void setDsUtente(String dsUtente) {
        throw new FieldNotMappedException("dsUtente");
    }

    @Override
    public int getDsVer() {
        throw new FieldNotMappedException("dsVer");
    }

    @Override
    public void setDsVer(int dsVer) {
        throw new FieldNotMappedException("dsVer");
    }

    @Override
    public String getDtDecorAdesDb() {
        throw new FieldNotMappedException("dtDecorAdesDb");
    }

    @Override
    public void setDtDecorAdesDb(String dtDecorAdesDb) {
        throw new FieldNotMappedException("dtDecorAdesDb");
    }

    @Override
    public String getDtDecorAdesDbObj() {
        return getDtDecorAdesDb();
    }

    @Override
    public void setDtDecorAdesDbObj(String dtDecorAdesDbObj) {
        setDtDecorAdesDb(dtDecorAdesDbObj);
    }

    @Override
    public String getDtDecorPoliDb() {
        throw new FieldNotMappedException("dtDecorPoliDb");
    }

    @Override
    public void setDtDecorPoliDb(String dtDecorPoliDb) {
        throw new FieldNotMappedException("dtDecorPoliDb");
    }

    @Override
    public String getDtDecorTrchDb() {
        throw new FieldNotMappedException("dtDecorTrchDb");
    }

    @Override
    public void setDtDecorTrchDb(String dtDecorTrchDb) {
        throw new FieldNotMappedException("dtDecorTrchDb");
    }

    @Override
    public String getDtEffCambStatDb() {
        throw new FieldNotMappedException("dtEffCambStatDb");
    }

    @Override
    public void setDtEffCambStatDb(String dtEffCambStatDb) {
        throw new FieldNotMappedException("dtEffCambStatDb");
    }

    @Override
    public String getDtEffCambStatDbObj() {
        return getDtEffCambStatDb();
    }

    @Override
    public void setDtEffCambStatDbObj(String dtEffCambStatDbObj) {
        setDtEffCambStatDb(dtEffCambStatDbObj);
    }

    @Override
    public String getDtEffRidzDb() {
        throw new FieldNotMappedException("dtEffRidzDb");
    }

    @Override
    public void setDtEffRidzDb(String dtEffRidzDb) {
        throw new FieldNotMappedException("dtEffRidzDb");
    }

    @Override
    public String getDtEffRidzDbObj() {
        return getDtEffRidzDb();
    }

    @Override
    public void setDtEffRidzDbObj(String dtEffRidzDbObj) {
        setDtEffRidzDb(dtEffRidzDbObj);
    }

    @Override
    public String getDtEffStabDb() {
        throw new FieldNotMappedException("dtEffStabDb");
    }

    @Override
    public void setDtEffStabDb(String dtEffStabDb) {
        throw new FieldNotMappedException("dtEffStabDb");
    }

    @Override
    public String getDtEffStabDbObj() {
        return getDtEffStabDb();
    }

    @Override
    public void setDtEffStabDbObj(String dtEffStabDbObj) {
        setDtEffStabDb(dtEffStabDbObj);
    }

    @Override
    public String getDtEmisCambStatDb() {
        throw new FieldNotMappedException("dtEmisCambStatDb");
    }

    @Override
    public void setDtEmisCambStatDb(String dtEmisCambStatDb) {
        throw new FieldNotMappedException("dtEmisCambStatDb");
    }

    @Override
    public String getDtEmisCambStatDbObj() {
        return getDtEmisCambStatDb();
    }

    @Override
    public void setDtEmisCambStatDbObj(String dtEmisCambStatDbObj) {
        setDtEmisCambStatDb(dtEmisCambStatDbObj);
    }

    @Override
    public String getDtEmisPoliDb() {
        throw new FieldNotMappedException("dtEmisPoliDb");
    }

    @Override
    public void setDtEmisPoliDb(String dtEmisPoliDb) {
        throw new FieldNotMappedException("dtEmisPoliDb");
    }

    @Override
    public String getDtEmisRidzDb() {
        throw new FieldNotMappedException("dtEmisRidzDb");
    }

    @Override
    public void setDtEmisRidzDb(String dtEmisRidzDb) {
        throw new FieldNotMappedException("dtEmisRidzDb");
    }

    @Override
    public String getDtEmisRidzDbObj() {
        return getDtEmisRidzDb();
    }

    @Override
    public void setDtEmisRidzDbObj(String dtEmisRidzDbObj) {
        setDtEmisRidzDb(dtEmisRidzDbObj);
    }

    @Override
    public String getDtEmisTrchDb() {
        throw new FieldNotMappedException("dtEmisTrchDb");
    }

    @Override
    public void setDtEmisTrchDb(String dtEmisTrchDb) {
        throw new FieldNotMappedException("dtEmisTrchDb");
    }

    @Override
    public String getDtEmisTrchDbObj() {
        return getDtEmisTrchDb();
    }

    @Override
    public void setDtEmisTrchDbObj(String dtEmisTrchDbObj) {
        setDtEmisTrchDb(dtEmisTrchDbObj);
    }

    @Override
    public String getDtIncUltPreDb() {
        throw new FieldNotMappedException("dtIncUltPreDb");
    }

    @Override
    public void setDtIncUltPreDb(String dtIncUltPreDb) {
        throw new FieldNotMappedException("dtIncUltPreDb");
    }

    @Override
    public String getDtIncUltPreDbObj() {
        return getDtIncUltPreDb();
    }

    @Override
    public void setDtIncUltPreDbObj(String dtIncUltPreDbObj) {
        setDtIncUltPreDb(dtIncUltPreDbObj);
    }

    @Override
    public String getDtIniValTarDb() {
        throw new FieldNotMappedException("dtIniValTarDb");
    }

    @Override
    public void setDtIniValTarDb(String dtIniValTarDb) {
        throw new FieldNotMappedException("dtIniValTarDb");
    }

    @Override
    public String getDtIniValTarDbObj() {
        return getDtIniValTarDb();
    }

    @Override
    public void setDtIniValTarDbObj(String dtIniValTarDbObj) {
        setDtIniValTarDb(dtIniValTarDbObj);
    }

    @Override
    public String getDtIniVldtProdDb() {
        throw new FieldNotMappedException("dtIniVldtProdDb");
    }

    @Override
    public void setDtIniVldtProdDb(String dtIniVldtProdDb) {
        throw new FieldNotMappedException("dtIniVldtProdDb");
    }

    @Override
    public String getDtNasc1oAsstoDb() {
        throw new FieldNotMappedException("dtNasc1oAsstoDb");
    }

    @Override
    public void setDtNasc1oAsstoDb(String dtNasc1oAsstoDb) {
        throw new FieldNotMappedException("dtNasc1oAsstoDb");
    }

    @Override
    public String getDtNasc1oAsstoDbObj() {
        return getDtNasc1oAsstoDb();
    }

    @Override
    public void setDtNasc1oAsstoDbObj(String dtNasc1oAsstoDbObj) {
        setDtNasc1oAsstoDb(dtNasc1oAsstoDbObj);
    }

    @Override
    public String getDtProduzioneDb() {
        throw new FieldNotMappedException("dtProduzioneDb");
    }

    @Override
    public void setDtProduzioneDb(String dtProduzioneDb) {
        throw new FieldNotMappedException("dtProduzioneDb");
    }

    @Override
    public String getDtQtzEmisDb() {
        throw new FieldNotMappedException("dtQtzEmisDb");
    }

    @Override
    public void setDtQtzEmisDb(String dtQtzEmisDb) {
        throw new FieldNotMappedException("dtQtzEmisDb");
    }

    @Override
    public String getDtQtzEmisDbObj() {
        return getDtQtzEmisDb();
    }

    @Override
    public void setDtQtzEmisDbObj(String dtQtzEmisDbObj) {
        setDtQtzEmisDb(dtQtzEmisDbObj);
    }

    @Override
    public String getDtRisDb() {
        throw new FieldNotMappedException("dtRisDb");
    }

    @Override
    public void setDtRisDb(String dtRisDb) {
        throw new FieldNotMappedException("dtRisDb");
    }

    @Override
    public String getDtScadIntmdDb() {
        throw new FieldNotMappedException("dtScadIntmdDb");
    }

    @Override
    public void setDtScadIntmdDb(String dtScadIntmdDb) {
        throw new FieldNotMappedException("dtScadIntmdDb");
    }

    @Override
    public String getDtScadIntmdDbObj() {
        return getDtScadIntmdDb();
    }

    @Override
    public void setDtScadIntmdDbObj(String dtScadIntmdDbObj) {
        setDtScadIntmdDb(dtScadIntmdDbObj);
    }

    @Override
    public String getDtScadPagPreDb() {
        throw new FieldNotMappedException("dtScadPagPreDb");
    }

    @Override
    public void setDtScadPagPreDb(String dtScadPagPreDb) {
        throw new FieldNotMappedException("dtScadPagPreDb");
    }

    @Override
    public String getDtScadPagPreDbObj() {
        return getDtScadPagPreDb();
    }

    @Override
    public void setDtScadPagPreDbObj(String dtScadPagPreDbObj) {
        setDtScadPagPreDb(dtScadPagPreDbObj);
    }

    @Override
    public String getDtScadTrchDb() {
        throw new FieldNotMappedException("dtScadTrchDb");
    }

    @Override
    public void setDtScadTrchDb(String dtScadTrchDb) {
        throw new FieldNotMappedException("dtScadTrchDb");
    }

    @Override
    public String getDtScadTrchDbObj() {
        return getDtScadTrchDb();
    }

    @Override
    public void setDtScadTrchDbObj(String dtScadTrchDbObj) {
        setDtScadTrchDb(dtScadTrchDbObj);
    }

    @Override
    public String getDtUltPrePagDb() {
        throw new FieldNotMappedException("dtUltPrePagDb");
    }

    @Override
    public void setDtUltPrePagDb(String dtUltPrePagDb) {
        throw new FieldNotMappedException("dtUltPrePagDb");
    }

    @Override
    public String getDtUltPrePagDbObj() {
        return getDtUltPrePagDb();
    }

    @Override
    public void setDtUltPrePagDbObj(String dtUltPrePagDbObj) {
        setDtUltPrePagDb(dtUltPrePagDbObj);
    }

    @Override
    public String getDtUltRivalDb() {
        throw new FieldNotMappedException("dtUltRivalDb");
    }

    @Override
    public void setDtUltRivalDb(String dtUltRivalDb) {
        throw new FieldNotMappedException("dtUltRivalDb");
    }

    @Override
    public String getDtUltRivalDbObj() {
        return getDtUltRivalDb();
    }

    @Override
    public void setDtUltRivalDbObj(String dtUltRivalDbObj) {
        setDtUltRivalDb(dtUltRivalDbObj);
    }

    @Override
    public int getDur1oPerAa() {
        throw new FieldNotMappedException("dur1oPerAa");
    }

    @Override
    public void setDur1oPerAa(int dur1oPerAa) {
        throw new FieldNotMappedException("dur1oPerAa");
    }

    @Override
    public Integer getDur1oPerAaObj() {
        return ((Integer)getDur1oPerAa());
    }

    @Override
    public void setDur1oPerAaObj(Integer dur1oPerAaObj) {
        setDur1oPerAa(((int)dur1oPerAaObj));
    }

    @Override
    public int getDur1oPerGg() {
        throw new FieldNotMappedException("dur1oPerGg");
    }

    @Override
    public void setDur1oPerGg(int dur1oPerGg) {
        throw new FieldNotMappedException("dur1oPerGg");
    }

    @Override
    public Integer getDur1oPerGgObj() {
        return ((Integer)getDur1oPerGg());
    }

    @Override
    public void setDur1oPerGgObj(Integer dur1oPerGgObj) {
        setDur1oPerGg(((int)dur1oPerGgObj));
    }

    @Override
    public int getDur1oPerMm() {
        throw new FieldNotMappedException("dur1oPerMm");
    }

    @Override
    public void setDur1oPerMm(int dur1oPerMm) {
        throw new FieldNotMappedException("dur1oPerMm");
    }

    @Override
    public Integer getDur1oPerMmObj() {
        return ((Integer)getDur1oPerMm());
    }

    @Override
    public void setDur1oPerMmObj(Integer dur1oPerMmObj) {
        setDur1oPerMm(((int)dur1oPerMmObj));
    }

    @Override
    public int getDurAa() {
        throw new FieldNotMappedException("durAa");
    }

    @Override
    public void setDurAa(int durAa) {
        throw new FieldNotMappedException("durAa");
    }

    @Override
    public Integer getDurAaObj() {
        return ((Integer)getDurAa());
    }

    @Override
    public void setDurAaObj(Integer durAaObj) {
        setDurAa(((int)durAaObj));
    }

    @Override
    public int getDurGarAa() {
        throw new FieldNotMappedException("durGarAa");
    }

    @Override
    public void setDurGarAa(int durGarAa) {
        throw new FieldNotMappedException("durGarAa");
    }

    @Override
    public Integer getDurGarAaObj() {
        return ((Integer)getDurGarAa());
    }

    @Override
    public void setDurGarAaObj(Integer durGarAaObj) {
        setDurGarAa(((int)durGarAaObj));
    }

    @Override
    public int getDurGarGg() {
        throw new FieldNotMappedException("durGarGg");
    }

    @Override
    public void setDurGarGg(int durGarGg) {
        throw new FieldNotMappedException("durGarGg");
    }

    @Override
    public Integer getDurGarGgObj() {
        return ((Integer)getDurGarGg());
    }

    @Override
    public void setDurGarGgObj(Integer durGarGgObj) {
        setDurGarGg(((int)durGarGgObj));
    }

    @Override
    public int getDurGarMm() {
        throw new FieldNotMappedException("durGarMm");
    }

    @Override
    public void setDurGarMm(int durGarMm) {
        throw new FieldNotMappedException("durGarMm");
    }

    @Override
    public Integer getDurGarMmObj() {
        return ((Integer)getDurGarMm());
    }

    @Override
    public void setDurGarMmObj(Integer durGarMmObj) {
        setDurGarMm(((int)durGarMmObj));
    }

    @Override
    public int getDurGg() {
        throw new FieldNotMappedException("durGg");
    }

    @Override
    public void setDurGg(int durGg) {
        throw new FieldNotMappedException("durGg");
    }

    @Override
    public Integer getDurGgObj() {
        return ((Integer)getDurGg());
    }

    @Override
    public void setDurGgObj(Integer durGgObj) {
        setDurGg(((int)durGgObj));
    }

    @Override
    public int getDurMm() {
        throw new FieldNotMappedException("durMm");
    }

    @Override
    public void setDurMm(int durMm) {
        throw new FieldNotMappedException("durMm");
    }

    @Override
    public Integer getDurMmObj() {
        return ((Integer)getDurMm());
    }

    @Override
    public void setDurMmObj(Integer durMmObj) {
        setDurMm(((int)durMmObj));
    }

    @Override
    public int getDurPagPre() {
        throw new FieldNotMappedException("durPagPre");
    }

    @Override
    public void setDurPagPre(int durPagPre) {
        throw new FieldNotMappedException("durPagPre");
    }

    @Override
    public Integer getDurPagPreObj() {
        return ((Integer)getDurPagPre());
    }

    @Override
    public void setDurPagPreObj(Integer durPagPreObj) {
        setDurPagPre(((int)durPagPreObj));
    }

    @Override
    public int getDurPagRen() {
        throw new FieldNotMappedException("durPagRen");
    }

    @Override
    public void setDurPagRen(int durPagRen) {
        throw new FieldNotMappedException("durPagRen");
    }

    @Override
    public Integer getDurPagRenObj() {
        return ((Integer)getDurPagRen());
    }

    @Override
    public void setDurPagRenObj(Integer durPagRenObj) {
        setDurPagRen(((int)durPagRenObj));
    }

    @Override
    public AfDecimal getDurResDtCalc() {
        throw new FieldNotMappedException("durResDtCalc");
    }

    @Override
    public void setDurResDtCalc(AfDecimal durResDtCalc) {
        throw new FieldNotMappedException("durResDtCalc");
    }

    @Override
    public AfDecimal getDurResDtCalcObj() {
        return getDurResDtCalc();
    }

    @Override
    public void setDurResDtCalcObj(AfDecimal durResDtCalcObj) {
        setDurResDtCalc(new AfDecimal(durResDtCalcObj, 11, 7));
    }

    @Override
    public String getEmitTitOpzVchar() {
        throw new FieldNotMappedException("emitTitOpzVchar");
    }

    @Override
    public void setEmitTitOpzVchar(String emitTitOpzVchar) {
        throw new FieldNotMappedException("emitTitOpzVchar");
    }

    @Override
    public String getEmitTitOpzVcharObj() {
        return getEmitTitOpzVchar();
    }

    @Override
    public void setEmitTitOpzVcharObj(String emitTitOpzVcharObj) {
        setEmitTitOpzVchar(emitTitOpzVcharObj);
    }

    @Override
    public int getEtaAa1oAssto() {
        throw new FieldNotMappedException("etaAa1oAssto");
    }

    @Override
    public void setEtaAa1oAssto(int etaAa1oAssto) {
        throw new FieldNotMappedException("etaAa1oAssto");
    }

    @Override
    public Integer getEtaAa1oAsstoObj() {
        return ((Integer)getEtaAa1oAssto());
    }

    @Override
    public void setEtaAa1oAsstoObj(Integer etaAa1oAsstoObj) {
        setEtaAa1oAssto(((int)etaAa1oAsstoObj));
    }

    @Override
    public int getEtaMm1oAssto() {
        throw new FieldNotMappedException("etaMm1oAssto");
    }

    @Override
    public void setEtaMm1oAssto(int etaMm1oAssto) {
        throw new FieldNotMappedException("etaMm1oAssto");
    }

    @Override
    public Integer getEtaMm1oAsstoObj() {
        return ((Integer)getEtaMm1oAssto());
    }

    @Override
    public void setEtaMm1oAsstoObj(Integer etaMm1oAsstoObj) {
        setEtaMm1oAssto(((int)etaMm1oAsstoObj));
    }

    @Override
    public AfDecimal getEtaRaggnDtCalc() {
        throw new FieldNotMappedException("etaRaggnDtCalc");
    }

    @Override
    public void setEtaRaggnDtCalc(AfDecimal etaRaggnDtCalc) {
        throw new FieldNotMappedException("etaRaggnDtCalc");
    }

    @Override
    public AfDecimal getEtaRaggnDtCalcObj() {
        return getEtaRaggnDtCalc();
    }

    @Override
    public void setEtaRaggnDtCalcObj(AfDecimal etaRaggnDtCalcObj) {
        setEtaRaggnDtCalc(new AfDecimal(etaRaggnDtCalcObj, 7, 3));
    }

    @Override
    public char getFlCarCont() {
        throw new FieldNotMappedException("flCarCont");
    }

    @Override
    public void setFlCarCont(char flCarCont) {
        throw new FieldNotMappedException("flCarCont");
    }

    @Override
    public Character getFlCarContObj() {
        return ((Character)getFlCarCont());
    }

    @Override
    public void setFlCarContObj(Character flCarContObj) {
        setFlCarCont(((char)flCarContObj));
    }

    @Override
    public char getFlDaTrasf() {
        throw new FieldNotMappedException("flDaTrasf");
    }

    @Override
    public void setFlDaTrasf(char flDaTrasf) {
        throw new FieldNotMappedException("flDaTrasf");
    }

    @Override
    public Character getFlDaTrasfObj() {
        return ((Character)getFlDaTrasf());
    }

    @Override
    public void setFlDaTrasfObj(Character flDaTrasfObj) {
        setFlDaTrasf(((char)flDaTrasfObj));
    }

    @Override
    public char getFlIas() {
        throw new FieldNotMappedException("flIas");
    }

    @Override
    public void setFlIas(char flIas) {
        throw new FieldNotMappedException("flIas");
    }

    @Override
    public Character getFlIasObj() {
        return ((Character)getFlIas());
    }

    @Override
    public void setFlIasObj(Character flIasObj) {
        setFlIas(((char)flIasObj));
    }

    @Override
    public char getFlPreAgg() {
        throw new FieldNotMappedException("flPreAgg");
    }

    @Override
    public void setFlPreAgg(char flPreAgg) {
        throw new FieldNotMappedException("flPreAgg");
    }

    @Override
    public Character getFlPreAggObj() {
        return ((Character)getFlPreAgg());
    }

    @Override
    public void setFlPreAggObj(Character flPreAggObj) {
        setFlPreAgg(((char)flPreAggObj));
    }

    @Override
    public char getFlPreDaRis() {
        throw new FieldNotMappedException("flPreDaRis");
    }

    @Override
    public void setFlPreDaRis(char flPreDaRis) {
        throw new FieldNotMappedException("flPreDaRis");
    }

    @Override
    public Character getFlPreDaRisObj() {
        return ((Character)getFlPreDaRis());
    }

    @Override
    public void setFlPreDaRisObj(Character flPreDaRisObj) {
        setFlPreDaRis(((char)flPreDaRisObj));
    }

    @Override
    public char getFlSimulazione() {
        throw new FieldNotMappedException("flSimulazione");
    }

    @Override
    public void setFlSimulazione(char flSimulazione) {
        throw new FieldNotMappedException("flSimulazione");
    }

    @Override
    public Character getFlSimulazioneObj() {
        return ((Character)getFlSimulazione());
    }

    @Override
    public void setFlSimulazioneObj(Character flSimulazioneObj) {
        setFlSimulazione(((char)flSimulazioneObj));
    }

    @Override
    public char getFlSwitch() {
        throw new FieldNotMappedException("flSwitch");
    }

    @Override
    public void setFlSwitch(char flSwitch) {
        throw new FieldNotMappedException("flSwitch");
    }

    @Override
    public Character getFlSwitchObj() {
        return ((Character)getFlSwitch());
    }

    @Override
    public void setFlSwitchObj(Character flSwitchObj) {
        setFlSwitch(((char)flSwitchObj));
    }

    @Override
    public int getFraz() {
        throw new FieldNotMappedException("fraz");
    }

    @Override
    public void setFraz(int fraz) {
        throw new FieldNotMappedException("fraz");
    }

    @Override
    public int getFrazDecrCpt() {
        throw new FieldNotMappedException("frazDecrCpt");
    }

    @Override
    public void setFrazDecrCpt(int frazDecrCpt) {
        throw new FieldNotMappedException("frazDecrCpt");
    }

    @Override
    public Integer getFrazDecrCptObj() {
        return ((Integer)getFrazDecrCpt());
    }

    @Override
    public void setFrazDecrCptObj(Integer frazDecrCptObj) {
        setFrazDecrCpt(((int)frazDecrCptObj));
    }

    @Override
    public int getFrazIniErogRen() {
        throw new FieldNotMappedException("frazIniErogRen");
    }

    @Override
    public void setFrazIniErogRen(int frazIniErogRen) {
        throw new FieldNotMappedException("frazIniErogRen");
    }

    @Override
    public Integer getFrazIniErogRenObj() {
        return ((Integer)getFrazIniErogRen());
    }

    @Override
    public void setFrazIniErogRenObj(Integer frazIniErogRenObj) {
        setFrazIniErogRen(((int)frazIniErogRenObj));
    }

    @Override
    public Integer getFrazObj() {
        return ((Integer)getFraz());
    }

    @Override
    public void setFrazObj(Integer frazObj) {
        setFraz(((int)frazObj));
    }

    @Override
    public String getIbAccComm() {
        throw new FieldNotMappedException("ibAccComm");
    }

    @Override
    public void setIbAccComm(String ibAccComm) {
        throw new FieldNotMappedException("ibAccComm");
    }

    @Override
    public String getIbAccCommObj() {
        return getIbAccComm();
    }

    @Override
    public void setIbAccCommObj(String ibAccCommObj) {
        setIbAccComm(ibAccCommObj);
    }

    @Override
    public String getIbAdes() {
        throw new FieldNotMappedException("ibAdes");
    }

    @Override
    public void setIbAdes(String ibAdes) {
        throw new FieldNotMappedException("ibAdes");
    }

    @Override
    public String getIbAdesObj() {
        return getIbAdes();
    }

    @Override
    public void setIbAdesObj(String ibAdesObj) {
        setIbAdes(ibAdesObj);
    }

    @Override
    public String getIbPoli() {
        throw new FieldNotMappedException("ibPoli");
    }

    @Override
    public void setIbPoli(String ibPoli) {
        throw new FieldNotMappedException("ibPoli");
    }

    @Override
    public String getIbPoliObj() {
        return getIbPoli();
    }

    @Override
    public void setIbPoliObj(String ibPoliObj) {
        setIbPoli(ibPoliObj);
    }

    @Override
    public String getIbTrchDiGar() {
        throw new FieldNotMappedException("ibTrchDiGar");
    }

    @Override
    public void setIbTrchDiGar(String ibTrchDiGar) {
        throw new FieldNotMappedException("ibTrchDiGar");
    }

    @Override
    public String getIbTrchDiGarObj() {
        return getIbTrchDiGar();
    }

    @Override
    public void setIbTrchDiGarObj(String ibTrchDiGarObj) {
        setIbTrchDiGar(ibTrchDiGarObj);
    }

    @Override
    public int getIdGar() {
        return bilaTrchEstr.getB03IdGar();
    }

    @Override
    public void setIdGar(int idGar) {
        this.bilaTrchEstr.setB03IdGar(idGar);
    }

    @Override
    public int getIdRichEstrazAgg() {
        throw new FieldNotMappedException("idRichEstrazAgg");
    }

    @Override
    public void setIdRichEstrazAgg(int idRichEstrazAgg) {
        throw new FieldNotMappedException("idRichEstrazAgg");
    }

    @Override
    public Integer getIdRichEstrazAggObj() {
        return ((Integer)getIdRichEstrazAgg());
    }

    @Override
    public void setIdRichEstrazAggObj(Integer idRichEstrazAggObj) {
        setIdRichEstrazAgg(((int)idRichEstrazAggObj));
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        return idsv0003.getCodiceCompagniaAnia();
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        this.idsv0003.setCodiceCompagniaAnia(idsv0003CodiceCompagniaAnia);
    }

    @Override
    public AfDecimal getImpCarCasoMor() {
        throw new FieldNotMappedException("impCarCasoMor");
    }

    @Override
    public void setImpCarCasoMor(AfDecimal impCarCasoMor) {
        throw new FieldNotMappedException("impCarCasoMor");
    }

    @Override
    public AfDecimal getImpCarCasoMorObj() {
        return getImpCarCasoMor();
    }

    @Override
    public void setImpCarCasoMorObj(AfDecimal impCarCasoMorObj) {
        setImpCarCasoMor(new AfDecimal(impCarCasoMorObj, 15, 3));
    }

    @Override
    public AfDecimal getIntrTecn() {
        throw new FieldNotMappedException("intrTecn");
    }

    @Override
    public void setIntrTecn(AfDecimal intrTecn) {
        throw new FieldNotMappedException("intrTecn");
    }

    @Override
    public AfDecimal getIntrTecnObj() {
        return getIntrTecn();
    }

    @Override
    public void setIntrTecnObj(AfDecimal intrTecnObj) {
        setIntrTecn(new AfDecimal(intrTecnObj, 6, 3));
    }

    @Override
    public short getMetRiscSpcl() {
        throw new FieldNotMappedException("metRiscSpcl");
    }

    @Override
    public void setMetRiscSpcl(short metRiscSpcl) {
        throw new FieldNotMappedException("metRiscSpcl");
    }

    @Override
    public Short getMetRiscSpclObj() {
        return ((Short)getMetRiscSpcl());
    }

    @Override
    public void setMetRiscSpclObj(Short metRiscSpclObj) {
        setMetRiscSpcl(((short)metRiscSpclObj));
    }

    @Override
    public AfDecimal getMinGartoT() {
        throw new FieldNotMappedException("minGartoT");
    }

    @Override
    public void setMinGartoT(AfDecimal minGartoT) {
        throw new FieldNotMappedException("minGartoT");
    }

    @Override
    public AfDecimal getMinGartoTObj() {
        return getMinGartoT();
    }

    @Override
    public void setMinGartoTObj(AfDecimal minGartoTObj) {
        setMinGartoT(new AfDecimal(minGartoTObj, 15, 3));
    }

    @Override
    public AfDecimal getMinTrnutT() {
        throw new FieldNotMappedException("minTrnutT");
    }

    @Override
    public void setMinTrnutT(AfDecimal minTrnutT) {
        throw new FieldNotMappedException("minTrnutT");
    }

    @Override
    public AfDecimal getMinTrnutTObj() {
        return getMinTrnutT();
    }

    @Override
    public void setMinTrnutTObj(AfDecimal minTrnutTObj) {
        setMinTrnutT(new AfDecimal(minTrnutTObj, 15, 3));
    }

    @Override
    public AfDecimal getNsQuo() {
        throw new FieldNotMappedException("nsQuo");
    }

    @Override
    public void setNsQuo(AfDecimal nsQuo) {
        throw new FieldNotMappedException("nsQuo");
    }

    @Override
    public AfDecimal getNsQuoObj() {
        return getNsQuo();
    }

    @Override
    public void setNsQuoObj(AfDecimal nsQuoObj) {
        setNsQuo(new AfDecimal(nsQuoObj, 6, 3));
    }

    @Override
    public String getNumFinanz() {
        throw new FieldNotMappedException("numFinanz");
    }

    @Override
    public void setNumFinanz(String numFinanz) {
        throw new FieldNotMappedException("numFinanz");
    }

    @Override
    public String getNumFinanzObj() {
        return getNumFinanz();
    }

    @Override
    public void setNumFinanzObj(String numFinanzObj) {
        setNumFinanz(numFinanzObj);
    }

    @Override
    public int getNumPrePatt() {
        throw new FieldNotMappedException("numPrePatt");
    }

    @Override
    public void setNumPrePatt(int numPrePatt) {
        throw new FieldNotMappedException("numPrePatt");
    }

    @Override
    public Integer getNumPrePattObj() {
        return ((Integer)getNumPrePatt());
    }

    @Override
    public void setNumPrePattObj(Integer numPrePattObj) {
        setNumPrePatt(((int)numPrePattObj));
    }

    @Override
    public AfDecimal getOverComm() {
        throw new FieldNotMappedException("overComm");
    }

    @Override
    public void setOverComm(AfDecimal overComm) {
        throw new FieldNotMappedException("overComm");
    }

    @Override
    public AfDecimal getOverCommObj() {
        return getOverComm();
    }

    @Override
    public void setOverCommObj(AfDecimal overCommObj) {
        setOverComm(new AfDecimal(overCommObj, 15, 3));
    }

    @Override
    public AfDecimal getPcCarAcq() {
        throw new FieldNotMappedException("pcCarAcq");
    }

    @Override
    public void setPcCarAcq(AfDecimal pcCarAcq) {
        throw new FieldNotMappedException("pcCarAcq");
    }

    @Override
    public AfDecimal getPcCarAcqObj() {
        return getPcCarAcq();
    }

    @Override
    public void setPcCarAcqObj(AfDecimal pcCarAcqObj) {
        setPcCarAcq(new AfDecimal(pcCarAcqObj, 6, 3));
    }

    @Override
    public AfDecimal getPcCarGest() {
        throw new FieldNotMappedException("pcCarGest");
    }

    @Override
    public void setPcCarGest(AfDecimal pcCarGest) {
        throw new FieldNotMappedException("pcCarGest");
    }

    @Override
    public AfDecimal getPcCarGestObj() {
        return getPcCarGest();
    }

    @Override
    public void setPcCarGestObj(AfDecimal pcCarGestObj) {
        setPcCarGest(new AfDecimal(pcCarGestObj, 6, 3));
    }

    @Override
    public AfDecimal getPcCarMor() {
        throw new FieldNotMappedException("pcCarMor");
    }

    @Override
    public void setPcCarMor(AfDecimal pcCarMor) {
        throw new FieldNotMappedException("pcCarMor");
    }

    @Override
    public AfDecimal getPcCarMorObj() {
        return getPcCarMor();
    }

    @Override
    public void setPcCarMorObj(AfDecimal pcCarMorObj) {
        setPcCarMor(new AfDecimal(pcCarMorObj, 6, 3));
    }

    @Override
    public char getPpInvrioTari() {
        throw new FieldNotMappedException("ppInvrioTari");
    }

    @Override
    public void setPpInvrioTari(char ppInvrioTari) {
        throw new FieldNotMappedException("ppInvrioTari");
    }

    @Override
    public Character getPpInvrioTariObj() {
        return ((Character)getPpInvrioTari());
    }

    @Override
    public void setPpInvrioTariObj(Character ppInvrioTariObj) {
        setPpInvrioTari(((char)ppInvrioTariObj));
    }

    @Override
    public AfDecimal getPreAnnualizRicor() {
        throw new FieldNotMappedException("preAnnualizRicor");
    }

    @Override
    public void setPreAnnualizRicor(AfDecimal preAnnualizRicor) {
        throw new FieldNotMappedException("preAnnualizRicor");
    }

    @Override
    public AfDecimal getPreAnnualizRicorObj() {
        return getPreAnnualizRicor();
    }

    @Override
    public void setPreAnnualizRicorObj(AfDecimal preAnnualizRicorObj) {
        setPreAnnualizRicor(new AfDecimal(preAnnualizRicorObj, 15, 3));
    }

    @Override
    public AfDecimal getPreCont() {
        throw new FieldNotMappedException("preCont");
    }

    @Override
    public void setPreCont(AfDecimal preCont) {
        throw new FieldNotMappedException("preCont");
    }

    @Override
    public AfDecimal getPreContObj() {
        return getPreCont();
    }

    @Override
    public void setPreContObj(AfDecimal preContObj) {
        setPreCont(new AfDecimal(preContObj, 15, 3));
    }

    @Override
    public AfDecimal getPreDovIni() {
        throw new FieldNotMappedException("preDovIni");
    }

    @Override
    public void setPreDovIni(AfDecimal preDovIni) {
        throw new FieldNotMappedException("preDovIni");
    }

    @Override
    public AfDecimal getPreDovIniObj() {
        return getPreDovIni();
    }

    @Override
    public void setPreDovIniObj(AfDecimal preDovIniObj) {
        setPreDovIni(new AfDecimal(preDovIniObj, 15, 3));
    }

    @Override
    public AfDecimal getPreDovRivtoT() {
        throw new FieldNotMappedException("preDovRivtoT");
    }

    @Override
    public void setPreDovRivtoT(AfDecimal preDovRivtoT) {
        throw new FieldNotMappedException("preDovRivtoT");
    }

    @Override
    public AfDecimal getPreDovRivtoTObj() {
        return getPreDovRivtoT();
    }

    @Override
    public void setPreDovRivtoTObj(AfDecimal preDovRivtoTObj) {
        setPreDovRivtoT(new AfDecimal(preDovRivtoTObj, 15, 3));
    }

    @Override
    public AfDecimal getPrePattuitoIni() {
        throw new FieldNotMappedException("prePattuitoIni");
    }

    @Override
    public void setPrePattuitoIni(AfDecimal prePattuitoIni) {
        throw new FieldNotMappedException("prePattuitoIni");
    }

    @Override
    public AfDecimal getPrePattuitoIniObj() {
        return getPrePattuitoIni();
    }

    @Override
    public void setPrePattuitoIniObj(AfDecimal prePattuitoIniObj) {
        setPrePattuitoIni(new AfDecimal(prePattuitoIniObj, 15, 3));
    }

    @Override
    public AfDecimal getPrePpIni() {
        throw new FieldNotMappedException("prePpIni");
    }

    @Override
    public void setPrePpIni(AfDecimal prePpIni) {
        throw new FieldNotMappedException("prePpIni");
    }

    @Override
    public AfDecimal getPrePpIniObj() {
        return getPrePpIni();
    }

    @Override
    public void setPrePpIniObj(AfDecimal prePpIniObj) {
        setPrePpIni(new AfDecimal(prePpIniObj, 15, 3));
    }

    @Override
    public AfDecimal getPrePpUlt() {
        throw new FieldNotMappedException("prePpUlt");
    }

    @Override
    public void setPrePpUlt(AfDecimal prePpUlt) {
        throw new FieldNotMappedException("prePpUlt");
    }

    @Override
    public AfDecimal getPrePpUltObj() {
        return getPrePpUlt();
    }

    @Override
    public void setPrePpUltObj(AfDecimal prePpUltObj) {
        setPrePpUlt(new AfDecimal(prePpUltObj, 15, 3));
    }

    @Override
    public AfDecimal getPreRiasto() {
        throw new FieldNotMappedException("preRiasto");
    }

    @Override
    public void setPreRiasto(AfDecimal preRiasto) {
        throw new FieldNotMappedException("preRiasto");
    }

    @Override
    public AfDecimal getPreRiastoEcc() {
        throw new FieldNotMappedException("preRiastoEcc");
    }

    @Override
    public void setPreRiastoEcc(AfDecimal preRiastoEcc) {
        throw new FieldNotMappedException("preRiastoEcc");
    }

    @Override
    public AfDecimal getPreRiastoEccObj() {
        return getPreRiastoEcc();
    }

    @Override
    public void setPreRiastoEccObj(AfDecimal preRiastoEccObj) {
        setPreRiastoEcc(new AfDecimal(preRiastoEccObj, 15, 3));
    }

    @Override
    public AfDecimal getPreRiastoObj() {
        return getPreRiasto();
    }

    @Override
    public void setPreRiastoObj(AfDecimal preRiastoObj) {
        setPreRiasto(new AfDecimal(preRiastoObj, 15, 3));
    }

    @Override
    public AfDecimal getPreRshT() {
        throw new FieldNotMappedException("preRshT");
    }

    @Override
    public void setPreRshT(AfDecimal preRshT) {
        throw new FieldNotMappedException("preRshT");
    }

    @Override
    public AfDecimal getPreRshTObj() {
        return getPreRshT();
    }

    @Override
    public void setPreRshTObj(AfDecimal preRshTObj) {
        setPreRshT(new AfDecimal(preRshTObj, 15, 3));
    }

    @Override
    public AfDecimal getProvAcq() {
        throw new FieldNotMappedException("provAcq");
    }

    @Override
    public void setProvAcq(AfDecimal provAcq) {
        throw new FieldNotMappedException("provAcq");
    }

    @Override
    public AfDecimal getProvAcqObj() {
        return getProvAcq();
    }

    @Override
    public void setProvAcqObj(AfDecimal provAcqObj) {
        setProvAcq(new AfDecimal(provAcqObj, 15, 3));
    }

    @Override
    public AfDecimal getProvAcqRicor() {
        throw new FieldNotMappedException("provAcqRicor");
    }

    @Override
    public void setProvAcqRicor(AfDecimal provAcqRicor) {
        throw new FieldNotMappedException("provAcqRicor");
    }

    @Override
    public AfDecimal getProvAcqRicorObj() {
        return getProvAcqRicor();
    }

    @Override
    public void setProvAcqRicorObj(AfDecimal provAcqRicorObj) {
        setProvAcqRicor(new AfDecimal(provAcqRicorObj, 15, 3));
    }

    @Override
    public AfDecimal getProvInc() {
        throw new FieldNotMappedException("provInc");
    }

    @Override
    public void setProvInc(AfDecimal provInc) {
        throw new FieldNotMappedException("provInc");
    }

    @Override
    public AfDecimal getProvIncObj() {
        return getProvInc();
    }

    @Override
    public void setProvIncObj(AfDecimal provIncObj) {
        setProvInc(new AfDecimal(provIncObj, 15, 3));
    }

    @Override
    public AfDecimal getPrstzAggIni() {
        throw new FieldNotMappedException("prstzAggIni");
    }

    @Override
    public void setPrstzAggIni(AfDecimal prstzAggIni) {
        throw new FieldNotMappedException("prstzAggIni");
    }

    @Override
    public AfDecimal getPrstzAggIniObj() {
        return getPrstzAggIni();
    }

    @Override
    public void setPrstzAggIniObj(AfDecimal prstzAggIniObj) {
        setPrstzAggIni(new AfDecimal(prstzAggIniObj, 15, 3));
    }

    @Override
    public AfDecimal getPrstzAggUlt() {
        throw new FieldNotMappedException("prstzAggUlt");
    }

    @Override
    public void setPrstzAggUlt(AfDecimal prstzAggUlt) {
        throw new FieldNotMappedException("prstzAggUlt");
    }

    @Override
    public AfDecimal getPrstzAggUltObj() {
        return getPrstzAggUlt();
    }

    @Override
    public void setPrstzAggUltObj(AfDecimal prstzAggUltObj) {
        setPrstzAggUlt(new AfDecimal(prstzAggUltObj, 15, 3));
    }

    @Override
    public AfDecimal getPrstzIni() {
        throw new FieldNotMappedException("prstzIni");
    }

    @Override
    public void setPrstzIni(AfDecimal prstzIni) {
        throw new FieldNotMappedException("prstzIni");
    }

    @Override
    public AfDecimal getPrstzIniObj() {
        return getPrstzIni();
    }

    @Override
    public void setPrstzIniObj(AfDecimal prstzIniObj) {
        setPrstzIni(new AfDecimal(prstzIniObj, 15, 3));
    }

    @Override
    public AfDecimal getPrstzT() {
        throw new FieldNotMappedException("prstzT");
    }

    @Override
    public void setPrstzT(AfDecimal prstzT) {
        throw new FieldNotMappedException("prstzT");
    }

    @Override
    public AfDecimal getPrstzTObj() {
        return getPrstzT();
    }

    @Override
    public void setPrstzTObj(AfDecimal prstzTObj) {
        setPrstzT(new AfDecimal(prstzTObj, 15, 3));
    }

    @Override
    public AfDecimal getQtzSpZCoupDtC() {
        throw new FieldNotMappedException("qtzSpZCoupDtC");
    }

    @Override
    public void setQtzSpZCoupDtC(AfDecimal qtzSpZCoupDtC) {
        throw new FieldNotMappedException("qtzSpZCoupDtC");
    }

    @Override
    public AfDecimal getQtzSpZCoupDtCObj() {
        return getQtzSpZCoupDtC();
    }

    @Override
    public void setQtzSpZCoupDtCObj(AfDecimal qtzSpZCoupDtCObj) {
        setQtzSpZCoupDtC(new AfDecimal(qtzSpZCoupDtCObj, 12, 5));
    }

    @Override
    public AfDecimal getQtzSpZCoupEmis() {
        throw new FieldNotMappedException("qtzSpZCoupEmis");
    }

    @Override
    public void setQtzSpZCoupEmis(AfDecimal qtzSpZCoupEmis) {
        throw new FieldNotMappedException("qtzSpZCoupEmis");
    }

    @Override
    public AfDecimal getQtzSpZCoupEmisObj() {
        return getQtzSpZCoupEmis();
    }

    @Override
    public void setQtzSpZCoupEmisObj(AfDecimal qtzSpZCoupEmisObj) {
        setQtzSpZCoupEmis(new AfDecimal(qtzSpZCoupEmisObj, 12, 5));
    }

    @Override
    public AfDecimal getQtzSpZOpzDtCa() {
        throw new FieldNotMappedException("qtzSpZOpzDtCa");
    }

    @Override
    public void setQtzSpZOpzDtCa(AfDecimal qtzSpZOpzDtCa) {
        throw new FieldNotMappedException("qtzSpZOpzDtCa");
    }

    @Override
    public AfDecimal getQtzSpZOpzDtCaObj() {
        return getQtzSpZOpzDtCa();
    }

    @Override
    public void setQtzSpZOpzDtCaObj(AfDecimal qtzSpZOpzDtCaObj) {
        setQtzSpZOpzDtCa(new AfDecimal(qtzSpZOpzDtCaObj, 12, 5));
    }

    @Override
    public AfDecimal getQtzSpZOpzEmis() {
        throw new FieldNotMappedException("qtzSpZOpzEmis");
    }

    @Override
    public void setQtzSpZOpzEmis(AfDecimal qtzSpZOpzEmis) {
        throw new FieldNotMappedException("qtzSpZOpzEmis");
    }

    @Override
    public AfDecimal getQtzSpZOpzEmisObj() {
        return getQtzSpZOpzEmis();
    }

    @Override
    public void setQtzSpZOpzEmisObj(AfDecimal qtzSpZOpzEmisObj) {
        setQtzSpZOpzEmis(new AfDecimal(qtzSpZOpzEmisObj, 12, 5));
    }

    @Override
    public AfDecimal getQtzTotDtCalc() {
        throw new FieldNotMappedException("qtzTotDtCalc");
    }

    @Override
    public void setQtzTotDtCalc(AfDecimal qtzTotDtCalc) {
        throw new FieldNotMappedException("qtzTotDtCalc");
    }

    @Override
    public AfDecimal getQtzTotDtCalcObj() {
        return getQtzTotDtCalc();
    }

    @Override
    public void setQtzTotDtCalcObj(AfDecimal qtzTotDtCalcObj) {
        setQtzTotDtCalc(new AfDecimal(qtzTotDtCalcObj, 12, 5));
    }

    @Override
    public AfDecimal getQtzTotDtUltBil() {
        throw new FieldNotMappedException("qtzTotDtUltBil");
    }

    @Override
    public void setQtzTotDtUltBil(AfDecimal qtzTotDtUltBil) {
        throw new FieldNotMappedException("qtzTotDtUltBil");
    }

    @Override
    public AfDecimal getQtzTotDtUltBilObj() {
        return getQtzTotDtUltBil();
    }

    @Override
    public void setQtzTotDtUltBilObj(AfDecimal qtzTotDtUltBilObj) {
        setQtzTotDtUltBil(new AfDecimal(qtzTotDtUltBilObj, 12, 5));
    }

    @Override
    public AfDecimal getQtzTotEmis() {
        throw new FieldNotMappedException("qtzTotEmis");
    }

    @Override
    public void setQtzTotEmis(AfDecimal qtzTotEmis) {
        throw new FieldNotMappedException("qtzTotEmis");
    }

    @Override
    public AfDecimal getQtzTotEmisObj() {
        return getQtzTotEmis();
    }

    @Override
    public void setQtzTotEmisObj(AfDecimal qtzTotEmisObj) {
        setQtzTotEmis(new AfDecimal(qtzTotEmisObj, 12, 5));
    }

    @Override
    public String getRamoBila() {
        throw new FieldNotMappedException("ramoBila");
    }

    @Override
    public void setRamoBila(String ramoBila) {
        throw new FieldNotMappedException("ramoBila");
    }

    @Override
    public AfDecimal getRappel() {
        throw new FieldNotMappedException("rappel");
    }

    @Override
    public void setRappel(AfDecimal rappel) {
        throw new FieldNotMappedException("rappel");
    }

    @Override
    public AfDecimal getRappelObj() {
        return getRappel();
    }

    @Override
    public void setRappelObj(AfDecimal rappelObj) {
        setRappel(new AfDecimal(rappelObj, 15, 3));
    }

    @Override
    public AfDecimal getRatRen() {
        throw new FieldNotMappedException("ratRen");
    }

    @Override
    public void setRatRen(AfDecimal ratRen) {
        throw new FieldNotMappedException("ratRen");
    }

    @Override
    public AfDecimal getRatRenObj() {
        return getRatRen();
    }

    @Override
    public void setRatRenObj(AfDecimal ratRenObj) {
        setRatRen(new AfDecimal(ratRenObj, 15, 3));
    }

    @Override
    public AfDecimal getRemunAss() {
        throw new FieldNotMappedException("remunAss");
    }

    @Override
    public void setRemunAss(AfDecimal remunAss) {
        throw new FieldNotMappedException("remunAss");
    }

    @Override
    public AfDecimal getRemunAssObj() {
        return getRemunAss();
    }

    @Override
    public void setRemunAssObj(AfDecimal remunAssObj) {
        setRemunAss(new AfDecimal(remunAssObj, 15, 3));
    }

    @Override
    public AfDecimal getRisAcqT() {
        throw new FieldNotMappedException("risAcqT");
    }

    @Override
    public void setRisAcqT(AfDecimal risAcqT) {
        throw new FieldNotMappedException("risAcqT");
    }

    @Override
    public AfDecimal getRisAcqTObj() {
        return getRisAcqT();
    }

    @Override
    public void setRisAcqTObj(AfDecimal risAcqTObj) {
        setRisAcqT(new AfDecimal(risAcqTObj, 15, 3));
    }

    @Override
    public AfDecimal getRisMatChiuPrec() {
        throw new FieldNotMappedException("risMatChiuPrec");
    }

    @Override
    public void setRisMatChiuPrec(AfDecimal risMatChiuPrec) {
        throw new FieldNotMappedException("risMatChiuPrec");
    }

    @Override
    public AfDecimal getRisMatChiuPrecObj() {
        return getRisMatChiuPrec();
    }

    @Override
    public void setRisMatChiuPrecObj(AfDecimal risMatChiuPrecObj) {
        setRisMatChiuPrec(new AfDecimal(risMatChiuPrecObj, 15, 3));
    }

    @Override
    public AfDecimal getRisPuraT() {
        throw new FieldNotMappedException("risPuraT");
    }

    @Override
    public void setRisPuraT(AfDecimal risPuraT) {
        throw new FieldNotMappedException("risPuraT");
    }

    @Override
    public AfDecimal getRisPuraTObj() {
        return getRisPuraT();
    }

    @Override
    public void setRisPuraTObj(AfDecimal risPuraTObj) {
        setRisPuraT(new AfDecimal(risPuraTObj, 15, 3));
    }

    @Override
    public AfDecimal getRisRiasta() {
        throw new FieldNotMappedException("risRiasta");
    }

    @Override
    public void setRisRiasta(AfDecimal risRiasta) {
        throw new FieldNotMappedException("risRiasta");
    }

    @Override
    public AfDecimal getRisRiastaEcc() {
        throw new FieldNotMappedException("risRiastaEcc");
    }

    @Override
    public void setRisRiastaEcc(AfDecimal risRiastaEcc) {
        throw new FieldNotMappedException("risRiastaEcc");
    }

    @Override
    public AfDecimal getRisRiastaEccObj() {
        return getRisRiastaEcc();
    }

    @Override
    public void setRisRiastaEccObj(AfDecimal risRiastaEccObj) {
        setRisRiastaEcc(new AfDecimal(risRiastaEccObj, 15, 3));
    }

    @Override
    public AfDecimal getRisRiastaObj() {
        return getRisRiasta();
    }

    @Override
    public void setRisRiastaObj(AfDecimal risRiastaObj) {
        setRisRiasta(new AfDecimal(risRiastaObj, 15, 3));
    }

    @Override
    public AfDecimal getRisRistorniCap() {
        throw new FieldNotMappedException("risRistorniCap");
    }

    @Override
    public void setRisRistorniCap(AfDecimal risRistorniCap) {
        throw new FieldNotMappedException("risRistorniCap");
    }

    @Override
    public AfDecimal getRisRistorniCapObj() {
        return getRisRistorniCap();
    }

    @Override
    public void setRisRistorniCapObj(AfDecimal risRistorniCapObj) {
        setRisRistorniCap(new AfDecimal(risRistorniCapObj, 15, 3));
    }

    @Override
    public AfDecimal getRisSpeT() {
        throw new FieldNotMappedException("risSpeT");
    }

    @Override
    public void setRisSpeT(AfDecimal risSpeT) {
        throw new FieldNotMappedException("risSpeT");
    }

    @Override
    public AfDecimal getRisSpeTObj() {
        return getRisSpeT();
    }

    @Override
    public void setRisSpeTObj(AfDecimal risSpeTObj) {
        setRisSpeT(new AfDecimal(risSpeTObj, 15, 3));
    }

    @Override
    public AfDecimal getRisZilT() {
        throw new FieldNotMappedException("risZilT");
    }

    @Override
    public void setRisZilT(AfDecimal risZilT) {
        throw new FieldNotMappedException("risZilT");
    }

    @Override
    public AfDecimal getRisZilTObj() {
        return getRisZilT();
    }

    @Override
    public void setRisZilTObj(AfDecimal risZilTObj) {
        setRisZilT(new AfDecimal(risZilTObj, 15, 3));
    }

    @Override
    public AfDecimal getRiscpar() {
        throw new FieldNotMappedException("riscpar");
    }

    @Override
    public void setRiscpar(AfDecimal riscpar) {
        throw new FieldNotMappedException("riscpar");
    }

    @Override
    public AfDecimal getRiscparObj() {
        return getRiscpar();
    }

    @Override
    public void setRiscparObj(AfDecimal riscparObj) {
        setRiscpar(new AfDecimal(riscparObj, 15, 3));
    }

    @Override
    public char getSex1oAssto() {
        throw new FieldNotMappedException("sex1oAssto");
    }

    @Override
    public void setSex1oAssto(char sex1oAssto) {
        throw new FieldNotMappedException("sex1oAssto");
    }

    @Override
    public Character getSex1oAsstoObj() {
        return ((Character)getSex1oAssto());
    }

    @Override
    public void setSex1oAsstoObj(Character sex1oAsstoObj) {
        setSex1oAssto(((char)sex1oAsstoObj));
    }

    @Override
    public char getStatAssto1() {
        throw new FieldNotMappedException("statAssto1");
    }

    @Override
    public void setStatAssto1(char statAssto1) {
        throw new FieldNotMappedException("statAssto1");
    }

    @Override
    public Character getStatAssto1Obj() {
        return ((Character)getStatAssto1());
    }

    @Override
    public void setStatAssto1Obj(Character statAssto1Obj) {
        setStatAssto1(((char)statAssto1Obj));
    }

    @Override
    public char getStatAssto2() {
        throw new FieldNotMappedException("statAssto2");
    }

    @Override
    public void setStatAssto2(char statAssto2) {
        throw new FieldNotMappedException("statAssto2");
    }

    @Override
    public Character getStatAssto2Obj() {
        return ((Character)getStatAssto2());
    }

    @Override
    public void setStatAssto2Obj(Character statAssto2Obj) {
        setStatAssto2(((char)statAssto2Obj));
    }

    @Override
    public char getStatAssto3() {
        throw new FieldNotMappedException("statAssto3");
    }

    @Override
    public void setStatAssto3(char statAssto3) {
        throw new FieldNotMappedException("statAssto3");
    }

    @Override
    public Character getStatAssto3Obj() {
        return ((Character)getStatAssto3());
    }

    @Override
    public void setStatAssto3Obj(Character statAssto3Obj) {
        setStatAssto3(((char)statAssto3Obj));
    }

    @Override
    public char getStatTbgcAssto1() {
        throw new FieldNotMappedException("statTbgcAssto1");
    }

    @Override
    public void setStatTbgcAssto1(char statTbgcAssto1) {
        throw new FieldNotMappedException("statTbgcAssto1");
    }

    @Override
    public Character getStatTbgcAssto1Obj() {
        return ((Character)getStatTbgcAssto1());
    }

    @Override
    public void setStatTbgcAssto1Obj(Character statTbgcAssto1Obj) {
        setStatTbgcAssto1(((char)statTbgcAssto1Obj));
    }

    @Override
    public char getStatTbgcAssto2() {
        throw new FieldNotMappedException("statTbgcAssto2");
    }

    @Override
    public void setStatTbgcAssto2(char statTbgcAssto2) {
        throw new FieldNotMappedException("statTbgcAssto2");
    }

    @Override
    public Character getStatTbgcAssto2Obj() {
        return ((Character)getStatTbgcAssto2());
    }

    @Override
    public void setStatTbgcAssto2Obj(Character statTbgcAssto2Obj) {
        setStatTbgcAssto2(((char)statTbgcAssto2Obj));
    }

    @Override
    public char getStatTbgcAssto3() {
        throw new FieldNotMappedException("statTbgcAssto3");
    }

    @Override
    public void setStatTbgcAssto3(char statTbgcAssto3) {
        throw new FieldNotMappedException("statTbgcAssto3");
    }

    @Override
    public Character getStatTbgcAssto3Obj() {
        return ((Character)getStatTbgcAssto3());
    }

    @Override
    public void setStatTbgcAssto3Obj(Character statTbgcAssto3Obj) {
        setStatTbgcAssto3(((char)statTbgcAssto3Obj));
    }

    @Override
    public String getTpAccComm() {
        throw new FieldNotMappedException("tpAccComm");
    }

    @Override
    public void setTpAccComm(String tpAccComm) {
        throw new FieldNotMappedException("tpAccComm");
    }

    @Override
    public String getTpAccCommObj() {
        return getTpAccComm();
    }

    @Override
    public void setTpAccCommObj(String tpAccCommObj) {
        setTpAccComm(tpAccCommObj);
    }

    @Override
    public char getTpAdegPre() {
        throw new FieldNotMappedException("tpAdegPre");
    }

    @Override
    public void setTpAdegPre(char tpAdegPre) {
        throw new FieldNotMappedException("tpAdegPre");
    }

    @Override
    public Character getTpAdegPreObj() {
        return ((Character)getTpAdegPre());
    }

    @Override
    public void setTpAdegPreObj(Character tpAdegPreObj) {
        setTpAdegPre(((char)tpAdegPreObj));
    }

    @Override
    public String getTpCalcRis() {
        throw new FieldNotMappedException("tpCalcRis");
    }

    @Override
    public void setTpCalcRis(String tpCalcRis) {
        throw new FieldNotMappedException("tpCalcRis");
    }

    @Override
    public String getTpCausAdes() {
        throw new FieldNotMappedException("tpCausAdes");
    }

    @Override
    public void setTpCausAdes(String tpCausAdes) {
        throw new FieldNotMappedException("tpCausAdes");
    }

    @Override
    public String getTpCausPoli() {
        throw new FieldNotMappedException("tpCausPoli");
    }

    @Override
    public void setTpCausPoli(String tpCausPoli) {
        throw new FieldNotMappedException("tpCausPoli");
    }

    @Override
    public String getTpCausTrch() {
        return bilaTrchEstr.getB03TpCausTrch();
    }

    @Override
    public void setTpCausTrch(String tpCausTrch) {
        this.bilaTrchEstr.setB03TpCausTrch(tpCausTrch);
    }

    @Override
    public String getTpCoass() {
        throw new FieldNotMappedException("tpCoass");
    }

    @Override
    public void setTpCoass(String tpCoass) {
        throw new FieldNotMappedException("tpCoass");
    }

    @Override
    public String getTpCoassObj() {
        return getTpCoass();
    }

    @Override
    public void setTpCoassObj(String tpCoassObj) {
        setTpCoass(tpCoassObj);
    }

    @Override
    public String getTpCopCasoMor() {
        throw new FieldNotMappedException("tpCopCasoMor");
    }

    @Override
    public void setTpCopCasoMor(String tpCopCasoMor) {
        throw new FieldNotMappedException("tpCopCasoMor");
    }

    @Override
    public String getTpCopCasoMorObj() {
        return getTpCopCasoMor();
    }

    @Override
    public void setTpCopCasoMorObj(String tpCopCasoMorObj) {
        setTpCopCasoMor(tpCopCasoMorObj);
    }

    @Override
    public String getTpFrmAssva() {
        throw new FieldNotMappedException("tpFrmAssva");
    }

    @Override
    public void setTpFrmAssva(String tpFrmAssva) {
        throw new FieldNotMappedException("tpFrmAssva");
    }

    @Override
    public String getTpIas() {
        throw new FieldNotMappedException("tpIas");
    }

    @Override
    public void setTpIas(String tpIas) {
        throw new FieldNotMappedException("tpIas");
    }

    @Override
    public String getTpIasObj() {
        return getTpIas();
    }

    @Override
    public void setTpIasObj(String tpIasObj) {
        setTpIas(tpIasObj);
    }

    @Override
    public char getTpPre() {
        throw new FieldNotMappedException("tpPre");
    }

    @Override
    public void setTpPre(char tpPre) {
        throw new FieldNotMappedException("tpPre");
    }

    @Override
    public Character getTpPreObj() {
        return ((Character)getTpPre());
    }

    @Override
    public void setTpPreObj(Character tpPreObj) {
        setTpPre(((char)tpPreObj));
    }

    @Override
    public String getTpPrstz() {
        throw new FieldNotMappedException("tpPrstz");
    }

    @Override
    public void setTpPrstz(String tpPrstz) {
        throw new FieldNotMappedException("tpPrstz");
    }

    @Override
    public String getTpPrstzObj() {
        return getTpPrstz();
    }

    @Override
    public void setTpPrstzObj(String tpPrstzObj) {
        setTpPrstz(tpPrstzObj);
    }

    @Override
    public String getTpRamoBila() {
        throw new FieldNotMappedException("tpRamoBila");
    }

    @Override
    public void setTpRamoBila(String tpRamoBila) {
        throw new FieldNotMappedException("tpRamoBila");
    }

    @Override
    public String getTpRgmFisc() {
        throw new FieldNotMappedException("tpRgmFisc");
    }

    @Override
    public void setTpRgmFisc(String tpRgmFisc) {
        throw new FieldNotMappedException("tpRgmFisc");
    }

    @Override
    public String getTpRgmFiscObj() {
        return getTpRgmFisc();
    }

    @Override
    public void setTpRgmFiscObj(String tpRgmFiscObj) {
        setTpRgmFisc(tpRgmFiscObj);
    }

    @Override
    public String getTpRival() {
        throw new FieldNotMappedException("tpRival");
    }

    @Override
    public void setTpRival(String tpRival) {
        throw new FieldNotMappedException("tpRival");
    }

    @Override
    public String getTpRivalObj() {
        return getTpRival();
    }

    @Override
    public void setTpRivalObj(String tpRivalObj) {
        setTpRival(tpRivalObj);
    }

    @Override
    public String getTpStatBusAdes() {
        throw new FieldNotMappedException("tpStatBusAdes");
    }

    @Override
    public void setTpStatBusAdes(String tpStatBusAdes) {
        throw new FieldNotMappedException("tpStatBusAdes");
    }

    @Override
    public String getTpStatBusPoli() {
        throw new FieldNotMappedException("tpStatBusPoli");
    }

    @Override
    public void setTpStatBusPoli(String tpStatBusPoli) {
        throw new FieldNotMappedException("tpStatBusPoli");
    }

    @Override
    public String getTpStatBusTrch() {
        return bilaTrchEstr.getB03TpStatBusTrch();
    }

    @Override
    public void setTpStatBusTrch(String tpStatBusTrch) {
        this.bilaTrchEstr.setB03TpStatBusTrch(tpStatBusTrch);
    }

    @Override
    public String getTpStatInvst() {
        throw new FieldNotMappedException("tpStatInvst");
    }

    @Override
    public void setTpStatInvst(String tpStatInvst) {
        throw new FieldNotMappedException("tpStatInvst");
    }

    @Override
    public String getTpStatInvstObj() {
        return getTpStatInvst();
    }

    @Override
    public void setTpStatInvstObj(String tpStatInvstObj) {
        setTpStatInvst(tpStatInvstObj);
    }

    @Override
    public String getTpTari() {
        throw new FieldNotMappedException("tpTari");
    }

    @Override
    public void setTpTari(String tpTari) {
        throw new FieldNotMappedException("tpTari");
    }

    @Override
    public String getTpTariObj() {
        return getTpTari();
    }

    @Override
    public void setTpTariObj(String tpTariObj) {
        setTpTari(tpTariObj);
    }

    @Override
    public String getTpTrasf() {
        throw new FieldNotMappedException("tpTrasf");
    }

    @Override
    public void setTpTrasf(String tpTrasf) {
        throw new FieldNotMappedException("tpTrasf");
    }

    @Override
    public String getTpTrasfObj() {
        return getTpTrasf();
    }

    @Override
    public void setTpTrasfObj(String tpTrasfObj) {
        setTpTrasf(tpTrasfObj);
    }

    @Override
    public String getTpTrch() {
        throw new FieldNotMappedException("tpTrch");
    }

    @Override
    public void setTpTrch(String tpTrch) {
        throw new FieldNotMappedException("tpTrch");
    }

    @Override
    public String getTpTrchObj() {
        return getTpTrch();
    }

    @Override
    public void setTpTrchObj(String tpTrchObj) {
        setTpTrch(tpTrchObj);
    }

    @Override
    public String getTpTst() {
        throw new FieldNotMappedException("tpTst");
    }

    @Override
    public void setTpTst(String tpTst) {
        throw new FieldNotMappedException("tpTst");
    }

    @Override
    public String getTpTstObj() {
        return getTpTst();
    }

    @Override
    public void setTpTstObj(String tpTstObj) {
        setTpTst(tpTstObj);
    }

    @Override
    public char getTpVers() {
        throw new FieldNotMappedException("tpVers");
    }

    @Override
    public void setTpVers(char tpVers) {
        throw new FieldNotMappedException("tpVers");
    }

    @Override
    public Character getTpVersObj() {
        return ((Character)getTpVers());
    }

    @Override
    public void setTpVersObj(Character tpVersObj) {
        setTpVers(((char)tpVersObj));
    }

    @Override
    public String getTratRiass() {
        throw new FieldNotMappedException("tratRiass");
    }

    @Override
    public void setTratRiass(String tratRiass) {
        throw new FieldNotMappedException("tratRiass");
    }

    @Override
    public String getTratRiassEcc() {
        throw new FieldNotMappedException("tratRiassEcc");
    }

    @Override
    public void setTratRiassEcc(String tratRiassEcc) {
        throw new FieldNotMappedException("tratRiassEcc");
    }

    @Override
    public String getTratRiassEccObj() {
        return getTratRiassEcc();
    }

    @Override
    public void setTratRiassEccObj(String tratRiassEccObj) {
        setTratRiassEcc(tratRiassEccObj);
    }

    @Override
    public String getTratRiassObj() {
        return getTratRiass();
    }

    @Override
    public void setTratRiassObj(String tratRiassObj) {
        setTratRiass(tratRiassObj);
    }

    @Override
    public AfDecimal getTsMedio() {
        throw new FieldNotMappedException("tsMedio");
    }

    @Override
    public void setTsMedio(AfDecimal tsMedio) {
        throw new FieldNotMappedException("tsMedio");
    }

    @Override
    public AfDecimal getTsMedioObj() {
        return getTsMedio();
    }

    @Override
    public void setTsMedioObj(AfDecimal tsMedioObj) {
        setTsMedio(new AfDecimal(tsMedioObj, 14, 9));
    }

    @Override
    public AfDecimal getTsNetT() {
        throw new FieldNotMappedException("tsNetT");
    }

    @Override
    public void setTsNetT(AfDecimal tsNetT) {
        throw new FieldNotMappedException("tsNetT");
    }

    @Override
    public AfDecimal getTsNetTObj() {
        return getTsNetT();
    }

    @Override
    public void setTsNetTObj(AfDecimal tsNetTObj) {
        setTsNetT(new AfDecimal(tsNetTObj, 14, 9));
    }

    @Override
    public AfDecimal getTsPp() {
        throw new FieldNotMappedException("tsPp");
    }

    @Override
    public void setTsPp(AfDecimal tsPp) {
        throw new FieldNotMappedException("tsPp");
    }

    @Override
    public AfDecimal getTsPpObj() {
        return getTsPp();
    }

    @Override
    public void setTsPpObj(AfDecimal tsPpObj) {
        setTsPp(new AfDecimal(tsPpObj, 14, 9));
    }

    @Override
    public AfDecimal getTsRendtoSppr() {
        throw new FieldNotMappedException("tsRendtoSppr");
    }

    @Override
    public void setTsRendtoSppr(AfDecimal tsRendtoSppr) {
        throw new FieldNotMappedException("tsRendtoSppr");
    }

    @Override
    public AfDecimal getTsRendtoSpprObj() {
        return getTsRendtoSppr();
    }

    @Override
    public void setTsRendtoSpprObj(AfDecimal tsRendtoSpprObj) {
        setTsRendtoSppr(new AfDecimal(tsRendtoSpprObj, 14, 9));
    }

    @Override
    public AfDecimal getTsRendtoT() {
        throw new FieldNotMappedException("tsRendtoT");
    }

    @Override
    public void setTsRendtoT(AfDecimal tsRendtoT) {
        throw new FieldNotMappedException("tsRendtoT");
    }

    @Override
    public AfDecimal getTsRendtoTObj() {
        return getTsRendtoT();
    }

    @Override
    public void setTsRendtoTObj(AfDecimal tsRendtoTObj) {
        setTsRendtoT(new AfDecimal(tsRendtoTObj, 14, 9));
    }

    @Override
    public AfDecimal getTsStabPre() {
        throw new FieldNotMappedException("tsStabPre");
    }

    @Override
    public void setTsStabPre(AfDecimal tsStabPre) {
        throw new FieldNotMappedException("tsStabPre");
    }

    @Override
    public AfDecimal getTsStabPreObj() {
        return getTsStabPre();
    }

    @Override
    public void setTsStabPreObj(AfDecimal tsStabPreObj) {
        setTsStabPre(new AfDecimal(tsStabPreObj, 14, 9));
    }

    @Override
    public AfDecimal getTsTariDov() {
        throw new FieldNotMappedException("tsTariDov");
    }

    @Override
    public void setTsTariDov(AfDecimal tsTariDov) {
        throw new FieldNotMappedException("tsTariDov");
    }

    @Override
    public AfDecimal getTsTariDovObj() {
        return getTsTariDov();
    }

    @Override
    public void setTsTariDovObj(AfDecimal tsTariDovObj) {
        setTsTariDov(new AfDecimal(tsTariDovObj, 14, 9));
    }

    @Override
    public AfDecimal getTsTariScon() {
        throw new FieldNotMappedException("tsTariScon");
    }

    @Override
    public void setTsTariScon(AfDecimal tsTariScon) {
        throw new FieldNotMappedException("tsTariScon");
    }

    @Override
    public AfDecimal getTsTariSconObj() {
        return getTsTariScon();
    }

    @Override
    public void setTsTariSconObj(AfDecimal tsTariSconObj) {
        setTsTariScon(new AfDecimal(tsTariSconObj, 14, 9));
    }

    @Override
    public AfDecimal getUltRm() {
        throw new FieldNotMappedException("ultRm");
    }

    @Override
    public void setUltRm(AfDecimal ultRm) {
        throw new FieldNotMappedException("ultRm");
    }

    @Override
    public AfDecimal getUltRmObj() {
        return getUltRm();
    }

    @Override
    public void setUltRmObj(AfDecimal ultRmObj) {
        setUltRm(new AfDecimal(ultRmObj, 15, 3));
    }

    @Override
    public String getVlt() {
        throw new FieldNotMappedException("vlt");
    }

    @Override
    public void setVlt(String vlt) {
        throw new FieldNotMappedException("vlt");
    }

    @Override
    public String getVltObj() {
        return getVlt();
    }

    @Override
    public void setVltObj(String vltObj) {
        setVlt(vltObj);
    }

    @Override
    public AfDecimal getcSubrshT() {
        throw new FieldNotMappedException("cSubrshT");
    }

    @Override
    public void setcSubrshT(AfDecimal cSubrshT) {
        throw new FieldNotMappedException("cSubrshT");
    }

    @Override
    public AfDecimal getcSubrshTObj() {
        return getcSubrshT();
    }

    @Override
    public void setcSubrshTObj(AfDecimal cSubrshTObj) {
        setcSubrshT(new AfDecimal(cSubrshTObj, 15, 3));
    }
}
