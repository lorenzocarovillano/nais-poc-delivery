package it.accenture.jnais;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.BatchProgram;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.ws.AreaIdsv0001;
import it.accenture.jnais.ws.enums.Idso0011SqlcodeSigned;
import it.accenture.jnais.ws.enums.WsTpRich;
import it.accenture.jnais.ws.Ieai9901Area;
import it.accenture.jnais.ws.Llbs0269Data;
import it.accenture.jnais.ws.Llbv0269;
import it.accenture.jnais.ws.WlbRecPren;

/**Original name: LLBS0269<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA VER. 1.0                          **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             NOMOS SISTEMA.
 * DATE-WRITTEN.       2008.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *     PROGRAMMA ..... LLBS0269
 *     TIPOLOGIA...... SERVIZIO
 *     PROCESSO....... XXX
 *     FUNZIONE....... XXX
 *     DESCRIZIONE.... RECUPERA IL TS DI COMPETENZA PRECEDENTE
 *     PAGINA WEB..... LEGGE & BILANCIO - CALCOLO DELLE RISERVE
 * **------------------------------------------------------------***</pre>*/
public class Llbs0269 extends BatchProgram {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Llbs0269Data ws = new Llbs0269Data();
    //Original name: AREA-IDSV0001
    private AreaIdsv0001 areaIdsv0001;
    //Original name: WLB-REC-PREN
    private WlbRecPren wlbRecPren;
    //Original name: LLBV0269
    private Llbv0269 llbv0269;

    //==== METHODS ====
    /**Original name: PROGRAM_LLBS0269_FIRST_SENTENCES<br>
	 * <pre>---------------------------------------------------------------*</pre>*/
    public long execute(AreaIdsv0001 areaIdsv0001, WlbRecPren wlbRecPren, Llbv0269 llbv0269) {
        this.areaIdsv0001 = areaIdsv0001;
        this.wlbRecPren = wlbRecPren;
        this.llbv0269 = llbv0269;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: PERFORM S1000-ELABORAZIONE
        //              THRU EX-S1000.
        s1000Elaborazione();
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Llbs0269 getInstance() {
        return ((Llbs0269)Programs.getInstance(Llbs0269.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *     OPERAZIONI INIZIALI                                         *
	 * ----------------------------------------------------------------*
	 *     INIZIALIZZAZIONI</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                        LLBV0269-OUTPUT.
        initOutput();
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*
	 *     RECUPERA IL TS DI COMPETENZA PRECEDENTE, NECESSARIO IN FASE
	 *     DI AGGIORNAMENTO, MA VIENE COMUNQUE VALORIZZATO, ANCHE
	 *     IN FASE DI ESTRAZIONE</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: PERFORM S1002-GESTIONE-TS-COMPET
        //              THRU EX-S1002.
        s1002GestioneTsCompet();
    }

    /**Original name: S1002-GESTIONE-TS-COMPET<br>
	 * <pre>----------------------------------------------------------------*
	 *     GESTIONE TIMESTAMP COMPETENZA PRECEDENTE                    *
	 * ----------------------------------------------------------------*</pre>*/
    private void s1002GestioneTsCompet() {
        // COB_CODE: MOVE WLB-TP-RICH             TO WS-TP-RICH
        ws.getLlbv0000().getTpRich().setTpRich(wlbRecPren.getTpRich());
        //       B2 => Aggiornamento Dati Estratti
        //       B6 => Aggiornamento Dati Estratti e Calcolo Riserve
        //       BA => Aggiornamento Calcolo Riserve
        // COB_CODE:      EVALUATE TRUE
        //           *       B2 => Aggiornamento Dati Estratti
        //           *       B6 => Aggiornamento Dati Estratti e Calcolo Riserve
        //           *       BA => Aggiornamento Calcolo Riserve
        //                   WHEN WS-RICH-AGG-ESTRAZ
        //                   WHEN WS-RICH-AGG-ESTR-E-CALC
        //                   WHEN WS-RICH-AGG-CALC
        //           *            VALORIZZA IL TIMESTAMP COMPETENZA PRECEDENTE
        //           *            IN CASO DI AGGIORNAMENTO GLOBALE
        //                        END-IF
        //                   WHEN OTHER
        //                        MOVE WK-TS-DATA-MIN           TO LLBV0269-TS-PRECED
        //                END-EVALUATE.
        switch (ws.getLlbv0000().getTpRich().getTpRich()) {

            case WsTpRich.AGG_ESTRAZ:
            case WsTpRich.AGG_ESTR_E_CALC:
            case WsTpRich.AGG_CALC://            VALORIZZA IL TIMESTAMP COMPETENZA PRECEDENTE
                //            IN CASO DI AGGIORNAMENTO GLOBALE
                // COB_CODE:              IF  (WLB-COD-PROD      = SPACES OR HIGH-VALUE)
                //                        AND (WLB-IB-POLI-FIRST = SPACES OR HIGH-VALUE)
                //                        AND (WLB-IB-POLI-LAST  = SPACES OR HIGH-VALUE)
                //                        AND (WLB-IB-ADE-FIRST  = SPACES OR HIGH-VALUE)
                //                        AND (WLB-IB-ADE-LAST   = SPACES OR HIGH-VALUE)
                //                              THRU EX-S1003
                //           *            SE STIAMO ESEGUENDO UN AGGIORNAMENTO PARZIALE
                //           *            (CIOE' I CAMPI "PRODOTTO", "RANGE POLIZZE" E
                //           *            "RANGE ADESIONI" SONO VALORIZZATI) IL TIMESTAMP
                //           *            COMPETENZA PRECEDENTE DOVRA' ESSERE UGUALE
                //           *            ALLA "DATA MINIMA" (DATA PIU' VECCHIA)
                //                        ELSE
                //                           MOVE WK-TS-DATA-MIN        TO LLBV0269-TS-PRECED
                //                        END-IF
                if ((Characters.EQ_SPACE.test(wlbRecPren.getCodProd()) || Characters.EQ_HIGH.test(wlbRecPren.getCodProdFormatted())) && (Characters.EQ_SPACE.test(wlbRecPren.getIbPoliFirst()) || Characters.EQ_HIGH.test(wlbRecPren.getIbPoliFirst(), WlbRecPren.Len.IB_POLI_FIRST)) && (Characters.EQ_SPACE.test(wlbRecPren.getIbPoliLast()) || Characters.EQ_HIGH.test(wlbRecPren.getIbPoliLast(), WlbRecPren.Len.IB_POLI_LAST)) && (Characters.EQ_SPACE.test(wlbRecPren.getIbAdeFirst()) || Characters.EQ_HIGH.test(wlbRecPren.getIbAdeFirst(), WlbRecPren.Len.IB_ADE_FIRST)) && (Characters.EQ_SPACE.test(wlbRecPren.getIbAdeLast()) || Characters.EQ_HIGH.test(wlbRecPren.getIbAdeLast(), WlbRecPren.Len.IB_ADE_LAST))) {
                    // COB_CODE: PERFORM S1003-RECUPERO-TS-COMP
                    //              THRU EX-S1003
                    s1003RecuperoTsComp();
                    //            SE STIAMO ESEGUENDO UN AGGIORNAMENTO PARZIALE
                    //            (CIOE' I CAMPI "PRODOTTO", "RANGE POLIZZE" E
                    //            "RANGE ADESIONI" SONO VALORIZZATI) IL TIMESTAMP
                    //            COMPETENZA PRECEDENTE DOVRA' ESSERE UGUALE
                    //            ALLA "DATA MINIMA" (DATA PIU' VECCHIA)
                }
                else {
                    // COB_CODE: MOVE WK-TS-DATA-MIN        TO LLBV0269-TS-PRECED
                    llbv0269.setTsPreced(ws.getWkTsDataMin());
                }
                break;

            default:// COB_CODE: MOVE WK-TS-DATA-MIN           TO LLBV0269-TS-PRECED
                llbv0269.setTsPreced(ws.getWkTsDataMin());
                break;
        }
    }

    /**Original name: S1003-RECUPERO-TS-COMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZA TS COMPETENZA PRECEDENTE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1003RecuperoTsComp() {
        ConcatUtil concatUtil = null;
        // COB_CODE: SET WS-FL-FINE-CICLO-NO       TO TRUE
        ws.getWsFlagCiclo().setNo();
        // COB_CODE: INITIALIZE                    RICH
        initRich();
        // COB_CODE: MOVE WLB-ID-RICH-COLL         TO RIC-ID-RICH
        ws.getRich().setRicIdRich(wlbRecPren.getIdRichColl());
        //    PRENDO IN CONSIDERAZIONE SOLO LE PRENOTAZIONI "ESEGUITE"
        // COB_CODE: SET WS-PREN-ST-ESEGUITA       TO TRUE
        ws.getLlbv0000().getStatiPrenotazione().setEseguita();
        // COB_CODE: MOVE WS-STATI-PRENOTAZIONE    TO LDBV4511-STATO-ELAB-00
        ws.getLdbv4511().setStatoElab00(ws.getLlbv0000().getStatiPrenotazione().getStatiPrenotazione());
        // COB_CODE: SET WS-RICH-ESTRAZ-MASS       TO TRUE
        ws.getLlbv0000().getTpRich().setEstrazMass();
        // COB_CODE: MOVE WS-TP-RICH               TO LDBV4511-TP-RICH-00.
        ws.getLdbv4511().setTpRich00(ws.getLlbv0000().getTpRich().getTpRich());
        // COB_CODE: SET WS-RICH-AGG-ESTRAZ        TO TRUE
        ws.getLlbv0000().getTpRich().setAggEstraz();
        // COB_CODE: MOVE WS-TP-RICH               TO LDBV4511-TP-RICH-01.
        ws.getLdbv4511().setTpRich01(ws.getLlbv0000().getTpRich().getTpRich());
        // COB_CODE: SET WS-RICH-CALC-RISERV       TO TRUE
        ws.getLlbv0000().getTpRich().setCalcRiserv();
        // COB_CODE: MOVE WS-TP-RICH               TO LDBV4511-TP-RICH-02.
        ws.getLdbv4511().setTpRich02(ws.getLlbv0000().getTpRich().getTpRich());
        // COB_CODE: MOVE RICH                     TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getRich().getRichFormatted());
        // COB_CODE: MOVE 'LDBS4510'               TO IDSI0011-CODICE-STR-DATO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("LDBS4510");
        // COB_CODE: MOVE LDBV4511                 TO IDSI0011-BUFFER-WHERE-COND
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferWhereCond(ws.getLdbv4511().getLdbv4511Formatted());
        // COB_CODE: MOVE ZEROES                   TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                            IDSI0011-DATA-FINE-EFFETTO
        //                                            IDSI0011-DATA-COMPETENZA
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC    TO TRUE
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL   TO TRUE
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
        // COB_CODE: SET IDSI0011-TRATT-SENZA-STOR TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattSenzaStor();
        // COB_CODE: SET IDSI0011-WHERE-CONDITION  TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setWhereCondition();
        // COB_CODE: SET IDSI0011-FETCH-FIRST      TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setFetchFirst();
        // COB_CODE: PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC
        //                      OR NOT IDSO0011-SUCCESSFUL-SQL
        //                      OR WS-FL-FINE-CICLO-SI
        //           END-IF
        //           END-PERFORM.
        while (!(!ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc() || !ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().isSuccessfulSql() || ws.getWsFlagCiclo().isSi())) {
            // COB_CODE: PERFORM CALL-DISPATCHER
            //              THRU CALL-DISPATCHER-EX
            callDispatcher();
            // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
            //                   END-EVALUATE
            //                ELSE
            //           *-->    ERRORE DISPATCHER
            //                      THRU EX-S0300
            //                END-IF
            if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
                //-->        OPERAZIONE ESEGUITA CON SUCCESSO
                // COB_CODE:         EVALUATE TRUE
                //           *-->        OPERAZIONE ESEGUITA CON SUCCESSO
                //                       WHEN IDSO0011-SUCCESSFUL-SQL
                //                            END-IF
                //           *-->        NON TROVATO
                //                       WHEN IDSO0011-NOT-FOUND
                //                          END-IF
                //                       WHEN OTHER
                //           *-->        ERRORE DB
                //                             THRU EX-S0300
                //                   END-EVALUATE
                switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                    case Idso0011SqlcodeSigned.SUCCESSFUL_SQL:// COB_CODE: IF NOT IDSI0011-CLOSE-CURSOR
                        //              SET IDSI0011-WHERE-CONDITION  TO TRUE
                        //           ELSE
                        //              SET WS-FL-FINE-CICLO-SI       TO TRUE
                        //           END-IF
                        if (!ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isCloseCursor()) {
                            // COB_CODE: MOVE IDSO0011-BUFFER-DATI
                            //             TO RICH
                            ws.getRich().setRichFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                            // COB_CODE: PERFORM S1500-LEGGI-DETT-RICH
                            //              THRU EX-S1500
                            s1500LeggiDettRich();
                            // COB_CODE: SET IDSI0011-CLOSE-CURSOR     TO TRUE
                            ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setIdsv0003CloseCursor();
                            // COB_CODE: MOVE RICH
                            //             TO IDSI0011-BUFFER-DATI
                            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getRich().getRichFormatted());
                            // COB_CODE: MOVE 'LDBS4510'
                            //             TO IDSI0011-CODICE-STR-DATO
                            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("LDBS4510");
                            // COB_CODE: MOVE LDBV4511
                            //             TO IDSI0011-BUFFER-WHERE-COND
                            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferWhereCond(ws.getLdbv4511().getLdbv4511Formatted());
                            // COB_CODE: SET IDSI0011-TRATT-SENZA-STOR TO TRUE
                            ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattSenzaStor();
                            // COB_CODE: SET IDSI0011-WHERE-CONDITION  TO TRUE
                            ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setWhereCondition();
                        }
                        else {
                            // COB_CODE: SET WS-FL-FINE-CICLO-SI       TO TRUE
                            ws.getWsFlagCiclo().setSi();
                        }
                        //-->        NON TROVATO
                        break;

                    case Idso0011SqlcodeSigned.NOT_FOUND:// COB_CODE: IF IDSI0011-FETCH-FIRST
                        //                 THRU EX-S0300
                        //           ELSE
                        //              SET WS-FL-FINE-CICLO-SI       TO TRUE
                        //           END-IF
                        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchFirst()) {
                            // COB_CODE: MOVE WK-PGM
                            //             TO IEAI9901-COD-SERVIZIO-BE
                            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                            // COB_CODE: MOVE 'S1003-RECUPERO-TS-COMP'
                            //             TO IEAI9901-LABEL-ERR
                            ws.getIeai9901Area().setLabelErr("S1003-RECUPERO-TS-COMP");
                            // COB_CODE: MOVE '001114'
                            //             TO IEAI9901-COD-ERRORE
                            ws.getIeai9901Area().setCodErroreFormatted("001114");
                            // COB_CODE: STRING 'LA RICHIESTA DI ESTRAZIONE MASSIVA '
                            //                  'COLLEGATA DEVE ESSERE IN STATO ESEGUITA'
                            //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                            //           END-STRING
                            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "LA RICHIESTA DI ESTRAZIONE MASSIVA ", "COLLEGATA DEVE ESSERE IN STATO ESEGUITA");
                            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                            //              THRU EX-S0300
                            s0300RicercaGravitaErrore();
                        }
                        else {
                            // COB_CODE: SET WS-FL-FINE-CICLO-SI       TO TRUE
                            ws.getWsFlagCiclo().setSi();
                        }
                        break;

                    default://-->        ERRORE DB
                        // COB_CODE: MOVE WK-PGM
                        //             TO IEAI9901-COD-SERVIZIO-BE
                        ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                        // COB_CODE: MOVE 'S1003-RECUPERO-TS-COMP'
                        //             TO IEAI9901-LABEL-ERR
                        ws.getIeai9901Area().setLabelErr("S1003-RECUPERO-TS-COMP");
                        // COB_CODE: MOVE '005016'
                        //             TO IEAI9901-COD-ERRORE
                        ws.getIeai9901Area().setCodErroreFormatted("005016");
                        // COB_CODE: STRING 'LDBS4510'         ';'
                        //                IDSO0011-RETURN-CODE ';'
                        //                IDSO0011-SQLCODE
                        //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                        //           END-STRING
                        concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "LDBS4510", ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                        ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                        // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        //              THRU EX-S0300
                        s0300RicercaGravitaErrore();
                        break;
                }
            }
            else {
                //-->    ERRORE DISPATCHER
                // COB_CODE: MOVE WK-PGM
                //             TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE 'S1003-RECUPERO-TS-COMP'
                //             TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr("S1003-RECUPERO-TS-COMP");
                // COB_CODE: MOVE '005016'
                //             TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("005016");
                // COB_CODE: STRING 'LDBS4510'           ';'
                //                  IDSO0011-RETURN-CODE ';'
                //                  IDSO0011-SQLCODE
                //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "LDBS4510", ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
            }
        }
    }

    /**Original name: S1500-LEGGI-DETT-RICH<br>
	 * <pre>----------------------------------------------------------------*
	 *    LETTURA DELLA DETTAGLIO RICHIESTA
	 * ----------------------------------------------------------------*</pre>*/
    private void s1500LeggiDettRich() {
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE DETT-RICH.
        initDettRich();
        // COB_CODE: MOVE RIC-ID-RICH              TO DER-ID-RICH.
        ws.getDettRich().setIdRich(ws.getRich().getRicIdRich());
        // COB_CODE: MOVE 1                        TO DER-PROG-DETT-RICH
        ws.getDettRich().setProgDettRich(1);
        //--> NOME TABELLA FISICA DB
        // COB_CODE: MOVE 'DETT-RICH'              TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("DETT-RICH");
        //--> DCLGEN TABELLA
        // COB_CODE: MOVE SPACES                   TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
        // COB_CODE: MOVE DETT-RICH                TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getDettRich().getDettRichFormatted());
        //--> TIPO OPERAZIONE
        // COB_CODE: SET IDSI0011-SELECT              TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        // COB_CODE: SET IDSI0011-TRATT-SENZA-STOR    TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattSenzaStor();
        // COB_CODE: SET IDSI0011-PRIMARY-KEY         TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setPrimaryKey();
        //--> TIPO LETTURA
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC       TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL      TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX
        callDispatcher();
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //                    END-EVALUATE
        //                 ELSE
        //           *--> ERRORE DISPATCHER
        //                       THRU EX-S0300
        //                 END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            // COB_CODE:         EVALUATE TRUE
            //                       WHEN IDSO0011-NOT-FOUND
            //           *--> CHIAVE NON TROVATA
            //                          END-IF
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //           *--> OPERAZIONE ESEGUITA CORRETTAMENTE
            //                            END-IF
            //                       WHEN OTHER
            //           *--> ERRORE DI ACCESSO AL DB
            //                             THRU EX-S0300
            //                    END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.NOT_FOUND://--> CHIAVE NON TROVATA
                    // COB_CODE: IF IDSI0011-FETCH-FIRST
                    //                 THRU EX-S0300
                    //           END-IF
                    if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchFirst()) {
                        // COB_CODE: MOVE WK-PGM
                        //             TO IEAI9901-COD-SERVIZIO-BE
                        ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                        // COB_CODE: MOVE 'S1003-RECUPERO-TS-COMP'
                        //             TO IEAI9901-LABEL-ERR
                        ws.getIeai9901Area().setLabelErr("S1003-RECUPERO-TS-COMP");
                        // COB_CODE: MOVE '001114'
                        //             TO IEAI9901-COD-ERRORE
                        ws.getIeai9901Area().setCodErroreFormatted("001114");
                        // COB_CODE: STRING 'DETTAGLIO RICHIESTA NON TROVATA'
                        //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                        //           END-STRING
                        ws.getIeai9901Area().setParametriErr("DETTAGLIO RICHIESTA NON TROVATA");
                        // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        //              THRU EX-S0300
                        s0300RicercaGravitaErrore();
                    }
                    break;

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://--> OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: MOVE IDSO0011-BUFFER-DATI
                    //             TO DETT-RICH
                    ws.getDettRich().setDettRichFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    // COB_CODE: MOVE SPACES
                    //             TO WLB-REC-PREN
                    wlbRecPren.initWlbRecPrenSpaces();
                    // COB_CODE: MOVE DER-AREA-D-RICH
                    //             TO WLB-REC-PREN
                    wlbRecPren.setWlbRecPrenFormatted(ws.getDettRich().getAreaDRichFormatted());
                    // COB_CODE: MOVE WLB-TP-RICH
                    //             TO WS-TP-RICH
                    ws.getLlbv0000().getTpRich().setTpRich(wlbRecPren.getTpRich());
                    // COB_CODE: IF (WS-RICH-AGG-ESTRAZ
                    //           OR  WS-RICH-AGG-ESTR-E-CALC
                    //           OR  WS-RICH-AGG-CALC)
                    //           AND (WLB-COD-PROD      = SPACES OR HIGH-VALUE)
                    //           AND (WLB-IB-POLI-FIRST = SPACES OR HIGH-VALUE)
                    //           AND (WLB-IB-POLI-LAST  = SPACES OR HIGH-VALUE)
                    //           AND (WLB-IB-ADE-FIRST  = SPACES OR HIGH-VALUE)
                    //           AND (WLB-IB-ADE-LAST   = SPACES OR HIGH-VALUE)
                    //                 TO LLBV0269-TS-PRECED
                    //           ELSE
                    //               END-IF
                    //           END-IF
                    if ((ws.getLlbv0000().getTpRich().isAggEstraz() || ws.getLlbv0000().getTpRich().isAggEstrECalc() || ws.getLlbv0000().getTpRich().isAggCalc()) && (Characters.EQ_SPACE.test(wlbRecPren.getCodProd()) || Characters.EQ_HIGH.test(wlbRecPren.getCodProdFormatted())) && (Characters.EQ_SPACE.test(wlbRecPren.getIbPoliFirst()) || Characters.EQ_HIGH.test(wlbRecPren.getIbPoliFirst(), WlbRecPren.Len.IB_POLI_FIRST)) && (Characters.EQ_SPACE.test(wlbRecPren.getIbPoliLast()) || Characters.EQ_HIGH.test(wlbRecPren.getIbPoliLast(), WlbRecPren.Len.IB_POLI_LAST)) && (Characters.EQ_SPACE.test(wlbRecPren.getIbAdeFirst()) || Characters.EQ_HIGH.test(wlbRecPren.getIbAdeFirst(), WlbRecPren.Len.IB_ADE_FIRST)) && (Characters.EQ_SPACE.test(wlbRecPren.getIbAdeLast()) || Characters.EQ_HIGH.test(wlbRecPren.getIbAdeLast(), WlbRecPren.Len.IB_ADE_LAST))) {
                        // COB_CODE: MOVE RIC-TS-EFF-ESEC-RICH
                        //             TO LLBV0269-TS-PRECED
                        llbv0269.setTsPreced(ws.getRich().getRicTsEffEsecRich().getRicTsEffEsecRich());
                    }
                    else if (ws.getLlbv0000().getTpRich().isEstrazMass()) {
                        // COB_CODE:                      IF WS-RICH-ESTRAZ-MASS
                        //                                     TO LLBV0269-TS-PRECED
                        //           *                    VUOL DIRE CHE E' UN AGGIORNAMENTO PARZIALE
                        //           *                    QUINDI VA SCARTATO E LEGGO
                        //           *                    L'OCCORRENZA SUCCESSIVA
                        //                                ELSE
                        //                                     TO IDSI0011-CODICE-STR-DATO
                        //                                END-IF
                        // COB_CODE: MOVE SPACES
                        //             TO WLB-REC-PREN
                        wlbRecPren.initWlbRecPrenSpaces();
                        // COB_CODE: MOVE DER-AREA-D-RICH
                        //             TO WLB-REC-PREN
                        wlbRecPren.setWlbRecPrenFormatted(ws.getDettRich().getAreaDRichFormatted());
                        // COB_CODE: MOVE RIC-TS-EFF-ESEC-RICH
                        //             TO LLBV0269-TS-PRECED
                        llbv0269.setTsPreced(ws.getRich().getRicTsEffEsecRich().getRicTsEffEsecRich());
                        //                    VUOL DIRE CHE E' UN AGGIORNAMENTO PARZIALE
                        //                    QUINDI VA SCARTATO E LEGGO
                        //                    L'OCCORRENZA SUCCESSIVA
                    }
                    else {
                        // COB_CODE: SET IDSI0011-TRATT-SENZA-STOR   TO TRUE
                        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattSenzaStor();
                        // COB_CODE: SET IDSI0011-WHERE-CONDITION    TO TRUE
                        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setWhereCondition();
                        // COB_CODE: SET IDSI0011-FETCH-NEXT         TO TRUE
                        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setFetchNext();
                        // COB_CODE: MOVE 'LDBS4510'
                        //             TO IDSI0011-CODICE-STR-DATO
                        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("LDBS4510");
                    }
                    break;

                default://--> ERRORE DI ACCESSO AL DB
                    // COB_CODE: MOVE WK-PGM    TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'S1500-LEGGI-DETT-RICH'
                    //                          TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("S1500-LEGGI-DETT-RICH");
                    // COB_CODE: MOVE '005016'  TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005016");
                    // COB_CODE: STRING 'DETT-RICH;'
                    //                  IDSO0011-RETURN-CODE ';'
                    //                  IDSO0011-SQLCODE
                    //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "DETT-RICH;", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;
            }
        }
        else {
            //--> ERRORE DISPATCHER
            // COB_CODE: MOVE WK-PGM             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S1500-LEGGI-DETT-RICH'
            //                                   TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S1500-LEGGI-DETT-RICH");
            // COB_CODE: MOVE '005016'           TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING 'DETT-RICH;'
            //                  IDSO0011-RETURN-CODE ';'
            //                  IDSO0011-SQLCODE
            //               DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "DETT-RICH;", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    /**Original name: S0300-RICERCA-GRAVITA-ERRORE<br>
	 * <pre>MUOVO L'AREA DEL SERVIZIO NELL'AREA DATI DELL'AREA DI CONTESTO.
	 * ----->VALORIZZO I CAMPI DELLA IDSV0001</pre>*/
    private void s0300RicercaGravitaErrore() {
        Ieas9900 ieas9900 = null;
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR       TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE  'IEAS9900'              TO CALL-PGM
        ws.getIdsv0002().setCallPgm("IEAS9900");
        // COB_CODE: CALL CALL-PGM USING IDSV0001-AREA-CONTESTO
        //                               IEAI9901-AREA
        //                               IEAO9901-AREA
        //             ON EXCEPTION
        //                   THRU EX-S0310-ERRORE-FATALE
        //           END-CALL.
        try {
            ieas9900 = Ieas9900.getInstance();
            ieas9900.run(areaIdsv0001, ws.getIeai9901Area(), ws.getIeao9901Area());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
            areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
            // COB_CODE: PERFORM S0310-ERRORE-FATALE
            //              THRU EX-S0310-ERRORE-FATALE
            s0310ErroreFatale();
        }
        //SE LA CHIAMATA AL SERVIZIO HA ESITO POSITIVO (NESSUN ERRORE DI
        //SISTEMA, VALORIZZO L'AREA DI OUTPUT.
        //INCREMENTO L'INDICE DEGLI ERRORI
        // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
        areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
        //SE L'INDICE E' MINORE DI 10 ...
        // COB_CODE:      IF IDSV0001-MAX-ELE-ERRORI NOT GREATER 10
        //           *SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
        //                   END-IF
        //                ELSE
        //           *IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        //                       TO IDSV0001-DESC-ERRORE-ESTESA
        //                END-IF.
        if (areaIdsv0001.getMaxEleErrori() <= 10) {
            //SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
            // COB_CODE: IF IEAO9901-COD-ERRORE-990 = ZEROES
            //                      EX-S0300-IMPOSTA-ERRORE
            //           ELSE
            //                   IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
            //           END-IF
            if (Characters.EQ_ZERO.test(ws.getIeao9901Area().getCodErrore990Formatted())) {
                // COB_CODE: PERFORM S0300-IMPOSTA-ERRORE THRU
                //                   EX-S0300-IMPOSTA-ERRORE
                s0300ImpostaErrore();
            }
            else {
                // COB_CODE: MOVE 'IEAS9900'
                //             TO IDSV0001-COD-SERVIZIO-BE
                areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
                // COB_CODE: MOVE IEAO9901-LABEL-ERR-990
                //             TO IDSV0001-LABEL-ERR
                areaIdsv0001.getLogErrore().setLabelErr(ws.getIeao9901Area().getLabelErr990());
                // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
                //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
                // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
                areaIdsv0001.getEsito().setIdsv0001EsitoKo();
                // IMPOSTO IL CODICE ERRORE GESTITO ALL'INTERNO DEL PROGRAMMA
                // COB_CODE: MOVE IEAO9901-COD-ERRORE-990
                //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeao9901Area().getCodErrore990Formatted());
                // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
                //             TO IDSV0001-DESC-ERRORE-ESTESA
                //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
            }
        }
        else {
            //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
            // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
            //             TO IDSV0001-LABEL-ERR
            areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
            // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 'IEAS9900'
            //             TO IDSV0001-COD-SERVIZIO-BE
            areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
            // COB_CODE: MOVE 'OVERFLOW IMPAGINAZIONE ERRORI'
            //             TO IDSV0001-DESC-ERRORE-ESTESA
            areaIdsv0001.getLogErrore().setDescErroreEstesa("OVERFLOW IMPAGINAZIONE ERRORI");
        }
    }

    /**Original name: S0300-IMPOSTA-ERRORE<br>
	 * <pre>  se la gravit— di tutti gli errori dell'elaborazione
	 *   h minore di zero ( ad esempio -3) vuol dire che il return-code
	 *   dell'elaborazione complessiva deve essere abbassato da '08' a
	 *   '04'. Per fare cir utilizzo il flag IDSV0001-FORZ-RC-04
	 * IMPOSTO LA GRAVITA'</pre>*/
    private void s0300ImpostaErrore() {
        // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
        // COB_CODE: EVALUATE TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = -3
        //                       IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        //             WHEN  IEAO9901-LIV-GRAVITA = 0 OR 1 OR 2
        //                   SET IDSV0001-FORZ-RC-04-NO  TO TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = 3 OR 4
        //                   SET IDSV0001-ESITO-KO       TO TRUE
        //           END-EVALUATE
        if (ws.getIeao9901Area().getLivGravita() == -3) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-YES TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04Yes();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 2  TO
            //               IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
            areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)2));
        }
        else if (ws.getIeao9901Area().getLivGravita() == 0 || ws.getIeao9901Area().getLivGravita() == 1 || ws.getIeao9901Area().getLivGravita() == 2) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
        }
        else if (ws.getIeao9901Area().getLivGravita() == 3 || ws.getIeao9901Area().getLivGravita() == 4) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        }
        // IMPOSTO IL CODICE ERRORE
        // COB_CODE: MOVE IEAI9901-COD-ERRORE
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeai9901Area().getCodErroreFormatted());
        // SE IL LIVELLO DI GRAVITA' RESTITUITO DALLO IAS9900 E' MAGGIORE
        // DI 2 (ERRORE BLOCCANTE)...IMPOSTO L'ESITO E I DATI DI CONTESTO
        //RELATIVI ALL'ERRORE BLOCCANTE
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE
        //             TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR
        //             TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
        //             TO IDSV0001-DESC-ERRORE-ESTESA
        //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
        // COB_CODE: MOVE SPACES          TO IEAI9901-COD-SERVIZIO-BE
        //                                  IEAI9901-LABEL-ERR
        //                                   IEAI9901-PARAMETRI-ERR.
        ws.getIeai9901Area().setCodServizioBe("");
        ws.getIeai9901Area().setLabelErr("");
        ws.getIeai9901Area().setParametriErr("");
        // COB_CODE: MOVE ZEROES          TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErrore(0);
    }

    /**Original name: S0310-ERRORE-FATALE<br>
	 * <pre>IMPOSTO LA GRAVITA' ERRORE</pre>*/
    private void s0310ErroreFatale() {
        // COB_CODE: MOVE  3
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)3));
        // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
        areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        //IMPOSTO IL CODICE ERRORE (ERRORE FISSO)
        // COB_CODE: MOVE 900001
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErrore(900001);
        //IMPOSTO NOME DEL MODULO
        // COB_CODE: MOVE 'IEAS9900'               TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
        //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
        //              TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
        // COB_CODE: MOVE  'ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900'
        //              TO IDSV0001-DESC-ERRORE-ESTESA
        //                 IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
    }

    /**Original name: CALL-DISPATCHER<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES CALL DISPATCHER
	 * ----------------------------------------------------------------*
	 * *****************************************************************
	 *    CALL DISPATCHER
	 * *****************************************************************</pre>*/
    private void callDispatcher() {
        Idss0010 idss0010 = null;
        // COB_CODE: MOVE IDSV0001-MODALITA-ESECUTIVA
        //                              TO IDSI0011-MODALITA-ESECUTIVA
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011ModalitaEsecutiva().setIdsi0011ModalitaEsecutiva(areaIdsv0001.getAreaComune().getModalitaEsecutiva().getModalitaEsecutiva());
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //                              TO IDSI0011-CODICE-COMPAGNIA-ANIA
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceCompagniaAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: MOVE IDSV0001-COD-MAIN-BATCH
        //                              TO IDSI0011-COD-MAIN-BATCH
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodMainBatch(areaIdsv0001.getAreaComune().getCodMainBatch());
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO
        //                              TO IDSI0011-TIPO-MOVIMENTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011TipoMovimentoFormatted(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimentoFormatted());
        // COB_CODE: MOVE IDSV0001-SESSIONE
        //                              TO IDSI0011-SESSIONE
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011Sessione(areaIdsv0001.getAreaComune().getSessione());
        // COB_CODE: MOVE IDSV0001-USER-NAME
        //                              TO IDSI0011-USER-NAME
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011UserName(areaIdsv0001.getAreaComune().getUserName());
        // COB_CODE: IF IDSI0011-DATA-INIZIO-EFFETTO = 0
        //              END-IF
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataInizioEffetto() == 0) {
            // COB_CODE: IF IDSV0001-LETT-ULT-IMMAGINE-SI
            //              END-IF
            //           ELSE
            //                TO IDSI0011-DATA-INIZIO-EFFETTO
            //           END-IF
            if (areaIdsv0001.getAreaComune().getLettUltImmagine().isIdsv0001LettUltImmagineSi()) {
                // COB_CODE: IF IDSI0011-SELECT
                //           OR IDSI0011-FETCH-FIRST
                //           OR IDSI0011-FETCH-NEXT
                //                TO IDSI0011-DATA-COMPETENZA
                //           ELSE
                //                TO IDSI0011-DATA-INIZIO-EFFETTO
                //           END-IF
                if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isSelect() || ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchFirst() || ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchNext()) {
                    // COB_CODE: MOVE 99991230
                    //             TO IDSI0011-DATA-INIZIO-EFFETTO
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(99991230);
                    // COB_CODE: MOVE 999912304023595999
                    //             TO IDSI0011-DATA-COMPETENZA
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(999912304023595999L);
                }
                else {
                    // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
                    //             TO IDSI0011-DATA-INIZIO-EFFETTO
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
                }
            }
            else {
                // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
                //             TO IDSI0011-DATA-INIZIO-EFFETTO
                ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
            }
        }
        // COB_CODE: IF IDSI0011-DATA-FINE-EFFETTO = 0
        //              MOVE 99991231   TO IDSI0011-DATA-FINE-EFFETTO
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataFineEffetto() == 0) {
            // COB_CODE: MOVE 99991231   TO IDSI0011-DATA-FINE-EFFETTO
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(99991231);
        }
        // COB_CODE: IF IDSI0011-DATA-COMPETENZA = 0
        //                              TO IDSI0011-DATA-COMPETENZA
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataCompetenza() == 0) {
            // COB_CODE: MOVE IDSV0001-DATA-COMPETENZA
            //                           TO IDSI0011-DATA-COMPETENZA
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(areaIdsv0001.getAreaComune().getIdsv0001DataCompetenza());
        }
        // COB_CODE: MOVE IDSV0001-DATA-COMP-AGG-STOR
        //                              TO IDSI0011-DATA-COMP-AGG-STOR
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompAggStor(areaIdsv0001.getAreaComune().getIdsv0001DataCompAggStor());
        // COB_CODE: IF IDSI0011-TRATT-DEFAULT
        //                              TO IDSI0011-TRATTAMENTO-STORICITA
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().isTrattDefault()) {
            // COB_CODE: MOVE IDSV0001-TRATTAMENTO-STORICITA
            //                           TO IDSI0011-TRATTAMENTO-STORICITA
            ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattamentoStoricita(areaIdsv0001.getAreaComune().getTrattamentoStoricita().getTrattamentoStoricita());
        }
        // COB_CODE: MOVE IDSV0001-FORMATO-DATA-DB
        //                              TO IDSI0011-FORMATO-DATA-DB
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011FormatoDataDb().setIdsi0011FormatoDataDb(areaIdsv0001.getAreaComune().getFormatoDataDb().getFormatoDataDb());
        // COB_CODE: MOVE IDSV0001-LIVELLO-DEBUG
        //                              TO IDSI0011-LIVELLO-DEBUG
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloDebug().setIdsi0011LivelloDebugFormatted(areaIdsv0001.getAreaComune().getLivelloDebug().getIdsi0011LivelloDebugFormatted());
        // COB_CODE: MOVE 0             TO IDSI0011-ID-MOVI-ANNULLATO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011IdMoviAnnullato(0);
        // COB_CODE: SET IDSI0011-PTF-NEWLIFE          TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011IdentitaChiamante().setPtfNewlife();
        // COB_CODE: SET IDSV0012-STATUS-SHARED-MEM-KO TO TRUE
        ws.getIdsv00122().getStatusSharedMemory().setKo();
        // COB_CODE: MOVE 'IDSS0010'             TO IDSI0011-PGM
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011Pgm("IDSS0010");
        //     SET WS-ADDRESS-DIS TO ADDRESS OF IN-OUT-IDSS0010.
        // COB_CODE: CALL IDSI0011-PGM           USING IDSI0011-AREA
        //                                             IDSO0011-AREA.
        idss0010 = Idss0010.getInstance();
        idss0010.run(ws.getDispatcherVariables(), ws.getDispatcherVariables().getIdso0011Area());
        // COB_CODE: MOVE IDSO0011-SQLCODE-SIGNED
        //                                  TO IDSO0011-SQLCODE.
        ws.getDispatcherVariables().getIdso0011Area().setSqlcode(ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned());
    }

    public void initOutput() {
        llbv0269.setTsPreced(0);
    }

    public void initRich() {
        ws.getRich().setRicIdRich(0);
        ws.getRich().setRicCodCompAnia(0);
        ws.getRich().setRicTpRich("");
        ws.getRich().setRicCodMacrofunct("");
        ws.getRich().setRicTpMovi(0);
        ws.getRich().setRicIbRich("");
        ws.getRich().setRicDtEff(0);
        ws.getRich().setRicDtRgstrzRich(0);
        ws.getRich().setRicDtPervRich(0);
        ws.getRich().setRicDtEsecRich(0);
        ws.getRich().getRicTsEffEsecRich().setRicTsEffEsecRich(0);
        ws.getRich().getRicIdOgg().setRicIdOgg(0);
        ws.getRich().setRicTpOgg("");
        ws.getRich().setRicIbPoli("");
        ws.getRich().setRicIbAdes("");
        ws.getRich().setRicIbGar("");
        ws.getRich().setRicIbTrchDiGar("");
        ws.getRich().getRicIdBatch().setRicIdBatch(0);
        ws.getRich().getRicIdJob().setRicIdJob(0);
        ws.getRich().setRicFlSimulazione(Types.SPACE_CHAR);
        ws.getRich().setRicKeyOrdinamentoLen(((short)0));
        ws.getRich().setRicKeyOrdinamento("");
        ws.getRich().getRicIdRichCollg().setRicIdRichCollg(0);
        ws.getRich().setRicTpRamoBila("");
        ws.getRich().setRicTpFrmAssva("");
        ws.getRich().setRicTpCalcRis("");
        ws.getRich().setRicDsOperSql(Types.SPACE_CHAR);
        ws.getRich().setRicDsVer(0);
        ws.getRich().setRicDsTsCptz(0);
        ws.getRich().setRicDsUtente("");
        ws.getRich().setRicDsStatoElab(Types.SPACE_CHAR);
        ws.getRich().setRicRamoBila("");
    }

    public void initDettRich() {
        ws.getDettRich().setIdRich(0);
        ws.getDettRich().setProgDettRich(0);
        ws.getDettRich().setCodCompAnia(0);
        ws.getDettRich().setTpAreaDRich("");
        ws.getDettRich().setAreaDRichLen(((short)0));
        ws.getDettRich().setAreaDRich("");
        ws.getDettRich().setDsOperSql(Types.SPACE_CHAR);
        ws.getDettRich().setDsVer(0);
        ws.getDettRich().setDsTsCptz(0);
        ws.getDettRich().setDsUtente("");
        ws.getDettRich().setDsStatoElab(Types.SPACE_CHAR);
    }
}
