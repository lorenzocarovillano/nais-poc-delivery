package it.accenture.jnais;

import com.bphx.ctu.af.core.buffer.BasicBytesClass;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.pointer.IPointerManager;
import com.bphx.ctu.af.core.program.DynamicCall;
import com.bphx.ctu.af.core.program.GenericParam;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.date.CalendarUtil;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Ispc0211DatiInput;
import it.accenture.jnais.copy.WpogDati;
import it.accenture.jnais.copy.WpolDati;
import it.accenture.jnais.ws.AreaIdsv0001;
import it.accenture.jnais.ws.AreaIoIsps0211;
import it.accenture.jnais.ws.AreaPassaggio;
import it.accenture.jnais.ws.enums.Idso0011SqlcodeSigned;
import it.accenture.jnais.ws.enums.WsTpDato;
import it.accenture.jnais.ws.Ieai9901Area;
import it.accenture.jnais.ws.Loas0800Data;
import it.accenture.jnais.ws.occurs.Ispc0211TabErrori;
import it.accenture.jnais.ws.ptr.Idsv8888StrPerformanceDbg;
import it.accenture.jnais.ws.redefines.Ispc0211TabValP;
import it.accenture.jnais.ws.redefines.Ispc0211TabValT;
import it.accenture.jnais.ws.redefines.WpogIdMoviChiu;
import it.accenture.jnais.ws.redefines.WpogValDt;
import it.accenture.jnais.ws.redefines.WpogValImp;
import it.accenture.jnais.ws.redefines.WpogValNum;
import it.accenture.jnais.ws.redefines.WpogValPc;
import it.accenture.jnais.ws.redefines.WpogValTs;
import it.accenture.jnais.ws.W660AreaPag;
import it.accenture.jnais.ws.W800AreaPag;
import it.accenture.jnais.ws.WadeAreaAdesioneLoas0800;
import it.accenture.jnais.ws.WgrzAreaGaranziaLccs0005;
import it.accenture.jnais.ws.WmovAreaMovimento;
import it.accenture.jnais.ws.WpmoAreaParamMoviLoas0800;
import it.accenture.jnais.ws.WpolAreaPolizzaLccs0005;
import it.accenture.jnais.ws.WskdAreaScheda;
import it.accenture.jnais.ws.WtgaAreaTrancheLoas0800;
import javax.inject.Inject;

/**Original name: LOAS0800<br>
 * <pre>  ============================================================= *
 *                                                                 *
 *         PORTAFOGLIO VITA ITALIA  VER 1.0                        *
 *                                                                 *
 *   ============================================================= *
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2009.
 * DATE-COMPILED.
 * ----------------------------------------------------------------*
 *      PROGRAMMA ..... LOAS0800                                   *
 *      TIPOLOGIA...... OPERAZIONI AUTOMATICHE                     *
 *      PROCESSO....... CALCOLO MANAGEMENT FEE                     *
 *      FUNZIONE....... BATCH                                      *
 *      DESCRIZIONE.... SERVIZIO DI CALCOLO MANAGEMENT FEE         *
 *      PAGINA WEB..... N.A.                                       *
 * ----------------------------------------------------------------*</pre>*/
public class Loas0800 extends Program {

    //==== PROPERTIES ====
    @Inject
    private IPointerManager pointerManager;
    //Original name: WORKING-STORAGE
    private Loas0800Data ws = new Loas0800Data();
    //Original name: AREA-IDSV0001
    private AreaIdsv0001 areaIdsv0001;
    //Original name: WPMO-AREA-PARAM-MOVI
    private WpmoAreaParamMoviLoas0800 wpmoAreaParamMovi;
    //Original name: WMOV-AREA-MOVIMENTO
    private WmovAreaMovimento wmovAreaMovimento;
    //Original name: WPOL-AREA-POLIZZA
    private WpolAreaPolizzaLccs0005 wpolAreaPolizza;
    //Original name: WADE-AREA-ADESIONE
    private WadeAreaAdesioneLoas0800 wadeAreaAdesione;
    //Original name: WGRZ-AREA-GARANZIA
    private WgrzAreaGaranziaLccs0005 wgrzAreaGaranzia;
    //Original name: WTGA-AREA-TRANCHE
    private WtgaAreaTrancheLoas0800 wtgaAreaTranche;
    //Original name: WSKD-AREA-SCHEDA
    private WskdAreaScheda wskdAreaScheda;
    //Original name: WCOM-IO-STATI
    private AreaPassaggio wcomIoStati;
    //Original name: W660-AREA-PAG
    private W660AreaPag w660AreaPag;
    //Original name: W800-AREA-PAG
    private W800AreaPag w800AreaPag;

    //==== METHODS ====
    /**Original name: PROGRAM_LOAS0800_FIRST_SENTENCES<br>*/
    public long execute(AreaIdsv0001 areaIdsv0001, AreaPassaggio wcomIoStati, WpmoAreaParamMoviLoas0800 wpmoAreaParamMovi, WmovAreaMovimento wmovAreaMovimento, WpolAreaPolizzaLccs0005 wpolAreaPolizza, WadeAreaAdesioneLoas0800 wadeAreaAdesione, WgrzAreaGaranziaLccs0005 wgrzAreaGaranzia, WtgaAreaTrancheLoas0800 wtgaAreaTranche, WskdAreaScheda wskdAreaScheda, W660AreaPag w660AreaPag, W800AreaPag w800AreaPag) {
        this.areaIdsv0001 = areaIdsv0001;
        this.wcomIoStati = wcomIoStati;
        this.wpmoAreaParamMovi = wpmoAreaParamMovi;
        this.wmovAreaMovimento = wmovAreaMovimento;
        this.wpolAreaPolizza = wpolAreaPolizza;
        this.wadeAreaAdesione = wadeAreaAdesione;
        this.wgrzAreaGaranzia = wgrzAreaGaranzia;
        this.wtgaAreaTranche = wtgaAreaTranche;
        this.wskdAreaScheda = wskdAreaScheda;
        this.w660AreaPag = w660AreaPag;
        this.w800AreaPag = w800AreaPag;
        // COB_CODE: PERFORM S00000-OPERAZ-INIZIALI
        //              THRU S00000-OPERAZ-INIZIALI-EX.
        s00000OperazIniziali();
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                      THRU S10000-ELABORAZIONE-EX
        //           *
        //                END-IF.
        if (this.areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE: PERFORM S10000-ELABORAZIONE
            //              THRU S10000-ELABORAZIONE-EX
            s10000Elaborazione();
            //
        }
        //
        // COB_CODE: PERFORM S90000-OPERAZ-FINALI
        //              THRU S90000-OPERAZ-FINALI-EX.
        s90000OperazFinali();
        //
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Loas0800 getInstance() {
        return ((Loas0800)Programs.getInstance(Loas0800.class));
    }

    /**Original name: S00000-OPERAZ-INIZIALI<br>
	 * <pre> ============================================================== *
	 *  -->           O P E R Z I O N I   I N I Z I A L I          <-- *
	 *  ============================================================== *</pre>*/
    private void s00000OperazIniziali() {
        // COB_CODE: MOVE 'S00000-OPERAZ-INIZIALI'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S00000-OPERAZ-INIZIALI");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        // COB_CODE: PERFORM S00100-INIZIALIZZA-WORK
        //              THRU S00100-INIZIALIZZA-WORK-EX.
        s00100InizializzaWork();
        // COB_CODE: PERFORM S00200-INIZIA-AREE-TAB
        //              THRU S00200-INIZIA-AREE-TAB-EX.
        s00200IniziaAreeTab();
        //
        // COB_CODE: PERFORM S00300-CTRL-INPUT
        //              THRU S00300-CTRL-INPUT-EX.
        s00300CtrlInput();
    }

    /**Original name: S00100-INIZIALIZZA-WORK<br>
	 * <pre>----------------------------------------------------------------*
	 *  INIZIALIZZAZIONE AREE WS
	 * ----------------------------------------------------------------*</pre>*/
    private void s00100InizializzaWork() {
        // COB_CODE: MOVE 'S00100-INIZIALIZZA-WORK'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S00100-INIZIALIZZA-WORK");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // COB_CODE: INITIALIZE IX-INDICI.
        initIxIndici();
        //
        // COB_CODE: ACCEPT WK-CURRENT-DATE
        //             FROM DATE YYYYMMDD.
        ws.setWkCurrentDateFormatted(CalendarUtil.getDateYYYYMMDD());
    }

    /**Original name: S00200-INIZIA-AREE-TAB<br>
	 * <pre>----------------------------------------------------------------*
	 *  INIZIALIZZAZIONE AREE TABELLE
	 * ----------------------------------------------------------------*</pre>*/
    private void s00200IniziaAreeTab() {
        // COB_CODE: MOVE 'S00200-INIZIA-AREE-TAB'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S00200-INIZIA-AREE-TAB");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // --> Inizializzazione Area Parametro Oggetto
        //
        // COB_CODE: PERFORM INIZIA-TOT-POG
        //              THRU INIZIA-TOT-POG-EX
        //           VARYING IX-TAB-POG FROM 1 BY 1
        //             UNTIL IX-TAB-POG > WK-POG-MAX-A.
        ws.getIxIndici().setTabPog(((short)1));
        while (!(ws.getIxIndici().getTabPog() > ws.getLccvpogz().getMaxA())) {
            iniziaTotPog();
            ws.getIxIndici().setTabPog(Trunc.toShort(ws.getIxIndici().getTabPog() + 1, 4));
        }
        //
        // COB_CODE: MOVE ZEROES
        //             TO WPOG-ELE-PARAM-OGG-MAX
        //                IX-TAB-POG.
        ws.setWpogEleParamOggMax(((short)0));
        ws.getIxIndici().setTabPog(((short)0));
    }

    /**Original name: S00300-CTRL-INPUT<br>
	 * <pre>----------------------------------------------------------------*
	 *  CONTROLLO DATI DI INPUT
	 * ----------------------------------------------------------------*</pre>*/
    private void s00300CtrlInput() {
        // COB_CODE: MOVE 'S00300-CTRL-INPUT'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S00300-CTRL-INPUT");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO
        //             TO WS-MOVIMENTO.
        ws.getWsMovimento().setWsMovimentoFormatted(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimentoFormatted());
        //
        // COB_CODE:      IF IDSV0001-TIPO-MOVIMENTO NOT = 6006 AND
        //                                           NOT = 6024 AND
        //                                           NOT = 6025 AND
        //                                           NOT = 6026
        //           *
        //                      THRU EX-S0300
        //           *
        //                END-IF.
        if (areaIdsv0001.getAreaComune().getIdsv0001TipoMovimento() != 6006 && areaIdsv0001.getAreaComune().getIdsv0001TipoMovimento() != 6024 && areaIdsv0001.getAreaComune().getIdsv0001TipoMovimento() != 6025 && areaIdsv0001.getAreaComune().getIdsv0001TipoMovimento() != 6026) {
            //
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL-ERR
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
            // COB_CODE: MOVE '001114'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("001114");
            // COB_CODE: MOVE 'TIPO MOVIMENTO ERRATO'
            //             TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("TIPO MOVIMENTO ERRATO");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
            //
        }
        //
        // COB_CODE:      IF WMOV-ELE-MOVI-MAX = 0
        //           *
        //                      THRU EX-S0300
        //           *
        //                END-IF.
        if (wmovAreaMovimento.getWmovEleMovMax() == 0) {
            //
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL-ERR
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
            // COB_CODE: MOVE '001114'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("001114");
            // COB_CODE: MOVE 'OCCORRENZA DI MOVIMENTO NON VALORIZZATA'
            //             TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("OCCORRENZA DI MOVIMENTO NON VALORIZZATA");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
            //
        }
        //
        // COB_CODE:      IF WPOL-ELE-POLI-MAX = 0
        //           *
        //                      THRU EX-S0300
        //           *
        //                END-IF.
        if (wpolAreaPolizza.getWpolElePoliMax() == 0) {
            //
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL-ERR
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
            // COB_CODE: MOVE '001114'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("001114");
            // COB_CODE: MOVE 'OCCORRENZA DI POLIZZA NON VALORIZZATA'
            //             TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("OCCORRENZA DI POLIZZA NON VALORIZZATA");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
            //
        }
        //
        // COB_CODE:      IF WADE-ELE-ADES-MAX = 0
        //           *
        //                      THRU EX-S0300
        //           *
        //                END-IF.
        if (wadeAreaAdesione.getEleAdesMax() == 0) {
            //
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL-ERR
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
            // COB_CODE: MOVE '001114'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("001114");
            // COB_CODE: MOVE 'OCCORRENZA DI ADESIONE NON VALORIZZATA'
            //             TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("OCCORRENZA DI ADESIONE NON VALORIZZATA");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
            //
        }
        //
        // COB_CODE:      IF WGRZ-ELE-GAR-MAX = 0
        //           *
        //                      THRU EX-S0300
        //           *
        //                END-IF.
        if (wgrzAreaGaranzia.getEleGarMax() == 0) {
            //
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL-ERR
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
            // COB_CODE: MOVE '001114'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("001114");
            // COB_CODE: MOVE 'NESSUNA OCCORRENZA DI GARANZIA ELABORABILE'
            //             TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("NESSUNA OCCORRENZA DI GARANZIA ELABORABILE");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
            //
        }
        //
        // COB_CODE:      IF WTGA-ELE-TRAN-MAX = 0
        //           *
        //                      THRU EX-S0300
        //           *
        //                END-IF.
        if (wtgaAreaTranche.getEleTranMax() == 0) {
            //
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL-ERR
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
            // COB_CODE: MOVE '001114'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("001114");
            // COB_CODE: MOVE 'NESSUNA OCCORRENZA DI TRANCHE ELABORABILE'
            //             TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("NESSUNA OCCORRENZA DI TRANCHE ELABORABILE");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
            //
        }
        // COB_CODE:      IF WSKD-ELE-LIVELLO-MAX-P = 0
        //           *
        //                      THRU EX-S0300
        //           *
        //                END-IF.
        if (wskdAreaScheda.getWskdEleLivelloMaxP() == 0) {
            //
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL-ERR
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
            // COB_CODE: MOVE '001114'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("001114");
            // COB_CODE: MOVE 'AREA VARIABILI PROD NON VALORIZZATA'
            //             TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("AREA VARIABILI PROD NON VALORIZZATA");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
            //
        }
        //
        // COB_CODE:      IF WSKD-ELE-LIVELLO-MAX-T = 0
        //           *
        //                      THRU EX-S0300
        //           *
        //                END-IF.
        if (wskdAreaScheda.getWskdEleLivelloMaxT() == 0) {
            //
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL-ERR
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
            // COB_CODE: MOVE '001114'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("001114");
            // COB_CODE: MOVE 'AREA VARIABILI GAR NON VALORIZZATA'
            //             TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("AREA VARIABILI GAR NON VALORIZZATA");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
            //
        }
        //
        // COB_CODE:      IF NOT W800-CON-CALCOLO   AND
        //                   NOT W800-SENZA-CALCOLO
        //           *
        //                      THRU EX-S0300
        //           *
        //                END-IF.
        if (!w800AreaPag.getModChiamataServizio().isConCalcolo() && !w800AreaPag.getModChiamataServizio().isSenzaCalcolo()) {
            //
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL-ERR
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
            // COB_CODE: MOVE '001114'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("001114");
            // COB_CODE: MOVE 'FLAG CHIAMATA SERVIZIO NON VALORIZAZTO'
            //             TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("FLAG CHIAMATA SERVIZIO NON VALORIZAZTO");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
            //
        }
        //
        // COB_CODE:      IF W660-MANFEE-CONT-SI
        //           *
        //                   END-IF
        //           *
        //                END-IF.
        if (w660AreaPag.getDatiManfeeContesto().isSi()) {
            //
            // COB_CODE:         IF W660-PERMANFEE NOT = 1 AND
            //                                     NOT = 2 AND
            //                                     NOT = 3 AND
            //                                     NOT = 4 AND
            //                                     NOT = 5 AND
            //                                     NOT = 12
            //           *
            //                         THRU EX-S0300
            //           *
            //                   END-IF
            if (w660AreaPag.getPermanfee() != 1 && w660AreaPag.getPermanfee() != 2 && w660AreaPag.getPermanfee() != 3 && w660AreaPag.getPermanfee() != 4 && w660AreaPag.getPermanfee() != 5 && w660AreaPag.getPermanfee() != 12) {
                //
                // COB_CODE: MOVE WK-PGM
                //             TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE WK-LABEL-ERR
                //             TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
                // COB_CODE: MOVE '001114'
                //             TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("001114");
                // COB_CODE: MOVE 'VALORE PERMANFEE NON PREVISTO NEL DOMINIO'
                //             TO IEAI9901-PARAMETRI-ERR
                ws.getIeai9901Area().setParametriErr("VALORE PERMANFEE NON PREVISTO NEL DOMINIO");
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
                //
            }
            //
        }
    }

    /**Original name: S10000-ELABORAZIONE<br>
	 * <pre> ============================================================== *
	 *  -->               E L A B O R A Z I O N E                  <-- *
	 *  ============================================================== *</pre>*/
    private void s10000Elaborazione() {
        // COB_CODE: MOVE 'S10000-ELABORAZIONE'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S10000-ELABORAZIONE");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // COB_CODE:      IF W800-CON-CALCOLO
        //           *
        //                      THRU S11000-GESTIONE-ISPS0211-EX
        //           *
        //                END-IF.
        if (w800AreaPag.getModChiamataServizio().isConCalcolo()) {
            //
            // COB_CODE: PERFORM S11000-GESTIONE-ISPS0211
            //              THRU S11000-GESTIONE-ISPS0211-EX
            s11000GestioneIsps0211();
            //
        }
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                   END-IF
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE:         IF W800-CON-CALCOLO  AND
            //                      WK-MANFEE-NO
            //           *
            //                         THRU EX-S0300
            //           *
            //                   ELSE
            //           *
            //                         THRU S12000-GESTIONE-MANFEE-EX
            //           *
            //                   END-IF
            if (w800AreaPag.getModChiamataServizio().isConCalcolo() && ws.getWkManfee().isNo()) {
                //
                // COB_CODE: MOVE WK-PGM
                //             TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE WK-LABEL-ERR
                //             TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
                // COB_CODE: MOVE '001114'
                //             TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("001114");
                // COB_CODE: MOVE 'IMPOSSIBILE EFETTUARE CALCOLO MF'
                //             TO IEAI9901-PARAMETRI-ERR
                ws.getIeai9901Area().setParametriErr("IMPOSSIBILE EFETTUARE CALCOLO MF");
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
                //
            }
            else {
                //
                // COB_CODE: PERFORM S12000-GESTIONE-MANFEE
                //              THRU S12000-GESTIONE-MANFEE-EX
                s12000GestioneManfee();
                //
            }
            //
        }
    }

    /**Original name: S11000-GESTIONE-ISPS0211<br>
	 * <pre>----------------------------------------------------------------*
	 *  GESTIONE CHIAMATA AL SERVIZIO CALC. NOTEVOLI E CONTR. DI LIQ.
	 * ----------------------------------------------------------------*</pre>*/
    private void s11000GestioneIsps0211() {
        // COB_CODE: MOVE 'S11000-GESTIONE-ISPS0211'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S11000-GESTIONE-ISPS0211");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // COB_CODE: SET WK-MANFEE-NO
        //             TO TRUE.
        ws.getWkManfee().setNo();
        //
        // COB_CODE: PERFORM S11100-PREPARA-ISPS0211
        //              THRU S11100-PREPARA-ISPS0211-EX.
        s11100PreparaIsps0211();
        //
        // COB_CODE: PERFORM S11200-CALL-ISPS0211
        //              THRU S11200-CALL-ISPS0211-EX.
        s11200CallIsps0211();
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                        OR IDSV0001-ESITO-KO
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE: PERFORM S11300-SCORRI-OUT-ISPS0211
            //              THRU S11300-SCORRI-OUT-ISPS0211-EX
            //           VARYING IX-AREA-ISPC0211 FROM 1 BY 1
            //             UNTIL IX-AREA-ISPC0211 >
            //                   ISPC0211-ELE-MAX-SCHEDA-T
            //                OR IDSV0001-ESITO-KO
            ws.getIxIndici().setAreaIspc0211(((short)1));
            while (!(ws.getIxIndici().getAreaIspc0211() > ws.getAreaIoIspc0211().getIspc0211EleMaxSchedaT() || areaIdsv0001.getEsito().isIdsv0001EsitoKo())) {
                s11300ScorriOutIsps0211();
                ws.getIxIndici().setAreaIspc0211(Trunc.toShort(ws.getIxIndici().getAreaIspc0211() + 1, 4));
            }
            //
        }
    }

    /**Original name: S11100-PREPARA-ISPS0211<br>
	 * <pre>----------------------------------------------------------------*
	 *  VALORIZZAZIONE DELL'INPUT DEL SERVIZIO ISPS0211
	 * ----------------------------------------------------------------*</pre>*/
    private void s11100PreparaIsps0211() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'S11100-PREPARA-ISPS0211'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S11100-PREPARA-ISPS0211");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // COB_CODE: SET  IDSV8888-BUSINESS-DBG      TO TRUE
        ws.getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
        // COB_CODE: MOVE 'LOAS0800'                 TO IDSV8888-NOME-PGM.
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("LOAS0800");
        // COB_CODE: SET  IDSV8888-INIZIO            TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setInizio();
        // COB_CODE: MOVE 'ISPC0211'                 TO IDSV8888-DESC-PGM
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("ISPC0211");
        // COB_CODE: STRING 'Iniz.tab. work ISPC0211'
        //                   DELIMITED BY SIZE
        //                   INTO IDSV8888-DESC-PGM
        //           END-STRING
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("Iniz.tab. work ISPC0211");
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX
        eseguiDisplay();
        //
        // COB_CODE: INITIALIZE ISPC0211-DATI-INPUT
        //                      ISPC0211-AREA-ERRORI
        //                      ISPC0211-TAB-SCHEDA-P(1)
        //                      ISPC0211-TAB-SCHEDA-T(1)
        initIspc0211DatiInput();
        initIspc0211AreaErrori();
        initIspc0211TabSchedaP();
        initIspc0211TabSchedaT();
        // COB_CODE: MOVE ISPC0211-TAB-VAL-P   TO  ISPC0211-RESTO-TAB-SCHEDA-P.
        ws.getAreaIoIspc0211().getIspc0211TabValP().setIspc0211RestoTabSchedaP(ws.getAreaIoIspc0211().getIspc0211TabValP().getIspc0211TabValPFormatted());
        // COB_CODE: MOVE ISPC0211-TAB-VAL-T   TO  ISPC0211-RESTO-TAB-SCHEDA-T.
        ws.getAreaIoIspc0211().getIspc0211TabValT().setIspc0211RestoTabSchedaT(ws.getAreaIoIspc0211().getIspc0211TabValT().getIspc0211TabValTFormatted());
        //
        // COB_CODE: SET  IDSV8888-BUSINESS-DBG      TO TRUE
        ws.getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
        // COB_CODE: MOVE 'LOAS0800'                 TO IDSV8888-NOME-PGM.
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("LOAS0800");
        // COB_CODE: SET  IDSV8888-FINE              TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setFine();
        // COB_CODE: MOVE 'ISPC0211'                 TO IDSV8888-DESC-PGM
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("ISPC0211");
        // COB_CODE: STRING 'Iniz.tab. work ISPC0211'
        //                   DELIMITED BY SIZE
        //                   INTO IDSV8888-DESC-PGM
        //           END-STRING
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("Iniz.tab. work ISPC0211");
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX
        eseguiDisplay();
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //             TO ISPC0211-COD-COMPAGNIA.
        ws.getAreaIoIspc0211().getIspc0211DatiInput().setIspc0211CodCompagniaFormatted(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAniaFormatted());
        //
        // COB_CODE: MOVE WPOL-COD-PROD
        //             TO ISPC0211-COD-PRODOTTO.
        ws.getAreaIoIspc0211().getIspc0211DatiInput().setCodProdotto(wpolAreaPolizza.getLccvpol1().getDati().getWpolCodProd());
        // COB_CODE: MOVE WSKD-DEE                  TO ISPC0211-DEE
        ws.getAreaIoIspc0211().getIspc0211DatiInput().setDee(wskdAreaScheda.getWskdDee());
        // COB_CODE: IF WPOL-IB-OGG-NULL = HIGH-VALUES
        //                TO ISPC0211-NUM-POLIZZA
        //           ELSE
        //                TO ISPC0211-NUM-POLIZZA
        //           END-IF.
        if (Characters.EQ_HIGH.test(wpolAreaPolizza.getLccvpol1().getDati().getWpolIbOgg(), WpolDati.Len.WPOL_IB_OGG)) {
            // COB_CODE: MOVE SPACES
            //             TO ISPC0211-NUM-POLIZZA
            ws.getAreaIoIspc0211().getIspc0211DatiInput().setNumPolizza("");
        }
        else {
            // COB_CODE: MOVE WPOL-IB-OGG
            //             TO ISPC0211-NUM-POLIZZA
            ws.getAreaIoIspc0211().getIspc0211DatiInput().setNumPolizza(wpolAreaPolizza.getLccvpol1().getDati().getWpolIbOgg());
        }
        //
        // COB_CODE:      IF WPOL-COD-CONV-NULL = HIGH-VALUES
        //           *
        //                     TO ISPC0211-COD-CONVENZIONE
        //           *
        //                ELSE
        //           *
        //                     TO ISPC0211-COD-CONVENZIONE
        //           *
        //                END-IF.
        if (Characters.EQ_HIGH.test(wpolAreaPolizza.getLccvpol1().getDati().getWpolCodConvFormatted())) {
            //
            // COB_CODE: MOVE SPACES
            //             TO ISPC0211-COD-CONVENZIONE
            ws.getAreaIoIspc0211().getIspc0211DatiInput().setCodConvenzione("");
            //
        }
        else {
            //
            // COB_CODE: MOVE WPOL-COD-CONV
            //             TO ISPC0211-COD-CONVENZIONE
            ws.getAreaIoIspc0211().getIspc0211DatiInput().setCodConvenzione(wpolAreaPolizza.getLccvpol1().getDati().getWpolCodConv());
            //
        }
        //
        // COB_CODE:      IF WPOL-DT-INI-VLDT-CONV-NULL = HIGH-VALUE
        //           *
        //                     TO ISPC0211-DATA-INIZ-VALID-CONV
        //           *
        //                ELSE
        //           *
        //                     TO ISPC0211-DATA-INIZ-VALID-CONV
        //           *
        //                END-IF.
        if (Characters.EQ_HIGH.test(wpolAreaPolizza.getLccvpol1().getDati().getWpolDtIniVldtConv().getWpolDtIniVldtConvNullFormatted())) {
            //
            // COB_CODE: MOVE SPACES
            //             TO ISPC0211-DATA-INIZ-VALID-CONV
            ws.getAreaIoIspc0211().getIspc0211DatiInput().setDataInizValidConv("");
            //
        }
        else {
            //
            // COB_CODE: MOVE WPOL-DT-INI-VLDT-CONV
            //             TO ISPC0211-DATA-INIZ-VALID-CONV
            ws.getAreaIoIspc0211().getIspc0211DatiInput().setDataInizValidConv(wpolAreaPolizza.getLccvpol1().getDati().getWpolDtIniVldtConv().getWpolDtIniVldtConvFormatted());
            //
        }
        //
        // COB_CODE: MOVE WCOM-DT-ULT-VERS-PROD
        //             TO ISPC0211-DATA-RIFERIMENTO.
        ws.getAreaIoIspc0211().getIspc0211DatiInput().setDataRiferimento(wcomIoStati.getLccc0001().getWcomDtUltVersProdFormatted());
        //
        // COB_CODE: MOVE WPOL-DT-DECOR
        //             TO ISPC0211-DATA-DECORR-POLIZZA.
        ws.getAreaIoIspc0211().getIspc0211DatiInput().setDataDecorrPolizza(wpolAreaPolizza.getLccvpol1().getDati().getWpolDtDecorFormatted());
        //
        // COB_CODE: MOVE WCOM-COD-LIV-AUT-PROFIL
        //             TO ISPC0211-LIVELLO-UTENTE.
        ws.getAreaIoIspc0211().getIspc0211DatiInput().setIspc0211LivelloUtente(TruncAbs.toShort(wcomIoStati.getLccc0001().getDatiDeroghe().getCodLivAutProfil(), 2));
        //
        // COB_CODE: MOVE IDSV0001-SESSIONE
        //             TO ISPC0211-SESSION-ID.
        ws.getAreaIoIspc0211().getIspc0211DatiInput().setSessionId(areaIdsv0001.getAreaComune().getSessione());
        //
        // COB_CODE: MOVE 'N'
        //             TO ISPC0211-FLG-REC-PROV.
        ws.getAreaIoIspc0211().getIspc0211DatiInput().setIspc0211FlgRecProvFormatted("N");
        //
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO
        //             TO ISPC0211-FUNZIONALITA.
        ws.getAreaIoIspc0211().getIspc0211DatiInput().setIspc0211FunzionalitaFormatted(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimentoFormatted());
        //
        // --> Valorizzazione occurs area input servizio calcoli e
        // --> controlli con l'output del valorizzatore variabili
        //
        // COB_CODE: MOVE WSKD-ELE-LIVELLO-MAX-P
        //             TO ISPC0211-ELE-MAX-SCHEDA-P.
        ws.getAreaIoIspc0211().setIspc0211EleMaxSchedaP(TruncAbs.toShort(wskdAreaScheda.getWskdEleLivelloMaxP(), 4));
        // COB_CODE: MOVE WSKD-ELE-LIVELLO-MAX-T
        //             TO ISPC0211-ELE-MAX-SCHEDA-T.
        ws.getAreaIoIspc0211().setIspc0211EleMaxSchedaT(TruncAbs.toShort(wskdAreaScheda.getWskdEleLivelloMaxT(), 4));
        //
        // COB_CODE: PERFORM VAL-SCHEDE-ISPC0211
        //              THRU VAL-SCHEDE-ISPC0211-EX.
        valSchedeIspc0211();
    }

    /**Original name: S11200-CALL-ISPS0211<br>
	 * <pre>----------------------------------------------------------------*
	 *  CHIAMATA AL SERVIZIO - ISPS0211 -
	 * ----------------------------------------------------------------*</pre>*/
    private void s11200CallIsps0211() {
        Isps0211 isps0211 = null;
        // COB_CODE: MOVE 'S11200-CALL-ISPS0211'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S11200-CALL-ISPS0211");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // COB_CODE:      CALL ISPS0211          USING AREA-IDSV0001
        //                                             WCOM-AREA-STATI
        //                                             AREA-IO-ISPC0211
        //                ON EXCEPTION
        //           *
        //                      THRU EX-S0290
        //           *
        //                END-CALL.
        try {
            isps0211 = Isps0211.getInstance();
            isps0211.run(areaIdsv0001, wcomIoStati, ws.getAreaIoIspc0211());
        }
        catch (ProgramExecutionException __ex) {
            //
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'ERRORE CHIAMATA ISPS0211'
            //             TO CALL-DESC
            ws.getIdsv0002().setCallDesc("ERRORE CHIAMATA ISPS0211");
            // COB_CODE: MOVE WK-LABEL-ERR
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
            // COB_CODE: PERFORM S0290-ERRORE-DI-SISTEMA
            //              THRU EX-S0290
            s0290ErroreDiSistema();
            //
        }
    }

    /**Original name: S11300-SCORRI-OUT-ISPS0211<br>
	 * <pre>----------------------------------------------------------------*
	 *      GESTIONE DELL'OUTPUT DEL SERVIZIO DI PRODOTTO ISPS0211     *
	 * ----------------------------------------------------------------*</pre>*/
    private void s11300ScorriOutIsps0211() {
        // COB_CODE: MOVE 'S11300-SCORRI-OUT-ISPS0211'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S11300-SCORRI-OUT-ISPS0211");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // COB_CODE:      IF ISPC0211-TIPO-LIVELLO-T(IX-AREA-ISPC0211) = 'G' AND
        //                   ISPC0211-IDENT-LIVELLO-T(IX-AREA-ISPC0211) >  0  AND
        //                   ISPC0211-FLG-LIQ-T(IX-AREA-ISPC0211) = 'NO'
        //           *
        //                      THRU S11310-ALLINEA-AREA-TRANCHE-EX
        //           *
        //                END-IF.
        if (ws.getAreaIoIspc0211().getIspc0211TabValT().getIspc0211TipoLivelloT(ws.getIxIndici().getAreaIspc0211()) == 'G' && ws.getAreaIoIspc0211().getIspc0211TabValT().getIspc0211IdentLivelloT(ws.getIxIndici().getAreaIspc0211()) > 0 && Conditions.eq(ws.getAreaIoIspc0211().getIspc0211TabValT().getIspc0211FlgLiqT(ws.getIxIndici().getAreaIspc0211()), "NO")) {
            //
            // COB_CODE: PERFORM S11310-ALLINEA-AREA-TRANCHE
            //              THRU S11310-ALLINEA-AREA-TRANCHE-EX
            s11310AllineaAreaTranche();
            //
        }
    }

    /**Original name: S11310-ALLINEA-AREA-TRANCHE<br>
	 * <pre>----------------------------------------------------------------*
	 *  GESTIONE DELL'OUTPUT DEL SERVIZIO ISPS0211
	 * ----------------------------------------------------------------*</pre>*/
    private void s11310AllineaAreaTranche() {
        // COB_CODE: MOVE 'S11310-ALLINEA-AREA-TRANCHE'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S11310-ALLINEA-AREA-TRANCHE");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // --> Gestione Tranche Di Granzia
        //
        // COB_CODE:      PERFORM VARYING IX-TAB-TGA FROM 1 BY 1
        //                  UNTIL IX-TAB-TGA > WTGA-ELE-TRAN-MAX
        //                     OR IDSV0001-ESITO-KO
        //           *
        //                    END-IF
        //           *
        //                END-PERFORM.
        ws.getIxIndici().setTabTga(((short)1));
        while (!(ws.getIxIndici().getTabTga() > wtgaAreaTranche.getEleTranMax() || areaIdsv0001.getEsito().isIdsv0001EsitoKo())) {
            //
            // COB_CODE:          IF WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA) =
            //                       ISPC0211-IDENT-LIVELLO-T(IX-AREA-ISPC0211)
            //           *
            //                          THRU S11320-GESTIONE-LIV-GAR-EX
            //           *
            //                    END-IF
            if (wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaIdTrchDiGar() == ws.getAreaIoIspc0211().getIspc0211TabValT().getIspc0211IdentLivelloT(ws.getIxIndici().getAreaIspc0211())) {
                //
                // COB_CODE: PERFORM S11320-GESTIONE-LIV-GAR
                //              THRU S11320-GESTIONE-LIV-GAR-EX
                s11320GestioneLivGar();
                //
            }
            //
            ws.getIxIndici().setTabTga(Trunc.toShort(ws.getIxIndici().getTabTga() + 1, 4));
        }
    }

    /**Original name: S11320-GESTIONE-LIV-GAR<br>
	 * <pre>----------------------------------------------------------------*
	 *          GESTIONE DEL LIVELLO DI GARANZIA                       *
	 * ----------------------------------------------------------------*</pre>*/
    private void s11320GestioneLivGar() {
        // COB_CODE: MOVE 'S11320-GESTIONE-LIV-GAR'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S11320-GESTIONE-LIV-GAR");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // COB_CODE: PERFORM S11330-CONTR-LIV-GAR
        //              THRU S11330-CONTR-LIV-GAR-EX
        //           VARYING IX-COMP-ISPC0211 FROM 1 BY 1
        //             UNTIL IX-COMP-ISPC0211 >
        //                   ISPC0211-NUM-COMPON-MAX-ELE-T(IX-AREA-ISPC0211)
        //                OR IDSV0001-ESITO-KO.
        ws.getIxIndici().setCompIspc0211(((short)1));
        while (!(ws.getIxIndici().getCompIspc0211() > ws.getAreaIoIspc0211().getIspc0211TabValT().getIspc0211NumComponMaxEleT(ws.getIxIndici().getAreaIspc0211()) || areaIdsv0001.getEsito().isIdsv0001EsitoKo())) {
            s11330ContrLivGar();
            ws.getIxIndici().setCompIspc0211(Trunc.toShort(ws.getIxIndici().getCompIspc0211() + 1, 4));
        }
    }

    /**Original name: S11330-CONTR-LIV-GAR<br>
	 * <pre>----------------------------------------------------------------*
	 *          TEST SUI COMPONENTI DELLA TABELLA TGA
	 * ----------------------------------------------------------------*</pre>*/
    private void s11330ContrLivGar() {
        // COB_CODE: MOVE 'S11330-CONTR-LIV-GAR'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S11330-CONTR-LIV-GAR");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                   END-IF
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE: MOVE ISPC0211-TIPO-DATO-T
            //                (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
            //             TO WS-TP-DATO
            ws.getWsTpDato().setWsTpDato(ws.getAreaIoIspc0211().getIspc0211TabValT().getIspc0211TipoDatoT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()));
            //
            // COB_CODE:         IF ISPC0211-CODICE-VARIABILE-T
            //                     (IX-AREA-ISPC0211, IX-COMP-ISPC0211) = 'MANFEE'
            //           *
            //                      END-EVALUATE
            //           *
            //                   END-IF
            if (Conditions.eq(ws.getAreaIoIspc0211().getIspc0211TabValT().getIspc0211CodiceVariabileT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), "MANFEE")) {
                //
                //
                // COB_CODE:            EVALUATE TRUE
                //           *
                //                        WHEN TD-IMPORTO
                //           *
                //                          END-IF
                //           *
                //                        WHEN OTHER
                //           *
                //                             THRU EX-S0300
                //           *
                //                      END-EVALUATE
                switch (ws.getWsTpDato().getWsTpDato()) {

                    case WsTpDato.IMPORTO://
                        // COB_CODE:                IF ISPC0211-VALORE-IMP-T
                        //                            (IX-AREA-ISPC0211, IX-COMP-ISPC0211) > 0
                        //           *
                        //                               TO TRUE
                        //           *
                        //                          END-IF
                        if (ws.getAreaIoIspc0211().getIspc0211TabValT().getIspc0211ValoreImpT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()).compareTo(0) > 0) {
                            //
                            // COB_CODE:                   IF IDSV0001-TIPO-MOVIMENTO = 6024
                            //           *
                            //                                  TO WTGA-TP-MANFEE-APPL(IX-TAB-TGA)
                            //           *
                            //                             ELSE
                            //           *
                            //                                END-IF
                            //           *
                            //                             END-IF
                            if (areaIdsv0001.getAreaComune().getIdsv0001TipoMovimento() == 6024) {
                                //
                                // COB_CODE: MOVE ISPC0211-VALORE-IMP-T
                                //               (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                                //             TO WTGA-MANFEE-RICOR(IX-TAB-TGA)
                                wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaManfeeRicor().setWtgaManfeeRicor(Trunc.toDecimal(ws.getAreaIoIspc0211().getIspc0211TabValT().getIspc0211ValoreImpT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), 15, 3));
                                //
                                // COB_CODE: MOVE 'RI'
                                //             TO WTGA-TP-MANFEE-APPL(IX-TAB-TGA)
                                wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().setWtgaTpManfeeAppl("RI");
                                //
                            }
                            else {
                                //
                                // COB_CODE: MOVE ISPC0211-VALORE-IMP-T
                                //               (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
                                //             TO WTGA-MANFEE-ANTIC(IX-TAB-TGA)
                                wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaManfeeAntic().setWtgaManfeeAntic(Trunc.toDecimal(ws.getAreaIoIspc0211().getIspc0211TabValT().getIspc0211ValoreImpT(ws.getIxIndici().getAreaIspc0211(), ws.getIxIndici().getCompIspc0211()), 15, 3));
                                //
                                // COB_CODE:                      IF IDSV0001-TIPO-MOVIMENTO = 6025
                                //           *
                                //                                     TO WTGA-TP-MANFEE-APPL(IX-TAB-TGA)
                                //           *
                                //                                ELSE
                                //           *
                                //                                     TO WTGA-TP-MANFEE-APPL(IX-TAB-TGA)
                                //           *
                                //                                END-IF
                                if (areaIdsv0001.getAreaComune().getIdsv0001TipoMovimento() == 6025) {
                                    //
                                    // COB_CODE: MOVE 'C1'
                                    //             TO WTGA-TP-MANFEE-APPL(IX-TAB-TGA)
                                    wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().setWtgaTpManfeeAppl("C1");
                                    //
                                }
                                else {
                                    //
                                    // COB_CODE: MOVE 'C2'
                                    //             TO WTGA-TP-MANFEE-APPL(IX-TAB-TGA)
                                    wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().setWtgaTpManfeeAppl("C2");
                                    //
                                }
                                //
                            }
                            //
                            // COB_CODE: SET  WTGA-ST-MOD(IX-TAB-TGA)
                            //             TO TRUE
                            wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getStatus().setMod();
                            //
                            // COB_CODE: SET WK-MANFEE-YES
                            //             TO TRUE
                            ws.getWkManfee().setYes();
                            //
                        }
                        //
                        break;

                    default://
                        // COB_CODE: MOVE WK-PGM
                        //             TO IEAI9901-COD-SERVIZIO-BE
                        ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                        // COB_CODE: MOVE WK-LABEL-ERR
                        //             TO IEAI9901-LABEL-ERR
                        ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
                        // COB_CODE: MOVE '001114'
                        //             TO IEAI9901-COD-ERRORE
                        ws.getIeai9901Area().setCodErroreFormatted("001114");
                        // COB_CODE: MOVE 'ERRORE TIPO COMP. MANFEE ISPS0211'
                        //             TO IEAI9901-PARAMETRI-ERR
                        ws.getIeai9901Area().setParametriErr("ERRORE TIPO COMP. MANFEE ISPS0211");
                        // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        //              THRU EX-S0300
                        s0300RicercaGravitaErrore();
                        //
                        break;
                }
                //
            }
            //
        }
    }

    /**Original name: S12000-GESTIONE-MANFEE<br>
	 * <pre>----------------------------------------------------------------*
	 *                GESTIONE DEL MANAGEMENT FEE                      *
	 * ----------------------------------------------------------------*</pre>*/
    private void s12000GestioneManfee() {
        // COB_CODE: MOVE 'S12000-GESTIONE-MANFEE'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S12000-GESTIONE-MANFEE");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                   END-IF
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE:         IF W660-MANFEE-CONT-SI
            //           *
            //                        TO WK-DECADELMFEE
            //           *
            //                   ELSE
            //           *
            //                         THRU S12100-PARAMETRI-MANFEE-EX
            //           *
            //                   END-IF
            if (w660AreaPag.getDatiManfeeContesto().isSi()) {
                //
                // COB_CODE: MOVE W660-PERMANFEE
                //             TO WK-PERMANFEE
                ws.setWkPermanfeeFormatted(w660AreaPag.getPermanfeeFormatted());
                //
                // COB_CODE: MOVE W660-MESIDIFFMFEE
                //             TO WK-MESIDIFFMFEE
                ws.setWkMesidiffmfeeFormatted(w660AreaPag.getMesidiffmfeeFormatted());
                //
                // COB_CODE: MOVE W660-DECADELMFEE
                //             TO WK-DECADELMFEE
                ws.setWkDecadelmfeeFormatted(w660AreaPag.getDecadelmfeeFormatted());
                //
            }
            else {
                //
                // COB_CODE: PERFORM S12100-PARAMETRI-MANFEE
                //              THRU S12100-PARAMETRI-MANFEE-EX
                s12100ParametriManfee();
                //
            }
            //
        }
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                      THRU S12200-CALCOLA-DATE-MANFEE-EX
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE: PERFORM S12200-CALCOLA-DATE-MANFEE
            //              THRU S12200-CALCOLA-DATE-MANFEE-EX
            s12200CalcolaDateManfee();
            //
        }
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                      THRU S12300-CALCOLA-RATA-MANFEE-EX
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE: PERFORM S12300-CALCOLA-RATA-MANFEE
            //              THRU S12300-CALCOLA-RATA-MANFEE-EX
            s12300CalcolaRataManfee();
            //
        }
    }

    /**Original name: S12100-PARAMETRI-MANFEE<br>
	 * <pre>----------------------------------------------------------------*
	 *  ACQUISIZIONE DEL PARAMETRO PERMANFEE
	 * ----------------------------------------------------------------*</pre>*/
    private void s12100ParametriManfee() {
        // COB_CODE: MOVE 'S12100-PARAMETRI-MANFEE'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S12100-PARAMETRI-MANFEE");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // COB_CODE:      IF IDSV0001-TIPO-MOVIMENTO = 6006 OR 6024
        //           *
        //                      THRU S12110-CERCA-PERMANFEE-EX
        //           *
        //                END-IF.
        if (areaIdsv0001.getAreaComune().getIdsv0001TipoMovimento() == 6006 || areaIdsv0001.getAreaComune().getIdsv0001TipoMovimento() == 6024) {
            //
            // COB_CODE: PERFORM S12110-CERCA-PERMANFEE
            //              THRU S12110-CERCA-PERMANFEE-EX
            s12110CercaPermanfee();
            //
        }
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                      THRU S12120-CERCA-MESIDIFFMFEE-EX
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE: PERFORM S12120-CERCA-MESIDIFFMFEE
            //              THRU S12120-CERCA-MESIDIFFMFEE-EX
            s12120CercaMesidiffmfee();
            //
        }
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                      THRU S12130-CERCA-DECADELMFEE-EX
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE: PERFORM S12130-CERCA-DECADELMFEE
            //              THRU S12130-CERCA-DECADELMFEE-EX
            s12130CercaDecadelmfee();
            //
        }
    }

    /**Original name: S12110-CERCA-PERMANFEE<br>
	 * <pre>----------------------------------------------------------------*
	 *  ACQUISIZIONE DEL PARAMETRO PERMANFEE
	 * ----------------------------------------------------------------*</pre>*/
    private void s12110CercaPermanfee() {
        // COB_CODE: MOVE 'S12110-CERCA-PERMANFEE'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S12110-CERCA-PERMANFEE");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // COB_CODE: MOVE 'PERMANFEE'
        //             TO LDBV1131-COD-PARAM.
        ws.getAreaLdbv1131().setCodParam("PERMANFEE");
        //
        // COB_CODE: PERFORM S12190-IMPOSTA-PARAM-OGG
        //              THRU S12190-IMPOSTA-PARAM-OGG-EX.
        s12190ImpostaParamOgg();
        //
        // COB_CODE: PERFORM S12199-LEGGI-PARAM-OGG
        //              THRU S12199-LEGGI-PARAM-OGG-EX.
        s12199LeggiParamOgg();
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                   END-IF
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE:         IF WPOG-VAL-NUM-NULL(IX-TAB-POG) NOT = HIGH-VALUES
            //           *
            //                        TO WK-PERMANFEE
            //           *
            //                   ELSE
            //           *
            //                         THRU EX-S0300
            //           *
            //                   END-IF
            if (!Characters.EQ_HIGH.test(ws.getWpogTabParamOgg(ws.getIxIndici().getTabPog()).getLccvpog1().getDati().getWpogValNum().getWpogValNumNullFormatted())) {
                //
                // COB_CODE: MOVE WPOG-VAL-NUM(IX-TAB-POG)
                //             TO WK-PERMANFEE
                ws.setWkPermanfee(TruncAbs.toInt(ws.getWpogTabParamOgg(ws.getIxIndici().getTabPog()).getLccvpog1().getDati().getWpogValNum().getWpogValNum(), 5));
                //
            }
            else {
                //
                // COB_CODE: MOVE WK-PGM
                //             TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE WK-LABEL-ERR
                //             TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
                // COB_CODE: MOVE '001114'
                //             TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("001114");
                // COB_CODE: MOVE 'PARAMETRO PERMANFEE NON REPERITO'
                //             TO IEAI9901-PARAMETRI-ERR
                ws.getIeai9901Area().setParametriErr("PARAMETRO PERMANFEE NON REPERITO");
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
                //
            }
            //
        }
    }

    /**Original name: S12120-CERCA-MESIDIFFMFEE<br>
	 * <pre>----------------------------------------------------------------*
	 *  ACQUISIZIONE DEL PARAMETRO MESIDIFFMFEE
	 * ----------------------------------------------------------------*</pre>*/
    private void s12120CercaMesidiffmfee() {
        // COB_CODE: MOVE 'S12120-CERCA-MESIDIFFMFEE'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S12120-CERCA-MESIDIFFMFEE");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // COB_CODE: MOVE 'MESIDIFFMFEE'
        //             TO LDBV1131-COD-PARAM.
        ws.getAreaLdbv1131().setCodParam("MESIDIFFMFEE");
        //
        // COB_CODE: PERFORM S12190-IMPOSTA-PARAM-OGG
        //              THRU S12190-IMPOSTA-PARAM-OGG-EX.
        s12190ImpostaParamOgg();
        //
        // COB_CODE: PERFORM S12199-LEGGI-PARAM-OGG
        //              THRU S12199-LEGGI-PARAM-OGG-EX.
        s12199LeggiParamOgg();
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                   END-IF
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE:         IF WPOG-VAL-NUM-NULL(IX-TAB-POG) NOT = HIGH-VALUES
            //           *
            //                        TO WK-MESIDIFFMFEE
            //           *
            //                   ELSE
            //           *
            //                         THRU EX-S0300
            //           *
            //                   END-IF
            if (!Characters.EQ_HIGH.test(ws.getWpogTabParamOgg(ws.getIxIndici().getTabPog()).getLccvpog1().getDati().getWpogValNum().getWpogValNumNullFormatted())) {
                //
                // COB_CODE: MOVE WPOG-VAL-NUM(IX-TAB-POG)
                //             TO WK-MESIDIFFMFEE
                ws.setWkMesidiffmfee(TruncAbs.toInt(ws.getWpogTabParamOgg(ws.getIxIndici().getTabPog()).getLccvpog1().getDati().getWpogValNum().getWpogValNum(), 5));
                //
            }
            else {
                //
                // COB_CODE: MOVE WK-PGM
                //             TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE WK-LABEL-ERR
                //             TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
                // COB_CODE: MOVE '001114'
                //             TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("001114");
                // COB_CODE: MOVE 'PARAMETRO MESIDIFFMFEE NON REPERITO'
                //             TO IEAI9901-PARAMETRI-ERR
                ws.getIeai9901Area().setParametriErr("PARAMETRO MESIDIFFMFEE NON REPERITO");
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
                //
            }
            //
        }
    }

    /**Original name: S12130-CERCA-DECADELMFEE<br>
	 * <pre>----------------------------------------------------------------*
	 *  ACQUISIZIONE DEL PARAMETRO DECADELMFEE
	 * ----------------------------------------------------------------*</pre>*/
    private void s12130CercaDecadelmfee() {
        // COB_CODE: MOVE 'S12130-CERCA-DECADELMFEE'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S12130-CERCA-DECADELMFEE");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // COB_CODE: MOVE 'DECADELMFEE'
        //             TO LDBV1131-COD-PARAM.
        ws.getAreaLdbv1131().setCodParam("DECADELMFEE");
        //
        // COB_CODE: PERFORM S12190-IMPOSTA-PARAM-OGG
        //              THRU S12190-IMPOSTA-PARAM-OGG-EX.
        s12190ImpostaParamOgg();
        //
        // COB_CODE: PERFORM S12199-LEGGI-PARAM-OGG
        //              THRU S12199-LEGGI-PARAM-OGG-EX.
        s12199LeggiParamOgg();
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                   END-IF
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE:         IF WPOG-VAL-NUM-NULL(IX-TAB-POG) NOT = HIGH-VALUES
            //           *
            //                        TO WK-DECADELMFEE
            //           *
            //                   ELSE
            //           *
            //                         THRU EX-S0300
            //           *
            //                   END-IF
            if (!Characters.EQ_HIGH.test(ws.getWpogTabParamOgg(ws.getIxIndici().getTabPog()).getLccvpog1().getDati().getWpogValNum().getWpogValNumNullFormatted())) {
                //
                // COB_CODE: MOVE WPOG-VAL-NUM(IX-TAB-POG)
                //             TO WK-DECADELMFEE
                ws.setWkDecadelmfee(TruncAbs.toInt(ws.getWpogTabParamOgg(ws.getIxIndici().getTabPog()).getLccvpog1().getDati().getWpogValNum().getWpogValNum(), 5));
                //
            }
            else {
                //
                // COB_CODE: MOVE WK-PGM
                //             TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE WK-LABEL-ERR
                //             TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
                // COB_CODE: MOVE '001114'
                //             TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("001114");
                // COB_CODE: MOVE 'PARAMETRO DECADELMFEE NON REPERITO'
                //             TO IEAI9901-PARAMETRI-ERR
                ws.getIeai9901Area().setParametriErr("PARAMETRO DECADELMFEE NON REPERITO");
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
                //
            }
            //
        }
    }

    /**Original name: S12190-IMPOSTA-PARAM-OGG<br>
	 * <pre>----------------------------------------------------------------*
	 *  VALORIZZAZIONI PER LA LETTURA CON WHERE CONDITION AD HOC
	 * ----------------------------------------------------------------*</pre>*/
    private void s12190ImpostaParamOgg() {
        // COB_CODE: MOVE 'S12190-IMPOSTA-PARAM-OGG'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S12190-IMPOSTA-PARAM-OGG");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // COB_CODE: MOVE WPOL-ID-POLI
        //             TO LDBV1131-ID-OGG.
        ws.getAreaLdbv1131().setIdOgg(wpolAreaPolizza.getLccvpol1().getDati().getWpolIdPoli());
        // COB_CODE: MOVE 'PO'
        //             TO LDBV1131-TP-OGG.
        ws.getAreaLdbv1131().setTpOgg("PO");
        //
        //  --> La data effetto viene sempre valorizzata
        //
        // COB_CODE: MOVE ZERO
        //             TO IDSI0011-DATA-INIZIO-EFFETTO
        //                IDSI0011-DATA-FINE-EFFETTO
        //                IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        //
        // COB_CODE: SET IDSI0011-TRATT-DEFAULT
        //             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setIdsi0011TrattDefault();
        //
        //  --> Nome tabella fisica db
        //
        // COB_CODE: MOVE LDBS1130
        //             TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato(ws.getLdbs1130());
        //
        //  --> Dclgen tabella
        //
        // COB_CODE: MOVE AREA-LDBV1131
        //             TO IDSI0011-BUFFER-WHERE-COND.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferWhereCond(ws.getAreaLdbv1131().getAreaLdbv1131Formatted());
        // COB_CODE: MOVE SPACES
        //             TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
        //
        //  --> Tipo operazione
        //
        // COB_CODE: SET IDSI0011-SELECT
        //             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        // COB_CODE: SET IDSI0011-TRATT-DEFAULT
        //             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setIdsi0011TrattDefault();
        // COB_CODE: SET IDSI0011-WHERE-CONDITION
        //             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setWhereCondition();
        //
        //  --> Modalita di accesso
        //
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC
        //             TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL
        //             TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
    }

    /**Original name: S12199-LEGGI-PARAM-OGG<br>
	 * <pre>----------------------------------------------------------------*
	 *  LETTURA DELLA TABELLA PARAMETRO OGGETTO
	 * ----------------------------------------------------------------*</pre>*/
    private void s12199LeggiParamOgg() {
        // COB_CODE: MOVE 'S12199-LEGGI-PARAM-OGG'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S12199-LEGGI-PARAM-OGG");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX
        callDispatcher();
        //
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //           *
        //                   END-EVALUATE
        //           *
        //                ELSE
        //           *
        //           *  --> Errore dispatcher
        //           *
        //                      THRU EX-S0300
        //           *
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            //
            //
            // COB_CODE:         EVALUATE TRUE
            //           *
            //                       WHEN IDSO0011-NOT-FOUND
            //           *
            //           *  --> Chiave non trovata
            //           *
            //                               THRU EX-S0300
            //           *
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //           *
            //           *  --> Operazione eseguita correttamente
            //           *
            //                            END-IF
            //           *
            //                       WHEN OTHER
            //           *
            //           *  --> Errore di accesso al db
            //           *
            //                               THRU EX-S0300
            //           *
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.NOT_FOUND://
                    //  --> Chiave non trovata
                    //
                    //
                    // COB_CODE:                  EVALUATE LDBV1131-COD-PARAM
                    //           *
                    //                              WHEN 'PERMANFEE'
                    //           *
                    //                                  TO IEAI9901-PARAMETRI-ERR
                    //           *
                    //                              WHEN 'MESIDIFFMFEE'
                    //           *
                    //                                  TO IEAI9901-PARAMETRI-ERR
                    //           *
                    //                            END-EVALUATE
                    switch (ws.getAreaLdbv1131().getCodParam()) {

                        case "PERMANFEE"://
                            // COB_CODE: MOVE 'PERMANFEE NON TROVATO SU PARAM_OGG'
                            //             TO IEAI9901-PARAMETRI-ERR
                            ws.getIeai9901Area().setParametriErr("PERMANFEE NON TROVATO SU PARAM_OGG");
                            //
                            break;

                        case "MESIDIFFMFEE"://
                            // COB_CODE: MOVE 'MESIDIFFMFEE NON TROVATO SU PARAM_OGG'
                            //             TO IEAI9901-PARAMETRI-ERR
                            ws.getIeai9901Area().setParametriErr("MESIDIFFMFEE NON TROVATO SU PARAM_OGG");
                            //
                            break;

                        default:break;
                    }
                    //
                    // COB_CODE: MOVE WK-PGM
                    //             TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE WK-LABEL-ERR
                    //             TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
                    // COB_CODE: MOVE '005069'
                    //             TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005069");
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    //
                    break;

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://
                    //  --> Operazione eseguita correttamente
                    //
                    // COB_CODE: MOVE WPOG-ELE-PARAM-OGG-MAX
                    //             TO IX-TAB-POG
                    ws.getIxIndici().setTabPog(ws.getWpogEleParamOggMax());
                    // COB_CODE: ADD 1
                    //             TO IX-TAB-POG
                    ws.getIxIndici().setTabPog(Trunc.toShort(1 + ws.getIxIndici().getTabPog(), 4));
                    //
                    // COB_CODE:                  IF IX-TAB-POG >  WK-POG-MAX-A
                    //           *
                    //                                  THRU EX-S0300
                    //           *
                    //                            ELSE
                    //           *
                    //                                TO TRUE
                    //           *
                    //                            END-IF
                    if (ws.getIxIndici().getTabPog() > ws.getLccvpogz().getMaxA()) {
                        //
                        // COB_CODE: MOVE WK-PGM
                        //             TO IEAI9901-COD-SERVIZIO-BE
                        ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                        // COB_CODE: MOVE WK-LABEL-ERR
                        //             TO IEAI9901-LABEL-ERR
                        ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
                        // COB_CODE: MOVE '005069'
                        //             TO IEAI9901-COD-ERRORE
                        ws.getIeai9901Area().setCodErroreFormatted("005069");
                        // COB_CODE: MOVE 'OVERFLOW CARICAMENTO TABELLA PARAM-OGG'
                        //             TO IEAI9901-PARAMETRI-ERR
                        ws.getIeai9901Area().setParametriErr("OVERFLOW CARICAMENTO TABELLA PARAM-OGG");
                        // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        //              THRU EX-S0300
                        s0300RicercaGravitaErrore();
                        //
                    }
                    else {
                        //
                        // COB_CODE: MOVE IX-TAB-POG
                        //             TO WPOG-ELE-PARAM-OGG-MAX
                        ws.setWpogEleParamOggMax(ws.getIxIndici().getTabPog());
                        // COB_CODE: MOVE IDSO0011-BUFFER-DATI
                        //             TO PARAM-OGG
                        ws.getParamOgg().setParamOggFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                        // COB_CODE: PERFORM VALORIZZA-OUTPUT-POG
                        //              THRU VALORIZZA-OUTPUT-POG-EX
                        valorizzaOutputPog();
                        // COB_CODE: SET WPOG-ST-INV(IX-TAB-POG)
                        //            TO TRUE
                        ws.getWpogTabParamOgg(ws.getIxIndici().getTabPog()).getLccvpog1().getStatus().setInv();
                        //
                    }
                    //
                    break;

                default://
                    //  --> Errore di accesso al db
                    //
                    // COB_CODE: MOVE WK-PGM
                    //             TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE WK-LABEL-ERR
                    //             TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
                    // COB_CODE: MOVE '005069'
                    //             TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005069");
                    // COB_CODE: MOVE 'ERRORE LETTURA PARAMETRO OGGETTO'
                    //             TO IEAI9901-PARAMETRI-ERR
                    ws.getIeai9901Area().setParametriErr("ERRORE LETTURA PARAMETRO OGGETTO");
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    //
                    break;
            }
            //
        }
        else {
            //
            //  --> Errore dispatcher
            //
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL-ERR
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
            // COB_CODE: MOVE '005016'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: MOVE 'ERRORE DISPATCHER LETTURA PARAMETRO OGGETTO'
            //             TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("ERRORE DISPATCHER LETTURA PARAMETRO OGGETTO");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
            //
        }
    }

    /**Original name: S12200-CALCOLA-DATE-MANFEE<br>
	 * <pre>----------------------------------------------------------------*
	 *  CALCOLO DELLE DATE DI RICORRENZA SUCCESSIVA E ULTIMA EROGAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s12200CalcolaDateManfee() {
        // COB_CODE: MOVE 'S12200-CALCOLA-DATE-MANFEE'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S12200-CALCOLA-DATE-MANFEE");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // COB_CODE: MOVE ZEROES
        //             TO WK-DT-CALCOLATA.
        ws.setWkDtCalcolata(0);
        //
        // --> Vengono Aggiunti Gli Eventuali Mesi Di Differimento
        //
        // COB_CODE: PERFORM S12210-AGGIUNGI-DIFFMANFEE
        //              THRU S12210-AGGIUNGI-DIFFMANFEE-EX
        s12210AggiungiDiffmanfee();
        //
        // --> Viene Determinato Il Giorno Di Ricorrenza In Funzione
        // --> Della Decade Prevista
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                      THRU S12292-VALUTA-DECADE-EX
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE: PERFORM S12292-VALUTA-DECADE
            //              THRU S12292-VALUTA-DECADE-EX
            s12292ValutaDecade();
            //
        }
        //
        // --> Alla Data Effetto Sono State Aggiunti I Mesi Di
        // --> Differimento Ed Il Giorno H Stato Calcolato In Base
        // --> Alla Decade Di Riferimento, il risultato h la
        // --> Data Ricorrenza Di Liquidazione
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                     TO WK-DT-RICOR-SUCC
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE: MOVE WK-DT-CALCOLATA
            //             TO WK-DT-RICOR-SUCC
            ws.setWkDtRicorSuccFormatted(ws.getWkDtCalcolataFormatted());
            //
        }
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //           * --> Se Il Parametro Permanfee H Uguale A 1
        //           * --> Il Management Fee Viene Erogato In Un Unica Rata Quindi
        //           * --> La Data Di Ricorrenza Successiva H Uguale Alla Data
        //           * --> Di Ultima Erogazione Management Fee
        //           * --> In Caso Contrario Il Parametro Permanfee Rappresenta Il
        //           * --> Numero Di Rate Da Erogare
        //           *
        //                   END-IF
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // --> Se Il Parametro Permanfee H Uguale A 1
            // --> Il Management Fee Viene Erogato In Un Unica Rata Quindi
            // --> La Data Di Ricorrenza Successiva H Uguale Alla Data
            // --> Di Ultima Erogazione Management Fee
            // --> In Caso Contrario Il Parametro Permanfee Rappresenta Il
            // --> Numero Di Rate Da Erogare
            //
            // COB_CODE:         IF WK-PERMANFEE = 1  OR
            //                      IDSV0001-TIPO-MOVIMENTO = 6025 OR 6026
            //           *
            //                        TO WK-DT-ULT-EROG-MF
            //           *
            //                   ELSE
            //           *
            //                         THRU S12220-CALC-ULT-EROG-MF-EX
            //           *
            //                   END-IF
            if (ws.getWkPermanfee() == 1 || areaIdsv0001.getAreaComune().getIdsv0001TipoMovimento() == 6025 || areaIdsv0001.getAreaComune().getIdsv0001TipoMovimento() == 6026) {
                //
                // COB_CODE: MOVE WK-DT-RICOR-SUCC
                //             TO WK-DT-ULT-EROG-MF
                ws.setWkDtUltErogMfFormatted(ws.getWkDtRicorSuccFormatted());
                //
            }
            else {
                //
                // COB_CODE: PERFORM S12220-CALC-ULT-EROG-MF
                //              THRU S12220-CALC-ULT-EROG-MF-EX
                s12220CalcUltErogMf();
                //
            }
            //
        }
    }

    /**Original name: S12210-AGGIUNGI-DIFFMANFEE<br>
	 * <pre>----------------------------------------------------------------*
	 *  AGGIUNGE I MESI DI DIFFERIMENTO ALLA RICORRENZA SUCCESSIVA
	 * ----------------------------------------------------------------*</pre>*/
    private void s12210AggiungiDiffmanfee() {
        // COB_CODE: MOVE 'S12210-AGGIUNGI-DIFFMANFEE'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S12210-AGGIUNGI-DIFFMANFEE");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // --> A2K-INAMG -> Data Di Partenza Alla Quale Sommare Il Delta
        //
        // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
        //             TO A2K-INAMG.
        ws.getIoA2kLccc0003().getInput().getA2kIndata().setA2kInamgFormatted(areaIdsv0001.getAreaComune().getIdsv0001DataEffettoFormatted());
        //
        // --> A2K-TDELTA -> Tipo Delta: Mesi(M) Anni(A)
        //
        // COB_CODE: MOVE 'M'
        //             TO A2K-TDELTA.
        ws.getIoA2kLccc0003().getInput().setA2kTdeltaFormatted("M");
        //
        // --> A2K-DELTA  -> Delta Da Sommare
        //
        // COB_CODE: MOVE WK-MESIDIFFMFEE
        //             TO A2K-DELTA.
        ws.getIoA2kLccc0003().getInput().setA2kDeltaFormatted(ws.getWkMesidiffmfeeFormatted());
        //
        // --> A2K-FUNZ -> Tipo Funzione: Somma Delta
        //
        // COB_CODE: MOVE '02'
        //             TO A2K-FUNZ.
        ws.getIoA2kLccc0003().getInput().setA2kFunz("02");
        //
        // --> A2K-INFO -> Tipo Formato Data: AAAAMMGG
        //
        // COB_CODE: MOVE '03'
        //             TO A2K-INFO.
        ws.getIoA2kLccc0003().getInput().setA2kInfo("03");
        //
        // --> A2K-INICON -> Giorno Inizio Conteggio (Stesso Giorno)
        //
        // COB_CODE: MOVE '0'
        //             TO A2K-INICON.
        ws.getIoA2kLccc0003().getInput().setA2kIniconFormatted("0");
        //
        // --> A2K-FISLAV -> Tipo Giorni Da Sommare (Fissi)
        //
        // COB_CODE: MOVE '0'
        //             TO A2K-FISLAV.
        ws.getIoA2kLccc0003().getInput().setA2kFislavFormatted("0");
        //
        // COB_CODE: PERFORM S12291-CHIAMA-LCCS0003
        //              THRU S12291-CHIAMA-LCCS0003-EX.
        s12291ChiamaLccs0003();
    }

    /**Original name: S12220-CALC-ULT-EROG-MF<br>
	 * <pre>----------------------------------------------------------------*
	 *  CALCOLA DATA ULTIMA EROGAZIONE MANAGEMENT FEE
	 * ----------------------------------------------------------------*</pre>*/
    private void s12220CalcUltErogMf() {
        // COB_CODE: MOVE 'S12220-CALC-ULT-EROG-MF'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S12220-CALC-ULT-EROG-MF");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // --> A2K-INAMG -> Data Di Partenza Alla Quale Sommare Il Delta
        //
        // COB_CODE: MOVE WK-DT-CALCOLATA
        //             TO A2K-INAMG.
        ws.getIoA2kLccc0003().getInput().getA2kIndata().setA2kInamgFormatted(ws.getWkDtCalcolataFormatted());
        //
        // --> A2K-TDELTA -> Tipo Delta: Mesi(M) Anni(A)
        //
        // COB_CODE: MOVE 'M'
        //             TO A2K-TDELTA.
        ws.getIoA2kLccc0003().getInput().setA2kTdeltaFormatted("M");
        //
        // --> A2K-DELTA  -> Delta Da Sommare
        //
        // COB_CODE: MOVE 1
        //             TO A2K-DELTA.
        ws.getIoA2kLccc0003().getInput().setA2kDelta(((short)1));
        //
        // --> A2K-FUNZ -> Tipo Funzione: Somma Delta
        //
        // COB_CODE: MOVE '02'
        //             TO A2K-FUNZ.
        ws.getIoA2kLccc0003().getInput().setA2kFunz("02");
        //
        // --> A2K-INFO -> Tipo Formato Data: AAAAMMGG
        //
        // COB_CODE: MOVE '03'
        //             TO A2K-INFO.
        ws.getIoA2kLccc0003().getInput().setA2kInfo("03");
        //
        // --> A2K-INICON -> Giorno Inizio Conteggio (Stesso Giorno)
        //
        // COB_CODE: MOVE '0'
        //             TO A2K-INICON.
        ws.getIoA2kLccc0003().getInput().setA2kIniconFormatted("0");
        //
        // --> A2K-FISLAV -> Tipo Giorni Da Sommare (Fissi)
        //
        // COB_CODE: MOVE '0'
        //             TO A2K-FISLAV.
        ws.getIoA2kLccc0003().getInput().setA2kFislavFormatted("0");
        //
        // COB_CODE: PERFORM S12230-AGGIUNGI-FRAZ
        //              THRU S12230-AGGIUNGI-FRAZ-EX
        //           VARYING IX-FRAZ FROM 1 BY 1
        //             UNTIL IX-FRAZ NOT < WK-PERMANFEE
        //                OR IDSV0001-ESITO-KO.
        ws.getIxIndici().setFraz(((short)1));
        while (!(ws.getIxIndici().getFraz() >= ws.getWkPermanfee() || areaIdsv0001.getEsito().isIdsv0001EsitoKo())) {
            s12230AggiungiFraz();
            ws.getIxIndici().setFraz(Trunc.toShort(ws.getIxIndici().getFraz() + 1, 4));
        }
    }

    /**Original name: S12230-AGGIUNGI-FRAZ<br>
	 * <pre>----------------------------------------------------------------*
	 *  AGGIUNGE TANTI FRAZIONAMENTI QUANTE SONO LE RATE
	 * ----------------------------------------------------------------*</pre>*/
    private void s12230AggiungiFraz() {
        // COB_CODE: MOVE 'S12230-AGGIUNGI-FRAZ'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S12230-AGGIUNGI-FRAZ");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // COB_CODE: PERFORM S12291-CHIAMA-LCCS0003
        //              THRU S12291-CHIAMA-LCCS0003-EX.
        s12291ChiamaLccs0003();
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                      THRU S12292-VALUTA-DECADE-EX
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE: PERFORM S12292-VALUTA-DECADE
            //              THRU S12292-VALUTA-DECADE-EX
            s12292ValutaDecade();
            //
        }
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                        WK-DT-ULT-EROG-MF
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE: MOVE WK-DT-CALCOLATA
            //             TO A2K-INAMG
            //                WK-DT-ULT-EROG-MF
            ws.getIoA2kLccc0003().getInput().getA2kIndata().setA2kInamgFormatted(ws.getWkDtCalcolataFormatted());
            ws.setWkDtUltErogMfFormatted(ws.getWkDtCalcolataFormatted());
            //
        }
    }

    /**Original name: S12300-CALCOLA-RATA-MANFEE<br>
	 * <pre>----------------------------------------------------------------*
	 *  CALCOLO DEL VALORE DI MANAGEMENT FEE DA RIPORATRE SULLA PMO
	 * ----------------------------------------------------------------*</pre>*/
    private void s12300CalcolaRataManfee() {
        // COB_CODE: MOVE 'S12300-CALCOLA-RATA-MANFEE'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S12300-CALCOLA-RATA-MANFEE");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                   END-PERFORM
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE:         PERFORM VARYING IX-TAB-GRZ FROM 1 BY 1
            //                     UNTIL IX-TAB-GRZ > WGRZ-ELE-GAR-MAX
            //           *
            //                          THRU S12320-CREA-PMO-MANFEE-EX
            //           *
            //                   END-PERFORM
            ws.getIxIndici().setTabGrz(((short)1));
            while (!(ws.getIxIndici().getTabGrz() > wgrzAreaGaranzia.getEleGarMax())) {
                //
                // COB_CODE: PERFORM S12310-SOMMA-MANFEE-TGA
                //              THRU S12310-SOMMA-MANFEE-TGA-EX
                s12310SommaManfeeTga();
                //
                // COB_CODE: PERFORM S12320-CREA-PMO-MANFEE
                //              THRU S12320-CREA-PMO-MANFEE-EX
                s12320CreaPmoManfee();
                //
                ws.getIxIndici().setTabGrz(Trunc.toShort(ws.getIxIndici().getTabGrz() + 1, 4));
            }
            //
        }
    }

    /**Original name: S12310-SOMMA-MANFEE-TGA<br>
	 * <pre>----------------------------------------------------------------*
	 *  SOMMATORIA DELL'IMPORTO DI MANFFE DELLE TRANCHE (PER OGNI GAR)
	 * ----------------------------------------------------------------*</pre>*/
    private void s12310SommaManfeeTga() {
        // COB_CODE: MOVE 'S12310-SOMMA-MANFEE-TGA'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S12310-SOMMA-MANFEE-TGA");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // COB_CODE: MOVE ZEROES
        //             TO WK-TOT-MANFEE.
        ws.setWkTotManfee(new AfDecimal(0, 15, 3));
        //
        // --> Per Ogni Garanzia Vengono Sommati I Valori Di Management
        // --> Fee Di Ogni Tranche Tenedo In Considerazione La Tipologia
        // --> Viene Abbattuto Il Valore Delle Tranche Negative E
        // --> Sommato Per Le Altre Tipologie
        //
        // COB_CODE:      PERFORM VARYING IX-TAB-TGA FROM 1 BY 1
        //                  UNTIL IX-TAB-TGA > WTGA-ELE-TRAN-MAX
        //           *
        //                     END-IF
        //           *
        //                END-PERFORM.
        ws.getIxIndici().setTabTga(((short)1));
        while (!(ws.getIxIndici().getTabTga() > wtgaAreaTranche.getEleTranMax())) {
            //
            // COB_CODE:           IF WGRZ-ID-GAR(IX-TAB-GRZ) =
            //                        WTGA-ID-GAR(IX-TAB-TGA)
            //           *
            //                        END-IF
            //           *
            //                     END-IF
            if (wgrzAreaGaranzia.getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzIdGar() == wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaIdGar()) {
                //
                // COB_CODE:              IF WTGA-ST-MOD(IX-TAB-TGA)
                //           *
                //                           END-IF
                //           *
                //                        END-IF
                if (wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getStatus().isWcomStDeMod()) {
                    //
                    // COB_CODE:                 IF WTGA-MANFEE-RICOR-NULL(IX-TAB-TGA)
                    //                              NOT = HIGH-VALUES
                    //           *
                    //                              END-IF
                    //           *
                    //                           END-IF
                    if (!Characters.EQ_HIGH.test(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaManfeeRicor().getWtgaManfeeRicorNullFormatted())) {
                        //
                        // COB_CODE: MOVE WTGA-TP-TRCH(IX-TAB-TGA)
                        //             TO WS-TP-TRCH
                        ws.getWsTpTrch().setWsTpTrch(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaTpTrch());
                        //
                        // COB_CODE:                    IF TP-TRCH-NEG-PRCOS       OR
                        //                                 TP-TRCH-NEG-RIS-PAR     OR
                        //                                 TP-TRCH-NEG-RIS-PRO     OR
                        //                                 TP-TRCH-NEG-IMPST-SOST  OR
                        //                                 TP-TRCH-NEG-DA-DIS
                        //           *
                        //                                         WTGA-MANFEE-RICOR(IX-TAB-TGA)
                        //           *
                        //                              ELSE
                        //           *
                        //                                         WTGA-MANFEE-RICOR(IX-TAB-TGA)
                        //           *
                        //                              END-IF
                        if (ws.getWsTpTrch().isNegPrcos() || ws.getWsTpTrch().isNegRisPar() || ws.getWsTpTrch().isNegRisPro() || ws.getWsTpTrch().isNegImpstSost() || ws.getWsTpTrch().isNegDaDis()) {
                            //
                            // COB_CODE: COMPUTE WK-TOT-MANFEE =
                            //                   WK-TOT-MANFEE -
                            //                   WTGA-MANFEE-RICOR(IX-TAB-TGA)
                            ws.setWkTotManfee(Trunc.toDecimal(ws.getWkTotManfee().subtract(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaManfeeRicor().getWtgaManfeeRicor()), 15, 3));
                            //
                        }
                        else {
                            //
                            // COB_CODE: COMPUTE WK-TOT-MANFEE =
                            //                   WK-TOT-MANFEE +
                            //                   WTGA-MANFEE-RICOR(IX-TAB-TGA)
                            ws.setWkTotManfee(Trunc.toDecimal(ws.getWkTotManfee().add(wtgaAreaTranche.getTabTran(ws.getIxIndici().getTabTga()).getLccvtga1().getDati().getWtgaManfeeRicor().getWtgaManfeeRicor()), 15, 3));
                            //
                        }
                        //
                    }
                    //
                }
                //
            }
            //
            ws.getIxIndici().setTabTga(Trunc.toShort(ws.getIxIndici().getTabTga() + 1, 4));
        }
    }

    /**Original name: S12320-CREA-PMO-MANFEE<br>
	 * <pre>----------------------------------------------------------------*
	 *          VALORIZZAZIONE DELLA PMO PER IL MANFEE                 *
	 * ----------------------------------------------------------------*</pre>*/
    private void s12320CreaPmoManfee() {
        // COB_CODE: MOVE 'S12320-CREA-PMO-MANFEE'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S12320-CREA-PMO-MANFEE");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // COB_CODE: MOVE WPMO-ELE-PARAM-MOV-MAX
        //             TO IX-TAB-PMO.
        ws.getIxIndici().setTabPmo(wpmoAreaParamMovi.getEleParamMovMax());
        // COB_CODE: ADD 1
        //             TO IX-TAB-PMO.
        ws.getIxIndici().setTabPmo(Trunc.toShort(1 + ws.getIxIndici().getTabPmo(), 4));
        // COB_CODE: MOVE IX-TAB-PMO
        //             TO WPMO-ELE-PARAM-MOV-MAX.
        wpmoAreaParamMovi.setEleParamMovMax(ws.getIxIndici().getTabPmo());
        //
        // COB_CODE: SET WPMO-ST-ADD(IX-TAB-PMO)
        //             TO TRUE.
        wpmoAreaParamMovi.getTabParamMov(ws.getIxIndici().getTabPmo()).getLccvpmo1().getStatus().setWcomStAdd();
        //
        // COB_CODE: MOVE IX-TAB-PMO
        //             TO WPMO-ID-PARAM-MOVI(IX-TAB-PMO).
        wpmoAreaParamMovi.getTabParamMov(ws.getIxIndici().getTabPmo()).getLccvpmo1().getDati().setWpmoIdParamMovi(ws.getIxIndici().getTabPmo());
        //
        // COB_CODE: MOVE WGRZ-ID-GAR(IX-TAB-GRZ)
        //             TO WPMO-ID-OGG(IX-TAB-PMO).
        wpmoAreaParamMovi.getTabParamMov(ws.getIxIndici().getTabPmo()).getLccvpmo1().getDati().setWpmoIdOgg(wgrzAreaGaranzia.getTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzIdGar());
        //
        // COB_CODE: SET GARANZIA
        //             TO TRUE
        ws.getWsTpOgg().setGaranzia();
        // COB_CODE: MOVE WS-TP-OGG
        //             TO WPMO-TP-OGG(IX-TAB-PMO).
        wpmoAreaParamMovi.getTabParamMov(ws.getIxIndici().getTabPmo()).getLccvpmo1().getDati().setWpmoTpOgg(ws.getWsTpOgg().getWsTpOgg());
        //
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //             TO WPMO-COD-COMP-ANIA(IX-TAB-PMO).
        wpmoAreaParamMovi.getTabParamMov(ws.getIxIndici().getTabPmo()).getLccvpmo1().getDati().setWpmoCodCompAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        //
        //
        // COB_CODE:      EVALUATE IDSV0001-TIPO-MOVIMENTO
        //           *
        //                    WHEN 6006
        //                         TO WPMO-TP-MOVI(IX-TAB-PMO)
        //           *
        //                    WHEN 6024
        //                         TO WPMO-TP-MOVI(IX-TAB-PMO)
        //           *
        //                    WHEN 6025
        //                         TO WPMO-TP-MOVI(IX-TAB-PMO)
        //           *
        //                    WHEN 6026
        //                         TO WPMO-TP-MOVI(IX-TAB-PMO)
        //           *
        //                END-EVALUATE.
        switch (areaIdsv0001.getAreaComune().getIdsv0001TipoMovimento()) {

            case 6006:// COB_CODE: MOVE 6023
                //             TO WPMO-TP-MOVI(IX-TAB-PMO)
                wpmoAreaParamMovi.getTabParamMov(ws.getIxIndici().getTabPmo()).getLccvpmo1().getDati().getWpmoTpMovi().setWpmoTpMovi(6023);
                //
                break;

            case 6024:// COB_CODE: MOVE 6028
                //             TO WPMO-TP-MOVI(IX-TAB-PMO)
                wpmoAreaParamMovi.getTabParamMov(ws.getIxIndici().getTabPmo()).getLccvpmo1().getDati().getWpmoTpMovi().setWpmoTpMovi(6028);
                //
                break;

            case 6025:// COB_CODE: MOVE 6029
                //             TO WPMO-TP-MOVI(IX-TAB-PMO)
                wpmoAreaParamMovi.getTabParamMov(ws.getIxIndici().getTabPmo()).getLccvpmo1().getDati().getWpmoTpMovi().setWpmoTpMovi(6029);
                //
                break;

            case 6026:// COB_CODE: MOVE 6030
                //             TO WPMO-TP-MOVI(IX-TAB-PMO)
                wpmoAreaParamMovi.getTabParamMov(ws.getIxIndici().getTabPmo()).getLccvpmo1().getDati().getWpmoTpMovi().setWpmoTpMovi(6030);
                //
                break;

            default:break;
        }
        //
        // COB_CODE: MOVE WK-MESIDIFFMFEE
        //             TO WPMO-MM-DIFF(IX-TAB-PMO).
        wpmoAreaParamMovi.getTabParamMov(ws.getIxIndici().getTabPmo()).getLccvpmo1().getDati().getWpmoMmDiff().setWpmoMmDiff(Trunc.toShort(ws.getWkMesidiffmfee(), 2));
        //
        // COB_CODE: MOVE WADE-ID-ADES(1)
        //             TO WPMO-ID-ADES(IX-TAB-PMO).
        wpmoAreaParamMovi.getTabParamMov(ws.getIxIndici().getTabPmo()).getLccvpmo1().getDati().getWpmoIdAdes().setWpmoIdAdes(wadeAreaAdesione.getTabAde(1).getLccvade1().getDati().getWadeIdAdes());
        //
        // COB_CODE: MOVE WPOL-ID-POLI
        //             TO WPMO-ID-POLI(IX-TAB-PMO).
        wpmoAreaParamMovi.getTabParamMov(ws.getIxIndici().getTabPmo()).getLccvpmo1().getDati().setWpmoIdPoli(wpolAreaPolizza.getLccvpol1().getDati().getWpolIdPoli());
        //
        // COB_CODE: MOVE WPOL-TP-FRM-ASSVA
        //             TO WPMO-TP-FRM-ASSVA(IX-TAB-PMO).
        wpmoAreaParamMovi.getTabParamMov(ws.getIxIndici().getTabPmo()).getLccvpmo1().getDati().setWpmoTpFrmAssva(wpolAreaPolizza.getLccvpol1().getDati().getWpolTpFrmAssva());
        //
        // COB_CODE:      IF IDSV0001-TIPO-MOVIMENTO = 6006 OR 6024
        //           *
        //                     TO WPMO-FRQ-MOVI(IX-TAB-PMO)
        //           *
        //                ELSE
        //           *
        //                     TO WPMO-IMP-RAT-MANFEE(IX-TAB-PMO)
        //           *
        //                END-IF.
        if (areaIdsv0001.getAreaComune().getIdsv0001TipoMovimento() == 6006 || areaIdsv0001.getAreaComune().getIdsv0001TipoMovimento() == 6024) {
            //
            // COB_CODE: COMPUTE WPMO-IMP-RAT-MANFEE(IX-TAB-PMO) =
            //                  (WK-TOT-MANFEE / WK-PERMANFEE)
            wpmoAreaParamMovi.getTabParamMov(ws.getIxIndici().getTabPmo()).getLccvpmo1().getDati().getWpmoImpRatManfee().setWpmoImpRatManfee(Trunc.toDecimal(new AfDecimal((ws.getWkTotManfee().divide(ws.getWkPermanfee())), 15, 3), 15, 3));
            //
            // COB_CODE: MOVE WK-PERMANFEE
            //             TO WPMO-FRQ-MOVI(IX-TAB-PMO)
            wpmoAreaParamMovi.getTabParamMov(ws.getIxIndici().getTabPmo()).getLccvpmo1().getDati().getWpmoFrqMovi().setWpmoFrqMovi(ws.getWkPermanfee());
            //
        }
        else {
            //
            // COB_CODE: MOVE WK-TOT-MANFEE
            //             TO WPMO-IMP-RAT-MANFEE(IX-TAB-PMO)
            wpmoAreaParamMovi.getTabParamMov(ws.getIxIndici().getTabPmo()).getLccvpmo1().getDati().getWpmoImpRatManfee().setWpmoImpRatManfee(Trunc.toDecimal(ws.getWkTotManfee(), 15, 3));
            //
        }
        //
        // COB_CODE: MOVE WK-DT-RICOR-SUCC
        //             TO WPMO-DT-RICOR-SUCC(IX-TAB-PMO).
        wpmoAreaParamMovi.getTabParamMov(ws.getIxIndici().getTabPmo()).getLccvpmo1().getDati().getWpmoDtRicorSucc().setWpmoDtRicorSucc(ws.getWkDtRicorSucc());
        //
        // COB_CODE: MOVE WK-DT-ULT-EROG-MF
        //             TO WPMO-DT-ULT-EROG-MANFEE(IX-TAB-PMO).
        wpmoAreaParamMovi.getTabParamMov(ws.getIxIndici().getTabPmo()).getLccvpmo1().getDati().getWpmoDtUltErogManfee().setWpmoDtUltErogManfee(ws.getWkDtUltErogMf());
        //
        // COB_CODE: MOVE WPOL-COD-RAMO
        //             TO WPMO-COD-RAMO(IX-TAB-PMO).
        wpmoAreaParamMovi.getTabParamMov(ws.getIxIndici().getTabPmo()).getLccvpmo1().getDati().setWpmoCodRamo(wpolAreaPolizza.getLccvpol1().getDati().getWpolCodRamo());
    }

    /**Original name: S12291-CHIAMA-LCCS0003<br>
	 * <pre>----------------------------------------------------------------*
	 *  CALL AL MODULO LCCS0003
	 * ----------------------------------------------------------------*</pre>*/
    private void s12291ChiamaLccs0003() {
        Lccs0003 lccs0003 = null;
        GenericParam inRcode = null;
        // COB_CODE: MOVE 'S12291-CHIAMA-LCCS0003'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S12291-CHIAMA-LCCS0003");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // COB_CODE:      CALL LCCS0003         USING IO-A2K-LCCC0003
        //                                            IN-RCODE
        //                ON EXCEPTION
        //           *
        //                         THRU EX-S0290
        //           *
        //                END-CALL.
        try {
            lccs0003 = Lccs0003.getInstance();
            inRcode = new GenericParam(MarshalByteExt.strToBuffer(ws.getInRcodeFormatted(), Loas0800Data.Len.IN_RCODE));
            lccs0003.run(ws.getIoA2kLccc0003(), inRcode);
            ws.setInRcodeFromBuffer(inRcode.getByteData());
        }
        catch (ProgramExecutionException __ex) {
            //
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'ERRORE CHIAMATA LCCS0003'
            //             TO CALL-DESC
            ws.getIdsv0002().setCallDesc("ERRORE CHIAMATA LCCS0003");
            // COB_CODE: MOVE WK-LABEL-ERR
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
            // COB_CODE: PERFORM S0290-ERRORE-DI-SISTEMA
            //              THRU EX-S0290
            s0290ErroreDiSistema();
            //
        }
        //
        // COB_CODE:      IF IN-RCODE  = '00'
        //           *
        //                        WK-DT-CALCOLATA
        //           *
        //                ELSE
        //           *
        //                      THRU EX-S0300
        //           *
        //                END-IF.
        if (ws.getInRcodeFormatted().equals("00")) {
            //
            // COB_CODE: MOVE A2K-OUAMG
            //             TO A2K-INAMG
            //                WK-DT-CALCOLATA
            ws.getIoA2kLccc0003().getInput().getA2kIndata().setA2kInamgFormatted(ws.getIoA2kLccc0003().getOutput().getA2kOuamgX().getA2kOuamgFormatted());
            ws.setWkDtCalcolataFormatted(ws.getIoA2kLccc0003().getOutput().getA2kOuamgX().getA2kOuamgFormatted());
            //
        }
        else {
            //
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL-ERR
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
            // COB_CODE: MOVE '001114'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("001114");
            // COB_CODE: MOVE 'ERRORE CALCOLO DATA MF'
            //             TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("ERRORE CALCOLO DATA MF");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
            //
        }
    }

    /**Original name: S12292-VALUTA-DECADE<br>
	 * <pre>----------------------------------------------------------------*
	 *  VALUTAZIONE DELLA DECADE INDICATA DAL PARAMETRO
	 * ----------------------------------------------------------------*</pre>*/
    private void s12292ValutaDecade() {
        // COB_CODE: MOVE 'S12292-VALUTA-DECADE'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S12292-VALUTA-DECADE");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // COB_CODE: MOVE WK-DT-CALCOLATA
        //             TO WK-APPO-DT
        ws.getWkAppoDt().setWkAppoDtFormatted(ws.getWkDtCalcolataFormatted());
        //
        //
        // COB_CODE:      EVALUATE WK-DECADELMFEE
        //           *
        //                    WHEN 1
        //           *
        //                         TO WK-DT-CALCOLATA
        //           *
        //                    WHEN 2
        //           *
        //                         TO WK-DT-CALCOLATA
        //           *
        //                    WHEN 3
        //           *
        //                          THRU S12293-GEST-FINE-MESE-EX
        //           *
        //                    WHEN OTHER
        //           *
        //                          THRU EX-S0300
        //           *
        //                END-EVALUATE.
        switch (ws.getWkDecadelmfee()) {

            case 1://
                // COB_CODE: MOVE 10
                //             TO WK-APPO-GG
                ws.getWkAppoDt().setGg(((short)10));
                // COB_CODE: MOVE WK-APPO-DT
                //             TO WK-DT-CALCOLATA
                ws.setWkDtCalcolataFromBuffer(ws.getWkAppoDt().getWkAppoDtBytes());
                //
                break;

            case 2://
                // COB_CODE: MOVE 20
                //             TO WK-APPO-GG
                ws.getWkAppoDt().setGg(((short)20));
                // COB_CODE: MOVE WK-APPO-DT
                //             TO WK-DT-CALCOLATA
                ws.setWkDtCalcolataFromBuffer(ws.getWkAppoDt().getWkAppoDtBytes());
                //
                break;

            case 3://
                // COB_CODE: MOVE WK-APPO-DT
                //             TO S029-DT-CALC
                ws.getAreaIoLccs0029().setDtCalcFromBuffer(ws.getWkAppoDt().getWkAppoDtBytes());
                //
                // COB_CODE: PERFORM S12293-GEST-FINE-MESE
                //              THRU S12293-GEST-FINE-MESE-EX
                s12293GestFineMese();
                //
                break;

            default://
                // COB_CODE: MOVE WK-PGM
                //             TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE WK-LABEL-ERR
                //             TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
                // COB_CODE: MOVE '001114'
                //             TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("001114");
                // COB_CODE: MOVE 'VALORE PARAMETRO DECADELMFEE NON AMMESSO'
                //             TO IEAI9901-PARAMETRI-ERR
                ws.getIeai9901Area().setParametriErr("VALORE PARAMETRO DECADELMFEE NON AMMESSO");
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
                //
                break;
        }
    }

    /**Original name: S12293-GEST-FINE-MESE<br>
	 * <pre>----------------------------------------------------------------*
	 *  CERCA L'ULTIMO GIORNO DEL MESE IN ESAME
	 * ----------------------------------------------------------------*</pre>*/
    private void s12293GestFineMese() {
        Lccs0029 lccs0029 = null;
        // COB_CODE: MOVE 'S12293-GEST-FINE-MESE'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S12293-GEST-FINE-MESE");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
        //
        // COB_CODE:      CALL LCCS0029         USING AREA-IDSV0001
        //                                            AREA-IO-LCCS0029
        //                ON EXCEPTION
        //           *
        //                         THRU EX-S0290
        //           *
        //                END-CALL.
        try {
            lccs0029 = Lccs0029.getInstance();
            lccs0029.run(areaIdsv0001, ws.getAreaIoLccs0029());
        }
        catch (ProgramExecutionException __ex) {
            //
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'ERRORE CHIAMATA LCCS0003'
            //             TO CALL-DESC
            ws.getIdsv0002().setCallDesc("ERRORE CHIAMATA LCCS0003");
            // COB_CODE: MOVE WK-LABEL-ERR
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
            // COB_CODE: PERFORM S0290-ERRORE-DI-SISTEMA
            //              THRU EX-S0290
            s0290ErroreDiSistema();
            //
        }
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                     TO WK-DT-CALCOLATA
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE: MOVE S029-DT-CALC
            //             TO WK-DT-CALCOLATA
            ws.setWkDtCalcolataFormatted(ws.getAreaIoLccs0029().getDtCalcFormatted());
            //
        }
    }

    /**Original name: S90000-OPERAZ-FINALI<br>
	 * <pre> ============================================================== *
	 *  -->           O P E R Z I O N I   F I N A L I              <-- *
	 *  ============================================================== *</pre>*/
    private void s90000OperazFinali() {
        // COB_CODE: MOVE 'S90000-OPERAZ-FINALI'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S90000-OPERAZ-FINALI");
        //
        // COB_CODE: PERFORM DISPLAY-LABEL
        //              THRU DISPLAY-LABEL-EX.
        displayLabel();
    }

    /**Original name: CALL-DISPATCHER<br>
	 * <pre>----------------------------------------------------------------*
	 *      ROUTINES CALL DISPATCHER                                   *
	 * ----------------------------------------------------------------*
	 * *****************************************************************
	 *    CALL DISPATCHER
	 * *****************************************************************</pre>*/
    private void callDispatcher() {
        Idss0010 idss0010 = null;
        // COB_CODE: MOVE IDSV0001-MODALITA-ESECUTIVA
        //                              TO IDSI0011-MODALITA-ESECUTIVA
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011ModalitaEsecutiva().setIdsi0011ModalitaEsecutiva(areaIdsv0001.getAreaComune().getModalitaEsecutiva().getModalitaEsecutiva());
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //                              TO IDSI0011-CODICE-COMPAGNIA-ANIA
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceCompagniaAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: MOVE IDSV0001-COD-MAIN-BATCH
        //                              TO IDSI0011-COD-MAIN-BATCH
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodMainBatch(areaIdsv0001.getAreaComune().getCodMainBatch());
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO
        //                              TO IDSI0011-TIPO-MOVIMENTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011TipoMovimentoFormatted(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimentoFormatted());
        // COB_CODE: MOVE IDSV0001-SESSIONE
        //                              TO IDSI0011-SESSIONE
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011Sessione(areaIdsv0001.getAreaComune().getSessione());
        // COB_CODE: MOVE IDSV0001-USER-NAME
        //                              TO IDSI0011-USER-NAME
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011UserName(areaIdsv0001.getAreaComune().getUserName());
        // COB_CODE: IF IDSI0011-DATA-INIZIO-EFFETTO = 0
        //              END-IF
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataInizioEffetto() == 0) {
            // COB_CODE: IF IDSV0001-LETT-ULT-IMMAGINE-SI
            //              END-IF
            //           ELSE
            //                TO IDSI0011-DATA-INIZIO-EFFETTO
            //           END-IF
            if (areaIdsv0001.getAreaComune().getLettUltImmagine().isIdsv0001LettUltImmagineSi()) {
                // COB_CODE: IF IDSI0011-SELECT
                //           OR IDSI0011-FETCH-FIRST
                //           OR IDSI0011-FETCH-NEXT
                //                TO IDSI0011-DATA-COMPETENZA
                //           ELSE
                //                TO IDSI0011-DATA-INIZIO-EFFETTO
                //           END-IF
                if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isSelect() || ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchFirst() || ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchNext()) {
                    // COB_CODE: MOVE 99991230
                    //             TO IDSI0011-DATA-INIZIO-EFFETTO
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(99991230);
                    // COB_CODE: MOVE 999912304023595999
                    //             TO IDSI0011-DATA-COMPETENZA
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(999912304023595999L);
                }
                else {
                    // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
                    //             TO IDSI0011-DATA-INIZIO-EFFETTO
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
                }
            }
            else {
                // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
                //             TO IDSI0011-DATA-INIZIO-EFFETTO
                ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
            }
        }
        // COB_CODE: IF IDSI0011-DATA-FINE-EFFETTO = 0
        //              MOVE 99991231   TO IDSI0011-DATA-FINE-EFFETTO
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataFineEffetto() == 0) {
            // COB_CODE: MOVE 99991231   TO IDSI0011-DATA-FINE-EFFETTO
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(99991231);
        }
        // COB_CODE: IF IDSI0011-DATA-COMPETENZA = 0
        //                              TO IDSI0011-DATA-COMPETENZA
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataCompetenza() == 0) {
            // COB_CODE: MOVE IDSV0001-DATA-COMPETENZA
            //                           TO IDSI0011-DATA-COMPETENZA
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(areaIdsv0001.getAreaComune().getIdsv0001DataCompetenza());
        }
        // COB_CODE: MOVE IDSV0001-DATA-COMP-AGG-STOR
        //                              TO IDSI0011-DATA-COMP-AGG-STOR
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompAggStor(areaIdsv0001.getAreaComune().getIdsv0001DataCompAggStor());
        // COB_CODE: IF IDSI0011-TRATT-DEFAULT
        //                              TO IDSI0011-TRATTAMENTO-STORICITA
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().isTrattDefault()) {
            // COB_CODE: MOVE IDSV0001-TRATTAMENTO-STORICITA
            //                           TO IDSI0011-TRATTAMENTO-STORICITA
            ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattamentoStoricita(areaIdsv0001.getAreaComune().getTrattamentoStoricita().getTrattamentoStoricita());
        }
        // COB_CODE: MOVE IDSV0001-FORMATO-DATA-DB
        //                              TO IDSI0011-FORMATO-DATA-DB
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011FormatoDataDb().setIdsi0011FormatoDataDb(areaIdsv0001.getAreaComune().getFormatoDataDb().getFormatoDataDb());
        // COB_CODE: MOVE IDSV0001-LIVELLO-DEBUG
        //                              TO IDSI0011-LIVELLO-DEBUG
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloDebug().setIdsi0011LivelloDebugFormatted(areaIdsv0001.getAreaComune().getLivelloDebug().getIdsi0011LivelloDebugFormatted());
        // COB_CODE: MOVE 0             TO IDSI0011-ID-MOVI-ANNULLATO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011IdMoviAnnullato(0);
        // COB_CODE: SET IDSI0011-PTF-NEWLIFE          TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011IdentitaChiamante().setPtfNewlife();
        // COB_CODE: SET IDSV0012-STATUS-SHARED-MEM-KO TO TRUE
        ws.getIdsv00122().getStatusSharedMemory().setKo();
        // COB_CODE: MOVE 'IDSS0010'             TO IDSI0011-PGM
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011Pgm("IDSS0010");
        //     SET WS-ADDRESS-DIS TO ADDRESS OF IN-OUT-IDSS0010.
        // COB_CODE: CALL IDSI0011-PGM           USING IDSI0011-AREA
        //                                             IDSO0011-AREA.
        idss0010 = Idss0010.getInstance();
        idss0010.run(ws.getDispatcherVariables(), ws.getDispatcherVariables().getIdso0011Area());
        // COB_CODE: MOVE IDSO0011-SQLCODE-SIGNED
        //                                  TO IDSO0011-SQLCODE.
        ws.getDispatcherVariables().getIdso0011Area().setSqlcode(ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned());
    }

    /**Original name: S0300-RICERCA-GRAVITA-ERRORE<br>
	 * <pre>----------------------------------------------------------------*
	 *   ROUTINES GESTIONE ERRORI                                      *
	 * ----------------------------------------------------------------*
	 * MUOVO L'AREA DEL SERVIZIO NELL'AREA DATI DELL'AREA DI CONTESTO.
	 * ----->VALORIZZO I CAMPI DELLA IDSV0001</pre>*/
    private void s0300RicercaGravitaErrore() {
        Ieas9900 ieas9900 = null;
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR       TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE  'IEAS9900'              TO CALL-PGM
        ws.getIdsv0002().setCallPgm("IEAS9900");
        // COB_CODE: CALL CALL-PGM USING IDSV0001-AREA-CONTESTO
        //                               IEAI9901-AREA
        //                               IEAO9901-AREA
        //             ON EXCEPTION
        //                   THRU EX-S0310-ERRORE-FATALE
        //           END-CALL.
        try {
            ieas9900 = Ieas9900.getInstance();
            ieas9900.run(areaIdsv0001, ws.getIeai9901Area(), ws.getIeao9901Area());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
            areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
            // COB_CODE: PERFORM S0310-ERRORE-FATALE
            //              THRU EX-S0310-ERRORE-FATALE
            s0310ErroreFatale();
        }
        //SE LA CHIAMATA AL SERVIZIO HA ESITO POSITIVO (NESSUN ERRORE DI
        //SISTEMA, VALORIZZO L'AREA DI OUTPUT.
        //INCREMENTO L'INDICE DEGLI ERRORI
        // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
        areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
        //SE L'INDICE E' MINORE DI 10 ...
        // COB_CODE:      IF IDSV0001-MAX-ELE-ERRORI NOT GREATER 10
        //           *SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
        //                   END-IF
        //                ELSE
        //           *IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        //                       TO IDSV0001-DESC-ERRORE-ESTESA
        //                END-IF.
        if (areaIdsv0001.getMaxEleErrori() <= 10) {
            //SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
            // COB_CODE: IF IEAO9901-COD-ERRORE-990 = ZEROES
            //                      EX-S0300-IMPOSTA-ERRORE
            //           ELSE
            //                   IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
            //           END-IF
            if (Characters.EQ_ZERO.test(ws.getIeao9901Area().getCodErrore990Formatted())) {
                // COB_CODE: PERFORM S0300-IMPOSTA-ERRORE THRU
                //                   EX-S0300-IMPOSTA-ERRORE
                s0300ImpostaErrore();
            }
            else {
                // COB_CODE: MOVE 'IEAS9900'
                //             TO IDSV0001-COD-SERVIZIO-BE
                areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
                // COB_CODE: MOVE IEAO9901-LABEL-ERR-990
                //             TO IDSV0001-LABEL-ERR
                areaIdsv0001.getLogErrore().setLabelErr(ws.getIeao9901Area().getLabelErr990());
                // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
                //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
                // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
                areaIdsv0001.getEsito().setIdsv0001EsitoKo();
                // IMPOSTO IL CODICE ERRORE GESTITO ALL'INTERNO DEL PROGRAMMA
                // COB_CODE: MOVE IEAO9901-COD-ERRORE-990
                //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeao9901Area().getCodErrore990Formatted());
                // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
                //             TO IDSV0001-DESC-ERRORE-ESTESA
                //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
            }
        }
        else {
            //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
            // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
            //             TO IDSV0001-LABEL-ERR
            areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
            // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 'IEAS9900'
            //             TO IDSV0001-COD-SERVIZIO-BE
            areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
            // COB_CODE: MOVE 'OVERFLOW IMPAGINAZIONE ERRORI'
            //             TO IDSV0001-DESC-ERRORE-ESTESA
            areaIdsv0001.getLogErrore().setDescErroreEstesa("OVERFLOW IMPAGINAZIONE ERRORI");
        }
    }

    /**Original name: S0300-IMPOSTA-ERRORE<br>
	 * <pre>  se la gravit— di tutti gli errori dell'elaborazione
	 *   h minore di zero ( ad esempio -3) vuol dire che il return-code
	 *   dell'elaborazione complessiva deve essere abbassato da '08' a
	 *   '04'. Per fare cir utilizzo il flag IDSV0001-FORZ-RC-04
	 * IMPOSTO LA GRAVITA'</pre>*/
    private void s0300ImpostaErrore() {
        // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
        // COB_CODE: EVALUATE TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = -3
        //                       IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        //             WHEN  IEAO9901-LIV-GRAVITA = 0 OR 1 OR 2
        //                   SET IDSV0001-FORZ-RC-04-NO  TO TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = 3 OR 4
        //                   SET IDSV0001-ESITO-KO       TO TRUE
        //           END-EVALUATE
        if (ws.getIeao9901Area().getLivGravita() == -3) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-YES TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04Yes();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 2  TO
            //               IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
            areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)2));
        }
        else if (ws.getIeao9901Area().getLivGravita() == 0 || ws.getIeao9901Area().getLivGravita() == 1 || ws.getIeao9901Area().getLivGravita() == 2) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
        }
        else if (ws.getIeao9901Area().getLivGravita() == 3 || ws.getIeao9901Area().getLivGravita() == 4) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        }
        // IMPOSTO IL CODICE ERRORE
        // COB_CODE: MOVE IEAI9901-COD-ERRORE
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeai9901Area().getCodErroreFormatted());
        // SE IL LIVELLO DI GRAVITA' RESTITUITO DALLO IAS9900 E' MAGGIORE
        // DI 2 (ERRORE BLOCCANTE)...IMPOSTO L'ESITO E I DATI DI CONTESTO
        //RELATIVI ALL'ERRORE BLOCCANTE
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE
        //             TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR
        //             TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
        //             TO IDSV0001-DESC-ERRORE-ESTESA
        //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
        // COB_CODE: MOVE SPACES          TO IEAI9901-COD-SERVIZIO-BE
        //                                  IEAI9901-LABEL-ERR
        //                                   IEAI9901-PARAMETRI-ERR.
        ws.getIeai9901Area().setCodServizioBe("");
        ws.getIeai9901Area().setLabelErr("");
        ws.getIeai9901Area().setParametriErr("");
        // COB_CODE: MOVE ZEROES          TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErrore(0);
    }

    /**Original name: S0310-ERRORE-FATALE<br>
	 * <pre>IMPOSTO LA GRAVITA' ERRORE</pre>*/
    private void s0310ErroreFatale() {
        // COB_CODE: MOVE  3
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)3));
        // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
        areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        //IMPOSTO IL CODICE ERRORE (ERRORE FISSO)
        // COB_CODE: MOVE 900001
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErrore(900001);
        //IMPOSTO NOME DEL MODULO
        // COB_CODE: MOVE 'IEAS9900'               TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
        //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
        //              TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
        // COB_CODE: MOVE  'ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900'
        //              TO IDSV0001-DESC-ERRORE-ESTESA
        //                 IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
    }

    /**Original name: S0290-ERRORE-DI-SISTEMA<br>
	 * <pre>**-------------------------------------------------------------**
	 *     PROGRAMMA ..... IERP9902
	 *     TIPOLOGIA...... COPY
	 *     DESCRIZIONE.... ROUTINE GENERALIZZATA GESTIONE ERRORI CALL
	 * **-------------------------------------------------------------**</pre>*/
    private void s0290ErroreDiSistema() {
        // COB_CODE: MOVE '005006'           TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErroreFormatted("005006");
        // COB_CODE: MOVE CALL-DESC          TO IEAI9901-PARAMETRI-ERR.
        ws.getIeai9901Area().setParametriErr(ws.getIdsv0002().getCallDesc());
        // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
        //              THRU EX-S0300.
        s0300RicercaGravitaErrore();
    }

    /**Original name: VALORIZZA-OUTPUT-POG<br>
	 * <pre>----------------------------------------------------------------*
	 *   COPY DI VALORIZZAZIONE OUTPUT LETTURA DB
	 * ----------------------------------------------------------------*
	 *   --> PARAMETRO OGGETTO
	 * ------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    VALORIZZA OUTPUT COPY LCCVPOG3
	 *    ULTIMO AGG. 04 SET 2008
	 * ------------------------------------------------------------</pre>*/
    private void valorizzaOutputPog() {
        // COB_CODE: MOVE POG-ID-PARAM-OGG
        //             TO (SF)-ID-PTF(IX-TAB-POG)
        ws.getWpogTabParamOgg(ws.getIxIndici().getTabPog()).getLccvpog1().setIdPtf(ws.getParamOgg().getPogIdParamOgg());
        // COB_CODE: MOVE POG-ID-PARAM-OGG
        //             TO (SF)-ID-PARAM-OGG(IX-TAB-POG)
        ws.getWpogTabParamOgg(ws.getIxIndici().getTabPog()).getLccvpog1().getDati().setWpogIdParamOgg(ws.getParamOgg().getPogIdParamOgg());
        // COB_CODE: MOVE POG-ID-OGG
        //             TO (SF)-ID-OGG(IX-TAB-POG)
        ws.getWpogTabParamOgg(ws.getIxIndici().getTabPog()).getLccvpog1().getDati().setWpogIdOgg(ws.getParamOgg().getPogIdOgg());
        // COB_CODE: MOVE POG-TP-OGG
        //             TO (SF)-TP-OGG(IX-TAB-POG)
        ws.getWpogTabParamOgg(ws.getIxIndici().getTabPog()).getLccvpog1().getDati().setWpogTpOgg(ws.getParamOgg().getPogTpOgg());
        // COB_CODE: MOVE POG-ID-MOVI-CRZ
        //             TO (SF)-ID-MOVI-CRZ(IX-TAB-POG)
        ws.getWpogTabParamOgg(ws.getIxIndici().getTabPog()).getLccvpog1().getDati().setWpogIdMoviCrz(ws.getParamOgg().getPogIdMoviCrz());
        // COB_CODE: IF POG-ID-MOVI-CHIU-NULL = HIGH-VALUES
        //                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-POG)
        //           ELSE
        //                TO (SF)-ID-MOVI-CHIU(IX-TAB-POG)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamOgg().getPogIdMoviChiu().getPogIdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE POG-ID-MOVI-CHIU-NULL
            //             TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-POG)
            ws.getWpogTabParamOgg(ws.getIxIndici().getTabPog()).getLccvpog1().getDati().getWpogIdMoviChiu().setWpogIdMoviChiuNull(ws.getParamOgg().getPogIdMoviChiu().getPogIdMoviChiuNull());
        }
        else {
            // COB_CODE: MOVE POG-ID-MOVI-CHIU
            //             TO (SF)-ID-MOVI-CHIU(IX-TAB-POG)
            ws.getWpogTabParamOgg(ws.getIxIndici().getTabPog()).getLccvpog1().getDati().getWpogIdMoviChiu().setWpogIdMoviChiu(ws.getParamOgg().getPogIdMoviChiu().getPogIdMoviChiu());
        }
        // COB_CODE: MOVE POG-DT-INI-EFF
        //             TO (SF)-DT-INI-EFF(IX-TAB-POG)
        ws.getWpogTabParamOgg(ws.getIxIndici().getTabPog()).getLccvpog1().getDati().setWpogDtIniEff(ws.getParamOgg().getPogDtIniEff());
        // COB_CODE: MOVE POG-DT-END-EFF
        //             TO (SF)-DT-END-EFF(IX-TAB-POG)
        ws.getWpogTabParamOgg(ws.getIxIndici().getTabPog()).getLccvpog1().getDati().setWpogDtEndEff(ws.getParamOgg().getPogDtEndEff());
        // COB_CODE: MOVE POG-COD-COMP-ANIA
        //             TO (SF)-COD-COMP-ANIA(IX-TAB-POG)
        ws.getWpogTabParamOgg(ws.getIxIndici().getTabPog()).getLccvpog1().getDati().setWpogCodCompAnia(ws.getParamOgg().getPogCodCompAnia());
        // COB_CODE: IF POG-COD-PARAM-NULL = HIGH-VALUES
        //                TO (SF)-COD-PARAM-NULL(IX-TAB-POG)
        //           ELSE
        //                TO (SF)-COD-PARAM(IX-TAB-POG)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamOgg().getPogCodParamFormatted())) {
            // COB_CODE: MOVE POG-COD-PARAM-NULL
            //             TO (SF)-COD-PARAM-NULL(IX-TAB-POG)
            ws.getWpogTabParamOgg(ws.getIxIndici().getTabPog()).getLccvpog1().getDati().setWpogCodParam(ws.getParamOgg().getPogCodParam());
        }
        else {
            // COB_CODE: MOVE POG-COD-PARAM
            //             TO (SF)-COD-PARAM(IX-TAB-POG)
            ws.getWpogTabParamOgg(ws.getIxIndici().getTabPog()).getLccvpog1().getDati().setWpogCodParam(ws.getParamOgg().getPogCodParam());
        }
        // COB_CODE: IF POG-TP-PARAM-NULL = HIGH-VALUES
        //                TO (SF)-TP-PARAM-NULL(IX-TAB-POG)
        //           ELSE
        //                TO (SF)-TP-PARAM(IX-TAB-POG)
        //           END-IF
        if (Conditions.eq(ws.getParamOgg().getPogTpParam(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE POG-TP-PARAM-NULL
            //             TO (SF)-TP-PARAM-NULL(IX-TAB-POG)
            ws.getWpogTabParamOgg(ws.getIxIndici().getTabPog()).getLccvpog1().getDati().setWpogTpParam(ws.getParamOgg().getPogTpParam());
        }
        else {
            // COB_CODE: MOVE POG-TP-PARAM
            //             TO (SF)-TP-PARAM(IX-TAB-POG)
            ws.getWpogTabParamOgg(ws.getIxIndici().getTabPog()).getLccvpog1().getDati().setWpogTpParam(ws.getParamOgg().getPogTpParam());
        }
        // COB_CODE: IF POG-TP-D-NULL = HIGH-VALUES
        //                TO (SF)-TP-D-NULL(IX-TAB-POG)
        //           ELSE
        //                TO (SF)-TP-D(IX-TAB-POG)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamOgg().getPogTpDFormatted())) {
            // COB_CODE: MOVE POG-TP-D-NULL
            //             TO (SF)-TP-D-NULL(IX-TAB-POG)
            ws.getWpogTabParamOgg(ws.getIxIndici().getTabPog()).getLccvpog1().getDati().setWpogTpD(ws.getParamOgg().getPogTpD());
        }
        else {
            // COB_CODE: MOVE POG-TP-D
            //             TO (SF)-TP-D(IX-TAB-POG)
            ws.getWpogTabParamOgg(ws.getIxIndici().getTabPog()).getLccvpog1().getDati().setWpogTpD(ws.getParamOgg().getPogTpD());
        }
        // COB_CODE: IF POG-VAL-IMP-NULL = HIGH-VALUES
        //                TO (SF)-VAL-IMP-NULL(IX-TAB-POG)
        //           ELSE
        //                TO (SF)-VAL-IMP(IX-TAB-POG)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamOgg().getPogValImp().getPogValImpNullFormatted())) {
            // COB_CODE: MOVE POG-VAL-IMP-NULL
            //             TO (SF)-VAL-IMP-NULL(IX-TAB-POG)
            ws.getWpogTabParamOgg(ws.getIxIndici().getTabPog()).getLccvpog1().getDati().getWpogValImp().setWpogValImpNull(ws.getParamOgg().getPogValImp().getPogValImpNull());
        }
        else {
            // COB_CODE: MOVE POG-VAL-IMP
            //             TO (SF)-VAL-IMP(IX-TAB-POG)
            ws.getWpogTabParamOgg(ws.getIxIndici().getTabPog()).getLccvpog1().getDati().getWpogValImp().setWpogValImp(Trunc.toDecimal(ws.getParamOgg().getPogValImp().getPogValImp(), 15, 3));
        }
        // COB_CODE: IF POG-VAL-DT-NULL = HIGH-VALUES
        //                TO (SF)-VAL-DT-NULL(IX-TAB-POG)
        //           ELSE
        //                TO (SF)-VAL-DT(IX-TAB-POG)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamOgg().getPogValDt().getPogValDtNullFormatted())) {
            // COB_CODE: MOVE POG-VAL-DT-NULL
            //             TO (SF)-VAL-DT-NULL(IX-TAB-POG)
            ws.getWpogTabParamOgg(ws.getIxIndici().getTabPog()).getLccvpog1().getDati().getWpogValDt().setWpogValDtNull(ws.getParamOgg().getPogValDt().getPogValDtNull());
        }
        else {
            // COB_CODE: MOVE POG-VAL-DT
            //             TO (SF)-VAL-DT(IX-TAB-POG)
            ws.getWpogTabParamOgg(ws.getIxIndici().getTabPog()).getLccvpog1().getDati().getWpogValDt().setWpogValDt(ws.getParamOgg().getPogValDt().getPogValDt());
        }
        // COB_CODE: IF POG-VAL-TS-NULL = HIGH-VALUES
        //                TO (SF)-VAL-TS-NULL(IX-TAB-POG)
        //           ELSE
        //                TO (SF)-VAL-TS(IX-TAB-POG)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamOgg().getPogValTs().getPogValTsNullFormatted())) {
            // COB_CODE: MOVE POG-VAL-TS-NULL
            //             TO (SF)-VAL-TS-NULL(IX-TAB-POG)
            ws.getWpogTabParamOgg(ws.getIxIndici().getTabPog()).getLccvpog1().getDati().getWpogValTs().setWpogValTsNull(ws.getParamOgg().getPogValTs().getPogValTsNull());
        }
        else {
            // COB_CODE: MOVE POG-VAL-TS
            //             TO (SF)-VAL-TS(IX-TAB-POG)
            ws.getWpogTabParamOgg(ws.getIxIndici().getTabPog()).getLccvpog1().getDati().getWpogValTs().setWpogValTs(Trunc.toDecimal(ws.getParamOgg().getPogValTs().getPogValTs(), 14, 9));
        }
        // COB_CODE: MOVE POG-VAL-TXT
        //             TO (SF)-VAL-TXT(IX-TAB-POG)
        ws.getWpogTabParamOgg(ws.getIxIndici().getTabPog()).getLccvpog1().getDati().setWpogValTxt(ws.getParamOgg().getPogValTxt());
        // COB_CODE: IF POG-VAL-FL-NULL = HIGH-VALUES
        //                TO (SF)-VAL-FL-NULL(IX-TAB-POG)
        //           ELSE
        //                TO (SF)-VAL-FL(IX-TAB-POG)
        //           END-IF
        if (Conditions.eq(ws.getParamOgg().getPogValFl(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE POG-VAL-FL-NULL
            //             TO (SF)-VAL-FL-NULL(IX-TAB-POG)
            ws.getWpogTabParamOgg(ws.getIxIndici().getTabPog()).getLccvpog1().getDati().setWpogValFl(ws.getParamOgg().getPogValFl());
        }
        else {
            // COB_CODE: MOVE POG-VAL-FL
            //             TO (SF)-VAL-FL(IX-TAB-POG)
            ws.getWpogTabParamOgg(ws.getIxIndici().getTabPog()).getLccvpog1().getDati().setWpogValFl(ws.getParamOgg().getPogValFl());
        }
        // COB_CODE: IF POG-VAL-NUM-NULL = HIGH-VALUES
        //                TO (SF)-VAL-NUM-NULL(IX-TAB-POG)
        //           ELSE
        //                TO (SF)-VAL-NUM(IX-TAB-POG)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamOgg().getPogValNum().getPogValNumNullFormatted())) {
            // COB_CODE: MOVE POG-VAL-NUM-NULL
            //             TO (SF)-VAL-NUM-NULL(IX-TAB-POG)
            ws.getWpogTabParamOgg(ws.getIxIndici().getTabPog()).getLccvpog1().getDati().getWpogValNum().setWpogValNumNull(ws.getParamOgg().getPogValNum().getPogValNumNull());
        }
        else {
            // COB_CODE: MOVE POG-VAL-NUM
            //             TO (SF)-VAL-NUM(IX-TAB-POG)
            ws.getWpogTabParamOgg(ws.getIxIndici().getTabPog()).getLccvpog1().getDati().getWpogValNum().setWpogValNum(ws.getParamOgg().getPogValNum().getPogValNum());
        }
        // COB_CODE: IF POG-VAL-PC-NULL = HIGH-VALUES
        //                TO (SF)-VAL-PC-NULL(IX-TAB-POG)
        //           ELSE
        //                TO (SF)-VAL-PC(IX-TAB-POG)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getParamOgg().getPogValPc().getPogValPcNullFormatted())) {
            // COB_CODE: MOVE POG-VAL-PC-NULL
            //             TO (SF)-VAL-PC-NULL(IX-TAB-POG)
            ws.getWpogTabParamOgg(ws.getIxIndici().getTabPog()).getLccvpog1().getDati().getWpogValPc().setWpogValPcNull(ws.getParamOgg().getPogValPc().getPogValPcNull());
        }
        else {
            // COB_CODE: MOVE POG-VAL-PC
            //             TO (SF)-VAL-PC(IX-TAB-POG)
            ws.getWpogTabParamOgg(ws.getIxIndici().getTabPog()).getLccvpog1().getDati().getWpogValPc().setWpogValPc(Trunc.toDecimal(ws.getParamOgg().getPogValPc().getPogValPc(), 14, 9));
        }
        // COB_CODE: MOVE POG-DS-RIGA
        //             TO (SF)-DS-RIGA(IX-TAB-POG)
        ws.getWpogTabParamOgg(ws.getIxIndici().getTabPog()).getLccvpog1().getDati().setWpogDsRiga(ws.getParamOgg().getPogDsRiga());
        // COB_CODE: MOVE POG-DS-OPER-SQL
        //             TO (SF)-DS-OPER-SQL(IX-TAB-POG)
        ws.getWpogTabParamOgg(ws.getIxIndici().getTabPog()).getLccvpog1().getDati().setWpogDsOperSql(ws.getParamOgg().getPogDsOperSql());
        // COB_CODE: MOVE POG-DS-VER
        //             TO (SF)-DS-VER(IX-TAB-POG)
        ws.getWpogTabParamOgg(ws.getIxIndici().getTabPog()).getLccvpog1().getDati().setWpogDsVer(ws.getParamOgg().getPogDsVer());
        // COB_CODE: MOVE POG-DS-TS-INI-CPTZ
        //             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-POG)
        ws.getWpogTabParamOgg(ws.getIxIndici().getTabPog()).getLccvpog1().getDati().setWpogDsTsIniCptz(ws.getParamOgg().getPogDsTsIniCptz());
        // COB_CODE: MOVE POG-DS-TS-END-CPTZ
        //             TO (SF)-DS-TS-END-CPTZ(IX-TAB-POG)
        ws.getWpogTabParamOgg(ws.getIxIndici().getTabPog()).getLccvpog1().getDati().setWpogDsTsEndCptz(ws.getParamOgg().getPogDsTsEndCptz());
        // COB_CODE: MOVE POG-DS-UTENTE
        //             TO (SF)-DS-UTENTE(IX-TAB-POG)
        ws.getWpogTabParamOgg(ws.getIxIndici().getTabPog()).getLccvpog1().getDati().setWpogDsUtente(ws.getParamOgg().getPogDsUtente());
        // COB_CODE: MOVE POG-DS-STATO-ELAB
        //             TO (SF)-DS-STATO-ELAB(IX-TAB-POG).
        ws.getWpogTabParamOgg(ws.getIxIndici().getTabPog()).getLccvpog1().getDati().setWpogDsStatoElab(ws.getParamOgg().getPogDsStatoElab());
    }

    /**Original name: INIZIA-TOT-POG<br>
	 * <pre>----------------------------------------------------------------*
	 *     COPY INIZIALIZZAZIONE TABELLE
	 * ----------------------------------------------------------------*
	 *   --> PARAMETRO OGGETTO
	 * ------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    INIZIALIZZAZIONE CAMPI NULL COPY LCCVPOG4
	 *    ULTIMO AGG. 02 SET 2008
	 * ------------------------------------------------------------</pre>*/
    private void iniziaTotPog() {
        // COB_CODE: PERFORM INIZIA-ZEROES-POG THRU INIZIA-ZEROES-POG-EX
        //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LCCVPOG4:line=10, because the code is unreachable.
        // COB_CODE: PERFORM INIZIA-SPACES-POG THRU INIZIA-SPACES-POG-EX
        //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LCCVPOG4:line=12, because the code is unreachable.
        // COB_CODE: PERFORM INIZIA-NULL-POG THRU INIZIA-NULL-POG-EX.
        iniziaNullPog();
    }

    /**Original name: INIZIA-NULL-POG<br>*/
    private void iniziaNullPog() {
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-POG)
        ws.getWpogTabParamOgg(ws.getIxIndici().getTabPog()).getLccvpog1().getDati().getWpogIdMoviChiu().setWpogIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpogIdMoviChiu.Len.WPOG_ID_MOVI_CHIU_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-COD-PARAM-NULL(IX-TAB-POG)
        ws.getWpogTabParamOgg(ws.getIxIndici().getTabPog()).getLccvpog1().getDati().setWpogCodParam(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpogDati.Len.WPOG_COD_PARAM));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TP-PARAM-NULL(IX-TAB-POG)
        ws.getWpogTabParamOgg(ws.getIxIndici().getTabPog()).getLccvpog1().getDati().setWpogTpParam(Types.HIGH_CHAR_VAL);
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TP-D-NULL(IX-TAB-POG)
        ws.getWpogTabParamOgg(ws.getIxIndici().getTabPog()).getLccvpog1().getDati().setWpogTpD(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpogDati.Len.WPOG_TP_D));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-VAL-IMP-NULL(IX-TAB-POG)
        ws.getWpogTabParamOgg(ws.getIxIndici().getTabPog()).getLccvpog1().getDati().getWpogValImp().setWpogValImpNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpogValImp.Len.WPOG_VAL_IMP_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-VAL-DT-NULL(IX-TAB-POG)
        ws.getWpogTabParamOgg(ws.getIxIndici().getTabPog()).getLccvpog1().getDati().getWpogValDt().setWpogValDtNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpogValDt.Len.WPOG_VAL_DT_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-VAL-TS-NULL(IX-TAB-POG)
        ws.getWpogTabParamOgg(ws.getIxIndici().getTabPog()).getLccvpog1().getDati().getWpogValTs().setWpogValTsNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpogValTs.Len.WPOG_VAL_TS_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-VAL-TXT(IX-TAB-POG)
        ws.getWpogTabParamOgg(ws.getIxIndici().getTabPog()).getLccvpog1().getDati().setWpogValTxt(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpogDati.Len.WPOG_VAL_TXT));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-VAL-FL-NULL(IX-TAB-POG)
        ws.getWpogTabParamOgg(ws.getIxIndici().getTabPog()).getLccvpog1().getDati().setWpogValFl(Types.HIGH_CHAR_VAL);
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-VAL-NUM-NULL(IX-TAB-POG)
        ws.getWpogTabParamOgg(ws.getIxIndici().getTabPog()).getLccvpog1().getDati().getWpogValNum().setWpogValNumNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpogValNum.Len.WPOG_VAL_NUM_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-VAL-PC-NULL(IX-TAB-POG).
        ws.getWpogTabParamOgg(ws.getIxIndici().getTabPog()).getLccvpog1().getDati().getWpogValPc().setWpogValPcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpogValPc.Len.WPOG_VAL_PC_NULL));
    }

    /**Original name: DISPLAY-LABEL<br>
	 * <pre>----------------------------------------------------------------*
	 *  DIPLAY LABEL
	 * ----------------------------------------------------------------*</pre>*/
    private void displayLabel() {
        // COB_CODE:      IF (IDSV0001-DEBUG-BASSO OR IDSV0001-DEBUG-ESASPERATO) AND
        //                NOT IDSV0001-ON-LINE
        //           *
        //           *       DISPLAY WK-LABEL-ERR
        //                   CONTINUE
        //           *
        //                END-IF.
        if ((areaIdsv0001.getAreaComune().getLivelloDebug().isIdsv0001DebugBasso() || areaIdsv0001.getAreaComune().getLivelloDebug().isIdsv0001DebugEsasperato()) && !areaIdsv0001.getAreaComune().getModalitaEsecutiva().isIdsv0001OnLine()) {
        //
        //       DISPLAY WK-LABEL-ERR
        // COB_CODE: CONTINUE
        //continue
        //
        }
    }

    /**Original name: ESEGUI-DISPLAY<br>
	 * <pre>----------------------------------------------------------------*
	 *  DIPLAY ESITO PRODOTTO
	 * ----------------------------------------------------------------*
	 * DISPLAY-ESITO-PROD.
	 *     IF IDSV0001-DEBUG-ESASPERATO  AND  NOT IDSV0001-ON-LINE
	 *       DISPLAY 'ESITO PRODOTTO : ' ISPC0211-ESITO
	 *       DISPLAY 'NUMERO ELEMENTI: ' ISPC0211-ERR-NUM-ELE
	 *       PERFORM VARYING TEST-I FROM 1 BY 1
	 *         UNTIL TEST-I > ISPC0211-ERR-NUM-ELE
	 *          DISPLAY 'DESCRIZIONE ERR: '
	 *                ISPC0211-DESCRIZIONE-ERR(TEST-I)
	 *       END-PERFORM
	 *     END-IF.
	 * DISPLAY-ESITO-PROD-EX.
	 *     EXIT.
	 * ----------------------------------------------------------------*
	 *  DIPLAY COMPONENTI OUTPUT DI PRODOTTO
	 * ----------------------------------------------------------------*
	 * DISPLAY-COMPONENTE.
	 *     IF (IDSV0001-DEBUG-MEDIO OR IDSV0001-DEBUG-ESASPERATO) AND
	 *     NOT IDSV0001-ON-LINE
	 *         DISPLAY ' TIPO-LIVELLO : '
	 *                   ISPC0211-TIPO-LIVELLO(IX-AREA-ISPC0211)
	 *         DISPLAY ' CODICE-LIVELLO: '
	 *                   ISPC0211-CODICE-LIVELLO(IX-AREA-ISPC0211)
	 *         MOVE ISPC0211-IDENT-LIVELLO(IX-AREA-ISPC0211)
	 *           TO WK-ID
	 *         DISPLAY ' ID-LIVELLO :'   WK-ID
	 *         MOVE ISPC0211-TIPO-DATO
	 *             (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
	 *           TO WS-TP-DATO
	 *         DISPLAY 'TIPO-DATO: ' WS-TP-DATO
	 *         DISPLAY 'COMPONENTE: '
	 *                  ISPC0211-CODICE-VARIABILE
	 *                 (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
	 *         EVALUATE TRUE
	 *           WHEN TD-IMPORTO
	 *              DISPLAY ' IMPORTO : '
	 *                        ISPC0211-VALORE-IMP
	 *                       (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
	 *           WHEN TD-PERC-CENTESIMI
	 *              DISPLAY ' PERCENTUALE : '
	 *                        ISPC0211-VALORE-PERC
	 *                       (IX-AREA-ISPC0211, IX-COMP-ISPC0211)
	 *           WHEN OTHER
	 *              DISPLAY '? ' WS-TP-DATO
	 *          END-EVALUATE
	 *     END-IF.
	 * DISPLAY-COMPONENTE-EX.
	 *     EXIT.
	 * ----------------------------------------------------------------*
	 *     ROUTINES DI GESTIONE DISPLAY
	 * ----------------------------------------------------------------*</pre>*/
    private void eseguiDisplay() {
        Idss8880 idss8880 = null;
        GenericParam idsv8888DisplayAddress = null;
        // COB_CODE: IF IDSV0001-ANY-TUNING-DBG AND
        //              IDSV8888-ANY-TUNING-DBG
        //              END-IF
        //           ELSE
        //              END-IF
        //           END-IF.
        if (areaIdsv0001.getAreaComune().getLivelloDebug().isAnyTuningDbg() && ws.getIdsv8888().getLivelloDebug().isAnyTuningDbg()) {
            // COB_CODE: IF IDSV0001-TOT-TUNING-DBG
            //              INITIALIZE IDSV8888-STR-PERFORMANCE-DBG
            //           ELSE
            //              END-IF
            //           END-IF
            if (areaIdsv0001.getAreaComune().getLivelloDebug().isTotTuningDbg()) {
                // COB_CODE: IF IDSV8888-STRESS-TEST-DBG
                //              CONTINUE
                //           ELSE
                //              END-IF
                //           END-IF
                if (ws.getIdsv8888().getLivelloDebug().isStressTestDbg()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else if (ws.getIdsv8888().getLivelloDebug().isComCobJavDbg()) {
                    // COB_CODE: IF IDSV8888-COM-COB-JAV-DBG
                    //              SET IDSV8888-COM-COB-JAV   TO TRUE
                    //           ELSE
                    //              END-IF
                    //           END-IF
                    // COB_CODE: SET IDSV8888-COM-COB-JAV   TO TRUE
                    ws.getIdsv8888().getStrPerformanceDbg().setComCobJav();
                }
                else if (ws.getIdsv8888().getLivelloDebug().isBusinessDbg()) {
                    // COB_CODE: IF IDSV8888-BUSINESS-DBG
                    //              SET IDSV8888-BUSINESS   TO TRUE
                    //           ELSE
                    //              END-IF
                    //           END-IF
                    // COB_CODE: SET IDSV8888-BUSINESS   TO TRUE
                    ws.getIdsv8888().getStrPerformanceDbg().setBusiness();
                }
                else if (ws.getIdsv8888().getLivelloDebug().isArchBatchDbg()) {
                    // COB_CODE: IF IDSV8888-ARCH-BATCH-DBG
                    //              SET IDSV8888-ARCH-BATCH TO TRUE
                    //           ELSE
                    //              MOVE 'ERRO'   TO IDSV8888-FASE
                    //           END-IF
                    // COB_CODE: SET IDSV8888-ARCH-BATCH TO TRUE
                    ws.getIdsv8888().getStrPerformanceDbg().setArchBatch();
                }
                else {
                    // COB_CODE: MOVE 'ERRO'   TO IDSV8888-FASE
                    ws.getIdsv8888().getStrPerformanceDbg().setFase("ERRO");
                }
                // COB_CODE: CALL IDSV8888-CALL-TIMESTAMP
                //                USING IDSV8888-TIMESTAMP
                DynamicCall.invoke(ws.getIdsv8888().getCallTimestamp(), new BasicBytesClass(ws.getIdsv8888().getStrPerformanceDbg().getArray(), Idsv8888StrPerformanceDbg.Pos.TIMESTAMP_FLD - 1));
                // COB_CODE: SET IDSV8888-DISPLAY-ADDRESS
                //                TO ADDRESS OF IDSV8888-STR-PERFORMANCE-DBG
                ws.getIdsv8888().setDisplayAddress(pointerManager.addressOf(ws.getIdsv8888().getStrPerformanceDbg()));
                // COB_CODE: CALL IDSV8888-CALL-DISPLAY
                //                USING IDSV8888-DISPLAY-ADDRESS
                idss8880 = Idss8880.getInstance();
                idsv8888DisplayAddress = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getIdsv8888().getDisplayAddress()));
                idss8880.run(idsv8888DisplayAddress);
                ws.getIdsv8888().setDisplayAddressFromBuffer(idsv8888DisplayAddress.getByteData());
                // COB_CODE: INITIALIZE IDSV8888-STR-PERFORMANCE-DBG
                initStrPerformanceDbg();
            }
            else if (ws.getIdsv8888().getLivelloDebug().getLivelloDebug() == areaIdsv0001.getAreaComune().getLivelloDebug().getIdsi0011LivelloDebug()) {
                // COB_CODE: IF IDSV8888-LIVELLO-DEBUG =
                //              IDSV0001-LIVELLO-DEBUG
                //              INITIALIZE IDSV8888-STR-PERFORMANCE-DBG
                //           END-IF
                // COB_CODE: IF IDSV8888-STRESS-TEST-DBG
                //              CONTINUE
                //           ELSE
                //              END-IF
                //           END-IF
                if (ws.getIdsv8888().getLivelloDebug().isStressTestDbg()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else if (ws.getIdsv8888().getLivelloDebug().isComCobJavDbg()) {
                    // COB_CODE: IF IDSV8888-COM-COB-JAV-DBG
                    //              SET IDSV8888-COM-COB-JAV   TO TRUE
                    //           ELSE
                    //              END-IF
                    //           END-IF
                    // COB_CODE: SET IDSV8888-COM-COB-JAV   TO TRUE
                    ws.getIdsv8888().getStrPerformanceDbg().setComCobJav();
                }
                else if (ws.getIdsv8888().getLivelloDebug().isBusinessDbg()) {
                    // COB_CODE: IF IDSV8888-BUSINESS-DBG
                    //              SET IDSV8888-BUSINESS   TO TRUE
                    //           ELSE
                    //              SET IDSV8888-ARCH-BATCH TO TRUE
                    //           END-IF
                    // COB_CODE: SET IDSV8888-BUSINESS   TO TRUE
                    ws.getIdsv8888().getStrPerformanceDbg().setBusiness();
                }
                else {
                    // COB_CODE: SET IDSV8888-ARCH-BATCH TO TRUE
                    ws.getIdsv8888().getStrPerformanceDbg().setArchBatch();
                }
                // COB_CODE: CALL IDSV8888-CALL-TIMESTAMP
                //                USING IDSV8888-TIMESTAMP
                DynamicCall.invoke(ws.getIdsv8888().getCallTimestamp(), new BasicBytesClass(ws.getIdsv8888().getStrPerformanceDbg().getArray(), Idsv8888StrPerformanceDbg.Pos.TIMESTAMP_FLD - 1));
                // COB_CODE: SET IDSV8888-DISPLAY-ADDRESS
                //               TO ADDRESS OF IDSV8888-STR-PERFORMANCE-DBG
                ws.getIdsv8888().setDisplayAddress(pointerManager.addressOf(ws.getIdsv8888().getStrPerformanceDbg()));
                // COB_CODE: CALL IDSV8888-CALL-DISPLAY
                //                USING IDSV8888-DISPLAY-ADDRESS
                idss8880 = Idss8880.getInstance();
                idsv8888DisplayAddress = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getIdsv8888().getDisplayAddress()));
                idss8880.run(idsv8888DisplayAddress);
                ws.getIdsv8888().setDisplayAddressFromBuffer(idsv8888DisplayAddress.getByteData());
                // COB_CODE: INITIALIZE IDSV8888-STR-PERFORMANCE-DBG
                initStrPerformanceDbg();
            }
        }
        else if (areaIdsv0001.getAreaComune().getLivelloDebug().isAnyApplDbg() && ws.getIdsv8888().getLivelloDebug().isAnyApplDbg()) {
            // COB_CODE: IF IDSV0001-ANY-APPL-DBG AND
            //              IDSV8888-ANY-APPL-DBG
            //               END-IF
            //           END-IF
            // COB_CODE: IF IDSV8888-LIVELLO-DEBUG <=
            //              IDSV0001-LIVELLO-DEBUG
            //              MOVE SPACES TO IDSV8888-AREA-DISPLAY
            //           END-IF
            if (ws.getIdsv8888().getLivelloDebug().getLivelloDebug() <= areaIdsv0001.getAreaComune().getLivelloDebug().getIdsi0011LivelloDebug()) {
                // COB_CODE: SET IDSV8888-DISPLAY-ADDRESS
                //               TO ADDRESS OF IDSV8888-AREA-DISPLAY
                ws.getIdsv8888().setDisplayAddress(pointerManager.addressOf(ws.getIdsv8888().getAreaDisplay()));
                // COB_CODE: CALL IDSV8888-CALL-DISPLAY
                //                USING IDSV8888-DISPLAY-ADDRESS
                idss8880 = Idss8880.getInstance();
                idsv8888DisplayAddress = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getIdsv8888().getDisplayAddress()));
                idss8880.run(idsv8888DisplayAddress);
                ws.getIdsv8888().setDisplayAddressFromBuffer(idsv8888DisplayAddress.getByteData());
                // COB_CODE: MOVE SPACES TO IDSV8888-AREA-DISPLAY
                ws.getIdsv8888().getAreaDisplay().setAreaDisplay("");
            }
        }
        // COB_CODE: SET IDSV8888-NO-DEBUG              TO TRUE.
        ws.getIdsv8888().getLivelloDebug().setNoDebug();
    }

    /**Original name: VAL-SCHEDE-ISPC0211<br>
	 * <pre>----------------------------------------------------------------*
	 *     COPY PER VALORIZZAZIONE SCHEDE SERVIZIO ISPS0211
	 * ----------------------------------------------------------------*
	 * ----------------------------------------------------------------*
	 *     VALORIZZAZIONE SCHEDE ISPC0211
	 * ----------------------------------------------------------------*
	 * --> VALORIZZAZIONE OCCURS AREA INPUT SERVIZIO CALCOLI E CONTROLLI
	 * --> CON L'OUTPUT DEL VALORIZZATORE VARIABILI</pre>*/
    private void valSchedeIspc0211() {
        // COB_CODE: MOVE WSKD-DEE
        //             TO ISPC0211-DEE
        ws.getAreaIoIspc0211().getIspc0211DatiInput().setDee(wskdAreaScheda.getWskdDee());
        // COB_CODE: PERFORM AREA-SCHEDA-P-ISPC0211
        //              THRU AREA-SCHEDA-P-ISPC0211-EX
        //           VARYING IX-AREA-SCHEDA-P FROM 1 BY 1
        //             UNTIL IX-AREA-SCHEDA-P > WSKD-ELE-LIVELLO-MAX-P
        ws.getIxIndici().setAreaSchedaP(((short)1));
        while (!(ws.getIxIndici().getAreaSchedaP() > wskdAreaScheda.getWskdEleLivelloMaxP())) {
            areaSchedaPIspc0211();
            ws.getIxIndici().setAreaSchedaP(Trunc.toShort(ws.getIxIndici().getAreaSchedaP() + 1, 4));
        }
        // COB_CODE: MOVE WSKD-ELE-LIVELLO-MAX-P   TO ISPC0211-ELE-MAX-SCHEDA-P
        ws.getAreaIoIspc0211().setIspc0211EleMaxSchedaP(TruncAbs.toShort(wskdAreaScheda.getWskdEleLivelloMaxP(), 4));
        // COB_CODE: PERFORM AREA-SCHEDA-T-ISPC0211
        //              THRU AREA-SCHEDA-T-ISPC0211-EX
        //           VARYING IX-AREA-SCHEDA-T FROM 1 BY 1
        //             UNTIL IX-AREA-SCHEDA-T > WSKD-ELE-LIVELLO-MAX-T
        ws.getIxIndici().setAreaSchedaT(((short)1));
        while (!(ws.getIxIndici().getAreaSchedaT() > wskdAreaScheda.getWskdEleLivelloMaxT())) {
            areaSchedaTIspc0211();
            ws.getIxIndici().setAreaSchedaT(Trunc.toShort(ws.getIxIndici().getAreaSchedaT() + 1, 4));
        }
        // COB_CODE: MOVE WSKD-ELE-LIVELLO-MAX-T   TO ISPC0211-ELE-MAX-SCHEDA-T.
        ws.getAreaIoIspc0211().setIspc0211EleMaxSchedaT(TruncAbs.toShort(wskdAreaScheda.getWskdEleLivelloMaxT(), 4));
    }

    /**Original name: AREA-SCHEDA-P-ISPC0211<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZAZIONE AREA SCHEDA P
	 * ----------------------------------------------------------------*</pre>*/
    private void areaSchedaPIspc0211() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE WSKD-TP-LIVELLO-P(IX-AREA-SCHEDA-P)
        //             TO ISPC0211-TIPO-LIVELLO-P(IX-AREA-SCHEDA-P)
        ws.getAreaIoIspc0211().getIspc0211TabValP().setIspc0211TipoLivelloP(ws.getIxIndici().getAreaSchedaP(), wskdAreaScheda.getWskdTabValP().getTpLivelloP(ws.getIxIndici().getAreaSchedaP()));
        // COB_CODE: MOVE WSKD-COD-LIVELLO-P(IX-AREA-SCHEDA-P)
        //             TO ISPC0211-CODICE-LIVELLO-P(IX-AREA-SCHEDA-P)
        ws.getAreaIoIspc0211().getIspc0211TabValP().setIspc0211CodiceLivelloP(ws.getIxIndici().getAreaSchedaP(), wskdAreaScheda.getWskdTabValP().getCodLivelloP(ws.getIxIndici().getAreaSchedaP()));
        // COB_CODE: MOVE WSKD-ID-LIVELLO-P(IX-AREA-SCHEDA-P)
        //             TO ISPC0211-IDENT-LIVELLO-P(IX-AREA-SCHEDA-P)
        ws.getAreaIoIspc0211().getIspc0211TabValP().setIspc0211IdentLivelloPFormatted(ws.getIxIndici().getAreaSchedaP(), wskdAreaScheda.getWskdTabValP().getIdLivelloPFormatted(ws.getIxIndici().getAreaSchedaP()));
        // COB_CODE: MOVE WSKD-DT-INIZ-PROD-P(IX-AREA-SCHEDA-P)
        //             TO ISPC0211-DT-INIZ-PROD(IX-AREA-SCHEDA-P)
        ws.getAreaIoIspc0211().getIspc0211TabValP().setIspc0211DtInizProd(ws.getIxIndici().getAreaSchedaP(), wskdAreaScheda.getWskdTabValP().getDtInizProdP(ws.getIxIndici().getAreaSchedaP()));
        // COB_CODE: MOVE WSKD-COD-TIPO-OPZIONE-P(IX-AREA-SCHEDA-P)
        //             TO ISPC0211-CODICE-OPZIONE-P(IX-AREA-SCHEDA-P)
        ws.getAreaIoIspc0211().getIspc0211TabValP().setIspc0211CodiceOpzioneP(ws.getIxIndici().getAreaSchedaP(), wskdAreaScheda.getWskdTabValP().getCodTipoOpzioneP(ws.getIxIndici().getAreaSchedaP()));
        // COB_CODE:      IF WSKD-COD-RGM-FISC-P(IX-AREA-SCHEDA-P) NOT EQUAL SPACES
        //                                                         AND LOW-VALUE
        //                                                         AND HIGH-VALUE
        //           *
        //                    TO ISPC0211-COD-RGM-FISC(IX-AREA-SCHEDA-P)
        //           *
        //                END-IF
        if (!Characters.EQ_SPACE.test(wskdAreaScheda.getWskdTabValP().getCodRgmFiscP(ws.getIxIndici().getAreaSchedaP())) && !Characters.EQ_LOW.test(wskdAreaScheda.getWskdTabValP().getCodRgmFiscPFormatted(ws.getIxIndici().getAreaSchedaP())) && !Characters.EQ_HIGH.test(wskdAreaScheda.getWskdTabValP().getCodRgmFiscPFormatted(ws.getIxIndici().getAreaSchedaP()))) {
            //
            // COB_CODE: MOVE WSKD-COD-RGM-FISC-P(IX-AREA-SCHEDA-P)
            //             TO ISPC0211-COD-RGM-FISC(IX-AREA-SCHEDA-P)
            ws.getAreaIoIspc0211().getIspc0211TabValP().setIspc0211CodRgmFiscFormatted(ws.getIxIndici().getAreaSchedaP(), wskdAreaScheda.getWskdTabValP().getCodRgmFiscPFormatted(ws.getIxIndici().getAreaSchedaP()));
            //
        }
        //
        //--> VALORIZZAZIONE DELL'AREA VARIABILI DI INPUT DEL SERVIZIO
        //--> CALCOLI E CONTROLLI
        //
        // COB_CODE: MOVE ZERO
        //             TO ISPC0211-NUM-COMPON-MAX-ELE-P(IX-AREA-SCHEDA-P)
        ws.getAreaIoIspc0211().getIspc0211TabValP().setIspc0211NumComponMaxEleP(ws.getIxIndici().getAreaSchedaP(), ((short)0));
        // COB_CODE: PERFORM AREA-VAR-P-ISPC0211
        //              THRU AREA-VAR-P-ISPC0211-EX
        //           VARYING IX-TAB-VAR-P FROM 1 BY 1
        //             UNTIL IX-TAB-VAR-P >
        //                   WSKD-ELE-VARIABILI-MAX-P(IX-AREA-SCHEDA-P)
        //                OR IX-TAB-VAR-P > WK-ISPC0211-NUM-COMPON-MAX-P
        ws.getIxIndici().setTabVarP(((short)1));
        while (!(ws.getIxIndici().getTabVarP() > wskdAreaScheda.getWskdTabValP().getEleVariabiliMaxP(ws.getIxIndici().getAreaSchedaP()) || ws.getIxIndici().getTabVarP() > ws.getWkIspcMax().getIspc0211NumComponMaxP())) {
            areaVarPIspc0211();
            ws.getIxIndici().setTabVarP(Trunc.toShort(ws.getIxIndici().getTabVarP() + 1, 4));
        }
        //
        //--> SEGNALO CHE LE VARIABILI FORNITEMI SONO IN NUMERO
        //--> MAGGIORE DEL NUMERO CONSENTITO DALL'AREA ISPC0211
        //
        // COB_CODE: IF WSKD-ELE-VARIABILI-MAX-P(IX-AREA-SCHEDA-P) >
        //                             WK-ISPC0211-NUM-COMPON-MAX-P
        //                     THRU EX-S0300
        //           END-IF.
        if (wskdAreaScheda.getWskdTabValP().getEleVariabiliMaxP(ws.getIxIndici().getAreaSchedaP()) > ws.getWkIspcMax().getIspc0211NumComponMaxP()) {
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S12100-AREA-SCHEDA-P'
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S12100-AREA-SCHEDA-P");
            // COB_CODE: MOVE '005247'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005247");
            // COB_CODE: STRING 'NUMERO VARIABILI DI PRODOTTO '
            //                  'SUPERA LIMITE PREVISTO'
            //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "NUMERO VARIABILI DI PRODOTTO ", "SUPERA LIMITE PREVISTO");
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: AREA-VAR-P-ISPC0211<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZAZIONE DELL'AREA VARIABILI DI PRODOTTO
	 *     DI INPUT DEL SERVIZIO CALCOLI E CONTROLLI
	 * ----------------------------------------------------------------*</pre>*/
    private void areaVarPIspc0211() {
        // COB_CODE: ADD 1 TO ISPC0211-NUM-COMPON-MAX-ELE-P(IX-AREA-SCHEDA-P)
        ws.getAreaIoIspc0211().getIspc0211TabValP().setIspc0211NumComponMaxEleP(ws.getIxIndici().getAreaSchedaP(), Trunc.toShort(1 + ws.getAreaIoIspc0211().getIspc0211TabValP().getIspc0211NumComponMaxEleP(ws.getIxIndici().getAreaSchedaP()), 3));
        // COB_CODE: MOVE WSKD-COD-VARIABILE-P(IX-AREA-SCHEDA-P, IX-TAB-VAR-P)
        //             TO ISPC0211-CODICE-VARIABILE-P
        //               (IX-AREA-SCHEDA-P, IX-TAB-VAR-P)
        ws.getAreaIoIspc0211().getIspc0211TabValP().setIspc0211CodiceVariabileP(ws.getIxIndici().getAreaSchedaP(), ws.getIxIndici().getTabVarP(), wskdAreaScheda.getWskdTabValP().getCodVariabileP(ws.getIxIndici().getAreaSchedaP(), ws.getIxIndici().getTabVarP()));
        // COB_CODE: MOVE WSKD-TP-DATO-P(IX-AREA-SCHEDA-P, IX-TAB-VAR-P)
        //             TO ISPC0211-TIPO-DATO-P(IX-AREA-SCHEDA-P, IX-TAB-VAR-P)
        ws.getAreaIoIspc0211().getIspc0211TabValP().setIspc0211TipoDatoP(ws.getIxIndici().getAreaSchedaP(), ws.getIxIndici().getTabVarP(), wskdAreaScheda.getWskdTabValP().getTpDatoP(ws.getIxIndici().getAreaSchedaP(), ws.getIxIndici().getTabVarP()));
        // COB_CODE: MOVE WSKD-VAL-GENERICO-P(IX-AREA-SCHEDA-P, IX-TAB-VAR-P)
        //             TO ISPC0211-VAL-GENERICO-P(IX-AREA-SCHEDA-P, IX-TAB-VAR-P).
        ws.getAreaIoIspc0211().getIspc0211TabValP().setIspc0211ValGenericoP(ws.getIxIndici().getAreaSchedaP(), ws.getIxIndici().getTabVarP(), wskdAreaScheda.getWskdTabValP().getValGenericoP(ws.getIxIndici().getAreaSchedaP(), ws.getIxIndici().getTabVarP()));
    }

    /**Original name: AREA-SCHEDA-T-ISPC0211<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZAZIONE AREA SCHEDA T
	 * ----------------------------------------------------------------*</pre>*/
    private void areaSchedaTIspc0211() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE WSKD-TP-LIVELLO-T(IX-AREA-SCHEDA-T)
        //             TO ISPC0211-TIPO-LIVELLO-T(IX-AREA-SCHEDA-T)
        ws.getAreaIoIspc0211().getIspc0211TabValT().setIspc0211TipoLivelloT(ws.getIxIndici().getAreaSchedaT(), wskdAreaScheda.getWskdTabValT().getTpLivelloT(ws.getIxIndici().getAreaSchedaT()));
        // COB_CODE: MOVE WSKD-COD-LIVELLO-T(IX-AREA-SCHEDA-T)
        //             TO ISPC0211-CODICE-LIVELLO-T(IX-AREA-SCHEDA-T)
        ws.getAreaIoIspc0211().getIspc0211TabValT().setIspc0211CodiceLivelloT(ws.getIxIndici().getAreaSchedaT(), wskdAreaScheda.getWskdTabValT().getCodLivelloT(ws.getIxIndici().getAreaSchedaT()));
        // COB_CODE: MOVE WSKD-ID-LIVELLO-T(IX-AREA-SCHEDA-T)
        //             TO ISPC0211-IDENT-LIVELLO-T(IX-AREA-SCHEDA-T)
        ws.getAreaIoIspc0211().getIspc0211TabValT().setIspc0211IdentLivelloTFormatted(ws.getIxIndici().getAreaSchedaT(), wskdAreaScheda.getWskdTabValT().getIdLivelloTFormatted(ws.getIxIndici().getAreaSchedaT()));
        // COB_CODE: MOVE WSKD-COD-TIPO-OPZIONE-T(IX-AREA-SCHEDA-T)
        //             TO ISPC0211-CODICE-OPZIONE-T(IX-AREA-SCHEDA-T)
        ws.getAreaIoIspc0211().getIspc0211TabValT().setIspc0211CodiceOpzioneT(ws.getIxIndici().getAreaSchedaT(), wskdAreaScheda.getWskdTabValT().getCodTipoOpzioneT(ws.getIxIndici().getAreaSchedaT()));
        //
        // COB_CODE:      IF WSKD-COD-RGM-FISC-T(IX-AREA-SCHEDA-T) NOT EQUAL SPACES
        //                                                         AND LOW-VALUE
        //                                                         AND HIGH-VALUE
        //           *
        //                    TO ISPC0211-COD-RGM-FISC-T(IX-AREA-SCHEDA-T)
        //           *
        //                END-IF
        if (!Characters.EQ_SPACE.test(wskdAreaScheda.getWskdTabValT().getCodRgmFiscT(ws.getIxIndici().getAreaSchedaT())) && !Characters.EQ_LOW.test(wskdAreaScheda.getWskdTabValT().getCodRgmFiscTFormatted(ws.getIxIndici().getAreaSchedaT())) && !Characters.EQ_HIGH.test(wskdAreaScheda.getWskdTabValT().getCodRgmFiscTFormatted(ws.getIxIndici().getAreaSchedaT()))) {
            //
            // COB_CODE: MOVE WSKD-COD-RGM-FISC-T(IX-AREA-SCHEDA-T)
            //             TO ISPC0211-COD-RGM-FISC-T(IX-AREA-SCHEDA-T)
            ws.getAreaIoIspc0211().getIspc0211TabValT().setIspc0211CodRgmFiscTFormatted(ws.getIxIndici().getAreaSchedaT(), wskdAreaScheda.getWskdTabValT().getCodRgmFiscTFormatted(ws.getIxIndici().getAreaSchedaT()));
            //
        }
        //
        // COB_CODE: MOVE WSKD-DT-INIZ-TARI-T(IX-AREA-SCHEDA-T)
        //             TO ISPC0211-DT-INIZ-TARI(IX-AREA-SCHEDA-T)
        ws.getAreaIoIspc0211().getIspc0211TabValT().setIspc0211DtInizTari(ws.getIxIndici().getAreaSchedaT(), wskdAreaScheda.getWskdTabValT().getDtInizTariT(ws.getIxIndici().getAreaSchedaT()));
        //
        // COB_CODE: MOVE WSKD-DT-DECOR-TRCH-T(IX-AREA-SCHEDA-T)
        //             TO ISPC0211-DT-DECOR-TRCH(IX-AREA-SCHEDA-T)
        ws.getAreaIoIspc0211().getIspc0211TabValT().setIspc0211DtDecorTrch(ws.getIxIndici().getAreaSchedaT(), wskdAreaScheda.getWskdTabValT().getDtDecorTrchT(ws.getIxIndici().getAreaSchedaT()));
        // COB_CODE: MOVE WSKD-TIPO-TRCH(IX-AREA-SCHEDA-T)
        //             TO ISPC0211-TIPO-TRCH(IX-AREA-SCHEDA-T)
        ws.getAreaIoIspc0211().getIspc0211TabValT().setIspc0211TipoTrchFormatted(ws.getIxIndici().getAreaSchedaT(), wskdAreaScheda.getWskdTabValT().getTipoTrchFormatted(ws.getIxIndici().getAreaSchedaT()));
        // COB_CODE: MOVE WSKD-FLG-ITN(IX-AREA-SCHEDA-T)
        //             TO ISPC0211-FLG-ITN(IX-AREA-SCHEDA-T)
        ws.getAreaIoIspc0211().getIspc0211TabValT().setIspc0211FlgItnFormatted(ws.getIxIndici().getAreaSchedaT(), wskdAreaScheda.getWskdTabValT().getFlgItnFormatted(ws.getIxIndici().getAreaSchedaT()));
        //--> VALORIZZAZIONE DELL'AREA VARIABILI DI INPUT DEL SERVIZIO
        //--> CALCOLI E CONTROLLI
        //
        // COB_CODE: MOVE ZERO
        //             TO ISPC0211-NUM-COMPON-MAX-ELE-T(IX-AREA-SCHEDA-T)
        ws.getAreaIoIspc0211().getIspc0211TabValT().setIspc0211NumComponMaxEleT(ws.getIxIndici().getAreaSchedaT(), ((short)0));
        // COB_CODE: PERFORM AREA-VAR-T-ISPC0211
        //              THRU AREA-VAR-T-ISPC0211-EX
        //           VARYING IX-TAB-VAR-T FROM 1 BY 1
        //             UNTIL IX-TAB-VAR-T >
        //                   WSKD-ELE-VARIABILI-MAX-T(IX-AREA-SCHEDA-T)
        //                OR IX-TAB-VAR-T > WK-ISPC0211-NUM-COMPON-MAX-T
        ws.getIxIndici().setTabVarT(((short)1));
        while (!(ws.getIxIndici().getTabVarT() > wskdAreaScheda.getWskdTabValT().getEleVariabiliMaxT(ws.getIxIndici().getAreaSchedaT()) || ws.getIxIndici().getTabVarT() > ws.getWkIspcMax().getIspc0211NumComponMaxT())) {
            areaVarTIspc0211();
            ws.getIxIndici().setTabVarT(Trunc.toShort(ws.getIxIndici().getTabVarT() + 1, 4));
        }
        //
        //--> SEGNALO CHE LE VARIABILI FORNITEMI SONO IN NUMERO
        //--> MAGGIORE DEL NUMERO CONSENTITO DALL'AREA ISPC0211
        //
        // COB_CODE: IF WSKD-ELE-VARIABILI-MAX-T(IX-AREA-SCHEDA-T) >
        //                             WK-ISPC0211-NUM-COMPON-MAX-T
        //                     THRU EX-S0300
        //           END-IF.
        if (wskdAreaScheda.getWskdTabValT().getEleVariabiliMaxT(ws.getIxIndici().getAreaSchedaT()) > ws.getWkIspcMax().getIspc0211NumComponMaxT()) {
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S12110-AREA-SCHEDA-T'
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S12110-AREA-SCHEDA-T");
            // COB_CODE: MOVE '005247'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005247");
            // COB_CODE: STRING 'NUMERO VARIABILI DI TARIFFA '
            //                  'SUPERA LIMITE PREVISTO'
            //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "NUMERO VARIABILI DI TARIFFA ", "SUPERA LIMITE PREVISTO");
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: AREA-VAR-T-ISPC0211<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZAZIONE DELL'AREA VARIABILI DI TARIFFA
	 *     DI INPUT DEL SERVIZIO CALCOLI E CONTROLLI
	 * ----------------------------------------------------------------*</pre>*/
    private void areaVarTIspc0211() {
        // COB_CODE: ADD 1 TO ISPC0211-NUM-COMPON-MAX-ELE-T(IX-AREA-SCHEDA-T)
        ws.getAreaIoIspc0211().getIspc0211TabValT().setIspc0211NumComponMaxEleT(ws.getIxIndici().getAreaSchedaT(), Trunc.toShort(1 + ws.getAreaIoIspc0211().getIspc0211TabValT().getIspc0211NumComponMaxEleT(ws.getIxIndici().getAreaSchedaT()), 4));
        // COB_CODE: MOVE WSKD-COD-VARIABILE-T(IX-AREA-SCHEDA-T, IX-TAB-VAR-T)
        //             TO ISPC0211-CODICE-VARIABILE-T
        //               (IX-AREA-SCHEDA-T, IX-TAB-VAR-T)
        ws.getAreaIoIspc0211().getIspc0211TabValT().setIspc0211CodiceVariabileT(ws.getIxIndici().getAreaSchedaT(), ws.getIxIndici().getTabVarT(), wskdAreaScheda.getWskdTabValT().getCodVariabileT(ws.getIxIndici().getAreaSchedaT(), ws.getIxIndici().getTabVarT()));
        // COB_CODE: MOVE WSKD-TP-DATO-T(IX-AREA-SCHEDA-T, IX-TAB-VAR-T)
        //             TO ISPC0211-TIPO-DATO-T(IX-AREA-SCHEDA-T, IX-TAB-VAR-T)
        ws.getAreaIoIspc0211().getIspc0211TabValT().setIspc0211TipoDatoT(ws.getIxIndici().getAreaSchedaT(), ws.getIxIndici().getTabVarT(), wskdAreaScheda.getWskdTabValT().getTpDatoT(ws.getIxIndici().getAreaSchedaT(), ws.getIxIndici().getTabVarT()));
        // COB_CODE: MOVE WSKD-VAL-GENERICO-T(IX-AREA-SCHEDA-T, IX-TAB-VAR-T)
        //             TO ISPC0211-VAL-GENERICO-T(IX-AREA-SCHEDA-T, IX-TAB-VAR-T).
        ws.getAreaIoIspc0211().getIspc0211TabValT().setIspc0211ValGenericoT(ws.getIxIndici().getAreaSchedaT(), ws.getIxIndici().getTabVarT(), wskdAreaScheda.getWskdTabValT().getValGenericoT(ws.getIxIndici().getAreaSchedaT(), ws.getIxIndici().getTabVarT()));
    }

    public void initIxIndici() {
        ws.getIxIndici().setTabPmo(((short)0));
        ws.getIxIndici().setTabGrz(((short)0));
        ws.getIxIndici().setTabTga(((short)0));
        ws.getIxIndici().setTabPog(((short)0));
        ws.getIxIndici().setAreaIspc0211(((short)0));
        ws.getIxIndici().setCompIspc0211(((short)0));
        ws.getIxIndici().setAreaSchedaP(((short)0));
        ws.getIxIndici().setAreaSchedaT(((short)0));
        ws.getIxIndici().setTabVarP(((short)0));
        ws.getIxIndici().setTabVarT(((short)0));
        ws.getIxIndici().setAreaScheda(((short)0));
        ws.getIxIndici().setTabVar(((short)0));
        ws.getIxIndici().setTabErr(((short)0));
        ws.getIxIndici().setFraz(((short)0));
        ws.getIxIndici().setTabIsps0211(((short)0));
    }

    public void initIspc0211DatiInput() {
        ws.getAreaIoIspc0211().getIspc0211DatiInput().setIspc0211CodCompagniaFormatted("00000");
        ws.getAreaIoIspc0211().getIspc0211DatiInput().setNumPolizza("");
        ws.getAreaIoIspc0211().getIspc0211DatiInput().setCodProdotto("");
        ws.getAreaIoIspc0211().getIspc0211DatiInput().setDataDecorrPolizza("");
        ws.getAreaIoIspc0211().getIspc0211DatiInput().setCodConvenzione("");
        ws.getAreaIoIspc0211().getIspc0211DatiInput().setDataInizValidConv("");
        ws.getAreaIoIspc0211().getIspc0211DatiInput().setDee("");
        ws.getAreaIoIspc0211().getIspc0211DatiInput().setDataRiferimento("");
        ws.getAreaIoIspc0211().getIspc0211DatiInput().setIspc0211LivelloUtenteFormatted("00");
        ws.getAreaIoIspc0211().getIspc0211DatiInput().setIspc0211OpzNumMaxEleFormatted("000");
        for (int idx0 = 1; idx0 <= Ispc0211DatiInput.OPZIONI_MAXOCCURS; idx0++) {
            ws.getAreaIoIspc0211().getIspc0211DatiInput().getOpzioni(idx0).setIspc0211OpzEsercitata("");
        }
        ws.getAreaIoIspc0211().getIspc0211DatiInput().setSessionId("");
        ws.getAreaIoIspc0211().getIspc0211DatiInput().setFlgRecProv(Types.SPACE_CHAR);
        ws.getAreaIoIspc0211().getIspc0211DatiInput().setIspc0211FunzionalitaFormatted("00000");
        ws.getAreaIoIspc0211().getIspc0211DatiInput().setCodIniziativa("");
    }

    public void initIspc0211AreaErrori() {
        ws.getAreaIoIspc0211().setIspc0211EsitoFormatted("00");
        ws.getAreaIoIspc0211().setIspc0211ErrNumEleFormatted("000");
        for (int idx0 = 1; idx0 <= AreaIoIsps0211.ISPC0211_TAB_ERRORI_MAXOCCURS; idx0++) {
            ws.getAreaIoIspc0211().getIspc0211TabErrori(idx0).setCodErr("");
            ws.getAreaIoIspc0211().getIspc0211TabErrori(idx0).setDescrizioneErr("");
            ws.getAreaIoIspc0211().getIspc0211TabErrori(idx0).setIspc0211GravitaErrFormatted("00");
            ws.getAreaIoIspc0211().getIspc0211TabErrori(idx0).setIspc0211LivelloDerogaFormatted("00");
        }
    }

    public void initIspc0211TabSchedaP() {
        ws.getAreaIoIspc0211().getIspc0211TabValP().setIspc0211TipoLivelloP(1, Types.SPACE_CHAR);
        ws.getAreaIoIspc0211().getIspc0211TabValP().setIspc0211CodiceLivelloP(1, "");
        ws.getAreaIoIspc0211().getIspc0211TabValP().setIspc0211IdentLivelloPFormatted(1, "000000000");
        ws.getAreaIoIspc0211().getIspc0211TabValP().setIspc0211DtInizProd(1, "");
        ws.getAreaIoIspc0211().getIspc0211TabValP().setIspc0211CodRgmFiscFormatted(1, "00");
        ws.getAreaIoIspc0211().getIspc0211TabValP().setIspc0211FlgLiqP(1, "");
        ws.getAreaIoIspc0211().getIspc0211TabValP().setIspc0211CodiceOpzioneP(1, "");
        ws.getAreaIoIspc0211().getIspc0211TabValP().setIspc0211NumComponMaxElePFormatted(1, "000");
        for (int idx0 = 1; idx0 <= Ispc0211TabValP.AREA_VARIABILI_P_MAXOCCURS; idx0++) {
            ws.getAreaIoIspc0211().getIspc0211TabValP().setIspc0211CodiceVariabileP(1, idx0, "");
            ws.getAreaIoIspc0211().getIspc0211TabValP().setIspc0211TipoDatoP(1, idx0, Types.SPACE_CHAR);
            ws.getAreaIoIspc0211().getIspc0211TabValP().setIspc0211ValGenericoP(1, idx0, "");
        }
    }

    public void initIspc0211TabSchedaT() {
        ws.getAreaIoIspc0211().getIspc0211TabValT().setIspc0211TipoLivelloT(1, Types.SPACE_CHAR);
        ws.getAreaIoIspc0211().getIspc0211TabValT().setIspc0211CodiceLivelloT(1, "");
        ws.getAreaIoIspc0211().getIspc0211TabValT().setIspc0211IdentLivelloTFormatted(1, "000000000");
        ws.getAreaIoIspc0211().getIspc0211TabValT().setIspc0211DtInizTari(1, "");
        ws.getAreaIoIspc0211().getIspc0211TabValT().setIspc0211CodRgmFiscTFormatted(1, "00");
        ws.getAreaIoIspc0211().getIspc0211TabValT().setIspc0211DtDecorTrch(1, "");
        ws.getAreaIoIspc0211().getIspc0211TabValT().setIspc0211FlgLiqT(1, "");
        ws.getAreaIoIspc0211().getIspc0211TabValT().setIspc0211CodiceOpzioneT(1, "");
        ws.getAreaIoIspc0211().getIspc0211TabValT().setIspc0211TipoTrchFormatted(1, "00");
        ws.getAreaIoIspc0211().getIspc0211TabValT().setIspc0211FlgItnFormatted(1, "0");
        ws.getAreaIoIspc0211().getIspc0211TabValT().setIspc0211NumComponMaxEleTFormatted(1, "0000");
        for (int idx0 = 1; idx0 <= Ispc0211TabValT.AREA_VARIABILI_T_MAXOCCURS; idx0++) {
            ws.getAreaIoIspc0211().getIspc0211TabValT().setIspc0211CodiceVariabileT(1, idx0, "");
            ws.getAreaIoIspc0211().getIspc0211TabValT().setIspc0211TipoDatoT(1, idx0, Types.SPACE_CHAR);
            ws.getAreaIoIspc0211().getIspc0211TabValT().setIspc0211ValGenericoT(1, idx0, "");
        }
    }

    public void initStrPerformanceDbg() {
        ws.getIdsv8888().getStrPerformanceDbg().setFase("");
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("");
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("");
        ws.getIdsv8888().getStrPerformanceDbg().setTimestampFld("");
        ws.getIdsv8888().getStrPerformanceDbg().setStato("");
        ws.getIdsv8888().getStrPerformanceDbg().setModalitaEsecutiva(Types.SPACE_CHAR);
        ws.getIdsv8888().getStrPerformanceDbg().setUserName("");
    }
}
