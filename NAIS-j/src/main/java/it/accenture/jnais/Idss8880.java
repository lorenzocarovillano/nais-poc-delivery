package it.accenture.jnais;

import com.bphx.ctu.af.core.pointer.IPointerManager;
import com.bphx.ctu.af.util.display.DisplayUtil;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.ws.ptr.WkAreaDisplay;
import it.accenture.jnais.ws.WkAddress;
import javax.inject.Inject;

/**Original name: IDSS8880<br>
 * <pre>AUTHOR.        ATS NAPOLI.
 * DATE-WRITTEN.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE
 *  F A S E         : CALL DISPLAY X ONLINE
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Idss8880 extends Program {

    //==== PROPERTIES ====
    @Inject
    private IPointerManager pointerManager;
    //Original name: WK-ADDRESS
    private WkAddress wkAddress;
    //Original name: WK-AREA-DISPLAY
    private WkAreaDisplay wkAreaDisplay = new WkAreaDisplay(null);

    //==== METHODS ====
    /**Original name: PROGRAM_IDSS8880_FIRST_SENTENCES<br>*/
    public long execute(WkAddress wkAddress, WkAreaDisplay wkAreaDisplay) {
        this.wkAddress = wkAddress;
        this.wkAreaDisplay.assignBc(wkAreaDisplay);
        // COB_CODE: SET ADDRESS OF WK-AREA-DISPLAY TO WK-ADDRESS.
        this.wkAreaDisplay = (((WkAreaDisplay)pointerManager.resolve(this.wkAddress.getWkAddress(), WkAreaDisplay.class)));
        // COB_CODE: DISPLAY WK-AREA-DISPLAY.
        DisplayUtil.sysout.write(this.wkAreaDisplay.getWkAreaDisplayFormatted());
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Idss8880 getInstance() {
        return ((Idss8880)Programs.getInstance(Idss8880.class));
    }
}
