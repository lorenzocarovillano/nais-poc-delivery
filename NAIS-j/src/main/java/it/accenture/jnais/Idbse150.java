package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.EstRappAnaDao;
import it.accenture.jnais.commons.data.to.IEstRappAna;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.EstRappAnaIdbse150;
import it.accenture.jnais.ws.Idbse150Data;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.redefines.E15IdMoviChiu;
import it.accenture.jnais.ws.redefines.E15IdRappAnaCollg;
import it.accenture.jnais.ws.redefines.E15IdSegmentazCli;

/**Original name: IDBSE150<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  04 MAR 2020.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Idbse150 extends Program implements IEstRappAna {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private EstRappAnaDao estRappAnaDao = new EstRappAnaDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Idbse150Data ws = new Idbse150Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: EST-RAPP-ANA
    private EstRappAnaIdbse150 estRappAna;

    //==== METHODS ====
    /**Original name: PROGRAM_IDBSE150_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, EstRappAnaIdbse150 estRappAna) {
        this.idsv0003 = idsv0003;
        this.estRappAna = estRappAna;
        // COB_CODE: PERFORM A000-INIZIO                    THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO          THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS          THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO          THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                        a300ElaboraIdEff();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                        a400ElaboraIdpEff();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO          THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS          THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO          THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                        b300ElaboraIdCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                        b400ElaboraIdpCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                        b500ElaboraIboCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                        b600ElaboraIbsCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                        b700ElaboraIdoCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //              SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-PRIMARY-KEY
                //                 PERFORM A200-ELABORA-PK          THRU A200-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO         THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS         THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO         THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.PRIMARY_KEY:// COB_CODE: PERFORM A200-ELABORA-PK          THRU A200-EX
                        a200ElaboraPk();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO         THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS         THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO         THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Idbse150 getInstance() {
        return ((Idbse150)Programs.getInstance(Idbse150.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'IDBSE150'   TO IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("IDBSE150");
        // COB_CODE: MOVE 'EST_RAPP_ANA' TO IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("EST_RAPP_ANA");
        // COB_CODE: MOVE '00'                     TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                   TO   IDSV0003-SQLCODE
        //                                              IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                              IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-AGG-STORICO-SOLO-INS  OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isAggStoricoSoloIns() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-PK<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a200ElaboraPk() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-PK          THRU A210-EX
        //              WHEN IDSV0003-INSERT
        //                 PERFORM A220-INSERT-PK          THRU A220-EX
        //              WHEN IDSV0003-UPDATE
        //                 PERFORM A230-UPDATE-PK          THRU A230-EX
        //              WHEN IDSV0003-DELETE
        //                 PERFORM A240-DELETE-PK          THRU A240-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-PK          THRU A210-EX
            a210SelectPk();
        }
        else if (idsv0003.getOperazione().isInsert()) {
            // COB_CODE: PERFORM A220-INSERT-PK          THRU A220-EX
            a220InsertPk();
        }
        else if (idsv0003.getOperazione().isUpdate()) {
            // COB_CODE: PERFORM A230-UPDATE-PK          THRU A230-EX
            a230UpdatePk();
        }
        else if (idsv0003.getOperazione().isDelete()) {
            // COB_CODE: PERFORM A240-DELETE-PK          THRU A240-EX
            a240DeletePk();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A300-ELABORA-ID-EFF<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void a300ElaboraIdEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A310-SELECT-ID-EFF          THRU A310-EX
            a310SelectIdEff();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A400-ELABORA-IDP-EFF<br>*/
    private void a400ElaboraIdpEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
            a410SelectIdpEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
            a460OpenCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
            a470CloseCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
            a480FetchFirstIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
            a490FetchNextIdpEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A500-ELABORA-IBO<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a500ElaboraIbo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A510-SELECT-IBO             THRU A510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A510-SELECT-IBO             THRU A510-EX
            a510SelectIbo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
            a560OpenCursorIbo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
            a570CloseCursorIbo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
            a580FetchFirstIbo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
            a590FetchNextIbo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A600-ELABORA-IBS<br>*/
    private void a600ElaboraIbs() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A610-SELECT-IBS             THRU A610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A610-SELECT-IBS             THRU A610-EX
            a610SelectIbs();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
            a660OpenCursorIbs();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
            a670CloseCursorIbs();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
            a680FetchFirstIbs();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
            a690FetchNextIbs();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A700-ELABORA-IDO<br>*/
    private void a700ElaboraIdo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A710-SELECT-IDO                 THRU A710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A710-SELECT-IDO                 THRU A710-EX
            a710SelectIdo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
            a760OpenCursorIdo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
            a770CloseCursorIdo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
            a780FetchFirstIdo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
            a790FetchNextIdo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B300-ELABORA-ID-CPZ<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void b300ElaboraIdCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
            b310SelectIdCpz();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B400-ELABORA-IDP-CPZ<br>*/
    private void b400ElaboraIdpCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
            b410SelectIdpCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
            b460OpenCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
            b470CloseCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
            b480FetchFirstIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
            b490FetchNextIdpCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B500-ELABORA-IBO-CPZ<br>*/
    private void b500ElaboraIboCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
            b510SelectIboCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
            b560OpenCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
            b570CloseCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
            b580FetchFirstIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B600-ELABORA-IBS-CPZ<br>*/
    private void b600ElaboraIbsCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
            b610SelectIbsCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
            b660OpenCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
            b670CloseCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
            b680FetchFirstIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B700-ELABORA-IDO-CPZ<br>*/
    private void b700ElaboraIdoCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
            b710SelectIdoCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
            b760OpenCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
            b770CloseCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
            b780FetchFirstIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A210-SELECT-PK<br>
	 * <pre>----
	 * ----  gestione PK
	 * ----</pre>*/
    private void a210SelectPk() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_EST_RAPP_ANA
        //                ,ID_RAPP_ANA
        //                ,ID_RAPP_ANA_COLLG
        //                ,ID_OGG
        //                ,TP_OGG
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,COD_SOGG
        //                ,TP_RAPP_ANA
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,ID_SEGMENTAZ_CLI
        //                ,FL_COINC_TIT_EFF
        //                ,FL_PERS_ESP_POL
        //                ,DESC_PERS_ESP_POL
        //                ,TP_LEG_CNTR
        //                ,DESC_LEG_CNTR
        //                ,TP_LEG_PERC_BNFICR
        //                ,D_LEG_PERC_BNFICR
        //                ,TP_CNT_CORR
        //                ,TP_COINC_PIC_PAC
        //             INTO
        //                :E15-ID-EST-RAPP-ANA
        //               ,:E15-ID-RAPP-ANA
        //               ,:E15-ID-RAPP-ANA-COLLG
        //                :IND-E15-ID-RAPP-ANA-COLLG
        //               ,:E15-ID-OGG
        //               ,:E15-TP-OGG
        //               ,:E15-ID-MOVI-CRZ
        //               ,:E15-ID-MOVI-CHIU
        //                :IND-E15-ID-MOVI-CHIU
        //               ,:E15-DT-INI-EFF-DB
        //               ,:E15-DT-END-EFF-DB
        //               ,:E15-COD-COMP-ANIA
        //               ,:E15-COD-SOGG
        //                :IND-E15-COD-SOGG
        //               ,:E15-TP-RAPP-ANA
        //               ,:E15-DS-RIGA
        //               ,:E15-DS-OPER-SQL
        //               ,:E15-DS-VER
        //               ,:E15-DS-TS-INI-CPTZ
        //               ,:E15-DS-TS-END-CPTZ
        //               ,:E15-DS-UTENTE
        //               ,:E15-DS-STATO-ELAB
        //               ,:E15-ID-SEGMENTAZ-CLI
        //                :IND-E15-ID-SEGMENTAZ-CLI
        //               ,:E15-FL-COINC-TIT-EFF
        //                :IND-E15-FL-COINC-TIT-EFF
        //               ,:E15-FL-PERS-ESP-POL
        //                :IND-E15-FL-PERS-ESP-POL
        //               ,:E15-DESC-PERS-ESP-POL-VCHAR
        //                :IND-E15-DESC-PERS-ESP-POL
        //               ,:E15-TP-LEG-CNTR
        //                :IND-E15-TP-LEG-CNTR
        //               ,:E15-DESC-LEG-CNTR-VCHAR
        //                :IND-E15-DESC-LEG-CNTR
        //               ,:E15-TP-LEG-PERC-BNFICR
        //                :IND-E15-TP-LEG-PERC-BNFICR
        //               ,:E15-D-LEG-PERC-BNFICR-VCHAR
        //                :IND-E15-D-LEG-PERC-BNFICR
        //               ,:E15-TP-CNT-CORR
        //                :IND-E15-TP-CNT-CORR
        //               ,:E15-TP-COINC-PIC-PAC
        //                :IND-E15-TP-COINC-PIC-PAC
        //             FROM EST_RAPP_ANA
        //             WHERE     DS_RIGA = :E15-DS-RIGA
        //           END-EXEC.
        estRappAnaDao.selectByE15DsRiga(estRappAna.getE15DsRiga(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A220-INSERT-PK<br>*/
    private void a220InsertPk() {
        // COB_CODE: PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.
        z400SeqRiga();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX
            z150ValorizzaDataServicesI();
            // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX
            z200SetIndicatoriNull();
            // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX
            z900ConvertiNToX();
            // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX
            z960LengthVchar();
            // COB_CODE: EXEC SQL
            //              INSERT
            //              INTO EST_RAPP_ANA
            //                  (
            //                     ID_EST_RAPP_ANA
            //                    ,ID_RAPP_ANA
            //                    ,ID_RAPP_ANA_COLLG
            //                    ,ID_OGG
            //                    ,TP_OGG
            //                    ,ID_MOVI_CRZ
            //                    ,ID_MOVI_CHIU
            //                    ,DT_INI_EFF
            //                    ,DT_END_EFF
            //                    ,COD_COMP_ANIA
            //                    ,COD_SOGG
            //                    ,TP_RAPP_ANA
            //                    ,DS_RIGA
            //                    ,DS_OPER_SQL
            //                    ,DS_VER
            //                    ,DS_TS_INI_CPTZ
            //                    ,DS_TS_END_CPTZ
            //                    ,DS_UTENTE
            //                    ,DS_STATO_ELAB
            //                    ,ID_SEGMENTAZ_CLI
            //                    ,FL_COINC_TIT_EFF
            //                    ,FL_PERS_ESP_POL
            //                    ,DESC_PERS_ESP_POL
            //                    ,TP_LEG_CNTR
            //                    ,DESC_LEG_CNTR
            //                    ,TP_LEG_PERC_BNFICR
            //                    ,D_LEG_PERC_BNFICR
            //                    ,TP_CNT_CORR
            //                    ,TP_COINC_PIC_PAC
            //                  )
            //              VALUES
            //                  (
            //                    :E15-ID-EST-RAPP-ANA
            //                    ,:E15-ID-RAPP-ANA
            //                    ,:E15-ID-RAPP-ANA-COLLG
            //                     :IND-E15-ID-RAPP-ANA-COLLG
            //                    ,:E15-ID-OGG
            //                    ,:E15-TP-OGG
            //                    ,:E15-ID-MOVI-CRZ
            //                    ,:E15-ID-MOVI-CHIU
            //                     :IND-E15-ID-MOVI-CHIU
            //                    ,:E15-DT-INI-EFF-DB
            //                    ,:E15-DT-END-EFF-DB
            //                    ,:E15-COD-COMP-ANIA
            //                    ,:E15-COD-SOGG
            //                     :IND-E15-COD-SOGG
            //                    ,:E15-TP-RAPP-ANA
            //                    ,:E15-DS-RIGA
            //                    ,:E15-DS-OPER-SQL
            //                    ,:E15-DS-VER
            //                    ,:E15-DS-TS-INI-CPTZ
            //                    ,:E15-DS-TS-END-CPTZ
            //                    ,:E15-DS-UTENTE
            //                    ,:E15-DS-STATO-ELAB
            //                    ,:E15-ID-SEGMENTAZ-CLI
            //                     :IND-E15-ID-SEGMENTAZ-CLI
            //                    ,:E15-FL-COINC-TIT-EFF
            //                     :IND-E15-FL-COINC-TIT-EFF
            //                    ,:E15-FL-PERS-ESP-POL
            //                     :IND-E15-FL-PERS-ESP-POL
            //                    ,:E15-DESC-PERS-ESP-POL-VCHAR
            //                     :IND-E15-DESC-PERS-ESP-POL
            //                    ,:E15-TP-LEG-CNTR
            //                     :IND-E15-TP-LEG-CNTR
            //                    ,:E15-DESC-LEG-CNTR-VCHAR
            //                     :IND-E15-DESC-LEG-CNTR
            //                    ,:E15-TP-LEG-PERC-BNFICR
            //                     :IND-E15-TP-LEG-PERC-BNFICR
            //                    ,:E15-D-LEG-PERC-BNFICR-VCHAR
            //                     :IND-E15-D-LEG-PERC-BNFICR
            //                    ,:E15-TP-CNT-CORR
            //                     :IND-E15-TP-CNT-CORR
            //                    ,:E15-TP-COINC-PIC-PAC
            //                     :IND-E15-TP-COINC-PIC-PAC
            //                  )
            //           END-EXEC
            estRappAnaDao.insertRec(this);
            // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
            a100CheckReturnCode();
        }
    }

    /**Original name: A230-UPDATE-PK<br>*/
    private void a230UpdatePk() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE EST_RAPP_ANA SET
        //                   ID_EST_RAPP_ANA        =
        //                :E15-ID-EST-RAPP-ANA
        //                  ,ID_RAPP_ANA            =
        //                :E15-ID-RAPP-ANA
        //                  ,ID_RAPP_ANA_COLLG      =
        //                :E15-ID-RAPP-ANA-COLLG
        //                                       :IND-E15-ID-RAPP-ANA-COLLG
        //                  ,ID_OGG                 =
        //                :E15-ID-OGG
        //                  ,TP_OGG                 =
        //                :E15-TP-OGG
        //                  ,ID_MOVI_CRZ            =
        //                :E15-ID-MOVI-CRZ
        //                  ,ID_MOVI_CHIU           =
        //                :E15-ID-MOVI-CHIU
        //                                       :IND-E15-ID-MOVI-CHIU
        //                  ,DT_INI_EFF             =
        //           :E15-DT-INI-EFF-DB
        //                  ,DT_END_EFF             =
        //           :E15-DT-END-EFF-DB
        //                  ,COD_COMP_ANIA          =
        //                :E15-COD-COMP-ANIA
        //                  ,COD_SOGG               =
        //                :E15-COD-SOGG
        //                                       :IND-E15-COD-SOGG
        //                  ,TP_RAPP_ANA            =
        //                :E15-TP-RAPP-ANA
        //                  ,DS_RIGA                =
        //                :E15-DS-RIGA
        //                  ,DS_OPER_SQL            =
        //                :E15-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :E15-DS-VER
        //                  ,DS_TS_INI_CPTZ         =
        //                :E15-DS-TS-INI-CPTZ
        //                  ,DS_TS_END_CPTZ         =
        //                :E15-DS-TS-END-CPTZ
        //                  ,DS_UTENTE              =
        //                :E15-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :E15-DS-STATO-ELAB
        //                  ,ID_SEGMENTAZ_CLI       =
        //                :E15-ID-SEGMENTAZ-CLI
        //                                       :IND-E15-ID-SEGMENTAZ-CLI
        //                  ,FL_COINC_TIT_EFF       =
        //                :E15-FL-COINC-TIT-EFF
        //                                       :IND-E15-FL-COINC-TIT-EFF
        //                  ,FL_PERS_ESP_POL        =
        //                :E15-FL-PERS-ESP-POL
        //                                       :IND-E15-FL-PERS-ESP-POL
        //                  ,DESC_PERS_ESP_POL      =
        //                :E15-DESC-PERS-ESP-POL-VCHAR
        //                                       :IND-E15-DESC-PERS-ESP-POL
        //                  ,TP_LEG_CNTR            =
        //                :E15-TP-LEG-CNTR
        //                                       :IND-E15-TP-LEG-CNTR
        //                  ,DESC_LEG_CNTR          =
        //                :E15-DESC-LEG-CNTR-VCHAR
        //                                       :IND-E15-DESC-LEG-CNTR
        //                  ,TP_LEG_PERC_BNFICR     =
        //                :E15-TP-LEG-PERC-BNFICR
        //                                       :IND-E15-TP-LEG-PERC-BNFICR
        //                  ,D_LEG_PERC_BNFICR      =
        //                :E15-D-LEG-PERC-BNFICR-VCHAR
        //                                       :IND-E15-D-LEG-PERC-BNFICR
        //                  ,TP_CNT_CORR            =
        //                :E15-TP-CNT-CORR
        //                                       :IND-E15-TP-CNT-CORR
        //                  ,TP_COINC_PIC_PAC       =
        //                :E15-TP-COINC-PIC-PAC
        //                                       :IND-E15-TP-COINC-PIC-PAC
        //                WHERE     DS_RIGA = :E15-DS-RIGA
        //           END-EXEC.
        estRappAnaDao.updateRec(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A240-DELETE-PK<br>*/
    private void a240DeletePk() {
        // COB_CODE: EXEC SQL
        //                DELETE
        //                FROM EST_RAPP_ANA
        //                WHERE     DS_RIGA = :E15-DS-RIGA
        //           END-EXEC.
        estRappAnaDao.deleteByE15DsRiga(estRappAna.getE15DsRiga());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A305-DECLARE-CURSOR-ID-EFF<br>
	 * <pre>----
	 * ----  gestione ID Effetto
	 * ----</pre>*/
    private void a305DeclareCursorIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-ID-UPD-EFF-E15 CURSOR FOR
        //              SELECT
        //                     ID_EST_RAPP_ANA
        //                    ,ID_RAPP_ANA
        //                    ,ID_RAPP_ANA_COLLG
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,COD_SOGG
        //                    ,TP_RAPP_ANA
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,ID_SEGMENTAZ_CLI
        //                    ,FL_COINC_TIT_EFF
        //                    ,FL_PERS_ESP_POL
        //                    ,DESC_PERS_ESP_POL
        //                    ,TP_LEG_CNTR
        //                    ,DESC_LEG_CNTR
        //                    ,TP_LEG_PERC_BNFICR
        //                    ,D_LEG_PERC_BNFICR
        //                    ,TP_CNT_CORR
        //                    ,TP_COINC_PIC_PAC
        //              FROM EST_RAPP_ANA
        //              WHERE     ID_EST_RAPP_ANA = :E15-ID-EST-RAPP-ANA
        //                    AND DS_TS_END_CPTZ = :WS-TS-INFINITO
        //                    AND DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //              ORDER BY DT_INI_EFF ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A310-SELECT-ID-EFF<br>*/
    private void a310SelectIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_EST_RAPP_ANA
        //                ,ID_RAPP_ANA
        //                ,ID_RAPP_ANA_COLLG
        //                ,ID_OGG
        //                ,TP_OGG
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,COD_SOGG
        //                ,TP_RAPP_ANA
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,ID_SEGMENTAZ_CLI
        //                ,FL_COINC_TIT_EFF
        //                ,FL_PERS_ESP_POL
        //                ,DESC_PERS_ESP_POL
        //                ,TP_LEG_CNTR
        //                ,DESC_LEG_CNTR
        //                ,TP_LEG_PERC_BNFICR
        //                ,D_LEG_PERC_BNFICR
        //                ,TP_CNT_CORR
        //                ,TP_COINC_PIC_PAC
        //             INTO
        //                :E15-ID-EST-RAPP-ANA
        //               ,:E15-ID-RAPP-ANA
        //               ,:E15-ID-RAPP-ANA-COLLG
        //                :IND-E15-ID-RAPP-ANA-COLLG
        //               ,:E15-ID-OGG
        //               ,:E15-TP-OGG
        //               ,:E15-ID-MOVI-CRZ
        //               ,:E15-ID-MOVI-CHIU
        //                :IND-E15-ID-MOVI-CHIU
        //               ,:E15-DT-INI-EFF-DB
        //               ,:E15-DT-END-EFF-DB
        //               ,:E15-COD-COMP-ANIA
        //               ,:E15-COD-SOGG
        //                :IND-E15-COD-SOGG
        //               ,:E15-TP-RAPP-ANA
        //               ,:E15-DS-RIGA
        //               ,:E15-DS-OPER-SQL
        //               ,:E15-DS-VER
        //               ,:E15-DS-TS-INI-CPTZ
        //               ,:E15-DS-TS-END-CPTZ
        //               ,:E15-DS-UTENTE
        //               ,:E15-DS-STATO-ELAB
        //               ,:E15-ID-SEGMENTAZ-CLI
        //                :IND-E15-ID-SEGMENTAZ-CLI
        //               ,:E15-FL-COINC-TIT-EFF
        //                :IND-E15-FL-COINC-TIT-EFF
        //               ,:E15-FL-PERS-ESP-POL
        //                :IND-E15-FL-PERS-ESP-POL
        //               ,:E15-DESC-PERS-ESP-POL-VCHAR
        //                :IND-E15-DESC-PERS-ESP-POL
        //               ,:E15-TP-LEG-CNTR
        //                :IND-E15-TP-LEG-CNTR
        //               ,:E15-DESC-LEG-CNTR-VCHAR
        //                :IND-E15-DESC-LEG-CNTR
        //               ,:E15-TP-LEG-PERC-BNFICR
        //                :IND-E15-TP-LEG-PERC-BNFICR
        //               ,:E15-D-LEG-PERC-BNFICR-VCHAR
        //                :IND-E15-D-LEG-PERC-BNFICR
        //               ,:E15-TP-CNT-CORR
        //                :IND-E15-TP-CNT-CORR
        //               ,:E15-TP-COINC-PIC-PAC
        //                :IND-E15-TP-COINC-PIC-PAC
        //             FROM EST_RAPP_ANA
        //             WHERE     ID_EST_RAPP_ANA = :E15-ID-EST-RAPP-ANA
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        estRappAnaDao.selectRec(estRappAna.getE15IdEstRappAna(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A330-UPDATE-ID-EFF<br>*/
    private void a330UpdateIdEff() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE EST_RAPP_ANA SET
        //                   ID_EST_RAPP_ANA        =
        //                :E15-ID-EST-RAPP-ANA
        //                  ,ID_RAPP_ANA            =
        //                :E15-ID-RAPP-ANA
        //                  ,ID_RAPP_ANA_COLLG      =
        //                :E15-ID-RAPP-ANA-COLLG
        //                                       :IND-E15-ID-RAPP-ANA-COLLG
        //                  ,ID_OGG                 =
        //                :E15-ID-OGG
        //                  ,TP_OGG                 =
        //                :E15-TP-OGG
        //                  ,ID_MOVI_CRZ            =
        //                :E15-ID-MOVI-CRZ
        //                  ,ID_MOVI_CHIU           =
        //                :E15-ID-MOVI-CHIU
        //                                       :IND-E15-ID-MOVI-CHIU
        //                  ,DT_INI_EFF             =
        //           :E15-DT-INI-EFF-DB
        //                  ,DT_END_EFF             =
        //           :E15-DT-END-EFF-DB
        //                  ,COD_COMP_ANIA          =
        //                :E15-COD-COMP-ANIA
        //                  ,COD_SOGG               =
        //                :E15-COD-SOGG
        //                                       :IND-E15-COD-SOGG
        //                  ,TP_RAPP_ANA            =
        //                :E15-TP-RAPP-ANA
        //                  ,DS_RIGA                =
        //                :E15-DS-RIGA
        //                  ,DS_OPER_SQL            =
        //                :E15-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :E15-DS-VER
        //                  ,DS_TS_INI_CPTZ         =
        //                :E15-DS-TS-INI-CPTZ
        //                  ,DS_TS_END_CPTZ         =
        //                :E15-DS-TS-END-CPTZ
        //                  ,DS_UTENTE              =
        //                :E15-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :E15-DS-STATO-ELAB
        //                  ,ID_SEGMENTAZ_CLI       =
        //                :E15-ID-SEGMENTAZ-CLI
        //                                       :IND-E15-ID-SEGMENTAZ-CLI
        //                  ,FL_COINC_TIT_EFF       =
        //                :E15-FL-COINC-TIT-EFF
        //                                       :IND-E15-FL-COINC-TIT-EFF
        //                  ,FL_PERS_ESP_POL        =
        //                :E15-FL-PERS-ESP-POL
        //                                       :IND-E15-FL-PERS-ESP-POL
        //                  ,DESC_PERS_ESP_POL      =
        //                :E15-DESC-PERS-ESP-POL-VCHAR
        //                                       :IND-E15-DESC-PERS-ESP-POL
        //                  ,TP_LEG_CNTR            =
        //                :E15-TP-LEG-CNTR
        //                                       :IND-E15-TP-LEG-CNTR
        //                  ,DESC_LEG_CNTR          =
        //                :E15-DESC-LEG-CNTR-VCHAR
        //                                       :IND-E15-DESC-LEG-CNTR
        //                  ,TP_LEG_PERC_BNFICR     =
        //                :E15-TP-LEG-PERC-BNFICR
        //                                       :IND-E15-TP-LEG-PERC-BNFICR
        //                  ,D_LEG_PERC_BNFICR      =
        //                :E15-D-LEG-PERC-BNFICR-VCHAR
        //                                       :IND-E15-D-LEG-PERC-BNFICR
        //                  ,TP_CNT_CORR            =
        //                :E15-TP-CNT-CORR
        //                                       :IND-E15-TP-CNT-CORR
        //                  ,TP_COINC_PIC_PAC       =
        //                :E15-TP-COINC-PIC-PAC
        //                                       :IND-E15-TP-COINC-PIC-PAC
        //                WHERE     DS_RIGA = :E15-DS-RIGA
        //                   AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        estRappAnaDao.updateRec1(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A360-OPEN-CURSOR-ID-EFF<br>*/
    private void a360OpenCursorIdEff() {
        // COB_CODE: PERFORM A305-DECLARE-CURSOR-ID-EFF THRU A305-EX.
        a305DeclareCursorIdEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-ID-UPD-EFF-E15
        //           END-EXEC.
        estRappAnaDao.openCIdUpdEffE15(estRappAna.getE15IdEstRappAna(), ws.getIdsv0010().getWsTsInfinito(), ws.getIdsv0010().getWsDataInizioEffettoDb(), idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A370-CLOSE-CURSOR-ID-EFF<br>*/
    private void a370CloseCursorIdEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-ID-UPD-EFF-E15
        //           END-EXEC.
        estRappAnaDao.closeCIdUpdEffE15();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A390-FETCH-NEXT-ID-EFF<br>*/
    private void a390FetchNextIdEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-ID-UPD-EFF-E15
        //           INTO
        //                :E15-ID-EST-RAPP-ANA
        //               ,:E15-ID-RAPP-ANA
        //               ,:E15-ID-RAPP-ANA-COLLG
        //                :IND-E15-ID-RAPP-ANA-COLLG
        //               ,:E15-ID-OGG
        //               ,:E15-TP-OGG
        //               ,:E15-ID-MOVI-CRZ
        //               ,:E15-ID-MOVI-CHIU
        //                :IND-E15-ID-MOVI-CHIU
        //               ,:E15-DT-INI-EFF-DB
        //               ,:E15-DT-END-EFF-DB
        //               ,:E15-COD-COMP-ANIA
        //               ,:E15-COD-SOGG
        //                :IND-E15-COD-SOGG
        //               ,:E15-TP-RAPP-ANA
        //               ,:E15-DS-RIGA
        //               ,:E15-DS-OPER-SQL
        //               ,:E15-DS-VER
        //               ,:E15-DS-TS-INI-CPTZ
        //               ,:E15-DS-TS-END-CPTZ
        //               ,:E15-DS-UTENTE
        //               ,:E15-DS-STATO-ELAB
        //               ,:E15-ID-SEGMENTAZ-CLI
        //                :IND-E15-ID-SEGMENTAZ-CLI
        //               ,:E15-FL-COINC-TIT-EFF
        //                :IND-E15-FL-COINC-TIT-EFF
        //               ,:E15-FL-PERS-ESP-POL
        //                :IND-E15-FL-PERS-ESP-POL
        //               ,:E15-DESC-PERS-ESP-POL-VCHAR
        //                :IND-E15-DESC-PERS-ESP-POL
        //               ,:E15-TP-LEG-CNTR
        //                :IND-E15-TP-LEG-CNTR
        //               ,:E15-DESC-LEG-CNTR-VCHAR
        //                :IND-E15-DESC-LEG-CNTR
        //               ,:E15-TP-LEG-PERC-BNFICR
        //                :IND-E15-TP-LEG-PERC-BNFICR
        //               ,:E15-D-LEG-PERC-BNFICR-VCHAR
        //                :IND-E15-D-LEG-PERC-BNFICR
        //               ,:E15-TP-CNT-CORR
        //                :IND-E15-TP-CNT-CORR
        //               ,:E15-TP-COINC-PIC-PAC
        //                :IND-E15-TP-COINC-PIC-PAC
        //           END-EXEC.
        estRappAnaDao.fetchCIdUpdEffE15(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A370-CLOSE-CURSOR-ID-EFF THRU A370-EX
            a370CloseCursorIdEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A405-DECLARE-CURSOR-IDP-EFF<br>
	 * <pre>----
	 * ----  gestione IDP Effetto
	 * ----</pre>*/
    private void a405DeclareCursorIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IDP-EFF-E15 CURSOR FOR
        //              SELECT
        //                     ID_EST_RAPP_ANA
        //                    ,ID_RAPP_ANA
        //                    ,ID_RAPP_ANA_COLLG
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,COD_SOGG
        //                    ,TP_RAPP_ANA
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,ID_SEGMENTAZ_CLI
        //                    ,FL_COINC_TIT_EFF
        //                    ,FL_PERS_ESP_POL
        //                    ,DESC_PERS_ESP_POL
        //                    ,TP_LEG_CNTR
        //                    ,DESC_LEG_CNTR
        //                    ,TP_LEG_PERC_BNFICR
        //                    ,D_LEG_PERC_BNFICR
        //                    ,TP_CNT_CORR
        //                    ,TP_COINC_PIC_PAC
        //              FROM EST_RAPP_ANA
        //              WHERE     ID_RAPP_ANA = :E15-ID-RAPP-ANA
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //              ORDER BY ID_EST_RAPP_ANA ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A410-SELECT-IDP-EFF<br>*/
    private void a410SelectIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_EST_RAPP_ANA
        //                ,ID_RAPP_ANA
        //                ,ID_RAPP_ANA_COLLG
        //                ,ID_OGG
        //                ,TP_OGG
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,COD_SOGG
        //                ,TP_RAPP_ANA
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,ID_SEGMENTAZ_CLI
        //                ,FL_COINC_TIT_EFF
        //                ,FL_PERS_ESP_POL
        //                ,DESC_PERS_ESP_POL
        //                ,TP_LEG_CNTR
        //                ,DESC_LEG_CNTR
        //                ,TP_LEG_PERC_BNFICR
        //                ,D_LEG_PERC_BNFICR
        //                ,TP_CNT_CORR
        //                ,TP_COINC_PIC_PAC
        //             INTO
        //                :E15-ID-EST-RAPP-ANA
        //               ,:E15-ID-RAPP-ANA
        //               ,:E15-ID-RAPP-ANA-COLLG
        //                :IND-E15-ID-RAPP-ANA-COLLG
        //               ,:E15-ID-OGG
        //               ,:E15-TP-OGG
        //               ,:E15-ID-MOVI-CRZ
        //               ,:E15-ID-MOVI-CHIU
        //                :IND-E15-ID-MOVI-CHIU
        //               ,:E15-DT-INI-EFF-DB
        //               ,:E15-DT-END-EFF-DB
        //               ,:E15-COD-COMP-ANIA
        //               ,:E15-COD-SOGG
        //                :IND-E15-COD-SOGG
        //               ,:E15-TP-RAPP-ANA
        //               ,:E15-DS-RIGA
        //               ,:E15-DS-OPER-SQL
        //               ,:E15-DS-VER
        //               ,:E15-DS-TS-INI-CPTZ
        //               ,:E15-DS-TS-END-CPTZ
        //               ,:E15-DS-UTENTE
        //               ,:E15-DS-STATO-ELAB
        //               ,:E15-ID-SEGMENTAZ-CLI
        //                :IND-E15-ID-SEGMENTAZ-CLI
        //               ,:E15-FL-COINC-TIT-EFF
        //                :IND-E15-FL-COINC-TIT-EFF
        //               ,:E15-FL-PERS-ESP-POL
        //                :IND-E15-FL-PERS-ESP-POL
        //               ,:E15-DESC-PERS-ESP-POL-VCHAR
        //                :IND-E15-DESC-PERS-ESP-POL
        //               ,:E15-TP-LEG-CNTR
        //                :IND-E15-TP-LEG-CNTR
        //               ,:E15-DESC-LEG-CNTR-VCHAR
        //                :IND-E15-DESC-LEG-CNTR
        //               ,:E15-TP-LEG-PERC-BNFICR
        //                :IND-E15-TP-LEG-PERC-BNFICR
        //               ,:E15-D-LEG-PERC-BNFICR-VCHAR
        //                :IND-E15-D-LEG-PERC-BNFICR
        //               ,:E15-TP-CNT-CORR
        //                :IND-E15-TP-CNT-CORR
        //               ,:E15-TP-COINC-PIC-PAC
        //                :IND-E15-TP-COINC-PIC-PAC
        //             FROM EST_RAPP_ANA
        //             WHERE     ID_RAPP_ANA = :E15-ID-RAPP-ANA
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        estRappAnaDao.selectRec1(estRappAna.getE15IdRappAna(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A460-OPEN-CURSOR-IDP-EFF<br>*/
    private void a460OpenCursorIdpEff() {
        // COB_CODE: PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.
        a405DeclareCursorIdpEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-IDP-EFF-E15
        //           END-EXEC.
        estRappAnaDao.openCIdpEffE15(estRappAna.getE15IdRappAna(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A470-CLOSE-CURSOR-IDP-EFF<br>*/
    private void a470CloseCursorIdpEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IDP-EFF-E15
        //           END-EXEC.
        estRappAnaDao.closeCIdpEffE15();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A480-FETCH-FIRST-IDP-EFF<br>*/
    private void a480FetchFirstIdpEff() {
        // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.
        a460OpenCursorIdpEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
            a490FetchNextIdpEff();
        }
    }

    /**Original name: A490-FETCH-NEXT-IDP-EFF<br>*/
    private void a490FetchNextIdpEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IDP-EFF-E15
        //           INTO
        //                :E15-ID-EST-RAPP-ANA
        //               ,:E15-ID-RAPP-ANA
        //               ,:E15-ID-RAPP-ANA-COLLG
        //                :IND-E15-ID-RAPP-ANA-COLLG
        //               ,:E15-ID-OGG
        //               ,:E15-TP-OGG
        //               ,:E15-ID-MOVI-CRZ
        //               ,:E15-ID-MOVI-CHIU
        //                :IND-E15-ID-MOVI-CHIU
        //               ,:E15-DT-INI-EFF-DB
        //               ,:E15-DT-END-EFF-DB
        //               ,:E15-COD-COMP-ANIA
        //               ,:E15-COD-SOGG
        //                :IND-E15-COD-SOGG
        //               ,:E15-TP-RAPP-ANA
        //               ,:E15-DS-RIGA
        //               ,:E15-DS-OPER-SQL
        //               ,:E15-DS-VER
        //               ,:E15-DS-TS-INI-CPTZ
        //               ,:E15-DS-TS-END-CPTZ
        //               ,:E15-DS-UTENTE
        //               ,:E15-DS-STATO-ELAB
        //               ,:E15-ID-SEGMENTAZ-CLI
        //                :IND-E15-ID-SEGMENTAZ-CLI
        //               ,:E15-FL-COINC-TIT-EFF
        //                :IND-E15-FL-COINC-TIT-EFF
        //               ,:E15-FL-PERS-ESP-POL
        //                :IND-E15-FL-PERS-ESP-POL
        //               ,:E15-DESC-PERS-ESP-POL-VCHAR
        //                :IND-E15-DESC-PERS-ESP-POL
        //               ,:E15-TP-LEG-CNTR
        //                :IND-E15-TP-LEG-CNTR
        //               ,:E15-DESC-LEG-CNTR-VCHAR
        //                :IND-E15-DESC-LEG-CNTR
        //               ,:E15-TP-LEG-PERC-BNFICR
        //                :IND-E15-TP-LEG-PERC-BNFICR
        //               ,:E15-D-LEG-PERC-BNFICR-VCHAR
        //                :IND-E15-D-LEG-PERC-BNFICR
        //               ,:E15-TP-CNT-CORR
        //                :IND-E15-TP-CNT-CORR
        //               ,:E15-TP-COINC-PIC-PAC
        //                :IND-E15-TP-COINC-PIC-PAC
        //           END-EXEC.
        estRappAnaDao.fetchCIdpEffE15(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
            a470CloseCursorIdpEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A505-DECLARE-CURSOR-IBO<br>
	 * <pre>----
	 * ----  gestione IBO Effetto e non
	 * ----</pre>*/
    private void a505DeclareCursorIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A510-SELECT-IBO<br>*/
    private void a510SelectIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A560-OPEN-CURSOR-IBO<br>*/
    private void a560OpenCursorIbo() {
        // COB_CODE: PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.
        a505DeclareCursorIbo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A570-CLOSE-CURSOR-IBO<br>*/
    private void a570CloseCursorIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A580-FETCH-FIRST-IBO<br>*/
    private void a580FetchFirstIbo() {
        // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.
        a560OpenCursorIbo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
            a590FetchNextIbo();
        }
    }

    /**Original name: A590-FETCH-NEXT-IBO<br>*/
    private void a590FetchNextIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A605-DECLARE-CURSOR-IBS<br>
	 * <pre>----
	 * ----  gestione IBS Effetto e non
	 * ----</pre>*/
    private void a605DeclareCursorIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A610-SELECT-IBS<br>*/
    private void a610SelectIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A660-OPEN-CURSOR-IBS<br>*/
    private void a660OpenCursorIbs() {
        // COB_CODE: PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.
        a605DeclareCursorIbs();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A670-CLOSE-CURSOR-IBS<br>*/
    private void a670CloseCursorIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A680-FETCH-FIRST-IBS<br>*/
    private void a680FetchFirstIbs() {
        // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.
        a660OpenCursorIbs();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
            a690FetchNextIbs();
        }
    }

    /**Original name: A690-FETCH-NEXT-IBS<br>*/
    private void a690FetchNextIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A705-DECLARE-CURSOR-IDO<br>
	 * <pre>----
	 * ----  gestione IDO Effetto e non
	 * ----</pre>*/
    private void a705DeclareCursorIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IDO-EFF-E15 CURSOR FOR
        //              SELECT
        //                     ID_EST_RAPP_ANA
        //                    ,ID_RAPP_ANA
        //                    ,ID_RAPP_ANA_COLLG
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,COD_SOGG
        //                    ,TP_RAPP_ANA
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,ID_SEGMENTAZ_CLI
        //                    ,FL_COINC_TIT_EFF
        //                    ,FL_PERS_ESP_POL
        //                    ,DESC_PERS_ESP_POL
        //                    ,TP_LEG_CNTR
        //                    ,DESC_LEG_CNTR
        //                    ,TP_LEG_PERC_BNFICR
        //                    ,D_LEG_PERC_BNFICR
        //                    ,TP_CNT_CORR
        //                    ,TP_COINC_PIC_PAC
        //              FROM EST_RAPP_ANA
        //              WHERE     ID_OGG = :E15-ID-OGG
        //                    AND TP_OGG = :E15-TP-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //              ORDER BY ID_EST_RAPP_ANA ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A710-SELECT-IDO<br>*/
    private void a710SelectIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_EST_RAPP_ANA
        //                ,ID_RAPP_ANA
        //                ,ID_RAPP_ANA_COLLG
        //                ,ID_OGG
        //                ,TP_OGG
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,COD_SOGG
        //                ,TP_RAPP_ANA
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,ID_SEGMENTAZ_CLI
        //                ,FL_COINC_TIT_EFF
        //                ,FL_PERS_ESP_POL
        //                ,DESC_PERS_ESP_POL
        //                ,TP_LEG_CNTR
        //                ,DESC_LEG_CNTR
        //                ,TP_LEG_PERC_BNFICR
        //                ,D_LEG_PERC_BNFICR
        //                ,TP_CNT_CORR
        //                ,TP_COINC_PIC_PAC
        //             INTO
        //                :E15-ID-EST-RAPP-ANA
        //               ,:E15-ID-RAPP-ANA
        //               ,:E15-ID-RAPP-ANA-COLLG
        //                :IND-E15-ID-RAPP-ANA-COLLG
        //               ,:E15-ID-OGG
        //               ,:E15-TP-OGG
        //               ,:E15-ID-MOVI-CRZ
        //               ,:E15-ID-MOVI-CHIU
        //                :IND-E15-ID-MOVI-CHIU
        //               ,:E15-DT-INI-EFF-DB
        //               ,:E15-DT-END-EFF-DB
        //               ,:E15-COD-COMP-ANIA
        //               ,:E15-COD-SOGG
        //                :IND-E15-COD-SOGG
        //               ,:E15-TP-RAPP-ANA
        //               ,:E15-DS-RIGA
        //               ,:E15-DS-OPER-SQL
        //               ,:E15-DS-VER
        //               ,:E15-DS-TS-INI-CPTZ
        //               ,:E15-DS-TS-END-CPTZ
        //               ,:E15-DS-UTENTE
        //               ,:E15-DS-STATO-ELAB
        //               ,:E15-ID-SEGMENTAZ-CLI
        //                :IND-E15-ID-SEGMENTAZ-CLI
        //               ,:E15-FL-COINC-TIT-EFF
        //                :IND-E15-FL-COINC-TIT-EFF
        //               ,:E15-FL-PERS-ESP-POL
        //                :IND-E15-FL-PERS-ESP-POL
        //               ,:E15-DESC-PERS-ESP-POL-VCHAR
        //                :IND-E15-DESC-PERS-ESP-POL
        //               ,:E15-TP-LEG-CNTR
        //                :IND-E15-TP-LEG-CNTR
        //               ,:E15-DESC-LEG-CNTR-VCHAR
        //                :IND-E15-DESC-LEG-CNTR
        //               ,:E15-TP-LEG-PERC-BNFICR
        //                :IND-E15-TP-LEG-PERC-BNFICR
        //               ,:E15-D-LEG-PERC-BNFICR-VCHAR
        //                :IND-E15-D-LEG-PERC-BNFICR
        //               ,:E15-TP-CNT-CORR
        //                :IND-E15-TP-CNT-CORR
        //               ,:E15-TP-COINC-PIC-PAC
        //                :IND-E15-TP-COINC-PIC-PAC
        //             FROM EST_RAPP_ANA
        //             WHERE     ID_OGG = :E15-ID-OGG
        //                    AND TP_OGG = :E15-TP-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        estRappAnaDao.selectRec2(estRappAna.getE15IdOgg(), estRappAna.getE15TpOgg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A760-OPEN-CURSOR-IDO<br>*/
    private void a760OpenCursorIdo() {
        // COB_CODE: PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.
        a705DeclareCursorIdo();
        // COB_CODE: EXEC SQL
        //                OPEN C-IDO-EFF-E15
        //           END-EXEC.
        estRappAnaDao.openCIdoEffE15(estRappAna.getE15IdOgg(), estRappAna.getE15TpOgg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A770-CLOSE-CURSOR-IDO<br>*/
    private void a770CloseCursorIdo() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IDO-EFF-E15
        //           END-EXEC.
        estRappAnaDao.closeCIdoEffE15();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A780-FETCH-FIRST-IDO<br>*/
    private void a780FetchFirstIdo() {
        // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.
        a760OpenCursorIdo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
            a790FetchNextIdo();
        }
    }

    /**Original name: A790-FETCH-NEXT-IDO<br>*/
    private void a790FetchNextIdo() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IDO-EFF-E15
        //           INTO
        //                :E15-ID-EST-RAPP-ANA
        //               ,:E15-ID-RAPP-ANA
        //               ,:E15-ID-RAPP-ANA-COLLG
        //                :IND-E15-ID-RAPP-ANA-COLLG
        //               ,:E15-ID-OGG
        //               ,:E15-TP-OGG
        //               ,:E15-ID-MOVI-CRZ
        //               ,:E15-ID-MOVI-CHIU
        //                :IND-E15-ID-MOVI-CHIU
        //               ,:E15-DT-INI-EFF-DB
        //               ,:E15-DT-END-EFF-DB
        //               ,:E15-COD-COMP-ANIA
        //               ,:E15-COD-SOGG
        //                :IND-E15-COD-SOGG
        //               ,:E15-TP-RAPP-ANA
        //               ,:E15-DS-RIGA
        //               ,:E15-DS-OPER-SQL
        //               ,:E15-DS-VER
        //               ,:E15-DS-TS-INI-CPTZ
        //               ,:E15-DS-TS-END-CPTZ
        //               ,:E15-DS-UTENTE
        //               ,:E15-DS-STATO-ELAB
        //               ,:E15-ID-SEGMENTAZ-CLI
        //                :IND-E15-ID-SEGMENTAZ-CLI
        //               ,:E15-FL-COINC-TIT-EFF
        //                :IND-E15-FL-COINC-TIT-EFF
        //               ,:E15-FL-PERS-ESP-POL
        //                :IND-E15-FL-PERS-ESP-POL
        //               ,:E15-DESC-PERS-ESP-POL-VCHAR
        //                :IND-E15-DESC-PERS-ESP-POL
        //               ,:E15-TP-LEG-CNTR
        //                :IND-E15-TP-LEG-CNTR
        //               ,:E15-DESC-LEG-CNTR-VCHAR
        //                :IND-E15-DESC-LEG-CNTR
        //               ,:E15-TP-LEG-PERC-BNFICR
        //                :IND-E15-TP-LEG-PERC-BNFICR
        //               ,:E15-D-LEG-PERC-BNFICR-VCHAR
        //                :IND-E15-D-LEG-PERC-BNFICR
        //               ,:E15-TP-CNT-CORR
        //                :IND-E15-TP-CNT-CORR
        //               ,:E15-TP-COINC-PIC-PAC
        //                :IND-E15-TP-COINC-PIC-PAC
        //           END-EXEC.
        estRappAnaDao.fetchCIdoEffE15(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A770-CLOSE-CURSOR-IDO     THRU A770-EX
            a770CloseCursorIdo();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B310-SELECT-ID-CPZ<br>
	 * <pre>----
	 * ----  gestione ID Competenza
	 * ----</pre>*/
    private void b310SelectIdCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_EST_RAPP_ANA
        //                ,ID_RAPP_ANA
        //                ,ID_RAPP_ANA_COLLG
        //                ,ID_OGG
        //                ,TP_OGG
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,COD_SOGG
        //                ,TP_RAPP_ANA
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,ID_SEGMENTAZ_CLI
        //                ,FL_COINC_TIT_EFF
        //                ,FL_PERS_ESP_POL
        //                ,DESC_PERS_ESP_POL
        //                ,TP_LEG_CNTR
        //                ,DESC_LEG_CNTR
        //                ,TP_LEG_PERC_BNFICR
        //                ,D_LEG_PERC_BNFICR
        //                ,TP_CNT_CORR
        //                ,TP_COINC_PIC_PAC
        //             INTO
        //                :E15-ID-EST-RAPP-ANA
        //               ,:E15-ID-RAPP-ANA
        //               ,:E15-ID-RAPP-ANA-COLLG
        //                :IND-E15-ID-RAPP-ANA-COLLG
        //               ,:E15-ID-OGG
        //               ,:E15-TP-OGG
        //               ,:E15-ID-MOVI-CRZ
        //               ,:E15-ID-MOVI-CHIU
        //                :IND-E15-ID-MOVI-CHIU
        //               ,:E15-DT-INI-EFF-DB
        //               ,:E15-DT-END-EFF-DB
        //               ,:E15-COD-COMP-ANIA
        //               ,:E15-COD-SOGG
        //                :IND-E15-COD-SOGG
        //               ,:E15-TP-RAPP-ANA
        //               ,:E15-DS-RIGA
        //               ,:E15-DS-OPER-SQL
        //               ,:E15-DS-VER
        //               ,:E15-DS-TS-INI-CPTZ
        //               ,:E15-DS-TS-END-CPTZ
        //               ,:E15-DS-UTENTE
        //               ,:E15-DS-STATO-ELAB
        //               ,:E15-ID-SEGMENTAZ-CLI
        //                :IND-E15-ID-SEGMENTAZ-CLI
        //               ,:E15-FL-COINC-TIT-EFF
        //                :IND-E15-FL-COINC-TIT-EFF
        //               ,:E15-FL-PERS-ESP-POL
        //                :IND-E15-FL-PERS-ESP-POL
        //               ,:E15-DESC-PERS-ESP-POL-VCHAR
        //                :IND-E15-DESC-PERS-ESP-POL
        //               ,:E15-TP-LEG-CNTR
        //                :IND-E15-TP-LEG-CNTR
        //               ,:E15-DESC-LEG-CNTR-VCHAR
        //                :IND-E15-DESC-LEG-CNTR
        //               ,:E15-TP-LEG-PERC-BNFICR
        //                :IND-E15-TP-LEG-PERC-BNFICR
        //               ,:E15-D-LEG-PERC-BNFICR-VCHAR
        //                :IND-E15-D-LEG-PERC-BNFICR
        //               ,:E15-TP-CNT-CORR
        //                :IND-E15-TP-CNT-CORR
        //               ,:E15-TP-COINC-PIC-PAC
        //                :IND-E15-TP-COINC-PIC-PAC
        //             FROM EST_RAPP_ANA
        //             WHERE     ID_EST_RAPP_ANA = :E15-ID-EST-RAPP-ANA
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        estRappAnaDao.selectRec3(estRappAna.getE15IdEstRappAna(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B405-DECLARE-CURSOR-IDP-CPZ<br>
	 * <pre>----
	 * ----  gestione IDP Competenza
	 * ----</pre>*/
    private void b405DeclareCursorIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IDP-CPZ-E15 CURSOR FOR
        //              SELECT
        //                     ID_EST_RAPP_ANA
        //                    ,ID_RAPP_ANA
        //                    ,ID_RAPP_ANA_COLLG
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,COD_SOGG
        //                    ,TP_RAPP_ANA
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,ID_SEGMENTAZ_CLI
        //                    ,FL_COINC_TIT_EFF
        //                    ,FL_PERS_ESP_POL
        //                    ,DESC_PERS_ESP_POL
        //                    ,TP_LEG_CNTR
        //                    ,DESC_LEG_CNTR
        //                    ,TP_LEG_PERC_BNFICR
        //                    ,D_LEG_PERC_BNFICR
        //                    ,TP_CNT_CORR
        //                    ,TP_COINC_PIC_PAC
        //              FROM EST_RAPP_ANA
        //              WHERE     ID_RAPP_ANA = :E15-ID-RAPP-ANA
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //              ORDER BY ID_EST_RAPP_ANA ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B410-SELECT-IDP-CPZ<br>*/
    private void b410SelectIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_EST_RAPP_ANA
        //                ,ID_RAPP_ANA
        //                ,ID_RAPP_ANA_COLLG
        //                ,ID_OGG
        //                ,TP_OGG
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,COD_SOGG
        //                ,TP_RAPP_ANA
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,ID_SEGMENTAZ_CLI
        //                ,FL_COINC_TIT_EFF
        //                ,FL_PERS_ESP_POL
        //                ,DESC_PERS_ESP_POL
        //                ,TP_LEG_CNTR
        //                ,DESC_LEG_CNTR
        //                ,TP_LEG_PERC_BNFICR
        //                ,D_LEG_PERC_BNFICR
        //                ,TP_CNT_CORR
        //                ,TP_COINC_PIC_PAC
        //             INTO
        //                :E15-ID-EST-RAPP-ANA
        //               ,:E15-ID-RAPP-ANA
        //               ,:E15-ID-RAPP-ANA-COLLG
        //                :IND-E15-ID-RAPP-ANA-COLLG
        //               ,:E15-ID-OGG
        //               ,:E15-TP-OGG
        //               ,:E15-ID-MOVI-CRZ
        //               ,:E15-ID-MOVI-CHIU
        //                :IND-E15-ID-MOVI-CHIU
        //               ,:E15-DT-INI-EFF-DB
        //               ,:E15-DT-END-EFF-DB
        //               ,:E15-COD-COMP-ANIA
        //               ,:E15-COD-SOGG
        //                :IND-E15-COD-SOGG
        //               ,:E15-TP-RAPP-ANA
        //               ,:E15-DS-RIGA
        //               ,:E15-DS-OPER-SQL
        //               ,:E15-DS-VER
        //               ,:E15-DS-TS-INI-CPTZ
        //               ,:E15-DS-TS-END-CPTZ
        //               ,:E15-DS-UTENTE
        //               ,:E15-DS-STATO-ELAB
        //               ,:E15-ID-SEGMENTAZ-CLI
        //                :IND-E15-ID-SEGMENTAZ-CLI
        //               ,:E15-FL-COINC-TIT-EFF
        //                :IND-E15-FL-COINC-TIT-EFF
        //               ,:E15-FL-PERS-ESP-POL
        //                :IND-E15-FL-PERS-ESP-POL
        //               ,:E15-DESC-PERS-ESP-POL-VCHAR
        //                :IND-E15-DESC-PERS-ESP-POL
        //               ,:E15-TP-LEG-CNTR
        //                :IND-E15-TP-LEG-CNTR
        //               ,:E15-DESC-LEG-CNTR-VCHAR
        //                :IND-E15-DESC-LEG-CNTR
        //               ,:E15-TP-LEG-PERC-BNFICR
        //                :IND-E15-TP-LEG-PERC-BNFICR
        //               ,:E15-D-LEG-PERC-BNFICR-VCHAR
        //                :IND-E15-D-LEG-PERC-BNFICR
        //               ,:E15-TP-CNT-CORR
        //                :IND-E15-TP-CNT-CORR
        //               ,:E15-TP-COINC-PIC-PAC
        //                :IND-E15-TP-COINC-PIC-PAC
        //             FROM EST_RAPP_ANA
        //             WHERE     ID_RAPP_ANA = :E15-ID-RAPP-ANA
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        estRappAnaDao.selectRec4(estRappAna.getE15IdRappAna(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B460-OPEN-CURSOR-IDP-CPZ<br>*/
    private void b460OpenCursorIdpCpz() {
        // COB_CODE: PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.
        b405DeclareCursorIdpCpz();
        // COB_CODE: EXEC SQL
        //                OPEN C-IDP-CPZ-E15
        //           END-EXEC.
        estRappAnaDao.openCIdpCpzE15(estRappAna.getE15IdRappAna(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B470-CLOSE-CURSOR-IDP-CPZ<br>*/
    private void b470CloseCursorIdpCpz() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IDP-CPZ-E15
        //           END-EXEC.
        estRappAnaDao.closeCIdpCpzE15();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B480-FETCH-FIRST-IDP-CPZ<br>*/
    private void b480FetchFirstIdpCpz() {
        // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.
        b460OpenCursorIdpCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
            b490FetchNextIdpCpz();
        }
    }

    /**Original name: B490-FETCH-NEXT-IDP-CPZ<br>*/
    private void b490FetchNextIdpCpz() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IDP-CPZ-E15
        //           INTO
        //                :E15-ID-EST-RAPP-ANA
        //               ,:E15-ID-RAPP-ANA
        //               ,:E15-ID-RAPP-ANA-COLLG
        //                :IND-E15-ID-RAPP-ANA-COLLG
        //               ,:E15-ID-OGG
        //               ,:E15-TP-OGG
        //               ,:E15-ID-MOVI-CRZ
        //               ,:E15-ID-MOVI-CHIU
        //                :IND-E15-ID-MOVI-CHIU
        //               ,:E15-DT-INI-EFF-DB
        //               ,:E15-DT-END-EFF-DB
        //               ,:E15-COD-COMP-ANIA
        //               ,:E15-COD-SOGG
        //                :IND-E15-COD-SOGG
        //               ,:E15-TP-RAPP-ANA
        //               ,:E15-DS-RIGA
        //               ,:E15-DS-OPER-SQL
        //               ,:E15-DS-VER
        //               ,:E15-DS-TS-INI-CPTZ
        //               ,:E15-DS-TS-END-CPTZ
        //               ,:E15-DS-UTENTE
        //               ,:E15-DS-STATO-ELAB
        //               ,:E15-ID-SEGMENTAZ-CLI
        //                :IND-E15-ID-SEGMENTAZ-CLI
        //               ,:E15-FL-COINC-TIT-EFF
        //                :IND-E15-FL-COINC-TIT-EFF
        //               ,:E15-FL-PERS-ESP-POL
        //                :IND-E15-FL-PERS-ESP-POL
        //               ,:E15-DESC-PERS-ESP-POL-VCHAR
        //                :IND-E15-DESC-PERS-ESP-POL
        //               ,:E15-TP-LEG-CNTR
        //                :IND-E15-TP-LEG-CNTR
        //               ,:E15-DESC-LEG-CNTR-VCHAR
        //                :IND-E15-DESC-LEG-CNTR
        //               ,:E15-TP-LEG-PERC-BNFICR
        //                :IND-E15-TP-LEG-PERC-BNFICR
        //               ,:E15-D-LEG-PERC-BNFICR-VCHAR
        //                :IND-E15-D-LEG-PERC-BNFICR
        //               ,:E15-TP-CNT-CORR
        //                :IND-E15-TP-CNT-CORR
        //               ,:E15-TP-COINC-PIC-PAC
        //                :IND-E15-TP-COINC-PIC-PAC
        //           END-EXEC.
        estRappAnaDao.fetchCIdpCpzE15(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
            b470CloseCursorIdpCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B505-DECLARE-CURSOR-IBO-CPZ<br>
	 * <pre>----
	 * ----  gestione IBO Competenza
	 * ----</pre>*/
    private void b505DeclareCursorIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B510-SELECT-IBO-CPZ<br>*/
    private void b510SelectIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B560-OPEN-CURSOR-IBO-CPZ<br>*/
    private void b560OpenCursorIboCpz() {
        // COB_CODE: PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.
        b505DeclareCursorIboCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B570-CLOSE-CURSOR-IBO-CPZ<br>*/
    private void b570CloseCursorIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B580-FETCH-FIRST-IBO-CPZ<br>*/
    private void b580FetchFirstIboCpz() {
        // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.
        b560OpenCursorIboCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
    }

    /**Original name: B590-FETCH-NEXT-IBO-CPZ<br>*/
    private void b590FetchNextIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B605-DECLARE-CURSOR-IBS-CPZ<br>
	 * <pre>----
	 * ----  gestione IBS Competenza
	 * ----</pre>*/
    private void b605DeclareCursorIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B610-SELECT-IBS-CPZ<br>*/
    private void b610SelectIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B660-OPEN-CURSOR-IBS-CPZ<br>*/
    private void b660OpenCursorIbsCpz() {
        // COB_CODE: PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.
        b605DeclareCursorIbsCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B670-CLOSE-CURSOR-IBS-CPZ<br>*/
    private void b670CloseCursorIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B680-FETCH-FIRST-IBS-CPZ<br>*/
    private void b680FetchFirstIbsCpz() {
        // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.
        b660OpenCursorIbsCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
    }

    /**Original name: B690-FETCH-NEXT-IBS-CPZ<br>*/
    private void b690FetchNextIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B705-DECLARE-CURSOR-IDO-CPZ<br>
	 * <pre>----
	 * ----  gestione IDO Competenza
	 * ----</pre>*/
    private void b705DeclareCursorIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IDO-CPZ-E15 CURSOR FOR
        //              SELECT
        //                     ID_EST_RAPP_ANA
        //                    ,ID_RAPP_ANA
        //                    ,ID_RAPP_ANA_COLLG
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,COD_SOGG
        //                    ,TP_RAPP_ANA
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,ID_SEGMENTAZ_CLI
        //                    ,FL_COINC_TIT_EFF
        //                    ,FL_PERS_ESP_POL
        //                    ,DESC_PERS_ESP_POL
        //                    ,TP_LEG_CNTR
        //                    ,DESC_LEG_CNTR
        //                    ,TP_LEG_PERC_BNFICR
        //                    ,D_LEG_PERC_BNFICR
        //                    ,TP_CNT_CORR
        //                    ,TP_COINC_PIC_PAC
        //              FROM EST_RAPP_ANA
        //              WHERE     ID_OGG = :E15-ID-OGG
        //           AND TP_OGG = :E15-TP-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //              ORDER BY ID_EST_RAPP_ANA ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B710-SELECT-IDO-CPZ<br>*/
    private void b710SelectIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_EST_RAPP_ANA
        //                ,ID_RAPP_ANA
        //                ,ID_RAPP_ANA_COLLG
        //                ,ID_OGG
        //                ,TP_OGG
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,COD_SOGG
        //                ,TP_RAPP_ANA
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,ID_SEGMENTAZ_CLI
        //                ,FL_COINC_TIT_EFF
        //                ,FL_PERS_ESP_POL
        //                ,DESC_PERS_ESP_POL
        //                ,TP_LEG_CNTR
        //                ,DESC_LEG_CNTR
        //                ,TP_LEG_PERC_BNFICR
        //                ,D_LEG_PERC_BNFICR
        //                ,TP_CNT_CORR
        //                ,TP_COINC_PIC_PAC
        //             INTO
        //                :E15-ID-EST-RAPP-ANA
        //               ,:E15-ID-RAPP-ANA
        //               ,:E15-ID-RAPP-ANA-COLLG
        //                :IND-E15-ID-RAPP-ANA-COLLG
        //               ,:E15-ID-OGG
        //               ,:E15-TP-OGG
        //               ,:E15-ID-MOVI-CRZ
        //               ,:E15-ID-MOVI-CHIU
        //                :IND-E15-ID-MOVI-CHIU
        //               ,:E15-DT-INI-EFF-DB
        //               ,:E15-DT-END-EFF-DB
        //               ,:E15-COD-COMP-ANIA
        //               ,:E15-COD-SOGG
        //                :IND-E15-COD-SOGG
        //               ,:E15-TP-RAPP-ANA
        //               ,:E15-DS-RIGA
        //               ,:E15-DS-OPER-SQL
        //               ,:E15-DS-VER
        //               ,:E15-DS-TS-INI-CPTZ
        //               ,:E15-DS-TS-END-CPTZ
        //               ,:E15-DS-UTENTE
        //               ,:E15-DS-STATO-ELAB
        //               ,:E15-ID-SEGMENTAZ-CLI
        //                :IND-E15-ID-SEGMENTAZ-CLI
        //               ,:E15-FL-COINC-TIT-EFF
        //                :IND-E15-FL-COINC-TIT-EFF
        //               ,:E15-FL-PERS-ESP-POL
        //                :IND-E15-FL-PERS-ESP-POL
        //               ,:E15-DESC-PERS-ESP-POL-VCHAR
        //                :IND-E15-DESC-PERS-ESP-POL
        //               ,:E15-TP-LEG-CNTR
        //                :IND-E15-TP-LEG-CNTR
        //               ,:E15-DESC-LEG-CNTR-VCHAR
        //                :IND-E15-DESC-LEG-CNTR
        //               ,:E15-TP-LEG-PERC-BNFICR
        //                :IND-E15-TP-LEG-PERC-BNFICR
        //               ,:E15-D-LEG-PERC-BNFICR-VCHAR
        //                :IND-E15-D-LEG-PERC-BNFICR
        //               ,:E15-TP-CNT-CORR
        //                :IND-E15-TP-CNT-CORR
        //               ,:E15-TP-COINC-PIC-PAC
        //                :IND-E15-TP-COINC-PIC-PAC
        //             FROM EST_RAPP_ANA
        //             WHERE     ID_OGG = :E15-ID-OGG
        //                    AND TP_OGG = :E15-TP-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        estRappAnaDao.selectRec5(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B760-OPEN-CURSOR-IDO-CPZ<br>*/
    private void b760OpenCursorIdoCpz() {
        // COB_CODE: PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.
        b705DeclareCursorIdoCpz();
        // COB_CODE: EXEC SQL
        //                OPEN C-IDO-CPZ-E15
        //           END-EXEC.
        estRappAnaDao.openCIdoCpzE15(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B770-CLOSE-CURSOR-IDO-CPZ<br>*/
    private void b770CloseCursorIdoCpz() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IDO-CPZ-E15
        //           END-EXEC.
        estRappAnaDao.closeCIdoCpzE15();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B780-FETCH-FIRST-IDO-CPZ<br>*/
    private void b780FetchFirstIdoCpz() {
        // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.
        b760OpenCursorIdoCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
    }

    /**Original name: B790-FETCH-NEXT-IDO-CPZ<br>*/
    private void b790FetchNextIdoCpz() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IDO-CPZ-E15
        //           INTO
        //                :E15-ID-EST-RAPP-ANA
        //               ,:E15-ID-RAPP-ANA
        //               ,:E15-ID-RAPP-ANA-COLLG
        //                :IND-E15-ID-RAPP-ANA-COLLG
        //               ,:E15-ID-OGG
        //               ,:E15-TP-OGG
        //               ,:E15-ID-MOVI-CRZ
        //               ,:E15-ID-MOVI-CHIU
        //                :IND-E15-ID-MOVI-CHIU
        //               ,:E15-DT-INI-EFF-DB
        //               ,:E15-DT-END-EFF-DB
        //               ,:E15-COD-COMP-ANIA
        //               ,:E15-COD-SOGG
        //                :IND-E15-COD-SOGG
        //               ,:E15-TP-RAPP-ANA
        //               ,:E15-DS-RIGA
        //               ,:E15-DS-OPER-SQL
        //               ,:E15-DS-VER
        //               ,:E15-DS-TS-INI-CPTZ
        //               ,:E15-DS-TS-END-CPTZ
        //               ,:E15-DS-UTENTE
        //               ,:E15-DS-STATO-ELAB
        //               ,:E15-ID-SEGMENTAZ-CLI
        //                :IND-E15-ID-SEGMENTAZ-CLI
        //               ,:E15-FL-COINC-TIT-EFF
        //                :IND-E15-FL-COINC-TIT-EFF
        //               ,:E15-FL-PERS-ESP-POL
        //                :IND-E15-FL-PERS-ESP-POL
        //               ,:E15-DESC-PERS-ESP-POL-VCHAR
        //                :IND-E15-DESC-PERS-ESP-POL
        //               ,:E15-TP-LEG-CNTR
        //                :IND-E15-TP-LEG-CNTR
        //               ,:E15-DESC-LEG-CNTR-VCHAR
        //                :IND-E15-DESC-LEG-CNTR
        //               ,:E15-TP-LEG-PERC-BNFICR
        //                :IND-E15-TP-LEG-PERC-BNFICR
        //               ,:E15-D-LEG-PERC-BNFICR-VCHAR
        //                :IND-E15-D-LEG-PERC-BNFICR
        //               ,:E15-TP-CNT-CORR
        //                :IND-E15-TP-CNT-CORR
        //               ,:E15-TP-COINC-PIC-PAC
        //                :IND-E15-TP-COINC-PIC-PAC
        //           END-EXEC.
        estRappAnaDao.fetchCIdoCpzE15(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B770-CLOSE-CURSOR-IDO-CPZ     THRU B770-EX
            b770CloseCursorIdoCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-E15-ID-RAPP-ANA-COLLG = -1
        //              MOVE HIGH-VALUES TO E15-ID-RAPP-ANA-COLLG-NULL
        //           END-IF
        if (ws.getIndEstRappAna().getIdRappAnaCollg() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO E15-ID-RAPP-ANA-COLLG-NULL
            estRappAna.getE15IdRappAnaCollg().setE15IdRappAnaCollgNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, E15IdRappAnaCollg.Len.E15_ID_RAPP_ANA_COLLG_NULL));
        }
        // COB_CODE: IF IND-E15-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO E15-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndEstRappAna().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO E15-ID-MOVI-CHIU-NULL
            estRappAna.getE15IdMoviChiu().setE15IdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, E15IdMoviChiu.Len.E15_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-E15-COD-SOGG = -1
        //              MOVE HIGH-VALUES TO E15-COD-SOGG-NULL
        //           END-IF
        if (ws.getIndEstRappAna().getCodSogg() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO E15-COD-SOGG-NULL
            estRappAna.setE15CodSogg(LiteralGenerator.create(Types.HIGH_CHAR_VAL, EstRappAnaIdbse150.Len.E15_COD_SOGG));
        }
        // COB_CODE: IF IND-E15-ID-SEGMENTAZ-CLI = -1
        //              MOVE HIGH-VALUES TO E15-ID-SEGMENTAZ-CLI-NULL
        //           END-IF
        if (ws.getIndEstRappAna().getIdSegmentazCli() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO E15-ID-SEGMENTAZ-CLI-NULL
            estRappAna.getE15IdSegmentazCli().setE15IdSegmentazCliNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, E15IdSegmentazCli.Len.E15_ID_SEGMENTAZ_CLI_NULL));
        }
        // COB_CODE: IF IND-E15-FL-COINC-TIT-EFF = -1
        //              MOVE HIGH-VALUES TO E15-FL-COINC-TIT-EFF-NULL
        //           END-IF
        if (ws.getIndEstRappAna().getFlCoincTitEff() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO E15-FL-COINC-TIT-EFF-NULL
            estRappAna.setE15FlCoincTitEff(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-E15-FL-PERS-ESP-POL = -1
        //              MOVE HIGH-VALUES TO E15-FL-PERS-ESP-POL-NULL
        //           END-IF
        if (ws.getIndEstRappAna().getFlPersEspPol() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO E15-FL-PERS-ESP-POL-NULL
            estRappAna.setE15FlPersEspPol(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-E15-DESC-PERS-ESP-POL = -1
        //              MOVE HIGH-VALUES TO E15-DESC-PERS-ESP-POL
        //           END-IF
        if (ws.getIndEstRappAna().getDescPersEspPol() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO E15-DESC-PERS-ESP-POL
            estRappAna.setE15DescPersEspPol(LiteralGenerator.create(Types.HIGH_CHAR_VAL, EstRappAnaIdbse150.Len.E15_DESC_PERS_ESP_POL));
        }
        // COB_CODE: IF IND-E15-TP-LEG-CNTR = -1
        //              MOVE HIGH-VALUES TO E15-TP-LEG-CNTR-NULL
        //           END-IF
        if (ws.getIndEstRappAna().getTpLegCntr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO E15-TP-LEG-CNTR-NULL
            estRappAna.setE15TpLegCntr(LiteralGenerator.create(Types.HIGH_CHAR_VAL, EstRappAnaIdbse150.Len.E15_TP_LEG_CNTR));
        }
        // COB_CODE: IF IND-E15-DESC-LEG-CNTR = -1
        //              MOVE HIGH-VALUES TO E15-DESC-LEG-CNTR
        //           END-IF
        if (ws.getIndEstRappAna().getDescLegCntr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO E15-DESC-LEG-CNTR
            estRappAna.setE15DescLegCntr(LiteralGenerator.create(Types.HIGH_CHAR_VAL, EstRappAnaIdbse150.Len.E15_DESC_LEG_CNTR));
        }
        // COB_CODE: IF IND-E15-TP-LEG-PERC-BNFICR = -1
        //              MOVE HIGH-VALUES TO E15-TP-LEG-PERC-BNFICR-NULL
        //           END-IF
        if (ws.getIndEstRappAna().getTpLegPercBnficr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO E15-TP-LEG-PERC-BNFICR-NULL
            estRappAna.setE15TpLegPercBnficr(LiteralGenerator.create(Types.HIGH_CHAR_VAL, EstRappAnaIdbse150.Len.E15_TP_LEG_PERC_BNFICR));
        }
        // COB_CODE: IF IND-E15-D-LEG-PERC-BNFICR = -1
        //              MOVE HIGH-VALUES TO E15-D-LEG-PERC-BNFICR
        //           END-IF
        if (ws.getIndEstRappAna().getdLegPercBnficr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO E15-D-LEG-PERC-BNFICR
            estRappAna.setE15DLegPercBnficr(LiteralGenerator.create(Types.HIGH_CHAR_VAL, EstRappAnaIdbse150.Len.E15_D_LEG_PERC_BNFICR));
        }
        // COB_CODE: IF IND-E15-TP-CNT-CORR = -1
        //              MOVE HIGH-VALUES TO E15-TP-CNT-CORR-NULL
        //           END-IF
        if (ws.getIndEstRappAna().getTpCntCorr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO E15-TP-CNT-CORR-NULL
            estRappAna.setE15TpCntCorr(LiteralGenerator.create(Types.HIGH_CHAR_VAL, EstRappAnaIdbse150.Len.E15_TP_CNT_CORR));
        }
        // COB_CODE: IF IND-E15-TP-COINC-PIC-PAC = -1
        //              MOVE HIGH-VALUES TO E15-TP-COINC-PIC-PAC-NULL
        //           END-IF.
        if (ws.getIndEstRappAna().getTpCoincPicPac() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO E15-TP-COINC-PIC-PAC-NULL
            estRappAna.setE15TpCoincPicPac(LiteralGenerator.create(Types.HIGH_CHAR_VAL, EstRappAnaIdbse150.Len.E15_TP_COINC_PIC_PAC));
        }
    }

    /**Original name: Z150-VALORIZZA-DATA-SERVICES-I<br>*/
    private void z150ValorizzaDataServicesI() {
        // COB_CODE: MOVE 'I' TO E15-DS-OPER-SQL
        estRappAna.setE15DsOperSqlFormatted("I");
        // COB_CODE: MOVE 0                   TO E15-DS-VER
        estRappAna.setE15DsVer(0);
        // COB_CODE: MOVE IDSV0003-USER-NAME TO E15-DS-UTENTE
        estRappAna.setE15DsUtente(idsv0003.getUserName());
        // COB_CODE: MOVE '1'                   TO E15-DS-STATO-ELAB.
        estRappAna.setE15DsStatoElabFormatted("1");
    }

    /**Original name: Z160-VALORIZZA-DATA-SERVICES-U<br>*/
    private void z160ValorizzaDataServicesU() {
        // COB_CODE: MOVE 'U' TO E15-DS-OPER-SQL
        estRappAna.setE15DsOperSqlFormatted("U");
        // COB_CODE: MOVE IDSV0003-USER-NAME TO E15-DS-UTENTE.
        estRappAna.setE15DsUtente(idsv0003.getUserName());
    }

    /**Original name: Z200-SET-INDICATORI-NULL<br>*/
    private void z200SetIndicatoriNull() {
        // COB_CODE: IF E15-ID-RAPP-ANA-COLLG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-E15-ID-RAPP-ANA-COLLG
        //           ELSE
        //              MOVE 0 TO IND-E15-ID-RAPP-ANA-COLLG
        //           END-IF
        if (Characters.EQ_HIGH.test(estRappAna.getE15IdRappAnaCollg().getE15IdRappAnaCollgNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-E15-ID-RAPP-ANA-COLLG
            ws.getIndEstRappAna().setIdRappAnaCollg(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-E15-ID-RAPP-ANA-COLLG
            ws.getIndEstRappAna().setIdRappAnaCollg(((short)0));
        }
        // COB_CODE: IF E15-ID-MOVI-CHIU-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-E15-ID-MOVI-CHIU
        //           ELSE
        //              MOVE 0 TO IND-E15-ID-MOVI-CHIU
        //           END-IF
        if (Characters.EQ_HIGH.test(estRappAna.getE15IdMoviChiu().getE15IdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-E15-ID-MOVI-CHIU
            ws.getIndEstRappAna().setIdMoviChiu(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-E15-ID-MOVI-CHIU
            ws.getIndEstRappAna().setIdMoviChiu(((short)0));
        }
        // COB_CODE: IF E15-COD-SOGG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-E15-COD-SOGG
        //           ELSE
        //              MOVE 0 TO IND-E15-COD-SOGG
        //           END-IF
        if (Characters.EQ_HIGH.test(estRappAna.getE15CodSoggFormatted())) {
            // COB_CODE: MOVE -1 TO IND-E15-COD-SOGG
            ws.getIndEstRappAna().setCodSogg(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-E15-COD-SOGG
            ws.getIndEstRappAna().setCodSogg(((short)0));
        }
        // COB_CODE: IF E15-ID-SEGMENTAZ-CLI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-E15-ID-SEGMENTAZ-CLI
        //           ELSE
        //              MOVE 0 TO IND-E15-ID-SEGMENTAZ-CLI
        //           END-IF
        if (Characters.EQ_HIGH.test(estRappAna.getE15IdSegmentazCli().getE15IdSegmentazCliNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-E15-ID-SEGMENTAZ-CLI
            ws.getIndEstRappAna().setIdSegmentazCli(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-E15-ID-SEGMENTAZ-CLI
            ws.getIndEstRappAna().setIdSegmentazCli(((short)0));
        }
        // COB_CODE: IF E15-FL-COINC-TIT-EFF-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-E15-FL-COINC-TIT-EFF
        //           ELSE
        //              MOVE 0 TO IND-E15-FL-COINC-TIT-EFF
        //           END-IF
        if (Conditions.eq(estRappAna.getE15FlCoincTitEff(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-E15-FL-COINC-TIT-EFF
            ws.getIndEstRappAna().setFlCoincTitEff(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-E15-FL-COINC-TIT-EFF
            ws.getIndEstRappAna().setFlCoincTitEff(((short)0));
        }
        // COB_CODE: IF E15-FL-PERS-ESP-POL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-E15-FL-PERS-ESP-POL
        //           ELSE
        //              MOVE 0 TO IND-E15-FL-PERS-ESP-POL
        //           END-IF
        if (Conditions.eq(estRappAna.getE15FlPersEspPol(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-E15-FL-PERS-ESP-POL
            ws.getIndEstRappAna().setFlPersEspPol(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-E15-FL-PERS-ESP-POL
            ws.getIndEstRappAna().setFlPersEspPol(((short)0));
        }
        // COB_CODE: IF E15-DESC-PERS-ESP-POL = HIGH-VALUES
        //              MOVE -1 TO IND-E15-DESC-PERS-ESP-POL
        //           ELSE
        //              MOVE 0 TO IND-E15-DESC-PERS-ESP-POL
        //           END-IF
        if (Characters.EQ_HIGH.test(estRappAna.getE15DescPersEspPol(), EstRappAnaIdbse150.Len.E15_DESC_PERS_ESP_POL)) {
            // COB_CODE: MOVE -1 TO IND-E15-DESC-PERS-ESP-POL
            ws.getIndEstRappAna().setDescPersEspPol(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-E15-DESC-PERS-ESP-POL
            ws.getIndEstRappAna().setDescPersEspPol(((short)0));
        }
        // COB_CODE: IF E15-TP-LEG-CNTR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-E15-TP-LEG-CNTR
        //           ELSE
        //              MOVE 0 TO IND-E15-TP-LEG-CNTR
        //           END-IF
        if (Characters.EQ_HIGH.test(estRappAna.getE15TpLegCntrFormatted())) {
            // COB_CODE: MOVE -1 TO IND-E15-TP-LEG-CNTR
            ws.getIndEstRappAna().setTpLegCntr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-E15-TP-LEG-CNTR
            ws.getIndEstRappAna().setTpLegCntr(((short)0));
        }
        // COB_CODE: IF E15-DESC-LEG-CNTR = HIGH-VALUES
        //              MOVE -1 TO IND-E15-DESC-LEG-CNTR
        //           ELSE
        //              MOVE 0 TO IND-E15-DESC-LEG-CNTR
        //           END-IF
        if (Characters.EQ_HIGH.test(estRappAna.getE15DescLegCntr(), EstRappAnaIdbse150.Len.E15_DESC_LEG_CNTR)) {
            // COB_CODE: MOVE -1 TO IND-E15-DESC-LEG-CNTR
            ws.getIndEstRappAna().setDescLegCntr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-E15-DESC-LEG-CNTR
            ws.getIndEstRappAna().setDescLegCntr(((short)0));
        }
        // COB_CODE: IF E15-TP-LEG-PERC-BNFICR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-E15-TP-LEG-PERC-BNFICR
        //           ELSE
        //              MOVE 0 TO IND-E15-TP-LEG-PERC-BNFICR
        //           END-IF
        if (Characters.EQ_HIGH.test(estRappAna.getE15TpLegPercBnficrFormatted())) {
            // COB_CODE: MOVE -1 TO IND-E15-TP-LEG-PERC-BNFICR
            ws.getIndEstRappAna().setTpLegPercBnficr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-E15-TP-LEG-PERC-BNFICR
            ws.getIndEstRappAna().setTpLegPercBnficr(((short)0));
        }
        // COB_CODE: IF E15-D-LEG-PERC-BNFICR = HIGH-VALUES
        //              MOVE -1 TO IND-E15-D-LEG-PERC-BNFICR
        //           ELSE
        //              MOVE 0 TO IND-E15-D-LEG-PERC-BNFICR
        //           END-IF
        if (Characters.EQ_HIGH.test(estRappAna.getE15DLegPercBnficr(), EstRappAnaIdbse150.Len.E15_D_LEG_PERC_BNFICR)) {
            // COB_CODE: MOVE -1 TO IND-E15-D-LEG-PERC-BNFICR
            ws.getIndEstRappAna().setdLegPercBnficr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-E15-D-LEG-PERC-BNFICR
            ws.getIndEstRappAna().setdLegPercBnficr(((short)0));
        }
        // COB_CODE: IF E15-TP-CNT-CORR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-E15-TP-CNT-CORR
        //           ELSE
        //              MOVE 0 TO IND-E15-TP-CNT-CORR
        //           END-IF
        if (Characters.EQ_HIGH.test(estRappAna.getE15TpCntCorrFormatted())) {
            // COB_CODE: MOVE -1 TO IND-E15-TP-CNT-CORR
            ws.getIndEstRappAna().setTpCntCorr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-E15-TP-CNT-CORR
            ws.getIndEstRappAna().setTpCntCorr(((short)0));
        }
        // COB_CODE: IF E15-TP-COINC-PIC-PAC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-E15-TP-COINC-PIC-PAC
        //           ELSE
        //              MOVE 0 TO IND-E15-TP-COINC-PIC-PAC
        //           END-IF.
        if (Characters.EQ_HIGH.test(estRappAna.getE15TpCoincPicPacFormatted())) {
            // COB_CODE: MOVE -1 TO IND-E15-TP-COINC-PIC-PAC
            ws.getIndEstRappAna().setTpCoincPicPac(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-E15-TP-COINC-PIC-PAC
            ws.getIndEstRappAna().setTpCoincPicPac(((short)0));
        }
    }

    /**Original name: Z400-SEQ-RIGA<br>*/
    private void z400SeqRiga() {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_RIGA
        //              INTO : E15-DS-RIGA
        //           END-EXEC.
        //TODO: Implement: valuesStmt;
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: Z500-AGGIORNAMENTO-STORICO<br>*/
    private void z500AggiornamentoStorico() {
        // COB_CODE: MOVE EST-RAPP-ANA TO WS-BUFFER-TABLE.
        ws.setWsBufferTable(estRappAna.getEstRappAnaFormatted());
        // COB_CODE: MOVE E15-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.
        ws.setWsIdMoviCrz(TruncAbs.toInt(estRappAna.getE15IdMoviCrz(), 9));
        // COB_CODE: PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.
        a360OpenCursorIdEff();
        // COB_CODE: PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-SQL
        //              END-IF
        //           END-PERFORM.
        while (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX
            a390FetchNextIdEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              END-IF
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: MOVE WS-ID-MOVI-CRZ   TO E15-ID-MOVI-CHIU
                estRappAna.getE15IdMoviChiu().setE15IdMoviChiu(ws.getWsIdMoviCrz());
                // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
                //                                 TO E15-DS-TS-END-CPTZ
                estRappAna.setE15DsTsEndCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
                // COB_CODE: PERFORM A330-UPDATE-ID-EFF THRU A330-EX
                a330UpdateIdEff();
                // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
                //                 END-IF
                //           END-IF
                if (idsv0003.getSqlcode().isSuccessfulSql()) {
                    // COB_CODE: MOVE WS-ID-MOVI-CRZ TO E15-ID-MOVI-CRZ
                    estRappAna.setE15IdMoviCrz(ws.getWsIdMoviCrz());
                    // COB_CODE: MOVE HIGH-VALUES    TO E15-ID-MOVI-CHIU-NULL
                    estRappAna.getE15IdMoviChiu().setE15IdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, E15IdMoviChiu.Len.E15_ID_MOVI_CHIU_NULL));
                    // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
                    //                               TO E15-DT-END-EFF
                    estRappAna.setE15DtEndEff(idsv0003.getDataInizioEffetto());
                    // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
                    //                               TO E15-DS-TS-INI-CPTZ
                    estRappAna.setE15DsTsIniCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
                    // COB_CODE: MOVE WS-TS-INFINITO
                    //                               TO E15-DS-TS-END-CPTZ
                    estRappAna.setE15DsTsEndCptz(ws.getIdsv0010().getWsTsInfinito());
                    // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
                    //              PERFORM A220-INSERT-PK THRU A220-EX
                    //           END-IF
                    if (idsv0003.getSqlcode().isSuccessfulSql()) {
                        // COB_CODE: PERFORM A220-INSERT-PK THRU A220-EX
                        a220InsertPk();
                    }
                }
            }
        }
        // COB_CODE: IF IDSV0003-NOT-FOUND
        //              END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF NOT IDSV0003-DELETE-LOGICA
            //              PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX
            //           ELSE
            //              SET IDSV0003-SUCCESSFUL-SQL TO TRUE
            //           END-IF
            if (!idsv0003.getOperazione().isDeleteLogica()) {
                // COB_CODE: PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX
                z600InsertNuovaRigaStorica();
            }
            else {
                // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL TO TRUE
                idsv0003.getSqlcode().setSuccessfulSql();
            }
        }
    }

    /**Original name: Z550-AGG-STORICO-SOLO-INS<br>*/
    private void z550AggStoricoSoloIns() {
        // COB_CODE: MOVE EST-RAPP-ANA TO WS-BUFFER-TABLE.
        ws.setWsBufferTable(estRappAna.getEstRappAnaFormatted());
        // COB_CODE: MOVE E15-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.
        ws.setWsIdMoviCrz(TruncAbs.toInt(estRappAna.getE15IdMoviCrz(), 9));
        // COB_CODE: PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX.
        z600InsertNuovaRigaStorica();
    }

    /**Original name: Z600-INSERT-NUOVA-RIGA-STORICA<br>*/
    private void z600InsertNuovaRigaStorica() {
        // COB_CODE: MOVE WS-BUFFER-TABLE TO EST-RAPP-ANA.
        estRappAna.setEstRappAnaFormatted(ws.getWsBufferTableFormatted());
        // COB_CODE: MOVE WS-ID-MOVI-CRZ  TO E15-ID-MOVI-CRZ.
        estRappAna.setE15IdMoviCrz(ws.getWsIdMoviCrz());
        // COB_CODE: MOVE HIGH-VALUES     TO E15-ID-MOVI-CHIU-NULL.
        estRappAna.getE15IdMoviChiu().setE15IdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, E15IdMoviChiu.Len.E15_ID_MOVI_CHIU_NULL));
        // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
        //                                TO E15-DT-INI-EFF.
        estRappAna.setE15DtIniEff(idsv0003.getDataInizioEffetto());
        // COB_CODE: MOVE WS-DT-INFINITO
        //                                TO E15-DT-END-EFF.
        estRappAna.setE15DtEndEff(ws.getIdsv0010().getWsDtInfinito());
        // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
        //                                TO E15-DS-TS-INI-CPTZ.
        estRappAna.setE15DsTsIniCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
        // COB_CODE: MOVE WS-TS-INFINITO
        //                                TO E15-DS-TS-END-CPTZ.
        estRappAna.setE15DsTsEndCptz(ws.getIdsv0010().getWsTsInfinito());
        // COB_CODE: MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
        //                                TO E15-COD-COMP-ANIA.
        estRappAna.setE15CodCompAnia(idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A220-INSERT-PK THRU A220-EX.
        a220InsertPk();
    }

    /**Original name: Z900-CONVERTI-N-TO-X<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da 9(8) comp-3 a date
	 * ----</pre>*/
    private void z900ConvertiNToX() {
        // COB_CODE: MOVE E15-DT-INI-EFF TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(estRappAna.getE15DtIniEff(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO E15-DT-INI-EFF-DB
        ws.getIdbve153().setE15DtIniEffDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: MOVE E15-DT-END-EFF TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(estRappAna.getE15DtEndEff(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO E15-DT-END-EFF-DB.
        ws.getIdbve153().setE15DtEndEffDb(ws.getIdsv0010().getWsDateX());
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE E15-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getIdbve153().getE15DtIniEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO E15-DT-INI-EFF
        estRappAna.setE15DtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE E15-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getIdbve153().getE15DtEndEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO E15-DT-END-EFF.
        estRappAna.setE15DtEndEff(ws.getIdsv0010().getWsDateN());
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
        // COB_CODE: MOVE LENGTH OF E15-DESC-PERS-ESP-POL
        //                       TO E15-DESC-PERS-ESP-POL-LEN
        estRappAna.setE15DescPersEspPolLen(((short)EstRappAnaIdbse150.Len.E15_DESC_PERS_ESP_POL));
        // COB_CODE: MOVE LENGTH OF E15-DESC-LEG-CNTR
        //                       TO E15-DESC-LEG-CNTR-LEN
        estRappAna.setE15DescLegCntrLen(((short)EstRappAnaIdbse150.Len.E15_DESC_LEG_CNTR));
        // COB_CODE: MOVE LENGTH OF E15-D-LEG-PERC-BNFICR
        //                       TO E15-D-LEG-PERC-BNFICR-LEN.
        estRappAna.setE15DLegPercBnficrLen(((short)EstRappAnaIdbse150.Len.E15_D_LEG_PERC_BNFICR));
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public int getCodCompAnia() {
        return estRappAna.getE15CodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.estRappAna.setE15CodCompAnia(codCompAnia);
    }

    @Override
    public String getCodSogg() {
        return estRappAna.getE15CodSogg();
    }

    @Override
    public void setCodSogg(String codSogg) {
        this.estRappAna.setE15CodSogg(codSogg);
    }

    @Override
    public String getCodSoggObj() {
        if (ws.getIndEstRappAna().getCodSogg() >= 0) {
            return getCodSogg();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodSoggObj(String codSoggObj) {
        if (codSoggObj != null) {
            setCodSogg(codSoggObj);
            ws.getIndEstRappAna().setCodSogg(((short)0));
        }
        else {
            ws.getIndEstRappAna().setCodSogg(((short)-1));
        }
    }

    @Override
    public String getDescLegCntrVchar() {
        return estRappAna.getE15DescLegCntrVcharFormatted();
    }

    @Override
    public void setDescLegCntrVchar(String descLegCntrVchar) {
        this.estRappAna.setE15DescLegCntrVcharFormatted(descLegCntrVchar);
    }

    @Override
    public String getDescLegCntrVcharObj() {
        if (ws.getIndEstRappAna().getDescLegCntr() >= 0) {
            return getDescLegCntrVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDescLegCntrVcharObj(String descLegCntrVcharObj) {
        if (descLegCntrVcharObj != null) {
            setDescLegCntrVchar(descLegCntrVcharObj);
            ws.getIndEstRappAna().setDescLegCntr(((short)0));
        }
        else {
            ws.getIndEstRappAna().setDescLegCntr(((short)-1));
        }
    }

    @Override
    public String getDescPersEspPolVchar() {
        return estRappAna.getE15DescPersEspPolVcharFormatted();
    }

    @Override
    public void setDescPersEspPolVchar(String descPersEspPolVchar) {
        this.estRappAna.setE15DescPersEspPolVcharFormatted(descPersEspPolVchar);
    }

    @Override
    public String getDescPersEspPolVcharObj() {
        if (ws.getIndEstRappAna().getDescPersEspPol() >= 0) {
            return getDescPersEspPolVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDescPersEspPolVcharObj(String descPersEspPolVcharObj) {
        if (descPersEspPolVcharObj != null) {
            setDescPersEspPolVchar(descPersEspPolVcharObj);
            ws.getIndEstRappAna().setDescPersEspPol(((short)0));
        }
        else {
            ws.getIndEstRappAna().setDescPersEspPol(((short)-1));
        }
    }

    @Override
    public char getDsOperSql() {
        return estRappAna.getE15DsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.estRappAna.setE15DsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return estRappAna.getE15DsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.estRappAna.setE15DsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsEndCptz() {
        return estRappAna.getE15DsTsEndCptz();
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.estRappAna.setE15DsTsEndCptz(dsTsEndCptz);
    }

    @Override
    public long getDsTsIniCptz() {
        return estRappAna.getE15DsTsIniCptz();
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.estRappAna.setE15DsTsIniCptz(dsTsIniCptz);
    }

    @Override
    public String getDsUtente() {
        return estRappAna.getE15DsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.estRappAna.setE15DsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return estRappAna.getE15DsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.estRappAna.setE15DsVer(dsVer);
    }

    @Override
    public String getDtEndEffDb() {
        return ws.getIdbve153().getE15DtEndEffDb();
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        this.ws.getIdbve153().setE15DtEndEffDb(dtEndEffDb);
    }

    @Override
    public String getDtIniEffDb() {
        return ws.getIdbve153().getE15DtIniEffDb();
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        this.ws.getIdbve153().setE15DtIniEffDb(dtIniEffDb);
    }

    @Override
    public long getE15DsRiga() {
        return estRappAna.getE15DsRiga();
    }

    @Override
    public void setE15DsRiga(long e15DsRiga) {
        this.estRappAna.setE15DsRiga(e15DsRiga);
    }

    @Override
    public int getE15IdOgg() {
        return estRappAna.getE15IdOgg();
    }

    @Override
    public void setE15IdOgg(int e15IdOgg) {
        this.estRappAna.setE15IdOgg(e15IdOgg);
    }

    @Override
    public String getE15TpOgg() {
        return estRappAna.getE15TpOgg();
    }

    @Override
    public void setE15TpOgg(String e15TpOgg) {
        this.estRappAna.setE15TpOgg(e15TpOgg);
    }

    @Override
    public char getFlCoincTitEff() {
        return estRappAna.getE15FlCoincTitEff();
    }

    @Override
    public void setFlCoincTitEff(char flCoincTitEff) {
        this.estRappAna.setE15FlCoincTitEff(flCoincTitEff);
    }

    @Override
    public Character getFlCoincTitEffObj() {
        if (ws.getIndEstRappAna().getFlCoincTitEff() >= 0) {
            return ((Character)getFlCoincTitEff());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlCoincTitEffObj(Character flCoincTitEffObj) {
        if (flCoincTitEffObj != null) {
            setFlCoincTitEff(((char)flCoincTitEffObj));
            ws.getIndEstRappAna().setFlCoincTitEff(((short)0));
        }
        else {
            ws.getIndEstRappAna().setFlCoincTitEff(((short)-1));
        }
    }

    @Override
    public char getFlPersEspPol() {
        return estRappAna.getE15FlPersEspPol();
    }

    @Override
    public void setFlPersEspPol(char flPersEspPol) {
        this.estRappAna.setE15FlPersEspPol(flPersEspPol);
    }

    @Override
    public Character getFlPersEspPolObj() {
        if (ws.getIndEstRappAna().getFlPersEspPol() >= 0) {
            return ((Character)getFlPersEspPol());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlPersEspPolObj(Character flPersEspPolObj) {
        if (flPersEspPolObj != null) {
            setFlPersEspPol(((char)flPersEspPolObj));
            ws.getIndEstRappAna().setFlPersEspPol(((short)0));
        }
        else {
            ws.getIndEstRappAna().setFlPersEspPol(((short)-1));
        }
    }

    @Override
    public int getIdEstRappAna() {
        return estRappAna.getE15IdEstRappAna();
    }

    @Override
    public void setIdEstRappAna(int idEstRappAna) {
        this.estRappAna.setE15IdEstRappAna(idEstRappAna);
    }

    @Override
    public int getIdMoviChiu() {
        return estRappAna.getE15IdMoviChiu().getE15IdMoviChiu();
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        this.estRappAna.getE15IdMoviChiu().setE15IdMoviChiu(idMoviChiu);
    }

    @Override
    public Integer getIdMoviChiuObj() {
        if (ws.getIndEstRappAna().getIdMoviChiu() >= 0) {
            return ((Integer)getIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        if (idMoviChiuObj != null) {
            setIdMoviChiu(((int)idMoviChiuObj));
            ws.getIndEstRappAna().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndEstRappAna().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getIdMoviCrz() {
        return estRappAna.getE15IdMoviCrz();
    }

    @Override
    public void setIdMoviCrz(int idMoviCrz) {
        this.estRappAna.setE15IdMoviCrz(idMoviCrz);
    }

    @Override
    public int getIdRappAna() {
        return estRappAna.getE15IdRappAna();
    }

    @Override
    public void setIdRappAna(int idRappAna) {
        this.estRappAna.setE15IdRappAna(idRappAna);
    }

    @Override
    public int getIdRappAnaCollg() {
        return estRappAna.getE15IdRappAnaCollg().getE15IdRappAnaCollg();
    }

    @Override
    public void setIdRappAnaCollg(int idRappAnaCollg) {
        this.estRappAna.getE15IdRappAnaCollg().setE15IdRappAnaCollg(idRappAnaCollg);
    }

    @Override
    public Integer getIdRappAnaCollgObj() {
        if (ws.getIndEstRappAna().getIdRappAnaCollg() >= 0) {
            return ((Integer)getIdRappAnaCollg());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdRappAnaCollgObj(Integer idRappAnaCollgObj) {
        if (idRappAnaCollgObj != null) {
            setIdRappAnaCollg(((int)idRappAnaCollgObj));
            ws.getIndEstRappAna().setIdRappAnaCollg(((short)0));
        }
        else {
            ws.getIndEstRappAna().setIdRappAnaCollg(((short)-1));
        }
    }

    @Override
    public int getIdSegmentazCli() {
        return estRappAna.getE15IdSegmentazCli().getE15IdSegmentazCli();
    }

    @Override
    public void setIdSegmentazCli(int idSegmentazCli) {
        this.estRappAna.getE15IdSegmentazCli().setE15IdSegmentazCli(idSegmentazCli);
    }

    @Override
    public Integer getIdSegmentazCliObj() {
        if (ws.getIndEstRappAna().getIdSegmentazCli() >= 0) {
            return ((Integer)getIdSegmentazCli());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdSegmentazCliObj(Integer idSegmentazCliObj) {
        if (idSegmentazCliObj != null) {
            setIdSegmentazCli(((int)idSegmentazCliObj));
            ws.getIndEstRappAna().setIdSegmentazCli(((short)0));
        }
        else {
            ws.getIndEstRappAna().setIdSegmentazCli(((short)-1));
        }
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        return idsv0003.getCodiceCompagniaAnia();
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        this.idsv0003.setCodiceCompagniaAnia(idsv0003CodiceCompagniaAnia);
    }

    @Override
    public String getTpCntCorr() {
        return estRappAna.getE15TpCntCorr();
    }

    @Override
    public void setTpCntCorr(String tpCntCorr) {
        this.estRappAna.setE15TpCntCorr(tpCntCorr);
    }

    @Override
    public String getTpCntCorrObj() {
        if (ws.getIndEstRappAna().getTpCntCorr() >= 0) {
            return getTpCntCorr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpCntCorrObj(String tpCntCorrObj) {
        if (tpCntCorrObj != null) {
            setTpCntCorr(tpCntCorrObj);
            ws.getIndEstRappAna().setTpCntCorr(((short)0));
        }
        else {
            ws.getIndEstRappAna().setTpCntCorr(((short)-1));
        }
    }

    @Override
    public String getTpCoincPicPac() {
        return estRappAna.getE15TpCoincPicPac();
    }

    @Override
    public void setTpCoincPicPac(String tpCoincPicPac) {
        this.estRappAna.setE15TpCoincPicPac(tpCoincPicPac);
    }

    @Override
    public String getTpCoincPicPacObj() {
        if (ws.getIndEstRappAna().getTpCoincPicPac() >= 0) {
            return getTpCoincPicPac();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpCoincPicPacObj(String tpCoincPicPacObj) {
        if (tpCoincPicPacObj != null) {
            setTpCoincPicPac(tpCoincPicPacObj);
            ws.getIndEstRappAna().setTpCoincPicPac(((short)0));
        }
        else {
            ws.getIndEstRappAna().setTpCoincPicPac(((short)-1));
        }
    }

    @Override
    public String getTpLegCntr() {
        return estRappAna.getE15TpLegCntr();
    }

    @Override
    public void setTpLegCntr(String tpLegCntr) {
        this.estRappAna.setE15TpLegCntr(tpLegCntr);
    }

    @Override
    public String getTpLegCntrObj() {
        if (ws.getIndEstRappAna().getTpLegCntr() >= 0) {
            return getTpLegCntr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpLegCntrObj(String tpLegCntrObj) {
        if (tpLegCntrObj != null) {
            setTpLegCntr(tpLegCntrObj);
            ws.getIndEstRappAna().setTpLegCntr(((short)0));
        }
        else {
            ws.getIndEstRappAna().setTpLegCntr(((short)-1));
        }
    }

    @Override
    public String getTpLegPercBnficr() {
        return estRappAna.getE15TpLegPercBnficr();
    }

    @Override
    public void setTpLegPercBnficr(String tpLegPercBnficr) {
        this.estRappAna.setE15TpLegPercBnficr(tpLegPercBnficr);
    }

    @Override
    public String getTpLegPercBnficrObj() {
        if (ws.getIndEstRappAna().getTpLegPercBnficr() >= 0) {
            return getTpLegPercBnficr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpLegPercBnficrObj(String tpLegPercBnficrObj) {
        if (tpLegPercBnficrObj != null) {
            setTpLegPercBnficr(tpLegPercBnficrObj);
            ws.getIndEstRappAna().setTpLegPercBnficr(((short)0));
        }
        else {
            ws.getIndEstRappAna().setTpLegPercBnficr(((short)-1));
        }
    }

    @Override
    public String getTpRappAna() {
        return estRappAna.getE15TpRappAna();
    }

    @Override
    public void setTpRappAna(String tpRappAna) {
        this.estRappAna.setE15TpRappAna(tpRappAna);
    }

    @Override
    public String getWsDataInizioEffettoDb() {
        return ws.getIdsv0010().getWsDataInizioEffettoDb();
    }

    @Override
    public void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb) {
        this.ws.getIdsv0010().setWsDataInizioEffettoDb(wsDataInizioEffettoDb);
    }

    @Override
    public long getWsTsCompetenza() {
        return ws.getIdsv0010().getWsTsCompetenza();
    }

    @Override
    public void setWsTsCompetenza(long wsTsCompetenza) {
        this.ws.getIdsv0010().setWsTsCompetenza(wsTsCompetenza);
    }

    @Override
    public String getdLegPercBnficrVchar() {
        return estRappAna.getE15DLegPercBnficrVcharFormatted();
    }

    @Override
    public void setdLegPercBnficrVchar(String dLegPercBnficrVchar) {
        this.estRappAna.setE15DLegPercBnficrVcharFormatted(dLegPercBnficrVchar);
    }

    @Override
    public String getdLegPercBnficrVcharObj() {
        if (ws.getIndEstRappAna().getdLegPercBnficr() >= 0) {
            return getdLegPercBnficrVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setdLegPercBnficrVcharObj(String dLegPercBnficrVcharObj) {
        if (dLegPercBnficrVcharObj != null) {
            setdLegPercBnficrVchar(dLegPercBnficrVcharObj);
            ws.getIndEstRappAna().setdLegPercBnficr(((short)0));
        }
        else {
            ws.getIndEstRappAna().setdLegPercBnficr(((short)-1));
        }
    }
}
