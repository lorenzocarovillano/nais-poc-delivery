package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.jdbc.FieldNotMappedException;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.AnagDatoMatrValVarDao;
import it.accenture.jnais.commons.data.to.IAnagDatoMatrValVar;
import it.accenture.jnais.copy.AnagDatoLdbs1390;
import it.accenture.jnais.copy.Idsv0003CampiEsito;
import it.accenture.jnais.copy.MatrValVar;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.DispatcherVariablesLdbs1390;
import it.accenture.jnais.ws.Ldbs1390Data;
import it.accenture.jnais.ws.redefines.AdaLunghezzaDato;
import it.accenture.jnais.ws.redefines.AdaPrecisioneDato;
import it.accenture.jnais.ws.redefines.MvvIdpMatrValVar;
import it.accenture.jnais.ws.redefines.MvvTipoMovimento;
import it.accenture.jnais.ws.V1391AreaActu;

/**Original name: LDBS1390<br>
 * <pre>****************************************************************
 *                                                                *
 *                     PGM :  LDBS1390                            *
 *                                                                *
 * ****************************************************************
 * AUTHOR.  ATS NAPOLI.
 * ****************************************************************
 * ****************************************************************
 * ****************************************************************</pre>*/
public class Ldbs1390 extends Program implements IAnagDatoMatrValVar {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private AnagDatoMatrValVarDao anagDatoMatrValVarDao = new AnagDatoMatrValVarDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Ldbs1390Data ws = new Ldbs1390Data();
    //Original name: DISPATCHER-VARIABLES
    private DispatcherVariablesLdbs1390 dispatcherVariables;
    //Original name: V1391-AREA-ACTU
    private V1391AreaActu v1391AreaActu;

    //==== METHODS ====
    /**Original name: PROGRAM_LDBS1390_FIRST_SENTENCES<br>*/
    public long execute(DispatcherVariablesLdbs1390 dispatcherVariables, V1391AreaActu v1391AreaActu) {
        this.dispatcherVariables = dispatcherVariables;
        this.v1391AreaActu = v1391AreaActu;
        // COB_CODE: MOVE ZEROES                       TO WK-IND-SERV.
        ws.setWkIndServ(((short)0));
        // COB_CODE: INITIALIZE ANAG-DATO
        //                      MATR-VAL-VAR.
        initAnagDato();
        initMatrValVar();
        //               IDSO0011-AREA
        //
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC        TO TRUE.
        this.dispatcherVariables.getIdso0011Area().getReturnCode().setSuccessfulRc();
        //
        // COB_CODE: MOVE V1391-COD-COMPAGNIA-ANIA     TO ADA-COD-COMPAGNIA-ANIA
        //                                                MVV-COD-COMPAGNIA-ANIA.
        ws.getAnagDato().setAdaCodCompagniaAnia(this.v1391AreaActu.getCodCompagniaAnia());
        ws.getMatrValVar().setMvvCodCompagniaAnia(this.v1391AreaActu.getCodCompagniaAnia());
        // COB_CODE: MOVE V1391-COD-ACTU               TO ADA-COD-DATO
        //                                                MVV-COD-DATO-EXT.
        ws.getAnagDato().setAdaCodDato(this.v1391AreaActu.getCodActu());
        ws.getMatrValVar().setMvvCodDatoExt(this.v1391AreaActu.getCodActu());
        // COB_CODE: PERFORM A100-SELECT-MVV           THRU A100-EX.
        a100SelectMvv();
        //
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Ldbs1390 getInstance() {
        return ((Ldbs1390)Programs.getInstance(Ldbs1390.class));
    }

    /**Original name: A100-SELECT-MVV<br>
	 * <pre>****************************************************************
	 *  SELECT MVV
	 * ****************************************************************</pre>*/
    private void a100SelectMvv() {
        ConcatUtil concatUtil = null;
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR             THRU Z960-EX.
        //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LDBS1390.cbl:line=77, because the code is unreachable.
        // COB_CODE: EXEC SQL
        //              SELECT A.TIPO_DATO
        //                    ,A.LUNGHEZZA_DATO
        //                    ,A.PRECISIONE_DATO
        //                    ,A.FORMATTAZIONE_DATO
        //                    ,B.ID_MATR_VAL_VAR
        //                    ,B.IDP_MATR_VAL_VAR
        //                    ,B.TIPO_MOVIMENTO
        //                    ,B.COD_DATO_EXT
        //                    ,B.OBBLIGATORIETA
        //                    ,B.VALORE_DEFAULT
        //                    ,B.COD_STR_DATO_PTF
        //                    ,B.COD_DATO_PTF
        //                    ,B.COD_PARAMETRO
        //                    ,B.OPERAZIONE
        //                    ,B.LIVELLO_OPERAZIONE
        //                    ,B.TIPO_OGGETTO
        //                    ,B.WHERE_CONDITION
        //                    ,B.SERVIZIO_LETTURA
        //                    ,B.MODULO_CALCOLO
        //                    ,B.COD_DATO_INTERNO
        //                INTO  :ADA-TIPO-DATO
        //                      :IND-ADA-TIPO-DATO
        //                     ,:ADA-LUNGHEZZA-DATO
        //                      :IND-ADA-LUNGHEZZA-DATO
        //                     ,:ADA-PRECISIONE-DATO
        //                      :IND-ADA-PRECISIONE-DATO
        //                     ,:ADA-FORMATTAZIONE-DATO
        //                      :IND-ADA-FORMATTAZIONE-DATO
        //                     ,:MVV-ID-MATR-VAL-VAR
        //                     ,:MVV-IDP-MATR-VAL-VAR
        //                      :IND-MVV-IDP-MATR-VAL-VAR
        //                     ,:MVV-TIPO-MOVIMENTO
        //                      :IND-MVV-TIPO-MOVIMENTO
        //                     ,:MVV-COD-DATO-EXT
        //                      :IND-MVV-COD-DATO-EXT
        //                     ,:MVV-OBBLIGATORIETA
        //                      :IND-MVV-OBBLIGATORIETA
        //                     ,:MVV-VALORE-DEFAULT
        //                      :IND-MVV-VALORE-DEFAULT
        //                     ,:MVV-COD-STR-DATO-PTF
        //                      :IND-MVV-COD-STR-DATO-PTF
        //                     ,:MVV-COD-DATO-PTF
        //                      :IND-MVV-COD-DATO-PTF
        //                     ,:MVV-COD-PARAMETRO
        //                      :IND-MVV-COD-PARAMETRO
        //                     ,:MVV-OPERAZIONE
        //                      :IND-MVV-OPERAZIONE
        //                     ,:MVV-LIVELLO-OPERAZIONE
        //                      :IND-MVV-LIVELLO-OPERAZIONE
        //                     ,:MVV-TIPO-OGGETTO
        //                      :IND-MVV-TIPO-OGGETTO
        //                     ,:MVV-WHERE-CONDITION-VCHAR
        //                      :IND-MVV-WHERE-CONDITION
        //                     ,:MVV-SERVIZIO-LETTURA
        //                      :IND-MVV-SERVIZIO-LETTURA
        //                     ,:MVV-MODULO-CALCOLO
        //                      :IND-MVV-MODULO-CALCOLO
        //                     ,:MVV-COD-DATO-INTERNO
        //                      :IND-MVV-COD-DATO-INTERNO
        //              FROM ANAG_DATO A, MATR_VAL_VAR B
        //               WHERE A.COD_COMPAGNIA_ANIA  =
        //                    :ADA-COD-COMPAGNIA-ANIA
        //                AND B.COD_COMPAGNIA_ANIA =
        //                    :MVV-COD-COMPAGNIA-ANIA
        //                AND A.COD_DATO =
        //                    :V1391-COD-ACTU
        //                AND B.COD_DATO_EXT =
        //                    :V1391-COD-ACTU
        //                AND B.STEP_VALORIZZATORE = 'B'
        //               END-EXEC.
        anagDatoMatrValVarDao.selectRec(ws.getAnagDato().getAdaCodCompagniaAnia(), ws.getMatrValVar().getMvvCodCompagniaAnia(), v1391AreaActu.getCodActu(), ws);
        // COB_CODE: MOVE SQLCODE           TO IDSO0011-SQLCODE-SIGNED.
        dispatcherVariables.getIdso0011Area().getSqlcodeSigned().setSqlcodeSigned(sqlca.getSqlcode());
        // COB_CODE: IF NOT IDSO0011-SUCCESSFUL-SQL
        //              END-IF
        //           ELSE
        //              MOVE WK-IND-SERV                    TO V1391-ELE-MAX-ACTU
        //           END-IF.
        if (!dispatcherVariables.getIdso0011Area().getSqlcodeSigned().isSuccessfulSql()) {
            // COB_CODE: IF IDSO0011-MORE-THAN-ONE-ROW
            //              PERFORM B000-ACCEDI-X-CURSORE THRU B000-EX
            //           ELSE
            //              SET IDSO0011-SQL-ERROR             TO TRUE
            //           END-IF
            if (dispatcherVariables.getIdso0011Area().getSqlcodeSigned().isMoreThanOneRow()) {
                // COB_CODE: PERFORM B000-ACCEDI-X-CURSORE THRU B000-EX
                b000AccediXCursore();
            }
            else {
                // COB_CODE: IF IDSO0011-NOT-FOUND
                //              DELIMITED BY SIZE INTO IDSO0011-DESCRIZ-ERR-DB2
                //           ELSE
                //              DELIMITED BY SIZE INTO IDSO0011-DESCRIZ-ERR-DB2
                //           END-IF
                if (dispatcherVariables.getIdso0011Area().getSqlcodeSigned().isNotFound()) {
                    // COB_CODE: STRING 'VARIABILE '
                    //             V1391-COD-ACTU ' '
                    //             'NON ANCORA CENSITA SULLA TABELLA '
                    //             'DEL VALORIZZATORE : '
                    //             IDSO0011-NOME-TABELLA
                    //           DELIMITED BY SIZE INTO IDSO0011-DESCRIZ-ERR-DB2
                    concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, new String[] {"VARIABILE ", v1391AreaActu.getCodActuFormatted(), " ", "NON ANCORA CENSITA SULLA TABELLA ", "DEL VALORIZZATORE : ", dispatcherVariables.getIdso0011Area().getCampiEsito().getNomeTabellaFormatted()});
                    dispatcherVariables.getIdso0011Area().getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(dispatcherVariables.getIdso0011Area().getCampiEsito().getDescrizErrDb2Formatted()));
                }
                else {
                    // COB_CODE: MOVE 'MATR_VAL_VAR'  TO IDSO0011-NOME-TABELLA
                    dispatcherVariables.getIdso0011Area().getCampiEsito().setNomeTabella("MATR_VAL_VAR");
                    // COB_CODE: STRING 'ERRORE CHIAMATA LDBS1390 ;'
                    //              IDSO0011-SQLCODE-SIGNED ';'
                    //              V1391-COD-ACTU
                    //           DELIMITED BY SIZE INTO IDSO0011-DESCRIZ-ERR-DB2
                    concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "ERRORE CHIAMATA LDBS1390 ;", dispatcherVariables.getIdso0011Area().getSqlcodeSigned().getIdso0021SqlcodeAsString(), ";", v1391AreaActu.getCodActuFormatted());
                    dispatcherVariables.getIdso0011Area().getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(dispatcherVariables.getIdso0011Area().getCampiEsito().getDescrizErrDb2Formatted()));
                }
                // COB_CODE: SET IDSO0011-SQL-ERROR             TO TRUE
                dispatcherVariables.getIdso0011Area().getReturnCode().setSqlError();
            }
        }
        else {
            // COB_CODE: PERFORM N000-CNTL-CAMPI-NULL        THRU N000-EX
            n000CntlCampiNull();
            // COB_CODE: MOVE 1                              TO WK-IND-SERV
            ws.setWkIndServ(((short)1));
            // COB_CODE: PERFORM N001-IMPOSTA-OUTPUT         THRU N001-EX
            n001ImpostaOutput();
            // COB_CODE: MOVE WK-IND-SERV                    TO V1391-ELE-MAX-ACTU
            v1391AreaActu.setEleMaxActu(TruncAbs.toShort(ws.getWkIndServ(), 2));
        }
    }

    /**Original name: B000-ACCEDI-X-CURSORE<br>
	 * <pre>****************************************************************
	 *  ACCEDI X CURSORE
	 * ****************************************************************</pre>*/
    private void b000AccediXCursore() {
        // COB_CODE: PERFORM B100-DECLARE-CUR-SERVIZI      THRU B100-EX.
        b100DeclareCurServizi();
        // COB_CODE: PERFORM B200-OPEN-CUR-SERVIZI         THRU B200-EX.
        b200OpenCurServizi();
        // COB_CODE: PERFORM B250-FETCH-FIRST              THRU B250-EX.
        b250FetchFirst();
        //
        // COB_CODE: IF IDSO0011-SUCCESSFUL-RC
        //                      UNTIL NOT IDSO0011-SUCCESSFUL-SQL
        //           END-IF.
        if (dispatcherVariables.getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM B300-FETCH-CUR-SERVIZI     THRU B300-EX
            //                   UNTIL NOT IDSO0011-SUCCESSFUL-SQL
            while (dispatcherVariables.getIdso0011Area().getSqlcodeSigned().isSuccessfulSql()) {
                b300FetchCurServizi();
            }
        }
        //
        // COB_CODE: IF IDSO0011-SUCCESSFUL-RC
        //              PERFORM B400-CLOSE-CUR-SERVIZI     THRU B400-EX
        //           END-IF.
        if (dispatcherVariables.getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM B400-CLOSE-CUR-SERVIZI     THRU B400-EX
            b400CloseCurServizi();
        }
    }

    /**Original name: B100-DECLARE-CUR-SERVIZI<br>
	 * <pre>****************************************************************
	 *  DECLARE CURSORE SERVIZI
	 * ****************************************************************</pre>*/
    private void b100DeclareCurServizi() {
    // COB_CODE: PERFORM Z960-LENGTH-VCHAR             THRU Z960-EX.
    //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LDBS1390.cbl:line=211, because the code is unreachable.
    // COB_CODE: EXEC SQL DECLARE CUR_SERVIZI CURSOR FOR
    //              SELECT A.TIPO_DATO
    //                    ,A.LUNGHEZZA_DATO
    //                    ,A.PRECISIONE_DATO
    //                    ,A.FORMATTAZIONE_DATO
    //                    ,B.ID_MATR_VAL_VAR
    //                    ,B.IDP_MATR_VAL_VAR
    //                    ,B.TIPO_MOVIMENTO
    //                    ,B.COD_DATO_EXT
    //                    ,B.OBBLIGATORIETA
    //                    ,B.VALORE_DEFAULT
    //                    ,B.COD_STR_DATO_PTF
    //                    ,B.COD_DATO_PTF
    //                    ,B.COD_PARAMETRO
    //                    ,B.OPERAZIONE
    //                    ,B.LIVELLO_OPERAZIONE
    //                    ,B.TIPO_OGGETTO
    //                    ,B.WHERE_CONDITION
    //                    ,B.SERVIZIO_LETTURA
    //                    ,B.MODULO_CALCOLO
    //                    ,B.COD_DATO_INTERNO
    //              FROM ANAG_DATO A, MATR_VAL_VAR B
    //               WHERE A.COD_COMPAGNIA_ANIA  =
    //                    :ADA-COD-COMPAGNIA-ANIA
    //                AND B.COD_COMPAGNIA_ANIA =
    //                    :MVV-COD-COMPAGNIA-ANIA
    //                AND A.COD_DATO =
    //                    :V1391-COD-ACTU
    //                AND B.COD_DATO_EXT =
    //                    :V1391-COD-ACTU
    //                AND B.STEP_VALORIZZATORE = 'B'
    //                ORDER BY B.ID_MATR_VAL_VAR
    //               END-EXEC.
    // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B200-OPEN-CUR-SERVIZI<br>
	 * <pre>****************************************************************
	 *  OPEN CUR-SERVIZI
	 * ***************************************************************</pre>*/
    private void b200OpenCurServizi() {
        ConcatUtil concatUtil = null;
        // COB_CODE: EXEC SQL OPEN CUR_SERVIZI END-EXEC.
        anagDatoMatrValVarDao.openCurServizi(ws.getAnagDato().getAdaCodCompagniaAnia(), ws.getMatrValVar().getMvvCodCompagniaAnia(), v1391AreaActu.getCodActu());
        // COB_CODE: MOVE SQLCODE           TO IDSO0011-SQLCODE-SIGNED.
        dispatcherVariables.getIdso0011Area().getSqlcodeSigned().setSqlcodeSigned(sqlca.getSqlcode());
        // COB_CODE: IF NOT IDSO0011-SUCCESSFUL-SQL
        //              SET IDSO0011-SQL-ERROR             TO TRUE
        //           END-IF.
        if (!dispatcherVariables.getIdso0011Area().getSqlcodeSigned().isSuccessfulSql()) {
            // COB_CODE: SET IDSO0011-SQL-ERROR    TO TRUE
            dispatcherVariables.getIdso0011Area().getReturnCode().setSqlError();
            // COB_CODE: STRING 'ERRORE CHIAMATA LDBS1390 ;'
            //                  IDSO0011-SQLCODE-SIGNED ';'
            //                  V1391-COD-ACTU
            //           DELIMITED BY SIZE INTO IDSO0011-DESCRIZ-ERR-DB2
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "ERRORE CHIAMATA LDBS1390 ;", dispatcherVariables.getIdso0011Area().getSqlcodeSigned().getIdso0021SqlcodeAsString(), ";", v1391AreaActu.getCodActuFormatted());
            dispatcherVariables.getIdso0011Area().getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(dispatcherVariables.getIdso0011Area().getCampiEsito().getDescrizErrDb2Formatted()));
            // COB_CODE: MOVE 'MATR_VAL_VAR'    TO IDSO0011-NOME-TABELLA
            dispatcherVariables.getIdso0011Area().getCampiEsito().setNomeTabella("MATR_VAL_VAR");
            // COB_CODE: SET IDSO0011-SQL-ERROR             TO TRUE
            dispatcherVariables.getIdso0011Area().getReturnCode().setSqlError();
        }
    }

    /**Original name: B250-FETCH-FIRST<br>
	 * <pre>****************************************************************
	 *  FETCH CUR-SERVIZI
	 * ****************************************************************</pre>*/
    private void b250FetchFirst() {
        ConcatUtil concatUtil = null;
        // COB_CODE:  EXEC SQL FETCH CUR_SERVIZI
        //                INTO  :ADA-TIPO-DATO
        //                      :IND-ADA-TIPO-DATO
        //                     ,:ADA-LUNGHEZZA-DATO
        //                      :IND-ADA-LUNGHEZZA-DATO
        //                     ,:ADA-PRECISIONE-DATO
        //                      :IND-ADA-PRECISIONE-DATO
        //                     ,:ADA-FORMATTAZIONE-DATO
        //                      :IND-ADA-FORMATTAZIONE-DATO
        //                     ,:MVV-ID-MATR-VAL-VAR
        //                     ,:MVV-IDP-MATR-VAL-VAR
        //                      :IND-MVV-IDP-MATR-VAL-VAR
        //                     ,:MVV-TIPO-MOVIMENTO
        //                      :IND-MVV-TIPO-MOVIMENTO
        //                     ,:MVV-COD-DATO-EXT
        //                      :IND-MVV-COD-DATO-EXT
        //                     ,:MVV-OBBLIGATORIETA
        //                      :IND-MVV-OBBLIGATORIETA
        //                     ,:MVV-VALORE-DEFAULT
        //                      :IND-MVV-VALORE-DEFAULT
        //                     ,:MVV-COD-STR-DATO-PTF
        //                      :IND-MVV-COD-STR-DATO-PTF
        //                     ,:MVV-COD-DATO-PTF
        //                      :IND-MVV-COD-DATO-PTF
        //                     ,:MVV-COD-PARAMETRO
        //                      :IND-MVV-COD-PARAMETRO
        //                     ,:MVV-OPERAZIONE
        //                      :IND-MVV-OPERAZIONE
        //                     ,:MVV-LIVELLO-OPERAZIONE
        //                      :IND-MVV-LIVELLO-OPERAZIONE
        //                     ,:MVV-TIPO-OGGETTO
        //                      :IND-MVV-TIPO-OGGETTO
        //                     ,:MVV-WHERE-CONDITION-VCHAR
        //                      :IND-MVV-WHERE-CONDITION
        //                     ,:MVV-SERVIZIO-LETTURA
        //                      :IND-MVV-SERVIZIO-LETTURA
        //                     ,:MVV-MODULO-CALCOLO
        //                      :IND-MVV-MODULO-CALCOLO
        //                     ,:MVV-COD-DATO-INTERNO
        //                      :IND-MVV-COD-DATO-INTERNO
        //           END-EXEC.
        anagDatoMatrValVarDao.fetchCurServizi(ws);
        // COB_CODE: MOVE SQLCODE           TO IDSO0011-SQLCODE-SIGNED.
        dispatcherVariables.getIdso0011Area().getSqlcodeSigned().setSqlcodeSigned(sqlca.getSqlcode());
        //
        // COB_CODE:      IF  NOT IDSO0011-SUCCESSFUL-SQL
        //           *         NOT IDSO0011-NOT-FOUND
        //                    SET IDSO0011-SQL-ERROR             TO TRUE
        //                END-IF.
        if (!dispatcherVariables.getIdso0011Area().getSqlcodeSigned().isSuccessfulSql()) {
            //         NOT IDSO0011-NOT-FOUND
            // COB_CODE: MOVE 'MATR_VAL_VAR'  TO IDSO0011-NOME-TABELLA
            dispatcherVariables.getIdso0011Area().getCampiEsito().setNomeTabella("MATR_VAL_VAR");
            //
            // COB_CODE: IF IDSO0011-NOT-FOUND
            //              DELIMITED BY SIZE INTO IDSO0011-DESCRIZ-ERR-DB2
            //           ELSE
            //              DELIMITED BY SIZE INTO IDSO0011-DESCRIZ-ERR-DB2
            //           END-IF
            if (dispatcherVariables.getIdso0011Area().getSqlcodeSigned().isNotFound()) {
                // COB_CODE: STRING 'VARIABILE '
                //             V1391-COD-ACTU ' '
                //             'NON ANCORA CENSITA SULLA TABELLA '
                //             'DEL VALORIZZATORE : '
                //             IDSO0011-NOME-TABELLA
                //           DELIMITED BY SIZE INTO IDSO0011-DESCRIZ-ERR-DB2
                concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, new String[] {"VARIABILE ", v1391AreaActu.getCodActuFormatted(), " ", "NON ANCORA CENSITA SULLA TABELLA ", "DEL VALORIZZATORE : ", dispatcherVariables.getIdso0011Area().getCampiEsito().getNomeTabellaFormatted()});
                dispatcherVariables.getIdso0011Area().getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(dispatcherVariables.getIdso0011Area().getCampiEsito().getDescrizErrDb2Formatted()));
            }
            else {
                // COB_CODE: STRING 'ERRORE CHIAMATA LDBS1390 ;'
                //              IDSO0011-SQLCODE-SIGNED ';'
                //              V1391-COD-ACTU
                //           DELIMITED BY SIZE INTO IDSO0011-DESCRIZ-ERR-DB2
                concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "ERRORE CHIAMATA LDBS1390 ;", dispatcherVariables.getIdso0011Area().getSqlcodeSigned().getIdso0021SqlcodeAsString(), ";", v1391AreaActu.getCodActuFormatted());
                dispatcherVariables.getIdso0011Area().getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(dispatcherVariables.getIdso0011Area().getCampiEsito().getDescrizErrDb2Formatted()));
            }
            //
            // COB_CODE: SET IDSO0011-SQL-ERROR             TO TRUE
            dispatcherVariables.getIdso0011Area().getReturnCode().setSqlError();
        }
        // COB_CODE: IF IDSO0011-SUCCESSFUL-SQL
        //              MOVE WK-IND-SERV                    TO V1391-ELE-MAX-ACTU
        //           ELSE
        //              PERFORM B350-CLOSE-CUR-SERVIZI      THRU B350-EX
        //           END-IF.
        if (dispatcherVariables.getIdso0011Area().getSqlcodeSigned().isSuccessfulSql()) {
            // COB_CODE: PERFORM N000-CNTL-CAMPI-NULL        THRU N000-EX
            n000CntlCampiNull();
            // COB_CODE: MOVE 1                              TO WK-IND-SERV
            ws.setWkIndServ(((short)1));
            // COB_CODE: PERFORM N001-IMPOSTA-OUTPUT         THRU N001-EX
            n001ImpostaOutput();
            // COB_CODE: MOVE WK-IND-SERV                    TO V1391-ELE-MAX-ACTU
            v1391AreaActu.setEleMaxActu(TruncAbs.toShort(ws.getWkIndServ(), 2));
        }
        else {
            // COB_CODE: PERFORM B350-CLOSE-CUR-SERVIZI      THRU B350-EX
            b350CloseCurServizi();
        }
    }

    /**Original name: B300-FETCH-CUR-SERVIZI<br>
	 * <pre>****************************************************************
	 *  FETCH CUR-SERVIZI
	 * ****************************************************************</pre>*/
    private void b300FetchCurServizi() {
        ConcatUtil concatUtil = null;
        // COB_CODE:  EXEC SQL FETCH CUR_SERVIZI
        //                INTO  :ADA-TIPO-DATO
        //                      :IND-ADA-TIPO-DATO
        //                     ,:ADA-LUNGHEZZA-DATO
        //                      :IND-ADA-LUNGHEZZA-DATO
        //                     ,:ADA-PRECISIONE-DATO
        //                      :IND-ADA-PRECISIONE-DATO
        //                     ,:ADA-FORMATTAZIONE-DATO
        //                      :IND-ADA-FORMATTAZIONE-DATO
        //                     ,:MVV-ID-MATR-VAL-VAR
        //                     ,:MVV-IDP-MATR-VAL-VAR
        //                      :IND-MVV-IDP-MATR-VAL-VAR
        //                     ,:MVV-TIPO-MOVIMENTO
        //                      :IND-MVV-TIPO-MOVIMENTO
        //                     ,:MVV-COD-DATO-EXT
        //                      :IND-MVV-COD-DATO-EXT
        //                     ,:MVV-OBBLIGATORIETA
        //                      :IND-MVV-OBBLIGATORIETA
        //                     ,:MVV-VALORE-DEFAULT
        //                      :IND-MVV-VALORE-DEFAULT
        //                     ,:MVV-COD-STR-DATO-PTF
        //                      :IND-MVV-COD-STR-DATO-PTF
        //                     ,:MVV-COD-DATO-PTF
        //                      :IND-MVV-COD-DATO-PTF
        //                     ,:MVV-COD-PARAMETRO
        //                      :IND-MVV-COD-PARAMETRO
        //                     ,:MVV-OPERAZIONE
        //                      :IND-MVV-OPERAZIONE
        //                     ,:MVV-LIVELLO-OPERAZIONE
        //                      :IND-MVV-LIVELLO-OPERAZIONE
        //                     ,:MVV-TIPO-OGGETTO
        //                      :IND-MVV-TIPO-OGGETTO
        //                     ,:MVV-WHERE-CONDITION-VCHAR
        //                      :IND-MVV-WHERE-CONDITION
        //                     ,:MVV-SERVIZIO-LETTURA
        //                      :IND-MVV-SERVIZIO-LETTURA
        //                     ,:MVV-MODULO-CALCOLO
        //                      :IND-MVV-MODULO-CALCOLO
        //                     ,:MVV-COD-DATO-INTERNO
        //                      :IND-MVV-COD-DATO-INTERNO
        //           END-EXEC.
        anagDatoMatrValVarDao.fetchCurServizi(ws);
        // COB_CODE: MOVE SQLCODE           TO IDSO0011-SQLCODE-SIGNED.
        dispatcherVariables.getIdso0011Area().getSqlcodeSigned().setSqlcodeSigned(sqlca.getSqlcode());
        //
        // COB_CODE: IF  NOT IDSO0011-SUCCESSFUL-SQL
        //           AND NOT IDSO0011-NOT-FOUND
        //               SET IDSO0011-SQL-ERROR             TO TRUE
        //           END-IF.
        if (!dispatcherVariables.getIdso0011Area().getSqlcodeSigned().isSuccessfulSql() && !dispatcherVariables.getIdso0011Area().getSqlcodeSigned().isNotFound()) {
            // COB_CODE: MOVE 'MATR_VAL_VAR'     TO IDSO0011-NOME-TABELLA
            dispatcherVariables.getIdso0011Area().getCampiEsito().setNomeTabella("MATR_VAL_VAR");
            //
            // COB_CODE: STRING 'ERRORE CHIAMATA LDBS1390 ;'
            //                 IDSO0011-SQLCODE-SIGNED ';'
            //                 V1391-COD-ACTU
            //           DELIMITED BY SIZE INTO IDSO0011-DESCRIZ-ERR-DB2
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "ERRORE CHIAMATA LDBS1390 ;", dispatcherVariables.getIdso0011Area().getSqlcodeSigned().getIdso0021SqlcodeAsString(), ";", v1391AreaActu.getCodActuFormatted());
            dispatcherVariables.getIdso0011Area().getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(dispatcherVariables.getIdso0011Area().getCampiEsito().getDescrizErrDb2Formatted()));
            //
            // COB_CODE: SET IDSO0011-SQL-ERROR             TO TRUE
            dispatcherVariables.getIdso0011Area().getReturnCode().setSqlError();
        }
        //
        // COB_CODE: IF IDSO0011-SUCCESSFUL-SQL
        //              MOVE WK-IND-SERV                    TO V1391-ELE-MAX-ACTU
        //           END-IF.
        if (dispatcherVariables.getIdso0011Area().getSqlcodeSigned().isSuccessfulSql()) {
            // COB_CODE: PERFORM N000-CNTL-CAMPI-NULL        THRU N000-EX
            n000CntlCampiNull();
            // COB_CODE: ADD 1                               TO WK-IND-SERV
            ws.setWkIndServ(Trunc.toShort(1 + ws.getWkIndServ(), 4));
            // COB_CODE: PERFORM N001-IMPOSTA-OUTPUT         THRU N001-EX
            n001ImpostaOutput();
            // COB_CODE: MOVE WK-IND-SERV                    TO V1391-ELE-MAX-ACTU
            v1391AreaActu.setEleMaxActu(TruncAbs.toShort(ws.getWkIndServ(), 2));
        }
    }

    /**Original name: B350-CLOSE-CUR-SERVIZI<br>
	 * <pre>****************************************************************
	 *  CLOSE CUR-SERVIZIO
	 * ****************************************************************</pre>*/
    private void b350CloseCurServizi() {
        // COB_CODE: MOVE IDSO0011-SQLCODE-SIGNED           TO WS-SQLCODE.
        ws.setWsSqlcode(dispatcherVariables.getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned());
        //
        // COB_CODE: EXEC SQL CLOSE CUR_SERVIZI END-EXEC.
        anagDatoMatrValVarDao.closeCurServizi();
        // COB_CODE: MOVE SQLCODE           TO IDSO0011-SQLCODE-SIGNED.
        dispatcherVariables.getIdso0011Area().getSqlcodeSigned().setSqlcodeSigned(sqlca.getSqlcode());
        //
        // COB_CODE: IF NOT IDSO0011-SUCCESSFUL-SQL
        //              SET IDSO0011-SQL-ERROR             TO TRUE
        //           ELSE
        //              MOVE WS-SQLCODE                TO IDSO0011-SQLCODE-SIGNED
        //           END-IF.
        if (!dispatcherVariables.getIdso0011Area().getSqlcodeSigned().isSuccessfulSql()) {
            // COB_CODE: MOVE 'MOV_STR_SERVIZI'
            //                               TO IDSO0011-NOME-TABELLA
            dispatcherVariables.getIdso0011Area().getCampiEsito().setNomeTabella("MOV_STR_SERVIZI");
            // COB_CODE: SET IDSO0011-SQL-ERROR             TO TRUE
            dispatcherVariables.getIdso0011Area().getReturnCode().setSqlError();
        }
        else {
            // COB_CODE: MOVE WS-SQLCODE                TO IDSO0011-SQLCODE-SIGNED
            dispatcherVariables.getIdso0011Area().getSqlcodeSigned().setSqlcodeSigned(ws.getWsSqlcode());
        }
    }

    /**Original name: B400-CLOSE-CUR-SERVIZI<br>
	 * <pre>****************************************************************
	 *  CLOSE CUR-SERVIZIO
	 * ****************************************************************</pre>*/
    private void b400CloseCurServizi() {
        // COB_CODE: EXEC SQL CLOSE CUR_SERVIZI END-EXEC.
        anagDatoMatrValVarDao.closeCurServizi();
        // COB_CODE: MOVE SQLCODE           TO IDSO0011-SQLCODE-SIGNED.
        dispatcherVariables.getIdso0011Area().getSqlcodeSigned().setSqlcodeSigned(sqlca.getSqlcode());
        //
        // COB_CODE: IF NOT IDSO0011-SUCCESSFUL-SQL
        //              SET IDSO0011-SQL-ERROR             TO TRUE
        //           END-IF.
        if (!dispatcherVariables.getIdso0011Area().getSqlcodeSigned().isSuccessfulSql()) {
            // COB_CODE: MOVE 'MOV_STR_SERVIZI'
            //                               TO IDSO0011-NOME-TABELLA
            dispatcherVariables.getIdso0011Area().getCampiEsito().setNomeTabella("MOV_STR_SERVIZI");
            // COB_CODE: SET IDSO0011-SQL-ERROR             TO TRUE
            dispatcherVariables.getIdso0011Area().getReturnCode().setSqlError();
        }
    }

    /**Original name: N000-CNTL-CAMPI-NULL<br>
	 * <pre>*****************************************************************</pre>*/
    private void n000CntlCampiNull() {
        // COB_CODE: IF IND-ADA-TIPO-DATO = -1
        //              MOVE HIGH-VALUES TO ADA-TIPO-DATO-NULL
        //           END-IF.
        if (ws.getIndAnagDato().getTipoDato() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADA-TIPO-DATO-NULL
            ws.getAnagDato().setAdaTipoDato(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AnagDatoLdbs1390.Len.ADA_TIPO_DATO));
        }
        // COB_CODE: IF IND-ADA-LUNGHEZZA-DATO = -1
        //              MOVE HIGH-VALUES TO ADA-LUNGHEZZA-DATO-NULL
        //           END-IF.
        if (ws.getIndAnagDato().getLunghezzaDato() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADA-LUNGHEZZA-DATO-NULL
            ws.getAnagDato().getAdaLunghezzaDato().setAdaLunghezzaDatoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdaLunghezzaDato.Len.ADA_LUNGHEZZA_DATO_NULL));
        }
        // COB_CODE: IF IND-ADA-PRECISIONE-DATO = -1
        //              MOVE HIGH-VALUES TO ADA-PRECISIONE-DATO-NULL
        //           END-IF.
        if (ws.getIndAnagDato().getPrecisioneDato() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADA-PRECISIONE-DATO-NULL
            ws.getAnagDato().getAdaPrecisioneDato().setAdaPrecisioneDatoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdaPrecisioneDato.Len.ADA_PRECISIONE_DATO_NULL));
        }
        // COB_CODE: IF IND-ADA-FORMATTAZIONE-DATO = -1
        //              MOVE HIGH-VALUES TO ADA-FORMATTAZIONE-DATO
        //           END-IF.
        if (ws.getIndAnagDato().getFormattazioneDato() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADA-FORMATTAZIONE-DATO
            ws.getAnagDato().setAdaFormattazioneDato(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AnagDatoLdbs1390.Len.ADA_FORMATTAZIONE_DATO));
        }
        // COB_CODE: IF IND-MVV-IDP-MATR-VAL-VAR = -1
        //              MOVE HIGH-VALUES TO MVV-IDP-MATR-VAL-VAR-NULL
        //           END-IF.
        if (ws.getIndMatrValVar().getIdpMatrValVar() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MVV-IDP-MATR-VAL-VAR-NULL
            ws.getMatrValVar().getMvvIdpMatrValVar().setMvvIdpMatrValVarNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MvvIdpMatrValVar.Len.MVV_IDP_MATR_VAL_VAR_NULL));
        }
        // COB_CODE: IF IND-MVV-TIPO-MOVIMENTO = -1
        //              MOVE HIGH-VALUES TO MVV-TIPO-MOVIMENTO-NULL
        //           END-IF.
        if (ws.getIndMatrValVar().getTipoMovimento() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MVV-TIPO-MOVIMENTO-NULL
            ws.getMatrValVar().getMvvTipoMovimento().setMvvTipoMovimentoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MvvTipoMovimento.Len.MVV_TIPO_MOVIMENTO_NULL));
        }
        // COB_CODE: IF IND-MVV-COD-DATO-EXT = -1
        //              MOVE HIGH-VALUES TO MVV-COD-DATO-EXT
        //           END-IF.
        if (ws.getIndMatrValVar().getCodDatoExt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MVV-COD-DATO-EXT
            ws.getMatrValVar().setMvvCodDatoExt(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MatrValVar.Len.MVV_COD_DATO_EXT));
        }
        // COB_CODE: IF IND-MVV-OBBLIGATORIETA = -1
        //              MOVE HIGH-VALUES TO MVV-OBBLIGATORIETA-NULL
        //           END-IF.
        if (ws.getIndMatrValVar().getObbligatorieta() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MVV-OBBLIGATORIETA-NULL
            ws.getMatrValVar().setMvvObbligatorieta(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-MVV-VALORE-DEFAULT = -1
        //              MOVE HIGH-VALUES TO MVV-VALORE-DEFAULT
        //           END-IF.
        if (ws.getIndMatrValVar().getValoreDefault() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MVV-VALORE-DEFAULT
            ws.getMatrValVar().setMvvValoreDefault(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MatrValVar.Len.MVV_VALORE_DEFAULT));
        }
        // COB_CODE: IF IND-MVV-COD-STR-DATO-PTF = -1
        //              MOVE HIGH-VALUES TO MVV-COD-STR-DATO-PTF
        //           END-IF.
        if (ws.getIndMatrValVar().getCodStrDatoPtf() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MVV-COD-STR-DATO-PTF
            ws.getMatrValVar().setMvvCodStrDatoPtf(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MatrValVar.Len.MVV_COD_STR_DATO_PTF));
        }
        // COB_CODE: IF IND-MVV-COD-DATO-PTF = -1
        //              MOVE HIGH-VALUES TO MVV-COD-DATO-PTF
        //           END-IF.
        if (ws.getIndMatrValVar().getCodDatoPtf() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MVV-COD-DATO-PTF
            ws.getMatrValVar().setMvvCodDatoPtf(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MatrValVar.Len.MVV_COD_DATO_PTF));
        }
        // COB_CODE: IF IND-MVV-COD-PARAMETRO = -1
        //              MOVE HIGH-VALUES TO MVV-COD-PARAMETRO
        //           END-IF.
        if (ws.getIndMatrValVar().getCodParametro() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MVV-COD-PARAMETRO
            ws.getMatrValVar().setMvvCodParametro(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MatrValVar.Len.MVV_COD_PARAMETRO));
        }
        // COB_CODE: IF IND-MVV-OPERAZIONE = -1
        //              MOVE HIGH-VALUES TO MVV-OPERAZIONE
        //           END-IF.
        if (ws.getIndMatrValVar().getOperazione() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MVV-OPERAZIONE
            ws.getMatrValVar().setMvvOperazione(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MatrValVar.Len.MVV_OPERAZIONE));
        }
        // COB_CODE: IF IND-MVV-LIVELLO-OPERAZIONE = -1
        //              MOVE HIGH-VALUES TO MVV-LIVELLO-OPERAZIONE-NULL
        //           END-IF.
        if (ws.getIndMatrValVar().getLivelloOperazione() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MVV-LIVELLO-OPERAZIONE-NULL
            ws.getMatrValVar().setMvvLivelloOperazione(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MatrValVar.Len.MVV_LIVELLO_OPERAZIONE));
        }
        // COB_CODE: IF IND-MVV-TIPO-OGGETTO = -1
        //              MOVE HIGH-VALUES TO MVV-TIPO-OGGETTO-NULL
        //           END-IF.
        if (ws.getIndMatrValVar().getTipoOggetto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MVV-TIPO-OGGETTO-NULL
            ws.getMatrValVar().setMvvTipoOggetto(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MatrValVar.Len.MVV_TIPO_OGGETTO));
        }
        // COB_CODE: IF IND-MVV-WHERE-CONDITION = -1
        //              MOVE HIGH-VALUES TO MVV-WHERE-CONDITION
        //           END-IF.
        if (ws.getIndMatrValVar().getWhereCondition() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MVV-WHERE-CONDITION
            ws.getMatrValVar().setMvvWhereCondition(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MatrValVar.Len.MVV_WHERE_CONDITION));
        }
        // COB_CODE: IF IND-MVV-SERVIZIO-LETTURA = -1
        //              MOVE HIGH-VALUES TO MVV-SERVIZIO-LETTURA-NULL
        //           END-IF.
        if (ws.getIndMatrValVar().getServizioLettura() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MVV-SERVIZIO-LETTURA-NULL
            ws.getMatrValVar().setMvvServizioLettura(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MatrValVar.Len.MVV_SERVIZIO_LETTURA));
        }
        // COB_CODE: IF IND-MVV-MODULO-CALCOLO = -1
        //              MOVE HIGH-VALUES TO MVV-MODULO-CALCOLO-NULL
        //           END-IF.
        if (ws.getIndMatrValVar().getModuloCalcolo() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MVV-MODULO-CALCOLO-NULL
            ws.getMatrValVar().setMvvModuloCalcolo(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MatrValVar.Len.MVV_MODULO_CALCOLO));
        }
        // COB_CODE: IF IND-MVV-COD-DATO-INTERNO = -1
        //              MOVE HIGH-VALUES TO MVV-COD-DATO-INTERNO
        //           END-IF.
        if (ws.getIndMatrValVar().getCodDatoInterno() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MVV-COD-DATO-INTERNO
            ws.getMatrValVar().setMvvCodDatoInterno(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MatrValVar.Len.MVV_COD_DATO_INTERNO));
        }
    }

    /**Original name: N001-IMPOSTA-OUTPUT<br>
	 * <pre>----------------------------------------------------------------*
	 *  VALORIZZA OUTPUT
	 * ----------------------------------------------------------------*</pre>*/
    private void n001ImpostaOutput() {
        // COB_CODE: IF ADA-TIPO-DATO-NULL = HIGH-VALUES
        //                TO V1391-TIPO-DATO-NULL(WK-IND-SERV)
        //           ELSE
        //                TO V1391-TIPO-DATO(WK-IND-SERV)
        //           END-IF.
        if (Characters.EQ_HIGH.test(ws.getAnagDato().getAdaTipoDatoFormatted())) {
            // COB_CODE: MOVE ADA-TIPO-DATO-NULL
            //             TO V1391-TIPO-DATO-NULL(WK-IND-SERV)
            v1391AreaActu.getTabParam(ws.getWkIndServ()).setV1391TipoDato(ws.getAnagDato().getAdaTipoDato());
        }
        else {
            // COB_CODE: MOVE ADA-TIPO-DATO
            //             TO V1391-TIPO-DATO(WK-IND-SERV)
            v1391AreaActu.getTabParam(ws.getWkIndServ()).setV1391TipoDato(ws.getAnagDato().getAdaTipoDato());
        }
        // COB_CODE: IF ADA-LUNGHEZZA-DATO-NULL = HIGH-VALUES
        //                TO V1391-LUNGHEZZA-DATO-NULL(WK-IND-SERV)
        //           ELSE
        //                TO V1391-LUNGHEZZA-DATO(WK-IND-SERV)
        //           END-IF.
        if (Characters.EQ_HIGH.test(ws.getAnagDato().getAdaLunghezzaDato().getAdaLunghezzaDatoNullFormatted())) {
            // COB_CODE: MOVE ADA-LUNGHEZZA-DATO-NULL
            //             TO V1391-LUNGHEZZA-DATO-NULL(WK-IND-SERV)
            v1391AreaActu.getTabParam(ws.getWkIndServ()).getV1391LunghezzaDato().setV1391LunghezzaDatoNull(ws.getAnagDato().getAdaLunghezzaDato().getAdaLunghezzaDatoNull());
        }
        else {
            // COB_CODE: MOVE ADA-LUNGHEZZA-DATO
            //             TO V1391-LUNGHEZZA-DATO(WK-IND-SERV)
            v1391AreaActu.getTabParam(ws.getWkIndServ()).getV1391LunghezzaDato().setV1391LunghezzaDato(ws.getAnagDato().getAdaLunghezzaDato().getAdaLunghezzaDato());
        }
        // COB_CODE: IF ADA-PRECISIONE-DATO-NULL = HIGH-VALUES
        //                TO V1391-PRECISIONE-DATO-NULL(WK-IND-SERV)
        //           ELSE
        //                TO V1391-PRECISIONE-DATO(WK-IND-SERV)
        //           END-IF.
        if (Characters.EQ_HIGH.test(ws.getAnagDato().getAdaPrecisioneDato().getAdaPrecisioneDatoNullFormatted())) {
            // COB_CODE: MOVE ADA-PRECISIONE-DATO-NULL
            //             TO V1391-PRECISIONE-DATO-NULL(WK-IND-SERV)
            v1391AreaActu.getTabParam(ws.getWkIndServ()).getV1391PrecisioneDato().setV1391PrecisioneDatoNull(ws.getAnagDato().getAdaPrecisioneDato().getAdaPrecisioneDatoNull());
        }
        else {
            // COB_CODE: MOVE ADA-PRECISIONE-DATO
            //             TO V1391-PRECISIONE-DATO(WK-IND-SERV)
            v1391AreaActu.getTabParam(ws.getWkIndServ()).getV1391PrecisioneDato().setV1391PrecisioneDato(ws.getAnagDato().getAdaPrecisioneDato().getAdaPrecisioneDato());
        }
        // COB_CODE: MOVE ADA-FORMATTAZIONE-DATO
        //             TO V1391-FORMATTAZIONE-DATO(WK-IND-SERV).
        v1391AreaActu.getTabParam(ws.getWkIndServ()).setV1391FormattazioneDato(ws.getAnagDato().getAdaFormattazioneDato());
        //
        // COB_CODE: MOVE MVV-ID-MATR-VAL-VAR
        //            TO V1391-ID-MATR-VAL-VAR(WK-IND-SERV).
        v1391AreaActu.getTabParam(ws.getWkIndServ()).setV1391IdMatrValVar(ws.getMatrValVar().getMvvIdMatrValVar());
        // COB_CODE: IF MVV-IDP-MATR-VAL-VAR-NULL = HIGH-VALUES
        //                TO V1391-IDP-MATR-VAL-VAR-NULL(WK-IND-SERV)
        //           ELSE
        //                TO V1391-IDP-MATR-VAL-VAR(WK-IND-SERV)
        //           END-IF.
        if (Characters.EQ_HIGH.test(ws.getMatrValVar().getMvvIdpMatrValVar().getMvvIdpMatrValVarNullFormatted())) {
            // COB_CODE: MOVE MVV-IDP-MATR-VAL-VAR-NULL
            //             TO V1391-IDP-MATR-VAL-VAR-NULL(WK-IND-SERV)
            v1391AreaActu.getTabParam(ws.getWkIndServ()).getV1391IdpMatrValVar().setV1391IdpMatrValVarNull(ws.getMatrValVar().getMvvIdpMatrValVar().getMvvIdpMatrValVarNull());
        }
        else {
            // COB_CODE: MOVE MVV-IDP-MATR-VAL-VAR
            //             TO V1391-IDP-MATR-VAL-VAR(WK-IND-SERV)
            v1391AreaActu.getTabParam(ws.getWkIndServ()).getV1391IdpMatrValVar().setV1391IdpMatrValVar(ws.getMatrValVar().getMvvIdpMatrValVar().getMvvIdpMatrValVar());
        }
        // COB_CODE: IF MVV-TIPO-MOVIMENTO-NULL = HIGH-VALUES
        //                TO V1391-TIPO-MOVIMENTO-NULL(WK-IND-SERV)
        //           ELSE
        //                TO V1391-TIPO-MOVIMENTO(WK-IND-SERV)
        //           END-IF.
        if (Characters.EQ_HIGH.test(ws.getMatrValVar().getMvvTipoMovimento().getMvvTipoMovimentoNullFormatted())) {
            // COB_CODE: MOVE MVV-TIPO-MOVIMENTO-NULL
            //             TO V1391-TIPO-MOVIMENTO-NULL(WK-IND-SERV)
            v1391AreaActu.getTabParam(ws.getWkIndServ()).getV1391TipoMovimento().setV1391TipoMovimentoNull(ws.getMatrValVar().getMvvTipoMovimento().getMvvTipoMovimentoNull());
        }
        else {
            // COB_CODE: MOVE MVV-TIPO-MOVIMENTO
            //             TO V1391-TIPO-MOVIMENTO(WK-IND-SERV)
            v1391AreaActu.getTabParam(ws.getWkIndServ()).getV1391TipoMovimento().setV1391TipoMovimento(ws.getMatrValVar().getMvvTipoMovimento().getMvvTipoMovimento());
        }
        // COB_CODE: MOVE MVV-COD-DATO-EXT
        //             TO V1391-COD-DATO-EXT(WK-IND-SERV).
        v1391AreaActu.getTabParam(ws.getWkIndServ()).setV1391CodDatoExt(ws.getMatrValVar().getMvvCodDatoExt());
        // COB_CODE: IF MVV-OBBLIGATORIETA-NULL = HIGH-VALUES
        //                TO V1391-OBBLIGATORIETA-NULL(WK-IND-SERV)
        //           ELSE
        //                TO V1391-OBBLIGATORIETA(WK-IND-SERV)
        //           END-IF.
        if (Conditions.eq(ws.getMatrValVar().getMvvObbligatorieta(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE MVV-OBBLIGATORIETA-NULL
            //             TO V1391-OBBLIGATORIETA-NULL(WK-IND-SERV)
            v1391AreaActu.getTabParam(ws.getWkIndServ()).setV1391Obbligatorieta(ws.getMatrValVar().getMvvObbligatorieta());
        }
        else {
            // COB_CODE: MOVE MVV-OBBLIGATORIETA
            //             TO V1391-OBBLIGATORIETA(WK-IND-SERV)
            v1391AreaActu.getTabParam(ws.getWkIndServ()).setV1391Obbligatorieta(ws.getMatrValVar().getMvvObbligatorieta());
        }
        // COB_CODE: MOVE MVV-VALORE-DEFAULT
        //             TO V1391-VALORE-DEFAULT(WK-IND-SERV).
        v1391AreaActu.getTabParam(ws.getWkIndServ()).setV1391ValoreDefault(ws.getMatrValVar().getMvvValoreDefault());
        // COB_CODE: MOVE MVV-COD-STR-DATO-PTF
        //             TO V1391-COD-STR-DATO-PTF(WK-IND-SERV).
        v1391AreaActu.getTabParam(ws.getWkIndServ()).setV1391CodStrDatoPtf(ws.getMatrValVar().getMvvCodStrDatoPtf());
        // COB_CODE: MOVE MVV-COD-DATO-PTF
        //             TO V1391-COD-DATO-PTF(WK-IND-SERV).
        v1391AreaActu.getTabParam(ws.getWkIndServ()).setV1391CodDatoPtf(ws.getMatrValVar().getMvvCodDatoPtf());
        // COB_CODE: MOVE MVV-COD-PARAMETRO
        //             TO V1391-COD-PARAMETRO(WK-IND-SERV).
        v1391AreaActu.getTabParam(ws.getWkIndServ()).setV1391CodParametro(ws.getMatrValVar().getMvvCodParametro());
        // COB_CODE: MOVE MVV-OPERAZIONE
        //             TO V1391-OPERAZIONE(WK-IND-SERV).
        v1391AreaActu.getTabParam(ws.getWkIndServ()).setV1391Operazione(ws.getMatrValVar().getMvvOperazione());
        // COB_CODE: IF MVV-LIVELLO-OPERAZIONE-NULL = HIGH-VALUES
        //                TO V1391-LIVELLO-OPERAZIONE-NULL(WK-IND-SERV)
        //           ELSE
        //                TO V1391-LIVELLO-OPERAZIONE(WK-IND-SERV)
        //           END-IF.
        if (Characters.EQ_HIGH.test(ws.getMatrValVar().getMvvLivelloOperazioneFormatted())) {
            // COB_CODE: MOVE MVV-LIVELLO-OPERAZIONE-NULL
            //             TO V1391-LIVELLO-OPERAZIONE-NULL(WK-IND-SERV)
            v1391AreaActu.getTabParam(ws.getWkIndServ()).setV1391LivelloOperazione(ws.getMatrValVar().getMvvLivelloOperazione());
        }
        else {
            // COB_CODE: MOVE MVV-LIVELLO-OPERAZIONE
            //             TO V1391-LIVELLO-OPERAZIONE(WK-IND-SERV)
            v1391AreaActu.getTabParam(ws.getWkIndServ()).setV1391LivelloOperazione(ws.getMatrValVar().getMvvLivelloOperazione());
        }
        // COB_CODE: IF MVV-TIPO-OGGETTO-NULL = HIGH-VALUES
        //                TO V1391-TIPO-OGGETTO-NULL(WK-IND-SERV)
        //           ELSE
        //                TO V1391-TIPO-OGGETTO(WK-IND-SERV)
        //           END-IF.
        if (Characters.EQ_HIGH.test(ws.getMatrValVar().getMvvTipoOggettoFormatted())) {
            // COB_CODE: MOVE MVV-TIPO-OGGETTO-NULL
            //             TO V1391-TIPO-OGGETTO-NULL(WK-IND-SERV)
            v1391AreaActu.getTabParam(ws.getWkIndServ()).setV1391TipoOggetto(ws.getMatrValVar().getMvvTipoOggetto());
        }
        else {
            // COB_CODE: MOVE MVV-TIPO-OGGETTO
            //             TO V1391-TIPO-OGGETTO(WK-IND-SERV)
            v1391AreaActu.getTabParam(ws.getWkIndServ()).setV1391TipoOggetto(ws.getMatrValVar().getMvvTipoOggetto());
        }
        // COB_CODE: MOVE MVV-WHERE-CONDITION
        //             TO V1391-WHERE-CONDITION(WK-IND-SERV).
        v1391AreaActu.getTabParam(ws.getWkIndServ()).setV1391WhereCondition(ws.getMatrValVar().getMvvWhereCondition());
        // COB_CODE: IF MVV-SERVIZIO-LETTURA-NULL = HIGH-VALUES
        //                TO V1391-SERVIZIO-LETTURA-NULL(WK-IND-SERV)
        //           ELSE
        //                TO V1391-SERVIZIO-LETTURA(WK-IND-SERV)
        //           END-IF.
        if (Characters.EQ_HIGH.test(ws.getMatrValVar().getMvvServizioLetturaFormatted())) {
            // COB_CODE: MOVE MVV-SERVIZIO-LETTURA-NULL
            //             TO V1391-SERVIZIO-LETTURA-NULL(WK-IND-SERV)
            v1391AreaActu.getTabParam(ws.getWkIndServ()).setV1391ServizioLettura(ws.getMatrValVar().getMvvServizioLettura());
        }
        else {
            // COB_CODE: MOVE MVV-SERVIZIO-LETTURA
            //             TO V1391-SERVIZIO-LETTURA(WK-IND-SERV)
            v1391AreaActu.getTabParam(ws.getWkIndServ()).setV1391ServizioLettura(ws.getMatrValVar().getMvvServizioLettura());
        }
        // COB_CODE: IF MVV-MODULO-CALCOLO-NULL = HIGH-VALUES
        //                TO V1391-MODULO-CALCOLO-NULL(WK-IND-SERV)
        //           ELSE
        //                TO V1391-MODULO-CALCOLO(WK-IND-SERV)
        //           END-IF.
        if (Characters.EQ_HIGH.test(ws.getMatrValVar().getMvvModuloCalcoloFormatted())) {
            // COB_CODE: MOVE MVV-MODULO-CALCOLO-NULL
            //             TO V1391-MODULO-CALCOLO-NULL(WK-IND-SERV)
            v1391AreaActu.getTabParam(ws.getWkIndServ()).setV1391ModuloCalcolo(ws.getMatrValVar().getMvvModuloCalcolo());
        }
        else {
            // COB_CODE: MOVE MVV-MODULO-CALCOLO
            //             TO V1391-MODULO-CALCOLO(WK-IND-SERV)
            v1391AreaActu.getTabParam(ws.getWkIndServ()).setV1391ModuloCalcolo(ws.getMatrValVar().getMvvModuloCalcolo());
        }
        // COB_CODE: MOVE MVV-COD-DATO-INTERNO
        //             TO V1391-COD-DATO-INTERNO(WK-IND-SERV).
        v1391AreaActu.getTabParam(ws.getWkIndServ()).setV1391CodDatoInterno(ws.getMatrValVar().getMvvCodDatoInterno());
    }

    public void initAnagDato() {
        ws.getAnagDato().setAdaCodCompagniaAnia(0);
        ws.getAnagDato().setAdaCodDato("");
        ws.getAnagDato().setAdaDescDatoLen(((short)0));
        ws.getAnagDato().setAdaDescDato("");
        ws.getAnagDato().setAdaTipoDato("");
        ws.getAnagDato().getAdaLunghezzaDato().setAdaLunghezzaDato(0);
        ws.getAnagDato().getAdaPrecisioneDato().setAdaPrecisioneDato(((short)0));
        ws.getAnagDato().setAdaCodDominio("");
        ws.getAnagDato().setAdaFormattazioneDato("");
        ws.getAnagDato().setAdaDsUtente("");
    }

    public void initMatrValVar() {
        ws.getMatrValVar().setMvvIdMatrValVar(0);
        ws.getMatrValVar().getMvvIdpMatrValVar().setMvvIdpMatrValVar(0);
        ws.getMatrValVar().setMvvCodCompagniaAnia(0);
        ws.getMatrValVar().getMvvTipoMovimento().setMvvTipoMovimento(0);
        ws.getMatrValVar().setMvvCodDatoExt("");
        ws.getMatrValVar().setMvvObbligatorieta(Types.SPACE_CHAR);
        ws.getMatrValVar().setMvvValoreDefault("");
        ws.getMatrValVar().setMvvCodStrDatoPtf("");
        ws.getMatrValVar().setMvvCodDatoPtf("");
        ws.getMatrValVar().setMvvCodParametro("");
        ws.getMatrValVar().setMvvOperazione("");
        ws.getMatrValVar().setMvvLivelloOperazione("");
        ws.getMatrValVar().setMvvTipoOggetto("");
        ws.getMatrValVar().setMvvWhereConditionLen(((short)0));
        ws.getMatrValVar().setMvvWhereCondition("");
        ws.getMatrValVar().setMvvServizioLettura("");
        ws.getMatrValVar().setMvvModuloCalcolo("");
        ws.getMatrValVar().setMvvStepValorizzatore(Types.SPACE_CHAR);
        ws.getMatrValVar().setMvvStepConversazione(Types.SPACE_CHAR);
        ws.getMatrValVar().setMvvOperLogStatoBus("");
        ws.getMatrValVar().setMvvArrayStatoBus("");
        ws.getMatrValVar().setMvvOperLogCausale("");
        ws.getMatrValVar().setMvvArrayCausale("");
        ws.getMatrValVar().setMvvCodDatoInterno("");
    }

    @Override
    public String getAdaFormattazioneDato() {
        throw new FieldNotMappedException("adaFormattazioneDato");
    }

    @Override
    public void setAdaFormattazioneDato(String adaFormattazioneDato) {
        throw new FieldNotMappedException("adaFormattazioneDato");
    }

    @Override
    public String getAdaFormattazioneDatoObj() {
        return getAdaFormattazioneDato();
    }

    @Override
    public void setAdaFormattazioneDatoObj(String adaFormattazioneDatoObj) {
        setAdaFormattazioneDato(adaFormattazioneDatoObj);
    }

    @Override
    public int getAdaLunghezzaDato() {
        throw new FieldNotMappedException("adaLunghezzaDato");
    }

    @Override
    public void setAdaLunghezzaDato(int adaLunghezzaDato) {
        throw new FieldNotMappedException("adaLunghezzaDato");
    }

    @Override
    public Integer getAdaLunghezzaDatoObj() {
        return ((Integer)getAdaLunghezzaDato());
    }

    @Override
    public void setAdaLunghezzaDatoObj(Integer adaLunghezzaDatoObj) {
        setAdaLunghezzaDato(((int)adaLunghezzaDatoObj));
    }

    @Override
    public short getAdaPrecisioneDato() {
        throw new FieldNotMappedException("adaPrecisioneDato");
    }

    @Override
    public void setAdaPrecisioneDato(short adaPrecisioneDato) {
        throw new FieldNotMappedException("adaPrecisioneDato");
    }

    @Override
    public Short getAdaPrecisioneDatoObj() {
        return ((Short)getAdaPrecisioneDato());
    }

    @Override
    public void setAdaPrecisioneDatoObj(Short adaPrecisioneDatoObj) {
        setAdaPrecisioneDato(((short)adaPrecisioneDatoObj));
    }

    @Override
    public String getAdaTipoDato() {
        throw new FieldNotMappedException("adaTipoDato");
    }

    @Override
    public void setAdaTipoDato(String adaTipoDato) {
        throw new FieldNotMappedException("adaTipoDato");
    }

    @Override
    public String getAdaTipoDatoObj() {
        return getAdaTipoDato();
    }

    @Override
    public void setAdaTipoDatoObj(String adaTipoDatoObj) {
        setAdaTipoDato(adaTipoDatoObj);
    }

    @Override
    public String getMvvCodDatoExt() {
        throw new FieldNotMappedException("mvvCodDatoExt");
    }

    @Override
    public void setMvvCodDatoExt(String mvvCodDatoExt) {
        throw new FieldNotMappedException("mvvCodDatoExt");
    }

    @Override
    public String getMvvCodDatoExtObj() {
        return getMvvCodDatoExt();
    }

    @Override
    public void setMvvCodDatoExtObj(String mvvCodDatoExtObj) {
        setMvvCodDatoExt(mvvCodDatoExtObj);
    }

    @Override
    public String getMvvCodDatoInterno() {
        throw new FieldNotMappedException("mvvCodDatoInterno");
    }

    @Override
    public void setMvvCodDatoInterno(String mvvCodDatoInterno) {
        throw new FieldNotMappedException("mvvCodDatoInterno");
    }

    @Override
    public String getMvvCodDatoInternoObj() {
        return getMvvCodDatoInterno();
    }

    @Override
    public void setMvvCodDatoInternoObj(String mvvCodDatoInternoObj) {
        setMvvCodDatoInterno(mvvCodDatoInternoObj);
    }

    @Override
    public String getMvvCodDatoPtf() {
        throw new FieldNotMappedException("mvvCodDatoPtf");
    }

    @Override
    public void setMvvCodDatoPtf(String mvvCodDatoPtf) {
        throw new FieldNotMappedException("mvvCodDatoPtf");
    }

    @Override
    public String getMvvCodDatoPtfObj() {
        return getMvvCodDatoPtf();
    }

    @Override
    public void setMvvCodDatoPtfObj(String mvvCodDatoPtfObj) {
        setMvvCodDatoPtf(mvvCodDatoPtfObj);
    }

    @Override
    public String getMvvCodParametro() {
        throw new FieldNotMappedException("mvvCodParametro");
    }

    @Override
    public void setMvvCodParametro(String mvvCodParametro) {
        throw new FieldNotMappedException("mvvCodParametro");
    }

    @Override
    public String getMvvCodParametroObj() {
        return getMvvCodParametro();
    }

    @Override
    public void setMvvCodParametroObj(String mvvCodParametroObj) {
        setMvvCodParametro(mvvCodParametroObj);
    }

    @Override
    public String getMvvCodStrDatoPtf() {
        throw new FieldNotMappedException("mvvCodStrDatoPtf");
    }

    @Override
    public void setMvvCodStrDatoPtf(String mvvCodStrDatoPtf) {
        throw new FieldNotMappedException("mvvCodStrDatoPtf");
    }

    @Override
    public String getMvvCodStrDatoPtfObj() {
        return getMvvCodStrDatoPtf();
    }

    @Override
    public void setMvvCodStrDatoPtfObj(String mvvCodStrDatoPtfObj) {
        setMvvCodStrDatoPtf(mvvCodStrDatoPtfObj);
    }

    @Override
    public int getMvvIdMatrValVar() {
        throw new FieldNotMappedException("mvvIdMatrValVar");
    }

    @Override
    public void setMvvIdMatrValVar(int mvvIdMatrValVar) {
        throw new FieldNotMappedException("mvvIdMatrValVar");
    }

    @Override
    public int getMvvIdpMatrValVar() {
        throw new FieldNotMappedException("mvvIdpMatrValVar");
    }

    @Override
    public void setMvvIdpMatrValVar(int mvvIdpMatrValVar) {
        throw new FieldNotMappedException("mvvIdpMatrValVar");
    }

    @Override
    public Integer getMvvIdpMatrValVarObj() {
        return ((Integer)getMvvIdpMatrValVar());
    }

    @Override
    public void setMvvIdpMatrValVarObj(Integer mvvIdpMatrValVarObj) {
        setMvvIdpMatrValVar(((int)mvvIdpMatrValVarObj));
    }

    @Override
    public String getMvvLivelloOperazione() {
        throw new FieldNotMappedException("mvvLivelloOperazione");
    }

    @Override
    public void setMvvLivelloOperazione(String mvvLivelloOperazione) {
        throw new FieldNotMappedException("mvvLivelloOperazione");
    }

    @Override
    public String getMvvLivelloOperazioneObj() {
        return getMvvLivelloOperazione();
    }

    @Override
    public void setMvvLivelloOperazioneObj(String mvvLivelloOperazioneObj) {
        setMvvLivelloOperazione(mvvLivelloOperazioneObj);
    }

    @Override
    public String getMvvModuloCalcolo() {
        throw new FieldNotMappedException("mvvModuloCalcolo");
    }

    @Override
    public void setMvvModuloCalcolo(String mvvModuloCalcolo) {
        throw new FieldNotMappedException("mvvModuloCalcolo");
    }

    @Override
    public String getMvvModuloCalcoloObj() {
        return getMvvModuloCalcolo();
    }

    @Override
    public void setMvvModuloCalcoloObj(String mvvModuloCalcoloObj) {
        setMvvModuloCalcolo(mvvModuloCalcoloObj);
    }

    @Override
    public char getMvvObbligatorieta() {
        throw new FieldNotMappedException("mvvObbligatorieta");
    }

    @Override
    public void setMvvObbligatorieta(char mvvObbligatorieta) {
        throw new FieldNotMappedException("mvvObbligatorieta");
    }

    @Override
    public Character getMvvObbligatorietaObj() {
        return ((Character)getMvvObbligatorieta());
    }

    @Override
    public void setMvvObbligatorietaObj(Character mvvObbligatorietaObj) {
        setMvvObbligatorieta(((char)mvvObbligatorietaObj));
    }

    @Override
    public String getMvvOperazione() {
        throw new FieldNotMappedException("mvvOperazione");
    }

    @Override
    public void setMvvOperazione(String mvvOperazione) {
        throw new FieldNotMappedException("mvvOperazione");
    }

    @Override
    public String getMvvOperazioneObj() {
        return getMvvOperazione();
    }

    @Override
    public void setMvvOperazioneObj(String mvvOperazioneObj) {
        setMvvOperazione(mvvOperazioneObj);
    }

    @Override
    public String getMvvServizioLettura() {
        throw new FieldNotMappedException("mvvServizioLettura");
    }

    @Override
    public void setMvvServizioLettura(String mvvServizioLettura) {
        throw new FieldNotMappedException("mvvServizioLettura");
    }

    @Override
    public String getMvvServizioLetturaObj() {
        return getMvvServizioLettura();
    }

    @Override
    public void setMvvServizioLetturaObj(String mvvServizioLetturaObj) {
        setMvvServizioLettura(mvvServizioLetturaObj);
    }

    @Override
    public int getMvvTipoMovimento() {
        throw new FieldNotMappedException("mvvTipoMovimento");
    }

    @Override
    public void setMvvTipoMovimento(int mvvTipoMovimento) {
        throw new FieldNotMappedException("mvvTipoMovimento");
    }

    @Override
    public Integer getMvvTipoMovimentoObj() {
        return ((Integer)getMvvTipoMovimento());
    }

    @Override
    public void setMvvTipoMovimentoObj(Integer mvvTipoMovimentoObj) {
        setMvvTipoMovimento(((int)mvvTipoMovimentoObj));
    }

    @Override
    public String getMvvTipoOggetto() {
        throw new FieldNotMappedException("mvvTipoOggetto");
    }

    @Override
    public void setMvvTipoOggetto(String mvvTipoOggetto) {
        throw new FieldNotMappedException("mvvTipoOggetto");
    }

    @Override
    public String getMvvTipoOggettoObj() {
        return getMvvTipoOggetto();
    }

    @Override
    public void setMvvTipoOggettoObj(String mvvTipoOggettoObj) {
        setMvvTipoOggetto(mvvTipoOggettoObj);
    }

    @Override
    public String getMvvValoreDefault() {
        throw new FieldNotMappedException("mvvValoreDefault");
    }

    @Override
    public void setMvvValoreDefault(String mvvValoreDefault) {
        throw new FieldNotMappedException("mvvValoreDefault");
    }

    @Override
    public String getMvvValoreDefaultObj() {
        return getMvvValoreDefault();
    }

    @Override
    public void setMvvValoreDefaultObj(String mvvValoreDefaultObj) {
        setMvvValoreDefault(mvvValoreDefaultObj);
    }

    @Override
    public String getMvvWhereConditionVchar() {
        throw new FieldNotMappedException("mvvWhereConditionVchar");
    }

    @Override
    public void setMvvWhereConditionVchar(String mvvWhereConditionVchar) {
        throw new FieldNotMappedException("mvvWhereConditionVchar");
    }

    @Override
    public String getMvvWhereConditionVcharObj() {
        return getMvvWhereConditionVchar();
    }

    @Override
    public void setMvvWhereConditionVcharObj(String mvvWhereConditionVcharObj) {
        setMvvWhereConditionVchar(mvvWhereConditionVcharObj);
    }
}
