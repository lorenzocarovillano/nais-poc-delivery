package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.jdbc.FieldNotMappedException;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.MoviDao;
import it.accenture.jnais.commons.data.to.IMovi;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ldbs2920Data;
import it.accenture.jnais.ws.Ldbv2921;

/**Original name: LDBS2920<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  02 LUG 2014.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO AD HOC PER ACCESSO RISORSE DB        *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Ldbs2920 extends Program implements IMovi {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private MoviDao moviDao = new MoviDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Ldbs2920Data ws = new Ldbs2920Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: LDBV2921
    private Ldbv2921 ldbv2921;

    //==== METHODS ====
    /**Original name: PROGRAM_LDBS2920_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, Ldbv2921 ldbv2921) {
        this.idsv0003 = idsv0003;
        this.ldbv2921 = ldbv2921;
        // COB_CODE: PERFORM A000-INIZIO              THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                        a200ElaboraWcEff();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                        b200ElaboraWcCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //               SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                        c200ElaboraWcNst();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Ldbs2920 getInstance() {
        return ((Ldbs2920)Programs.getInstance(Ldbs2920.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'LDBS2920'              TO   IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("LDBS2920");
        // COB_CODE: MOVE ' LDBV2921' TO   IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella(" LDBV2921");
        // COB_CODE: MOVE '00'                      TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                    TO   IDSV0003-SQLCODE
        //                                               IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                    TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                               IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-WC-EFF<br>*/
    private void a200ElaboraWcEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-WC-EFF          THRU A210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-WC-EFF          THRU A210-EX
            a210SelectWcEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
            a260OpenCursorWcEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
            a270CloseCursorWcEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
            a280FetchFirstWcEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
            a290FetchNextWcEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B200-ELABORA-WC-CPZ<br>*/
    private void b200ElaboraWcCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
            b210SelectWcCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
            b260OpenCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
            b270CloseCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
            b280FetchFirstWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
            b290FetchNextWcCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: C200-ELABORA-WC-NST<br>*/
    private void c200ElaboraWcNst() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM C210-SELECT-WC-NST          THRU C210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM C210-SELECT-WC-NST          THRU C210-EX
            c210SelectWcNst();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
            c260OpenCursorWcNst();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
            c270CloseCursorWcNst();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
            c280FetchFirstWcNst();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
            c290FetchNextWcNst();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A205-DECLARE-CURSOR-WC-EFF<br>
	 * <pre>----
	 * ----  gestione WC Effetto
	 * ----</pre>*/
    private void a205DeclareCursorWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A210-SELECT-WC-EFF<br>*/
    private void a210SelectWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //            SELECT MAX (A.DT_EFF)
        //             INTO :MOV-DT-EFF-DB
        //             FROM MOVI A, POLI B, LIQ D
        //             WHERE A.TP_MOVI IN ( 5005 , 2316 , 2318 )
        //             AND A.ID_OGG = B.ID_POLI
        //             AND A.TP_OGG = 'PO'
        //             AND A.DT_EFF < :POL-DT-INI-EFF-DB
        //             AND B.ID_POLI = :LDBV2921-ID-POL
        //             AND B.DT_EMIS < :POL-DT-EMIS-DB
        //             AND D.ID_MOVI_CRZ = A.ID_MOVI
        //             AND D.TOT_IMP_RIT_ACC > 0
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND B.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.ID_MOVI_CHIU IS NULL
        //                    AND D.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND D.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND D.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND D.ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        ws.setMovDtEffDb(moviDao.selectRec5(this, ws.getMovDtEffDb()));
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: A260-OPEN-CURSOR-WC-EFF<br>*/
    private void a260OpenCursorWcEff() {
        // COB_CODE: PERFORM A205-DECLARE-CURSOR-WC-EFF THRU A205-EX.
        a205DeclareCursorWcEff();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A270-CLOSE-CURSOR-WC-EFF<br>*/
    private void a270CloseCursorWcEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A280-FETCH-FIRST-WC-EFF<br>*/
    private void a280FetchFirstWcEff() {
        // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF    THRU A260-EX.
        a260OpenCursorWcEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
            a290FetchNextWcEff();
        }
    }

    /**Original name: A290-FETCH-NEXT-WC-EFF<br>*/
    private void a290FetchNextWcEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B205-DECLARE-CURSOR-WC-CPZ<br>
	 * <pre>----
	 * ----  gestione WC Competenza
	 * ----</pre>*/
    private void b205DeclareCursorWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B210-SELECT-WC-CPZ<br>*/
    private void b210SelectWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //            SELECT MAX (A.DT_EFF)
        //             INTO :MOV-DT-EFF-DB
        //             FROM MOVI A, POLI B, LIQ D
        //             WHERE A.TP_MOVI IN ( 5005 , 2316 , 2318 )
        //             AND A.ID_OGG = B.ID_POLI
        //             AND A.TP_OGG = 'PO'
        //             AND A.DT_EFF < :POL-DT-INI-EFF-DB
        //             AND B.ID_POLI = :LDBV2921-ID-POL
        //             AND B.DT_EMIS < :POL-DT-EMIS-DB
        //             AND D.ID_MOVI_CRZ = A.ID_MOVI
        //             AND D.TOT_IMP_RIT_ACC > 0
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND B.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND B.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //                    AND D.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND D.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND D.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND D.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND D.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        ws.setMovDtEffDb(moviDao.selectRec6(this, ws.getMovDtEffDb()));
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: B260-OPEN-CURSOR-WC-CPZ<br>*/
    private void b260OpenCursorWcCpz() {
        // COB_CODE: PERFORM B205-DECLARE-CURSOR-WC-CPZ THRU B205-EX.
        b205DeclareCursorWcCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B270-CLOSE-CURSOR-WC-CPZ<br>*/
    private void b270CloseCursorWcCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B280-FETCH-FIRST-WC-CPZ<br>*/
    private void b280FetchFirstWcCpz() {
        // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ    THRU B260-EX.
        b260OpenCursorWcCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
            b290FetchNextWcCpz();
        }
    }

    /**Original name: B290-FETCH-NEXT-WC-CPZ<br>*/
    private void b290FetchNextWcCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C205-DECLARE-CURSOR-WC-NST<br>
	 * <pre>----
	 * ----  gestione WC Senza Storicità
	 * ----</pre>*/
    private void c205DeclareCursorWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C210-SELECT-WC-NST<br>*/
    private void c210SelectWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C260-OPEN-CURSOR-WC-NST<br>*/
    private void c260OpenCursorWcNst() {
        // COB_CODE: PERFORM C205-DECLARE-CURSOR-WC-NST THRU C205-EX.
        c205DeclareCursorWcNst();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C270-CLOSE-CURSOR-WC-NST<br>*/
    private void c270CloseCursorWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C280-FETCH-FIRST-WC-NST<br>*/
    private void c280FetchFirstWcNst() {
        // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST    THRU C260-EX.
        c260OpenCursorWcNst();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
            c290FetchNextWcNst();
        }
    }

    /**Original name: C290-FETCH-NEXT-WC-NST<br>*/
    private void c290FetchNextWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>----
	 * ----  utilità comuni a tutti i livelli operazione
	 * ----</pre>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: CONTINUE.
        //continue
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z970-CODICE-ADHOC-PRE<br>
	 * <pre>----
	 * ----  prevede statements AD HOC PRE Query
	 * ----</pre>*/
    private void z970CodiceAdhocPre() {
        // COB_CODE: MOVE LDBV2921-DT-DECOR-MAX    TO WS-DATE-N.
        ws.getIdsv0010().setWsDateNFormatted(ldbv2921.getDtDecorMaxFormatted());
        // COB_CODE: PERFORM Z700-DT-N-TO-X        THRU Z700-EX.
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X                TO POL-DT-INI-EFF-DB.
        ws.getPoliDb().setEndEffDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: MOVE LDBV2921-DT-EMIS         TO WS-DATE-N.
        ws.getIdsv0010().setWsDateNFormatted(ldbv2921.getDtEmisFormatted());
        // COB_CODE: PERFORM Z700-DT-N-TO-X        THRU Z700-EX.
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X                TO POL-DT-EMIS-DB.
        ws.getPoliDb().setVarzTpIasDb(ws.getIdsv0010().getWsDateX());
    }

    /**Original name: Z980-CODICE-ADHOC-POST<br>
	 * <pre>----
	 * ----  prevede statements AD HOC POST Query
	 * ----</pre>*/
    private void z980CodiceAdhocPost() {
        // COB_CODE: IF IDSV0003-SQLCODE = ZEROES
        //              END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: IF MOV-DT-EFF-DB NOT EQUAL SPACES
            //              MOVE WS-DATE-N           TO LDBV2921-DT-EFF
            //            END-IF
            if (!Characters.EQ_SPACE.test(ws.getMovDtEffDb())) {
                // COB_CODE: MOVE MOV-DT-EFF-DB       TO WS-DATE-X
                ws.getIdsv0010().setWsDateX(ws.getMovDtEffDb());
                // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
                z800DtXToN();
                // COB_CODE: MOVE WS-DATE-N           TO LDBV2921-DT-EFF
                ldbv2921.setDtEffFormatted(ws.getIdsv0010().getWsDateNFormatted());
            }
        }
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public int getCodCompAnia() {
        throw new FieldNotMappedException("codCompAnia");
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        throw new FieldNotMappedException("codCompAnia");
    }

    @Override
    public char getDsOperSql() {
        throw new FieldNotMappedException("dsOperSql");
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        throw new FieldNotMappedException("dsOperSql");
    }

    @Override
    public char getDsStatoElab() {
        throw new FieldNotMappedException("dsStatoElab");
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        throw new FieldNotMappedException("dsStatoElab");
    }

    @Override
    public long getDsTsCptz() {
        throw new FieldNotMappedException("dsTsCptz");
    }

    @Override
    public void setDsTsCptz(long dsTsCptz) {
        throw new FieldNotMappedException("dsTsCptz");
    }

    @Override
    public String getDsUtente() {
        throw new FieldNotMappedException("dsUtente");
    }

    @Override
    public void setDsUtente(String dsUtente) {
        throw new FieldNotMappedException("dsUtente");
    }

    @Override
    public int getDsVer() {
        throw new FieldNotMappedException("dsVer");
    }

    @Override
    public void setDsVer(int dsVer) {
        throw new FieldNotMappedException("dsVer");
    }

    @Override
    public String getIbMovi() {
        throw new FieldNotMappedException("ibMovi");
    }

    @Override
    public void setIbMovi(String ibMovi) {
        throw new FieldNotMappedException("ibMovi");
    }

    @Override
    public String getIbMoviObj() {
        return getIbMovi();
    }

    @Override
    public void setIbMoviObj(String ibMoviObj) {
        setIbMovi(ibMoviObj);
    }

    @Override
    public String getIbOgg() {
        throw new FieldNotMappedException("ibOgg");
    }

    @Override
    public void setIbOgg(String ibOgg) {
        throw new FieldNotMappedException("ibOgg");
    }

    @Override
    public String getIbOggObj() {
        return getIbOgg();
    }

    @Override
    public void setIbOggObj(String ibOggObj) {
        setIbOgg(ibOggObj);
    }

    @Override
    public int getIdMoviAnn() {
        throw new FieldNotMappedException("idMoviAnn");
    }

    @Override
    public void setIdMoviAnn(int idMoviAnn) {
        throw new FieldNotMappedException("idMoviAnn");
    }

    @Override
    public Integer getIdMoviAnnObj() {
        return ((Integer)getIdMoviAnn());
    }

    @Override
    public void setIdMoviAnnObj(Integer idMoviAnnObj) {
        setIdMoviAnn(((int)idMoviAnnObj));
    }

    @Override
    public int getIdMoviCollg() {
        throw new FieldNotMappedException("idMoviCollg");
    }

    @Override
    public void setIdMoviCollg(int idMoviCollg) {
        throw new FieldNotMappedException("idMoviCollg");
    }

    @Override
    public Integer getIdMoviCollgObj() {
        return ((Integer)getIdMoviCollg());
    }

    @Override
    public void setIdMoviCollgObj(Integer idMoviCollgObj) {
        setIdMoviCollg(((int)idMoviCollgObj));
    }

    @Override
    public int getIdRich() {
        throw new FieldNotMappedException("idRich");
    }

    @Override
    public void setIdRich(int idRich) {
        throw new FieldNotMappedException("idRich");
    }

    @Override
    public Integer getIdRichObj() {
        return ((Integer)getIdRich());
    }

    @Override
    public void setIdRichObj(Integer idRichObj) {
        setIdRich(((int)idRichObj));
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        return idsv0003.getCodiceCompagniaAnia();
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        this.idsv0003.setCodiceCompagniaAnia(idsv0003CodiceCompagniaAnia);
    }

    @Override
    public String getLdbv2651DtFineDb() {
        throw new FieldNotMappedException("ldbv2651DtFineDb");
    }

    @Override
    public void setLdbv2651DtFineDb(String ldbv2651DtFineDb) {
        throw new FieldNotMappedException("ldbv2651DtFineDb");
    }

    @Override
    public String getLdbv2651DtInizDb() {
        throw new FieldNotMappedException("ldbv2651DtInizDb");
    }

    @Override
    public void setLdbv2651DtInizDb(String ldbv2651DtInizDb) {
        throw new FieldNotMappedException("ldbv2651DtInizDb");
    }

    @Override
    public int getLdbv2651IdOgg() {
        throw new FieldNotMappedException("ldbv2651IdOgg");
    }

    @Override
    public void setLdbv2651IdOgg(int ldbv2651IdOgg) {
        throw new FieldNotMappedException("ldbv2651IdOgg");
    }

    @Override
    public int getLdbv2651TpMov1() {
        throw new FieldNotMappedException("ldbv2651TpMov1");
    }

    @Override
    public void setLdbv2651TpMov1(int ldbv2651TpMov1) {
        throw new FieldNotMappedException("ldbv2651TpMov1");
    }

    @Override
    public int getLdbv2651TpMov2() {
        throw new FieldNotMappedException("ldbv2651TpMov2");
    }

    @Override
    public void setLdbv2651TpMov2(int ldbv2651TpMov2) {
        throw new FieldNotMappedException("ldbv2651TpMov2");
    }

    @Override
    public int getLdbv2651TpMov3() {
        throw new FieldNotMappedException("ldbv2651TpMov3");
    }

    @Override
    public void setLdbv2651TpMov3(int ldbv2651TpMov3) {
        throw new FieldNotMappedException("ldbv2651TpMov3");
    }

    @Override
    public String getLdbv2651TpOgg() {
        throw new FieldNotMappedException("ldbv2651TpOgg");
    }

    @Override
    public void setLdbv2651TpOgg(String ldbv2651TpOgg) {
        throw new FieldNotMappedException("ldbv2651TpOgg");
    }

    @Override
    public int getLdbv2921IdPol() {
        return ldbv2921.getIdPol();
    }

    @Override
    public void setLdbv2921IdPol(int ldbv2921IdPol) {
        this.ldbv2921.setIdPol(ldbv2921IdPol);
    }

    @Override
    public String getLdbv3611DtADb() {
        throw new FieldNotMappedException("ldbv3611DtADb");
    }

    @Override
    public void setLdbv3611DtADb(String ldbv3611DtADb) {
        throw new FieldNotMappedException("ldbv3611DtADb");
    }

    @Override
    public String getLdbv3611DtDaDb() {
        throw new FieldNotMappedException("ldbv3611DtDaDb");
    }

    @Override
    public void setLdbv3611DtDaDb(String ldbv3611DtDaDb) {
        throw new FieldNotMappedException("ldbv3611DtDaDb");
    }

    @Override
    public int getLdbv3611IdOgg() {
        throw new FieldNotMappedException("ldbv3611IdOgg");
    }

    @Override
    public void setLdbv3611IdOgg(int ldbv3611IdOgg) {
        throw new FieldNotMappedException("ldbv3611IdOgg");
    }

    @Override
    public int getLdbv3611TpMovi10() {
        throw new FieldNotMappedException("ldbv3611TpMovi10");
    }

    @Override
    public void setLdbv3611TpMovi10(int ldbv3611TpMovi10) {
        throw new FieldNotMappedException("ldbv3611TpMovi10");
    }

    @Override
    public int getLdbv3611TpMovi11() {
        throw new FieldNotMappedException("ldbv3611TpMovi11");
    }

    @Override
    public void setLdbv3611TpMovi11(int ldbv3611TpMovi11) {
        throw new FieldNotMappedException("ldbv3611TpMovi11");
    }

    @Override
    public int getLdbv3611TpMovi12() {
        throw new FieldNotMappedException("ldbv3611TpMovi12");
    }

    @Override
    public void setLdbv3611TpMovi12(int ldbv3611TpMovi12) {
        throw new FieldNotMappedException("ldbv3611TpMovi12");
    }

    @Override
    public int getLdbv3611TpMovi13() {
        throw new FieldNotMappedException("ldbv3611TpMovi13");
    }

    @Override
    public void setLdbv3611TpMovi13(int ldbv3611TpMovi13) {
        throw new FieldNotMappedException("ldbv3611TpMovi13");
    }

    @Override
    public int getLdbv3611TpMovi14() {
        throw new FieldNotMappedException("ldbv3611TpMovi14");
    }

    @Override
    public void setLdbv3611TpMovi14(int ldbv3611TpMovi14) {
        throw new FieldNotMappedException("ldbv3611TpMovi14");
    }

    @Override
    public int getLdbv3611TpMovi15() {
        throw new FieldNotMappedException("ldbv3611TpMovi15");
    }

    @Override
    public void setLdbv3611TpMovi15(int ldbv3611TpMovi15) {
        throw new FieldNotMappedException("ldbv3611TpMovi15");
    }

    @Override
    public int getLdbv3611TpMovi1() {
        throw new FieldNotMappedException("ldbv3611TpMovi1");
    }

    @Override
    public void setLdbv3611TpMovi1(int ldbv3611TpMovi1) {
        throw new FieldNotMappedException("ldbv3611TpMovi1");
    }

    @Override
    public int getLdbv3611TpMovi2() {
        throw new FieldNotMappedException("ldbv3611TpMovi2");
    }

    @Override
    public void setLdbv3611TpMovi2(int ldbv3611TpMovi2) {
        throw new FieldNotMappedException("ldbv3611TpMovi2");
    }

    @Override
    public int getLdbv3611TpMovi3() {
        throw new FieldNotMappedException("ldbv3611TpMovi3");
    }

    @Override
    public void setLdbv3611TpMovi3(int ldbv3611TpMovi3) {
        throw new FieldNotMappedException("ldbv3611TpMovi3");
    }

    @Override
    public int getLdbv3611TpMovi4() {
        throw new FieldNotMappedException("ldbv3611TpMovi4");
    }

    @Override
    public void setLdbv3611TpMovi4(int ldbv3611TpMovi4) {
        throw new FieldNotMappedException("ldbv3611TpMovi4");
    }

    @Override
    public int getLdbv3611TpMovi5() {
        throw new FieldNotMappedException("ldbv3611TpMovi5");
    }

    @Override
    public void setLdbv3611TpMovi5(int ldbv3611TpMovi5) {
        throw new FieldNotMappedException("ldbv3611TpMovi5");
    }

    @Override
    public int getLdbv3611TpMovi6() {
        throw new FieldNotMappedException("ldbv3611TpMovi6");
    }

    @Override
    public void setLdbv3611TpMovi6(int ldbv3611TpMovi6) {
        throw new FieldNotMappedException("ldbv3611TpMovi6");
    }

    @Override
    public int getLdbv3611TpMovi7() {
        throw new FieldNotMappedException("ldbv3611TpMovi7");
    }

    @Override
    public void setLdbv3611TpMovi7(int ldbv3611TpMovi7) {
        throw new FieldNotMappedException("ldbv3611TpMovi7");
    }

    @Override
    public int getLdbv3611TpMovi8() {
        throw new FieldNotMappedException("ldbv3611TpMovi8");
    }

    @Override
    public void setLdbv3611TpMovi8(int ldbv3611TpMovi8) {
        throw new FieldNotMappedException("ldbv3611TpMovi8");
    }

    @Override
    public int getLdbv3611TpMovi9() {
        throw new FieldNotMappedException("ldbv3611TpMovi9");
    }

    @Override
    public void setLdbv3611TpMovi9(int ldbv3611TpMovi9) {
        throw new FieldNotMappedException("ldbv3611TpMovi9");
    }

    @Override
    public String getLdbv3611TpOgg() {
        throw new FieldNotMappedException("ldbv3611TpOgg");
    }

    @Override
    public void setLdbv3611TpOgg(String ldbv3611TpOgg) {
        throw new FieldNotMappedException("ldbv3611TpOgg");
    }

    @Override
    public int getLdbv5991TpMov10() {
        throw new FieldNotMappedException("ldbv5991TpMov10");
    }

    @Override
    public void setLdbv5991TpMov10(int ldbv5991TpMov10) {
        throw new FieldNotMappedException("ldbv5991TpMov10");
    }

    @Override
    public int getLdbv5991TpMov1() {
        throw new FieldNotMappedException("ldbv5991TpMov1");
    }

    @Override
    public void setLdbv5991TpMov1(int ldbv5991TpMov1) {
        throw new FieldNotMappedException("ldbv5991TpMov1");
    }

    @Override
    public int getLdbv5991TpMov2() {
        throw new FieldNotMappedException("ldbv5991TpMov2");
    }

    @Override
    public void setLdbv5991TpMov2(int ldbv5991TpMov2) {
        throw new FieldNotMappedException("ldbv5991TpMov2");
    }

    @Override
    public int getLdbv5991TpMov3() {
        throw new FieldNotMappedException("ldbv5991TpMov3");
    }

    @Override
    public void setLdbv5991TpMov3(int ldbv5991TpMov3) {
        throw new FieldNotMappedException("ldbv5991TpMov3");
    }

    @Override
    public int getLdbv5991TpMov4() {
        throw new FieldNotMappedException("ldbv5991TpMov4");
    }

    @Override
    public void setLdbv5991TpMov4(int ldbv5991TpMov4) {
        throw new FieldNotMappedException("ldbv5991TpMov4");
    }

    @Override
    public int getLdbv5991TpMov5() {
        throw new FieldNotMappedException("ldbv5991TpMov5");
    }

    @Override
    public void setLdbv5991TpMov5(int ldbv5991TpMov5) {
        throw new FieldNotMappedException("ldbv5991TpMov5");
    }

    @Override
    public int getLdbv5991TpMov6() {
        throw new FieldNotMappedException("ldbv5991TpMov6");
    }

    @Override
    public void setLdbv5991TpMov6(int ldbv5991TpMov6) {
        throw new FieldNotMappedException("ldbv5991TpMov6");
    }

    @Override
    public int getLdbv5991TpMov7() {
        throw new FieldNotMappedException("ldbv5991TpMov7");
    }

    @Override
    public void setLdbv5991TpMov7(int ldbv5991TpMov7) {
        throw new FieldNotMappedException("ldbv5991TpMov7");
    }

    @Override
    public int getLdbv5991TpMov8() {
        throw new FieldNotMappedException("ldbv5991TpMov8");
    }

    @Override
    public void setLdbv5991TpMov8(int ldbv5991TpMov8) {
        throw new FieldNotMappedException("ldbv5991TpMov8");
    }

    @Override
    public int getLdbv5991TpMov9() {
        throw new FieldNotMappedException("ldbv5991TpMov9");
    }

    @Override
    public void setLdbv5991TpMov9(int ldbv5991TpMov9) {
        throw new FieldNotMappedException("ldbv5991TpMov9");
    }

    @Override
    public int getLdbvc591TpMovi1() {
        throw new FieldNotMappedException("ldbvc591TpMovi1");
    }

    @Override
    public void setLdbvc591TpMovi1(int ldbvc591TpMovi1) {
        throw new FieldNotMappedException("ldbvc591TpMovi1");
    }

    @Override
    public int getLdbvc591TpMovi2() {
        throw new FieldNotMappedException("ldbvc591TpMovi2");
    }

    @Override
    public void setLdbvc591TpMovi2(int ldbvc591TpMovi2) {
        throw new FieldNotMappedException("ldbvc591TpMovi2");
    }

    @Override
    public int getLdbve061IdOgg() {
        throw new FieldNotMappedException("ldbve061IdOgg");
    }

    @Override
    public void setLdbve061IdOgg(int ldbve061IdOgg) {
        throw new FieldNotMappedException("ldbve061IdOgg");
    }

    @Override
    public int getLdbve061TpMovi() {
        throw new FieldNotMappedException("ldbve061TpMovi");
    }

    @Override
    public void setLdbve061TpMovi(int ldbve061TpMovi) {
        throw new FieldNotMappedException("ldbve061TpMovi");
    }

    @Override
    public String getLdbve061TpOgg() {
        throw new FieldNotMappedException("ldbve061TpOgg");
    }

    @Override
    public void setLdbve061TpOgg(String ldbve061TpOgg) {
        throw new FieldNotMappedException("ldbve061TpOgg");
    }

    @Override
    public String getMovDtEffDb() {
        return ws.getMovDtEffDb();
    }

    @Override
    public void setMovDtEffDb(String movDtEffDb) {
        this.ws.setMovDtEffDb(movDtEffDb);
    }

    @Override
    public int getMovIdMovi() {
        throw new FieldNotMappedException("movIdMovi");
    }

    @Override
    public void setMovIdMovi(int movIdMovi) {
        throw new FieldNotMappedException("movIdMovi");
    }

    @Override
    public int getMovIdOgg() {
        throw new FieldNotMappedException("movIdOgg");
    }

    @Override
    public void setMovIdOgg(int movIdOgg) {
        throw new FieldNotMappedException("movIdOgg");
    }

    @Override
    public Integer getMovIdOggObj() {
        return ((Integer)getMovIdOgg());
    }

    @Override
    public void setMovIdOggObj(Integer movIdOggObj) {
        setMovIdOgg(((int)movIdOggObj));
    }

    @Override
    public String getMovTpOgg() {
        throw new FieldNotMappedException("movTpOgg");
    }

    @Override
    public void setMovTpOgg(String movTpOgg) {
        throw new FieldNotMappedException("movTpOgg");
    }

    @Override
    public String getMovTpOggObj() {
        return getMovTpOgg();
    }

    @Override
    public void setMovTpOggObj(String movTpOggObj) {
        setMovTpOgg(movTpOggObj);
    }

    @Override
    public String getPolDtEmisDb() {
        return ws.getPoliDb().getVarzTpIasDb();
    }

    @Override
    public void setPolDtEmisDb(String polDtEmisDb) {
        this.ws.getPoliDb().setVarzTpIasDb(polDtEmisDb);
    }

    @Override
    public String getPolDtIniEffDb() {
        return ws.getPoliDb().getEndEffDb();
    }

    @Override
    public void setPolDtIniEffDb(String polDtIniEffDb) {
        this.ws.getPoliDb().setEndEffDb(polDtIniEffDb);
    }

    @Override
    public int getTpMovi() {
        throw new FieldNotMappedException("tpMovi");
    }

    @Override
    public void setTpMovi(int tpMovi) {
        throw new FieldNotMappedException("tpMovi");
    }

    @Override
    public Integer getTpMoviObj() {
        return ((Integer)getTpMovi());
    }

    @Override
    public void setTpMoviObj(Integer tpMoviObj) {
        setTpMovi(((int)tpMoviObj));
    }

    @Override
    public String getWsDataInizioEffettoDb() {
        return ws.getIdsv0010().getWsDataInizioEffettoDb();
    }

    @Override
    public void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb) {
        this.ws.getIdsv0010().setWsDataInizioEffettoDb(wsDataInizioEffettoDb);
    }

    @Override
    public long getWsTsCompetenza() {
        return ws.getIdsv0010().getWsTsCompetenza();
    }

    @Override
    public void setWsTsCompetenza(long wsTsCompetenza) {
        this.ws.getIdsv0010().setWsTsCompetenza(wsTsCompetenza);
    }
}
