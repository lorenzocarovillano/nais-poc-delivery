package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.jdbc.FieldNotMappedException;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.PrestDao;
import it.accenture.jnais.commons.data.to.IPrest;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ldbs6160Data;
import it.accenture.jnais.ws.PrestIdbspre0;
import it.accenture.jnais.ws.redefines.PreDtConcsPrest;
import it.accenture.jnais.ws.redefines.PreDtDecorPrest;
import it.accenture.jnais.ws.redefines.PreDtRimb;
import it.accenture.jnais.ws.redefines.PreFrazPagIntr;
import it.accenture.jnais.ws.redefines.PreIdMoviChiu;
import it.accenture.jnais.ws.redefines.PreImpPrest;
import it.accenture.jnais.ws.redefines.PreImpPrestLiqto;
import it.accenture.jnais.ws.redefines.PreImpRimb;
import it.accenture.jnais.ws.redefines.PreIntrPrest;
import it.accenture.jnais.ws.redefines.PrePrestResEff;
import it.accenture.jnais.ws.redefines.PreRimbEff;
import it.accenture.jnais.ws.redefines.PreSdoIntr;
import it.accenture.jnais.ws.redefines.PreSpePrest;

/**Original name: LDBS6160<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  19 LUG 2010.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO AD HOC PER ACCESSO RISORSE DB        *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Ldbs6160 extends Program implements IPrest {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private PrestDao prestDao = new PrestDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Ldbs6160Data ws = new Ldbs6160Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: PREST
    private PrestIdbspre0 prest;

    //==== METHODS ====
    /**Original name: PROGRAM_LDBS6160_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, PrestIdbspre0 prest) {
        this.idsv0003 = idsv0003;
        this.prest = prest;
        // COB_CODE: PERFORM A000-INIZIO              THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                        a200ElaboraWcEff();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                        b200ElaboraWcCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //               SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                        c200ElaboraWcNst();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Ldbs6160 getInstance() {
        return ((Ldbs6160)Programs.getInstance(Ldbs6160.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'LDBS6160'              TO   IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("LDBS6160");
        // COB_CODE: MOVE 'PREST' TO   IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("PREST");
        // COB_CODE: MOVE '00'                      TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                    TO   IDSV0003-SQLCODE
        //                                               IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                    TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                               IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-WC-EFF<br>*/
    private void a200ElaboraWcEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-WC-EFF          THRU A210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-WC-EFF          THRU A210-EX
            a210SelectWcEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
            a260OpenCursorWcEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
            a270CloseCursorWcEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
            a280FetchFirstWcEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
            a290FetchNextWcEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B200-ELABORA-WC-CPZ<br>*/
    private void b200ElaboraWcCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
            b210SelectWcCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
            b260OpenCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
            b270CloseCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
            b280FetchFirstWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
            b290FetchNextWcCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: C200-ELABORA-WC-NST<br>*/
    private void c200ElaboraWcNst() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM C210-SELECT-WC-NST          THRU C210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM C210-SELECT-WC-NST          THRU C210-EX
            c210SelectWcNst();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
            c260OpenCursorWcNst();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
            c270CloseCursorWcNst();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
            c280FetchFirstWcNst();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
            c290FetchNextWcNst();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A205-DECLARE-CURSOR-WC-EFF<br>
	 * <pre>----
	 * ----  gestione WC Effetto
	 * ----</pre>*/
    private void a205DeclareCursorWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A210-SELECT-WC-EFF<br>*/
    private void a210SelectWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                     ID_PREST
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,DT_CONCS_PREST
        //                    ,DT_DECOR_PREST
        //                    ,IMP_PREST
        //                    ,INTR_PREST
        //                    ,TP_PREST
        //                    ,FRAZ_PAG_INTR
        //                    ,DT_RIMB
        //                    ,IMP_RIMB
        //                    ,COD_DVS
        //                    ,DT_RICH_PREST
        //                    ,MOD_INTR_PREST
        //                    ,SPE_PREST
        //                    ,IMP_PREST_LIQTO
        //                    ,SDO_INTR
        //                    ,RIMB_EFF
        //                    ,PREST_RES_EFF
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //             INTO
        //                :PRE-ID-PREST
        //               ,:PRE-ID-OGG
        //               ,:PRE-TP-OGG
        //               ,:PRE-ID-MOVI-CRZ
        //               ,:PRE-ID-MOVI-CHIU
        //                :IND-PRE-ID-MOVI-CHIU
        //               ,:PRE-DT-INI-EFF-DB
        //               ,:PRE-DT-END-EFF-DB
        //               ,:PRE-COD-COMP-ANIA
        //               ,:PRE-DT-CONCS-PREST-DB
        //                :IND-PRE-DT-CONCS-PREST
        //               ,:PRE-DT-DECOR-PREST-DB
        //                :IND-PRE-DT-DECOR-PREST
        //               ,:PRE-IMP-PREST
        //                :IND-PRE-IMP-PREST
        //               ,:PRE-INTR-PREST
        //                :IND-PRE-INTR-PREST
        //               ,:PRE-TP-PREST
        //                :IND-PRE-TP-PREST
        //               ,:PRE-FRAZ-PAG-INTR
        //                :IND-PRE-FRAZ-PAG-INTR
        //               ,:PRE-DT-RIMB-DB
        //                :IND-PRE-DT-RIMB
        //               ,:PRE-IMP-RIMB
        //                :IND-PRE-IMP-RIMB
        //               ,:PRE-COD-DVS
        //                :IND-PRE-COD-DVS
        //               ,:PRE-DT-RICH-PREST-DB
        //               ,:PRE-MOD-INTR-PREST
        //                :IND-PRE-MOD-INTR-PREST
        //               ,:PRE-SPE-PREST
        //                :IND-PRE-SPE-PREST
        //               ,:PRE-IMP-PREST-LIQTO
        //                :IND-PRE-IMP-PREST-LIQTO
        //               ,:PRE-SDO-INTR
        //                :IND-PRE-SDO-INTR
        //               ,:PRE-RIMB-EFF
        //                :IND-PRE-RIMB-EFF
        //               ,:PRE-PREST-RES-EFF
        //                :IND-PRE-PREST-RES-EFF
        //               ,:PRE-DS-RIGA
        //               ,:PRE-DS-OPER-SQL
        //               ,:PRE-DS-VER
        //               ,:PRE-DS-TS-INI-CPTZ
        //               ,:PRE-DS-TS-END-CPTZ
        //               ,:PRE-DS-UTENTE
        //               ,:PRE-DS-STATO-ELAB
        //             FROM PREST
        //             WHERE  ID_OGG        = :PRE-ID-OGG
        //                    AND  TP_OGG   = :PRE-TP-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //              ORDER BY DT_DECOR_PREST ASC
        //              FETCH FIRST ROW ONLY
        //           END-EXEC.
        prestDao.selectRec6(prest.getPreIdOgg(), prest.getPreTpOgg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: A260-OPEN-CURSOR-WC-EFF<br>*/
    private void a260OpenCursorWcEff() {
        // COB_CODE: PERFORM A205-DECLARE-CURSOR-WC-EFF THRU A205-EX.
        a205DeclareCursorWcEff();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A270-CLOSE-CURSOR-WC-EFF<br>*/
    private void a270CloseCursorWcEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A280-FETCH-FIRST-WC-EFF<br>*/
    private void a280FetchFirstWcEff() {
        // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF    THRU A260-EX.
        a260OpenCursorWcEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
            a290FetchNextWcEff();
        }
    }

    /**Original name: A290-FETCH-NEXT-WC-EFF<br>*/
    private void a290FetchNextWcEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B205-DECLARE-CURSOR-WC-CPZ<br>
	 * <pre>----
	 * ----  gestione WC Competenza
	 * ----</pre>*/
    private void b205DeclareCursorWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B210-SELECT-WC-CPZ<br>*/
    private void b210SelectWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                     ID_PREST
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,DT_CONCS_PREST
        //                    ,DT_DECOR_PREST
        //                    ,IMP_PREST
        //                    ,INTR_PREST
        //                    ,TP_PREST
        //                    ,FRAZ_PAG_INTR
        //                    ,DT_RIMB
        //                    ,IMP_RIMB
        //                    ,COD_DVS
        //                    ,DT_RICH_PREST
        //                    ,MOD_INTR_PREST
        //                    ,SPE_PREST
        //                    ,IMP_PREST_LIQTO
        //                    ,SDO_INTR
        //                    ,RIMB_EFF
        //                    ,PREST_RES_EFF
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //             INTO
        //                :PRE-ID-PREST
        //               ,:PRE-ID-OGG
        //               ,:PRE-TP-OGG
        //               ,:PRE-ID-MOVI-CRZ
        //               ,:PRE-ID-MOVI-CHIU
        //                :IND-PRE-ID-MOVI-CHIU
        //               ,:PRE-DT-INI-EFF-DB
        //               ,:PRE-DT-END-EFF-DB
        //               ,:PRE-COD-COMP-ANIA
        //               ,:PRE-DT-CONCS-PREST-DB
        //                :IND-PRE-DT-CONCS-PREST
        //               ,:PRE-DT-DECOR-PREST-DB
        //                :IND-PRE-DT-DECOR-PREST
        //               ,:PRE-IMP-PREST
        //                :IND-PRE-IMP-PREST
        //               ,:PRE-INTR-PREST
        //                :IND-PRE-INTR-PREST
        //               ,:PRE-TP-PREST
        //                :IND-PRE-TP-PREST
        //               ,:PRE-FRAZ-PAG-INTR
        //                :IND-PRE-FRAZ-PAG-INTR
        //               ,:PRE-DT-RIMB-DB
        //                :IND-PRE-DT-RIMB
        //               ,:PRE-IMP-RIMB
        //                :IND-PRE-IMP-RIMB
        //               ,:PRE-COD-DVS
        //                :IND-PRE-COD-DVS
        //               ,:PRE-DT-RICH-PREST-DB
        //               ,:PRE-MOD-INTR-PREST
        //                :IND-PRE-MOD-INTR-PREST
        //               ,:PRE-SPE-PREST
        //                :IND-PRE-SPE-PREST
        //               ,:PRE-IMP-PREST-LIQTO
        //                :IND-PRE-IMP-PREST-LIQTO
        //               ,:PRE-SDO-INTR
        //                :IND-PRE-SDO-INTR
        //               ,:PRE-RIMB-EFF
        //                :IND-PRE-RIMB-EFF
        //               ,:PRE-PREST-RES-EFF
        //                :IND-PRE-PREST-RES-EFF
        //               ,:PRE-DS-RIGA
        //               ,:PRE-DS-OPER-SQL
        //               ,:PRE-DS-VER
        //               ,:PRE-DS-TS-INI-CPTZ
        //               ,:PRE-DS-TS-END-CPTZ
        //               ,:PRE-DS-UTENTE
        //               ,:PRE-DS-STATO-ELAB
        //             FROM PREST
        //             WHERE  ID_OGG        = :PRE-ID-OGG
        //                    AND  TP_OGG   = :PRE-TP-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //              ORDER BY DT_DECOR_PREST ASC
        //              FETCH FIRST ROW ONLY
        //           END-EXEC.
        prestDao.selectRec7(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: B260-OPEN-CURSOR-WC-CPZ<br>*/
    private void b260OpenCursorWcCpz() {
        // COB_CODE: PERFORM B205-DECLARE-CURSOR-WC-CPZ THRU B205-EX.
        b205DeclareCursorWcCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B270-CLOSE-CURSOR-WC-CPZ<br>*/
    private void b270CloseCursorWcCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B280-FETCH-FIRST-WC-CPZ<br>*/
    private void b280FetchFirstWcCpz() {
        // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ    THRU B260-EX.
        b260OpenCursorWcCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
            b290FetchNextWcCpz();
        }
    }

    /**Original name: B290-FETCH-NEXT-WC-CPZ<br>*/
    private void b290FetchNextWcCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C205-DECLARE-CURSOR-WC-NST<br>
	 * <pre>----
	 * ----  gestione WC Senza Storicità
	 * ----</pre>*/
    private void c205DeclareCursorWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C210-SELECT-WC-NST<br>*/
    private void c210SelectWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C260-OPEN-CURSOR-WC-NST<br>*/
    private void c260OpenCursorWcNst() {
        // COB_CODE: PERFORM C205-DECLARE-CURSOR-WC-NST THRU C205-EX.
        c205DeclareCursorWcNst();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C270-CLOSE-CURSOR-WC-NST<br>*/
    private void c270CloseCursorWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C280-FETCH-FIRST-WC-NST<br>*/
    private void c280FetchFirstWcNst() {
        // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST    THRU C260-EX.
        c260OpenCursorWcNst();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
            c290FetchNextWcNst();
        }
    }

    /**Original name: C290-FETCH-NEXT-WC-NST<br>*/
    private void c290FetchNextWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>----
	 * ----  utilità comuni a tutti i livelli operazione
	 * ----</pre>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-PRE-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO PRE-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndPrest().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PRE-ID-MOVI-CHIU-NULL
            prest.getPreIdMoviChiu().setPreIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PreIdMoviChiu.Len.PRE_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-PRE-DT-CONCS-PREST = -1
        //              MOVE HIGH-VALUES TO PRE-DT-CONCS-PREST-NULL
        //           END-IF
        if (ws.getIndPrest().getDtConcsPrest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PRE-DT-CONCS-PREST-NULL
            prest.getPreDtConcsPrest().setPreDtConcsPrestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PreDtConcsPrest.Len.PRE_DT_CONCS_PREST_NULL));
        }
        // COB_CODE: IF IND-PRE-DT-DECOR-PREST = -1
        //              MOVE HIGH-VALUES TO PRE-DT-DECOR-PREST-NULL
        //           END-IF
        if (ws.getIndPrest().getDtDecorPrest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PRE-DT-DECOR-PREST-NULL
            prest.getPreDtDecorPrest().setPreDtDecorPrestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PreDtDecorPrest.Len.PRE_DT_DECOR_PREST_NULL));
        }
        // COB_CODE: IF IND-PRE-IMP-PREST = -1
        //              MOVE HIGH-VALUES TO PRE-IMP-PREST-NULL
        //           END-IF
        if (ws.getIndPrest().getImpPrest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PRE-IMP-PREST-NULL
            prest.getPreImpPrest().setPreImpPrestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PreImpPrest.Len.PRE_IMP_PREST_NULL));
        }
        // COB_CODE: IF IND-PRE-INTR-PREST = -1
        //              MOVE HIGH-VALUES TO PRE-INTR-PREST-NULL
        //           END-IF
        if (ws.getIndPrest().getIntrPrest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PRE-INTR-PREST-NULL
            prest.getPreIntrPrest().setPreIntrPrestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PreIntrPrest.Len.PRE_INTR_PREST_NULL));
        }
        // COB_CODE: IF IND-PRE-TP-PREST = -1
        //              MOVE HIGH-VALUES TO PRE-TP-PREST-NULL
        //           END-IF
        if (ws.getIndPrest().getTpPrest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PRE-TP-PREST-NULL
            prest.setPreTpPrest(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PrestIdbspre0.Len.PRE_TP_PREST));
        }
        // COB_CODE: IF IND-PRE-FRAZ-PAG-INTR = -1
        //              MOVE HIGH-VALUES TO PRE-FRAZ-PAG-INTR-NULL
        //           END-IF
        if (ws.getIndPrest().getFrazPagIntr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PRE-FRAZ-PAG-INTR-NULL
            prest.getPreFrazPagIntr().setPreFrazPagIntrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PreFrazPagIntr.Len.PRE_FRAZ_PAG_INTR_NULL));
        }
        // COB_CODE: IF IND-PRE-DT-RIMB = -1
        //              MOVE HIGH-VALUES TO PRE-DT-RIMB-NULL
        //           END-IF
        if (ws.getIndPrest().getDtRimb() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PRE-DT-RIMB-NULL
            prest.getPreDtRimb().setPreDtRimbNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PreDtRimb.Len.PRE_DT_RIMB_NULL));
        }
        // COB_CODE: IF IND-PRE-IMP-RIMB = -1
        //              MOVE HIGH-VALUES TO PRE-IMP-RIMB-NULL
        //           END-IF
        if (ws.getIndPrest().getImpRimb() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PRE-IMP-RIMB-NULL
            prest.getPreImpRimb().setPreImpRimbNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PreImpRimb.Len.PRE_IMP_RIMB_NULL));
        }
        // COB_CODE: IF IND-PRE-COD-DVS = -1
        //              MOVE HIGH-VALUES TO PRE-COD-DVS-NULL
        //           END-IF
        if (ws.getIndPrest().getCodDvs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PRE-COD-DVS-NULL
            prest.setPreCodDvs(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PrestIdbspre0.Len.PRE_COD_DVS));
        }
        // COB_CODE: IF IND-PRE-MOD-INTR-PREST = -1
        //              MOVE HIGH-VALUES TO PRE-MOD-INTR-PREST-NULL
        //           END-IF
        if (ws.getIndPrest().getModIntrPrest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PRE-MOD-INTR-PREST-NULL
            prest.setPreModIntrPrest(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PrestIdbspre0.Len.PRE_MOD_INTR_PREST));
        }
        // COB_CODE: IF IND-PRE-SPE-PREST = -1
        //              MOVE HIGH-VALUES TO PRE-SPE-PREST-NULL
        //           END-IF
        if (ws.getIndPrest().getSpePrest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PRE-SPE-PREST-NULL
            prest.getPreSpePrest().setPreSpePrestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PreSpePrest.Len.PRE_SPE_PREST_NULL));
        }
        // COB_CODE: IF IND-PRE-IMP-PREST-LIQTO = -1
        //              MOVE HIGH-VALUES TO PRE-IMP-PREST-LIQTO-NULL
        //           END-IF
        if (ws.getIndPrest().getImpPrestLiqto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PRE-IMP-PREST-LIQTO-NULL
            prest.getPreImpPrestLiqto().setPreImpPrestLiqtoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PreImpPrestLiqto.Len.PRE_IMP_PREST_LIQTO_NULL));
        }
        // COB_CODE: IF IND-PRE-SDO-INTR = -1
        //              MOVE HIGH-VALUES TO PRE-SDO-INTR-NULL
        //           END-IF
        if (ws.getIndPrest().getSdoIntr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PRE-SDO-INTR-NULL
            prest.getPreSdoIntr().setPreSdoIntrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PreSdoIntr.Len.PRE_SDO_INTR_NULL));
        }
        // COB_CODE: IF IND-PRE-RIMB-EFF = -1
        //              MOVE HIGH-VALUES TO PRE-RIMB-EFF-NULL
        //           END-IF
        if (ws.getIndPrest().getRimbEff() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PRE-RIMB-EFF-NULL
            prest.getPreRimbEff().setPreRimbEffNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PreRimbEff.Len.PRE_RIMB_EFF_NULL));
        }
        // COB_CODE: IF IND-PRE-PREST-RES-EFF = -1
        //              MOVE HIGH-VALUES TO PRE-PREST-RES-EFF-NULL
        //           END-IF.
        if (ws.getIndPrest().getPrestResEff() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PRE-PREST-RES-EFF-NULL
            prest.getPrePrestResEff().setPrePrestResEffNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PrePrestResEff.Len.PRE_PREST_RES_EFF_NULL));
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE PRE-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getPrestDb().getIniEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO PRE-DT-INI-EFF
        prest.setPreDtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE PRE-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getPrestDb().getEndEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO PRE-DT-END-EFF
        prest.setPreDtEndEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-PRE-DT-CONCS-PREST = 0
        //               MOVE WS-DATE-N      TO PRE-DT-CONCS-PREST
        //           END-IF
        if (ws.getIndPrest().getDtConcsPrest() == 0) {
            // COB_CODE: MOVE PRE-DT-CONCS-PREST-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getPrestDb().getConcsPrestDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PRE-DT-CONCS-PREST
            prest.getPreDtConcsPrest().setPreDtConcsPrest(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PRE-DT-DECOR-PREST = 0
        //               MOVE WS-DATE-N      TO PRE-DT-DECOR-PREST
        //           END-IF
        if (ws.getIndPrest().getDtDecorPrest() == 0) {
            // COB_CODE: MOVE PRE-DT-DECOR-PREST-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getPrestDb().getDecorPrestDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PRE-DT-DECOR-PREST
            prest.getPreDtDecorPrest().setPreDtDecorPrest(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PRE-DT-RIMB = 0
        //               MOVE WS-DATE-N      TO PRE-DT-RIMB
        //           END-IF
        if (ws.getIndPrest().getDtRimb() == 0) {
            // COB_CODE: MOVE PRE-DT-RIMB-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getPrestDb().getRimbDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PRE-DT-RIMB
            prest.getPreDtRimb().setPreDtRimb(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: MOVE PRE-DT-RICH-PREST-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getPrestDb().getRichPrestDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO PRE-DT-RICH-PREST.
        prest.setPreDtRichPrest(ws.getIdsv0010().getWsDateN());
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z970-CODICE-ADHOC-PRE<br>
	 * <pre>----
	 * ----  prevede statements AD HOC PRE Query
	 * ----</pre>*/
    private void z970CodiceAdhocPre() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z980-CODICE-ADHOC-POST<br>
	 * <pre>----
	 * ----  prevede statements AD HOC POST Query
	 * ----</pre>*/
    private void z980CodiceAdhocPost() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public int getCodCompAnia() {
        return prest.getPreCodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.prest.setPreCodCompAnia(codCompAnia);
    }

    @Override
    public String getCodDvs() {
        return prest.getPreCodDvs();
    }

    @Override
    public void setCodDvs(String codDvs) {
        this.prest.setPreCodDvs(codDvs);
    }

    @Override
    public String getCodDvsObj() {
        if (ws.getIndPrest().getCodDvs() >= 0) {
            return getCodDvs();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodDvsObj(String codDvsObj) {
        if (codDvsObj != null) {
            setCodDvs(codDvsObj);
            ws.getIndPrest().setCodDvs(((short)0));
        }
        else {
            ws.getIndPrest().setCodDvs(((short)-1));
        }
    }

    @Override
    public char getDsOperSql() {
        return prest.getPreDsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.prest.setPreDsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return prest.getPreDsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.prest.setPreDsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsEndCptz() {
        return prest.getPreDsTsEndCptz();
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.prest.setPreDsTsEndCptz(dsTsEndCptz);
    }

    @Override
    public long getDsTsIniCptz() {
        return prest.getPreDsTsIniCptz();
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.prest.setPreDsTsIniCptz(dsTsIniCptz);
    }

    @Override
    public String getDsUtente() {
        return prest.getPreDsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.prest.setPreDsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return prest.getPreDsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.prest.setPreDsVer(dsVer);
    }

    @Override
    public String getDtConcsPrestDb() {
        return ws.getPrestDb().getConcsPrestDb();
    }

    @Override
    public void setDtConcsPrestDb(String dtConcsPrestDb) {
        this.ws.getPrestDb().setConcsPrestDb(dtConcsPrestDb);
    }

    @Override
    public String getDtConcsPrestDbObj() {
        if (ws.getIndPrest().getDtConcsPrest() >= 0) {
            return getDtConcsPrestDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtConcsPrestDbObj(String dtConcsPrestDbObj) {
        if (dtConcsPrestDbObj != null) {
            setDtConcsPrestDb(dtConcsPrestDbObj);
            ws.getIndPrest().setDtConcsPrest(((short)0));
        }
        else {
            ws.getIndPrest().setDtConcsPrest(((short)-1));
        }
    }

    @Override
    public String getDtDecorPrestDb() {
        return ws.getPrestDb().getDecorPrestDb();
    }

    @Override
    public void setDtDecorPrestDb(String dtDecorPrestDb) {
        this.ws.getPrestDb().setDecorPrestDb(dtDecorPrestDb);
    }

    @Override
    public String getDtDecorPrestDbObj() {
        if (ws.getIndPrest().getDtDecorPrest() >= 0) {
            return getDtDecorPrestDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtDecorPrestDbObj(String dtDecorPrestDbObj) {
        if (dtDecorPrestDbObj != null) {
            setDtDecorPrestDb(dtDecorPrestDbObj);
            ws.getIndPrest().setDtDecorPrest(((short)0));
        }
        else {
            ws.getIndPrest().setDtDecorPrest(((short)-1));
        }
    }

    @Override
    public String getDtEndEffDb() {
        return ws.getPrestDb().getEndEffDb();
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        this.ws.getPrestDb().setEndEffDb(dtEndEffDb);
    }

    @Override
    public String getDtIniEffDb() {
        return ws.getPrestDb().getIniEffDb();
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        this.ws.getPrestDb().setIniEffDb(dtIniEffDb);
    }

    @Override
    public String getDtRichPrestDb() {
        return ws.getPrestDb().getRichPrestDb();
    }

    @Override
    public void setDtRichPrestDb(String dtRichPrestDb) {
        this.ws.getPrestDb().setRichPrestDb(dtRichPrestDb);
    }

    @Override
    public String getDtRimbDb() {
        return ws.getPrestDb().getRimbDb();
    }

    @Override
    public void setDtRimbDb(String dtRimbDb) {
        this.ws.getPrestDb().setRimbDb(dtRimbDb);
    }

    @Override
    public String getDtRimbDbObj() {
        if (ws.getIndPrest().getDtRimb() >= 0) {
            return getDtRimbDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtRimbDbObj(String dtRimbDbObj) {
        if (dtRimbDbObj != null) {
            setDtRimbDb(dtRimbDbObj);
            ws.getIndPrest().setDtRimb(((short)0));
        }
        else {
            ws.getIndPrest().setDtRimb(((short)-1));
        }
    }

    @Override
    public int getFrazPagIntr() {
        return prest.getPreFrazPagIntr().getPreFrazPagIntr();
    }

    @Override
    public void setFrazPagIntr(int frazPagIntr) {
        this.prest.getPreFrazPagIntr().setPreFrazPagIntr(frazPagIntr);
    }

    @Override
    public Integer getFrazPagIntrObj() {
        if (ws.getIndPrest().getFrazPagIntr() >= 0) {
            return ((Integer)getFrazPagIntr());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFrazPagIntrObj(Integer frazPagIntrObj) {
        if (frazPagIntrObj != null) {
            setFrazPagIntr(((int)frazPagIntrObj));
            ws.getIndPrest().setFrazPagIntr(((short)0));
        }
        else {
            ws.getIndPrest().setFrazPagIntr(((short)-1));
        }
    }

    @Override
    public int getIdMoviChiu() {
        return prest.getPreIdMoviChiu().getPreIdMoviChiu();
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        this.prest.getPreIdMoviChiu().setPreIdMoviChiu(idMoviChiu);
    }

    @Override
    public Integer getIdMoviChiuObj() {
        if (ws.getIndPrest().getIdMoviChiu() >= 0) {
            return ((Integer)getIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        if (idMoviChiuObj != null) {
            setIdMoviChiu(((int)idMoviChiuObj));
            ws.getIndPrest().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndPrest().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getIdMoviCrz() {
        return prest.getPreIdMoviCrz();
    }

    @Override
    public void setIdMoviCrz(int idMoviCrz) {
        this.prest.setPreIdMoviCrz(idMoviCrz);
    }

    @Override
    public int getIdPrest() {
        return prest.getPreIdPrest();
    }

    @Override
    public void setIdPrest(int idPrest) {
        this.prest.setPreIdPrest(idPrest);
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        return idsv0003.getCodiceCompagniaAnia();
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        this.idsv0003.setCodiceCompagniaAnia(idsv0003CodiceCompagniaAnia);
    }

    @Override
    public AfDecimal getImpPrest() {
        return prest.getPreImpPrest().getPreImpPrest();
    }

    @Override
    public void setImpPrest(AfDecimal impPrest) {
        this.prest.getPreImpPrest().setPreImpPrest(impPrest.copy());
    }

    @Override
    public AfDecimal getImpPrestLiqto() {
        return prest.getPreImpPrestLiqto().getPreImpPrestLiqto();
    }

    @Override
    public void setImpPrestLiqto(AfDecimal impPrestLiqto) {
        this.prest.getPreImpPrestLiqto().setPreImpPrestLiqto(impPrestLiqto.copy());
    }

    @Override
    public AfDecimal getImpPrestLiqtoObj() {
        if (ws.getIndPrest().getImpPrestLiqto() >= 0) {
            return getImpPrestLiqto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpPrestLiqtoObj(AfDecimal impPrestLiqtoObj) {
        if (impPrestLiqtoObj != null) {
            setImpPrestLiqto(new AfDecimal(impPrestLiqtoObj, 15, 3));
            ws.getIndPrest().setImpPrestLiqto(((short)0));
        }
        else {
            ws.getIndPrest().setImpPrestLiqto(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpPrestObj() {
        if (ws.getIndPrest().getImpPrest() >= 0) {
            return getImpPrest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpPrestObj(AfDecimal impPrestObj) {
        if (impPrestObj != null) {
            setImpPrest(new AfDecimal(impPrestObj, 15, 3));
            ws.getIndPrest().setImpPrest(((short)0));
        }
        else {
            ws.getIndPrest().setImpPrest(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpRimb() {
        return prest.getPreImpRimb().getPreImpRimb();
    }

    @Override
    public void setImpRimb(AfDecimal impRimb) {
        this.prest.getPreImpRimb().setPreImpRimb(impRimb.copy());
    }

    @Override
    public AfDecimal getImpRimbObj() {
        if (ws.getIndPrest().getImpRimb() >= 0) {
            return getImpRimb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpRimbObj(AfDecimal impRimbObj) {
        if (impRimbObj != null) {
            setImpRimb(new AfDecimal(impRimbObj, 15, 3));
            ws.getIndPrest().setImpRimb(((short)0));
        }
        else {
            ws.getIndPrest().setImpRimb(((short)-1));
        }
    }

    @Override
    public AfDecimal getIntrPrest() {
        return prest.getPreIntrPrest().getPreIntrPrest();
    }

    @Override
    public void setIntrPrest(AfDecimal intrPrest) {
        this.prest.getPreIntrPrest().setPreIntrPrest(intrPrest.copy());
    }

    @Override
    public AfDecimal getIntrPrestObj() {
        if (ws.getIndPrest().getIntrPrest() >= 0) {
            return getIntrPrest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIntrPrestObj(AfDecimal intrPrestObj) {
        if (intrPrestObj != null) {
            setIntrPrest(new AfDecimal(intrPrestObj, 6, 3));
            ws.getIndPrest().setIntrPrest(((short)0));
        }
        else {
            ws.getIndPrest().setIntrPrest(((short)-1));
        }
    }

    @Override
    public int getLdbv2821IdOgg() {
        throw new FieldNotMappedException("ldbv2821IdOgg");
    }

    @Override
    public void setLdbv2821IdOgg(int ldbv2821IdOgg) {
        throw new FieldNotMappedException("ldbv2821IdOgg");
    }

    @Override
    public AfDecimal getLdbv2821ImpPrest() {
        throw new FieldNotMappedException("ldbv2821ImpPrest");
    }

    @Override
    public void setLdbv2821ImpPrest(AfDecimal ldbv2821ImpPrest) {
        throw new FieldNotMappedException("ldbv2821ImpPrest");
    }

    @Override
    public AfDecimal getLdbv2821ImpPrestObj() {
        return getLdbv2821ImpPrest();
    }

    @Override
    public void setLdbv2821ImpPrestObj(AfDecimal ldbv2821ImpPrestObj) {
        setLdbv2821ImpPrest(new AfDecimal(ldbv2821ImpPrestObj, 15, 3));
    }

    @Override
    public String getLdbv2821TpOgg() {
        throw new FieldNotMappedException("ldbv2821TpOgg");
    }

    @Override
    public void setLdbv2821TpOgg(String ldbv2821TpOgg) {
        throw new FieldNotMappedException("ldbv2821TpOgg");
    }

    @Override
    public String getModIntrPrest() {
        return prest.getPreModIntrPrest();
    }

    @Override
    public void setModIntrPrest(String modIntrPrest) {
        this.prest.setPreModIntrPrest(modIntrPrest);
    }

    @Override
    public String getModIntrPrestObj() {
        if (ws.getIndPrest().getModIntrPrest() >= 0) {
            return getModIntrPrest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setModIntrPrestObj(String modIntrPrestObj) {
        if (modIntrPrestObj != null) {
            setModIntrPrest(modIntrPrestObj);
            ws.getIndPrest().setModIntrPrest(((short)0));
        }
        else {
            ws.getIndPrest().setModIntrPrest(((short)-1));
        }
    }

    @Override
    public long getPreDsRiga() {
        return prest.getPreDsRiga();
    }

    @Override
    public void setPreDsRiga(long preDsRiga) {
        this.prest.setPreDsRiga(preDsRiga);
    }

    @Override
    public int getPreIdOgg() {
        return prest.getPreIdOgg();
    }

    @Override
    public void setPreIdOgg(int preIdOgg) {
        this.prest.setPreIdOgg(preIdOgg);
    }

    @Override
    public String getPreTpOgg() {
        return prest.getPreTpOgg();
    }

    @Override
    public void setPreTpOgg(String preTpOgg) {
        this.prest.setPreTpOgg(preTpOgg);
    }

    @Override
    public AfDecimal getPrestResEff() {
        return prest.getPrePrestResEff().getPrePrestResEff();
    }

    @Override
    public void setPrestResEff(AfDecimal prestResEff) {
        this.prest.getPrePrestResEff().setPrePrestResEff(prestResEff.copy());
    }

    @Override
    public AfDecimal getPrestResEffObj() {
        if (ws.getIndPrest().getPrestResEff() >= 0) {
            return getPrestResEff();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPrestResEffObj(AfDecimal prestResEffObj) {
        if (prestResEffObj != null) {
            setPrestResEff(new AfDecimal(prestResEffObj, 15, 3));
            ws.getIndPrest().setPrestResEff(((short)0));
        }
        else {
            ws.getIndPrest().setPrestResEff(((short)-1));
        }
    }

    @Override
    public AfDecimal getRimbEff() {
        return prest.getPreRimbEff().getPreRimbEff();
    }

    @Override
    public void setRimbEff(AfDecimal rimbEff) {
        this.prest.getPreRimbEff().setPreRimbEff(rimbEff.copy());
    }

    @Override
    public AfDecimal getRimbEffObj() {
        if (ws.getIndPrest().getRimbEff() >= 0) {
            return getRimbEff();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRimbEffObj(AfDecimal rimbEffObj) {
        if (rimbEffObj != null) {
            setRimbEff(new AfDecimal(rimbEffObj, 15, 3));
            ws.getIndPrest().setRimbEff(((short)0));
        }
        else {
            ws.getIndPrest().setRimbEff(((short)-1));
        }
    }

    @Override
    public AfDecimal getSdoIntr() {
        return prest.getPreSdoIntr().getPreSdoIntr();
    }

    @Override
    public void setSdoIntr(AfDecimal sdoIntr) {
        this.prest.getPreSdoIntr().setPreSdoIntr(sdoIntr.copy());
    }

    @Override
    public AfDecimal getSdoIntrObj() {
        if (ws.getIndPrest().getSdoIntr() >= 0) {
            return getSdoIntr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setSdoIntrObj(AfDecimal sdoIntrObj) {
        if (sdoIntrObj != null) {
            setSdoIntr(new AfDecimal(sdoIntrObj, 15, 3));
            ws.getIndPrest().setSdoIntr(((short)0));
        }
        else {
            ws.getIndPrest().setSdoIntr(((short)-1));
        }
    }

    @Override
    public AfDecimal getSpePrest() {
        return prest.getPreSpePrest().getPreSpePrest();
    }

    @Override
    public void setSpePrest(AfDecimal spePrest) {
        this.prest.getPreSpePrest().setPreSpePrest(spePrest.copy());
    }

    @Override
    public AfDecimal getSpePrestObj() {
        if (ws.getIndPrest().getSpePrest() >= 0) {
            return getSpePrest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setSpePrestObj(AfDecimal spePrestObj) {
        if (spePrestObj != null) {
            setSpePrest(new AfDecimal(spePrestObj, 15, 3));
            ws.getIndPrest().setSpePrest(((short)0));
        }
        else {
            ws.getIndPrest().setSpePrest(((short)-1));
        }
    }

    @Override
    public String getTpPrest() {
        return prest.getPreTpPrest();
    }

    @Override
    public void setTpPrest(String tpPrest) {
        this.prest.setPreTpPrest(tpPrest);
    }

    @Override
    public String getTpPrestObj() {
        if (ws.getIndPrest().getTpPrest() >= 0) {
            return getTpPrest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpPrestObj(String tpPrestObj) {
        if (tpPrestObj != null) {
            setTpPrest(tpPrestObj);
            ws.getIndPrest().setTpPrest(((short)0));
        }
        else {
            ws.getIndPrest().setTpPrest(((short)-1));
        }
    }

    @Override
    public String getWsDataInizioEffettoDb() {
        return ws.getIdsv0010().getWsDataInizioEffettoDb();
    }

    @Override
    public void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb) {
        this.ws.getIdsv0010().setWsDataInizioEffettoDb(wsDataInizioEffettoDb);
    }

    @Override
    public long getWsTsCompetenza() {
        return ws.getIdsv0010().getWsTsCompetenza();
    }

    @Override
    public void setWsTsCompetenza(long wsTsCompetenza) {
        this.ws.getIdsv0010().setWsTsCompetenza(wsTsCompetenza);
    }
}
