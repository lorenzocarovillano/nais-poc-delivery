package it.accenture.jnais;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.Lccs0090Data;
import it.accenture.jnais.ws.LinkArea;

/**Original name: LCCS0090<br>
 * <pre>*****************************************************************
 * *                                                        ********
 * *    PORTAFOGLIO VITA ITALIA  VER 1.0                    ********
 * *                                                        ********
 * *****************************************************************
 * AUTHOR.             AISS.
 * DATE-WRITTEN.       7 NOV 2006.
 * DATE-COMPILED.
 * ----------------------------------------------------------------*
 *     PROGRAMMA ..... LCCS0090
 *     TIPOLOGIA...... SERVIZIO TRASVERSALE
 *     DESCRIZIONE.... ESTRAZIONE SEQUENCE TABELLA DB2
 * ----------------------------------------------------------------*</pre>*/
public class Lccs0090 extends Program {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    //Original name: WORKING-STORAGE
    private Lccs0090Data ws = new Lccs0090Data();
    //Original name: LINK-AREA
    private LinkArea linkArea;

    //==== METHODS ====
    /**Original name: PROGRAM_LCCS0090_FIRST_SENTENCES<br>
	 * <pre>---------------------------------------------------------------*</pre>*/
    public long execute(LinkArea linkArea) {
        this.linkArea = linkArea;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: IF WCOM-RETURN-CODE = '00'
        //              PERFORM S1000-ELABORAZIONE     THRU EX-S1000
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getReturnCode().getReturnCode(), "00")) {
            // COB_CODE: PERFORM S1000-ELABORAZIONE     THRU EX-S1000
            s1000Elaborazione();
        }
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI   THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lccs0090 getInstance() {
        return ((Lccs0090)Programs.getInstance(Lccs0090.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *  OPERAZIONI INIZIALI                                            *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: MOVE LINK-AREA                    TO WORK-AREA.
        ws.setWorkAreaBytes(linkArea.getLinkAreaBytes());
        // COB_CODE: MOVE '00'                         TO WCOM-RETURN-CODE.
        ws.getLccc0090().getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                       TO WCOM-SEQ-TABELLA
        //                                                WCOM-SQLCODE.
        ws.getLccc0090().setSeqTabella(0);
        ws.getLccc0090().getSqlcode().setSqlcodeSigned(0);
        // COB_CODE: MOVE SPACES                       TO WCOM-DESCRIZ-ERR-DB2.
        ws.getLccc0090().setDescrizErrDb2("");
        // COB_CODE: PERFORM S0005-CTRL-DATI-INPUT     THRU EX-S0005.
        s0005CtrlDatiInput();
    }

    /**Original name: S0005-CTRL-DATI-INPUT<br>
	 * <pre>----------------------------------------------------------------*
	 *     CONTROLLO DATI INPUT
	 * ----------------------------------------------------------------*</pre>*/
    private void s0005CtrlDatiInput() {
        // COB_CODE: MOVE WCOM-NOME-TABELLA  TO WS-CTRL-TABELLA
        //                                    WS-CTRL-TABELLA-CLIENT.
        ws.setWsCtrlTabella(ws.getLccc0090().getNomeTabella());
        ws.setWsCtrlTabellaClient(ws.getLccc0090().getNomeTabella());
        // COB_CODE: IF NOT TAB-VALIDA AND NOT TAB-CLIENT-VALIDA
        //              MOVE 'NOME SEQUENCE NON VALIDO' TO WCOM-DESCRIZ-ERR-DB2
        //           END-IF.
        if (!ws.isTabValida() && !ws.isTabClientValida()) {
            // COB_CODE: MOVE 'S1'                       TO WCOM-RETURN-CODE
            ws.getLccc0090().getReturnCode().setReturnCode("S1");
            // COB_CODE: MOVE 'NOME SEQUENCE NON VALIDO' TO WCOM-DESCRIZ-ERR-DB2
            ws.getLccc0090().setDescrizErrDb2("NOME SEQUENCE NON VALIDO");
        }
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: PERFORM S1100-ESTRAZIONE-SEQ       THRU EX-S1100.
        s1100EstrazioneSeq();
    }

    /**Original name: S1100-ESTRAZIONE-SEQ<br>
	 * <pre>----------------------------------------------------------------*
	 *     ESTRAZIONE SEQUENCE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1100EstrazioneSeq() {
        // COB_CODE: IF WCOM-NOME-TABELLA = 'ADES'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "ADES")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_ADES
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'ACC-COMM'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "ACC-COMM")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_ACC_COMM
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'ANA-SOPR-AUT'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "ANA-SOPR-AUT")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_ANA_SOPR_AUT
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'ANTIRICIC'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "ANTIRICIC")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_ANTIRICIC
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'ASSICURATI'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "ASSICURATI")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_ASSICURATI
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'AST-ALLOC'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "AST-ALLOC")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_AST_ALLOC
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'ATT-SERV-VAL'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "ATT-SERV-VAL")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_ATT_SERV_VAL
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'D-ATT-SERV-VAL'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "D-ATT-SERV-VAL")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_D_ATT_SERV_VAL
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'BNFIC'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "BNFIC")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_BNFIC
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'BILA-FND-CALC'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "BILA-FND-CALC")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_BILA_FND_CALC
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'BILA-FND-ESTR'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "BILA-FND-ESTR")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_BILA_FND_ESTR
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'BILA-TRCH-CALC'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "BILA-TRCH-CALC")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_BILA_TRCH_CALC
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'BILA-TRCH-ESTR'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "BILA-TRCH-ESTR")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_BILA_TRCH_ESTR
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'BILA-TRCH-SOM-P'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "BILA-TRCH-SOM-P")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_BILA_TRCH_SOM_P
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'BILA-VAR-CALC-P'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "BILA-VAR-CALC-P")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_BILA_VAR_CALC_P
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'BILA-VAR-CALC-T'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "BILA-VAR-CALC-T")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_BILA_VAR_CALC_T
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'BNFICR-LIQ'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "BNFICR-LIQ")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_BNFICR_LIQ
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'CLAU-TXT'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "CLAU-TXT")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_CLAU_TXT
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'CNBT-NDED'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "CNBT-NDED")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_CNBT_NDED
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'CONV-AZ'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "CONV-AZ")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_CONV_AZ
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'D-CRIST'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "D-CRIST")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_D_CRIST
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'DETT-ANTIC'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "DETT-ANTIC")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_DETT_ANTIC
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'DETT-DEROGA'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "DETT-DEROGA")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_DETT_DEROGA
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'DETT-QUEST'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "DETT-QUEST")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_DETT_QUEST
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'DETT-TCONT-LIQ'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "DETT-TCONT-LIQ")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_DETT_TCONT_LIQ
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'DETT-TIT-CONT'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "DETT-TIT-CONT")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_DETT_TIT_CONT
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'DETT-TIT-DI-RAT'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "DETT-TIT-DI-RAT")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_DETT_TIT_DI_RAT
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'DETT-TIT-COA-AG'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "DETT-TIT-COA-AG")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_DETT_TIT_COA_AG
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'DETT-TRASFE'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "DETT-TRASFE")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_DETT_TRASFE
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'DFLT-ADES'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "DFLT-ADES")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_DFLT_ADES
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'DFLT-RINN-TRCH'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "DFLT-RINN-TRCH")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_DFLT_RINN_TRCH
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'DOCTO-LIQ'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "DOCTO-LIQ")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_DOCTO_LIQ
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'D-COLL'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "D-COLL")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_D_COLL
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'D-FISC-ADES'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "D-FISC-ADES")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_D_FISC_ADES
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'D-FORZ-LIQ'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "D-FORZ-LIQ")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_D_FORZ_LIQ
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'ESTENZ-CLAU-TXT'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "ESTENZ-CLAU-TXT")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_ESTENZ_CLAU_TXT
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'EST-MOVI'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "EST-MOVI")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_EST_MOVI
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'ESTR-CNT-COASS'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "ESTR-CNT-COASS")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_ESTR_CNT_COASS
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'EVE-SERV-VAL'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "EVE-SERV-VAL")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_EVE_SERV_VAL
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'FNZ-F03-ACCOUNT'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "FNZ-F03-ACCOUNT")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_FNZ_F03_ACCOUNT
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'FNZ-F03-ORDER'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "FNZ-F03-ORDER")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_FNZ_F03_ORDER
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'FNZ-F05-ACCOUNT'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "FNZ-F05-ACCOUNT")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_FNZ_F05_ACCOUNT
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'FNZ-F5A-ACC-PF'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "FNZ-F5A-ACC-PF")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_FNZ_F5A_ACC_PF
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'FNZ-F5A-FUND-PF'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "FNZ-F5A-FUND-PF")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_FNZ_F5A_FUND_PF
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'GAR'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "GAR")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_GAR
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'GAR-LIQ'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "GAR-LIQ")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_GAR_LIQ
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'IDEN-ESTNI'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "IDEN-ESTNI")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_IDEN_ESTNI
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'IMPST-BOLLO'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "IMPST-BOLLO")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_IMPST_BOLLO
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'IMPST-SOST'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "IMPST-SOST")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_IMPST_SOST
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'LEG-ADES'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "LEG-ADES")) {
        // COB_CODE: EXEC SQL
        //                 VALUES NEXTVAL FOR SEQ_LEG_ADES
        //                   INTO :WS-ID-TABELLA
        //              END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'LIQ'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "LIQ")) {
        // COB_CODE: EXEC SQL
        //                 VALUES NEXTVAL FOR SEQ_LIQ
        //                   INTO :WS-ID-TABELLA
        //              END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'MATR-ELAB-BATCH'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "MATR-ELAB-BATCH")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_MATR_ELAB_BATCH
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'MATR-OBBL-ATTB'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "MATR-OBBL-ATTB")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_MATR_OBBL_ATTB
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'MOT-DEROGA'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "MOT-DEROGA")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_MOT_DEROGA
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'MOVI'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "MOVI")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_MOVI
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'MOVI-BATCH-SOSP'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "MOVI-BATCH-SOSP")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_MOVI_BATCH_SOSP
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'MOVI-FINRIO'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "MOVI-FINRIO")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_MOVI_FINRIO
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'NOTE-OGG'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "NOTE-OGG")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_NOTE_OGG
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'OGG-BLOCCO'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "OGG-BLOCCO")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_OGG_BLOCCO
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'OGG-COLLG'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "OGG-COLLG")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_OGG_COLLG
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'OGG-DEROGA'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "OGG-DEROGA")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_OGG_DEROGA
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'OGG-IN-COASS'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "OGG-IN-COASS")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_OGG_IN_COASS
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'PARAM-CLAU-TXT'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "PARAM-CLAU-TXT")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_PARAM_CLAU_TXT
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'PARAM-DI-CALC'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "PARAM-DI-CALC")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_PARAM_DI_CALC
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'PARAM-MOVI'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "PARAM-MOVI")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_PARAM_MOVI
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'PARAM-OGG'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "PARAM-OGG")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_PARAM_OGG
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'PERC-LIQ'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "PERC-LIQ")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_PERC_LIQ
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'POLI'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "POLI")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_POLI
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'PREST'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "PREST")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_PREST
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'PREV'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "PREV")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_PREV
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'PROG-PROD-IVASS'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "PROG-PROD-IVASS")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_PROG_PROD_IVASS
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'PROV-DI-GAR'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "PROV-DI-GAR")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_PROV_DI_GAR
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'QUEST'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "QUEST")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_QUEST
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'RAPP-ANA'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "RAPP-ANA")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_RAPP_ANA
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'RAPP-RETE'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "RAPP-RETE")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_RAPP_RETE
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'REINVST-POLI-LQ'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "REINVST-POLI-LQ")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_REINVST_POLI_LQ
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'RICH'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "RICH")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_RICH
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'RICH-DIS-FND'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "RICH-DIS-FND")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_RICH_DIS_FND
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'RICH-INVST-FND'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "RICH-INVST-FND")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_RICH_INVST_FND
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'RIP-COASS-AGE'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "RIP-COASS-AGE")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_RIP_COASS_AGE
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'RIP-DI-RIASS'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "RIP-DI-RIASS")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_RIP_DI_RIASS
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'RIS-DI-TRCH'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "RIS-DI-TRCH")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_RIS_DI_TRCH
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'RIV-TRCH-ESTR'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "RIV-TRCH-ESTR")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_RIV_TRCH_ESTR
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'RIV-VAR-CALC-P'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "RIV-VAR-CALC-P")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_RIV_VAR_CALC_P
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'RIV-VAR-CALC-T'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "RIV-VAR-CALC-T")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_RIV_VAR_CALC_T
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'SOGG-ANTIRICIC'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "SOGG-ANTIRICIC")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_SOGG_ANTIRICIC
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'SOPR-DETT-QUEST'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "SOPR-DETT-QUEST")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_SOPR_DETT_QUEST
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'SOPR-DI-GAR'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "SOPR-DI-GAR")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_SOPR_DI_GAR
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'STAT-OGG-BUS'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "STAT-OGG-BUS")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_STAT_OGG_BUS
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'STAT-OGG-WF'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "STAT-OGG-WF")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_STAT_OGG_WF
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'STRA-DI-INVST'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "STRA-DI-INVST")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_STRA_DI_INVST
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'STRC-MOVI-SOSP'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "STRC-MOVI-SOSP")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_STRC_MOVI_SOSP
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'TCONT-LIQ'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "TCONT-LIQ")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_TCONT_LIQ
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'TIT-CONT'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "TIT-CONT")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_TIT_CONT
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'TIT-RAT'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "TIT-RAT")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_TIT_RAT
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'TRANS-POLI'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "TRANS-POLI")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_TRANS_POLI
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'TRANS-RAPP-ANA'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "TRANS-RAPP-ANA")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_TRANS_RAPP_ANA
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'TRANS-REN'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "TRANS-REN")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_TRANS_REN
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'TRCH-DI-GAR'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "TRCH-DI-GAR")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_TRCH_DI_GAR
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'TRCH-LIQ'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "TRCH-LIQ")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_TRCH_LIQ
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'TRCH-RIASTA'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "TRCH-RIASTA")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_TRCH_RIASTA
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'QUEST-ADEG-VER'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "QUEST-ADEG-VER")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_QUEST_ADEG_VER
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'TS-BNS-SIN-PRE'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "TS-BNS-SIN-PRE")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_TS_BNS_SIN_PRE
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'VAL-AST'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "VAL-AST")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_VAL_AST
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'VAL-PLFD'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "VAL-PLFD")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_VAL_PLFD
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'VAL-PMC'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "VAL-PMC")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_VAL_PMC
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'VINC-PEG'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "VINC-PEG")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_VINC_PEG
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'RICH-EST'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "RICH-EST")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_RICH_EST
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'STAT-RICH-EST'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "STAT-RICH-EST")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_STAT_RICH_EST
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'TRA-COND-MODMIN'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "TRA-COND-MODMIN")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_TRA_COND_MODMIN
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'TRA-VAL-MODMIN'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "TRA-VAL-MODMIN")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_TRA_VAL_MODMIN
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'PRE-LRD-CONT'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "PRE-LRD-CONT")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_PRE_LRD_CONT
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'D-CALC-SWITCH'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "D-CALC-SWITCH")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_D_CALC_SWITCH
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'RIBIL'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "RIBIL")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_RIBIL
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'SERV-INV-MIFID'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "SERV-INV-MIFID")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_SERV_INV_MIFID
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'PRESTAZ-PRE-ASS'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "PRESTAZ-PRE-ASS")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_PRESTAZ_PRE_ASS
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'MOT-LIQ'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "MOT-LIQ")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_MOT_LIQ
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'PDM-POLI-FORZ'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "PDM-POLI-FORZ")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_PDM_POLI_FORZ
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'PDM-CONF-SCARTO'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "PDM-CONF-SCARTO")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_PDM_CONF_SCARTO
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'PDM-CONF-GAP-IM'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "PDM-CONF-GAP-IM")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_PDM_CONF_GAP_IM
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'PDM-CONF-ELAB'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "PDM-CONF-ELAB")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_PDM_CONF_ELAB
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'AUT-DUE-DIL'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "AUT-DUE-DIL")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_AUT_DUE_DIL
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        //***********************************************************
        //  SEQUENCE DI UTILIZZO INFRASTRUTTURALE
        //  BATCH EXECUTOR
        //***********************************************************
        // COB_CODE: IF WCOM-NOME-TABELLA = 'SEQ_ID_BATCH'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "SEQ_ID_BATCH")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_ID_BATCH
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'SEQ_ID_EXECUTION'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "SEQ_ID_EXECUTION")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_ID_EXECUTION
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'SEQ_ID_JOB'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "SEQ_ID_JOB")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_ID_JOB
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'SEQ_ID_MESSAGE'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "SEQ_ID_MESSAGE")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_ID_MESSAGE
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        //***********************************************************
        //  SEQUENCE DI UTILIZZO INFRASTRUTTURALE
        //  DATA DISPATCHER
        //***********************************************************
        // COB_CODE: IF WCOM-NOME-TABELLA = 'SEQ_RIGA'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "SEQ_RIGA")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_RIGA
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'SEQ_SESSIONE'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "SEQ_SESSIONE")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_SESSIONE
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'SHARED_MEMORY'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "SHARED_MEMORY")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_SHARED_MEMORY
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'REGOLE-VEND'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "REGOLE-VEND")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_REGOLE_VEND
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'BILA-PROV-AMM-D'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "BILA-PROV-AMM-D")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_BILA_PROV_AMM_D
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'BILA-PROV-AMM-T'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "BILA-PROV-AMM-T")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_BILA_PROV_AMM_T
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'SEQ_IVASS_TARI'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "SEQ_IVASS_TARI")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_IVASS_TARI
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'SEQ_IVASS_PROD'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "SEQ_IVASS_PROD")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_IVASS_PROD
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'FNZ-STANDBY'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "FNZ-STANDBY")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_FNZ_STANDBY
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'FNZ-EXT-ACC-ID'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "FNZ-EXT-ACC-ID")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_FNZ_EXT_ACC_ID
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'COSTI-IDD'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "COSTI-IDD")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_COSTI_IDD
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'DETT-COSTI-IDD'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "DETT-COSTI-IDD")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_DETT_COSTI_IDD
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'TERZO-REFERENTE'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "TERZO-REFERENTE")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_TERZO_REFERENTE
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'CNTR-PTF-DIFF'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "CNTR-PTF-DIFF")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_CNTR_PTF_DIFF
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'FNZ-MON-FL-QTD'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "FNZ-MON-FL-QTD")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_FNZ_MON_FL_QTD
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'PDM-CONF-ELAB-B'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "PDM-CONF-ELAB-B")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_PDM_CONF_ELAB_B
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'PDM-STRC-ELAB'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "PDM-STRC-ELAB")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_PDM_STRC_ELAB
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'PDM-STAT-ELAB'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "PDM-STAT-ELAB")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_PDM_STAT_ELAB
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'CNTSTR-CNT-CORR'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "CNTSTR-CNT-CORR")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_CNTSTR_CNT_CORR
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'STRA-ACTV-PTF'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "STRA-ACTV-PTF")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_STRA_ACTV_PTF
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'D-STRA-ACTV-PTF'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "D-STRA-ACTV-PTF")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_D_STRA_ACTV_PTF
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'STAT-STRA-AP'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "STAT-STRA-AP")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_STAT_STRA_AP
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'FNZ-EXT-ORDER-ID'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "FNZ-EXT-ORDER-ID")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_FNZ_EXT_ORDER_ID
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: PERFORM S1101-ESTRAZIONE-SEQ-CLIENT THRU EX-S1101.
        s1101EstrazioneSeqClient();
        //***********************************************************
        //
        //***********************************************************
        // COB_CODE: IF SQLCODE NOT = 0
        //              MOVE DESCRIZ-ERR-DB2  TO WCOM-DESCRIZ-ERR-DB2
        //           ELSE
        //              MOVE WS-ID-TABELLA    TO WCOM-SEQ-TABELLA
        //           END-IF.
        if (sqlca.getSqlcode() != 0) {
            // COB_CODE: MOVE 'D3'             TO WCOM-RETURN-CODE
            ws.getLccc0090().getReturnCode().setReturnCode("D3");
            // COB_CODE: MOVE SQLCODE          TO WCOM-SQLCODE
            ws.getLccc0090().getSqlcode().setSqlcodeSigned(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2  TO WCOM-DESCRIZ-ERR-DB2
            ws.getLccc0090().setDescrizErrDb2(ws.getDescrizErrDb2());
        }
        else {
            // COB_CODE: MOVE WS-ID-TABELLA    TO WCOM-SEQ-TABELLA
            ws.getLccc0090().setSeqTabella(ws.getWsIdTabella());
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: MOVE WORK-AREA           TO LINK-AREA.
        linkArea.setLinkAreaBytes(ws.getWorkAreaBytes());
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    /**Original name: S1101-ESTRAZIONE-SEQ-CLIENT<br>
	 * <pre>----------------------------------------------------------------*
	 *     ESTRAZIONE SEQUENCE CLIENT bnl
	 * ----------------------------------------------------------------*</pre>*/
    private void s1101EstrazioneSeqClient() {
        // COB_CODE: IF WCOM-NOME-TABELLA = 'EST-POLI'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "EST-POLI")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_EST_POLI
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'EST-RAPP-ANA'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "EST-RAPP-ANA")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_EST_RAPP_ANA
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'EST-TRCH-DI-GAR'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "EST-TRCH-DI-GAR")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_EST_TRCH_DI_GAR
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'PDM-GAR-UL-ESTR'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "PDM-GAR-UL-ESTR")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_PDM_GAR_UL_ESTR
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'PDM-TRCH-ESTR'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "PDM-TRCH-ESTR")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_PDM_TRCH_ESTR
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'PDM-VAR-CALC-P'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "PDM-VAR-CALC-P")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_PDM_VAR_CALC_P
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'PDM-VAR-CALC-T'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "PDM-VAR-CALC-T")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_PDM_VAR_CALC_T
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
        // COB_CODE: IF WCOM-NOME-TABELLA = 'SEGMENTAZ-CLI'
        //              END-EXEC
        //           END-IF.
        if (Conditions.eq(ws.getLccc0090().getNomeTabella(), "SEGMENTAZ-CLI")) {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_SEGMENTAZ_CLI
        //                INTO :WS-ID-TABELLA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        }
    }
}
