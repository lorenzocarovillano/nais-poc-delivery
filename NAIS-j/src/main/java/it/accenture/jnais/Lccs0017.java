package it.accenture.jnais;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.ws.DataInferioreLccs0010;
import it.accenture.jnais.ws.DataSuperioreLccs0010;
import it.accenture.jnais.ws.Formato;
import it.accenture.jnais.ws.GgDiff;
import it.accenture.jnais.ws.Lccs0017Data;
import static java.lang.Math.abs;

/**Original name: LCCS0017<br>
 * <pre>AUTHOR.            SYNTAX VITA (TP).
 * DATE-WRITTEN.           30 NOVEMBRE 1988.
 * ¹ INIZIO MODIFICA 09/02/89
 * ¹ MODIFICATA GESTIONE DIFFERENZA GIORNI CON ANNO A 365
 * ****************************************************************
 *             F U N Z I O N I   D E L   P R O G R A M M A        *
 * ****************************************************************
 *    CALCOLO DELLA DIFFERENZA IN GIORNI O IN MESI TRA DUE DATE   *
 *                                                                *
 *       PARAMETRI DI INPUT :                                     *
 *       - FORMATO DELLE DATE  ---- 'G' (GGMMAAAA)                *
 *                             ---- 'A' (AAAAMMGG)                *
 *                             ---- 'M' (AAAAMMGG PER CALCOLO     *
 *                                       DIFFERENZA IN MESI)      *
 *       - DATA INFERIORE   E  DATA SUPERIORE                     *
 *                                                                *
 *       VALORI DI OUTPUT :                                       *
 *       - DIFFERNZA IN GIORNI                                    *
 *         O IN MESI                                              *
 *       - CODICE RITORNO      ---- '0' (ELABORAZIONE O.K.)       *
 *                             ---- '1' (ERRATO 'FORMATO')        *
 *                             ---- '2' (DATE NON NUMERICHE)      *
 *                             ---- '3' (DATE NON IN SEQUENZA)    *
 *                             ---- '4' (MESE INESISTENTE)        *
 * ****************************************************************
 *                                                                *
 *   INFINE,DOPO AVER AGGIORNATO L'AREA COMUNE,IL CONTROLLO       *
 *   VIENE CEDUTO AL PROGRAMMA CHIAMANTE.                         *
 *                                                                *
 * ****************************************************************
 *                                                                *
 *             R O U T I N E         B A T C H                    *
 *                                                                *
 *      C A L C O L O   D I F F E R E N Z A  T R A  D A T E       *
 *                                                                *
 *             ( PROGRAMMA COBOL CHIAMATO CON UNA CALL)           *
 *                                                                *
 * ****************************************************************</pre>*/
public class Lccs0017 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lccs0017Data ws = new Lccs0017Data();
    //Original name: FORMATO
    private Formato formato;
    //Original name: DATA-INFERIORE
    private DataInferioreLccs0010 dataInferiore;
    //Original name: DATA-SUPERIORE
    private DataSuperioreLccs0010 dataSuperiore;
    //Original name: GG-DIFF
    private GgDiff ggDiff;
    //Original name: CODICE-RITORNO
    private Formato codiceRitorno;

    //==== METHODS ====
    /**Original name: MAIN_SUBROUTINE<br>*/
    public long execute(Formato formato, DataInferioreLccs0010 dataInferiore, DataSuperioreLccs0010 dataSuperiore, GgDiff ggDiff, Formato codiceRitorno) {
        this.formato = formato;
        this.dataInferiore = dataInferiore;
        this.dataSuperiore = dataSuperiore;
        this.ggDiff = ggDiff;
        this.codiceRitorno = codiceRitorno;
        inizio();
        fineTest();
        testAnni();
        return 0;
    }

    public static Lccs0017 getInstance() {
        return ((Lccs0017)Programs.getInstance(Lccs0017.class));
    }

    /**Original name: INIZIO<br>*/
    private void inizio() {
        // COB_CODE: MOVE        ZERO        TO  GG-DIFF
        //                                       CODICE-RITORNO.
        ggDiff.setGgDiff(0);
        codiceRitorno.setFormato('0');
        // COB_CODE: IF          FORMATO     NOT EQUAL   'G' AND 'A' AND 'M'
        //                                       GO TO FINE.
        if (formato.getFormato() != 'G' && formato.getFormato() != 'A' && formato.getFormato() != 'M') {
            // COB_CODE: MOVE    '1'  TO CODICE-RITORNO
            codiceRitorno.setCodiceRitornoFormatted("1");
            // COB_CODE: GO TO FINE.
            fine();
        }
        // COB_CODE: IF         (DATA-INFERIORE  NOT NUMERIC)
        //                OR    (DATA-SUPERIORE  NOT NUMERIC)
        //                                       GO TO FINE.
        if (!Functions.isNumber(dataInferiore.getDataInferioreBytes()) || !Functions.isNumber(dataSuperiore.getDataSuperioreBytes())) {
            // COB_CODE: MOVE     '2' TO CODICE-RITORNO
            codiceRitorno.setCodiceRitornoFormatted("2");
            // COB_CODE: GO TO FINE.
            fine();
        }
        // COB_CODE: IF          FORMATO         EQUAL     'A' OR 'M'
        //                       MOVE      DATA-SUPERIORE  TO AMGSUP
        //                 ELSE
        //                       MOVE      GG-SUP          TO GG-S.
        if (formato.getFormato() == 'A' || formato.getFormato() == 'M') {
            // COB_CODE: MOVE      DATA-INFERIORE  TO AMGINF
            ws.getAmginf().setAmginfBytes(dataInferiore.getDataInferioreBytes());
            // COB_CODE: MOVE      DATA-SUPERIORE  TO AMGSUP
            ws.getAmgsup().setAmgsupBytes(dataSuperiore.getDataSuperioreBytes());
        }
        else {
            // COB_CODE: MOVE      AAAA-INF        TO AAAA-I
            ws.getAmginf().setAaaaIFormatted(dataInferiore.getAaaaInfFormatted());
            // COB_CODE: MOVE      MM-INF          TO MM-I
            ws.getAmginf().setMmI(dataInferiore.getMmInf());
            // COB_CODE: MOVE      GG-INF          TO GG-I
            ws.getAmginf().setGgIFormatted(dataInferiore.getGgInfFormatted());
            // COB_CODE: MOVE      AAAA-SUP        TO AAAA-S
            ws.getAmgsup().setAaaaSFormatted(dataSuperiore.getAaaaSupFormatted());
            // COB_CODE: MOVE      MM-SUP          TO MM-S
            ws.getAmgsup().setMmSFormatted(dataSuperiore.getMmSupFormatted());
            // COB_CODE: MOVE      GG-SUP          TO GG-S.
            ws.getAmgsup().setGgSFormatted(dataSuperiore.getGgSupFormatted());
        }
        // COB_CODE: IF          AMGINF          GREATER   AMGSUP
        //                                       GO TO FINE.
        if (Conditions.gt(ws.getAmginf().getAmginfBytes(), ws.getAmgsup().getAmgsupBytes())) {
            // COB_CODE: MOVE    '3'   TO CODICE-RITORNO
            codiceRitorno.setCodiceRitornoFormatted("3");
            // COB_CODE: GO TO FINE.
            fine();
        }
        // COB_CODE: IF          MM-I              LESS     1
        //                OR     MM-I              GREATER 12
        //                OR     MM-S              LESS     1
        //                OR     MM-S              GREATER 12
        //                                       GO TO FINE.
        if (ws.getAmginf().getMmI() < 1 || ws.getAmginf().getMmI() > 12 || ws.getAmgsup().getMmS() < 1 || ws.getAmgsup().getMmS() > 12) {
            // COB_CODE: MOVE    '4' TO CODICE-RITORNO
            codiceRitorno.setCodiceRitornoFormatted("4");
            // COB_CODE: GO TO FINE.
            fine();
        }
        // COB_CODE: IF          AMGINF          EQUAL   AMGSUP
        //                                       GO TO     FINE.
        if (Conditions.eq(ws.getAmginf().getAmginfBytes(), ws.getAmgsup().getAmgsupBytes())) {
            // COB_CODE: GO TO     FINE.
            fine();
        }
        //¹ INIZIO MODIFICA 09/02/89
        // COB_CODE: IF   GG-I    =   28           AND
        //                GG-S    =   28
        //                GO TO FINE-TEST.
        if (ws.getAmginf().getGgI() == 28 && ws.getAmgsup().getGgS() == 28) {
            // COB_CODE: GO TO FINE-TEST.
            return;
        }
        // COB_CODE: IF   GG-I    =   29           AND
        //                GG-S    =   29
        //                GO TO FINE-TEST.
        if (ws.getAmginf().getGgI() == 29 && ws.getAmgsup().getGgS() == 29) {
            // COB_CODE: GO TO FINE-TEST.
            return;
        }
    }

    /**Original name: FINE-TEST<br>
	 * <pre>    IF   (GG-I    =   28 OR 29)   AND   MM-I   =  02
	 *            MOVE  30     TO GG-I.
	 *     IF    GG-I    =       31
	 *            MOVE  30     TO GG-I.
	 *     IF   (GG-S    =   28 OR 29)   AND   MM-S   =  02
	 *            MOVE  30     TO GG-S.
	 *     IF    GG-S    =       31
	 *            MOVE  30     TO GG-S.
	 * ¹ FINE MODIFICA 09/02/89</pre>*/
    private void fineTest() {
        // COB_CODE: IF FORMATO  EQUAL  'M'
        //             GO TO FINE.
        if (formato.getFormato() == 'M') {
            // COB_CODE: PERFORM  CALCOLO-MESI
            rngCalcoloMesi();
            // COB_CODE: GO TO FINE.
            fine();
        }
    }

    /**Original name: TEST-ANNI<br>*/
    private void testAnni() {
        // COB_CODE: IF          AAAA-I              EQUAL   AAAA-S
        //             GO TO IMPOSTA-DATA-SUP.
        if (ws.getAmginf().getAaaaI() == ws.getAmgsup().getAaaaS()) {
            // COB_CODE: GO TO IMPOSTA-DATA-SUP.
            impostaDataSup();
        }
        // COB_CODE: MOVE        12          TO  MM.
        ws.getAaaammgg().setMm(((short)12));
        //    MOVE        30          TO  GG.
        // COB_CODE: MOVE        31          TO  GG.
        ws.getAaaammgg().setGg(((short)31));
        // COB_CODE: PERFORM     CALCOLO-GIORNI.
        rngCalcoloGiorni();
        // COB_CODE: ADD         1           TO  AAAA-I.
        ws.getAmginf().setAaaaI(Trunc.toShort(1 + ws.getAmginf().getAaaaI(), 4));
        // COB_CODE: MOVE        01          TO  MM-I.
        ws.getAmginf().setMmI(((short)1));
        // COB_CODE: MOVE        ZERO        TO  GG-I.
        ws.getAmginf().setGgI(((short)0));
        // COB_CODE: GO TO       TEST-ANNI.
        testAnni();
    }

    /**Original name: IMPOSTA-DATA-SUP<br>*/
    private void impostaDataSup() {
        // COB_CODE: MOVE        AMGSUP      TO  AAAAMMGG.
        ws.getAaaammgg().setAaaammggBytes(ws.getAmgsup().getAmgsupBytes());
        // COB_CODE: PERFORM     CALCOLO-GIORNI.
        rngCalcoloGiorni();
        // COB_CODE: GO TO FINE.
        fine();
    }

    /**Original name: CALCOLO-GIORNI_FIRST_SENTENCES<br>*/
    private String calcoloGiorni() {
        // COB_CODE: IF          MM-I              EQUAL   MM
        //              GO TO   CALCOLO-GIORNI-999.
        if (ws.getAmginf().getMmI() == ws.getAaaammgg().getMm()) {
            // COB_CODE: COMPUTE GG-DIFF = GG-DIFF + (GG - GG-I)
            ggDiff.setGgDiff(Trunc.toInt(abs(ggDiff.getGgDiff() + (ws.getAaaammgg().getGg() - ws.getAmginf().getGgI())), 5));
            // COB_CODE: GO TO   CALCOLO-GIORNI-999.
            return "";
        }
        // COB_CODE: COMPUTE     GG-DIFF  =  GG-DIFF + (T-GG (MM-I) - GG-I).
        ggDiff.setGgDiff(Trunc.toInt(abs(ggDiff.getGgDiff() + (ws.getTabGiorni().gettGg(ws.getAmginf().getMmI()) - ws.getAmginf().getGgI())), 5));
        //    GESTIONE SE ANNO BISESTILE
        // COB_CODE: IF MM-I = 2
        //              END-IF
        //           END-IF
        if (ws.getAmginf().getMmI() == 2) {
            // COB_CODE: MOVE 0                      TO  DREST
            ws.setDrest(((short)0));
            // COB_CODE:    DIVIDE AAAA-I
            //                  BY 4
            //              GIVING DQUOZ
            //           REMAINDER DREST
            ws.setDquoz(((short)(ws.getAmginf().getAaaaI() / 4)));
            ws.setDrest(((short)(ws.getAmginf().getAaaaI() % 4)));
            // COB_CODE: IF DREST EQUAL ZEROES
            //              ADD 1                    TO GG-DIFF
            //           END-IF
            if (Characters.EQ_ZERO.test(ws.getDrestFormatted())) {
                // COB_CODE: ADD 1                    TO GG-DIFF
                ggDiff.setGgDiff(Trunc.toInt(1 + ggDiff.getGgDiff(), 5));
            }
        }
        // COB_CODE: ADD         1            TO  MM-I.
        ws.getAmginf().setMmI(Trunc.toShort(1 + ws.getAmginf().getMmI(), 2));
        // COB_CODE: MOVE        ZERO         TO  GG-I.
        ws.getAmginf().setGgI(((short)0));
        // COB_CODE: GO TO       CALCOLO-GIORNI.
        return "CALCOLO-GIORNI_FIRST_SENTENCES";
    }

    /**Original name: CALCOLO-GIORNI-999<br>*/
    private void calcoloGiorni999() {
    }

    /**Original name: CALCOLO-MESI_FIRST_SENTENCES<br>*/
    private String calcoloMesi() {
        // COB_CODE: IF          AAAA-S            GREATER AAAA-I
        //              GO TO   CALCOLO-MESI.
        if (ws.getAmgsup().getAaaaS() > ws.getAmginf().getAaaaI()) {
            // COB_CODE: COMPUTE GG-DIFF = GG-DIFF + 12
            ggDiff.setGgDiff(Trunc.toInt(ggDiff.getGgDiff() + 12, 5));
            // COB_CODE: COMPUTE AAAA-S = AAAA-S - 1
            ws.getAmgsup().setAaaaS(Trunc.toShort(abs(ws.getAmgsup().getAaaaS() - 1), 4));
            // COB_CODE: GO TO   CALCOLO-MESI.
            return "CALCOLO-MESI_FIRST_SENTENCES";
        }
        // COB_CODE: COMPUTE     GG-DIFF      =  GG-DIFF + (MM-S - MM-I).
        ggDiff.setGgDiff(Trunc.toInt(abs(ggDiff.getGgDiff() + (ws.getAmgsup().getMmS() - ws.getAmginf().getMmI())), 5));
        return "";
    }

    /**Original name: CALCOLO-MESI-999<br>*/
    private void calcoloMesi999() {
    }

    /**Original name: FINE_FIRST_SENTENCES<br>*/
    private void fine() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    /**Original name: RNG_CALCOLO-GIORNI_FIRST_SENTENCES-_-CALCOLO-GIORNI-999<br>*/
    private void rngCalcoloGiorni() {
        String retcode = "";
        boolean gotoCalcoloGiorniFirstSentences = false;
        do {
            gotoCalcoloGiorniFirstSentences = false;
            retcode = calcoloGiorni();
        }
        while (retcode.equals("CALCOLO-GIORNI_FIRST_SENTENCES"));
        calcoloGiorni999();
    }

    /**Original name: RNG_CALCOLO-MESI_FIRST_SENTENCES-_-CALCOLO-MESI-999<br>*/
    private void rngCalcoloMesi() {
        String retcode = "";
        boolean gotoCalcoloMesiFirstSentences = false;
        do {
            gotoCalcoloMesiFirstSentences = false;
            retcode = calcoloMesi();
        }
        while (retcode.equals("CALCOLO-MESI_FIRST_SENTENCES"));
        calcoloMesi999();
    }
}
