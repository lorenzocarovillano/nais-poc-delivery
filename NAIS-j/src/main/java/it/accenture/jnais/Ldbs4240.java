package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.RappReteDao;
import it.accenture.jnais.commons.data.to.IRappRete;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ldbs4240Data;
import it.accenture.jnais.ws.RappReteIdbsrre0;
import it.accenture.jnais.ws.redefines.RreCodAcqsCntrt;
import it.accenture.jnais.ws.redefines.RreCodCan;
import it.accenture.jnais.ws.redefines.RreCodPntReteEnd;
import it.accenture.jnais.ws.redefines.RreCodPntReteIni;
import it.accenture.jnais.ws.redefines.RreCodPntReteIniC;
import it.accenture.jnais.ws.redefines.RreIdMoviChiu;
import it.accenture.jnais.ws.redefines.RreIdOgg;
import it.accenture.jnais.ws.redefines.RreTpAcqsCntrt;

/**Original name: LDBS4240<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  27 APR 2010.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO AD HOC PER ACCESSO RISORSE DB        *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Ldbs4240 extends Program implements IRappRete {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private RappReteDao rappReteDao = new RappReteDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Ldbs4240Data ws = new Ldbs4240Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: RAPP-RETE
    private RappReteIdbsrre0 rappRete;

    //==== METHODS ====
    /**Original name: PROGRAM_LDBS4240_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, RappReteIdbsrre0 rappRete) {
        this.idsv0003 = idsv0003;
        this.rappRete = rappRete;
        // COB_CODE: PERFORM A000-INIZIO              THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                        a200ElaboraWcEff();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                        b200ElaboraWcCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //               SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                        c200ElaboraWcNst();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Ldbs4240 getInstance() {
        return ((Ldbs4240)Programs.getInstance(Ldbs4240.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'LDBS4240'              TO   IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("LDBS4240");
        // COB_CODE: MOVE 'RAPP-RETE' TO   IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("RAPP-RETE");
        // COB_CODE: MOVE '00'                      TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                    TO   IDSV0003-SQLCODE
        //                                               IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                    TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                               IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-WC-EFF<br>*/
    private void a200ElaboraWcEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-WC-EFF          THRU A210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-WC-EFF          THRU A210-EX
            a210SelectWcEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
            a260OpenCursorWcEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
            a270CloseCursorWcEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
            a280FetchFirstWcEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
            a290FetchNextWcEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B200-ELABORA-WC-CPZ<br>*/
    private void b200ElaboraWcCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
            b210SelectWcCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
            b260OpenCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
            b270CloseCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
            b280FetchFirstWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
            b290FetchNextWcCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: C200-ELABORA-WC-NST<br>*/
    private void c200ElaboraWcNst() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM C210-SELECT-WC-NST          THRU C210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM C210-SELECT-WC-NST          THRU C210-EX
            c210SelectWcNst();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
            c260OpenCursorWcNst();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
            c270CloseCursorWcNst();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
            c280FetchFirstWcNst();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
            c290FetchNextWcNst();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A205-DECLARE-CURSOR-WC-EFF<br>
	 * <pre>----
	 * ----  gestione WC Effetto
	 * ----</pre>*/
    private void a205DeclareCursorWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //              DECLARE C-EFF CURSOR FOR
        //              SELECT
        //                     ID_RAPP_RETE
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,TP_RETE
        //                    ,TP_ACQS_CNTRT
        //                    ,COD_ACQS_CNTRT
        //                    ,COD_PNT_RETE_INI
        //                    ,COD_PNT_RETE_END
        //                    ,FL_PNT_RETE_1RIO
        //                    ,COD_CAN
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,COD_PNT_RETE_INI_C
        //                    ,MATR_OPRT
        //              FROM RAPP_RETE
        //              WHERE      ID_OGG          = :RRE-ID-OGG
        //                    AND TP_OGG          = :RRE-TP-OGG
        //                    AND TP_RETE         = :RRE-TP-RETE
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A210-SELECT-WC-EFF<br>*/
    private void a210SelectWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                     ID_RAPP_RETE
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,TP_RETE
        //                    ,TP_ACQS_CNTRT
        //                    ,COD_ACQS_CNTRT
        //                    ,COD_PNT_RETE_INI
        //                    ,COD_PNT_RETE_END
        //                    ,FL_PNT_RETE_1RIO
        //                    ,COD_CAN
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,COD_PNT_RETE_INI_C
        //                    ,MATR_OPRT
        //             INTO
        //                :RRE-ID-RAPP-RETE
        //               ,:RRE-ID-OGG
        //                :IND-RRE-ID-OGG
        //               ,:RRE-TP-OGG
        //               ,:RRE-ID-MOVI-CRZ
        //               ,:RRE-ID-MOVI-CHIU
        //                :IND-RRE-ID-MOVI-CHIU
        //               ,:RRE-DT-INI-EFF-DB
        //               ,:RRE-DT-END-EFF-DB
        //               ,:RRE-COD-COMP-ANIA
        //               ,:RRE-TP-RETE
        //               ,:RRE-TP-ACQS-CNTRT
        //                :IND-RRE-TP-ACQS-CNTRT
        //               ,:RRE-COD-ACQS-CNTRT
        //                :IND-RRE-COD-ACQS-CNTRT
        //               ,:RRE-COD-PNT-RETE-INI
        //                :IND-RRE-COD-PNT-RETE-INI
        //               ,:RRE-COD-PNT-RETE-END
        //                :IND-RRE-COD-PNT-RETE-END
        //               ,:RRE-FL-PNT-RETE-1RIO
        //                :IND-RRE-FL-PNT-RETE-1RIO
        //               ,:RRE-COD-CAN
        //                :IND-RRE-COD-CAN
        //               ,:RRE-DS-RIGA
        //               ,:RRE-DS-OPER-SQL
        //               ,:RRE-DS-VER
        //               ,:RRE-DS-TS-INI-CPTZ
        //               ,:RRE-DS-TS-END-CPTZ
        //               ,:RRE-DS-UTENTE
        //               ,:RRE-DS-STATO-ELAB
        //               ,:RRE-COD-PNT-RETE-INI-C
        //                :IND-RRE-COD-PNT-RETE-INI-C
        //               ,:RRE-MATR-OPRT
        //                :IND-RRE-MATR-OPRT
        //             FROM RAPP_RETE
        //             WHERE      ID_OGG          = :RRE-ID-OGG
        //                    AND TP_OGG          = :RRE-TP-OGG
        //                    AND TP_RETE         = :RRE-TP-RETE
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        rappReteDao.selectRec4(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: A260-OPEN-CURSOR-WC-EFF<br>*/
    private void a260OpenCursorWcEff() {
        // COB_CODE: PERFORM A205-DECLARE-CURSOR-WC-EFF THRU A205-EX.
        a205DeclareCursorWcEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-EFF
        //           END-EXEC.
        rappReteDao.openCEff23(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A270-CLOSE-CURSOR-WC-EFF<br>*/
    private void a270CloseCursorWcEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-EFF
        //           END-EXEC.
        rappReteDao.closeCEff23();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A280-FETCH-FIRST-WC-EFF<br>*/
    private void a280FetchFirstWcEff() {
        // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF    THRU A260-EX.
        a260OpenCursorWcEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
            a290FetchNextWcEff();
        }
    }

    /**Original name: A290-FETCH-NEXT-WC-EFF<br>*/
    private void a290FetchNextWcEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-EFF
        //           INTO
        //                :RRE-ID-RAPP-RETE
        //               ,:RRE-ID-OGG
        //                :IND-RRE-ID-OGG
        //               ,:RRE-TP-OGG
        //               ,:RRE-ID-MOVI-CRZ
        //               ,:RRE-ID-MOVI-CHIU
        //                :IND-RRE-ID-MOVI-CHIU
        //               ,:RRE-DT-INI-EFF-DB
        //               ,:RRE-DT-END-EFF-DB
        //               ,:RRE-COD-COMP-ANIA
        //               ,:RRE-TP-RETE
        //               ,:RRE-TP-ACQS-CNTRT
        //                :IND-RRE-TP-ACQS-CNTRT
        //               ,:RRE-COD-ACQS-CNTRT
        //                :IND-RRE-COD-ACQS-CNTRT
        //               ,:RRE-COD-PNT-RETE-INI
        //                :IND-RRE-COD-PNT-RETE-INI
        //               ,:RRE-COD-PNT-RETE-END
        //                :IND-RRE-COD-PNT-RETE-END
        //               ,:RRE-FL-PNT-RETE-1RIO
        //                :IND-RRE-FL-PNT-RETE-1RIO
        //               ,:RRE-COD-CAN
        //                :IND-RRE-COD-CAN
        //               ,:RRE-DS-RIGA
        //               ,:RRE-DS-OPER-SQL
        //               ,:RRE-DS-VER
        //               ,:RRE-DS-TS-INI-CPTZ
        //               ,:RRE-DS-TS-END-CPTZ
        //               ,:RRE-DS-UTENTE
        //               ,:RRE-DS-STATO-ELAB
        //               ,:RRE-COD-PNT-RETE-INI-C
        //                :IND-RRE-COD-PNT-RETE-INI-C
        //               ,:RRE-MATR-OPRT
        //                :IND-RRE-MATR-OPRT
        //           END-EXEC.
        rappReteDao.fetchCEff23(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF THRU A270-EX
            a270CloseCursorWcEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B205-DECLARE-CURSOR-WC-CPZ<br>
	 * <pre>----
	 * ----  gestione WC Competenza
	 * ----</pre>*/
    private void b205DeclareCursorWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //              DECLARE C-CPZ CURSOR FOR
        //              SELECT
        //                     ID_RAPP_RETE
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,TP_RETE
        //                    ,TP_ACQS_CNTRT
        //                    ,COD_ACQS_CNTRT
        //                    ,COD_PNT_RETE_INI
        //                    ,COD_PNT_RETE_END
        //                    ,FL_PNT_RETE_1RIO
        //                    ,COD_CAN
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,COD_PNT_RETE_INI_C
        //                    ,MATR_OPRT
        //              FROM RAPP_RETE
        //              WHERE      ID_OGG          = :RRE-ID-OGG
        //                        AND TP_OGG          = :RRE-TP-OGG
        //                        AND TP_RETE         = :RRE-TP-RETE
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B210-SELECT-WC-CPZ<br>*/
    private void b210SelectWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                     ID_RAPP_RETE
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,TP_RETE
        //                    ,TP_ACQS_CNTRT
        //                    ,COD_ACQS_CNTRT
        //                    ,COD_PNT_RETE_INI
        //                    ,COD_PNT_RETE_END
        //                    ,FL_PNT_RETE_1RIO
        //                    ,COD_CAN
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,COD_PNT_RETE_INI_C
        //                    ,MATR_OPRT
        //             INTO
        //                :RRE-ID-RAPP-RETE
        //               ,:RRE-ID-OGG
        //                :IND-RRE-ID-OGG
        //               ,:RRE-TP-OGG
        //               ,:RRE-ID-MOVI-CRZ
        //               ,:RRE-ID-MOVI-CHIU
        //                :IND-RRE-ID-MOVI-CHIU
        //               ,:RRE-DT-INI-EFF-DB
        //               ,:RRE-DT-END-EFF-DB
        //               ,:RRE-COD-COMP-ANIA
        //               ,:RRE-TP-RETE
        //               ,:RRE-TP-ACQS-CNTRT
        //                :IND-RRE-TP-ACQS-CNTRT
        //               ,:RRE-COD-ACQS-CNTRT
        //                :IND-RRE-COD-ACQS-CNTRT
        //               ,:RRE-COD-PNT-RETE-INI
        //                :IND-RRE-COD-PNT-RETE-INI
        //               ,:RRE-COD-PNT-RETE-END
        //                :IND-RRE-COD-PNT-RETE-END
        //               ,:RRE-FL-PNT-RETE-1RIO
        //                :IND-RRE-FL-PNT-RETE-1RIO
        //               ,:RRE-COD-CAN
        //                :IND-RRE-COD-CAN
        //               ,:RRE-DS-RIGA
        //               ,:RRE-DS-OPER-SQL
        //               ,:RRE-DS-VER
        //               ,:RRE-DS-TS-INI-CPTZ
        //               ,:RRE-DS-TS-END-CPTZ
        //               ,:RRE-DS-UTENTE
        //               ,:RRE-DS-STATO-ELAB
        //               ,:RRE-COD-PNT-RETE-INI-C
        //                :IND-RRE-COD-PNT-RETE-INI-C
        //               ,:RRE-MATR-OPRT
        //                :IND-RRE-MATR-OPRT
        //             FROM RAPP_RETE
        //             WHERE      ID_OGG          = :RRE-ID-OGG
        //                    AND TP_OGG          = :RRE-TP-OGG
        //                    AND TP_RETE         = :RRE-TP-RETE
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        rappReteDao.selectRec5(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: B260-OPEN-CURSOR-WC-CPZ<br>*/
    private void b260OpenCursorWcCpz() {
        // COB_CODE: PERFORM B205-DECLARE-CURSOR-WC-CPZ THRU B205-EX.
        b205DeclareCursorWcCpz();
        // COB_CODE: EXEC SQL
        //                OPEN C-CPZ
        //           END-EXEC.
        rappReteDao.openCCpz23(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B270-CLOSE-CURSOR-WC-CPZ<br>*/
    private void b270CloseCursorWcCpz() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-CPZ
        //           END-EXEC.
        rappReteDao.closeCCpz23();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B280-FETCH-FIRST-WC-CPZ<br>*/
    private void b280FetchFirstWcCpz() {
        // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ    THRU B260-EX.
        b260OpenCursorWcCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
            b290FetchNextWcCpz();
        }
    }

    /**Original name: B290-FETCH-NEXT-WC-CPZ<br>*/
    private void b290FetchNextWcCpz() {
        // COB_CODE: EXEC SQL
        //                FETCH C-CPZ
        //           INTO
        //                :RRE-ID-RAPP-RETE
        //               ,:RRE-ID-OGG
        //                :IND-RRE-ID-OGG
        //               ,:RRE-TP-OGG
        //               ,:RRE-ID-MOVI-CRZ
        //               ,:RRE-ID-MOVI-CHIU
        //                :IND-RRE-ID-MOVI-CHIU
        //               ,:RRE-DT-INI-EFF-DB
        //               ,:RRE-DT-END-EFF-DB
        //               ,:RRE-COD-COMP-ANIA
        //               ,:RRE-TP-RETE
        //               ,:RRE-TP-ACQS-CNTRT
        //                :IND-RRE-TP-ACQS-CNTRT
        //               ,:RRE-COD-ACQS-CNTRT
        //                :IND-RRE-COD-ACQS-CNTRT
        //               ,:RRE-COD-PNT-RETE-INI
        //                :IND-RRE-COD-PNT-RETE-INI
        //               ,:RRE-COD-PNT-RETE-END
        //                :IND-RRE-COD-PNT-RETE-END
        //               ,:RRE-FL-PNT-RETE-1RIO
        //                :IND-RRE-FL-PNT-RETE-1RIO
        //               ,:RRE-COD-CAN
        //                :IND-RRE-COD-CAN
        //               ,:RRE-DS-RIGA
        //               ,:RRE-DS-OPER-SQL
        //               ,:RRE-DS-VER
        //               ,:RRE-DS-TS-INI-CPTZ
        //               ,:RRE-DS-TS-END-CPTZ
        //               ,:RRE-DS-UTENTE
        //               ,:RRE-DS-STATO-ELAB
        //               ,:RRE-COD-PNT-RETE-INI-C
        //                :IND-RRE-COD-PNT-RETE-INI-C
        //               ,:RRE-MATR-OPRT
        //                :IND-RRE-MATR-OPRT
        //           END-EXEC.
        rappReteDao.fetchCCpz23(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ THRU B270-EX
            b270CloseCursorWcCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: C205-DECLARE-CURSOR-WC-NST<br>
	 * <pre>----
	 * ----  gestione WC Senza Storicità
	 * ----</pre>*/
    private void c205DeclareCursorWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C210-SELECT-WC-NST<br>*/
    private void c210SelectWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C260-OPEN-CURSOR-WC-NST<br>*/
    private void c260OpenCursorWcNst() {
        // COB_CODE: PERFORM C205-DECLARE-CURSOR-WC-NST THRU C205-EX.
        c205DeclareCursorWcNst();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C270-CLOSE-CURSOR-WC-NST<br>*/
    private void c270CloseCursorWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C280-FETCH-FIRST-WC-NST<br>*/
    private void c280FetchFirstWcNst() {
        // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST    THRU C260-EX.
        c260OpenCursorWcNst();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
            c290FetchNextWcNst();
        }
    }

    /**Original name: C290-FETCH-NEXT-WC-NST<br>*/
    private void c290FetchNextWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>----
	 * ----  utilità comuni a tutti i livelli operazione
	 * ----</pre>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-RRE-ID-OGG = -1
        //              MOVE HIGH-VALUES TO RRE-ID-OGG-NULL
        //           END-IF
        if (ws.getIndRappRete().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RRE-ID-OGG-NULL
            rappRete.getRreIdOgg().setRreIdOggNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RreIdOgg.Len.RRE_ID_OGG_NULL));
        }
        // COB_CODE: IF IND-RRE-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO RRE-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndRappRete().getDtSin1oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RRE-ID-MOVI-CHIU-NULL
            rappRete.getRreIdMoviChiu().setRreIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RreIdMoviChiu.Len.RRE_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-RRE-TP-ACQS-CNTRT = -1
        //              MOVE HIGH-VALUES TO RRE-TP-ACQS-CNTRT-NULL
        //           END-IF
        if (ws.getIndRappRete().getCauSin1oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RRE-TP-ACQS-CNTRT-NULL
            rappRete.getRreTpAcqsCntrt().setRreTpAcqsCntrtNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RreTpAcqsCntrt.Len.RRE_TP_ACQS_CNTRT_NULL));
        }
        // COB_CODE: IF IND-RRE-COD-ACQS-CNTRT = -1
        //              MOVE HIGH-VALUES TO RRE-COD-ACQS-CNTRT-NULL
        //           END-IF
        if (ws.getIndRappRete().getTpSin1oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RRE-COD-ACQS-CNTRT-NULL
            rappRete.getRreCodAcqsCntrt().setRreCodAcqsCntrtNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RreCodAcqsCntrt.Len.RRE_COD_ACQS_CNTRT_NULL));
        }
        // COB_CODE: IF IND-RRE-COD-PNT-RETE-INI = -1
        //              MOVE HIGH-VALUES TO RRE-COD-PNT-RETE-INI-NULL
        //           END-IF
        if (ws.getIndRappRete().getDtSin2oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RRE-COD-PNT-RETE-INI-NULL
            rappRete.getRreCodPntReteIni().setRreCodPntReteIniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RreCodPntReteIni.Len.RRE_COD_PNT_RETE_INI_NULL));
        }
        // COB_CODE: IF IND-RRE-COD-PNT-RETE-END = -1
        //              MOVE HIGH-VALUES TO RRE-COD-PNT-RETE-END-NULL
        //           END-IF
        if (ws.getIndRappRete().getCauSin2oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RRE-COD-PNT-RETE-END-NULL
            rappRete.getRreCodPntReteEnd().setRreCodPntReteEndNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RreCodPntReteEnd.Len.RRE_COD_PNT_RETE_END_NULL));
        }
        // COB_CODE: IF IND-RRE-FL-PNT-RETE-1RIO = -1
        //              MOVE HIGH-VALUES TO RRE-FL-PNT-RETE-1RIO-NULL
        //           END-IF
        if (ws.getIndRappRete().getTpSin2oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RRE-FL-PNT-RETE-1RIO-NULL
            rappRete.setRreFlPntRete1rio(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-RRE-COD-CAN = -1
        //              MOVE HIGH-VALUES TO RRE-COD-CAN-NULL
        //           END-IF
        if (ws.getIndRappRete().getDtSin3oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RRE-COD-CAN-NULL
            rappRete.getRreCodCan().setRreCodCanNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RreCodCan.Len.RRE_COD_CAN_NULL));
        }
        // COB_CODE: IF IND-RRE-COD-PNT-RETE-INI-C = -1
        //              MOVE HIGH-VALUES TO RRE-COD-PNT-RETE-INI-C-NULL
        //           END-IF
        if (ws.getIndRappRete().getCauSin3oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RRE-COD-PNT-RETE-INI-C-NULL
            rappRete.getRreCodPntReteIniC().setRreCodPntReteIniCNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RreCodPntReteIniC.Len.RRE_COD_PNT_RETE_INI_C_NULL));
        }
        // COB_CODE: IF IND-RRE-MATR-OPRT = -1
        //              MOVE HIGH-VALUES TO RRE-MATR-OPRT-NULL
        //           END-IF.
        if (ws.getIndRappRete().getTpSin3oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RRE-MATR-OPRT-NULL
            rappRete.setRreMatrOprt(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappReteIdbsrre0.Len.RRE_MATR_OPRT));
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE RRE-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getIdbvrre3().getRreDtIniEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO RRE-DT-INI-EFF
        rappRete.setRreDtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE RRE-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getIdbvrre3().getRreDtEndEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO RRE-DT-END-EFF.
        rappRete.setRreDtEndEff(ws.getIdsv0010().getWsDateN());
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z970-CODICE-ADHOC-PRE<br>
	 * <pre>----
	 * ----  prevede statements AD HOC PRE Query
	 * ----</pre>*/
    private void z970CodiceAdhocPre() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z980-CODICE-ADHOC-POST<br>
	 * <pre>----
	 * ----  prevede statements AD HOC POST Query
	 * ----</pre>*/
    private void z980CodiceAdhocPost() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public int getCodAcqsCntrt() {
        return rappRete.getRreCodAcqsCntrt().getRreCodAcqsCntrt();
    }

    @Override
    public void setCodAcqsCntrt(int codAcqsCntrt) {
        this.rappRete.getRreCodAcqsCntrt().setRreCodAcqsCntrt(codAcqsCntrt);
    }

    @Override
    public Integer getCodAcqsCntrtObj() {
        if (ws.getIndRappRete().getTpSin1oAssto() >= 0) {
            return ((Integer)getCodAcqsCntrt());
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodAcqsCntrtObj(Integer codAcqsCntrtObj) {
        if (codAcqsCntrtObj != null) {
            setCodAcqsCntrt(((int)codAcqsCntrtObj));
            ws.getIndRappRete().setTpSin1oAssto(((short)0));
        }
        else {
            ws.getIndRappRete().setTpSin1oAssto(((short)-1));
        }
    }

    @Override
    public int getCodCan() {
        return rappRete.getRreCodCan().getRreCodCan();
    }

    @Override
    public void setCodCan(int codCan) {
        this.rappRete.getRreCodCan().setRreCodCan(codCan);
    }

    @Override
    public Integer getCodCanObj() {
        if (ws.getIndRappRete().getDtSin3oAssto() >= 0) {
            return ((Integer)getCodCan());
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodCanObj(Integer codCanObj) {
        if (codCanObj != null) {
            setCodCan(((int)codCanObj));
            ws.getIndRappRete().setDtSin3oAssto(((short)0));
        }
        else {
            ws.getIndRappRete().setDtSin3oAssto(((short)-1));
        }
    }

    @Override
    public int getCodCompAnia() {
        return rappRete.getRreCodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.rappRete.setRreCodCompAnia(codCompAnia);
    }

    @Override
    public long getCodPntReteEnd() {
        return rappRete.getRreCodPntReteEnd().getRreCodPntReteEnd();
    }

    @Override
    public void setCodPntReteEnd(long codPntReteEnd) {
        this.rappRete.getRreCodPntReteEnd().setRreCodPntReteEnd(codPntReteEnd);
    }

    @Override
    public Long getCodPntReteEndObj() {
        if (ws.getIndRappRete().getCauSin2oAssto() >= 0) {
            return ((Long)getCodPntReteEnd());
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodPntReteEndObj(Long codPntReteEndObj) {
        if (codPntReteEndObj != null) {
            setCodPntReteEnd(((long)codPntReteEndObj));
            ws.getIndRappRete().setCauSin2oAssto(((short)0));
        }
        else {
            ws.getIndRappRete().setCauSin2oAssto(((short)-1));
        }
    }

    @Override
    public long getCodPntReteIni() {
        return rappRete.getRreCodPntReteIni().getRreCodPntReteIni();
    }

    @Override
    public void setCodPntReteIni(long codPntReteIni) {
        this.rappRete.getRreCodPntReteIni().setRreCodPntReteIni(codPntReteIni);
    }

    @Override
    public long getCodPntReteIniC() {
        return rappRete.getRreCodPntReteIniC().getRreCodPntReteIniC();
    }

    @Override
    public void setCodPntReteIniC(long codPntReteIniC) {
        this.rappRete.getRreCodPntReteIniC().setRreCodPntReteIniC(codPntReteIniC);
    }

    @Override
    public Long getCodPntReteIniCObj() {
        if (ws.getIndRappRete().getCauSin3oAssto() >= 0) {
            return ((Long)getCodPntReteIniC());
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodPntReteIniCObj(Long codPntReteIniCObj) {
        if (codPntReteIniCObj != null) {
            setCodPntReteIniC(((long)codPntReteIniCObj));
            ws.getIndRappRete().setCauSin3oAssto(((short)0));
        }
        else {
            ws.getIndRappRete().setCauSin3oAssto(((short)-1));
        }
    }

    @Override
    public Long getCodPntReteIniObj() {
        if (ws.getIndRappRete().getDtSin2oAssto() >= 0) {
            return ((Long)getCodPntReteIni());
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodPntReteIniObj(Long codPntReteIniObj) {
        if (codPntReteIniObj != null) {
            setCodPntReteIni(((long)codPntReteIniObj));
            ws.getIndRappRete().setDtSin2oAssto(((short)0));
        }
        else {
            ws.getIndRappRete().setDtSin2oAssto(((short)-1));
        }
    }

    @Override
    public char getDsOperSql() {
        return rappRete.getRreDsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.rappRete.setRreDsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return rappRete.getRreDsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.rappRete.setRreDsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsEndCptz() {
        return rappRete.getRreDsTsEndCptz();
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.rappRete.setRreDsTsEndCptz(dsTsEndCptz);
    }

    @Override
    public long getDsTsIniCptz() {
        return rappRete.getRreDsTsIniCptz();
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.rappRete.setRreDsTsIniCptz(dsTsIniCptz);
    }

    @Override
    public String getDsUtente() {
        return rappRete.getRreDsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.rappRete.setRreDsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return rappRete.getRreDsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.rappRete.setRreDsVer(dsVer);
    }

    @Override
    public String getDtEndEffDb() {
        return ws.getIdbvrre3().getRreDtEndEffDb();
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        this.ws.getIdbvrre3().setRreDtEndEffDb(dtEndEffDb);
    }

    @Override
    public String getDtIniEffDb() {
        return ws.getIdbvrre3().getRreDtIniEffDb();
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        this.ws.getIdbvrre3().setRreDtIniEffDb(dtIniEffDb);
    }

    @Override
    public char getFlPntRete1rio() {
        return rappRete.getRreFlPntRete1rio();
    }

    @Override
    public void setFlPntRete1rio(char flPntRete1rio) {
        this.rappRete.setRreFlPntRete1rio(flPntRete1rio);
    }

    @Override
    public Character getFlPntRete1rioObj() {
        if (ws.getIndRappRete().getTpSin2oAssto() >= 0) {
            return ((Character)getFlPntRete1rio());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlPntRete1rioObj(Character flPntRete1rioObj) {
        if (flPntRete1rioObj != null) {
            setFlPntRete1rio(((char)flPntRete1rioObj));
            ws.getIndRappRete().setTpSin2oAssto(((short)0));
        }
        else {
            ws.getIndRappRete().setTpSin2oAssto(((short)-1));
        }
    }

    @Override
    public int getIdMoviChiu() {
        return rappRete.getRreIdMoviChiu().getRreIdMoviChiu();
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        this.rappRete.getRreIdMoviChiu().setRreIdMoviChiu(idMoviChiu);
    }

    @Override
    public Integer getIdMoviChiuObj() {
        if (ws.getIndRappRete().getDtSin1oAssto() >= 0) {
            return ((Integer)getIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        if (idMoviChiuObj != null) {
            setIdMoviChiu(((int)idMoviChiuObj));
            ws.getIndRappRete().setDtSin1oAssto(((short)0));
        }
        else {
            ws.getIndRappRete().setDtSin1oAssto(((short)-1));
        }
    }

    @Override
    public int getIdMoviCrz() {
        return rappRete.getRreIdMoviCrz();
    }

    @Override
    public void setIdMoviCrz(int idMoviCrz) {
        this.rappRete.setRreIdMoviCrz(idMoviCrz);
    }

    @Override
    public int getIdRappRete() {
        return rappRete.getRreIdRappRete();
    }

    @Override
    public void setIdRappRete(int idRappRete) {
        this.rappRete.setRreIdRappRete(idRappRete);
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        return idsv0003.getCodiceCompagniaAnia();
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        this.idsv0003.setCodiceCompagniaAnia(idsv0003CodiceCompagniaAnia);
    }

    @Override
    public String getMatrOprt() {
        return rappRete.getRreMatrOprt();
    }

    @Override
    public void setMatrOprt(String matrOprt) {
        this.rappRete.setRreMatrOprt(matrOprt);
    }

    @Override
    public String getMatrOprtObj() {
        if (ws.getIndRappRete().getTpSin3oAssto() >= 0) {
            return getMatrOprt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setMatrOprtObj(String matrOprtObj) {
        if (matrOprtObj != null) {
            setMatrOprt(matrOprtObj);
            ws.getIndRappRete().setTpSin3oAssto(((short)0));
        }
        else {
            ws.getIndRappRete().setTpSin3oAssto(((short)-1));
        }
    }

    @Override
    public long getRreDsRiga() {
        return rappRete.getRreDsRiga();
    }

    @Override
    public void setRreDsRiga(long rreDsRiga) {
        this.rappRete.setRreDsRiga(rreDsRiga);
    }

    @Override
    public int getRreIdOgg() {
        return rappRete.getRreIdOgg().getRreIdOgg();
    }

    @Override
    public void setRreIdOgg(int rreIdOgg) {
        this.rappRete.getRreIdOgg().setRreIdOgg(rreIdOgg);
    }

    @Override
    public Integer getRreIdOggObj() {
        if (ws.getIndRappRete().getIdMoviChiu() >= 0) {
            return ((Integer)getRreIdOgg());
        }
        else {
            return null;
        }
    }

    @Override
    public void setRreIdOggObj(Integer rreIdOggObj) {
        if (rreIdOggObj != null) {
            setRreIdOgg(((int)rreIdOggObj));
            ws.getIndRappRete().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndRappRete().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public String getRreTpOgg() {
        return rappRete.getRreTpOgg();
    }

    @Override
    public void setRreTpOgg(String rreTpOgg) {
        this.rappRete.setRreTpOgg(rreTpOgg);
    }

    @Override
    public String getRreTpRete() {
        return rappRete.getRreTpRete();
    }

    @Override
    public void setRreTpRete(String rreTpRete) {
        this.rappRete.setRreTpRete(rreTpRete);
    }

    @Override
    public int getTpAcqsCntrt() {
        return rappRete.getRreTpAcqsCntrt().getRreTpAcqsCntrt();
    }

    @Override
    public void setTpAcqsCntrt(int tpAcqsCntrt) {
        this.rappRete.getRreTpAcqsCntrt().setRreTpAcqsCntrt(tpAcqsCntrt);
    }

    @Override
    public Integer getTpAcqsCntrtObj() {
        if (ws.getIndRappRete().getCauSin1oAssto() >= 0) {
            return ((Integer)getTpAcqsCntrt());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpAcqsCntrtObj(Integer tpAcqsCntrtObj) {
        if (tpAcqsCntrtObj != null) {
            setTpAcqsCntrt(((int)tpAcqsCntrtObj));
            ws.getIndRappRete().setCauSin1oAssto(((short)0));
        }
        else {
            ws.getIndRappRete().setCauSin1oAssto(((short)-1));
        }
    }

    @Override
    public String getWsDataInizioEffettoDb() {
        return ws.getIdsv0010().getWsDataInizioEffettoDb();
    }

    @Override
    public void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb) {
        this.ws.getIdsv0010().setWsDataInizioEffettoDb(wsDataInizioEffettoDb);
    }

    @Override
    public long getWsTsCompetenza() {
        return ws.getIdsv0010().getWsTsCompetenza();
    }

    @Override
    public void setWsTsCompetenza(long wsTsCompetenza) {
        this.ws.getIdsv0010().setWsTsCompetenza(wsTsCompetenza);
    }
}
