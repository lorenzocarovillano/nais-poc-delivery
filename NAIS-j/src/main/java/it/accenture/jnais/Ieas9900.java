package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.GravitaErroreDao;
import it.accenture.jnais.commons.data.dao.LinguaErroreDao;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.AreaIdsv0001;
import it.accenture.jnais.ws.Ieai9901Area;
import it.accenture.jnais.ws.Ieao9901Area;
import it.accenture.jnais.ws.Ieas9900Data;

/**Original name: IEAS9900<br>
 * <pre>****************************************************************
 *                                                                *
 *                    IDENTIFICATION DIVISION                     *
 *                                                                *
 * ****************************************************************
 * AUTHOR.        ACCENTURE.
 * DATE-WRITTEN.  27/10/2006.
 * ****************************************************************
 *                                                                *
 *     NOME :           IEAS9900                                  *
 *     TIPO :                                                     *
 *     DESCRIZIONE :    RICERCA GRAVITA' ERRORE                   *
 *                                                                *
 *     AREE DI PASSAGGIO DATI                                     *
 *                                                                *
 *     DATI DI INPUT/OUTPUT : IEAI9901/IEAO9901                   *
 *                                                                *
 * ****************************************************************
 *                       LOG MODIFICHE                            *
 * ****************************************************************
 *                                                                *
 *  CODICE MODIF.            AUTORE          DATA                 *
 *  ---------------  **   -----------   **   --/--/----           *
 *                                                                *
 *  DESCRZIONE: XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX           *
 *              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX           *
 *                                                                *
 * ****************************************************************
 * ****************************************************************
 *                                                                *
 *                    ENVIRONMENT  DIVISION                       *
 *                                                                *
 * ****************************************************************
 *     NULL-IND     Carattere usato per indicare campi NULL;</pre>*/
public class Ieas9900 extends Program {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private GravitaErroreDao gravitaErroreDao = new GravitaErroreDao(dbAccessStatus);
    private LinguaErroreDao linguaErroreDao = new LinguaErroreDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Ieas9900Data ws = new Ieas9900Data();
    //Original name: AREA-IDSV0001
    private AreaIdsv0001 areaIdsv0001;
    //Original name: IEAI9901-AREA
    private Ieai9901Area ieai9901Area;
    //Original name: IEAO9901-AREA
    private Ieao9901Area ieao9901Area;

    //==== METHODS ====
    /**Original name: MAIN_SUBROUTINE<br>*/
    public long execute(AreaIdsv0001 areaIdsv0001, Ieai9901Area ieai9901Area, Ieao9901Area ieao9901Area) {
        this.areaIdsv0001 = areaIdsv0001;
        this.ieai9901Area = ieai9901Area;
        this.ieao9901Area = ieao9901Area;
        principale();
        principaleFine();
        return 0;
    }

    public static Ieas9900 getInstance() {
        return ((Ieas9900)Programs.getInstance(Ieas9900.class));
    }

    /**Original name: 1000-PRINCIPALE<br>
	 * <pre>****************************************************************
	 *                                                                *
	 *                    1000-PRINCIPALE                             *
	 *                                                                *
	 * ****************************************************************</pre>*/
    private void principale() {
        // COB_CODE: PERFORM 2000-APERTURA
        //              THRU 2000-APERTURA-FINE.
        apertura();
        // COB_CODE:      IF SI-CORRETTO
        //           *        PERFORM 3000-PROCESSA
        //           *           THRU 3000-PROCESSA-FINE.
        //                      THRU 4000-PROCESSA-FINE.
        if (ws.getSwSwitch().isSwCorretto()) {
            //        PERFORM 3000-PROCESSA
            //           THRU 3000-PROCESSA-FINE.
            // COB_CODE: PERFORM 4000-PROCESSA
            //              THRU 4000-PROCESSA-FINE.
            processa();
        }
    }

    /**Original name: 1000-PRINCIPALE-FINE<br>
	 * <pre>     MOVE SPACES          TO IEAI9901-COD-SERVIZIO-BE
	 *                             IEAI9901-LABEL-ERR
	 *                              IEAI9901-PARAMETRI-ERR.
	 *      MOVE ZEROES          TO IEAI9901-COD-ERRORE.</pre>*/
    private void principaleFine() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    /**Original name: 2000-APERTURA<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *                    2000-APERTURA                                *
	 *                                                                 *
	 *  INITIALIZZAZIONI AREE E VALIDAZIONE FORMALE DEI DATI DI INPUT  *
	 * *****************************************************************
	 * - INIZIALIZZO SWITCH E VARIABILI</pre>*/
    private void apertura() {
        // COB_CODE: SET SI-CORRETTO                  TO TRUE.
        ws.getSwSwitch().setSwCorretto(true);
        // COB_CODE: SET ERRORE-TROVATO-NO            TO TRUE.
        ws.getSwSwitch().getSwErroreTrovato().setNo();
        // COB_CODE: MOVE 1                           TO IX-IND.
        ws.setIxInd(((short)1));
        // COB_CODE: PERFORM 2010-VALIDAZIONE-DATI
        //              THRU 2010-VALIDAZIONE-DATI-FINE.
        validazioneDati();
    }

    /**Original name: 2010-VALIDAZIONE-DATI<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *                  2010-VALIDAZIONE-DATI.                         *
	 *                                                                 *
	 *  VALIDAZIONE DEI CAMPI IN INPUT                                 *
	 * *****************************************************************</pre>*/
    private void validazioneDati() {
        // COB_CODE: IF IEAI9901-COD-SERVIZIO-BE = SPACES  OR
        //              IEAI9901-COD-SERVIZIO-BE = LOW-VALUE
        //                                      TO IEAO9901-DESC-ERRORE-ESTESA.
        if (Characters.EQ_SPACE.test(ieai9901Area.getCodServizioBe()) || Characters.EQ_LOW.test(ieai9901Area.getCodServizioBeFormatted())) {
            // COB_CODE: SET NO-CORRETTO         TO TRUE
            ws.getSwSwitch().setSwCorretto(false);
            // COB_CODE: MOVE 99999              TO IEAO9901-LIV-GRAVITA
            ieao9901Area.setLivGravita(99999);
            // COB_CODE: MOVE 01                 TO IEAO9901-COD-ERRORE-990
            ieao9901Area.setCodErrore990(1);
            // COB_CODE: MOVE '2010-VALIDAZIONE-DATI'
            //                                   TO IEAO9901-LABEL-ERR-990
            ieao9901Area.setLabelErr990("2010-VALIDAZIONE-DATI");
            // COB_CODE: MOVE 'CODICE SERVIZIO NON VALORIZZATO'
            //                                   TO IEAO9901-DESC-ERRORE-ESTESA.
            ieao9901Area.setDescErroreEstesa("CODICE SERVIZIO NON VALORIZZATO");
        }
        // COB_CODE: IF SI-CORRETTO
        //              END-IF
        //           END-IF.
        if (ws.getSwSwitch().isSwCorretto()) {
            // COB_CODE: IF IEAI9901-LABEL-ERR = SPACES    OR
            //              IEAI9901-LABEL-ERR = LOW-VALUE
            //                                   TO IEAO9901-DESC-ERRORE-ESTESA
            //           END-IF
            if (Characters.EQ_SPACE.test(ieai9901Area.getLabelErr()) || Characters.EQ_LOW.test(ieai9901Area.getLabelErr(), Ieai9901Area.Len.LABEL_ERR)) {
                // COB_CODE: SET NO-CORRETTO      TO TRUE
                ws.getSwSwitch().setSwCorretto(false);
                // COB_CODE: MOVE 02              TO IEAO9901-COD-ERRORE-990
                ieao9901Area.setCodErrore990(2);
                // COB_CODE: MOVE '2010-VALIDAZIONE-DATI'
                //                                TO IEAO9901-LABEL-ERR-990
                ieao9901Area.setLabelErr990("2010-VALIDAZIONE-DATI");
                // COB_CODE: MOVE 'LABEL ERRORE NON VALORIZZATO'
                //                                TO IEAO9901-DESC-ERRORE-ESTESA
                ieao9901Area.setDescErroreEstesa("LABEL ERRORE NON VALORIZZATO");
            }
        }
        // COB_CODE: IF SI-CORRETTO
        //              END-IF
        //           END-IF.
        if (ws.getSwSwitch().isSwCorretto()) {
            // COB_CODE: IF IEAI9901-COD-ERRORE = SPACES    OR
            //              IEAI9901-COD-ERRORE = LOW-VALUE
            //                                   TO IEAO9901-DESC-ERRORE-ESTESA
            //           END-IF
            if (Characters.EQ_SPACE.test(ieai9901Area.getCodErroreFormatted()) || Characters.EQ_LOW.test(ieai9901Area.getCodErroreFormatted())) {
                // COB_CODE: SET NO-CORRETTO      TO TRUE
                ws.getSwSwitch().setSwCorretto(false);
                // COB_CODE: MOVE 03              TO IEAO9901-COD-ERRORE-990
                ieao9901Area.setCodErrore990(3);
                // COB_CODE: MOVE '2010-VALIDAZIONE-DATI'
                //                                TO IEAO9901-LABEL-ERR-990
                ieao9901Area.setLabelErr990("2010-VALIDAZIONE-DATI");
                // COB_CODE: MOVE 'CODICE ERRORE NON VALORIZZATO'
                //                                TO IEAO9901-DESC-ERRORE-ESTESA
                ieao9901Area.setDescErroreEstesa("CODICE ERRORE NON VALORIZZATO");
            }
        }
        // COB_CODE: PERFORM 2015-INIZIALIZZA THRU 2015-INIZIALIZZA-EX.
        inizializza();
    }

    /**Original name: 4000-PROCESSA<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *                    4000-PROCESSA                                *
	 *                                                                 *
	 *  -- RICERCA GRAVITA' ERRORE MEDIANTE L'ACCESSO ALLA TABELLA     *
	 *     GRAVITA_ERRORE.                                             *
	 *                                                                 *
	 * *****************************************************************</pre>*/
    private void processa() {
        ConcatUtil concatUtil = null;
        // COB_CODE: SET NO-TROVATO                    TO TRUE.
        ws.getSwSwitch().setSwTrovato(false);
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA  TO WS-COMPAGNIA.
        ws.getWsVariabili().setCompagniaFormatted(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAniaFormatted());
        // COB_CODE: PERFORM 3800-SEARCH-GRAVITA
        //              THRU 3800-SEARCH-GRAVITA-EX.
        searchGravita();
        // COB_CODE: IF SI-CORRETTO
        //              END-IF
        //           END-IF.
        if (ws.getSwSwitch().isSwCorretto()) {
            // COB_CODE: IF IX-IND > WN-ACCESSI AND
            //              NO-TROVATO
            //              SET NO-CORRETTO      TO TRUE
            //            ELSE
            //              END-IF
            //           END-IF
            if (ws.getIxInd() > ws.getCostanti().getWnAccessi() && !ws.getSwSwitch().isSwTrovato()) {
                // COB_CODE: MOVE 99999           TO IEAO9901-LIV-GRAVITA
                ieao9901Area.setLivGravita(99999);
                // COB_CODE: MOVE 03              TO IEAO9901-COD-ERRORE-990
                ieao9901Area.setCodErrore990(3);
                // COB_CODE: MOVE '3000-PROCESSA' TO IEAO9901-LABEL-ERR-990
                ieao9901Area.setLabelErr990("3000-PROCESSA");
                // COB_CODE: STRING 'OCCORRENZA NON TROVATA SU GRAVITA_ERRORE'
                //                    DELIMITED BY SIZE
                //                    ' - COMPAGNIA :'
                //                    DELIMITED BY SIZE
                //                    WS-COMPAGNIA
                //                    DELIMITED BY SIZE
                //                    ' - COD ERR : '
                //                    DELIMITED BY SIZE
                //                    IEAI9901-COD-ERRORE
                //                    DELIMITED BY SIZE
                //                    INTO
                //                    IEAO9901-DESC-ERRORE-ESTESA
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Ieao9901Area.Len.DESC_ERRORE_ESTESA, "OCCORRENZA NON TROVATA SU GRAVITA_ERRORE", " - COMPAGNIA :", ws.getWsVariabili().getCompagniaAsString(), " - COD ERR : ", ieai9901Area.getCodErroreAsString());
                ieao9901Area.setDescErroreEstesa(concatUtil.replaceInString(ieao9901Area.getDescErroreEstesaFormatted()));
                // COB_CODE: SET NO-CORRETTO      TO TRUE
                ws.getSwSwitch().setSwCorretto(false);
            }
            else if (ws.getSwSwitch().isSwTrovato() && ws.getSwSwitch().getSwErroreTrovato().isNo()) {
                // COB_CODE: IF  SI-TROVATO
                //           AND ERRORE-TROVATO-NO
                //                 THRU 3200-SELEZ-DESC-ERRORE-FINE
                //           END-IF
                // COB_CODE: PERFORM 3200-SELEZ-DESC-ERRORE
                //              THRU 3200-SELEZ-DESC-ERRORE-FINE
                selezDescErrore();
            }
        }
        // COB_CODE: IF  SI-CORRETTO
        //              END-IF
        //           END-IF.
        if (ws.getSwSwitch().isSwCorretto()) {
            // COB_CODE: IF ERRORE-TROVATO-NO
            //                 THRU 3230-VALORIZZA-OUTPUT-FINE
            //           END-IF
            if (ws.getSwSwitch().getSwErroreTrovato().isNo()) {
                // COB_CODE: PERFORM 3230-VALORIZZA-OUTPUT
                //              THRU 3230-VALORIZZA-OUTPUT-FINE
                valorizzaOutput();
            }
        }
        // COB_CODE: IF SI-CORRETTO AND IDSV0001-BATCH
        //                 THRU 3240-SCRIVI-LOG-FINE
        //           END-IF.
        if (ws.getSwSwitch().isSwCorretto() && areaIdsv0001.getAreaComune().getModalitaEsecutiva().isIdsv0001Batch()) {
            // COB_CODE: PERFORM 3240-SCRIVI-LOG
            //              THRU 3240-SCRIVI-LOG-FINE
            scriviLog();
        }
        // COB_CODE: IF  SI-CORRETTO
        //              END-IF
        //           END-IF.
        if (ws.getSwSwitch().isSwCorretto()) {
            // COB_CODE: IF ERRORE-TROVATO-NO
            //                 THRU 3270-VALORIZZA-TAB-FINE
            //           END-IF
            if (ws.getSwSwitch().getSwErroreTrovato().isNo()) {
                // COB_CODE: PERFORM 3270-VALORIZZA-TAB
                //              THRU 3270-VALORIZZA-TAB-FINE
                valorizzaTab();
            }
        }
    }

    /**Original name: 3800-SEARCH-GRAVITA<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *                    3800-RICERCA-GRAVITA                         *
	 *                                                                 *
	 * *****************************************************************</pre>*/
    private void searchGravita() {
        ConcatUtil concatUtil = null;
        // COB_CODE: IF  WK-ERRORI-NUM-MAX-ELE > 0
        //           AND IEAI9901-PARAMETRI-ERR = SPACES
        //              END-PERFORM
        //           END-IF.
        if (ws.getWkErroriNumMaxEle() > 0 && Characters.EQ_SPACE.test(ieai9901Area.getParametriErr())) {
            // COB_CODE: PERFORM VARYING IX-TAB-ERR FROM 1 BY 1
            //           UNTIL IX-TAB-ERR > WK-ERRORI-NUM-MAX-ELE
            //           OR ERRORE-TROVATO-SI
            //             END-IF
            //           END-PERFORM
            ws.setIxTabErr(((short)1));
            while (!(ws.getIxTabErr() > ws.getWkErroriNumMaxEle() || ws.getSwSwitch().getSwErroreTrovato().isSi())) {
                // COB_CODE: IF IEAI9901-COD-ERRORE = WK-COD-ERRORE(IX-TAB-ERR)
                //                TO IEAO9901-LABEL-ERR-990
                //           END-IF
                if (ieai9901Area.getCodErrore() == ws.getWkTabellaErrori(ws.getIxTabErr()).getCodErrore()) {
                    // COB_CODE: SET  ERRORE-TROVATO-SI TO TRUE
                    ws.getSwSwitch().getSwErroreTrovato().setSi();
                    // COB_CODE: MOVE WK-ID-GRAVITA-ERRORE(IX-TAB-ERR)
                    //             TO IEAO9901-ID-GRAVITA-ERRORE
                    ieao9901Area.setIdGravitaErroreFormatted(ws.getWkTabellaErrori(ws.getIxTabErr()).getIdGravitaErroreFormatted());
                    // COB_CODE: MOVE WK-LIV-GRAVITA(IX-TAB-ERR)
                    //             TO IEAO9901-LIV-GRAVITA
                    ieao9901Area.setLivGravita(ws.getWkTabellaErrori(ws.getIxTabErr()).getLivGravita());
                    // COB_CODE: MOVE WK-TIPO-TRATT-FE(IX-TAB-ERR)
                    //             TO IEAO9901-TIPO-TRATT-FE
                    ieao9901Area.setTipoTrattFe(ws.getWkTabellaErrori(ws.getIxTabErr()).getTipoTrattFe());
                    // COB_CODE: MOVE WK-DESC-ERRORE-BREVE(IX-TAB-ERR)
                    //             TO IEAO9901-DESC-ERRORE-BREVE
                    ieao9901Area.setDescErroreBreve(ws.getWkTabellaErrori(ws.getIxTabErr()).getDescErroreBreve());
                    // COB_CODE: MOVE WK-DESC-ERRORE-ESTESA(IX-TAB-ERR)
                    //             TO IEAO9901-DESC-ERRORE-ESTESA
                    ieao9901Area.setDescErroreEstesa(ws.getWkTabellaErrori(ws.getIxTabErr()).getDescErroreEstesa());
                    // COB_CODE: MOVE WK-COD-ERRORE-990(IX-TAB-ERR)
                    //             TO IEAO9901-COD-ERRORE-990
                    ieao9901Area.setCodErrore990Formatted(ws.getWkTabellaErrori(ws.getIxTabErr()).getCodErrore990Formatted());
                    // COB_CODE: MOVE WK-LABEL-ERR-990(IX-TAB-ERR)
                    //             TO IEAO9901-LABEL-ERR-990
                    ieao9901Area.setLabelErr990(ws.getWkTabellaErrori(ws.getIxTabErr()).getLabelErr990());
                }
                ws.setIxTabErr(Trunc.toShort(ws.getIxTabErr() + 1, 2));
            }
        }
        // COB_CODE: IF  ERRORE-TROVATO-NO
        //               END-EVALUATE
        //           END-IF.
        if (ws.getSwSwitch().getSwErroreTrovato().isNo()) {
            // COB_CODE: MOVE WS-COMPAGNIA              TO GER-COD-COMPAGNIA-ANIA
            ws.getGravitaErrore().setCodCompagniaAnia(ws.getWsVariabili().getCompagnia());
            // COB_CODE: MOVE IEAI9901-COD-ERRORE       TO GER-COD-ERRORE
            ws.getGravitaErrore().setCodErrore(ieai9901Area.getCodErrore());
            // COB_CODE: EXEC SQL
            //             SELECT ID_GRAVITA_ERRORE,
            //                    LIVELLO_GRAVITA,
            //                    TIPO_TRATT_FE
            //               INTO :GER-ID-GRAVITA-ERRORE
            //                   ,:GER-LIVELLO-GRAVITA
            //                   ,:GER-TIPO-TRATT-FE
            //               FROM GRAVITA_ERRORE
            //              WHERE COD_COMPAGNIA_ANIA   = :GER-COD-COMPAGNIA-ANIA
            //                AND COD_ERRORE           = :GER-COD-ERRORE
            //                FETCH FIRST ROW ONLY
            //           END-EXEC
            gravitaErroreDao.selectRec(ws.getGravitaErrore().getCodCompagniaAnia(), ws.getGravitaErrore().getCodErrore(), ws.getGravitaErrore());
            // COB_CODE: EVALUATE SQLCODE
            //               WHEN ZEROES
            //                    SET SI-TROVATO        TO TRUE
            //               WHEN +100
            //                    SET NO-TROVATO        TO TRUE
            //               WHEN OTHER
            //                    SET NO-CORRETTO       TO TRUE
            //           END-EVALUATE
            if (sqlca.getSqlcode() == 0) {
                // COB_CODE: SET SI-TROVATO        TO TRUE
                ws.getSwSwitch().setSwTrovato(true);
            }
            else if (sqlca.getSqlcode() == 100) {
                // COB_CODE: SET NO-TROVATO        TO TRUE
                ws.getSwSwitch().setSwTrovato(false);
            }
            else {
                // COB_CODE: MOVE 04               TO IEAO9901-COD-ERRORE-990
                ieao9901Area.setCodErrore990(4);
                // COB_CODE: MOVE '3120-ACCEDI-GRAVITA'
                //                                 TO IEAO9901-LABEL-ERR-990
                ieao9901Area.setLabelErr990("3120-ACCEDI-GRAVITA");
                // COB_CODE: MOVE SQLCODE          TO WS-SQLCODE
                ws.setWsSqlcode(sqlca.getSqlcode());
                // COB_CODE: STRING 'ERRORE ACCESSO GRAVITA_ERRORE SQLCODE: '
                //               WS-SQLCODE DELIMITED BY SIZE
                //             INTO IEAO9901-DESC-ERRORE-ESTESA
                concatUtil = ConcatUtil.buildString(Ieao9901Area.Len.DESC_ERRORE_ESTESA, "ERRORE ACCESSO GRAVITA_ERRORE SQLCODE: ", ws.getWsSqlcodeAsString());
                ieao9901Area.setDescErroreEstesa(concatUtil.replaceInString(ieao9901Area.getDescErroreEstesaFormatted()));
                // COB_CODE: SET NO-CORRETTO       TO TRUE
                ws.getSwSwitch().setSwCorretto(false);
            }
        }
    }

    /**Original name: 3200-SELEZ-DESC-ERRORE<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *                    3200-SELEZ-DESC-ERRORE                       *
	 *                                                                 *
	 * *****************************************************************</pre>*/
    private void selezDescErrore() {
        // COB_CODE: PERFORM 3210-ACCEDI-LNGERR
        //              THRU 3210-ACCEDI-LNGERR-FINE.
        accediLngerr();
        // COB_CODE: IF SI-CORRETTO
        //                 THRU 3220-SOST-PLACEHOLDER-FINE.
        if (ws.getSwSwitch().isSwCorretto()) {
            // COB_CODE: PERFORM 3220-SOST-PLACEHOLDER
            //              THRU 3220-SOST-PLACEHOLDER-FINE.
            sostPlaceholder();
        }
    }

    /**Original name: 3210-ACCEDI-LNGERR<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *                    3210-ACCEDI-LNGERR                           *
	 *                                                                 *
	 * *****************************************************************</pre>*/
    private void accediLngerr() {
        // COB_CODE: MOVE IDSV0001-LINGUA           TO LER-LINGUA.
        ws.getLinguaErrore().setLingua(areaIdsv0001.getAreaComune().getLingua());
        // COB_CODE: MOVE IDSV0001-PAESE            TO LER-PAESE.
        ws.getLinguaErrore().setPaese(areaIdsv0001.getAreaComune().getPaese());
        // COB_CODE: INITIALIZE LER-DESC-ERRORE-BREVE
        //                      LER-DESC-ERRORE-ESTESA
        ws.getLinguaErrore().setDescErroreBreve("");
        ws.getLinguaErrore().setDescErroreEstesa("");
        // COB_CODE: EXEC SQL
        //             SELECT DESC_ERRORE_BREVE,
        //                    DESC_ERRORE_ESTESA
        //               INTO :LER-DESC-ERRORE-BREVE-VCHAR
        //                   ,:LER-DESC-ERRORE-ESTESA-VCHAR
        //               FROM LINGUA_ERRORE
        //              WHERE COD_ERRORE         = :GER-COD-ERRORE
        //                AND COD_COMPAGNIA_ANIA = :GER-COD-COMPAGNIA-ANIA
        //                AND LINGUA             = :LER-LINGUA
        //                AND PAESE              = :LER-PAESE
        //           END-EXEC.
        linguaErroreDao.selectRec(ws.getGravitaErrore().getCodErrore(), ws.getGravitaErrore().getCodCompagniaAnia(), ws.getLinguaErrore().getLingua(), ws.getLinguaErrore().getPaese(), ws.getLinguaErrore());
        // COB_CODE: EVALUATE SQLCODE
        //               WHEN ZEROES
        //                    CONTINUE
        //               WHEN +100
        //                                          TO IEAO9901-DESC-ERRORE-ESTESA
        //               WHEN OTHER
        //                                          TO IEAO9901-DESC-ERRORE-ESTESA
        //           END-EVALUATE.
        if (sqlca.getSqlcode() == 0) {
        // COB_CODE: CONTINUE
        //continue
        }
        else if (sqlca.getSqlcode() == 100) {
            // COB_CODE: SET NO-CORRETTO       TO TRUE
            ws.getSwSwitch().setSwCorretto(false);
            // COB_CODE: MOVE 04               TO IEAO9901-COD-ERRORE-990
            ieao9901Area.setCodErrore990(4);
            // COB_CODE: MOVE '3210-ACCEDI-LNGERR'
            //                                 TO IEAO9901-LABEL-ERR-990
            ieao9901Area.setLabelErr990("3210-ACCEDI-LNGERR");
            // COB_CODE: MOVE 'DESCRIZIONE ASSENTE IN LINGUA_ERRORE'
            //                                 TO IEAO9901-DESC-ERRORE-ESTESA
            ieao9901Area.setDescErroreEstesa("DESCRIZIONE ASSENTE IN LINGUA_ERRORE");
        }
        else {
            // COB_CODE: SET NO-CORRETTO       TO TRUE
            ws.getSwSwitch().setSwCorretto(false);
            // COB_CODE: MOVE 04               TO IEAO9901-COD-ERRORE-990
            ieao9901Area.setCodErrore990(4);
            // COB_CODE: MOVE '3210-ACCEDI-LNGERR'
            //                                 TO IEAO9901-LABEL-ERR-990
            ieao9901Area.setLabelErr990("3210-ACCEDI-LNGERR");
            // COB_CODE: MOVE 'ERRORE ACCESSO LINGUA_ERRORE'
            //                                 TO IEAO9901-DESC-ERRORE-ESTESA
            ieao9901Area.setDescErroreEstesa("ERRORE ACCESSO LINGUA_ERRORE");
        }
    }

    /**Original name: 3220-SOST-PLACEHOLDER<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *                    3220-SOST-PLACEHOLDER                        *
	 *                                                                 *
	 * *****************************************************************
	 *  USO IL PUNTO DAT PERCHE' NON COMPRENDE LA LUNGHEZZA</pre>*/
    private void sostPlaceholder() {
        Ieas9800 ieas9800 = null;
        // COB_CODE: MOVE LER-DESC-ERRORE-ESTESA   TO IEAI9801-DESC-ERRORE-ESTESA.
        ws.getIeai9801Area().setDescErroreEstesa(ws.getLinguaErrore().getDescErroreEstesa());
        // COB_CODE: MOVE IEAI9901-PARAMETRI-ERR   TO IEAI9801-PARAMETRI-ERR.
        ws.getIeai9801Area().setParametriErr(ieai9901Area.getParametriErr());
        // COB_CODE: CALL LT-IEAS9800                USING AREA-IDSV0001
        //                                                 IEAI9801-AREA
        //                                                 IEAO9801-AREA.
        ieas9800 = Ieas9800.getInstance();
        ieas9800.run(areaIdsv0001, ws.getIeai9801Area(), ws.getIeao9801Area());
        // COB_CODE: IF IEAO9801-COD-ERRORE-990 = ZERO
        //              MOVE IEAO9801-AREA         TO WS-DESC-ESTESA
        //           ELSE
        //                                         TO IEAO9901-DESC-ERRORE-ESTESA
        //           END-IF.
        if (Characters.EQ_ZERO.test(ws.getIeao9801Area().getCodErrore990Formatted())) {
            // COB_CODE: MOVE ZEROES                TO IEAO9901-COD-ERRORE-990
            ieao9901Area.setCodErrore990(0);
            // COB_CODE: MOVE IEAO9801-AREA         TO WS-DESC-ESTESA
            ws.getWsVariabili().setDescEstesa(ws.getIeao9801Area().getIeao9801AreaFormatted());
        }
        else {
            // COB_CODE: MOVE 04                    TO IEAO9901-COD-ERRORE-990
            ieao9901Area.setCodErrore990(4);
            // COB_CODE: MOVE '3220-SOST-PLACEHOLDER'
            //                                      TO IEAO9901-LABEL-ERR-990
            ieao9901Area.setLabelErr990("3220-SOST-PLACEHOLDER");
            // COB_CODE: MOVE 'ERRORE ROUTINE IEAS9800'
            //                                      TO IEAO9901-DESC-ERRORE-ESTESA
            ieao9901Area.setDescErroreEstesa("ERRORE ROUTINE IEAS9800");
        }
    }

    /**Original name: 3230-VALORIZZA-OUTPUT<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *                    3230-VALORIZZA-OUTPUT                        *
	 *                                                                 *
	 * *****************************************************************</pre>*/
    private void valorizzaOutput() {
        // COB_CODE: MOVE GER-ID-GRAVITA-ERRORE    TO IEAO9901-ID-GRAVITA-ERRORE.
        ieao9901Area.setIdGravitaErrore(TruncAbs.toInt(ws.getGravitaErrore().getIdGravitaErrore(), 9));
        // COB_CODE: MOVE GER-LIVELLO-GRAVITA      TO IEAO9901-LIV-GRAVITA.
        ieao9901Area.setLivGravita(ws.getGravitaErrore().getLivelloGravita());
        // COB_CODE: MOVE GER-TIPO-TRATT-FE        TO IEAO9901-TIPO-TRATT-FE.
        ieao9901Area.setTipoTrattFe(ws.getGravitaErrore().getTipoTrattFe());
        // COB_CODE: MOVE IDSV0001-LABEL-ERR       TO IEAO9901-LABEL-ERR-990.
        ieao9901Area.setLabelErr990(areaIdsv0001.getLogErrore().getLabelErr());
        // COB_CODE: MOVE LER-DESC-ERRORE-BREVE    TO IEAO9901-DESC-ERRORE-BREVE.
        ieao9901Area.setDescErroreBreve(ws.getLinguaErrore().getDescErroreBreve());
        // COB_CODE: MOVE WS-DESC-ESTESA           TO IEAO9901-DESC-ERRORE-ESTESA.
        ieao9901Area.setDescErroreEstesa(ws.getWsVariabili().getDescEstesa());
    }

    /**Original name: 3270-VALORIZZA-TAB<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *                    3270-VALORIZZA-TAB                           *
	 *                                                                 *
	 * *****************************************************************</pre>*/
    private void valorizzaTab() {
        // COB_CODE: IF WK-ERRORI-NUM-MAX-ELE < 20
        //                TO WK-LABEL-ERR-990(WK-ERRORI-NUM-MAX-ELE)
        //           END-IF.
        if (ws.getWkErroriNumMaxEle() < 20) {
            // COB_CODE: ADD 1 TO WK-ERRORI-NUM-MAX-ELE
            ws.setWkErroriNumMaxEle(Trunc.toShort(1 + ws.getWkErroriNumMaxEle(), 2));
            // COB_CODE: MOVE IEAI9901-COD-ERRORE
            //             TO WK-COD-ERRORE(WK-ERRORI-NUM-MAX-ELE)
            ws.getWkTabellaErrori(ws.getWkErroriNumMaxEle()).setCodErroreFormatted(ieai9901Area.getCodErroreFormatted());
            // COB_CODE: MOVE IEAO9901-ID-GRAVITA-ERRORE
            //             TO WK-ID-GRAVITA-ERRORE(WK-ERRORI-NUM-MAX-ELE)
            ws.getWkTabellaErrori(ws.getWkErroriNumMaxEle()).setIdGravitaErroreFormatted(ieao9901Area.getIdGravitaErroreFormatted());
            // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
            //             TO WK-LIV-GRAVITA(WK-ERRORI-NUM-MAX-ELE)
            ws.getWkTabellaErrori(ws.getWkErroriNumMaxEle()).setLivGravita(ieao9901Area.getLivGravita());
            // COB_CODE: MOVE IEAO9901-TIPO-TRATT-FE
            //             TO WK-TIPO-TRATT-FE(WK-ERRORI-NUM-MAX-ELE)
            ws.getWkTabellaErrori(ws.getWkErroriNumMaxEle()).setTipoTrattFe(ieao9901Area.getTipoTrattFe());
            // COB_CODE: MOVE IEAO9901-DESC-ERRORE-BREVE
            //             TO WK-DESC-ERRORE-BREVE(WK-ERRORI-NUM-MAX-ELE)
            ws.getWkTabellaErrori(ws.getWkErroriNumMaxEle()).setDescErroreBreve(ieao9901Area.getDescErroreBreve());
            // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
            //             TO WK-DESC-ERRORE-ESTESA(WK-ERRORI-NUM-MAX-ELE)
            ws.getWkTabellaErrori(ws.getWkErroriNumMaxEle()).setDescErroreEstesa(ieao9901Area.getDescErroreEstesa());
            // COB_CODE: MOVE IEAO9901-COD-ERRORE-990
            //             TO WK-COD-ERRORE-990(WK-ERRORI-NUM-MAX-ELE)
            ws.getWkTabellaErrori(ws.getWkErroriNumMaxEle()).setCodErrore990Formatted(ieao9901Area.getCodErrore990Formatted());
            // COB_CODE: MOVE IEAO9901-LABEL-ERR-990
            //             TO WK-LABEL-ERR-990(WK-ERRORI-NUM-MAX-ELE)
            ws.getWkTabellaErrori(ws.getWkErroriNumMaxEle()).setLabelErr990(ieao9901Area.getLabelErr990());
        }
    }

    /**Original name: 3240-SCRIVI-LOG<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *                    3240-SCRIVI-LOG                              *
	 *                                                                 *
	 * *****************************************************************</pre>*/
    private void scriviLog() {
        Ieas9700 ieas9700 = null;
        // COB_CODE: MOVE SPACES                   TO IEAI9701-AREA
        //                                            IEAO9701-AREA.
        ws.getIeai9701Area().initIeai9701AreaSpaces();
        ws.getIeao9701Area().initIeao9701AreaSpaces();
        // COB_CODE: MOVE  WS-DESC-ESTESA          TO IDSV0001-DESC-ERRORE-ESTESA
        //                                         OF IDSV0001-LOG-ERRORE
        areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getWsVariabili().getDescEstesa());
        // COB_CODE: MOVE GER-ID-GRAVITA-ERRORE    TO IEAI9701-ID-GRAVITA-ERRORE.
        ws.getIeai9701Area().setIeai9701IdGravitaErrore(TruncAbs.toInt(ws.getGravitaErrore().getIdGravitaErrore(), 9));
        // COB_CODE: CALL LT-IEAS9700           USING AREA-IDSV0001
        //                                            IEAI9701-AREA
        //                                            IEAO9701-AREA.
        ieas9700 = Ieas9700.getInstance();
        ieas9700.run(areaIdsv0001, ws.getIeai9701Area(), ws.getIeao9701Area());
        // COB_CODE: IF IEAO9701-COD-ERRORE-990 = ZERO
        //              MOVE ZEROES                TO IEAO9901-COD-ERRORE-990
        //           ELSE
        //                                         TO IEAO9901-DESC-ERRORE-ESTESA
        //           END-IF.
        if (Characters.EQ_ZERO.test(ws.getIeao9701Area().getCodErrore990Formatted())) {
            // COB_CODE: MOVE ZEROES                TO IEAO9901-COD-ERRORE-990
            ieao9901Area.setCodErrore990(0);
        }
        else {
            // COB_CODE: MOVE 04                    TO IEAO9901-COD-ERRORE-990
            ieao9901Area.setCodErrore990(4);
            // COB_CODE: MOVE '3240-SCRIVI-LOG'     TO IEAO9901-LABEL-ERR-990
            ieao9901Area.setLabelErr990("3240-SCRIVI-LOG");
            // COB_CODE: MOVE 'ERRORE ROUTINE IEAS9700'
            //                                      TO IEAO9901-DESC-ERRORE-ESTESA
            ieao9901Area.setDescErroreEstesa("ERRORE ROUTINE IEAS9700");
        }
    }

    /**Original name: 2015-INIZIALIZZA<br>
	 * <pre>----> INIZIALIZZO TUTTI I CAMPI DESTINATI A VALORIZZARE LE
	 * ----> VARIABILI HOST, CON I VALORI DI DEFAULT
	 * ----> ( : SPAZIO PER I CHAR , ZERO PER I NUMERICI)</pre>*/
    private void inizializza() {
        // COB_CODE: IF IDSV0001-COD-COMPAGNIA-ANIA = LOW-VALUE
        //              MOVE ZEROES TO IDSV0001-COD-COMPAGNIA-ANIA
        //           END-IF.
        if (Characters.EQ_LOW.test(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAniaFormatted())) {
            // COB_CODE: MOVE ZEROES TO IDSV0001-COD-COMPAGNIA-ANIA
            areaIdsv0001.getAreaComune().setIdsv0001CodCompagniaAnia(0);
        }
        // COB_CODE: IF IEAI9901-COD-ERRORE = LOW-VALUE
        //              MOVE ZEROES TO IEAI9901-COD-ERRORE
        //           END-IF.
        if (Characters.EQ_LOW.test(ieai9901Area.getCodErroreFormatted())) {
            // COB_CODE: MOVE ZEROES TO IEAI9901-COD-ERRORE
            ieai9901Area.setCodErrore(0);
        }
        // COB_CODE: IF IDSV0001-COD-MAIN-BATCH = LOW-VALUE
        //              MOVE SPACES TO IDSV0001-COD-MAIN-BATCH
        //           END-IF.
        if (Characters.EQ_LOW.test(areaIdsv0001.getAreaComune().getCodMainBatchFormatted())) {
            // COB_CODE: MOVE SPACES TO IDSV0001-COD-MAIN-BATCH
            areaIdsv0001.getAreaComune().setCodMainBatch("");
        }
        // COB_CODE: IF IEAI9901-COD-SERVIZIO-BE = LOW-VALUE
        //              MOVE SPACES TO IEAI9901-COD-SERVIZIO-BE
        //           END-IF.
        if (Characters.EQ_LOW.test(ieai9901Area.getCodServizioBeFormatted())) {
            // COB_CODE: MOVE SPACES TO IEAI9901-COD-SERVIZIO-BE
            ieai9901Area.setCodServizioBe("");
        }
        // COB_CODE: IF IDSV0001-KEY-BUSINESS1 = LOW-VALUE
        //           END-IF.
        if (Characters.EQ_LOW.test(areaIdsv0001.getAreaComune().getIdsv0001KeyBusiness1Formatted())) {
            // COB_CODE: MOVE SPACES TO IDSV0001-KEY-BUSINESS1
            areaIdsv0001.getAreaComune().setKeyBusiness1("");
            // COB_CODE: IF IDSV0001-KEY-BUSINESS2 = LOW-VALUE
            //              MOVE SPACES TO IDSV0001-KEY-BUSINESS2
            //           END-IF.
            if (Characters.EQ_LOW.test(areaIdsv0001.getAreaComune().getIdsv0001KeyBusiness2Formatted())) {
                // COB_CODE: MOVE SPACES TO IDSV0001-KEY-BUSINESS2
                areaIdsv0001.getAreaComune().setKeyBusiness2("");
            }
        }
        // COB_CODE: IF IDSV0001-KEY-BUSINESS3 = LOW-VALUE
        //              MOVE SPACES TO IDSV0001-KEY-BUSINESS3
        //           END-IF.
        if (Characters.EQ_LOW.test(areaIdsv0001.getAreaComune().getIdsv0001KeyBusiness3Formatted())) {
            // COB_CODE: MOVE SPACES TO IDSV0001-KEY-BUSINESS3
            areaIdsv0001.getAreaComune().setKeyBusiness3("");
        }
        // COB_CODE: IF IDSV0001-KEY-BUSINESS4 = LOW-VALUE
        //              MOVE SPACES TO IDSV0001-KEY-BUSINESS4
        //           END-IF.
        if (Characters.EQ_LOW.test(areaIdsv0001.getAreaComune().getIdsv0001KeyBusiness4Formatted())) {
            // COB_CODE: MOVE SPACES TO IDSV0001-KEY-BUSINESS4
            areaIdsv0001.getAreaComune().setKeyBusiness4("");
        }
        // COB_CODE: IF IDSV0001-KEY-BUSINESS5 = LOW-VALUE
        //              MOVE SPACES TO IDSV0001-KEY-BUSINESS5
        //           END-IF.
        if (Characters.EQ_LOW.test(areaIdsv0001.getAreaComune().getIdsv0001KeyBusiness5Formatted())) {
            // COB_CODE: MOVE SPACES TO IDSV0001-KEY-BUSINESS5
            areaIdsv0001.getAreaComune().setKeyBusiness5("");
        }
        //PER ACCESSO A LINGUA_ERRORE
        // COB_CODE: IF IDSV0001-LINGUA = LOW-VALUE
        //              MOVE SPACES TO IDSV0001-LINGUA
        //           END-IF.
        if (Characters.EQ_LOW.test(areaIdsv0001.getAreaComune().getIdsv0001LinguaFormatted())) {
            // COB_CODE: MOVE SPACES TO IDSV0001-LINGUA
            areaIdsv0001.getAreaComune().setLingua("");
        }
        // COB_CODE: IF IDSV0001-PAESE = LOW-VALUE
        //              MOVE SPACES TO IDSV0001-PAESE
        //           END-IF.
        if (Characters.EQ_LOW.test(areaIdsv0001.getAreaComune().getIdsv0001PaeseFormatted())) {
            // COB_CODE: MOVE SPACES TO IDSV0001-PAESE
            areaIdsv0001.getAreaComune().setPaese("");
        }
    }
}
