package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.jdbc.FieldNotMappedException;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.OggCollgDao;
import it.accenture.jnais.commons.data.to.IOggCollg;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idbsoco0Data;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.OggCollgIdbsoco0;
import it.accenture.jnais.ws.redefines.OcoCarAcq;
import it.accenture.jnais.ws.redefines.OcoDtDecor;
import it.accenture.jnais.ws.redefines.OcoDtScad;
import it.accenture.jnais.ws.redefines.OcoDtUltPrePag;
import it.accenture.jnais.ws.redefines.OcoIdMoviChiu;
import it.accenture.jnais.ws.redefines.OcoImpCollg;
import it.accenture.jnais.ws.redefines.OcoImpReinvst;
import it.accenture.jnais.ws.redefines.OcoImpTrasf;
import it.accenture.jnais.ws.redefines.OcoImpTrasferito;
import it.accenture.jnais.ws.redefines.OcoPcPreTrasferito;
import it.accenture.jnais.ws.redefines.OcoPcReinvstRilievi;
import it.accenture.jnais.ws.redefines.OcoPre1aAnnualita;
import it.accenture.jnais.ws.redefines.OcoPrePerTrasf;
import it.accenture.jnais.ws.redefines.OcoRecProv;
import it.accenture.jnais.ws.redefines.OcoRisMat;
import it.accenture.jnais.ws.redefines.OcoRisZil;
import it.accenture.jnais.ws.redefines.OcoTotPre;

/**Original name: IDBSOCO0<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  09 LUG 2010.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Idbsoco0 extends Program implements IOggCollg {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private OggCollgDao oggCollgDao = new OggCollgDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Idbsoco0Data ws = new Idbsoco0Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: OGG-COLLG
    private OggCollgIdbsoco0 oggCollg;

    //==== METHODS ====
    /**Original name: PROGRAM_IDBSOCO0_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, OggCollgIdbsoco0 oggCollg) {
        this.idsv0003 = idsv0003;
        this.oggCollg = oggCollg;
        // COB_CODE: PERFORM A000-INIZIO                    THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO          THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS          THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO          THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                        a300ElaboraIdEff();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                        a400ElaboraIdpEff();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO          THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS          THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO          THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                        b300ElaboraIdCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                        b400ElaboraIdpCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                        b500ElaboraIboCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                        b600ElaboraIbsCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                        b700ElaboraIdoCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //              SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-PRIMARY-KEY
                //                 PERFORM A200-ELABORA-PK          THRU A200-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO         THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS         THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO         THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.PRIMARY_KEY:// COB_CODE: PERFORM A200-ELABORA-PK          THRU A200-EX
                        a200ElaboraPk();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO         THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS         THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO         THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Idbsoco0 getInstance() {
        return ((Idbsoco0)Programs.getInstance(Idbsoco0.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'IDBSOCO0'   TO IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("IDBSOCO0");
        // COB_CODE: MOVE 'OGG_COLLG' TO IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("OGG_COLLG");
        // COB_CODE: MOVE '00'                     TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                   TO   IDSV0003-SQLCODE
        //                                              IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                              IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-AGG-STORICO-SOLO-INS  OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isAggStoricoSoloIns() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-PK<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a200ElaboraPk() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-PK          THRU A210-EX
        //              WHEN IDSV0003-INSERT
        //                 PERFORM A220-INSERT-PK          THRU A220-EX
        //              WHEN IDSV0003-UPDATE
        //                 PERFORM A230-UPDATE-PK          THRU A230-EX
        //              WHEN IDSV0003-DELETE
        //                 PERFORM A240-DELETE-PK          THRU A240-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-PK          THRU A210-EX
            a210SelectPk();
        }
        else if (idsv0003.getOperazione().isInsert()) {
            // COB_CODE: PERFORM A220-INSERT-PK          THRU A220-EX
            a220InsertPk();
        }
        else if (idsv0003.getOperazione().isUpdate()) {
            // COB_CODE: PERFORM A230-UPDATE-PK          THRU A230-EX
            a230UpdatePk();
        }
        else if (idsv0003.getOperazione().isDelete()) {
            // COB_CODE: PERFORM A240-DELETE-PK          THRU A240-EX
            a240DeletePk();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A300-ELABORA-ID-EFF<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void a300ElaboraIdEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A310-SELECT-ID-EFF          THRU A310-EX
            a310SelectIdEff();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A400-ELABORA-IDP-EFF<br>*/
    private void a400ElaboraIdpEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
            a410SelectIdpEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
            a460OpenCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
            a470CloseCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
            a480FetchFirstIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
            a490FetchNextIdpEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A500-ELABORA-IBO<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a500ElaboraIbo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A510-SELECT-IBO             THRU A510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A510-SELECT-IBO             THRU A510-EX
            a510SelectIbo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
            a560OpenCursorIbo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
            a570CloseCursorIbo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
            a580FetchFirstIbo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
            a590FetchNextIbo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A600-ELABORA-IBS<br>*/
    private void a600ElaboraIbs() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A610-SELECT-IBS             THRU A610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A610-SELECT-IBS             THRU A610-EX
            a610SelectIbs();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
            a660OpenCursorIbs();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
            a670CloseCursorIbs();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
            a680FetchFirstIbs();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
            a690FetchNextIbs();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A700-ELABORA-IDO<br>*/
    private void a700ElaboraIdo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A710-SELECT-IDO                 THRU A710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A710-SELECT-IDO                 THRU A710-EX
            a710SelectIdo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
            a760OpenCursorIdo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
            a770CloseCursorIdo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
            a780FetchFirstIdo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
            a790FetchNextIdo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B300-ELABORA-ID-CPZ<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void b300ElaboraIdCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
            b310SelectIdCpz();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B400-ELABORA-IDP-CPZ<br>*/
    private void b400ElaboraIdpCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
            b410SelectIdpCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
            b460OpenCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
            b470CloseCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
            b480FetchFirstIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
            b490FetchNextIdpCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B500-ELABORA-IBO-CPZ<br>*/
    private void b500ElaboraIboCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
            b510SelectIboCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
            b560OpenCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
            b570CloseCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
            b580FetchFirstIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B600-ELABORA-IBS-CPZ<br>*/
    private void b600ElaboraIbsCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
            b610SelectIbsCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
            b660OpenCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
            b670CloseCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
            b680FetchFirstIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B700-ELABORA-IDO-CPZ<br>*/
    private void b700ElaboraIdoCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
            b710SelectIdoCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
            b760OpenCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
            b770CloseCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
            b780FetchFirstIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A210-SELECT-PK<br>
	 * <pre>----
	 * ----  gestione PK
	 * ----</pre>*/
    private void a210SelectPk() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_OGG_COLLG
        //                ,ID_OGG_COINV
        //                ,TP_OGG_COINV
        //                ,ID_OGG_DER
        //                ,TP_OGG_DER
        //                ,IB_RIFTO_ESTNO
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,COD_PROD
        //                ,DT_SCAD
        //                ,TP_COLLGM
        //                ,TP_TRASF
        //                ,REC_PROV
        //                ,DT_DECOR
        //                ,DT_ULT_PRE_PAG
        //                ,TOT_PRE
        //                ,RIS_MAT
        //                ,RIS_ZIL
        //                ,IMP_TRASF
        //                ,IMP_REINVST
        //                ,PC_REINVST_RILIEVI
        //                ,IB_2O_RIFTO_ESTNO
        //                ,IND_LIQ_AGG_MAN
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,IMP_COLLG
        //                ,PRE_PER_TRASF
        //                ,CAR_ACQ
        //                ,PRE_1A_ANNUALITA
        //                ,IMP_TRASFERITO
        //                ,TP_MOD_ABBINAMENTO
        //                ,PC_PRE_TRASFERITO
        //             INTO
        //                :OCO-ID-OGG-COLLG
        //               ,:OCO-ID-OGG-COINV
        //               ,:OCO-TP-OGG-COINV
        //               ,:OCO-ID-OGG-DER
        //               ,:OCO-TP-OGG-DER
        //               ,:OCO-IB-RIFTO-ESTNO
        //                :IND-OCO-IB-RIFTO-ESTNO
        //               ,:OCO-ID-MOVI-CRZ
        //               ,:OCO-ID-MOVI-CHIU
        //                :IND-OCO-ID-MOVI-CHIU
        //               ,:OCO-DT-INI-EFF-DB
        //               ,:OCO-DT-END-EFF-DB
        //               ,:OCO-COD-COMP-ANIA
        //               ,:OCO-COD-PROD
        //               ,:OCO-DT-SCAD-DB
        //                :IND-OCO-DT-SCAD
        //               ,:OCO-TP-COLLGM
        //               ,:OCO-TP-TRASF
        //                :IND-OCO-TP-TRASF
        //               ,:OCO-REC-PROV
        //                :IND-OCO-REC-PROV
        //               ,:OCO-DT-DECOR-DB
        //                :IND-OCO-DT-DECOR
        //               ,:OCO-DT-ULT-PRE-PAG-DB
        //                :IND-OCO-DT-ULT-PRE-PAG
        //               ,:OCO-TOT-PRE
        //                :IND-OCO-TOT-PRE
        //               ,:OCO-RIS-MAT
        //                :IND-OCO-RIS-MAT
        //               ,:OCO-RIS-ZIL
        //                :IND-OCO-RIS-ZIL
        //               ,:OCO-IMP-TRASF
        //                :IND-OCO-IMP-TRASF
        //               ,:OCO-IMP-REINVST
        //                :IND-OCO-IMP-REINVST
        //               ,:OCO-PC-REINVST-RILIEVI
        //                :IND-OCO-PC-REINVST-RILIEVI
        //               ,:OCO-IB-2O-RIFTO-ESTNO
        //                :IND-OCO-IB-2O-RIFTO-ESTNO
        //               ,:OCO-IND-LIQ-AGG-MAN
        //                :IND-OCO-IND-LIQ-AGG-MAN
        //               ,:OCO-DS-RIGA
        //               ,:OCO-DS-OPER-SQL
        //               ,:OCO-DS-VER
        //               ,:OCO-DS-TS-INI-CPTZ
        //               ,:OCO-DS-TS-END-CPTZ
        //               ,:OCO-DS-UTENTE
        //               ,:OCO-DS-STATO-ELAB
        //               ,:OCO-IMP-COLLG
        //                :IND-OCO-IMP-COLLG
        //               ,:OCO-PRE-PER-TRASF
        //                :IND-OCO-PRE-PER-TRASF
        //               ,:OCO-CAR-ACQ
        //                :IND-OCO-CAR-ACQ
        //               ,:OCO-PRE-1A-ANNUALITA
        //                :IND-OCO-PRE-1A-ANNUALITA
        //               ,:OCO-IMP-TRASFERITO
        //                :IND-OCO-IMP-TRASFERITO
        //               ,:OCO-TP-MOD-ABBINAMENTO
        //                :IND-OCO-TP-MOD-ABBINAMENTO
        //               ,:OCO-PC-PRE-TRASFERITO
        //                :IND-OCO-PC-PRE-TRASFERITO
        //             FROM OGG_COLLG
        //             WHERE     DS_RIGA = :OCO-DS-RIGA
        //           END-EXEC.
        oggCollgDao.selectByOcoDsRiga(oggCollg.getOcoDsRiga(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A220-INSERT-PK<br>*/
    private void a220InsertPk() {
        // COB_CODE: PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.
        z400SeqRiga();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX
            z150ValorizzaDataServicesI();
            // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX
            z200SetIndicatoriNull();
            // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX
            z900ConvertiNToX();
            // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX
            z960LengthVchar();
            // COB_CODE: EXEC SQL
            //              INSERT
            //              INTO OGG_COLLG
            //                  (
            //                     ID_OGG_COLLG
            //                    ,ID_OGG_COINV
            //                    ,TP_OGG_COINV
            //                    ,ID_OGG_DER
            //                    ,TP_OGG_DER
            //                    ,IB_RIFTO_ESTNO
            //                    ,ID_MOVI_CRZ
            //                    ,ID_MOVI_CHIU
            //                    ,DT_INI_EFF
            //                    ,DT_END_EFF
            //                    ,COD_COMP_ANIA
            //                    ,COD_PROD
            //                    ,DT_SCAD
            //                    ,TP_COLLGM
            //                    ,TP_TRASF
            //                    ,REC_PROV
            //                    ,DT_DECOR
            //                    ,DT_ULT_PRE_PAG
            //                    ,TOT_PRE
            //                    ,RIS_MAT
            //                    ,RIS_ZIL
            //                    ,IMP_TRASF
            //                    ,IMP_REINVST
            //                    ,PC_REINVST_RILIEVI
            //                    ,IB_2O_RIFTO_ESTNO
            //                    ,IND_LIQ_AGG_MAN
            //                    ,DS_RIGA
            //                    ,DS_OPER_SQL
            //                    ,DS_VER
            //                    ,DS_TS_INI_CPTZ
            //                    ,DS_TS_END_CPTZ
            //                    ,DS_UTENTE
            //                    ,DS_STATO_ELAB
            //                    ,IMP_COLLG
            //                    ,PRE_PER_TRASF
            //                    ,CAR_ACQ
            //                    ,PRE_1A_ANNUALITA
            //                    ,IMP_TRASFERITO
            //                    ,TP_MOD_ABBINAMENTO
            //                    ,PC_PRE_TRASFERITO
            //                  )
            //              VALUES
            //                  (
            //                    :OCO-ID-OGG-COLLG
            //                    ,:OCO-ID-OGG-COINV
            //                    ,:OCO-TP-OGG-COINV
            //                    ,:OCO-ID-OGG-DER
            //                    ,:OCO-TP-OGG-DER
            //                    ,:OCO-IB-RIFTO-ESTNO
            //                     :IND-OCO-IB-RIFTO-ESTNO
            //                    ,:OCO-ID-MOVI-CRZ
            //                    ,:OCO-ID-MOVI-CHIU
            //                     :IND-OCO-ID-MOVI-CHIU
            //                    ,:OCO-DT-INI-EFF-DB
            //                    ,:OCO-DT-END-EFF-DB
            //                    ,:OCO-COD-COMP-ANIA
            //                    ,:OCO-COD-PROD
            //                    ,:OCO-DT-SCAD-DB
            //                     :IND-OCO-DT-SCAD
            //                    ,:OCO-TP-COLLGM
            //                    ,:OCO-TP-TRASF
            //                     :IND-OCO-TP-TRASF
            //                    ,:OCO-REC-PROV
            //                     :IND-OCO-REC-PROV
            //                    ,:OCO-DT-DECOR-DB
            //                     :IND-OCO-DT-DECOR
            //                    ,:OCO-DT-ULT-PRE-PAG-DB
            //                     :IND-OCO-DT-ULT-PRE-PAG
            //                    ,:OCO-TOT-PRE
            //                     :IND-OCO-TOT-PRE
            //                    ,:OCO-RIS-MAT
            //                     :IND-OCO-RIS-MAT
            //                    ,:OCO-RIS-ZIL
            //                     :IND-OCO-RIS-ZIL
            //                    ,:OCO-IMP-TRASF
            //                     :IND-OCO-IMP-TRASF
            //                    ,:OCO-IMP-REINVST
            //                     :IND-OCO-IMP-REINVST
            //                    ,:OCO-PC-REINVST-RILIEVI
            //                     :IND-OCO-PC-REINVST-RILIEVI
            //                    ,:OCO-IB-2O-RIFTO-ESTNO
            //                     :IND-OCO-IB-2O-RIFTO-ESTNO
            //                    ,:OCO-IND-LIQ-AGG-MAN
            //                     :IND-OCO-IND-LIQ-AGG-MAN
            //                    ,:OCO-DS-RIGA
            //                    ,:OCO-DS-OPER-SQL
            //                    ,:OCO-DS-VER
            //                    ,:OCO-DS-TS-INI-CPTZ
            //                    ,:OCO-DS-TS-END-CPTZ
            //                    ,:OCO-DS-UTENTE
            //                    ,:OCO-DS-STATO-ELAB
            //                    ,:OCO-IMP-COLLG
            //                     :IND-OCO-IMP-COLLG
            //                    ,:OCO-PRE-PER-TRASF
            //                     :IND-OCO-PRE-PER-TRASF
            //                    ,:OCO-CAR-ACQ
            //                     :IND-OCO-CAR-ACQ
            //                    ,:OCO-PRE-1A-ANNUALITA
            //                     :IND-OCO-PRE-1A-ANNUALITA
            //                    ,:OCO-IMP-TRASFERITO
            //                     :IND-OCO-IMP-TRASFERITO
            //                    ,:OCO-TP-MOD-ABBINAMENTO
            //                     :IND-OCO-TP-MOD-ABBINAMENTO
            //                    ,:OCO-PC-PRE-TRASFERITO
            //                     :IND-OCO-PC-PRE-TRASFERITO
            //                  )
            //           END-EXEC
            oggCollgDao.insertRec(this);
            // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
            a100CheckReturnCode();
        }
    }

    /**Original name: A230-UPDATE-PK<br>*/
    private void a230UpdatePk() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE OGG_COLLG SET
        //                   ID_OGG_COLLG           =
        //                :OCO-ID-OGG-COLLG
        //                  ,ID_OGG_COINV           =
        //                :OCO-ID-OGG-COINV
        //                  ,TP_OGG_COINV           =
        //                :OCO-TP-OGG-COINV
        //                  ,ID_OGG_DER             =
        //                :OCO-ID-OGG-DER
        //                  ,TP_OGG_DER             =
        //                :OCO-TP-OGG-DER
        //                  ,IB_RIFTO_ESTNO         =
        //                :OCO-IB-RIFTO-ESTNO
        //                                       :IND-OCO-IB-RIFTO-ESTNO
        //                  ,ID_MOVI_CRZ            =
        //                :OCO-ID-MOVI-CRZ
        //                  ,ID_MOVI_CHIU           =
        //                :OCO-ID-MOVI-CHIU
        //                                       :IND-OCO-ID-MOVI-CHIU
        //                  ,DT_INI_EFF             =
        //           :OCO-DT-INI-EFF-DB
        //                  ,DT_END_EFF             =
        //           :OCO-DT-END-EFF-DB
        //                  ,COD_COMP_ANIA          =
        //                :OCO-COD-COMP-ANIA
        //                  ,COD_PROD               =
        //                :OCO-COD-PROD
        //                  ,DT_SCAD                =
        //           :OCO-DT-SCAD-DB
        //                                       :IND-OCO-DT-SCAD
        //                  ,TP_COLLGM              =
        //                :OCO-TP-COLLGM
        //                  ,TP_TRASF               =
        //                :OCO-TP-TRASF
        //                                       :IND-OCO-TP-TRASF
        //                  ,REC_PROV               =
        //                :OCO-REC-PROV
        //                                       :IND-OCO-REC-PROV
        //                  ,DT_DECOR               =
        //           :OCO-DT-DECOR-DB
        //                                       :IND-OCO-DT-DECOR
        //                  ,DT_ULT_PRE_PAG         =
        //           :OCO-DT-ULT-PRE-PAG-DB
        //                                       :IND-OCO-DT-ULT-PRE-PAG
        //                  ,TOT_PRE                =
        //                :OCO-TOT-PRE
        //                                       :IND-OCO-TOT-PRE
        //                  ,RIS_MAT                =
        //                :OCO-RIS-MAT
        //                                       :IND-OCO-RIS-MAT
        //                  ,RIS_ZIL                =
        //                :OCO-RIS-ZIL
        //                                       :IND-OCO-RIS-ZIL
        //                  ,IMP_TRASF              =
        //                :OCO-IMP-TRASF
        //                                       :IND-OCO-IMP-TRASF
        //                  ,IMP_REINVST            =
        //                :OCO-IMP-REINVST
        //                                       :IND-OCO-IMP-REINVST
        //                  ,PC_REINVST_RILIEVI     =
        //                :OCO-PC-REINVST-RILIEVI
        //                                       :IND-OCO-PC-REINVST-RILIEVI
        //                  ,IB_2O_RIFTO_ESTNO      =
        //                :OCO-IB-2O-RIFTO-ESTNO
        //                                       :IND-OCO-IB-2O-RIFTO-ESTNO
        //                  ,IND_LIQ_AGG_MAN        =
        //                :OCO-IND-LIQ-AGG-MAN
        //                                       :IND-OCO-IND-LIQ-AGG-MAN
        //                  ,DS_RIGA                =
        //                :OCO-DS-RIGA
        //                  ,DS_OPER_SQL            =
        //                :OCO-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :OCO-DS-VER
        //                  ,DS_TS_INI_CPTZ         =
        //                :OCO-DS-TS-INI-CPTZ
        //                  ,DS_TS_END_CPTZ         =
        //                :OCO-DS-TS-END-CPTZ
        //                  ,DS_UTENTE              =
        //                :OCO-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :OCO-DS-STATO-ELAB
        //                  ,IMP_COLLG              =
        //                :OCO-IMP-COLLG
        //                                       :IND-OCO-IMP-COLLG
        //                  ,PRE_PER_TRASF          =
        //                :OCO-PRE-PER-TRASF
        //                                       :IND-OCO-PRE-PER-TRASF
        //                  ,CAR_ACQ                =
        //                :OCO-CAR-ACQ
        //                                       :IND-OCO-CAR-ACQ
        //                  ,PRE_1A_ANNUALITA       =
        //                :OCO-PRE-1A-ANNUALITA
        //                                       :IND-OCO-PRE-1A-ANNUALITA
        //                  ,IMP_TRASFERITO         =
        //                :OCO-IMP-TRASFERITO
        //                                       :IND-OCO-IMP-TRASFERITO
        //                  ,TP_MOD_ABBINAMENTO     =
        //                :OCO-TP-MOD-ABBINAMENTO
        //                                       :IND-OCO-TP-MOD-ABBINAMENTO
        //                  ,PC_PRE_TRASFERITO      =
        //                :OCO-PC-PRE-TRASFERITO
        //                                       :IND-OCO-PC-PRE-TRASFERITO
        //                WHERE     DS_RIGA = :OCO-DS-RIGA
        //           END-EXEC.
        oggCollgDao.updateRec(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A240-DELETE-PK<br>*/
    private void a240DeletePk() {
        // COB_CODE: EXEC SQL
        //                DELETE
        //                FROM OGG_COLLG
        //                WHERE     DS_RIGA = :OCO-DS-RIGA
        //           END-EXEC.
        oggCollgDao.deleteByOcoDsRiga(oggCollg.getOcoDsRiga());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A305-DECLARE-CURSOR-ID-EFF<br>
	 * <pre>----
	 * ----  gestione ID Effetto
	 * ----</pre>*/
    private void a305DeclareCursorIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-ID-UPD-EFF-OCO CURSOR FOR
        //              SELECT
        //                     ID_OGG_COLLG
        //                    ,ID_OGG_COINV
        //                    ,TP_OGG_COINV
        //                    ,ID_OGG_DER
        //                    ,TP_OGG_DER
        //                    ,IB_RIFTO_ESTNO
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,COD_PROD
        //                    ,DT_SCAD
        //                    ,TP_COLLGM
        //                    ,TP_TRASF
        //                    ,REC_PROV
        //                    ,DT_DECOR
        //                    ,DT_ULT_PRE_PAG
        //                    ,TOT_PRE
        //                    ,RIS_MAT
        //                    ,RIS_ZIL
        //                    ,IMP_TRASF
        //                    ,IMP_REINVST
        //                    ,PC_REINVST_RILIEVI
        //                    ,IB_2O_RIFTO_ESTNO
        //                    ,IND_LIQ_AGG_MAN
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,IMP_COLLG
        //                    ,PRE_PER_TRASF
        //                    ,CAR_ACQ
        //                    ,PRE_1A_ANNUALITA
        //                    ,IMP_TRASFERITO
        //                    ,TP_MOD_ABBINAMENTO
        //                    ,PC_PRE_TRASFERITO
        //              FROM OGG_COLLG
        //              WHERE     ID_OGG_COLLG = :OCO-ID-OGG-COLLG
        //                    AND DS_TS_END_CPTZ = :WS-TS-INFINITO
        //                    AND DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //              ORDER BY DT_INI_EFF ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A310-SELECT-ID-EFF<br>*/
    private void a310SelectIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_OGG_COLLG
        //                ,ID_OGG_COINV
        //                ,TP_OGG_COINV
        //                ,ID_OGG_DER
        //                ,TP_OGG_DER
        //                ,IB_RIFTO_ESTNO
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,COD_PROD
        //                ,DT_SCAD
        //                ,TP_COLLGM
        //                ,TP_TRASF
        //                ,REC_PROV
        //                ,DT_DECOR
        //                ,DT_ULT_PRE_PAG
        //                ,TOT_PRE
        //                ,RIS_MAT
        //                ,RIS_ZIL
        //                ,IMP_TRASF
        //                ,IMP_REINVST
        //                ,PC_REINVST_RILIEVI
        //                ,IB_2O_RIFTO_ESTNO
        //                ,IND_LIQ_AGG_MAN
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,IMP_COLLG
        //                ,PRE_PER_TRASF
        //                ,CAR_ACQ
        //                ,PRE_1A_ANNUALITA
        //                ,IMP_TRASFERITO
        //                ,TP_MOD_ABBINAMENTO
        //                ,PC_PRE_TRASFERITO
        //             INTO
        //                :OCO-ID-OGG-COLLG
        //               ,:OCO-ID-OGG-COINV
        //               ,:OCO-TP-OGG-COINV
        //               ,:OCO-ID-OGG-DER
        //               ,:OCO-TP-OGG-DER
        //               ,:OCO-IB-RIFTO-ESTNO
        //                :IND-OCO-IB-RIFTO-ESTNO
        //               ,:OCO-ID-MOVI-CRZ
        //               ,:OCO-ID-MOVI-CHIU
        //                :IND-OCO-ID-MOVI-CHIU
        //               ,:OCO-DT-INI-EFF-DB
        //               ,:OCO-DT-END-EFF-DB
        //               ,:OCO-COD-COMP-ANIA
        //               ,:OCO-COD-PROD
        //               ,:OCO-DT-SCAD-DB
        //                :IND-OCO-DT-SCAD
        //               ,:OCO-TP-COLLGM
        //               ,:OCO-TP-TRASF
        //                :IND-OCO-TP-TRASF
        //               ,:OCO-REC-PROV
        //                :IND-OCO-REC-PROV
        //               ,:OCO-DT-DECOR-DB
        //                :IND-OCO-DT-DECOR
        //               ,:OCO-DT-ULT-PRE-PAG-DB
        //                :IND-OCO-DT-ULT-PRE-PAG
        //               ,:OCO-TOT-PRE
        //                :IND-OCO-TOT-PRE
        //               ,:OCO-RIS-MAT
        //                :IND-OCO-RIS-MAT
        //               ,:OCO-RIS-ZIL
        //                :IND-OCO-RIS-ZIL
        //               ,:OCO-IMP-TRASF
        //                :IND-OCO-IMP-TRASF
        //               ,:OCO-IMP-REINVST
        //                :IND-OCO-IMP-REINVST
        //               ,:OCO-PC-REINVST-RILIEVI
        //                :IND-OCO-PC-REINVST-RILIEVI
        //               ,:OCO-IB-2O-RIFTO-ESTNO
        //                :IND-OCO-IB-2O-RIFTO-ESTNO
        //               ,:OCO-IND-LIQ-AGG-MAN
        //                :IND-OCO-IND-LIQ-AGG-MAN
        //               ,:OCO-DS-RIGA
        //               ,:OCO-DS-OPER-SQL
        //               ,:OCO-DS-VER
        //               ,:OCO-DS-TS-INI-CPTZ
        //               ,:OCO-DS-TS-END-CPTZ
        //               ,:OCO-DS-UTENTE
        //               ,:OCO-DS-STATO-ELAB
        //               ,:OCO-IMP-COLLG
        //                :IND-OCO-IMP-COLLG
        //               ,:OCO-PRE-PER-TRASF
        //                :IND-OCO-PRE-PER-TRASF
        //               ,:OCO-CAR-ACQ
        //                :IND-OCO-CAR-ACQ
        //               ,:OCO-PRE-1A-ANNUALITA
        //                :IND-OCO-PRE-1A-ANNUALITA
        //               ,:OCO-IMP-TRASFERITO
        //                :IND-OCO-IMP-TRASFERITO
        //               ,:OCO-TP-MOD-ABBINAMENTO
        //                :IND-OCO-TP-MOD-ABBINAMENTO
        //               ,:OCO-PC-PRE-TRASFERITO
        //                :IND-OCO-PC-PRE-TRASFERITO
        //             FROM OGG_COLLG
        //             WHERE     ID_OGG_COLLG = :OCO-ID-OGG-COLLG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        oggCollgDao.selectRec(oggCollg.getOcoIdOggCollg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A330-UPDATE-ID-EFF<br>*/
    private void a330UpdateIdEff() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE OGG_COLLG SET
        //                   ID_OGG_COLLG           =
        //                :OCO-ID-OGG-COLLG
        //                  ,ID_OGG_COINV           =
        //                :OCO-ID-OGG-COINV
        //                  ,TP_OGG_COINV           =
        //                :OCO-TP-OGG-COINV
        //                  ,ID_OGG_DER             =
        //                :OCO-ID-OGG-DER
        //                  ,TP_OGG_DER             =
        //                :OCO-TP-OGG-DER
        //                  ,IB_RIFTO_ESTNO         =
        //                :OCO-IB-RIFTO-ESTNO
        //                                       :IND-OCO-IB-RIFTO-ESTNO
        //                  ,ID_MOVI_CRZ            =
        //                :OCO-ID-MOVI-CRZ
        //                  ,ID_MOVI_CHIU           =
        //                :OCO-ID-MOVI-CHIU
        //                                       :IND-OCO-ID-MOVI-CHIU
        //                  ,DT_INI_EFF             =
        //           :OCO-DT-INI-EFF-DB
        //                  ,DT_END_EFF             =
        //           :OCO-DT-END-EFF-DB
        //                  ,COD_COMP_ANIA          =
        //                :OCO-COD-COMP-ANIA
        //                  ,COD_PROD               =
        //                :OCO-COD-PROD
        //                  ,DT_SCAD                =
        //           :OCO-DT-SCAD-DB
        //                                       :IND-OCO-DT-SCAD
        //                  ,TP_COLLGM              =
        //                :OCO-TP-COLLGM
        //                  ,TP_TRASF               =
        //                :OCO-TP-TRASF
        //                                       :IND-OCO-TP-TRASF
        //                  ,REC_PROV               =
        //                :OCO-REC-PROV
        //                                       :IND-OCO-REC-PROV
        //                  ,DT_DECOR               =
        //           :OCO-DT-DECOR-DB
        //                                       :IND-OCO-DT-DECOR
        //                  ,DT_ULT_PRE_PAG         =
        //           :OCO-DT-ULT-PRE-PAG-DB
        //                                       :IND-OCO-DT-ULT-PRE-PAG
        //                  ,TOT_PRE                =
        //                :OCO-TOT-PRE
        //                                       :IND-OCO-TOT-PRE
        //                  ,RIS_MAT                =
        //                :OCO-RIS-MAT
        //                                       :IND-OCO-RIS-MAT
        //                  ,RIS_ZIL                =
        //                :OCO-RIS-ZIL
        //                                       :IND-OCO-RIS-ZIL
        //                  ,IMP_TRASF              =
        //                :OCO-IMP-TRASF
        //                                       :IND-OCO-IMP-TRASF
        //                  ,IMP_REINVST            =
        //                :OCO-IMP-REINVST
        //                                       :IND-OCO-IMP-REINVST
        //                  ,PC_REINVST_RILIEVI     =
        //                :OCO-PC-REINVST-RILIEVI
        //                                       :IND-OCO-PC-REINVST-RILIEVI
        //                  ,IB_2O_RIFTO_ESTNO      =
        //                :OCO-IB-2O-RIFTO-ESTNO
        //                                       :IND-OCO-IB-2O-RIFTO-ESTNO
        //                  ,IND_LIQ_AGG_MAN        =
        //                :OCO-IND-LIQ-AGG-MAN
        //                                       :IND-OCO-IND-LIQ-AGG-MAN
        //                  ,DS_RIGA                =
        //                :OCO-DS-RIGA
        //                  ,DS_OPER_SQL            =
        //                :OCO-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :OCO-DS-VER
        //                  ,DS_TS_INI_CPTZ         =
        //                :OCO-DS-TS-INI-CPTZ
        //                  ,DS_TS_END_CPTZ         =
        //                :OCO-DS-TS-END-CPTZ
        //                  ,DS_UTENTE              =
        //                :OCO-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :OCO-DS-STATO-ELAB
        //                  ,IMP_COLLG              =
        //                :OCO-IMP-COLLG
        //                                       :IND-OCO-IMP-COLLG
        //                  ,PRE_PER_TRASF          =
        //                :OCO-PRE-PER-TRASF
        //                                       :IND-OCO-PRE-PER-TRASF
        //                  ,CAR_ACQ                =
        //                :OCO-CAR-ACQ
        //                                       :IND-OCO-CAR-ACQ
        //                  ,PRE_1A_ANNUALITA       =
        //                :OCO-PRE-1A-ANNUALITA
        //                                       :IND-OCO-PRE-1A-ANNUALITA
        //                  ,IMP_TRASFERITO         =
        //                :OCO-IMP-TRASFERITO
        //                                       :IND-OCO-IMP-TRASFERITO
        //                  ,TP_MOD_ABBINAMENTO     =
        //                :OCO-TP-MOD-ABBINAMENTO
        //                                       :IND-OCO-TP-MOD-ABBINAMENTO
        //                  ,PC_PRE_TRASFERITO      =
        //                :OCO-PC-PRE-TRASFERITO
        //                                       :IND-OCO-PC-PRE-TRASFERITO
        //                WHERE     DS_RIGA = :OCO-DS-RIGA
        //                   AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        oggCollgDao.updateRec1(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A360-OPEN-CURSOR-ID-EFF<br>*/
    private void a360OpenCursorIdEff() {
        // COB_CODE: PERFORM A305-DECLARE-CURSOR-ID-EFF THRU A305-EX.
        a305DeclareCursorIdEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-ID-UPD-EFF-OCO
        //           END-EXEC.
        oggCollgDao.openCIdUpdEffOco(oggCollg.getOcoIdOggCollg(), ws.getIdsv0010().getWsTsInfinito(), ws.getIdsv0010().getWsDataInizioEffettoDb(), idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A370-CLOSE-CURSOR-ID-EFF<br>*/
    private void a370CloseCursorIdEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-ID-UPD-EFF-OCO
        //           END-EXEC.
        oggCollgDao.closeCIdUpdEffOco();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A390-FETCH-NEXT-ID-EFF<br>*/
    private void a390FetchNextIdEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-ID-UPD-EFF-OCO
        //           INTO
        //                :OCO-ID-OGG-COLLG
        //               ,:OCO-ID-OGG-COINV
        //               ,:OCO-TP-OGG-COINV
        //               ,:OCO-ID-OGG-DER
        //               ,:OCO-TP-OGG-DER
        //               ,:OCO-IB-RIFTO-ESTNO
        //                :IND-OCO-IB-RIFTO-ESTNO
        //               ,:OCO-ID-MOVI-CRZ
        //               ,:OCO-ID-MOVI-CHIU
        //                :IND-OCO-ID-MOVI-CHIU
        //               ,:OCO-DT-INI-EFF-DB
        //               ,:OCO-DT-END-EFF-DB
        //               ,:OCO-COD-COMP-ANIA
        //               ,:OCO-COD-PROD
        //               ,:OCO-DT-SCAD-DB
        //                :IND-OCO-DT-SCAD
        //               ,:OCO-TP-COLLGM
        //               ,:OCO-TP-TRASF
        //                :IND-OCO-TP-TRASF
        //               ,:OCO-REC-PROV
        //                :IND-OCO-REC-PROV
        //               ,:OCO-DT-DECOR-DB
        //                :IND-OCO-DT-DECOR
        //               ,:OCO-DT-ULT-PRE-PAG-DB
        //                :IND-OCO-DT-ULT-PRE-PAG
        //               ,:OCO-TOT-PRE
        //                :IND-OCO-TOT-PRE
        //               ,:OCO-RIS-MAT
        //                :IND-OCO-RIS-MAT
        //               ,:OCO-RIS-ZIL
        //                :IND-OCO-RIS-ZIL
        //               ,:OCO-IMP-TRASF
        //                :IND-OCO-IMP-TRASF
        //               ,:OCO-IMP-REINVST
        //                :IND-OCO-IMP-REINVST
        //               ,:OCO-PC-REINVST-RILIEVI
        //                :IND-OCO-PC-REINVST-RILIEVI
        //               ,:OCO-IB-2O-RIFTO-ESTNO
        //                :IND-OCO-IB-2O-RIFTO-ESTNO
        //               ,:OCO-IND-LIQ-AGG-MAN
        //                :IND-OCO-IND-LIQ-AGG-MAN
        //               ,:OCO-DS-RIGA
        //               ,:OCO-DS-OPER-SQL
        //               ,:OCO-DS-VER
        //               ,:OCO-DS-TS-INI-CPTZ
        //               ,:OCO-DS-TS-END-CPTZ
        //               ,:OCO-DS-UTENTE
        //               ,:OCO-DS-STATO-ELAB
        //               ,:OCO-IMP-COLLG
        //                :IND-OCO-IMP-COLLG
        //               ,:OCO-PRE-PER-TRASF
        //                :IND-OCO-PRE-PER-TRASF
        //               ,:OCO-CAR-ACQ
        //                :IND-OCO-CAR-ACQ
        //               ,:OCO-PRE-1A-ANNUALITA
        //                :IND-OCO-PRE-1A-ANNUALITA
        //               ,:OCO-IMP-TRASFERITO
        //                :IND-OCO-IMP-TRASFERITO
        //               ,:OCO-TP-MOD-ABBINAMENTO
        //                :IND-OCO-TP-MOD-ABBINAMENTO
        //               ,:OCO-PC-PRE-TRASFERITO
        //                :IND-OCO-PC-PRE-TRASFERITO
        //           END-EXEC.
        oggCollgDao.fetchCIdUpdEffOco(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A370-CLOSE-CURSOR-ID-EFF THRU A370-EX
            a370CloseCursorIdEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A405-DECLARE-CURSOR-IDP-EFF<br>
	 * <pre>----
	 * ----  gestione IDP Effetto
	 * ----</pre>*/
    private void a405DeclareCursorIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A410-SELECT-IDP-EFF<br>*/
    private void a410SelectIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A460-OPEN-CURSOR-IDP-EFF<br>*/
    private void a460OpenCursorIdpEff() {
        // COB_CODE: PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.
        a405DeclareCursorIdpEff();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A470-CLOSE-CURSOR-IDP-EFF<br>*/
    private void a470CloseCursorIdpEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A480-FETCH-FIRST-IDP-EFF<br>*/
    private void a480FetchFirstIdpEff() {
        // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.
        a460OpenCursorIdpEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
            a490FetchNextIdpEff();
        }
    }

    /**Original name: A490-FETCH-NEXT-IDP-EFF<br>*/
    private void a490FetchNextIdpEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A505-DECLARE-CURSOR-IBO<br>
	 * <pre>----
	 * ----  gestione IBO Effetto e non
	 * ----</pre>*/
    private void a505DeclareCursorIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A510-SELECT-IBO<br>*/
    private void a510SelectIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A560-OPEN-CURSOR-IBO<br>*/
    private void a560OpenCursorIbo() {
        // COB_CODE: PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.
        a505DeclareCursorIbo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A570-CLOSE-CURSOR-IBO<br>*/
    private void a570CloseCursorIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A580-FETCH-FIRST-IBO<br>*/
    private void a580FetchFirstIbo() {
        // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.
        a560OpenCursorIbo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
            a590FetchNextIbo();
        }
    }

    /**Original name: A590-FETCH-NEXT-IBO<br>*/
    private void a590FetchNextIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A605-DCL-CUR-IBS-RIFTO-ESTNO<br>
	 * <pre>----
	 * ----  gestione IBS Effetto e non
	 * ----</pre>*/
    private void a605DclCurIbsRiftoEstno() {
    // COB_CODE: EXEC SQL
    //                DECLARE C-IBS-EFF-OCO-0 CURSOR FOR
    //              SELECT
    //                     ID_OGG_COLLG
    //                    ,ID_OGG_COINV
    //                    ,TP_OGG_COINV
    //                    ,ID_OGG_DER
    //                    ,TP_OGG_DER
    //                    ,IB_RIFTO_ESTNO
    //                    ,ID_MOVI_CRZ
    //                    ,ID_MOVI_CHIU
    //                    ,DT_INI_EFF
    //                    ,DT_END_EFF
    //                    ,COD_COMP_ANIA
    //                    ,COD_PROD
    //                    ,DT_SCAD
    //                    ,TP_COLLGM
    //                    ,TP_TRASF
    //                    ,REC_PROV
    //                    ,DT_DECOR
    //                    ,DT_ULT_PRE_PAG
    //                    ,TOT_PRE
    //                    ,RIS_MAT
    //                    ,RIS_ZIL
    //                    ,IMP_TRASF
    //                    ,IMP_REINVST
    //                    ,PC_REINVST_RILIEVI
    //                    ,IB_2O_RIFTO_ESTNO
    //                    ,IND_LIQ_AGG_MAN
    //                    ,DS_RIGA
    //                    ,DS_OPER_SQL
    //                    ,DS_VER
    //                    ,DS_TS_INI_CPTZ
    //                    ,DS_TS_END_CPTZ
    //                    ,DS_UTENTE
    //                    ,DS_STATO_ELAB
    //                    ,IMP_COLLG
    //                    ,PRE_PER_TRASF
    //                    ,CAR_ACQ
    //                    ,PRE_1A_ANNUALITA
    //                    ,IMP_TRASFERITO
    //                    ,TP_MOD_ABBINAMENTO
    //                    ,PC_PRE_TRASFERITO
    //              FROM OGG_COLLG
    //              WHERE     IB_RIFTO_ESTNO = :OCO-IB-RIFTO-ESTNO
    //                    AND COD_COMP_ANIA =
    //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
    //                    AND DT_INI_EFF <=
    //                        :WS-DATA-INIZIO-EFFETTO-DB
    //                    AND DT_END_EFF >
    //                        :WS-DATA-INIZIO-EFFETTO-DB
    //                    AND ID_MOVI_CHIU IS NULL
    //              ORDER BY ID_OGG_COLLG ASC
    //           END-EXEC.
    // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A605-DCL-CUR-2O-RIFTO-ESTNO<br>*/
    private void a605DclCur2oRiftoEstno() {
    // COB_CODE: EXEC SQL
    //                DECLARE C-IBS-EFF-OCO-1 CURSOR FOR
    //              SELECT
    //                     ID_OGG_COLLG
    //                    ,ID_OGG_COINV
    //                    ,TP_OGG_COINV
    //                    ,ID_OGG_DER
    //                    ,TP_OGG_DER
    //                    ,IB_RIFTO_ESTNO
    //                    ,ID_MOVI_CRZ
    //                    ,ID_MOVI_CHIU
    //                    ,DT_INI_EFF
    //                    ,DT_END_EFF
    //                    ,COD_COMP_ANIA
    //                    ,COD_PROD
    //                    ,DT_SCAD
    //                    ,TP_COLLGM
    //                    ,TP_TRASF
    //                    ,REC_PROV
    //                    ,DT_DECOR
    //                    ,DT_ULT_PRE_PAG
    //                    ,TOT_PRE
    //                    ,RIS_MAT
    //                    ,RIS_ZIL
    //                    ,IMP_TRASF
    //                    ,IMP_REINVST
    //                    ,PC_REINVST_RILIEVI
    //                    ,IB_2O_RIFTO_ESTNO
    //                    ,IND_LIQ_AGG_MAN
    //                    ,DS_RIGA
    //                    ,DS_OPER_SQL
    //                    ,DS_VER
    //                    ,DS_TS_INI_CPTZ
    //                    ,DS_TS_END_CPTZ
    //                    ,DS_UTENTE
    //                    ,DS_STATO_ELAB
    //                    ,IMP_COLLG
    //                    ,PRE_PER_TRASF
    //                    ,CAR_ACQ
    //                    ,PRE_1A_ANNUALITA
    //                    ,IMP_TRASFERITO
    //                    ,TP_MOD_ABBINAMENTO
    //                    ,PC_PRE_TRASFERITO
    //              FROM OGG_COLLG
    //              WHERE     IB_2O_RIFTO_ESTNO = :OCO-IB-2O-RIFTO-ESTNO
    //                    AND COD_COMP_ANIA =
    //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
    //                    AND DT_INI_EFF <=
    //                        :WS-DATA-INIZIO-EFFETTO-DB
    //                    AND DT_END_EFF >
    //                        :WS-DATA-INIZIO-EFFETTO-DB
    //                    AND ID_MOVI_CHIU IS NULL
    //              ORDER BY ID_OGG_COLLG ASC
    //           END-EXEC.
    // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A605-DECLARE-CURSOR-IBS<br>*/
    private void a605DeclareCursorIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: IF OCO-IB-RIFTO-ESTNO NOT = HIGH-VALUES
        //                  THRU A605-RIFTO-ESTNO-EX
        //           ELSE
        //           END-IF
        //           END-IF.
        if (!Characters.EQ_HIGH.test(oggCollg.getOcoIbRiftoEstno(), OggCollgIdbsoco0.Len.OCO_IB_RIFTO_ESTNO)) {
            // COB_CODE: PERFORM A605-DCL-CUR-IBS-RIFTO-ESTNO
            //              THRU A605-RIFTO-ESTNO-EX
            a605DclCurIbsRiftoEstno();
        }
        else if (!Characters.EQ_HIGH.test(oggCollg.getOcoIb2oRiftoEstno(), OggCollgIdbsoco0.Len.OCO_IB2O_RIFTO_ESTNO)) {
            // COB_CODE: IF OCO-IB-2O-RIFTO-ESTNO NOT = HIGH-VALUES
            //                  THRU A605-2O-RIFTO-ESTNO-EX
            //           END-IF
            // COB_CODE: PERFORM A605-DCL-CUR-2O-RIFTO-ESTNO
            //              THRU A605-2O-RIFTO-ESTNO-EX
            a605DclCur2oRiftoEstno();
        }
    }

    /**Original name: A610-SELECT-IBS-RIFTO-ESTNO<br>*/
    private void a610SelectIbsRiftoEstno() {
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_OGG_COLLG
        //                ,ID_OGG_COINV
        //                ,TP_OGG_COINV
        //                ,ID_OGG_DER
        //                ,TP_OGG_DER
        //                ,IB_RIFTO_ESTNO
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,COD_PROD
        //                ,DT_SCAD
        //                ,TP_COLLGM
        //                ,TP_TRASF
        //                ,REC_PROV
        //                ,DT_DECOR
        //                ,DT_ULT_PRE_PAG
        //                ,TOT_PRE
        //                ,RIS_MAT
        //                ,RIS_ZIL
        //                ,IMP_TRASF
        //                ,IMP_REINVST
        //                ,PC_REINVST_RILIEVI
        //                ,IB_2O_RIFTO_ESTNO
        //                ,IND_LIQ_AGG_MAN
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,IMP_COLLG
        //                ,PRE_PER_TRASF
        //                ,CAR_ACQ
        //                ,PRE_1A_ANNUALITA
        //                ,IMP_TRASFERITO
        //                ,TP_MOD_ABBINAMENTO
        //                ,PC_PRE_TRASFERITO
        //             INTO
        //                :OCO-ID-OGG-COLLG
        //               ,:OCO-ID-OGG-COINV
        //               ,:OCO-TP-OGG-COINV
        //               ,:OCO-ID-OGG-DER
        //               ,:OCO-TP-OGG-DER
        //               ,:OCO-IB-RIFTO-ESTNO
        //                :IND-OCO-IB-RIFTO-ESTNO
        //               ,:OCO-ID-MOVI-CRZ
        //               ,:OCO-ID-MOVI-CHIU
        //                :IND-OCO-ID-MOVI-CHIU
        //               ,:OCO-DT-INI-EFF-DB
        //               ,:OCO-DT-END-EFF-DB
        //               ,:OCO-COD-COMP-ANIA
        //               ,:OCO-COD-PROD
        //               ,:OCO-DT-SCAD-DB
        //                :IND-OCO-DT-SCAD
        //               ,:OCO-TP-COLLGM
        //               ,:OCO-TP-TRASF
        //                :IND-OCO-TP-TRASF
        //               ,:OCO-REC-PROV
        //                :IND-OCO-REC-PROV
        //               ,:OCO-DT-DECOR-DB
        //                :IND-OCO-DT-DECOR
        //               ,:OCO-DT-ULT-PRE-PAG-DB
        //                :IND-OCO-DT-ULT-PRE-PAG
        //               ,:OCO-TOT-PRE
        //                :IND-OCO-TOT-PRE
        //               ,:OCO-RIS-MAT
        //                :IND-OCO-RIS-MAT
        //               ,:OCO-RIS-ZIL
        //                :IND-OCO-RIS-ZIL
        //               ,:OCO-IMP-TRASF
        //                :IND-OCO-IMP-TRASF
        //               ,:OCO-IMP-REINVST
        //                :IND-OCO-IMP-REINVST
        //               ,:OCO-PC-REINVST-RILIEVI
        //                :IND-OCO-PC-REINVST-RILIEVI
        //               ,:OCO-IB-2O-RIFTO-ESTNO
        //                :IND-OCO-IB-2O-RIFTO-ESTNO
        //               ,:OCO-IND-LIQ-AGG-MAN
        //                :IND-OCO-IND-LIQ-AGG-MAN
        //               ,:OCO-DS-RIGA
        //               ,:OCO-DS-OPER-SQL
        //               ,:OCO-DS-VER
        //               ,:OCO-DS-TS-INI-CPTZ
        //               ,:OCO-DS-TS-END-CPTZ
        //               ,:OCO-DS-UTENTE
        //               ,:OCO-DS-STATO-ELAB
        //               ,:OCO-IMP-COLLG
        //                :IND-OCO-IMP-COLLG
        //               ,:OCO-PRE-PER-TRASF
        //                :IND-OCO-PRE-PER-TRASF
        //               ,:OCO-CAR-ACQ
        //                :IND-OCO-CAR-ACQ
        //               ,:OCO-PRE-1A-ANNUALITA
        //                :IND-OCO-PRE-1A-ANNUALITA
        //               ,:OCO-IMP-TRASFERITO
        //                :IND-OCO-IMP-TRASFERITO
        //               ,:OCO-TP-MOD-ABBINAMENTO
        //                :IND-OCO-TP-MOD-ABBINAMENTO
        //               ,:OCO-PC-PRE-TRASFERITO
        //                :IND-OCO-PC-PRE-TRASFERITO
        //             FROM OGG_COLLG
        //             WHERE     IB_RIFTO_ESTNO = :OCO-IB-RIFTO-ESTNO
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        oggCollgDao.selectRec1(oggCollg.getOcoIbRiftoEstno(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
    }

    /**Original name: A610-SELECT-IBS-2O-RIFTO-ESTNO<br>*/
    private void a610SelectIbs2oRiftoEstno() {
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_OGG_COLLG
        //                ,ID_OGG_COINV
        //                ,TP_OGG_COINV
        //                ,ID_OGG_DER
        //                ,TP_OGG_DER
        //                ,IB_RIFTO_ESTNO
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,COD_PROD
        //                ,DT_SCAD
        //                ,TP_COLLGM
        //                ,TP_TRASF
        //                ,REC_PROV
        //                ,DT_DECOR
        //                ,DT_ULT_PRE_PAG
        //                ,TOT_PRE
        //                ,RIS_MAT
        //                ,RIS_ZIL
        //                ,IMP_TRASF
        //                ,IMP_REINVST
        //                ,PC_REINVST_RILIEVI
        //                ,IB_2O_RIFTO_ESTNO
        //                ,IND_LIQ_AGG_MAN
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,IMP_COLLG
        //                ,PRE_PER_TRASF
        //                ,CAR_ACQ
        //                ,PRE_1A_ANNUALITA
        //                ,IMP_TRASFERITO
        //                ,TP_MOD_ABBINAMENTO
        //                ,PC_PRE_TRASFERITO
        //             INTO
        //                :OCO-ID-OGG-COLLG
        //               ,:OCO-ID-OGG-COINV
        //               ,:OCO-TP-OGG-COINV
        //               ,:OCO-ID-OGG-DER
        //               ,:OCO-TP-OGG-DER
        //               ,:OCO-IB-RIFTO-ESTNO
        //                :IND-OCO-IB-RIFTO-ESTNO
        //               ,:OCO-ID-MOVI-CRZ
        //               ,:OCO-ID-MOVI-CHIU
        //                :IND-OCO-ID-MOVI-CHIU
        //               ,:OCO-DT-INI-EFF-DB
        //               ,:OCO-DT-END-EFF-DB
        //               ,:OCO-COD-COMP-ANIA
        //               ,:OCO-COD-PROD
        //               ,:OCO-DT-SCAD-DB
        //                :IND-OCO-DT-SCAD
        //               ,:OCO-TP-COLLGM
        //               ,:OCO-TP-TRASF
        //                :IND-OCO-TP-TRASF
        //               ,:OCO-REC-PROV
        //                :IND-OCO-REC-PROV
        //               ,:OCO-DT-DECOR-DB
        //                :IND-OCO-DT-DECOR
        //               ,:OCO-DT-ULT-PRE-PAG-DB
        //                :IND-OCO-DT-ULT-PRE-PAG
        //               ,:OCO-TOT-PRE
        //                :IND-OCO-TOT-PRE
        //               ,:OCO-RIS-MAT
        //                :IND-OCO-RIS-MAT
        //               ,:OCO-RIS-ZIL
        //                :IND-OCO-RIS-ZIL
        //               ,:OCO-IMP-TRASF
        //                :IND-OCO-IMP-TRASF
        //               ,:OCO-IMP-REINVST
        //                :IND-OCO-IMP-REINVST
        //               ,:OCO-PC-REINVST-RILIEVI
        //                :IND-OCO-PC-REINVST-RILIEVI
        //               ,:OCO-IB-2O-RIFTO-ESTNO
        //                :IND-OCO-IB-2O-RIFTO-ESTNO
        //               ,:OCO-IND-LIQ-AGG-MAN
        //                :IND-OCO-IND-LIQ-AGG-MAN
        //               ,:OCO-DS-RIGA
        //               ,:OCO-DS-OPER-SQL
        //               ,:OCO-DS-VER
        //               ,:OCO-DS-TS-INI-CPTZ
        //               ,:OCO-DS-TS-END-CPTZ
        //               ,:OCO-DS-UTENTE
        //               ,:OCO-DS-STATO-ELAB
        //               ,:OCO-IMP-COLLG
        //                :IND-OCO-IMP-COLLG
        //               ,:OCO-PRE-PER-TRASF
        //                :IND-OCO-PRE-PER-TRASF
        //               ,:OCO-CAR-ACQ
        //                :IND-OCO-CAR-ACQ
        //               ,:OCO-PRE-1A-ANNUALITA
        //                :IND-OCO-PRE-1A-ANNUALITA
        //               ,:OCO-IMP-TRASFERITO
        //                :IND-OCO-IMP-TRASFERITO
        //               ,:OCO-TP-MOD-ABBINAMENTO
        //                :IND-OCO-TP-MOD-ABBINAMENTO
        //               ,:OCO-PC-PRE-TRASFERITO
        //                :IND-OCO-PC-PRE-TRASFERITO
        //             FROM OGG_COLLG
        //             WHERE     IB_2O_RIFTO_ESTNO = :OCO-IB-2O-RIFTO-ESTNO
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        oggCollgDao.selectRec2(oggCollg.getOcoIb2oRiftoEstno(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
    }

    /**Original name: A610-SELECT-IBS<br>*/
    private void a610SelectIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: IF OCO-IB-RIFTO-ESTNO NOT = HIGH-VALUES
        //                  THRU A610-RIFTO-ESTNO-EX
        //           ELSE
        //           END-IF
        //           END-IF.
        if (!Characters.EQ_HIGH.test(oggCollg.getOcoIbRiftoEstno(), OggCollgIdbsoco0.Len.OCO_IB_RIFTO_ESTNO)) {
            // COB_CODE: PERFORM A610-SELECT-IBS-RIFTO-ESTNO
            //              THRU A610-RIFTO-ESTNO-EX
            a610SelectIbsRiftoEstno();
        }
        else if (!Characters.EQ_HIGH.test(oggCollg.getOcoIb2oRiftoEstno(), OggCollgIdbsoco0.Len.OCO_IB2O_RIFTO_ESTNO)) {
            // COB_CODE: IF OCO-IB-2O-RIFTO-ESTNO NOT = HIGH-VALUES
            //                  THRU A610-2O-RIFTO-ESTNO-EX
            //           END-IF
            // COB_CODE: PERFORM A610-SELECT-IBS-2O-RIFTO-ESTNO
            //              THRU A610-2O-RIFTO-ESTNO-EX
            a610SelectIbs2oRiftoEstno();
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A660-OPEN-CURSOR-IBS<br>*/
    private void a660OpenCursorIbs() {
        // COB_CODE: PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.
        a605DeclareCursorIbs();
        // COB_CODE: IF OCO-IB-RIFTO-ESTNO NOT = HIGH-VALUES
        //              END-EXEC
        //           ELSE
        //           END-IF
        //           END-IF.
        if (!Characters.EQ_HIGH.test(oggCollg.getOcoIbRiftoEstno(), OggCollgIdbsoco0.Len.OCO_IB_RIFTO_ESTNO)) {
            // COB_CODE: EXEC SQL
            //                OPEN C-IBS-EFF-OCO-0
            //           END-EXEC
            oggCollgDao.openCIbsEffOco0(oggCollg.getOcoIbRiftoEstno(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb());
        }
        else if (!Characters.EQ_HIGH.test(oggCollg.getOcoIb2oRiftoEstno(), OggCollgIdbsoco0.Len.OCO_IB2O_RIFTO_ESTNO)) {
            // COB_CODE: IF OCO-IB-2O-RIFTO-ESTNO NOT = HIGH-VALUES
            //              END-EXEC
            //           END-IF
            // COB_CODE: EXEC SQL
            //                OPEN C-IBS-EFF-OCO-1
            //           END-EXEC
            oggCollgDao.openCIbsEffOco1(oggCollg.getOcoIb2oRiftoEstno(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb());
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A670-CLOSE-CURSOR-IBS<br>*/
    private void a670CloseCursorIbs() {
        // COB_CODE: IF OCO-IB-RIFTO-ESTNO NOT = HIGH-VALUES
        //              END-EXEC
        //           ELSE
        //           END-IF
        //           END-IF.
        if (!Characters.EQ_HIGH.test(oggCollg.getOcoIbRiftoEstno(), OggCollgIdbsoco0.Len.OCO_IB_RIFTO_ESTNO)) {
            // COB_CODE: EXEC SQL
            //                CLOSE C-IBS-EFF-OCO-0
            //           END-EXEC
            oggCollgDao.closeCIbsEffOco0();
        }
        else if (!Characters.EQ_HIGH.test(oggCollg.getOcoIb2oRiftoEstno(), OggCollgIdbsoco0.Len.OCO_IB2O_RIFTO_ESTNO)) {
            // COB_CODE: IF OCO-IB-2O-RIFTO-ESTNO NOT = HIGH-VALUES
            //              END-EXEC
            //           END-IF
            // COB_CODE: EXEC SQL
            //                CLOSE C-IBS-EFF-OCO-1
            //           END-EXEC
            oggCollgDao.closeCIbsEffOco1();
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A680-FETCH-FIRST-IBS<br>*/
    private void a680FetchFirstIbs() {
        // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.
        a660OpenCursorIbs();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
            a690FetchNextIbs();
        }
    }

    /**Original name: A690-FN-IBS-RIFTO-ESTNO<br>*/
    private void a690FnIbsRiftoEstno() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IBS-EFF-OCO-0
        //           INTO
        //                :OCO-ID-OGG-COLLG
        //               ,:OCO-ID-OGG-COINV
        //               ,:OCO-TP-OGG-COINV
        //               ,:OCO-ID-OGG-DER
        //               ,:OCO-TP-OGG-DER
        //               ,:OCO-IB-RIFTO-ESTNO
        //                :IND-OCO-IB-RIFTO-ESTNO
        //               ,:OCO-ID-MOVI-CRZ
        //               ,:OCO-ID-MOVI-CHIU
        //                :IND-OCO-ID-MOVI-CHIU
        //               ,:OCO-DT-INI-EFF-DB
        //               ,:OCO-DT-END-EFF-DB
        //               ,:OCO-COD-COMP-ANIA
        //               ,:OCO-COD-PROD
        //               ,:OCO-DT-SCAD-DB
        //                :IND-OCO-DT-SCAD
        //               ,:OCO-TP-COLLGM
        //               ,:OCO-TP-TRASF
        //                :IND-OCO-TP-TRASF
        //               ,:OCO-REC-PROV
        //                :IND-OCO-REC-PROV
        //               ,:OCO-DT-DECOR-DB
        //                :IND-OCO-DT-DECOR
        //               ,:OCO-DT-ULT-PRE-PAG-DB
        //                :IND-OCO-DT-ULT-PRE-PAG
        //               ,:OCO-TOT-PRE
        //                :IND-OCO-TOT-PRE
        //               ,:OCO-RIS-MAT
        //                :IND-OCO-RIS-MAT
        //               ,:OCO-RIS-ZIL
        //                :IND-OCO-RIS-ZIL
        //               ,:OCO-IMP-TRASF
        //                :IND-OCO-IMP-TRASF
        //               ,:OCO-IMP-REINVST
        //                :IND-OCO-IMP-REINVST
        //               ,:OCO-PC-REINVST-RILIEVI
        //                :IND-OCO-PC-REINVST-RILIEVI
        //               ,:OCO-IB-2O-RIFTO-ESTNO
        //                :IND-OCO-IB-2O-RIFTO-ESTNO
        //               ,:OCO-IND-LIQ-AGG-MAN
        //                :IND-OCO-IND-LIQ-AGG-MAN
        //               ,:OCO-DS-RIGA
        //               ,:OCO-DS-OPER-SQL
        //               ,:OCO-DS-VER
        //               ,:OCO-DS-TS-INI-CPTZ
        //               ,:OCO-DS-TS-END-CPTZ
        //               ,:OCO-DS-UTENTE
        //               ,:OCO-DS-STATO-ELAB
        //               ,:OCO-IMP-COLLG
        //                :IND-OCO-IMP-COLLG
        //               ,:OCO-PRE-PER-TRASF
        //                :IND-OCO-PRE-PER-TRASF
        //               ,:OCO-CAR-ACQ
        //                :IND-OCO-CAR-ACQ
        //               ,:OCO-PRE-1A-ANNUALITA
        //                :IND-OCO-PRE-1A-ANNUALITA
        //               ,:OCO-IMP-TRASFERITO
        //                :IND-OCO-IMP-TRASFERITO
        //               ,:OCO-TP-MOD-ABBINAMENTO
        //                :IND-OCO-TP-MOD-ABBINAMENTO
        //               ,:OCO-PC-PRE-TRASFERITO
        //                :IND-OCO-PC-PRE-TRASFERITO
        //           END-EXEC.
        oggCollgDao.fetchCIbsEffOco0(this);
    }

    /**Original name: A690-FN-IBS-2O-RIFTO-ESTNO<br>*/
    private void a690FnIbs2oRiftoEstno() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IBS-EFF-OCO-1
        //           INTO
        //                :OCO-ID-OGG-COLLG
        //               ,:OCO-ID-OGG-COINV
        //               ,:OCO-TP-OGG-COINV
        //               ,:OCO-ID-OGG-DER
        //               ,:OCO-TP-OGG-DER
        //               ,:OCO-IB-RIFTO-ESTNO
        //                :IND-OCO-IB-RIFTO-ESTNO
        //               ,:OCO-ID-MOVI-CRZ
        //               ,:OCO-ID-MOVI-CHIU
        //                :IND-OCO-ID-MOVI-CHIU
        //               ,:OCO-DT-INI-EFF-DB
        //               ,:OCO-DT-END-EFF-DB
        //               ,:OCO-COD-COMP-ANIA
        //               ,:OCO-COD-PROD
        //               ,:OCO-DT-SCAD-DB
        //                :IND-OCO-DT-SCAD
        //               ,:OCO-TP-COLLGM
        //               ,:OCO-TP-TRASF
        //                :IND-OCO-TP-TRASF
        //               ,:OCO-REC-PROV
        //                :IND-OCO-REC-PROV
        //               ,:OCO-DT-DECOR-DB
        //                :IND-OCO-DT-DECOR
        //               ,:OCO-DT-ULT-PRE-PAG-DB
        //                :IND-OCO-DT-ULT-PRE-PAG
        //               ,:OCO-TOT-PRE
        //                :IND-OCO-TOT-PRE
        //               ,:OCO-RIS-MAT
        //                :IND-OCO-RIS-MAT
        //               ,:OCO-RIS-ZIL
        //                :IND-OCO-RIS-ZIL
        //               ,:OCO-IMP-TRASF
        //                :IND-OCO-IMP-TRASF
        //               ,:OCO-IMP-REINVST
        //                :IND-OCO-IMP-REINVST
        //               ,:OCO-PC-REINVST-RILIEVI
        //                :IND-OCO-PC-REINVST-RILIEVI
        //               ,:OCO-IB-2O-RIFTO-ESTNO
        //                :IND-OCO-IB-2O-RIFTO-ESTNO
        //               ,:OCO-IND-LIQ-AGG-MAN
        //                :IND-OCO-IND-LIQ-AGG-MAN
        //               ,:OCO-DS-RIGA
        //               ,:OCO-DS-OPER-SQL
        //               ,:OCO-DS-VER
        //               ,:OCO-DS-TS-INI-CPTZ
        //               ,:OCO-DS-TS-END-CPTZ
        //               ,:OCO-DS-UTENTE
        //               ,:OCO-DS-STATO-ELAB
        //               ,:OCO-IMP-COLLG
        //                :IND-OCO-IMP-COLLG
        //               ,:OCO-PRE-PER-TRASF
        //                :IND-OCO-PRE-PER-TRASF
        //               ,:OCO-CAR-ACQ
        //                :IND-OCO-CAR-ACQ
        //               ,:OCO-PRE-1A-ANNUALITA
        //                :IND-OCO-PRE-1A-ANNUALITA
        //               ,:OCO-IMP-TRASFERITO
        //                :IND-OCO-IMP-TRASFERITO
        //               ,:OCO-TP-MOD-ABBINAMENTO
        //                :IND-OCO-TP-MOD-ABBINAMENTO
        //               ,:OCO-PC-PRE-TRASFERITO
        //                :IND-OCO-PC-PRE-TRASFERITO
        //           END-EXEC.
        oggCollgDao.fetchCIbsEffOco1(this);
    }

    /**Original name: A690-FETCH-NEXT-IBS<br>*/
    private void a690FetchNextIbs() {
        // COB_CODE: IF OCO-IB-RIFTO-ESTNO NOT = HIGH-VALUES
        //                  THRU A690-RIFTO-ESTNO-EX
        //           ELSE
        //           END-IF
        //           END-IF.
        if (!Characters.EQ_HIGH.test(oggCollg.getOcoIbRiftoEstno(), OggCollgIdbsoco0.Len.OCO_IB_RIFTO_ESTNO)) {
            // COB_CODE: PERFORM A690-FN-IBS-RIFTO-ESTNO
            //              THRU A690-RIFTO-ESTNO-EX
            a690FnIbsRiftoEstno();
        }
        else if (!Characters.EQ_HIGH.test(oggCollg.getOcoIb2oRiftoEstno(), OggCollgIdbsoco0.Len.OCO_IB2O_RIFTO_ESTNO)) {
            // COB_CODE: IF OCO-IB-2O-RIFTO-ESTNO NOT = HIGH-VALUES
            //                  THRU A690-2O-RIFTO-ESTNO-EX
            //           END-IF
            // COB_CODE: PERFORM A690-FN-IBS-2O-RIFTO-ESTNO
            //              THRU A690-2O-RIFTO-ESTNO-EX
            a690FnIbs2oRiftoEstno();
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A670-CLOSE-CURSOR-IBS     THRU A670-EX
            a670CloseCursorIbs();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A705-DECLARE-CURSOR-IDO<br>
	 * <pre>----
	 * ----  gestione IDO Effetto e non
	 * ----</pre>*/
    private void a705DeclareCursorIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A710-SELECT-IDO<br>*/
    private void a710SelectIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A760-OPEN-CURSOR-IDO<br>*/
    private void a760OpenCursorIdo() {
        // COB_CODE: PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.
        a705DeclareCursorIdo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A770-CLOSE-CURSOR-IDO<br>*/
    private void a770CloseCursorIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A780-FETCH-FIRST-IDO<br>*/
    private void a780FetchFirstIdo() {
        // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.
        a760OpenCursorIdo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
            a790FetchNextIdo();
        }
    }

    /**Original name: A790-FETCH-NEXT-IDO<br>*/
    private void a790FetchNextIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B310-SELECT-ID-CPZ<br>
	 * <pre>----
	 * ----  gestione ID Competenza
	 * ----</pre>*/
    private void b310SelectIdCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_OGG_COLLG
        //                ,ID_OGG_COINV
        //                ,TP_OGG_COINV
        //                ,ID_OGG_DER
        //                ,TP_OGG_DER
        //                ,IB_RIFTO_ESTNO
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,COD_PROD
        //                ,DT_SCAD
        //                ,TP_COLLGM
        //                ,TP_TRASF
        //                ,REC_PROV
        //                ,DT_DECOR
        //                ,DT_ULT_PRE_PAG
        //                ,TOT_PRE
        //                ,RIS_MAT
        //                ,RIS_ZIL
        //                ,IMP_TRASF
        //                ,IMP_REINVST
        //                ,PC_REINVST_RILIEVI
        //                ,IB_2O_RIFTO_ESTNO
        //                ,IND_LIQ_AGG_MAN
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,IMP_COLLG
        //                ,PRE_PER_TRASF
        //                ,CAR_ACQ
        //                ,PRE_1A_ANNUALITA
        //                ,IMP_TRASFERITO
        //                ,TP_MOD_ABBINAMENTO
        //                ,PC_PRE_TRASFERITO
        //             INTO
        //                :OCO-ID-OGG-COLLG
        //               ,:OCO-ID-OGG-COINV
        //               ,:OCO-TP-OGG-COINV
        //               ,:OCO-ID-OGG-DER
        //               ,:OCO-TP-OGG-DER
        //               ,:OCO-IB-RIFTO-ESTNO
        //                :IND-OCO-IB-RIFTO-ESTNO
        //               ,:OCO-ID-MOVI-CRZ
        //               ,:OCO-ID-MOVI-CHIU
        //                :IND-OCO-ID-MOVI-CHIU
        //               ,:OCO-DT-INI-EFF-DB
        //               ,:OCO-DT-END-EFF-DB
        //               ,:OCO-COD-COMP-ANIA
        //               ,:OCO-COD-PROD
        //               ,:OCO-DT-SCAD-DB
        //                :IND-OCO-DT-SCAD
        //               ,:OCO-TP-COLLGM
        //               ,:OCO-TP-TRASF
        //                :IND-OCO-TP-TRASF
        //               ,:OCO-REC-PROV
        //                :IND-OCO-REC-PROV
        //               ,:OCO-DT-DECOR-DB
        //                :IND-OCO-DT-DECOR
        //               ,:OCO-DT-ULT-PRE-PAG-DB
        //                :IND-OCO-DT-ULT-PRE-PAG
        //               ,:OCO-TOT-PRE
        //                :IND-OCO-TOT-PRE
        //               ,:OCO-RIS-MAT
        //                :IND-OCO-RIS-MAT
        //               ,:OCO-RIS-ZIL
        //                :IND-OCO-RIS-ZIL
        //               ,:OCO-IMP-TRASF
        //                :IND-OCO-IMP-TRASF
        //               ,:OCO-IMP-REINVST
        //                :IND-OCO-IMP-REINVST
        //               ,:OCO-PC-REINVST-RILIEVI
        //                :IND-OCO-PC-REINVST-RILIEVI
        //               ,:OCO-IB-2O-RIFTO-ESTNO
        //                :IND-OCO-IB-2O-RIFTO-ESTNO
        //               ,:OCO-IND-LIQ-AGG-MAN
        //                :IND-OCO-IND-LIQ-AGG-MAN
        //               ,:OCO-DS-RIGA
        //               ,:OCO-DS-OPER-SQL
        //               ,:OCO-DS-VER
        //               ,:OCO-DS-TS-INI-CPTZ
        //               ,:OCO-DS-TS-END-CPTZ
        //               ,:OCO-DS-UTENTE
        //               ,:OCO-DS-STATO-ELAB
        //               ,:OCO-IMP-COLLG
        //                :IND-OCO-IMP-COLLG
        //               ,:OCO-PRE-PER-TRASF
        //                :IND-OCO-PRE-PER-TRASF
        //               ,:OCO-CAR-ACQ
        //                :IND-OCO-CAR-ACQ
        //               ,:OCO-PRE-1A-ANNUALITA
        //                :IND-OCO-PRE-1A-ANNUALITA
        //               ,:OCO-IMP-TRASFERITO
        //                :IND-OCO-IMP-TRASFERITO
        //               ,:OCO-TP-MOD-ABBINAMENTO
        //                :IND-OCO-TP-MOD-ABBINAMENTO
        //               ,:OCO-PC-PRE-TRASFERITO
        //                :IND-OCO-PC-PRE-TRASFERITO
        //             FROM OGG_COLLG
        //             WHERE     ID_OGG_COLLG = :OCO-ID-OGG-COLLG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        oggCollgDao.selectRec3(oggCollg.getOcoIdOggCollg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B405-DECLARE-CURSOR-IDP-CPZ<br>
	 * <pre>----
	 * ----  gestione IDP Competenza
	 * ----</pre>*/
    private void b405DeclareCursorIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B410-SELECT-IDP-CPZ<br>*/
    private void b410SelectIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B460-OPEN-CURSOR-IDP-CPZ<br>*/
    private void b460OpenCursorIdpCpz() {
        // COB_CODE: PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.
        b405DeclareCursorIdpCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B470-CLOSE-CURSOR-IDP-CPZ<br>*/
    private void b470CloseCursorIdpCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B480-FETCH-FIRST-IDP-CPZ<br>*/
    private void b480FetchFirstIdpCpz() {
        // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.
        b460OpenCursorIdpCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
            b490FetchNextIdpCpz();
        }
    }

    /**Original name: B490-FETCH-NEXT-IDP-CPZ<br>*/
    private void b490FetchNextIdpCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B505-DECLARE-CURSOR-IBO-CPZ<br>
	 * <pre>----
	 * ----  gestione IBO Competenza
	 * ----</pre>*/
    private void b505DeclareCursorIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B510-SELECT-IBO-CPZ<br>*/
    private void b510SelectIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B560-OPEN-CURSOR-IBO-CPZ<br>*/
    private void b560OpenCursorIboCpz() {
        // COB_CODE: PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.
        b505DeclareCursorIboCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B570-CLOSE-CURSOR-IBO-CPZ<br>*/
    private void b570CloseCursorIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B580-FETCH-FIRST-IBO-CPZ<br>*/
    private void b580FetchFirstIboCpz() {
        // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.
        b560OpenCursorIboCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
    }

    /**Original name: B590-FETCH-NEXT-IBO-CPZ<br>*/
    private void b590FetchNextIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B605-DCL-CUR-IBS-RIFTO-ESTNO<br>
	 * <pre>----
	 * ----  gestione IBS Competenza
	 * ----</pre>*/
    private void b605DclCurIbsRiftoEstno() {
    // COB_CODE: EXEC SQL
    //                DECLARE C-IBS-CPZ-OCO-0 CURSOR FOR
    //              SELECT
    //                     ID_OGG_COLLG
    //                    ,ID_OGG_COINV
    //                    ,TP_OGG_COINV
    //                    ,ID_OGG_DER
    //                    ,TP_OGG_DER
    //                    ,IB_RIFTO_ESTNO
    //                    ,ID_MOVI_CRZ
    //                    ,ID_MOVI_CHIU
    //                    ,DT_INI_EFF
    //                    ,DT_END_EFF
    //                    ,COD_COMP_ANIA
    //                    ,COD_PROD
    //                    ,DT_SCAD
    //                    ,TP_COLLGM
    //                    ,TP_TRASF
    //                    ,REC_PROV
    //                    ,DT_DECOR
    //                    ,DT_ULT_PRE_PAG
    //                    ,TOT_PRE
    //                    ,RIS_MAT
    //                    ,RIS_ZIL
    //                    ,IMP_TRASF
    //                    ,IMP_REINVST
    //                    ,PC_REINVST_RILIEVI
    //                    ,IB_2O_RIFTO_ESTNO
    //                    ,IND_LIQ_AGG_MAN
    //                    ,DS_RIGA
    //                    ,DS_OPER_SQL
    //                    ,DS_VER
    //                    ,DS_TS_INI_CPTZ
    //                    ,DS_TS_END_CPTZ
    //                    ,DS_UTENTE
    //                    ,DS_STATO_ELAB
    //                    ,IMP_COLLG
    //                    ,PRE_PER_TRASF
    //                    ,CAR_ACQ
    //                    ,PRE_1A_ANNUALITA
    //                    ,IMP_TRASFERITO
    //                    ,TP_MOD_ABBINAMENTO
    //                    ,PC_PRE_TRASFERITO
    //              FROM OGG_COLLG
    //              WHERE     IB_RIFTO_ESTNO = :OCO-IB-RIFTO-ESTNO
    //                    AND COD_COMP_ANIA =
    //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
    //                    AND DT_INI_EFF <=
    //                        :WS-DATA-INIZIO-EFFETTO-DB
    //                    AND DT_END_EFF >
    //                        :WS-DATA-INIZIO-EFFETTO-DB
    //                    AND DS_TS_INI_CPTZ <=
    //                         :WS-TS-COMPETENZA
    //                    AND DS_TS_END_CPTZ >
    //                         :WS-TS-COMPETENZA
    //              ORDER BY ID_OGG_COLLG ASC
    //           END-EXEC.
    // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B605-DCL-CUR-2O-RIFTO-ESTNO<br>*/
    private void b605DclCur2oRiftoEstno() {
    // COB_CODE: EXEC SQL
    //                DECLARE C-IBS-CPZ-OCO-1 CURSOR FOR
    //              SELECT
    //                     ID_OGG_COLLG
    //                    ,ID_OGG_COINV
    //                    ,TP_OGG_COINV
    //                    ,ID_OGG_DER
    //                    ,TP_OGG_DER
    //                    ,IB_RIFTO_ESTNO
    //                    ,ID_MOVI_CRZ
    //                    ,ID_MOVI_CHIU
    //                    ,DT_INI_EFF
    //                    ,DT_END_EFF
    //                    ,COD_COMP_ANIA
    //                    ,COD_PROD
    //                    ,DT_SCAD
    //                    ,TP_COLLGM
    //                    ,TP_TRASF
    //                    ,REC_PROV
    //                    ,DT_DECOR
    //                    ,DT_ULT_PRE_PAG
    //                    ,TOT_PRE
    //                    ,RIS_MAT
    //                    ,RIS_ZIL
    //                    ,IMP_TRASF
    //                    ,IMP_REINVST
    //                    ,PC_REINVST_RILIEVI
    //                    ,IB_2O_RIFTO_ESTNO
    //                    ,IND_LIQ_AGG_MAN
    //                    ,DS_RIGA
    //                    ,DS_OPER_SQL
    //                    ,DS_VER
    //                    ,DS_TS_INI_CPTZ
    //                    ,DS_TS_END_CPTZ
    //                    ,DS_UTENTE
    //                    ,DS_STATO_ELAB
    //                    ,IMP_COLLG
    //                    ,PRE_PER_TRASF
    //                    ,CAR_ACQ
    //                    ,PRE_1A_ANNUALITA
    //                    ,IMP_TRASFERITO
    //                    ,TP_MOD_ABBINAMENTO
    //                    ,PC_PRE_TRASFERITO
    //              FROM OGG_COLLG
    //              WHERE     IB_2O_RIFTO_ESTNO = :OCO-IB-2O-RIFTO-ESTNO
    //                    AND COD_COMP_ANIA =
    //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
    //                    AND DT_INI_EFF <=
    //                        :WS-DATA-INIZIO-EFFETTO-DB
    //                    AND DT_END_EFF >
    //                        :WS-DATA-INIZIO-EFFETTO-DB
    //                    AND DS_TS_INI_CPTZ <=
    //                         :WS-TS-COMPETENZA
    //                    AND DS_TS_END_CPTZ >
    //                         :WS-TS-COMPETENZA
    //              ORDER BY ID_OGG_COLLG ASC
    //           END-EXEC.
    // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B605-DECLARE-CURSOR-IBS-CPZ<br>*/
    private void b605DeclareCursorIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: IF OCO-IB-RIFTO-ESTNO NOT = HIGH-VALUES
        //                  THRU B605-RIFTO-ESTNO-EX
        //           ELSE
        //           END-IF
        //           END-IF.
        if (!Characters.EQ_HIGH.test(oggCollg.getOcoIbRiftoEstno(), OggCollgIdbsoco0.Len.OCO_IB_RIFTO_ESTNO)) {
            // COB_CODE: PERFORM B605-DCL-CUR-IBS-RIFTO-ESTNO
            //              THRU B605-RIFTO-ESTNO-EX
            b605DclCurIbsRiftoEstno();
        }
        else if (!Characters.EQ_HIGH.test(oggCollg.getOcoIb2oRiftoEstno(), OggCollgIdbsoco0.Len.OCO_IB2O_RIFTO_ESTNO)) {
            // COB_CODE: IF OCO-IB-2O-RIFTO-ESTNO NOT = HIGH-VALUES
            //                  THRU B605-2O-RIFTO-ESTNO-EX
            //           END-IF
            // COB_CODE: PERFORM B605-DCL-CUR-2O-RIFTO-ESTNO
            //              THRU B605-2O-RIFTO-ESTNO-EX
            b605DclCur2oRiftoEstno();
        }
    }

    /**Original name: B610-SELECT-IBS-RIFTO-ESTNO<br>*/
    private void b610SelectIbsRiftoEstno() {
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_OGG_COLLG
        //                ,ID_OGG_COINV
        //                ,TP_OGG_COINV
        //                ,ID_OGG_DER
        //                ,TP_OGG_DER
        //                ,IB_RIFTO_ESTNO
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,COD_PROD
        //                ,DT_SCAD
        //                ,TP_COLLGM
        //                ,TP_TRASF
        //                ,REC_PROV
        //                ,DT_DECOR
        //                ,DT_ULT_PRE_PAG
        //                ,TOT_PRE
        //                ,RIS_MAT
        //                ,RIS_ZIL
        //                ,IMP_TRASF
        //                ,IMP_REINVST
        //                ,PC_REINVST_RILIEVI
        //                ,IB_2O_RIFTO_ESTNO
        //                ,IND_LIQ_AGG_MAN
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,IMP_COLLG
        //                ,PRE_PER_TRASF
        //                ,CAR_ACQ
        //                ,PRE_1A_ANNUALITA
        //                ,IMP_TRASFERITO
        //                ,TP_MOD_ABBINAMENTO
        //                ,PC_PRE_TRASFERITO
        //             INTO
        //                :OCO-ID-OGG-COLLG
        //               ,:OCO-ID-OGG-COINV
        //               ,:OCO-TP-OGG-COINV
        //               ,:OCO-ID-OGG-DER
        //               ,:OCO-TP-OGG-DER
        //               ,:OCO-IB-RIFTO-ESTNO
        //                :IND-OCO-IB-RIFTO-ESTNO
        //               ,:OCO-ID-MOVI-CRZ
        //               ,:OCO-ID-MOVI-CHIU
        //                :IND-OCO-ID-MOVI-CHIU
        //               ,:OCO-DT-INI-EFF-DB
        //               ,:OCO-DT-END-EFF-DB
        //               ,:OCO-COD-COMP-ANIA
        //               ,:OCO-COD-PROD
        //               ,:OCO-DT-SCAD-DB
        //                :IND-OCO-DT-SCAD
        //               ,:OCO-TP-COLLGM
        //               ,:OCO-TP-TRASF
        //                :IND-OCO-TP-TRASF
        //               ,:OCO-REC-PROV
        //                :IND-OCO-REC-PROV
        //               ,:OCO-DT-DECOR-DB
        //                :IND-OCO-DT-DECOR
        //               ,:OCO-DT-ULT-PRE-PAG-DB
        //                :IND-OCO-DT-ULT-PRE-PAG
        //               ,:OCO-TOT-PRE
        //                :IND-OCO-TOT-PRE
        //               ,:OCO-RIS-MAT
        //                :IND-OCO-RIS-MAT
        //               ,:OCO-RIS-ZIL
        //                :IND-OCO-RIS-ZIL
        //               ,:OCO-IMP-TRASF
        //                :IND-OCO-IMP-TRASF
        //               ,:OCO-IMP-REINVST
        //                :IND-OCO-IMP-REINVST
        //               ,:OCO-PC-REINVST-RILIEVI
        //                :IND-OCO-PC-REINVST-RILIEVI
        //               ,:OCO-IB-2O-RIFTO-ESTNO
        //                :IND-OCO-IB-2O-RIFTO-ESTNO
        //               ,:OCO-IND-LIQ-AGG-MAN
        //                :IND-OCO-IND-LIQ-AGG-MAN
        //               ,:OCO-DS-RIGA
        //               ,:OCO-DS-OPER-SQL
        //               ,:OCO-DS-VER
        //               ,:OCO-DS-TS-INI-CPTZ
        //               ,:OCO-DS-TS-END-CPTZ
        //               ,:OCO-DS-UTENTE
        //               ,:OCO-DS-STATO-ELAB
        //               ,:OCO-IMP-COLLG
        //                :IND-OCO-IMP-COLLG
        //               ,:OCO-PRE-PER-TRASF
        //                :IND-OCO-PRE-PER-TRASF
        //               ,:OCO-CAR-ACQ
        //                :IND-OCO-CAR-ACQ
        //               ,:OCO-PRE-1A-ANNUALITA
        //                :IND-OCO-PRE-1A-ANNUALITA
        //               ,:OCO-IMP-TRASFERITO
        //                :IND-OCO-IMP-TRASFERITO
        //               ,:OCO-TP-MOD-ABBINAMENTO
        //                :IND-OCO-TP-MOD-ABBINAMENTO
        //               ,:OCO-PC-PRE-TRASFERITO
        //                :IND-OCO-PC-PRE-TRASFERITO
        //             FROM OGG_COLLG
        //             WHERE     IB_RIFTO_ESTNO = :OCO-IB-RIFTO-ESTNO
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        oggCollgDao.selectRec4(oggCollg.getOcoIbRiftoEstno(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
    }

    /**Original name: B610-SELECT-IBS-2O-RIFTO-ESTNO<br>*/
    private void b610SelectIbs2oRiftoEstno() {
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_OGG_COLLG
        //                ,ID_OGG_COINV
        //                ,TP_OGG_COINV
        //                ,ID_OGG_DER
        //                ,TP_OGG_DER
        //                ,IB_RIFTO_ESTNO
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,COD_PROD
        //                ,DT_SCAD
        //                ,TP_COLLGM
        //                ,TP_TRASF
        //                ,REC_PROV
        //                ,DT_DECOR
        //                ,DT_ULT_PRE_PAG
        //                ,TOT_PRE
        //                ,RIS_MAT
        //                ,RIS_ZIL
        //                ,IMP_TRASF
        //                ,IMP_REINVST
        //                ,PC_REINVST_RILIEVI
        //                ,IB_2O_RIFTO_ESTNO
        //                ,IND_LIQ_AGG_MAN
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,IMP_COLLG
        //                ,PRE_PER_TRASF
        //                ,CAR_ACQ
        //                ,PRE_1A_ANNUALITA
        //                ,IMP_TRASFERITO
        //                ,TP_MOD_ABBINAMENTO
        //                ,PC_PRE_TRASFERITO
        //             INTO
        //                :OCO-ID-OGG-COLLG
        //               ,:OCO-ID-OGG-COINV
        //               ,:OCO-TP-OGG-COINV
        //               ,:OCO-ID-OGG-DER
        //               ,:OCO-TP-OGG-DER
        //               ,:OCO-IB-RIFTO-ESTNO
        //                :IND-OCO-IB-RIFTO-ESTNO
        //               ,:OCO-ID-MOVI-CRZ
        //               ,:OCO-ID-MOVI-CHIU
        //                :IND-OCO-ID-MOVI-CHIU
        //               ,:OCO-DT-INI-EFF-DB
        //               ,:OCO-DT-END-EFF-DB
        //               ,:OCO-COD-COMP-ANIA
        //               ,:OCO-COD-PROD
        //               ,:OCO-DT-SCAD-DB
        //                :IND-OCO-DT-SCAD
        //               ,:OCO-TP-COLLGM
        //               ,:OCO-TP-TRASF
        //                :IND-OCO-TP-TRASF
        //               ,:OCO-REC-PROV
        //                :IND-OCO-REC-PROV
        //               ,:OCO-DT-DECOR-DB
        //                :IND-OCO-DT-DECOR
        //               ,:OCO-DT-ULT-PRE-PAG-DB
        //                :IND-OCO-DT-ULT-PRE-PAG
        //               ,:OCO-TOT-PRE
        //                :IND-OCO-TOT-PRE
        //               ,:OCO-RIS-MAT
        //                :IND-OCO-RIS-MAT
        //               ,:OCO-RIS-ZIL
        //                :IND-OCO-RIS-ZIL
        //               ,:OCO-IMP-TRASF
        //                :IND-OCO-IMP-TRASF
        //               ,:OCO-IMP-REINVST
        //                :IND-OCO-IMP-REINVST
        //               ,:OCO-PC-REINVST-RILIEVI
        //                :IND-OCO-PC-REINVST-RILIEVI
        //               ,:OCO-IB-2O-RIFTO-ESTNO
        //                :IND-OCO-IB-2O-RIFTO-ESTNO
        //               ,:OCO-IND-LIQ-AGG-MAN
        //                :IND-OCO-IND-LIQ-AGG-MAN
        //               ,:OCO-DS-RIGA
        //               ,:OCO-DS-OPER-SQL
        //               ,:OCO-DS-VER
        //               ,:OCO-DS-TS-INI-CPTZ
        //               ,:OCO-DS-TS-END-CPTZ
        //               ,:OCO-DS-UTENTE
        //               ,:OCO-DS-STATO-ELAB
        //               ,:OCO-IMP-COLLG
        //                :IND-OCO-IMP-COLLG
        //               ,:OCO-PRE-PER-TRASF
        //                :IND-OCO-PRE-PER-TRASF
        //               ,:OCO-CAR-ACQ
        //                :IND-OCO-CAR-ACQ
        //               ,:OCO-PRE-1A-ANNUALITA
        //                :IND-OCO-PRE-1A-ANNUALITA
        //               ,:OCO-IMP-TRASFERITO
        //                :IND-OCO-IMP-TRASFERITO
        //               ,:OCO-TP-MOD-ABBINAMENTO
        //                :IND-OCO-TP-MOD-ABBINAMENTO
        //               ,:OCO-PC-PRE-TRASFERITO
        //                :IND-OCO-PC-PRE-TRASFERITO
        //             FROM OGG_COLLG
        //             WHERE     IB_2O_RIFTO_ESTNO = :OCO-IB-2O-RIFTO-ESTNO
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        oggCollgDao.selectRec5(oggCollg.getOcoIb2oRiftoEstno(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
    }

    /**Original name: B610-SELECT-IBS-CPZ<br>*/
    private void b610SelectIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: IF OCO-IB-RIFTO-ESTNO NOT = HIGH-VALUES
        //                  THRU B610-RIFTO-ESTNO-EX
        //           ELSE
        //           END-IF
        //           END-IF.
        if (!Characters.EQ_HIGH.test(oggCollg.getOcoIbRiftoEstno(), OggCollgIdbsoco0.Len.OCO_IB_RIFTO_ESTNO)) {
            // COB_CODE: PERFORM B610-SELECT-IBS-RIFTO-ESTNO
            //              THRU B610-RIFTO-ESTNO-EX
            b610SelectIbsRiftoEstno();
        }
        else if (!Characters.EQ_HIGH.test(oggCollg.getOcoIb2oRiftoEstno(), OggCollgIdbsoco0.Len.OCO_IB2O_RIFTO_ESTNO)) {
            // COB_CODE: IF OCO-IB-2O-RIFTO-ESTNO NOT = HIGH-VALUES
            //                  THRU B610-2O-RIFTO-ESTNO-EX
            //           END-IF
            // COB_CODE: PERFORM B610-SELECT-IBS-2O-RIFTO-ESTNO
            //              THRU B610-2O-RIFTO-ESTNO-EX
            b610SelectIbs2oRiftoEstno();
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B660-OPEN-CURSOR-IBS-CPZ<br>*/
    private void b660OpenCursorIbsCpz() {
        // COB_CODE: PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.
        b605DeclareCursorIbsCpz();
        // COB_CODE: IF OCO-IB-RIFTO-ESTNO NOT = HIGH-VALUES
        //              END-EXEC
        //           ELSE
        //           END-IF
        //           END-IF.
        if (!Characters.EQ_HIGH.test(oggCollg.getOcoIbRiftoEstno(), OggCollgIdbsoco0.Len.OCO_IB_RIFTO_ESTNO)) {
            // COB_CODE: EXEC SQL
            //                OPEN C-IBS-CPZ-OCO-0
            //           END-EXEC
            oggCollgDao.openCIbsCpzOco0(oggCollg.getOcoIbRiftoEstno(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza());
        }
        else if (!Characters.EQ_HIGH.test(oggCollg.getOcoIb2oRiftoEstno(), OggCollgIdbsoco0.Len.OCO_IB2O_RIFTO_ESTNO)) {
            // COB_CODE: IF OCO-IB-2O-RIFTO-ESTNO NOT = HIGH-VALUES
            //              END-EXEC
            //           END-IF
            // COB_CODE: EXEC SQL
            //                OPEN C-IBS-CPZ-OCO-1
            //           END-EXEC
            oggCollgDao.openCIbsCpzOco1(oggCollg.getOcoIb2oRiftoEstno(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza());
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B670-CLOSE-CURSOR-IBS-CPZ<br>*/
    private void b670CloseCursorIbsCpz() {
        // COB_CODE: IF OCO-IB-RIFTO-ESTNO NOT = HIGH-VALUES
        //              END-EXEC
        //           ELSE
        //           END-IF
        //           END-IF.
        if (!Characters.EQ_HIGH.test(oggCollg.getOcoIbRiftoEstno(), OggCollgIdbsoco0.Len.OCO_IB_RIFTO_ESTNO)) {
            // COB_CODE: EXEC SQL
            //                CLOSE C-IBS-CPZ-OCO-0
            //           END-EXEC
            oggCollgDao.closeCIbsCpzOco0();
        }
        else if (!Characters.EQ_HIGH.test(oggCollg.getOcoIb2oRiftoEstno(), OggCollgIdbsoco0.Len.OCO_IB2O_RIFTO_ESTNO)) {
            // COB_CODE: IF OCO-IB-2O-RIFTO-ESTNO NOT = HIGH-VALUES
            //              END-EXEC
            //           END-IF
            // COB_CODE: EXEC SQL
            //                CLOSE C-IBS-CPZ-OCO-1
            //           END-EXEC
            oggCollgDao.closeCIbsCpzOco1();
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B680-FETCH-FIRST-IBS-CPZ<br>*/
    private void b680FetchFirstIbsCpz() {
        // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.
        b660OpenCursorIbsCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
    }

    /**Original name: B690-FN-IBS-RIFTO-ESTNO<br>*/
    private void b690FnIbsRiftoEstno() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IBS-CPZ-OCO-0
        //           INTO
        //                :OCO-ID-OGG-COLLG
        //               ,:OCO-ID-OGG-COINV
        //               ,:OCO-TP-OGG-COINV
        //               ,:OCO-ID-OGG-DER
        //               ,:OCO-TP-OGG-DER
        //               ,:OCO-IB-RIFTO-ESTNO
        //                :IND-OCO-IB-RIFTO-ESTNO
        //               ,:OCO-ID-MOVI-CRZ
        //               ,:OCO-ID-MOVI-CHIU
        //                :IND-OCO-ID-MOVI-CHIU
        //               ,:OCO-DT-INI-EFF-DB
        //               ,:OCO-DT-END-EFF-DB
        //               ,:OCO-COD-COMP-ANIA
        //               ,:OCO-COD-PROD
        //               ,:OCO-DT-SCAD-DB
        //                :IND-OCO-DT-SCAD
        //               ,:OCO-TP-COLLGM
        //               ,:OCO-TP-TRASF
        //                :IND-OCO-TP-TRASF
        //               ,:OCO-REC-PROV
        //                :IND-OCO-REC-PROV
        //               ,:OCO-DT-DECOR-DB
        //                :IND-OCO-DT-DECOR
        //               ,:OCO-DT-ULT-PRE-PAG-DB
        //                :IND-OCO-DT-ULT-PRE-PAG
        //               ,:OCO-TOT-PRE
        //                :IND-OCO-TOT-PRE
        //               ,:OCO-RIS-MAT
        //                :IND-OCO-RIS-MAT
        //               ,:OCO-RIS-ZIL
        //                :IND-OCO-RIS-ZIL
        //               ,:OCO-IMP-TRASF
        //                :IND-OCO-IMP-TRASF
        //               ,:OCO-IMP-REINVST
        //                :IND-OCO-IMP-REINVST
        //               ,:OCO-PC-REINVST-RILIEVI
        //                :IND-OCO-PC-REINVST-RILIEVI
        //               ,:OCO-IB-2O-RIFTO-ESTNO
        //                :IND-OCO-IB-2O-RIFTO-ESTNO
        //               ,:OCO-IND-LIQ-AGG-MAN
        //                :IND-OCO-IND-LIQ-AGG-MAN
        //               ,:OCO-DS-RIGA
        //               ,:OCO-DS-OPER-SQL
        //               ,:OCO-DS-VER
        //               ,:OCO-DS-TS-INI-CPTZ
        //               ,:OCO-DS-TS-END-CPTZ
        //               ,:OCO-DS-UTENTE
        //               ,:OCO-DS-STATO-ELAB
        //               ,:OCO-IMP-COLLG
        //                :IND-OCO-IMP-COLLG
        //               ,:OCO-PRE-PER-TRASF
        //                :IND-OCO-PRE-PER-TRASF
        //               ,:OCO-CAR-ACQ
        //                :IND-OCO-CAR-ACQ
        //               ,:OCO-PRE-1A-ANNUALITA
        //                :IND-OCO-PRE-1A-ANNUALITA
        //               ,:OCO-IMP-TRASFERITO
        //                :IND-OCO-IMP-TRASFERITO
        //               ,:OCO-TP-MOD-ABBINAMENTO
        //                :IND-OCO-TP-MOD-ABBINAMENTO
        //               ,:OCO-PC-PRE-TRASFERITO
        //                :IND-OCO-PC-PRE-TRASFERITO
        //           END-EXEC.
        oggCollgDao.fetchCIbsCpzOco0(this);
    }

    /**Original name: B690-FN-IBS-2O-RIFTO-ESTNO<br>*/
    private void b690FnIbs2oRiftoEstno() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IBS-CPZ-OCO-1
        //           INTO
        //                :OCO-ID-OGG-COLLG
        //               ,:OCO-ID-OGG-COINV
        //               ,:OCO-TP-OGG-COINV
        //               ,:OCO-ID-OGG-DER
        //               ,:OCO-TP-OGG-DER
        //               ,:OCO-IB-RIFTO-ESTNO
        //                :IND-OCO-IB-RIFTO-ESTNO
        //               ,:OCO-ID-MOVI-CRZ
        //               ,:OCO-ID-MOVI-CHIU
        //                :IND-OCO-ID-MOVI-CHIU
        //               ,:OCO-DT-INI-EFF-DB
        //               ,:OCO-DT-END-EFF-DB
        //               ,:OCO-COD-COMP-ANIA
        //               ,:OCO-COD-PROD
        //               ,:OCO-DT-SCAD-DB
        //                :IND-OCO-DT-SCAD
        //               ,:OCO-TP-COLLGM
        //               ,:OCO-TP-TRASF
        //                :IND-OCO-TP-TRASF
        //               ,:OCO-REC-PROV
        //                :IND-OCO-REC-PROV
        //               ,:OCO-DT-DECOR-DB
        //                :IND-OCO-DT-DECOR
        //               ,:OCO-DT-ULT-PRE-PAG-DB
        //                :IND-OCO-DT-ULT-PRE-PAG
        //               ,:OCO-TOT-PRE
        //                :IND-OCO-TOT-PRE
        //               ,:OCO-RIS-MAT
        //                :IND-OCO-RIS-MAT
        //               ,:OCO-RIS-ZIL
        //                :IND-OCO-RIS-ZIL
        //               ,:OCO-IMP-TRASF
        //                :IND-OCO-IMP-TRASF
        //               ,:OCO-IMP-REINVST
        //                :IND-OCO-IMP-REINVST
        //               ,:OCO-PC-REINVST-RILIEVI
        //                :IND-OCO-PC-REINVST-RILIEVI
        //               ,:OCO-IB-2O-RIFTO-ESTNO
        //                :IND-OCO-IB-2O-RIFTO-ESTNO
        //               ,:OCO-IND-LIQ-AGG-MAN
        //                :IND-OCO-IND-LIQ-AGG-MAN
        //               ,:OCO-DS-RIGA
        //               ,:OCO-DS-OPER-SQL
        //               ,:OCO-DS-VER
        //               ,:OCO-DS-TS-INI-CPTZ
        //               ,:OCO-DS-TS-END-CPTZ
        //               ,:OCO-DS-UTENTE
        //               ,:OCO-DS-STATO-ELAB
        //               ,:OCO-IMP-COLLG
        //                :IND-OCO-IMP-COLLG
        //               ,:OCO-PRE-PER-TRASF
        //                :IND-OCO-PRE-PER-TRASF
        //               ,:OCO-CAR-ACQ
        //                :IND-OCO-CAR-ACQ
        //               ,:OCO-PRE-1A-ANNUALITA
        //                :IND-OCO-PRE-1A-ANNUALITA
        //               ,:OCO-IMP-TRASFERITO
        //                :IND-OCO-IMP-TRASFERITO
        //               ,:OCO-TP-MOD-ABBINAMENTO
        //                :IND-OCO-TP-MOD-ABBINAMENTO
        //               ,:OCO-PC-PRE-TRASFERITO
        //                :IND-OCO-PC-PRE-TRASFERITO
        //           END-EXEC.
        oggCollgDao.fetchCIbsCpzOco1(this);
    }

    /**Original name: B690-FETCH-NEXT-IBS-CPZ<br>*/
    private void b690FetchNextIbsCpz() {
        // COB_CODE: IF OCO-IB-RIFTO-ESTNO NOT = HIGH-VALUES
        //                  THRU B690-RIFTO-ESTNO-EX
        //           ELSE
        //           END-IF
        //           END-IF.
        if (!Characters.EQ_HIGH.test(oggCollg.getOcoIbRiftoEstno(), OggCollgIdbsoco0.Len.OCO_IB_RIFTO_ESTNO)) {
            // COB_CODE: PERFORM B690-FN-IBS-RIFTO-ESTNO
            //              THRU B690-RIFTO-ESTNO-EX
            b690FnIbsRiftoEstno();
        }
        else if (!Characters.EQ_HIGH.test(oggCollg.getOcoIb2oRiftoEstno(), OggCollgIdbsoco0.Len.OCO_IB2O_RIFTO_ESTNO)) {
            // COB_CODE: IF OCO-IB-2O-RIFTO-ESTNO NOT = HIGH-VALUES
            //                  THRU B690-2O-RIFTO-ESTNO-EX
            //           END-IF
            // COB_CODE: PERFORM B690-FN-IBS-2O-RIFTO-ESTNO
            //              THRU B690-2O-RIFTO-ESTNO-EX
            b690FnIbs2oRiftoEstno();
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B670-CLOSE-CURSOR-IBS-CPZ     THRU B670-EX
            b670CloseCursorIbsCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B705-DECLARE-CURSOR-IDO-CPZ<br>
	 * <pre>----
	 * ----  gestione IDO Competenza
	 * ----</pre>*/
    private void b705DeclareCursorIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B710-SELECT-IDO-CPZ<br>*/
    private void b710SelectIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B760-OPEN-CURSOR-IDO-CPZ<br>*/
    private void b760OpenCursorIdoCpz() {
        // COB_CODE: PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.
        b705DeclareCursorIdoCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B770-CLOSE-CURSOR-IDO-CPZ<br>*/
    private void b770CloseCursorIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B780-FETCH-FIRST-IDO-CPZ<br>*/
    private void b780FetchFirstIdoCpz() {
        // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.
        b760OpenCursorIdoCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
    }

    /**Original name: B790-FETCH-NEXT-IDO-CPZ<br>*/
    private void b790FetchNextIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-OCO-IB-RIFTO-ESTNO = -1
        //              MOVE HIGH-VALUES TO OCO-IB-RIFTO-ESTNO-NULL
        //           END-IF
        if (ws.getIndOggCollg().getIbRiftoEstno() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO OCO-IB-RIFTO-ESTNO-NULL
            oggCollg.setOcoIbRiftoEstno(LiteralGenerator.create(Types.HIGH_CHAR_VAL, OggCollgIdbsoco0.Len.OCO_IB_RIFTO_ESTNO));
        }
        // COB_CODE: IF IND-OCO-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO OCO-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndOggCollg().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO OCO-ID-MOVI-CHIU-NULL
            oggCollg.getOcoIdMoviChiu().setOcoIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, OcoIdMoviChiu.Len.OCO_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-OCO-DT-SCAD = -1
        //              MOVE HIGH-VALUES TO OCO-DT-SCAD-NULL
        //           END-IF
        if (ws.getIndOggCollg().getDtScad() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO OCO-DT-SCAD-NULL
            oggCollg.getOcoDtScad().setOcoDtScadNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, OcoDtScad.Len.OCO_DT_SCAD_NULL));
        }
        // COB_CODE: IF IND-OCO-TP-TRASF = -1
        //              MOVE HIGH-VALUES TO OCO-TP-TRASF-NULL
        //           END-IF
        if (ws.getIndOggCollg().getTpTrasf() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO OCO-TP-TRASF-NULL
            oggCollg.setOcoTpTrasf(LiteralGenerator.create(Types.HIGH_CHAR_VAL, OggCollgIdbsoco0.Len.OCO_TP_TRASF));
        }
        // COB_CODE: IF IND-OCO-REC-PROV = -1
        //              MOVE HIGH-VALUES TO OCO-REC-PROV-NULL
        //           END-IF
        if (ws.getIndOggCollg().getRecProv() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO OCO-REC-PROV-NULL
            oggCollg.getOcoRecProv().setOcoRecProvNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, OcoRecProv.Len.OCO_REC_PROV_NULL));
        }
        // COB_CODE: IF IND-OCO-DT-DECOR = -1
        //              MOVE HIGH-VALUES TO OCO-DT-DECOR-NULL
        //           END-IF
        if (ws.getIndOggCollg().getDtDecor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO OCO-DT-DECOR-NULL
            oggCollg.getOcoDtDecor().setOcoDtDecorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, OcoDtDecor.Len.OCO_DT_DECOR_NULL));
        }
        // COB_CODE: IF IND-OCO-DT-ULT-PRE-PAG = -1
        //              MOVE HIGH-VALUES TO OCO-DT-ULT-PRE-PAG-NULL
        //           END-IF
        if (ws.getIndOggCollg().getDtUltPrePag() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO OCO-DT-ULT-PRE-PAG-NULL
            oggCollg.getOcoDtUltPrePag().setOcoDtUltPrePagNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, OcoDtUltPrePag.Len.OCO_DT_ULT_PRE_PAG_NULL));
        }
        // COB_CODE: IF IND-OCO-TOT-PRE = -1
        //              MOVE HIGH-VALUES TO OCO-TOT-PRE-NULL
        //           END-IF
        if (ws.getIndOggCollg().getTotPre() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO OCO-TOT-PRE-NULL
            oggCollg.getOcoTotPre().setOcoTotPreNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, OcoTotPre.Len.OCO_TOT_PRE_NULL));
        }
        // COB_CODE: IF IND-OCO-RIS-MAT = -1
        //              MOVE HIGH-VALUES TO OCO-RIS-MAT-NULL
        //           END-IF
        if (ws.getIndOggCollg().getRisMat() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO OCO-RIS-MAT-NULL
            oggCollg.getOcoRisMat().setOcoRisMatNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, OcoRisMat.Len.OCO_RIS_MAT_NULL));
        }
        // COB_CODE: IF IND-OCO-RIS-ZIL = -1
        //              MOVE HIGH-VALUES TO OCO-RIS-ZIL-NULL
        //           END-IF
        if (ws.getIndOggCollg().getRisZil() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO OCO-RIS-ZIL-NULL
            oggCollg.getOcoRisZil().setOcoRisZilNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, OcoRisZil.Len.OCO_RIS_ZIL_NULL));
        }
        // COB_CODE: IF IND-OCO-IMP-TRASF = -1
        //              MOVE HIGH-VALUES TO OCO-IMP-TRASF-NULL
        //           END-IF
        if (ws.getIndOggCollg().getImpTrasf() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO OCO-IMP-TRASF-NULL
            oggCollg.getOcoImpTrasf().setOcoImpTrasfNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, OcoImpTrasf.Len.OCO_IMP_TRASF_NULL));
        }
        // COB_CODE: IF IND-OCO-IMP-REINVST = -1
        //              MOVE HIGH-VALUES TO OCO-IMP-REINVST-NULL
        //           END-IF
        if (ws.getIndOggCollg().getImpReinvst() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO OCO-IMP-REINVST-NULL
            oggCollg.getOcoImpReinvst().setOcoImpReinvstNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, OcoImpReinvst.Len.OCO_IMP_REINVST_NULL));
        }
        // COB_CODE: IF IND-OCO-PC-REINVST-RILIEVI = -1
        //              MOVE HIGH-VALUES TO OCO-PC-REINVST-RILIEVI-NULL
        //           END-IF
        if (ws.getIndOggCollg().getPcReinvstRilievi() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO OCO-PC-REINVST-RILIEVI-NULL
            oggCollg.getOcoPcReinvstRilievi().setOcoPcReinvstRilieviNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, OcoPcReinvstRilievi.Len.OCO_PC_REINVST_RILIEVI_NULL));
        }
        // COB_CODE: IF IND-OCO-IB-2O-RIFTO-ESTNO = -1
        //              MOVE HIGH-VALUES TO OCO-IB-2O-RIFTO-ESTNO-NULL
        //           END-IF
        if (ws.getIndOggCollg().getIb2oRiftoEstno() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO OCO-IB-2O-RIFTO-ESTNO-NULL
            oggCollg.setOcoIb2oRiftoEstno(LiteralGenerator.create(Types.HIGH_CHAR_VAL, OggCollgIdbsoco0.Len.OCO_IB2O_RIFTO_ESTNO));
        }
        // COB_CODE: IF IND-OCO-IND-LIQ-AGG-MAN = -1
        //              MOVE HIGH-VALUES TO OCO-IND-LIQ-AGG-MAN-NULL
        //           END-IF
        if (ws.getIndOggCollg().getIndLiqAggMan() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO OCO-IND-LIQ-AGG-MAN-NULL
            oggCollg.setOcoIndLiqAggMan(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-OCO-IMP-COLLG = -1
        //              MOVE HIGH-VALUES TO OCO-IMP-COLLG-NULL
        //           END-IF
        if (ws.getIndOggCollg().getImpCollg() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO OCO-IMP-COLLG-NULL
            oggCollg.getOcoImpCollg().setOcoImpCollgNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, OcoImpCollg.Len.OCO_IMP_COLLG_NULL));
        }
        // COB_CODE: IF IND-OCO-PRE-PER-TRASF = -1
        //              MOVE HIGH-VALUES TO OCO-PRE-PER-TRASF-NULL
        //           END-IF
        if (ws.getIndOggCollg().getPrePerTrasf() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO OCO-PRE-PER-TRASF-NULL
            oggCollg.getOcoPrePerTrasf().setOcoPrePerTrasfNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, OcoPrePerTrasf.Len.OCO_PRE_PER_TRASF_NULL));
        }
        // COB_CODE: IF IND-OCO-CAR-ACQ = -1
        //              MOVE HIGH-VALUES TO OCO-CAR-ACQ-NULL
        //           END-IF
        if (ws.getIndOggCollg().getCarAcq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO OCO-CAR-ACQ-NULL
            oggCollg.getOcoCarAcq().setOcoCarAcqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, OcoCarAcq.Len.OCO_CAR_ACQ_NULL));
        }
        // COB_CODE: IF IND-OCO-PRE-1A-ANNUALITA = -1
        //              MOVE HIGH-VALUES TO OCO-PRE-1A-ANNUALITA-NULL
        //           END-IF
        if (ws.getIndOggCollg().getPre1aAnnualita() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO OCO-PRE-1A-ANNUALITA-NULL
            oggCollg.getOcoPre1aAnnualita().setOcoPre1aAnnualitaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, OcoPre1aAnnualita.Len.OCO_PRE1A_ANNUALITA_NULL));
        }
        // COB_CODE: IF IND-OCO-IMP-TRASFERITO = -1
        //              MOVE HIGH-VALUES TO OCO-IMP-TRASFERITO-NULL
        //           END-IF
        if (ws.getIndOggCollg().getImpTrasferito() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO OCO-IMP-TRASFERITO-NULL
            oggCollg.getOcoImpTrasferito().setOcoImpTrasferitoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, OcoImpTrasferito.Len.OCO_IMP_TRASFERITO_NULL));
        }
        // COB_CODE: IF IND-OCO-TP-MOD-ABBINAMENTO = -1
        //              MOVE HIGH-VALUES TO OCO-TP-MOD-ABBINAMENTO-NULL
        //           END-IF
        if (ws.getIndOggCollg().getTpModAbbinamento() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO OCO-TP-MOD-ABBINAMENTO-NULL
            oggCollg.setOcoTpModAbbinamento(LiteralGenerator.create(Types.HIGH_CHAR_VAL, OggCollgIdbsoco0.Len.OCO_TP_MOD_ABBINAMENTO));
        }
        // COB_CODE: IF IND-OCO-PC-PRE-TRASFERITO = -1
        //              MOVE HIGH-VALUES TO OCO-PC-PRE-TRASFERITO-NULL
        //           END-IF.
        if (ws.getIndOggCollg().getPcPreTrasferito() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO OCO-PC-PRE-TRASFERITO-NULL
            oggCollg.getOcoPcPreTrasferito().setOcoPcPreTrasferitoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, OcoPcPreTrasferito.Len.OCO_PC_PRE_TRASFERITO_NULL));
        }
    }

    /**Original name: Z150-VALORIZZA-DATA-SERVICES-I<br>*/
    private void z150ValorizzaDataServicesI() {
        // COB_CODE: MOVE 'I' TO OCO-DS-OPER-SQL
        oggCollg.setOcoDsOperSqlFormatted("I");
        // COB_CODE: MOVE 0                   TO OCO-DS-VER
        oggCollg.setOcoDsVer(0);
        // COB_CODE: MOVE IDSV0003-USER-NAME TO OCO-DS-UTENTE
        oggCollg.setOcoDsUtente(idsv0003.getUserName());
        // COB_CODE: MOVE '1'                   TO OCO-DS-STATO-ELAB.
        oggCollg.setOcoDsStatoElabFormatted("1");
    }

    /**Original name: Z160-VALORIZZA-DATA-SERVICES-U<br>*/
    private void z160ValorizzaDataServicesU() {
        // COB_CODE: MOVE 'U' TO OCO-DS-OPER-SQL
        oggCollg.setOcoDsOperSqlFormatted("U");
        // COB_CODE: MOVE IDSV0003-USER-NAME TO OCO-DS-UTENTE.
        oggCollg.setOcoDsUtente(idsv0003.getUserName());
    }

    /**Original name: Z200-SET-INDICATORI-NULL<br>*/
    private void z200SetIndicatoriNull() {
        // COB_CODE: IF OCO-IB-RIFTO-ESTNO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-OCO-IB-RIFTO-ESTNO
        //           ELSE
        //              MOVE 0 TO IND-OCO-IB-RIFTO-ESTNO
        //           END-IF
        if (Characters.EQ_HIGH.test(oggCollg.getOcoIbRiftoEstno(), OggCollgIdbsoco0.Len.OCO_IB_RIFTO_ESTNO)) {
            // COB_CODE: MOVE -1 TO IND-OCO-IB-RIFTO-ESTNO
            ws.getIndOggCollg().setIbRiftoEstno(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-OCO-IB-RIFTO-ESTNO
            ws.getIndOggCollg().setIbRiftoEstno(((short)0));
        }
        // COB_CODE: IF OCO-ID-MOVI-CHIU-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-OCO-ID-MOVI-CHIU
        //           ELSE
        //              MOVE 0 TO IND-OCO-ID-MOVI-CHIU
        //           END-IF
        if (Characters.EQ_HIGH.test(oggCollg.getOcoIdMoviChiu().getOcoIdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-OCO-ID-MOVI-CHIU
            ws.getIndOggCollg().setIdMoviChiu(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-OCO-ID-MOVI-CHIU
            ws.getIndOggCollg().setIdMoviChiu(((short)0));
        }
        // COB_CODE: IF OCO-DT-SCAD-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-OCO-DT-SCAD
        //           ELSE
        //              MOVE 0 TO IND-OCO-DT-SCAD
        //           END-IF
        if (Characters.EQ_HIGH.test(oggCollg.getOcoDtScad().getOcoDtScadNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-OCO-DT-SCAD
            ws.getIndOggCollg().setDtScad(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-OCO-DT-SCAD
            ws.getIndOggCollg().setDtScad(((short)0));
        }
        // COB_CODE: IF OCO-TP-TRASF-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-OCO-TP-TRASF
        //           ELSE
        //              MOVE 0 TO IND-OCO-TP-TRASF
        //           END-IF
        if (Characters.EQ_HIGH.test(oggCollg.getOcoTpTrasfFormatted())) {
            // COB_CODE: MOVE -1 TO IND-OCO-TP-TRASF
            ws.getIndOggCollg().setTpTrasf(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-OCO-TP-TRASF
            ws.getIndOggCollg().setTpTrasf(((short)0));
        }
        // COB_CODE: IF OCO-REC-PROV-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-OCO-REC-PROV
        //           ELSE
        //              MOVE 0 TO IND-OCO-REC-PROV
        //           END-IF
        if (Characters.EQ_HIGH.test(oggCollg.getOcoRecProv().getOcoRecProvNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-OCO-REC-PROV
            ws.getIndOggCollg().setRecProv(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-OCO-REC-PROV
            ws.getIndOggCollg().setRecProv(((short)0));
        }
        // COB_CODE: IF OCO-DT-DECOR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-OCO-DT-DECOR
        //           ELSE
        //              MOVE 0 TO IND-OCO-DT-DECOR
        //           END-IF
        if (Characters.EQ_HIGH.test(oggCollg.getOcoDtDecor().getOcoDtDecorNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-OCO-DT-DECOR
            ws.getIndOggCollg().setDtDecor(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-OCO-DT-DECOR
            ws.getIndOggCollg().setDtDecor(((short)0));
        }
        // COB_CODE: IF OCO-DT-ULT-PRE-PAG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-OCO-DT-ULT-PRE-PAG
        //           ELSE
        //              MOVE 0 TO IND-OCO-DT-ULT-PRE-PAG
        //           END-IF
        if (Characters.EQ_HIGH.test(oggCollg.getOcoDtUltPrePag().getOcoDtUltPrePagNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-OCO-DT-ULT-PRE-PAG
            ws.getIndOggCollg().setDtUltPrePag(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-OCO-DT-ULT-PRE-PAG
            ws.getIndOggCollg().setDtUltPrePag(((short)0));
        }
        // COB_CODE: IF OCO-TOT-PRE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-OCO-TOT-PRE
        //           ELSE
        //              MOVE 0 TO IND-OCO-TOT-PRE
        //           END-IF
        if (Characters.EQ_HIGH.test(oggCollg.getOcoTotPre().getOcoTotPreNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-OCO-TOT-PRE
            ws.getIndOggCollg().setTotPre(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-OCO-TOT-PRE
            ws.getIndOggCollg().setTotPre(((short)0));
        }
        // COB_CODE: IF OCO-RIS-MAT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-OCO-RIS-MAT
        //           ELSE
        //              MOVE 0 TO IND-OCO-RIS-MAT
        //           END-IF
        if (Characters.EQ_HIGH.test(oggCollg.getOcoRisMat().getOcoRisMatNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-OCO-RIS-MAT
            ws.getIndOggCollg().setRisMat(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-OCO-RIS-MAT
            ws.getIndOggCollg().setRisMat(((short)0));
        }
        // COB_CODE: IF OCO-RIS-ZIL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-OCO-RIS-ZIL
        //           ELSE
        //              MOVE 0 TO IND-OCO-RIS-ZIL
        //           END-IF
        if (Characters.EQ_HIGH.test(oggCollg.getOcoRisZil().getOcoRisZilNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-OCO-RIS-ZIL
            ws.getIndOggCollg().setRisZil(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-OCO-RIS-ZIL
            ws.getIndOggCollg().setRisZil(((short)0));
        }
        // COB_CODE: IF OCO-IMP-TRASF-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-OCO-IMP-TRASF
        //           ELSE
        //              MOVE 0 TO IND-OCO-IMP-TRASF
        //           END-IF
        if (Characters.EQ_HIGH.test(oggCollg.getOcoImpTrasf().getOcoImpTrasfNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-OCO-IMP-TRASF
            ws.getIndOggCollg().setImpTrasf(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-OCO-IMP-TRASF
            ws.getIndOggCollg().setImpTrasf(((short)0));
        }
        // COB_CODE: IF OCO-IMP-REINVST-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-OCO-IMP-REINVST
        //           ELSE
        //              MOVE 0 TO IND-OCO-IMP-REINVST
        //           END-IF
        if (Characters.EQ_HIGH.test(oggCollg.getOcoImpReinvst().getOcoImpReinvstNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-OCO-IMP-REINVST
            ws.getIndOggCollg().setImpReinvst(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-OCO-IMP-REINVST
            ws.getIndOggCollg().setImpReinvst(((short)0));
        }
        // COB_CODE: IF OCO-PC-REINVST-RILIEVI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-OCO-PC-REINVST-RILIEVI
        //           ELSE
        //              MOVE 0 TO IND-OCO-PC-REINVST-RILIEVI
        //           END-IF
        if (Characters.EQ_HIGH.test(oggCollg.getOcoPcReinvstRilievi().getOcoPcReinvstRilieviNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-OCO-PC-REINVST-RILIEVI
            ws.getIndOggCollg().setPcReinvstRilievi(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-OCO-PC-REINVST-RILIEVI
            ws.getIndOggCollg().setPcReinvstRilievi(((short)0));
        }
        // COB_CODE: IF OCO-IB-2O-RIFTO-ESTNO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-OCO-IB-2O-RIFTO-ESTNO
        //           ELSE
        //              MOVE 0 TO IND-OCO-IB-2O-RIFTO-ESTNO
        //           END-IF
        if (Characters.EQ_HIGH.test(oggCollg.getOcoIb2oRiftoEstno(), OggCollgIdbsoco0.Len.OCO_IB2O_RIFTO_ESTNO)) {
            // COB_CODE: MOVE -1 TO IND-OCO-IB-2O-RIFTO-ESTNO
            ws.getIndOggCollg().setIb2oRiftoEstno(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-OCO-IB-2O-RIFTO-ESTNO
            ws.getIndOggCollg().setIb2oRiftoEstno(((short)0));
        }
        // COB_CODE: IF OCO-IND-LIQ-AGG-MAN-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-OCO-IND-LIQ-AGG-MAN
        //           ELSE
        //              MOVE 0 TO IND-OCO-IND-LIQ-AGG-MAN
        //           END-IF
        if (Conditions.eq(oggCollg.getOcoIndLiqAggMan(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-OCO-IND-LIQ-AGG-MAN
            ws.getIndOggCollg().setIndLiqAggMan(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-OCO-IND-LIQ-AGG-MAN
            ws.getIndOggCollg().setIndLiqAggMan(((short)0));
        }
        // COB_CODE: IF OCO-IMP-COLLG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-OCO-IMP-COLLG
        //           ELSE
        //              MOVE 0 TO IND-OCO-IMP-COLLG
        //           END-IF
        if (Characters.EQ_HIGH.test(oggCollg.getOcoImpCollg().getOcoImpCollgNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-OCO-IMP-COLLG
            ws.getIndOggCollg().setImpCollg(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-OCO-IMP-COLLG
            ws.getIndOggCollg().setImpCollg(((short)0));
        }
        // COB_CODE: IF OCO-PRE-PER-TRASF-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-OCO-PRE-PER-TRASF
        //           ELSE
        //              MOVE 0 TO IND-OCO-PRE-PER-TRASF
        //           END-IF
        if (Characters.EQ_HIGH.test(oggCollg.getOcoPrePerTrasf().getOcoPrePerTrasfNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-OCO-PRE-PER-TRASF
            ws.getIndOggCollg().setPrePerTrasf(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-OCO-PRE-PER-TRASF
            ws.getIndOggCollg().setPrePerTrasf(((short)0));
        }
        // COB_CODE: IF OCO-CAR-ACQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-OCO-CAR-ACQ
        //           ELSE
        //              MOVE 0 TO IND-OCO-CAR-ACQ
        //           END-IF
        if (Characters.EQ_HIGH.test(oggCollg.getOcoCarAcq().getOcoCarAcqNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-OCO-CAR-ACQ
            ws.getIndOggCollg().setCarAcq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-OCO-CAR-ACQ
            ws.getIndOggCollg().setCarAcq(((short)0));
        }
        // COB_CODE: IF OCO-PRE-1A-ANNUALITA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-OCO-PRE-1A-ANNUALITA
        //           ELSE
        //              MOVE 0 TO IND-OCO-PRE-1A-ANNUALITA
        //           END-IF
        if (Characters.EQ_HIGH.test(oggCollg.getOcoPre1aAnnualita().getOcoPre1aAnnualitaNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-OCO-PRE-1A-ANNUALITA
            ws.getIndOggCollg().setPre1aAnnualita(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-OCO-PRE-1A-ANNUALITA
            ws.getIndOggCollg().setPre1aAnnualita(((short)0));
        }
        // COB_CODE: IF OCO-IMP-TRASFERITO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-OCO-IMP-TRASFERITO
        //           ELSE
        //              MOVE 0 TO IND-OCO-IMP-TRASFERITO
        //           END-IF
        if (Characters.EQ_HIGH.test(oggCollg.getOcoImpTrasferito().getOcoImpTrasferitoNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-OCO-IMP-TRASFERITO
            ws.getIndOggCollg().setImpTrasferito(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-OCO-IMP-TRASFERITO
            ws.getIndOggCollg().setImpTrasferito(((short)0));
        }
        // COB_CODE: IF OCO-TP-MOD-ABBINAMENTO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-OCO-TP-MOD-ABBINAMENTO
        //           ELSE
        //              MOVE 0 TO IND-OCO-TP-MOD-ABBINAMENTO
        //           END-IF
        if (Characters.EQ_HIGH.test(oggCollg.getOcoTpModAbbinamentoFormatted())) {
            // COB_CODE: MOVE -1 TO IND-OCO-TP-MOD-ABBINAMENTO
            ws.getIndOggCollg().setTpModAbbinamento(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-OCO-TP-MOD-ABBINAMENTO
            ws.getIndOggCollg().setTpModAbbinamento(((short)0));
        }
        // COB_CODE: IF OCO-PC-PRE-TRASFERITO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-OCO-PC-PRE-TRASFERITO
        //           ELSE
        //              MOVE 0 TO IND-OCO-PC-PRE-TRASFERITO
        //           END-IF.
        if (Characters.EQ_HIGH.test(oggCollg.getOcoPcPreTrasferito().getOcoPcPreTrasferitoNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-OCO-PC-PRE-TRASFERITO
            ws.getIndOggCollg().setPcPreTrasferito(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-OCO-PC-PRE-TRASFERITO
            ws.getIndOggCollg().setPcPreTrasferito(((short)0));
        }
    }

    /**Original name: Z400-SEQ-RIGA<br>*/
    private void z400SeqRiga() {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_RIGA
        //              INTO : OCO-DS-RIGA
        //           END-EXEC.
        //TODO: Implement: valuesStmt;
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: Z500-AGGIORNAMENTO-STORICO<br>*/
    private void z500AggiornamentoStorico() {
        // COB_CODE: MOVE OGG-COLLG TO WS-BUFFER-TABLE.
        ws.setWsBufferTable(oggCollg.getOggCollgFormatted());
        // COB_CODE: MOVE OCO-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.
        ws.setWsIdMoviCrz(TruncAbs.toInt(oggCollg.getOcoIdMoviCrz(), 9));
        // COB_CODE: PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.
        a360OpenCursorIdEff();
        // COB_CODE: PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-SQL
        //              END-IF
        //           END-PERFORM.
        while (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX
            a390FetchNextIdEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              END-IF
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: MOVE WS-ID-MOVI-CRZ   TO OCO-ID-MOVI-CHIU
                oggCollg.getOcoIdMoviChiu().setOcoIdMoviChiu(ws.getWsIdMoviCrz());
                // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
                //                                 TO OCO-DS-TS-END-CPTZ
                oggCollg.setOcoDsTsEndCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
                // COB_CODE: PERFORM A330-UPDATE-ID-EFF THRU A330-EX
                a330UpdateIdEff();
                // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
                //                 END-IF
                //           END-IF
                if (idsv0003.getSqlcode().isSuccessfulSql()) {
                    // COB_CODE: MOVE WS-ID-MOVI-CRZ TO OCO-ID-MOVI-CRZ
                    oggCollg.setOcoIdMoviCrz(ws.getWsIdMoviCrz());
                    // COB_CODE: MOVE HIGH-VALUES    TO OCO-ID-MOVI-CHIU-NULL
                    oggCollg.getOcoIdMoviChiu().setOcoIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, OcoIdMoviChiu.Len.OCO_ID_MOVI_CHIU_NULL));
                    // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
                    //                               TO OCO-DT-END-EFF
                    oggCollg.setOcoDtEndEff(idsv0003.getDataInizioEffetto());
                    // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
                    //                               TO OCO-DS-TS-INI-CPTZ
                    oggCollg.setOcoDsTsIniCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
                    // COB_CODE: MOVE WS-TS-INFINITO
                    //                               TO OCO-DS-TS-END-CPTZ
                    oggCollg.setOcoDsTsEndCptz(ws.getIdsv0010().getWsTsInfinito());
                    // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
                    //              PERFORM A220-INSERT-PK THRU A220-EX
                    //           END-IF
                    if (idsv0003.getSqlcode().isSuccessfulSql()) {
                        // COB_CODE: PERFORM A220-INSERT-PK THRU A220-EX
                        a220InsertPk();
                    }
                }
            }
        }
        // COB_CODE: IF IDSV0003-NOT-FOUND
        //              END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF NOT IDSV0003-DELETE-LOGICA
            //              PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX
            //           ELSE
            //              SET IDSV0003-SUCCESSFUL-SQL TO TRUE
            //           END-IF
            if (!idsv0003.getOperazione().isDeleteLogica()) {
                // COB_CODE: PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX
                z600InsertNuovaRigaStorica();
            }
            else {
                // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL TO TRUE
                idsv0003.getSqlcode().setSuccessfulSql();
            }
        }
    }

    /**Original name: Z550-AGG-STORICO-SOLO-INS<br>*/
    private void z550AggStoricoSoloIns() {
        // COB_CODE: MOVE OGG-COLLG TO WS-BUFFER-TABLE.
        ws.setWsBufferTable(oggCollg.getOggCollgFormatted());
        // COB_CODE: MOVE OCO-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.
        ws.setWsIdMoviCrz(TruncAbs.toInt(oggCollg.getOcoIdMoviCrz(), 9));
        // COB_CODE: PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX.
        z600InsertNuovaRigaStorica();
    }

    /**Original name: Z600-INSERT-NUOVA-RIGA-STORICA<br>*/
    private void z600InsertNuovaRigaStorica() {
        // COB_CODE: MOVE WS-BUFFER-TABLE TO OGG-COLLG.
        oggCollg.setOggCollgFormatted(ws.getWsBufferTableFormatted());
        // COB_CODE: MOVE WS-ID-MOVI-CRZ  TO OCO-ID-MOVI-CRZ.
        oggCollg.setOcoIdMoviCrz(ws.getWsIdMoviCrz());
        // COB_CODE: MOVE HIGH-VALUES     TO OCO-ID-MOVI-CHIU-NULL.
        oggCollg.getOcoIdMoviChiu().setOcoIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, OcoIdMoviChiu.Len.OCO_ID_MOVI_CHIU_NULL));
        // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
        //                                TO OCO-DT-INI-EFF.
        oggCollg.setOcoDtIniEff(idsv0003.getDataInizioEffetto());
        // COB_CODE: MOVE WS-DT-INFINITO
        //                                TO OCO-DT-END-EFF.
        oggCollg.setOcoDtEndEff(ws.getIdsv0010().getWsDtInfinito());
        // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
        //                                TO OCO-DS-TS-INI-CPTZ.
        oggCollg.setOcoDsTsIniCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
        // COB_CODE: MOVE WS-TS-INFINITO
        //                                TO OCO-DS-TS-END-CPTZ.
        oggCollg.setOcoDsTsEndCptz(ws.getIdsv0010().getWsTsInfinito());
        // COB_CODE: MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
        //                                TO OCO-COD-COMP-ANIA.
        oggCollg.setOcoCodCompAnia(idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A220-INSERT-PK THRU A220-EX.
        a220InsertPk();
    }

    /**Original name: Z900-CONVERTI-N-TO-X<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da 9(8) comp-3 a date
	 * ----</pre>*/
    private void z900ConvertiNToX() {
        // COB_CODE: MOVE OCO-DT-INI-EFF TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(oggCollg.getOcoDtIniEff(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO OCO-DT-INI-EFF-DB
        ws.getOggCollgDb().setIniEffDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: MOVE OCO-DT-END-EFF TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(oggCollg.getOcoDtEndEff(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO OCO-DT-END-EFF-DB
        ws.getOggCollgDb().setEndEffDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: IF IND-OCO-DT-SCAD = 0
        //               MOVE WS-DATE-X      TO OCO-DT-SCAD-DB
        //           END-IF
        if (ws.getIndOggCollg().getDtScad() == 0) {
            // COB_CODE: MOVE OCO-DT-SCAD TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(oggCollg.getOcoDtScad().getOcoDtScad(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO OCO-DT-SCAD-DB
            ws.getOggCollgDb().setIniCopDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-OCO-DT-DECOR = 0
        //               MOVE WS-DATE-X      TO OCO-DT-DECOR-DB
        //           END-IF
        if (ws.getIndOggCollg().getDtDecor() == 0) {
            // COB_CODE: MOVE OCO-DT-DECOR TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(oggCollg.getOcoDtDecor().getOcoDtDecor(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO OCO-DT-DECOR-DB
            ws.getOggCollgDb().setEndCopDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-OCO-DT-ULT-PRE-PAG = 0
        //               MOVE WS-DATE-X      TO OCO-DT-ULT-PRE-PAG-DB
        //           END-IF.
        if (ws.getIndOggCollg().getDtUltPrePag() == 0) {
            // COB_CODE: MOVE OCO-DT-ULT-PRE-PAG TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(oggCollg.getOcoDtUltPrePag().getOcoDtUltPrePag(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO OCO-DT-ULT-PRE-PAG-DB
            ws.getOggCollgDb().setEsiTitDb(ws.getIdsv0010().getWsDateX());
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE OCO-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getOggCollgDb().getIniEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO OCO-DT-INI-EFF
        oggCollg.setOcoDtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE OCO-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getOggCollgDb().getEndEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO OCO-DT-END-EFF
        oggCollg.setOcoDtEndEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-OCO-DT-SCAD = 0
        //               MOVE WS-DATE-N      TO OCO-DT-SCAD
        //           END-IF
        if (ws.getIndOggCollg().getDtScad() == 0) {
            // COB_CODE: MOVE OCO-DT-SCAD-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getOggCollgDb().getIniCopDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO OCO-DT-SCAD
            oggCollg.getOcoDtScad().setOcoDtScad(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-OCO-DT-DECOR = 0
        //               MOVE WS-DATE-N      TO OCO-DT-DECOR
        //           END-IF
        if (ws.getIndOggCollg().getDtDecor() == 0) {
            // COB_CODE: MOVE OCO-DT-DECOR-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getOggCollgDb().getEndCopDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO OCO-DT-DECOR
            oggCollg.getOcoDtDecor().setOcoDtDecor(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-OCO-DT-ULT-PRE-PAG = 0
        //               MOVE WS-DATE-N      TO OCO-DT-ULT-PRE-PAG
        //           END-IF.
        if (ws.getIndOggCollg().getDtUltPrePag() == 0) {
            // COB_CODE: MOVE OCO-DT-ULT-PRE-PAG-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getOggCollgDb().getEsiTitDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO OCO-DT-ULT-PRE-PAG
            oggCollg.getOcoDtUltPrePag().setOcoDtUltPrePag(ws.getIdsv0010().getWsDateN());
        }
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public AfDecimal getCarAcq() {
        return oggCollg.getOcoCarAcq().getOcoCarAcq();
    }

    @Override
    public void setCarAcq(AfDecimal carAcq) {
        this.oggCollg.getOcoCarAcq().setOcoCarAcq(carAcq.copy());
    }

    @Override
    public AfDecimal getCarAcqObj() {
        if (ws.getIndOggCollg().getCarAcq() >= 0) {
            return getCarAcq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCarAcqObj(AfDecimal carAcqObj) {
        if (carAcqObj != null) {
            setCarAcq(new AfDecimal(carAcqObj, 15, 3));
            ws.getIndOggCollg().setCarAcq(((short)0));
        }
        else {
            ws.getIndOggCollg().setCarAcq(((short)-1));
        }
    }

    @Override
    public int getCodCompAnia() {
        return oggCollg.getOcoCodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.oggCollg.setOcoCodCompAnia(codCompAnia);
    }

    @Override
    public String getCodProd() {
        return oggCollg.getOcoCodProd();
    }

    @Override
    public void setCodProd(String codProd) {
        this.oggCollg.setOcoCodProd(codProd);
    }

    @Override
    public char getDsOperSql() {
        return oggCollg.getOcoDsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.oggCollg.setOcoDsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return oggCollg.getOcoDsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.oggCollg.setOcoDsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsEndCptz() {
        return oggCollg.getOcoDsTsEndCptz();
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.oggCollg.setOcoDsTsEndCptz(dsTsEndCptz);
    }

    @Override
    public long getDsTsIniCptz() {
        return oggCollg.getOcoDsTsIniCptz();
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.oggCollg.setOcoDsTsIniCptz(dsTsIniCptz);
    }

    @Override
    public String getDsUtente() {
        return oggCollg.getOcoDsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.oggCollg.setOcoDsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return oggCollg.getOcoDsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.oggCollg.setOcoDsVer(dsVer);
    }

    @Override
    public String getDtDecorDb() {
        return ws.getOggCollgDb().getEndCopDb();
    }

    @Override
    public void setDtDecorDb(String dtDecorDb) {
        this.ws.getOggCollgDb().setEndCopDb(dtDecorDb);
    }

    @Override
    public String getDtDecorDbObj() {
        if (ws.getIndOggCollg().getDtDecor() >= 0) {
            return getDtDecorDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtDecorDbObj(String dtDecorDbObj) {
        if (dtDecorDbObj != null) {
            setDtDecorDb(dtDecorDbObj);
            ws.getIndOggCollg().setDtDecor(((short)0));
        }
        else {
            ws.getIndOggCollg().setDtDecor(((short)-1));
        }
    }

    @Override
    public String getDtEndEffDb() {
        return ws.getOggCollgDb().getEndEffDb();
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        this.ws.getOggCollgDb().setEndEffDb(dtEndEffDb);
    }

    @Override
    public String getDtIniEffDb() {
        return ws.getOggCollgDb().getIniEffDb();
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        this.ws.getOggCollgDb().setIniEffDb(dtIniEffDb);
    }

    @Override
    public String getDtScadDb() {
        return ws.getOggCollgDb().getIniCopDb();
    }

    @Override
    public void setDtScadDb(String dtScadDb) {
        this.ws.getOggCollgDb().setIniCopDb(dtScadDb);
    }

    @Override
    public String getDtScadDbObj() {
        if (ws.getIndOggCollg().getDtScad() >= 0) {
            return getDtScadDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtScadDbObj(String dtScadDbObj) {
        if (dtScadDbObj != null) {
            setDtScadDb(dtScadDbObj);
            ws.getIndOggCollg().setDtScad(((short)0));
        }
        else {
            ws.getIndOggCollg().setDtScad(((short)-1));
        }
    }

    @Override
    public String getDtUltPrePagDb() {
        return ws.getOggCollgDb().getEsiTitDb();
    }

    @Override
    public void setDtUltPrePagDb(String dtUltPrePagDb) {
        this.ws.getOggCollgDb().setEsiTitDb(dtUltPrePagDb);
    }

    @Override
    public String getDtUltPrePagDbObj() {
        if (ws.getIndOggCollg().getDtUltPrePag() >= 0) {
            return getDtUltPrePagDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltPrePagDbObj(String dtUltPrePagDbObj) {
        if (dtUltPrePagDbObj != null) {
            setDtUltPrePagDb(dtUltPrePagDbObj);
            ws.getIndOggCollg().setDtUltPrePag(((short)0));
        }
        else {
            ws.getIndOggCollg().setDtUltPrePag(((short)-1));
        }
    }

    @Override
    public String getIb2oRiftoEstno() {
        return oggCollg.getOcoIb2oRiftoEstno();
    }

    @Override
    public void setIb2oRiftoEstno(String ib2oRiftoEstno) {
        this.oggCollg.setOcoIb2oRiftoEstno(ib2oRiftoEstno);
    }

    @Override
    public String getIb2oRiftoEstnoObj() {
        if (ws.getIndOggCollg().getIb2oRiftoEstno() >= 0) {
            return getIb2oRiftoEstno();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIb2oRiftoEstnoObj(String ib2oRiftoEstnoObj) {
        if (ib2oRiftoEstnoObj != null) {
            setIb2oRiftoEstno(ib2oRiftoEstnoObj);
            ws.getIndOggCollg().setIb2oRiftoEstno(((short)0));
        }
        else {
            ws.getIndOggCollg().setIb2oRiftoEstno(((short)-1));
        }
    }

    @Override
    public String getIbRiftoEstno() {
        return oggCollg.getOcoIbRiftoEstno();
    }

    @Override
    public void setIbRiftoEstno(String ibRiftoEstno) {
        this.oggCollg.setOcoIbRiftoEstno(ibRiftoEstno);
    }

    @Override
    public String getIbRiftoEstnoObj() {
        if (ws.getIndOggCollg().getIbRiftoEstno() >= 0) {
            return getIbRiftoEstno();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIbRiftoEstnoObj(String ibRiftoEstnoObj) {
        if (ibRiftoEstnoObj != null) {
            setIbRiftoEstno(ibRiftoEstnoObj);
            ws.getIndOggCollg().setIbRiftoEstno(((short)0));
        }
        else {
            ws.getIndOggCollg().setIbRiftoEstno(((short)-1));
        }
    }

    @Override
    public int getIdMoviChiu() {
        return oggCollg.getOcoIdMoviChiu().getOcoIdMoviChiu();
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        this.oggCollg.getOcoIdMoviChiu().setOcoIdMoviChiu(idMoviChiu);
    }

    @Override
    public Integer getIdMoviChiuObj() {
        if (ws.getIndOggCollg().getIdMoviChiu() >= 0) {
            return ((Integer)getIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        if (idMoviChiuObj != null) {
            setIdMoviChiu(((int)idMoviChiuObj));
            ws.getIndOggCollg().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndOggCollg().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getIdMoviCrz() {
        return oggCollg.getOcoIdMoviCrz();
    }

    @Override
    public void setIdMoviCrz(int idMoviCrz) {
        this.oggCollg.setOcoIdMoviCrz(idMoviCrz);
    }

    @Override
    public int getIdOggCoinv() {
        return oggCollg.getOcoIdOggCoinv();
    }

    @Override
    public void setIdOggCoinv(int idOggCoinv) {
        this.oggCollg.setOcoIdOggCoinv(idOggCoinv);
    }

    @Override
    public int getIdOggCollg() {
        return oggCollg.getOcoIdOggCollg();
    }

    @Override
    public void setIdOggCollg(int idOggCollg) {
        this.oggCollg.setOcoIdOggCollg(idOggCollg);
    }

    @Override
    public int getIdOggDer() {
        return oggCollg.getOcoIdOggDer();
    }

    @Override
    public void setIdOggDer(int idOggDer) {
        this.oggCollg.setOcoIdOggDer(idOggDer);
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        throw new FieldNotMappedException("idsv0003CodiceCompagniaAnia");
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        throw new FieldNotMappedException("idsv0003CodiceCompagniaAnia");
    }

    @Override
    public AfDecimal getImpCollg() {
        return oggCollg.getOcoImpCollg().getOcoImpCollg();
    }

    @Override
    public void setImpCollg(AfDecimal impCollg) {
        this.oggCollg.getOcoImpCollg().setOcoImpCollg(impCollg.copy());
    }

    @Override
    public AfDecimal getImpCollgObj() {
        if (ws.getIndOggCollg().getImpCollg() >= 0) {
            return getImpCollg();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpCollgObj(AfDecimal impCollgObj) {
        if (impCollgObj != null) {
            setImpCollg(new AfDecimal(impCollgObj, 15, 3));
            ws.getIndOggCollg().setImpCollg(((short)0));
        }
        else {
            ws.getIndOggCollg().setImpCollg(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpReinvst() {
        return oggCollg.getOcoImpReinvst().getOcoImpReinvst();
    }

    @Override
    public void setImpReinvst(AfDecimal impReinvst) {
        this.oggCollg.getOcoImpReinvst().setOcoImpReinvst(impReinvst.copy());
    }

    @Override
    public AfDecimal getImpReinvstObj() {
        if (ws.getIndOggCollg().getImpReinvst() >= 0) {
            return getImpReinvst();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpReinvstObj(AfDecimal impReinvstObj) {
        if (impReinvstObj != null) {
            setImpReinvst(new AfDecimal(impReinvstObj, 15, 3));
            ws.getIndOggCollg().setImpReinvst(((short)0));
        }
        else {
            ws.getIndOggCollg().setImpReinvst(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpTrasf() {
        return oggCollg.getOcoImpTrasf().getOcoImpTrasf();
    }

    @Override
    public void setImpTrasf(AfDecimal impTrasf) {
        this.oggCollg.getOcoImpTrasf().setOcoImpTrasf(impTrasf.copy());
    }

    @Override
    public AfDecimal getImpTrasfObj() {
        if (ws.getIndOggCollg().getImpTrasf() >= 0) {
            return getImpTrasf();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpTrasfObj(AfDecimal impTrasfObj) {
        if (impTrasfObj != null) {
            setImpTrasf(new AfDecimal(impTrasfObj, 15, 3));
            ws.getIndOggCollg().setImpTrasf(((short)0));
        }
        else {
            ws.getIndOggCollg().setImpTrasf(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpTrasferito() {
        return oggCollg.getOcoImpTrasferito().getOcoImpTrasferito();
    }

    @Override
    public void setImpTrasferito(AfDecimal impTrasferito) {
        this.oggCollg.getOcoImpTrasferito().setOcoImpTrasferito(impTrasferito.copy());
    }

    @Override
    public AfDecimal getImpTrasferitoObj() {
        if (ws.getIndOggCollg().getImpTrasferito() >= 0) {
            return getImpTrasferito();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpTrasferitoObj(AfDecimal impTrasferitoObj) {
        if (impTrasferitoObj != null) {
            setImpTrasferito(new AfDecimal(impTrasferitoObj, 15, 3));
            ws.getIndOggCollg().setImpTrasferito(((short)0));
        }
        else {
            ws.getIndOggCollg().setImpTrasferito(((short)-1));
        }
    }

    @Override
    public char getIndLiqAggMan() {
        return oggCollg.getOcoIndLiqAggMan();
    }

    @Override
    public void setIndLiqAggMan(char indLiqAggMan) {
        this.oggCollg.setOcoIndLiqAggMan(indLiqAggMan);
    }

    @Override
    public Character getIndLiqAggManObj() {
        if (ws.getIndOggCollg().getIndLiqAggMan() >= 0) {
            return ((Character)getIndLiqAggMan());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIndLiqAggManObj(Character indLiqAggManObj) {
        if (indLiqAggManObj != null) {
            setIndLiqAggMan(((char)indLiqAggManObj));
            ws.getIndOggCollg().setIndLiqAggMan(((short)0));
        }
        else {
            ws.getIndOggCollg().setIndLiqAggMan(((short)-1));
        }
    }

    @Override
    public int getLdbv0721IdOggCollg() {
        throw new FieldNotMappedException("ldbv0721IdOggCollg");
    }

    @Override
    public void setLdbv0721IdOggCollg(int ldbv0721IdOggCollg) {
        throw new FieldNotMappedException("ldbv0721IdOggCollg");
    }

    @Override
    public String getLdbv0721TpOggCollg() {
        throw new FieldNotMappedException("ldbv0721TpOggCollg");
    }

    @Override
    public void setLdbv0721TpOggCollg(String ldbv0721TpOggCollg) {
        throw new FieldNotMappedException("ldbv0721TpOggCollg");
    }

    @Override
    public int getLdbv5571IdOggDer() {
        throw new FieldNotMappedException("ldbv5571IdOggDer");
    }

    @Override
    public void setLdbv5571IdOggDer(int ldbv5571IdOggDer) {
        throw new FieldNotMappedException("ldbv5571IdOggDer");
    }

    @Override
    public AfDecimal getLdbv5571TotImpColl() {
        throw new FieldNotMappedException("ldbv5571TotImpColl");
    }

    @Override
    public void setLdbv5571TotImpColl(AfDecimal ldbv5571TotImpColl) {
        throw new FieldNotMappedException("ldbv5571TotImpColl");
    }

    @Override
    public AfDecimal getLdbv5571TotImpCollObj() {
        return getLdbv5571TotImpColl();
    }

    @Override
    public void setLdbv5571TotImpCollObj(AfDecimal ldbv5571TotImpCollObj) {
        setLdbv5571TotImpColl(new AfDecimal(ldbv5571TotImpCollObj, 15, 3));
    }

    @Override
    public String getLdbv5571TpColl1() {
        throw new FieldNotMappedException("ldbv5571TpColl1");
    }

    @Override
    public void setLdbv5571TpColl1(String ldbv5571TpColl1) {
        throw new FieldNotMappedException("ldbv5571TpColl1");
    }

    @Override
    public String getLdbv5571TpColl2() {
        throw new FieldNotMappedException("ldbv5571TpColl2");
    }

    @Override
    public void setLdbv5571TpColl2(String ldbv5571TpColl2) {
        throw new FieldNotMappedException("ldbv5571TpColl2");
    }

    @Override
    public String getLdbv5571TpColl3() {
        throw new FieldNotMappedException("ldbv5571TpColl3");
    }

    @Override
    public void setLdbv5571TpColl3(String ldbv5571TpColl3) {
        throw new FieldNotMappedException("ldbv5571TpColl3");
    }

    @Override
    public String getLdbv5571TpOggDer() {
        throw new FieldNotMappedException("ldbv5571TpOggDer");
    }

    @Override
    public void setLdbv5571TpOggDer(String ldbv5571TpOggDer) {
        throw new FieldNotMappedException("ldbv5571TpOggDer");
    }

    @Override
    public long getOcoDsRiga() {
        return oggCollg.getOcoDsRiga();
    }

    @Override
    public void setOcoDsRiga(long ocoDsRiga) {
        this.oggCollg.setOcoDsRiga(ocoDsRiga);
    }

    @Override
    public AfDecimal getPcPreTrasferito() {
        return oggCollg.getOcoPcPreTrasferito().getOcoPcPreTrasferito();
    }

    @Override
    public void setPcPreTrasferito(AfDecimal pcPreTrasferito) {
        this.oggCollg.getOcoPcPreTrasferito().setOcoPcPreTrasferito(pcPreTrasferito.copy());
    }

    @Override
    public AfDecimal getPcPreTrasferitoObj() {
        if (ws.getIndOggCollg().getPcPreTrasferito() >= 0) {
            return getPcPreTrasferito();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcPreTrasferitoObj(AfDecimal pcPreTrasferitoObj) {
        if (pcPreTrasferitoObj != null) {
            setPcPreTrasferito(new AfDecimal(pcPreTrasferitoObj, 6, 3));
            ws.getIndOggCollg().setPcPreTrasferito(((short)0));
        }
        else {
            ws.getIndOggCollg().setPcPreTrasferito(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcReinvstRilievi() {
        return oggCollg.getOcoPcReinvstRilievi().getOcoPcReinvstRilievi();
    }

    @Override
    public void setPcReinvstRilievi(AfDecimal pcReinvstRilievi) {
        this.oggCollg.getOcoPcReinvstRilievi().setOcoPcReinvstRilievi(pcReinvstRilievi.copy());
    }

    @Override
    public AfDecimal getPcReinvstRilieviObj() {
        if (ws.getIndOggCollg().getPcReinvstRilievi() >= 0) {
            return getPcReinvstRilievi();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcReinvstRilieviObj(AfDecimal pcReinvstRilieviObj) {
        if (pcReinvstRilieviObj != null) {
            setPcReinvstRilievi(new AfDecimal(pcReinvstRilieviObj, 6, 3));
            ws.getIndOggCollg().setPcReinvstRilievi(((short)0));
        }
        else {
            ws.getIndOggCollg().setPcReinvstRilievi(((short)-1));
        }
    }

    @Override
    public AfDecimal getPre1aAnnualita() {
        return oggCollg.getOcoPre1aAnnualita().getOcoPre1aAnnualita();
    }

    @Override
    public void setPre1aAnnualita(AfDecimal pre1aAnnualita) {
        this.oggCollg.getOcoPre1aAnnualita().setOcoPre1aAnnualita(pre1aAnnualita.copy());
    }

    @Override
    public AfDecimal getPre1aAnnualitaObj() {
        if (ws.getIndOggCollg().getPre1aAnnualita() >= 0) {
            return getPre1aAnnualita();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPre1aAnnualitaObj(AfDecimal pre1aAnnualitaObj) {
        if (pre1aAnnualitaObj != null) {
            setPre1aAnnualita(new AfDecimal(pre1aAnnualitaObj, 15, 3));
            ws.getIndOggCollg().setPre1aAnnualita(((short)0));
        }
        else {
            ws.getIndOggCollg().setPre1aAnnualita(((short)-1));
        }
    }

    @Override
    public AfDecimal getPrePerTrasf() {
        return oggCollg.getOcoPrePerTrasf().getOcoPrePerTrasf();
    }

    @Override
    public void setPrePerTrasf(AfDecimal prePerTrasf) {
        this.oggCollg.getOcoPrePerTrasf().setOcoPrePerTrasf(prePerTrasf.copy());
    }

    @Override
    public AfDecimal getPrePerTrasfObj() {
        if (ws.getIndOggCollg().getPrePerTrasf() >= 0) {
            return getPrePerTrasf();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPrePerTrasfObj(AfDecimal prePerTrasfObj) {
        if (prePerTrasfObj != null) {
            setPrePerTrasf(new AfDecimal(prePerTrasfObj, 15, 3));
            ws.getIndOggCollg().setPrePerTrasf(((short)0));
        }
        else {
            ws.getIndOggCollg().setPrePerTrasf(((short)-1));
        }
    }

    @Override
    public AfDecimal getRecProv() {
        return oggCollg.getOcoRecProv().getOcoRecProv();
    }

    @Override
    public void setRecProv(AfDecimal recProv) {
        this.oggCollg.getOcoRecProv().setOcoRecProv(recProv.copy());
    }

    @Override
    public AfDecimal getRecProvObj() {
        if (ws.getIndOggCollg().getRecProv() >= 0) {
            return getRecProv();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRecProvObj(AfDecimal recProvObj) {
        if (recProvObj != null) {
            setRecProv(new AfDecimal(recProvObj, 15, 3));
            ws.getIndOggCollg().setRecProv(((short)0));
        }
        else {
            ws.getIndOggCollg().setRecProv(((short)-1));
        }
    }

    @Override
    public AfDecimal getRisMat() {
        return oggCollg.getOcoRisMat().getOcoRisMat();
    }

    @Override
    public void setRisMat(AfDecimal risMat) {
        this.oggCollg.getOcoRisMat().setOcoRisMat(risMat.copy());
    }

    @Override
    public AfDecimal getRisMatObj() {
        if (ws.getIndOggCollg().getRisMat() >= 0) {
            return getRisMat();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRisMatObj(AfDecimal risMatObj) {
        if (risMatObj != null) {
            setRisMat(new AfDecimal(risMatObj, 15, 3));
            ws.getIndOggCollg().setRisMat(((short)0));
        }
        else {
            ws.getIndOggCollg().setRisMat(((short)-1));
        }
    }

    @Override
    public AfDecimal getRisZil() {
        return oggCollg.getOcoRisZil().getOcoRisZil();
    }

    @Override
    public void setRisZil(AfDecimal risZil) {
        this.oggCollg.getOcoRisZil().setOcoRisZil(risZil.copy());
    }

    @Override
    public AfDecimal getRisZilObj() {
        if (ws.getIndOggCollg().getRisZil() >= 0) {
            return getRisZil();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRisZilObj(AfDecimal risZilObj) {
        if (risZilObj != null) {
            setRisZil(new AfDecimal(risZilObj, 15, 3));
            ws.getIndOggCollg().setRisZil(((short)0));
        }
        else {
            ws.getIndOggCollg().setRisZil(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotPre() {
        return oggCollg.getOcoTotPre().getOcoTotPre();
    }

    @Override
    public void setTotPre(AfDecimal totPre) {
        this.oggCollg.getOcoTotPre().setOcoTotPre(totPre.copy());
    }

    @Override
    public AfDecimal getTotPreObj() {
        if (ws.getIndOggCollg().getTotPre() >= 0) {
            return getTotPre();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotPreObj(AfDecimal totPreObj) {
        if (totPreObj != null) {
            setTotPre(new AfDecimal(totPreObj, 15, 3));
            ws.getIndOggCollg().setTotPre(((short)0));
        }
        else {
            ws.getIndOggCollg().setTotPre(((short)-1));
        }
    }

    @Override
    public String getTpCollgm() {
        return oggCollg.getOcoTpCollgm();
    }

    @Override
    public void setTpCollgm(String tpCollgm) {
        this.oggCollg.setOcoTpCollgm(tpCollgm);
    }

    @Override
    public String getTpModAbbinamento() {
        return oggCollg.getOcoTpModAbbinamento();
    }

    @Override
    public void setTpModAbbinamento(String tpModAbbinamento) {
        this.oggCollg.setOcoTpModAbbinamento(tpModAbbinamento);
    }

    @Override
    public String getTpModAbbinamentoObj() {
        if (ws.getIndOggCollg().getTpModAbbinamento() >= 0) {
            return getTpModAbbinamento();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpModAbbinamentoObj(String tpModAbbinamentoObj) {
        if (tpModAbbinamentoObj != null) {
            setTpModAbbinamento(tpModAbbinamentoObj);
            ws.getIndOggCollg().setTpModAbbinamento(((short)0));
        }
        else {
            ws.getIndOggCollg().setTpModAbbinamento(((short)-1));
        }
    }

    @Override
    public String getTpOggCoinv() {
        return oggCollg.getOcoTpOggCoinv();
    }

    @Override
    public void setTpOggCoinv(String tpOggCoinv) {
        this.oggCollg.setOcoTpOggCoinv(tpOggCoinv);
    }

    @Override
    public String getTpOggDer() {
        return oggCollg.getOcoTpOggDer();
    }

    @Override
    public void setTpOggDer(String tpOggDer) {
        this.oggCollg.setOcoTpOggDer(tpOggDer);
    }

    @Override
    public String getTpTrasf() {
        return oggCollg.getOcoTpTrasf();
    }

    @Override
    public void setTpTrasf(String tpTrasf) {
        this.oggCollg.setOcoTpTrasf(tpTrasf);
    }

    @Override
    public String getTpTrasfObj() {
        if (ws.getIndOggCollg().getTpTrasf() >= 0) {
            return getTpTrasf();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpTrasfObj(String tpTrasfObj) {
        if (tpTrasfObj != null) {
            setTpTrasf(tpTrasfObj);
            ws.getIndOggCollg().setTpTrasf(((short)0));
        }
        else {
            ws.getIndOggCollg().setTpTrasf(((short)-1));
        }
    }

    @Override
    public String getWsDataInizioEffettoDb() {
        throw new FieldNotMappedException("wsDataInizioEffettoDb");
    }

    @Override
    public void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb) {
        throw new FieldNotMappedException("wsDataInizioEffettoDb");
    }

    @Override
    public long getWsTsCompetenza() {
        throw new FieldNotMappedException("wsTsCompetenza");
    }

    @Override
    public void setWsTsCompetenza(long wsTsCompetenza) {
        throw new FieldNotMappedException("wsTsCompetenza");
    }
}
