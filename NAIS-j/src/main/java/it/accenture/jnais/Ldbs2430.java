package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.MoviStatOggWfDao;
import it.accenture.jnais.commons.data.to.IMoviStatOggWf;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ldbs2430Data;
import it.accenture.jnais.ws.MoviLdbs1530;
import it.accenture.jnais.ws.redefines.MovIdMoviAnn;
import it.accenture.jnais.ws.redefines.MovIdMoviCollg;
import it.accenture.jnais.ws.redefines.MovIdOgg;
import it.accenture.jnais.ws.redefines.MovIdRich;
import it.accenture.jnais.ws.redefines.MovTpMovi;

/**Original name: LDBS2430<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  27 APR 2010.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO AD HOC PER ACCESSO RISORSE DB        *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Ldbs2430 extends Program implements IMoviStatOggWf {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private MoviStatOggWfDao moviStatOggWfDao = new MoviStatOggWfDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Ldbs2430Data ws = new Ldbs2430Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: MOVI
    private MoviLdbs1530 movi;

    //==== METHODS ====
    /**Original name: PROGRAM_LDBS2430_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, MoviLdbs1530 movi) {
        this.idsv0003 = idsv0003;
        this.movi = movi;
        // COB_CODE: PERFORM A000-INIZIO              THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                        a200ElaboraWcEff();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                        b200ElaboraWcCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //               SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                        c200ElaboraWcNst();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Ldbs2430 getInstance() {
        return ((Ldbs2430)Programs.getInstance(Ldbs2430.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'LDBS2430'              TO   IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("LDBS2430");
        // COB_CODE: MOVE 'MOVI' TO   IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("MOVI");
        // COB_CODE: MOVE '00'                      TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                    TO   IDSV0003-SQLCODE
        //                                               IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                    TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                               IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-WC-EFF<br>*/
    private void a200ElaboraWcEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-WC-EFF          THRU A210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-WC-EFF          THRU A210-EX
            a210SelectWcEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
            a260OpenCursorWcEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
            a270CloseCursorWcEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
            a280FetchFirstWcEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
            a290FetchNextWcEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B200-ELABORA-WC-CPZ<br>*/
    private void b200ElaboraWcCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
            b210SelectWcCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
            b260OpenCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
            b270CloseCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
            b280FetchFirstWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
            b290FetchNextWcCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: C200-ELABORA-WC-NST<br>*/
    private void c200ElaboraWcNst() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM C210-SELECT-WC-NST          THRU C210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM C210-SELECT-WC-NST          THRU C210-EX
            c210SelectWcNst();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
            c260OpenCursorWcNst();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
            c270CloseCursorWcNst();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
            c280FetchFirstWcNst();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
            c290FetchNextWcNst();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A205-DECLARE-CURSOR-WC-EFF<br>
	 * <pre>----
	 * ----  gestione WC Effetto
	 * ----</pre>*/
    private void a205DeclareCursorWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //              DECLARE C-EFF CURSOR FOR
        //              SELECT
        //                     A.ID_MOVI
        //                    ,A.COD_COMP_ANIA
        //                    ,A.ID_OGG
        //                    ,A.IB_OGG
        //                    ,A.IB_MOVI
        //                    ,A.TP_OGG
        //                    ,A.ID_RICH
        //                    ,A.TP_MOVI
        //                    ,A.DT_EFF
        //                    ,A.ID_MOVI_ANN
        //                    ,A.ID_MOVI_COLLG
        //                    ,A.DS_OPER_SQL
        //                    ,A.DS_VER
        //                    ,A.DS_TS_CPTZ
        //                    ,A.DS_UTENTE
        //                    ,A.DS_STATO_ELAB
        //              FROM MOVI A,
        //                   STAT_OGG_WF B
        //              WHERE      A.ID_OGG = :MOV-ID-OGG
        //                    AND A.TP_OGG = :MOV-TP-OGG
        //                    AND B.ID_OGG = A.ID_MOVI
        //                    AND B.TP_OGG = 'MO'
        //                    AND B.STAT_OGG_WF = 'CO'
        //                    AND A.DT_EFF > :MOV-DT-EFF-DB
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DT-INFINITO-1
        //                    AND B.DT_END_EFF >
        //                        :WS-DT-INFINITO-1
        //                    AND B.DS_TS_INI_CPTZ <=
        //                         :WS-TS-INFINITO-1
        //                    AND B.DS_TS_END_CPTZ >
        //                         :WS-TS-INFINITO-1
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND B.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //              ORDER BY DT_EFF DESC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A210-SELECT-WC-EFF<br>*/
    private void a210SelectWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                     A.ID_MOVI
        //                    ,A.COD_COMP_ANIA
        //                    ,A.ID_OGG
        //                    ,A.IB_OGG
        //                    ,A.IB_MOVI
        //                    ,A.TP_OGG
        //                    ,A.ID_RICH
        //                    ,A.TP_MOVI
        //                    ,A.DT_EFF
        //                    ,A.ID_MOVI_ANN
        //                    ,A.ID_MOVI_COLLG
        //                    ,A.DS_OPER_SQL
        //                    ,A.DS_VER
        //                    ,A.DS_TS_CPTZ
        //                    ,A.DS_UTENTE
        //                    ,A.DS_STATO_ELAB
        //             INTO
        //                :MOV-ID-MOVI
        //               ,:MOV-COD-COMP-ANIA
        //               ,:MOV-ID-OGG
        //                :IND-MOV-ID-OGG
        //               ,:MOV-IB-OGG
        //                :IND-MOV-IB-OGG
        //               ,:MOV-IB-MOVI
        //                :IND-MOV-IB-MOVI
        //               ,:MOV-TP-OGG
        //                :IND-MOV-TP-OGG
        //               ,:MOV-ID-RICH
        //                :IND-MOV-ID-RICH
        //               ,:MOV-TP-MOVI
        //                :IND-MOV-TP-MOVI
        //               ,:MOV-DT-EFF-DB
        //               ,:MOV-ID-MOVI-ANN
        //                :IND-MOV-ID-MOVI-ANN
        //               ,:MOV-ID-MOVI-COLLG
        //                :IND-MOV-ID-MOVI-COLLG
        //               ,:MOV-DS-OPER-SQL
        //               ,:MOV-DS-VER
        //               ,:MOV-DS-TS-CPTZ
        //               ,:MOV-DS-UTENTE
        //               ,:MOV-DS-STATO-ELAB
        //             FROM MOVI A,
        //                  STAT_OGG_WF B
        //             WHERE      A.ID_OGG = :MOV-ID-OGG
        //                    AND A.TP_OGG = :MOV-TP-OGG
        //                    AND B.ID_OGG = A.ID_MOVI
        //                    AND B.TP_OGG = 'MO'
        //                    AND B.STAT_OGG_WF = 'CO'
        //                    AND A.DT_EFF > :MOV-DT-EFF-DB
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DT-INFINITO-1
        //                    AND B.DT_END_EFF >
        //                        :WS-DT-INFINITO-1
        //                    AND B.DS_TS_INI_CPTZ <=
        //                         :WS-TS-INFINITO-1
        //                    AND B.DS_TS_END_CPTZ >
        //                         :WS-TS-INFINITO-1
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND B.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //              ORDER BY DT_EFF DESC
        //           END-EXEC.
        moviStatOggWfDao.selectRec(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: A260-OPEN-CURSOR-WC-EFF<br>*/
    private void a260OpenCursorWcEff() {
        // COB_CODE: PERFORM A205-DECLARE-CURSOR-WC-EFF THRU A205-EX.
        a205DeclareCursorWcEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-EFF
        //           END-EXEC.
        moviStatOggWfDao.openCEff51(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A270-CLOSE-CURSOR-WC-EFF<br>*/
    private void a270CloseCursorWcEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-EFF
        //           END-EXEC.
        moviStatOggWfDao.closeCEff51();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A280-FETCH-FIRST-WC-EFF<br>*/
    private void a280FetchFirstWcEff() {
        // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF    THRU A260-EX.
        a260OpenCursorWcEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
            a290FetchNextWcEff();
        }
    }

    /**Original name: A290-FETCH-NEXT-WC-EFF<br>*/
    private void a290FetchNextWcEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-EFF
        //           INTO
        //                :MOV-ID-MOVI
        //               ,:MOV-COD-COMP-ANIA
        //               ,:MOV-ID-OGG
        //                :IND-MOV-ID-OGG
        //               ,:MOV-IB-OGG
        //                :IND-MOV-IB-OGG
        //               ,:MOV-IB-MOVI
        //                :IND-MOV-IB-MOVI
        //               ,:MOV-TP-OGG
        //                :IND-MOV-TP-OGG
        //               ,:MOV-ID-RICH
        //                :IND-MOV-ID-RICH
        //               ,:MOV-TP-MOVI
        //                :IND-MOV-TP-MOVI
        //               ,:MOV-DT-EFF-DB
        //               ,:MOV-ID-MOVI-ANN
        //                :IND-MOV-ID-MOVI-ANN
        //               ,:MOV-ID-MOVI-COLLG
        //                :IND-MOV-ID-MOVI-COLLG
        //               ,:MOV-DS-OPER-SQL
        //               ,:MOV-DS-VER
        //               ,:MOV-DS-TS-CPTZ
        //               ,:MOV-DS-UTENTE
        //               ,:MOV-DS-STATO-ELAB
        //           END-EXEC.
        moviStatOggWfDao.fetchCEff51(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF THRU A270-EX
            a270CloseCursorWcEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B205-DECLARE-CURSOR-WC-CPZ<br>
	 * <pre>----
	 * ----  gestione WC Competenza
	 * ----</pre>*/
    private void b205DeclareCursorWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //              DECLARE C-CPZ CURSOR FOR
        //              SELECT
        //                     A.ID_MOVI
        //                    ,A.COD_COMP_ANIA
        //                    ,A.ID_OGG
        //                    ,A.IB_OGG
        //                    ,A.IB_MOVI
        //                    ,A.TP_OGG
        //                    ,A.ID_RICH
        //                    ,A.TP_MOVI
        //                    ,A.DT_EFF
        //                    ,A.ID_MOVI_ANN
        //                    ,A.ID_MOVI_COLLG
        //                    ,A.DS_OPER_SQL
        //                    ,A.DS_VER
        //                    ,A.DS_TS_CPTZ
        //                    ,A.DS_UTENTE
        //                    ,A.DS_STATO_ELAB
        //              FROM MOVI A,
        //                   STAT_OGG_WF B
        //              WHERE      A.ID_OGG = :MOV-ID-OGG
        //                        AND A.TP_OGG = :MOV-TP-OGG
        //                        AND B.ID_OGG = A.ID_MOVI
        //                        AND B.TP_OGG = 'MO'
        //                        AND B.STAT_OGG_WF = 'CO'
        //                        AND A.DT_EFF > :MOV-DT-EFF-DB
        //                        AND B.DT_INI_EFF <=
        //                            :WS-DT-INFINITO-1
        //                        AND B.DT_END_EFF >
        //                            :WS-DT-INFINITO-1
        //                        AND B.DS_TS_INI_CPTZ <=
        //                             :WS-TS-INFINITO-1
        //                        AND B.DS_TS_END_CPTZ >
        //                             :WS-TS-INFINITO-1
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND B.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //              ORDER BY DT_EFF DESC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B210-SELECT-WC-CPZ<br>*/
    private void b210SelectWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                     A.ID_MOVI
        //                    ,A.COD_COMP_ANIA
        //                    ,A.ID_OGG
        //                    ,A.IB_OGG
        //                    ,A.IB_MOVI
        //                    ,A.TP_OGG
        //                    ,A.ID_RICH
        //                    ,A.TP_MOVI
        //                    ,A.DT_EFF
        //                    ,A.ID_MOVI_ANN
        //                    ,A.ID_MOVI_COLLG
        //                    ,A.DS_OPER_SQL
        //                    ,A.DS_VER
        //                    ,A.DS_TS_CPTZ
        //                    ,A.DS_UTENTE
        //                    ,A.DS_STATO_ELAB
        //             INTO
        //                :MOV-ID-MOVI
        //               ,:MOV-COD-COMP-ANIA
        //               ,:MOV-ID-OGG
        //                :IND-MOV-ID-OGG
        //               ,:MOV-IB-OGG
        //                :IND-MOV-IB-OGG
        //               ,:MOV-IB-MOVI
        //                :IND-MOV-IB-MOVI
        //               ,:MOV-TP-OGG
        //                :IND-MOV-TP-OGG
        //               ,:MOV-ID-RICH
        //                :IND-MOV-ID-RICH
        //               ,:MOV-TP-MOVI
        //                :IND-MOV-TP-MOVI
        //               ,:MOV-DT-EFF-DB
        //               ,:MOV-ID-MOVI-ANN
        //                :IND-MOV-ID-MOVI-ANN
        //               ,:MOV-ID-MOVI-COLLG
        //                :IND-MOV-ID-MOVI-COLLG
        //               ,:MOV-DS-OPER-SQL
        //               ,:MOV-DS-VER
        //               ,:MOV-DS-TS-CPTZ
        //               ,:MOV-DS-UTENTE
        //               ,:MOV-DS-STATO-ELAB
        //             FROM MOVI A,
        //                  STAT_OGG_WF B
        //             WHERE      A.ID_OGG = :MOV-ID-OGG
        //                    AND A.TP_OGG = :MOV-TP-OGG
        //                    AND B.ID_OGG = A.ID_MOVI
        //                    AND B.TP_OGG = 'MO'
        //                    AND B.STAT_OGG_WF = 'CO'
        //                    AND A.DT_EFF > :MOV-DT-EFF-DB
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DT-INFINITO-1
        //                    AND B.DT_END_EFF >
        //                        :WS-DT-INFINITO-1
        //                    AND B.DS_TS_INI_CPTZ <=
        //                         :WS-TS-INFINITO-1
        //                    AND B.DS_TS_END_CPTZ >
        //                         :WS-TS-INFINITO-1
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND B.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //              ORDER BY DT_EFF DESC
        //           END-EXEC.
        moviStatOggWfDao.selectRec(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: B260-OPEN-CURSOR-WC-CPZ<br>*/
    private void b260OpenCursorWcCpz() {
        // COB_CODE: PERFORM B205-DECLARE-CURSOR-WC-CPZ THRU B205-EX.
        b205DeclareCursorWcCpz();
        // COB_CODE: EXEC SQL
        //                OPEN C-CPZ
        //           END-EXEC.
        moviStatOggWfDao.openCEff51(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B270-CLOSE-CURSOR-WC-CPZ<br>*/
    private void b270CloseCursorWcCpz() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-CPZ
        //           END-EXEC.
        moviStatOggWfDao.closeCEff51();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B280-FETCH-FIRST-WC-CPZ<br>*/
    private void b280FetchFirstWcCpz() {
        // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ    THRU B260-EX.
        b260OpenCursorWcCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
            b290FetchNextWcCpz();
        }
    }

    /**Original name: B290-FETCH-NEXT-WC-CPZ<br>*/
    private void b290FetchNextWcCpz() {
        // COB_CODE: EXEC SQL
        //                FETCH C-CPZ
        //           INTO
        //                :MOV-ID-MOVI
        //               ,:MOV-COD-COMP-ANIA
        //               ,:MOV-ID-OGG
        //                :IND-MOV-ID-OGG
        //               ,:MOV-IB-OGG
        //                :IND-MOV-IB-OGG
        //               ,:MOV-IB-MOVI
        //                :IND-MOV-IB-MOVI
        //               ,:MOV-TP-OGG
        //                :IND-MOV-TP-OGG
        //               ,:MOV-ID-RICH
        //                :IND-MOV-ID-RICH
        //               ,:MOV-TP-MOVI
        //                :IND-MOV-TP-MOVI
        //               ,:MOV-DT-EFF-DB
        //               ,:MOV-ID-MOVI-ANN
        //                :IND-MOV-ID-MOVI-ANN
        //               ,:MOV-ID-MOVI-COLLG
        //                :IND-MOV-ID-MOVI-COLLG
        //               ,:MOV-DS-OPER-SQL
        //               ,:MOV-DS-VER
        //               ,:MOV-DS-TS-CPTZ
        //               ,:MOV-DS-UTENTE
        //               ,:MOV-DS-STATO-ELAB
        //           END-EXEC.
        moviStatOggWfDao.fetchCEff51(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ THRU B270-EX
            b270CloseCursorWcCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: C205-DECLARE-CURSOR-WC-NST<br>
	 * <pre>----
	 * ----  gestione WC Senza Storicità
	 * ----</pre>*/
    private void c205DeclareCursorWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C210-SELECT-WC-NST<br>*/
    private void c210SelectWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C260-OPEN-CURSOR-WC-NST<br>*/
    private void c260OpenCursorWcNst() {
        // COB_CODE: PERFORM C205-DECLARE-CURSOR-WC-NST THRU C205-EX.
        c205DeclareCursorWcNst();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C270-CLOSE-CURSOR-WC-NST<br>*/
    private void c270CloseCursorWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C280-FETCH-FIRST-WC-NST<br>*/
    private void c280FetchFirstWcNst() {
        // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST    THRU C260-EX.
        c260OpenCursorWcNst();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
            c290FetchNextWcNst();
        }
    }

    /**Original name: C290-FETCH-NEXT-WC-NST<br>*/
    private void c290FetchNextWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>----
	 * ----  utilità comuni a tutti i livelli operazione
	 * ----</pre>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-MOV-ID-OGG = -1
        //              MOVE HIGH-VALUES TO MOV-ID-OGG-NULL
        //           END-IF
        if (ws.getIndMovi().getDtStart() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MOV-ID-OGG-NULL
            movi.getMovIdOgg().setMovIdOggNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MovIdOgg.Len.MOV_ID_OGG_NULL));
        }
        // COB_CODE: IF IND-MOV-IB-OGG = -1
        //              MOVE HIGH-VALUES TO MOV-IB-OGG-NULL
        //           END-IF
        if (ws.getIndMovi().getDtEnd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MOV-IB-OGG-NULL
            movi.setMovIbOgg(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MoviLdbs1530.Len.MOV_IB_OGG));
        }
        // COB_CODE: IF IND-MOV-IB-MOVI = -1
        //              MOVE HIGH-VALUES TO MOV-IB-MOVI-NULL
        //           END-IF
        if (ws.getIndMovi().getUserStart() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MOV-IB-MOVI-NULL
            movi.setMovIbMovi(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MoviLdbs1530.Len.MOV_IB_MOVI));
        }
        // COB_CODE: IF IND-MOV-TP-OGG = -1
        //              MOVE HIGH-VALUES TO MOV-TP-OGG-NULL
        //           END-IF
        if (ws.getIndMovi().getDescParallelism() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MOV-TP-OGG-NULL
            movi.setMovTpOgg(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MoviLdbs1530.Len.MOV_TP_OGG));
        }
        // COB_CODE: IF IND-MOV-ID-RICH = -1
        //              MOVE HIGH-VALUES TO MOV-ID-RICH-NULL
        //           END-IF
        if (ws.getIndMovi().getIdOggDa() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MOV-ID-RICH-NULL
            movi.getMovIdRich().setMovIdRichNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MovIdRich.Len.MOV_ID_RICH_NULL));
        }
        // COB_CODE: IF IND-MOV-TP-MOVI = -1
        //              MOVE HIGH-VALUES TO MOV-TP-MOVI-NULL
        //           END-IF
        if (ws.getIndMovi().getIdOggA() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MOV-TP-MOVI-NULL
            movi.getMovTpMovi().setMovTpMoviNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MovTpMovi.Len.MOV_TP_MOVI_NULL));
        }
        // COB_CODE: IF IND-MOV-ID-MOVI-ANN = -1
        //              MOVE HIGH-VALUES TO MOV-ID-MOVI-ANN-NULL
        //           END-IF
        if (ws.getIndMovi().getTpOgg() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MOV-ID-MOVI-ANN-NULL
            movi.getMovIdMoviAnn().setMovIdMoviAnnNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MovIdMoviAnn.Len.MOV_ID_MOVI_ANN_NULL));
        }
        // COB_CODE: IF IND-MOV-ID-MOVI-COLLG = -1
        //              MOVE HIGH-VALUES TO MOV-ID-MOVI-COLLG-NULL
        //           END-IF.
        if (ws.getIndMovi().getNumRowSchedule() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MOV-ID-MOVI-COLLG-NULL
            movi.getMovIdMoviCollg().setMovIdMoviCollgNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MovIdMoviCollg.Len.MOV_ID_MOVI_COLLG_NULL));
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE MOV-DT-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getMovDtEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO MOV-DT-EFF.
        movi.setMovDtEff(ws.getIdsv0010().getWsDateN());
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z970-CODICE-ADHOC-PRE<br>
	 * <pre>----
	 * ----  prevede statements AD HOC PRE Query
	 * ----</pre>*/
    private void z970CodiceAdhocPre() {
        // COB_CODE: IF MOV-DT-EFF IS NUMERIC
        //             END-IF
        //           END-IF.
        if (Functions.isNumber(movi.getMovDtEff())) {
            // COB_CODE: IF MOV-DT-EFF GREATER ZERO
            //              MOVE WS-DATE-X      TO MOV-DT-EFF-DB
            //           END-IF
            if (movi.getMovDtEff() > 0) {
                // COB_CODE: MOVE MOV-DT-EFF TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(movi.getMovDtEff(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X      TO MOV-DT-EFF-DB
                ws.setMovDtEffDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: Z980-CODICE-ADHOC-POST<br>
	 * <pre>----
	 * ----  prevede statements AD HOC POST Query
	 * ----</pre>*/
    private void z980CodiceAdhocPost() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public int getCodCompAnia() {
        return movi.getMovCodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.movi.setMovCodCompAnia(codCompAnia);
    }

    @Override
    public char getDsOperSql() {
        return movi.getMovDsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.movi.setMovDsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return movi.getMovDsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.movi.setMovDsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsCptz() {
        return movi.getMovDsTsCptz();
    }

    @Override
    public void setDsTsCptz(long dsTsCptz) {
        this.movi.setMovDsTsCptz(dsTsCptz);
    }

    @Override
    public String getDsUtente() {
        return movi.getMovDsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.movi.setMovDsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return movi.getMovDsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.movi.setMovDsVer(dsVer);
    }

    @Override
    public String getIbMovi() {
        return movi.getMovIbMovi();
    }

    @Override
    public void setIbMovi(String ibMovi) {
        this.movi.setMovIbMovi(ibMovi);
    }

    @Override
    public String getIbMoviObj() {
        if (ws.getIndMovi().getUserStart() >= 0) {
            return getIbMovi();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIbMoviObj(String ibMoviObj) {
        if (ibMoviObj != null) {
            setIbMovi(ibMoviObj);
            ws.getIndMovi().setUserStart(((short)0));
        }
        else {
            ws.getIndMovi().setUserStart(((short)-1));
        }
    }

    @Override
    public String getIbOgg() {
        return movi.getMovIbOgg();
    }

    @Override
    public void setIbOgg(String ibOgg) {
        this.movi.setMovIbOgg(ibOgg);
    }

    @Override
    public String getIbOggObj() {
        if (ws.getIndMovi().getDtEnd() >= 0) {
            return getIbOgg();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIbOggObj(String ibOggObj) {
        if (ibOggObj != null) {
            setIbOgg(ibOggObj);
            ws.getIndMovi().setDtEnd(((short)0));
        }
        else {
            ws.getIndMovi().setDtEnd(((short)-1));
        }
    }

    @Override
    public int getIdMovi() {
        return movi.getMovIdMovi();
    }

    @Override
    public int getIdMoviAnn() {
        return movi.getMovIdMoviAnn().getMovIdMoviAnn();
    }

    @Override
    public void setIdMoviAnn(int idMoviAnn) {
        this.movi.getMovIdMoviAnn().setMovIdMoviAnn(idMoviAnn);
    }

    @Override
    public Integer getIdMoviAnnObj() {
        if (ws.getIndMovi().getTpOgg() >= 0) {
            return ((Integer)getIdMoviAnn());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdMoviAnnObj(Integer idMoviAnnObj) {
        if (idMoviAnnObj != null) {
            setIdMoviAnn(((int)idMoviAnnObj));
            ws.getIndMovi().setTpOgg(((short)0));
        }
        else {
            ws.getIndMovi().setTpOgg(((short)-1));
        }
    }

    @Override
    public void setIdMovi(int idMovi) {
        this.movi.setMovIdMovi(idMovi);
    }

    @Override
    public int getIdMoviCollg() {
        return movi.getMovIdMoviCollg().getMovIdMoviCollg();
    }

    @Override
    public void setIdMoviCollg(int idMoviCollg) {
        this.movi.getMovIdMoviCollg().setMovIdMoviCollg(idMoviCollg);
    }

    @Override
    public Integer getIdMoviCollgObj() {
        if (ws.getIndMovi().getNumRowSchedule() >= 0) {
            return ((Integer)getIdMoviCollg());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdMoviCollgObj(Integer idMoviCollgObj) {
        if (idMoviCollgObj != null) {
            setIdMoviCollg(((int)idMoviCollgObj));
            ws.getIndMovi().setNumRowSchedule(((short)0));
        }
        else {
            ws.getIndMovi().setNumRowSchedule(((short)-1));
        }
    }

    @Override
    public int getIdRich() {
        return movi.getMovIdRich().getMovIdRich();
    }

    @Override
    public void setIdRich(int idRich) {
        this.movi.getMovIdRich().setMovIdRich(idRich);
    }

    @Override
    public Integer getIdRichObj() {
        if (ws.getIndMovi().getIdOggDa() >= 0) {
            return ((Integer)getIdRich());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdRichObj(Integer idRichObj) {
        if (idRichObj != null) {
            setIdRich(((int)idRichObj));
            ws.getIndMovi().setIdOggDa(((short)0));
        }
        else {
            ws.getIndMovi().setIdOggDa(((short)-1));
        }
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        return idsv0003.getCodiceCompagniaAnia();
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        this.idsv0003.setCodiceCompagniaAnia(idsv0003CodiceCompagniaAnia);
    }

    @Override
    public String getMovDtEffDb() {
        return ws.getMovDtEffDb();
    }

    @Override
    public void setMovDtEffDb(String movDtEffDb) {
        this.ws.setMovDtEffDb(movDtEffDb);
    }

    @Override
    public int getMovIdOgg() {
        return movi.getMovIdOgg().getMovIdOgg();
    }

    @Override
    public void setMovIdOgg(int movIdOgg) {
        this.movi.getMovIdOgg().setMovIdOgg(movIdOgg);
    }

    @Override
    public Integer getMovIdOggObj() {
        if (ws.getIndMovi().getDtStart() >= 0) {
            return ((Integer)getMovIdOgg());
        }
        else {
            return null;
        }
    }

    @Override
    public void setMovIdOggObj(Integer movIdOggObj) {
        if (movIdOggObj != null) {
            setMovIdOgg(((int)movIdOggObj));
            ws.getIndMovi().setDtStart(((short)0));
        }
        else {
            ws.getIndMovi().setDtStart(((short)-1));
        }
    }

    @Override
    public String getMovTpOgg() {
        return movi.getMovTpOgg();
    }

    @Override
    public void setMovTpOgg(String movTpOgg) {
        this.movi.setMovTpOgg(movTpOgg);
    }

    @Override
    public String getMovTpOggObj() {
        if (ws.getIndMovi().getDescParallelism() >= 0) {
            return getMovTpOgg();
        }
        else {
            return null;
        }
    }

    @Override
    public void setMovTpOggObj(String movTpOggObj) {
        if (movTpOggObj != null) {
            setMovTpOgg(movTpOggObj);
            ws.getIndMovi().setDescParallelism(((short)0));
        }
        else {
            ws.getIndMovi().setDescParallelism(((short)-1));
        }
    }

    @Override
    public int getTpMovi() {
        return movi.getMovTpMovi().getMovTpMovi();
    }

    @Override
    public void setTpMovi(int tpMovi) {
        this.movi.getMovTpMovi().setMovTpMovi(tpMovi);
    }

    @Override
    public Integer getTpMoviObj() {
        if (ws.getIndMovi().getIdOggA() >= 0) {
            return ((Integer)getTpMovi());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpMoviObj(Integer tpMoviObj) {
        if (tpMoviObj != null) {
            setTpMovi(((int)tpMoviObj));
            ws.getIndMovi().setIdOggA(((short)0));
        }
        else {
            ws.getIndMovi().setIdOggA(((short)-1));
        }
    }

    @Override
    public String getWsDtInfinito1() {
        return ws.getIdsv0010().getWsDtInfinito1();
    }

    @Override
    public void setWsDtInfinito1(String wsDtInfinito1) {
        this.ws.getIdsv0010().setWsDtInfinito1(wsDtInfinito1);
    }

    @Override
    public long getWsTsInfinito1() {
        return ws.getIdsv0010().getWsTsInfinito1();
    }

    @Override
    public void setWsTsInfinito1(long wsTsInfinito1) {
        this.ws.getIdsv0010().setWsTsInfinito1(wsTsInfinito1);
    }
}
