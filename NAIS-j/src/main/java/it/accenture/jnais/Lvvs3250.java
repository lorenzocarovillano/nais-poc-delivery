package it.accenture.jnais;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Idsv0003CampiEsito;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.Lvvs3250Data;

/**Original name: LVVS3250<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2011.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 * **         CALCOLO DELLA VARIABILE FP - Frequenza movimento   ***
 * **------------------------------------------------------------***</pre>*/
public class Lvvs3250 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lvvs3250Data ws = new Lvvs3250Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS3250
    private Ivvc0213 ivvc0213;

    //==== METHODS ====
    /**Original name: PROGRAM_LVVS3250_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        //
        // COB_CODE: PERFORM S1000-ELABORAZIONE
        //              THRU EX-S1000
        s1000Elaborazione();
        //
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lvvs3250 getInstance() {
        return ((Lvvs3250)Programs.getInstance(Lvvs3250.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                        IX-INDICI
        //                                             IVVC0213-TAB-OUTPUT.
        initIxIndici();
        initTabOutput();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: INITIALIZE AREA-IO-PMO.
        initAreaIoPmo();
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE
        //             TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
        // COB_CODE: MOVE IDSV0003-TIPO-MOVIMENTO      TO WS-MOVIMENTO.
        ws.getWsMovimento().setWsMovimentoFormatted(idsv0003.getTipoMovimentoFormatted());
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: MOVE ZEROES TO IVVC0213-VAL-IMP-O.
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        //--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
        //--> RISPETTIVE AREE DCLGEN IN WORKING
        // COB_CODE: PERFORM S1100-VALORIZZA-DCLGEN
        //              THRU S1100-VALORIZZA-DCLGEN-EX
        //           VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
        //                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //                   SPACES OR LOW-VALUE OR HIGH-VALUE
        ws.setIxDclgen(((short)1));
        while (!(ws.getIxDclgen() > ivvc0213.getEleInfoMax() || Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias()) || Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()) || Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()))) {
            s1100ValorizzaDclgen();
            ws.setIxDclgen(Trunc.toShort(ws.getIxDclgen() + 1, 4));
        }
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM S1300-CERCA-PARAM-MOVI
            //              THRU S1300-EX
            s1300CercaParamMovi();
            // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
            //           AND IDSV0003-SUCCESSFUL-SQL
            //               END-IF
            //           END-IF
            if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: IF  CALCOLO-RISERVE
                //           AND IVVC0213-VAL-IMP-O = ZEROES
                //                  THRU S1310-EX
                //           END-IF
                if (ws.getWsMovimento().isCalcoloRiserve() && ivvc0213.getTabOutput().getValImpO().compareTo(0) == 0) {
                    // COB_CODE: PERFORM S1310-LEGGI-PMO-CHIUSA
                    //              THRU S1310-EX
                    s1310LeggiPmoChiusa();
                }
            }
        }
    }

    /**Original name: S1100-VALORIZZA-DCLGEN<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 *     RISPETTIVE AREE DCLGEN IN WORKING
	 * ----------------------------------------------------------------*</pre>*/
    private void s1100ValorizzaDclgen() {
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-PARAM-MOV
        //                TO DPMO-AREA-PMO
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias(), ws.getIvvc0218().getAliasParamMov())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO DPMO-AREA-PMO
            ws.setDpmoAreaPmoFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxDclgen()).getLunghezza() - 1));
        }
    }

    /**Original name: S1300-CERCA-PARAM-MOVI<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLO DATI DCLGEN
	 * ----------------------------------------------------------------*</pre>*/
    private void s1300CercaParamMovi() {
        // COB_CODE: IF DPMO-ELE-PMO-MAX > 0
        //              END-PERFORM
        //           END-IF.
        if (ws.getDpmoElePmoMax() > 0) {
            // COB_CODE: PERFORM VARYING IX-TAB-PMO FROM 1 BY 1
            //             UNTIL IX-TAB-PMO > DPMO-ELE-PMO-MAX
            //             END-IF
            //           END-PERFORM
            ws.setIxTabPmo(((short)1));
            while (!(ws.getIxTabPmo() > ws.getDpmoElePmoMax())) {
                // COB_CODE: IF DPMO-TP-MOVI(IX-TAB-PMO) = 6101
                //           OR DPMO-TP-MOVI(IX-TAB-PMO) = 6002
                //           OR DPMO-TP-MOVI(IX-TAB-PMO) = 6003
                //              END-IF
                //           END-IF
                if (ws.getDpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoTpMovi().getWpmoTpMovi() == 6101 || ws.getDpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoTpMovi().getWpmoTpMovi() == 6002 || ws.getDpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoTpMovi().getWpmoTpMovi() == 6003) {
                    // COB_CODE: IF DPMO-FRQ-MOVI-NULL(IX-TAB-PMO)= HIGH-VALUES
                    //              SET IDSV0003-FIELD-NOT-VALUED   TO TRUE
                    //           ELSE
                    //                TO IVVC0213-VAL-IMP-O
                    //           END-IF
                    if (Characters.EQ_HIGH.test(ws.getDpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoFrqMovi().getWpmoFrqMoviNullFormatted())) {
                        // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED   TO TRUE
                        idsv0003.getReturnCode().setFieldNotValued();
                    }
                    else {
                        // COB_CODE: MOVE DPMO-FRQ-MOVI(IX-TAB-PMO)
                        //             TO IVVC0213-VAL-IMP-O
                        ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(ws.getDpmoTabParamMov(ws.getIxTabPmo()).getLccvpmo1().getDati().getWpmoFrqMovi().getWpmoFrqMovi(), 18, 7));
                    }
                }
                ws.setIxTabPmo(Trunc.toShort(ws.getIxTabPmo() + 1, 4));
            }
        }
    }

    /**Original name: S1310-LEGGI-PMO-CHIUSA<br>
	 * <pre>----------------------------------------------------------------*
	 *    LEGGI L'IMMAGINE DELLA PMO CHIUSA DALLA RIDUZIONE O
	 *    DALLA SOSPENSIONE PIANO VERSAMENTO
	 * ----------------------------------------------------------------*</pre>*/
    private void s1310LeggiPmoChiusa() {
        Ldbsf970 ldbsf970 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE PARAM-MOVI.
        initParamMovi();
        // COB_CODE: SET  IDSV0003-TRATT-X-COMPETENZA TO TRUE
        idsv0003.getTrattamentoStoricita().setTrattXCompetenza();
        //-->  LIVELLO OPERAZIONE
        // COB_CODE: SET IDSV0003-WHERE-CONDITION  TO TRUE
        idsv0003.getLivelloOperazione().setWhereCondition();
        //-->  TIPO OPERAZIONE
        // COB_CODE: SET IDSV0003-SELECT           TO TRUE
        idsv0003.getOperazione().setSelect();
        // COB_CODE: MOVE IVVC0213-ID-POLIZZA       TO PMO-ID-POLI
        ws.getParamMovi().setPmoIdPoli(ivvc0213.getIdPolizza());
        // COB_CODE: MOVE 6101                      TO LDBVF971-TP-MOVI-1
        ws.getLdbvf971().setLdbvf971TpMovi1(6101);
        // COB_CODE: MOVE 6002                      TO LDBVF971-TP-MOVI-2
        ws.getLdbvf971().setLdbvf971TpMovi2(6002);
        // COB_CODE: MOVE 6003                      TO LDBVF971-TP-MOVI-3
        ws.getLdbvf971().setLdbvf971TpMovi3(6003);
        // COB_CODE: MOVE LDBVF971                 TO IDSV0003-BUFFER-WHERE-COND
        idsv0003.setBufferWhereCond(ws.getLdbvf971().getLdbvf971Formatted());
        // COB_CODE: MOVE 'LDBSF970'               TO WK-CALL-PGM
        ws.setWkCallPgm("LDBSF970");
        //
        // COB_CODE:      CALL WK-CALL-PGM  USING  IDSV0003 PARAM-MOVI
        //           *
        //                ON EXCEPTION
        //                     SET IDSV0003-INVALID-OPER  TO TRUE
        //                END-CALL.
        try {
            ldbsf970 = Ldbsf970.getInstance();
            ldbsf970.run(idsv0003, ws.getParamMovi());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-CALL-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: MOVE 'CALL-LDBSF970 ERRORE CHIAMATA '
            //              TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBSF970 ERRORE CHIAMATA ");
            // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        //
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //               END-IF
        //           ELSE
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: IF PMO-FRQ-MOVI-NULL = HIGH-VALUES
            //              SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
            //           ELSE
            //              MOVE PMO-FRQ-MOVI        TO  IVVC0213-VAL-IMP-O
            //           END-IF
            if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoFrqMovi().getPmoFrqMoviNullFormatted())) {
                // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
            }
            else {
                // COB_CODE: MOVE PMO-FRQ-MOVI        TO  IVVC0213-VAL-IMP-O
                ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(ws.getParamMovi().getPmoFrqMovi().getPmoFrqMovi(), 18, 7));
            }
        }
        else {
            // COB_CODE: MOVE WK-CALL-PGM             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: STRING 'CHIAMATA LDBSF970 ;'
            //                  IDSV0003-RETURN-CODE ';'
            //                  IDSV0003-SQLCODE
            //              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LDBSF970 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
            //           ELSE
            //              SET IDSV0003-INVALID-OPER            TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isNotFound()) {
                // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-OPER            TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    public void initIxIndici() {
        ws.setIxDclgen(((short)0));
        ws.setIxTabPmo(((short)0));
    }

    public void initTabOutput() {
        ivvc0213.getTabOutput().setCodVariabileO("");
        ivvc0213.getTabOutput().setTpDatoO(Types.SPACE_CHAR);
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        ivvc0213.getTabOutput().setValPercO(new AfDecimal(0, 14, 9));
        ivvc0213.getTabOutput().setValStrO("");
    }

    public void initAreaIoPmo() {
        ws.setDpmoElePmoMax(((short)0));
        for (int idx0 = 1; idx0 <= Lvvs3250Data.DPMO_TAB_PARAM_MOV_MAXOCCURS; idx0++) {
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().setIdPtf(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoIdParamMovi(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoIdOgg(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoTpOgg("");
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoIdMoviCrz(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoIdMoviChiu().setWpmoIdMoviChiu(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoDtIniEff(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoDtEndEff(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoCodCompAnia(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoTpMovi().setWpmoTpMovi(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoFrqMovi().setWpmoFrqMovi(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoDurAa().setWpmoDurAa(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoDurMm().setWpmoDurMm(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoDurGg().setWpmoDurGg(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoDtRicorPrec().setWpmoDtRicorPrec(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoDtRicorSucc().setWpmoDtRicorSucc(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoPcIntrFraz().setWpmoPcIntrFraz(new AfDecimal(0, 6, 3));
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoImpBnsDaScoTot().setWpmoImpBnsDaScoTot(new AfDecimal(0, 15, 3));
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoImpBnsDaSco().setWpmoImpBnsDaSco(new AfDecimal(0, 15, 3));
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoPcAnticBns().setWpmoPcAnticBns(new AfDecimal(0, 6, 3));
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoTpRinnColl("");
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoTpRivalPre("");
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoTpRivalPrstz("");
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoFlEvidRival(Types.SPACE_CHAR);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoUltPcPerd().setWpmoUltPcPerd(new AfDecimal(0, 6, 3));
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoTotAaGiaPror().setWpmoTotAaGiaPror(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoTpOpz("");
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoAaRenCer().setWpmoAaRenCer(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoPcRevrsb().setWpmoPcRevrsb(new AfDecimal(0, 6, 3));
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoImpRiscParzPrgt().setWpmoImpRiscParzPrgt(new AfDecimal(0, 15, 3));
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoImpLrdDiRat().setWpmoImpLrdDiRat(new AfDecimal(0, 15, 3));
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoIbOgg("");
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoCosOner().setWpmoCosOner(new AfDecimal(0, 15, 3));
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoSpePc().setWpmoSpePc(new AfDecimal(0, 6, 3));
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoFlAttivGar(Types.SPACE_CHAR);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoCambioVerProd(Types.SPACE_CHAR);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoMmDiff().setWpmoMmDiff(((short)0));
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoImpRatManfee().setWpmoImpRatManfee(new AfDecimal(0, 15, 3));
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoDtUltErogManfee().setWpmoDtUltErogManfee(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoTpOggRival("");
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoSomAsstaGarac().setWpmoSomAsstaGarac(new AfDecimal(0, 15, 3));
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoPcApplzOpz().setWpmoPcApplzOpz(new AfDecimal(0, 6, 3));
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoIdAdes().setWpmoIdAdes(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoIdPoli(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoTpFrmAssva("");
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoDsRiga(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoDsOperSql(Types.SPACE_CHAR);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoDsVer(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoDsTsIniCptz(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoDsTsEndCptz(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoDsUtente("");
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoDsStatoElab(Types.SPACE_CHAR);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoTpEstrCnt("");
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoCodRamo("");
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoGenDaSin(Types.SPACE_CHAR);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoCodTari("");
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoNumRatPagPre().setWpmoNumRatPagPre(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoPcServVal().setWpmoPcServVal(new AfDecimal(0, 6, 3));
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoEtaAaSoglBnficr().setWpmoEtaAaSoglBnficr(((short)0));
        }
    }

    public void initParamMovi() {
        ws.getParamMovi().setPmoIdParamMovi(0);
        ws.getParamMovi().setPmoIdOgg(0);
        ws.getParamMovi().setPmoTpOgg("");
        ws.getParamMovi().setPmoIdMoviCrz(0);
        ws.getParamMovi().getPmoIdMoviChiu().setPmoIdMoviChiu(0);
        ws.getParamMovi().setPmoDtIniEff(0);
        ws.getParamMovi().setPmoDtEndEff(0);
        ws.getParamMovi().setPmoCodCompAnia(0);
        ws.getParamMovi().getPmoTpMovi().setPmoTpMovi(0);
        ws.getParamMovi().getPmoFrqMovi().setPmoFrqMovi(0);
        ws.getParamMovi().getPmoDurAa().setPmoDurAa(0);
        ws.getParamMovi().getPmoDurMm().setPmoDurMm(0);
        ws.getParamMovi().getPmoDurGg().setPmoDurGg(0);
        ws.getParamMovi().getPmoDtRicorPrec().setPmoDtRicorPrec(0);
        ws.getParamMovi().getPmoDtRicorSucc().setPmoDtRicorSucc(0);
        ws.getParamMovi().getPmoPcIntrFraz().setPmoPcIntrFraz(new AfDecimal(0, 6, 3));
        ws.getParamMovi().getPmoImpBnsDaScoTot().setPmoImpBnsDaScoTot(new AfDecimal(0, 15, 3));
        ws.getParamMovi().getPmoImpBnsDaSco().setPmoImpBnsDaSco(new AfDecimal(0, 15, 3));
        ws.getParamMovi().getPmoPcAnticBns().setPmoPcAnticBns(new AfDecimal(0, 6, 3));
        ws.getParamMovi().setPmoTpRinnColl("");
        ws.getParamMovi().setPmoTpRivalPre("");
        ws.getParamMovi().setPmoTpRivalPrstz("");
        ws.getParamMovi().setPmoFlEvidRival(Types.SPACE_CHAR);
        ws.getParamMovi().getPmoUltPcPerd().setPmoUltPcPerd(new AfDecimal(0, 6, 3));
        ws.getParamMovi().getPmoTotAaGiaPror().setPmoTotAaGiaPror(0);
        ws.getParamMovi().setPmoTpOpz("");
        ws.getParamMovi().getPmoAaRenCer().setPmoAaRenCer(0);
        ws.getParamMovi().getPmoPcRevrsb().setPmoPcRevrsb(new AfDecimal(0, 6, 3));
        ws.getParamMovi().getPmoImpRiscParzPrgt().setPmoImpRiscParzPrgt(new AfDecimal(0, 15, 3));
        ws.getParamMovi().getPmoImpLrdDiRat().setPmoImpLrdDiRat(new AfDecimal(0, 15, 3));
        ws.getParamMovi().setPmoIbOgg("");
        ws.getParamMovi().getPmoCosOner().setPmoCosOner(new AfDecimal(0, 15, 3));
        ws.getParamMovi().getPmoSpePc().setPmoSpePc(new AfDecimal(0, 6, 3));
        ws.getParamMovi().setPmoFlAttivGar(Types.SPACE_CHAR);
        ws.getParamMovi().setPmoCambioVerProd(Types.SPACE_CHAR);
        ws.getParamMovi().getPmoMmDiff().setPmoMmDiff(((short)0));
        ws.getParamMovi().getPmoImpRatManfee().setPmoImpRatManfee(new AfDecimal(0, 15, 3));
        ws.getParamMovi().getPmoDtUltErogManfee().setPmoDtUltErogManfee(0);
        ws.getParamMovi().setPmoTpOggRival("");
        ws.getParamMovi().getPmoSomAsstaGarac().setPmoSomAsstaGarac(new AfDecimal(0, 15, 3));
        ws.getParamMovi().getPmoPcApplzOpz().setPmoPcApplzOpz(new AfDecimal(0, 6, 3));
        ws.getParamMovi().getPmoIdAdes().setPmoIdAdes(0);
        ws.getParamMovi().setPmoIdPoli(0);
        ws.getParamMovi().setPmoTpFrmAssva("");
        ws.getParamMovi().setPmoDsRiga(0);
        ws.getParamMovi().setPmoDsOperSql(Types.SPACE_CHAR);
        ws.getParamMovi().setPmoDsVer(0);
        ws.getParamMovi().setPmoDsTsIniCptz(0);
        ws.getParamMovi().setPmoDsTsEndCptz(0);
        ws.getParamMovi().setPmoDsUtente("");
        ws.getParamMovi().setPmoDsStatoElab(Types.SPACE_CHAR);
        ws.getParamMovi().setPmoTpEstrCnt("");
        ws.getParamMovi().setPmoCodRamo("");
        ws.getParamMovi().setPmoGenDaSin(Types.SPACE_CHAR);
        ws.getParamMovi().setPmoCodTari("");
        ws.getParamMovi().getPmoNumRatPagPre().setPmoNumRatPagPre(0);
        ws.getParamMovi().getPmoPcServVal().setPmoPcServVal(new AfDecimal(0, 6, 3));
        ws.getParamMovi().getPmoEtaAaSoglBnficr().setPmoEtaAaSoglBnficr(((short)0));
    }
}
