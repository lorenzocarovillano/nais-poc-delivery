package it.accenture.jnais;

import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.ws.AreaIdsv0001;
import it.accenture.jnais.ws.Ieai9801Area;
import it.accenture.jnais.ws.Ieao9801Area;
import it.accenture.jnais.ws.Ieas9800Data;
import it.accenture.jnais.ws.occurs.WsTabellaParametri;

/**Original name: IEAS9800<br>
 * <pre>****************************************************************
 *                                                                *
 *                    IDENTIFICATION DIVISION                     *
 *                                                                *
 * ****************************************************************
 * AUTHOR.        ACCENTURE.
 * DATE-WRITTEN.  18/07/06.
 * ****************************************************************
 *                                                                *
 *     NOME :           IEAS9800                                  *
 *     TIPO :           ROUTINE COBOL-DB2                         *
 *     DESCRIZIONE :    ROUTINE SOSTITUZIONE PARAMETRI IN         *
 *                      DESCREIZIONE ESTESA TRAMITE PLACEHOLDER $ *
 *                      AD ESCLUSIVO USO DELLA ROUTINE IEAS9900   *
 *                                                                *
 *     AREE DI PASSAGGIO DATI                                     *
 *                                                                *
 *     DATI DI INPUT : IDSV0001, IEAI9801                         *
 *     DATI DI OUTPUT: IEAO9801                                   *
 *                                                                *
 * ****************************************************************
 *  LOG MODIFICHE :                                               *
 *                                                                *
 *                                                                *
 * ****************************************************************
 * ****************************************************************
 *                                                                *
 *                    ENVIRONMENT  DIVISION                       *
 *                                                                *
 * ****************************************************************
 * SOURCE-COMPUTER. IBM-370 WITH DEBUGGING MODE.
 * OBJECT-COMPUTER. IBM-370.</pre>*/
public class Ieas9800 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Ieas9800Data ws = new Ieas9800Data();
    //Original name: AREA-IDSV0001
    private AreaIdsv0001 areaIdsv0001;
    //Original name: IEAI9801-AREA
    private Ieai9801Area ieai9801Area;
    //Original name: IEAO9801-AREA
    private Ieao9801Area ieao9801Area;

    //==== METHODS ====
    /**Original name: MAIN_SUBROUTINE<br>*/
    public long execute(AreaIdsv0001 areaIdsv0001, Ieai9801Area ieai9801Area, Ieao9801Area ieao9801Area) {
        this.areaIdsv0001 = areaIdsv0001;
        this.ieai9801Area = ieai9801Area;
        this.ieao9801Area = ieao9801Area;
        principale();
        principaleExit();
        return 0;
    }

    public static Ieas9800 getInstance() {
        return ((Ieas9800)Programs.getInstance(Ieas9800.class));
    }

    /**Original name: 1000-PRINCIPALE<br>
	 * <pre>****************************************************************
	 *                                                                *
	 *                    1000-PRINCIPALE                             *
	 *                                                                *
	 * ****************************************************************
	 * -- INIZIALIZZO IL FLAG PER L'ESITO DELL'ELABORAZIONE</pre>*/
    private void principale() {
        // COB_CODE: SET SI-CORRETTO                    TO TRUE.
        ws.getEsito().setSiCorretto();
        //-- INIZIALIZZO AREA DI INPUT/OUTPUT
        //      MOVE SPACES                       TO IEAI9801-AREA
        //                                           IEAO9801-AREA
        // COB_CODE: MOVE SPACES                       TO WS-DESC-ERRORE
        //                                                WS-LISTA-PARAMETRI.
        ws.setDescErrore("");
        ws.initListaParametriSpaces();
        //-- ESTRAZIONE DEI PARAMETRI DI SOSTITUZIONE
        // COB_CODE: IF SI-CORRETTO
        //                 THRU 1100-ESTRAI-PARAMETRI-EXIT
        //           END-IF.
        if (ws.getEsito().isSiCorretto()) {
            // COB_CODE: PERFORM 1100-ESTRAI-PARAMETRI
            //              THRU 1100-ESTRAI-PARAMETRI-EXIT
            estraiParametri();
        }
        //-- INIZIA RICERCA NELLA STRINGA,
        //-- CREANDO UNA STRINGA DI APPOGGIO FORMATTATA
        // COB_CODE: IF SI-CORRETTO
        //                 THRU 1200-RICERCA-DOLLARO-EXIT
        //           END-IF.
        if (ws.getEsito().isSiCorretto()) {
            // COB_CODE: PERFORM 1200-RICERCA-DOLLARO
            //              THRU 1200-RICERCA-DOLLARO-EXIT
            ricercaDollaro();
        }
        //-- VALORIZZAZIONE DELL'OUTPUT
        // COB_CODE: IF SI-CORRETTO
        //                 THRU 1300-VALORIZZA-OUTPUT-EXIT
        //           END-IF.
        if (ws.getEsito().isSiCorretto()) {
            // COB_CODE: PERFORM 1300-VALORIZZA-OUTPUT
            //              THRU 1300-VALORIZZA-OUTPUT-EXIT
            valorizzaOutput();
        }
    }

    /**Original name: 1000-PRINCIPALE-EXIT<br>*/
    private void principaleExit() {
        // COB_CODE: EXIT.
        //exit
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    /**Original name: 1100-ESTRAI-PARAMETRI<br>
	 * <pre>-- ESTRAZIONE DEI PARAMETRI DI SOSTITUZIONE</pre>*/
    private void estraiParametri() {
        // COB_CODE: MOVE 1                        TO WS-IND-POS
        //                                            WS-IND-PARM.
        ws.getIndici().setIndPos(1);
        ws.getIndici().setIndParm(1);
        // COB_CODE: PERFORM VARYING WS-INDICE FROM 1 BY 1
        //             UNTIL WS-INDICE > WS-LIMITE-STR-PARAMETRO
        //                OR IEAI9801-PARAMETRI-ERR(WS-INDICE:)
        //                            = SPACES OR LOW-VALUE OR HIGH-VALUE
        //             END-IF
        //           END-PERFORM.
        ws.getIndici().setIndice(1);
        while (!(ws.getIndici().getIndice() > ws.getLimiteStrParametro() || Conditions.eq(ieai9801Area.getParametriErrFormatted().substring((ws.getIndici().getIndice()) - 1), "") || Conditions.eq(ieai9801Area.getParametriErrFormatted().substring((ws.getIndici().getIndice()) - 1), LiteralGenerator.create(Types.LOW_CHAR_VAL, Ieai9801Area.Len.PARAMETRI_ERR - ws.getIndici().getIndice() + 1)) || Conditions.eq(ieai9801Area.getParametriErrFormatted().substring((ws.getIndici().getIndice()) - 1), LiteralGenerator.create(Types.HIGH_CHAR_VAL, Ieai9801Area.Len.PARAMETRI_ERR - ws.getIndici().getIndice() + 1)))) {
            // COB_CODE: IF IEAI9801-PARAMETRI-ERR(WS-INDICE:1) = ';'
            //              MOVE 1                   TO WS-IND-POS
            //           ELSE
            //              ADD 1 TO WS-IND-POS
            //           END-IF
            if (Conditions.eq(ieai9801Area.getParametriErrFormatted().substring((ws.getIndici().getIndice()) - 1, ws.getIndici().getIndice()), ";")) {
                // COB_CODE: ADD  1                   TO WS-IND-PARM
                ws.getIndici().setIndParm(Trunc.toInt(1 + ws.getIndici().getIndParm(), 9));
                // COB_CODE: MOVE 1                   TO WS-IND-POS
                ws.getIndici().setIndPos(1);
            }
            else {
                // COB_CODE: MOVE IEAI9801-PARAMETRI-ERR(WS-INDICE:1)
                //             TO WS-PARAMETRO(WS-IND-PARM)(WS-IND-POS:1)
                ws.getTabellaParametri(ws.getIndici().getIndParm()).setWsParametroSubstring(ieai9801Area.getParametriErrFormatted().substring((ws.getIndici().getIndice()) - 1, ws.getIndici().getIndice()), ws.getIndici().getIndPos(), 1);
                // COB_CODE: ADD 1 TO WS-IND-POS
                ws.getIndici().setIndPos(Trunc.toInt(1 + ws.getIndici().getIndPos(), 9));
            }
            ws.getIndici().setIndice(Trunc.toInt(ws.getIndici().getIndice() + 1, 9));
        }
    }

    /**Original name: 1200-RICERCA-DOLLARO<br>
	 * <pre>-- EFFETTUA LA RICERCA DEI DOLLARI, FORMATTANDO UNA STRINGA DI
	 * -- APPOGGIO E GESTITA ANCHE LA SOSTITUZIONE INDICIZZATA,
	 * -- DOVE SUBITO DOPO IL DOLLARO VIENE SPECIFICATO
	 * -- IL PARAMETRO CHE VA INSERITO NELLA GIUSTA POSIZIONE.</pre>*/
    private void ricercaDollaro() {
        // COB_CODE: MOVE 1 TO WS-IND-STRINGA
        //                     WS-IND-PARAM.
        ws.getIndici().setIndStringa(1);
        ws.getIndici().setIndParam(1);
        // COB_CODE: PERFORM VARYING WS-INDICE FROM 1 BY 1
        //             UNTIL WS-INDICE > WS-LIMITE-STR-DESC-ERR
        //                OR IEAI9801-DESC-ERRORE-ESTESA(WS-INDICE:) =
        //                   SPACES OR LOW-VALUE OR HIGH-VALUE
        //                OR NO-CORRETTO
        //                END-IF
        //           END-PERFORM.
        ws.getIndici().setIndice(1);
        while (!(ws.getIndici().getIndice() > ws.getLimiteStrDescErr() || Conditions.eq(ieai9801Area.getDescErroreEstesaFormatted().substring((ws.getIndici().getIndice()) - 1), "") || Conditions.eq(ieai9801Area.getDescErroreEstesaFormatted().substring((ws.getIndici().getIndice()) - 1), LiteralGenerator.create(Types.LOW_CHAR_VAL, Ieai9801Area.Len.DESC_ERRORE_ESTESA - ws.getIndici().getIndice() + 1)) || Conditions.eq(ieai9801Area.getDescErroreEstesaFormatted().substring((ws.getIndici().getIndice()) - 1), LiteralGenerator.create(Types.HIGH_CHAR_VAL, Ieai9801Area.Len.DESC_ERRORE_ESTESA - ws.getIndici().getIndice() + 1)) || ws.getEsito().isNoCorretto())) {
            // COB_CODE: IF WS-IND-PARAM >= WS-LIMITE-ELE-PARAMETRI
            //              MOVE 99              TO IEAO9801-COD-ERRORE-990
            //           ELSE
            //              END-IF
            //           END-IF
            if (ws.getIndici().getIndParam() >= ws.getLimiteEleParametri()) {
                // COB_CODE: SET NO-CORRETTO      TO TRUE
                ws.getEsito().setNoCorretto();
                // COB_CODE: MOVE 99              TO IEAO9801-COD-ERRORE-990
                ieao9801Area.setCodErrore990(99);
            }
            else if (Conditions.eq(ieai9801Area.getDescErroreEstesaFormatted().substring((ws.getIndici().getIndice()) - 1, ws.getIndici().getIndice()), "$")) {
                // COB_CODE: IF IEAI9801-DESC-ERRORE-ESTESA
                //                                 (WS-INDICE:1) = '$'
                //              END-IF
                //           ELSE
                //              ADD 1                TO WS-IND-STRINGA
                //           END-IF
                // COB_CODE: COMPUTE WS-POS-SUCC = WS-INDICE + 1
                ws.getIndici().setPosSucc(Trunc.toInt(ws.getIndici().getIndice() + 1, 9));
                // COB_CODE: COMPUTE WS-POS-NEXT = WS-INDICE + 2
                ws.getIndici().setPosNext(Trunc.toInt(ws.getIndici().getIndice() + 2, 9));
                // COB_CODE: IF IEAI9801-DESC-ERRORE-ESTESA(WS-POS-SUCC:1)
                //              IS NUMERIC
                //              END-PERFORM
                //           ELSE
                //               ADD 1           TO WS-IND-PARAM
                //           END-IF
                if (Functions.isNumber(ieai9801Area.getDescErroreEstesaFormatted().substring((ws.getIndici().getPosSucc()) - 1, ws.getIndici().getPosSucc()))) {
                    // COB_CODE: IF IEAI9801-DESC-ERRORE-ESTESA
                    //                           (WS-POS-NEXT:1)
                    //              IS NUMERIC
                    //              ADD 2          TO WS-INDICE
                    //           ELSE
                    //              ADD 1          TO WS-INDICE
                    //           END-IF
                    if (Functions.isNumber(ieai9801Area.getDescErroreEstesaFormatted().substring((ws.getIndici().getPosNext()) - 1, ws.getIndici().getPosNext()))) {
                        // COB_CODE: MOVE IEAI9801-DESC-ERRORE-ESTESA
                        //                         (WS-POS-SUCC:2)
                        //                           TO WS-IND-PARM
                        ws.getIndici().setIndParmFormatted(ieai9801Area.getDescErroreEstesaFormatted().substring((ws.getIndici().getPosSucc()) - 1, ws.getIndici().getPosSucc() + 1));
                        // COB_CODE: ADD 2          TO WS-INDICE
                        ws.getIndici().setIndice(Trunc.toInt(2 + ws.getIndici().getIndice(), 9));
                    }
                    else {
                        // COB_CODE: MOVE IEAI9801-DESC-ERRORE-ESTESA
                        //                         (WS-POS-SUCC:1)
                        //                           TO WS-IND-PARM
                        ws.getIndici().setIndParmFormatted(ieai9801Area.getDescErroreEstesaFormatted().substring((ws.getIndici().getPosSucc()) - 1, ws.getIndici().getPosSucc()));
                        // COB_CODE: ADD 1          TO WS-INDICE
                        ws.getIndici().setIndice(Trunc.toInt(1 + ws.getIndici().getIndice(), 9));
                    }
                    // COB_CODE: PERFORM VARYING WS-IND FROM 1 BY 1
                    //              UNTIL WS-IND > WS-LIMITE-STR-PARAMETRO OR
                    //                   (WS-PARAMETRO(WS-IND-PARAM)(WS-IND:)
                    //                = SPACES OR LOW-VALUE OR HIGH-VALUE)
                    //               ADD 1        TO WS-IND-STRINGA
                    //           END-PERFORM
                    ws.getIndici().setInd(1);
                    while (!(ws.getIndici().getInd() > ws.getLimiteStrParametro() || Conditions.eq(ws.getTabellaParametri(ws.getIndici().getIndParam()).getWsParametroFormatted().substring((ws.getIndici().getInd()) - 1), "") || Conditions.eq(ws.getTabellaParametri(ws.getIndici().getIndParam()).getWsParametroFormatted().substring((ws.getIndici().getInd()) - 1), LiteralGenerator.create(Types.LOW_CHAR_VAL, WsTabellaParametri.Len.WS_PARAMETRO - ws.getIndici().getInd() + 1)) || Conditions.eq(ws.getTabellaParametri(ws.getIndici().getIndParam()).getWsParametroFormatted().substring((ws.getIndici().getInd()) - 1), LiteralGenerator.create(Types.HIGH_CHAR_VAL, WsTabellaParametri.Len.WS_PARAMETRO - ws.getIndici().getInd() + 1)))) {
                        // COB_CODE: MOVE WS-PARAMETRO(WS-IND-PARM)(WS-IND:1)
                        //                        TO WS-DESC-ERRORE
                        //                        (WS-IND-STRINGA:1)
                        ws.setDescErroreSubstring(ws.getTabellaParametri(ws.getIndici().getIndParm()).getWsParametroFormatted().substring((ws.getIndici().getInd()) - 1, ws.getIndici().getInd()), ws.getIndici().getIndStringa(), 1);
                        // COB_CODE: ADD 1        TO WS-IND-STRINGA
                        ws.getIndici().setIndStringa(Trunc.toInt(1 + ws.getIndici().getIndStringa(), 9));
                        ws.getIndici().setInd(Trunc.toInt(ws.getIndici().getInd() + 1, 9));
                    }
                }
                else {
                    // COB_CODE: PERFORM VARYING WS-IND FROM 1 BY 1
                    //              UNTIL WS-IND > WS-LIMITE-STR-PARAMETRO OR
                    //                   (WS-PARAMETRO(WS-IND-PARAM)
                    //                   (WS-IND:)
                    //                = SPACES OR LOW-VALUE OR HIGH-VALUE)
                    //               ADD 1       TO WS-IND-STRINGA
                    //           END-PERFORM
                    ws.getIndici().setInd(1);
                    while (!(ws.getIndici().getInd() > ws.getLimiteStrParametro() || Conditions.eq(ws.getTabellaParametri(ws.getIndici().getIndParam()).getWsParametroFormatted().substring((ws.getIndici().getInd()) - 1), "") || Conditions.eq(ws.getTabellaParametri(ws.getIndici().getIndParam()).getWsParametroFormatted().substring((ws.getIndici().getInd()) - 1), LiteralGenerator.create(Types.LOW_CHAR_VAL, WsTabellaParametri.Len.WS_PARAMETRO - ws.getIndici().getInd() + 1)) || Conditions.eq(ws.getTabellaParametri(ws.getIndici().getIndParam()).getWsParametroFormatted().substring((ws.getIndici().getInd()) - 1), LiteralGenerator.create(Types.HIGH_CHAR_VAL, WsTabellaParametri.Len.WS_PARAMETRO - ws.getIndici().getInd() + 1)))) {
                        // COB_CODE: MOVE WS-PARAMETRO(WS-IND-PARAM)
                        //           (WS-IND:1)
                        //                       TO WS-DESC-ERRORE
                        //                       (WS-IND-STRINGA:1)
                        ws.setDescErroreSubstring(ws.getTabellaParametri(ws.getIndici().getIndParam()).getWsParametroFormatted().substring((ws.getIndici().getInd()) - 1, ws.getIndici().getInd()), ws.getIndici().getIndStringa(), 1);
                        // COB_CODE: ADD 1       TO WS-IND-STRINGA
                        ws.getIndici().setIndStringa(Trunc.toInt(1 + ws.getIndici().getIndStringa(), 9));
                        ws.getIndici().setInd(Trunc.toInt(ws.getIndici().getInd() + 1, 9));
                    }
                    // COB_CODE: ADD 1           TO WS-IND-PARAM
                    ws.getIndici().setIndParam(Trunc.toInt(1 + ws.getIndici().getIndParam(), 9));
                }
            }
            else {
                // COB_CODE: MOVE IEAI9801-DESC-ERRORE-ESTESA(WS-INDICE:1)
                //             TO  WS-DESC-ERRORE(WS-IND-STRINGA:1)
                ws.setDescErroreSubstring(ieai9801Area.getDescErroreEstesaFormatted().substring((ws.getIndici().getIndice()) - 1, ws.getIndici().getIndice()), ws.getIndici().getIndStringa(), 1);
                // COB_CODE: ADD 1                TO WS-IND-STRINGA
                ws.getIndici().setIndStringa(Trunc.toInt(1 + ws.getIndici().getIndStringa(), 9));
            }
            ws.getIndici().setIndice(Trunc.toInt(ws.getIndici().getIndice() + 1, 9));
        }
        // COB_CODE: MOVE SPACE       TO WS-DESC-ERRORE(WS-IND-STRINGA:).
        ws.setDescErroreSubstring(LiteralGenerator.create(Types.SPACE_CHAR, Ieas9800Data.Len.DESC_ERRORE - ws.getIndici().getIndStringa() + 1), ws.getIndici().getIndStringa(), -1);
    }

    /**Original name: 1300-VALORIZZA-OUTPUT<br>
	 * <pre>-- VALORIZZAZIONE DEL'OUTPUT</pre>*/
    private void valorizzaOutput() {
        // COB_CODE: MOVE SPACES                   TO IEAO9801-DESC-ERRORE-ESTESA.
        ieao9801Area.setDescErroreEstesa("");
        // COB_CODE: MOVE WS-DESC-ERRORE           TO IEAO9801-DESC-ERRORE-ESTESA.
        ieao9801Area.setDescErroreEstesa(ws.getDescErrore());
        // COB_CODE: MOVE ZEROES                   TO IEAO9801-COD-ERRORE-990.
        ieao9801Area.setCodErrore990(0);
    }
}
