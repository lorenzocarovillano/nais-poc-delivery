package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.DFiscAdesDao;
import it.accenture.jnais.commons.data.to.IDFiscAdes;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.DFiscAdesIdbsdfa0;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idbsdfa0Data;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.redefines.DfaAaCnbzK1;
import it.accenture.jnais.ws.redefines.DfaAaCnbzK2;
import it.accenture.jnais.ws.redefines.DfaAaCnbzK3;
import it.accenture.jnais.ws.redefines.DfaAlqPrvr;
import it.accenture.jnais.ws.redefines.DfaAlqTfr;
import it.accenture.jnais.ws.redefines.DfaAnzCnbtvaCarass;
import it.accenture.jnais.ws.redefines.DfaAnzCnbtvaCarazi;
import it.accenture.jnais.ws.redefines.DfaAnzSrvz;
import it.accenture.jnais.ws.redefines.DfaCnbtEcc4x100K1;
import it.accenture.jnais.ws.redefines.DfaCommisDiTrasfe;
import it.accenture.jnais.ws.redefines.DfaCreditoIs;
import it.accenture.jnais.ws.redefines.DfaDt1aCnbz;
import it.accenture.jnais.ws.redefines.DfaDtAccnsRappFnd;
import it.accenture.jnais.ws.redefines.DfaDtCessazione;
import it.accenture.jnais.ws.redefines.DfaIdMoviChiu;
import it.accenture.jnais.ws.redefines.DfaImpb252Antic;
import it.accenture.jnais.ws.redefines.DfaImpb252K3;
import it.accenture.jnais.ws.redefines.DfaImpbIrpefK1Anti;
import it.accenture.jnais.ws.redefines.DfaImpbIrpefK2Anti;
import it.accenture.jnais.ws.redefines.DfaImpbIsK2;
import it.accenture.jnais.ws.redefines.DfaImpbIsK2Antic;
import it.accenture.jnais.ws.redefines.DfaImpbIsK3;
import it.accenture.jnais.ws.redefines.DfaImpbIsK3Antic;
import it.accenture.jnais.ws.redefines.DfaImpbTfrAntic;
import it.accenture.jnais.ws.redefines.DfaImpbVis;
import it.accenture.jnais.ws.redefines.DfaImpbVisAntic;
import it.accenture.jnais.ws.redefines.DfaImpCnbtAzK1;
import it.accenture.jnais.ws.redefines.DfaImpCnbtAzK2;
import it.accenture.jnais.ws.redefines.DfaImpCnbtAzK3;
import it.accenture.jnais.ws.redefines.DfaImpCnbtIscK1;
import it.accenture.jnais.ws.redefines.DfaImpCnbtIscK2;
import it.accenture.jnais.ws.redefines.DfaImpCnbtIscK3;
import it.accenture.jnais.ws.redefines.DfaImpCnbtNdedK1;
import it.accenture.jnais.ws.redefines.DfaImpCnbtNdedK2;
import it.accenture.jnais.ws.redefines.DfaImpCnbtNdedK3;
import it.accenture.jnais.ws.redefines.DfaImpCnbtTfrK1;
import it.accenture.jnais.ws.redefines.DfaImpCnbtTfrK2;
import it.accenture.jnais.ws.redefines.DfaImpCnbtTfrK3;
import it.accenture.jnais.ws.redefines.DfaImpCnbtVolK1;
import it.accenture.jnais.ws.redefines.DfaImpCnbtVolK2;
import it.accenture.jnais.ws.redefines.DfaImpCnbtVolK3;
import it.accenture.jnais.ws.redefines.DfaImpEseImpstTfr;
import it.accenture.jnais.ws.redefines.DfaImpNetTrasferito;
import it.accenture.jnais.ws.redefines.DfaImpst252Antic;
import it.accenture.jnais.ws.redefines.DfaImpst252K3;
import it.accenture.jnais.ws.redefines.DfaImpstIrpefK1Ant;
import it.accenture.jnais.ws.redefines.DfaImpstIrpefK2Ant;
import it.accenture.jnais.ws.redefines.DfaImpstSostK2;
import it.accenture.jnais.ws.redefines.DfaImpstSostK2Anti;
import it.accenture.jnais.ws.redefines.DfaImpstSostK3;
import it.accenture.jnais.ws.redefines.DfaImpstSostK3Anti;
import it.accenture.jnais.ws.redefines.DfaImpstTfrAntic;
import it.accenture.jnais.ws.redefines.DfaImpstVis;
import it.accenture.jnais.ws.redefines.DfaImpstVisAntic;
import it.accenture.jnais.ws.redefines.DfaMatuK1;
import it.accenture.jnais.ws.redefines.DfaMatuK2;
import it.accenture.jnais.ws.redefines.DfaMatuK3;
import it.accenture.jnais.ws.redefines.DfaMatuResK1;
import it.accenture.jnais.ws.redefines.DfaMmCnbzK1;
import it.accenture.jnais.ws.redefines.DfaMmCnbzK2;
import it.accenture.jnais.ws.redefines.DfaMmCnbzK3;
import it.accenture.jnais.ws.redefines.DfaOnerTrasfe;
import it.accenture.jnais.ws.redefines.DfaPcEseImpstTfr;
import it.accenture.jnais.ws.redefines.DfaPcTfr;
import it.accenture.jnais.ws.redefines.DfaRedtTassAbbatK2;
import it.accenture.jnais.ws.redefines.DfaRedtTassAbbatK3;
import it.accenture.jnais.ws.redefines.DfaRidzTfr;
import it.accenture.jnais.ws.redefines.DfaRidzTfrSuAntic;
import it.accenture.jnais.ws.redefines.DfaTotAntic;
import it.accenture.jnais.ws.redefines.DfaTotImpst;
import it.accenture.jnais.ws.redefines.DfaUltCommisTrasfe;

/**Original name: IDBSDFA0<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  22 APR 2010.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Idbsdfa0 extends Program implements IDFiscAdes {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private DFiscAdesDao dFiscAdesDao = new DFiscAdesDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Idbsdfa0Data ws = new Idbsdfa0Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: D-FISC-ADES
    private DFiscAdesIdbsdfa0 dFiscAdes;

    //==== METHODS ====
    /**Original name: PROGRAM_IDBSDFA0_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, DFiscAdesIdbsdfa0 dFiscAdes) {
        this.idsv0003 = idsv0003;
        this.dFiscAdes = dFiscAdes;
        // COB_CODE: PERFORM A000-INIZIO                    THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO          THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS          THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO          THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                        a300ElaboraIdEff();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                        a400ElaboraIdpEff();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO          THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS          THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO          THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                        b300ElaboraIdCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                        b400ElaboraIdpCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                        b500ElaboraIboCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                        b600ElaboraIbsCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                        b700ElaboraIdoCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //              SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-PRIMARY-KEY
                //                 PERFORM A200-ELABORA-PK          THRU A200-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO         THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS         THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO         THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.PRIMARY_KEY:// COB_CODE: PERFORM A200-ELABORA-PK          THRU A200-EX
                        a200ElaboraPk();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO         THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS         THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO         THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Idbsdfa0 getInstance() {
        return ((Idbsdfa0)Programs.getInstance(Idbsdfa0.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'IDBSDFA0'   TO IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("IDBSDFA0");
        // COB_CODE: MOVE 'D_FISC_ADES' TO IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("D_FISC_ADES");
        // COB_CODE: MOVE '00'                     TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                   TO   IDSV0003-SQLCODE
        //                                              IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                              IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-AGG-STORICO-SOLO-INS  OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isAggStoricoSoloIns() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-PK<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a200ElaboraPk() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-PK          THRU A210-EX
        //              WHEN IDSV0003-INSERT
        //                 PERFORM A220-INSERT-PK          THRU A220-EX
        //              WHEN IDSV0003-UPDATE
        //                 PERFORM A230-UPDATE-PK          THRU A230-EX
        //              WHEN IDSV0003-DELETE
        //                 PERFORM A240-DELETE-PK          THRU A240-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-PK          THRU A210-EX
            a210SelectPk();
        }
        else if (idsv0003.getOperazione().isInsert()) {
            // COB_CODE: PERFORM A220-INSERT-PK          THRU A220-EX
            a220InsertPk();
        }
        else if (idsv0003.getOperazione().isUpdate()) {
            // COB_CODE: PERFORM A230-UPDATE-PK          THRU A230-EX
            a230UpdatePk();
        }
        else if (idsv0003.getOperazione().isDelete()) {
            // COB_CODE: PERFORM A240-DELETE-PK          THRU A240-EX
            a240DeletePk();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A300-ELABORA-ID-EFF<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void a300ElaboraIdEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A310-SELECT-ID-EFF          THRU A310-EX
            a310SelectIdEff();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A400-ELABORA-IDP-EFF<br>*/
    private void a400ElaboraIdpEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
            a410SelectIdpEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
            a460OpenCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
            a470CloseCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
            a480FetchFirstIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
            a490FetchNextIdpEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A500-ELABORA-IBO<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a500ElaboraIbo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A510-SELECT-IBO             THRU A510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A510-SELECT-IBO             THRU A510-EX
            a510SelectIbo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
            a560OpenCursorIbo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
            a570CloseCursorIbo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
            a580FetchFirstIbo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
            a590FetchNextIbo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A600-ELABORA-IBS<br>*/
    private void a600ElaboraIbs() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A610-SELECT-IBS             THRU A610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A610-SELECT-IBS             THRU A610-EX
            a610SelectIbs();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
            a660OpenCursorIbs();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
            a670CloseCursorIbs();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
            a680FetchFirstIbs();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
            a690FetchNextIbs();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A700-ELABORA-IDO<br>*/
    private void a700ElaboraIdo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A710-SELECT-IDO                 THRU A710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A710-SELECT-IDO                 THRU A710-EX
            a710SelectIdo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
            a760OpenCursorIdo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
            a770CloseCursorIdo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
            a780FetchFirstIdo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
            a790FetchNextIdo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B300-ELABORA-ID-CPZ<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void b300ElaboraIdCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
            b310SelectIdCpz();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B400-ELABORA-IDP-CPZ<br>*/
    private void b400ElaboraIdpCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
            b410SelectIdpCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
            b460OpenCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
            b470CloseCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
            b480FetchFirstIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
            b490FetchNextIdpCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B500-ELABORA-IBO-CPZ<br>*/
    private void b500ElaboraIboCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
            b510SelectIboCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
            b560OpenCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
            b570CloseCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
            b580FetchFirstIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B600-ELABORA-IBS-CPZ<br>*/
    private void b600ElaboraIbsCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
            b610SelectIbsCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
            b660OpenCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
            b670CloseCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
            b680FetchFirstIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B700-ELABORA-IDO-CPZ<br>*/
    private void b700ElaboraIdoCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
            b710SelectIdoCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
            b760OpenCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
            b770CloseCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
            b780FetchFirstIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A210-SELECT-PK<br>
	 * <pre>----
	 * ----  gestione PK
	 * ----</pre>*/
    private void a210SelectPk() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_D_FISC_ADES
        //                ,ID_ADES
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,TP_ISC_FND
        //                ,DT_ACCNS_RAPP_FND
        //                ,IMP_CNBT_AZ_K1
        //                ,IMP_CNBT_ISC_K1
        //                ,IMP_CNBT_TFR_K1
        //                ,IMP_CNBT_VOL_K1
        //                ,IMP_CNBT_AZ_K2
        //                ,IMP_CNBT_ISC_K2
        //                ,IMP_CNBT_TFR_K2
        //                ,IMP_CNBT_VOL_K2
        //                ,MATU_K1
        //                ,MATU_RES_K1
        //                ,MATU_K2
        //                ,IMPB_VIS
        //                ,IMPST_VIS
        //                ,IMPB_IS_K2
        //                ,IMPST_SOST_K2
        //                ,RIDZ_TFR
        //                ,PC_TFR
        //                ,ALQ_TFR
        //                ,TOT_ANTIC
        //                ,IMPB_TFR_ANTIC
        //                ,IMPST_TFR_ANTIC
        //                ,RIDZ_TFR_SU_ANTIC
        //                ,IMPB_VIS_ANTIC
        //                ,IMPST_VIS_ANTIC
        //                ,IMPB_IS_K2_ANTIC
        //                ,IMPST_SOST_K2_ANTI
        //                ,ULT_COMMIS_TRASFE
        //                ,COD_DVS
        //                ,ALQ_PRVR
        //                ,PC_ESE_IMPST_TFR
        //                ,IMP_ESE_IMPST_TFR
        //                ,ANZ_CNBTVA_CARASS
        //                ,ANZ_CNBTVA_CARAZI
        //                ,ANZ_SRVZ
        //                ,IMP_CNBT_NDED_K1
        //                ,IMP_CNBT_NDED_K2
        //                ,IMP_CNBT_NDED_K3
        //                ,IMP_CNBT_AZ_K3
        //                ,IMP_CNBT_ISC_K3
        //                ,IMP_CNBT_TFR_K3
        //                ,IMP_CNBT_VOL_K3
        //                ,MATU_K3
        //                ,IMPB_252_ANTIC
        //                ,IMPST_252_ANTIC
        //                ,DT_1A_CNBZ
        //                ,COMMIS_DI_TRASFE
        //                ,AA_CNBZ_K1
        //                ,AA_CNBZ_K2
        //                ,AA_CNBZ_K3
        //                ,MM_CNBZ_K1
        //                ,MM_CNBZ_K2
        //                ,MM_CNBZ_K3
        //                ,FL_APPLZ_NEWFIS
        //                ,REDT_TASS_ABBAT_K3
        //                ,CNBT_ECC_4X100_K1
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,CREDITO_IS
        //                ,REDT_TASS_ABBAT_K2
        //                ,IMPB_IS_K3
        //                ,IMPST_SOST_K3
        //                ,IMPB_252_K3
        //                ,IMPST_252_K3
        //                ,IMPB_IS_K3_ANTIC
        //                ,IMPST_SOST_K3_ANTI
        //                ,IMPB_IRPEF_K1_ANTI
        //                ,IMPST_IRPEF_K1_ANT
        //                ,IMPB_IRPEF_K2_ANTI
        //                ,IMPST_IRPEF_K2_ANT
        //                ,DT_CESSAZIONE
        //                ,TOT_IMPST
        //                ,ONER_TRASFE
        //                ,IMP_NET_TRASFERITO
        //             INTO
        //                :DFA-ID-D-FISC-ADES
        //               ,:DFA-ID-ADES
        //               ,:DFA-ID-MOVI-CRZ
        //               ,:DFA-ID-MOVI-CHIU
        //                :IND-DFA-ID-MOVI-CHIU
        //               ,:DFA-DT-INI-EFF-DB
        //               ,:DFA-DT-END-EFF-DB
        //               ,:DFA-COD-COMP-ANIA
        //               ,:DFA-TP-ISC-FND
        //                :IND-DFA-TP-ISC-FND
        //               ,:DFA-DT-ACCNS-RAPP-FND-DB
        //                :IND-DFA-DT-ACCNS-RAPP-FND
        //               ,:DFA-IMP-CNBT-AZ-K1
        //                :IND-DFA-IMP-CNBT-AZ-K1
        //               ,:DFA-IMP-CNBT-ISC-K1
        //                :IND-DFA-IMP-CNBT-ISC-K1
        //               ,:DFA-IMP-CNBT-TFR-K1
        //                :IND-DFA-IMP-CNBT-TFR-K1
        //               ,:DFA-IMP-CNBT-VOL-K1
        //                :IND-DFA-IMP-CNBT-VOL-K1
        //               ,:DFA-IMP-CNBT-AZ-K2
        //                :IND-DFA-IMP-CNBT-AZ-K2
        //               ,:DFA-IMP-CNBT-ISC-K2
        //                :IND-DFA-IMP-CNBT-ISC-K2
        //               ,:DFA-IMP-CNBT-TFR-K2
        //                :IND-DFA-IMP-CNBT-TFR-K2
        //               ,:DFA-IMP-CNBT-VOL-K2
        //                :IND-DFA-IMP-CNBT-VOL-K2
        //               ,:DFA-MATU-K1
        //                :IND-DFA-MATU-K1
        //               ,:DFA-MATU-RES-K1
        //                :IND-DFA-MATU-RES-K1
        //               ,:DFA-MATU-K2
        //                :IND-DFA-MATU-K2
        //               ,:DFA-IMPB-VIS
        //                :IND-DFA-IMPB-VIS
        //               ,:DFA-IMPST-VIS
        //                :IND-DFA-IMPST-VIS
        //               ,:DFA-IMPB-IS-K2
        //                :IND-DFA-IMPB-IS-K2
        //               ,:DFA-IMPST-SOST-K2
        //                :IND-DFA-IMPST-SOST-K2
        //               ,:DFA-RIDZ-TFR
        //                :IND-DFA-RIDZ-TFR
        //               ,:DFA-PC-TFR
        //                :IND-DFA-PC-TFR
        //               ,:DFA-ALQ-TFR
        //                :IND-DFA-ALQ-TFR
        //               ,:DFA-TOT-ANTIC
        //                :IND-DFA-TOT-ANTIC
        //               ,:DFA-IMPB-TFR-ANTIC
        //                :IND-DFA-IMPB-TFR-ANTIC
        //               ,:DFA-IMPST-TFR-ANTIC
        //                :IND-DFA-IMPST-TFR-ANTIC
        //               ,:DFA-RIDZ-TFR-SU-ANTIC
        //                :IND-DFA-RIDZ-TFR-SU-ANTIC
        //               ,:DFA-IMPB-VIS-ANTIC
        //                :IND-DFA-IMPB-VIS-ANTIC
        //               ,:DFA-IMPST-VIS-ANTIC
        //                :IND-DFA-IMPST-VIS-ANTIC
        //               ,:DFA-IMPB-IS-K2-ANTIC
        //                :IND-DFA-IMPB-IS-K2-ANTIC
        //               ,:DFA-IMPST-SOST-K2-ANTI
        //                :IND-DFA-IMPST-SOST-K2-ANTI
        //               ,:DFA-ULT-COMMIS-TRASFE
        //                :IND-DFA-ULT-COMMIS-TRASFE
        //               ,:DFA-COD-DVS
        //                :IND-DFA-COD-DVS
        //               ,:DFA-ALQ-PRVR
        //                :IND-DFA-ALQ-PRVR
        //               ,:DFA-PC-ESE-IMPST-TFR
        //                :IND-DFA-PC-ESE-IMPST-TFR
        //               ,:DFA-IMP-ESE-IMPST-TFR
        //                :IND-DFA-IMP-ESE-IMPST-TFR
        //               ,:DFA-ANZ-CNBTVA-CARASS
        //                :IND-DFA-ANZ-CNBTVA-CARASS
        //               ,:DFA-ANZ-CNBTVA-CARAZI
        //                :IND-DFA-ANZ-CNBTVA-CARAZI
        //               ,:DFA-ANZ-SRVZ
        //                :IND-DFA-ANZ-SRVZ
        //               ,:DFA-IMP-CNBT-NDED-K1
        //                :IND-DFA-IMP-CNBT-NDED-K1
        //               ,:DFA-IMP-CNBT-NDED-K2
        //                :IND-DFA-IMP-CNBT-NDED-K2
        //               ,:DFA-IMP-CNBT-NDED-K3
        //                :IND-DFA-IMP-CNBT-NDED-K3
        //               ,:DFA-IMP-CNBT-AZ-K3
        //                :IND-DFA-IMP-CNBT-AZ-K3
        //               ,:DFA-IMP-CNBT-ISC-K3
        //                :IND-DFA-IMP-CNBT-ISC-K3
        //               ,:DFA-IMP-CNBT-TFR-K3
        //                :IND-DFA-IMP-CNBT-TFR-K3
        //               ,:DFA-IMP-CNBT-VOL-K3
        //                :IND-DFA-IMP-CNBT-VOL-K3
        //               ,:DFA-MATU-K3
        //                :IND-DFA-MATU-K3
        //               ,:DFA-IMPB-252-ANTIC
        //                :IND-DFA-IMPB-252-ANTIC
        //               ,:DFA-IMPST-252-ANTIC
        //                :IND-DFA-IMPST-252-ANTIC
        //               ,:DFA-DT-1A-CNBZ-DB
        //                :IND-DFA-DT-1A-CNBZ
        //               ,:DFA-COMMIS-DI-TRASFE
        //                :IND-DFA-COMMIS-DI-TRASFE
        //               ,:DFA-AA-CNBZ-K1
        //                :IND-DFA-AA-CNBZ-K1
        //               ,:DFA-AA-CNBZ-K2
        //                :IND-DFA-AA-CNBZ-K2
        //               ,:DFA-AA-CNBZ-K3
        //                :IND-DFA-AA-CNBZ-K3
        //               ,:DFA-MM-CNBZ-K1
        //                :IND-DFA-MM-CNBZ-K1
        //               ,:DFA-MM-CNBZ-K2
        //                :IND-DFA-MM-CNBZ-K2
        //               ,:DFA-MM-CNBZ-K3
        //                :IND-DFA-MM-CNBZ-K3
        //               ,:DFA-FL-APPLZ-NEWFIS
        //                :IND-DFA-FL-APPLZ-NEWFIS
        //               ,:DFA-REDT-TASS-ABBAT-K3
        //                :IND-DFA-REDT-TASS-ABBAT-K3
        //               ,:DFA-CNBT-ECC-4X100-K1
        //                :IND-DFA-CNBT-ECC-4X100-K1
        //               ,:DFA-DS-RIGA
        //               ,:DFA-DS-OPER-SQL
        //               ,:DFA-DS-VER
        //               ,:DFA-DS-TS-INI-CPTZ
        //               ,:DFA-DS-TS-END-CPTZ
        //               ,:DFA-DS-UTENTE
        //               ,:DFA-DS-STATO-ELAB
        //               ,:DFA-CREDITO-IS
        //                :IND-DFA-CREDITO-IS
        //               ,:DFA-REDT-TASS-ABBAT-K2
        //                :IND-DFA-REDT-TASS-ABBAT-K2
        //               ,:DFA-IMPB-IS-K3
        //                :IND-DFA-IMPB-IS-K3
        //               ,:DFA-IMPST-SOST-K3
        //                :IND-DFA-IMPST-SOST-K3
        //               ,:DFA-IMPB-252-K3
        //                :IND-DFA-IMPB-252-K3
        //               ,:DFA-IMPST-252-K3
        //                :IND-DFA-IMPST-252-K3
        //               ,:DFA-IMPB-IS-K3-ANTIC
        //                :IND-DFA-IMPB-IS-K3-ANTIC
        //               ,:DFA-IMPST-SOST-K3-ANTI
        //                :IND-DFA-IMPST-SOST-K3-ANTI
        //               ,:DFA-IMPB-IRPEF-K1-ANTI
        //                :IND-DFA-IMPB-IRPEF-K1-ANTI
        //               ,:DFA-IMPST-IRPEF-K1-ANT
        //                :IND-DFA-IMPST-IRPEF-K1-ANT
        //               ,:DFA-IMPB-IRPEF-K2-ANTI
        //                :IND-DFA-IMPB-IRPEF-K2-ANTI
        //               ,:DFA-IMPST-IRPEF-K2-ANT
        //                :IND-DFA-IMPST-IRPEF-K2-ANT
        //               ,:DFA-DT-CESSAZIONE-DB
        //                :IND-DFA-DT-CESSAZIONE
        //               ,:DFA-TOT-IMPST
        //                :IND-DFA-TOT-IMPST
        //               ,:DFA-ONER-TRASFE
        //                :IND-DFA-ONER-TRASFE
        //               ,:DFA-IMP-NET-TRASFERITO
        //                :IND-DFA-IMP-NET-TRASFERITO
        //             FROM D_FISC_ADES
        //             WHERE     DS_RIGA = :DFA-DS-RIGA
        //           END-EXEC.
        dFiscAdesDao.selectByDfaDsRiga(dFiscAdes.getDfaDsRiga(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A220-INSERT-PK<br>*/
    private void a220InsertPk() {
        // COB_CODE: PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.
        z400SeqRiga();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX
            z150ValorizzaDataServicesI();
            // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX
            z200SetIndicatoriNull();
            // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX
            z900ConvertiNToX();
            // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX
            z960LengthVchar();
            // COB_CODE: EXEC SQL
            //              INSERT
            //              INTO D_FISC_ADES
            //                  (
            //                     ID_D_FISC_ADES
            //                    ,ID_ADES
            //                    ,ID_MOVI_CRZ
            //                    ,ID_MOVI_CHIU
            //                    ,DT_INI_EFF
            //                    ,DT_END_EFF
            //                    ,COD_COMP_ANIA
            //                    ,TP_ISC_FND
            //                    ,DT_ACCNS_RAPP_FND
            //                    ,IMP_CNBT_AZ_K1
            //                    ,IMP_CNBT_ISC_K1
            //                    ,IMP_CNBT_TFR_K1
            //                    ,IMP_CNBT_VOL_K1
            //                    ,IMP_CNBT_AZ_K2
            //                    ,IMP_CNBT_ISC_K2
            //                    ,IMP_CNBT_TFR_K2
            //                    ,IMP_CNBT_VOL_K2
            //                    ,MATU_K1
            //                    ,MATU_RES_K1
            //                    ,MATU_K2
            //                    ,IMPB_VIS
            //                    ,IMPST_VIS
            //                    ,IMPB_IS_K2
            //                    ,IMPST_SOST_K2
            //                    ,RIDZ_TFR
            //                    ,PC_TFR
            //                    ,ALQ_TFR
            //                    ,TOT_ANTIC
            //                    ,IMPB_TFR_ANTIC
            //                    ,IMPST_TFR_ANTIC
            //                    ,RIDZ_TFR_SU_ANTIC
            //                    ,IMPB_VIS_ANTIC
            //                    ,IMPST_VIS_ANTIC
            //                    ,IMPB_IS_K2_ANTIC
            //                    ,IMPST_SOST_K2_ANTI
            //                    ,ULT_COMMIS_TRASFE
            //                    ,COD_DVS
            //                    ,ALQ_PRVR
            //                    ,PC_ESE_IMPST_TFR
            //                    ,IMP_ESE_IMPST_TFR
            //                    ,ANZ_CNBTVA_CARASS
            //                    ,ANZ_CNBTVA_CARAZI
            //                    ,ANZ_SRVZ
            //                    ,IMP_CNBT_NDED_K1
            //                    ,IMP_CNBT_NDED_K2
            //                    ,IMP_CNBT_NDED_K3
            //                    ,IMP_CNBT_AZ_K3
            //                    ,IMP_CNBT_ISC_K3
            //                    ,IMP_CNBT_TFR_K3
            //                    ,IMP_CNBT_VOL_K3
            //                    ,MATU_K3
            //                    ,IMPB_252_ANTIC
            //                    ,IMPST_252_ANTIC
            //                    ,DT_1A_CNBZ
            //                    ,COMMIS_DI_TRASFE
            //                    ,AA_CNBZ_K1
            //                    ,AA_CNBZ_K2
            //                    ,AA_CNBZ_K3
            //                    ,MM_CNBZ_K1
            //                    ,MM_CNBZ_K2
            //                    ,MM_CNBZ_K3
            //                    ,FL_APPLZ_NEWFIS
            //                    ,REDT_TASS_ABBAT_K3
            //                    ,CNBT_ECC_4X100_K1
            //                    ,DS_RIGA
            //                    ,DS_OPER_SQL
            //                    ,DS_VER
            //                    ,DS_TS_INI_CPTZ
            //                    ,DS_TS_END_CPTZ
            //                    ,DS_UTENTE
            //                    ,DS_STATO_ELAB
            //                    ,CREDITO_IS
            //                    ,REDT_TASS_ABBAT_K2
            //                    ,IMPB_IS_K3
            //                    ,IMPST_SOST_K3
            //                    ,IMPB_252_K3
            //                    ,IMPST_252_K3
            //                    ,IMPB_IS_K3_ANTIC
            //                    ,IMPST_SOST_K3_ANTI
            //                    ,IMPB_IRPEF_K1_ANTI
            //                    ,IMPST_IRPEF_K1_ANT
            //                    ,IMPB_IRPEF_K2_ANTI
            //                    ,IMPST_IRPEF_K2_ANT
            //                    ,DT_CESSAZIONE
            //                    ,TOT_IMPST
            //                    ,ONER_TRASFE
            //                    ,IMP_NET_TRASFERITO
            //                  )
            //              VALUES
            //                  (
            //                    :DFA-ID-D-FISC-ADES
            //                    ,:DFA-ID-ADES
            //                    ,:DFA-ID-MOVI-CRZ
            //                    ,:DFA-ID-MOVI-CHIU
            //                     :IND-DFA-ID-MOVI-CHIU
            //                    ,:DFA-DT-INI-EFF-DB
            //                    ,:DFA-DT-END-EFF-DB
            //                    ,:DFA-COD-COMP-ANIA
            //                    ,:DFA-TP-ISC-FND
            //                     :IND-DFA-TP-ISC-FND
            //                    ,:DFA-DT-ACCNS-RAPP-FND-DB
            //                     :IND-DFA-DT-ACCNS-RAPP-FND
            //                    ,:DFA-IMP-CNBT-AZ-K1
            //                     :IND-DFA-IMP-CNBT-AZ-K1
            //                    ,:DFA-IMP-CNBT-ISC-K1
            //                     :IND-DFA-IMP-CNBT-ISC-K1
            //                    ,:DFA-IMP-CNBT-TFR-K1
            //                     :IND-DFA-IMP-CNBT-TFR-K1
            //                    ,:DFA-IMP-CNBT-VOL-K1
            //                     :IND-DFA-IMP-CNBT-VOL-K1
            //                    ,:DFA-IMP-CNBT-AZ-K2
            //                     :IND-DFA-IMP-CNBT-AZ-K2
            //                    ,:DFA-IMP-CNBT-ISC-K2
            //                     :IND-DFA-IMP-CNBT-ISC-K2
            //                    ,:DFA-IMP-CNBT-TFR-K2
            //                     :IND-DFA-IMP-CNBT-TFR-K2
            //                    ,:DFA-IMP-CNBT-VOL-K2
            //                     :IND-DFA-IMP-CNBT-VOL-K2
            //                    ,:DFA-MATU-K1
            //                     :IND-DFA-MATU-K1
            //                    ,:DFA-MATU-RES-K1
            //                     :IND-DFA-MATU-RES-K1
            //                    ,:DFA-MATU-K2
            //                     :IND-DFA-MATU-K2
            //                    ,:DFA-IMPB-VIS
            //                     :IND-DFA-IMPB-VIS
            //                    ,:DFA-IMPST-VIS
            //                     :IND-DFA-IMPST-VIS
            //                    ,:DFA-IMPB-IS-K2
            //                     :IND-DFA-IMPB-IS-K2
            //                    ,:DFA-IMPST-SOST-K2
            //                     :IND-DFA-IMPST-SOST-K2
            //                    ,:DFA-RIDZ-TFR
            //                     :IND-DFA-RIDZ-TFR
            //                    ,:DFA-PC-TFR
            //                     :IND-DFA-PC-TFR
            //                    ,:DFA-ALQ-TFR
            //                     :IND-DFA-ALQ-TFR
            //                    ,:DFA-TOT-ANTIC
            //                     :IND-DFA-TOT-ANTIC
            //                    ,:DFA-IMPB-TFR-ANTIC
            //                     :IND-DFA-IMPB-TFR-ANTIC
            //                    ,:DFA-IMPST-TFR-ANTIC
            //                     :IND-DFA-IMPST-TFR-ANTIC
            //                    ,:DFA-RIDZ-TFR-SU-ANTIC
            //                     :IND-DFA-RIDZ-TFR-SU-ANTIC
            //                    ,:DFA-IMPB-VIS-ANTIC
            //                     :IND-DFA-IMPB-VIS-ANTIC
            //                    ,:DFA-IMPST-VIS-ANTIC
            //                     :IND-DFA-IMPST-VIS-ANTIC
            //                    ,:DFA-IMPB-IS-K2-ANTIC
            //                     :IND-DFA-IMPB-IS-K2-ANTIC
            //                    ,:DFA-IMPST-SOST-K2-ANTI
            //                     :IND-DFA-IMPST-SOST-K2-ANTI
            //                    ,:DFA-ULT-COMMIS-TRASFE
            //                     :IND-DFA-ULT-COMMIS-TRASFE
            //                    ,:DFA-COD-DVS
            //                     :IND-DFA-COD-DVS
            //                    ,:DFA-ALQ-PRVR
            //                     :IND-DFA-ALQ-PRVR
            //                    ,:DFA-PC-ESE-IMPST-TFR
            //                     :IND-DFA-PC-ESE-IMPST-TFR
            //                    ,:DFA-IMP-ESE-IMPST-TFR
            //                     :IND-DFA-IMP-ESE-IMPST-TFR
            //                    ,:DFA-ANZ-CNBTVA-CARASS
            //                     :IND-DFA-ANZ-CNBTVA-CARASS
            //                    ,:DFA-ANZ-CNBTVA-CARAZI
            //                     :IND-DFA-ANZ-CNBTVA-CARAZI
            //                    ,:DFA-ANZ-SRVZ
            //                     :IND-DFA-ANZ-SRVZ
            //                    ,:DFA-IMP-CNBT-NDED-K1
            //                     :IND-DFA-IMP-CNBT-NDED-K1
            //                    ,:DFA-IMP-CNBT-NDED-K2
            //                     :IND-DFA-IMP-CNBT-NDED-K2
            //                    ,:DFA-IMP-CNBT-NDED-K3
            //                     :IND-DFA-IMP-CNBT-NDED-K3
            //                    ,:DFA-IMP-CNBT-AZ-K3
            //                     :IND-DFA-IMP-CNBT-AZ-K3
            //                    ,:DFA-IMP-CNBT-ISC-K3
            //                     :IND-DFA-IMP-CNBT-ISC-K3
            //                    ,:DFA-IMP-CNBT-TFR-K3
            //                     :IND-DFA-IMP-CNBT-TFR-K3
            //                    ,:DFA-IMP-CNBT-VOL-K3
            //                     :IND-DFA-IMP-CNBT-VOL-K3
            //                    ,:DFA-MATU-K3
            //                     :IND-DFA-MATU-K3
            //                    ,:DFA-IMPB-252-ANTIC
            //                     :IND-DFA-IMPB-252-ANTIC
            //                    ,:DFA-IMPST-252-ANTIC
            //                     :IND-DFA-IMPST-252-ANTIC
            //                    ,:DFA-DT-1A-CNBZ-DB
            //                     :IND-DFA-DT-1A-CNBZ
            //                    ,:DFA-COMMIS-DI-TRASFE
            //                     :IND-DFA-COMMIS-DI-TRASFE
            //                    ,:DFA-AA-CNBZ-K1
            //                     :IND-DFA-AA-CNBZ-K1
            //                    ,:DFA-AA-CNBZ-K2
            //                     :IND-DFA-AA-CNBZ-K2
            //                    ,:DFA-AA-CNBZ-K3
            //                     :IND-DFA-AA-CNBZ-K3
            //                    ,:DFA-MM-CNBZ-K1
            //                     :IND-DFA-MM-CNBZ-K1
            //                    ,:DFA-MM-CNBZ-K2
            //                     :IND-DFA-MM-CNBZ-K2
            //                    ,:DFA-MM-CNBZ-K3
            //                     :IND-DFA-MM-CNBZ-K3
            //                    ,:DFA-FL-APPLZ-NEWFIS
            //                     :IND-DFA-FL-APPLZ-NEWFIS
            //                    ,:DFA-REDT-TASS-ABBAT-K3
            //                     :IND-DFA-REDT-TASS-ABBAT-K3
            //                    ,:DFA-CNBT-ECC-4X100-K1
            //                     :IND-DFA-CNBT-ECC-4X100-K1
            //                    ,:DFA-DS-RIGA
            //                    ,:DFA-DS-OPER-SQL
            //                    ,:DFA-DS-VER
            //                    ,:DFA-DS-TS-INI-CPTZ
            //                    ,:DFA-DS-TS-END-CPTZ
            //                    ,:DFA-DS-UTENTE
            //                    ,:DFA-DS-STATO-ELAB
            //                    ,:DFA-CREDITO-IS
            //                     :IND-DFA-CREDITO-IS
            //                    ,:DFA-REDT-TASS-ABBAT-K2
            //                     :IND-DFA-REDT-TASS-ABBAT-K2
            //                    ,:DFA-IMPB-IS-K3
            //                     :IND-DFA-IMPB-IS-K3
            //                    ,:DFA-IMPST-SOST-K3
            //                     :IND-DFA-IMPST-SOST-K3
            //                    ,:DFA-IMPB-252-K3
            //                     :IND-DFA-IMPB-252-K3
            //                    ,:DFA-IMPST-252-K3
            //                     :IND-DFA-IMPST-252-K3
            //                    ,:DFA-IMPB-IS-K3-ANTIC
            //                     :IND-DFA-IMPB-IS-K3-ANTIC
            //                    ,:DFA-IMPST-SOST-K3-ANTI
            //                     :IND-DFA-IMPST-SOST-K3-ANTI
            //                    ,:DFA-IMPB-IRPEF-K1-ANTI
            //                     :IND-DFA-IMPB-IRPEF-K1-ANTI
            //                    ,:DFA-IMPST-IRPEF-K1-ANT
            //                     :IND-DFA-IMPST-IRPEF-K1-ANT
            //                    ,:DFA-IMPB-IRPEF-K2-ANTI
            //                     :IND-DFA-IMPB-IRPEF-K2-ANTI
            //                    ,:DFA-IMPST-IRPEF-K2-ANT
            //                     :IND-DFA-IMPST-IRPEF-K2-ANT
            //                    ,:DFA-DT-CESSAZIONE-DB
            //                     :IND-DFA-DT-CESSAZIONE
            //                    ,:DFA-TOT-IMPST
            //                     :IND-DFA-TOT-IMPST
            //                    ,:DFA-ONER-TRASFE
            //                     :IND-DFA-ONER-TRASFE
            //                    ,:DFA-IMP-NET-TRASFERITO
            //                     :IND-DFA-IMP-NET-TRASFERITO
            //                  )
            //           END-EXEC
            dFiscAdesDao.insertRec(this);
            // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
            a100CheckReturnCode();
        }
    }

    /**Original name: A230-UPDATE-PK<br>*/
    private void a230UpdatePk() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE D_FISC_ADES SET
        //                   ID_D_FISC_ADES         =
        //                :DFA-ID-D-FISC-ADES
        //                  ,ID_ADES                =
        //                :DFA-ID-ADES
        //                  ,ID_MOVI_CRZ            =
        //                :DFA-ID-MOVI-CRZ
        //                  ,ID_MOVI_CHIU           =
        //                :DFA-ID-MOVI-CHIU
        //                                       :IND-DFA-ID-MOVI-CHIU
        //                  ,DT_INI_EFF             =
        //           :DFA-DT-INI-EFF-DB
        //                  ,DT_END_EFF             =
        //           :DFA-DT-END-EFF-DB
        //                  ,COD_COMP_ANIA          =
        //                :DFA-COD-COMP-ANIA
        //                  ,TP_ISC_FND             =
        //                :DFA-TP-ISC-FND
        //                                       :IND-DFA-TP-ISC-FND
        //                  ,DT_ACCNS_RAPP_FND      =
        //           :DFA-DT-ACCNS-RAPP-FND-DB
        //                                       :IND-DFA-DT-ACCNS-RAPP-FND
        //                  ,IMP_CNBT_AZ_K1         =
        //                :DFA-IMP-CNBT-AZ-K1
        //                                       :IND-DFA-IMP-CNBT-AZ-K1
        //                  ,IMP_CNBT_ISC_K1        =
        //                :DFA-IMP-CNBT-ISC-K1
        //                                       :IND-DFA-IMP-CNBT-ISC-K1
        //                  ,IMP_CNBT_TFR_K1        =
        //                :DFA-IMP-CNBT-TFR-K1
        //                                       :IND-DFA-IMP-CNBT-TFR-K1
        //                  ,IMP_CNBT_VOL_K1        =
        //                :DFA-IMP-CNBT-VOL-K1
        //                                       :IND-DFA-IMP-CNBT-VOL-K1
        //                  ,IMP_CNBT_AZ_K2         =
        //                :DFA-IMP-CNBT-AZ-K2
        //                                       :IND-DFA-IMP-CNBT-AZ-K2
        //                  ,IMP_CNBT_ISC_K2        =
        //                :DFA-IMP-CNBT-ISC-K2
        //                                       :IND-DFA-IMP-CNBT-ISC-K2
        //                  ,IMP_CNBT_TFR_K2        =
        //                :DFA-IMP-CNBT-TFR-K2
        //                                       :IND-DFA-IMP-CNBT-TFR-K2
        //                  ,IMP_CNBT_VOL_K2        =
        //                :DFA-IMP-CNBT-VOL-K2
        //                                       :IND-DFA-IMP-CNBT-VOL-K2
        //                  ,MATU_K1                =
        //                :DFA-MATU-K1
        //                                       :IND-DFA-MATU-K1
        //                  ,MATU_RES_K1            =
        //                :DFA-MATU-RES-K1
        //                                       :IND-DFA-MATU-RES-K1
        //                  ,MATU_K2                =
        //                :DFA-MATU-K2
        //                                       :IND-DFA-MATU-K2
        //                  ,IMPB_VIS               =
        //                :DFA-IMPB-VIS
        //                                       :IND-DFA-IMPB-VIS
        //                  ,IMPST_VIS              =
        //                :DFA-IMPST-VIS
        //                                       :IND-DFA-IMPST-VIS
        //                  ,IMPB_IS_K2             =
        //                :DFA-IMPB-IS-K2
        //                                       :IND-DFA-IMPB-IS-K2
        //                  ,IMPST_SOST_K2          =
        //                :DFA-IMPST-SOST-K2
        //                                       :IND-DFA-IMPST-SOST-K2
        //                  ,RIDZ_TFR               =
        //                :DFA-RIDZ-TFR
        //                                       :IND-DFA-RIDZ-TFR
        //                  ,PC_TFR                 =
        //                :DFA-PC-TFR
        //                                       :IND-DFA-PC-TFR
        //                  ,ALQ_TFR                =
        //                :DFA-ALQ-TFR
        //                                       :IND-DFA-ALQ-TFR
        //                  ,TOT_ANTIC              =
        //                :DFA-TOT-ANTIC
        //                                       :IND-DFA-TOT-ANTIC
        //                  ,IMPB_TFR_ANTIC         =
        //                :DFA-IMPB-TFR-ANTIC
        //                                       :IND-DFA-IMPB-TFR-ANTIC
        //                  ,IMPST_TFR_ANTIC        =
        //                :DFA-IMPST-TFR-ANTIC
        //                                       :IND-DFA-IMPST-TFR-ANTIC
        //                  ,RIDZ_TFR_SU_ANTIC      =
        //                :DFA-RIDZ-TFR-SU-ANTIC
        //                                       :IND-DFA-RIDZ-TFR-SU-ANTIC
        //                  ,IMPB_VIS_ANTIC         =
        //                :DFA-IMPB-VIS-ANTIC
        //                                       :IND-DFA-IMPB-VIS-ANTIC
        //                  ,IMPST_VIS_ANTIC        =
        //                :DFA-IMPST-VIS-ANTIC
        //                                       :IND-DFA-IMPST-VIS-ANTIC
        //                  ,IMPB_IS_K2_ANTIC       =
        //                :DFA-IMPB-IS-K2-ANTIC
        //                                       :IND-DFA-IMPB-IS-K2-ANTIC
        //                  ,IMPST_SOST_K2_ANTI     =
        //                :DFA-IMPST-SOST-K2-ANTI
        //                                       :IND-DFA-IMPST-SOST-K2-ANTI
        //                  ,ULT_COMMIS_TRASFE      =
        //                :DFA-ULT-COMMIS-TRASFE
        //                                       :IND-DFA-ULT-COMMIS-TRASFE
        //                  ,COD_DVS                =
        //                :DFA-COD-DVS
        //                                       :IND-DFA-COD-DVS
        //                  ,ALQ_PRVR               =
        //                :DFA-ALQ-PRVR
        //                                       :IND-DFA-ALQ-PRVR
        //                  ,PC_ESE_IMPST_TFR       =
        //                :DFA-PC-ESE-IMPST-TFR
        //                                       :IND-DFA-PC-ESE-IMPST-TFR
        //                  ,IMP_ESE_IMPST_TFR      =
        //                :DFA-IMP-ESE-IMPST-TFR
        //                                       :IND-DFA-IMP-ESE-IMPST-TFR
        //                  ,ANZ_CNBTVA_CARASS      =
        //                :DFA-ANZ-CNBTVA-CARASS
        //                                       :IND-DFA-ANZ-CNBTVA-CARASS
        //                  ,ANZ_CNBTVA_CARAZI      =
        //                :DFA-ANZ-CNBTVA-CARAZI
        //                                       :IND-DFA-ANZ-CNBTVA-CARAZI
        //                  ,ANZ_SRVZ               =
        //                :DFA-ANZ-SRVZ
        //                                       :IND-DFA-ANZ-SRVZ
        //                  ,IMP_CNBT_NDED_K1       =
        //                :DFA-IMP-CNBT-NDED-K1
        //                                       :IND-DFA-IMP-CNBT-NDED-K1
        //                  ,IMP_CNBT_NDED_K2       =
        //                :DFA-IMP-CNBT-NDED-K2
        //                                       :IND-DFA-IMP-CNBT-NDED-K2
        //                  ,IMP_CNBT_NDED_K3       =
        //                :DFA-IMP-CNBT-NDED-K3
        //                                       :IND-DFA-IMP-CNBT-NDED-K3
        //                  ,IMP_CNBT_AZ_K3         =
        //                :DFA-IMP-CNBT-AZ-K3
        //                                       :IND-DFA-IMP-CNBT-AZ-K3
        //                  ,IMP_CNBT_ISC_K3        =
        //                :DFA-IMP-CNBT-ISC-K3
        //                                       :IND-DFA-IMP-CNBT-ISC-K3
        //                  ,IMP_CNBT_TFR_K3        =
        //                :DFA-IMP-CNBT-TFR-K3
        //                                       :IND-DFA-IMP-CNBT-TFR-K3
        //                  ,IMP_CNBT_VOL_K3        =
        //                :DFA-IMP-CNBT-VOL-K3
        //                                       :IND-DFA-IMP-CNBT-VOL-K3
        //                  ,MATU_K3                =
        //                :DFA-MATU-K3
        //                                       :IND-DFA-MATU-K3
        //                  ,IMPB_252_ANTIC         =
        //                :DFA-IMPB-252-ANTIC
        //                                       :IND-DFA-IMPB-252-ANTIC
        //                  ,IMPST_252_ANTIC        =
        //                :DFA-IMPST-252-ANTIC
        //                                       :IND-DFA-IMPST-252-ANTIC
        //                  ,DT_1A_CNBZ             =
        //           :DFA-DT-1A-CNBZ-DB
        //                                       :IND-DFA-DT-1A-CNBZ
        //                  ,COMMIS_DI_TRASFE       =
        //                :DFA-COMMIS-DI-TRASFE
        //                                       :IND-DFA-COMMIS-DI-TRASFE
        //                  ,AA_CNBZ_K1             =
        //                :DFA-AA-CNBZ-K1
        //                                       :IND-DFA-AA-CNBZ-K1
        //                  ,AA_CNBZ_K2             =
        //                :DFA-AA-CNBZ-K2
        //                                       :IND-DFA-AA-CNBZ-K2
        //                  ,AA_CNBZ_K3             =
        //                :DFA-AA-CNBZ-K3
        //                                       :IND-DFA-AA-CNBZ-K3
        //                  ,MM_CNBZ_K1             =
        //                :DFA-MM-CNBZ-K1
        //                                       :IND-DFA-MM-CNBZ-K1
        //                  ,MM_CNBZ_K2             =
        //                :DFA-MM-CNBZ-K2
        //                                       :IND-DFA-MM-CNBZ-K2
        //                  ,MM_CNBZ_K3             =
        //                :DFA-MM-CNBZ-K3
        //                                       :IND-DFA-MM-CNBZ-K3
        //                  ,FL_APPLZ_NEWFIS        =
        //                :DFA-FL-APPLZ-NEWFIS
        //                                       :IND-DFA-FL-APPLZ-NEWFIS
        //                  ,REDT_TASS_ABBAT_K3     =
        //                :DFA-REDT-TASS-ABBAT-K3
        //                                       :IND-DFA-REDT-TASS-ABBAT-K3
        //                  ,CNBT_ECC_4X100_K1      =
        //                :DFA-CNBT-ECC-4X100-K1
        //                                       :IND-DFA-CNBT-ECC-4X100-K1
        //                  ,DS_RIGA                =
        //                :DFA-DS-RIGA
        //                  ,DS_OPER_SQL            =
        //                :DFA-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :DFA-DS-VER
        //                  ,DS_TS_INI_CPTZ         =
        //                :DFA-DS-TS-INI-CPTZ
        //                  ,DS_TS_END_CPTZ         =
        //                :DFA-DS-TS-END-CPTZ
        //                  ,DS_UTENTE              =
        //                :DFA-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :DFA-DS-STATO-ELAB
        //                  ,CREDITO_IS             =
        //                :DFA-CREDITO-IS
        //                                       :IND-DFA-CREDITO-IS
        //                  ,REDT_TASS_ABBAT_K2     =
        //                :DFA-REDT-TASS-ABBAT-K2
        //                                       :IND-DFA-REDT-TASS-ABBAT-K2
        //                  ,IMPB_IS_K3             =
        //                :DFA-IMPB-IS-K3
        //                                       :IND-DFA-IMPB-IS-K3
        //                  ,IMPST_SOST_K3          =
        //                :DFA-IMPST-SOST-K3
        //                                       :IND-DFA-IMPST-SOST-K3
        //                  ,IMPB_252_K3            =
        //                :DFA-IMPB-252-K3
        //                                       :IND-DFA-IMPB-252-K3
        //                  ,IMPST_252_K3           =
        //                :DFA-IMPST-252-K3
        //                                       :IND-DFA-IMPST-252-K3
        //                  ,IMPB_IS_K3_ANTIC       =
        //                :DFA-IMPB-IS-K3-ANTIC
        //                                       :IND-DFA-IMPB-IS-K3-ANTIC
        //                  ,IMPST_SOST_K3_ANTI     =
        //                :DFA-IMPST-SOST-K3-ANTI
        //                                       :IND-DFA-IMPST-SOST-K3-ANTI
        //                  ,IMPB_IRPEF_K1_ANTI     =
        //                :DFA-IMPB-IRPEF-K1-ANTI
        //                                       :IND-DFA-IMPB-IRPEF-K1-ANTI
        //                  ,IMPST_IRPEF_K1_ANT     =
        //                :DFA-IMPST-IRPEF-K1-ANT
        //                                       :IND-DFA-IMPST-IRPEF-K1-ANT
        //                  ,IMPB_IRPEF_K2_ANTI     =
        //                :DFA-IMPB-IRPEF-K2-ANTI
        //                                       :IND-DFA-IMPB-IRPEF-K2-ANTI
        //                  ,IMPST_IRPEF_K2_ANT     =
        //                :DFA-IMPST-IRPEF-K2-ANT
        //                                       :IND-DFA-IMPST-IRPEF-K2-ANT
        //                  ,DT_CESSAZIONE          =
        //           :DFA-DT-CESSAZIONE-DB
        //                                       :IND-DFA-DT-CESSAZIONE
        //                  ,TOT_IMPST              =
        //                :DFA-TOT-IMPST
        //                                       :IND-DFA-TOT-IMPST
        //                  ,ONER_TRASFE            =
        //                :DFA-ONER-TRASFE
        //                                       :IND-DFA-ONER-TRASFE
        //                  ,IMP_NET_TRASFERITO     =
        //                :DFA-IMP-NET-TRASFERITO
        //                                       :IND-DFA-IMP-NET-TRASFERITO
        //                WHERE     DS_RIGA = :DFA-DS-RIGA
        //           END-EXEC.
        dFiscAdesDao.updateRec(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A240-DELETE-PK<br>*/
    private void a240DeletePk() {
        // COB_CODE: EXEC SQL
        //                DELETE
        //                FROM D_FISC_ADES
        //                WHERE     DS_RIGA = :DFA-DS-RIGA
        //           END-EXEC.
        dFiscAdesDao.deleteByDfaDsRiga(dFiscAdes.getDfaDsRiga());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A305-DECLARE-CURSOR-ID-EFF<br>
	 * <pre>----
	 * ----  gestione ID Effetto
	 * ----</pre>*/
    private void a305DeclareCursorIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-ID-UPD-EFF-DFA CURSOR FOR
        //              SELECT
        //                     ID_D_FISC_ADES
        //                    ,ID_ADES
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,TP_ISC_FND
        //                    ,DT_ACCNS_RAPP_FND
        //                    ,IMP_CNBT_AZ_K1
        //                    ,IMP_CNBT_ISC_K1
        //                    ,IMP_CNBT_TFR_K1
        //                    ,IMP_CNBT_VOL_K1
        //                    ,IMP_CNBT_AZ_K2
        //                    ,IMP_CNBT_ISC_K2
        //                    ,IMP_CNBT_TFR_K2
        //                    ,IMP_CNBT_VOL_K2
        //                    ,MATU_K1
        //                    ,MATU_RES_K1
        //                    ,MATU_K2
        //                    ,IMPB_VIS
        //                    ,IMPST_VIS
        //                    ,IMPB_IS_K2
        //                    ,IMPST_SOST_K2
        //                    ,RIDZ_TFR
        //                    ,PC_TFR
        //                    ,ALQ_TFR
        //                    ,TOT_ANTIC
        //                    ,IMPB_TFR_ANTIC
        //                    ,IMPST_TFR_ANTIC
        //                    ,RIDZ_TFR_SU_ANTIC
        //                    ,IMPB_VIS_ANTIC
        //                    ,IMPST_VIS_ANTIC
        //                    ,IMPB_IS_K2_ANTIC
        //                    ,IMPST_SOST_K2_ANTI
        //                    ,ULT_COMMIS_TRASFE
        //                    ,COD_DVS
        //                    ,ALQ_PRVR
        //                    ,PC_ESE_IMPST_TFR
        //                    ,IMP_ESE_IMPST_TFR
        //                    ,ANZ_CNBTVA_CARASS
        //                    ,ANZ_CNBTVA_CARAZI
        //                    ,ANZ_SRVZ
        //                    ,IMP_CNBT_NDED_K1
        //                    ,IMP_CNBT_NDED_K2
        //                    ,IMP_CNBT_NDED_K3
        //                    ,IMP_CNBT_AZ_K3
        //                    ,IMP_CNBT_ISC_K3
        //                    ,IMP_CNBT_TFR_K3
        //                    ,IMP_CNBT_VOL_K3
        //                    ,MATU_K3
        //                    ,IMPB_252_ANTIC
        //                    ,IMPST_252_ANTIC
        //                    ,DT_1A_CNBZ
        //                    ,COMMIS_DI_TRASFE
        //                    ,AA_CNBZ_K1
        //                    ,AA_CNBZ_K2
        //                    ,AA_CNBZ_K3
        //                    ,MM_CNBZ_K1
        //                    ,MM_CNBZ_K2
        //                    ,MM_CNBZ_K3
        //                    ,FL_APPLZ_NEWFIS
        //                    ,REDT_TASS_ABBAT_K3
        //                    ,CNBT_ECC_4X100_K1
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,CREDITO_IS
        //                    ,REDT_TASS_ABBAT_K2
        //                    ,IMPB_IS_K3
        //                    ,IMPST_SOST_K3
        //                    ,IMPB_252_K3
        //                    ,IMPST_252_K3
        //                    ,IMPB_IS_K3_ANTIC
        //                    ,IMPST_SOST_K3_ANTI
        //                    ,IMPB_IRPEF_K1_ANTI
        //                    ,IMPST_IRPEF_K1_ANT
        //                    ,IMPB_IRPEF_K2_ANTI
        //                    ,IMPST_IRPEF_K2_ANT
        //                    ,DT_CESSAZIONE
        //                    ,TOT_IMPST
        //                    ,ONER_TRASFE
        //                    ,IMP_NET_TRASFERITO
        //              FROM D_FISC_ADES
        //              WHERE     ID_D_FISC_ADES = :DFA-ID-D-FISC-ADES
        //                    AND DS_TS_END_CPTZ = :WS-TS-INFINITO
        //                    AND DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //              ORDER BY DT_INI_EFF ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A310-SELECT-ID-EFF<br>*/
    private void a310SelectIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_D_FISC_ADES
        //                ,ID_ADES
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,TP_ISC_FND
        //                ,DT_ACCNS_RAPP_FND
        //                ,IMP_CNBT_AZ_K1
        //                ,IMP_CNBT_ISC_K1
        //                ,IMP_CNBT_TFR_K1
        //                ,IMP_CNBT_VOL_K1
        //                ,IMP_CNBT_AZ_K2
        //                ,IMP_CNBT_ISC_K2
        //                ,IMP_CNBT_TFR_K2
        //                ,IMP_CNBT_VOL_K2
        //                ,MATU_K1
        //                ,MATU_RES_K1
        //                ,MATU_K2
        //                ,IMPB_VIS
        //                ,IMPST_VIS
        //                ,IMPB_IS_K2
        //                ,IMPST_SOST_K2
        //                ,RIDZ_TFR
        //                ,PC_TFR
        //                ,ALQ_TFR
        //                ,TOT_ANTIC
        //                ,IMPB_TFR_ANTIC
        //                ,IMPST_TFR_ANTIC
        //                ,RIDZ_TFR_SU_ANTIC
        //                ,IMPB_VIS_ANTIC
        //                ,IMPST_VIS_ANTIC
        //                ,IMPB_IS_K2_ANTIC
        //                ,IMPST_SOST_K2_ANTI
        //                ,ULT_COMMIS_TRASFE
        //                ,COD_DVS
        //                ,ALQ_PRVR
        //                ,PC_ESE_IMPST_TFR
        //                ,IMP_ESE_IMPST_TFR
        //                ,ANZ_CNBTVA_CARASS
        //                ,ANZ_CNBTVA_CARAZI
        //                ,ANZ_SRVZ
        //                ,IMP_CNBT_NDED_K1
        //                ,IMP_CNBT_NDED_K2
        //                ,IMP_CNBT_NDED_K3
        //                ,IMP_CNBT_AZ_K3
        //                ,IMP_CNBT_ISC_K3
        //                ,IMP_CNBT_TFR_K3
        //                ,IMP_CNBT_VOL_K3
        //                ,MATU_K3
        //                ,IMPB_252_ANTIC
        //                ,IMPST_252_ANTIC
        //                ,DT_1A_CNBZ
        //                ,COMMIS_DI_TRASFE
        //                ,AA_CNBZ_K1
        //                ,AA_CNBZ_K2
        //                ,AA_CNBZ_K3
        //                ,MM_CNBZ_K1
        //                ,MM_CNBZ_K2
        //                ,MM_CNBZ_K3
        //                ,FL_APPLZ_NEWFIS
        //                ,REDT_TASS_ABBAT_K3
        //                ,CNBT_ECC_4X100_K1
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,CREDITO_IS
        //                ,REDT_TASS_ABBAT_K2
        //                ,IMPB_IS_K3
        //                ,IMPST_SOST_K3
        //                ,IMPB_252_K3
        //                ,IMPST_252_K3
        //                ,IMPB_IS_K3_ANTIC
        //                ,IMPST_SOST_K3_ANTI
        //                ,IMPB_IRPEF_K1_ANTI
        //                ,IMPST_IRPEF_K1_ANT
        //                ,IMPB_IRPEF_K2_ANTI
        //                ,IMPST_IRPEF_K2_ANT
        //                ,DT_CESSAZIONE
        //                ,TOT_IMPST
        //                ,ONER_TRASFE
        //                ,IMP_NET_TRASFERITO
        //             INTO
        //                :DFA-ID-D-FISC-ADES
        //               ,:DFA-ID-ADES
        //               ,:DFA-ID-MOVI-CRZ
        //               ,:DFA-ID-MOVI-CHIU
        //                :IND-DFA-ID-MOVI-CHIU
        //               ,:DFA-DT-INI-EFF-DB
        //               ,:DFA-DT-END-EFF-DB
        //               ,:DFA-COD-COMP-ANIA
        //               ,:DFA-TP-ISC-FND
        //                :IND-DFA-TP-ISC-FND
        //               ,:DFA-DT-ACCNS-RAPP-FND-DB
        //                :IND-DFA-DT-ACCNS-RAPP-FND
        //               ,:DFA-IMP-CNBT-AZ-K1
        //                :IND-DFA-IMP-CNBT-AZ-K1
        //               ,:DFA-IMP-CNBT-ISC-K1
        //                :IND-DFA-IMP-CNBT-ISC-K1
        //               ,:DFA-IMP-CNBT-TFR-K1
        //                :IND-DFA-IMP-CNBT-TFR-K1
        //               ,:DFA-IMP-CNBT-VOL-K1
        //                :IND-DFA-IMP-CNBT-VOL-K1
        //               ,:DFA-IMP-CNBT-AZ-K2
        //                :IND-DFA-IMP-CNBT-AZ-K2
        //               ,:DFA-IMP-CNBT-ISC-K2
        //                :IND-DFA-IMP-CNBT-ISC-K2
        //               ,:DFA-IMP-CNBT-TFR-K2
        //                :IND-DFA-IMP-CNBT-TFR-K2
        //               ,:DFA-IMP-CNBT-VOL-K2
        //                :IND-DFA-IMP-CNBT-VOL-K2
        //               ,:DFA-MATU-K1
        //                :IND-DFA-MATU-K1
        //               ,:DFA-MATU-RES-K1
        //                :IND-DFA-MATU-RES-K1
        //               ,:DFA-MATU-K2
        //                :IND-DFA-MATU-K2
        //               ,:DFA-IMPB-VIS
        //                :IND-DFA-IMPB-VIS
        //               ,:DFA-IMPST-VIS
        //                :IND-DFA-IMPST-VIS
        //               ,:DFA-IMPB-IS-K2
        //                :IND-DFA-IMPB-IS-K2
        //               ,:DFA-IMPST-SOST-K2
        //                :IND-DFA-IMPST-SOST-K2
        //               ,:DFA-RIDZ-TFR
        //                :IND-DFA-RIDZ-TFR
        //               ,:DFA-PC-TFR
        //                :IND-DFA-PC-TFR
        //               ,:DFA-ALQ-TFR
        //                :IND-DFA-ALQ-TFR
        //               ,:DFA-TOT-ANTIC
        //                :IND-DFA-TOT-ANTIC
        //               ,:DFA-IMPB-TFR-ANTIC
        //                :IND-DFA-IMPB-TFR-ANTIC
        //               ,:DFA-IMPST-TFR-ANTIC
        //                :IND-DFA-IMPST-TFR-ANTIC
        //               ,:DFA-RIDZ-TFR-SU-ANTIC
        //                :IND-DFA-RIDZ-TFR-SU-ANTIC
        //               ,:DFA-IMPB-VIS-ANTIC
        //                :IND-DFA-IMPB-VIS-ANTIC
        //               ,:DFA-IMPST-VIS-ANTIC
        //                :IND-DFA-IMPST-VIS-ANTIC
        //               ,:DFA-IMPB-IS-K2-ANTIC
        //                :IND-DFA-IMPB-IS-K2-ANTIC
        //               ,:DFA-IMPST-SOST-K2-ANTI
        //                :IND-DFA-IMPST-SOST-K2-ANTI
        //               ,:DFA-ULT-COMMIS-TRASFE
        //                :IND-DFA-ULT-COMMIS-TRASFE
        //               ,:DFA-COD-DVS
        //                :IND-DFA-COD-DVS
        //               ,:DFA-ALQ-PRVR
        //                :IND-DFA-ALQ-PRVR
        //               ,:DFA-PC-ESE-IMPST-TFR
        //                :IND-DFA-PC-ESE-IMPST-TFR
        //               ,:DFA-IMP-ESE-IMPST-TFR
        //                :IND-DFA-IMP-ESE-IMPST-TFR
        //               ,:DFA-ANZ-CNBTVA-CARASS
        //                :IND-DFA-ANZ-CNBTVA-CARASS
        //               ,:DFA-ANZ-CNBTVA-CARAZI
        //                :IND-DFA-ANZ-CNBTVA-CARAZI
        //               ,:DFA-ANZ-SRVZ
        //                :IND-DFA-ANZ-SRVZ
        //               ,:DFA-IMP-CNBT-NDED-K1
        //                :IND-DFA-IMP-CNBT-NDED-K1
        //               ,:DFA-IMP-CNBT-NDED-K2
        //                :IND-DFA-IMP-CNBT-NDED-K2
        //               ,:DFA-IMP-CNBT-NDED-K3
        //                :IND-DFA-IMP-CNBT-NDED-K3
        //               ,:DFA-IMP-CNBT-AZ-K3
        //                :IND-DFA-IMP-CNBT-AZ-K3
        //               ,:DFA-IMP-CNBT-ISC-K3
        //                :IND-DFA-IMP-CNBT-ISC-K3
        //               ,:DFA-IMP-CNBT-TFR-K3
        //                :IND-DFA-IMP-CNBT-TFR-K3
        //               ,:DFA-IMP-CNBT-VOL-K3
        //                :IND-DFA-IMP-CNBT-VOL-K3
        //               ,:DFA-MATU-K3
        //                :IND-DFA-MATU-K3
        //               ,:DFA-IMPB-252-ANTIC
        //                :IND-DFA-IMPB-252-ANTIC
        //               ,:DFA-IMPST-252-ANTIC
        //                :IND-DFA-IMPST-252-ANTIC
        //               ,:DFA-DT-1A-CNBZ-DB
        //                :IND-DFA-DT-1A-CNBZ
        //               ,:DFA-COMMIS-DI-TRASFE
        //                :IND-DFA-COMMIS-DI-TRASFE
        //               ,:DFA-AA-CNBZ-K1
        //                :IND-DFA-AA-CNBZ-K1
        //               ,:DFA-AA-CNBZ-K2
        //                :IND-DFA-AA-CNBZ-K2
        //               ,:DFA-AA-CNBZ-K3
        //                :IND-DFA-AA-CNBZ-K3
        //               ,:DFA-MM-CNBZ-K1
        //                :IND-DFA-MM-CNBZ-K1
        //               ,:DFA-MM-CNBZ-K2
        //                :IND-DFA-MM-CNBZ-K2
        //               ,:DFA-MM-CNBZ-K3
        //                :IND-DFA-MM-CNBZ-K3
        //               ,:DFA-FL-APPLZ-NEWFIS
        //                :IND-DFA-FL-APPLZ-NEWFIS
        //               ,:DFA-REDT-TASS-ABBAT-K3
        //                :IND-DFA-REDT-TASS-ABBAT-K3
        //               ,:DFA-CNBT-ECC-4X100-K1
        //                :IND-DFA-CNBT-ECC-4X100-K1
        //               ,:DFA-DS-RIGA
        //               ,:DFA-DS-OPER-SQL
        //               ,:DFA-DS-VER
        //               ,:DFA-DS-TS-INI-CPTZ
        //               ,:DFA-DS-TS-END-CPTZ
        //               ,:DFA-DS-UTENTE
        //               ,:DFA-DS-STATO-ELAB
        //               ,:DFA-CREDITO-IS
        //                :IND-DFA-CREDITO-IS
        //               ,:DFA-REDT-TASS-ABBAT-K2
        //                :IND-DFA-REDT-TASS-ABBAT-K2
        //               ,:DFA-IMPB-IS-K3
        //                :IND-DFA-IMPB-IS-K3
        //               ,:DFA-IMPST-SOST-K3
        //                :IND-DFA-IMPST-SOST-K3
        //               ,:DFA-IMPB-252-K3
        //                :IND-DFA-IMPB-252-K3
        //               ,:DFA-IMPST-252-K3
        //                :IND-DFA-IMPST-252-K3
        //               ,:DFA-IMPB-IS-K3-ANTIC
        //                :IND-DFA-IMPB-IS-K3-ANTIC
        //               ,:DFA-IMPST-SOST-K3-ANTI
        //                :IND-DFA-IMPST-SOST-K3-ANTI
        //               ,:DFA-IMPB-IRPEF-K1-ANTI
        //                :IND-DFA-IMPB-IRPEF-K1-ANTI
        //               ,:DFA-IMPST-IRPEF-K1-ANT
        //                :IND-DFA-IMPST-IRPEF-K1-ANT
        //               ,:DFA-IMPB-IRPEF-K2-ANTI
        //                :IND-DFA-IMPB-IRPEF-K2-ANTI
        //               ,:DFA-IMPST-IRPEF-K2-ANT
        //                :IND-DFA-IMPST-IRPEF-K2-ANT
        //               ,:DFA-DT-CESSAZIONE-DB
        //                :IND-DFA-DT-CESSAZIONE
        //               ,:DFA-TOT-IMPST
        //                :IND-DFA-TOT-IMPST
        //               ,:DFA-ONER-TRASFE
        //                :IND-DFA-ONER-TRASFE
        //               ,:DFA-IMP-NET-TRASFERITO
        //                :IND-DFA-IMP-NET-TRASFERITO
        //             FROM D_FISC_ADES
        //             WHERE     ID_D_FISC_ADES = :DFA-ID-D-FISC-ADES
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        dFiscAdesDao.selectRec(dFiscAdes.getDfaIdDFiscAdes(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A330-UPDATE-ID-EFF<br>*/
    private void a330UpdateIdEff() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE D_FISC_ADES SET
        //                   ID_D_FISC_ADES         =
        //                :DFA-ID-D-FISC-ADES
        //                  ,ID_ADES                =
        //                :DFA-ID-ADES
        //                  ,ID_MOVI_CRZ            =
        //                :DFA-ID-MOVI-CRZ
        //                  ,ID_MOVI_CHIU           =
        //                :DFA-ID-MOVI-CHIU
        //                                       :IND-DFA-ID-MOVI-CHIU
        //                  ,DT_INI_EFF             =
        //           :DFA-DT-INI-EFF-DB
        //                  ,DT_END_EFF             =
        //           :DFA-DT-END-EFF-DB
        //                  ,COD_COMP_ANIA          =
        //                :DFA-COD-COMP-ANIA
        //                  ,TP_ISC_FND             =
        //                :DFA-TP-ISC-FND
        //                                       :IND-DFA-TP-ISC-FND
        //                  ,DT_ACCNS_RAPP_FND      =
        //           :DFA-DT-ACCNS-RAPP-FND-DB
        //                                       :IND-DFA-DT-ACCNS-RAPP-FND
        //                  ,IMP_CNBT_AZ_K1         =
        //                :DFA-IMP-CNBT-AZ-K1
        //                                       :IND-DFA-IMP-CNBT-AZ-K1
        //                  ,IMP_CNBT_ISC_K1        =
        //                :DFA-IMP-CNBT-ISC-K1
        //                                       :IND-DFA-IMP-CNBT-ISC-K1
        //                  ,IMP_CNBT_TFR_K1        =
        //                :DFA-IMP-CNBT-TFR-K1
        //                                       :IND-DFA-IMP-CNBT-TFR-K1
        //                  ,IMP_CNBT_VOL_K1        =
        //                :DFA-IMP-CNBT-VOL-K1
        //                                       :IND-DFA-IMP-CNBT-VOL-K1
        //                  ,IMP_CNBT_AZ_K2         =
        //                :DFA-IMP-CNBT-AZ-K2
        //                                       :IND-DFA-IMP-CNBT-AZ-K2
        //                  ,IMP_CNBT_ISC_K2        =
        //                :DFA-IMP-CNBT-ISC-K2
        //                                       :IND-DFA-IMP-CNBT-ISC-K2
        //                  ,IMP_CNBT_TFR_K2        =
        //                :DFA-IMP-CNBT-TFR-K2
        //                                       :IND-DFA-IMP-CNBT-TFR-K2
        //                  ,IMP_CNBT_VOL_K2        =
        //                :DFA-IMP-CNBT-VOL-K2
        //                                       :IND-DFA-IMP-CNBT-VOL-K2
        //                  ,MATU_K1                =
        //                :DFA-MATU-K1
        //                                       :IND-DFA-MATU-K1
        //                  ,MATU_RES_K1            =
        //                :DFA-MATU-RES-K1
        //                                       :IND-DFA-MATU-RES-K1
        //                  ,MATU_K2                =
        //                :DFA-MATU-K2
        //                                       :IND-DFA-MATU-K2
        //                  ,IMPB_VIS               =
        //                :DFA-IMPB-VIS
        //                                       :IND-DFA-IMPB-VIS
        //                  ,IMPST_VIS              =
        //                :DFA-IMPST-VIS
        //                                       :IND-DFA-IMPST-VIS
        //                  ,IMPB_IS_K2             =
        //                :DFA-IMPB-IS-K2
        //                                       :IND-DFA-IMPB-IS-K2
        //                  ,IMPST_SOST_K2          =
        //                :DFA-IMPST-SOST-K2
        //                                       :IND-DFA-IMPST-SOST-K2
        //                  ,RIDZ_TFR               =
        //                :DFA-RIDZ-TFR
        //                                       :IND-DFA-RIDZ-TFR
        //                  ,PC_TFR                 =
        //                :DFA-PC-TFR
        //                                       :IND-DFA-PC-TFR
        //                  ,ALQ_TFR                =
        //                :DFA-ALQ-TFR
        //                                       :IND-DFA-ALQ-TFR
        //                  ,TOT_ANTIC              =
        //                :DFA-TOT-ANTIC
        //                                       :IND-DFA-TOT-ANTIC
        //                  ,IMPB_TFR_ANTIC         =
        //                :DFA-IMPB-TFR-ANTIC
        //                                       :IND-DFA-IMPB-TFR-ANTIC
        //                  ,IMPST_TFR_ANTIC        =
        //                :DFA-IMPST-TFR-ANTIC
        //                                       :IND-DFA-IMPST-TFR-ANTIC
        //                  ,RIDZ_TFR_SU_ANTIC      =
        //                :DFA-RIDZ-TFR-SU-ANTIC
        //                                       :IND-DFA-RIDZ-TFR-SU-ANTIC
        //                  ,IMPB_VIS_ANTIC         =
        //                :DFA-IMPB-VIS-ANTIC
        //                                       :IND-DFA-IMPB-VIS-ANTIC
        //                  ,IMPST_VIS_ANTIC        =
        //                :DFA-IMPST-VIS-ANTIC
        //                                       :IND-DFA-IMPST-VIS-ANTIC
        //                  ,IMPB_IS_K2_ANTIC       =
        //                :DFA-IMPB-IS-K2-ANTIC
        //                                       :IND-DFA-IMPB-IS-K2-ANTIC
        //                  ,IMPST_SOST_K2_ANTI     =
        //                :DFA-IMPST-SOST-K2-ANTI
        //                                       :IND-DFA-IMPST-SOST-K2-ANTI
        //                  ,ULT_COMMIS_TRASFE      =
        //                :DFA-ULT-COMMIS-TRASFE
        //                                       :IND-DFA-ULT-COMMIS-TRASFE
        //                  ,COD_DVS                =
        //                :DFA-COD-DVS
        //                                       :IND-DFA-COD-DVS
        //                  ,ALQ_PRVR               =
        //                :DFA-ALQ-PRVR
        //                                       :IND-DFA-ALQ-PRVR
        //                  ,PC_ESE_IMPST_TFR       =
        //                :DFA-PC-ESE-IMPST-TFR
        //                                       :IND-DFA-PC-ESE-IMPST-TFR
        //                  ,IMP_ESE_IMPST_TFR      =
        //                :DFA-IMP-ESE-IMPST-TFR
        //                                       :IND-DFA-IMP-ESE-IMPST-TFR
        //                  ,ANZ_CNBTVA_CARASS      =
        //                :DFA-ANZ-CNBTVA-CARASS
        //                                       :IND-DFA-ANZ-CNBTVA-CARASS
        //                  ,ANZ_CNBTVA_CARAZI      =
        //                :DFA-ANZ-CNBTVA-CARAZI
        //                                       :IND-DFA-ANZ-CNBTVA-CARAZI
        //                  ,ANZ_SRVZ               =
        //                :DFA-ANZ-SRVZ
        //                                       :IND-DFA-ANZ-SRVZ
        //                  ,IMP_CNBT_NDED_K1       =
        //                :DFA-IMP-CNBT-NDED-K1
        //                                       :IND-DFA-IMP-CNBT-NDED-K1
        //                  ,IMP_CNBT_NDED_K2       =
        //                :DFA-IMP-CNBT-NDED-K2
        //                                       :IND-DFA-IMP-CNBT-NDED-K2
        //                  ,IMP_CNBT_NDED_K3       =
        //                :DFA-IMP-CNBT-NDED-K3
        //                                       :IND-DFA-IMP-CNBT-NDED-K3
        //                  ,IMP_CNBT_AZ_K3         =
        //                :DFA-IMP-CNBT-AZ-K3
        //                                       :IND-DFA-IMP-CNBT-AZ-K3
        //                  ,IMP_CNBT_ISC_K3        =
        //                :DFA-IMP-CNBT-ISC-K3
        //                                       :IND-DFA-IMP-CNBT-ISC-K3
        //                  ,IMP_CNBT_TFR_K3        =
        //                :DFA-IMP-CNBT-TFR-K3
        //                                       :IND-DFA-IMP-CNBT-TFR-K3
        //                  ,IMP_CNBT_VOL_K3        =
        //                :DFA-IMP-CNBT-VOL-K3
        //                                       :IND-DFA-IMP-CNBT-VOL-K3
        //                  ,MATU_K3                =
        //                :DFA-MATU-K3
        //                                       :IND-DFA-MATU-K3
        //                  ,IMPB_252_ANTIC         =
        //                :DFA-IMPB-252-ANTIC
        //                                       :IND-DFA-IMPB-252-ANTIC
        //                  ,IMPST_252_ANTIC        =
        //                :DFA-IMPST-252-ANTIC
        //                                       :IND-DFA-IMPST-252-ANTIC
        //                  ,DT_1A_CNBZ             =
        //           :DFA-DT-1A-CNBZ-DB
        //                                       :IND-DFA-DT-1A-CNBZ
        //                  ,COMMIS_DI_TRASFE       =
        //                :DFA-COMMIS-DI-TRASFE
        //                                       :IND-DFA-COMMIS-DI-TRASFE
        //                  ,AA_CNBZ_K1             =
        //                :DFA-AA-CNBZ-K1
        //                                       :IND-DFA-AA-CNBZ-K1
        //                  ,AA_CNBZ_K2             =
        //                :DFA-AA-CNBZ-K2
        //                                       :IND-DFA-AA-CNBZ-K2
        //                  ,AA_CNBZ_K3             =
        //                :DFA-AA-CNBZ-K3
        //                                       :IND-DFA-AA-CNBZ-K3
        //                  ,MM_CNBZ_K1             =
        //                :DFA-MM-CNBZ-K1
        //                                       :IND-DFA-MM-CNBZ-K1
        //                  ,MM_CNBZ_K2             =
        //                :DFA-MM-CNBZ-K2
        //                                       :IND-DFA-MM-CNBZ-K2
        //                  ,MM_CNBZ_K3             =
        //                :DFA-MM-CNBZ-K3
        //                                       :IND-DFA-MM-CNBZ-K3
        //                  ,FL_APPLZ_NEWFIS        =
        //                :DFA-FL-APPLZ-NEWFIS
        //                                       :IND-DFA-FL-APPLZ-NEWFIS
        //                  ,REDT_TASS_ABBAT_K3     =
        //                :DFA-REDT-TASS-ABBAT-K3
        //                                       :IND-DFA-REDT-TASS-ABBAT-K3
        //                  ,CNBT_ECC_4X100_K1      =
        //                :DFA-CNBT-ECC-4X100-K1
        //                                       :IND-DFA-CNBT-ECC-4X100-K1
        //                  ,DS_RIGA                =
        //                :DFA-DS-RIGA
        //                  ,DS_OPER_SQL            =
        //                :DFA-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :DFA-DS-VER
        //                  ,DS_TS_INI_CPTZ         =
        //                :DFA-DS-TS-INI-CPTZ
        //                  ,DS_TS_END_CPTZ         =
        //                :DFA-DS-TS-END-CPTZ
        //                  ,DS_UTENTE              =
        //                :DFA-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :DFA-DS-STATO-ELAB
        //                  ,CREDITO_IS             =
        //                :DFA-CREDITO-IS
        //                                       :IND-DFA-CREDITO-IS
        //                  ,REDT_TASS_ABBAT_K2     =
        //                :DFA-REDT-TASS-ABBAT-K2
        //                                       :IND-DFA-REDT-TASS-ABBAT-K2
        //                  ,IMPB_IS_K3             =
        //                :DFA-IMPB-IS-K3
        //                                       :IND-DFA-IMPB-IS-K3
        //                  ,IMPST_SOST_K3          =
        //                :DFA-IMPST-SOST-K3
        //                                       :IND-DFA-IMPST-SOST-K3
        //                  ,IMPB_252_K3            =
        //                :DFA-IMPB-252-K3
        //                                       :IND-DFA-IMPB-252-K3
        //                  ,IMPST_252_K3           =
        //                :DFA-IMPST-252-K3
        //                                       :IND-DFA-IMPST-252-K3
        //                  ,IMPB_IS_K3_ANTIC       =
        //                :DFA-IMPB-IS-K3-ANTIC
        //                                       :IND-DFA-IMPB-IS-K3-ANTIC
        //                  ,IMPST_SOST_K3_ANTI     =
        //                :DFA-IMPST-SOST-K3-ANTI
        //                                       :IND-DFA-IMPST-SOST-K3-ANTI
        //                  ,IMPB_IRPEF_K1_ANTI     =
        //                :DFA-IMPB-IRPEF-K1-ANTI
        //                                       :IND-DFA-IMPB-IRPEF-K1-ANTI
        //                  ,IMPST_IRPEF_K1_ANT     =
        //                :DFA-IMPST-IRPEF-K1-ANT
        //                                       :IND-DFA-IMPST-IRPEF-K1-ANT
        //                  ,IMPB_IRPEF_K2_ANTI     =
        //                :DFA-IMPB-IRPEF-K2-ANTI
        //                                       :IND-DFA-IMPB-IRPEF-K2-ANTI
        //                  ,IMPST_IRPEF_K2_ANT     =
        //                :DFA-IMPST-IRPEF-K2-ANT
        //                                       :IND-DFA-IMPST-IRPEF-K2-ANT
        //                  ,DT_CESSAZIONE          =
        //           :DFA-DT-CESSAZIONE-DB
        //                                       :IND-DFA-DT-CESSAZIONE
        //                  ,TOT_IMPST              =
        //                :DFA-TOT-IMPST
        //                                       :IND-DFA-TOT-IMPST
        //                  ,ONER_TRASFE            =
        //                :DFA-ONER-TRASFE
        //                                       :IND-DFA-ONER-TRASFE
        //                  ,IMP_NET_TRASFERITO     =
        //                :DFA-IMP-NET-TRASFERITO
        //                                       :IND-DFA-IMP-NET-TRASFERITO
        //                WHERE     DS_RIGA = :DFA-DS-RIGA
        //                   AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        dFiscAdesDao.updateRec1(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A360-OPEN-CURSOR-ID-EFF<br>*/
    private void a360OpenCursorIdEff() {
        // COB_CODE: PERFORM A305-DECLARE-CURSOR-ID-EFF THRU A305-EX.
        a305DeclareCursorIdEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-ID-UPD-EFF-DFA
        //           END-EXEC.
        dFiscAdesDao.openCIdUpdEffDfa(dFiscAdes.getDfaIdDFiscAdes(), ws.getIdsv0010().getWsTsInfinito(), ws.getIdsv0010().getWsDataInizioEffettoDb(), idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A370-CLOSE-CURSOR-ID-EFF<br>*/
    private void a370CloseCursorIdEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-ID-UPD-EFF-DFA
        //           END-EXEC.
        dFiscAdesDao.closeCIdUpdEffDfa();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A390-FETCH-NEXT-ID-EFF<br>*/
    private void a390FetchNextIdEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-ID-UPD-EFF-DFA
        //           INTO
        //                :DFA-ID-D-FISC-ADES
        //               ,:DFA-ID-ADES
        //               ,:DFA-ID-MOVI-CRZ
        //               ,:DFA-ID-MOVI-CHIU
        //                :IND-DFA-ID-MOVI-CHIU
        //               ,:DFA-DT-INI-EFF-DB
        //               ,:DFA-DT-END-EFF-DB
        //               ,:DFA-COD-COMP-ANIA
        //               ,:DFA-TP-ISC-FND
        //                :IND-DFA-TP-ISC-FND
        //               ,:DFA-DT-ACCNS-RAPP-FND-DB
        //                :IND-DFA-DT-ACCNS-RAPP-FND
        //               ,:DFA-IMP-CNBT-AZ-K1
        //                :IND-DFA-IMP-CNBT-AZ-K1
        //               ,:DFA-IMP-CNBT-ISC-K1
        //                :IND-DFA-IMP-CNBT-ISC-K1
        //               ,:DFA-IMP-CNBT-TFR-K1
        //                :IND-DFA-IMP-CNBT-TFR-K1
        //               ,:DFA-IMP-CNBT-VOL-K1
        //                :IND-DFA-IMP-CNBT-VOL-K1
        //               ,:DFA-IMP-CNBT-AZ-K2
        //                :IND-DFA-IMP-CNBT-AZ-K2
        //               ,:DFA-IMP-CNBT-ISC-K2
        //                :IND-DFA-IMP-CNBT-ISC-K2
        //               ,:DFA-IMP-CNBT-TFR-K2
        //                :IND-DFA-IMP-CNBT-TFR-K2
        //               ,:DFA-IMP-CNBT-VOL-K2
        //                :IND-DFA-IMP-CNBT-VOL-K2
        //               ,:DFA-MATU-K1
        //                :IND-DFA-MATU-K1
        //               ,:DFA-MATU-RES-K1
        //                :IND-DFA-MATU-RES-K1
        //               ,:DFA-MATU-K2
        //                :IND-DFA-MATU-K2
        //               ,:DFA-IMPB-VIS
        //                :IND-DFA-IMPB-VIS
        //               ,:DFA-IMPST-VIS
        //                :IND-DFA-IMPST-VIS
        //               ,:DFA-IMPB-IS-K2
        //                :IND-DFA-IMPB-IS-K2
        //               ,:DFA-IMPST-SOST-K2
        //                :IND-DFA-IMPST-SOST-K2
        //               ,:DFA-RIDZ-TFR
        //                :IND-DFA-RIDZ-TFR
        //               ,:DFA-PC-TFR
        //                :IND-DFA-PC-TFR
        //               ,:DFA-ALQ-TFR
        //                :IND-DFA-ALQ-TFR
        //               ,:DFA-TOT-ANTIC
        //                :IND-DFA-TOT-ANTIC
        //               ,:DFA-IMPB-TFR-ANTIC
        //                :IND-DFA-IMPB-TFR-ANTIC
        //               ,:DFA-IMPST-TFR-ANTIC
        //                :IND-DFA-IMPST-TFR-ANTIC
        //               ,:DFA-RIDZ-TFR-SU-ANTIC
        //                :IND-DFA-RIDZ-TFR-SU-ANTIC
        //               ,:DFA-IMPB-VIS-ANTIC
        //                :IND-DFA-IMPB-VIS-ANTIC
        //               ,:DFA-IMPST-VIS-ANTIC
        //                :IND-DFA-IMPST-VIS-ANTIC
        //               ,:DFA-IMPB-IS-K2-ANTIC
        //                :IND-DFA-IMPB-IS-K2-ANTIC
        //               ,:DFA-IMPST-SOST-K2-ANTI
        //                :IND-DFA-IMPST-SOST-K2-ANTI
        //               ,:DFA-ULT-COMMIS-TRASFE
        //                :IND-DFA-ULT-COMMIS-TRASFE
        //               ,:DFA-COD-DVS
        //                :IND-DFA-COD-DVS
        //               ,:DFA-ALQ-PRVR
        //                :IND-DFA-ALQ-PRVR
        //               ,:DFA-PC-ESE-IMPST-TFR
        //                :IND-DFA-PC-ESE-IMPST-TFR
        //               ,:DFA-IMP-ESE-IMPST-TFR
        //                :IND-DFA-IMP-ESE-IMPST-TFR
        //               ,:DFA-ANZ-CNBTVA-CARASS
        //                :IND-DFA-ANZ-CNBTVA-CARASS
        //               ,:DFA-ANZ-CNBTVA-CARAZI
        //                :IND-DFA-ANZ-CNBTVA-CARAZI
        //               ,:DFA-ANZ-SRVZ
        //                :IND-DFA-ANZ-SRVZ
        //               ,:DFA-IMP-CNBT-NDED-K1
        //                :IND-DFA-IMP-CNBT-NDED-K1
        //               ,:DFA-IMP-CNBT-NDED-K2
        //                :IND-DFA-IMP-CNBT-NDED-K2
        //               ,:DFA-IMP-CNBT-NDED-K3
        //                :IND-DFA-IMP-CNBT-NDED-K3
        //               ,:DFA-IMP-CNBT-AZ-K3
        //                :IND-DFA-IMP-CNBT-AZ-K3
        //               ,:DFA-IMP-CNBT-ISC-K3
        //                :IND-DFA-IMP-CNBT-ISC-K3
        //               ,:DFA-IMP-CNBT-TFR-K3
        //                :IND-DFA-IMP-CNBT-TFR-K3
        //               ,:DFA-IMP-CNBT-VOL-K3
        //                :IND-DFA-IMP-CNBT-VOL-K3
        //               ,:DFA-MATU-K3
        //                :IND-DFA-MATU-K3
        //               ,:DFA-IMPB-252-ANTIC
        //                :IND-DFA-IMPB-252-ANTIC
        //               ,:DFA-IMPST-252-ANTIC
        //                :IND-DFA-IMPST-252-ANTIC
        //               ,:DFA-DT-1A-CNBZ-DB
        //                :IND-DFA-DT-1A-CNBZ
        //               ,:DFA-COMMIS-DI-TRASFE
        //                :IND-DFA-COMMIS-DI-TRASFE
        //               ,:DFA-AA-CNBZ-K1
        //                :IND-DFA-AA-CNBZ-K1
        //               ,:DFA-AA-CNBZ-K2
        //                :IND-DFA-AA-CNBZ-K2
        //               ,:DFA-AA-CNBZ-K3
        //                :IND-DFA-AA-CNBZ-K3
        //               ,:DFA-MM-CNBZ-K1
        //                :IND-DFA-MM-CNBZ-K1
        //               ,:DFA-MM-CNBZ-K2
        //                :IND-DFA-MM-CNBZ-K2
        //               ,:DFA-MM-CNBZ-K3
        //                :IND-DFA-MM-CNBZ-K3
        //               ,:DFA-FL-APPLZ-NEWFIS
        //                :IND-DFA-FL-APPLZ-NEWFIS
        //               ,:DFA-REDT-TASS-ABBAT-K3
        //                :IND-DFA-REDT-TASS-ABBAT-K3
        //               ,:DFA-CNBT-ECC-4X100-K1
        //                :IND-DFA-CNBT-ECC-4X100-K1
        //               ,:DFA-DS-RIGA
        //               ,:DFA-DS-OPER-SQL
        //               ,:DFA-DS-VER
        //               ,:DFA-DS-TS-INI-CPTZ
        //               ,:DFA-DS-TS-END-CPTZ
        //               ,:DFA-DS-UTENTE
        //               ,:DFA-DS-STATO-ELAB
        //               ,:DFA-CREDITO-IS
        //                :IND-DFA-CREDITO-IS
        //               ,:DFA-REDT-TASS-ABBAT-K2
        //                :IND-DFA-REDT-TASS-ABBAT-K2
        //               ,:DFA-IMPB-IS-K3
        //                :IND-DFA-IMPB-IS-K3
        //               ,:DFA-IMPST-SOST-K3
        //                :IND-DFA-IMPST-SOST-K3
        //               ,:DFA-IMPB-252-K3
        //                :IND-DFA-IMPB-252-K3
        //               ,:DFA-IMPST-252-K3
        //                :IND-DFA-IMPST-252-K3
        //               ,:DFA-IMPB-IS-K3-ANTIC
        //                :IND-DFA-IMPB-IS-K3-ANTIC
        //               ,:DFA-IMPST-SOST-K3-ANTI
        //                :IND-DFA-IMPST-SOST-K3-ANTI
        //               ,:DFA-IMPB-IRPEF-K1-ANTI
        //                :IND-DFA-IMPB-IRPEF-K1-ANTI
        //               ,:DFA-IMPST-IRPEF-K1-ANT
        //                :IND-DFA-IMPST-IRPEF-K1-ANT
        //               ,:DFA-IMPB-IRPEF-K2-ANTI
        //                :IND-DFA-IMPB-IRPEF-K2-ANTI
        //               ,:DFA-IMPST-IRPEF-K2-ANT
        //                :IND-DFA-IMPST-IRPEF-K2-ANT
        //               ,:DFA-DT-CESSAZIONE-DB
        //                :IND-DFA-DT-CESSAZIONE
        //               ,:DFA-TOT-IMPST
        //                :IND-DFA-TOT-IMPST
        //               ,:DFA-ONER-TRASFE
        //                :IND-DFA-ONER-TRASFE
        //               ,:DFA-IMP-NET-TRASFERITO
        //                :IND-DFA-IMP-NET-TRASFERITO
        //           END-EXEC.
        dFiscAdesDao.fetchCIdUpdEffDfa(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A370-CLOSE-CURSOR-ID-EFF THRU A370-EX
            a370CloseCursorIdEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A405-DECLARE-CURSOR-IDP-EFF<br>
	 * <pre>----
	 * ----  gestione IDP Effetto
	 * ----</pre>*/
    private void a405DeclareCursorIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IDP-EFF-DFA CURSOR FOR
        //              SELECT
        //                     ID_D_FISC_ADES
        //                    ,ID_ADES
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,TP_ISC_FND
        //                    ,DT_ACCNS_RAPP_FND
        //                    ,IMP_CNBT_AZ_K1
        //                    ,IMP_CNBT_ISC_K1
        //                    ,IMP_CNBT_TFR_K1
        //                    ,IMP_CNBT_VOL_K1
        //                    ,IMP_CNBT_AZ_K2
        //                    ,IMP_CNBT_ISC_K2
        //                    ,IMP_CNBT_TFR_K2
        //                    ,IMP_CNBT_VOL_K2
        //                    ,MATU_K1
        //                    ,MATU_RES_K1
        //                    ,MATU_K2
        //                    ,IMPB_VIS
        //                    ,IMPST_VIS
        //                    ,IMPB_IS_K2
        //                    ,IMPST_SOST_K2
        //                    ,RIDZ_TFR
        //                    ,PC_TFR
        //                    ,ALQ_TFR
        //                    ,TOT_ANTIC
        //                    ,IMPB_TFR_ANTIC
        //                    ,IMPST_TFR_ANTIC
        //                    ,RIDZ_TFR_SU_ANTIC
        //                    ,IMPB_VIS_ANTIC
        //                    ,IMPST_VIS_ANTIC
        //                    ,IMPB_IS_K2_ANTIC
        //                    ,IMPST_SOST_K2_ANTI
        //                    ,ULT_COMMIS_TRASFE
        //                    ,COD_DVS
        //                    ,ALQ_PRVR
        //                    ,PC_ESE_IMPST_TFR
        //                    ,IMP_ESE_IMPST_TFR
        //                    ,ANZ_CNBTVA_CARASS
        //                    ,ANZ_CNBTVA_CARAZI
        //                    ,ANZ_SRVZ
        //                    ,IMP_CNBT_NDED_K1
        //                    ,IMP_CNBT_NDED_K2
        //                    ,IMP_CNBT_NDED_K3
        //                    ,IMP_CNBT_AZ_K3
        //                    ,IMP_CNBT_ISC_K3
        //                    ,IMP_CNBT_TFR_K3
        //                    ,IMP_CNBT_VOL_K3
        //                    ,MATU_K3
        //                    ,IMPB_252_ANTIC
        //                    ,IMPST_252_ANTIC
        //                    ,DT_1A_CNBZ
        //                    ,COMMIS_DI_TRASFE
        //                    ,AA_CNBZ_K1
        //                    ,AA_CNBZ_K2
        //                    ,AA_CNBZ_K3
        //                    ,MM_CNBZ_K1
        //                    ,MM_CNBZ_K2
        //                    ,MM_CNBZ_K3
        //                    ,FL_APPLZ_NEWFIS
        //                    ,REDT_TASS_ABBAT_K3
        //                    ,CNBT_ECC_4X100_K1
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,CREDITO_IS
        //                    ,REDT_TASS_ABBAT_K2
        //                    ,IMPB_IS_K3
        //                    ,IMPST_SOST_K3
        //                    ,IMPB_252_K3
        //                    ,IMPST_252_K3
        //                    ,IMPB_IS_K3_ANTIC
        //                    ,IMPST_SOST_K3_ANTI
        //                    ,IMPB_IRPEF_K1_ANTI
        //                    ,IMPST_IRPEF_K1_ANT
        //                    ,IMPB_IRPEF_K2_ANTI
        //                    ,IMPST_IRPEF_K2_ANT
        //                    ,DT_CESSAZIONE
        //                    ,TOT_IMPST
        //                    ,ONER_TRASFE
        //                    ,IMP_NET_TRASFERITO
        //              FROM D_FISC_ADES
        //              WHERE     ID_ADES = :DFA-ID-ADES
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //              ORDER BY ID_D_FISC_ADES ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A410-SELECT-IDP-EFF<br>*/
    private void a410SelectIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_D_FISC_ADES
        //                ,ID_ADES
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,TP_ISC_FND
        //                ,DT_ACCNS_RAPP_FND
        //                ,IMP_CNBT_AZ_K1
        //                ,IMP_CNBT_ISC_K1
        //                ,IMP_CNBT_TFR_K1
        //                ,IMP_CNBT_VOL_K1
        //                ,IMP_CNBT_AZ_K2
        //                ,IMP_CNBT_ISC_K2
        //                ,IMP_CNBT_TFR_K2
        //                ,IMP_CNBT_VOL_K2
        //                ,MATU_K1
        //                ,MATU_RES_K1
        //                ,MATU_K2
        //                ,IMPB_VIS
        //                ,IMPST_VIS
        //                ,IMPB_IS_K2
        //                ,IMPST_SOST_K2
        //                ,RIDZ_TFR
        //                ,PC_TFR
        //                ,ALQ_TFR
        //                ,TOT_ANTIC
        //                ,IMPB_TFR_ANTIC
        //                ,IMPST_TFR_ANTIC
        //                ,RIDZ_TFR_SU_ANTIC
        //                ,IMPB_VIS_ANTIC
        //                ,IMPST_VIS_ANTIC
        //                ,IMPB_IS_K2_ANTIC
        //                ,IMPST_SOST_K2_ANTI
        //                ,ULT_COMMIS_TRASFE
        //                ,COD_DVS
        //                ,ALQ_PRVR
        //                ,PC_ESE_IMPST_TFR
        //                ,IMP_ESE_IMPST_TFR
        //                ,ANZ_CNBTVA_CARASS
        //                ,ANZ_CNBTVA_CARAZI
        //                ,ANZ_SRVZ
        //                ,IMP_CNBT_NDED_K1
        //                ,IMP_CNBT_NDED_K2
        //                ,IMP_CNBT_NDED_K3
        //                ,IMP_CNBT_AZ_K3
        //                ,IMP_CNBT_ISC_K3
        //                ,IMP_CNBT_TFR_K3
        //                ,IMP_CNBT_VOL_K3
        //                ,MATU_K3
        //                ,IMPB_252_ANTIC
        //                ,IMPST_252_ANTIC
        //                ,DT_1A_CNBZ
        //                ,COMMIS_DI_TRASFE
        //                ,AA_CNBZ_K1
        //                ,AA_CNBZ_K2
        //                ,AA_CNBZ_K3
        //                ,MM_CNBZ_K1
        //                ,MM_CNBZ_K2
        //                ,MM_CNBZ_K3
        //                ,FL_APPLZ_NEWFIS
        //                ,REDT_TASS_ABBAT_K3
        //                ,CNBT_ECC_4X100_K1
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,CREDITO_IS
        //                ,REDT_TASS_ABBAT_K2
        //                ,IMPB_IS_K3
        //                ,IMPST_SOST_K3
        //                ,IMPB_252_K3
        //                ,IMPST_252_K3
        //                ,IMPB_IS_K3_ANTIC
        //                ,IMPST_SOST_K3_ANTI
        //                ,IMPB_IRPEF_K1_ANTI
        //                ,IMPST_IRPEF_K1_ANT
        //                ,IMPB_IRPEF_K2_ANTI
        //                ,IMPST_IRPEF_K2_ANT
        //                ,DT_CESSAZIONE
        //                ,TOT_IMPST
        //                ,ONER_TRASFE
        //                ,IMP_NET_TRASFERITO
        //             INTO
        //                :DFA-ID-D-FISC-ADES
        //               ,:DFA-ID-ADES
        //               ,:DFA-ID-MOVI-CRZ
        //               ,:DFA-ID-MOVI-CHIU
        //                :IND-DFA-ID-MOVI-CHIU
        //               ,:DFA-DT-INI-EFF-DB
        //               ,:DFA-DT-END-EFF-DB
        //               ,:DFA-COD-COMP-ANIA
        //               ,:DFA-TP-ISC-FND
        //                :IND-DFA-TP-ISC-FND
        //               ,:DFA-DT-ACCNS-RAPP-FND-DB
        //                :IND-DFA-DT-ACCNS-RAPP-FND
        //               ,:DFA-IMP-CNBT-AZ-K1
        //                :IND-DFA-IMP-CNBT-AZ-K1
        //               ,:DFA-IMP-CNBT-ISC-K1
        //                :IND-DFA-IMP-CNBT-ISC-K1
        //               ,:DFA-IMP-CNBT-TFR-K1
        //                :IND-DFA-IMP-CNBT-TFR-K1
        //               ,:DFA-IMP-CNBT-VOL-K1
        //                :IND-DFA-IMP-CNBT-VOL-K1
        //               ,:DFA-IMP-CNBT-AZ-K2
        //                :IND-DFA-IMP-CNBT-AZ-K2
        //               ,:DFA-IMP-CNBT-ISC-K2
        //                :IND-DFA-IMP-CNBT-ISC-K2
        //               ,:DFA-IMP-CNBT-TFR-K2
        //                :IND-DFA-IMP-CNBT-TFR-K2
        //               ,:DFA-IMP-CNBT-VOL-K2
        //                :IND-DFA-IMP-CNBT-VOL-K2
        //               ,:DFA-MATU-K1
        //                :IND-DFA-MATU-K1
        //               ,:DFA-MATU-RES-K1
        //                :IND-DFA-MATU-RES-K1
        //               ,:DFA-MATU-K2
        //                :IND-DFA-MATU-K2
        //               ,:DFA-IMPB-VIS
        //                :IND-DFA-IMPB-VIS
        //               ,:DFA-IMPST-VIS
        //                :IND-DFA-IMPST-VIS
        //               ,:DFA-IMPB-IS-K2
        //                :IND-DFA-IMPB-IS-K2
        //               ,:DFA-IMPST-SOST-K2
        //                :IND-DFA-IMPST-SOST-K2
        //               ,:DFA-RIDZ-TFR
        //                :IND-DFA-RIDZ-TFR
        //               ,:DFA-PC-TFR
        //                :IND-DFA-PC-TFR
        //               ,:DFA-ALQ-TFR
        //                :IND-DFA-ALQ-TFR
        //               ,:DFA-TOT-ANTIC
        //                :IND-DFA-TOT-ANTIC
        //               ,:DFA-IMPB-TFR-ANTIC
        //                :IND-DFA-IMPB-TFR-ANTIC
        //               ,:DFA-IMPST-TFR-ANTIC
        //                :IND-DFA-IMPST-TFR-ANTIC
        //               ,:DFA-RIDZ-TFR-SU-ANTIC
        //                :IND-DFA-RIDZ-TFR-SU-ANTIC
        //               ,:DFA-IMPB-VIS-ANTIC
        //                :IND-DFA-IMPB-VIS-ANTIC
        //               ,:DFA-IMPST-VIS-ANTIC
        //                :IND-DFA-IMPST-VIS-ANTIC
        //               ,:DFA-IMPB-IS-K2-ANTIC
        //                :IND-DFA-IMPB-IS-K2-ANTIC
        //               ,:DFA-IMPST-SOST-K2-ANTI
        //                :IND-DFA-IMPST-SOST-K2-ANTI
        //               ,:DFA-ULT-COMMIS-TRASFE
        //                :IND-DFA-ULT-COMMIS-TRASFE
        //               ,:DFA-COD-DVS
        //                :IND-DFA-COD-DVS
        //               ,:DFA-ALQ-PRVR
        //                :IND-DFA-ALQ-PRVR
        //               ,:DFA-PC-ESE-IMPST-TFR
        //                :IND-DFA-PC-ESE-IMPST-TFR
        //               ,:DFA-IMP-ESE-IMPST-TFR
        //                :IND-DFA-IMP-ESE-IMPST-TFR
        //               ,:DFA-ANZ-CNBTVA-CARASS
        //                :IND-DFA-ANZ-CNBTVA-CARASS
        //               ,:DFA-ANZ-CNBTVA-CARAZI
        //                :IND-DFA-ANZ-CNBTVA-CARAZI
        //               ,:DFA-ANZ-SRVZ
        //                :IND-DFA-ANZ-SRVZ
        //               ,:DFA-IMP-CNBT-NDED-K1
        //                :IND-DFA-IMP-CNBT-NDED-K1
        //               ,:DFA-IMP-CNBT-NDED-K2
        //                :IND-DFA-IMP-CNBT-NDED-K2
        //               ,:DFA-IMP-CNBT-NDED-K3
        //                :IND-DFA-IMP-CNBT-NDED-K3
        //               ,:DFA-IMP-CNBT-AZ-K3
        //                :IND-DFA-IMP-CNBT-AZ-K3
        //               ,:DFA-IMP-CNBT-ISC-K3
        //                :IND-DFA-IMP-CNBT-ISC-K3
        //               ,:DFA-IMP-CNBT-TFR-K3
        //                :IND-DFA-IMP-CNBT-TFR-K3
        //               ,:DFA-IMP-CNBT-VOL-K3
        //                :IND-DFA-IMP-CNBT-VOL-K3
        //               ,:DFA-MATU-K3
        //                :IND-DFA-MATU-K3
        //               ,:DFA-IMPB-252-ANTIC
        //                :IND-DFA-IMPB-252-ANTIC
        //               ,:DFA-IMPST-252-ANTIC
        //                :IND-DFA-IMPST-252-ANTIC
        //               ,:DFA-DT-1A-CNBZ-DB
        //                :IND-DFA-DT-1A-CNBZ
        //               ,:DFA-COMMIS-DI-TRASFE
        //                :IND-DFA-COMMIS-DI-TRASFE
        //               ,:DFA-AA-CNBZ-K1
        //                :IND-DFA-AA-CNBZ-K1
        //               ,:DFA-AA-CNBZ-K2
        //                :IND-DFA-AA-CNBZ-K2
        //               ,:DFA-AA-CNBZ-K3
        //                :IND-DFA-AA-CNBZ-K3
        //               ,:DFA-MM-CNBZ-K1
        //                :IND-DFA-MM-CNBZ-K1
        //               ,:DFA-MM-CNBZ-K2
        //                :IND-DFA-MM-CNBZ-K2
        //               ,:DFA-MM-CNBZ-K3
        //                :IND-DFA-MM-CNBZ-K3
        //               ,:DFA-FL-APPLZ-NEWFIS
        //                :IND-DFA-FL-APPLZ-NEWFIS
        //               ,:DFA-REDT-TASS-ABBAT-K3
        //                :IND-DFA-REDT-TASS-ABBAT-K3
        //               ,:DFA-CNBT-ECC-4X100-K1
        //                :IND-DFA-CNBT-ECC-4X100-K1
        //               ,:DFA-DS-RIGA
        //               ,:DFA-DS-OPER-SQL
        //               ,:DFA-DS-VER
        //               ,:DFA-DS-TS-INI-CPTZ
        //               ,:DFA-DS-TS-END-CPTZ
        //               ,:DFA-DS-UTENTE
        //               ,:DFA-DS-STATO-ELAB
        //               ,:DFA-CREDITO-IS
        //                :IND-DFA-CREDITO-IS
        //               ,:DFA-REDT-TASS-ABBAT-K2
        //                :IND-DFA-REDT-TASS-ABBAT-K2
        //               ,:DFA-IMPB-IS-K3
        //                :IND-DFA-IMPB-IS-K3
        //               ,:DFA-IMPST-SOST-K3
        //                :IND-DFA-IMPST-SOST-K3
        //               ,:DFA-IMPB-252-K3
        //                :IND-DFA-IMPB-252-K3
        //               ,:DFA-IMPST-252-K3
        //                :IND-DFA-IMPST-252-K3
        //               ,:DFA-IMPB-IS-K3-ANTIC
        //                :IND-DFA-IMPB-IS-K3-ANTIC
        //               ,:DFA-IMPST-SOST-K3-ANTI
        //                :IND-DFA-IMPST-SOST-K3-ANTI
        //               ,:DFA-IMPB-IRPEF-K1-ANTI
        //                :IND-DFA-IMPB-IRPEF-K1-ANTI
        //               ,:DFA-IMPST-IRPEF-K1-ANT
        //                :IND-DFA-IMPST-IRPEF-K1-ANT
        //               ,:DFA-IMPB-IRPEF-K2-ANTI
        //                :IND-DFA-IMPB-IRPEF-K2-ANTI
        //               ,:DFA-IMPST-IRPEF-K2-ANT
        //                :IND-DFA-IMPST-IRPEF-K2-ANT
        //               ,:DFA-DT-CESSAZIONE-DB
        //                :IND-DFA-DT-CESSAZIONE
        //               ,:DFA-TOT-IMPST
        //                :IND-DFA-TOT-IMPST
        //               ,:DFA-ONER-TRASFE
        //                :IND-DFA-ONER-TRASFE
        //               ,:DFA-IMP-NET-TRASFERITO
        //                :IND-DFA-IMP-NET-TRASFERITO
        //             FROM D_FISC_ADES
        //             WHERE     ID_ADES = :DFA-ID-ADES
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        dFiscAdesDao.selectRec1(dFiscAdes.getDfaIdAdes(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A460-OPEN-CURSOR-IDP-EFF<br>*/
    private void a460OpenCursorIdpEff() {
        // COB_CODE: PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.
        a405DeclareCursorIdpEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-IDP-EFF-DFA
        //           END-EXEC.
        dFiscAdesDao.openCIdpEffDfa(dFiscAdes.getDfaIdAdes(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A470-CLOSE-CURSOR-IDP-EFF<br>*/
    private void a470CloseCursorIdpEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IDP-EFF-DFA
        //           END-EXEC.
        dFiscAdesDao.closeCIdpEffDfa();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A480-FETCH-FIRST-IDP-EFF<br>*/
    private void a480FetchFirstIdpEff() {
        // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.
        a460OpenCursorIdpEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
            a490FetchNextIdpEff();
        }
    }

    /**Original name: A490-FETCH-NEXT-IDP-EFF<br>*/
    private void a490FetchNextIdpEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IDP-EFF-DFA
        //           INTO
        //                :DFA-ID-D-FISC-ADES
        //               ,:DFA-ID-ADES
        //               ,:DFA-ID-MOVI-CRZ
        //               ,:DFA-ID-MOVI-CHIU
        //                :IND-DFA-ID-MOVI-CHIU
        //               ,:DFA-DT-INI-EFF-DB
        //               ,:DFA-DT-END-EFF-DB
        //               ,:DFA-COD-COMP-ANIA
        //               ,:DFA-TP-ISC-FND
        //                :IND-DFA-TP-ISC-FND
        //               ,:DFA-DT-ACCNS-RAPP-FND-DB
        //                :IND-DFA-DT-ACCNS-RAPP-FND
        //               ,:DFA-IMP-CNBT-AZ-K1
        //                :IND-DFA-IMP-CNBT-AZ-K1
        //               ,:DFA-IMP-CNBT-ISC-K1
        //                :IND-DFA-IMP-CNBT-ISC-K1
        //               ,:DFA-IMP-CNBT-TFR-K1
        //                :IND-DFA-IMP-CNBT-TFR-K1
        //               ,:DFA-IMP-CNBT-VOL-K1
        //                :IND-DFA-IMP-CNBT-VOL-K1
        //               ,:DFA-IMP-CNBT-AZ-K2
        //                :IND-DFA-IMP-CNBT-AZ-K2
        //               ,:DFA-IMP-CNBT-ISC-K2
        //                :IND-DFA-IMP-CNBT-ISC-K2
        //               ,:DFA-IMP-CNBT-TFR-K2
        //                :IND-DFA-IMP-CNBT-TFR-K2
        //               ,:DFA-IMP-CNBT-VOL-K2
        //                :IND-DFA-IMP-CNBT-VOL-K2
        //               ,:DFA-MATU-K1
        //                :IND-DFA-MATU-K1
        //               ,:DFA-MATU-RES-K1
        //                :IND-DFA-MATU-RES-K1
        //               ,:DFA-MATU-K2
        //                :IND-DFA-MATU-K2
        //               ,:DFA-IMPB-VIS
        //                :IND-DFA-IMPB-VIS
        //               ,:DFA-IMPST-VIS
        //                :IND-DFA-IMPST-VIS
        //               ,:DFA-IMPB-IS-K2
        //                :IND-DFA-IMPB-IS-K2
        //               ,:DFA-IMPST-SOST-K2
        //                :IND-DFA-IMPST-SOST-K2
        //               ,:DFA-RIDZ-TFR
        //                :IND-DFA-RIDZ-TFR
        //               ,:DFA-PC-TFR
        //                :IND-DFA-PC-TFR
        //               ,:DFA-ALQ-TFR
        //                :IND-DFA-ALQ-TFR
        //               ,:DFA-TOT-ANTIC
        //                :IND-DFA-TOT-ANTIC
        //               ,:DFA-IMPB-TFR-ANTIC
        //                :IND-DFA-IMPB-TFR-ANTIC
        //               ,:DFA-IMPST-TFR-ANTIC
        //                :IND-DFA-IMPST-TFR-ANTIC
        //               ,:DFA-RIDZ-TFR-SU-ANTIC
        //                :IND-DFA-RIDZ-TFR-SU-ANTIC
        //               ,:DFA-IMPB-VIS-ANTIC
        //                :IND-DFA-IMPB-VIS-ANTIC
        //               ,:DFA-IMPST-VIS-ANTIC
        //                :IND-DFA-IMPST-VIS-ANTIC
        //               ,:DFA-IMPB-IS-K2-ANTIC
        //                :IND-DFA-IMPB-IS-K2-ANTIC
        //               ,:DFA-IMPST-SOST-K2-ANTI
        //                :IND-DFA-IMPST-SOST-K2-ANTI
        //               ,:DFA-ULT-COMMIS-TRASFE
        //                :IND-DFA-ULT-COMMIS-TRASFE
        //               ,:DFA-COD-DVS
        //                :IND-DFA-COD-DVS
        //               ,:DFA-ALQ-PRVR
        //                :IND-DFA-ALQ-PRVR
        //               ,:DFA-PC-ESE-IMPST-TFR
        //                :IND-DFA-PC-ESE-IMPST-TFR
        //               ,:DFA-IMP-ESE-IMPST-TFR
        //                :IND-DFA-IMP-ESE-IMPST-TFR
        //               ,:DFA-ANZ-CNBTVA-CARASS
        //                :IND-DFA-ANZ-CNBTVA-CARASS
        //               ,:DFA-ANZ-CNBTVA-CARAZI
        //                :IND-DFA-ANZ-CNBTVA-CARAZI
        //               ,:DFA-ANZ-SRVZ
        //                :IND-DFA-ANZ-SRVZ
        //               ,:DFA-IMP-CNBT-NDED-K1
        //                :IND-DFA-IMP-CNBT-NDED-K1
        //               ,:DFA-IMP-CNBT-NDED-K2
        //                :IND-DFA-IMP-CNBT-NDED-K2
        //               ,:DFA-IMP-CNBT-NDED-K3
        //                :IND-DFA-IMP-CNBT-NDED-K3
        //               ,:DFA-IMP-CNBT-AZ-K3
        //                :IND-DFA-IMP-CNBT-AZ-K3
        //               ,:DFA-IMP-CNBT-ISC-K3
        //                :IND-DFA-IMP-CNBT-ISC-K3
        //               ,:DFA-IMP-CNBT-TFR-K3
        //                :IND-DFA-IMP-CNBT-TFR-K3
        //               ,:DFA-IMP-CNBT-VOL-K3
        //                :IND-DFA-IMP-CNBT-VOL-K3
        //               ,:DFA-MATU-K3
        //                :IND-DFA-MATU-K3
        //               ,:DFA-IMPB-252-ANTIC
        //                :IND-DFA-IMPB-252-ANTIC
        //               ,:DFA-IMPST-252-ANTIC
        //                :IND-DFA-IMPST-252-ANTIC
        //               ,:DFA-DT-1A-CNBZ-DB
        //                :IND-DFA-DT-1A-CNBZ
        //               ,:DFA-COMMIS-DI-TRASFE
        //                :IND-DFA-COMMIS-DI-TRASFE
        //               ,:DFA-AA-CNBZ-K1
        //                :IND-DFA-AA-CNBZ-K1
        //               ,:DFA-AA-CNBZ-K2
        //                :IND-DFA-AA-CNBZ-K2
        //               ,:DFA-AA-CNBZ-K3
        //                :IND-DFA-AA-CNBZ-K3
        //               ,:DFA-MM-CNBZ-K1
        //                :IND-DFA-MM-CNBZ-K1
        //               ,:DFA-MM-CNBZ-K2
        //                :IND-DFA-MM-CNBZ-K2
        //               ,:DFA-MM-CNBZ-K3
        //                :IND-DFA-MM-CNBZ-K3
        //               ,:DFA-FL-APPLZ-NEWFIS
        //                :IND-DFA-FL-APPLZ-NEWFIS
        //               ,:DFA-REDT-TASS-ABBAT-K3
        //                :IND-DFA-REDT-TASS-ABBAT-K3
        //               ,:DFA-CNBT-ECC-4X100-K1
        //                :IND-DFA-CNBT-ECC-4X100-K1
        //               ,:DFA-DS-RIGA
        //               ,:DFA-DS-OPER-SQL
        //               ,:DFA-DS-VER
        //               ,:DFA-DS-TS-INI-CPTZ
        //               ,:DFA-DS-TS-END-CPTZ
        //               ,:DFA-DS-UTENTE
        //               ,:DFA-DS-STATO-ELAB
        //               ,:DFA-CREDITO-IS
        //                :IND-DFA-CREDITO-IS
        //               ,:DFA-REDT-TASS-ABBAT-K2
        //                :IND-DFA-REDT-TASS-ABBAT-K2
        //               ,:DFA-IMPB-IS-K3
        //                :IND-DFA-IMPB-IS-K3
        //               ,:DFA-IMPST-SOST-K3
        //                :IND-DFA-IMPST-SOST-K3
        //               ,:DFA-IMPB-252-K3
        //                :IND-DFA-IMPB-252-K3
        //               ,:DFA-IMPST-252-K3
        //                :IND-DFA-IMPST-252-K3
        //               ,:DFA-IMPB-IS-K3-ANTIC
        //                :IND-DFA-IMPB-IS-K3-ANTIC
        //               ,:DFA-IMPST-SOST-K3-ANTI
        //                :IND-DFA-IMPST-SOST-K3-ANTI
        //               ,:DFA-IMPB-IRPEF-K1-ANTI
        //                :IND-DFA-IMPB-IRPEF-K1-ANTI
        //               ,:DFA-IMPST-IRPEF-K1-ANT
        //                :IND-DFA-IMPST-IRPEF-K1-ANT
        //               ,:DFA-IMPB-IRPEF-K2-ANTI
        //                :IND-DFA-IMPB-IRPEF-K2-ANTI
        //               ,:DFA-IMPST-IRPEF-K2-ANT
        //                :IND-DFA-IMPST-IRPEF-K2-ANT
        //               ,:DFA-DT-CESSAZIONE-DB
        //                :IND-DFA-DT-CESSAZIONE
        //               ,:DFA-TOT-IMPST
        //                :IND-DFA-TOT-IMPST
        //               ,:DFA-ONER-TRASFE
        //                :IND-DFA-ONER-TRASFE
        //               ,:DFA-IMP-NET-TRASFERITO
        //                :IND-DFA-IMP-NET-TRASFERITO
        //           END-EXEC.
        dFiscAdesDao.fetchCIdpEffDfa(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
            a470CloseCursorIdpEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A505-DECLARE-CURSOR-IBO<br>
	 * <pre>----
	 * ----  gestione IBO Effetto e non
	 * ----</pre>*/
    private void a505DeclareCursorIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A510-SELECT-IBO<br>*/
    private void a510SelectIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A560-OPEN-CURSOR-IBO<br>*/
    private void a560OpenCursorIbo() {
        // COB_CODE: PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.
        a505DeclareCursorIbo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A570-CLOSE-CURSOR-IBO<br>*/
    private void a570CloseCursorIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A580-FETCH-FIRST-IBO<br>*/
    private void a580FetchFirstIbo() {
        // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.
        a560OpenCursorIbo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
            a590FetchNextIbo();
        }
    }

    /**Original name: A590-FETCH-NEXT-IBO<br>*/
    private void a590FetchNextIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A605-DECLARE-CURSOR-IBS<br>
	 * <pre>----
	 * ----  gestione IBS Effetto e non
	 * ----</pre>*/
    private void a605DeclareCursorIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A610-SELECT-IBS<br>*/
    private void a610SelectIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A660-OPEN-CURSOR-IBS<br>*/
    private void a660OpenCursorIbs() {
        // COB_CODE: PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.
        a605DeclareCursorIbs();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A670-CLOSE-CURSOR-IBS<br>*/
    private void a670CloseCursorIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A680-FETCH-FIRST-IBS<br>*/
    private void a680FetchFirstIbs() {
        // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.
        a660OpenCursorIbs();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
            a690FetchNextIbs();
        }
    }

    /**Original name: A690-FETCH-NEXT-IBS<br>*/
    private void a690FetchNextIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A705-DECLARE-CURSOR-IDO<br>
	 * <pre>----
	 * ----  gestione IDO Effetto e non
	 * ----</pre>*/
    private void a705DeclareCursorIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A710-SELECT-IDO<br>*/
    private void a710SelectIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A760-OPEN-CURSOR-IDO<br>*/
    private void a760OpenCursorIdo() {
        // COB_CODE: PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.
        a705DeclareCursorIdo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A770-CLOSE-CURSOR-IDO<br>*/
    private void a770CloseCursorIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A780-FETCH-FIRST-IDO<br>*/
    private void a780FetchFirstIdo() {
        // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.
        a760OpenCursorIdo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
            a790FetchNextIdo();
        }
    }

    /**Original name: A790-FETCH-NEXT-IDO<br>*/
    private void a790FetchNextIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B310-SELECT-ID-CPZ<br>
	 * <pre>----
	 * ----  gestione ID Competenza
	 * ----</pre>*/
    private void b310SelectIdCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_D_FISC_ADES
        //                ,ID_ADES
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,TP_ISC_FND
        //                ,DT_ACCNS_RAPP_FND
        //                ,IMP_CNBT_AZ_K1
        //                ,IMP_CNBT_ISC_K1
        //                ,IMP_CNBT_TFR_K1
        //                ,IMP_CNBT_VOL_K1
        //                ,IMP_CNBT_AZ_K2
        //                ,IMP_CNBT_ISC_K2
        //                ,IMP_CNBT_TFR_K2
        //                ,IMP_CNBT_VOL_K2
        //                ,MATU_K1
        //                ,MATU_RES_K1
        //                ,MATU_K2
        //                ,IMPB_VIS
        //                ,IMPST_VIS
        //                ,IMPB_IS_K2
        //                ,IMPST_SOST_K2
        //                ,RIDZ_TFR
        //                ,PC_TFR
        //                ,ALQ_TFR
        //                ,TOT_ANTIC
        //                ,IMPB_TFR_ANTIC
        //                ,IMPST_TFR_ANTIC
        //                ,RIDZ_TFR_SU_ANTIC
        //                ,IMPB_VIS_ANTIC
        //                ,IMPST_VIS_ANTIC
        //                ,IMPB_IS_K2_ANTIC
        //                ,IMPST_SOST_K2_ANTI
        //                ,ULT_COMMIS_TRASFE
        //                ,COD_DVS
        //                ,ALQ_PRVR
        //                ,PC_ESE_IMPST_TFR
        //                ,IMP_ESE_IMPST_TFR
        //                ,ANZ_CNBTVA_CARASS
        //                ,ANZ_CNBTVA_CARAZI
        //                ,ANZ_SRVZ
        //                ,IMP_CNBT_NDED_K1
        //                ,IMP_CNBT_NDED_K2
        //                ,IMP_CNBT_NDED_K3
        //                ,IMP_CNBT_AZ_K3
        //                ,IMP_CNBT_ISC_K3
        //                ,IMP_CNBT_TFR_K3
        //                ,IMP_CNBT_VOL_K3
        //                ,MATU_K3
        //                ,IMPB_252_ANTIC
        //                ,IMPST_252_ANTIC
        //                ,DT_1A_CNBZ
        //                ,COMMIS_DI_TRASFE
        //                ,AA_CNBZ_K1
        //                ,AA_CNBZ_K2
        //                ,AA_CNBZ_K3
        //                ,MM_CNBZ_K1
        //                ,MM_CNBZ_K2
        //                ,MM_CNBZ_K3
        //                ,FL_APPLZ_NEWFIS
        //                ,REDT_TASS_ABBAT_K3
        //                ,CNBT_ECC_4X100_K1
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,CREDITO_IS
        //                ,REDT_TASS_ABBAT_K2
        //                ,IMPB_IS_K3
        //                ,IMPST_SOST_K3
        //                ,IMPB_252_K3
        //                ,IMPST_252_K3
        //                ,IMPB_IS_K3_ANTIC
        //                ,IMPST_SOST_K3_ANTI
        //                ,IMPB_IRPEF_K1_ANTI
        //                ,IMPST_IRPEF_K1_ANT
        //                ,IMPB_IRPEF_K2_ANTI
        //                ,IMPST_IRPEF_K2_ANT
        //                ,DT_CESSAZIONE
        //                ,TOT_IMPST
        //                ,ONER_TRASFE
        //                ,IMP_NET_TRASFERITO
        //             INTO
        //                :DFA-ID-D-FISC-ADES
        //               ,:DFA-ID-ADES
        //               ,:DFA-ID-MOVI-CRZ
        //               ,:DFA-ID-MOVI-CHIU
        //                :IND-DFA-ID-MOVI-CHIU
        //               ,:DFA-DT-INI-EFF-DB
        //               ,:DFA-DT-END-EFF-DB
        //               ,:DFA-COD-COMP-ANIA
        //               ,:DFA-TP-ISC-FND
        //                :IND-DFA-TP-ISC-FND
        //               ,:DFA-DT-ACCNS-RAPP-FND-DB
        //                :IND-DFA-DT-ACCNS-RAPP-FND
        //               ,:DFA-IMP-CNBT-AZ-K1
        //                :IND-DFA-IMP-CNBT-AZ-K1
        //               ,:DFA-IMP-CNBT-ISC-K1
        //                :IND-DFA-IMP-CNBT-ISC-K1
        //               ,:DFA-IMP-CNBT-TFR-K1
        //                :IND-DFA-IMP-CNBT-TFR-K1
        //               ,:DFA-IMP-CNBT-VOL-K1
        //                :IND-DFA-IMP-CNBT-VOL-K1
        //               ,:DFA-IMP-CNBT-AZ-K2
        //                :IND-DFA-IMP-CNBT-AZ-K2
        //               ,:DFA-IMP-CNBT-ISC-K2
        //                :IND-DFA-IMP-CNBT-ISC-K2
        //               ,:DFA-IMP-CNBT-TFR-K2
        //                :IND-DFA-IMP-CNBT-TFR-K2
        //               ,:DFA-IMP-CNBT-VOL-K2
        //                :IND-DFA-IMP-CNBT-VOL-K2
        //               ,:DFA-MATU-K1
        //                :IND-DFA-MATU-K1
        //               ,:DFA-MATU-RES-K1
        //                :IND-DFA-MATU-RES-K1
        //               ,:DFA-MATU-K2
        //                :IND-DFA-MATU-K2
        //               ,:DFA-IMPB-VIS
        //                :IND-DFA-IMPB-VIS
        //               ,:DFA-IMPST-VIS
        //                :IND-DFA-IMPST-VIS
        //               ,:DFA-IMPB-IS-K2
        //                :IND-DFA-IMPB-IS-K2
        //               ,:DFA-IMPST-SOST-K2
        //                :IND-DFA-IMPST-SOST-K2
        //               ,:DFA-RIDZ-TFR
        //                :IND-DFA-RIDZ-TFR
        //               ,:DFA-PC-TFR
        //                :IND-DFA-PC-TFR
        //               ,:DFA-ALQ-TFR
        //                :IND-DFA-ALQ-TFR
        //               ,:DFA-TOT-ANTIC
        //                :IND-DFA-TOT-ANTIC
        //               ,:DFA-IMPB-TFR-ANTIC
        //                :IND-DFA-IMPB-TFR-ANTIC
        //               ,:DFA-IMPST-TFR-ANTIC
        //                :IND-DFA-IMPST-TFR-ANTIC
        //               ,:DFA-RIDZ-TFR-SU-ANTIC
        //                :IND-DFA-RIDZ-TFR-SU-ANTIC
        //               ,:DFA-IMPB-VIS-ANTIC
        //                :IND-DFA-IMPB-VIS-ANTIC
        //               ,:DFA-IMPST-VIS-ANTIC
        //                :IND-DFA-IMPST-VIS-ANTIC
        //               ,:DFA-IMPB-IS-K2-ANTIC
        //                :IND-DFA-IMPB-IS-K2-ANTIC
        //               ,:DFA-IMPST-SOST-K2-ANTI
        //                :IND-DFA-IMPST-SOST-K2-ANTI
        //               ,:DFA-ULT-COMMIS-TRASFE
        //                :IND-DFA-ULT-COMMIS-TRASFE
        //               ,:DFA-COD-DVS
        //                :IND-DFA-COD-DVS
        //               ,:DFA-ALQ-PRVR
        //                :IND-DFA-ALQ-PRVR
        //               ,:DFA-PC-ESE-IMPST-TFR
        //                :IND-DFA-PC-ESE-IMPST-TFR
        //               ,:DFA-IMP-ESE-IMPST-TFR
        //                :IND-DFA-IMP-ESE-IMPST-TFR
        //               ,:DFA-ANZ-CNBTVA-CARASS
        //                :IND-DFA-ANZ-CNBTVA-CARASS
        //               ,:DFA-ANZ-CNBTVA-CARAZI
        //                :IND-DFA-ANZ-CNBTVA-CARAZI
        //               ,:DFA-ANZ-SRVZ
        //                :IND-DFA-ANZ-SRVZ
        //               ,:DFA-IMP-CNBT-NDED-K1
        //                :IND-DFA-IMP-CNBT-NDED-K1
        //               ,:DFA-IMP-CNBT-NDED-K2
        //                :IND-DFA-IMP-CNBT-NDED-K2
        //               ,:DFA-IMP-CNBT-NDED-K3
        //                :IND-DFA-IMP-CNBT-NDED-K3
        //               ,:DFA-IMP-CNBT-AZ-K3
        //                :IND-DFA-IMP-CNBT-AZ-K3
        //               ,:DFA-IMP-CNBT-ISC-K3
        //                :IND-DFA-IMP-CNBT-ISC-K3
        //               ,:DFA-IMP-CNBT-TFR-K3
        //                :IND-DFA-IMP-CNBT-TFR-K3
        //               ,:DFA-IMP-CNBT-VOL-K3
        //                :IND-DFA-IMP-CNBT-VOL-K3
        //               ,:DFA-MATU-K3
        //                :IND-DFA-MATU-K3
        //               ,:DFA-IMPB-252-ANTIC
        //                :IND-DFA-IMPB-252-ANTIC
        //               ,:DFA-IMPST-252-ANTIC
        //                :IND-DFA-IMPST-252-ANTIC
        //               ,:DFA-DT-1A-CNBZ-DB
        //                :IND-DFA-DT-1A-CNBZ
        //               ,:DFA-COMMIS-DI-TRASFE
        //                :IND-DFA-COMMIS-DI-TRASFE
        //               ,:DFA-AA-CNBZ-K1
        //                :IND-DFA-AA-CNBZ-K1
        //               ,:DFA-AA-CNBZ-K2
        //                :IND-DFA-AA-CNBZ-K2
        //               ,:DFA-AA-CNBZ-K3
        //                :IND-DFA-AA-CNBZ-K3
        //               ,:DFA-MM-CNBZ-K1
        //                :IND-DFA-MM-CNBZ-K1
        //               ,:DFA-MM-CNBZ-K2
        //                :IND-DFA-MM-CNBZ-K2
        //               ,:DFA-MM-CNBZ-K3
        //                :IND-DFA-MM-CNBZ-K3
        //               ,:DFA-FL-APPLZ-NEWFIS
        //                :IND-DFA-FL-APPLZ-NEWFIS
        //               ,:DFA-REDT-TASS-ABBAT-K3
        //                :IND-DFA-REDT-TASS-ABBAT-K3
        //               ,:DFA-CNBT-ECC-4X100-K1
        //                :IND-DFA-CNBT-ECC-4X100-K1
        //               ,:DFA-DS-RIGA
        //               ,:DFA-DS-OPER-SQL
        //               ,:DFA-DS-VER
        //               ,:DFA-DS-TS-INI-CPTZ
        //               ,:DFA-DS-TS-END-CPTZ
        //               ,:DFA-DS-UTENTE
        //               ,:DFA-DS-STATO-ELAB
        //               ,:DFA-CREDITO-IS
        //                :IND-DFA-CREDITO-IS
        //               ,:DFA-REDT-TASS-ABBAT-K2
        //                :IND-DFA-REDT-TASS-ABBAT-K2
        //               ,:DFA-IMPB-IS-K3
        //                :IND-DFA-IMPB-IS-K3
        //               ,:DFA-IMPST-SOST-K3
        //                :IND-DFA-IMPST-SOST-K3
        //               ,:DFA-IMPB-252-K3
        //                :IND-DFA-IMPB-252-K3
        //               ,:DFA-IMPST-252-K3
        //                :IND-DFA-IMPST-252-K3
        //               ,:DFA-IMPB-IS-K3-ANTIC
        //                :IND-DFA-IMPB-IS-K3-ANTIC
        //               ,:DFA-IMPST-SOST-K3-ANTI
        //                :IND-DFA-IMPST-SOST-K3-ANTI
        //               ,:DFA-IMPB-IRPEF-K1-ANTI
        //                :IND-DFA-IMPB-IRPEF-K1-ANTI
        //               ,:DFA-IMPST-IRPEF-K1-ANT
        //                :IND-DFA-IMPST-IRPEF-K1-ANT
        //               ,:DFA-IMPB-IRPEF-K2-ANTI
        //                :IND-DFA-IMPB-IRPEF-K2-ANTI
        //               ,:DFA-IMPST-IRPEF-K2-ANT
        //                :IND-DFA-IMPST-IRPEF-K2-ANT
        //               ,:DFA-DT-CESSAZIONE-DB
        //                :IND-DFA-DT-CESSAZIONE
        //               ,:DFA-TOT-IMPST
        //                :IND-DFA-TOT-IMPST
        //               ,:DFA-ONER-TRASFE
        //                :IND-DFA-ONER-TRASFE
        //               ,:DFA-IMP-NET-TRASFERITO
        //                :IND-DFA-IMP-NET-TRASFERITO
        //             FROM D_FISC_ADES
        //             WHERE     ID_D_FISC_ADES = :DFA-ID-D-FISC-ADES
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        dFiscAdesDao.selectRec2(dFiscAdes.getDfaIdDFiscAdes(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B405-DECLARE-CURSOR-IDP-CPZ<br>
	 * <pre>----
	 * ----  gestione IDP Competenza
	 * ----</pre>*/
    private void b405DeclareCursorIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IDP-CPZ-DFA CURSOR FOR
        //              SELECT
        //                     ID_D_FISC_ADES
        //                    ,ID_ADES
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,TP_ISC_FND
        //                    ,DT_ACCNS_RAPP_FND
        //                    ,IMP_CNBT_AZ_K1
        //                    ,IMP_CNBT_ISC_K1
        //                    ,IMP_CNBT_TFR_K1
        //                    ,IMP_CNBT_VOL_K1
        //                    ,IMP_CNBT_AZ_K2
        //                    ,IMP_CNBT_ISC_K2
        //                    ,IMP_CNBT_TFR_K2
        //                    ,IMP_CNBT_VOL_K2
        //                    ,MATU_K1
        //                    ,MATU_RES_K1
        //                    ,MATU_K2
        //                    ,IMPB_VIS
        //                    ,IMPST_VIS
        //                    ,IMPB_IS_K2
        //                    ,IMPST_SOST_K2
        //                    ,RIDZ_TFR
        //                    ,PC_TFR
        //                    ,ALQ_TFR
        //                    ,TOT_ANTIC
        //                    ,IMPB_TFR_ANTIC
        //                    ,IMPST_TFR_ANTIC
        //                    ,RIDZ_TFR_SU_ANTIC
        //                    ,IMPB_VIS_ANTIC
        //                    ,IMPST_VIS_ANTIC
        //                    ,IMPB_IS_K2_ANTIC
        //                    ,IMPST_SOST_K2_ANTI
        //                    ,ULT_COMMIS_TRASFE
        //                    ,COD_DVS
        //                    ,ALQ_PRVR
        //                    ,PC_ESE_IMPST_TFR
        //                    ,IMP_ESE_IMPST_TFR
        //                    ,ANZ_CNBTVA_CARASS
        //                    ,ANZ_CNBTVA_CARAZI
        //                    ,ANZ_SRVZ
        //                    ,IMP_CNBT_NDED_K1
        //                    ,IMP_CNBT_NDED_K2
        //                    ,IMP_CNBT_NDED_K3
        //                    ,IMP_CNBT_AZ_K3
        //                    ,IMP_CNBT_ISC_K3
        //                    ,IMP_CNBT_TFR_K3
        //                    ,IMP_CNBT_VOL_K3
        //                    ,MATU_K3
        //                    ,IMPB_252_ANTIC
        //                    ,IMPST_252_ANTIC
        //                    ,DT_1A_CNBZ
        //                    ,COMMIS_DI_TRASFE
        //                    ,AA_CNBZ_K1
        //                    ,AA_CNBZ_K2
        //                    ,AA_CNBZ_K3
        //                    ,MM_CNBZ_K1
        //                    ,MM_CNBZ_K2
        //                    ,MM_CNBZ_K3
        //                    ,FL_APPLZ_NEWFIS
        //                    ,REDT_TASS_ABBAT_K3
        //                    ,CNBT_ECC_4X100_K1
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,CREDITO_IS
        //                    ,REDT_TASS_ABBAT_K2
        //                    ,IMPB_IS_K3
        //                    ,IMPST_SOST_K3
        //                    ,IMPB_252_K3
        //                    ,IMPST_252_K3
        //                    ,IMPB_IS_K3_ANTIC
        //                    ,IMPST_SOST_K3_ANTI
        //                    ,IMPB_IRPEF_K1_ANTI
        //                    ,IMPST_IRPEF_K1_ANT
        //                    ,IMPB_IRPEF_K2_ANTI
        //                    ,IMPST_IRPEF_K2_ANT
        //                    ,DT_CESSAZIONE
        //                    ,TOT_IMPST
        //                    ,ONER_TRASFE
        //                    ,IMP_NET_TRASFERITO
        //              FROM D_FISC_ADES
        //              WHERE     ID_ADES = :DFA-ID-ADES
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //              ORDER BY ID_D_FISC_ADES ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B410-SELECT-IDP-CPZ<br>*/
    private void b410SelectIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_D_FISC_ADES
        //                ,ID_ADES
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,TP_ISC_FND
        //                ,DT_ACCNS_RAPP_FND
        //                ,IMP_CNBT_AZ_K1
        //                ,IMP_CNBT_ISC_K1
        //                ,IMP_CNBT_TFR_K1
        //                ,IMP_CNBT_VOL_K1
        //                ,IMP_CNBT_AZ_K2
        //                ,IMP_CNBT_ISC_K2
        //                ,IMP_CNBT_TFR_K2
        //                ,IMP_CNBT_VOL_K2
        //                ,MATU_K1
        //                ,MATU_RES_K1
        //                ,MATU_K2
        //                ,IMPB_VIS
        //                ,IMPST_VIS
        //                ,IMPB_IS_K2
        //                ,IMPST_SOST_K2
        //                ,RIDZ_TFR
        //                ,PC_TFR
        //                ,ALQ_TFR
        //                ,TOT_ANTIC
        //                ,IMPB_TFR_ANTIC
        //                ,IMPST_TFR_ANTIC
        //                ,RIDZ_TFR_SU_ANTIC
        //                ,IMPB_VIS_ANTIC
        //                ,IMPST_VIS_ANTIC
        //                ,IMPB_IS_K2_ANTIC
        //                ,IMPST_SOST_K2_ANTI
        //                ,ULT_COMMIS_TRASFE
        //                ,COD_DVS
        //                ,ALQ_PRVR
        //                ,PC_ESE_IMPST_TFR
        //                ,IMP_ESE_IMPST_TFR
        //                ,ANZ_CNBTVA_CARASS
        //                ,ANZ_CNBTVA_CARAZI
        //                ,ANZ_SRVZ
        //                ,IMP_CNBT_NDED_K1
        //                ,IMP_CNBT_NDED_K2
        //                ,IMP_CNBT_NDED_K3
        //                ,IMP_CNBT_AZ_K3
        //                ,IMP_CNBT_ISC_K3
        //                ,IMP_CNBT_TFR_K3
        //                ,IMP_CNBT_VOL_K3
        //                ,MATU_K3
        //                ,IMPB_252_ANTIC
        //                ,IMPST_252_ANTIC
        //                ,DT_1A_CNBZ
        //                ,COMMIS_DI_TRASFE
        //                ,AA_CNBZ_K1
        //                ,AA_CNBZ_K2
        //                ,AA_CNBZ_K3
        //                ,MM_CNBZ_K1
        //                ,MM_CNBZ_K2
        //                ,MM_CNBZ_K3
        //                ,FL_APPLZ_NEWFIS
        //                ,REDT_TASS_ABBAT_K3
        //                ,CNBT_ECC_4X100_K1
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,CREDITO_IS
        //                ,REDT_TASS_ABBAT_K2
        //                ,IMPB_IS_K3
        //                ,IMPST_SOST_K3
        //                ,IMPB_252_K3
        //                ,IMPST_252_K3
        //                ,IMPB_IS_K3_ANTIC
        //                ,IMPST_SOST_K3_ANTI
        //                ,IMPB_IRPEF_K1_ANTI
        //                ,IMPST_IRPEF_K1_ANT
        //                ,IMPB_IRPEF_K2_ANTI
        //                ,IMPST_IRPEF_K2_ANT
        //                ,DT_CESSAZIONE
        //                ,TOT_IMPST
        //                ,ONER_TRASFE
        //                ,IMP_NET_TRASFERITO
        //             INTO
        //                :DFA-ID-D-FISC-ADES
        //               ,:DFA-ID-ADES
        //               ,:DFA-ID-MOVI-CRZ
        //               ,:DFA-ID-MOVI-CHIU
        //                :IND-DFA-ID-MOVI-CHIU
        //               ,:DFA-DT-INI-EFF-DB
        //               ,:DFA-DT-END-EFF-DB
        //               ,:DFA-COD-COMP-ANIA
        //               ,:DFA-TP-ISC-FND
        //                :IND-DFA-TP-ISC-FND
        //               ,:DFA-DT-ACCNS-RAPP-FND-DB
        //                :IND-DFA-DT-ACCNS-RAPP-FND
        //               ,:DFA-IMP-CNBT-AZ-K1
        //                :IND-DFA-IMP-CNBT-AZ-K1
        //               ,:DFA-IMP-CNBT-ISC-K1
        //                :IND-DFA-IMP-CNBT-ISC-K1
        //               ,:DFA-IMP-CNBT-TFR-K1
        //                :IND-DFA-IMP-CNBT-TFR-K1
        //               ,:DFA-IMP-CNBT-VOL-K1
        //                :IND-DFA-IMP-CNBT-VOL-K1
        //               ,:DFA-IMP-CNBT-AZ-K2
        //                :IND-DFA-IMP-CNBT-AZ-K2
        //               ,:DFA-IMP-CNBT-ISC-K2
        //                :IND-DFA-IMP-CNBT-ISC-K2
        //               ,:DFA-IMP-CNBT-TFR-K2
        //                :IND-DFA-IMP-CNBT-TFR-K2
        //               ,:DFA-IMP-CNBT-VOL-K2
        //                :IND-DFA-IMP-CNBT-VOL-K2
        //               ,:DFA-MATU-K1
        //                :IND-DFA-MATU-K1
        //               ,:DFA-MATU-RES-K1
        //                :IND-DFA-MATU-RES-K1
        //               ,:DFA-MATU-K2
        //                :IND-DFA-MATU-K2
        //               ,:DFA-IMPB-VIS
        //                :IND-DFA-IMPB-VIS
        //               ,:DFA-IMPST-VIS
        //                :IND-DFA-IMPST-VIS
        //               ,:DFA-IMPB-IS-K2
        //                :IND-DFA-IMPB-IS-K2
        //               ,:DFA-IMPST-SOST-K2
        //                :IND-DFA-IMPST-SOST-K2
        //               ,:DFA-RIDZ-TFR
        //                :IND-DFA-RIDZ-TFR
        //               ,:DFA-PC-TFR
        //                :IND-DFA-PC-TFR
        //               ,:DFA-ALQ-TFR
        //                :IND-DFA-ALQ-TFR
        //               ,:DFA-TOT-ANTIC
        //                :IND-DFA-TOT-ANTIC
        //               ,:DFA-IMPB-TFR-ANTIC
        //                :IND-DFA-IMPB-TFR-ANTIC
        //               ,:DFA-IMPST-TFR-ANTIC
        //                :IND-DFA-IMPST-TFR-ANTIC
        //               ,:DFA-RIDZ-TFR-SU-ANTIC
        //                :IND-DFA-RIDZ-TFR-SU-ANTIC
        //               ,:DFA-IMPB-VIS-ANTIC
        //                :IND-DFA-IMPB-VIS-ANTIC
        //               ,:DFA-IMPST-VIS-ANTIC
        //                :IND-DFA-IMPST-VIS-ANTIC
        //               ,:DFA-IMPB-IS-K2-ANTIC
        //                :IND-DFA-IMPB-IS-K2-ANTIC
        //               ,:DFA-IMPST-SOST-K2-ANTI
        //                :IND-DFA-IMPST-SOST-K2-ANTI
        //               ,:DFA-ULT-COMMIS-TRASFE
        //                :IND-DFA-ULT-COMMIS-TRASFE
        //               ,:DFA-COD-DVS
        //                :IND-DFA-COD-DVS
        //               ,:DFA-ALQ-PRVR
        //                :IND-DFA-ALQ-PRVR
        //               ,:DFA-PC-ESE-IMPST-TFR
        //                :IND-DFA-PC-ESE-IMPST-TFR
        //               ,:DFA-IMP-ESE-IMPST-TFR
        //                :IND-DFA-IMP-ESE-IMPST-TFR
        //               ,:DFA-ANZ-CNBTVA-CARASS
        //                :IND-DFA-ANZ-CNBTVA-CARASS
        //               ,:DFA-ANZ-CNBTVA-CARAZI
        //                :IND-DFA-ANZ-CNBTVA-CARAZI
        //               ,:DFA-ANZ-SRVZ
        //                :IND-DFA-ANZ-SRVZ
        //               ,:DFA-IMP-CNBT-NDED-K1
        //                :IND-DFA-IMP-CNBT-NDED-K1
        //               ,:DFA-IMP-CNBT-NDED-K2
        //                :IND-DFA-IMP-CNBT-NDED-K2
        //               ,:DFA-IMP-CNBT-NDED-K3
        //                :IND-DFA-IMP-CNBT-NDED-K3
        //               ,:DFA-IMP-CNBT-AZ-K3
        //                :IND-DFA-IMP-CNBT-AZ-K3
        //               ,:DFA-IMP-CNBT-ISC-K3
        //                :IND-DFA-IMP-CNBT-ISC-K3
        //               ,:DFA-IMP-CNBT-TFR-K3
        //                :IND-DFA-IMP-CNBT-TFR-K3
        //               ,:DFA-IMP-CNBT-VOL-K3
        //                :IND-DFA-IMP-CNBT-VOL-K3
        //               ,:DFA-MATU-K3
        //                :IND-DFA-MATU-K3
        //               ,:DFA-IMPB-252-ANTIC
        //                :IND-DFA-IMPB-252-ANTIC
        //               ,:DFA-IMPST-252-ANTIC
        //                :IND-DFA-IMPST-252-ANTIC
        //               ,:DFA-DT-1A-CNBZ-DB
        //                :IND-DFA-DT-1A-CNBZ
        //               ,:DFA-COMMIS-DI-TRASFE
        //                :IND-DFA-COMMIS-DI-TRASFE
        //               ,:DFA-AA-CNBZ-K1
        //                :IND-DFA-AA-CNBZ-K1
        //               ,:DFA-AA-CNBZ-K2
        //                :IND-DFA-AA-CNBZ-K2
        //               ,:DFA-AA-CNBZ-K3
        //                :IND-DFA-AA-CNBZ-K3
        //               ,:DFA-MM-CNBZ-K1
        //                :IND-DFA-MM-CNBZ-K1
        //               ,:DFA-MM-CNBZ-K2
        //                :IND-DFA-MM-CNBZ-K2
        //               ,:DFA-MM-CNBZ-K3
        //                :IND-DFA-MM-CNBZ-K3
        //               ,:DFA-FL-APPLZ-NEWFIS
        //                :IND-DFA-FL-APPLZ-NEWFIS
        //               ,:DFA-REDT-TASS-ABBAT-K3
        //                :IND-DFA-REDT-TASS-ABBAT-K3
        //               ,:DFA-CNBT-ECC-4X100-K1
        //                :IND-DFA-CNBT-ECC-4X100-K1
        //               ,:DFA-DS-RIGA
        //               ,:DFA-DS-OPER-SQL
        //               ,:DFA-DS-VER
        //               ,:DFA-DS-TS-INI-CPTZ
        //               ,:DFA-DS-TS-END-CPTZ
        //               ,:DFA-DS-UTENTE
        //               ,:DFA-DS-STATO-ELAB
        //               ,:DFA-CREDITO-IS
        //                :IND-DFA-CREDITO-IS
        //               ,:DFA-REDT-TASS-ABBAT-K2
        //                :IND-DFA-REDT-TASS-ABBAT-K2
        //               ,:DFA-IMPB-IS-K3
        //                :IND-DFA-IMPB-IS-K3
        //               ,:DFA-IMPST-SOST-K3
        //                :IND-DFA-IMPST-SOST-K3
        //               ,:DFA-IMPB-252-K3
        //                :IND-DFA-IMPB-252-K3
        //               ,:DFA-IMPST-252-K3
        //                :IND-DFA-IMPST-252-K3
        //               ,:DFA-IMPB-IS-K3-ANTIC
        //                :IND-DFA-IMPB-IS-K3-ANTIC
        //               ,:DFA-IMPST-SOST-K3-ANTI
        //                :IND-DFA-IMPST-SOST-K3-ANTI
        //               ,:DFA-IMPB-IRPEF-K1-ANTI
        //                :IND-DFA-IMPB-IRPEF-K1-ANTI
        //               ,:DFA-IMPST-IRPEF-K1-ANT
        //                :IND-DFA-IMPST-IRPEF-K1-ANT
        //               ,:DFA-IMPB-IRPEF-K2-ANTI
        //                :IND-DFA-IMPB-IRPEF-K2-ANTI
        //               ,:DFA-IMPST-IRPEF-K2-ANT
        //                :IND-DFA-IMPST-IRPEF-K2-ANT
        //               ,:DFA-DT-CESSAZIONE-DB
        //                :IND-DFA-DT-CESSAZIONE
        //               ,:DFA-TOT-IMPST
        //                :IND-DFA-TOT-IMPST
        //               ,:DFA-ONER-TRASFE
        //                :IND-DFA-ONER-TRASFE
        //               ,:DFA-IMP-NET-TRASFERITO
        //                :IND-DFA-IMP-NET-TRASFERITO
        //             FROM D_FISC_ADES
        //             WHERE     ID_ADES = :DFA-ID-ADES
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        dFiscAdesDao.selectRec3(dFiscAdes.getDfaIdAdes(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B460-OPEN-CURSOR-IDP-CPZ<br>*/
    private void b460OpenCursorIdpCpz() {
        // COB_CODE: PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.
        b405DeclareCursorIdpCpz();
        // COB_CODE: EXEC SQL
        //                OPEN C-IDP-CPZ-DFA
        //           END-EXEC.
        dFiscAdesDao.openCIdpCpzDfa(dFiscAdes.getDfaIdAdes(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B470-CLOSE-CURSOR-IDP-CPZ<br>*/
    private void b470CloseCursorIdpCpz() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IDP-CPZ-DFA
        //           END-EXEC.
        dFiscAdesDao.closeCIdpCpzDfa();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B480-FETCH-FIRST-IDP-CPZ<br>*/
    private void b480FetchFirstIdpCpz() {
        // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.
        b460OpenCursorIdpCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
            b490FetchNextIdpCpz();
        }
    }

    /**Original name: B490-FETCH-NEXT-IDP-CPZ<br>*/
    private void b490FetchNextIdpCpz() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IDP-CPZ-DFA
        //           INTO
        //                :DFA-ID-D-FISC-ADES
        //               ,:DFA-ID-ADES
        //               ,:DFA-ID-MOVI-CRZ
        //               ,:DFA-ID-MOVI-CHIU
        //                :IND-DFA-ID-MOVI-CHIU
        //               ,:DFA-DT-INI-EFF-DB
        //               ,:DFA-DT-END-EFF-DB
        //               ,:DFA-COD-COMP-ANIA
        //               ,:DFA-TP-ISC-FND
        //                :IND-DFA-TP-ISC-FND
        //               ,:DFA-DT-ACCNS-RAPP-FND-DB
        //                :IND-DFA-DT-ACCNS-RAPP-FND
        //               ,:DFA-IMP-CNBT-AZ-K1
        //                :IND-DFA-IMP-CNBT-AZ-K1
        //               ,:DFA-IMP-CNBT-ISC-K1
        //                :IND-DFA-IMP-CNBT-ISC-K1
        //               ,:DFA-IMP-CNBT-TFR-K1
        //                :IND-DFA-IMP-CNBT-TFR-K1
        //               ,:DFA-IMP-CNBT-VOL-K1
        //                :IND-DFA-IMP-CNBT-VOL-K1
        //               ,:DFA-IMP-CNBT-AZ-K2
        //                :IND-DFA-IMP-CNBT-AZ-K2
        //               ,:DFA-IMP-CNBT-ISC-K2
        //                :IND-DFA-IMP-CNBT-ISC-K2
        //               ,:DFA-IMP-CNBT-TFR-K2
        //                :IND-DFA-IMP-CNBT-TFR-K2
        //               ,:DFA-IMP-CNBT-VOL-K2
        //                :IND-DFA-IMP-CNBT-VOL-K2
        //               ,:DFA-MATU-K1
        //                :IND-DFA-MATU-K1
        //               ,:DFA-MATU-RES-K1
        //                :IND-DFA-MATU-RES-K1
        //               ,:DFA-MATU-K2
        //                :IND-DFA-MATU-K2
        //               ,:DFA-IMPB-VIS
        //                :IND-DFA-IMPB-VIS
        //               ,:DFA-IMPST-VIS
        //                :IND-DFA-IMPST-VIS
        //               ,:DFA-IMPB-IS-K2
        //                :IND-DFA-IMPB-IS-K2
        //               ,:DFA-IMPST-SOST-K2
        //                :IND-DFA-IMPST-SOST-K2
        //               ,:DFA-RIDZ-TFR
        //                :IND-DFA-RIDZ-TFR
        //               ,:DFA-PC-TFR
        //                :IND-DFA-PC-TFR
        //               ,:DFA-ALQ-TFR
        //                :IND-DFA-ALQ-TFR
        //               ,:DFA-TOT-ANTIC
        //                :IND-DFA-TOT-ANTIC
        //               ,:DFA-IMPB-TFR-ANTIC
        //                :IND-DFA-IMPB-TFR-ANTIC
        //               ,:DFA-IMPST-TFR-ANTIC
        //                :IND-DFA-IMPST-TFR-ANTIC
        //               ,:DFA-RIDZ-TFR-SU-ANTIC
        //                :IND-DFA-RIDZ-TFR-SU-ANTIC
        //               ,:DFA-IMPB-VIS-ANTIC
        //                :IND-DFA-IMPB-VIS-ANTIC
        //               ,:DFA-IMPST-VIS-ANTIC
        //                :IND-DFA-IMPST-VIS-ANTIC
        //               ,:DFA-IMPB-IS-K2-ANTIC
        //                :IND-DFA-IMPB-IS-K2-ANTIC
        //               ,:DFA-IMPST-SOST-K2-ANTI
        //                :IND-DFA-IMPST-SOST-K2-ANTI
        //               ,:DFA-ULT-COMMIS-TRASFE
        //                :IND-DFA-ULT-COMMIS-TRASFE
        //               ,:DFA-COD-DVS
        //                :IND-DFA-COD-DVS
        //               ,:DFA-ALQ-PRVR
        //                :IND-DFA-ALQ-PRVR
        //               ,:DFA-PC-ESE-IMPST-TFR
        //                :IND-DFA-PC-ESE-IMPST-TFR
        //               ,:DFA-IMP-ESE-IMPST-TFR
        //                :IND-DFA-IMP-ESE-IMPST-TFR
        //               ,:DFA-ANZ-CNBTVA-CARASS
        //                :IND-DFA-ANZ-CNBTVA-CARASS
        //               ,:DFA-ANZ-CNBTVA-CARAZI
        //                :IND-DFA-ANZ-CNBTVA-CARAZI
        //               ,:DFA-ANZ-SRVZ
        //                :IND-DFA-ANZ-SRVZ
        //               ,:DFA-IMP-CNBT-NDED-K1
        //                :IND-DFA-IMP-CNBT-NDED-K1
        //               ,:DFA-IMP-CNBT-NDED-K2
        //                :IND-DFA-IMP-CNBT-NDED-K2
        //               ,:DFA-IMP-CNBT-NDED-K3
        //                :IND-DFA-IMP-CNBT-NDED-K3
        //               ,:DFA-IMP-CNBT-AZ-K3
        //                :IND-DFA-IMP-CNBT-AZ-K3
        //               ,:DFA-IMP-CNBT-ISC-K3
        //                :IND-DFA-IMP-CNBT-ISC-K3
        //               ,:DFA-IMP-CNBT-TFR-K3
        //                :IND-DFA-IMP-CNBT-TFR-K3
        //               ,:DFA-IMP-CNBT-VOL-K3
        //                :IND-DFA-IMP-CNBT-VOL-K3
        //               ,:DFA-MATU-K3
        //                :IND-DFA-MATU-K3
        //               ,:DFA-IMPB-252-ANTIC
        //                :IND-DFA-IMPB-252-ANTIC
        //               ,:DFA-IMPST-252-ANTIC
        //                :IND-DFA-IMPST-252-ANTIC
        //               ,:DFA-DT-1A-CNBZ-DB
        //                :IND-DFA-DT-1A-CNBZ
        //               ,:DFA-COMMIS-DI-TRASFE
        //                :IND-DFA-COMMIS-DI-TRASFE
        //               ,:DFA-AA-CNBZ-K1
        //                :IND-DFA-AA-CNBZ-K1
        //               ,:DFA-AA-CNBZ-K2
        //                :IND-DFA-AA-CNBZ-K2
        //               ,:DFA-AA-CNBZ-K3
        //                :IND-DFA-AA-CNBZ-K3
        //               ,:DFA-MM-CNBZ-K1
        //                :IND-DFA-MM-CNBZ-K1
        //               ,:DFA-MM-CNBZ-K2
        //                :IND-DFA-MM-CNBZ-K2
        //               ,:DFA-MM-CNBZ-K3
        //                :IND-DFA-MM-CNBZ-K3
        //               ,:DFA-FL-APPLZ-NEWFIS
        //                :IND-DFA-FL-APPLZ-NEWFIS
        //               ,:DFA-REDT-TASS-ABBAT-K3
        //                :IND-DFA-REDT-TASS-ABBAT-K3
        //               ,:DFA-CNBT-ECC-4X100-K1
        //                :IND-DFA-CNBT-ECC-4X100-K1
        //               ,:DFA-DS-RIGA
        //               ,:DFA-DS-OPER-SQL
        //               ,:DFA-DS-VER
        //               ,:DFA-DS-TS-INI-CPTZ
        //               ,:DFA-DS-TS-END-CPTZ
        //               ,:DFA-DS-UTENTE
        //               ,:DFA-DS-STATO-ELAB
        //               ,:DFA-CREDITO-IS
        //                :IND-DFA-CREDITO-IS
        //               ,:DFA-REDT-TASS-ABBAT-K2
        //                :IND-DFA-REDT-TASS-ABBAT-K2
        //               ,:DFA-IMPB-IS-K3
        //                :IND-DFA-IMPB-IS-K3
        //               ,:DFA-IMPST-SOST-K3
        //                :IND-DFA-IMPST-SOST-K3
        //               ,:DFA-IMPB-252-K3
        //                :IND-DFA-IMPB-252-K3
        //               ,:DFA-IMPST-252-K3
        //                :IND-DFA-IMPST-252-K3
        //               ,:DFA-IMPB-IS-K3-ANTIC
        //                :IND-DFA-IMPB-IS-K3-ANTIC
        //               ,:DFA-IMPST-SOST-K3-ANTI
        //                :IND-DFA-IMPST-SOST-K3-ANTI
        //               ,:DFA-IMPB-IRPEF-K1-ANTI
        //                :IND-DFA-IMPB-IRPEF-K1-ANTI
        //               ,:DFA-IMPST-IRPEF-K1-ANT
        //                :IND-DFA-IMPST-IRPEF-K1-ANT
        //               ,:DFA-IMPB-IRPEF-K2-ANTI
        //                :IND-DFA-IMPB-IRPEF-K2-ANTI
        //               ,:DFA-IMPST-IRPEF-K2-ANT
        //                :IND-DFA-IMPST-IRPEF-K2-ANT
        //               ,:DFA-DT-CESSAZIONE-DB
        //                :IND-DFA-DT-CESSAZIONE
        //               ,:DFA-TOT-IMPST
        //                :IND-DFA-TOT-IMPST
        //               ,:DFA-ONER-TRASFE
        //                :IND-DFA-ONER-TRASFE
        //               ,:DFA-IMP-NET-TRASFERITO
        //                :IND-DFA-IMP-NET-TRASFERITO
        //           END-EXEC.
        dFiscAdesDao.fetchCIdpCpzDfa(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
            b470CloseCursorIdpCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B505-DECLARE-CURSOR-IBO-CPZ<br>
	 * <pre>----
	 * ----  gestione IBO Competenza
	 * ----</pre>*/
    private void b505DeclareCursorIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B510-SELECT-IBO-CPZ<br>*/
    private void b510SelectIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B560-OPEN-CURSOR-IBO-CPZ<br>*/
    private void b560OpenCursorIboCpz() {
        // COB_CODE: PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.
        b505DeclareCursorIboCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B570-CLOSE-CURSOR-IBO-CPZ<br>*/
    private void b570CloseCursorIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B580-FETCH-FIRST-IBO-CPZ<br>*/
    private void b580FetchFirstIboCpz() {
        // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.
        b560OpenCursorIboCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
    }

    /**Original name: B590-FETCH-NEXT-IBO-CPZ<br>*/
    private void b590FetchNextIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B605-DECLARE-CURSOR-IBS-CPZ<br>
	 * <pre>----
	 * ----  gestione IBS Competenza
	 * ----</pre>*/
    private void b605DeclareCursorIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B610-SELECT-IBS-CPZ<br>*/
    private void b610SelectIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B660-OPEN-CURSOR-IBS-CPZ<br>*/
    private void b660OpenCursorIbsCpz() {
        // COB_CODE: PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.
        b605DeclareCursorIbsCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B670-CLOSE-CURSOR-IBS-CPZ<br>*/
    private void b670CloseCursorIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B680-FETCH-FIRST-IBS-CPZ<br>*/
    private void b680FetchFirstIbsCpz() {
        // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.
        b660OpenCursorIbsCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
    }

    /**Original name: B690-FETCH-NEXT-IBS-CPZ<br>*/
    private void b690FetchNextIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B705-DECLARE-CURSOR-IDO-CPZ<br>
	 * <pre>----
	 * ----  gestione IDO Competenza
	 * ----</pre>*/
    private void b705DeclareCursorIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B710-SELECT-IDO-CPZ<br>*/
    private void b710SelectIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B760-OPEN-CURSOR-IDO-CPZ<br>*/
    private void b760OpenCursorIdoCpz() {
        // COB_CODE: PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.
        b705DeclareCursorIdoCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B770-CLOSE-CURSOR-IDO-CPZ<br>*/
    private void b770CloseCursorIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B780-FETCH-FIRST-IDO-CPZ<br>*/
    private void b780FetchFirstIdoCpz() {
        // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.
        b760OpenCursorIdoCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
    }

    /**Original name: B790-FETCH-NEXT-IDO-CPZ<br>*/
    private void b790FetchNextIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-DFA-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO DFA-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-ID-MOVI-CHIU-NULL
            dFiscAdes.getDfaIdMoviChiu().setDfaIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaIdMoviChiu.Len.DFA_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-DFA-TP-ISC-FND = -1
        //              MOVE HIGH-VALUES TO DFA-TP-ISC-FND-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getTpIscFnd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-TP-ISC-FND-NULL
            dFiscAdes.setDfaTpIscFnd(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DFiscAdesIdbsdfa0.Len.DFA_TP_ISC_FND));
        }
        // COB_CODE: IF IND-DFA-DT-ACCNS-RAPP-FND = -1
        //              MOVE HIGH-VALUES TO DFA-DT-ACCNS-RAPP-FND-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getDtAccnsRappFnd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-DT-ACCNS-RAPP-FND-NULL
            dFiscAdes.getDfaDtAccnsRappFnd().setDfaDtAccnsRappFndNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaDtAccnsRappFnd.Len.DFA_DT_ACCNS_RAPP_FND_NULL));
        }
        // COB_CODE: IF IND-DFA-IMP-CNBT-AZ-K1 = -1
        //              MOVE HIGH-VALUES TO DFA-IMP-CNBT-AZ-K1-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getImpCnbtAzK1() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-IMP-CNBT-AZ-K1-NULL
            dFiscAdes.getDfaImpCnbtAzK1().setDfaImpCnbtAzK1Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaImpCnbtAzK1.Len.DFA_IMP_CNBT_AZ_K1_NULL));
        }
        // COB_CODE: IF IND-DFA-IMP-CNBT-ISC-K1 = -1
        //              MOVE HIGH-VALUES TO DFA-IMP-CNBT-ISC-K1-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getImpCnbtIscK1() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-IMP-CNBT-ISC-K1-NULL
            dFiscAdes.getDfaImpCnbtIscK1().setDfaImpCnbtIscK1Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaImpCnbtIscK1.Len.DFA_IMP_CNBT_ISC_K1_NULL));
        }
        // COB_CODE: IF IND-DFA-IMP-CNBT-TFR-K1 = -1
        //              MOVE HIGH-VALUES TO DFA-IMP-CNBT-TFR-K1-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getImpCnbtTfrK1() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-IMP-CNBT-TFR-K1-NULL
            dFiscAdes.getDfaImpCnbtTfrK1().setDfaImpCnbtTfrK1Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaImpCnbtTfrK1.Len.DFA_IMP_CNBT_TFR_K1_NULL));
        }
        // COB_CODE: IF IND-DFA-IMP-CNBT-VOL-K1 = -1
        //              MOVE HIGH-VALUES TO DFA-IMP-CNBT-VOL-K1-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getImpCnbtVolK1() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-IMP-CNBT-VOL-K1-NULL
            dFiscAdes.getDfaImpCnbtVolK1().setDfaImpCnbtVolK1Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaImpCnbtVolK1.Len.DFA_IMP_CNBT_VOL_K1_NULL));
        }
        // COB_CODE: IF IND-DFA-IMP-CNBT-AZ-K2 = -1
        //              MOVE HIGH-VALUES TO DFA-IMP-CNBT-AZ-K2-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getImpCnbtAzK2() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-IMP-CNBT-AZ-K2-NULL
            dFiscAdes.getDfaImpCnbtAzK2().setDfaImpCnbtAzK2Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaImpCnbtAzK2.Len.DFA_IMP_CNBT_AZ_K2_NULL));
        }
        // COB_CODE: IF IND-DFA-IMP-CNBT-ISC-K2 = -1
        //              MOVE HIGH-VALUES TO DFA-IMP-CNBT-ISC-K2-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getImpCnbtIscK2() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-IMP-CNBT-ISC-K2-NULL
            dFiscAdes.getDfaImpCnbtIscK2().setDfaImpCnbtIscK2Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaImpCnbtIscK2.Len.DFA_IMP_CNBT_ISC_K2_NULL));
        }
        // COB_CODE: IF IND-DFA-IMP-CNBT-TFR-K2 = -1
        //              MOVE HIGH-VALUES TO DFA-IMP-CNBT-TFR-K2-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getImpCnbtTfrK2() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-IMP-CNBT-TFR-K2-NULL
            dFiscAdes.getDfaImpCnbtTfrK2().setDfaImpCnbtTfrK2Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaImpCnbtTfrK2.Len.DFA_IMP_CNBT_TFR_K2_NULL));
        }
        // COB_CODE: IF IND-DFA-IMP-CNBT-VOL-K2 = -1
        //              MOVE HIGH-VALUES TO DFA-IMP-CNBT-VOL-K2-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getImpCnbtVolK2() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-IMP-CNBT-VOL-K2-NULL
            dFiscAdes.getDfaImpCnbtVolK2().setDfaImpCnbtVolK2Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaImpCnbtVolK2.Len.DFA_IMP_CNBT_VOL_K2_NULL));
        }
        // COB_CODE: IF IND-DFA-MATU-K1 = -1
        //              MOVE HIGH-VALUES TO DFA-MATU-K1-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getMatuK1() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-MATU-K1-NULL
            dFiscAdes.getDfaMatuK1().setDfaMatuK1Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaMatuK1.Len.DFA_MATU_K1_NULL));
        }
        // COB_CODE: IF IND-DFA-MATU-RES-K1 = -1
        //              MOVE HIGH-VALUES TO DFA-MATU-RES-K1-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getMatuResK1() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-MATU-RES-K1-NULL
            dFiscAdes.getDfaMatuResK1().setDfaMatuResK1Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaMatuResK1.Len.DFA_MATU_RES_K1_NULL));
        }
        // COB_CODE: IF IND-DFA-MATU-K2 = -1
        //              MOVE HIGH-VALUES TO DFA-MATU-K2-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getMatuK2() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-MATU-K2-NULL
            dFiscAdes.getDfaMatuK2().setDfaMatuK2Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaMatuK2.Len.DFA_MATU_K2_NULL));
        }
        // COB_CODE: IF IND-DFA-IMPB-VIS = -1
        //              MOVE HIGH-VALUES TO DFA-IMPB-VIS-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getImpbVis() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-IMPB-VIS-NULL
            dFiscAdes.getDfaImpbVis().setDfaImpbVisNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaImpbVis.Len.DFA_IMPB_VIS_NULL));
        }
        // COB_CODE: IF IND-DFA-IMPST-VIS = -1
        //              MOVE HIGH-VALUES TO DFA-IMPST-VIS-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getImpstVis() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-IMPST-VIS-NULL
            dFiscAdes.getDfaImpstVis().setDfaImpstVisNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaImpstVis.Len.DFA_IMPST_VIS_NULL));
        }
        // COB_CODE: IF IND-DFA-IMPB-IS-K2 = -1
        //              MOVE HIGH-VALUES TO DFA-IMPB-IS-K2-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getImpbIsK2() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-IMPB-IS-K2-NULL
            dFiscAdes.getDfaImpbIsK2().setDfaImpbIsK2Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaImpbIsK2.Len.DFA_IMPB_IS_K2_NULL));
        }
        // COB_CODE: IF IND-DFA-IMPST-SOST-K2 = -1
        //              MOVE HIGH-VALUES TO DFA-IMPST-SOST-K2-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getImpstSostK2() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-IMPST-SOST-K2-NULL
            dFiscAdes.getDfaImpstSostK2().setDfaImpstSostK2Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaImpstSostK2.Len.DFA_IMPST_SOST_K2_NULL));
        }
        // COB_CODE: IF IND-DFA-RIDZ-TFR = -1
        //              MOVE HIGH-VALUES TO DFA-RIDZ-TFR-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getRidzTfr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-RIDZ-TFR-NULL
            dFiscAdes.getDfaRidzTfr().setDfaRidzTfrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaRidzTfr.Len.DFA_RIDZ_TFR_NULL));
        }
        // COB_CODE: IF IND-DFA-PC-TFR = -1
        //              MOVE HIGH-VALUES TO DFA-PC-TFR-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getPcTfr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-PC-TFR-NULL
            dFiscAdes.getDfaPcTfr().setDfaPcTfrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaPcTfr.Len.DFA_PC_TFR_NULL));
        }
        // COB_CODE: IF IND-DFA-ALQ-TFR = -1
        //              MOVE HIGH-VALUES TO DFA-ALQ-TFR-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getAlqTfr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-ALQ-TFR-NULL
            dFiscAdes.getDfaAlqTfr().setDfaAlqTfrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaAlqTfr.Len.DFA_ALQ_TFR_NULL));
        }
        // COB_CODE: IF IND-DFA-TOT-ANTIC = -1
        //              MOVE HIGH-VALUES TO DFA-TOT-ANTIC-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getTotAntic() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-TOT-ANTIC-NULL
            dFiscAdes.getDfaTotAntic().setDfaTotAnticNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaTotAntic.Len.DFA_TOT_ANTIC_NULL));
        }
        // COB_CODE: IF IND-DFA-IMPB-TFR-ANTIC = -1
        //              MOVE HIGH-VALUES TO DFA-IMPB-TFR-ANTIC-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getImpbTfrAntic() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-IMPB-TFR-ANTIC-NULL
            dFiscAdes.getDfaImpbTfrAntic().setDfaImpbTfrAnticNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaImpbTfrAntic.Len.DFA_IMPB_TFR_ANTIC_NULL));
        }
        // COB_CODE: IF IND-DFA-IMPST-TFR-ANTIC = -1
        //              MOVE HIGH-VALUES TO DFA-IMPST-TFR-ANTIC-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getImpstTfrAntic() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-IMPST-TFR-ANTIC-NULL
            dFiscAdes.getDfaImpstTfrAntic().setDfaImpstTfrAnticNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaImpstTfrAntic.Len.DFA_IMPST_TFR_ANTIC_NULL));
        }
        // COB_CODE: IF IND-DFA-RIDZ-TFR-SU-ANTIC = -1
        //              MOVE HIGH-VALUES TO DFA-RIDZ-TFR-SU-ANTIC-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getRidzTfrSuAntic() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-RIDZ-TFR-SU-ANTIC-NULL
            dFiscAdes.getDfaRidzTfrSuAntic().setDfaRidzTfrSuAnticNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaRidzTfrSuAntic.Len.DFA_RIDZ_TFR_SU_ANTIC_NULL));
        }
        // COB_CODE: IF IND-DFA-IMPB-VIS-ANTIC = -1
        //              MOVE HIGH-VALUES TO DFA-IMPB-VIS-ANTIC-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getImpbVisAntic() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-IMPB-VIS-ANTIC-NULL
            dFiscAdes.getDfaImpbVisAntic().setDfaImpbVisAnticNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaImpbVisAntic.Len.DFA_IMPB_VIS_ANTIC_NULL));
        }
        // COB_CODE: IF IND-DFA-IMPST-VIS-ANTIC = -1
        //              MOVE HIGH-VALUES TO DFA-IMPST-VIS-ANTIC-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getImpstVisAntic() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-IMPST-VIS-ANTIC-NULL
            dFiscAdes.getDfaImpstVisAntic().setDfaImpstVisAnticNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaImpstVisAntic.Len.DFA_IMPST_VIS_ANTIC_NULL));
        }
        // COB_CODE: IF IND-DFA-IMPB-IS-K2-ANTIC = -1
        //              MOVE HIGH-VALUES TO DFA-IMPB-IS-K2-ANTIC-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getImpbIsK2Antic() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-IMPB-IS-K2-ANTIC-NULL
            dFiscAdes.getDfaImpbIsK2Antic().setDfaImpbIsK2AnticNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaImpbIsK2Antic.Len.DFA_IMPB_IS_K2_ANTIC_NULL));
        }
        // COB_CODE: IF IND-DFA-IMPST-SOST-K2-ANTI = -1
        //              MOVE HIGH-VALUES TO DFA-IMPST-SOST-K2-ANTI-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getImpstSostK2Anti() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-IMPST-SOST-K2-ANTI-NULL
            dFiscAdes.getDfaImpstSostK2Anti().setDfaImpstSostK2AntiNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaImpstSostK2Anti.Len.DFA_IMPST_SOST_K2_ANTI_NULL));
        }
        // COB_CODE: IF IND-DFA-ULT-COMMIS-TRASFE = -1
        //              MOVE HIGH-VALUES TO DFA-ULT-COMMIS-TRASFE-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getUltCommisTrasfe() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-ULT-COMMIS-TRASFE-NULL
            dFiscAdes.getDfaUltCommisTrasfe().setDfaUltCommisTrasfeNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaUltCommisTrasfe.Len.DFA_ULT_COMMIS_TRASFE_NULL));
        }
        // COB_CODE: IF IND-DFA-COD-DVS = -1
        //              MOVE HIGH-VALUES TO DFA-COD-DVS-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getCodDvs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-COD-DVS-NULL
            dFiscAdes.setDfaCodDvs(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DFiscAdesIdbsdfa0.Len.DFA_COD_DVS));
        }
        // COB_CODE: IF IND-DFA-ALQ-PRVR = -1
        //              MOVE HIGH-VALUES TO DFA-ALQ-PRVR-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getAlqPrvr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-ALQ-PRVR-NULL
            dFiscAdes.getDfaAlqPrvr().setDfaAlqPrvrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaAlqPrvr.Len.DFA_ALQ_PRVR_NULL));
        }
        // COB_CODE: IF IND-DFA-PC-ESE-IMPST-TFR = -1
        //              MOVE HIGH-VALUES TO DFA-PC-ESE-IMPST-TFR-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getPcEseImpstTfr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-PC-ESE-IMPST-TFR-NULL
            dFiscAdes.getDfaPcEseImpstTfr().setDfaPcEseImpstTfrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaPcEseImpstTfr.Len.DFA_PC_ESE_IMPST_TFR_NULL));
        }
        // COB_CODE: IF IND-DFA-IMP-ESE-IMPST-TFR = -1
        //              MOVE HIGH-VALUES TO DFA-IMP-ESE-IMPST-TFR-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getImpEseImpstTfr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-IMP-ESE-IMPST-TFR-NULL
            dFiscAdes.getDfaImpEseImpstTfr().setDfaImpEseImpstTfrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaImpEseImpstTfr.Len.DFA_IMP_ESE_IMPST_TFR_NULL));
        }
        // COB_CODE: IF IND-DFA-ANZ-CNBTVA-CARASS = -1
        //              MOVE HIGH-VALUES TO DFA-ANZ-CNBTVA-CARASS-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getAnzCnbtvaCarass() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-ANZ-CNBTVA-CARASS-NULL
            dFiscAdes.getDfaAnzCnbtvaCarass().setDfaAnzCnbtvaCarassNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaAnzCnbtvaCarass.Len.DFA_ANZ_CNBTVA_CARASS_NULL));
        }
        // COB_CODE: IF IND-DFA-ANZ-CNBTVA-CARAZI = -1
        //              MOVE HIGH-VALUES TO DFA-ANZ-CNBTVA-CARAZI-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getAnzCnbtvaCarazi() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-ANZ-CNBTVA-CARAZI-NULL
            dFiscAdes.getDfaAnzCnbtvaCarazi().setDfaAnzCnbtvaCaraziNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaAnzCnbtvaCarazi.Len.DFA_ANZ_CNBTVA_CARAZI_NULL));
        }
        // COB_CODE: IF IND-DFA-ANZ-SRVZ = -1
        //              MOVE HIGH-VALUES TO DFA-ANZ-SRVZ-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getAnzSrvz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-ANZ-SRVZ-NULL
            dFiscAdes.getDfaAnzSrvz().setDfaAnzSrvzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaAnzSrvz.Len.DFA_ANZ_SRVZ_NULL));
        }
        // COB_CODE: IF IND-DFA-IMP-CNBT-NDED-K1 = -1
        //              MOVE HIGH-VALUES TO DFA-IMP-CNBT-NDED-K1-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getImpCnbtNdedK1() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-IMP-CNBT-NDED-K1-NULL
            dFiscAdes.getDfaImpCnbtNdedK1().setDfaImpCnbtNdedK1Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaImpCnbtNdedK1.Len.DFA_IMP_CNBT_NDED_K1_NULL));
        }
        // COB_CODE: IF IND-DFA-IMP-CNBT-NDED-K2 = -1
        //              MOVE HIGH-VALUES TO DFA-IMP-CNBT-NDED-K2-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getImpCnbtNdedK2() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-IMP-CNBT-NDED-K2-NULL
            dFiscAdes.getDfaImpCnbtNdedK2().setDfaImpCnbtNdedK2Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaImpCnbtNdedK2.Len.DFA_IMP_CNBT_NDED_K2_NULL));
        }
        // COB_CODE: IF IND-DFA-IMP-CNBT-NDED-K3 = -1
        //              MOVE HIGH-VALUES TO DFA-IMP-CNBT-NDED-K3-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getImpCnbtNdedK3() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-IMP-CNBT-NDED-K3-NULL
            dFiscAdes.getDfaImpCnbtNdedK3().setDfaImpCnbtNdedK3Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaImpCnbtNdedK3.Len.DFA_IMP_CNBT_NDED_K3_NULL));
        }
        // COB_CODE: IF IND-DFA-IMP-CNBT-AZ-K3 = -1
        //              MOVE HIGH-VALUES TO DFA-IMP-CNBT-AZ-K3-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getImpCnbtAzK3() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-IMP-CNBT-AZ-K3-NULL
            dFiscAdes.getDfaImpCnbtAzK3().setDfaImpCnbtAzK3Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaImpCnbtAzK3.Len.DFA_IMP_CNBT_AZ_K3_NULL));
        }
        // COB_CODE: IF IND-DFA-IMP-CNBT-ISC-K3 = -1
        //              MOVE HIGH-VALUES TO DFA-IMP-CNBT-ISC-K3-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getImpCnbtIscK3() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-IMP-CNBT-ISC-K3-NULL
            dFiscAdes.getDfaImpCnbtIscK3().setDfaImpCnbtIscK3Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaImpCnbtIscK3.Len.DFA_IMP_CNBT_ISC_K3_NULL));
        }
        // COB_CODE: IF IND-DFA-IMP-CNBT-TFR-K3 = -1
        //              MOVE HIGH-VALUES TO DFA-IMP-CNBT-TFR-K3-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getImpCnbtTfrK3() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-IMP-CNBT-TFR-K3-NULL
            dFiscAdes.getDfaImpCnbtTfrK3().setDfaImpCnbtTfrK3Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaImpCnbtTfrK3.Len.DFA_IMP_CNBT_TFR_K3_NULL));
        }
        // COB_CODE: IF IND-DFA-IMP-CNBT-VOL-K3 = -1
        //              MOVE HIGH-VALUES TO DFA-IMP-CNBT-VOL-K3-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getImpCnbtVolK3() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-IMP-CNBT-VOL-K3-NULL
            dFiscAdes.getDfaImpCnbtVolK3().setDfaImpCnbtVolK3Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaImpCnbtVolK3.Len.DFA_IMP_CNBT_VOL_K3_NULL));
        }
        // COB_CODE: IF IND-DFA-MATU-K3 = -1
        //              MOVE HIGH-VALUES TO DFA-MATU-K3-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getMatuK3() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-MATU-K3-NULL
            dFiscAdes.getDfaMatuK3().setDfaMatuK3Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaMatuK3.Len.DFA_MATU_K3_NULL));
        }
        // COB_CODE: IF IND-DFA-IMPB-252-ANTIC = -1
        //              MOVE HIGH-VALUES TO DFA-IMPB-252-ANTIC-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getImpb252Antic() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-IMPB-252-ANTIC-NULL
            dFiscAdes.getDfaImpb252Antic().setDfaImpb252AnticNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaImpb252Antic.Len.DFA_IMPB252_ANTIC_NULL));
        }
        // COB_CODE: IF IND-DFA-IMPST-252-ANTIC = -1
        //              MOVE HIGH-VALUES TO DFA-IMPST-252-ANTIC-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getImpst252Antic() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-IMPST-252-ANTIC-NULL
            dFiscAdes.getDfaImpst252Antic().setDfaImpst252AnticNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaImpst252Antic.Len.DFA_IMPST252_ANTIC_NULL));
        }
        // COB_CODE: IF IND-DFA-DT-1A-CNBZ = -1
        //              MOVE HIGH-VALUES TO DFA-DT-1A-CNBZ-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getDt1aCnbz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-DT-1A-CNBZ-NULL
            dFiscAdes.getDfaDt1aCnbz().setDfaDt1aCnbzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaDt1aCnbz.Len.DFA_DT1A_CNBZ_NULL));
        }
        // COB_CODE: IF IND-DFA-COMMIS-DI-TRASFE = -1
        //              MOVE HIGH-VALUES TO DFA-COMMIS-DI-TRASFE-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getCommisDiTrasfe() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-COMMIS-DI-TRASFE-NULL
            dFiscAdes.getDfaCommisDiTrasfe().setDfaCommisDiTrasfeNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaCommisDiTrasfe.Len.DFA_COMMIS_DI_TRASFE_NULL));
        }
        // COB_CODE: IF IND-DFA-AA-CNBZ-K1 = -1
        //              MOVE HIGH-VALUES TO DFA-AA-CNBZ-K1-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getAaCnbzK1() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-AA-CNBZ-K1-NULL
            dFiscAdes.getDfaAaCnbzK1().setDfaAaCnbzK1Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaAaCnbzK1.Len.DFA_AA_CNBZ_K1_NULL));
        }
        // COB_CODE: IF IND-DFA-AA-CNBZ-K2 = -1
        //              MOVE HIGH-VALUES TO DFA-AA-CNBZ-K2-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getAaCnbzK2() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-AA-CNBZ-K2-NULL
            dFiscAdes.getDfaAaCnbzK2().setDfaAaCnbzK2Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaAaCnbzK2.Len.DFA_AA_CNBZ_K2_NULL));
        }
        // COB_CODE: IF IND-DFA-AA-CNBZ-K3 = -1
        //              MOVE HIGH-VALUES TO DFA-AA-CNBZ-K3-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getAaCnbzK3() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-AA-CNBZ-K3-NULL
            dFiscAdes.getDfaAaCnbzK3().setDfaAaCnbzK3Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaAaCnbzK3.Len.DFA_AA_CNBZ_K3_NULL));
        }
        // COB_CODE: IF IND-DFA-MM-CNBZ-K1 = -1
        //              MOVE HIGH-VALUES TO DFA-MM-CNBZ-K1-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getMmCnbzK1() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-MM-CNBZ-K1-NULL
            dFiscAdes.getDfaMmCnbzK1().setDfaMmCnbzK1Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaMmCnbzK1.Len.DFA_MM_CNBZ_K1_NULL));
        }
        // COB_CODE: IF IND-DFA-MM-CNBZ-K2 = -1
        //              MOVE HIGH-VALUES TO DFA-MM-CNBZ-K2-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getMmCnbzK2() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-MM-CNBZ-K2-NULL
            dFiscAdes.getDfaMmCnbzK2().setDfaMmCnbzK2Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaMmCnbzK2.Len.DFA_MM_CNBZ_K2_NULL));
        }
        // COB_CODE: IF IND-DFA-MM-CNBZ-K3 = -1
        //              MOVE HIGH-VALUES TO DFA-MM-CNBZ-K3-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getMmCnbzK3() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-MM-CNBZ-K3-NULL
            dFiscAdes.getDfaMmCnbzK3().setDfaMmCnbzK3Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaMmCnbzK3.Len.DFA_MM_CNBZ_K3_NULL));
        }
        // COB_CODE: IF IND-DFA-FL-APPLZ-NEWFIS = -1
        //              MOVE HIGH-VALUES TO DFA-FL-APPLZ-NEWFIS-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getFlApplzNewfis() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-FL-APPLZ-NEWFIS-NULL
            dFiscAdes.setDfaFlApplzNewfis(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-DFA-REDT-TASS-ABBAT-K3 = -1
        //              MOVE HIGH-VALUES TO DFA-REDT-TASS-ABBAT-K3-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getRedtTassAbbatK3() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-REDT-TASS-ABBAT-K3-NULL
            dFiscAdes.getDfaRedtTassAbbatK3().setDfaRedtTassAbbatK3Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaRedtTassAbbatK3.Len.DFA_REDT_TASS_ABBAT_K3_NULL));
        }
        // COB_CODE: IF IND-DFA-CNBT-ECC-4X100-K1 = -1
        //              MOVE HIGH-VALUES TO DFA-CNBT-ECC-4X100-K1-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getCnbtEcc4x100K1() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-CNBT-ECC-4X100-K1-NULL
            dFiscAdes.getDfaCnbtEcc4x100K1().setDfaCnbtEcc4x100K1Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaCnbtEcc4x100K1.Len.DFA_CNBT_ECC4X100_K1_NULL));
        }
        // COB_CODE: IF IND-DFA-CREDITO-IS = -1
        //              MOVE HIGH-VALUES TO DFA-CREDITO-IS-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getCreditoIs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-CREDITO-IS-NULL
            dFiscAdes.getDfaCreditoIs().setDfaCreditoIsNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaCreditoIs.Len.DFA_CREDITO_IS_NULL));
        }
        // COB_CODE: IF IND-DFA-REDT-TASS-ABBAT-K2 = -1
        //              MOVE HIGH-VALUES TO DFA-REDT-TASS-ABBAT-K2-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getRedtTassAbbatK2() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-REDT-TASS-ABBAT-K2-NULL
            dFiscAdes.getDfaRedtTassAbbatK2().setDfaRedtTassAbbatK2Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaRedtTassAbbatK2.Len.DFA_REDT_TASS_ABBAT_K2_NULL));
        }
        // COB_CODE: IF IND-DFA-IMPB-IS-K3 = -1
        //              MOVE HIGH-VALUES TO DFA-IMPB-IS-K3-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getImpbIsK3() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-IMPB-IS-K3-NULL
            dFiscAdes.getDfaImpbIsK3().setDfaImpbIsK3Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaImpbIsK3.Len.DFA_IMPB_IS_K3_NULL));
        }
        // COB_CODE: IF IND-DFA-IMPST-SOST-K3 = -1
        //              MOVE HIGH-VALUES TO DFA-IMPST-SOST-K3-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getImpstSostK3() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-IMPST-SOST-K3-NULL
            dFiscAdes.getDfaImpstSostK3().setDfaImpstSostK3Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaImpstSostK3.Len.DFA_IMPST_SOST_K3_NULL));
        }
        // COB_CODE: IF IND-DFA-IMPB-252-K3 = -1
        //              MOVE HIGH-VALUES TO DFA-IMPB-252-K3-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getImpb252K3() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-IMPB-252-K3-NULL
            dFiscAdes.getDfaImpb252K3().setDfaImpb252K3Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaImpb252K3.Len.DFA_IMPB252_K3_NULL));
        }
        // COB_CODE: IF IND-DFA-IMPST-252-K3 = -1
        //              MOVE HIGH-VALUES TO DFA-IMPST-252-K3-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getImpst252K3() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-IMPST-252-K3-NULL
            dFiscAdes.getDfaImpst252K3().setDfaImpst252K3Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaImpst252K3.Len.DFA_IMPST252_K3_NULL));
        }
        // COB_CODE: IF IND-DFA-IMPB-IS-K3-ANTIC = -1
        //              MOVE HIGH-VALUES TO DFA-IMPB-IS-K3-ANTIC-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getImpbIsK3Antic() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-IMPB-IS-K3-ANTIC-NULL
            dFiscAdes.getDfaImpbIsK3Antic().setDfaImpbIsK3AnticNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaImpbIsK3Antic.Len.DFA_IMPB_IS_K3_ANTIC_NULL));
        }
        // COB_CODE: IF IND-DFA-IMPST-SOST-K3-ANTI = -1
        //              MOVE HIGH-VALUES TO DFA-IMPST-SOST-K3-ANTI-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getImpstSostK3Anti() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-IMPST-SOST-K3-ANTI-NULL
            dFiscAdes.getDfaImpstSostK3Anti().setDfaImpstSostK3AntiNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaImpstSostK3Anti.Len.DFA_IMPST_SOST_K3_ANTI_NULL));
        }
        // COB_CODE: IF IND-DFA-IMPB-IRPEF-K1-ANTI = -1
        //              MOVE HIGH-VALUES TO DFA-IMPB-IRPEF-K1-ANTI-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getImpbIrpefK1Anti() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-IMPB-IRPEF-K1-ANTI-NULL
            dFiscAdes.getDfaImpbIrpefK1Anti().setDfaImpbIrpefK1AntiNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaImpbIrpefK1Anti.Len.DFA_IMPB_IRPEF_K1_ANTI_NULL));
        }
        // COB_CODE: IF IND-DFA-IMPST-IRPEF-K1-ANT = -1
        //              MOVE HIGH-VALUES TO DFA-IMPST-IRPEF-K1-ANT-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getImpstIrpefK1Ant() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-IMPST-IRPEF-K1-ANT-NULL
            dFiscAdes.getDfaImpstIrpefK1Ant().setDfaImpstIrpefK1AntNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaImpstIrpefK1Ant.Len.DFA_IMPST_IRPEF_K1_ANT_NULL));
        }
        // COB_CODE: IF IND-DFA-IMPB-IRPEF-K2-ANTI = -1
        //              MOVE HIGH-VALUES TO DFA-IMPB-IRPEF-K2-ANTI-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getImpbIrpefK2Anti() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-IMPB-IRPEF-K2-ANTI-NULL
            dFiscAdes.getDfaImpbIrpefK2Anti().setDfaImpbIrpefK2AntiNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaImpbIrpefK2Anti.Len.DFA_IMPB_IRPEF_K2_ANTI_NULL));
        }
        // COB_CODE: IF IND-DFA-IMPST-IRPEF-K2-ANT = -1
        //              MOVE HIGH-VALUES TO DFA-IMPST-IRPEF-K2-ANT-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getImpstIrpefK2Ant() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-IMPST-IRPEF-K2-ANT-NULL
            dFiscAdes.getDfaImpstIrpefK2Ant().setDfaImpstIrpefK2AntNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaImpstIrpefK2Ant.Len.DFA_IMPST_IRPEF_K2_ANT_NULL));
        }
        // COB_CODE: IF IND-DFA-DT-CESSAZIONE = -1
        //              MOVE HIGH-VALUES TO DFA-DT-CESSAZIONE-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getDtCessazione() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-DT-CESSAZIONE-NULL
            dFiscAdes.getDfaDtCessazione().setDfaDtCessazioneNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaDtCessazione.Len.DFA_DT_CESSAZIONE_NULL));
        }
        // COB_CODE: IF IND-DFA-TOT-IMPST = -1
        //              MOVE HIGH-VALUES TO DFA-TOT-IMPST-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getTotImpst() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-TOT-IMPST-NULL
            dFiscAdes.getDfaTotImpst().setDfaTotImpstNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaTotImpst.Len.DFA_TOT_IMPST_NULL));
        }
        // COB_CODE: IF IND-DFA-ONER-TRASFE = -1
        //              MOVE HIGH-VALUES TO DFA-ONER-TRASFE-NULL
        //           END-IF
        if (ws.getIndDFiscAdes().getOnerTrasfe() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-ONER-TRASFE-NULL
            dFiscAdes.getDfaOnerTrasfe().setDfaOnerTrasfeNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaOnerTrasfe.Len.DFA_ONER_TRASFE_NULL));
        }
        // COB_CODE: IF IND-DFA-IMP-NET-TRASFERITO = -1
        //              MOVE HIGH-VALUES TO DFA-IMP-NET-TRASFERITO-NULL
        //           END-IF.
        if (ws.getIndDFiscAdes().getImpNetTrasferito() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DFA-IMP-NET-TRASFERITO-NULL
            dFiscAdes.getDfaImpNetTrasferito().setDfaImpNetTrasferitoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaImpNetTrasferito.Len.DFA_IMP_NET_TRASFERITO_NULL));
        }
    }

    /**Original name: Z150-VALORIZZA-DATA-SERVICES-I<br>*/
    private void z150ValorizzaDataServicesI() {
        // COB_CODE: MOVE 'I' TO DFA-DS-OPER-SQL
        dFiscAdes.setDfaDsOperSqlFormatted("I");
        // COB_CODE: MOVE 0                   TO DFA-DS-VER
        dFiscAdes.setDfaDsVer(0);
        // COB_CODE: MOVE IDSV0003-USER-NAME TO DFA-DS-UTENTE
        dFiscAdes.setDfaDsUtente(idsv0003.getUserName());
        // COB_CODE: MOVE '1'                   TO DFA-DS-STATO-ELAB.
        dFiscAdes.setDfaDsStatoElabFormatted("1");
    }

    /**Original name: Z160-VALORIZZA-DATA-SERVICES-U<br>*/
    private void z160ValorizzaDataServicesU() {
        // COB_CODE: MOVE 'U' TO DFA-DS-OPER-SQL
        dFiscAdes.setDfaDsOperSqlFormatted("U");
        // COB_CODE: MOVE IDSV0003-USER-NAME TO DFA-DS-UTENTE.
        dFiscAdes.setDfaDsUtente(idsv0003.getUserName());
    }

    /**Original name: Z200-SET-INDICATORI-NULL<br>*/
    private void z200SetIndicatoriNull() {
        // COB_CODE: IF DFA-ID-MOVI-CHIU-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-ID-MOVI-CHIU
        //           ELSE
        //              MOVE 0 TO IND-DFA-ID-MOVI-CHIU
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaIdMoviChiu().getDfaIdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-ID-MOVI-CHIU
            ws.getIndDFiscAdes().setIdMoviChiu(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-ID-MOVI-CHIU
            ws.getIndDFiscAdes().setIdMoviChiu(((short)0));
        }
        // COB_CODE: IF DFA-TP-ISC-FND-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-TP-ISC-FND
        //           ELSE
        //              MOVE 0 TO IND-DFA-TP-ISC-FND
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaTpIscFndFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-TP-ISC-FND
            ws.getIndDFiscAdes().setTpIscFnd(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-TP-ISC-FND
            ws.getIndDFiscAdes().setTpIscFnd(((short)0));
        }
        // COB_CODE: IF DFA-DT-ACCNS-RAPP-FND-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-DT-ACCNS-RAPP-FND
        //           ELSE
        //              MOVE 0 TO IND-DFA-DT-ACCNS-RAPP-FND
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaDtAccnsRappFnd().getDfaDtAccnsRappFndNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-DT-ACCNS-RAPP-FND
            ws.getIndDFiscAdes().setDtAccnsRappFnd(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-DT-ACCNS-RAPP-FND
            ws.getIndDFiscAdes().setDtAccnsRappFnd(((short)0));
        }
        // COB_CODE: IF DFA-IMP-CNBT-AZ-K1-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-IMP-CNBT-AZ-K1
        //           ELSE
        //              MOVE 0 TO IND-DFA-IMP-CNBT-AZ-K1
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaImpCnbtAzK1().getDfaImpCnbtAzK1NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-IMP-CNBT-AZ-K1
            ws.getIndDFiscAdes().setImpCnbtAzK1(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-IMP-CNBT-AZ-K1
            ws.getIndDFiscAdes().setImpCnbtAzK1(((short)0));
        }
        // COB_CODE: IF DFA-IMP-CNBT-ISC-K1-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-IMP-CNBT-ISC-K1
        //           ELSE
        //              MOVE 0 TO IND-DFA-IMP-CNBT-ISC-K1
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaImpCnbtIscK1().getDfaImpCnbtIscK1NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-IMP-CNBT-ISC-K1
            ws.getIndDFiscAdes().setImpCnbtIscK1(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-IMP-CNBT-ISC-K1
            ws.getIndDFiscAdes().setImpCnbtIscK1(((short)0));
        }
        // COB_CODE: IF DFA-IMP-CNBT-TFR-K1-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-IMP-CNBT-TFR-K1
        //           ELSE
        //              MOVE 0 TO IND-DFA-IMP-CNBT-TFR-K1
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaImpCnbtTfrK1().getDfaImpCnbtTfrK1NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-IMP-CNBT-TFR-K1
            ws.getIndDFiscAdes().setImpCnbtTfrK1(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-IMP-CNBT-TFR-K1
            ws.getIndDFiscAdes().setImpCnbtTfrK1(((short)0));
        }
        // COB_CODE: IF DFA-IMP-CNBT-VOL-K1-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-IMP-CNBT-VOL-K1
        //           ELSE
        //              MOVE 0 TO IND-DFA-IMP-CNBT-VOL-K1
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaImpCnbtVolK1().getDfaImpCnbtVolK1NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-IMP-CNBT-VOL-K1
            ws.getIndDFiscAdes().setImpCnbtVolK1(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-IMP-CNBT-VOL-K1
            ws.getIndDFiscAdes().setImpCnbtVolK1(((short)0));
        }
        // COB_CODE: IF DFA-IMP-CNBT-AZ-K2-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-IMP-CNBT-AZ-K2
        //           ELSE
        //              MOVE 0 TO IND-DFA-IMP-CNBT-AZ-K2
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaImpCnbtAzK2().getDfaImpCnbtAzK2NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-IMP-CNBT-AZ-K2
            ws.getIndDFiscAdes().setImpCnbtAzK2(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-IMP-CNBT-AZ-K2
            ws.getIndDFiscAdes().setImpCnbtAzK2(((short)0));
        }
        // COB_CODE: IF DFA-IMP-CNBT-ISC-K2-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-IMP-CNBT-ISC-K2
        //           ELSE
        //              MOVE 0 TO IND-DFA-IMP-CNBT-ISC-K2
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaImpCnbtIscK2().getDfaImpCnbtIscK2NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-IMP-CNBT-ISC-K2
            ws.getIndDFiscAdes().setImpCnbtIscK2(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-IMP-CNBT-ISC-K2
            ws.getIndDFiscAdes().setImpCnbtIscK2(((short)0));
        }
        // COB_CODE: IF DFA-IMP-CNBT-TFR-K2-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-IMP-CNBT-TFR-K2
        //           ELSE
        //              MOVE 0 TO IND-DFA-IMP-CNBT-TFR-K2
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaImpCnbtTfrK2().getDfaImpCnbtTfrK2NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-IMP-CNBT-TFR-K2
            ws.getIndDFiscAdes().setImpCnbtTfrK2(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-IMP-CNBT-TFR-K2
            ws.getIndDFiscAdes().setImpCnbtTfrK2(((short)0));
        }
        // COB_CODE: IF DFA-IMP-CNBT-VOL-K2-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-IMP-CNBT-VOL-K2
        //           ELSE
        //              MOVE 0 TO IND-DFA-IMP-CNBT-VOL-K2
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaImpCnbtVolK2().getDfaImpCnbtVolK2NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-IMP-CNBT-VOL-K2
            ws.getIndDFiscAdes().setImpCnbtVolK2(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-IMP-CNBT-VOL-K2
            ws.getIndDFiscAdes().setImpCnbtVolK2(((short)0));
        }
        // COB_CODE: IF DFA-MATU-K1-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-MATU-K1
        //           ELSE
        //              MOVE 0 TO IND-DFA-MATU-K1
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaMatuK1().getDfaMatuK1NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-MATU-K1
            ws.getIndDFiscAdes().setMatuK1(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-MATU-K1
            ws.getIndDFiscAdes().setMatuK1(((short)0));
        }
        // COB_CODE: IF DFA-MATU-RES-K1-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-MATU-RES-K1
        //           ELSE
        //              MOVE 0 TO IND-DFA-MATU-RES-K1
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaMatuResK1().getDfaMatuResK1NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-MATU-RES-K1
            ws.getIndDFiscAdes().setMatuResK1(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-MATU-RES-K1
            ws.getIndDFiscAdes().setMatuResK1(((short)0));
        }
        // COB_CODE: IF DFA-MATU-K2-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-MATU-K2
        //           ELSE
        //              MOVE 0 TO IND-DFA-MATU-K2
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaMatuK2().getDfaMatuK2NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-MATU-K2
            ws.getIndDFiscAdes().setMatuK2(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-MATU-K2
            ws.getIndDFiscAdes().setMatuK2(((short)0));
        }
        // COB_CODE: IF DFA-IMPB-VIS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-IMPB-VIS
        //           ELSE
        //              MOVE 0 TO IND-DFA-IMPB-VIS
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaImpbVis().getDfaImpbVisNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-IMPB-VIS
            ws.getIndDFiscAdes().setImpbVis(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-IMPB-VIS
            ws.getIndDFiscAdes().setImpbVis(((short)0));
        }
        // COB_CODE: IF DFA-IMPST-VIS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-IMPST-VIS
        //           ELSE
        //              MOVE 0 TO IND-DFA-IMPST-VIS
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaImpstVis().getDfaImpstVisNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-IMPST-VIS
            ws.getIndDFiscAdes().setImpstVis(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-IMPST-VIS
            ws.getIndDFiscAdes().setImpstVis(((short)0));
        }
        // COB_CODE: IF DFA-IMPB-IS-K2-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-IMPB-IS-K2
        //           ELSE
        //              MOVE 0 TO IND-DFA-IMPB-IS-K2
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaImpbIsK2().getDfaImpbIsK2NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-IMPB-IS-K2
            ws.getIndDFiscAdes().setImpbIsK2(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-IMPB-IS-K2
            ws.getIndDFiscAdes().setImpbIsK2(((short)0));
        }
        // COB_CODE: IF DFA-IMPST-SOST-K2-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-IMPST-SOST-K2
        //           ELSE
        //              MOVE 0 TO IND-DFA-IMPST-SOST-K2
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaImpstSostK2().getDfaImpstSostK2NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-IMPST-SOST-K2
            ws.getIndDFiscAdes().setImpstSostK2(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-IMPST-SOST-K2
            ws.getIndDFiscAdes().setImpstSostK2(((short)0));
        }
        // COB_CODE: IF DFA-RIDZ-TFR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-RIDZ-TFR
        //           ELSE
        //              MOVE 0 TO IND-DFA-RIDZ-TFR
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaRidzTfr().getDfaRidzTfrNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-RIDZ-TFR
            ws.getIndDFiscAdes().setRidzTfr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-RIDZ-TFR
            ws.getIndDFiscAdes().setRidzTfr(((short)0));
        }
        // COB_CODE: IF DFA-PC-TFR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-PC-TFR
        //           ELSE
        //              MOVE 0 TO IND-DFA-PC-TFR
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaPcTfr().getDfaPcTfrNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-PC-TFR
            ws.getIndDFiscAdes().setPcTfr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-PC-TFR
            ws.getIndDFiscAdes().setPcTfr(((short)0));
        }
        // COB_CODE: IF DFA-ALQ-TFR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-ALQ-TFR
        //           ELSE
        //              MOVE 0 TO IND-DFA-ALQ-TFR
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaAlqTfr().getDfaAlqTfrNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-ALQ-TFR
            ws.getIndDFiscAdes().setAlqTfr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-ALQ-TFR
            ws.getIndDFiscAdes().setAlqTfr(((short)0));
        }
        // COB_CODE: IF DFA-TOT-ANTIC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-TOT-ANTIC
        //           ELSE
        //              MOVE 0 TO IND-DFA-TOT-ANTIC
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaTotAntic().getDfaTotAnticNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-TOT-ANTIC
            ws.getIndDFiscAdes().setTotAntic(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-TOT-ANTIC
            ws.getIndDFiscAdes().setTotAntic(((short)0));
        }
        // COB_CODE: IF DFA-IMPB-TFR-ANTIC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-IMPB-TFR-ANTIC
        //           ELSE
        //              MOVE 0 TO IND-DFA-IMPB-TFR-ANTIC
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaImpbTfrAntic().getDfaImpbTfrAnticNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-IMPB-TFR-ANTIC
            ws.getIndDFiscAdes().setImpbTfrAntic(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-IMPB-TFR-ANTIC
            ws.getIndDFiscAdes().setImpbTfrAntic(((short)0));
        }
        // COB_CODE: IF DFA-IMPST-TFR-ANTIC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-IMPST-TFR-ANTIC
        //           ELSE
        //              MOVE 0 TO IND-DFA-IMPST-TFR-ANTIC
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaImpstTfrAntic().getDfaImpstTfrAnticNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-IMPST-TFR-ANTIC
            ws.getIndDFiscAdes().setImpstTfrAntic(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-IMPST-TFR-ANTIC
            ws.getIndDFiscAdes().setImpstTfrAntic(((short)0));
        }
        // COB_CODE: IF DFA-RIDZ-TFR-SU-ANTIC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-RIDZ-TFR-SU-ANTIC
        //           ELSE
        //              MOVE 0 TO IND-DFA-RIDZ-TFR-SU-ANTIC
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaRidzTfrSuAntic().getDfaRidzTfrSuAnticNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-RIDZ-TFR-SU-ANTIC
            ws.getIndDFiscAdes().setRidzTfrSuAntic(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-RIDZ-TFR-SU-ANTIC
            ws.getIndDFiscAdes().setRidzTfrSuAntic(((short)0));
        }
        // COB_CODE: IF DFA-IMPB-VIS-ANTIC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-IMPB-VIS-ANTIC
        //           ELSE
        //              MOVE 0 TO IND-DFA-IMPB-VIS-ANTIC
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaImpbVisAntic().getDfaImpbVisAnticNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-IMPB-VIS-ANTIC
            ws.getIndDFiscAdes().setImpbVisAntic(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-IMPB-VIS-ANTIC
            ws.getIndDFiscAdes().setImpbVisAntic(((short)0));
        }
        // COB_CODE: IF DFA-IMPST-VIS-ANTIC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-IMPST-VIS-ANTIC
        //           ELSE
        //              MOVE 0 TO IND-DFA-IMPST-VIS-ANTIC
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaImpstVisAntic().getDfaImpstVisAnticNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-IMPST-VIS-ANTIC
            ws.getIndDFiscAdes().setImpstVisAntic(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-IMPST-VIS-ANTIC
            ws.getIndDFiscAdes().setImpstVisAntic(((short)0));
        }
        // COB_CODE: IF DFA-IMPB-IS-K2-ANTIC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-IMPB-IS-K2-ANTIC
        //           ELSE
        //              MOVE 0 TO IND-DFA-IMPB-IS-K2-ANTIC
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaImpbIsK2Antic().getDfaImpbIsK2AnticNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-IMPB-IS-K2-ANTIC
            ws.getIndDFiscAdes().setImpbIsK2Antic(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-IMPB-IS-K2-ANTIC
            ws.getIndDFiscAdes().setImpbIsK2Antic(((short)0));
        }
        // COB_CODE: IF DFA-IMPST-SOST-K2-ANTI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-IMPST-SOST-K2-ANTI
        //           ELSE
        //              MOVE 0 TO IND-DFA-IMPST-SOST-K2-ANTI
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaImpstSostK2Anti().getDfaImpstSostK2AntiNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-IMPST-SOST-K2-ANTI
            ws.getIndDFiscAdes().setImpstSostK2Anti(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-IMPST-SOST-K2-ANTI
            ws.getIndDFiscAdes().setImpstSostK2Anti(((short)0));
        }
        // COB_CODE: IF DFA-ULT-COMMIS-TRASFE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-ULT-COMMIS-TRASFE
        //           ELSE
        //              MOVE 0 TO IND-DFA-ULT-COMMIS-TRASFE
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaUltCommisTrasfe().getDfaUltCommisTrasfeNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-ULT-COMMIS-TRASFE
            ws.getIndDFiscAdes().setUltCommisTrasfe(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-ULT-COMMIS-TRASFE
            ws.getIndDFiscAdes().setUltCommisTrasfe(((short)0));
        }
        // COB_CODE: IF DFA-COD-DVS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-COD-DVS
        //           ELSE
        //              MOVE 0 TO IND-DFA-COD-DVS
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaCodDvsFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-COD-DVS
            ws.getIndDFiscAdes().setCodDvs(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-COD-DVS
            ws.getIndDFiscAdes().setCodDvs(((short)0));
        }
        // COB_CODE: IF DFA-ALQ-PRVR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-ALQ-PRVR
        //           ELSE
        //              MOVE 0 TO IND-DFA-ALQ-PRVR
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaAlqPrvr().getDfaAlqPrvrNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-ALQ-PRVR
            ws.getIndDFiscAdes().setAlqPrvr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-ALQ-PRVR
            ws.getIndDFiscAdes().setAlqPrvr(((short)0));
        }
        // COB_CODE: IF DFA-PC-ESE-IMPST-TFR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-PC-ESE-IMPST-TFR
        //           ELSE
        //              MOVE 0 TO IND-DFA-PC-ESE-IMPST-TFR
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaPcEseImpstTfr().getDfaPcEseImpstTfrNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-PC-ESE-IMPST-TFR
            ws.getIndDFiscAdes().setPcEseImpstTfr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-PC-ESE-IMPST-TFR
            ws.getIndDFiscAdes().setPcEseImpstTfr(((short)0));
        }
        // COB_CODE: IF DFA-IMP-ESE-IMPST-TFR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-IMP-ESE-IMPST-TFR
        //           ELSE
        //              MOVE 0 TO IND-DFA-IMP-ESE-IMPST-TFR
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaImpEseImpstTfr().getDfaImpEseImpstTfrNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-IMP-ESE-IMPST-TFR
            ws.getIndDFiscAdes().setImpEseImpstTfr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-IMP-ESE-IMPST-TFR
            ws.getIndDFiscAdes().setImpEseImpstTfr(((short)0));
        }
        // COB_CODE: IF DFA-ANZ-CNBTVA-CARASS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-ANZ-CNBTVA-CARASS
        //           ELSE
        //              MOVE 0 TO IND-DFA-ANZ-CNBTVA-CARASS
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaAnzCnbtvaCarass().getDfaAnzCnbtvaCarassNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-ANZ-CNBTVA-CARASS
            ws.getIndDFiscAdes().setAnzCnbtvaCarass(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-ANZ-CNBTVA-CARASS
            ws.getIndDFiscAdes().setAnzCnbtvaCarass(((short)0));
        }
        // COB_CODE: IF DFA-ANZ-CNBTVA-CARAZI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-ANZ-CNBTVA-CARAZI
        //           ELSE
        //              MOVE 0 TO IND-DFA-ANZ-CNBTVA-CARAZI
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaAnzCnbtvaCarazi().getDfaAnzCnbtvaCaraziNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-ANZ-CNBTVA-CARAZI
            ws.getIndDFiscAdes().setAnzCnbtvaCarazi(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-ANZ-CNBTVA-CARAZI
            ws.getIndDFiscAdes().setAnzCnbtvaCarazi(((short)0));
        }
        // COB_CODE: IF DFA-ANZ-SRVZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-ANZ-SRVZ
        //           ELSE
        //              MOVE 0 TO IND-DFA-ANZ-SRVZ
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaAnzSrvz().getDfaAnzSrvzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-ANZ-SRVZ
            ws.getIndDFiscAdes().setAnzSrvz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-ANZ-SRVZ
            ws.getIndDFiscAdes().setAnzSrvz(((short)0));
        }
        // COB_CODE: IF DFA-IMP-CNBT-NDED-K1-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-IMP-CNBT-NDED-K1
        //           ELSE
        //              MOVE 0 TO IND-DFA-IMP-CNBT-NDED-K1
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaImpCnbtNdedK1().getDfaImpCnbtNdedK1NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-IMP-CNBT-NDED-K1
            ws.getIndDFiscAdes().setImpCnbtNdedK1(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-IMP-CNBT-NDED-K1
            ws.getIndDFiscAdes().setImpCnbtNdedK1(((short)0));
        }
        // COB_CODE: IF DFA-IMP-CNBT-NDED-K2-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-IMP-CNBT-NDED-K2
        //           ELSE
        //              MOVE 0 TO IND-DFA-IMP-CNBT-NDED-K2
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaImpCnbtNdedK2().getDfaImpCnbtNdedK2NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-IMP-CNBT-NDED-K2
            ws.getIndDFiscAdes().setImpCnbtNdedK2(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-IMP-CNBT-NDED-K2
            ws.getIndDFiscAdes().setImpCnbtNdedK2(((short)0));
        }
        // COB_CODE: IF DFA-IMP-CNBT-NDED-K3-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-IMP-CNBT-NDED-K3
        //           ELSE
        //              MOVE 0 TO IND-DFA-IMP-CNBT-NDED-K3
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaImpCnbtNdedK3().getDfaImpCnbtNdedK3NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-IMP-CNBT-NDED-K3
            ws.getIndDFiscAdes().setImpCnbtNdedK3(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-IMP-CNBT-NDED-K3
            ws.getIndDFiscAdes().setImpCnbtNdedK3(((short)0));
        }
        // COB_CODE: IF DFA-IMP-CNBT-AZ-K3-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-IMP-CNBT-AZ-K3
        //           ELSE
        //              MOVE 0 TO IND-DFA-IMP-CNBT-AZ-K3
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaImpCnbtAzK3().getDfaImpCnbtAzK3NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-IMP-CNBT-AZ-K3
            ws.getIndDFiscAdes().setImpCnbtAzK3(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-IMP-CNBT-AZ-K3
            ws.getIndDFiscAdes().setImpCnbtAzK3(((short)0));
        }
        // COB_CODE: IF DFA-IMP-CNBT-ISC-K3-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-IMP-CNBT-ISC-K3
        //           ELSE
        //              MOVE 0 TO IND-DFA-IMP-CNBT-ISC-K3
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaImpCnbtIscK3().getDfaImpCnbtIscK3NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-IMP-CNBT-ISC-K3
            ws.getIndDFiscAdes().setImpCnbtIscK3(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-IMP-CNBT-ISC-K3
            ws.getIndDFiscAdes().setImpCnbtIscK3(((short)0));
        }
        // COB_CODE: IF DFA-IMP-CNBT-TFR-K3-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-IMP-CNBT-TFR-K3
        //           ELSE
        //              MOVE 0 TO IND-DFA-IMP-CNBT-TFR-K3
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaImpCnbtTfrK3().getDfaImpCnbtTfrK3NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-IMP-CNBT-TFR-K3
            ws.getIndDFiscAdes().setImpCnbtTfrK3(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-IMP-CNBT-TFR-K3
            ws.getIndDFiscAdes().setImpCnbtTfrK3(((short)0));
        }
        // COB_CODE: IF DFA-IMP-CNBT-VOL-K3-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-IMP-CNBT-VOL-K3
        //           ELSE
        //              MOVE 0 TO IND-DFA-IMP-CNBT-VOL-K3
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaImpCnbtVolK3().getDfaImpCnbtVolK3NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-IMP-CNBT-VOL-K3
            ws.getIndDFiscAdes().setImpCnbtVolK3(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-IMP-CNBT-VOL-K3
            ws.getIndDFiscAdes().setImpCnbtVolK3(((short)0));
        }
        // COB_CODE: IF DFA-MATU-K3-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-MATU-K3
        //           ELSE
        //              MOVE 0 TO IND-DFA-MATU-K3
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaMatuK3().getDfaMatuK3NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-MATU-K3
            ws.getIndDFiscAdes().setMatuK3(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-MATU-K3
            ws.getIndDFiscAdes().setMatuK3(((short)0));
        }
        // COB_CODE: IF DFA-IMPB-252-ANTIC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-IMPB-252-ANTIC
        //           ELSE
        //              MOVE 0 TO IND-DFA-IMPB-252-ANTIC
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaImpb252Antic().getDfaImpb252AnticNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-IMPB-252-ANTIC
            ws.getIndDFiscAdes().setImpb252Antic(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-IMPB-252-ANTIC
            ws.getIndDFiscAdes().setImpb252Antic(((short)0));
        }
        // COB_CODE: IF DFA-IMPST-252-ANTIC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-IMPST-252-ANTIC
        //           ELSE
        //              MOVE 0 TO IND-DFA-IMPST-252-ANTIC
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaImpst252Antic().getDfaImpst252AnticNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-IMPST-252-ANTIC
            ws.getIndDFiscAdes().setImpst252Antic(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-IMPST-252-ANTIC
            ws.getIndDFiscAdes().setImpst252Antic(((short)0));
        }
        // COB_CODE: IF DFA-DT-1A-CNBZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-DT-1A-CNBZ
        //           ELSE
        //              MOVE 0 TO IND-DFA-DT-1A-CNBZ
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaDt1aCnbz().getDfaDt1aCnbzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-DT-1A-CNBZ
            ws.getIndDFiscAdes().setDt1aCnbz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-DT-1A-CNBZ
            ws.getIndDFiscAdes().setDt1aCnbz(((short)0));
        }
        // COB_CODE: IF DFA-COMMIS-DI-TRASFE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-COMMIS-DI-TRASFE
        //           ELSE
        //              MOVE 0 TO IND-DFA-COMMIS-DI-TRASFE
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaCommisDiTrasfe().getDfaCommisDiTrasfeNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-COMMIS-DI-TRASFE
            ws.getIndDFiscAdes().setCommisDiTrasfe(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-COMMIS-DI-TRASFE
            ws.getIndDFiscAdes().setCommisDiTrasfe(((short)0));
        }
        // COB_CODE: IF DFA-AA-CNBZ-K1-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-AA-CNBZ-K1
        //           ELSE
        //              MOVE 0 TO IND-DFA-AA-CNBZ-K1
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaAaCnbzK1().getDfaAaCnbzK1NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-AA-CNBZ-K1
            ws.getIndDFiscAdes().setAaCnbzK1(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-AA-CNBZ-K1
            ws.getIndDFiscAdes().setAaCnbzK1(((short)0));
        }
        // COB_CODE: IF DFA-AA-CNBZ-K2-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-AA-CNBZ-K2
        //           ELSE
        //              MOVE 0 TO IND-DFA-AA-CNBZ-K2
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaAaCnbzK2().getDfaAaCnbzK2NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-AA-CNBZ-K2
            ws.getIndDFiscAdes().setAaCnbzK2(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-AA-CNBZ-K2
            ws.getIndDFiscAdes().setAaCnbzK2(((short)0));
        }
        // COB_CODE: IF DFA-AA-CNBZ-K3-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-AA-CNBZ-K3
        //           ELSE
        //              MOVE 0 TO IND-DFA-AA-CNBZ-K3
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaAaCnbzK3().getDfaAaCnbzK3NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-AA-CNBZ-K3
            ws.getIndDFiscAdes().setAaCnbzK3(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-AA-CNBZ-K3
            ws.getIndDFiscAdes().setAaCnbzK3(((short)0));
        }
        // COB_CODE: IF DFA-MM-CNBZ-K1-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-MM-CNBZ-K1
        //           ELSE
        //              MOVE 0 TO IND-DFA-MM-CNBZ-K1
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaMmCnbzK1().getDfaMmCnbzK1NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-MM-CNBZ-K1
            ws.getIndDFiscAdes().setMmCnbzK1(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-MM-CNBZ-K1
            ws.getIndDFiscAdes().setMmCnbzK1(((short)0));
        }
        // COB_CODE: IF DFA-MM-CNBZ-K2-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-MM-CNBZ-K2
        //           ELSE
        //              MOVE 0 TO IND-DFA-MM-CNBZ-K2
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaMmCnbzK2().getDfaMmCnbzK2NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-MM-CNBZ-K2
            ws.getIndDFiscAdes().setMmCnbzK2(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-MM-CNBZ-K2
            ws.getIndDFiscAdes().setMmCnbzK2(((short)0));
        }
        // COB_CODE: IF DFA-MM-CNBZ-K3-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-MM-CNBZ-K3
        //           ELSE
        //              MOVE 0 TO IND-DFA-MM-CNBZ-K3
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaMmCnbzK3().getDfaMmCnbzK3NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-MM-CNBZ-K3
            ws.getIndDFiscAdes().setMmCnbzK3(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-MM-CNBZ-K3
            ws.getIndDFiscAdes().setMmCnbzK3(((short)0));
        }
        // COB_CODE: IF DFA-FL-APPLZ-NEWFIS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-FL-APPLZ-NEWFIS
        //           ELSE
        //              MOVE 0 TO IND-DFA-FL-APPLZ-NEWFIS
        //           END-IF
        if (Conditions.eq(dFiscAdes.getDfaFlApplzNewfis(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-DFA-FL-APPLZ-NEWFIS
            ws.getIndDFiscAdes().setFlApplzNewfis(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-FL-APPLZ-NEWFIS
            ws.getIndDFiscAdes().setFlApplzNewfis(((short)0));
        }
        // COB_CODE: IF DFA-REDT-TASS-ABBAT-K3-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-REDT-TASS-ABBAT-K3
        //           ELSE
        //              MOVE 0 TO IND-DFA-REDT-TASS-ABBAT-K3
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaRedtTassAbbatK3().getDfaRedtTassAbbatK3NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-REDT-TASS-ABBAT-K3
            ws.getIndDFiscAdes().setRedtTassAbbatK3(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-REDT-TASS-ABBAT-K3
            ws.getIndDFiscAdes().setRedtTassAbbatK3(((short)0));
        }
        // COB_CODE: IF DFA-CNBT-ECC-4X100-K1-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-CNBT-ECC-4X100-K1
        //           ELSE
        //              MOVE 0 TO IND-DFA-CNBT-ECC-4X100-K1
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaCnbtEcc4x100K1().getDfaCnbtEcc4x100K1NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-CNBT-ECC-4X100-K1
            ws.getIndDFiscAdes().setCnbtEcc4x100K1(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-CNBT-ECC-4X100-K1
            ws.getIndDFiscAdes().setCnbtEcc4x100K1(((short)0));
        }
        // COB_CODE: IF DFA-CREDITO-IS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-CREDITO-IS
        //           ELSE
        //              MOVE 0 TO IND-DFA-CREDITO-IS
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaCreditoIs().getDfaCreditoIsNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-CREDITO-IS
            ws.getIndDFiscAdes().setCreditoIs(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-CREDITO-IS
            ws.getIndDFiscAdes().setCreditoIs(((short)0));
        }
        // COB_CODE: IF DFA-REDT-TASS-ABBAT-K2-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-REDT-TASS-ABBAT-K2
        //           ELSE
        //              MOVE 0 TO IND-DFA-REDT-TASS-ABBAT-K2
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaRedtTassAbbatK2().getDfaRedtTassAbbatK2NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-REDT-TASS-ABBAT-K2
            ws.getIndDFiscAdes().setRedtTassAbbatK2(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-REDT-TASS-ABBAT-K2
            ws.getIndDFiscAdes().setRedtTassAbbatK2(((short)0));
        }
        // COB_CODE: IF DFA-IMPB-IS-K3-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-IMPB-IS-K3
        //           ELSE
        //              MOVE 0 TO IND-DFA-IMPB-IS-K3
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaImpbIsK3().getDfaImpbIsK3NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-IMPB-IS-K3
            ws.getIndDFiscAdes().setImpbIsK3(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-IMPB-IS-K3
            ws.getIndDFiscAdes().setImpbIsK3(((short)0));
        }
        // COB_CODE: IF DFA-IMPST-SOST-K3-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-IMPST-SOST-K3
        //           ELSE
        //              MOVE 0 TO IND-DFA-IMPST-SOST-K3
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaImpstSostK3().getDfaImpstSostK3NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-IMPST-SOST-K3
            ws.getIndDFiscAdes().setImpstSostK3(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-IMPST-SOST-K3
            ws.getIndDFiscAdes().setImpstSostK3(((short)0));
        }
        // COB_CODE: IF DFA-IMPB-252-K3-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-IMPB-252-K3
        //           ELSE
        //              MOVE 0 TO IND-DFA-IMPB-252-K3
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaImpb252K3().getDfaImpb252K3NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-IMPB-252-K3
            ws.getIndDFiscAdes().setImpb252K3(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-IMPB-252-K3
            ws.getIndDFiscAdes().setImpb252K3(((short)0));
        }
        // COB_CODE: IF DFA-IMPST-252-K3-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-IMPST-252-K3
        //           ELSE
        //              MOVE 0 TO IND-DFA-IMPST-252-K3
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaImpst252K3().getDfaImpst252K3NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-IMPST-252-K3
            ws.getIndDFiscAdes().setImpst252K3(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-IMPST-252-K3
            ws.getIndDFiscAdes().setImpst252K3(((short)0));
        }
        // COB_CODE: IF DFA-IMPB-IS-K3-ANTIC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-IMPB-IS-K3-ANTIC
        //           ELSE
        //              MOVE 0 TO IND-DFA-IMPB-IS-K3-ANTIC
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaImpbIsK3Antic().getDfaImpbIsK3AnticNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-IMPB-IS-K3-ANTIC
            ws.getIndDFiscAdes().setImpbIsK3Antic(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-IMPB-IS-K3-ANTIC
            ws.getIndDFiscAdes().setImpbIsK3Antic(((short)0));
        }
        // COB_CODE: IF DFA-IMPST-SOST-K3-ANTI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-IMPST-SOST-K3-ANTI
        //           ELSE
        //              MOVE 0 TO IND-DFA-IMPST-SOST-K3-ANTI
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaImpstSostK3Anti().getDfaImpstSostK3AntiNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-IMPST-SOST-K3-ANTI
            ws.getIndDFiscAdes().setImpstSostK3Anti(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-IMPST-SOST-K3-ANTI
            ws.getIndDFiscAdes().setImpstSostK3Anti(((short)0));
        }
        // COB_CODE: IF DFA-IMPB-IRPEF-K1-ANTI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-IMPB-IRPEF-K1-ANTI
        //           ELSE
        //              MOVE 0 TO IND-DFA-IMPB-IRPEF-K1-ANTI
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaImpbIrpefK1Anti().getDfaImpbIrpefK1AntiNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-IMPB-IRPEF-K1-ANTI
            ws.getIndDFiscAdes().setImpbIrpefK1Anti(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-IMPB-IRPEF-K1-ANTI
            ws.getIndDFiscAdes().setImpbIrpefK1Anti(((short)0));
        }
        // COB_CODE: IF DFA-IMPST-IRPEF-K1-ANT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-IMPST-IRPEF-K1-ANT
        //           ELSE
        //              MOVE 0 TO IND-DFA-IMPST-IRPEF-K1-ANT
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaImpstIrpefK1Ant().getDfaImpstIrpefK1AntNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-IMPST-IRPEF-K1-ANT
            ws.getIndDFiscAdes().setImpstIrpefK1Ant(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-IMPST-IRPEF-K1-ANT
            ws.getIndDFiscAdes().setImpstIrpefK1Ant(((short)0));
        }
        // COB_CODE: IF DFA-IMPB-IRPEF-K2-ANTI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-IMPB-IRPEF-K2-ANTI
        //           ELSE
        //              MOVE 0 TO IND-DFA-IMPB-IRPEF-K2-ANTI
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaImpbIrpefK2Anti().getDfaImpbIrpefK2AntiNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-IMPB-IRPEF-K2-ANTI
            ws.getIndDFiscAdes().setImpbIrpefK2Anti(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-IMPB-IRPEF-K2-ANTI
            ws.getIndDFiscAdes().setImpbIrpefK2Anti(((short)0));
        }
        // COB_CODE: IF DFA-IMPST-IRPEF-K2-ANT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-IMPST-IRPEF-K2-ANT
        //           ELSE
        //              MOVE 0 TO IND-DFA-IMPST-IRPEF-K2-ANT
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaImpstIrpefK2Ant().getDfaImpstIrpefK2AntNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-IMPST-IRPEF-K2-ANT
            ws.getIndDFiscAdes().setImpstIrpefK2Ant(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-IMPST-IRPEF-K2-ANT
            ws.getIndDFiscAdes().setImpstIrpefK2Ant(((short)0));
        }
        // COB_CODE: IF DFA-DT-CESSAZIONE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-DT-CESSAZIONE
        //           ELSE
        //              MOVE 0 TO IND-DFA-DT-CESSAZIONE
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaDtCessazione().getDfaDtCessazioneNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-DT-CESSAZIONE
            ws.getIndDFiscAdes().setDtCessazione(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-DT-CESSAZIONE
            ws.getIndDFiscAdes().setDtCessazione(((short)0));
        }
        // COB_CODE: IF DFA-TOT-IMPST-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-TOT-IMPST
        //           ELSE
        //              MOVE 0 TO IND-DFA-TOT-IMPST
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaTotImpst().getDfaTotImpstNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-TOT-IMPST
            ws.getIndDFiscAdes().setTotImpst(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-TOT-IMPST
            ws.getIndDFiscAdes().setTotImpst(((short)0));
        }
        // COB_CODE: IF DFA-ONER-TRASFE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-ONER-TRASFE
        //           ELSE
        //              MOVE 0 TO IND-DFA-ONER-TRASFE
        //           END-IF
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaOnerTrasfe().getDfaOnerTrasfeNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-ONER-TRASFE
            ws.getIndDFiscAdes().setOnerTrasfe(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-ONER-TRASFE
            ws.getIndDFiscAdes().setOnerTrasfe(((short)0));
        }
        // COB_CODE: IF DFA-IMP-NET-TRASFERITO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DFA-IMP-NET-TRASFERITO
        //           ELSE
        //              MOVE 0 TO IND-DFA-IMP-NET-TRASFERITO
        //           END-IF.
        if (Characters.EQ_HIGH.test(dFiscAdes.getDfaImpNetTrasferito().getDfaImpNetTrasferitoNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DFA-IMP-NET-TRASFERITO
            ws.getIndDFiscAdes().setImpNetTrasferito(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DFA-IMP-NET-TRASFERITO
            ws.getIndDFiscAdes().setImpNetTrasferito(((short)0));
        }
    }

    /**Original name: Z400-SEQ-RIGA<br>*/
    private void z400SeqRiga() {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_RIGA
        //              INTO : DFA-DS-RIGA
        //           END-EXEC.
        //TODO: Implement: valuesStmt;
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: Z500-AGGIORNAMENTO-STORICO<br>*/
    private void z500AggiornamentoStorico() {
        // COB_CODE: MOVE D-FISC-ADES TO WS-BUFFER-TABLE.
        ws.setWsBufferTable(dFiscAdes.getdFiscAdesFormatted());
        // COB_CODE: MOVE DFA-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.
        ws.setWsIdMoviCrz(TruncAbs.toInt(dFiscAdes.getDfaIdMoviCrz(), 9));
        // COB_CODE: PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.
        a360OpenCursorIdEff();
        // COB_CODE: PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-SQL
        //              END-IF
        //           END-PERFORM.
        while (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX
            a390FetchNextIdEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              END-IF
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: MOVE WS-ID-MOVI-CRZ   TO DFA-ID-MOVI-CHIU
                dFiscAdes.getDfaIdMoviChiu().setDfaIdMoviChiu(ws.getWsIdMoviCrz());
                // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
                //                                 TO DFA-DS-TS-END-CPTZ
                dFiscAdes.setDfaDsTsEndCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
                // COB_CODE: PERFORM A330-UPDATE-ID-EFF THRU A330-EX
                a330UpdateIdEff();
                // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
                //                 END-IF
                //           END-IF
                if (idsv0003.getSqlcode().isSuccessfulSql()) {
                    // COB_CODE: MOVE WS-ID-MOVI-CRZ TO DFA-ID-MOVI-CRZ
                    dFiscAdes.setDfaIdMoviCrz(ws.getWsIdMoviCrz());
                    // COB_CODE: MOVE HIGH-VALUES    TO DFA-ID-MOVI-CHIU-NULL
                    dFiscAdes.getDfaIdMoviChiu().setDfaIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaIdMoviChiu.Len.DFA_ID_MOVI_CHIU_NULL));
                    // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
                    //                               TO DFA-DT-END-EFF
                    dFiscAdes.setDfaDtEndEff(idsv0003.getDataInizioEffetto());
                    // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
                    //                               TO DFA-DS-TS-INI-CPTZ
                    dFiscAdes.setDfaDsTsIniCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
                    // COB_CODE: MOVE WS-TS-INFINITO
                    //                               TO DFA-DS-TS-END-CPTZ
                    dFiscAdes.setDfaDsTsEndCptz(ws.getIdsv0010().getWsTsInfinito());
                    // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
                    //              PERFORM A220-INSERT-PK THRU A220-EX
                    //           END-IF
                    if (idsv0003.getSqlcode().isSuccessfulSql()) {
                        // COB_CODE: PERFORM A220-INSERT-PK THRU A220-EX
                        a220InsertPk();
                    }
                }
            }
        }
        // COB_CODE: IF IDSV0003-NOT-FOUND
        //              END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF NOT IDSV0003-DELETE-LOGICA
            //              PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX
            //           ELSE
            //              SET IDSV0003-SUCCESSFUL-SQL TO TRUE
            //           END-IF
            if (!idsv0003.getOperazione().isDeleteLogica()) {
                // COB_CODE: PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX
                z600InsertNuovaRigaStorica();
            }
            else {
                // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL TO TRUE
                idsv0003.getSqlcode().setSuccessfulSql();
            }
        }
    }

    /**Original name: Z550-AGG-STORICO-SOLO-INS<br>*/
    private void z550AggStoricoSoloIns() {
        // COB_CODE: MOVE D-FISC-ADES TO WS-BUFFER-TABLE.
        ws.setWsBufferTable(dFiscAdes.getdFiscAdesFormatted());
        // COB_CODE: MOVE DFA-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.
        ws.setWsIdMoviCrz(TruncAbs.toInt(dFiscAdes.getDfaIdMoviCrz(), 9));
        // COB_CODE: PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX.
        z600InsertNuovaRigaStorica();
    }

    /**Original name: Z600-INSERT-NUOVA-RIGA-STORICA<br>*/
    private void z600InsertNuovaRigaStorica() {
        // COB_CODE: MOVE WS-BUFFER-TABLE TO D-FISC-ADES.
        dFiscAdes.setdFiscAdesFormatted(ws.getWsBufferTableFormatted());
        // COB_CODE: MOVE WS-ID-MOVI-CRZ  TO DFA-ID-MOVI-CRZ.
        dFiscAdes.setDfaIdMoviCrz(ws.getWsIdMoviCrz());
        // COB_CODE: MOVE HIGH-VALUES     TO DFA-ID-MOVI-CHIU-NULL.
        dFiscAdes.getDfaIdMoviChiu().setDfaIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfaIdMoviChiu.Len.DFA_ID_MOVI_CHIU_NULL));
        // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
        //                                TO DFA-DT-INI-EFF.
        dFiscAdes.setDfaDtIniEff(idsv0003.getDataInizioEffetto());
        // COB_CODE: MOVE WS-DT-INFINITO
        //                                TO DFA-DT-END-EFF.
        dFiscAdes.setDfaDtEndEff(ws.getIdsv0010().getWsDtInfinito());
        // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
        //                                TO DFA-DS-TS-INI-CPTZ.
        dFiscAdes.setDfaDsTsIniCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
        // COB_CODE: MOVE WS-TS-INFINITO
        //                                TO DFA-DS-TS-END-CPTZ.
        dFiscAdes.setDfaDsTsEndCptz(ws.getIdsv0010().getWsTsInfinito());
        // COB_CODE: MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
        //                                TO DFA-COD-COMP-ANIA.
        dFiscAdes.setDfaCodCompAnia(idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A220-INSERT-PK THRU A220-EX.
        a220InsertPk();
    }

    /**Original name: Z900-CONVERTI-N-TO-X<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da 9(8) comp-3 a date
	 * ----</pre>*/
    private void z900ConvertiNToX() {
        // COB_CODE: MOVE DFA-DT-INI-EFF TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(dFiscAdes.getDfaDtIniEff(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO DFA-DT-INI-EFF-DB
        ws.getdFiscAdesDb().setIniEffDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: MOVE DFA-DT-END-EFF TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(dFiscAdes.getDfaDtEndEff(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO DFA-DT-END-EFF-DB
        ws.getdFiscAdesDb().setEndEffDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: IF IND-DFA-DT-ACCNS-RAPP-FND = 0
        //               MOVE WS-DATE-X      TO DFA-DT-ACCNS-RAPP-FND-DB
        //           END-IF
        if (ws.getIndDFiscAdes().getDtAccnsRappFnd() == 0) {
            // COB_CODE: MOVE DFA-DT-ACCNS-RAPP-FND TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(dFiscAdes.getDfaDtAccnsRappFnd().getDfaDtAccnsRappFnd(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO DFA-DT-ACCNS-RAPP-FND-DB
            ws.getdFiscAdesDb().setIniCopDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-DFA-DT-1A-CNBZ = 0
        //               MOVE WS-DATE-X      TO DFA-DT-1A-CNBZ-DB
        //           END-IF
        if (ws.getIndDFiscAdes().getDt1aCnbz() == 0) {
            // COB_CODE: MOVE DFA-DT-1A-CNBZ TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(dFiscAdes.getDfaDt1aCnbz().getDfaDt1aCnbz(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO DFA-DT-1A-CNBZ-DB
            ws.getdFiscAdesDb().setEndCopDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-DFA-DT-CESSAZIONE = 0
        //               MOVE WS-DATE-X      TO DFA-DT-CESSAZIONE-DB
        //           END-IF.
        if (ws.getIndDFiscAdes().getDtCessazione() == 0) {
            // COB_CODE: MOVE DFA-DT-CESSAZIONE TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(dFiscAdes.getDfaDtCessazione().getDfaDtCessazione(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO DFA-DT-CESSAZIONE-DB
            ws.getdFiscAdesDb().setEsiTitDb(ws.getIdsv0010().getWsDateX());
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE DFA-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getdFiscAdesDb().getIniEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO DFA-DT-INI-EFF
        dFiscAdes.setDfaDtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE DFA-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getdFiscAdesDb().getEndEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO DFA-DT-END-EFF
        dFiscAdes.setDfaDtEndEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-DFA-DT-ACCNS-RAPP-FND = 0
        //               MOVE WS-DATE-N      TO DFA-DT-ACCNS-RAPP-FND
        //           END-IF
        if (ws.getIndDFiscAdes().getDtAccnsRappFnd() == 0) {
            // COB_CODE: MOVE DFA-DT-ACCNS-RAPP-FND-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getdFiscAdesDb().getIniCopDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO DFA-DT-ACCNS-RAPP-FND
            dFiscAdes.getDfaDtAccnsRappFnd().setDfaDtAccnsRappFnd(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-DFA-DT-1A-CNBZ = 0
        //               MOVE WS-DATE-N      TO DFA-DT-1A-CNBZ
        //           END-IF
        if (ws.getIndDFiscAdes().getDt1aCnbz() == 0) {
            // COB_CODE: MOVE DFA-DT-1A-CNBZ-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getdFiscAdesDb().getEndCopDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO DFA-DT-1A-CNBZ
            dFiscAdes.getDfaDt1aCnbz().setDfaDt1aCnbz(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-DFA-DT-CESSAZIONE = 0
        //               MOVE WS-DATE-N      TO DFA-DT-CESSAZIONE
        //           END-IF.
        if (ws.getIndDFiscAdes().getDtCessazione() == 0) {
            // COB_CODE: MOVE DFA-DT-CESSAZIONE-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getdFiscAdesDb().getEsiTitDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO DFA-DT-CESSAZIONE
            dFiscAdes.getDfaDtCessazione().setDfaDtCessazione(ws.getIdsv0010().getWsDateN());
        }
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public short getAaCnbzK1() {
        return dFiscAdes.getDfaAaCnbzK1().getDfaAaCnbzK1();
    }

    @Override
    public void setAaCnbzK1(short aaCnbzK1) {
        this.dFiscAdes.getDfaAaCnbzK1().setDfaAaCnbzK1(aaCnbzK1);
    }

    @Override
    public Short getAaCnbzK1Obj() {
        if (ws.getIndDFiscAdes().getAaCnbzK1() >= 0) {
            return ((Short)getAaCnbzK1());
        }
        else {
            return null;
        }
    }

    @Override
    public void setAaCnbzK1Obj(Short aaCnbzK1Obj) {
        if (aaCnbzK1Obj != null) {
            setAaCnbzK1(((short)aaCnbzK1Obj));
            ws.getIndDFiscAdes().setAaCnbzK1(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setAaCnbzK1(((short)-1));
        }
    }

    @Override
    public short getAaCnbzK2() {
        return dFiscAdes.getDfaAaCnbzK2().getDfaAaCnbzK2();
    }

    @Override
    public void setAaCnbzK2(short aaCnbzK2) {
        this.dFiscAdes.getDfaAaCnbzK2().setDfaAaCnbzK2(aaCnbzK2);
    }

    @Override
    public Short getAaCnbzK2Obj() {
        if (ws.getIndDFiscAdes().getAaCnbzK2() >= 0) {
            return ((Short)getAaCnbzK2());
        }
        else {
            return null;
        }
    }

    @Override
    public void setAaCnbzK2Obj(Short aaCnbzK2Obj) {
        if (aaCnbzK2Obj != null) {
            setAaCnbzK2(((short)aaCnbzK2Obj));
            ws.getIndDFiscAdes().setAaCnbzK2(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setAaCnbzK2(((short)-1));
        }
    }

    @Override
    public short getAaCnbzK3() {
        return dFiscAdes.getDfaAaCnbzK3().getDfaAaCnbzK3();
    }

    @Override
    public void setAaCnbzK3(short aaCnbzK3) {
        this.dFiscAdes.getDfaAaCnbzK3().setDfaAaCnbzK3(aaCnbzK3);
    }

    @Override
    public Short getAaCnbzK3Obj() {
        if (ws.getIndDFiscAdes().getAaCnbzK3() >= 0) {
            return ((Short)getAaCnbzK3());
        }
        else {
            return null;
        }
    }

    @Override
    public void setAaCnbzK3Obj(Short aaCnbzK3Obj) {
        if (aaCnbzK3Obj != null) {
            setAaCnbzK3(((short)aaCnbzK3Obj));
            ws.getIndDFiscAdes().setAaCnbzK3(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setAaCnbzK3(((short)-1));
        }
    }

    @Override
    public AfDecimal getAlqPrvr() {
        return dFiscAdes.getDfaAlqPrvr().getDfaAlqPrvr();
    }

    @Override
    public void setAlqPrvr(AfDecimal alqPrvr) {
        this.dFiscAdes.getDfaAlqPrvr().setDfaAlqPrvr(alqPrvr.copy());
    }

    @Override
    public AfDecimal getAlqPrvrObj() {
        if (ws.getIndDFiscAdes().getAlqPrvr() >= 0) {
            return getAlqPrvr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAlqPrvrObj(AfDecimal alqPrvrObj) {
        if (alqPrvrObj != null) {
            setAlqPrvr(new AfDecimal(alqPrvrObj, 6, 3));
            ws.getIndDFiscAdes().setAlqPrvr(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setAlqPrvr(((short)-1));
        }
    }

    @Override
    public AfDecimal getAlqTfr() {
        return dFiscAdes.getDfaAlqTfr().getDfaAlqTfr();
    }

    @Override
    public void setAlqTfr(AfDecimal alqTfr) {
        this.dFiscAdes.getDfaAlqTfr().setDfaAlqTfr(alqTfr.copy());
    }

    @Override
    public AfDecimal getAlqTfrObj() {
        if (ws.getIndDFiscAdes().getAlqTfr() >= 0) {
            return getAlqTfr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAlqTfrObj(AfDecimal alqTfrObj) {
        if (alqTfrObj != null) {
            setAlqTfr(new AfDecimal(alqTfrObj, 6, 3));
            ws.getIndDFiscAdes().setAlqTfr(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setAlqTfr(((short)-1));
        }
    }

    @Override
    public short getAnzCnbtvaCarass() {
        return dFiscAdes.getDfaAnzCnbtvaCarass().getDfaAnzCnbtvaCarass();
    }

    @Override
    public void setAnzCnbtvaCarass(short anzCnbtvaCarass) {
        this.dFiscAdes.getDfaAnzCnbtvaCarass().setDfaAnzCnbtvaCarass(anzCnbtvaCarass);
    }

    @Override
    public Short getAnzCnbtvaCarassObj() {
        if (ws.getIndDFiscAdes().getAnzCnbtvaCarass() >= 0) {
            return ((Short)getAnzCnbtvaCarass());
        }
        else {
            return null;
        }
    }

    @Override
    public void setAnzCnbtvaCarassObj(Short anzCnbtvaCarassObj) {
        if (anzCnbtvaCarassObj != null) {
            setAnzCnbtvaCarass(((short)anzCnbtvaCarassObj));
            ws.getIndDFiscAdes().setAnzCnbtvaCarass(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setAnzCnbtvaCarass(((short)-1));
        }
    }

    @Override
    public short getAnzCnbtvaCarazi() {
        return dFiscAdes.getDfaAnzCnbtvaCarazi().getDfaAnzCnbtvaCarazi();
    }

    @Override
    public void setAnzCnbtvaCarazi(short anzCnbtvaCarazi) {
        this.dFiscAdes.getDfaAnzCnbtvaCarazi().setDfaAnzCnbtvaCarazi(anzCnbtvaCarazi);
    }

    @Override
    public Short getAnzCnbtvaCaraziObj() {
        if (ws.getIndDFiscAdes().getAnzCnbtvaCarazi() >= 0) {
            return ((Short)getAnzCnbtvaCarazi());
        }
        else {
            return null;
        }
    }

    @Override
    public void setAnzCnbtvaCaraziObj(Short anzCnbtvaCaraziObj) {
        if (anzCnbtvaCaraziObj != null) {
            setAnzCnbtvaCarazi(((short)anzCnbtvaCaraziObj));
            ws.getIndDFiscAdes().setAnzCnbtvaCarazi(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setAnzCnbtvaCarazi(((short)-1));
        }
    }

    @Override
    public short getAnzSrvz() {
        return dFiscAdes.getDfaAnzSrvz().getDfaAnzSrvz();
    }

    @Override
    public void setAnzSrvz(short anzSrvz) {
        this.dFiscAdes.getDfaAnzSrvz().setDfaAnzSrvz(anzSrvz);
    }

    @Override
    public Short getAnzSrvzObj() {
        if (ws.getIndDFiscAdes().getAnzSrvz() >= 0) {
            return ((Short)getAnzSrvz());
        }
        else {
            return null;
        }
    }

    @Override
    public void setAnzSrvzObj(Short anzSrvzObj) {
        if (anzSrvzObj != null) {
            setAnzSrvz(((short)anzSrvzObj));
            ws.getIndDFiscAdes().setAnzSrvz(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setAnzSrvz(((short)-1));
        }
    }

    @Override
    public AfDecimal getCnbtEcc4x100K1() {
        return dFiscAdes.getDfaCnbtEcc4x100K1().getDfaCnbtEcc4x100K1();
    }

    @Override
    public void setCnbtEcc4x100K1(AfDecimal cnbtEcc4x100K1) {
        this.dFiscAdes.getDfaCnbtEcc4x100K1().setDfaCnbtEcc4x100K1(cnbtEcc4x100K1.copy());
    }

    @Override
    public AfDecimal getCnbtEcc4x100K1Obj() {
        if (ws.getIndDFiscAdes().getCnbtEcc4x100K1() >= 0) {
            return getCnbtEcc4x100K1();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCnbtEcc4x100K1Obj(AfDecimal cnbtEcc4x100K1Obj) {
        if (cnbtEcc4x100K1Obj != null) {
            setCnbtEcc4x100K1(new AfDecimal(cnbtEcc4x100K1Obj, 15, 3));
            ws.getIndDFiscAdes().setCnbtEcc4x100K1(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setCnbtEcc4x100K1(((short)-1));
        }
    }

    @Override
    public int getCodCompAnia() {
        return dFiscAdes.getDfaCodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.dFiscAdes.setDfaCodCompAnia(codCompAnia);
    }

    @Override
    public String getCodDvs() {
        return dFiscAdes.getDfaCodDvs();
    }

    @Override
    public void setCodDvs(String codDvs) {
        this.dFiscAdes.setDfaCodDvs(codDvs);
    }

    @Override
    public String getCodDvsObj() {
        if (ws.getIndDFiscAdes().getCodDvs() >= 0) {
            return getCodDvs();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodDvsObj(String codDvsObj) {
        if (codDvsObj != null) {
            setCodDvs(codDvsObj);
            ws.getIndDFiscAdes().setCodDvs(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setCodDvs(((short)-1));
        }
    }

    @Override
    public AfDecimal getCommisDiTrasfe() {
        return dFiscAdes.getDfaCommisDiTrasfe().getDfaCommisDiTrasfe();
    }

    @Override
    public void setCommisDiTrasfe(AfDecimal commisDiTrasfe) {
        this.dFiscAdes.getDfaCommisDiTrasfe().setDfaCommisDiTrasfe(commisDiTrasfe.copy());
    }

    @Override
    public AfDecimal getCommisDiTrasfeObj() {
        if (ws.getIndDFiscAdes().getCommisDiTrasfe() >= 0) {
            return getCommisDiTrasfe();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCommisDiTrasfeObj(AfDecimal commisDiTrasfeObj) {
        if (commisDiTrasfeObj != null) {
            setCommisDiTrasfe(new AfDecimal(commisDiTrasfeObj, 15, 3));
            ws.getIndDFiscAdes().setCommisDiTrasfe(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setCommisDiTrasfe(((short)-1));
        }
    }

    @Override
    public AfDecimal getCreditoIs() {
        return dFiscAdes.getDfaCreditoIs().getDfaCreditoIs();
    }

    @Override
    public void setCreditoIs(AfDecimal creditoIs) {
        this.dFiscAdes.getDfaCreditoIs().setDfaCreditoIs(creditoIs.copy());
    }

    @Override
    public AfDecimal getCreditoIsObj() {
        if (ws.getIndDFiscAdes().getCreditoIs() >= 0) {
            return getCreditoIs();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCreditoIsObj(AfDecimal creditoIsObj) {
        if (creditoIsObj != null) {
            setCreditoIs(new AfDecimal(creditoIsObj, 15, 3));
            ws.getIndDFiscAdes().setCreditoIs(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setCreditoIs(((short)-1));
        }
    }

    @Override
    public long getDfaDsRiga() {
        return dFiscAdes.getDfaDsRiga();
    }

    @Override
    public void setDfaDsRiga(long dfaDsRiga) {
        this.dFiscAdes.setDfaDsRiga(dfaDsRiga);
    }

    @Override
    public char getDsOperSql() {
        return dFiscAdes.getDfaDsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.dFiscAdes.setDfaDsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return dFiscAdes.getDfaDsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.dFiscAdes.setDfaDsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsEndCptz() {
        return dFiscAdes.getDfaDsTsEndCptz();
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.dFiscAdes.setDfaDsTsEndCptz(dsTsEndCptz);
    }

    @Override
    public long getDsTsIniCptz() {
        return dFiscAdes.getDfaDsTsIniCptz();
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.dFiscAdes.setDfaDsTsIniCptz(dsTsIniCptz);
    }

    @Override
    public String getDsUtente() {
        return dFiscAdes.getDfaDsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.dFiscAdes.setDfaDsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return dFiscAdes.getDfaDsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.dFiscAdes.setDfaDsVer(dsVer);
    }

    @Override
    public String getDt1aCnbzDb() {
        return ws.getdFiscAdesDb().getEndCopDb();
    }

    @Override
    public void setDt1aCnbzDb(String dt1aCnbzDb) {
        this.ws.getdFiscAdesDb().setEndCopDb(dt1aCnbzDb);
    }

    @Override
    public String getDt1aCnbzDbObj() {
        if (ws.getIndDFiscAdes().getDt1aCnbz() >= 0) {
            return getDt1aCnbzDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDt1aCnbzDbObj(String dt1aCnbzDbObj) {
        if (dt1aCnbzDbObj != null) {
            setDt1aCnbzDb(dt1aCnbzDbObj);
            ws.getIndDFiscAdes().setDt1aCnbz(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setDt1aCnbz(((short)-1));
        }
    }

    @Override
    public String getDtAccnsRappFndDb() {
        return ws.getdFiscAdesDb().getIniCopDb();
    }

    @Override
    public void setDtAccnsRappFndDb(String dtAccnsRappFndDb) {
        this.ws.getdFiscAdesDb().setIniCopDb(dtAccnsRappFndDb);
    }

    @Override
    public String getDtAccnsRappFndDbObj() {
        if (ws.getIndDFiscAdes().getDtAccnsRappFnd() >= 0) {
            return getDtAccnsRappFndDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtAccnsRappFndDbObj(String dtAccnsRappFndDbObj) {
        if (dtAccnsRappFndDbObj != null) {
            setDtAccnsRappFndDb(dtAccnsRappFndDbObj);
            ws.getIndDFiscAdes().setDtAccnsRappFnd(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setDtAccnsRappFnd(((short)-1));
        }
    }

    @Override
    public String getDtCessazioneDb() {
        return ws.getdFiscAdesDb().getEsiTitDb();
    }

    @Override
    public void setDtCessazioneDb(String dtCessazioneDb) {
        this.ws.getdFiscAdesDb().setEsiTitDb(dtCessazioneDb);
    }

    @Override
    public String getDtCessazioneDbObj() {
        if (ws.getIndDFiscAdes().getDtCessazione() >= 0) {
            return getDtCessazioneDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtCessazioneDbObj(String dtCessazioneDbObj) {
        if (dtCessazioneDbObj != null) {
            setDtCessazioneDb(dtCessazioneDbObj);
            ws.getIndDFiscAdes().setDtCessazione(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setDtCessazione(((short)-1));
        }
    }

    @Override
    public String getDtEndEffDb() {
        return ws.getdFiscAdesDb().getEndEffDb();
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        this.ws.getdFiscAdesDb().setEndEffDb(dtEndEffDb);
    }

    @Override
    public String getDtIniEffDb() {
        return ws.getdFiscAdesDb().getIniEffDb();
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        this.ws.getdFiscAdesDb().setIniEffDb(dtIniEffDb);
    }

    @Override
    public char getFlApplzNewfis() {
        return dFiscAdes.getDfaFlApplzNewfis();
    }

    @Override
    public void setFlApplzNewfis(char flApplzNewfis) {
        this.dFiscAdes.setDfaFlApplzNewfis(flApplzNewfis);
    }

    @Override
    public Character getFlApplzNewfisObj() {
        if (ws.getIndDFiscAdes().getFlApplzNewfis() >= 0) {
            return ((Character)getFlApplzNewfis());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlApplzNewfisObj(Character flApplzNewfisObj) {
        if (flApplzNewfisObj != null) {
            setFlApplzNewfis(((char)flApplzNewfisObj));
            ws.getIndDFiscAdes().setFlApplzNewfis(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setFlApplzNewfis(((short)-1));
        }
    }

    @Override
    public int getIdAdes() {
        return dFiscAdes.getDfaIdAdes();
    }

    @Override
    public void setIdAdes(int idAdes) {
        this.dFiscAdes.setDfaIdAdes(idAdes);
    }

    @Override
    public int getIdDFiscAdes() {
        return dFiscAdes.getDfaIdDFiscAdes();
    }

    @Override
    public void setIdDFiscAdes(int idDFiscAdes) {
        this.dFiscAdes.setDfaIdDFiscAdes(idDFiscAdes);
    }

    @Override
    public int getIdMoviChiu() {
        return dFiscAdes.getDfaIdMoviChiu().getDfaIdMoviChiu();
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        this.dFiscAdes.getDfaIdMoviChiu().setDfaIdMoviChiu(idMoviChiu);
    }

    @Override
    public Integer getIdMoviChiuObj() {
        if (ws.getIndDFiscAdes().getIdMoviChiu() >= 0) {
            return ((Integer)getIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        if (idMoviChiuObj != null) {
            setIdMoviChiu(((int)idMoviChiuObj));
            ws.getIndDFiscAdes().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getIdMoviCrz() {
        return dFiscAdes.getDfaIdMoviCrz();
    }

    @Override
    public void setIdMoviCrz(int idMoviCrz) {
        this.dFiscAdes.setDfaIdMoviCrz(idMoviCrz);
    }

    @Override
    public AfDecimal getImpCnbtAzK1() {
        return dFiscAdes.getDfaImpCnbtAzK1().getDfaImpCnbtAzK1();
    }

    @Override
    public void setImpCnbtAzK1(AfDecimal impCnbtAzK1) {
        this.dFiscAdes.getDfaImpCnbtAzK1().setDfaImpCnbtAzK1(impCnbtAzK1.copy());
    }

    @Override
    public AfDecimal getImpCnbtAzK1Obj() {
        if (ws.getIndDFiscAdes().getImpCnbtAzK1() >= 0) {
            return getImpCnbtAzK1();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpCnbtAzK1Obj(AfDecimal impCnbtAzK1Obj) {
        if (impCnbtAzK1Obj != null) {
            setImpCnbtAzK1(new AfDecimal(impCnbtAzK1Obj, 15, 3));
            ws.getIndDFiscAdes().setImpCnbtAzK1(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setImpCnbtAzK1(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpCnbtAzK2() {
        return dFiscAdes.getDfaImpCnbtAzK2().getDfaImpCnbtAzK2();
    }

    @Override
    public void setImpCnbtAzK2(AfDecimal impCnbtAzK2) {
        this.dFiscAdes.getDfaImpCnbtAzK2().setDfaImpCnbtAzK2(impCnbtAzK2.copy());
    }

    @Override
    public AfDecimal getImpCnbtAzK2Obj() {
        if (ws.getIndDFiscAdes().getImpCnbtAzK2() >= 0) {
            return getImpCnbtAzK2();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpCnbtAzK2Obj(AfDecimal impCnbtAzK2Obj) {
        if (impCnbtAzK2Obj != null) {
            setImpCnbtAzK2(new AfDecimal(impCnbtAzK2Obj, 15, 3));
            ws.getIndDFiscAdes().setImpCnbtAzK2(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setImpCnbtAzK2(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpCnbtAzK3() {
        return dFiscAdes.getDfaImpCnbtAzK3().getDfaImpCnbtAzK3();
    }

    @Override
    public void setImpCnbtAzK3(AfDecimal impCnbtAzK3) {
        this.dFiscAdes.getDfaImpCnbtAzK3().setDfaImpCnbtAzK3(impCnbtAzK3.copy());
    }

    @Override
    public AfDecimal getImpCnbtAzK3Obj() {
        if (ws.getIndDFiscAdes().getImpCnbtAzK3() >= 0) {
            return getImpCnbtAzK3();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpCnbtAzK3Obj(AfDecimal impCnbtAzK3Obj) {
        if (impCnbtAzK3Obj != null) {
            setImpCnbtAzK3(new AfDecimal(impCnbtAzK3Obj, 15, 3));
            ws.getIndDFiscAdes().setImpCnbtAzK3(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setImpCnbtAzK3(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpCnbtIscK1() {
        return dFiscAdes.getDfaImpCnbtIscK1().getDfaImpCnbtIscK1();
    }

    @Override
    public void setImpCnbtIscK1(AfDecimal impCnbtIscK1) {
        this.dFiscAdes.getDfaImpCnbtIscK1().setDfaImpCnbtIscK1(impCnbtIscK1.copy());
    }

    @Override
    public AfDecimal getImpCnbtIscK1Obj() {
        if (ws.getIndDFiscAdes().getImpCnbtIscK1() >= 0) {
            return getImpCnbtIscK1();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpCnbtIscK1Obj(AfDecimal impCnbtIscK1Obj) {
        if (impCnbtIscK1Obj != null) {
            setImpCnbtIscK1(new AfDecimal(impCnbtIscK1Obj, 15, 3));
            ws.getIndDFiscAdes().setImpCnbtIscK1(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setImpCnbtIscK1(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpCnbtIscK2() {
        return dFiscAdes.getDfaImpCnbtIscK2().getDfaImpCnbtIscK2();
    }

    @Override
    public void setImpCnbtIscK2(AfDecimal impCnbtIscK2) {
        this.dFiscAdes.getDfaImpCnbtIscK2().setDfaImpCnbtIscK2(impCnbtIscK2.copy());
    }

    @Override
    public AfDecimal getImpCnbtIscK2Obj() {
        if (ws.getIndDFiscAdes().getImpCnbtIscK2() >= 0) {
            return getImpCnbtIscK2();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpCnbtIscK2Obj(AfDecimal impCnbtIscK2Obj) {
        if (impCnbtIscK2Obj != null) {
            setImpCnbtIscK2(new AfDecimal(impCnbtIscK2Obj, 15, 3));
            ws.getIndDFiscAdes().setImpCnbtIscK2(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setImpCnbtIscK2(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpCnbtIscK3() {
        return dFiscAdes.getDfaImpCnbtIscK3().getDfaImpCnbtIscK3();
    }

    @Override
    public void setImpCnbtIscK3(AfDecimal impCnbtIscK3) {
        this.dFiscAdes.getDfaImpCnbtIscK3().setDfaImpCnbtIscK3(impCnbtIscK3.copy());
    }

    @Override
    public AfDecimal getImpCnbtIscK3Obj() {
        if (ws.getIndDFiscAdes().getImpCnbtIscK3() >= 0) {
            return getImpCnbtIscK3();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpCnbtIscK3Obj(AfDecimal impCnbtIscK3Obj) {
        if (impCnbtIscK3Obj != null) {
            setImpCnbtIscK3(new AfDecimal(impCnbtIscK3Obj, 15, 3));
            ws.getIndDFiscAdes().setImpCnbtIscK3(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setImpCnbtIscK3(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpCnbtNdedK1() {
        return dFiscAdes.getDfaImpCnbtNdedK1().getDfaImpCnbtNdedK1();
    }

    @Override
    public void setImpCnbtNdedK1(AfDecimal impCnbtNdedK1) {
        this.dFiscAdes.getDfaImpCnbtNdedK1().setDfaImpCnbtNdedK1(impCnbtNdedK1.copy());
    }

    @Override
    public AfDecimal getImpCnbtNdedK1Obj() {
        if (ws.getIndDFiscAdes().getImpCnbtNdedK1() >= 0) {
            return getImpCnbtNdedK1();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpCnbtNdedK1Obj(AfDecimal impCnbtNdedK1Obj) {
        if (impCnbtNdedK1Obj != null) {
            setImpCnbtNdedK1(new AfDecimal(impCnbtNdedK1Obj, 15, 3));
            ws.getIndDFiscAdes().setImpCnbtNdedK1(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setImpCnbtNdedK1(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpCnbtNdedK2() {
        return dFiscAdes.getDfaImpCnbtNdedK2().getDfaImpCnbtNdedK2();
    }

    @Override
    public void setImpCnbtNdedK2(AfDecimal impCnbtNdedK2) {
        this.dFiscAdes.getDfaImpCnbtNdedK2().setDfaImpCnbtNdedK2(impCnbtNdedK2.copy());
    }

    @Override
    public AfDecimal getImpCnbtNdedK2Obj() {
        if (ws.getIndDFiscAdes().getImpCnbtNdedK2() >= 0) {
            return getImpCnbtNdedK2();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpCnbtNdedK2Obj(AfDecimal impCnbtNdedK2Obj) {
        if (impCnbtNdedK2Obj != null) {
            setImpCnbtNdedK2(new AfDecimal(impCnbtNdedK2Obj, 15, 3));
            ws.getIndDFiscAdes().setImpCnbtNdedK2(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setImpCnbtNdedK2(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpCnbtNdedK3() {
        return dFiscAdes.getDfaImpCnbtNdedK3().getDfaImpCnbtNdedK3();
    }

    @Override
    public void setImpCnbtNdedK3(AfDecimal impCnbtNdedK3) {
        this.dFiscAdes.getDfaImpCnbtNdedK3().setDfaImpCnbtNdedK3(impCnbtNdedK3.copy());
    }

    @Override
    public AfDecimal getImpCnbtNdedK3Obj() {
        if (ws.getIndDFiscAdes().getImpCnbtNdedK3() >= 0) {
            return getImpCnbtNdedK3();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpCnbtNdedK3Obj(AfDecimal impCnbtNdedK3Obj) {
        if (impCnbtNdedK3Obj != null) {
            setImpCnbtNdedK3(new AfDecimal(impCnbtNdedK3Obj, 15, 3));
            ws.getIndDFiscAdes().setImpCnbtNdedK3(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setImpCnbtNdedK3(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpCnbtTfrK1() {
        return dFiscAdes.getDfaImpCnbtTfrK1().getDfaImpCnbtTfrK1();
    }

    @Override
    public void setImpCnbtTfrK1(AfDecimal impCnbtTfrK1) {
        this.dFiscAdes.getDfaImpCnbtTfrK1().setDfaImpCnbtTfrK1(impCnbtTfrK1.copy());
    }

    @Override
    public AfDecimal getImpCnbtTfrK1Obj() {
        if (ws.getIndDFiscAdes().getImpCnbtTfrK1() >= 0) {
            return getImpCnbtTfrK1();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpCnbtTfrK1Obj(AfDecimal impCnbtTfrK1Obj) {
        if (impCnbtTfrK1Obj != null) {
            setImpCnbtTfrK1(new AfDecimal(impCnbtTfrK1Obj, 15, 3));
            ws.getIndDFiscAdes().setImpCnbtTfrK1(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setImpCnbtTfrK1(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpCnbtTfrK2() {
        return dFiscAdes.getDfaImpCnbtTfrK2().getDfaImpCnbtTfrK2();
    }

    @Override
    public void setImpCnbtTfrK2(AfDecimal impCnbtTfrK2) {
        this.dFiscAdes.getDfaImpCnbtTfrK2().setDfaImpCnbtTfrK2(impCnbtTfrK2.copy());
    }

    @Override
    public AfDecimal getImpCnbtTfrK2Obj() {
        if (ws.getIndDFiscAdes().getImpCnbtTfrK2() >= 0) {
            return getImpCnbtTfrK2();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpCnbtTfrK2Obj(AfDecimal impCnbtTfrK2Obj) {
        if (impCnbtTfrK2Obj != null) {
            setImpCnbtTfrK2(new AfDecimal(impCnbtTfrK2Obj, 15, 3));
            ws.getIndDFiscAdes().setImpCnbtTfrK2(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setImpCnbtTfrK2(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpCnbtTfrK3() {
        return dFiscAdes.getDfaImpCnbtTfrK3().getDfaImpCnbtTfrK3();
    }

    @Override
    public void setImpCnbtTfrK3(AfDecimal impCnbtTfrK3) {
        this.dFiscAdes.getDfaImpCnbtTfrK3().setDfaImpCnbtTfrK3(impCnbtTfrK3.copy());
    }

    @Override
    public AfDecimal getImpCnbtTfrK3Obj() {
        if (ws.getIndDFiscAdes().getImpCnbtTfrK3() >= 0) {
            return getImpCnbtTfrK3();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpCnbtTfrK3Obj(AfDecimal impCnbtTfrK3Obj) {
        if (impCnbtTfrK3Obj != null) {
            setImpCnbtTfrK3(new AfDecimal(impCnbtTfrK3Obj, 15, 3));
            ws.getIndDFiscAdes().setImpCnbtTfrK3(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setImpCnbtTfrK3(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpCnbtVolK1() {
        return dFiscAdes.getDfaImpCnbtVolK1().getDfaImpCnbtVolK1();
    }

    @Override
    public void setImpCnbtVolK1(AfDecimal impCnbtVolK1) {
        this.dFiscAdes.getDfaImpCnbtVolK1().setDfaImpCnbtVolK1(impCnbtVolK1.copy());
    }

    @Override
    public AfDecimal getImpCnbtVolK1Obj() {
        if (ws.getIndDFiscAdes().getImpCnbtVolK1() >= 0) {
            return getImpCnbtVolK1();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpCnbtVolK1Obj(AfDecimal impCnbtVolK1Obj) {
        if (impCnbtVolK1Obj != null) {
            setImpCnbtVolK1(new AfDecimal(impCnbtVolK1Obj, 15, 3));
            ws.getIndDFiscAdes().setImpCnbtVolK1(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setImpCnbtVolK1(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpCnbtVolK2() {
        return dFiscAdes.getDfaImpCnbtVolK2().getDfaImpCnbtVolK2();
    }

    @Override
    public void setImpCnbtVolK2(AfDecimal impCnbtVolK2) {
        this.dFiscAdes.getDfaImpCnbtVolK2().setDfaImpCnbtVolK2(impCnbtVolK2.copy());
    }

    @Override
    public AfDecimal getImpCnbtVolK2Obj() {
        if (ws.getIndDFiscAdes().getImpCnbtVolK2() >= 0) {
            return getImpCnbtVolK2();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpCnbtVolK2Obj(AfDecimal impCnbtVolK2Obj) {
        if (impCnbtVolK2Obj != null) {
            setImpCnbtVolK2(new AfDecimal(impCnbtVolK2Obj, 15, 3));
            ws.getIndDFiscAdes().setImpCnbtVolK2(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setImpCnbtVolK2(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpCnbtVolK3() {
        return dFiscAdes.getDfaImpCnbtVolK3().getDfaImpCnbtVolK3();
    }

    @Override
    public void setImpCnbtVolK3(AfDecimal impCnbtVolK3) {
        this.dFiscAdes.getDfaImpCnbtVolK3().setDfaImpCnbtVolK3(impCnbtVolK3.copy());
    }

    @Override
    public AfDecimal getImpCnbtVolK3Obj() {
        if (ws.getIndDFiscAdes().getImpCnbtVolK3() >= 0) {
            return getImpCnbtVolK3();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpCnbtVolK3Obj(AfDecimal impCnbtVolK3Obj) {
        if (impCnbtVolK3Obj != null) {
            setImpCnbtVolK3(new AfDecimal(impCnbtVolK3Obj, 15, 3));
            ws.getIndDFiscAdes().setImpCnbtVolK3(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setImpCnbtVolK3(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpEseImpstTfr() {
        return dFiscAdes.getDfaImpEseImpstTfr().getDfaImpEseImpstTfr();
    }

    @Override
    public void setImpEseImpstTfr(AfDecimal impEseImpstTfr) {
        this.dFiscAdes.getDfaImpEseImpstTfr().setDfaImpEseImpstTfr(impEseImpstTfr.copy());
    }

    @Override
    public AfDecimal getImpEseImpstTfrObj() {
        if (ws.getIndDFiscAdes().getImpEseImpstTfr() >= 0) {
            return getImpEseImpstTfr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpEseImpstTfrObj(AfDecimal impEseImpstTfrObj) {
        if (impEseImpstTfrObj != null) {
            setImpEseImpstTfr(new AfDecimal(impEseImpstTfrObj, 15, 3));
            ws.getIndDFiscAdes().setImpEseImpstTfr(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setImpEseImpstTfr(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpNetTrasferito() {
        return dFiscAdes.getDfaImpNetTrasferito().getDfaImpNetTrasferito();
    }

    @Override
    public void setImpNetTrasferito(AfDecimal impNetTrasferito) {
        this.dFiscAdes.getDfaImpNetTrasferito().setDfaImpNetTrasferito(impNetTrasferito.copy());
    }

    @Override
    public AfDecimal getImpNetTrasferitoObj() {
        if (ws.getIndDFiscAdes().getImpNetTrasferito() >= 0) {
            return getImpNetTrasferito();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpNetTrasferitoObj(AfDecimal impNetTrasferitoObj) {
        if (impNetTrasferitoObj != null) {
            setImpNetTrasferito(new AfDecimal(impNetTrasferitoObj, 15, 3));
            ws.getIndDFiscAdes().setImpNetTrasferito(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setImpNetTrasferito(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpb252Antic() {
        return dFiscAdes.getDfaImpb252Antic().getDfaImpb252Antic();
    }

    @Override
    public void setImpb252Antic(AfDecimal impb252Antic) {
        this.dFiscAdes.getDfaImpb252Antic().setDfaImpb252Antic(impb252Antic.copy());
    }

    @Override
    public AfDecimal getImpb252AnticObj() {
        if (ws.getIndDFiscAdes().getImpb252Antic() >= 0) {
            return getImpb252Antic();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpb252AnticObj(AfDecimal impb252AnticObj) {
        if (impb252AnticObj != null) {
            setImpb252Antic(new AfDecimal(impb252AnticObj, 15, 3));
            ws.getIndDFiscAdes().setImpb252Antic(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setImpb252Antic(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpb252K3() {
        return dFiscAdes.getDfaImpb252K3().getDfaImpb252K3();
    }

    @Override
    public void setImpb252K3(AfDecimal impb252K3) {
        this.dFiscAdes.getDfaImpb252K3().setDfaImpb252K3(impb252K3.copy());
    }

    @Override
    public AfDecimal getImpb252K3Obj() {
        if (ws.getIndDFiscAdes().getImpb252K3() >= 0) {
            return getImpb252K3();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpb252K3Obj(AfDecimal impb252K3Obj) {
        if (impb252K3Obj != null) {
            setImpb252K3(new AfDecimal(impb252K3Obj, 15, 3));
            ws.getIndDFiscAdes().setImpb252K3(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setImpb252K3(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbIrpefK1Anti() {
        return dFiscAdes.getDfaImpbIrpefK1Anti().getDfaImpbIrpefK1Anti();
    }

    @Override
    public void setImpbIrpefK1Anti(AfDecimal impbIrpefK1Anti) {
        this.dFiscAdes.getDfaImpbIrpefK1Anti().setDfaImpbIrpefK1Anti(impbIrpefK1Anti.copy());
    }

    @Override
    public AfDecimal getImpbIrpefK1AntiObj() {
        if (ws.getIndDFiscAdes().getImpbIrpefK1Anti() >= 0) {
            return getImpbIrpefK1Anti();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbIrpefK1AntiObj(AfDecimal impbIrpefK1AntiObj) {
        if (impbIrpefK1AntiObj != null) {
            setImpbIrpefK1Anti(new AfDecimal(impbIrpefK1AntiObj, 15, 3));
            ws.getIndDFiscAdes().setImpbIrpefK1Anti(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setImpbIrpefK1Anti(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbIrpefK2Anti() {
        return dFiscAdes.getDfaImpbIrpefK2Anti().getDfaImpbIrpefK2Anti();
    }

    @Override
    public void setImpbIrpefK2Anti(AfDecimal impbIrpefK2Anti) {
        this.dFiscAdes.getDfaImpbIrpefK2Anti().setDfaImpbIrpefK2Anti(impbIrpefK2Anti.copy());
    }

    @Override
    public AfDecimal getImpbIrpefK2AntiObj() {
        if (ws.getIndDFiscAdes().getImpbIrpefK2Anti() >= 0) {
            return getImpbIrpefK2Anti();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbIrpefK2AntiObj(AfDecimal impbIrpefK2AntiObj) {
        if (impbIrpefK2AntiObj != null) {
            setImpbIrpefK2Anti(new AfDecimal(impbIrpefK2AntiObj, 15, 3));
            ws.getIndDFiscAdes().setImpbIrpefK2Anti(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setImpbIrpefK2Anti(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbIsK2() {
        return dFiscAdes.getDfaImpbIsK2().getDfaImpbIsK2();
    }

    @Override
    public AfDecimal getImpbIsK2Antic() {
        return dFiscAdes.getDfaImpbIsK2Antic().getDfaImpbIsK2Antic();
    }

    @Override
    public void setImpbIsK2Antic(AfDecimal impbIsK2Antic) {
        this.dFiscAdes.getDfaImpbIsK2Antic().setDfaImpbIsK2Antic(impbIsK2Antic.copy());
    }

    @Override
    public AfDecimal getImpbIsK2AnticObj() {
        if (ws.getIndDFiscAdes().getImpbIsK2Antic() >= 0) {
            return getImpbIsK2Antic();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbIsK2AnticObj(AfDecimal impbIsK2AnticObj) {
        if (impbIsK2AnticObj != null) {
            setImpbIsK2Antic(new AfDecimal(impbIsK2AnticObj, 15, 3));
            ws.getIndDFiscAdes().setImpbIsK2Antic(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setImpbIsK2Antic(((short)-1));
        }
    }

    @Override
    public void setImpbIsK2(AfDecimal impbIsK2) {
        this.dFiscAdes.getDfaImpbIsK2().setDfaImpbIsK2(impbIsK2.copy());
    }

    @Override
    public AfDecimal getImpbIsK2Obj() {
        if (ws.getIndDFiscAdes().getImpbIsK2() >= 0) {
            return getImpbIsK2();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbIsK2Obj(AfDecimal impbIsK2Obj) {
        if (impbIsK2Obj != null) {
            setImpbIsK2(new AfDecimal(impbIsK2Obj, 15, 3));
            ws.getIndDFiscAdes().setImpbIsK2(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setImpbIsK2(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbIsK3() {
        return dFiscAdes.getDfaImpbIsK3().getDfaImpbIsK3();
    }

    @Override
    public AfDecimal getImpbIsK3Antic() {
        return dFiscAdes.getDfaImpbIsK3Antic().getDfaImpbIsK3Antic();
    }

    @Override
    public void setImpbIsK3Antic(AfDecimal impbIsK3Antic) {
        this.dFiscAdes.getDfaImpbIsK3Antic().setDfaImpbIsK3Antic(impbIsK3Antic.copy());
    }

    @Override
    public AfDecimal getImpbIsK3AnticObj() {
        if (ws.getIndDFiscAdes().getImpbIsK3Antic() >= 0) {
            return getImpbIsK3Antic();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbIsK3AnticObj(AfDecimal impbIsK3AnticObj) {
        if (impbIsK3AnticObj != null) {
            setImpbIsK3Antic(new AfDecimal(impbIsK3AnticObj, 15, 3));
            ws.getIndDFiscAdes().setImpbIsK3Antic(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setImpbIsK3Antic(((short)-1));
        }
    }

    @Override
    public void setImpbIsK3(AfDecimal impbIsK3) {
        this.dFiscAdes.getDfaImpbIsK3().setDfaImpbIsK3(impbIsK3.copy());
    }

    @Override
    public AfDecimal getImpbIsK3Obj() {
        if (ws.getIndDFiscAdes().getImpbIsK3() >= 0) {
            return getImpbIsK3();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbIsK3Obj(AfDecimal impbIsK3Obj) {
        if (impbIsK3Obj != null) {
            setImpbIsK3(new AfDecimal(impbIsK3Obj, 15, 3));
            ws.getIndDFiscAdes().setImpbIsK3(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setImpbIsK3(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbTfrAntic() {
        return dFiscAdes.getDfaImpbTfrAntic().getDfaImpbTfrAntic();
    }

    @Override
    public void setImpbTfrAntic(AfDecimal impbTfrAntic) {
        this.dFiscAdes.getDfaImpbTfrAntic().setDfaImpbTfrAntic(impbTfrAntic.copy());
    }

    @Override
    public AfDecimal getImpbTfrAnticObj() {
        if (ws.getIndDFiscAdes().getImpbTfrAntic() >= 0) {
            return getImpbTfrAntic();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbTfrAnticObj(AfDecimal impbTfrAnticObj) {
        if (impbTfrAnticObj != null) {
            setImpbTfrAntic(new AfDecimal(impbTfrAnticObj, 15, 3));
            ws.getIndDFiscAdes().setImpbTfrAntic(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setImpbTfrAntic(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbVis() {
        return dFiscAdes.getDfaImpbVis().getDfaImpbVis();
    }

    @Override
    public AfDecimal getImpbVisAntic() {
        return dFiscAdes.getDfaImpbVisAntic().getDfaImpbVisAntic();
    }

    @Override
    public void setImpbVisAntic(AfDecimal impbVisAntic) {
        this.dFiscAdes.getDfaImpbVisAntic().setDfaImpbVisAntic(impbVisAntic.copy());
    }

    @Override
    public AfDecimal getImpbVisAnticObj() {
        if (ws.getIndDFiscAdes().getImpbVisAntic() >= 0) {
            return getImpbVisAntic();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbVisAnticObj(AfDecimal impbVisAnticObj) {
        if (impbVisAnticObj != null) {
            setImpbVisAntic(new AfDecimal(impbVisAnticObj, 15, 3));
            ws.getIndDFiscAdes().setImpbVisAntic(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setImpbVisAntic(((short)-1));
        }
    }

    @Override
    public void setImpbVis(AfDecimal impbVis) {
        this.dFiscAdes.getDfaImpbVis().setDfaImpbVis(impbVis.copy());
    }

    @Override
    public AfDecimal getImpbVisObj() {
        if (ws.getIndDFiscAdes().getImpbVis() >= 0) {
            return getImpbVis();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbVisObj(AfDecimal impbVisObj) {
        if (impbVisObj != null) {
            setImpbVis(new AfDecimal(impbVisObj, 15, 3));
            ws.getIndDFiscAdes().setImpbVis(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setImpbVis(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpst252Antic() {
        return dFiscAdes.getDfaImpst252Antic().getDfaImpst252Antic();
    }

    @Override
    public void setImpst252Antic(AfDecimal impst252Antic) {
        this.dFiscAdes.getDfaImpst252Antic().setDfaImpst252Antic(impst252Antic.copy());
    }

    @Override
    public AfDecimal getImpst252AnticObj() {
        if (ws.getIndDFiscAdes().getImpst252Antic() >= 0) {
            return getImpst252Antic();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpst252AnticObj(AfDecimal impst252AnticObj) {
        if (impst252AnticObj != null) {
            setImpst252Antic(new AfDecimal(impst252AnticObj, 15, 3));
            ws.getIndDFiscAdes().setImpst252Antic(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setImpst252Antic(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpst252K3() {
        return dFiscAdes.getDfaImpst252K3().getDfaImpst252K3();
    }

    @Override
    public void setImpst252K3(AfDecimal impst252K3) {
        this.dFiscAdes.getDfaImpst252K3().setDfaImpst252K3(impst252K3.copy());
    }

    @Override
    public AfDecimal getImpst252K3Obj() {
        if (ws.getIndDFiscAdes().getImpst252K3() >= 0) {
            return getImpst252K3();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpst252K3Obj(AfDecimal impst252K3Obj) {
        if (impst252K3Obj != null) {
            setImpst252K3(new AfDecimal(impst252K3Obj, 15, 3));
            ws.getIndDFiscAdes().setImpst252K3(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setImpst252K3(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstIrpefK1Ant() {
        return dFiscAdes.getDfaImpstIrpefK1Ant().getDfaImpstIrpefK1Ant();
    }

    @Override
    public void setImpstIrpefK1Ant(AfDecimal impstIrpefK1Ant) {
        this.dFiscAdes.getDfaImpstIrpefK1Ant().setDfaImpstIrpefK1Ant(impstIrpefK1Ant.copy());
    }

    @Override
    public AfDecimal getImpstIrpefK1AntObj() {
        if (ws.getIndDFiscAdes().getImpstIrpefK1Ant() >= 0) {
            return getImpstIrpefK1Ant();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstIrpefK1AntObj(AfDecimal impstIrpefK1AntObj) {
        if (impstIrpefK1AntObj != null) {
            setImpstIrpefK1Ant(new AfDecimal(impstIrpefK1AntObj, 15, 3));
            ws.getIndDFiscAdes().setImpstIrpefK1Ant(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setImpstIrpefK1Ant(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstIrpefK2Ant() {
        return dFiscAdes.getDfaImpstIrpefK2Ant().getDfaImpstIrpefK2Ant();
    }

    @Override
    public void setImpstIrpefK2Ant(AfDecimal impstIrpefK2Ant) {
        this.dFiscAdes.getDfaImpstIrpefK2Ant().setDfaImpstIrpefK2Ant(impstIrpefK2Ant.copy());
    }

    @Override
    public AfDecimal getImpstIrpefK2AntObj() {
        if (ws.getIndDFiscAdes().getImpstIrpefK2Ant() >= 0) {
            return getImpstIrpefK2Ant();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstIrpefK2AntObj(AfDecimal impstIrpefK2AntObj) {
        if (impstIrpefK2AntObj != null) {
            setImpstIrpefK2Ant(new AfDecimal(impstIrpefK2AntObj, 15, 3));
            ws.getIndDFiscAdes().setImpstIrpefK2Ant(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setImpstIrpefK2Ant(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstSostK2() {
        return dFiscAdes.getDfaImpstSostK2().getDfaImpstSostK2();
    }

    @Override
    public AfDecimal getImpstSostK2Anti() {
        return dFiscAdes.getDfaImpstSostK2Anti().getDfaImpstSostK2Anti();
    }

    @Override
    public void setImpstSostK2Anti(AfDecimal impstSostK2Anti) {
        this.dFiscAdes.getDfaImpstSostK2Anti().setDfaImpstSostK2Anti(impstSostK2Anti.copy());
    }

    @Override
    public AfDecimal getImpstSostK2AntiObj() {
        if (ws.getIndDFiscAdes().getImpstSostK2Anti() >= 0) {
            return getImpstSostK2Anti();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstSostK2AntiObj(AfDecimal impstSostK2AntiObj) {
        if (impstSostK2AntiObj != null) {
            setImpstSostK2Anti(new AfDecimal(impstSostK2AntiObj, 15, 3));
            ws.getIndDFiscAdes().setImpstSostK2Anti(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setImpstSostK2Anti(((short)-1));
        }
    }

    @Override
    public void setImpstSostK2(AfDecimal impstSostK2) {
        this.dFiscAdes.getDfaImpstSostK2().setDfaImpstSostK2(impstSostK2.copy());
    }

    @Override
    public AfDecimal getImpstSostK2Obj() {
        if (ws.getIndDFiscAdes().getImpstSostK2() >= 0) {
            return getImpstSostK2();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstSostK2Obj(AfDecimal impstSostK2Obj) {
        if (impstSostK2Obj != null) {
            setImpstSostK2(new AfDecimal(impstSostK2Obj, 15, 3));
            ws.getIndDFiscAdes().setImpstSostK2(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setImpstSostK2(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstSostK3() {
        return dFiscAdes.getDfaImpstSostK3().getDfaImpstSostK3();
    }

    @Override
    public AfDecimal getImpstSostK3Anti() {
        return dFiscAdes.getDfaImpstSostK3Anti().getDfaImpstSostK3Anti();
    }

    @Override
    public void setImpstSostK3Anti(AfDecimal impstSostK3Anti) {
        this.dFiscAdes.getDfaImpstSostK3Anti().setDfaImpstSostK3Anti(impstSostK3Anti.copy());
    }

    @Override
    public AfDecimal getImpstSostK3AntiObj() {
        if (ws.getIndDFiscAdes().getImpstSostK3Anti() >= 0) {
            return getImpstSostK3Anti();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstSostK3AntiObj(AfDecimal impstSostK3AntiObj) {
        if (impstSostK3AntiObj != null) {
            setImpstSostK3Anti(new AfDecimal(impstSostK3AntiObj, 15, 3));
            ws.getIndDFiscAdes().setImpstSostK3Anti(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setImpstSostK3Anti(((short)-1));
        }
    }

    @Override
    public void setImpstSostK3(AfDecimal impstSostK3) {
        this.dFiscAdes.getDfaImpstSostK3().setDfaImpstSostK3(impstSostK3.copy());
    }

    @Override
    public AfDecimal getImpstSostK3Obj() {
        if (ws.getIndDFiscAdes().getImpstSostK3() >= 0) {
            return getImpstSostK3();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstSostK3Obj(AfDecimal impstSostK3Obj) {
        if (impstSostK3Obj != null) {
            setImpstSostK3(new AfDecimal(impstSostK3Obj, 15, 3));
            ws.getIndDFiscAdes().setImpstSostK3(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setImpstSostK3(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstTfrAntic() {
        return dFiscAdes.getDfaImpstTfrAntic().getDfaImpstTfrAntic();
    }

    @Override
    public void setImpstTfrAntic(AfDecimal impstTfrAntic) {
        this.dFiscAdes.getDfaImpstTfrAntic().setDfaImpstTfrAntic(impstTfrAntic.copy());
    }

    @Override
    public AfDecimal getImpstTfrAnticObj() {
        if (ws.getIndDFiscAdes().getImpstTfrAntic() >= 0) {
            return getImpstTfrAntic();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstTfrAnticObj(AfDecimal impstTfrAnticObj) {
        if (impstTfrAnticObj != null) {
            setImpstTfrAntic(new AfDecimal(impstTfrAnticObj, 15, 3));
            ws.getIndDFiscAdes().setImpstTfrAntic(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setImpstTfrAntic(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstVis() {
        return dFiscAdes.getDfaImpstVis().getDfaImpstVis();
    }

    @Override
    public AfDecimal getImpstVisAntic() {
        return dFiscAdes.getDfaImpstVisAntic().getDfaImpstVisAntic();
    }

    @Override
    public void setImpstVisAntic(AfDecimal impstVisAntic) {
        this.dFiscAdes.getDfaImpstVisAntic().setDfaImpstVisAntic(impstVisAntic.copy());
    }

    @Override
    public AfDecimal getImpstVisAnticObj() {
        if (ws.getIndDFiscAdes().getImpstVisAntic() >= 0) {
            return getImpstVisAntic();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstVisAnticObj(AfDecimal impstVisAnticObj) {
        if (impstVisAnticObj != null) {
            setImpstVisAntic(new AfDecimal(impstVisAnticObj, 15, 3));
            ws.getIndDFiscAdes().setImpstVisAntic(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setImpstVisAntic(((short)-1));
        }
    }

    @Override
    public void setImpstVis(AfDecimal impstVis) {
        this.dFiscAdes.getDfaImpstVis().setDfaImpstVis(impstVis.copy());
    }

    @Override
    public AfDecimal getImpstVisObj() {
        if (ws.getIndDFiscAdes().getImpstVis() >= 0) {
            return getImpstVis();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstVisObj(AfDecimal impstVisObj) {
        if (impstVisObj != null) {
            setImpstVis(new AfDecimal(impstVisObj, 15, 3));
            ws.getIndDFiscAdes().setImpstVis(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setImpstVis(((short)-1));
        }
    }

    @Override
    public AfDecimal getMatuK1() {
        return dFiscAdes.getDfaMatuK1().getDfaMatuK1();
    }

    @Override
    public void setMatuK1(AfDecimal matuK1) {
        this.dFiscAdes.getDfaMatuK1().setDfaMatuK1(matuK1.copy());
    }

    @Override
    public AfDecimal getMatuK1Obj() {
        if (ws.getIndDFiscAdes().getMatuK1() >= 0) {
            return getMatuK1();
        }
        else {
            return null;
        }
    }

    @Override
    public void setMatuK1Obj(AfDecimal matuK1Obj) {
        if (matuK1Obj != null) {
            setMatuK1(new AfDecimal(matuK1Obj, 15, 3));
            ws.getIndDFiscAdes().setMatuK1(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setMatuK1(((short)-1));
        }
    }

    @Override
    public AfDecimal getMatuK2() {
        return dFiscAdes.getDfaMatuK2().getDfaMatuK2();
    }

    @Override
    public void setMatuK2(AfDecimal matuK2) {
        this.dFiscAdes.getDfaMatuK2().setDfaMatuK2(matuK2.copy());
    }

    @Override
    public AfDecimal getMatuK2Obj() {
        if (ws.getIndDFiscAdes().getMatuK2() >= 0) {
            return getMatuK2();
        }
        else {
            return null;
        }
    }

    @Override
    public void setMatuK2Obj(AfDecimal matuK2Obj) {
        if (matuK2Obj != null) {
            setMatuK2(new AfDecimal(matuK2Obj, 15, 3));
            ws.getIndDFiscAdes().setMatuK2(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setMatuK2(((short)-1));
        }
    }

    @Override
    public AfDecimal getMatuK3() {
        return dFiscAdes.getDfaMatuK3().getDfaMatuK3();
    }

    @Override
    public void setMatuK3(AfDecimal matuK3) {
        this.dFiscAdes.getDfaMatuK3().setDfaMatuK3(matuK3.copy());
    }

    @Override
    public AfDecimal getMatuK3Obj() {
        if (ws.getIndDFiscAdes().getMatuK3() >= 0) {
            return getMatuK3();
        }
        else {
            return null;
        }
    }

    @Override
    public void setMatuK3Obj(AfDecimal matuK3Obj) {
        if (matuK3Obj != null) {
            setMatuK3(new AfDecimal(matuK3Obj, 15, 3));
            ws.getIndDFiscAdes().setMatuK3(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setMatuK3(((short)-1));
        }
    }

    @Override
    public AfDecimal getMatuResK1() {
        return dFiscAdes.getDfaMatuResK1().getDfaMatuResK1();
    }

    @Override
    public void setMatuResK1(AfDecimal matuResK1) {
        this.dFiscAdes.getDfaMatuResK1().setDfaMatuResK1(matuResK1.copy());
    }

    @Override
    public AfDecimal getMatuResK1Obj() {
        if (ws.getIndDFiscAdes().getMatuResK1() >= 0) {
            return getMatuResK1();
        }
        else {
            return null;
        }
    }

    @Override
    public void setMatuResK1Obj(AfDecimal matuResK1Obj) {
        if (matuResK1Obj != null) {
            setMatuResK1(new AfDecimal(matuResK1Obj, 15, 3));
            ws.getIndDFiscAdes().setMatuResK1(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setMatuResK1(((short)-1));
        }
    }

    @Override
    public short getMmCnbzK1() {
        return dFiscAdes.getDfaMmCnbzK1().getDfaMmCnbzK1();
    }

    @Override
    public void setMmCnbzK1(short mmCnbzK1) {
        this.dFiscAdes.getDfaMmCnbzK1().setDfaMmCnbzK1(mmCnbzK1);
    }

    @Override
    public Short getMmCnbzK1Obj() {
        if (ws.getIndDFiscAdes().getMmCnbzK1() >= 0) {
            return ((Short)getMmCnbzK1());
        }
        else {
            return null;
        }
    }

    @Override
    public void setMmCnbzK1Obj(Short mmCnbzK1Obj) {
        if (mmCnbzK1Obj != null) {
            setMmCnbzK1(((short)mmCnbzK1Obj));
            ws.getIndDFiscAdes().setMmCnbzK1(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setMmCnbzK1(((short)-1));
        }
    }

    @Override
    public short getMmCnbzK2() {
        return dFiscAdes.getDfaMmCnbzK2().getDfaMmCnbzK2();
    }

    @Override
    public void setMmCnbzK2(short mmCnbzK2) {
        this.dFiscAdes.getDfaMmCnbzK2().setDfaMmCnbzK2(mmCnbzK2);
    }

    @Override
    public Short getMmCnbzK2Obj() {
        if (ws.getIndDFiscAdes().getMmCnbzK2() >= 0) {
            return ((Short)getMmCnbzK2());
        }
        else {
            return null;
        }
    }

    @Override
    public void setMmCnbzK2Obj(Short mmCnbzK2Obj) {
        if (mmCnbzK2Obj != null) {
            setMmCnbzK2(((short)mmCnbzK2Obj));
            ws.getIndDFiscAdes().setMmCnbzK2(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setMmCnbzK2(((short)-1));
        }
    }

    @Override
    public short getMmCnbzK3() {
        return dFiscAdes.getDfaMmCnbzK3().getDfaMmCnbzK3();
    }

    @Override
    public void setMmCnbzK3(short mmCnbzK3) {
        this.dFiscAdes.getDfaMmCnbzK3().setDfaMmCnbzK3(mmCnbzK3);
    }

    @Override
    public Short getMmCnbzK3Obj() {
        if (ws.getIndDFiscAdes().getMmCnbzK3() >= 0) {
            return ((Short)getMmCnbzK3());
        }
        else {
            return null;
        }
    }

    @Override
    public void setMmCnbzK3Obj(Short mmCnbzK3Obj) {
        if (mmCnbzK3Obj != null) {
            setMmCnbzK3(((short)mmCnbzK3Obj));
            ws.getIndDFiscAdes().setMmCnbzK3(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setMmCnbzK3(((short)-1));
        }
    }

    @Override
    public AfDecimal getOnerTrasfe() {
        return dFiscAdes.getDfaOnerTrasfe().getDfaOnerTrasfe();
    }

    @Override
    public void setOnerTrasfe(AfDecimal onerTrasfe) {
        this.dFiscAdes.getDfaOnerTrasfe().setDfaOnerTrasfe(onerTrasfe.copy());
    }

    @Override
    public AfDecimal getOnerTrasfeObj() {
        if (ws.getIndDFiscAdes().getOnerTrasfe() >= 0) {
            return getOnerTrasfe();
        }
        else {
            return null;
        }
    }

    @Override
    public void setOnerTrasfeObj(AfDecimal onerTrasfeObj) {
        if (onerTrasfeObj != null) {
            setOnerTrasfe(new AfDecimal(onerTrasfeObj, 15, 3));
            ws.getIndDFiscAdes().setOnerTrasfe(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setOnerTrasfe(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcEseImpstTfr() {
        return dFiscAdes.getDfaPcEseImpstTfr().getDfaPcEseImpstTfr();
    }

    @Override
    public void setPcEseImpstTfr(AfDecimal pcEseImpstTfr) {
        this.dFiscAdes.getDfaPcEseImpstTfr().setDfaPcEseImpstTfr(pcEseImpstTfr.copy());
    }

    @Override
    public AfDecimal getPcEseImpstTfrObj() {
        if (ws.getIndDFiscAdes().getPcEseImpstTfr() >= 0) {
            return getPcEseImpstTfr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcEseImpstTfrObj(AfDecimal pcEseImpstTfrObj) {
        if (pcEseImpstTfrObj != null) {
            setPcEseImpstTfr(new AfDecimal(pcEseImpstTfrObj, 6, 3));
            ws.getIndDFiscAdes().setPcEseImpstTfr(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setPcEseImpstTfr(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcTfr() {
        return dFiscAdes.getDfaPcTfr().getDfaPcTfr();
    }

    @Override
    public void setPcTfr(AfDecimal pcTfr) {
        this.dFiscAdes.getDfaPcTfr().setDfaPcTfr(pcTfr.copy());
    }

    @Override
    public AfDecimal getPcTfrObj() {
        if (ws.getIndDFiscAdes().getPcTfr() >= 0) {
            return getPcTfr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcTfrObj(AfDecimal pcTfrObj) {
        if (pcTfrObj != null) {
            setPcTfr(new AfDecimal(pcTfrObj, 6, 3));
            ws.getIndDFiscAdes().setPcTfr(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setPcTfr(((short)-1));
        }
    }

    @Override
    public AfDecimal getRedtTassAbbatK2() {
        return dFiscAdes.getDfaRedtTassAbbatK2().getDfaRedtTassAbbatK2();
    }

    @Override
    public void setRedtTassAbbatK2(AfDecimal redtTassAbbatK2) {
        this.dFiscAdes.getDfaRedtTassAbbatK2().setDfaRedtTassAbbatK2(redtTassAbbatK2.copy());
    }

    @Override
    public AfDecimal getRedtTassAbbatK2Obj() {
        if (ws.getIndDFiscAdes().getRedtTassAbbatK2() >= 0) {
            return getRedtTassAbbatK2();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRedtTassAbbatK2Obj(AfDecimal redtTassAbbatK2Obj) {
        if (redtTassAbbatK2Obj != null) {
            setRedtTassAbbatK2(new AfDecimal(redtTassAbbatK2Obj, 15, 3));
            ws.getIndDFiscAdes().setRedtTassAbbatK2(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setRedtTassAbbatK2(((short)-1));
        }
    }

    @Override
    public AfDecimal getRedtTassAbbatK3() {
        return dFiscAdes.getDfaRedtTassAbbatK3().getDfaRedtTassAbbatK3();
    }

    @Override
    public void setRedtTassAbbatK3(AfDecimal redtTassAbbatK3) {
        this.dFiscAdes.getDfaRedtTassAbbatK3().setDfaRedtTassAbbatK3(redtTassAbbatK3.copy());
    }

    @Override
    public AfDecimal getRedtTassAbbatK3Obj() {
        if (ws.getIndDFiscAdes().getRedtTassAbbatK3() >= 0) {
            return getRedtTassAbbatK3();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRedtTassAbbatK3Obj(AfDecimal redtTassAbbatK3Obj) {
        if (redtTassAbbatK3Obj != null) {
            setRedtTassAbbatK3(new AfDecimal(redtTassAbbatK3Obj, 15, 3));
            ws.getIndDFiscAdes().setRedtTassAbbatK3(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setRedtTassAbbatK3(((short)-1));
        }
    }

    @Override
    public AfDecimal getRidzTfr() {
        return dFiscAdes.getDfaRidzTfr().getDfaRidzTfr();
    }

    @Override
    public void setRidzTfr(AfDecimal ridzTfr) {
        this.dFiscAdes.getDfaRidzTfr().setDfaRidzTfr(ridzTfr.copy());
    }

    @Override
    public AfDecimal getRidzTfrObj() {
        if (ws.getIndDFiscAdes().getRidzTfr() >= 0) {
            return getRidzTfr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRidzTfrObj(AfDecimal ridzTfrObj) {
        if (ridzTfrObj != null) {
            setRidzTfr(new AfDecimal(ridzTfrObj, 15, 3));
            ws.getIndDFiscAdes().setRidzTfr(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setRidzTfr(((short)-1));
        }
    }

    @Override
    public AfDecimal getRidzTfrSuAntic() {
        return dFiscAdes.getDfaRidzTfrSuAntic().getDfaRidzTfrSuAntic();
    }

    @Override
    public void setRidzTfrSuAntic(AfDecimal ridzTfrSuAntic) {
        this.dFiscAdes.getDfaRidzTfrSuAntic().setDfaRidzTfrSuAntic(ridzTfrSuAntic.copy());
    }

    @Override
    public AfDecimal getRidzTfrSuAnticObj() {
        if (ws.getIndDFiscAdes().getRidzTfrSuAntic() >= 0) {
            return getRidzTfrSuAntic();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRidzTfrSuAnticObj(AfDecimal ridzTfrSuAnticObj) {
        if (ridzTfrSuAnticObj != null) {
            setRidzTfrSuAntic(new AfDecimal(ridzTfrSuAnticObj, 15, 3));
            ws.getIndDFiscAdes().setRidzTfrSuAntic(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setRidzTfrSuAntic(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotAntic() {
        return dFiscAdes.getDfaTotAntic().getDfaTotAntic();
    }

    @Override
    public void setTotAntic(AfDecimal totAntic) {
        this.dFiscAdes.getDfaTotAntic().setDfaTotAntic(totAntic.copy());
    }

    @Override
    public AfDecimal getTotAnticObj() {
        if (ws.getIndDFiscAdes().getTotAntic() >= 0) {
            return getTotAntic();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotAnticObj(AfDecimal totAnticObj) {
        if (totAnticObj != null) {
            setTotAntic(new AfDecimal(totAnticObj, 15, 3));
            ws.getIndDFiscAdes().setTotAntic(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setTotAntic(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotImpst() {
        return dFiscAdes.getDfaTotImpst().getDfaTotImpst();
    }

    @Override
    public void setTotImpst(AfDecimal totImpst) {
        this.dFiscAdes.getDfaTotImpst().setDfaTotImpst(totImpst.copy());
    }

    @Override
    public AfDecimal getTotImpstObj() {
        if (ws.getIndDFiscAdes().getTotImpst() >= 0) {
            return getTotImpst();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotImpstObj(AfDecimal totImpstObj) {
        if (totImpstObj != null) {
            setTotImpst(new AfDecimal(totImpstObj, 15, 3));
            ws.getIndDFiscAdes().setTotImpst(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setTotImpst(((short)-1));
        }
    }

    @Override
    public String getTpIscFnd() {
        return dFiscAdes.getDfaTpIscFnd();
    }

    @Override
    public void setTpIscFnd(String tpIscFnd) {
        this.dFiscAdes.setDfaTpIscFnd(tpIscFnd);
    }

    @Override
    public String getTpIscFndObj() {
        if (ws.getIndDFiscAdes().getTpIscFnd() >= 0) {
            return getTpIscFnd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpIscFndObj(String tpIscFndObj) {
        if (tpIscFndObj != null) {
            setTpIscFnd(tpIscFndObj);
            ws.getIndDFiscAdes().setTpIscFnd(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setTpIscFnd(((short)-1));
        }
    }

    @Override
    public AfDecimal getUltCommisTrasfe() {
        return dFiscAdes.getDfaUltCommisTrasfe().getDfaUltCommisTrasfe();
    }

    @Override
    public void setUltCommisTrasfe(AfDecimal ultCommisTrasfe) {
        this.dFiscAdes.getDfaUltCommisTrasfe().setDfaUltCommisTrasfe(ultCommisTrasfe.copy());
    }

    @Override
    public AfDecimal getUltCommisTrasfeObj() {
        if (ws.getIndDFiscAdes().getUltCommisTrasfe() >= 0) {
            return getUltCommisTrasfe();
        }
        else {
            return null;
        }
    }

    @Override
    public void setUltCommisTrasfeObj(AfDecimal ultCommisTrasfeObj) {
        if (ultCommisTrasfeObj != null) {
            setUltCommisTrasfe(new AfDecimal(ultCommisTrasfeObj, 15, 3));
            ws.getIndDFiscAdes().setUltCommisTrasfe(((short)0));
        }
        else {
            ws.getIndDFiscAdes().setUltCommisTrasfe(((short)-1));
        }
    }
}
