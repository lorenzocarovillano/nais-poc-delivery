package it.accenture.jnais;

import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.program.GenericParam;
import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Idsv0003CampiEsito;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.Ldbv2971;
import it.accenture.jnais.ws.Lvvs0095Data;
import it.accenture.jnais.ws.occurs.W1tgaTabTran;

/**Original name: LVVS0095<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2007.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *   PROGRAMMA...... LVVS0007
 *   TIPOLOGIA...... SERVIZIO
 *   PROCESSO....... XXX
 *   FUNZIONE....... XXX
 *   DESCRIZIONE.... CONVERSIONE DATA DECORRENZA POLIZZA
 * **------------------------------------------------------------***</pre>*/
public class Lvvs0095 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lvvs0095Data ws = new Lvvs0095Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS0007
    private Ivvc0213 ivvc0213;

    //==== METHODS ====
    /**Original name: PROGRAM_LVVS0095_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: PERFORM S1000-ELABORAZIONE
        //              THRU EX-S1000
        s1000Elaborazione();
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lvvs0095 getInstance() {
        return ((Lvvs0095)Programs.getInstance(Lvvs0095.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                        IX-INDICI
        //                                             IVVC0213-TAB-OUTPUT
        //                                             WK-DATA-OUT.
        initIxIndici();
        initTabOutput();
        ws.setWkDataOutFormatted("00000000");
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET WK-POLI-OK                    TO TRUE.
        ws.getWkPolizza().setOk();
        // COB_CODE: INITIALIZE LDBV2971.
        initLdbv2971();
        // COB_CODE: MOVE IDSV0003-TIPO-MOVIMENTO
        //             TO WS-MOVIMENTO.
        ws.getWsMovimento().setWsMovimentoFormatted(idsv0003.getTipoMovimentoFormatted());
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE
        //             TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: INITIALIZE AREA-IO-TGA
        //                      WK-DATA-X-12.
        initAreaIoTga();
        initWkDataX12();
        //
        //--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
        //--> RISPETTIVE AREE DCLGEN IN WORKING
        // COB_CODE: PERFORM S1100-VALORIZZA-DCLGEN
        //              THRU S1100-VALORIZZA-DCLGEN-EX
        //           VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
        //                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //                   SPACES OR LOW-VALUE OR HIGH-VALUE.
        ws.setIxDclgen(((short)1));
        while (!(ws.getIxDclgen() > ivvc0213.getEleInfoMax() || Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias()) || Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()) || Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()))) {
            s1100ValorizzaDclgen();
            ws.setIxDclgen(Trunc.toShort(ws.getIxDclgen() + 1, 4));
        }
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //           AND ( VARIA-RIDUZI
        //           OR  RISTO-INDIVI
        //           OR COMUN-RISTOT-INCAPIENZA
        //           OR COMUN-RISTOT-INCAP)
        //               CONTINUE
        //           ELSE
        //               PERFORM S1250-VERIFICA-POLI         THRU S1250-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql() && (ws.getWsMovimento().isVariaRiduzi() || ws.getWsMovimento().isRistoIndivi() || ws.getWsMovimento().isComunRistotIncapienza() || ws.getWsMovimento().isComunRistotIncap())) {
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: PERFORM S1250-VERIFICA-POLI         THRU S1250-EX
            s1250VerificaPoli();
        }
        //
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //             END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: IF WK-POLI-OK
            //             PERFORM S1255-CALCOLA-DATA2         THRU S1255-EX
            //           END-IF
            if (ws.getWkPolizza().isOk()) {
                // COB_CODE: PERFORM S1255-CALCOLA-DATA2         THRU S1255-EX
                s1255CalcolaData2();
            }
        }
        //
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //               PERFORM S1260-CALCOLA-DIFF          THRU S1260-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM S1260-CALCOLA-DIFF          THRU S1260-EX
            s1260CalcolaDiff();
        }
    }

    /**Original name: S1100-VALORIZZA-DCLGEN<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 *     RISPETTIVE AREE DCLGEN IN WORKING
	 * ----------------------------------------------------------------*</pre>*/
    private void s1100ValorizzaDclgen() {
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-TRCH-GAR
        //                TO DTGA-AREA-TGA
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias(), ws.getIvvc0218().getAliasTrchGar())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO DTGA-AREA-TGA
            ws.setDtgaAreaTgaFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxDclgen()).getLunghezza() - 1));
        }
    }

    /**Original name: S1250-VERIFICA-POLI<br>
	 * <pre>----------------------------------------------------------------*
	 *   CALCOLA DATAULTADEGP
	 * ----------------------------------------------------------------*</pre>*/
    private void s1250VerificaPoli() {
        Ldbs2960 ldbs2960 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE STAT-OGG-BUS
        //                      LDBV2961.
        initStatOggBus();
        initLdbv2961();
        // COB_CODE: MOVE IVVC0213-ID-POLIZZA            TO STB-ID-OGG.
        ws.getStatOggBus().setStbIdOgg(ivvc0213.getIdPolizza());
        // COB_CODE: MOVE 'PO'                           TO STB-TP-OGG.
        ws.getStatOggBus().setStbTpOgg("PO");
        // COB_CODE: MOVE 'ST'                           TO STB-TP-STAT-BUS.
        ws.getStatOggBus().setStbTpStatBus("ST");
        // COB_CODE: MOVE 'AP'                           TO LDBV2961-TP-CAUS-01.
        ws.getLdbv2961().setFunz1("AP");
        // COB_CODE: MOVE 'MP'                           TO LDBV2961-TP-CAUS-02.
        ws.getLdbv2961().setFunz2("MP");
        //
        // COB_CODE: MOVE SPACES
        //             TO IDSV0003-BUFFER-WHERE-COND.
        idsv0003.setBufferWhereCond("");
        // COB_CODE: MOVE LDBV2961
        //             TO IDSV0003-BUFFER-WHERE-COND.
        idsv0003.setBufferWhereCond(ws.getLdbv2961().getLdbv2441Formatted());
        //
        // COB_CODE: MOVE 'LDBS2960'                     TO WK-CALL-PGM.
        ws.setWkCallPgm("LDBS2960");
        //
        //
        // COB_CODE:      CALL WK-CALL-PGM  USING  IDSV0003 STAT-OGG-BUS
        //           *
        //                ON EXCEPTION
        //                     SET IDSV0003-INVALID-OPER  TO TRUE
        //                END-CALL.
        try {
            ldbs2960 = Ldbs2960.getInstance();
            ldbs2960.run(idsv0003, ws.getStatOggBus());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-CALL-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: MOVE 'CALL-LDBS2960 ERRORE CHIAMATA - T0000-TRATTA-MATRICE'
            //              TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBS2960 ERRORE CHIAMATA - T0000-TRATTA-MATRICE");
            // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              CONTINUE
        //           ELSE
        //              END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
        // COB_CODE: CONTINUE
        //continue
        }
        else if (idsv0003.getSqlcode().getSqlcode() == 100) {
            // COB_CODE: IF IDSV0003-SQLCODE = +100
            //              MOVE 0                           TO IVVC0213-VAL-IMP-O
            //           ELSE
            //              END-STRING
            //           END-IF
            // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL      TO TRUE
            idsv0003.getSqlcode().setSuccessfulSql();
            // COB_CODE: SET IDSV0003-SUCCESSFUL-RC       TO TRUE
            idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
            // COB_CODE: SET WK-POLI-KO                   TO TRUE
            ws.getWkPolizza().setKo();
            // COB_CODE: MOVE ZERO                        TO IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(0);
            // COB_CODE: MOVE 0                           TO IVVC0213-VAL-IMP-O
            ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(0, 18, 7));
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER        TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
            // COB_CODE: MOVE WK-CALL-PGM           TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: STRING 'CHIAMATA LDBS2960 ;'
            //               IDSV0003-RETURN-CODE ';'
            //               IDSV0003-SQLCODE
            //               DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LDBS2960 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
        }
    }

    /**Original name: S1255-CALCOLA-DATA2<br>
	 * <pre>----------------------------------------------------------------*
	 *   CALCOLA
	 * ----------------------------------------------------------------*</pre>*/
    private void s1255CalcolaData2() {
        Ldbs2970 ldbs2970 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE LDBV2971.
        initLdbv2971();
        // COB_CODE: MOVE DTGA-ID-TRCH-DI-GAR(IVVC0213-IX-TABB)
        //                                               TO LDBV2971-ID-OGG.
        ws.getLdbv2971().setIdOgg(ws.getDtgaTabTran(ivvc0213.getIxTabb()).getLccvtga1().getDati().getWtgaIdTrchDiGar());
        // COB_CODE: MOVE 'TG'                           TO LDBV2971-TP-OGG.
        ws.getLdbv2971().setTpOgg("TG");
        //
        // COB_CODE: MOVE 'LDBS2970'                     TO WK-CALL-PGM.
        ws.setWkCallPgm("LDBS2970");
        //
        //
        // COB_CODE:      CALL WK-CALL-PGM  USING  IDSV0003 LDBV2971
        //           *
        //                ON EXCEPTION
        //                     SET IDSV0003-INVALID-OPER  TO TRUE
        //                END-CALL.
        try {
            ldbs2970 = Ldbs2970.getInstance();
            ldbs2970.run(idsv0003, ws.getLdbv2971());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-CALL-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: MOVE 'CALL-LDBS2970 ERRORE CHIAMATA - T0000-TRATTA-MATRICE'
            //              TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBS2970 ERRORE CHIAMATA - T0000-TRATTA-MATRICE");
            // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE:      IF IDSV0003-SUCCESSFUL-SQL
        //                   CONTINUE
        //                ELSE
        //                   END-STRING
        //           *
        //                END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //           OR IDSV0003-SQLCODE = -305
            //              SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
            //           ELSE
            //              SET IDSV0003-INVALID-OPER            TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isNotFound() || idsv0003.getSqlcode().getSqlcode() == -305) {
                // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-OPER            TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
            //
            // COB_CODE: MOVE WK-CALL-PGM           TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: STRING 'CHIAMATA LDBS2970 ;'
            //               IDSV0003-RETURN-CODE ';'
            //               IDSV0003-SQLCODE
            //               DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LDBS2970 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
            //
        }
    }

    /**Original name: S1260-CALCOLA-DIFF<br>
	 * <pre>----------------------------------------------------------------*
	 *   CALCOLA DATA 1
	 * ----------------------------------------------------------------*</pre>*/
    private void s1260CalcolaDiff() {
        Lccs0010 lccs0010 = null;
        GenericParam formato = null;
        GenericParam ggDiff = null;
        GenericParam codiceRitorno = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'A'                               TO FORMATO.
        ws.setFormatoFormatted("A");
        // COB_CODE: MOVE DTGA-DT-DECOR(IVVC0213-IX-TABB)   TO WK-DATA-OUT.
        ws.setWkDataOut(TruncAbs.toInt(ws.getDtgaTabTran(ivvc0213.getIxTabb()).getLccvtga1().getDati().getWtgaDtDecor(), 8));
        // COB_CODE: MOVE WK-DATA-OUT                       TO DATA-INFERIORE.
        ws.getDataInferiore().setDataInferioreFormatted(ws.getWkDataOutFormatted());
        // COB_CODE: MOVE LDBV2971-DT-INI-COP               TO DATA-SUPERIORE.
        ws.getDataSuperiore().setDataSuperioreFormatted(ws.getLdbv2971().getDtIniCopFormatted());
        // COB_CODE: MOVE 'LCCS0010'                        TO WK-CALL-PGM.
        ws.setWkCallPgm("LCCS0010");
        //
        // COB_CODE: CALL WK-CALL-PGM  USING FORMATO,
        //                                   DATA-INFERIORE,
        //                                   DATA-SUPERIORE,
        //                                   GG-DIFF,
        //                                   CODICE-RITORNO
        //           ON EXCEPTION
        //                SET IDSV0003-INVALID-OPER  TO TRUE
        //           END-CALL.
        try {
            lccs0010 = Lccs0010.getInstance();
            formato = new GenericParam(MarshalByteExt.chToBuffer(ws.getFormato()));
            ggDiff = new GenericParam(MarshalByteExt.strToBuffer(ws.getGgDiffFormatted(), Lvvs0095Data.Len.GG_DIFF));
            codiceRitorno = new GenericParam(MarshalByteExt.chToBuffer(ws.getCodiceRitorno()));
            lccs0010.run(formato, ws.getDataInferiore(), ws.getDataSuperiore(), ggDiff, codiceRitorno);
            ws.setFormatoFromBuffer(formato.getByteData());
            ws.setGgDiffFromBuffer(ggDiff.getByteData());
            ws.setCodiceRitornoFromBuffer(codiceRitorno.getByteData());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-CALL-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: MOVE 'CALL-LDBS1660 ERRORE CHIAMATA - T0000-TRATTA-MATRICE'
            //              TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBS1660 ERRORE CHIAMATA - T0000-TRATTA-MATRICE");
            // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        //
        // COB_CODE: IF CODICE-RITORNO = ZERO
        //              COMPUTE IVVC0213-VAL-IMP-O = ((GG-DIFF + WK-GG-APP) / 360)
        //           ELSE
        //              SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           END-IF.
        if (Conditions.eq(ws.getCodiceRitorno(), '0')) {
            // COB_CODE:         IF LDBV2971-FRAZ GREATER ZEROES
            //           *SIR 19310 ******
            //                      END-IF
            //           *SIR 19310 ******
            //                   ELSE
            //                      MOVE ZEROES               TO WK-GG-APP
            //                   END-IF
            if (ws.getLdbv2971().getFraz() > 0) {
                //SIR 19310 ******
                // COB_CODE: IF LDBV2971-NUM-RAT-ACCORPATE > 0
                //                                      LDBV2971-NUM-RAT-ACCORPATE)
                //           ELSE
                //             COMPUTE WK-GG-APP = (360 / LDBV2971-FRAZ)
                //           END-IF
                if (ws.getLdbv2971().getNumRatAccorpate() > 0) {
                    // COB_CODE: COMPUTE WK-GG-APP = ((360 / LDBV2971-FRAZ) *
                    //                                    LDBV2971-NUM-RAT-ACCORPATE)
                    ws.setWkGgApp(Trunc.toInt((new AfDecimal(((((double)360)) / ws.getLdbv2971().getFraz()), 3, 0)).multiply(ws.getLdbv2971().getNumRatAccorpate()), 5));
                }
                else {
                    // COB_CODE: COMPUTE WK-GG-APP = (360 / LDBV2971-FRAZ)
                    ws.setWkGgApp((new AfDecimal(((((double)360)) / ws.getLdbv2971().getFraz()), 3, 0)).toInt());
                }
                //SIR 19310 ******
            }
            else {
                // COB_CODE: MOVE ZEROES               TO WK-GG-APP
                ws.setWkGgApp(0);
            }
            // COB_CODE: COMPUTE IVVC0213-VAL-IMP-O = ((GG-DIFF + WK-GG-APP) / 360)
            ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(new AfDecimal(((((double)(ws.getGgDiff() + ws.getWkGgApp()))) / 360), 13, 7), 18, 7));
        }
        else {
            // COB_CODE: MOVE WK-CALL-PGM             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: STRING 'CHIAMATA LCCS0010 COD-RIT:'
            //                   CODICE-RITORNO ';'
            //              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LCCS0010 COD-RIT:", String.valueOf(ws.getCodiceRitorno()), ";");
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
            // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED TO TRUE
            idsv0003.getReturnCode().setFieldNotValued();
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    public void initIxIndici() {
        ws.setIxDclgen(((short)0));
        ws.setIxTabTga(((short)0));
    }

    public void initTabOutput() {
        ivvc0213.getTabOutput().setCodVariabileO("");
        ivvc0213.getTabOutput().setTpDatoO(Types.SPACE_CHAR);
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        ivvc0213.getTabOutput().setValPercO(new AfDecimal(0, 14, 9));
        ivvc0213.getTabOutput().setValStrO("");
    }

    public void initLdbv2971() {
        ws.getLdbv2971().setIdOgg(0);
        ws.getLdbv2971().setTpOgg("");
        ws.getLdbv2971().setDtIniCopFormatted("00000000");
        ws.getLdbv2971().setFraz(0);
        ws.getLdbv2971().setNumRatAccorpate(0);
    }

    public void initAreaIoTga() {
        ws.setDtgaEleTgaMax(((short)0));
        ws.getDtgaTabTranObj().fill(new W1tgaTabTran().initW1tgaTabTran());
    }

    public void initWkDataX12() {
        ws.getWkDataX12().setxAa("");
        ws.getWkDataX12().setVirogla(Types.SPACE_CHAR);
        ws.getWkDataX12().setxGg("");
    }

    public void initStatOggBus() {
        ws.getStatOggBus().setStbIdStatOggBus(0);
        ws.getStatOggBus().setStbIdOgg(0);
        ws.getStatOggBus().setStbTpOgg("");
        ws.getStatOggBus().setStbIdMoviCrz(0);
        ws.getStatOggBus().getStbIdMoviChiu().setStbIdMoviChiu(0);
        ws.getStatOggBus().setStbDtIniEff(0);
        ws.getStatOggBus().setStbDtEndEff(0);
        ws.getStatOggBus().setStbCodCompAnia(0);
        ws.getStatOggBus().setStbTpStatBus("");
        ws.getStatOggBus().setStbTpCaus("");
        ws.getStatOggBus().setStbDsRiga(0);
        ws.getStatOggBus().setStbDsOperSql(Types.SPACE_CHAR);
        ws.getStatOggBus().setStbDsVer(0);
        ws.getStatOggBus().setStbDsTsIniCptz(0);
        ws.getStatOggBus().setStbDsTsEndCptz(0);
        ws.getStatOggBus().setStbDsUtente("");
        ws.getStatOggBus().setStbDsStatoElab(Types.SPACE_CHAR);
    }

    public void initLdbv2961() {
        ws.getLdbv2961().setFunz1("");
        ws.getLdbv2961().setFunz2("");
        ws.getLdbv2961().setFunz3("");
        ws.getLdbv2961().setFunz4("");
        ws.getLdbv2961().setFunz5("");
    }
}
