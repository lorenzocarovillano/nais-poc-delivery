package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.jdbc.FieldNotMappedException;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.BilaTrchEstrDao;
import it.accenture.jnais.commons.data.to.IBilaTrchEstr;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.BilaTrchEstrIdbsb030;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idbsb030Data;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.redefines.B03AaRenCer;
import it.accenture.jnais.ws.redefines.B03Abb;
import it.accenture.jnais.ws.redefines.B03AcqExp;
import it.accenture.jnais.ws.redefines.B03AlqMargCSubrsh;
import it.accenture.jnais.ws.redefines.B03AlqMargRis;
import it.accenture.jnais.ws.redefines.B03AlqRetrT;
import it.accenture.jnais.ws.redefines.B03AntidurCalc365;
import it.accenture.jnais.ws.redefines.B03AntidurDtCalc;
import it.accenture.jnais.ws.redefines.B03AntidurRicorPrec;
import it.accenture.jnais.ws.redefines.B03CarAcqNonScon;
import it.accenture.jnais.ws.redefines.B03CarAcqPrecontato;
import it.accenture.jnais.ws.redefines.B03CarGest;
import it.accenture.jnais.ws.redefines.B03CarGestNonScon;
import it.accenture.jnais.ws.redefines.B03CarInc;
import it.accenture.jnais.ws.redefines.B03CarIncNonScon;
import it.accenture.jnais.ws.redefines.B03Carz;
import it.accenture.jnais.ws.redefines.B03CodAge;
import it.accenture.jnais.ws.redefines.B03CodCan;
import it.accenture.jnais.ws.redefines.B03CodPrdt;
import it.accenture.jnais.ws.redefines.B03CodSubage;
import it.accenture.jnais.ws.redefines.B03CoeffOpzCpt;
import it.accenture.jnais.ws.redefines.B03CoeffOpzRen;
import it.accenture.jnais.ws.redefines.B03CoeffRis1T;
import it.accenture.jnais.ws.redefines.B03CoeffRis2T;
import it.accenture.jnais.ws.redefines.B03CommisInter;
import it.accenture.jnais.ws.redefines.B03CptAsstoIniMor;
import it.accenture.jnais.ws.redefines.B03CptDtRidz;
import it.accenture.jnais.ws.redefines.B03CptDtStab;
import it.accenture.jnais.ws.redefines.B03CptRiasto;
import it.accenture.jnais.ws.redefines.B03CptRiastoEcc;
import it.accenture.jnais.ws.redefines.B03CptRshMor;
import it.accenture.jnais.ws.redefines.B03CSubrshT;
import it.accenture.jnais.ws.redefines.B03CumRiscpar;
import it.accenture.jnais.ws.redefines.B03Dir;
import it.accenture.jnais.ws.redefines.B03DirEmis;
import it.accenture.jnais.ws.redefines.B03DtDecorAdes;
import it.accenture.jnais.ws.redefines.B03DtEffCambStat;
import it.accenture.jnais.ws.redefines.B03DtEffRidz;
import it.accenture.jnais.ws.redefines.B03DtEffStab;
import it.accenture.jnais.ws.redefines.B03DtEmisCambStat;
import it.accenture.jnais.ws.redefines.B03DtEmisRidz;
import it.accenture.jnais.ws.redefines.B03DtEmisTrch;
import it.accenture.jnais.ws.redefines.B03DtIncUltPre;
import it.accenture.jnais.ws.redefines.B03DtIniValTar;
import it.accenture.jnais.ws.redefines.B03DtNasc1oAssto;
import it.accenture.jnais.ws.redefines.B03DtQtzEmis;
import it.accenture.jnais.ws.redefines.B03DtScadIntmd;
import it.accenture.jnais.ws.redefines.B03DtScadPagPre;
import it.accenture.jnais.ws.redefines.B03DtScadTrch;
import it.accenture.jnais.ws.redefines.B03DtUltPrePag;
import it.accenture.jnais.ws.redefines.B03DtUltRival;
import it.accenture.jnais.ws.redefines.B03Dur1oPerAa;
import it.accenture.jnais.ws.redefines.B03Dur1oPerGg;
import it.accenture.jnais.ws.redefines.B03Dur1oPerMm;
import it.accenture.jnais.ws.redefines.B03DurAa;
import it.accenture.jnais.ws.redefines.B03DurGarAa;
import it.accenture.jnais.ws.redefines.B03DurGarGg;
import it.accenture.jnais.ws.redefines.B03DurGarMm;
import it.accenture.jnais.ws.redefines.B03DurGg;
import it.accenture.jnais.ws.redefines.B03DurMm;
import it.accenture.jnais.ws.redefines.B03DurPagPre;
import it.accenture.jnais.ws.redefines.B03DurPagRen;
import it.accenture.jnais.ws.redefines.B03DurResDtCalc;
import it.accenture.jnais.ws.redefines.B03EtaAa1oAssto;
import it.accenture.jnais.ws.redefines.B03EtaMm1oAssto;
import it.accenture.jnais.ws.redefines.B03EtaRaggnDtCalc;
import it.accenture.jnais.ws.redefines.B03Fraz;
import it.accenture.jnais.ws.redefines.B03FrazDecrCpt;
import it.accenture.jnais.ws.redefines.B03FrazIniErogRen;
import it.accenture.jnais.ws.redefines.B03IdRichEstrazAgg;
import it.accenture.jnais.ws.redefines.B03ImpCarCasoMor;
import it.accenture.jnais.ws.redefines.B03IntrTecn;
import it.accenture.jnais.ws.redefines.B03MinGartoT;
import it.accenture.jnais.ws.redefines.B03MinTrnutT;
import it.accenture.jnais.ws.redefines.B03NsQuo;
import it.accenture.jnais.ws.redefines.B03NumPrePatt;
import it.accenture.jnais.ws.redefines.B03OverComm;
import it.accenture.jnais.ws.redefines.B03PcCarAcq;
import it.accenture.jnais.ws.redefines.B03PcCarGest;
import it.accenture.jnais.ws.redefines.B03PcCarMor;
import it.accenture.jnais.ws.redefines.B03PreAnnualizRicor;
import it.accenture.jnais.ws.redefines.B03PreCont;
import it.accenture.jnais.ws.redefines.B03PreDovIni;
import it.accenture.jnais.ws.redefines.B03PreDovRivtoT;
import it.accenture.jnais.ws.redefines.B03PrePattuitoIni;
import it.accenture.jnais.ws.redefines.B03PrePpIni;
import it.accenture.jnais.ws.redefines.B03PrePpUlt;
import it.accenture.jnais.ws.redefines.B03PreRiasto;
import it.accenture.jnais.ws.redefines.B03PreRiastoEcc;
import it.accenture.jnais.ws.redefines.B03PreRshT;
import it.accenture.jnais.ws.redefines.B03ProvAcq;
import it.accenture.jnais.ws.redefines.B03ProvAcqRicor;
import it.accenture.jnais.ws.redefines.B03ProvInc;
import it.accenture.jnais.ws.redefines.B03PrstzAggIni;
import it.accenture.jnais.ws.redefines.B03PrstzAggUlt;
import it.accenture.jnais.ws.redefines.B03PrstzIni;
import it.accenture.jnais.ws.redefines.B03PrstzT;
import it.accenture.jnais.ws.redefines.B03QtzSpZCoupDtC;
import it.accenture.jnais.ws.redefines.B03QtzSpZCoupEmis;
import it.accenture.jnais.ws.redefines.B03QtzSpZOpzDtCa;
import it.accenture.jnais.ws.redefines.B03QtzSpZOpzEmis;
import it.accenture.jnais.ws.redefines.B03QtzTotDtCalc;
import it.accenture.jnais.ws.redefines.B03QtzTotDtUltBil;
import it.accenture.jnais.ws.redefines.B03QtzTotEmis;
import it.accenture.jnais.ws.redefines.B03Rappel;
import it.accenture.jnais.ws.redefines.B03RatRen;
import it.accenture.jnais.ws.redefines.B03RemunAss;
import it.accenture.jnais.ws.redefines.B03RisAcqT;
import it.accenture.jnais.ws.redefines.B03Riscpar;
import it.accenture.jnais.ws.redefines.B03RisMatChiuPrec;
import it.accenture.jnais.ws.redefines.B03RisPuraT;
import it.accenture.jnais.ws.redefines.B03RisRiasta;
import it.accenture.jnais.ws.redefines.B03RisRiastaEcc;
import it.accenture.jnais.ws.redefines.B03RisRistorniCap;
import it.accenture.jnais.ws.redefines.B03RisSpeT;
import it.accenture.jnais.ws.redefines.B03RisZilT;
import it.accenture.jnais.ws.redefines.B03TsMedio;
import it.accenture.jnais.ws.redefines.B03TsNetT;
import it.accenture.jnais.ws.redefines.B03TsPp;
import it.accenture.jnais.ws.redefines.B03TsRendtoSppr;
import it.accenture.jnais.ws.redefines.B03TsRendtoT;
import it.accenture.jnais.ws.redefines.B03TsStabPre;
import it.accenture.jnais.ws.redefines.B03TsTariDov;
import it.accenture.jnais.ws.redefines.B03TsTariScon;
import it.accenture.jnais.ws.redefines.B03UltRm;

/**Original name: IDBSB030<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  12 SET 2014.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Idbsb030 extends Program implements IBilaTrchEstr {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private BilaTrchEstrDao bilaTrchEstrDao = new BilaTrchEstrDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Idbsb030Data ws = new Idbsb030Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: BILA-TRCH-ESTR
    private BilaTrchEstrIdbsb030 bilaTrchEstr;

    //==== METHODS ====
    /**Original name: PROGRAM_IDBSB030_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, BilaTrchEstrIdbsb030 bilaTrchEstr) {
        this.idsv0003 = idsv0003;
        this.bilaTrchEstr = bilaTrchEstr;
        // COB_CODE: PERFORM A000-INIZIO                    THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO          THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS          THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO          THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                        a300ElaboraIdEff();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                        a400ElaboraIdpEff();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO          THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS          THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO          THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                        b300ElaboraIdCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                        b400ElaboraIdpCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                        b500ElaboraIboCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                        b600ElaboraIbsCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                        b700ElaboraIdoCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //              SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-PRIMARY-KEY
                //                 PERFORM A200-ELABORA-PK          THRU A200-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO         THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS         THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO         THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.PRIMARY_KEY:// COB_CODE: PERFORM A200-ELABORA-PK          THRU A200-EX
                        a200ElaboraPk();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO         THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS         THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO         THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Idbsb030 getInstance() {
        return ((Idbsb030)Programs.getInstance(Idbsb030.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'IDBSB030'   TO IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("IDBSB030");
        // COB_CODE: MOVE 'BILA_TRCH_ESTR' TO IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("BILA_TRCH_ESTR");
        // COB_CODE: MOVE '00'                     TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                   TO   IDSV0003-SQLCODE
        //                                              IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                              IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-AGG-STORICO-SOLO-INS  OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isAggStoricoSoloIns() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-PK<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a200ElaboraPk() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-PK          THRU A210-EX
        //              WHEN IDSV0003-INSERT
        //                 PERFORM A220-INSERT-PK          THRU A220-EX
        //              WHEN IDSV0003-UPDATE
        //                 PERFORM A230-UPDATE-PK          THRU A230-EX
        //              WHEN IDSV0003-DELETE
        //                 PERFORM A240-DELETE-PK          THRU A240-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-PK          THRU A210-EX
            a210SelectPk();
        }
        else if (idsv0003.getOperazione().isInsert()) {
            // COB_CODE: PERFORM A220-INSERT-PK          THRU A220-EX
            a220InsertPk();
        }
        else if (idsv0003.getOperazione().isUpdate()) {
            // COB_CODE: PERFORM A230-UPDATE-PK          THRU A230-EX
            a230UpdatePk();
        }
        else if (idsv0003.getOperazione().isDelete()) {
            // COB_CODE: PERFORM A240-DELETE-PK          THRU A240-EX
            a240DeletePk();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A300-ELABORA-ID-EFF<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void a300ElaboraIdEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A310-SELECT-ID-EFF          THRU A310-EX
            a310SelectIdEff();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A400-ELABORA-IDP-EFF<br>*/
    private void a400ElaboraIdpEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
            a410SelectIdpEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
            a460OpenCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
            a470CloseCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
            a480FetchFirstIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
            a490FetchNextIdpEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A500-ELABORA-IBO<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a500ElaboraIbo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A510-SELECT-IBO             THRU A510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A510-SELECT-IBO             THRU A510-EX
            a510SelectIbo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
            a560OpenCursorIbo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
            a570CloseCursorIbo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
            a580FetchFirstIbo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
            a590FetchNextIbo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A600-ELABORA-IBS<br>*/
    private void a600ElaboraIbs() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A610-SELECT-IBS             THRU A610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A610-SELECT-IBS             THRU A610-EX
            a610SelectIbs();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
            a660OpenCursorIbs();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
            a670CloseCursorIbs();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
            a680FetchFirstIbs();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
            a690FetchNextIbs();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A700-ELABORA-IDO<br>*/
    private void a700ElaboraIdo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A710-SELECT-IDO                 THRU A710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A710-SELECT-IDO                 THRU A710-EX
            a710SelectIdo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
            a760OpenCursorIdo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
            a770CloseCursorIdo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
            a780FetchFirstIdo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
            a790FetchNextIdo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B300-ELABORA-ID-CPZ<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void b300ElaboraIdCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
            b310SelectIdCpz();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B400-ELABORA-IDP-CPZ<br>*/
    private void b400ElaboraIdpCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
            b410SelectIdpCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
            b460OpenCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
            b470CloseCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
            b480FetchFirstIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
            b490FetchNextIdpCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B500-ELABORA-IBO-CPZ<br>*/
    private void b500ElaboraIboCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
            b510SelectIboCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
            b560OpenCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
            b570CloseCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
            b580FetchFirstIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B600-ELABORA-IBS-CPZ<br>*/
    private void b600ElaboraIbsCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
            b610SelectIbsCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
            b660OpenCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
            b670CloseCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
            b680FetchFirstIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B700-ELABORA-IDO-CPZ<br>*/
    private void b700ElaboraIdoCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
            b710SelectIdoCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
            b760OpenCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
            b770CloseCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
            b780FetchFirstIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A210-SELECT-PK<br>
	 * <pre>----
	 * ----  gestione PK
	 * ----</pre>*/
    private void a210SelectPk() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        bilaTrchEstrDao.selectByB03IdBilaTrchEstr(bilaTrchEstr.getB03IdBilaTrchEstr(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A220-INSERT-PK<br>*/
    private void a220InsertPk() {
        // COB_CODE: PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.
        z400SeqRiga();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX
            z150ValorizzaDataServicesI();
            // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX
            z200SetIndicatoriNull();
            // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX
            z900ConvertiNToX();
            // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX
            z960LengthVchar();
            // COB_CODE: EXEC SQL
            //              INSERT
            bilaTrchEstrDao.insertRec(this);
            // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
            a100CheckReturnCode();
        }
    }

    /**Original name: A230-UPDATE-PK<br>*/
    private void a230UpdatePk() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE BILA_TRCH_ESTR SET
        bilaTrchEstrDao.updateRec(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A240-DELETE-PK<br>*/
    private void a240DeletePk() {
        // COB_CODE: EXEC SQL
        //                DELETE
        //                FROM BILA_TRCH_ESTR
        //                WHERE     ID_BILA_TRCH_ESTR = :B03-ID-BILA-TRCH-ESTR
        //           END-EXEC.
        bilaTrchEstrDao.deleteByB03IdBilaTrchEstr(bilaTrchEstr.getB03IdBilaTrchEstr());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A310-SELECT-ID-EFF<br>*/
    private void a310SelectIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A405-DECLARE-CURSOR-IDP-EFF<br>
	 * <pre>----
	 * ----  gestione IDP Effetto
	 * ----</pre>*/
    private void a405DeclareCursorIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A410-SELECT-IDP-EFF<br>*/
    private void a410SelectIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A460-OPEN-CURSOR-IDP-EFF<br>*/
    private void a460OpenCursorIdpEff() {
        // COB_CODE: PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.
        a405DeclareCursorIdpEff();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A470-CLOSE-CURSOR-IDP-EFF<br>*/
    private void a470CloseCursorIdpEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A480-FETCH-FIRST-IDP-EFF<br>*/
    private void a480FetchFirstIdpEff() {
        // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.
        a460OpenCursorIdpEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
            a490FetchNextIdpEff();
        }
    }

    /**Original name: A490-FETCH-NEXT-IDP-EFF<br>*/
    private void a490FetchNextIdpEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A505-DECLARE-CURSOR-IBO<br>
	 * <pre>----
	 * ----  gestione IBO Effetto e non
	 * ----</pre>*/
    private void a505DeclareCursorIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A510-SELECT-IBO<br>*/
    private void a510SelectIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A560-OPEN-CURSOR-IBO<br>*/
    private void a560OpenCursorIbo() {
        // COB_CODE: PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.
        a505DeclareCursorIbo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A570-CLOSE-CURSOR-IBO<br>*/
    private void a570CloseCursorIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A580-FETCH-FIRST-IBO<br>*/
    private void a580FetchFirstIbo() {
        // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.
        a560OpenCursorIbo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
            a590FetchNextIbo();
        }
    }

    /**Original name: A590-FETCH-NEXT-IBO<br>*/
    private void a590FetchNextIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A605-DECLARE-CURSOR-IBS<br>
	 * <pre>----
	 * ----  gestione IBS Effetto e non
	 * ----</pre>*/
    private void a605DeclareCursorIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A610-SELECT-IBS<br>*/
    private void a610SelectIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A660-OPEN-CURSOR-IBS<br>*/
    private void a660OpenCursorIbs() {
        // COB_CODE: PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.
        a605DeclareCursorIbs();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A670-CLOSE-CURSOR-IBS<br>*/
    private void a670CloseCursorIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A680-FETCH-FIRST-IBS<br>*/
    private void a680FetchFirstIbs() {
        // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.
        a660OpenCursorIbs();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
            a690FetchNextIbs();
        }
    }

    /**Original name: A690-FETCH-NEXT-IBS<br>*/
    private void a690FetchNextIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A705-DECLARE-CURSOR-IDO<br>
	 * <pre>----
	 * ----  gestione IDO Effetto e non
	 * ----</pre>*/
    private void a705DeclareCursorIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A710-SELECT-IDO<br>*/
    private void a710SelectIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A760-OPEN-CURSOR-IDO<br>*/
    private void a760OpenCursorIdo() {
        // COB_CODE: PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.
        a705DeclareCursorIdo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A770-CLOSE-CURSOR-IDO<br>*/
    private void a770CloseCursorIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A780-FETCH-FIRST-IDO<br>*/
    private void a780FetchFirstIdo() {
        // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.
        a760OpenCursorIdo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
            a790FetchNextIdo();
        }
    }

    /**Original name: A790-FETCH-NEXT-IDO<br>*/
    private void a790FetchNextIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B310-SELECT-ID-CPZ<br>
	 * <pre>----
	 * ----  gestione ID Competenza
	 * ----</pre>*/
    private void b310SelectIdCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B405-DECLARE-CURSOR-IDP-CPZ<br>
	 * <pre>----
	 * ----  gestione IDP Competenza
	 * ----</pre>*/
    private void b405DeclareCursorIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B410-SELECT-IDP-CPZ<br>*/
    private void b410SelectIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B460-OPEN-CURSOR-IDP-CPZ<br>*/
    private void b460OpenCursorIdpCpz() {
        // COB_CODE: PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.
        b405DeclareCursorIdpCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B470-CLOSE-CURSOR-IDP-CPZ<br>*/
    private void b470CloseCursorIdpCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B480-FETCH-FIRST-IDP-CPZ<br>*/
    private void b480FetchFirstIdpCpz() {
        // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.
        b460OpenCursorIdpCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
            b490FetchNextIdpCpz();
        }
    }

    /**Original name: B490-FETCH-NEXT-IDP-CPZ<br>*/
    private void b490FetchNextIdpCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B505-DECLARE-CURSOR-IBO-CPZ<br>
	 * <pre>----
	 * ----  gestione IBO Competenza
	 * ----</pre>*/
    private void b505DeclareCursorIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B510-SELECT-IBO-CPZ<br>*/
    private void b510SelectIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B560-OPEN-CURSOR-IBO-CPZ<br>*/
    private void b560OpenCursorIboCpz() {
        // COB_CODE: PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.
        b505DeclareCursorIboCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B570-CLOSE-CURSOR-IBO-CPZ<br>*/
    private void b570CloseCursorIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B580-FETCH-FIRST-IBO-CPZ<br>*/
    private void b580FetchFirstIboCpz() {
        // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.
        b560OpenCursorIboCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
    }

    /**Original name: B590-FETCH-NEXT-IBO-CPZ<br>*/
    private void b590FetchNextIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B605-DECLARE-CURSOR-IBS-CPZ<br>
	 * <pre>----
	 * ----  gestione IBS Competenza
	 * ----</pre>*/
    private void b605DeclareCursorIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B610-SELECT-IBS-CPZ<br>*/
    private void b610SelectIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B660-OPEN-CURSOR-IBS-CPZ<br>*/
    private void b660OpenCursorIbsCpz() {
        // COB_CODE: PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.
        b605DeclareCursorIbsCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B670-CLOSE-CURSOR-IBS-CPZ<br>*/
    private void b670CloseCursorIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B680-FETCH-FIRST-IBS-CPZ<br>*/
    private void b680FetchFirstIbsCpz() {
        // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.
        b660OpenCursorIbsCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
    }

    /**Original name: B690-FETCH-NEXT-IBS-CPZ<br>*/
    private void b690FetchNextIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B705-DECLARE-CURSOR-IDO-CPZ<br>
	 * <pre>----
	 * ----  gestione IDO Competenza
	 * ----</pre>*/
    private void b705DeclareCursorIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B710-SELECT-IDO-CPZ<br>*/
    private void b710SelectIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B760-OPEN-CURSOR-IDO-CPZ<br>*/
    private void b760OpenCursorIdoCpz() {
        // COB_CODE: PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.
        b705DeclareCursorIdoCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B770-CLOSE-CURSOR-IDO-CPZ<br>*/
    private void b770CloseCursorIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B780-FETCH-FIRST-IDO-CPZ<br>*/
    private void b780FetchFirstIdoCpz() {
        // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.
        b760OpenCursorIdoCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
    }

    /**Original name: B790-FETCH-NEXT-IDO-CPZ<br>*/
    private void b790FetchNextIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-B03-ID-RICH-ESTRAZ-AGG = -1
        //              MOVE HIGH-VALUES TO B03-ID-RICH-ESTRAZ-AGG-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getIdRichEstrazAgg() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-ID-RICH-ESTRAZ-AGG-NULL
            bilaTrchEstr.getB03IdRichEstrazAgg().setB03IdRichEstrazAggNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03IdRichEstrazAgg.Len.B03_ID_RICH_ESTRAZ_AGG_NULL));
        }
        // COB_CODE: IF IND-B03-FL-SIMULAZIONE = -1
        //              MOVE HIGH-VALUES TO B03-FL-SIMULAZIONE-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getFlSimulazione() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-FL-SIMULAZIONE-NULL
            bilaTrchEstr.setB03FlSimulazione(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-B03-DT-INI-VAL-TAR = -1
        //              MOVE HIGH-VALUES TO B03-DT-INI-VAL-TAR-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtIniValTar() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DT-INI-VAL-TAR-NULL
            bilaTrchEstr.getB03DtIniValTar().setB03DtIniValTarNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DtIniValTar.Len.B03_DT_INI_VAL_TAR_NULL));
        }
        // COB_CODE: IF IND-B03-COD-PROD = -1
        //              MOVE HIGH-VALUES TO B03-COD-PROD-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCodProd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-COD-PROD-NULL
            bilaTrchEstr.setB03CodProd(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_COD_PROD));
        }
        // COB_CODE: IF IND-B03-COD-TARI-ORGN = -1
        //              MOVE HIGH-VALUES TO B03-COD-TARI-ORGN-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCodTariOrgn() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-COD-TARI-ORGN-NULL
            bilaTrchEstr.setB03CodTariOrgn(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_COD_TARI_ORGN));
        }
        // COB_CODE: IF IND-B03-MIN-GARTO-T = -1
        //              MOVE HIGH-VALUES TO B03-MIN-GARTO-T-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getMinGartoT() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-MIN-GARTO-T-NULL
            bilaTrchEstr.getB03MinGartoT().setB03MinGartoTNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03MinGartoT.Len.B03_MIN_GARTO_T_NULL));
        }
        // COB_CODE: IF IND-B03-TP-TARI = -1
        //              MOVE HIGH-VALUES TO B03-TP-TARI-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getTpTari() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-TP-TARI-NULL
            bilaTrchEstr.setB03TpTari(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_TP_TARI));
        }
        // COB_CODE: IF IND-B03-TP-PRE = -1
        //              MOVE HIGH-VALUES TO B03-TP-PRE-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getTpPre() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-TP-PRE-NULL
            bilaTrchEstr.setB03TpPre(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-B03-TP-ADEG-PRE = -1
        //              MOVE HIGH-VALUES TO B03-TP-ADEG-PRE-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getTpAdegPre() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-TP-ADEG-PRE-NULL
            bilaTrchEstr.setB03TpAdegPre(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-B03-TP-RIVAL = -1
        //              MOVE HIGH-VALUES TO B03-TP-RIVAL-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getTpRival() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-TP-RIVAL-NULL
            bilaTrchEstr.setB03TpRival(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_TP_RIVAL));
        }
        // COB_CODE: IF IND-B03-FL-DA-TRASF = -1
        //              MOVE HIGH-VALUES TO B03-FL-DA-TRASF-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getFlDaTrasf() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-FL-DA-TRASF-NULL
            bilaTrchEstr.setB03FlDaTrasf(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-B03-FL-CAR-CONT = -1
        //              MOVE HIGH-VALUES TO B03-FL-CAR-CONT-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getFlCarCont() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-FL-CAR-CONT-NULL
            bilaTrchEstr.setB03FlCarCont(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-B03-FL-PRE-DA-RIS = -1
        //              MOVE HIGH-VALUES TO B03-FL-PRE-DA-RIS-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getFlPreDaRis() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-FL-PRE-DA-RIS-NULL
            bilaTrchEstr.setB03FlPreDaRis(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-B03-FL-PRE-AGG = -1
        //              MOVE HIGH-VALUES TO B03-FL-PRE-AGG-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getFlPreAgg() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-FL-PRE-AGG-NULL
            bilaTrchEstr.setB03FlPreAgg(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-B03-TP-TRCH = -1
        //              MOVE HIGH-VALUES TO B03-TP-TRCH-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getTpTrch() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-TP-TRCH-NULL
            bilaTrchEstr.setB03TpTrch(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_TP_TRCH));
        }
        // COB_CODE: IF IND-B03-TP-TST = -1
        //              MOVE HIGH-VALUES TO B03-TP-TST-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getTpTst() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-TP-TST-NULL
            bilaTrchEstr.setB03TpTst(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_TP_TST));
        }
        // COB_CODE: IF IND-B03-COD-CONV = -1
        //              MOVE HIGH-VALUES TO B03-COD-CONV-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCodConv() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-COD-CONV-NULL
            bilaTrchEstr.setB03CodConv(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_COD_CONV));
        }
        // COB_CODE: IF IND-B03-DT-DECOR-ADES = -1
        //              MOVE HIGH-VALUES TO B03-DT-DECOR-ADES-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtDecorAdes() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DT-DECOR-ADES-NULL
            bilaTrchEstr.getB03DtDecorAdes().setB03DtDecorAdesNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DtDecorAdes.Len.B03_DT_DECOR_ADES_NULL));
        }
        // COB_CODE: IF IND-B03-DT-EMIS-TRCH = -1
        //              MOVE HIGH-VALUES TO B03-DT-EMIS-TRCH-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtEmisTrch() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DT-EMIS-TRCH-NULL
            bilaTrchEstr.getB03DtEmisTrch().setB03DtEmisTrchNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DtEmisTrch.Len.B03_DT_EMIS_TRCH_NULL));
        }
        // COB_CODE: IF IND-B03-DT-SCAD-TRCH = -1
        //              MOVE HIGH-VALUES TO B03-DT-SCAD-TRCH-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtScadTrch() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DT-SCAD-TRCH-NULL
            bilaTrchEstr.getB03DtScadTrch().setB03DtScadTrchNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DtScadTrch.Len.B03_DT_SCAD_TRCH_NULL));
        }
        // COB_CODE: IF IND-B03-DT-SCAD-INTMD = -1
        //              MOVE HIGH-VALUES TO B03-DT-SCAD-INTMD-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtScadIntmd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DT-SCAD-INTMD-NULL
            bilaTrchEstr.getB03DtScadIntmd().setB03DtScadIntmdNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DtScadIntmd.Len.B03_DT_SCAD_INTMD_NULL));
        }
        // COB_CODE: IF IND-B03-DT-SCAD-PAG-PRE = -1
        //              MOVE HIGH-VALUES TO B03-DT-SCAD-PAG-PRE-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtScadPagPre() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DT-SCAD-PAG-PRE-NULL
            bilaTrchEstr.getB03DtScadPagPre().setB03DtScadPagPreNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DtScadPagPre.Len.B03_DT_SCAD_PAG_PRE_NULL));
        }
        // COB_CODE: IF IND-B03-DT-ULT-PRE-PAG = -1
        //              MOVE HIGH-VALUES TO B03-DT-ULT-PRE-PAG-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtUltPrePag() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DT-ULT-PRE-PAG-NULL
            bilaTrchEstr.getB03DtUltPrePag().setB03DtUltPrePagNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DtUltPrePag.Len.B03_DT_ULT_PRE_PAG_NULL));
        }
        // COB_CODE: IF IND-B03-DT-NASC-1O-ASSTO = -1
        //              MOVE HIGH-VALUES TO B03-DT-NASC-1O-ASSTO-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtNasc1oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DT-NASC-1O-ASSTO-NULL
            bilaTrchEstr.getB03DtNasc1oAssto().setB03DtNasc1oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DtNasc1oAssto.Len.B03_DT_NASC1O_ASSTO_NULL));
        }
        // COB_CODE: IF IND-B03-SEX-1O-ASSTO = -1
        //              MOVE HIGH-VALUES TO B03-SEX-1O-ASSTO-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getSex1oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-SEX-1O-ASSTO-NULL
            bilaTrchEstr.setB03Sex1oAssto(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-B03-ETA-AA-1O-ASSTO = -1
        //              MOVE HIGH-VALUES TO B03-ETA-AA-1O-ASSTO-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getEtaAa1oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-ETA-AA-1O-ASSTO-NULL
            bilaTrchEstr.getB03EtaAa1oAssto().setB03EtaAa1oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03EtaAa1oAssto.Len.B03_ETA_AA1O_ASSTO_NULL));
        }
        // COB_CODE: IF IND-B03-ETA-MM-1O-ASSTO = -1
        //              MOVE HIGH-VALUES TO B03-ETA-MM-1O-ASSTO-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getEtaMm1oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-ETA-MM-1O-ASSTO-NULL
            bilaTrchEstr.getB03EtaMm1oAssto().setB03EtaMm1oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03EtaMm1oAssto.Len.B03_ETA_MM1O_ASSTO_NULL));
        }
        // COB_CODE: IF IND-B03-ETA-RAGGN-DT-CALC = -1
        //              MOVE HIGH-VALUES TO B03-ETA-RAGGN-DT-CALC-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getEtaRaggnDtCalc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-ETA-RAGGN-DT-CALC-NULL
            bilaTrchEstr.getB03EtaRaggnDtCalc().setB03EtaRaggnDtCalcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03EtaRaggnDtCalc.Len.B03_ETA_RAGGN_DT_CALC_NULL));
        }
        // COB_CODE: IF IND-B03-DUR-AA = -1
        //              MOVE HIGH-VALUES TO B03-DUR-AA-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDurAa() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DUR-AA-NULL
            bilaTrchEstr.getB03DurAa().setB03DurAaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DurAa.Len.B03_DUR_AA_NULL));
        }
        // COB_CODE: IF IND-B03-DUR-MM = -1
        //              MOVE HIGH-VALUES TO B03-DUR-MM-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDurMm() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DUR-MM-NULL
            bilaTrchEstr.getB03DurMm().setB03DurMmNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DurMm.Len.B03_DUR_MM_NULL));
        }
        // COB_CODE: IF IND-B03-DUR-GG = -1
        //              MOVE HIGH-VALUES TO B03-DUR-GG-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDurGg() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DUR-GG-NULL
            bilaTrchEstr.getB03DurGg().setB03DurGgNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DurGg.Len.B03_DUR_GG_NULL));
        }
        // COB_CODE: IF IND-B03-DUR-1O-PER-AA = -1
        //              MOVE HIGH-VALUES TO B03-DUR-1O-PER-AA-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDur1oPerAa() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DUR-1O-PER-AA-NULL
            bilaTrchEstr.getB03Dur1oPerAa().setB03Dur1oPerAaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03Dur1oPerAa.Len.B03_DUR1O_PER_AA_NULL));
        }
        // COB_CODE: IF IND-B03-DUR-1O-PER-MM = -1
        //              MOVE HIGH-VALUES TO B03-DUR-1O-PER-MM-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDur1oPerMm() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DUR-1O-PER-MM-NULL
            bilaTrchEstr.getB03Dur1oPerMm().setB03Dur1oPerMmNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03Dur1oPerMm.Len.B03_DUR1O_PER_MM_NULL));
        }
        // COB_CODE: IF IND-B03-DUR-1O-PER-GG = -1
        //              MOVE HIGH-VALUES TO B03-DUR-1O-PER-GG-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDur1oPerGg() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DUR-1O-PER-GG-NULL
            bilaTrchEstr.getB03Dur1oPerGg().setB03Dur1oPerGgNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03Dur1oPerGg.Len.B03_DUR1O_PER_GG_NULL));
        }
        // COB_CODE: IF IND-B03-ANTIDUR-RICOR-PREC = -1
        //              MOVE HIGH-VALUES TO B03-ANTIDUR-RICOR-PREC-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getAntidurRicorPrec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-ANTIDUR-RICOR-PREC-NULL
            bilaTrchEstr.getB03AntidurRicorPrec().setB03AntidurRicorPrecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03AntidurRicorPrec.Len.B03_ANTIDUR_RICOR_PREC_NULL));
        }
        // COB_CODE: IF IND-B03-ANTIDUR-DT-CALC = -1
        //              MOVE HIGH-VALUES TO B03-ANTIDUR-DT-CALC-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getAntidurDtCalc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-ANTIDUR-DT-CALC-NULL
            bilaTrchEstr.getB03AntidurDtCalc().setB03AntidurDtCalcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03AntidurDtCalc.Len.B03_ANTIDUR_DT_CALC_NULL));
        }
        // COB_CODE: IF IND-B03-DUR-RES-DT-CALC = -1
        //              MOVE HIGH-VALUES TO B03-DUR-RES-DT-CALC-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDurResDtCalc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DUR-RES-DT-CALC-NULL
            bilaTrchEstr.getB03DurResDtCalc().setB03DurResDtCalcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DurResDtCalc.Len.B03_DUR_RES_DT_CALC_NULL));
        }
        // COB_CODE: IF IND-B03-DT-EFF-CAMB-STAT = -1
        //              MOVE HIGH-VALUES TO B03-DT-EFF-CAMB-STAT-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtEffCambStat() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DT-EFF-CAMB-STAT-NULL
            bilaTrchEstr.getB03DtEffCambStat().setB03DtEffCambStatNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DtEffCambStat.Len.B03_DT_EFF_CAMB_STAT_NULL));
        }
        // COB_CODE: IF IND-B03-DT-EMIS-CAMB-STAT = -1
        //              MOVE HIGH-VALUES TO B03-DT-EMIS-CAMB-STAT-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtEmisCambStat() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DT-EMIS-CAMB-STAT-NULL
            bilaTrchEstr.getB03DtEmisCambStat().setB03DtEmisCambStatNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DtEmisCambStat.Len.B03_DT_EMIS_CAMB_STAT_NULL));
        }
        // COB_CODE: IF IND-B03-DT-EFF-STAB = -1
        //              MOVE HIGH-VALUES TO B03-DT-EFF-STAB-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtEffStab() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DT-EFF-STAB-NULL
            bilaTrchEstr.getB03DtEffStab().setB03DtEffStabNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DtEffStab.Len.B03_DT_EFF_STAB_NULL));
        }
        // COB_CODE: IF IND-B03-CPT-DT-STAB = -1
        //              MOVE HIGH-VALUES TO B03-CPT-DT-STAB-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCptDtStab() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-CPT-DT-STAB-NULL
            bilaTrchEstr.getB03CptDtStab().setB03CptDtStabNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03CptDtStab.Len.B03_CPT_DT_STAB_NULL));
        }
        // COB_CODE: IF IND-B03-DT-EFF-RIDZ = -1
        //              MOVE HIGH-VALUES TO B03-DT-EFF-RIDZ-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtEffRidz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DT-EFF-RIDZ-NULL
            bilaTrchEstr.getB03DtEffRidz().setB03DtEffRidzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DtEffRidz.Len.B03_DT_EFF_RIDZ_NULL));
        }
        // COB_CODE: IF IND-B03-DT-EMIS-RIDZ = -1
        //              MOVE HIGH-VALUES TO B03-DT-EMIS-RIDZ-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtEmisRidz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DT-EMIS-RIDZ-NULL
            bilaTrchEstr.getB03DtEmisRidz().setB03DtEmisRidzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DtEmisRidz.Len.B03_DT_EMIS_RIDZ_NULL));
        }
        // COB_CODE: IF IND-B03-CPT-DT-RIDZ = -1
        //              MOVE HIGH-VALUES TO B03-CPT-DT-RIDZ-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCptDtRidz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-CPT-DT-RIDZ-NULL
            bilaTrchEstr.getB03CptDtRidz().setB03CptDtRidzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03CptDtRidz.Len.B03_CPT_DT_RIDZ_NULL));
        }
        // COB_CODE: IF IND-B03-FRAZ = -1
        //              MOVE HIGH-VALUES TO B03-FRAZ-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getFraz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-FRAZ-NULL
            bilaTrchEstr.getB03Fraz().setB03FrazNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03Fraz.Len.B03_FRAZ_NULL));
        }
        // COB_CODE: IF IND-B03-DUR-PAG-PRE = -1
        //              MOVE HIGH-VALUES TO B03-DUR-PAG-PRE-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDurPagPre() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DUR-PAG-PRE-NULL
            bilaTrchEstr.getB03DurPagPre().setB03DurPagPreNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DurPagPre.Len.B03_DUR_PAG_PRE_NULL));
        }
        // COB_CODE: IF IND-B03-NUM-PRE-PATT = -1
        //              MOVE HIGH-VALUES TO B03-NUM-PRE-PATT-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getNumPrePatt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-NUM-PRE-PATT-NULL
            bilaTrchEstr.getB03NumPrePatt().setB03NumPrePattNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03NumPrePatt.Len.B03_NUM_PRE_PATT_NULL));
        }
        // COB_CODE: IF IND-B03-FRAZ-INI-EROG-REN = -1
        //              MOVE HIGH-VALUES TO B03-FRAZ-INI-EROG-REN-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getFrazIniErogRen() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-FRAZ-INI-EROG-REN-NULL
            bilaTrchEstr.getB03FrazIniErogRen().setB03FrazIniErogRenNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03FrazIniErogRen.Len.B03_FRAZ_INI_EROG_REN_NULL));
        }
        // COB_CODE: IF IND-B03-AA-REN-CER = -1
        //              MOVE HIGH-VALUES TO B03-AA-REN-CER-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getAaRenCer() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-AA-REN-CER-NULL
            bilaTrchEstr.getB03AaRenCer().setB03AaRenCerNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03AaRenCer.Len.B03_AA_REN_CER_NULL));
        }
        // COB_CODE: IF IND-B03-RAT-REN = -1
        //              MOVE HIGH-VALUES TO B03-RAT-REN-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getRatRen() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-RAT-REN-NULL
            bilaTrchEstr.getB03RatRen().setB03RatRenNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03RatRen.Len.B03_RAT_REN_NULL));
        }
        // COB_CODE: IF IND-B03-COD-DIV = -1
        //              MOVE HIGH-VALUES TO B03-COD-DIV-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCodDiv() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-COD-DIV-NULL
            bilaTrchEstr.setB03CodDiv(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_COD_DIV));
        }
        // COB_CODE: IF IND-B03-RISCPAR = -1
        //              MOVE HIGH-VALUES TO B03-RISCPAR-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getRiscpar() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-RISCPAR-NULL
            bilaTrchEstr.getB03Riscpar().setB03RiscparNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03Riscpar.Len.B03_RISCPAR_NULL));
        }
        // COB_CODE: IF IND-B03-CUM-RISCPAR = -1
        //              MOVE HIGH-VALUES TO B03-CUM-RISCPAR-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCumRiscpar() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-CUM-RISCPAR-NULL
            bilaTrchEstr.getB03CumRiscpar().setB03CumRiscparNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03CumRiscpar.Len.B03_CUM_RISCPAR_NULL));
        }
        // COB_CODE: IF IND-B03-ULT-RM = -1
        //              MOVE HIGH-VALUES TO B03-ULT-RM-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getUltRm() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-ULT-RM-NULL
            bilaTrchEstr.getB03UltRm().setB03UltRmNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03UltRm.Len.B03_ULT_RM_NULL));
        }
        // COB_CODE: IF IND-B03-TS-RENDTO-T = -1
        //              MOVE HIGH-VALUES TO B03-TS-RENDTO-T-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getTsRendtoT() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-TS-RENDTO-T-NULL
            bilaTrchEstr.getB03TsRendtoT().setB03TsRendtoTNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03TsRendtoT.Len.B03_TS_RENDTO_T_NULL));
        }
        // COB_CODE: IF IND-B03-ALQ-RETR-T = -1
        //              MOVE HIGH-VALUES TO B03-ALQ-RETR-T-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getAlqRetrT() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-ALQ-RETR-T-NULL
            bilaTrchEstr.getB03AlqRetrT().setB03AlqRetrTNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03AlqRetrT.Len.B03_ALQ_RETR_T_NULL));
        }
        // COB_CODE: IF IND-B03-MIN-TRNUT-T = -1
        //              MOVE HIGH-VALUES TO B03-MIN-TRNUT-T-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getMinTrnutT() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-MIN-TRNUT-T-NULL
            bilaTrchEstr.getB03MinTrnutT().setB03MinTrnutTNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03MinTrnutT.Len.B03_MIN_TRNUT_T_NULL));
        }
        // COB_CODE: IF IND-B03-TS-NET-T = -1
        //              MOVE HIGH-VALUES TO B03-TS-NET-T-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getTsNetT() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-TS-NET-T-NULL
            bilaTrchEstr.getB03TsNetT().setB03TsNetTNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03TsNetT.Len.B03_TS_NET_T_NULL));
        }
        // COB_CODE: IF IND-B03-DT-ULT-RIVAL = -1
        //              MOVE HIGH-VALUES TO B03-DT-ULT-RIVAL-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtUltRival() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DT-ULT-RIVAL-NULL
            bilaTrchEstr.getB03DtUltRival().setB03DtUltRivalNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DtUltRival.Len.B03_DT_ULT_RIVAL_NULL));
        }
        // COB_CODE: IF IND-B03-PRSTZ-INI = -1
        //              MOVE HIGH-VALUES TO B03-PRSTZ-INI-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getPrstzIni() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-PRSTZ-INI-NULL
            bilaTrchEstr.getB03PrstzIni().setB03PrstzIniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03PrstzIni.Len.B03_PRSTZ_INI_NULL));
        }
        // COB_CODE: IF IND-B03-PRSTZ-AGG-INI = -1
        //              MOVE HIGH-VALUES TO B03-PRSTZ-AGG-INI-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getPrstzAggIni() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-PRSTZ-AGG-INI-NULL
            bilaTrchEstr.getB03PrstzAggIni().setB03PrstzAggIniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03PrstzAggIni.Len.B03_PRSTZ_AGG_INI_NULL));
        }
        // COB_CODE: IF IND-B03-PRSTZ-AGG-ULT = -1
        //              MOVE HIGH-VALUES TO B03-PRSTZ-AGG-ULT-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getPrstzAggUlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-PRSTZ-AGG-ULT-NULL
            bilaTrchEstr.getB03PrstzAggUlt().setB03PrstzAggUltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03PrstzAggUlt.Len.B03_PRSTZ_AGG_ULT_NULL));
        }
        // COB_CODE: IF IND-B03-RAPPEL = -1
        //              MOVE HIGH-VALUES TO B03-RAPPEL-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getRappel() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-RAPPEL-NULL
            bilaTrchEstr.getB03Rappel().setB03RappelNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03Rappel.Len.B03_RAPPEL_NULL));
        }
        // COB_CODE: IF IND-B03-PRE-PATTUITO-INI = -1
        //              MOVE HIGH-VALUES TO B03-PRE-PATTUITO-INI-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getPrePattuitoIni() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-PRE-PATTUITO-INI-NULL
            bilaTrchEstr.getB03PrePattuitoIni().setB03PrePattuitoIniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03PrePattuitoIni.Len.B03_PRE_PATTUITO_INI_NULL));
        }
        // COB_CODE: IF IND-B03-PRE-DOV-INI = -1
        //              MOVE HIGH-VALUES TO B03-PRE-DOV-INI-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getPreDovIni() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-PRE-DOV-INI-NULL
            bilaTrchEstr.getB03PreDovIni().setB03PreDovIniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03PreDovIni.Len.B03_PRE_DOV_INI_NULL));
        }
        // COB_CODE: IF IND-B03-PRE-DOV-RIVTO-T = -1
        //              MOVE HIGH-VALUES TO B03-PRE-DOV-RIVTO-T-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getPreDovRivtoT() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-PRE-DOV-RIVTO-T-NULL
            bilaTrchEstr.getB03PreDovRivtoT().setB03PreDovRivtoTNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03PreDovRivtoT.Len.B03_PRE_DOV_RIVTO_T_NULL));
        }
        // COB_CODE: IF IND-B03-PRE-ANNUALIZ-RICOR = -1
        //              MOVE HIGH-VALUES TO B03-PRE-ANNUALIZ-RICOR-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getPreAnnualizRicor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-PRE-ANNUALIZ-RICOR-NULL
            bilaTrchEstr.getB03PreAnnualizRicor().setB03PreAnnualizRicorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03PreAnnualizRicor.Len.B03_PRE_ANNUALIZ_RICOR_NULL));
        }
        // COB_CODE: IF IND-B03-PRE-CONT = -1
        //              MOVE HIGH-VALUES TO B03-PRE-CONT-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getPreCont() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-PRE-CONT-NULL
            bilaTrchEstr.getB03PreCont().setB03PreContNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03PreCont.Len.B03_PRE_CONT_NULL));
        }
        // COB_CODE: IF IND-B03-PRE-PP-INI = -1
        //              MOVE HIGH-VALUES TO B03-PRE-PP-INI-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getPrePpIni() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-PRE-PP-INI-NULL
            bilaTrchEstr.getB03PrePpIni().setB03PrePpIniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03PrePpIni.Len.B03_PRE_PP_INI_NULL));
        }
        // COB_CODE: IF IND-B03-RIS-PURA-T = -1
        //              MOVE HIGH-VALUES TO B03-RIS-PURA-T-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getRisPuraT() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-RIS-PURA-T-NULL
            bilaTrchEstr.getB03RisPuraT().setB03RisPuraTNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03RisPuraT.Len.B03_RIS_PURA_T_NULL));
        }
        // COB_CODE: IF IND-B03-PROV-ACQ = -1
        //              MOVE HIGH-VALUES TO B03-PROV-ACQ-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getProvAcq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-PROV-ACQ-NULL
            bilaTrchEstr.getB03ProvAcq().setB03ProvAcqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03ProvAcq.Len.B03_PROV_ACQ_NULL));
        }
        // COB_CODE: IF IND-B03-PROV-ACQ-RICOR = -1
        //              MOVE HIGH-VALUES TO B03-PROV-ACQ-RICOR-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getProvAcqRicor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-PROV-ACQ-RICOR-NULL
            bilaTrchEstr.getB03ProvAcqRicor().setB03ProvAcqRicorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03ProvAcqRicor.Len.B03_PROV_ACQ_RICOR_NULL));
        }
        // COB_CODE: IF IND-B03-PROV-INC = -1
        //              MOVE HIGH-VALUES TO B03-PROV-INC-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getProvInc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-PROV-INC-NULL
            bilaTrchEstr.getB03ProvInc().setB03ProvIncNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03ProvInc.Len.B03_PROV_INC_NULL));
        }
        // COB_CODE: IF IND-B03-CAR-ACQ-NON-SCON = -1
        //              MOVE HIGH-VALUES TO B03-CAR-ACQ-NON-SCON-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCarAcqNonScon() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-CAR-ACQ-NON-SCON-NULL
            bilaTrchEstr.getB03CarAcqNonScon().setB03CarAcqNonSconNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03CarAcqNonScon.Len.B03_CAR_ACQ_NON_SCON_NULL));
        }
        // COB_CODE: IF IND-B03-OVER-COMM = -1
        //              MOVE HIGH-VALUES TO B03-OVER-COMM-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getOverComm() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-OVER-COMM-NULL
            bilaTrchEstr.getB03OverComm().setB03OverCommNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03OverComm.Len.B03_OVER_COMM_NULL));
        }
        // COB_CODE: IF IND-B03-CAR-ACQ-PRECONTATO = -1
        //              MOVE HIGH-VALUES TO B03-CAR-ACQ-PRECONTATO-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCarAcqPrecontato() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-CAR-ACQ-PRECONTATO-NULL
            bilaTrchEstr.getB03CarAcqPrecontato().setB03CarAcqPrecontatoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03CarAcqPrecontato.Len.B03_CAR_ACQ_PRECONTATO_NULL));
        }
        // COB_CODE: IF IND-B03-RIS-ACQ-T = -1
        //              MOVE HIGH-VALUES TO B03-RIS-ACQ-T-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getRisAcqT() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-RIS-ACQ-T-NULL
            bilaTrchEstr.getB03RisAcqT().setB03RisAcqTNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03RisAcqT.Len.B03_RIS_ACQ_T_NULL));
        }
        // COB_CODE: IF IND-B03-RIS-ZIL-T = -1
        //              MOVE HIGH-VALUES TO B03-RIS-ZIL-T-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getRisZilT() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-RIS-ZIL-T-NULL
            bilaTrchEstr.getB03RisZilT().setB03RisZilTNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03RisZilT.Len.B03_RIS_ZIL_T_NULL));
        }
        // COB_CODE: IF IND-B03-CAR-GEST-NON-SCON = -1
        //              MOVE HIGH-VALUES TO B03-CAR-GEST-NON-SCON-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCarGestNonScon() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-CAR-GEST-NON-SCON-NULL
            bilaTrchEstr.getB03CarGestNonScon().setB03CarGestNonSconNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03CarGestNonScon.Len.B03_CAR_GEST_NON_SCON_NULL));
        }
        // COB_CODE: IF IND-B03-CAR-GEST = -1
        //              MOVE HIGH-VALUES TO B03-CAR-GEST-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCarGest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-CAR-GEST-NULL
            bilaTrchEstr.getB03CarGest().setB03CarGestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03CarGest.Len.B03_CAR_GEST_NULL));
        }
        // COB_CODE: IF IND-B03-RIS-SPE-T = -1
        //              MOVE HIGH-VALUES TO B03-RIS-SPE-T-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getRisSpeT() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-RIS-SPE-T-NULL
            bilaTrchEstr.getB03RisSpeT().setB03RisSpeTNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03RisSpeT.Len.B03_RIS_SPE_T_NULL));
        }
        // COB_CODE: IF IND-B03-CAR-INC-NON-SCON = -1
        //              MOVE HIGH-VALUES TO B03-CAR-INC-NON-SCON-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCarIncNonScon() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-CAR-INC-NON-SCON-NULL
            bilaTrchEstr.getB03CarIncNonScon().setB03CarIncNonSconNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03CarIncNonScon.Len.B03_CAR_INC_NON_SCON_NULL));
        }
        // COB_CODE: IF IND-B03-CAR-INC = -1
        //              MOVE HIGH-VALUES TO B03-CAR-INC-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCarInc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-CAR-INC-NULL
            bilaTrchEstr.getB03CarInc().setB03CarIncNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03CarInc.Len.B03_CAR_INC_NULL));
        }
        // COB_CODE: IF IND-B03-RIS-RISTORNI-CAP = -1
        //              MOVE HIGH-VALUES TO B03-RIS-RISTORNI-CAP-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getRisRistorniCap() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-RIS-RISTORNI-CAP-NULL
            bilaTrchEstr.getB03RisRistorniCap().setB03RisRistorniCapNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03RisRistorniCap.Len.B03_RIS_RISTORNI_CAP_NULL));
        }
        // COB_CODE: IF IND-B03-INTR-TECN = -1
        //              MOVE HIGH-VALUES TO B03-INTR-TECN-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getIntrTecn() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-INTR-TECN-NULL
            bilaTrchEstr.getB03IntrTecn().setB03IntrTecnNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03IntrTecn.Len.B03_INTR_TECN_NULL));
        }
        // COB_CODE: IF IND-B03-CPT-RSH-MOR = -1
        //              MOVE HIGH-VALUES TO B03-CPT-RSH-MOR-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCptRshMor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-CPT-RSH-MOR-NULL
            bilaTrchEstr.getB03CptRshMor().setB03CptRshMorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03CptRshMor.Len.B03_CPT_RSH_MOR_NULL));
        }
        // COB_CODE: IF IND-B03-C-SUBRSH-T = -1
        //              MOVE HIGH-VALUES TO B03-C-SUBRSH-T-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getcSubrshT() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-C-SUBRSH-T-NULL
            bilaTrchEstr.getB03CSubrshT().setB03CSubrshTNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03CSubrshT.Len.B03_C_SUBRSH_T_NULL));
        }
        // COB_CODE: IF IND-B03-PRE-RSH-T = -1
        //              MOVE HIGH-VALUES TO B03-PRE-RSH-T-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getPreRshT() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-PRE-RSH-T-NULL
            bilaTrchEstr.getB03PreRshT().setB03PreRshTNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03PreRshT.Len.B03_PRE_RSH_T_NULL));
        }
        // COB_CODE: IF IND-B03-ALQ-MARG-RIS = -1
        //              MOVE HIGH-VALUES TO B03-ALQ-MARG-RIS-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getAlqMargRis() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-ALQ-MARG-RIS-NULL
            bilaTrchEstr.getB03AlqMargRis().setB03AlqMargRisNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03AlqMargRis.Len.B03_ALQ_MARG_RIS_NULL));
        }
        // COB_CODE: IF IND-B03-ALQ-MARG-C-SUBRSH = -1
        //              MOVE HIGH-VALUES TO B03-ALQ-MARG-C-SUBRSH-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getAlqMargCSubrsh() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-ALQ-MARG-C-SUBRSH-NULL
            bilaTrchEstr.getB03AlqMargCSubrsh().setB03AlqMargCSubrshNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03AlqMargCSubrsh.Len.B03_ALQ_MARG_C_SUBRSH_NULL));
        }
        // COB_CODE: IF IND-B03-TS-RENDTO-SPPR = -1
        //              MOVE HIGH-VALUES TO B03-TS-RENDTO-SPPR-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getTsRendtoSppr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-TS-RENDTO-SPPR-NULL
            bilaTrchEstr.getB03TsRendtoSppr().setB03TsRendtoSpprNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03TsRendtoSppr.Len.B03_TS_RENDTO_SPPR_NULL));
        }
        // COB_CODE: IF IND-B03-TP-IAS = -1
        //              MOVE HIGH-VALUES TO B03-TP-IAS-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getTpIas() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-TP-IAS-NULL
            bilaTrchEstr.setB03TpIas(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_TP_IAS));
        }
        // COB_CODE: IF IND-B03-NS-QUO = -1
        //              MOVE HIGH-VALUES TO B03-NS-QUO-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getNsQuo() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-NS-QUO-NULL
            bilaTrchEstr.getB03NsQuo().setB03NsQuoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03NsQuo.Len.B03_NS_QUO_NULL));
        }
        // COB_CODE: IF IND-B03-TS-MEDIO = -1
        //              MOVE HIGH-VALUES TO B03-TS-MEDIO-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getTsMedio() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-TS-MEDIO-NULL
            bilaTrchEstr.getB03TsMedio().setB03TsMedioNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03TsMedio.Len.B03_TS_MEDIO_NULL));
        }
        // COB_CODE: IF IND-B03-CPT-RIASTO = -1
        //              MOVE HIGH-VALUES TO B03-CPT-RIASTO-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCptRiasto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-CPT-RIASTO-NULL
            bilaTrchEstr.getB03CptRiasto().setB03CptRiastoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03CptRiasto.Len.B03_CPT_RIASTO_NULL));
        }
        // COB_CODE: IF IND-B03-PRE-RIASTO = -1
        //              MOVE HIGH-VALUES TO B03-PRE-RIASTO-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getPreRiasto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-PRE-RIASTO-NULL
            bilaTrchEstr.getB03PreRiasto().setB03PreRiastoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03PreRiasto.Len.B03_PRE_RIASTO_NULL));
        }
        // COB_CODE: IF IND-B03-RIS-RIASTA = -1
        //              MOVE HIGH-VALUES TO B03-RIS-RIASTA-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getRisRiasta() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-RIS-RIASTA-NULL
            bilaTrchEstr.getB03RisRiasta().setB03RisRiastaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03RisRiasta.Len.B03_RIS_RIASTA_NULL));
        }
        // COB_CODE: IF IND-B03-CPT-RIASTO-ECC = -1
        //              MOVE HIGH-VALUES TO B03-CPT-RIASTO-ECC-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCptRiastoEcc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-CPT-RIASTO-ECC-NULL
            bilaTrchEstr.getB03CptRiastoEcc().setB03CptRiastoEccNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03CptRiastoEcc.Len.B03_CPT_RIASTO_ECC_NULL));
        }
        // COB_CODE: IF IND-B03-PRE-RIASTO-ECC = -1
        //              MOVE HIGH-VALUES TO B03-PRE-RIASTO-ECC-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getPreRiastoEcc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-PRE-RIASTO-ECC-NULL
            bilaTrchEstr.getB03PreRiastoEcc().setB03PreRiastoEccNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03PreRiastoEcc.Len.B03_PRE_RIASTO_ECC_NULL));
        }
        // COB_CODE: IF IND-B03-RIS-RIASTA-ECC = -1
        //              MOVE HIGH-VALUES TO B03-RIS-RIASTA-ECC-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getRisRiastaEcc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-RIS-RIASTA-ECC-NULL
            bilaTrchEstr.getB03RisRiastaEcc().setB03RisRiastaEccNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03RisRiastaEcc.Len.B03_RIS_RIASTA_ECC_NULL));
        }
        // COB_CODE: IF IND-B03-COD-AGE = -1
        //              MOVE HIGH-VALUES TO B03-COD-AGE-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCodAge() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-COD-AGE-NULL
            bilaTrchEstr.getB03CodAge().setB03CodAgeNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03CodAge.Len.B03_COD_AGE_NULL));
        }
        // COB_CODE: IF IND-B03-COD-SUBAGE = -1
        //              MOVE HIGH-VALUES TO B03-COD-SUBAGE-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCodSubage() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-COD-SUBAGE-NULL
            bilaTrchEstr.getB03CodSubage().setB03CodSubageNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03CodSubage.Len.B03_COD_SUBAGE_NULL));
        }
        // COB_CODE: IF IND-B03-COD-CAN = -1
        //              MOVE HIGH-VALUES TO B03-COD-CAN-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCodCan() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-COD-CAN-NULL
            bilaTrchEstr.getB03CodCan().setB03CodCanNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03CodCan.Len.B03_COD_CAN_NULL));
        }
        // COB_CODE: IF IND-B03-IB-POLI = -1
        //              MOVE HIGH-VALUES TO B03-IB-POLI-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getIbPoli() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-IB-POLI-NULL
            bilaTrchEstr.setB03IbPoli(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_IB_POLI));
        }
        // COB_CODE: IF IND-B03-IB-ADES = -1
        //              MOVE HIGH-VALUES TO B03-IB-ADES-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getIbAdes() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-IB-ADES-NULL
            bilaTrchEstr.setB03IbAdes(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_IB_ADES));
        }
        // COB_CODE: IF IND-B03-IB-TRCH-DI-GAR = -1
        //              MOVE HIGH-VALUES TO B03-IB-TRCH-DI-GAR-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getIbTrchDiGar() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-IB-TRCH-DI-GAR-NULL
            bilaTrchEstr.setB03IbTrchDiGar(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_IB_TRCH_DI_GAR));
        }
        // COB_CODE: IF IND-B03-TP-PRSTZ = -1
        //              MOVE HIGH-VALUES TO B03-TP-PRSTZ-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getTpPrstz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-TP-PRSTZ-NULL
            bilaTrchEstr.setB03TpPrstz(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_TP_PRSTZ));
        }
        // COB_CODE: IF IND-B03-TP-TRASF = -1
        //              MOVE HIGH-VALUES TO B03-TP-TRASF-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getTpTrasf() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-TP-TRASF-NULL
            bilaTrchEstr.setB03TpTrasf(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_TP_TRASF));
        }
        // COB_CODE: IF IND-B03-PP-INVRIO-TARI = -1
        //              MOVE HIGH-VALUES TO B03-PP-INVRIO-TARI-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getPpInvrioTari() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-PP-INVRIO-TARI-NULL
            bilaTrchEstr.setB03PpInvrioTari(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-B03-COEFF-OPZ-REN = -1
        //              MOVE HIGH-VALUES TO B03-COEFF-OPZ-REN-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCoeffOpzRen() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-COEFF-OPZ-REN-NULL
            bilaTrchEstr.getB03CoeffOpzRen().setB03CoeffOpzRenNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03CoeffOpzRen.Len.B03_COEFF_OPZ_REN_NULL));
        }
        // COB_CODE: IF IND-B03-COEFF-OPZ-CPT = -1
        //              MOVE HIGH-VALUES TO B03-COEFF-OPZ-CPT-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCoeffOpzCpt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-COEFF-OPZ-CPT-NULL
            bilaTrchEstr.getB03CoeffOpzCpt().setB03CoeffOpzCptNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03CoeffOpzCpt.Len.B03_COEFF_OPZ_CPT_NULL));
        }
        // COB_CODE: IF IND-B03-DUR-PAG-REN = -1
        //              MOVE HIGH-VALUES TO B03-DUR-PAG-REN-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDurPagRen() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DUR-PAG-REN-NULL
            bilaTrchEstr.getB03DurPagRen().setB03DurPagRenNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DurPagRen.Len.B03_DUR_PAG_REN_NULL));
        }
        // COB_CODE: IF IND-B03-VLT = -1
        //              MOVE HIGH-VALUES TO B03-VLT-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getVlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-VLT-NULL
            bilaTrchEstr.setB03Vlt(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_VLT));
        }
        // COB_CODE: IF IND-B03-RIS-MAT-CHIU-PREC = -1
        //              MOVE HIGH-VALUES TO B03-RIS-MAT-CHIU-PREC-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getRisMatChiuPrec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-RIS-MAT-CHIU-PREC-NULL
            bilaTrchEstr.getB03RisMatChiuPrec().setB03RisMatChiuPrecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03RisMatChiuPrec.Len.B03_RIS_MAT_CHIU_PREC_NULL));
        }
        // COB_CODE: IF IND-B03-COD-FND = -1
        //              MOVE HIGH-VALUES TO B03-COD-FND-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCodFnd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-COD-FND-NULL
            bilaTrchEstr.setB03CodFnd(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_COD_FND));
        }
        // COB_CODE: IF IND-B03-PRSTZ-T = -1
        //              MOVE HIGH-VALUES TO B03-PRSTZ-T-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getPrstzT() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-PRSTZ-T-NULL
            bilaTrchEstr.getB03PrstzT().setB03PrstzTNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03PrstzT.Len.B03_PRSTZ_T_NULL));
        }
        // COB_CODE: IF IND-B03-TS-TARI-DOV = -1
        //              MOVE HIGH-VALUES TO B03-TS-TARI-DOV-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getTsTariDov() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-TS-TARI-DOV-NULL
            bilaTrchEstr.getB03TsTariDov().setB03TsTariDovNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03TsTariDov.Len.B03_TS_TARI_DOV_NULL));
        }
        // COB_CODE: IF IND-B03-TS-TARI-SCON = -1
        //              MOVE HIGH-VALUES TO B03-TS-TARI-SCON-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getTsTariScon() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-TS-TARI-SCON-NULL
            bilaTrchEstr.getB03TsTariScon().setB03TsTariSconNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03TsTariScon.Len.B03_TS_TARI_SCON_NULL));
        }
        // COB_CODE: IF IND-B03-TS-PP = -1
        //              MOVE HIGH-VALUES TO B03-TS-PP-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getTsPp() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-TS-PP-NULL
            bilaTrchEstr.getB03TsPp().setB03TsPpNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03TsPp.Len.B03_TS_PP_NULL));
        }
        // COB_CODE: IF IND-B03-COEFF-RIS-1-T = -1
        //              MOVE HIGH-VALUES TO B03-COEFF-RIS-1-T-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCoeffRis1T() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-COEFF-RIS-1-T-NULL
            bilaTrchEstr.getB03CoeffRis1T().setB03CoeffRis1TNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03CoeffRis1T.Len.B03_COEFF_RIS1_T_NULL));
        }
        // COB_CODE: IF IND-B03-COEFF-RIS-2-T = -1
        //              MOVE HIGH-VALUES TO B03-COEFF-RIS-2-T-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCoeffRis2T() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-COEFF-RIS-2-T-NULL
            bilaTrchEstr.getB03CoeffRis2T().setB03CoeffRis2TNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03CoeffRis2T.Len.B03_COEFF_RIS2_T_NULL));
        }
        // COB_CODE: IF IND-B03-ABB = -1
        //              MOVE HIGH-VALUES TO B03-ABB-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getAbb() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-ABB-NULL
            bilaTrchEstr.getB03Abb().setB03AbbNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03Abb.Len.B03_ABB_NULL));
        }
        // COB_CODE: IF IND-B03-TP-COASS = -1
        //              MOVE HIGH-VALUES TO B03-TP-COASS-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getTpCoass() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-TP-COASS-NULL
            bilaTrchEstr.setB03TpCoass(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_TP_COASS));
        }
        // COB_CODE: IF IND-B03-TRAT-RIASS = -1
        //              MOVE HIGH-VALUES TO B03-TRAT-RIASS-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getTratRiass() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-TRAT-RIASS-NULL
            bilaTrchEstr.setB03TratRiass(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_TRAT_RIASS));
        }
        // COB_CODE: IF IND-B03-TRAT-RIASS-ECC = -1
        //              MOVE HIGH-VALUES TO B03-TRAT-RIASS-ECC-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getTratRiassEcc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-TRAT-RIASS-ECC-NULL
            bilaTrchEstr.setB03TratRiassEcc(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_TRAT_RIASS_ECC));
        }
        // COB_CODE: IF IND-B03-TP-RGM-FISC = -1
        //              MOVE HIGH-VALUES TO B03-TP-RGM-FISC-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getTpRgmFisc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-TP-RGM-FISC-NULL
            bilaTrchEstr.setB03TpRgmFisc(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_TP_RGM_FISC));
        }
        // COB_CODE: IF IND-B03-DUR-GAR-AA = -1
        //              MOVE HIGH-VALUES TO B03-DUR-GAR-AA-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDurGarAa() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DUR-GAR-AA-NULL
            bilaTrchEstr.getB03DurGarAa().setB03DurGarAaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DurGarAa.Len.B03_DUR_GAR_AA_NULL));
        }
        // COB_CODE: IF IND-B03-DUR-GAR-MM = -1
        //              MOVE HIGH-VALUES TO B03-DUR-GAR-MM-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDurGarMm() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DUR-GAR-MM-NULL
            bilaTrchEstr.getB03DurGarMm().setB03DurGarMmNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DurGarMm.Len.B03_DUR_GAR_MM_NULL));
        }
        // COB_CODE: IF IND-B03-DUR-GAR-GG = -1
        //              MOVE HIGH-VALUES TO B03-DUR-GAR-GG-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDurGarGg() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DUR-GAR-GG-NULL
            bilaTrchEstr.getB03DurGarGg().setB03DurGarGgNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DurGarGg.Len.B03_DUR_GAR_GG_NULL));
        }
        // COB_CODE: IF IND-B03-ANTIDUR-CALC-365 = -1
        //              MOVE HIGH-VALUES TO B03-ANTIDUR-CALC-365-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getAntidurCalc365() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-ANTIDUR-CALC-365-NULL
            bilaTrchEstr.getB03AntidurCalc365().setB03AntidurCalc365Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03AntidurCalc365.Len.B03_ANTIDUR_CALC365_NULL));
        }
        // COB_CODE: IF IND-B03-COD-FISC-CNTR = -1
        //              MOVE HIGH-VALUES TO B03-COD-FISC-CNTR-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCodFiscCntr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-COD-FISC-CNTR-NULL
            bilaTrchEstr.setB03CodFiscCntr(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_COD_FISC_CNTR));
        }
        // COB_CODE: IF IND-B03-COD-FISC-ASSTO1 = -1
        //              MOVE HIGH-VALUES TO B03-COD-FISC-ASSTO1-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCodFiscAssto1() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-COD-FISC-ASSTO1-NULL
            bilaTrchEstr.setB03CodFiscAssto1(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_COD_FISC_ASSTO1));
        }
        // COB_CODE: IF IND-B03-COD-FISC-ASSTO2 = -1
        //              MOVE HIGH-VALUES TO B03-COD-FISC-ASSTO2-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCodFiscAssto2() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-COD-FISC-ASSTO2-NULL
            bilaTrchEstr.setB03CodFiscAssto2(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_COD_FISC_ASSTO2));
        }
        // COB_CODE: IF IND-B03-COD-FISC-ASSTO3 = -1
        //              MOVE HIGH-VALUES TO B03-COD-FISC-ASSTO3-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCodFiscAssto3() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-COD-FISC-ASSTO3-NULL
            bilaTrchEstr.setB03CodFiscAssto3(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_COD_FISC_ASSTO3));
        }
        // COB_CODE: IF IND-B03-CAUS-SCON = -1
        //              MOVE HIGH-VALUES TO B03-CAUS-SCON
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCausScon() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-CAUS-SCON
            bilaTrchEstr.setB03CausScon(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_CAUS_SCON));
        }
        // COB_CODE: IF IND-B03-EMIT-TIT-OPZ = -1
        //              MOVE HIGH-VALUES TO B03-EMIT-TIT-OPZ
        //           END-IF
        if (ws.getIndBilaTrchEstr().getEmitTitOpz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-EMIT-TIT-OPZ
            bilaTrchEstr.setB03EmitTitOpz(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_EMIT_TIT_OPZ));
        }
        // COB_CODE: IF IND-B03-QTZ-SP-Z-COUP-EMIS = -1
        //              MOVE HIGH-VALUES TO B03-QTZ-SP-Z-COUP-EMIS-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getQtzSpZCoupEmis() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-QTZ-SP-Z-COUP-EMIS-NULL
            bilaTrchEstr.getB03QtzSpZCoupEmis().setB03QtzSpZCoupEmisNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03QtzSpZCoupEmis.Len.B03_QTZ_SP_Z_COUP_EMIS_NULL));
        }
        // COB_CODE: IF IND-B03-QTZ-SP-Z-OPZ-EMIS = -1
        //              MOVE HIGH-VALUES TO B03-QTZ-SP-Z-OPZ-EMIS-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getQtzSpZOpzEmis() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-QTZ-SP-Z-OPZ-EMIS-NULL
            bilaTrchEstr.getB03QtzSpZOpzEmis().setB03QtzSpZOpzEmisNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03QtzSpZOpzEmis.Len.B03_QTZ_SP_Z_OPZ_EMIS_NULL));
        }
        // COB_CODE: IF IND-B03-QTZ-SP-Z-COUP-DT-C = -1
        //              MOVE HIGH-VALUES TO B03-QTZ-SP-Z-COUP-DT-C-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getQtzSpZCoupDtC() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-QTZ-SP-Z-COUP-DT-C-NULL
            bilaTrchEstr.getB03QtzSpZCoupDtC().setB03QtzSpZCoupDtCNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03QtzSpZCoupDtC.Len.B03_QTZ_SP_Z_COUP_DT_C_NULL));
        }
        // COB_CODE: IF IND-B03-QTZ-SP-Z-OPZ-DT-CA = -1
        //              MOVE HIGH-VALUES TO B03-QTZ-SP-Z-OPZ-DT-CA-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getQtzSpZOpzDtCa() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-QTZ-SP-Z-OPZ-DT-CA-NULL
            bilaTrchEstr.getB03QtzSpZOpzDtCa().setB03QtzSpZOpzDtCaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03QtzSpZOpzDtCa.Len.B03_QTZ_SP_Z_OPZ_DT_CA_NULL));
        }
        // COB_CODE: IF IND-B03-QTZ-TOT-EMIS = -1
        //              MOVE HIGH-VALUES TO B03-QTZ-TOT-EMIS-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getQtzTotEmis() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-QTZ-TOT-EMIS-NULL
            bilaTrchEstr.getB03QtzTotEmis().setB03QtzTotEmisNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03QtzTotEmis.Len.B03_QTZ_TOT_EMIS_NULL));
        }
        // COB_CODE: IF IND-B03-QTZ-TOT-DT-CALC = -1
        //              MOVE HIGH-VALUES TO B03-QTZ-TOT-DT-CALC-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getQtzTotDtCalc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-QTZ-TOT-DT-CALC-NULL
            bilaTrchEstr.getB03QtzTotDtCalc().setB03QtzTotDtCalcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03QtzTotDtCalc.Len.B03_QTZ_TOT_DT_CALC_NULL));
        }
        // COB_CODE: IF IND-B03-QTZ-TOT-DT-ULT-BIL = -1
        //              MOVE HIGH-VALUES TO B03-QTZ-TOT-DT-ULT-BIL-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getQtzTotDtUltBil() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-QTZ-TOT-DT-ULT-BIL-NULL
            bilaTrchEstr.getB03QtzTotDtUltBil().setB03QtzTotDtUltBilNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03QtzTotDtUltBil.Len.B03_QTZ_TOT_DT_ULT_BIL_NULL));
        }
        // COB_CODE: IF IND-B03-DT-QTZ-EMIS = -1
        //              MOVE HIGH-VALUES TO B03-DT-QTZ-EMIS-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtQtzEmis() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DT-QTZ-EMIS-NULL
            bilaTrchEstr.getB03DtQtzEmis().setB03DtQtzEmisNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DtQtzEmis.Len.B03_DT_QTZ_EMIS_NULL));
        }
        // COB_CODE: IF IND-B03-PC-CAR-GEST = -1
        //              MOVE HIGH-VALUES TO B03-PC-CAR-GEST-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getPcCarGest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-PC-CAR-GEST-NULL
            bilaTrchEstr.getB03PcCarGest().setB03PcCarGestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03PcCarGest.Len.B03_PC_CAR_GEST_NULL));
        }
        // COB_CODE: IF IND-B03-PC-CAR-ACQ = -1
        //              MOVE HIGH-VALUES TO B03-PC-CAR-ACQ-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getPcCarAcq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-PC-CAR-ACQ-NULL
            bilaTrchEstr.getB03PcCarAcq().setB03PcCarAcqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03PcCarAcq.Len.B03_PC_CAR_ACQ_NULL));
        }
        // COB_CODE: IF IND-B03-IMP-CAR-CASO-MOR = -1
        //              MOVE HIGH-VALUES TO B03-IMP-CAR-CASO-MOR-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getImpCarCasoMor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-IMP-CAR-CASO-MOR-NULL
            bilaTrchEstr.getB03ImpCarCasoMor().setB03ImpCarCasoMorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03ImpCarCasoMor.Len.B03_IMP_CAR_CASO_MOR_NULL));
        }
        // COB_CODE: IF IND-B03-PC-CAR-MOR = -1
        //              MOVE HIGH-VALUES TO B03-PC-CAR-MOR-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getPcCarMor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-PC-CAR-MOR-NULL
            bilaTrchEstr.getB03PcCarMor().setB03PcCarMorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03PcCarMor.Len.B03_PC_CAR_MOR_NULL));
        }
        // COB_CODE: IF IND-B03-TP-VERS = -1
        //              MOVE HIGH-VALUES TO B03-TP-VERS-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getTpVers() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-TP-VERS-NULL
            bilaTrchEstr.setB03TpVers(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-B03-FL-SWITCH = -1
        //              MOVE HIGH-VALUES TO B03-FL-SWITCH-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getFlSwitch() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-FL-SWITCH-NULL
            bilaTrchEstr.setB03FlSwitch(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-B03-FL-IAS = -1
        //              MOVE HIGH-VALUES TO B03-FL-IAS-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getFlIas() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-FL-IAS-NULL
            bilaTrchEstr.setB03FlIas(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-B03-DIR = -1
        //              MOVE HIGH-VALUES TO B03-DIR-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDir() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DIR-NULL
            bilaTrchEstr.getB03Dir().setB03DirNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03Dir.Len.B03_DIR_NULL));
        }
        // COB_CODE: IF IND-B03-TP-COP-CASO-MOR = -1
        //              MOVE HIGH-VALUES TO B03-TP-COP-CASO-MOR-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getTpCopCasoMor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-TP-COP-CASO-MOR-NULL
            bilaTrchEstr.setB03TpCopCasoMor(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_TP_COP_CASO_MOR));
        }
        // COB_CODE: IF IND-B03-MET-RISC-SPCL = -1
        //              MOVE HIGH-VALUES TO B03-MET-RISC-SPCL-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getMetRiscSpcl() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-MET-RISC-SPCL-NULL
            bilaTrchEstr.getB03MetRiscSpcl().setB03MetRiscSpclNull(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-B03-TP-STAT-INVST = -1
        //              MOVE HIGH-VALUES TO B03-TP-STAT-INVST-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getTpStatInvst() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-TP-STAT-INVST-NULL
            bilaTrchEstr.setB03TpStatInvst(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_TP_STAT_INVST));
        }
        // COB_CODE: IF IND-B03-COD-PRDT = -1
        //              MOVE HIGH-VALUES TO B03-COD-PRDT-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCodPrdt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-COD-PRDT-NULL
            bilaTrchEstr.getB03CodPrdt().setB03CodPrdtNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03CodPrdt.Len.B03_COD_PRDT_NULL));
        }
        // COB_CODE: IF IND-B03-STAT-ASSTO-1 = -1
        //              MOVE HIGH-VALUES TO B03-STAT-ASSTO-1-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getStatAssto1() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-STAT-ASSTO-1-NULL
            bilaTrchEstr.setB03StatAssto1(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-B03-STAT-ASSTO-2 = -1
        //              MOVE HIGH-VALUES TO B03-STAT-ASSTO-2-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getStatAssto2() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-STAT-ASSTO-2-NULL
            bilaTrchEstr.setB03StatAssto2(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-B03-STAT-ASSTO-3 = -1
        //              MOVE HIGH-VALUES TO B03-STAT-ASSTO-3-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getStatAssto3() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-STAT-ASSTO-3-NULL
            bilaTrchEstr.setB03StatAssto3(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-B03-CPT-ASSTO-INI-MOR = -1
        //              MOVE HIGH-VALUES TO B03-CPT-ASSTO-INI-MOR-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCptAsstoIniMor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-CPT-ASSTO-INI-MOR-NULL
            bilaTrchEstr.getB03CptAsstoIniMor().setB03CptAsstoIniMorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03CptAsstoIniMor.Len.B03_CPT_ASSTO_INI_MOR_NULL));
        }
        // COB_CODE: IF IND-B03-TS-STAB-PRE = -1
        //              MOVE HIGH-VALUES TO B03-TS-STAB-PRE-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getTsStabPre() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-TS-STAB-PRE-NULL
            bilaTrchEstr.getB03TsStabPre().setB03TsStabPreNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03TsStabPre.Len.B03_TS_STAB_PRE_NULL));
        }
        // COB_CODE: IF IND-B03-DIR-EMIS = -1
        //              MOVE HIGH-VALUES TO B03-DIR-EMIS-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDirEmis() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DIR-EMIS-NULL
            bilaTrchEstr.getB03DirEmis().setB03DirEmisNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DirEmis.Len.B03_DIR_EMIS_NULL));
        }
        // COB_CODE: IF IND-B03-DT-INC-ULT-PRE = -1
        //              MOVE HIGH-VALUES TO B03-DT-INC-ULT-PRE-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtIncUltPre() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-DT-INC-ULT-PRE-NULL
            bilaTrchEstr.getB03DtIncUltPre().setB03DtIncUltPreNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DtIncUltPre.Len.B03_DT_INC_ULT_PRE_NULL));
        }
        // COB_CODE: IF IND-B03-STAT-TBGC-ASSTO-1 = -1
        //              MOVE HIGH-VALUES TO B03-STAT-TBGC-ASSTO-1-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getStatTbgcAssto1() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-STAT-TBGC-ASSTO-1-NULL
            bilaTrchEstr.setB03StatTbgcAssto1(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-B03-STAT-TBGC-ASSTO-2 = -1
        //              MOVE HIGH-VALUES TO B03-STAT-TBGC-ASSTO-2-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getStatTbgcAssto2() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-STAT-TBGC-ASSTO-2-NULL
            bilaTrchEstr.setB03StatTbgcAssto2(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-B03-STAT-TBGC-ASSTO-3 = -1
        //              MOVE HIGH-VALUES TO B03-STAT-TBGC-ASSTO-3-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getStatTbgcAssto3() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-STAT-TBGC-ASSTO-3-NULL
            bilaTrchEstr.setB03StatTbgcAssto3(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-B03-FRAZ-DECR-CPT = -1
        //              MOVE HIGH-VALUES TO B03-FRAZ-DECR-CPT-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getFrazDecrCpt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-FRAZ-DECR-CPT-NULL
            bilaTrchEstr.getB03FrazDecrCpt().setB03FrazDecrCptNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03FrazDecrCpt.Len.B03_FRAZ_DECR_CPT_NULL));
        }
        // COB_CODE: IF IND-B03-PRE-PP-ULT = -1
        //              MOVE HIGH-VALUES TO B03-PRE-PP-ULT-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getPrePpUlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-PRE-PP-ULT-NULL
            bilaTrchEstr.getB03PrePpUlt().setB03PrePpUltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03PrePpUlt.Len.B03_PRE_PP_ULT_NULL));
        }
        // COB_CODE: IF IND-B03-ACQ-EXP = -1
        //              MOVE HIGH-VALUES TO B03-ACQ-EXP-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getAcqExp() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-ACQ-EXP-NULL
            bilaTrchEstr.getB03AcqExp().setB03AcqExpNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03AcqExp.Len.B03_ACQ_EXP_NULL));
        }
        // COB_CODE: IF IND-B03-REMUN-ASS = -1
        //              MOVE HIGH-VALUES TO B03-REMUN-ASS-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getRemunAss() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-REMUN-ASS-NULL
            bilaTrchEstr.getB03RemunAss().setB03RemunAssNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03RemunAss.Len.B03_REMUN_ASS_NULL));
        }
        // COB_CODE: IF IND-B03-COMMIS-INTER = -1
        //              MOVE HIGH-VALUES TO B03-COMMIS-INTER-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getCommisInter() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-COMMIS-INTER-NULL
            bilaTrchEstr.getB03CommisInter().setB03CommisInterNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03CommisInter.Len.B03_COMMIS_INTER_NULL));
        }
        // COB_CODE: IF IND-B03-NUM-FINANZ = -1
        //              MOVE HIGH-VALUES TO B03-NUM-FINANZ-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getNumFinanz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-NUM-FINANZ-NULL
            bilaTrchEstr.setB03NumFinanz(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_NUM_FINANZ));
        }
        // COB_CODE: IF IND-B03-TP-ACC-COMM = -1
        //              MOVE HIGH-VALUES TO B03-TP-ACC-COMM-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getTpAccComm() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-TP-ACC-COMM-NULL
            bilaTrchEstr.setB03TpAccComm(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_TP_ACC_COMM));
        }
        // COB_CODE: IF IND-B03-IB-ACC-COMM = -1
        //              MOVE HIGH-VALUES TO B03-IB-ACC-COMM-NULL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getIbAccComm() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-IB-ACC-COMM-NULL
            bilaTrchEstr.setB03IbAccComm(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BilaTrchEstrIdbsb030.Len.B03_IB_ACC_COMM));
        }
        // COB_CODE: IF IND-B03-CARZ = -1
        //              MOVE HIGH-VALUES TO B03-CARZ-NULL
        //           END-IF.
        if (ws.getIndBilaTrchEstr().getCarz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B03-CARZ-NULL
            bilaTrchEstr.getB03Carz().setB03CarzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03Carz.Len.B03_CARZ_NULL));
        }
    }

    /**Original name: Z150-VALORIZZA-DATA-SERVICES-I<br>*/
    private void z150ValorizzaDataServicesI() {
        // COB_CODE: MOVE 'I' TO B03-DS-OPER-SQL
        bilaTrchEstr.setB03DsOperSqlFormatted("I");
        // COB_CODE: MOVE 0                   TO B03-DS-VER
        bilaTrchEstr.setB03DsVer(0);
        // COB_CODE: MOVE IDSV0003-USER-NAME TO B03-DS-UTENTE
        bilaTrchEstr.setB03DsUtente(idsv0003.getUserName());
        // COB_CODE: MOVE '1'                   TO B03-DS-STATO-ELAB
        bilaTrchEstr.setB03DsStatoElabFormatted("1");
        // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR TO B03-DS-TS-CPTZ.
        bilaTrchEstr.setB03DsTsCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
    }

    /**Original name: Z160-VALORIZZA-DATA-SERVICES-U<br>*/
    private void z160ValorizzaDataServicesU() {
        // COB_CODE: MOVE 'U' TO B03-DS-OPER-SQL
        bilaTrchEstr.setB03DsOperSqlFormatted("U");
        // COB_CODE: MOVE IDSV0003-USER-NAME TO B03-DS-UTENTE
        bilaTrchEstr.setB03DsUtente(idsv0003.getUserName());
        // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR TO B03-DS-TS-CPTZ.
        bilaTrchEstr.setB03DsTsCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
    }

    /**Original name: Z200-SET-INDICATORI-NULL<br>*/
    private void z200SetIndicatoriNull() {
        // COB_CODE: IF B03-ID-RICH-ESTRAZ-AGG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-ID-RICH-ESTRAZ-AGG
        //           ELSE
        //              MOVE 0 TO IND-B03-ID-RICH-ESTRAZ-AGG
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03IdRichEstrazAgg().getB03IdRichEstrazAggNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-ID-RICH-ESTRAZ-AGG
            ws.getIndBilaTrchEstr().setIdRichEstrazAgg(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-ID-RICH-ESTRAZ-AGG
            ws.getIndBilaTrchEstr().setIdRichEstrazAgg(((short)0));
        }
        // COB_CODE: IF B03-FL-SIMULAZIONE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-FL-SIMULAZIONE
        //           ELSE
        //              MOVE 0 TO IND-B03-FL-SIMULAZIONE
        //           END-IF
        if (Conditions.eq(bilaTrchEstr.getB03FlSimulazione(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-B03-FL-SIMULAZIONE
            ws.getIndBilaTrchEstr().setFlSimulazione(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-FL-SIMULAZIONE
            ws.getIndBilaTrchEstr().setFlSimulazione(((short)0));
        }
        // COB_CODE: IF B03-DT-INI-VAL-TAR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DT-INI-VAL-TAR
        //           ELSE
        //              MOVE 0 TO IND-B03-DT-INI-VAL-TAR
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03DtIniValTar().getB03DtIniValTarNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DT-INI-VAL-TAR
            ws.getIndBilaTrchEstr().setDtIniValTar(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DT-INI-VAL-TAR
            ws.getIndBilaTrchEstr().setDtIniValTar(((short)0));
        }
        // COB_CODE: IF B03-COD-PROD-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-COD-PROD
        //           ELSE
        //              MOVE 0 TO IND-B03-COD-PROD
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CodProdFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-COD-PROD
            ws.getIndBilaTrchEstr().setCodProd(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-COD-PROD
            ws.getIndBilaTrchEstr().setCodProd(((short)0));
        }
        // COB_CODE: IF B03-COD-TARI-ORGN-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-COD-TARI-ORGN
        //           ELSE
        //              MOVE 0 TO IND-B03-COD-TARI-ORGN
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CodTariOrgnFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-COD-TARI-ORGN
            ws.getIndBilaTrchEstr().setCodTariOrgn(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-COD-TARI-ORGN
            ws.getIndBilaTrchEstr().setCodTariOrgn(((short)0));
        }
        // COB_CODE: IF B03-MIN-GARTO-T-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-MIN-GARTO-T
        //           ELSE
        //              MOVE 0 TO IND-B03-MIN-GARTO-T
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03MinGartoT().getB03MinGartoTNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-MIN-GARTO-T
            ws.getIndBilaTrchEstr().setMinGartoT(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-MIN-GARTO-T
            ws.getIndBilaTrchEstr().setMinGartoT(((short)0));
        }
        // COB_CODE: IF B03-TP-TARI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-TP-TARI
        //           ELSE
        //              MOVE 0 TO IND-B03-TP-TARI
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03TpTariFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-TP-TARI
            ws.getIndBilaTrchEstr().setTpTari(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-TP-TARI
            ws.getIndBilaTrchEstr().setTpTari(((short)0));
        }
        // COB_CODE: IF B03-TP-PRE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-TP-PRE
        //           ELSE
        //              MOVE 0 TO IND-B03-TP-PRE
        //           END-IF
        if (Conditions.eq(bilaTrchEstr.getB03TpPre(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-B03-TP-PRE
            ws.getIndBilaTrchEstr().setTpPre(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-TP-PRE
            ws.getIndBilaTrchEstr().setTpPre(((short)0));
        }
        // COB_CODE: IF B03-TP-ADEG-PRE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-TP-ADEG-PRE
        //           ELSE
        //              MOVE 0 TO IND-B03-TP-ADEG-PRE
        //           END-IF
        if (Conditions.eq(bilaTrchEstr.getB03TpAdegPre(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-B03-TP-ADEG-PRE
            ws.getIndBilaTrchEstr().setTpAdegPre(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-TP-ADEG-PRE
            ws.getIndBilaTrchEstr().setTpAdegPre(((short)0));
        }
        // COB_CODE: IF B03-TP-RIVAL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-TP-RIVAL
        //           ELSE
        //              MOVE 0 TO IND-B03-TP-RIVAL
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03TpRivalFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-TP-RIVAL
            ws.getIndBilaTrchEstr().setTpRival(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-TP-RIVAL
            ws.getIndBilaTrchEstr().setTpRival(((short)0));
        }
        // COB_CODE: IF B03-FL-DA-TRASF-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-FL-DA-TRASF
        //           ELSE
        //              MOVE 0 TO IND-B03-FL-DA-TRASF
        //           END-IF
        if (Conditions.eq(bilaTrchEstr.getB03FlDaTrasf(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-B03-FL-DA-TRASF
            ws.getIndBilaTrchEstr().setFlDaTrasf(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-FL-DA-TRASF
            ws.getIndBilaTrchEstr().setFlDaTrasf(((short)0));
        }
        // COB_CODE: IF B03-FL-CAR-CONT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-FL-CAR-CONT
        //           ELSE
        //              MOVE 0 TO IND-B03-FL-CAR-CONT
        //           END-IF
        if (Conditions.eq(bilaTrchEstr.getB03FlCarCont(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-B03-FL-CAR-CONT
            ws.getIndBilaTrchEstr().setFlCarCont(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-FL-CAR-CONT
            ws.getIndBilaTrchEstr().setFlCarCont(((short)0));
        }
        // COB_CODE: IF B03-FL-PRE-DA-RIS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-FL-PRE-DA-RIS
        //           ELSE
        //              MOVE 0 TO IND-B03-FL-PRE-DA-RIS
        //           END-IF
        if (Conditions.eq(bilaTrchEstr.getB03FlPreDaRis(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-B03-FL-PRE-DA-RIS
            ws.getIndBilaTrchEstr().setFlPreDaRis(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-FL-PRE-DA-RIS
            ws.getIndBilaTrchEstr().setFlPreDaRis(((short)0));
        }
        // COB_CODE: IF B03-FL-PRE-AGG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-FL-PRE-AGG
        //           ELSE
        //              MOVE 0 TO IND-B03-FL-PRE-AGG
        //           END-IF
        if (Conditions.eq(bilaTrchEstr.getB03FlPreAgg(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-B03-FL-PRE-AGG
            ws.getIndBilaTrchEstr().setFlPreAgg(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-FL-PRE-AGG
            ws.getIndBilaTrchEstr().setFlPreAgg(((short)0));
        }
        // COB_CODE: IF B03-TP-TRCH-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-TP-TRCH
        //           ELSE
        //              MOVE 0 TO IND-B03-TP-TRCH
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03TpTrchFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-TP-TRCH
            ws.getIndBilaTrchEstr().setTpTrch(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-TP-TRCH
            ws.getIndBilaTrchEstr().setTpTrch(((short)0));
        }
        // COB_CODE: IF B03-TP-TST-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-TP-TST
        //           ELSE
        //              MOVE 0 TO IND-B03-TP-TST
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03TpTstFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-TP-TST
            ws.getIndBilaTrchEstr().setTpTst(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-TP-TST
            ws.getIndBilaTrchEstr().setTpTst(((short)0));
        }
        // COB_CODE: IF B03-COD-CONV-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-COD-CONV
        //           ELSE
        //              MOVE 0 TO IND-B03-COD-CONV
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CodConvFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-COD-CONV
            ws.getIndBilaTrchEstr().setCodConv(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-COD-CONV
            ws.getIndBilaTrchEstr().setCodConv(((short)0));
        }
        // COB_CODE: IF B03-DT-DECOR-ADES-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DT-DECOR-ADES
        //           ELSE
        //              MOVE 0 TO IND-B03-DT-DECOR-ADES
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03DtDecorAdes().getB03DtDecorAdesNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DT-DECOR-ADES
            ws.getIndBilaTrchEstr().setDtDecorAdes(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DT-DECOR-ADES
            ws.getIndBilaTrchEstr().setDtDecorAdes(((short)0));
        }
        // COB_CODE: IF B03-DT-EMIS-TRCH-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DT-EMIS-TRCH
        //           ELSE
        //              MOVE 0 TO IND-B03-DT-EMIS-TRCH
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03DtEmisTrch().getB03DtEmisTrchNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DT-EMIS-TRCH
            ws.getIndBilaTrchEstr().setDtEmisTrch(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DT-EMIS-TRCH
            ws.getIndBilaTrchEstr().setDtEmisTrch(((short)0));
        }
        // COB_CODE: IF B03-DT-SCAD-TRCH-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DT-SCAD-TRCH
        //           ELSE
        //              MOVE 0 TO IND-B03-DT-SCAD-TRCH
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03DtScadTrch().getB03DtScadTrchNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DT-SCAD-TRCH
            ws.getIndBilaTrchEstr().setDtScadTrch(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DT-SCAD-TRCH
            ws.getIndBilaTrchEstr().setDtScadTrch(((short)0));
        }
        // COB_CODE: IF B03-DT-SCAD-INTMD-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DT-SCAD-INTMD
        //           ELSE
        //              MOVE 0 TO IND-B03-DT-SCAD-INTMD
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03DtScadIntmd().getB03DtScadIntmdNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DT-SCAD-INTMD
            ws.getIndBilaTrchEstr().setDtScadIntmd(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DT-SCAD-INTMD
            ws.getIndBilaTrchEstr().setDtScadIntmd(((short)0));
        }
        // COB_CODE: IF B03-DT-SCAD-PAG-PRE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DT-SCAD-PAG-PRE
        //           ELSE
        //              MOVE 0 TO IND-B03-DT-SCAD-PAG-PRE
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03DtScadPagPre().getB03DtScadPagPreNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DT-SCAD-PAG-PRE
            ws.getIndBilaTrchEstr().setDtScadPagPre(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DT-SCAD-PAG-PRE
            ws.getIndBilaTrchEstr().setDtScadPagPre(((short)0));
        }
        // COB_CODE: IF B03-DT-ULT-PRE-PAG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DT-ULT-PRE-PAG
        //           ELSE
        //              MOVE 0 TO IND-B03-DT-ULT-PRE-PAG
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03DtUltPrePag().getB03DtUltPrePagNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DT-ULT-PRE-PAG
            ws.getIndBilaTrchEstr().setDtUltPrePag(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DT-ULT-PRE-PAG
            ws.getIndBilaTrchEstr().setDtUltPrePag(((short)0));
        }
        // COB_CODE: IF B03-DT-NASC-1O-ASSTO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DT-NASC-1O-ASSTO
        //           ELSE
        //              MOVE 0 TO IND-B03-DT-NASC-1O-ASSTO
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03DtNasc1oAssto().getB03DtNasc1oAsstoNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DT-NASC-1O-ASSTO
            ws.getIndBilaTrchEstr().setDtNasc1oAssto(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DT-NASC-1O-ASSTO
            ws.getIndBilaTrchEstr().setDtNasc1oAssto(((short)0));
        }
        // COB_CODE: IF B03-SEX-1O-ASSTO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-SEX-1O-ASSTO
        //           ELSE
        //              MOVE 0 TO IND-B03-SEX-1O-ASSTO
        //           END-IF
        if (Conditions.eq(bilaTrchEstr.getB03Sex1oAssto(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-B03-SEX-1O-ASSTO
            ws.getIndBilaTrchEstr().setSex1oAssto(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-SEX-1O-ASSTO
            ws.getIndBilaTrchEstr().setSex1oAssto(((short)0));
        }
        // COB_CODE: IF B03-ETA-AA-1O-ASSTO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-ETA-AA-1O-ASSTO
        //           ELSE
        //              MOVE 0 TO IND-B03-ETA-AA-1O-ASSTO
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03EtaAa1oAssto().getB03EtaAa1oAsstoNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-ETA-AA-1O-ASSTO
            ws.getIndBilaTrchEstr().setEtaAa1oAssto(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-ETA-AA-1O-ASSTO
            ws.getIndBilaTrchEstr().setEtaAa1oAssto(((short)0));
        }
        // COB_CODE: IF B03-ETA-MM-1O-ASSTO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-ETA-MM-1O-ASSTO
        //           ELSE
        //              MOVE 0 TO IND-B03-ETA-MM-1O-ASSTO
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03EtaMm1oAssto().getB03EtaMm1oAsstoNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-ETA-MM-1O-ASSTO
            ws.getIndBilaTrchEstr().setEtaMm1oAssto(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-ETA-MM-1O-ASSTO
            ws.getIndBilaTrchEstr().setEtaMm1oAssto(((short)0));
        }
        // COB_CODE: IF B03-ETA-RAGGN-DT-CALC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-ETA-RAGGN-DT-CALC
        //           ELSE
        //              MOVE 0 TO IND-B03-ETA-RAGGN-DT-CALC
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03EtaRaggnDtCalc().getB03EtaRaggnDtCalcNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-ETA-RAGGN-DT-CALC
            ws.getIndBilaTrchEstr().setEtaRaggnDtCalc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-ETA-RAGGN-DT-CALC
            ws.getIndBilaTrchEstr().setEtaRaggnDtCalc(((short)0));
        }
        // COB_CODE: IF B03-DUR-AA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DUR-AA
        //           ELSE
        //              MOVE 0 TO IND-B03-DUR-AA
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03DurAa().getB03DurAaNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DUR-AA
            ws.getIndBilaTrchEstr().setDurAa(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DUR-AA
            ws.getIndBilaTrchEstr().setDurAa(((short)0));
        }
        // COB_CODE: IF B03-DUR-MM-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DUR-MM
        //           ELSE
        //              MOVE 0 TO IND-B03-DUR-MM
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03DurMm().getB03DurMmNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DUR-MM
            ws.getIndBilaTrchEstr().setDurMm(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DUR-MM
            ws.getIndBilaTrchEstr().setDurMm(((short)0));
        }
        // COB_CODE: IF B03-DUR-GG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DUR-GG
        //           ELSE
        //              MOVE 0 TO IND-B03-DUR-GG
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03DurGg().getB03DurGgNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DUR-GG
            ws.getIndBilaTrchEstr().setDurGg(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DUR-GG
            ws.getIndBilaTrchEstr().setDurGg(((short)0));
        }
        // COB_CODE: IF B03-DUR-1O-PER-AA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DUR-1O-PER-AA
        //           ELSE
        //              MOVE 0 TO IND-B03-DUR-1O-PER-AA
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03Dur1oPerAa().getB03Dur1oPerAaNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DUR-1O-PER-AA
            ws.getIndBilaTrchEstr().setDur1oPerAa(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DUR-1O-PER-AA
            ws.getIndBilaTrchEstr().setDur1oPerAa(((short)0));
        }
        // COB_CODE: IF B03-DUR-1O-PER-MM-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DUR-1O-PER-MM
        //           ELSE
        //              MOVE 0 TO IND-B03-DUR-1O-PER-MM
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03Dur1oPerMm().getB03Dur1oPerMmNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DUR-1O-PER-MM
            ws.getIndBilaTrchEstr().setDur1oPerMm(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DUR-1O-PER-MM
            ws.getIndBilaTrchEstr().setDur1oPerMm(((short)0));
        }
        // COB_CODE: IF B03-DUR-1O-PER-GG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DUR-1O-PER-GG
        //           ELSE
        //              MOVE 0 TO IND-B03-DUR-1O-PER-GG
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03Dur1oPerGg().getB03Dur1oPerGgNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DUR-1O-PER-GG
            ws.getIndBilaTrchEstr().setDur1oPerGg(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DUR-1O-PER-GG
            ws.getIndBilaTrchEstr().setDur1oPerGg(((short)0));
        }
        // COB_CODE: IF B03-ANTIDUR-RICOR-PREC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-ANTIDUR-RICOR-PREC
        //           ELSE
        //              MOVE 0 TO IND-B03-ANTIDUR-RICOR-PREC
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03AntidurRicorPrec().getB03AntidurRicorPrecNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-ANTIDUR-RICOR-PREC
            ws.getIndBilaTrchEstr().setAntidurRicorPrec(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-ANTIDUR-RICOR-PREC
            ws.getIndBilaTrchEstr().setAntidurRicorPrec(((short)0));
        }
        // COB_CODE: IF B03-ANTIDUR-DT-CALC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-ANTIDUR-DT-CALC
        //           ELSE
        //              MOVE 0 TO IND-B03-ANTIDUR-DT-CALC
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03AntidurDtCalc().getB03AntidurDtCalcNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-ANTIDUR-DT-CALC
            ws.getIndBilaTrchEstr().setAntidurDtCalc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-ANTIDUR-DT-CALC
            ws.getIndBilaTrchEstr().setAntidurDtCalc(((short)0));
        }
        // COB_CODE: IF B03-DUR-RES-DT-CALC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DUR-RES-DT-CALC
        //           ELSE
        //              MOVE 0 TO IND-B03-DUR-RES-DT-CALC
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03DurResDtCalc().getB03DurResDtCalcNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DUR-RES-DT-CALC
            ws.getIndBilaTrchEstr().setDurResDtCalc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DUR-RES-DT-CALC
            ws.getIndBilaTrchEstr().setDurResDtCalc(((short)0));
        }
        // COB_CODE: IF B03-DT-EFF-CAMB-STAT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DT-EFF-CAMB-STAT
        //           ELSE
        //              MOVE 0 TO IND-B03-DT-EFF-CAMB-STAT
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03DtEffCambStat().getB03DtEffCambStatNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DT-EFF-CAMB-STAT
            ws.getIndBilaTrchEstr().setDtEffCambStat(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DT-EFF-CAMB-STAT
            ws.getIndBilaTrchEstr().setDtEffCambStat(((short)0));
        }
        // COB_CODE: IF B03-DT-EMIS-CAMB-STAT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DT-EMIS-CAMB-STAT
        //           ELSE
        //              MOVE 0 TO IND-B03-DT-EMIS-CAMB-STAT
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03DtEmisCambStat().getB03DtEmisCambStatNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DT-EMIS-CAMB-STAT
            ws.getIndBilaTrchEstr().setDtEmisCambStat(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DT-EMIS-CAMB-STAT
            ws.getIndBilaTrchEstr().setDtEmisCambStat(((short)0));
        }
        // COB_CODE: IF B03-DT-EFF-STAB-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DT-EFF-STAB
        //           ELSE
        //              MOVE 0 TO IND-B03-DT-EFF-STAB
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03DtEffStab().getB03DtEffStabNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DT-EFF-STAB
            ws.getIndBilaTrchEstr().setDtEffStab(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DT-EFF-STAB
            ws.getIndBilaTrchEstr().setDtEffStab(((short)0));
        }
        // COB_CODE: IF B03-CPT-DT-STAB-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-CPT-DT-STAB
        //           ELSE
        //              MOVE 0 TO IND-B03-CPT-DT-STAB
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CptDtStab().getB03CptDtStabNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-CPT-DT-STAB
            ws.getIndBilaTrchEstr().setCptDtStab(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-CPT-DT-STAB
            ws.getIndBilaTrchEstr().setCptDtStab(((short)0));
        }
        // COB_CODE: IF B03-DT-EFF-RIDZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DT-EFF-RIDZ
        //           ELSE
        //              MOVE 0 TO IND-B03-DT-EFF-RIDZ
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03DtEffRidz().getB03DtEffRidzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DT-EFF-RIDZ
            ws.getIndBilaTrchEstr().setDtEffRidz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DT-EFF-RIDZ
            ws.getIndBilaTrchEstr().setDtEffRidz(((short)0));
        }
        // COB_CODE: IF B03-DT-EMIS-RIDZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DT-EMIS-RIDZ
        //           ELSE
        //              MOVE 0 TO IND-B03-DT-EMIS-RIDZ
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03DtEmisRidz().getB03DtEmisRidzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DT-EMIS-RIDZ
            ws.getIndBilaTrchEstr().setDtEmisRidz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DT-EMIS-RIDZ
            ws.getIndBilaTrchEstr().setDtEmisRidz(((short)0));
        }
        // COB_CODE: IF B03-CPT-DT-RIDZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-CPT-DT-RIDZ
        //           ELSE
        //              MOVE 0 TO IND-B03-CPT-DT-RIDZ
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CptDtRidz().getB03CptDtRidzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-CPT-DT-RIDZ
            ws.getIndBilaTrchEstr().setCptDtRidz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-CPT-DT-RIDZ
            ws.getIndBilaTrchEstr().setCptDtRidz(((short)0));
        }
        // COB_CODE: IF B03-FRAZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-FRAZ
        //           ELSE
        //              MOVE 0 TO IND-B03-FRAZ
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03Fraz().getB03FrazNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-FRAZ
            ws.getIndBilaTrchEstr().setFraz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-FRAZ
            ws.getIndBilaTrchEstr().setFraz(((short)0));
        }
        // COB_CODE: IF B03-DUR-PAG-PRE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DUR-PAG-PRE
        //           ELSE
        //              MOVE 0 TO IND-B03-DUR-PAG-PRE
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03DurPagPre().getB03DurPagPreNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DUR-PAG-PRE
            ws.getIndBilaTrchEstr().setDurPagPre(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DUR-PAG-PRE
            ws.getIndBilaTrchEstr().setDurPagPre(((short)0));
        }
        // COB_CODE: IF B03-NUM-PRE-PATT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-NUM-PRE-PATT
        //           ELSE
        //              MOVE 0 TO IND-B03-NUM-PRE-PATT
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03NumPrePatt().getB03NumPrePattNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-NUM-PRE-PATT
            ws.getIndBilaTrchEstr().setNumPrePatt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-NUM-PRE-PATT
            ws.getIndBilaTrchEstr().setNumPrePatt(((short)0));
        }
        // COB_CODE: IF B03-FRAZ-INI-EROG-REN-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-FRAZ-INI-EROG-REN
        //           ELSE
        //              MOVE 0 TO IND-B03-FRAZ-INI-EROG-REN
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03FrazIniErogRen().getB03FrazIniErogRenNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-FRAZ-INI-EROG-REN
            ws.getIndBilaTrchEstr().setFrazIniErogRen(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-FRAZ-INI-EROG-REN
            ws.getIndBilaTrchEstr().setFrazIniErogRen(((short)0));
        }
        // COB_CODE: IF B03-AA-REN-CER-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-AA-REN-CER
        //           ELSE
        //              MOVE 0 TO IND-B03-AA-REN-CER
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03AaRenCer().getB03AaRenCerNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-AA-REN-CER
            ws.getIndBilaTrchEstr().setAaRenCer(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-AA-REN-CER
            ws.getIndBilaTrchEstr().setAaRenCer(((short)0));
        }
        // COB_CODE: IF B03-RAT-REN-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-RAT-REN
        //           ELSE
        //              MOVE 0 TO IND-B03-RAT-REN
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03RatRen().getB03RatRenNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-RAT-REN
            ws.getIndBilaTrchEstr().setRatRen(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-RAT-REN
            ws.getIndBilaTrchEstr().setRatRen(((short)0));
        }
        // COB_CODE: IF B03-COD-DIV-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-COD-DIV
        //           ELSE
        //              MOVE 0 TO IND-B03-COD-DIV
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CodDivFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-COD-DIV
            ws.getIndBilaTrchEstr().setCodDiv(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-COD-DIV
            ws.getIndBilaTrchEstr().setCodDiv(((short)0));
        }
        // COB_CODE: IF B03-RISCPAR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-RISCPAR
        //           ELSE
        //              MOVE 0 TO IND-B03-RISCPAR
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03Riscpar().getB03RiscparNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-RISCPAR
            ws.getIndBilaTrchEstr().setRiscpar(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-RISCPAR
            ws.getIndBilaTrchEstr().setRiscpar(((short)0));
        }
        // COB_CODE: IF B03-CUM-RISCPAR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-CUM-RISCPAR
        //           ELSE
        //              MOVE 0 TO IND-B03-CUM-RISCPAR
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CumRiscpar().getB03CumRiscparNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-CUM-RISCPAR
            ws.getIndBilaTrchEstr().setCumRiscpar(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-CUM-RISCPAR
            ws.getIndBilaTrchEstr().setCumRiscpar(((short)0));
        }
        // COB_CODE: IF B03-ULT-RM-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-ULT-RM
        //           ELSE
        //              MOVE 0 TO IND-B03-ULT-RM
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03UltRm().getB03UltRmNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-ULT-RM
            ws.getIndBilaTrchEstr().setUltRm(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-ULT-RM
            ws.getIndBilaTrchEstr().setUltRm(((short)0));
        }
        // COB_CODE: IF B03-TS-RENDTO-T-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-TS-RENDTO-T
        //           ELSE
        //              MOVE 0 TO IND-B03-TS-RENDTO-T
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03TsRendtoT().getB03TsRendtoTNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-TS-RENDTO-T
            ws.getIndBilaTrchEstr().setTsRendtoT(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-TS-RENDTO-T
            ws.getIndBilaTrchEstr().setTsRendtoT(((short)0));
        }
        // COB_CODE: IF B03-ALQ-RETR-T-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-ALQ-RETR-T
        //           ELSE
        //              MOVE 0 TO IND-B03-ALQ-RETR-T
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03AlqRetrT().getB03AlqRetrTNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-ALQ-RETR-T
            ws.getIndBilaTrchEstr().setAlqRetrT(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-ALQ-RETR-T
            ws.getIndBilaTrchEstr().setAlqRetrT(((short)0));
        }
        // COB_CODE: IF B03-MIN-TRNUT-T-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-MIN-TRNUT-T
        //           ELSE
        //              MOVE 0 TO IND-B03-MIN-TRNUT-T
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03MinTrnutT().getB03MinTrnutTNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-MIN-TRNUT-T
            ws.getIndBilaTrchEstr().setMinTrnutT(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-MIN-TRNUT-T
            ws.getIndBilaTrchEstr().setMinTrnutT(((short)0));
        }
        // COB_CODE: IF B03-TS-NET-T-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-TS-NET-T
        //           ELSE
        //              MOVE 0 TO IND-B03-TS-NET-T
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03TsNetT().getB03TsNetTNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-TS-NET-T
            ws.getIndBilaTrchEstr().setTsNetT(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-TS-NET-T
            ws.getIndBilaTrchEstr().setTsNetT(((short)0));
        }
        // COB_CODE: IF B03-DT-ULT-RIVAL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DT-ULT-RIVAL
        //           ELSE
        //              MOVE 0 TO IND-B03-DT-ULT-RIVAL
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03DtUltRival().getB03DtUltRivalNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DT-ULT-RIVAL
            ws.getIndBilaTrchEstr().setDtUltRival(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DT-ULT-RIVAL
            ws.getIndBilaTrchEstr().setDtUltRival(((short)0));
        }
        // COB_CODE: IF B03-PRSTZ-INI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-PRSTZ-INI
        //           ELSE
        //              MOVE 0 TO IND-B03-PRSTZ-INI
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03PrstzIni().getB03PrstzIniNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-PRSTZ-INI
            ws.getIndBilaTrchEstr().setPrstzIni(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-PRSTZ-INI
            ws.getIndBilaTrchEstr().setPrstzIni(((short)0));
        }
        // COB_CODE: IF B03-PRSTZ-AGG-INI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-PRSTZ-AGG-INI
        //           ELSE
        //              MOVE 0 TO IND-B03-PRSTZ-AGG-INI
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03PrstzAggIni().getB03PrstzAggIniNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-PRSTZ-AGG-INI
            ws.getIndBilaTrchEstr().setPrstzAggIni(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-PRSTZ-AGG-INI
            ws.getIndBilaTrchEstr().setPrstzAggIni(((short)0));
        }
        // COB_CODE: IF B03-PRSTZ-AGG-ULT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-PRSTZ-AGG-ULT
        //           ELSE
        //              MOVE 0 TO IND-B03-PRSTZ-AGG-ULT
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03PrstzAggUlt().getB03PrstzAggUltNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-PRSTZ-AGG-ULT
            ws.getIndBilaTrchEstr().setPrstzAggUlt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-PRSTZ-AGG-ULT
            ws.getIndBilaTrchEstr().setPrstzAggUlt(((short)0));
        }
        // COB_CODE: IF B03-RAPPEL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-RAPPEL
        //           ELSE
        //              MOVE 0 TO IND-B03-RAPPEL
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03Rappel().getB03RappelNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-RAPPEL
            ws.getIndBilaTrchEstr().setRappel(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-RAPPEL
            ws.getIndBilaTrchEstr().setRappel(((short)0));
        }
        // COB_CODE: IF B03-PRE-PATTUITO-INI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-PRE-PATTUITO-INI
        //           ELSE
        //              MOVE 0 TO IND-B03-PRE-PATTUITO-INI
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03PrePattuitoIni().getB03PrePattuitoIniNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-PRE-PATTUITO-INI
            ws.getIndBilaTrchEstr().setPrePattuitoIni(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-PRE-PATTUITO-INI
            ws.getIndBilaTrchEstr().setPrePattuitoIni(((short)0));
        }
        // COB_CODE: IF B03-PRE-DOV-INI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-PRE-DOV-INI
        //           ELSE
        //              MOVE 0 TO IND-B03-PRE-DOV-INI
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03PreDovIni().getB03PreDovIniNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-PRE-DOV-INI
            ws.getIndBilaTrchEstr().setPreDovIni(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-PRE-DOV-INI
            ws.getIndBilaTrchEstr().setPreDovIni(((short)0));
        }
        // COB_CODE: IF B03-PRE-DOV-RIVTO-T-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-PRE-DOV-RIVTO-T
        //           ELSE
        //              MOVE 0 TO IND-B03-PRE-DOV-RIVTO-T
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03PreDovRivtoT().getB03PreDovRivtoTNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-PRE-DOV-RIVTO-T
            ws.getIndBilaTrchEstr().setPreDovRivtoT(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-PRE-DOV-RIVTO-T
            ws.getIndBilaTrchEstr().setPreDovRivtoT(((short)0));
        }
        // COB_CODE: IF B03-PRE-ANNUALIZ-RICOR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-PRE-ANNUALIZ-RICOR
        //           ELSE
        //              MOVE 0 TO IND-B03-PRE-ANNUALIZ-RICOR
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03PreAnnualizRicor().getB03PreAnnualizRicorNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-PRE-ANNUALIZ-RICOR
            ws.getIndBilaTrchEstr().setPreAnnualizRicor(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-PRE-ANNUALIZ-RICOR
            ws.getIndBilaTrchEstr().setPreAnnualizRicor(((short)0));
        }
        // COB_CODE: IF B03-PRE-CONT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-PRE-CONT
        //           ELSE
        //              MOVE 0 TO IND-B03-PRE-CONT
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03PreCont().getB03PreContNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-PRE-CONT
            ws.getIndBilaTrchEstr().setPreCont(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-PRE-CONT
            ws.getIndBilaTrchEstr().setPreCont(((short)0));
        }
        // COB_CODE: IF B03-PRE-PP-INI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-PRE-PP-INI
        //           ELSE
        //              MOVE 0 TO IND-B03-PRE-PP-INI
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03PrePpIni().getB03PrePpIniNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-PRE-PP-INI
            ws.getIndBilaTrchEstr().setPrePpIni(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-PRE-PP-INI
            ws.getIndBilaTrchEstr().setPrePpIni(((short)0));
        }
        // COB_CODE: IF B03-RIS-PURA-T-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-RIS-PURA-T
        //           ELSE
        //              MOVE 0 TO IND-B03-RIS-PURA-T
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03RisPuraT().getB03RisPuraTNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-RIS-PURA-T
            ws.getIndBilaTrchEstr().setRisPuraT(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-RIS-PURA-T
            ws.getIndBilaTrchEstr().setRisPuraT(((short)0));
        }
        // COB_CODE: IF B03-PROV-ACQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-PROV-ACQ
        //           ELSE
        //              MOVE 0 TO IND-B03-PROV-ACQ
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03ProvAcq().getB03ProvAcqNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-PROV-ACQ
            ws.getIndBilaTrchEstr().setProvAcq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-PROV-ACQ
            ws.getIndBilaTrchEstr().setProvAcq(((short)0));
        }
        // COB_CODE: IF B03-PROV-ACQ-RICOR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-PROV-ACQ-RICOR
        //           ELSE
        //              MOVE 0 TO IND-B03-PROV-ACQ-RICOR
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03ProvAcqRicor().getB03ProvAcqRicorNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-PROV-ACQ-RICOR
            ws.getIndBilaTrchEstr().setProvAcqRicor(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-PROV-ACQ-RICOR
            ws.getIndBilaTrchEstr().setProvAcqRicor(((short)0));
        }
        // COB_CODE: IF B03-PROV-INC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-PROV-INC
        //           ELSE
        //              MOVE 0 TO IND-B03-PROV-INC
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03ProvInc().getB03ProvIncNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-PROV-INC
            ws.getIndBilaTrchEstr().setProvInc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-PROV-INC
            ws.getIndBilaTrchEstr().setProvInc(((short)0));
        }
        // COB_CODE: IF B03-CAR-ACQ-NON-SCON-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-CAR-ACQ-NON-SCON
        //           ELSE
        //              MOVE 0 TO IND-B03-CAR-ACQ-NON-SCON
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CarAcqNonScon().getB03CarAcqNonSconNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-CAR-ACQ-NON-SCON
            ws.getIndBilaTrchEstr().setCarAcqNonScon(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-CAR-ACQ-NON-SCON
            ws.getIndBilaTrchEstr().setCarAcqNonScon(((short)0));
        }
        // COB_CODE: IF B03-OVER-COMM-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-OVER-COMM
        //           ELSE
        //              MOVE 0 TO IND-B03-OVER-COMM
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03OverComm().getB03OverCommNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-OVER-COMM
            ws.getIndBilaTrchEstr().setOverComm(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-OVER-COMM
            ws.getIndBilaTrchEstr().setOverComm(((short)0));
        }
        // COB_CODE: IF B03-CAR-ACQ-PRECONTATO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-CAR-ACQ-PRECONTATO
        //           ELSE
        //              MOVE 0 TO IND-B03-CAR-ACQ-PRECONTATO
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CarAcqPrecontato().getB03CarAcqPrecontatoNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-CAR-ACQ-PRECONTATO
            ws.getIndBilaTrchEstr().setCarAcqPrecontato(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-CAR-ACQ-PRECONTATO
            ws.getIndBilaTrchEstr().setCarAcqPrecontato(((short)0));
        }
        // COB_CODE: IF B03-RIS-ACQ-T-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-RIS-ACQ-T
        //           ELSE
        //              MOVE 0 TO IND-B03-RIS-ACQ-T
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03RisAcqT().getB03RisAcqTNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-RIS-ACQ-T
            ws.getIndBilaTrchEstr().setRisAcqT(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-RIS-ACQ-T
            ws.getIndBilaTrchEstr().setRisAcqT(((short)0));
        }
        // COB_CODE: IF B03-RIS-ZIL-T-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-RIS-ZIL-T
        //           ELSE
        //              MOVE 0 TO IND-B03-RIS-ZIL-T
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03RisZilT().getB03RisZilTNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-RIS-ZIL-T
            ws.getIndBilaTrchEstr().setRisZilT(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-RIS-ZIL-T
            ws.getIndBilaTrchEstr().setRisZilT(((short)0));
        }
        // COB_CODE: IF B03-CAR-GEST-NON-SCON-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-CAR-GEST-NON-SCON
        //           ELSE
        //              MOVE 0 TO IND-B03-CAR-GEST-NON-SCON
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CarGestNonScon().getB03CarGestNonSconNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-CAR-GEST-NON-SCON
            ws.getIndBilaTrchEstr().setCarGestNonScon(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-CAR-GEST-NON-SCON
            ws.getIndBilaTrchEstr().setCarGestNonScon(((short)0));
        }
        // COB_CODE: IF B03-CAR-GEST-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-CAR-GEST
        //           ELSE
        //              MOVE 0 TO IND-B03-CAR-GEST
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CarGest().getB03CarGestNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-CAR-GEST
            ws.getIndBilaTrchEstr().setCarGest(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-CAR-GEST
            ws.getIndBilaTrchEstr().setCarGest(((short)0));
        }
        // COB_CODE: IF B03-RIS-SPE-T-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-RIS-SPE-T
        //           ELSE
        //              MOVE 0 TO IND-B03-RIS-SPE-T
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03RisSpeT().getB03RisSpeTNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-RIS-SPE-T
            ws.getIndBilaTrchEstr().setRisSpeT(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-RIS-SPE-T
            ws.getIndBilaTrchEstr().setRisSpeT(((short)0));
        }
        // COB_CODE: IF B03-CAR-INC-NON-SCON-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-CAR-INC-NON-SCON
        //           ELSE
        //              MOVE 0 TO IND-B03-CAR-INC-NON-SCON
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CarIncNonScon().getB03CarIncNonSconNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-CAR-INC-NON-SCON
            ws.getIndBilaTrchEstr().setCarIncNonScon(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-CAR-INC-NON-SCON
            ws.getIndBilaTrchEstr().setCarIncNonScon(((short)0));
        }
        // COB_CODE: IF B03-CAR-INC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-CAR-INC
        //           ELSE
        //              MOVE 0 TO IND-B03-CAR-INC
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CarInc().getB03CarIncNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-CAR-INC
            ws.getIndBilaTrchEstr().setCarInc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-CAR-INC
            ws.getIndBilaTrchEstr().setCarInc(((short)0));
        }
        // COB_CODE: IF B03-RIS-RISTORNI-CAP-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-RIS-RISTORNI-CAP
        //           ELSE
        //              MOVE 0 TO IND-B03-RIS-RISTORNI-CAP
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03RisRistorniCap().getB03RisRistorniCapNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-RIS-RISTORNI-CAP
            ws.getIndBilaTrchEstr().setRisRistorniCap(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-RIS-RISTORNI-CAP
            ws.getIndBilaTrchEstr().setRisRistorniCap(((short)0));
        }
        // COB_CODE: IF B03-INTR-TECN-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-INTR-TECN
        //           ELSE
        //              MOVE 0 TO IND-B03-INTR-TECN
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03IntrTecn().getB03IntrTecnNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-INTR-TECN
            ws.getIndBilaTrchEstr().setIntrTecn(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-INTR-TECN
            ws.getIndBilaTrchEstr().setIntrTecn(((short)0));
        }
        // COB_CODE: IF B03-CPT-RSH-MOR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-CPT-RSH-MOR
        //           ELSE
        //              MOVE 0 TO IND-B03-CPT-RSH-MOR
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CptRshMor().getB03CptRshMorNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-CPT-RSH-MOR
            ws.getIndBilaTrchEstr().setCptRshMor(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-CPT-RSH-MOR
            ws.getIndBilaTrchEstr().setCptRshMor(((short)0));
        }
        // COB_CODE: IF B03-C-SUBRSH-T-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-C-SUBRSH-T
        //           ELSE
        //              MOVE 0 TO IND-B03-C-SUBRSH-T
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CSubrshT().getB03CSubrshTNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-C-SUBRSH-T
            ws.getIndBilaTrchEstr().setcSubrshT(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-C-SUBRSH-T
            ws.getIndBilaTrchEstr().setcSubrshT(((short)0));
        }
        // COB_CODE: IF B03-PRE-RSH-T-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-PRE-RSH-T
        //           ELSE
        //              MOVE 0 TO IND-B03-PRE-RSH-T
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03PreRshT().getB03PreRshTNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-PRE-RSH-T
            ws.getIndBilaTrchEstr().setPreRshT(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-PRE-RSH-T
            ws.getIndBilaTrchEstr().setPreRshT(((short)0));
        }
        // COB_CODE: IF B03-ALQ-MARG-RIS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-ALQ-MARG-RIS
        //           ELSE
        //              MOVE 0 TO IND-B03-ALQ-MARG-RIS
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03AlqMargRis().getB03AlqMargRisNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-ALQ-MARG-RIS
            ws.getIndBilaTrchEstr().setAlqMargRis(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-ALQ-MARG-RIS
            ws.getIndBilaTrchEstr().setAlqMargRis(((short)0));
        }
        // COB_CODE: IF B03-ALQ-MARG-C-SUBRSH-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-ALQ-MARG-C-SUBRSH
        //           ELSE
        //              MOVE 0 TO IND-B03-ALQ-MARG-C-SUBRSH
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03AlqMargCSubrsh().getB03AlqMargCSubrshNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-ALQ-MARG-C-SUBRSH
            ws.getIndBilaTrchEstr().setAlqMargCSubrsh(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-ALQ-MARG-C-SUBRSH
            ws.getIndBilaTrchEstr().setAlqMargCSubrsh(((short)0));
        }
        // COB_CODE: IF B03-TS-RENDTO-SPPR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-TS-RENDTO-SPPR
        //           ELSE
        //              MOVE 0 TO IND-B03-TS-RENDTO-SPPR
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03TsRendtoSppr().getB03TsRendtoSpprNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-TS-RENDTO-SPPR
            ws.getIndBilaTrchEstr().setTsRendtoSppr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-TS-RENDTO-SPPR
            ws.getIndBilaTrchEstr().setTsRendtoSppr(((short)0));
        }
        // COB_CODE: IF B03-TP-IAS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-TP-IAS
        //           ELSE
        //              MOVE 0 TO IND-B03-TP-IAS
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03TpIasFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-TP-IAS
            ws.getIndBilaTrchEstr().setTpIas(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-TP-IAS
            ws.getIndBilaTrchEstr().setTpIas(((short)0));
        }
        // COB_CODE: IF B03-NS-QUO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-NS-QUO
        //           ELSE
        //              MOVE 0 TO IND-B03-NS-QUO
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03NsQuo().getB03NsQuoNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-NS-QUO
            ws.getIndBilaTrchEstr().setNsQuo(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-NS-QUO
            ws.getIndBilaTrchEstr().setNsQuo(((short)0));
        }
        // COB_CODE: IF B03-TS-MEDIO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-TS-MEDIO
        //           ELSE
        //              MOVE 0 TO IND-B03-TS-MEDIO
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03TsMedio().getB03TsMedioNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-TS-MEDIO
            ws.getIndBilaTrchEstr().setTsMedio(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-TS-MEDIO
            ws.getIndBilaTrchEstr().setTsMedio(((short)0));
        }
        // COB_CODE: IF B03-CPT-RIASTO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-CPT-RIASTO
        //           ELSE
        //              MOVE 0 TO IND-B03-CPT-RIASTO
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CptRiasto().getB03CptRiastoNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-CPT-RIASTO
            ws.getIndBilaTrchEstr().setCptRiasto(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-CPT-RIASTO
            ws.getIndBilaTrchEstr().setCptRiasto(((short)0));
        }
        // COB_CODE: IF B03-PRE-RIASTO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-PRE-RIASTO
        //           ELSE
        //              MOVE 0 TO IND-B03-PRE-RIASTO
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03PreRiasto().getB03PreRiastoNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-PRE-RIASTO
            ws.getIndBilaTrchEstr().setPreRiasto(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-PRE-RIASTO
            ws.getIndBilaTrchEstr().setPreRiasto(((short)0));
        }
        // COB_CODE: IF B03-RIS-RIASTA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-RIS-RIASTA
        //           ELSE
        //              MOVE 0 TO IND-B03-RIS-RIASTA
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03RisRiasta().getB03RisRiastaNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-RIS-RIASTA
            ws.getIndBilaTrchEstr().setRisRiasta(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-RIS-RIASTA
            ws.getIndBilaTrchEstr().setRisRiasta(((short)0));
        }
        // COB_CODE: IF B03-CPT-RIASTO-ECC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-CPT-RIASTO-ECC
        //           ELSE
        //              MOVE 0 TO IND-B03-CPT-RIASTO-ECC
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CptRiastoEcc().getB03CptRiastoEccNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-CPT-RIASTO-ECC
            ws.getIndBilaTrchEstr().setCptRiastoEcc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-CPT-RIASTO-ECC
            ws.getIndBilaTrchEstr().setCptRiastoEcc(((short)0));
        }
        // COB_CODE: IF B03-PRE-RIASTO-ECC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-PRE-RIASTO-ECC
        //           ELSE
        //              MOVE 0 TO IND-B03-PRE-RIASTO-ECC
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03PreRiastoEcc().getB03PreRiastoEccNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-PRE-RIASTO-ECC
            ws.getIndBilaTrchEstr().setPreRiastoEcc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-PRE-RIASTO-ECC
            ws.getIndBilaTrchEstr().setPreRiastoEcc(((short)0));
        }
        // COB_CODE: IF B03-RIS-RIASTA-ECC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-RIS-RIASTA-ECC
        //           ELSE
        //              MOVE 0 TO IND-B03-RIS-RIASTA-ECC
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03RisRiastaEcc().getB03RisRiastaEccNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-RIS-RIASTA-ECC
            ws.getIndBilaTrchEstr().setRisRiastaEcc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-RIS-RIASTA-ECC
            ws.getIndBilaTrchEstr().setRisRiastaEcc(((short)0));
        }
        // COB_CODE: IF B03-COD-AGE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-COD-AGE
        //           ELSE
        //              MOVE 0 TO IND-B03-COD-AGE
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CodAge().getB03CodAgeNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-COD-AGE
            ws.getIndBilaTrchEstr().setCodAge(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-COD-AGE
            ws.getIndBilaTrchEstr().setCodAge(((short)0));
        }
        // COB_CODE: IF B03-COD-SUBAGE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-COD-SUBAGE
        //           ELSE
        //              MOVE 0 TO IND-B03-COD-SUBAGE
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CodSubage().getB03CodSubageNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-COD-SUBAGE
            ws.getIndBilaTrchEstr().setCodSubage(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-COD-SUBAGE
            ws.getIndBilaTrchEstr().setCodSubage(((short)0));
        }
        // COB_CODE: IF B03-COD-CAN-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-COD-CAN
        //           ELSE
        //              MOVE 0 TO IND-B03-COD-CAN
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CodCan().getB03CodCanNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-COD-CAN
            ws.getIndBilaTrchEstr().setCodCan(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-COD-CAN
            ws.getIndBilaTrchEstr().setCodCan(((short)0));
        }
        // COB_CODE: IF B03-IB-POLI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-IB-POLI
        //           ELSE
        //              MOVE 0 TO IND-B03-IB-POLI
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03IbPoli(), BilaTrchEstrIdbsb030.Len.B03_IB_POLI)) {
            // COB_CODE: MOVE -1 TO IND-B03-IB-POLI
            ws.getIndBilaTrchEstr().setIbPoli(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-IB-POLI
            ws.getIndBilaTrchEstr().setIbPoli(((short)0));
        }
        // COB_CODE: IF B03-IB-ADES-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-IB-ADES
        //           ELSE
        //              MOVE 0 TO IND-B03-IB-ADES
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03IbAdes(), BilaTrchEstrIdbsb030.Len.B03_IB_ADES)) {
            // COB_CODE: MOVE -1 TO IND-B03-IB-ADES
            ws.getIndBilaTrchEstr().setIbAdes(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-IB-ADES
            ws.getIndBilaTrchEstr().setIbAdes(((short)0));
        }
        // COB_CODE: IF B03-IB-TRCH-DI-GAR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-IB-TRCH-DI-GAR
        //           ELSE
        //              MOVE 0 TO IND-B03-IB-TRCH-DI-GAR
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03IbTrchDiGar(), BilaTrchEstrIdbsb030.Len.B03_IB_TRCH_DI_GAR)) {
            // COB_CODE: MOVE -1 TO IND-B03-IB-TRCH-DI-GAR
            ws.getIndBilaTrchEstr().setIbTrchDiGar(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-IB-TRCH-DI-GAR
            ws.getIndBilaTrchEstr().setIbTrchDiGar(((short)0));
        }
        // COB_CODE: IF B03-TP-PRSTZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-TP-PRSTZ
        //           ELSE
        //              MOVE 0 TO IND-B03-TP-PRSTZ
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03TpPrstzFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-TP-PRSTZ
            ws.getIndBilaTrchEstr().setTpPrstz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-TP-PRSTZ
            ws.getIndBilaTrchEstr().setTpPrstz(((short)0));
        }
        // COB_CODE: IF B03-TP-TRASF-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-TP-TRASF
        //           ELSE
        //              MOVE 0 TO IND-B03-TP-TRASF
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03TpTrasfFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-TP-TRASF
            ws.getIndBilaTrchEstr().setTpTrasf(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-TP-TRASF
            ws.getIndBilaTrchEstr().setTpTrasf(((short)0));
        }
        // COB_CODE: IF B03-PP-INVRIO-TARI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-PP-INVRIO-TARI
        //           ELSE
        //              MOVE 0 TO IND-B03-PP-INVRIO-TARI
        //           END-IF
        if (Conditions.eq(bilaTrchEstr.getB03PpInvrioTari(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-B03-PP-INVRIO-TARI
            ws.getIndBilaTrchEstr().setPpInvrioTari(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-PP-INVRIO-TARI
            ws.getIndBilaTrchEstr().setPpInvrioTari(((short)0));
        }
        // COB_CODE: IF B03-COEFF-OPZ-REN-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-COEFF-OPZ-REN
        //           ELSE
        //              MOVE 0 TO IND-B03-COEFF-OPZ-REN
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CoeffOpzRen().getB03CoeffOpzRenNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-COEFF-OPZ-REN
            ws.getIndBilaTrchEstr().setCoeffOpzRen(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-COEFF-OPZ-REN
            ws.getIndBilaTrchEstr().setCoeffOpzRen(((short)0));
        }
        // COB_CODE: IF B03-COEFF-OPZ-CPT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-COEFF-OPZ-CPT
        //           ELSE
        //              MOVE 0 TO IND-B03-COEFF-OPZ-CPT
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CoeffOpzCpt().getB03CoeffOpzCptNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-COEFF-OPZ-CPT
            ws.getIndBilaTrchEstr().setCoeffOpzCpt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-COEFF-OPZ-CPT
            ws.getIndBilaTrchEstr().setCoeffOpzCpt(((short)0));
        }
        // COB_CODE: IF B03-DUR-PAG-REN-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DUR-PAG-REN
        //           ELSE
        //              MOVE 0 TO IND-B03-DUR-PAG-REN
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03DurPagRen().getB03DurPagRenNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DUR-PAG-REN
            ws.getIndBilaTrchEstr().setDurPagRen(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DUR-PAG-REN
            ws.getIndBilaTrchEstr().setDurPagRen(((short)0));
        }
        // COB_CODE: IF B03-VLT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-VLT
        //           ELSE
        //              MOVE 0 TO IND-B03-VLT
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03VltFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-VLT
            ws.getIndBilaTrchEstr().setVlt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-VLT
            ws.getIndBilaTrchEstr().setVlt(((short)0));
        }
        // COB_CODE: IF B03-RIS-MAT-CHIU-PREC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-RIS-MAT-CHIU-PREC
        //           ELSE
        //              MOVE 0 TO IND-B03-RIS-MAT-CHIU-PREC
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03RisMatChiuPrec().getB03RisMatChiuPrecNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-RIS-MAT-CHIU-PREC
            ws.getIndBilaTrchEstr().setRisMatChiuPrec(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-RIS-MAT-CHIU-PREC
            ws.getIndBilaTrchEstr().setRisMatChiuPrec(((short)0));
        }
        // COB_CODE: IF B03-COD-FND-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-COD-FND
        //           ELSE
        //              MOVE 0 TO IND-B03-COD-FND
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CodFndFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-COD-FND
            ws.getIndBilaTrchEstr().setCodFnd(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-COD-FND
            ws.getIndBilaTrchEstr().setCodFnd(((short)0));
        }
        // COB_CODE: IF B03-PRSTZ-T-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-PRSTZ-T
        //           ELSE
        //              MOVE 0 TO IND-B03-PRSTZ-T
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03PrstzT().getB03PrstzTNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-PRSTZ-T
            ws.getIndBilaTrchEstr().setPrstzT(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-PRSTZ-T
            ws.getIndBilaTrchEstr().setPrstzT(((short)0));
        }
        // COB_CODE: IF B03-TS-TARI-DOV-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-TS-TARI-DOV
        //           ELSE
        //              MOVE 0 TO IND-B03-TS-TARI-DOV
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03TsTariDov().getB03TsTariDovNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-TS-TARI-DOV
            ws.getIndBilaTrchEstr().setTsTariDov(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-TS-TARI-DOV
            ws.getIndBilaTrchEstr().setTsTariDov(((short)0));
        }
        // COB_CODE: IF B03-TS-TARI-SCON-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-TS-TARI-SCON
        //           ELSE
        //              MOVE 0 TO IND-B03-TS-TARI-SCON
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03TsTariScon().getB03TsTariSconNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-TS-TARI-SCON
            ws.getIndBilaTrchEstr().setTsTariScon(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-TS-TARI-SCON
            ws.getIndBilaTrchEstr().setTsTariScon(((short)0));
        }
        // COB_CODE: IF B03-TS-PP-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-TS-PP
        //           ELSE
        //              MOVE 0 TO IND-B03-TS-PP
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03TsPp().getB03TsPpNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-TS-PP
            ws.getIndBilaTrchEstr().setTsPp(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-TS-PP
            ws.getIndBilaTrchEstr().setTsPp(((short)0));
        }
        // COB_CODE: IF B03-COEFF-RIS-1-T-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-COEFF-RIS-1-T
        //           ELSE
        //              MOVE 0 TO IND-B03-COEFF-RIS-1-T
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CoeffRis1T().getB03CoeffRis1TNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-COEFF-RIS-1-T
            ws.getIndBilaTrchEstr().setCoeffRis1T(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-COEFF-RIS-1-T
            ws.getIndBilaTrchEstr().setCoeffRis1T(((short)0));
        }
        // COB_CODE: IF B03-COEFF-RIS-2-T-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-COEFF-RIS-2-T
        //           ELSE
        //              MOVE 0 TO IND-B03-COEFF-RIS-2-T
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CoeffRis2T().getB03CoeffRis2TNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-COEFF-RIS-2-T
            ws.getIndBilaTrchEstr().setCoeffRis2T(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-COEFF-RIS-2-T
            ws.getIndBilaTrchEstr().setCoeffRis2T(((short)0));
        }
        // COB_CODE: IF B03-ABB-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-ABB
        //           ELSE
        //              MOVE 0 TO IND-B03-ABB
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03Abb().getB03AbbNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-ABB
            ws.getIndBilaTrchEstr().setAbb(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-ABB
            ws.getIndBilaTrchEstr().setAbb(((short)0));
        }
        // COB_CODE: IF B03-TP-COASS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-TP-COASS
        //           ELSE
        //              MOVE 0 TO IND-B03-TP-COASS
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03TpCoassFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-TP-COASS
            ws.getIndBilaTrchEstr().setTpCoass(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-TP-COASS
            ws.getIndBilaTrchEstr().setTpCoass(((short)0));
        }
        // COB_CODE: IF B03-TRAT-RIASS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-TRAT-RIASS
        //           ELSE
        //              MOVE 0 TO IND-B03-TRAT-RIASS
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03TratRiassFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-TRAT-RIASS
            ws.getIndBilaTrchEstr().setTratRiass(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-TRAT-RIASS
            ws.getIndBilaTrchEstr().setTratRiass(((short)0));
        }
        // COB_CODE: IF B03-TRAT-RIASS-ECC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-TRAT-RIASS-ECC
        //           ELSE
        //              MOVE 0 TO IND-B03-TRAT-RIASS-ECC
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03TratRiassEccFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-TRAT-RIASS-ECC
            ws.getIndBilaTrchEstr().setTratRiassEcc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-TRAT-RIASS-ECC
            ws.getIndBilaTrchEstr().setTratRiassEcc(((short)0));
        }
        // COB_CODE: IF B03-TP-RGM-FISC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-TP-RGM-FISC
        //           ELSE
        //              MOVE 0 TO IND-B03-TP-RGM-FISC
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03TpRgmFiscFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-TP-RGM-FISC
            ws.getIndBilaTrchEstr().setTpRgmFisc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-TP-RGM-FISC
            ws.getIndBilaTrchEstr().setTpRgmFisc(((short)0));
        }
        // COB_CODE: IF B03-DUR-GAR-AA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DUR-GAR-AA
        //           ELSE
        //              MOVE 0 TO IND-B03-DUR-GAR-AA
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03DurGarAa().getB03DurGarAaNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DUR-GAR-AA
            ws.getIndBilaTrchEstr().setDurGarAa(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DUR-GAR-AA
            ws.getIndBilaTrchEstr().setDurGarAa(((short)0));
        }
        // COB_CODE: IF B03-DUR-GAR-MM-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DUR-GAR-MM
        //           ELSE
        //              MOVE 0 TO IND-B03-DUR-GAR-MM
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03DurGarMm().getB03DurGarMmNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DUR-GAR-MM
            ws.getIndBilaTrchEstr().setDurGarMm(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DUR-GAR-MM
            ws.getIndBilaTrchEstr().setDurGarMm(((short)0));
        }
        // COB_CODE: IF B03-DUR-GAR-GG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DUR-GAR-GG
        //           ELSE
        //              MOVE 0 TO IND-B03-DUR-GAR-GG
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03DurGarGg().getB03DurGarGgNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DUR-GAR-GG
            ws.getIndBilaTrchEstr().setDurGarGg(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DUR-GAR-GG
            ws.getIndBilaTrchEstr().setDurGarGg(((short)0));
        }
        // COB_CODE: IF B03-ANTIDUR-CALC-365-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-ANTIDUR-CALC-365
        //           ELSE
        //              MOVE 0 TO IND-B03-ANTIDUR-CALC-365
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03AntidurCalc365().getB03AntidurCalc365NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-ANTIDUR-CALC-365
            ws.getIndBilaTrchEstr().setAntidurCalc365(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-ANTIDUR-CALC-365
            ws.getIndBilaTrchEstr().setAntidurCalc365(((short)0));
        }
        // COB_CODE: IF B03-COD-FISC-CNTR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-COD-FISC-CNTR
        //           ELSE
        //              MOVE 0 TO IND-B03-COD-FISC-CNTR
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CodFiscCntrFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-COD-FISC-CNTR
            ws.getIndBilaTrchEstr().setCodFiscCntr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-COD-FISC-CNTR
            ws.getIndBilaTrchEstr().setCodFiscCntr(((short)0));
        }
        // COB_CODE: IF B03-COD-FISC-ASSTO1-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-COD-FISC-ASSTO1
        //           ELSE
        //              MOVE 0 TO IND-B03-COD-FISC-ASSTO1
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CodFiscAssto1Formatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-COD-FISC-ASSTO1
            ws.getIndBilaTrchEstr().setCodFiscAssto1(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-COD-FISC-ASSTO1
            ws.getIndBilaTrchEstr().setCodFiscAssto1(((short)0));
        }
        // COB_CODE: IF B03-COD-FISC-ASSTO2-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-COD-FISC-ASSTO2
        //           ELSE
        //              MOVE 0 TO IND-B03-COD-FISC-ASSTO2
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CodFiscAssto2Formatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-COD-FISC-ASSTO2
            ws.getIndBilaTrchEstr().setCodFiscAssto2(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-COD-FISC-ASSTO2
            ws.getIndBilaTrchEstr().setCodFiscAssto2(((short)0));
        }
        // COB_CODE: IF B03-COD-FISC-ASSTO3-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-COD-FISC-ASSTO3
        //           ELSE
        //              MOVE 0 TO IND-B03-COD-FISC-ASSTO3
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CodFiscAssto3Formatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-COD-FISC-ASSTO3
            ws.getIndBilaTrchEstr().setCodFiscAssto3(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-COD-FISC-ASSTO3
            ws.getIndBilaTrchEstr().setCodFiscAssto3(((short)0));
        }
        // COB_CODE: IF B03-CAUS-SCON = HIGH-VALUES
        //              MOVE -1 TO IND-B03-CAUS-SCON
        //           ELSE
        //              MOVE 0 TO IND-B03-CAUS-SCON
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CausScon(), BilaTrchEstrIdbsb030.Len.B03_CAUS_SCON)) {
            // COB_CODE: MOVE -1 TO IND-B03-CAUS-SCON
            ws.getIndBilaTrchEstr().setCausScon(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-CAUS-SCON
            ws.getIndBilaTrchEstr().setCausScon(((short)0));
        }
        // COB_CODE: IF B03-EMIT-TIT-OPZ = HIGH-VALUES
        //              MOVE -1 TO IND-B03-EMIT-TIT-OPZ
        //           ELSE
        //              MOVE 0 TO IND-B03-EMIT-TIT-OPZ
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03EmitTitOpz(), BilaTrchEstrIdbsb030.Len.B03_EMIT_TIT_OPZ)) {
            // COB_CODE: MOVE -1 TO IND-B03-EMIT-TIT-OPZ
            ws.getIndBilaTrchEstr().setEmitTitOpz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-EMIT-TIT-OPZ
            ws.getIndBilaTrchEstr().setEmitTitOpz(((short)0));
        }
        // COB_CODE: IF B03-QTZ-SP-Z-COUP-EMIS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-QTZ-SP-Z-COUP-EMIS
        //           ELSE
        //              MOVE 0 TO IND-B03-QTZ-SP-Z-COUP-EMIS
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03QtzSpZCoupEmis().getB03QtzSpZCoupEmisNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-QTZ-SP-Z-COUP-EMIS
            ws.getIndBilaTrchEstr().setQtzSpZCoupEmis(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-QTZ-SP-Z-COUP-EMIS
            ws.getIndBilaTrchEstr().setQtzSpZCoupEmis(((short)0));
        }
        // COB_CODE: IF B03-QTZ-SP-Z-OPZ-EMIS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-QTZ-SP-Z-OPZ-EMIS
        //           ELSE
        //              MOVE 0 TO IND-B03-QTZ-SP-Z-OPZ-EMIS
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03QtzSpZOpzEmis().getB03QtzSpZOpzEmisNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-QTZ-SP-Z-OPZ-EMIS
            ws.getIndBilaTrchEstr().setQtzSpZOpzEmis(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-QTZ-SP-Z-OPZ-EMIS
            ws.getIndBilaTrchEstr().setQtzSpZOpzEmis(((short)0));
        }
        // COB_CODE: IF B03-QTZ-SP-Z-COUP-DT-C-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-QTZ-SP-Z-COUP-DT-C
        //           ELSE
        //              MOVE 0 TO IND-B03-QTZ-SP-Z-COUP-DT-C
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03QtzSpZCoupDtC().getB03QtzSpZCoupDtCNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-QTZ-SP-Z-COUP-DT-C
            ws.getIndBilaTrchEstr().setQtzSpZCoupDtC(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-QTZ-SP-Z-COUP-DT-C
            ws.getIndBilaTrchEstr().setQtzSpZCoupDtC(((short)0));
        }
        // COB_CODE: IF B03-QTZ-SP-Z-OPZ-DT-CA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-QTZ-SP-Z-OPZ-DT-CA
        //           ELSE
        //              MOVE 0 TO IND-B03-QTZ-SP-Z-OPZ-DT-CA
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03QtzSpZOpzDtCa().getB03QtzSpZOpzDtCaNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-QTZ-SP-Z-OPZ-DT-CA
            ws.getIndBilaTrchEstr().setQtzSpZOpzDtCa(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-QTZ-SP-Z-OPZ-DT-CA
            ws.getIndBilaTrchEstr().setQtzSpZOpzDtCa(((short)0));
        }
        // COB_CODE: IF B03-QTZ-TOT-EMIS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-QTZ-TOT-EMIS
        //           ELSE
        //              MOVE 0 TO IND-B03-QTZ-TOT-EMIS
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03QtzTotEmis().getB03QtzTotEmisNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-QTZ-TOT-EMIS
            ws.getIndBilaTrchEstr().setQtzTotEmis(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-QTZ-TOT-EMIS
            ws.getIndBilaTrchEstr().setQtzTotEmis(((short)0));
        }
        // COB_CODE: IF B03-QTZ-TOT-DT-CALC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-QTZ-TOT-DT-CALC
        //           ELSE
        //              MOVE 0 TO IND-B03-QTZ-TOT-DT-CALC
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03QtzTotDtCalc().getB03QtzTotDtCalcNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-QTZ-TOT-DT-CALC
            ws.getIndBilaTrchEstr().setQtzTotDtCalc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-QTZ-TOT-DT-CALC
            ws.getIndBilaTrchEstr().setQtzTotDtCalc(((short)0));
        }
        // COB_CODE: IF B03-QTZ-TOT-DT-ULT-BIL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-QTZ-TOT-DT-ULT-BIL
        //           ELSE
        //              MOVE 0 TO IND-B03-QTZ-TOT-DT-ULT-BIL
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03QtzTotDtUltBil().getB03QtzTotDtUltBilNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-QTZ-TOT-DT-ULT-BIL
            ws.getIndBilaTrchEstr().setQtzTotDtUltBil(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-QTZ-TOT-DT-ULT-BIL
            ws.getIndBilaTrchEstr().setQtzTotDtUltBil(((short)0));
        }
        // COB_CODE: IF B03-DT-QTZ-EMIS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DT-QTZ-EMIS
        //           ELSE
        //              MOVE 0 TO IND-B03-DT-QTZ-EMIS
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03DtQtzEmis().getB03DtQtzEmisNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DT-QTZ-EMIS
            ws.getIndBilaTrchEstr().setDtQtzEmis(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DT-QTZ-EMIS
            ws.getIndBilaTrchEstr().setDtQtzEmis(((short)0));
        }
        // COB_CODE: IF B03-PC-CAR-GEST-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-PC-CAR-GEST
        //           ELSE
        //              MOVE 0 TO IND-B03-PC-CAR-GEST
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03PcCarGest().getB03PcCarGestNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-PC-CAR-GEST
            ws.getIndBilaTrchEstr().setPcCarGest(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-PC-CAR-GEST
            ws.getIndBilaTrchEstr().setPcCarGest(((short)0));
        }
        // COB_CODE: IF B03-PC-CAR-ACQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-PC-CAR-ACQ
        //           ELSE
        //              MOVE 0 TO IND-B03-PC-CAR-ACQ
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03PcCarAcq().getB03PcCarAcqNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-PC-CAR-ACQ
            ws.getIndBilaTrchEstr().setPcCarAcq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-PC-CAR-ACQ
            ws.getIndBilaTrchEstr().setPcCarAcq(((short)0));
        }
        // COB_CODE: IF B03-IMP-CAR-CASO-MOR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-IMP-CAR-CASO-MOR
        //           ELSE
        //              MOVE 0 TO IND-B03-IMP-CAR-CASO-MOR
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03ImpCarCasoMor().getB03ImpCarCasoMorNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-IMP-CAR-CASO-MOR
            ws.getIndBilaTrchEstr().setImpCarCasoMor(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-IMP-CAR-CASO-MOR
            ws.getIndBilaTrchEstr().setImpCarCasoMor(((short)0));
        }
        // COB_CODE: IF B03-PC-CAR-MOR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-PC-CAR-MOR
        //           ELSE
        //              MOVE 0 TO IND-B03-PC-CAR-MOR
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03PcCarMor().getB03PcCarMorNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-PC-CAR-MOR
            ws.getIndBilaTrchEstr().setPcCarMor(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-PC-CAR-MOR
            ws.getIndBilaTrchEstr().setPcCarMor(((short)0));
        }
        // COB_CODE: IF B03-TP-VERS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-TP-VERS
        //           ELSE
        //              MOVE 0 TO IND-B03-TP-VERS
        //           END-IF
        if (Conditions.eq(bilaTrchEstr.getB03TpVers(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-B03-TP-VERS
            ws.getIndBilaTrchEstr().setTpVers(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-TP-VERS
            ws.getIndBilaTrchEstr().setTpVers(((short)0));
        }
        // COB_CODE: IF B03-FL-SWITCH-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-FL-SWITCH
        //           ELSE
        //              MOVE 0 TO IND-B03-FL-SWITCH
        //           END-IF
        if (Conditions.eq(bilaTrchEstr.getB03FlSwitch(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-B03-FL-SWITCH
            ws.getIndBilaTrchEstr().setFlSwitch(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-FL-SWITCH
            ws.getIndBilaTrchEstr().setFlSwitch(((short)0));
        }
        // COB_CODE: IF B03-FL-IAS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-FL-IAS
        //           ELSE
        //              MOVE 0 TO IND-B03-FL-IAS
        //           END-IF
        if (Conditions.eq(bilaTrchEstr.getB03FlIas(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-B03-FL-IAS
            ws.getIndBilaTrchEstr().setFlIas(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-FL-IAS
            ws.getIndBilaTrchEstr().setFlIas(((short)0));
        }
        // COB_CODE: IF B03-DIR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DIR
        //           ELSE
        //              MOVE 0 TO IND-B03-DIR
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03Dir().getB03DirNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DIR
            ws.getIndBilaTrchEstr().setDir(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DIR
            ws.getIndBilaTrchEstr().setDir(((short)0));
        }
        // COB_CODE: IF B03-TP-COP-CASO-MOR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-TP-COP-CASO-MOR
        //           ELSE
        //              MOVE 0 TO IND-B03-TP-COP-CASO-MOR
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03TpCopCasoMorFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-TP-COP-CASO-MOR
            ws.getIndBilaTrchEstr().setTpCopCasoMor(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-TP-COP-CASO-MOR
            ws.getIndBilaTrchEstr().setTpCopCasoMor(((short)0));
        }
        // COB_CODE: IF B03-MET-RISC-SPCL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-MET-RISC-SPCL
        //           ELSE
        //              MOVE 0 TO IND-B03-MET-RISC-SPCL
        //           END-IF
        if (Conditions.eq(bilaTrchEstr.getB03MetRiscSpcl().getB03MetRiscSpclNull(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-B03-MET-RISC-SPCL
            ws.getIndBilaTrchEstr().setMetRiscSpcl(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-MET-RISC-SPCL
            ws.getIndBilaTrchEstr().setMetRiscSpcl(((short)0));
        }
        // COB_CODE: IF B03-TP-STAT-INVST-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-TP-STAT-INVST
        //           ELSE
        //              MOVE 0 TO IND-B03-TP-STAT-INVST
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03TpStatInvstFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-TP-STAT-INVST
            ws.getIndBilaTrchEstr().setTpStatInvst(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-TP-STAT-INVST
            ws.getIndBilaTrchEstr().setTpStatInvst(((short)0));
        }
        // COB_CODE: IF B03-COD-PRDT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-COD-PRDT
        //           ELSE
        //              MOVE 0 TO IND-B03-COD-PRDT
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CodPrdt().getB03CodPrdtNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-COD-PRDT
            ws.getIndBilaTrchEstr().setCodPrdt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-COD-PRDT
            ws.getIndBilaTrchEstr().setCodPrdt(((short)0));
        }
        // COB_CODE: IF B03-STAT-ASSTO-1-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-STAT-ASSTO-1
        //           ELSE
        //              MOVE 0 TO IND-B03-STAT-ASSTO-1
        //           END-IF
        if (Conditions.eq(bilaTrchEstr.getB03StatAssto1(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-B03-STAT-ASSTO-1
            ws.getIndBilaTrchEstr().setStatAssto1(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-STAT-ASSTO-1
            ws.getIndBilaTrchEstr().setStatAssto1(((short)0));
        }
        // COB_CODE: IF B03-STAT-ASSTO-2-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-STAT-ASSTO-2
        //           ELSE
        //              MOVE 0 TO IND-B03-STAT-ASSTO-2
        //           END-IF
        if (Conditions.eq(bilaTrchEstr.getB03StatAssto2(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-B03-STAT-ASSTO-2
            ws.getIndBilaTrchEstr().setStatAssto2(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-STAT-ASSTO-2
            ws.getIndBilaTrchEstr().setStatAssto2(((short)0));
        }
        // COB_CODE: IF B03-STAT-ASSTO-3-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-STAT-ASSTO-3
        //           ELSE
        //              MOVE 0 TO IND-B03-STAT-ASSTO-3
        //           END-IF
        if (Conditions.eq(bilaTrchEstr.getB03StatAssto3(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-B03-STAT-ASSTO-3
            ws.getIndBilaTrchEstr().setStatAssto3(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-STAT-ASSTO-3
            ws.getIndBilaTrchEstr().setStatAssto3(((short)0));
        }
        // COB_CODE: IF B03-CPT-ASSTO-INI-MOR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-CPT-ASSTO-INI-MOR
        //           ELSE
        //              MOVE 0 TO IND-B03-CPT-ASSTO-INI-MOR
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CptAsstoIniMor().getB03CptAsstoIniMorNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-CPT-ASSTO-INI-MOR
            ws.getIndBilaTrchEstr().setCptAsstoIniMor(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-CPT-ASSTO-INI-MOR
            ws.getIndBilaTrchEstr().setCptAsstoIniMor(((short)0));
        }
        // COB_CODE: IF B03-TS-STAB-PRE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-TS-STAB-PRE
        //           ELSE
        //              MOVE 0 TO IND-B03-TS-STAB-PRE
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03TsStabPre().getB03TsStabPreNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-TS-STAB-PRE
            ws.getIndBilaTrchEstr().setTsStabPre(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-TS-STAB-PRE
            ws.getIndBilaTrchEstr().setTsStabPre(((short)0));
        }
        // COB_CODE: IF B03-DIR-EMIS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DIR-EMIS
        //           ELSE
        //              MOVE 0 TO IND-B03-DIR-EMIS
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03DirEmis().getB03DirEmisNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DIR-EMIS
            ws.getIndBilaTrchEstr().setDirEmis(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DIR-EMIS
            ws.getIndBilaTrchEstr().setDirEmis(((short)0));
        }
        // COB_CODE: IF B03-DT-INC-ULT-PRE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DT-INC-ULT-PRE
        //           ELSE
        //              MOVE 0 TO IND-B03-DT-INC-ULT-PRE
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03DtIncUltPre().getB03DtIncUltPreNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DT-INC-ULT-PRE
            ws.getIndBilaTrchEstr().setDtIncUltPre(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DT-INC-ULT-PRE
            ws.getIndBilaTrchEstr().setDtIncUltPre(((short)0));
        }
        // COB_CODE: IF B03-STAT-TBGC-ASSTO-1-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-STAT-TBGC-ASSTO-1
        //           ELSE
        //              MOVE 0 TO IND-B03-STAT-TBGC-ASSTO-1
        //           END-IF
        if (Conditions.eq(bilaTrchEstr.getB03StatTbgcAssto1(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-B03-STAT-TBGC-ASSTO-1
            ws.getIndBilaTrchEstr().setStatTbgcAssto1(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-STAT-TBGC-ASSTO-1
            ws.getIndBilaTrchEstr().setStatTbgcAssto1(((short)0));
        }
        // COB_CODE: IF B03-STAT-TBGC-ASSTO-2-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-STAT-TBGC-ASSTO-2
        //           ELSE
        //              MOVE 0 TO IND-B03-STAT-TBGC-ASSTO-2
        //           END-IF
        if (Conditions.eq(bilaTrchEstr.getB03StatTbgcAssto2(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-B03-STAT-TBGC-ASSTO-2
            ws.getIndBilaTrchEstr().setStatTbgcAssto2(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-STAT-TBGC-ASSTO-2
            ws.getIndBilaTrchEstr().setStatTbgcAssto2(((short)0));
        }
        // COB_CODE: IF B03-STAT-TBGC-ASSTO-3-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-STAT-TBGC-ASSTO-3
        //           ELSE
        //              MOVE 0 TO IND-B03-STAT-TBGC-ASSTO-3
        //           END-IF
        if (Conditions.eq(bilaTrchEstr.getB03StatTbgcAssto3(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-B03-STAT-TBGC-ASSTO-3
            ws.getIndBilaTrchEstr().setStatTbgcAssto3(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-STAT-TBGC-ASSTO-3
            ws.getIndBilaTrchEstr().setStatTbgcAssto3(((short)0));
        }
        // COB_CODE: IF B03-FRAZ-DECR-CPT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-FRAZ-DECR-CPT
        //           ELSE
        //              MOVE 0 TO IND-B03-FRAZ-DECR-CPT
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03FrazDecrCpt().getB03FrazDecrCptNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-FRAZ-DECR-CPT
            ws.getIndBilaTrchEstr().setFrazDecrCpt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-FRAZ-DECR-CPT
            ws.getIndBilaTrchEstr().setFrazDecrCpt(((short)0));
        }
        // COB_CODE: IF B03-PRE-PP-ULT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-PRE-PP-ULT
        //           ELSE
        //              MOVE 0 TO IND-B03-PRE-PP-ULT
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03PrePpUlt().getB03PrePpUltNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-PRE-PP-ULT
            ws.getIndBilaTrchEstr().setPrePpUlt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-PRE-PP-ULT
            ws.getIndBilaTrchEstr().setPrePpUlt(((short)0));
        }
        // COB_CODE: IF B03-ACQ-EXP-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-ACQ-EXP
        //           ELSE
        //              MOVE 0 TO IND-B03-ACQ-EXP
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03AcqExp().getB03AcqExpNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-ACQ-EXP
            ws.getIndBilaTrchEstr().setAcqExp(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-ACQ-EXP
            ws.getIndBilaTrchEstr().setAcqExp(((short)0));
        }
        // COB_CODE: IF B03-REMUN-ASS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-REMUN-ASS
        //           ELSE
        //              MOVE 0 TO IND-B03-REMUN-ASS
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03RemunAss().getB03RemunAssNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-REMUN-ASS
            ws.getIndBilaTrchEstr().setRemunAss(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-REMUN-ASS
            ws.getIndBilaTrchEstr().setRemunAss(((short)0));
        }
        // COB_CODE: IF B03-COMMIS-INTER-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-COMMIS-INTER
        //           ELSE
        //              MOVE 0 TO IND-B03-COMMIS-INTER
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CommisInter().getB03CommisInterNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-COMMIS-INTER
            ws.getIndBilaTrchEstr().setCommisInter(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-COMMIS-INTER
            ws.getIndBilaTrchEstr().setCommisInter(((short)0));
        }
        // COB_CODE: IF B03-NUM-FINANZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-NUM-FINANZ
        //           ELSE
        //              MOVE 0 TO IND-B03-NUM-FINANZ
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03NumFinanz(), BilaTrchEstrIdbsb030.Len.B03_NUM_FINANZ)) {
            // COB_CODE: MOVE -1 TO IND-B03-NUM-FINANZ
            ws.getIndBilaTrchEstr().setNumFinanz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-NUM-FINANZ
            ws.getIndBilaTrchEstr().setNumFinanz(((short)0));
        }
        // COB_CODE: IF B03-TP-ACC-COMM-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-TP-ACC-COMM
        //           ELSE
        //              MOVE 0 TO IND-B03-TP-ACC-COMM
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03TpAccCommFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-TP-ACC-COMM
            ws.getIndBilaTrchEstr().setTpAccComm(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-TP-ACC-COMM
            ws.getIndBilaTrchEstr().setTpAccComm(((short)0));
        }
        // COB_CODE: IF B03-IB-ACC-COMM-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-IB-ACC-COMM
        //           ELSE
        //              MOVE 0 TO IND-B03-IB-ACC-COMM
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03IbAccComm(), BilaTrchEstrIdbsb030.Len.B03_IB_ACC_COMM)) {
            // COB_CODE: MOVE -1 TO IND-B03-IB-ACC-COMM
            ws.getIndBilaTrchEstr().setIbAccComm(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-IB-ACC-COMM
            ws.getIndBilaTrchEstr().setIbAccComm(((short)0));
        }
        // COB_CODE: IF B03-CARZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-CARZ
        //           ELSE
        //              MOVE 0 TO IND-B03-CARZ
        //           END-IF.
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03Carz().getB03CarzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-CARZ
            ws.getIndBilaTrchEstr().setCarz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-CARZ
            ws.getIndBilaTrchEstr().setCarz(((short)0));
        }
    }

    /**Original name: Z400-SEQ-RIGA<br>*/
    private void z400SeqRiga() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z500-AGGIORNAMENTO-STORICO<br>*/
    private void z500AggiornamentoStorico() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z550-AGG-STORICO-SOLO-INS<br>*/
    private void z550AggStoricoSoloIns() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z900-CONVERTI-N-TO-X<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da 9(8) comp-3 a date
	 * ----</pre>*/
    private void z900ConvertiNToX() {
        // COB_CODE: MOVE B03-DT-RIS TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(bilaTrchEstr.getB03DtRis(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO B03-DT-RIS-DB
        ws.getBilaTrchEstrDb().setRisDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: MOVE B03-DT-PRODUZIONE TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(bilaTrchEstr.getB03DtProduzione(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO B03-DT-PRODUZIONE-DB
        ws.getBilaTrchEstrDb().setProduzioneDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: IF IND-B03-DT-INI-VAL-TAR = 0
        //               MOVE WS-DATE-X      TO B03-DT-INI-VAL-TAR-DB
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtIniValTar() == 0) {
            // COB_CODE: MOVE B03-DT-INI-VAL-TAR TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(bilaTrchEstr.getB03DtIniValTar().getB03DtIniValTar(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO B03-DT-INI-VAL-TAR-DB
            ws.getBilaTrchEstrDb().setIniValTarDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: MOVE B03-DT-INI-VLDT-PROD TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(bilaTrchEstr.getB03DtIniVldtProd(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO B03-DT-INI-VLDT-PROD-DB
        ws.getBilaTrchEstrDb().setIniVldtProdDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: MOVE B03-DT-DECOR-POLI TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(bilaTrchEstr.getB03DtDecorPoli(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO B03-DT-DECOR-POLI-DB
        ws.getBilaTrchEstrDb().setDecorPoliDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: IF IND-B03-DT-DECOR-ADES = 0
        //               MOVE WS-DATE-X      TO B03-DT-DECOR-ADES-DB
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtDecorAdes() == 0) {
            // COB_CODE: MOVE B03-DT-DECOR-ADES TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(bilaTrchEstr.getB03DtDecorAdes().getB03DtDecorAdes(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO B03-DT-DECOR-ADES-DB
            ws.getBilaTrchEstrDb().setDecorAdesDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: MOVE B03-DT-DECOR-TRCH TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(bilaTrchEstr.getB03DtDecorTrch(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO B03-DT-DECOR-TRCH-DB
        ws.getBilaTrchEstrDb().setDecorTrchDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: MOVE B03-DT-EMIS-POLI TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(bilaTrchEstr.getB03DtEmisPoli(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO B03-DT-EMIS-POLI-DB
        ws.getBilaTrchEstrDb().setEmisPoliDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: IF IND-B03-DT-EMIS-TRCH = 0
        //               MOVE WS-DATE-X      TO B03-DT-EMIS-TRCH-DB
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtEmisTrch() == 0) {
            // COB_CODE: MOVE B03-DT-EMIS-TRCH TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(bilaTrchEstr.getB03DtEmisTrch().getB03DtEmisTrch(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO B03-DT-EMIS-TRCH-DB
            ws.getBilaTrchEstrDb().setEmisTrchDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-B03-DT-SCAD-TRCH = 0
        //               MOVE WS-DATE-X      TO B03-DT-SCAD-TRCH-DB
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtScadTrch() == 0) {
            // COB_CODE: MOVE B03-DT-SCAD-TRCH TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(bilaTrchEstr.getB03DtScadTrch().getB03DtScadTrch(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO B03-DT-SCAD-TRCH-DB
            ws.getBilaTrchEstrDb().setScadTrchDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-B03-DT-SCAD-INTMD = 0
        //               MOVE WS-DATE-X      TO B03-DT-SCAD-INTMD-DB
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtScadIntmd() == 0) {
            // COB_CODE: MOVE B03-DT-SCAD-INTMD TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(bilaTrchEstr.getB03DtScadIntmd().getB03DtScadIntmd(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO B03-DT-SCAD-INTMD-DB
            ws.getBilaTrchEstrDb().setScadIntmdDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-B03-DT-SCAD-PAG-PRE = 0
        //               MOVE WS-DATE-X      TO B03-DT-SCAD-PAG-PRE-DB
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtScadPagPre() == 0) {
            // COB_CODE: MOVE B03-DT-SCAD-PAG-PRE TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(bilaTrchEstr.getB03DtScadPagPre().getB03DtScadPagPre(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO B03-DT-SCAD-PAG-PRE-DB
            ws.getBilaTrchEstrDb().setScadPagPreDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-B03-DT-ULT-PRE-PAG = 0
        //               MOVE WS-DATE-X      TO B03-DT-ULT-PRE-PAG-DB
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtUltPrePag() == 0) {
            // COB_CODE: MOVE B03-DT-ULT-PRE-PAG TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(bilaTrchEstr.getB03DtUltPrePag().getB03DtUltPrePag(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO B03-DT-ULT-PRE-PAG-DB
            ws.getBilaTrchEstrDb().setUltPrePagDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-B03-DT-NASC-1O-ASSTO = 0
        //               MOVE WS-DATE-X      TO B03-DT-NASC-1O-ASSTO-DB
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtNasc1oAssto() == 0) {
            // COB_CODE: MOVE B03-DT-NASC-1O-ASSTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(bilaTrchEstr.getB03DtNasc1oAssto().getB03DtNasc1oAssto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO B03-DT-NASC-1O-ASSTO-DB
            ws.getBilaTrchEstrDb().setNasc1oAsstoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-B03-DT-EFF-CAMB-STAT = 0
        //               MOVE WS-DATE-X      TO B03-DT-EFF-CAMB-STAT-DB
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtEffCambStat() == 0) {
            // COB_CODE: MOVE B03-DT-EFF-CAMB-STAT TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(bilaTrchEstr.getB03DtEffCambStat().getB03DtEffCambStat(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO B03-DT-EFF-CAMB-STAT-DB
            ws.getBilaTrchEstrDb().setEffCambStatDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-B03-DT-EMIS-CAMB-STAT = 0
        //               MOVE WS-DATE-X      TO B03-DT-EMIS-CAMB-STAT-DB
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtEmisCambStat() == 0) {
            // COB_CODE: MOVE B03-DT-EMIS-CAMB-STAT TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(bilaTrchEstr.getB03DtEmisCambStat().getB03DtEmisCambStat(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO B03-DT-EMIS-CAMB-STAT-DB
            ws.getBilaTrchEstrDb().setEmisCambStatDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-B03-DT-EFF-STAB = 0
        //               MOVE WS-DATE-X      TO B03-DT-EFF-STAB-DB
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtEffStab() == 0) {
            // COB_CODE: MOVE B03-DT-EFF-STAB TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(bilaTrchEstr.getB03DtEffStab().getB03DtEffStab(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO B03-DT-EFF-STAB-DB
            ws.getBilaTrchEstrDb().setEffStabDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-B03-DT-EFF-RIDZ = 0
        //               MOVE WS-DATE-X      TO B03-DT-EFF-RIDZ-DB
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtEffRidz() == 0) {
            // COB_CODE: MOVE B03-DT-EFF-RIDZ TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(bilaTrchEstr.getB03DtEffRidz().getB03DtEffRidz(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO B03-DT-EFF-RIDZ-DB
            ws.getBilaTrchEstrDb().setEffRidzDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-B03-DT-EMIS-RIDZ = 0
        //               MOVE WS-DATE-X      TO B03-DT-EMIS-RIDZ-DB
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtEmisRidz() == 0) {
            // COB_CODE: MOVE B03-DT-EMIS-RIDZ TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(bilaTrchEstr.getB03DtEmisRidz().getB03DtEmisRidz(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO B03-DT-EMIS-RIDZ-DB
            ws.getBilaTrchEstrDb().setEmisRidzDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-B03-DT-ULT-RIVAL = 0
        //               MOVE WS-DATE-X      TO B03-DT-ULT-RIVAL-DB
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtUltRival() == 0) {
            // COB_CODE: MOVE B03-DT-ULT-RIVAL TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(bilaTrchEstr.getB03DtUltRival().getB03DtUltRival(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO B03-DT-ULT-RIVAL-DB
            ws.getBilaTrchEstrDb().setUltRivalDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-B03-DT-QTZ-EMIS = 0
        //               MOVE WS-DATE-X      TO B03-DT-QTZ-EMIS-DB
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtQtzEmis() == 0) {
            // COB_CODE: MOVE B03-DT-QTZ-EMIS TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(bilaTrchEstr.getB03DtQtzEmis().getB03DtQtzEmis(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO B03-DT-QTZ-EMIS-DB
            ws.getBilaTrchEstrDb().setQtzEmisDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-B03-DT-INC-ULT-PRE = 0
        //               MOVE WS-DATE-X      TO B03-DT-INC-ULT-PRE-DB
        //           END-IF.
        if (ws.getIndBilaTrchEstr().getDtIncUltPre() == 0) {
            // COB_CODE: MOVE B03-DT-INC-ULT-PRE TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(bilaTrchEstr.getB03DtIncUltPre().getB03DtIncUltPre(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO B03-DT-INC-ULT-PRE-DB
            ws.getBilaTrchEstrDb().setIncUltPreDb(ws.getIdsv0010().getWsDateX());
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE B03-DT-RIS-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getBilaTrchEstrDb().getRisDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO B03-DT-RIS
        bilaTrchEstr.setB03DtRis(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE B03-DT-PRODUZIONE-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getBilaTrchEstrDb().getProduzioneDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO B03-DT-PRODUZIONE
        bilaTrchEstr.setB03DtProduzione(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-B03-DT-INI-VAL-TAR = 0
        //               MOVE WS-DATE-N      TO B03-DT-INI-VAL-TAR
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtIniValTar() == 0) {
            // COB_CODE: MOVE B03-DT-INI-VAL-TAR-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getBilaTrchEstrDb().getIniValTarDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO B03-DT-INI-VAL-TAR
            bilaTrchEstr.getB03DtIniValTar().setB03DtIniValTar(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: MOVE B03-DT-INI-VLDT-PROD-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getBilaTrchEstrDb().getIniVldtProdDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO B03-DT-INI-VLDT-PROD
        bilaTrchEstr.setB03DtIniVldtProd(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE B03-DT-DECOR-POLI-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getBilaTrchEstrDb().getDecorPoliDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO B03-DT-DECOR-POLI
        bilaTrchEstr.setB03DtDecorPoli(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-B03-DT-DECOR-ADES = 0
        //               MOVE WS-DATE-N      TO B03-DT-DECOR-ADES
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtDecorAdes() == 0) {
            // COB_CODE: MOVE B03-DT-DECOR-ADES-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getBilaTrchEstrDb().getDecorAdesDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO B03-DT-DECOR-ADES
            bilaTrchEstr.getB03DtDecorAdes().setB03DtDecorAdes(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: MOVE B03-DT-DECOR-TRCH-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getBilaTrchEstrDb().getDecorTrchDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO B03-DT-DECOR-TRCH
        bilaTrchEstr.setB03DtDecorTrch(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE B03-DT-EMIS-POLI-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getBilaTrchEstrDb().getEmisPoliDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO B03-DT-EMIS-POLI
        bilaTrchEstr.setB03DtEmisPoli(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-B03-DT-EMIS-TRCH = 0
        //               MOVE WS-DATE-N      TO B03-DT-EMIS-TRCH
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtEmisTrch() == 0) {
            // COB_CODE: MOVE B03-DT-EMIS-TRCH-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getBilaTrchEstrDb().getEmisTrchDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO B03-DT-EMIS-TRCH
            bilaTrchEstr.getB03DtEmisTrch().setB03DtEmisTrch(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-B03-DT-SCAD-TRCH = 0
        //               MOVE WS-DATE-N      TO B03-DT-SCAD-TRCH
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtScadTrch() == 0) {
            // COB_CODE: MOVE B03-DT-SCAD-TRCH-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getBilaTrchEstrDb().getScadTrchDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO B03-DT-SCAD-TRCH
            bilaTrchEstr.getB03DtScadTrch().setB03DtScadTrch(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-B03-DT-SCAD-INTMD = 0
        //               MOVE WS-DATE-N      TO B03-DT-SCAD-INTMD
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtScadIntmd() == 0) {
            // COB_CODE: MOVE B03-DT-SCAD-INTMD-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getBilaTrchEstrDb().getScadIntmdDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO B03-DT-SCAD-INTMD
            bilaTrchEstr.getB03DtScadIntmd().setB03DtScadIntmd(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-B03-DT-SCAD-PAG-PRE = 0
        //               MOVE WS-DATE-N      TO B03-DT-SCAD-PAG-PRE
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtScadPagPre() == 0) {
            // COB_CODE: MOVE B03-DT-SCAD-PAG-PRE-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getBilaTrchEstrDb().getScadPagPreDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO B03-DT-SCAD-PAG-PRE
            bilaTrchEstr.getB03DtScadPagPre().setB03DtScadPagPre(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-B03-DT-ULT-PRE-PAG = 0
        //               MOVE WS-DATE-N      TO B03-DT-ULT-PRE-PAG
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtUltPrePag() == 0) {
            // COB_CODE: MOVE B03-DT-ULT-PRE-PAG-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getBilaTrchEstrDb().getUltPrePagDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO B03-DT-ULT-PRE-PAG
            bilaTrchEstr.getB03DtUltPrePag().setB03DtUltPrePag(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-B03-DT-NASC-1O-ASSTO = 0
        //               MOVE WS-DATE-N      TO B03-DT-NASC-1O-ASSTO
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtNasc1oAssto() == 0) {
            // COB_CODE: MOVE B03-DT-NASC-1O-ASSTO-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getBilaTrchEstrDb().getNasc1oAsstoDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO B03-DT-NASC-1O-ASSTO
            bilaTrchEstr.getB03DtNasc1oAssto().setB03DtNasc1oAssto(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-B03-DT-EFF-CAMB-STAT = 0
        //               MOVE WS-DATE-N      TO B03-DT-EFF-CAMB-STAT
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtEffCambStat() == 0) {
            // COB_CODE: MOVE B03-DT-EFF-CAMB-STAT-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getBilaTrchEstrDb().getEffCambStatDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO B03-DT-EFF-CAMB-STAT
            bilaTrchEstr.getB03DtEffCambStat().setB03DtEffCambStat(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-B03-DT-EMIS-CAMB-STAT = 0
        //               MOVE WS-DATE-N      TO B03-DT-EMIS-CAMB-STAT
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtEmisCambStat() == 0) {
            // COB_CODE: MOVE B03-DT-EMIS-CAMB-STAT-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getBilaTrchEstrDb().getEmisCambStatDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO B03-DT-EMIS-CAMB-STAT
            bilaTrchEstr.getB03DtEmisCambStat().setB03DtEmisCambStat(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-B03-DT-EFF-STAB = 0
        //               MOVE WS-DATE-N      TO B03-DT-EFF-STAB
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtEffStab() == 0) {
            // COB_CODE: MOVE B03-DT-EFF-STAB-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getBilaTrchEstrDb().getEffStabDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO B03-DT-EFF-STAB
            bilaTrchEstr.getB03DtEffStab().setB03DtEffStab(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-B03-DT-EFF-RIDZ = 0
        //               MOVE WS-DATE-N      TO B03-DT-EFF-RIDZ
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtEffRidz() == 0) {
            // COB_CODE: MOVE B03-DT-EFF-RIDZ-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getBilaTrchEstrDb().getEffRidzDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO B03-DT-EFF-RIDZ
            bilaTrchEstr.getB03DtEffRidz().setB03DtEffRidz(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-B03-DT-EMIS-RIDZ = 0
        //               MOVE WS-DATE-N      TO B03-DT-EMIS-RIDZ
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtEmisRidz() == 0) {
            // COB_CODE: MOVE B03-DT-EMIS-RIDZ-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getBilaTrchEstrDb().getEmisRidzDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO B03-DT-EMIS-RIDZ
            bilaTrchEstr.getB03DtEmisRidz().setB03DtEmisRidz(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-B03-DT-ULT-RIVAL = 0
        //               MOVE WS-DATE-N      TO B03-DT-ULT-RIVAL
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtUltRival() == 0) {
            // COB_CODE: MOVE B03-DT-ULT-RIVAL-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getBilaTrchEstrDb().getUltRivalDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO B03-DT-ULT-RIVAL
            bilaTrchEstr.getB03DtUltRival().setB03DtUltRival(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-B03-DT-QTZ-EMIS = 0
        //               MOVE WS-DATE-N      TO B03-DT-QTZ-EMIS
        //           END-IF
        if (ws.getIndBilaTrchEstr().getDtQtzEmis() == 0) {
            // COB_CODE: MOVE B03-DT-QTZ-EMIS-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getBilaTrchEstrDb().getQtzEmisDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO B03-DT-QTZ-EMIS
            bilaTrchEstr.getB03DtQtzEmis().setB03DtQtzEmis(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-B03-DT-INC-ULT-PRE = 0
        //               MOVE WS-DATE-N      TO B03-DT-INC-ULT-PRE
        //           END-IF.
        if (ws.getIndBilaTrchEstr().getDtIncUltPre() == 0) {
            // COB_CODE: MOVE B03-DT-INC-ULT-PRE-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getBilaTrchEstrDb().getIncUltPreDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO B03-DT-INC-ULT-PRE
            bilaTrchEstr.getB03DtIncUltPre().setB03DtIncUltPre(ws.getIdsv0010().getWsDateN());
        }
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
        // COB_CODE: MOVE LENGTH OF B03-CAUS-SCON
        //                       TO B03-CAUS-SCON-LEN
        bilaTrchEstr.setB03CausSconLen(((short)BilaTrchEstrIdbsb030.Len.B03_CAUS_SCON));
        // COB_CODE: MOVE LENGTH OF B03-EMIT-TIT-OPZ
        //                       TO B03-EMIT-TIT-OPZ-LEN.
        bilaTrchEstr.setB03EmitTitOpzLen(((short)BilaTrchEstrIdbsb030.Len.B03_EMIT_TIT_OPZ));
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public int getAaRenCer() {
        return bilaTrchEstr.getB03AaRenCer().getB03AaRenCer();
    }

    @Override
    public void setAaRenCer(int aaRenCer) {
        this.bilaTrchEstr.getB03AaRenCer().setB03AaRenCer(aaRenCer);
    }

    @Override
    public Integer getAaRenCerObj() {
        if (ws.getIndBilaTrchEstr().getAaRenCer() >= 0) {
            return ((Integer)getAaRenCer());
        }
        else {
            return null;
        }
    }

    @Override
    public void setAaRenCerObj(Integer aaRenCerObj) {
        if (aaRenCerObj != null) {
            setAaRenCer(((int)aaRenCerObj));
            ws.getIndBilaTrchEstr().setAaRenCer(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setAaRenCer(((short)-1));
        }
    }

    @Override
    public AfDecimal getAbb() {
        return bilaTrchEstr.getB03Abb().getB03Abb();
    }

    @Override
    public void setAbb(AfDecimal abb) {
        this.bilaTrchEstr.getB03Abb().setB03Abb(abb.copy());
    }

    @Override
    public AfDecimal getAbbObj() {
        if (ws.getIndBilaTrchEstr().getAbb() >= 0) {
            return getAbb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAbbObj(AfDecimal abbObj) {
        if (abbObj != null) {
            setAbb(new AfDecimal(abbObj, 15, 3));
            ws.getIndBilaTrchEstr().setAbb(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setAbb(((short)-1));
        }
    }

    @Override
    public AfDecimal getAcqExp() {
        return bilaTrchEstr.getB03AcqExp().getB03AcqExp();
    }

    @Override
    public void setAcqExp(AfDecimal acqExp) {
        this.bilaTrchEstr.getB03AcqExp().setB03AcqExp(acqExp.copy());
    }

    @Override
    public AfDecimal getAcqExpObj() {
        if (ws.getIndBilaTrchEstr().getAcqExp() >= 0) {
            return getAcqExp();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAcqExpObj(AfDecimal acqExpObj) {
        if (acqExpObj != null) {
            setAcqExp(new AfDecimal(acqExpObj, 15, 3));
            ws.getIndBilaTrchEstr().setAcqExp(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setAcqExp(((short)-1));
        }
    }

    @Override
    public AfDecimal getAlqMargCSubrsh() {
        return bilaTrchEstr.getB03AlqMargCSubrsh().getB03AlqMargCSubrsh();
    }

    @Override
    public void setAlqMargCSubrsh(AfDecimal alqMargCSubrsh) {
        this.bilaTrchEstr.getB03AlqMargCSubrsh().setB03AlqMargCSubrsh(alqMargCSubrsh.copy());
    }

    @Override
    public AfDecimal getAlqMargCSubrshObj() {
        if (ws.getIndBilaTrchEstr().getAlqMargCSubrsh() >= 0) {
            return getAlqMargCSubrsh();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAlqMargCSubrshObj(AfDecimal alqMargCSubrshObj) {
        if (alqMargCSubrshObj != null) {
            setAlqMargCSubrsh(new AfDecimal(alqMargCSubrshObj, 6, 3));
            ws.getIndBilaTrchEstr().setAlqMargCSubrsh(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setAlqMargCSubrsh(((short)-1));
        }
    }

    @Override
    public AfDecimal getAlqMargRis() {
        return bilaTrchEstr.getB03AlqMargRis().getB03AlqMargRis();
    }

    @Override
    public void setAlqMargRis(AfDecimal alqMargRis) {
        this.bilaTrchEstr.getB03AlqMargRis().setB03AlqMargRis(alqMargRis.copy());
    }

    @Override
    public AfDecimal getAlqMargRisObj() {
        if (ws.getIndBilaTrchEstr().getAlqMargRis() >= 0) {
            return getAlqMargRis();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAlqMargRisObj(AfDecimal alqMargRisObj) {
        if (alqMargRisObj != null) {
            setAlqMargRis(new AfDecimal(alqMargRisObj, 6, 3));
            ws.getIndBilaTrchEstr().setAlqMargRis(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setAlqMargRis(((short)-1));
        }
    }

    @Override
    public AfDecimal getAlqRetrT() {
        return bilaTrchEstr.getB03AlqRetrT().getB03AlqRetrT();
    }

    @Override
    public void setAlqRetrT(AfDecimal alqRetrT) {
        this.bilaTrchEstr.getB03AlqRetrT().setB03AlqRetrT(alqRetrT.copy());
    }

    @Override
    public AfDecimal getAlqRetrTObj() {
        if (ws.getIndBilaTrchEstr().getAlqRetrT() >= 0) {
            return getAlqRetrT();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAlqRetrTObj(AfDecimal alqRetrTObj) {
        if (alqRetrTObj != null) {
            setAlqRetrT(new AfDecimal(alqRetrTObj, 6, 3));
            ws.getIndBilaTrchEstr().setAlqRetrT(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setAlqRetrT(((short)-1));
        }
    }

    @Override
    public AfDecimal getAntidurCalc365() {
        return bilaTrchEstr.getB03AntidurCalc365().getB03AntidurCalc365();
    }

    @Override
    public void setAntidurCalc365(AfDecimal antidurCalc365) {
        this.bilaTrchEstr.getB03AntidurCalc365().setB03AntidurCalc365(antidurCalc365.copy());
    }

    @Override
    public AfDecimal getAntidurCalc365Obj() {
        if (ws.getIndBilaTrchEstr().getAntidurCalc365() >= 0) {
            return getAntidurCalc365();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAntidurCalc365Obj(AfDecimal antidurCalc365Obj) {
        if (antidurCalc365Obj != null) {
            setAntidurCalc365(new AfDecimal(antidurCalc365Obj, 11, 7));
            ws.getIndBilaTrchEstr().setAntidurCalc365(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setAntidurCalc365(((short)-1));
        }
    }

    @Override
    public AfDecimal getAntidurDtCalc() {
        return bilaTrchEstr.getB03AntidurDtCalc().getB03AntidurDtCalc();
    }

    @Override
    public void setAntidurDtCalc(AfDecimal antidurDtCalc) {
        this.bilaTrchEstr.getB03AntidurDtCalc().setB03AntidurDtCalc(antidurDtCalc.copy());
    }

    @Override
    public AfDecimal getAntidurDtCalcObj() {
        if (ws.getIndBilaTrchEstr().getAntidurDtCalc() >= 0) {
            return getAntidurDtCalc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAntidurDtCalcObj(AfDecimal antidurDtCalcObj) {
        if (antidurDtCalcObj != null) {
            setAntidurDtCalc(new AfDecimal(antidurDtCalcObj, 11, 7));
            ws.getIndBilaTrchEstr().setAntidurDtCalc(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setAntidurDtCalc(((short)-1));
        }
    }

    @Override
    public int getAntidurRicorPrec() {
        return bilaTrchEstr.getB03AntidurRicorPrec().getB03AntidurRicorPrec();
    }

    @Override
    public void setAntidurRicorPrec(int antidurRicorPrec) {
        this.bilaTrchEstr.getB03AntidurRicorPrec().setB03AntidurRicorPrec(antidurRicorPrec);
    }

    @Override
    public Integer getAntidurRicorPrecObj() {
        if (ws.getIndBilaTrchEstr().getAntidurRicorPrec() >= 0) {
            return ((Integer)getAntidurRicorPrec());
        }
        else {
            return null;
        }
    }

    @Override
    public void setAntidurRicorPrecObj(Integer antidurRicorPrecObj) {
        if (antidurRicorPrecObj != null) {
            setAntidurRicorPrec(((int)antidurRicorPrecObj));
            ws.getIndBilaTrchEstr().setAntidurRicorPrec(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setAntidurRicorPrec(((short)-1));
        }
    }

    @Override
    public int getB03IdAdes() {
        return bilaTrchEstr.getB03IdAdes();
    }

    @Override
    public void setB03IdAdes(int b03IdAdes) {
        this.bilaTrchEstr.setB03IdAdes(b03IdAdes);
    }

    @Override
    public int getB03IdBilaTrchEstr() {
        return bilaTrchEstr.getB03IdBilaTrchEstr();
    }

    @Override
    public void setB03IdBilaTrchEstr(int b03IdBilaTrchEstr) {
        this.bilaTrchEstr.setB03IdBilaTrchEstr(b03IdBilaTrchEstr);
    }

    @Override
    public int getB03IdPoli() {
        return bilaTrchEstr.getB03IdPoli();
    }

    @Override
    public void setB03IdPoli(int b03IdPoli) {
        this.bilaTrchEstr.setB03IdPoli(b03IdPoli);
    }

    @Override
    public int getB03IdRichEstrazMas() {
        return bilaTrchEstr.getB03IdRichEstrazMas();
    }

    @Override
    public void setB03IdRichEstrazMas(int b03IdRichEstrazMas) {
        this.bilaTrchEstr.setB03IdRichEstrazMas(b03IdRichEstrazMas);
    }

    @Override
    public int getB03IdTrchDiGar() {
        return bilaTrchEstr.getB03IdTrchDiGar();
    }

    @Override
    public void setB03IdTrchDiGar(int b03IdTrchDiGar) {
        this.bilaTrchEstr.setB03IdTrchDiGar(b03IdTrchDiGar);
    }

    @Override
    public AfDecimal getCarAcqNonScon() {
        return bilaTrchEstr.getB03CarAcqNonScon().getB03CarAcqNonScon();
    }

    @Override
    public void setCarAcqNonScon(AfDecimal carAcqNonScon) {
        this.bilaTrchEstr.getB03CarAcqNonScon().setB03CarAcqNonScon(carAcqNonScon.copy());
    }

    @Override
    public AfDecimal getCarAcqNonSconObj() {
        if (ws.getIndBilaTrchEstr().getCarAcqNonScon() >= 0) {
            return getCarAcqNonScon();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCarAcqNonSconObj(AfDecimal carAcqNonSconObj) {
        if (carAcqNonSconObj != null) {
            setCarAcqNonScon(new AfDecimal(carAcqNonSconObj, 15, 3));
            ws.getIndBilaTrchEstr().setCarAcqNonScon(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setCarAcqNonScon(((short)-1));
        }
    }

    @Override
    public AfDecimal getCarAcqPrecontato() {
        return bilaTrchEstr.getB03CarAcqPrecontato().getB03CarAcqPrecontato();
    }

    @Override
    public void setCarAcqPrecontato(AfDecimal carAcqPrecontato) {
        this.bilaTrchEstr.getB03CarAcqPrecontato().setB03CarAcqPrecontato(carAcqPrecontato.copy());
    }

    @Override
    public AfDecimal getCarAcqPrecontatoObj() {
        if (ws.getIndBilaTrchEstr().getCarAcqPrecontato() >= 0) {
            return getCarAcqPrecontato();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCarAcqPrecontatoObj(AfDecimal carAcqPrecontatoObj) {
        if (carAcqPrecontatoObj != null) {
            setCarAcqPrecontato(new AfDecimal(carAcqPrecontatoObj, 15, 3));
            ws.getIndBilaTrchEstr().setCarAcqPrecontato(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setCarAcqPrecontato(((short)-1));
        }
    }

    @Override
    public AfDecimal getCarGest() {
        return bilaTrchEstr.getB03CarGest().getB03CarGest();
    }

    @Override
    public void setCarGest(AfDecimal carGest) {
        this.bilaTrchEstr.getB03CarGest().setB03CarGest(carGest.copy());
    }

    @Override
    public AfDecimal getCarGestNonScon() {
        return bilaTrchEstr.getB03CarGestNonScon().getB03CarGestNonScon();
    }

    @Override
    public void setCarGestNonScon(AfDecimal carGestNonScon) {
        this.bilaTrchEstr.getB03CarGestNonScon().setB03CarGestNonScon(carGestNonScon.copy());
    }

    @Override
    public AfDecimal getCarGestNonSconObj() {
        if (ws.getIndBilaTrchEstr().getCarGestNonScon() >= 0) {
            return getCarGestNonScon();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCarGestNonSconObj(AfDecimal carGestNonSconObj) {
        if (carGestNonSconObj != null) {
            setCarGestNonScon(new AfDecimal(carGestNonSconObj, 15, 3));
            ws.getIndBilaTrchEstr().setCarGestNonScon(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setCarGestNonScon(((short)-1));
        }
    }

    @Override
    public AfDecimal getCarGestObj() {
        if (ws.getIndBilaTrchEstr().getCarGest() >= 0) {
            return getCarGest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCarGestObj(AfDecimal carGestObj) {
        if (carGestObj != null) {
            setCarGest(new AfDecimal(carGestObj, 15, 3));
            ws.getIndBilaTrchEstr().setCarGest(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setCarGest(((short)-1));
        }
    }

    @Override
    public AfDecimal getCarInc() {
        return bilaTrchEstr.getB03CarInc().getB03CarInc();
    }

    @Override
    public void setCarInc(AfDecimal carInc) {
        this.bilaTrchEstr.getB03CarInc().setB03CarInc(carInc.copy());
    }

    @Override
    public AfDecimal getCarIncNonScon() {
        return bilaTrchEstr.getB03CarIncNonScon().getB03CarIncNonScon();
    }

    @Override
    public void setCarIncNonScon(AfDecimal carIncNonScon) {
        this.bilaTrchEstr.getB03CarIncNonScon().setB03CarIncNonScon(carIncNonScon.copy());
    }

    @Override
    public AfDecimal getCarIncNonSconObj() {
        if (ws.getIndBilaTrchEstr().getCarIncNonScon() >= 0) {
            return getCarIncNonScon();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCarIncNonSconObj(AfDecimal carIncNonSconObj) {
        if (carIncNonSconObj != null) {
            setCarIncNonScon(new AfDecimal(carIncNonSconObj, 15, 3));
            ws.getIndBilaTrchEstr().setCarIncNonScon(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setCarIncNonScon(((short)-1));
        }
    }

    @Override
    public AfDecimal getCarIncObj() {
        if (ws.getIndBilaTrchEstr().getCarInc() >= 0) {
            return getCarInc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCarIncObj(AfDecimal carIncObj) {
        if (carIncObj != null) {
            setCarInc(new AfDecimal(carIncObj, 15, 3));
            ws.getIndBilaTrchEstr().setCarInc(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setCarInc(((short)-1));
        }
    }

    @Override
    public int getCarz() {
        return bilaTrchEstr.getB03Carz().getB03Carz();
    }

    @Override
    public void setCarz(int carz) {
        this.bilaTrchEstr.getB03Carz().setB03Carz(carz);
    }

    @Override
    public Integer getCarzObj() {
        if (ws.getIndBilaTrchEstr().getCarz() >= 0) {
            return ((Integer)getCarz());
        }
        else {
            return null;
        }
    }

    @Override
    public void setCarzObj(Integer carzObj) {
        if (carzObj != null) {
            setCarz(((int)carzObj));
            ws.getIndBilaTrchEstr().setCarz(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setCarz(((short)-1));
        }
    }

    @Override
    public String getCausSconVchar() {
        return bilaTrchEstr.getB03CausSconVcharFormatted();
    }

    @Override
    public void setCausSconVchar(String causSconVchar) {
        this.bilaTrchEstr.setB03CausSconVcharFormatted(causSconVchar);
    }

    @Override
    public String getCausSconVcharObj() {
        if (ws.getIndBilaTrchEstr().getCausScon() >= 0) {
            return getCausSconVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCausSconVcharObj(String causSconVcharObj) {
        if (causSconVcharObj != null) {
            setCausSconVchar(causSconVcharObj);
            ws.getIndBilaTrchEstr().setCausScon(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setCausScon(((short)-1));
        }
    }

    @Override
    public int getCodAge() {
        return bilaTrchEstr.getB03CodAge().getB03CodAge();
    }

    @Override
    public void setCodAge(int codAge) {
        this.bilaTrchEstr.getB03CodAge().setB03CodAge(codAge);
    }

    @Override
    public Integer getCodAgeObj() {
        if (ws.getIndBilaTrchEstr().getCodAge() >= 0) {
            return ((Integer)getCodAge());
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodAgeObj(Integer codAgeObj) {
        if (codAgeObj != null) {
            setCodAge(((int)codAgeObj));
            ws.getIndBilaTrchEstr().setCodAge(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setCodAge(((short)-1));
        }
    }

    @Override
    public int getCodCan() {
        return bilaTrchEstr.getB03CodCan().getB03CodCan();
    }

    @Override
    public void setCodCan(int codCan) {
        this.bilaTrchEstr.getB03CodCan().setB03CodCan(codCan);
    }

    @Override
    public Integer getCodCanObj() {
        if (ws.getIndBilaTrchEstr().getCodCan() >= 0) {
            return ((Integer)getCodCan());
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodCanObj(Integer codCanObj) {
        if (codCanObj != null) {
            setCodCan(((int)codCanObj));
            ws.getIndBilaTrchEstr().setCodCan(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setCodCan(((short)-1));
        }
    }

    @Override
    public int getCodCompAnia() {
        return bilaTrchEstr.getB03CodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.bilaTrchEstr.setB03CodCompAnia(codCompAnia);
    }

    @Override
    public String getCodConv() {
        return bilaTrchEstr.getB03CodConv();
    }

    @Override
    public void setCodConv(String codConv) {
        this.bilaTrchEstr.setB03CodConv(codConv);
    }

    @Override
    public String getCodConvObj() {
        if (ws.getIndBilaTrchEstr().getCodConv() >= 0) {
            return getCodConv();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodConvObj(String codConvObj) {
        if (codConvObj != null) {
            setCodConv(codConvObj);
            ws.getIndBilaTrchEstr().setCodConv(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setCodConv(((short)-1));
        }
    }

    @Override
    public String getCodDiv() {
        return bilaTrchEstr.getB03CodDiv();
    }

    @Override
    public void setCodDiv(String codDiv) {
        this.bilaTrchEstr.setB03CodDiv(codDiv);
    }

    @Override
    public String getCodDivObj() {
        if (ws.getIndBilaTrchEstr().getCodDiv() >= 0) {
            return getCodDiv();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodDivObj(String codDivObj) {
        if (codDivObj != null) {
            setCodDiv(codDivObj);
            ws.getIndBilaTrchEstr().setCodDiv(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setCodDiv(((short)-1));
        }
    }

    @Override
    public String getCodFiscAssto1() {
        return bilaTrchEstr.getB03CodFiscAssto1();
    }

    @Override
    public void setCodFiscAssto1(String codFiscAssto1) {
        this.bilaTrchEstr.setB03CodFiscAssto1(codFiscAssto1);
    }

    @Override
    public String getCodFiscAssto1Obj() {
        if (ws.getIndBilaTrchEstr().getCodFiscAssto1() >= 0) {
            return getCodFiscAssto1();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodFiscAssto1Obj(String codFiscAssto1Obj) {
        if (codFiscAssto1Obj != null) {
            setCodFiscAssto1(codFiscAssto1Obj);
            ws.getIndBilaTrchEstr().setCodFiscAssto1(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setCodFiscAssto1(((short)-1));
        }
    }

    @Override
    public String getCodFiscAssto2() {
        return bilaTrchEstr.getB03CodFiscAssto2();
    }

    @Override
    public void setCodFiscAssto2(String codFiscAssto2) {
        this.bilaTrchEstr.setB03CodFiscAssto2(codFiscAssto2);
    }

    @Override
    public String getCodFiscAssto2Obj() {
        if (ws.getIndBilaTrchEstr().getCodFiscAssto2() >= 0) {
            return getCodFiscAssto2();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodFiscAssto2Obj(String codFiscAssto2Obj) {
        if (codFiscAssto2Obj != null) {
            setCodFiscAssto2(codFiscAssto2Obj);
            ws.getIndBilaTrchEstr().setCodFiscAssto2(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setCodFiscAssto2(((short)-1));
        }
    }

    @Override
    public String getCodFiscAssto3() {
        return bilaTrchEstr.getB03CodFiscAssto3();
    }

    @Override
    public void setCodFiscAssto3(String codFiscAssto3) {
        this.bilaTrchEstr.setB03CodFiscAssto3(codFiscAssto3);
    }

    @Override
    public String getCodFiscAssto3Obj() {
        if (ws.getIndBilaTrchEstr().getCodFiscAssto3() >= 0) {
            return getCodFiscAssto3();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodFiscAssto3Obj(String codFiscAssto3Obj) {
        if (codFiscAssto3Obj != null) {
            setCodFiscAssto3(codFiscAssto3Obj);
            ws.getIndBilaTrchEstr().setCodFiscAssto3(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setCodFiscAssto3(((short)-1));
        }
    }

    @Override
    public String getCodFiscCntr() {
        return bilaTrchEstr.getB03CodFiscCntr();
    }

    @Override
    public void setCodFiscCntr(String codFiscCntr) {
        this.bilaTrchEstr.setB03CodFiscCntr(codFiscCntr);
    }

    @Override
    public String getCodFiscCntrObj() {
        if (ws.getIndBilaTrchEstr().getCodFiscCntr() >= 0) {
            return getCodFiscCntr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodFiscCntrObj(String codFiscCntrObj) {
        if (codFiscCntrObj != null) {
            setCodFiscCntr(codFiscCntrObj);
            ws.getIndBilaTrchEstr().setCodFiscCntr(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setCodFiscCntr(((short)-1));
        }
    }

    @Override
    public String getCodFnd() {
        return bilaTrchEstr.getB03CodFnd();
    }

    @Override
    public void setCodFnd(String codFnd) {
        this.bilaTrchEstr.setB03CodFnd(codFnd);
    }

    @Override
    public String getCodFndObj() {
        if (ws.getIndBilaTrchEstr().getCodFnd() >= 0) {
            return getCodFnd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodFndObj(String codFndObj) {
        if (codFndObj != null) {
            setCodFnd(codFndObj);
            ws.getIndBilaTrchEstr().setCodFnd(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setCodFnd(((short)-1));
        }
    }

    @Override
    public int getCodPrdt() {
        return bilaTrchEstr.getB03CodPrdt().getB03CodPrdt();
    }

    @Override
    public void setCodPrdt(int codPrdt) {
        this.bilaTrchEstr.getB03CodPrdt().setB03CodPrdt(codPrdt);
    }

    @Override
    public Integer getCodPrdtObj() {
        if (ws.getIndBilaTrchEstr().getCodPrdt() >= 0) {
            return ((Integer)getCodPrdt());
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodPrdtObj(Integer codPrdtObj) {
        if (codPrdtObj != null) {
            setCodPrdt(((int)codPrdtObj));
            ws.getIndBilaTrchEstr().setCodPrdt(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setCodPrdt(((short)-1));
        }
    }

    @Override
    public String getCodProd() {
        return bilaTrchEstr.getB03CodProd();
    }

    @Override
    public void setCodProd(String codProd) {
        this.bilaTrchEstr.setB03CodProd(codProd);
    }

    @Override
    public String getCodProdObj() {
        if (ws.getIndBilaTrchEstr().getCodProd() >= 0) {
            return getCodProd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodProdObj(String codProdObj) {
        if (codProdObj != null) {
            setCodProd(codProdObj);
            ws.getIndBilaTrchEstr().setCodProd(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setCodProd(((short)-1));
        }
    }

    @Override
    public String getCodRamo() {
        return bilaTrchEstr.getB03CodRamo();
    }

    @Override
    public void setCodRamo(String codRamo) {
        this.bilaTrchEstr.setB03CodRamo(codRamo);
    }

    @Override
    public int getCodSubage() {
        return bilaTrchEstr.getB03CodSubage().getB03CodSubage();
    }

    @Override
    public void setCodSubage(int codSubage) {
        this.bilaTrchEstr.getB03CodSubage().setB03CodSubage(codSubage);
    }

    @Override
    public Integer getCodSubageObj() {
        if (ws.getIndBilaTrchEstr().getCodSubage() >= 0) {
            return ((Integer)getCodSubage());
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodSubageObj(Integer codSubageObj) {
        if (codSubageObj != null) {
            setCodSubage(((int)codSubageObj));
            ws.getIndBilaTrchEstr().setCodSubage(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setCodSubage(((short)-1));
        }
    }

    @Override
    public String getCodTari() {
        return bilaTrchEstr.getB03CodTari();
    }

    @Override
    public void setCodTari(String codTari) {
        this.bilaTrchEstr.setB03CodTari(codTari);
    }

    @Override
    public String getCodTariOrgn() {
        return bilaTrchEstr.getB03CodTariOrgn();
    }

    @Override
    public void setCodTariOrgn(String codTariOrgn) {
        this.bilaTrchEstr.setB03CodTariOrgn(codTariOrgn);
    }

    @Override
    public String getCodTariOrgnObj() {
        if (ws.getIndBilaTrchEstr().getCodTariOrgn() >= 0) {
            return getCodTariOrgn();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodTariOrgnObj(String codTariOrgnObj) {
        if (codTariOrgnObj != null) {
            setCodTariOrgn(codTariOrgnObj);
            ws.getIndBilaTrchEstr().setCodTariOrgn(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setCodTariOrgn(((short)-1));
        }
    }

    @Override
    public AfDecimal getCoeffOpzCpt() {
        return bilaTrchEstr.getB03CoeffOpzCpt().getB03CoeffOpzCpt();
    }

    @Override
    public void setCoeffOpzCpt(AfDecimal coeffOpzCpt) {
        this.bilaTrchEstr.getB03CoeffOpzCpt().setB03CoeffOpzCpt(coeffOpzCpt.copy());
    }

    @Override
    public AfDecimal getCoeffOpzCptObj() {
        if (ws.getIndBilaTrchEstr().getCoeffOpzCpt() >= 0) {
            return getCoeffOpzCpt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCoeffOpzCptObj(AfDecimal coeffOpzCptObj) {
        if (coeffOpzCptObj != null) {
            setCoeffOpzCpt(new AfDecimal(coeffOpzCptObj, 6, 3));
            ws.getIndBilaTrchEstr().setCoeffOpzCpt(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setCoeffOpzCpt(((short)-1));
        }
    }

    @Override
    public AfDecimal getCoeffOpzRen() {
        return bilaTrchEstr.getB03CoeffOpzRen().getB03CoeffOpzRen();
    }

    @Override
    public void setCoeffOpzRen(AfDecimal coeffOpzRen) {
        this.bilaTrchEstr.getB03CoeffOpzRen().setB03CoeffOpzRen(coeffOpzRen.copy());
    }

    @Override
    public AfDecimal getCoeffOpzRenObj() {
        if (ws.getIndBilaTrchEstr().getCoeffOpzRen() >= 0) {
            return getCoeffOpzRen();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCoeffOpzRenObj(AfDecimal coeffOpzRenObj) {
        if (coeffOpzRenObj != null) {
            setCoeffOpzRen(new AfDecimal(coeffOpzRenObj, 6, 3));
            ws.getIndBilaTrchEstr().setCoeffOpzRen(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setCoeffOpzRen(((short)-1));
        }
    }

    @Override
    public AfDecimal getCoeffRis1T() {
        return bilaTrchEstr.getB03CoeffRis1T().getB03CoeffRis1T();
    }

    @Override
    public void setCoeffRis1T(AfDecimal coeffRis1T) {
        this.bilaTrchEstr.getB03CoeffRis1T().setB03CoeffRis1T(coeffRis1T.copy());
    }

    @Override
    public AfDecimal getCoeffRis1TObj() {
        if (ws.getIndBilaTrchEstr().getCoeffRis1T() >= 0) {
            return getCoeffRis1T();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCoeffRis1TObj(AfDecimal coeffRis1TObj) {
        if (coeffRis1TObj != null) {
            setCoeffRis1T(new AfDecimal(coeffRis1TObj, 14, 9));
            ws.getIndBilaTrchEstr().setCoeffRis1T(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setCoeffRis1T(((short)-1));
        }
    }

    @Override
    public AfDecimal getCoeffRis2T() {
        return bilaTrchEstr.getB03CoeffRis2T().getB03CoeffRis2T();
    }

    @Override
    public void setCoeffRis2T(AfDecimal coeffRis2T) {
        this.bilaTrchEstr.getB03CoeffRis2T().setB03CoeffRis2T(coeffRis2T.copy());
    }

    @Override
    public AfDecimal getCoeffRis2TObj() {
        if (ws.getIndBilaTrchEstr().getCoeffRis2T() >= 0) {
            return getCoeffRis2T();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCoeffRis2TObj(AfDecimal coeffRis2TObj) {
        if (coeffRis2TObj != null) {
            setCoeffRis2T(new AfDecimal(coeffRis2TObj, 14, 9));
            ws.getIndBilaTrchEstr().setCoeffRis2T(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setCoeffRis2T(((short)-1));
        }
    }

    @Override
    public AfDecimal getCommisInter() {
        return bilaTrchEstr.getB03CommisInter().getB03CommisInter();
    }

    @Override
    public void setCommisInter(AfDecimal commisInter) {
        this.bilaTrchEstr.getB03CommisInter().setB03CommisInter(commisInter.copy());
    }

    @Override
    public AfDecimal getCommisInterObj() {
        if (ws.getIndBilaTrchEstr().getCommisInter() >= 0) {
            return getCommisInter();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCommisInterObj(AfDecimal commisInterObj) {
        if (commisInterObj != null) {
            setCommisInter(new AfDecimal(commisInterObj, 15, 3));
            ws.getIndBilaTrchEstr().setCommisInter(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setCommisInter(((short)-1));
        }
    }

    @Override
    public AfDecimal getCptAsstoIniMor() {
        return bilaTrchEstr.getB03CptAsstoIniMor().getB03CptAsstoIniMor();
    }

    @Override
    public void setCptAsstoIniMor(AfDecimal cptAsstoIniMor) {
        this.bilaTrchEstr.getB03CptAsstoIniMor().setB03CptAsstoIniMor(cptAsstoIniMor.copy());
    }

    @Override
    public AfDecimal getCptAsstoIniMorObj() {
        if (ws.getIndBilaTrchEstr().getCptAsstoIniMor() >= 0) {
            return getCptAsstoIniMor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCptAsstoIniMorObj(AfDecimal cptAsstoIniMorObj) {
        if (cptAsstoIniMorObj != null) {
            setCptAsstoIniMor(new AfDecimal(cptAsstoIniMorObj, 15, 3));
            ws.getIndBilaTrchEstr().setCptAsstoIniMor(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setCptAsstoIniMor(((short)-1));
        }
    }

    @Override
    public AfDecimal getCptDtRidz() {
        return bilaTrchEstr.getB03CptDtRidz().getB03CptDtRidz();
    }

    @Override
    public void setCptDtRidz(AfDecimal cptDtRidz) {
        this.bilaTrchEstr.getB03CptDtRidz().setB03CptDtRidz(cptDtRidz.copy());
    }

    @Override
    public AfDecimal getCptDtRidzObj() {
        if (ws.getIndBilaTrchEstr().getCptDtRidz() >= 0) {
            return getCptDtRidz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCptDtRidzObj(AfDecimal cptDtRidzObj) {
        if (cptDtRidzObj != null) {
            setCptDtRidz(new AfDecimal(cptDtRidzObj, 15, 3));
            ws.getIndBilaTrchEstr().setCptDtRidz(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setCptDtRidz(((short)-1));
        }
    }

    @Override
    public AfDecimal getCptDtStab() {
        return bilaTrchEstr.getB03CptDtStab().getB03CptDtStab();
    }

    @Override
    public void setCptDtStab(AfDecimal cptDtStab) {
        this.bilaTrchEstr.getB03CptDtStab().setB03CptDtStab(cptDtStab.copy());
    }

    @Override
    public AfDecimal getCptDtStabObj() {
        if (ws.getIndBilaTrchEstr().getCptDtStab() >= 0) {
            return getCptDtStab();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCptDtStabObj(AfDecimal cptDtStabObj) {
        if (cptDtStabObj != null) {
            setCptDtStab(new AfDecimal(cptDtStabObj, 15, 3));
            ws.getIndBilaTrchEstr().setCptDtStab(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setCptDtStab(((short)-1));
        }
    }

    @Override
    public AfDecimal getCptRiasto() {
        return bilaTrchEstr.getB03CptRiasto().getB03CptRiasto();
    }

    @Override
    public void setCptRiasto(AfDecimal cptRiasto) {
        this.bilaTrchEstr.getB03CptRiasto().setB03CptRiasto(cptRiasto.copy());
    }

    @Override
    public AfDecimal getCptRiastoEcc() {
        return bilaTrchEstr.getB03CptRiastoEcc().getB03CptRiastoEcc();
    }

    @Override
    public void setCptRiastoEcc(AfDecimal cptRiastoEcc) {
        this.bilaTrchEstr.getB03CptRiastoEcc().setB03CptRiastoEcc(cptRiastoEcc.copy());
    }

    @Override
    public AfDecimal getCptRiastoEccObj() {
        if (ws.getIndBilaTrchEstr().getCptRiastoEcc() >= 0) {
            return getCptRiastoEcc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCptRiastoEccObj(AfDecimal cptRiastoEccObj) {
        if (cptRiastoEccObj != null) {
            setCptRiastoEcc(new AfDecimal(cptRiastoEccObj, 15, 3));
            ws.getIndBilaTrchEstr().setCptRiastoEcc(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setCptRiastoEcc(((short)-1));
        }
    }

    @Override
    public AfDecimal getCptRiastoObj() {
        if (ws.getIndBilaTrchEstr().getCptRiasto() >= 0) {
            return getCptRiasto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCptRiastoObj(AfDecimal cptRiastoObj) {
        if (cptRiastoObj != null) {
            setCptRiasto(new AfDecimal(cptRiastoObj, 15, 3));
            ws.getIndBilaTrchEstr().setCptRiasto(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setCptRiasto(((short)-1));
        }
    }

    @Override
    public AfDecimal getCptRshMor() {
        return bilaTrchEstr.getB03CptRshMor().getB03CptRshMor();
    }

    @Override
    public void setCptRshMor(AfDecimal cptRshMor) {
        this.bilaTrchEstr.getB03CptRshMor().setB03CptRshMor(cptRshMor.copy());
    }

    @Override
    public AfDecimal getCptRshMorObj() {
        if (ws.getIndBilaTrchEstr().getCptRshMor() >= 0) {
            return getCptRshMor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCptRshMorObj(AfDecimal cptRshMorObj) {
        if (cptRshMorObj != null) {
            setCptRshMor(new AfDecimal(cptRshMorObj, 15, 3));
            ws.getIndBilaTrchEstr().setCptRshMor(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setCptRshMor(((short)-1));
        }
    }

    @Override
    public AfDecimal getCumRiscpar() {
        return bilaTrchEstr.getB03CumRiscpar().getB03CumRiscpar();
    }

    @Override
    public void setCumRiscpar(AfDecimal cumRiscpar) {
        this.bilaTrchEstr.getB03CumRiscpar().setB03CumRiscpar(cumRiscpar.copy());
    }

    @Override
    public AfDecimal getCumRiscparObj() {
        if (ws.getIndBilaTrchEstr().getCumRiscpar() >= 0) {
            return getCumRiscpar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCumRiscparObj(AfDecimal cumRiscparObj) {
        if (cumRiscparObj != null) {
            setCumRiscpar(new AfDecimal(cumRiscparObj, 15, 3));
            ws.getIndBilaTrchEstr().setCumRiscpar(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setCumRiscpar(((short)-1));
        }
    }

    @Override
    public AfDecimal getDir() {
        return bilaTrchEstr.getB03Dir().getB03Dir();
    }

    @Override
    public void setDir(AfDecimal dir) {
        this.bilaTrchEstr.getB03Dir().setB03Dir(dir.copy());
    }

    @Override
    public AfDecimal getDirEmis() {
        return bilaTrchEstr.getB03DirEmis().getB03DirEmis();
    }

    @Override
    public void setDirEmis(AfDecimal dirEmis) {
        this.bilaTrchEstr.getB03DirEmis().setB03DirEmis(dirEmis.copy());
    }

    @Override
    public AfDecimal getDirEmisObj() {
        if (ws.getIndBilaTrchEstr().getDirEmis() >= 0) {
            return getDirEmis();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDirEmisObj(AfDecimal dirEmisObj) {
        if (dirEmisObj != null) {
            setDirEmis(new AfDecimal(dirEmisObj, 15, 3));
            ws.getIndBilaTrchEstr().setDirEmis(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setDirEmis(((short)-1));
        }
    }

    @Override
    public AfDecimal getDirObj() {
        if (ws.getIndBilaTrchEstr().getDir() >= 0) {
            return getDir();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDirObj(AfDecimal dirObj) {
        if (dirObj != null) {
            setDir(new AfDecimal(dirObj, 15, 3));
            ws.getIndBilaTrchEstr().setDir(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setDir(((short)-1));
        }
    }

    @Override
    public char getDsOperSql() {
        return bilaTrchEstr.getB03DsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.bilaTrchEstr.setB03DsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return bilaTrchEstr.getB03DsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.bilaTrchEstr.setB03DsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsCptz() {
        return bilaTrchEstr.getB03DsTsCptz();
    }

    @Override
    public void setDsTsCptz(long dsTsCptz) {
        this.bilaTrchEstr.setB03DsTsCptz(dsTsCptz);
    }

    @Override
    public String getDsUtente() {
        return bilaTrchEstr.getB03DsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.bilaTrchEstr.setB03DsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return bilaTrchEstr.getB03DsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.bilaTrchEstr.setB03DsVer(dsVer);
    }

    @Override
    public String getDtDecorAdesDb() {
        return ws.getBilaTrchEstrDb().getDecorAdesDb();
    }

    @Override
    public void setDtDecorAdesDb(String dtDecorAdesDb) {
        this.ws.getBilaTrchEstrDb().setDecorAdesDb(dtDecorAdesDb);
    }

    @Override
    public String getDtDecorAdesDbObj() {
        if (ws.getIndBilaTrchEstr().getDtDecorAdes() >= 0) {
            return getDtDecorAdesDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtDecorAdesDbObj(String dtDecorAdesDbObj) {
        if (dtDecorAdesDbObj != null) {
            setDtDecorAdesDb(dtDecorAdesDbObj);
            ws.getIndBilaTrchEstr().setDtDecorAdes(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setDtDecorAdes(((short)-1));
        }
    }

    @Override
    public String getDtDecorPoliDb() {
        return ws.getBilaTrchEstrDb().getDecorPoliDb();
    }

    @Override
    public void setDtDecorPoliDb(String dtDecorPoliDb) {
        this.ws.getBilaTrchEstrDb().setDecorPoliDb(dtDecorPoliDb);
    }

    @Override
    public String getDtDecorTrchDb() {
        return ws.getBilaTrchEstrDb().getDecorTrchDb();
    }

    @Override
    public void setDtDecorTrchDb(String dtDecorTrchDb) {
        this.ws.getBilaTrchEstrDb().setDecorTrchDb(dtDecorTrchDb);
    }

    @Override
    public String getDtEffCambStatDb() {
        return ws.getBilaTrchEstrDb().getEffCambStatDb();
    }

    @Override
    public void setDtEffCambStatDb(String dtEffCambStatDb) {
        this.ws.getBilaTrchEstrDb().setEffCambStatDb(dtEffCambStatDb);
    }

    @Override
    public String getDtEffCambStatDbObj() {
        if (ws.getIndBilaTrchEstr().getDtEffCambStat() >= 0) {
            return getDtEffCambStatDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtEffCambStatDbObj(String dtEffCambStatDbObj) {
        if (dtEffCambStatDbObj != null) {
            setDtEffCambStatDb(dtEffCambStatDbObj);
            ws.getIndBilaTrchEstr().setDtEffCambStat(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setDtEffCambStat(((short)-1));
        }
    }

    @Override
    public String getDtEffRidzDb() {
        return ws.getBilaTrchEstrDb().getEffRidzDb();
    }

    @Override
    public void setDtEffRidzDb(String dtEffRidzDb) {
        this.ws.getBilaTrchEstrDb().setEffRidzDb(dtEffRidzDb);
    }

    @Override
    public String getDtEffRidzDbObj() {
        if (ws.getIndBilaTrchEstr().getDtEffRidz() >= 0) {
            return getDtEffRidzDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtEffRidzDbObj(String dtEffRidzDbObj) {
        if (dtEffRidzDbObj != null) {
            setDtEffRidzDb(dtEffRidzDbObj);
            ws.getIndBilaTrchEstr().setDtEffRidz(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setDtEffRidz(((short)-1));
        }
    }

    @Override
    public String getDtEffStabDb() {
        return ws.getBilaTrchEstrDb().getEffStabDb();
    }

    @Override
    public void setDtEffStabDb(String dtEffStabDb) {
        this.ws.getBilaTrchEstrDb().setEffStabDb(dtEffStabDb);
    }

    @Override
    public String getDtEffStabDbObj() {
        if (ws.getIndBilaTrchEstr().getDtEffStab() >= 0) {
            return getDtEffStabDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtEffStabDbObj(String dtEffStabDbObj) {
        if (dtEffStabDbObj != null) {
            setDtEffStabDb(dtEffStabDbObj);
            ws.getIndBilaTrchEstr().setDtEffStab(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setDtEffStab(((short)-1));
        }
    }

    @Override
    public String getDtEmisCambStatDb() {
        return ws.getBilaTrchEstrDb().getEmisCambStatDb();
    }

    @Override
    public void setDtEmisCambStatDb(String dtEmisCambStatDb) {
        this.ws.getBilaTrchEstrDb().setEmisCambStatDb(dtEmisCambStatDb);
    }

    @Override
    public String getDtEmisCambStatDbObj() {
        if (ws.getIndBilaTrchEstr().getDtEmisCambStat() >= 0) {
            return getDtEmisCambStatDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtEmisCambStatDbObj(String dtEmisCambStatDbObj) {
        if (dtEmisCambStatDbObj != null) {
            setDtEmisCambStatDb(dtEmisCambStatDbObj);
            ws.getIndBilaTrchEstr().setDtEmisCambStat(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setDtEmisCambStat(((short)-1));
        }
    }

    @Override
    public String getDtEmisPoliDb() {
        return ws.getBilaTrchEstrDb().getEmisPoliDb();
    }

    @Override
    public void setDtEmisPoliDb(String dtEmisPoliDb) {
        this.ws.getBilaTrchEstrDb().setEmisPoliDb(dtEmisPoliDb);
    }

    @Override
    public String getDtEmisRidzDb() {
        return ws.getBilaTrchEstrDb().getEmisRidzDb();
    }

    @Override
    public void setDtEmisRidzDb(String dtEmisRidzDb) {
        this.ws.getBilaTrchEstrDb().setEmisRidzDb(dtEmisRidzDb);
    }

    @Override
    public String getDtEmisRidzDbObj() {
        if (ws.getIndBilaTrchEstr().getDtEmisRidz() >= 0) {
            return getDtEmisRidzDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtEmisRidzDbObj(String dtEmisRidzDbObj) {
        if (dtEmisRidzDbObj != null) {
            setDtEmisRidzDb(dtEmisRidzDbObj);
            ws.getIndBilaTrchEstr().setDtEmisRidz(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setDtEmisRidz(((short)-1));
        }
    }

    @Override
    public String getDtEmisTrchDb() {
        return ws.getBilaTrchEstrDb().getEmisTrchDb();
    }

    @Override
    public void setDtEmisTrchDb(String dtEmisTrchDb) {
        this.ws.getBilaTrchEstrDb().setEmisTrchDb(dtEmisTrchDb);
    }

    @Override
    public String getDtEmisTrchDbObj() {
        if (ws.getIndBilaTrchEstr().getDtEmisTrch() >= 0) {
            return getDtEmisTrchDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtEmisTrchDbObj(String dtEmisTrchDbObj) {
        if (dtEmisTrchDbObj != null) {
            setDtEmisTrchDb(dtEmisTrchDbObj);
            ws.getIndBilaTrchEstr().setDtEmisTrch(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setDtEmisTrch(((short)-1));
        }
    }

    @Override
    public String getDtIncUltPreDb() {
        return ws.getBilaTrchEstrDb().getIncUltPreDb();
    }

    @Override
    public void setDtIncUltPreDb(String dtIncUltPreDb) {
        this.ws.getBilaTrchEstrDb().setIncUltPreDb(dtIncUltPreDb);
    }

    @Override
    public String getDtIncUltPreDbObj() {
        if (ws.getIndBilaTrchEstr().getDtIncUltPre() >= 0) {
            return getDtIncUltPreDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtIncUltPreDbObj(String dtIncUltPreDbObj) {
        if (dtIncUltPreDbObj != null) {
            setDtIncUltPreDb(dtIncUltPreDbObj);
            ws.getIndBilaTrchEstr().setDtIncUltPre(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setDtIncUltPre(((short)-1));
        }
    }

    @Override
    public String getDtIniValTarDb() {
        return ws.getBilaTrchEstrDb().getIniValTarDb();
    }

    @Override
    public void setDtIniValTarDb(String dtIniValTarDb) {
        this.ws.getBilaTrchEstrDb().setIniValTarDb(dtIniValTarDb);
    }

    @Override
    public String getDtIniValTarDbObj() {
        if (ws.getIndBilaTrchEstr().getDtIniValTar() >= 0) {
            return getDtIniValTarDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtIniValTarDbObj(String dtIniValTarDbObj) {
        if (dtIniValTarDbObj != null) {
            setDtIniValTarDb(dtIniValTarDbObj);
            ws.getIndBilaTrchEstr().setDtIniValTar(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setDtIniValTar(((short)-1));
        }
    }

    @Override
    public String getDtIniVldtProdDb() {
        return ws.getBilaTrchEstrDb().getIniVldtProdDb();
    }

    @Override
    public void setDtIniVldtProdDb(String dtIniVldtProdDb) {
        this.ws.getBilaTrchEstrDb().setIniVldtProdDb(dtIniVldtProdDb);
    }

    @Override
    public String getDtNasc1oAsstoDb() {
        return ws.getBilaTrchEstrDb().getNasc1oAsstoDb();
    }

    @Override
    public void setDtNasc1oAsstoDb(String dtNasc1oAsstoDb) {
        this.ws.getBilaTrchEstrDb().setNasc1oAsstoDb(dtNasc1oAsstoDb);
    }

    @Override
    public String getDtNasc1oAsstoDbObj() {
        if (ws.getIndBilaTrchEstr().getDtNasc1oAssto() >= 0) {
            return getDtNasc1oAsstoDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtNasc1oAsstoDbObj(String dtNasc1oAsstoDbObj) {
        if (dtNasc1oAsstoDbObj != null) {
            setDtNasc1oAsstoDb(dtNasc1oAsstoDbObj);
            ws.getIndBilaTrchEstr().setDtNasc1oAssto(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setDtNasc1oAssto(((short)-1));
        }
    }

    @Override
    public String getDtProduzioneDb() {
        return ws.getBilaTrchEstrDb().getProduzioneDb();
    }

    @Override
    public void setDtProduzioneDb(String dtProduzioneDb) {
        this.ws.getBilaTrchEstrDb().setProduzioneDb(dtProduzioneDb);
    }

    @Override
    public String getDtQtzEmisDb() {
        return ws.getBilaTrchEstrDb().getQtzEmisDb();
    }

    @Override
    public void setDtQtzEmisDb(String dtQtzEmisDb) {
        this.ws.getBilaTrchEstrDb().setQtzEmisDb(dtQtzEmisDb);
    }

    @Override
    public String getDtQtzEmisDbObj() {
        if (ws.getIndBilaTrchEstr().getDtQtzEmis() >= 0) {
            return getDtQtzEmisDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtQtzEmisDbObj(String dtQtzEmisDbObj) {
        if (dtQtzEmisDbObj != null) {
            setDtQtzEmisDb(dtQtzEmisDbObj);
            ws.getIndBilaTrchEstr().setDtQtzEmis(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setDtQtzEmis(((short)-1));
        }
    }

    @Override
    public String getDtRisDb() {
        return ws.getBilaTrchEstrDb().getRisDb();
    }

    @Override
    public void setDtRisDb(String dtRisDb) {
        this.ws.getBilaTrchEstrDb().setRisDb(dtRisDb);
    }

    @Override
    public String getDtScadIntmdDb() {
        return ws.getBilaTrchEstrDb().getScadIntmdDb();
    }

    @Override
    public void setDtScadIntmdDb(String dtScadIntmdDb) {
        this.ws.getBilaTrchEstrDb().setScadIntmdDb(dtScadIntmdDb);
    }

    @Override
    public String getDtScadIntmdDbObj() {
        if (ws.getIndBilaTrchEstr().getDtScadIntmd() >= 0) {
            return getDtScadIntmdDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtScadIntmdDbObj(String dtScadIntmdDbObj) {
        if (dtScadIntmdDbObj != null) {
            setDtScadIntmdDb(dtScadIntmdDbObj);
            ws.getIndBilaTrchEstr().setDtScadIntmd(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setDtScadIntmd(((short)-1));
        }
    }

    @Override
    public String getDtScadPagPreDb() {
        return ws.getBilaTrchEstrDb().getScadPagPreDb();
    }

    @Override
    public void setDtScadPagPreDb(String dtScadPagPreDb) {
        this.ws.getBilaTrchEstrDb().setScadPagPreDb(dtScadPagPreDb);
    }

    @Override
    public String getDtScadPagPreDbObj() {
        if (ws.getIndBilaTrchEstr().getDtScadPagPre() >= 0) {
            return getDtScadPagPreDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtScadPagPreDbObj(String dtScadPagPreDbObj) {
        if (dtScadPagPreDbObj != null) {
            setDtScadPagPreDb(dtScadPagPreDbObj);
            ws.getIndBilaTrchEstr().setDtScadPagPre(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setDtScadPagPre(((short)-1));
        }
    }

    @Override
    public String getDtScadTrchDb() {
        return ws.getBilaTrchEstrDb().getScadTrchDb();
    }

    @Override
    public void setDtScadTrchDb(String dtScadTrchDb) {
        this.ws.getBilaTrchEstrDb().setScadTrchDb(dtScadTrchDb);
    }

    @Override
    public String getDtScadTrchDbObj() {
        if (ws.getIndBilaTrchEstr().getDtScadTrch() >= 0) {
            return getDtScadTrchDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtScadTrchDbObj(String dtScadTrchDbObj) {
        if (dtScadTrchDbObj != null) {
            setDtScadTrchDb(dtScadTrchDbObj);
            ws.getIndBilaTrchEstr().setDtScadTrch(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setDtScadTrch(((short)-1));
        }
    }

    @Override
    public String getDtUltPrePagDb() {
        return ws.getBilaTrchEstrDb().getUltPrePagDb();
    }

    @Override
    public void setDtUltPrePagDb(String dtUltPrePagDb) {
        this.ws.getBilaTrchEstrDb().setUltPrePagDb(dtUltPrePagDb);
    }

    @Override
    public String getDtUltPrePagDbObj() {
        if (ws.getIndBilaTrchEstr().getDtUltPrePag() >= 0) {
            return getDtUltPrePagDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltPrePagDbObj(String dtUltPrePagDbObj) {
        if (dtUltPrePagDbObj != null) {
            setDtUltPrePagDb(dtUltPrePagDbObj);
            ws.getIndBilaTrchEstr().setDtUltPrePag(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setDtUltPrePag(((short)-1));
        }
    }

    @Override
    public String getDtUltRivalDb() {
        return ws.getBilaTrchEstrDb().getUltRivalDb();
    }

    @Override
    public void setDtUltRivalDb(String dtUltRivalDb) {
        this.ws.getBilaTrchEstrDb().setUltRivalDb(dtUltRivalDb);
    }

    @Override
    public String getDtUltRivalDbObj() {
        if (ws.getIndBilaTrchEstr().getDtUltRival() >= 0) {
            return getDtUltRivalDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltRivalDbObj(String dtUltRivalDbObj) {
        if (dtUltRivalDbObj != null) {
            setDtUltRivalDb(dtUltRivalDbObj);
            ws.getIndBilaTrchEstr().setDtUltRival(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setDtUltRival(((short)-1));
        }
    }

    @Override
    public int getDur1oPerAa() {
        return bilaTrchEstr.getB03Dur1oPerAa().getB03Dur1oPerAa();
    }

    @Override
    public void setDur1oPerAa(int dur1oPerAa) {
        this.bilaTrchEstr.getB03Dur1oPerAa().setB03Dur1oPerAa(dur1oPerAa);
    }

    @Override
    public Integer getDur1oPerAaObj() {
        if (ws.getIndBilaTrchEstr().getDur1oPerAa() >= 0) {
            return ((Integer)getDur1oPerAa());
        }
        else {
            return null;
        }
    }

    @Override
    public void setDur1oPerAaObj(Integer dur1oPerAaObj) {
        if (dur1oPerAaObj != null) {
            setDur1oPerAa(((int)dur1oPerAaObj));
            ws.getIndBilaTrchEstr().setDur1oPerAa(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setDur1oPerAa(((short)-1));
        }
    }

    @Override
    public int getDur1oPerGg() {
        return bilaTrchEstr.getB03Dur1oPerGg().getB03Dur1oPerGg();
    }

    @Override
    public void setDur1oPerGg(int dur1oPerGg) {
        this.bilaTrchEstr.getB03Dur1oPerGg().setB03Dur1oPerGg(dur1oPerGg);
    }

    @Override
    public Integer getDur1oPerGgObj() {
        if (ws.getIndBilaTrchEstr().getDur1oPerGg() >= 0) {
            return ((Integer)getDur1oPerGg());
        }
        else {
            return null;
        }
    }

    @Override
    public void setDur1oPerGgObj(Integer dur1oPerGgObj) {
        if (dur1oPerGgObj != null) {
            setDur1oPerGg(((int)dur1oPerGgObj));
            ws.getIndBilaTrchEstr().setDur1oPerGg(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setDur1oPerGg(((short)-1));
        }
    }

    @Override
    public int getDur1oPerMm() {
        return bilaTrchEstr.getB03Dur1oPerMm().getB03Dur1oPerMm();
    }

    @Override
    public void setDur1oPerMm(int dur1oPerMm) {
        this.bilaTrchEstr.getB03Dur1oPerMm().setB03Dur1oPerMm(dur1oPerMm);
    }

    @Override
    public Integer getDur1oPerMmObj() {
        if (ws.getIndBilaTrchEstr().getDur1oPerMm() >= 0) {
            return ((Integer)getDur1oPerMm());
        }
        else {
            return null;
        }
    }

    @Override
    public void setDur1oPerMmObj(Integer dur1oPerMmObj) {
        if (dur1oPerMmObj != null) {
            setDur1oPerMm(((int)dur1oPerMmObj));
            ws.getIndBilaTrchEstr().setDur1oPerMm(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setDur1oPerMm(((short)-1));
        }
    }

    @Override
    public int getDurAa() {
        return bilaTrchEstr.getB03DurAa().getB03DurAa();
    }

    @Override
    public void setDurAa(int durAa) {
        this.bilaTrchEstr.getB03DurAa().setB03DurAa(durAa);
    }

    @Override
    public Integer getDurAaObj() {
        if (ws.getIndBilaTrchEstr().getDurAa() >= 0) {
            return ((Integer)getDurAa());
        }
        else {
            return null;
        }
    }

    @Override
    public void setDurAaObj(Integer durAaObj) {
        if (durAaObj != null) {
            setDurAa(((int)durAaObj));
            ws.getIndBilaTrchEstr().setDurAa(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setDurAa(((short)-1));
        }
    }

    @Override
    public int getDurGarAa() {
        return bilaTrchEstr.getB03DurGarAa().getB03DurGarAa();
    }

    @Override
    public void setDurGarAa(int durGarAa) {
        this.bilaTrchEstr.getB03DurGarAa().setB03DurGarAa(durGarAa);
    }

    @Override
    public Integer getDurGarAaObj() {
        if (ws.getIndBilaTrchEstr().getDurGarAa() >= 0) {
            return ((Integer)getDurGarAa());
        }
        else {
            return null;
        }
    }

    @Override
    public void setDurGarAaObj(Integer durGarAaObj) {
        if (durGarAaObj != null) {
            setDurGarAa(((int)durGarAaObj));
            ws.getIndBilaTrchEstr().setDurGarAa(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setDurGarAa(((short)-1));
        }
    }

    @Override
    public int getDurGarGg() {
        return bilaTrchEstr.getB03DurGarGg().getB03DurGarGg();
    }

    @Override
    public void setDurGarGg(int durGarGg) {
        this.bilaTrchEstr.getB03DurGarGg().setB03DurGarGg(durGarGg);
    }

    @Override
    public Integer getDurGarGgObj() {
        if (ws.getIndBilaTrchEstr().getDurGarGg() >= 0) {
            return ((Integer)getDurGarGg());
        }
        else {
            return null;
        }
    }

    @Override
    public void setDurGarGgObj(Integer durGarGgObj) {
        if (durGarGgObj != null) {
            setDurGarGg(((int)durGarGgObj));
            ws.getIndBilaTrchEstr().setDurGarGg(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setDurGarGg(((short)-1));
        }
    }

    @Override
    public int getDurGarMm() {
        return bilaTrchEstr.getB03DurGarMm().getB03DurGarMm();
    }

    @Override
    public void setDurGarMm(int durGarMm) {
        this.bilaTrchEstr.getB03DurGarMm().setB03DurGarMm(durGarMm);
    }

    @Override
    public Integer getDurGarMmObj() {
        if (ws.getIndBilaTrchEstr().getDurGarMm() >= 0) {
            return ((Integer)getDurGarMm());
        }
        else {
            return null;
        }
    }

    @Override
    public void setDurGarMmObj(Integer durGarMmObj) {
        if (durGarMmObj != null) {
            setDurGarMm(((int)durGarMmObj));
            ws.getIndBilaTrchEstr().setDurGarMm(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setDurGarMm(((short)-1));
        }
    }

    @Override
    public int getDurGg() {
        return bilaTrchEstr.getB03DurGg().getB03DurGg();
    }

    @Override
    public void setDurGg(int durGg) {
        this.bilaTrchEstr.getB03DurGg().setB03DurGg(durGg);
    }

    @Override
    public Integer getDurGgObj() {
        if (ws.getIndBilaTrchEstr().getDurGg() >= 0) {
            return ((Integer)getDurGg());
        }
        else {
            return null;
        }
    }

    @Override
    public void setDurGgObj(Integer durGgObj) {
        if (durGgObj != null) {
            setDurGg(((int)durGgObj));
            ws.getIndBilaTrchEstr().setDurGg(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setDurGg(((short)-1));
        }
    }

    @Override
    public int getDurMm() {
        return bilaTrchEstr.getB03DurMm().getB03DurMm();
    }

    @Override
    public void setDurMm(int durMm) {
        this.bilaTrchEstr.getB03DurMm().setB03DurMm(durMm);
    }

    @Override
    public Integer getDurMmObj() {
        if (ws.getIndBilaTrchEstr().getDurMm() >= 0) {
            return ((Integer)getDurMm());
        }
        else {
            return null;
        }
    }

    @Override
    public void setDurMmObj(Integer durMmObj) {
        if (durMmObj != null) {
            setDurMm(((int)durMmObj));
            ws.getIndBilaTrchEstr().setDurMm(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setDurMm(((short)-1));
        }
    }

    @Override
    public int getDurPagPre() {
        return bilaTrchEstr.getB03DurPagPre().getB03DurPagPre();
    }

    @Override
    public void setDurPagPre(int durPagPre) {
        this.bilaTrchEstr.getB03DurPagPre().setB03DurPagPre(durPagPre);
    }

    @Override
    public Integer getDurPagPreObj() {
        if (ws.getIndBilaTrchEstr().getDurPagPre() >= 0) {
            return ((Integer)getDurPagPre());
        }
        else {
            return null;
        }
    }

    @Override
    public void setDurPagPreObj(Integer durPagPreObj) {
        if (durPagPreObj != null) {
            setDurPagPre(((int)durPagPreObj));
            ws.getIndBilaTrchEstr().setDurPagPre(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setDurPagPre(((short)-1));
        }
    }

    @Override
    public int getDurPagRen() {
        return bilaTrchEstr.getB03DurPagRen().getB03DurPagRen();
    }

    @Override
    public void setDurPagRen(int durPagRen) {
        this.bilaTrchEstr.getB03DurPagRen().setB03DurPagRen(durPagRen);
    }

    @Override
    public Integer getDurPagRenObj() {
        if (ws.getIndBilaTrchEstr().getDurPagRen() >= 0) {
            return ((Integer)getDurPagRen());
        }
        else {
            return null;
        }
    }

    @Override
    public void setDurPagRenObj(Integer durPagRenObj) {
        if (durPagRenObj != null) {
            setDurPagRen(((int)durPagRenObj));
            ws.getIndBilaTrchEstr().setDurPagRen(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setDurPagRen(((short)-1));
        }
    }

    @Override
    public AfDecimal getDurResDtCalc() {
        return bilaTrchEstr.getB03DurResDtCalc().getB03DurResDtCalc();
    }

    @Override
    public void setDurResDtCalc(AfDecimal durResDtCalc) {
        this.bilaTrchEstr.getB03DurResDtCalc().setB03DurResDtCalc(durResDtCalc.copy());
    }

    @Override
    public AfDecimal getDurResDtCalcObj() {
        if (ws.getIndBilaTrchEstr().getDurResDtCalc() >= 0) {
            return getDurResDtCalc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDurResDtCalcObj(AfDecimal durResDtCalcObj) {
        if (durResDtCalcObj != null) {
            setDurResDtCalc(new AfDecimal(durResDtCalcObj, 11, 7));
            ws.getIndBilaTrchEstr().setDurResDtCalc(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setDurResDtCalc(((short)-1));
        }
    }

    @Override
    public String getEmitTitOpzVchar() {
        return bilaTrchEstr.getB03EmitTitOpzVcharFormatted();
    }

    @Override
    public void setEmitTitOpzVchar(String emitTitOpzVchar) {
        this.bilaTrchEstr.setB03EmitTitOpzVcharFormatted(emitTitOpzVchar);
    }

    @Override
    public String getEmitTitOpzVcharObj() {
        if (ws.getIndBilaTrchEstr().getEmitTitOpz() >= 0) {
            return getEmitTitOpzVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setEmitTitOpzVcharObj(String emitTitOpzVcharObj) {
        if (emitTitOpzVcharObj != null) {
            setEmitTitOpzVchar(emitTitOpzVcharObj);
            ws.getIndBilaTrchEstr().setEmitTitOpz(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setEmitTitOpz(((short)-1));
        }
    }

    @Override
    public int getEtaAa1oAssto() {
        return bilaTrchEstr.getB03EtaAa1oAssto().getB03EtaAa1oAssto();
    }

    @Override
    public void setEtaAa1oAssto(int etaAa1oAssto) {
        this.bilaTrchEstr.getB03EtaAa1oAssto().setB03EtaAa1oAssto(etaAa1oAssto);
    }

    @Override
    public Integer getEtaAa1oAsstoObj() {
        if (ws.getIndBilaTrchEstr().getEtaAa1oAssto() >= 0) {
            return ((Integer)getEtaAa1oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setEtaAa1oAsstoObj(Integer etaAa1oAsstoObj) {
        if (etaAa1oAsstoObj != null) {
            setEtaAa1oAssto(((int)etaAa1oAsstoObj));
            ws.getIndBilaTrchEstr().setEtaAa1oAssto(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setEtaAa1oAssto(((short)-1));
        }
    }

    @Override
    public int getEtaMm1oAssto() {
        return bilaTrchEstr.getB03EtaMm1oAssto().getB03EtaMm1oAssto();
    }

    @Override
    public void setEtaMm1oAssto(int etaMm1oAssto) {
        this.bilaTrchEstr.getB03EtaMm1oAssto().setB03EtaMm1oAssto(etaMm1oAssto);
    }

    @Override
    public Integer getEtaMm1oAsstoObj() {
        if (ws.getIndBilaTrchEstr().getEtaMm1oAssto() >= 0) {
            return ((Integer)getEtaMm1oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setEtaMm1oAsstoObj(Integer etaMm1oAsstoObj) {
        if (etaMm1oAsstoObj != null) {
            setEtaMm1oAssto(((int)etaMm1oAsstoObj));
            ws.getIndBilaTrchEstr().setEtaMm1oAssto(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setEtaMm1oAssto(((short)-1));
        }
    }

    @Override
    public AfDecimal getEtaRaggnDtCalc() {
        return bilaTrchEstr.getB03EtaRaggnDtCalc().getB03EtaRaggnDtCalc();
    }

    @Override
    public void setEtaRaggnDtCalc(AfDecimal etaRaggnDtCalc) {
        this.bilaTrchEstr.getB03EtaRaggnDtCalc().setB03EtaRaggnDtCalc(etaRaggnDtCalc.copy());
    }

    @Override
    public AfDecimal getEtaRaggnDtCalcObj() {
        if (ws.getIndBilaTrchEstr().getEtaRaggnDtCalc() >= 0) {
            return getEtaRaggnDtCalc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setEtaRaggnDtCalcObj(AfDecimal etaRaggnDtCalcObj) {
        if (etaRaggnDtCalcObj != null) {
            setEtaRaggnDtCalc(new AfDecimal(etaRaggnDtCalcObj, 7, 3));
            ws.getIndBilaTrchEstr().setEtaRaggnDtCalc(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setEtaRaggnDtCalc(((short)-1));
        }
    }

    @Override
    public char getFlCarCont() {
        return bilaTrchEstr.getB03FlCarCont();
    }

    @Override
    public void setFlCarCont(char flCarCont) {
        this.bilaTrchEstr.setB03FlCarCont(flCarCont);
    }

    @Override
    public Character getFlCarContObj() {
        if (ws.getIndBilaTrchEstr().getFlCarCont() >= 0) {
            return ((Character)getFlCarCont());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlCarContObj(Character flCarContObj) {
        if (flCarContObj != null) {
            setFlCarCont(((char)flCarContObj));
            ws.getIndBilaTrchEstr().setFlCarCont(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setFlCarCont(((short)-1));
        }
    }

    @Override
    public char getFlDaTrasf() {
        return bilaTrchEstr.getB03FlDaTrasf();
    }

    @Override
    public void setFlDaTrasf(char flDaTrasf) {
        this.bilaTrchEstr.setB03FlDaTrasf(flDaTrasf);
    }

    @Override
    public Character getFlDaTrasfObj() {
        if (ws.getIndBilaTrchEstr().getFlDaTrasf() >= 0) {
            return ((Character)getFlDaTrasf());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlDaTrasfObj(Character flDaTrasfObj) {
        if (flDaTrasfObj != null) {
            setFlDaTrasf(((char)flDaTrasfObj));
            ws.getIndBilaTrchEstr().setFlDaTrasf(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setFlDaTrasf(((short)-1));
        }
    }

    @Override
    public char getFlIas() {
        return bilaTrchEstr.getB03FlIas();
    }

    @Override
    public void setFlIas(char flIas) {
        this.bilaTrchEstr.setB03FlIas(flIas);
    }

    @Override
    public Character getFlIasObj() {
        if (ws.getIndBilaTrchEstr().getFlIas() >= 0) {
            return ((Character)getFlIas());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlIasObj(Character flIasObj) {
        if (flIasObj != null) {
            setFlIas(((char)flIasObj));
            ws.getIndBilaTrchEstr().setFlIas(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setFlIas(((short)-1));
        }
    }

    @Override
    public char getFlPreAgg() {
        return bilaTrchEstr.getB03FlPreAgg();
    }

    @Override
    public void setFlPreAgg(char flPreAgg) {
        this.bilaTrchEstr.setB03FlPreAgg(flPreAgg);
    }

    @Override
    public Character getFlPreAggObj() {
        if (ws.getIndBilaTrchEstr().getFlPreAgg() >= 0) {
            return ((Character)getFlPreAgg());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlPreAggObj(Character flPreAggObj) {
        if (flPreAggObj != null) {
            setFlPreAgg(((char)flPreAggObj));
            ws.getIndBilaTrchEstr().setFlPreAgg(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setFlPreAgg(((short)-1));
        }
    }

    @Override
    public char getFlPreDaRis() {
        return bilaTrchEstr.getB03FlPreDaRis();
    }

    @Override
    public void setFlPreDaRis(char flPreDaRis) {
        this.bilaTrchEstr.setB03FlPreDaRis(flPreDaRis);
    }

    @Override
    public Character getFlPreDaRisObj() {
        if (ws.getIndBilaTrchEstr().getFlPreDaRis() >= 0) {
            return ((Character)getFlPreDaRis());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlPreDaRisObj(Character flPreDaRisObj) {
        if (flPreDaRisObj != null) {
            setFlPreDaRis(((char)flPreDaRisObj));
            ws.getIndBilaTrchEstr().setFlPreDaRis(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setFlPreDaRis(((short)-1));
        }
    }

    @Override
    public char getFlSimulazione() {
        return bilaTrchEstr.getB03FlSimulazione();
    }

    @Override
    public void setFlSimulazione(char flSimulazione) {
        this.bilaTrchEstr.setB03FlSimulazione(flSimulazione);
    }

    @Override
    public Character getFlSimulazioneObj() {
        if (ws.getIndBilaTrchEstr().getFlSimulazione() >= 0) {
            return ((Character)getFlSimulazione());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlSimulazioneObj(Character flSimulazioneObj) {
        if (flSimulazioneObj != null) {
            setFlSimulazione(((char)flSimulazioneObj));
            ws.getIndBilaTrchEstr().setFlSimulazione(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setFlSimulazione(((short)-1));
        }
    }

    @Override
    public char getFlSwitch() {
        return bilaTrchEstr.getB03FlSwitch();
    }

    @Override
    public void setFlSwitch(char flSwitch) {
        this.bilaTrchEstr.setB03FlSwitch(flSwitch);
    }

    @Override
    public Character getFlSwitchObj() {
        if (ws.getIndBilaTrchEstr().getFlSwitch() >= 0) {
            return ((Character)getFlSwitch());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlSwitchObj(Character flSwitchObj) {
        if (flSwitchObj != null) {
            setFlSwitch(((char)flSwitchObj));
            ws.getIndBilaTrchEstr().setFlSwitch(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setFlSwitch(((short)-1));
        }
    }

    @Override
    public int getFraz() {
        return bilaTrchEstr.getB03Fraz().getB03Fraz();
    }

    @Override
    public void setFraz(int fraz) {
        this.bilaTrchEstr.getB03Fraz().setB03Fraz(fraz);
    }

    @Override
    public int getFrazDecrCpt() {
        return bilaTrchEstr.getB03FrazDecrCpt().getB03FrazDecrCpt();
    }

    @Override
    public void setFrazDecrCpt(int frazDecrCpt) {
        this.bilaTrchEstr.getB03FrazDecrCpt().setB03FrazDecrCpt(frazDecrCpt);
    }

    @Override
    public Integer getFrazDecrCptObj() {
        if (ws.getIndBilaTrchEstr().getFrazDecrCpt() >= 0) {
            return ((Integer)getFrazDecrCpt());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFrazDecrCptObj(Integer frazDecrCptObj) {
        if (frazDecrCptObj != null) {
            setFrazDecrCpt(((int)frazDecrCptObj));
            ws.getIndBilaTrchEstr().setFrazDecrCpt(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setFrazDecrCpt(((short)-1));
        }
    }

    @Override
    public int getFrazIniErogRen() {
        return bilaTrchEstr.getB03FrazIniErogRen().getB03FrazIniErogRen();
    }

    @Override
    public void setFrazIniErogRen(int frazIniErogRen) {
        this.bilaTrchEstr.getB03FrazIniErogRen().setB03FrazIniErogRen(frazIniErogRen);
    }

    @Override
    public Integer getFrazIniErogRenObj() {
        if (ws.getIndBilaTrchEstr().getFrazIniErogRen() >= 0) {
            return ((Integer)getFrazIniErogRen());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFrazIniErogRenObj(Integer frazIniErogRenObj) {
        if (frazIniErogRenObj != null) {
            setFrazIniErogRen(((int)frazIniErogRenObj));
            ws.getIndBilaTrchEstr().setFrazIniErogRen(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setFrazIniErogRen(((short)-1));
        }
    }

    @Override
    public Integer getFrazObj() {
        if (ws.getIndBilaTrchEstr().getFraz() >= 0) {
            return ((Integer)getFraz());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFrazObj(Integer frazObj) {
        if (frazObj != null) {
            setFraz(((int)frazObj));
            ws.getIndBilaTrchEstr().setFraz(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setFraz(((short)-1));
        }
    }

    @Override
    public String getIbAccComm() {
        return bilaTrchEstr.getB03IbAccComm();
    }

    @Override
    public void setIbAccComm(String ibAccComm) {
        this.bilaTrchEstr.setB03IbAccComm(ibAccComm);
    }

    @Override
    public String getIbAccCommObj() {
        if (ws.getIndBilaTrchEstr().getIbAccComm() >= 0) {
            return getIbAccComm();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIbAccCommObj(String ibAccCommObj) {
        if (ibAccCommObj != null) {
            setIbAccComm(ibAccCommObj);
            ws.getIndBilaTrchEstr().setIbAccComm(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setIbAccComm(((short)-1));
        }
    }

    @Override
    public String getIbAdes() {
        return bilaTrchEstr.getB03IbAdes();
    }

    @Override
    public void setIbAdes(String ibAdes) {
        this.bilaTrchEstr.setB03IbAdes(ibAdes);
    }

    @Override
    public String getIbAdesObj() {
        if (ws.getIndBilaTrchEstr().getIbAdes() >= 0) {
            return getIbAdes();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIbAdesObj(String ibAdesObj) {
        if (ibAdesObj != null) {
            setIbAdes(ibAdesObj);
            ws.getIndBilaTrchEstr().setIbAdes(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setIbAdes(((short)-1));
        }
    }

    @Override
    public String getIbPoli() {
        return bilaTrchEstr.getB03IbPoli();
    }

    @Override
    public void setIbPoli(String ibPoli) {
        this.bilaTrchEstr.setB03IbPoli(ibPoli);
    }

    @Override
    public String getIbPoliObj() {
        if (ws.getIndBilaTrchEstr().getIbPoli() >= 0) {
            return getIbPoli();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIbPoliObj(String ibPoliObj) {
        if (ibPoliObj != null) {
            setIbPoli(ibPoliObj);
            ws.getIndBilaTrchEstr().setIbPoli(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setIbPoli(((short)-1));
        }
    }

    @Override
    public String getIbTrchDiGar() {
        return bilaTrchEstr.getB03IbTrchDiGar();
    }

    @Override
    public void setIbTrchDiGar(String ibTrchDiGar) {
        this.bilaTrchEstr.setB03IbTrchDiGar(ibTrchDiGar);
    }

    @Override
    public String getIbTrchDiGarObj() {
        if (ws.getIndBilaTrchEstr().getIbTrchDiGar() >= 0) {
            return getIbTrchDiGar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIbTrchDiGarObj(String ibTrchDiGarObj) {
        if (ibTrchDiGarObj != null) {
            setIbTrchDiGar(ibTrchDiGarObj);
            ws.getIndBilaTrchEstr().setIbTrchDiGar(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setIbTrchDiGar(((short)-1));
        }
    }

    @Override
    public int getIdGar() {
        return bilaTrchEstr.getB03IdGar();
    }

    @Override
    public void setIdGar(int idGar) {
        this.bilaTrchEstr.setB03IdGar(idGar);
    }

    @Override
    public int getIdRichEstrazAgg() {
        return bilaTrchEstr.getB03IdRichEstrazAgg().getB03IdRichEstrazAgg();
    }

    @Override
    public void setIdRichEstrazAgg(int idRichEstrazAgg) {
        this.bilaTrchEstr.getB03IdRichEstrazAgg().setB03IdRichEstrazAgg(idRichEstrazAgg);
    }

    @Override
    public Integer getIdRichEstrazAggObj() {
        if (ws.getIndBilaTrchEstr().getIdRichEstrazAgg() >= 0) {
            return ((Integer)getIdRichEstrazAgg());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdRichEstrazAggObj(Integer idRichEstrazAggObj) {
        if (idRichEstrazAggObj != null) {
            setIdRichEstrazAgg(((int)idRichEstrazAggObj));
            ws.getIndBilaTrchEstr().setIdRichEstrazAgg(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setIdRichEstrazAgg(((short)-1));
        }
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        throw new FieldNotMappedException("idsv0003CodiceCompagniaAnia");
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        throw new FieldNotMappedException("idsv0003CodiceCompagniaAnia");
    }

    @Override
    public AfDecimal getImpCarCasoMor() {
        return bilaTrchEstr.getB03ImpCarCasoMor().getB03ImpCarCasoMor();
    }

    @Override
    public void setImpCarCasoMor(AfDecimal impCarCasoMor) {
        this.bilaTrchEstr.getB03ImpCarCasoMor().setB03ImpCarCasoMor(impCarCasoMor.copy());
    }

    @Override
    public AfDecimal getImpCarCasoMorObj() {
        if (ws.getIndBilaTrchEstr().getImpCarCasoMor() >= 0) {
            return getImpCarCasoMor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpCarCasoMorObj(AfDecimal impCarCasoMorObj) {
        if (impCarCasoMorObj != null) {
            setImpCarCasoMor(new AfDecimal(impCarCasoMorObj, 15, 3));
            ws.getIndBilaTrchEstr().setImpCarCasoMor(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setImpCarCasoMor(((short)-1));
        }
    }

    @Override
    public AfDecimal getIntrTecn() {
        return bilaTrchEstr.getB03IntrTecn().getB03IntrTecn();
    }

    @Override
    public void setIntrTecn(AfDecimal intrTecn) {
        this.bilaTrchEstr.getB03IntrTecn().setB03IntrTecn(intrTecn.copy());
    }

    @Override
    public AfDecimal getIntrTecnObj() {
        if (ws.getIndBilaTrchEstr().getIntrTecn() >= 0) {
            return getIntrTecn();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIntrTecnObj(AfDecimal intrTecnObj) {
        if (intrTecnObj != null) {
            setIntrTecn(new AfDecimal(intrTecnObj, 6, 3));
            ws.getIndBilaTrchEstr().setIntrTecn(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setIntrTecn(((short)-1));
        }
    }

    @Override
    public short getMetRiscSpcl() {
        return bilaTrchEstr.getB03MetRiscSpcl().getB03MetRiscSpcl();
    }

    @Override
    public void setMetRiscSpcl(short metRiscSpcl) {
        this.bilaTrchEstr.getB03MetRiscSpcl().setB03MetRiscSpcl(metRiscSpcl);
    }

    @Override
    public Short getMetRiscSpclObj() {
        if (ws.getIndBilaTrchEstr().getMetRiscSpcl() >= 0) {
            return ((Short)getMetRiscSpcl());
        }
        else {
            return null;
        }
    }

    @Override
    public void setMetRiscSpclObj(Short metRiscSpclObj) {
        if (metRiscSpclObj != null) {
            setMetRiscSpcl(((short)metRiscSpclObj));
            ws.getIndBilaTrchEstr().setMetRiscSpcl(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setMetRiscSpcl(((short)-1));
        }
    }

    @Override
    public AfDecimal getMinGartoT() {
        return bilaTrchEstr.getB03MinGartoT().getB03MinGartoT();
    }

    @Override
    public void setMinGartoT(AfDecimal minGartoT) {
        this.bilaTrchEstr.getB03MinGartoT().setB03MinGartoT(minGartoT.copy());
    }

    @Override
    public AfDecimal getMinGartoTObj() {
        if (ws.getIndBilaTrchEstr().getMinGartoT() >= 0) {
            return getMinGartoT();
        }
        else {
            return null;
        }
    }

    @Override
    public void setMinGartoTObj(AfDecimal minGartoTObj) {
        if (minGartoTObj != null) {
            setMinGartoT(new AfDecimal(minGartoTObj, 15, 3));
            ws.getIndBilaTrchEstr().setMinGartoT(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setMinGartoT(((short)-1));
        }
    }

    @Override
    public AfDecimal getMinTrnutT() {
        return bilaTrchEstr.getB03MinTrnutT().getB03MinTrnutT();
    }

    @Override
    public void setMinTrnutT(AfDecimal minTrnutT) {
        this.bilaTrchEstr.getB03MinTrnutT().setB03MinTrnutT(minTrnutT.copy());
    }

    @Override
    public AfDecimal getMinTrnutTObj() {
        if (ws.getIndBilaTrchEstr().getMinTrnutT() >= 0) {
            return getMinTrnutT();
        }
        else {
            return null;
        }
    }

    @Override
    public void setMinTrnutTObj(AfDecimal minTrnutTObj) {
        if (minTrnutTObj != null) {
            setMinTrnutT(new AfDecimal(minTrnutTObj, 15, 3));
            ws.getIndBilaTrchEstr().setMinTrnutT(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setMinTrnutT(((short)-1));
        }
    }

    @Override
    public AfDecimal getNsQuo() {
        return bilaTrchEstr.getB03NsQuo().getB03NsQuo();
    }

    @Override
    public void setNsQuo(AfDecimal nsQuo) {
        this.bilaTrchEstr.getB03NsQuo().setB03NsQuo(nsQuo.copy());
    }

    @Override
    public AfDecimal getNsQuoObj() {
        if (ws.getIndBilaTrchEstr().getNsQuo() >= 0) {
            return getNsQuo();
        }
        else {
            return null;
        }
    }

    @Override
    public void setNsQuoObj(AfDecimal nsQuoObj) {
        if (nsQuoObj != null) {
            setNsQuo(new AfDecimal(nsQuoObj, 6, 3));
            ws.getIndBilaTrchEstr().setNsQuo(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setNsQuo(((short)-1));
        }
    }

    @Override
    public String getNumFinanz() {
        return bilaTrchEstr.getB03NumFinanz();
    }

    @Override
    public void setNumFinanz(String numFinanz) {
        this.bilaTrchEstr.setB03NumFinanz(numFinanz);
    }

    @Override
    public String getNumFinanzObj() {
        if (ws.getIndBilaTrchEstr().getNumFinanz() >= 0) {
            return getNumFinanz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setNumFinanzObj(String numFinanzObj) {
        if (numFinanzObj != null) {
            setNumFinanz(numFinanzObj);
            ws.getIndBilaTrchEstr().setNumFinanz(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setNumFinanz(((short)-1));
        }
    }

    @Override
    public int getNumPrePatt() {
        return bilaTrchEstr.getB03NumPrePatt().getB03NumPrePatt();
    }

    @Override
    public void setNumPrePatt(int numPrePatt) {
        this.bilaTrchEstr.getB03NumPrePatt().setB03NumPrePatt(numPrePatt);
    }

    @Override
    public Integer getNumPrePattObj() {
        if (ws.getIndBilaTrchEstr().getNumPrePatt() >= 0) {
            return ((Integer)getNumPrePatt());
        }
        else {
            return null;
        }
    }

    @Override
    public void setNumPrePattObj(Integer numPrePattObj) {
        if (numPrePattObj != null) {
            setNumPrePatt(((int)numPrePattObj));
            ws.getIndBilaTrchEstr().setNumPrePatt(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setNumPrePatt(((short)-1));
        }
    }

    @Override
    public AfDecimal getOverComm() {
        return bilaTrchEstr.getB03OverComm().getB03OverComm();
    }

    @Override
    public void setOverComm(AfDecimal overComm) {
        this.bilaTrchEstr.getB03OverComm().setB03OverComm(overComm.copy());
    }

    @Override
    public AfDecimal getOverCommObj() {
        if (ws.getIndBilaTrchEstr().getOverComm() >= 0) {
            return getOverComm();
        }
        else {
            return null;
        }
    }

    @Override
    public void setOverCommObj(AfDecimal overCommObj) {
        if (overCommObj != null) {
            setOverComm(new AfDecimal(overCommObj, 15, 3));
            ws.getIndBilaTrchEstr().setOverComm(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setOverComm(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcCarAcq() {
        return bilaTrchEstr.getB03PcCarAcq().getB03PcCarAcq();
    }

    @Override
    public void setPcCarAcq(AfDecimal pcCarAcq) {
        this.bilaTrchEstr.getB03PcCarAcq().setB03PcCarAcq(pcCarAcq.copy());
    }

    @Override
    public AfDecimal getPcCarAcqObj() {
        if (ws.getIndBilaTrchEstr().getPcCarAcq() >= 0) {
            return getPcCarAcq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcCarAcqObj(AfDecimal pcCarAcqObj) {
        if (pcCarAcqObj != null) {
            setPcCarAcq(new AfDecimal(pcCarAcqObj, 6, 3));
            ws.getIndBilaTrchEstr().setPcCarAcq(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setPcCarAcq(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcCarGest() {
        return bilaTrchEstr.getB03PcCarGest().getB03PcCarGest();
    }

    @Override
    public void setPcCarGest(AfDecimal pcCarGest) {
        this.bilaTrchEstr.getB03PcCarGest().setB03PcCarGest(pcCarGest.copy());
    }

    @Override
    public AfDecimal getPcCarGestObj() {
        if (ws.getIndBilaTrchEstr().getPcCarGest() >= 0) {
            return getPcCarGest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcCarGestObj(AfDecimal pcCarGestObj) {
        if (pcCarGestObj != null) {
            setPcCarGest(new AfDecimal(pcCarGestObj, 6, 3));
            ws.getIndBilaTrchEstr().setPcCarGest(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setPcCarGest(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcCarMor() {
        return bilaTrchEstr.getB03PcCarMor().getB03PcCarMor();
    }

    @Override
    public void setPcCarMor(AfDecimal pcCarMor) {
        this.bilaTrchEstr.getB03PcCarMor().setB03PcCarMor(pcCarMor.copy());
    }

    @Override
    public AfDecimal getPcCarMorObj() {
        if (ws.getIndBilaTrchEstr().getPcCarMor() >= 0) {
            return getPcCarMor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcCarMorObj(AfDecimal pcCarMorObj) {
        if (pcCarMorObj != null) {
            setPcCarMor(new AfDecimal(pcCarMorObj, 6, 3));
            ws.getIndBilaTrchEstr().setPcCarMor(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setPcCarMor(((short)-1));
        }
    }

    @Override
    public char getPpInvrioTari() {
        return bilaTrchEstr.getB03PpInvrioTari();
    }

    @Override
    public void setPpInvrioTari(char ppInvrioTari) {
        this.bilaTrchEstr.setB03PpInvrioTari(ppInvrioTari);
    }

    @Override
    public Character getPpInvrioTariObj() {
        if (ws.getIndBilaTrchEstr().getPpInvrioTari() >= 0) {
            return ((Character)getPpInvrioTari());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPpInvrioTariObj(Character ppInvrioTariObj) {
        if (ppInvrioTariObj != null) {
            setPpInvrioTari(((char)ppInvrioTariObj));
            ws.getIndBilaTrchEstr().setPpInvrioTari(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setPpInvrioTari(((short)-1));
        }
    }

    @Override
    public AfDecimal getPreAnnualizRicor() {
        return bilaTrchEstr.getB03PreAnnualizRicor().getB03PreAnnualizRicor();
    }

    @Override
    public void setPreAnnualizRicor(AfDecimal preAnnualizRicor) {
        this.bilaTrchEstr.getB03PreAnnualizRicor().setB03PreAnnualizRicor(preAnnualizRicor.copy());
    }

    @Override
    public AfDecimal getPreAnnualizRicorObj() {
        if (ws.getIndBilaTrchEstr().getPreAnnualizRicor() >= 0) {
            return getPreAnnualizRicor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPreAnnualizRicorObj(AfDecimal preAnnualizRicorObj) {
        if (preAnnualizRicorObj != null) {
            setPreAnnualizRicor(new AfDecimal(preAnnualizRicorObj, 15, 3));
            ws.getIndBilaTrchEstr().setPreAnnualizRicor(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setPreAnnualizRicor(((short)-1));
        }
    }

    @Override
    public AfDecimal getPreCont() {
        return bilaTrchEstr.getB03PreCont().getB03PreCont();
    }

    @Override
    public void setPreCont(AfDecimal preCont) {
        this.bilaTrchEstr.getB03PreCont().setB03PreCont(preCont.copy());
    }

    @Override
    public AfDecimal getPreContObj() {
        if (ws.getIndBilaTrchEstr().getPreCont() >= 0) {
            return getPreCont();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPreContObj(AfDecimal preContObj) {
        if (preContObj != null) {
            setPreCont(new AfDecimal(preContObj, 15, 3));
            ws.getIndBilaTrchEstr().setPreCont(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setPreCont(((short)-1));
        }
    }

    @Override
    public AfDecimal getPreDovIni() {
        return bilaTrchEstr.getB03PreDovIni().getB03PreDovIni();
    }

    @Override
    public void setPreDovIni(AfDecimal preDovIni) {
        this.bilaTrchEstr.getB03PreDovIni().setB03PreDovIni(preDovIni.copy());
    }

    @Override
    public AfDecimal getPreDovIniObj() {
        if (ws.getIndBilaTrchEstr().getPreDovIni() >= 0) {
            return getPreDovIni();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPreDovIniObj(AfDecimal preDovIniObj) {
        if (preDovIniObj != null) {
            setPreDovIni(new AfDecimal(preDovIniObj, 15, 3));
            ws.getIndBilaTrchEstr().setPreDovIni(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setPreDovIni(((short)-1));
        }
    }

    @Override
    public AfDecimal getPreDovRivtoT() {
        return bilaTrchEstr.getB03PreDovRivtoT().getB03PreDovRivtoT();
    }

    @Override
    public void setPreDovRivtoT(AfDecimal preDovRivtoT) {
        this.bilaTrchEstr.getB03PreDovRivtoT().setB03PreDovRivtoT(preDovRivtoT.copy());
    }

    @Override
    public AfDecimal getPreDovRivtoTObj() {
        if (ws.getIndBilaTrchEstr().getPreDovRivtoT() >= 0) {
            return getPreDovRivtoT();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPreDovRivtoTObj(AfDecimal preDovRivtoTObj) {
        if (preDovRivtoTObj != null) {
            setPreDovRivtoT(new AfDecimal(preDovRivtoTObj, 15, 3));
            ws.getIndBilaTrchEstr().setPreDovRivtoT(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setPreDovRivtoT(((short)-1));
        }
    }

    @Override
    public AfDecimal getPrePattuitoIni() {
        return bilaTrchEstr.getB03PrePattuitoIni().getB03PrePattuitoIni();
    }

    @Override
    public void setPrePattuitoIni(AfDecimal prePattuitoIni) {
        this.bilaTrchEstr.getB03PrePattuitoIni().setB03PrePattuitoIni(prePattuitoIni.copy());
    }

    @Override
    public AfDecimal getPrePattuitoIniObj() {
        if (ws.getIndBilaTrchEstr().getPrePattuitoIni() >= 0) {
            return getPrePattuitoIni();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPrePattuitoIniObj(AfDecimal prePattuitoIniObj) {
        if (prePattuitoIniObj != null) {
            setPrePattuitoIni(new AfDecimal(prePattuitoIniObj, 15, 3));
            ws.getIndBilaTrchEstr().setPrePattuitoIni(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setPrePattuitoIni(((short)-1));
        }
    }

    @Override
    public AfDecimal getPrePpIni() {
        return bilaTrchEstr.getB03PrePpIni().getB03PrePpIni();
    }

    @Override
    public void setPrePpIni(AfDecimal prePpIni) {
        this.bilaTrchEstr.getB03PrePpIni().setB03PrePpIni(prePpIni.copy());
    }

    @Override
    public AfDecimal getPrePpIniObj() {
        if (ws.getIndBilaTrchEstr().getPrePpIni() >= 0) {
            return getPrePpIni();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPrePpIniObj(AfDecimal prePpIniObj) {
        if (prePpIniObj != null) {
            setPrePpIni(new AfDecimal(prePpIniObj, 15, 3));
            ws.getIndBilaTrchEstr().setPrePpIni(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setPrePpIni(((short)-1));
        }
    }

    @Override
    public AfDecimal getPrePpUlt() {
        return bilaTrchEstr.getB03PrePpUlt().getB03PrePpUlt();
    }

    @Override
    public void setPrePpUlt(AfDecimal prePpUlt) {
        this.bilaTrchEstr.getB03PrePpUlt().setB03PrePpUlt(prePpUlt.copy());
    }

    @Override
    public AfDecimal getPrePpUltObj() {
        if (ws.getIndBilaTrchEstr().getPrePpUlt() >= 0) {
            return getPrePpUlt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPrePpUltObj(AfDecimal prePpUltObj) {
        if (prePpUltObj != null) {
            setPrePpUlt(new AfDecimal(prePpUltObj, 15, 3));
            ws.getIndBilaTrchEstr().setPrePpUlt(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setPrePpUlt(((short)-1));
        }
    }

    @Override
    public AfDecimal getPreRiasto() {
        return bilaTrchEstr.getB03PreRiasto().getB03PreRiasto();
    }

    @Override
    public void setPreRiasto(AfDecimal preRiasto) {
        this.bilaTrchEstr.getB03PreRiasto().setB03PreRiasto(preRiasto.copy());
    }

    @Override
    public AfDecimal getPreRiastoEcc() {
        return bilaTrchEstr.getB03PreRiastoEcc().getB03PreRiastoEcc();
    }

    @Override
    public void setPreRiastoEcc(AfDecimal preRiastoEcc) {
        this.bilaTrchEstr.getB03PreRiastoEcc().setB03PreRiastoEcc(preRiastoEcc.copy());
    }

    @Override
    public AfDecimal getPreRiastoEccObj() {
        if (ws.getIndBilaTrchEstr().getPreRiastoEcc() >= 0) {
            return getPreRiastoEcc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPreRiastoEccObj(AfDecimal preRiastoEccObj) {
        if (preRiastoEccObj != null) {
            setPreRiastoEcc(new AfDecimal(preRiastoEccObj, 15, 3));
            ws.getIndBilaTrchEstr().setPreRiastoEcc(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setPreRiastoEcc(((short)-1));
        }
    }

    @Override
    public AfDecimal getPreRiastoObj() {
        if (ws.getIndBilaTrchEstr().getPreRiasto() >= 0) {
            return getPreRiasto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPreRiastoObj(AfDecimal preRiastoObj) {
        if (preRiastoObj != null) {
            setPreRiasto(new AfDecimal(preRiastoObj, 15, 3));
            ws.getIndBilaTrchEstr().setPreRiasto(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setPreRiasto(((short)-1));
        }
    }

    @Override
    public AfDecimal getPreRshT() {
        return bilaTrchEstr.getB03PreRshT().getB03PreRshT();
    }

    @Override
    public void setPreRshT(AfDecimal preRshT) {
        this.bilaTrchEstr.getB03PreRshT().setB03PreRshT(preRshT.copy());
    }

    @Override
    public AfDecimal getPreRshTObj() {
        if (ws.getIndBilaTrchEstr().getPreRshT() >= 0) {
            return getPreRshT();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPreRshTObj(AfDecimal preRshTObj) {
        if (preRshTObj != null) {
            setPreRshT(new AfDecimal(preRshTObj, 15, 3));
            ws.getIndBilaTrchEstr().setPreRshT(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setPreRshT(((short)-1));
        }
    }

    @Override
    public AfDecimal getProvAcq() {
        return bilaTrchEstr.getB03ProvAcq().getB03ProvAcq();
    }

    @Override
    public void setProvAcq(AfDecimal provAcq) {
        this.bilaTrchEstr.getB03ProvAcq().setB03ProvAcq(provAcq.copy());
    }

    @Override
    public AfDecimal getProvAcqObj() {
        if (ws.getIndBilaTrchEstr().getProvAcq() >= 0) {
            return getProvAcq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setProvAcqObj(AfDecimal provAcqObj) {
        if (provAcqObj != null) {
            setProvAcq(new AfDecimal(provAcqObj, 15, 3));
            ws.getIndBilaTrchEstr().setProvAcq(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setProvAcq(((short)-1));
        }
    }

    @Override
    public AfDecimal getProvAcqRicor() {
        return bilaTrchEstr.getB03ProvAcqRicor().getB03ProvAcqRicor();
    }

    @Override
    public void setProvAcqRicor(AfDecimal provAcqRicor) {
        this.bilaTrchEstr.getB03ProvAcqRicor().setB03ProvAcqRicor(provAcqRicor.copy());
    }

    @Override
    public AfDecimal getProvAcqRicorObj() {
        if (ws.getIndBilaTrchEstr().getProvAcqRicor() >= 0) {
            return getProvAcqRicor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setProvAcqRicorObj(AfDecimal provAcqRicorObj) {
        if (provAcqRicorObj != null) {
            setProvAcqRicor(new AfDecimal(provAcqRicorObj, 15, 3));
            ws.getIndBilaTrchEstr().setProvAcqRicor(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setProvAcqRicor(((short)-1));
        }
    }

    @Override
    public AfDecimal getProvInc() {
        return bilaTrchEstr.getB03ProvInc().getB03ProvInc();
    }

    @Override
    public void setProvInc(AfDecimal provInc) {
        this.bilaTrchEstr.getB03ProvInc().setB03ProvInc(provInc.copy());
    }

    @Override
    public AfDecimal getProvIncObj() {
        if (ws.getIndBilaTrchEstr().getProvInc() >= 0) {
            return getProvInc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setProvIncObj(AfDecimal provIncObj) {
        if (provIncObj != null) {
            setProvInc(new AfDecimal(provIncObj, 15, 3));
            ws.getIndBilaTrchEstr().setProvInc(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setProvInc(((short)-1));
        }
    }

    @Override
    public AfDecimal getPrstzAggIni() {
        return bilaTrchEstr.getB03PrstzAggIni().getB03PrstzAggIni();
    }

    @Override
    public void setPrstzAggIni(AfDecimal prstzAggIni) {
        this.bilaTrchEstr.getB03PrstzAggIni().setB03PrstzAggIni(prstzAggIni.copy());
    }

    @Override
    public AfDecimal getPrstzAggIniObj() {
        if (ws.getIndBilaTrchEstr().getPrstzAggIni() >= 0) {
            return getPrstzAggIni();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPrstzAggIniObj(AfDecimal prstzAggIniObj) {
        if (prstzAggIniObj != null) {
            setPrstzAggIni(new AfDecimal(prstzAggIniObj, 15, 3));
            ws.getIndBilaTrchEstr().setPrstzAggIni(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setPrstzAggIni(((short)-1));
        }
    }

    @Override
    public AfDecimal getPrstzAggUlt() {
        return bilaTrchEstr.getB03PrstzAggUlt().getB03PrstzAggUlt();
    }

    @Override
    public void setPrstzAggUlt(AfDecimal prstzAggUlt) {
        this.bilaTrchEstr.getB03PrstzAggUlt().setB03PrstzAggUlt(prstzAggUlt.copy());
    }

    @Override
    public AfDecimal getPrstzAggUltObj() {
        if (ws.getIndBilaTrchEstr().getPrstzAggUlt() >= 0) {
            return getPrstzAggUlt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPrstzAggUltObj(AfDecimal prstzAggUltObj) {
        if (prstzAggUltObj != null) {
            setPrstzAggUlt(new AfDecimal(prstzAggUltObj, 15, 3));
            ws.getIndBilaTrchEstr().setPrstzAggUlt(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setPrstzAggUlt(((short)-1));
        }
    }

    @Override
    public AfDecimal getPrstzIni() {
        return bilaTrchEstr.getB03PrstzIni().getB03PrstzIni();
    }

    @Override
    public void setPrstzIni(AfDecimal prstzIni) {
        this.bilaTrchEstr.getB03PrstzIni().setB03PrstzIni(prstzIni.copy());
    }

    @Override
    public AfDecimal getPrstzIniObj() {
        if (ws.getIndBilaTrchEstr().getPrstzIni() >= 0) {
            return getPrstzIni();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPrstzIniObj(AfDecimal prstzIniObj) {
        if (prstzIniObj != null) {
            setPrstzIni(new AfDecimal(prstzIniObj, 15, 3));
            ws.getIndBilaTrchEstr().setPrstzIni(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setPrstzIni(((short)-1));
        }
    }

    @Override
    public AfDecimal getPrstzT() {
        return bilaTrchEstr.getB03PrstzT().getB03PrstzT();
    }

    @Override
    public void setPrstzT(AfDecimal prstzT) {
        this.bilaTrchEstr.getB03PrstzT().setB03PrstzT(prstzT.copy());
    }

    @Override
    public AfDecimal getPrstzTObj() {
        if (ws.getIndBilaTrchEstr().getPrstzT() >= 0) {
            return getPrstzT();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPrstzTObj(AfDecimal prstzTObj) {
        if (prstzTObj != null) {
            setPrstzT(new AfDecimal(prstzTObj, 15, 3));
            ws.getIndBilaTrchEstr().setPrstzT(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setPrstzT(((short)-1));
        }
    }

    @Override
    public AfDecimal getQtzSpZCoupDtC() {
        return bilaTrchEstr.getB03QtzSpZCoupDtC().getB03QtzSpZCoupDtC();
    }

    @Override
    public void setQtzSpZCoupDtC(AfDecimal qtzSpZCoupDtC) {
        this.bilaTrchEstr.getB03QtzSpZCoupDtC().setB03QtzSpZCoupDtC(qtzSpZCoupDtC.copy());
    }

    @Override
    public AfDecimal getQtzSpZCoupDtCObj() {
        if (ws.getIndBilaTrchEstr().getQtzSpZCoupDtC() >= 0) {
            return getQtzSpZCoupDtC();
        }
        else {
            return null;
        }
    }

    @Override
    public void setQtzSpZCoupDtCObj(AfDecimal qtzSpZCoupDtCObj) {
        if (qtzSpZCoupDtCObj != null) {
            setQtzSpZCoupDtC(new AfDecimal(qtzSpZCoupDtCObj, 12, 5));
            ws.getIndBilaTrchEstr().setQtzSpZCoupDtC(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setQtzSpZCoupDtC(((short)-1));
        }
    }

    @Override
    public AfDecimal getQtzSpZCoupEmis() {
        return bilaTrchEstr.getB03QtzSpZCoupEmis().getB03QtzSpZCoupEmis();
    }

    @Override
    public void setQtzSpZCoupEmis(AfDecimal qtzSpZCoupEmis) {
        this.bilaTrchEstr.getB03QtzSpZCoupEmis().setB03QtzSpZCoupEmis(qtzSpZCoupEmis.copy());
    }

    @Override
    public AfDecimal getQtzSpZCoupEmisObj() {
        if (ws.getIndBilaTrchEstr().getQtzSpZCoupEmis() >= 0) {
            return getQtzSpZCoupEmis();
        }
        else {
            return null;
        }
    }

    @Override
    public void setQtzSpZCoupEmisObj(AfDecimal qtzSpZCoupEmisObj) {
        if (qtzSpZCoupEmisObj != null) {
            setQtzSpZCoupEmis(new AfDecimal(qtzSpZCoupEmisObj, 12, 5));
            ws.getIndBilaTrchEstr().setQtzSpZCoupEmis(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setQtzSpZCoupEmis(((short)-1));
        }
    }

    @Override
    public AfDecimal getQtzSpZOpzDtCa() {
        return bilaTrchEstr.getB03QtzSpZOpzDtCa().getB03QtzSpZOpzDtCa();
    }

    @Override
    public void setQtzSpZOpzDtCa(AfDecimal qtzSpZOpzDtCa) {
        this.bilaTrchEstr.getB03QtzSpZOpzDtCa().setB03QtzSpZOpzDtCa(qtzSpZOpzDtCa.copy());
    }

    @Override
    public AfDecimal getQtzSpZOpzDtCaObj() {
        if (ws.getIndBilaTrchEstr().getQtzSpZOpzDtCa() >= 0) {
            return getQtzSpZOpzDtCa();
        }
        else {
            return null;
        }
    }

    @Override
    public void setQtzSpZOpzDtCaObj(AfDecimal qtzSpZOpzDtCaObj) {
        if (qtzSpZOpzDtCaObj != null) {
            setQtzSpZOpzDtCa(new AfDecimal(qtzSpZOpzDtCaObj, 12, 5));
            ws.getIndBilaTrchEstr().setQtzSpZOpzDtCa(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setQtzSpZOpzDtCa(((short)-1));
        }
    }

    @Override
    public AfDecimal getQtzSpZOpzEmis() {
        return bilaTrchEstr.getB03QtzSpZOpzEmis().getB03QtzSpZOpzEmis();
    }

    @Override
    public void setQtzSpZOpzEmis(AfDecimal qtzSpZOpzEmis) {
        this.bilaTrchEstr.getB03QtzSpZOpzEmis().setB03QtzSpZOpzEmis(qtzSpZOpzEmis.copy());
    }

    @Override
    public AfDecimal getQtzSpZOpzEmisObj() {
        if (ws.getIndBilaTrchEstr().getQtzSpZOpzEmis() >= 0) {
            return getQtzSpZOpzEmis();
        }
        else {
            return null;
        }
    }

    @Override
    public void setQtzSpZOpzEmisObj(AfDecimal qtzSpZOpzEmisObj) {
        if (qtzSpZOpzEmisObj != null) {
            setQtzSpZOpzEmis(new AfDecimal(qtzSpZOpzEmisObj, 12, 5));
            ws.getIndBilaTrchEstr().setQtzSpZOpzEmis(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setQtzSpZOpzEmis(((short)-1));
        }
    }

    @Override
    public AfDecimal getQtzTotDtCalc() {
        return bilaTrchEstr.getB03QtzTotDtCalc().getB03QtzTotDtCalc();
    }

    @Override
    public void setQtzTotDtCalc(AfDecimal qtzTotDtCalc) {
        this.bilaTrchEstr.getB03QtzTotDtCalc().setB03QtzTotDtCalc(qtzTotDtCalc.copy());
    }

    @Override
    public AfDecimal getQtzTotDtCalcObj() {
        if (ws.getIndBilaTrchEstr().getQtzTotDtCalc() >= 0) {
            return getQtzTotDtCalc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setQtzTotDtCalcObj(AfDecimal qtzTotDtCalcObj) {
        if (qtzTotDtCalcObj != null) {
            setQtzTotDtCalc(new AfDecimal(qtzTotDtCalcObj, 12, 5));
            ws.getIndBilaTrchEstr().setQtzTotDtCalc(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setQtzTotDtCalc(((short)-1));
        }
    }

    @Override
    public AfDecimal getQtzTotDtUltBil() {
        return bilaTrchEstr.getB03QtzTotDtUltBil().getB03QtzTotDtUltBil();
    }

    @Override
    public void setQtzTotDtUltBil(AfDecimal qtzTotDtUltBil) {
        this.bilaTrchEstr.getB03QtzTotDtUltBil().setB03QtzTotDtUltBil(qtzTotDtUltBil.copy());
    }

    @Override
    public AfDecimal getQtzTotDtUltBilObj() {
        if (ws.getIndBilaTrchEstr().getQtzTotDtUltBil() >= 0) {
            return getQtzTotDtUltBil();
        }
        else {
            return null;
        }
    }

    @Override
    public void setQtzTotDtUltBilObj(AfDecimal qtzTotDtUltBilObj) {
        if (qtzTotDtUltBilObj != null) {
            setQtzTotDtUltBil(new AfDecimal(qtzTotDtUltBilObj, 12, 5));
            ws.getIndBilaTrchEstr().setQtzTotDtUltBil(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setQtzTotDtUltBil(((short)-1));
        }
    }

    @Override
    public AfDecimal getQtzTotEmis() {
        return bilaTrchEstr.getB03QtzTotEmis().getB03QtzTotEmis();
    }

    @Override
    public void setQtzTotEmis(AfDecimal qtzTotEmis) {
        this.bilaTrchEstr.getB03QtzTotEmis().setB03QtzTotEmis(qtzTotEmis.copy());
    }

    @Override
    public AfDecimal getQtzTotEmisObj() {
        if (ws.getIndBilaTrchEstr().getQtzTotEmis() >= 0) {
            return getQtzTotEmis();
        }
        else {
            return null;
        }
    }

    @Override
    public void setQtzTotEmisObj(AfDecimal qtzTotEmisObj) {
        if (qtzTotEmisObj != null) {
            setQtzTotEmis(new AfDecimal(qtzTotEmisObj, 12, 5));
            ws.getIndBilaTrchEstr().setQtzTotEmis(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setQtzTotEmis(((short)-1));
        }
    }

    @Override
    public String getRamoBila() {
        return bilaTrchEstr.getB03RamoBila();
    }

    @Override
    public void setRamoBila(String ramoBila) {
        this.bilaTrchEstr.setB03RamoBila(ramoBila);
    }

    @Override
    public AfDecimal getRappel() {
        return bilaTrchEstr.getB03Rappel().getB03Rappel();
    }

    @Override
    public void setRappel(AfDecimal rappel) {
        this.bilaTrchEstr.getB03Rappel().setB03Rappel(rappel.copy());
    }

    @Override
    public AfDecimal getRappelObj() {
        if (ws.getIndBilaTrchEstr().getRappel() >= 0) {
            return getRappel();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRappelObj(AfDecimal rappelObj) {
        if (rappelObj != null) {
            setRappel(new AfDecimal(rappelObj, 15, 3));
            ws.getIndBilaTrchEstr().setRappel(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setRappel(((short)-1));
        }
    }

    @Override
    public AfDecimal getRatRen() {
        return bilaTrchEstr.getB03RatRen().getB03RatRen();
    }

    @Override
    public void setRatRen(AfDecimal ratRen) {
        this.bilaTrchEstr.getB03RatRen().setB03RatRen(ratRen.copy());
    }

    @Override
    public AfDecimal getRatRenObj() {
        if (ws.getIndBilaTrchEstr().getRatRen() >= 0) {
            return getRatRen();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRatRenObj(AfDecimal ratRenObj) {
        if (ratRenObj != null) {
            setRatRen(new AfDecimal(ratRenObj, 15, 3));
            ws.getIndBilaTrchEstr().setRatRen(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setRatRen(((short)-1));
        }
    }

    @Override
    public AfDecimal getRemunAss() {
        return bilaTrchEstr.getB03RemunAss().getB03RemunAss();
    }

    @Override
    public void setRemunAss(AfDecimal remunAss) {
        this.bilaTrchEstr.getB03RemunAss().setB03RemunAss(remunAss.copy());
    }

    @Override
    public AfDecimal getRemunAssObj() {
        if (ws.getIndBilaTrchEstr().getRemunAss() >= 0) {
            return getRemunAss();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRemunAssObj(AfDecimal remunAssObj) {
        if (remunAssObj != null) {
            setRemunAss(new AfDecimal(remunAssObj, 15, 3));
            ws.getIndBilaTrchEstr().setRemunAss(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setRemunAss(((short)-1));
        }
    }

    @Override
    public AfDecimal getRisAcqT() {
        return bilaTrchEstr.getB03RisAcqT().getB03RisAcqT();
    }

    @Override
    public void setRisAcqT(AfDecimal risAcqT) {
        this.bilaTrchEstr.getB03RisAcqT().setB03RisAcqT(risAcqT.copy());
    }

    @Override
    public AfDecimal getRisAcqTObj() {
        if (ws.getIndBilaTrchEstr().getRisAcqT() >= 0) {
            return getRisAcqT();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRisAcqTObj(AfDecimal risAcqTObj) {
        if (risAcqTObj != null) {
            setRisAcqT(new AfDecimal(risAcqTObj, 15, 3));
            ws.getIndBilaTrchEstr().setRisAcqT(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setRisAcqT(((short)-1));
        }
    }

    @Override
    public AfDecimal getRisMatChiuPrec() {
        return bilaTrchEstr.getB03RisMatChiuPrec().getB03RisMatChiuPrec();
    }

    @Override
    public void setRisMatChiuPrec(AfDecimal risMatChiuPrec) {
        this.bilaTrchEstr.getB03RisMatChiuPrec().setB03RisMatChiuPrec(risMatChiuPrec.copy());
    }

    @Override
    public AfDecimal getRisMatChiuPrecObj() {
        if (ws.getIndBilaTrchEstr().getRisMatChiuPrec() >= 0) {
            return getRisMatChiuPrec();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRisMatChiuPrecObj(AfDecimal risMatChiuPrecObj) {
        if (risMatChiuPrecObj != null) {
            setRisMatChiuPrec(new AfDecimal(risMatChiuPrecObj, 15, 3));
            ws.getIndBilaTrchEstr().setRisMatChiuPrec(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setRisMatChiuPrec(((short)-1));
        }
    }

    @Override
    public AfDecimal getRisPuraT() {
        return bilaTrchEstr.getB03RisPuraT().getB03RisPuraT();
    }

    @Override
    public void setRisPuraT(AfDecimal risPuraT) {
        this.bilaTrchEstr.getB03RisPuraT().setB03RisPuraT(risPuraT.copy());
    }

    @Override
    public AfDecimal getRisPuraTObj() {
        if (ws.getIndBilaTrchEstr().getRisPuraT() >= 0) {
            return getRisPuraT();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRisPuraTObj(AfDecimal risPuraTObj) {
        if (risPuraTObj != null) {
            setRisPuraT(new AfDecimal(risPuraTObj, 15, 3));
            ws.getIndBilaTrchEstr().setRisPuraT(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setRisPuraT(((short)-1));
        }
    }

    @Override
    public AfDecimal getRisRiasta() {
        return bilaTrchEstr.getB03RisRiasta().getB03RisRiasta();
    }

    @Override
    public void setRisRiasta(AfDecimal risRiasta) {
        this.bilaTrchEstr.getB03RisRiasta().setB03RisRiasta(risRiasta.copy());
    }

    @Override
    public AfDecimal getRisRiastaEcc() {
        return bilaTrchEstr.getB03RisRiastaEcc().getB03RisRiastaEcc();
    }

    @Override
    public void setRisRiastaEcc(AfDecimal risRiastaEcc) {
        this.bilaTrchEstr.getB03RisRiastaEcc().setB03RisRiastaEcc(risRiastaEcc.copy());
    }

    @Override
    public AfDecimal getRisRiastaEccObj() {
        if (ws.getIndBilaTrchEstr().getRisRiastaEcc() >= 0) {
            return getRisRiastaEcc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRisRiastaEccObj(AfDecimal risRiastaEccObj) {
        if (risRiastaEccObj != null) {
            setRisRiastaEcc(new AfDecimal(risRiastaEccObj, 15, 3));
            ws.getIndBilaTrchEstr().setRisRiastaEcc(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setRisRiastaEcc(((short)-1));
        }
    }

    @Override
    public AfDecimal getRisRiastaObj() {
        if (ws.getIndBilaTrchEstr().getRisRiasta() >= 0) {
            return getRisRiasta();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRisRiastaObj(AfDecimal risRiastaObj) {
        if (risRiastaObj != null) {
            setRisRiasta(new AfDecimal(risRiastaObj, 15, 3));
            ws.getIndBilaTrchEstr().setRisRiasta(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setRisRiasta(((short)-1));
        }
    }

    @Override
    public AfDecimal getRisRistorniCap() {
        return bilaTrchEstr.getB03RisRistorniCap().getB03RisRistorniCap();
    }

    @Override
    public void setRisRistorniCap(AfDecimal risRistorniCap) {
        this.bilaTrchEstr.getB03RisRistorniCap().setB03RisRistorniCap(risRistorniCap.copy());
    }

    @Override
    public AfDecimal getRisRistorniCapObj() {
        if (ws.getIndBilaTrchEstr().getRisRistorniCap() >= 0) {
            return getRisRistorniCap();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRisRistorniCapObj(AfDecimal risRistorniCapObj) {
        if (risRistorniCapObj != null) {
            setRisRistorniCap(new AfDecimal(risRistorniCapObj, 15, 3));
            ws.getIndBilaTrchEstr().setRisRistorniCap(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setRisRistorniCap(((short)-1));
        }
    }

    @Override
    public AfDecimal getRisSpeT() {
        return bilaTrchEstr.getB03RisSpeT().getB03RisSpeT();
    }

    @Override
    public void setRisSpeT(AfDecimal risSpeT) {
        this.bilaTrchEstr.getB03RisSpeT().setB03RisSpeT(risSpeT.copy());
    }

    @Override
    public AfDecimal getRisSpeTObj() {
        if (ws.getIndBilaTrchEstr().getRisSpeT() >= 0) {
            return getRisSpeT();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRisSpeTObj(AfDecimal risSpeTObj) {
        if (risSpeTObj != null) {
            setRisSpeT(new AfDecimal(risSpeTObj, 15, 3));
            ws.getIndBilaTrchEstr().setRisSpeT(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setRisSpeT(((short)-1));
        }
    }

    @Override
    public AfDecimal getRisZilT() {
        return bilaTrchEstr.getB03RisZilT().getB03RisZilT();
    }

    @Override
    public void setRisZilT(AfDecimal risZilT) {
        this.bilaTrchEstr.getB03RisZilT().setB03RisZilT(risZilT.copy());
    }

    @Override
    public AfDecimal getRisZilTObj() {
        if (ws.getIndBilaTrchEstr().getRisZilT() >= 0) {
            return getRisZilT();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRisZilTObj(AfDecimal risZilTObj) {
        if (risZilTObj != null) {
            setRisZilT(new AfDecimal(risZilTObj, 15, 3));
            ws.getIndBilaTrchEstr().setRisZilT(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setRisZilT(((short)-1));
        }
    }

    @Override
    public AfDecimal getRiscpar() {
        return bilaTrchEstr.getB03Riscpar().getB03Riscpar();
    }

    @Override
    public void setRiscpar(AfDecimal riscpar) {
        this.bilaTrchEstr.getB03Riscpar().setB03Riscpar(riscpar.copy());
    }

    @Override
    public AfDecimal getRiscparObj() {
        if (ws.getIndBilaTrchEstr().getRiscpar() >= 0) {
            return getRiscpar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRiscparObj(AfDecimal riscparObj) {
        if (riscparObj != null) {
            setRiscpar(new AfDecimal(riscparObj, 15, 3));
            ws.getIndBilaTrchEstr().setRiscpar(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setRiscpar(((short)-1));
        }
    }

    @Override
    public char getSex1oAssto() {
        return bilaTrchEstr.getB03Sex1oAssto();
    }

    @Override
    public void setSex1oAssto(char sex1oAssto) {
        this.bilaTrchEstr.setB03Sex1oAssto(sex1oAssto);
    }

    @Override
    public Character getSex1oAsstoObj() {
        if (ws.getIndBilaTrchEstr().getSex1oAssto() >= 0) {
            return ((Character)getSex1oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setSex1oAsstoObj(Character sex1oAsstoObj) {
        if (sex1oAsstoObj != null) {
            setSex1oAssto(((char)sex1oAsstoObj));
            ws.getIndBilaTrchEstr().setSex1oAssto(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setSex1oAssto(((short)-1));
        }
    }

    @Override
    public char getStatAssto1() {
        return bilaTrchEstr.getB03StatAssto1();
    }

    @Override
    public void setStatAssto1(char statAssto1) {
        this.bilaTrchEstr.setB03StatAssto1(statAssto1);
    }

    @Override
    public Character getStatAssto1Obj() {
        if (ws.getIndBilaTrchEstr().getStatAssto1() >= 0) {
            return ((Character)getStatAssto1());
        }
        else {
            return null;
        }
    }

    @Override
    public void setStatAssto1Obj(Character statAssto1Obj) {
        if (statAssto1Obj != null) {
            setStatAssto1(((char)statAssto1Obj));
            ws.getIndBilaTrchEstr().setStatAssto1(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setStatAssto1(((short)-1));
        }
    }

    @Override
    public char getStatAssto2() {
        return bilaTrchEstr.getB03StatAssto2();
    }

    @Override
    public void setStatAssto2(char statAssto2) {
        this.bilaTrchEstr.setB03StatAssto2(statAssto2);
    }

    @Override
    public Character getStatAssto2Obj() {
        if (ws.getIndBilaTrchEstr().getStatAssto2() >= 0) {
            return ((Character)getStatAssto2());
        }
        else {
            return null;
        }
    }

    @Override
    public void setStatAssto2Obj(Character statAssto2Obj) {
        if (statAssto2Obj != null) {
            setStatAssto2(((char)statAssto2Obj));
            ws.getIndBilaTrchEstr().setStatAssto2(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setStatAssto2(((short)-1));
        }
    }

    @Override
    public char getStatAssto3() {
        return bilaTrchEstr.getB03StatAssto3();
    }

    @Override
    public void setStatAssto3(char statAssto3) {
        this.bilaTrchEstr.setB03StatAssto3(statAssto3);
    }

    @Override
    public Character getStatAssto3Obj() {
        if (ws.getIndBilaTrchEstr().getStatAssto3() >= 0) {
            return ((Character)getStatAssto3());
        }
        else {
            return null;
        }
    }

    @Override
    public void setStatAssto3Obj(Character statAssto3Obj) {
        if (statAssto3Obj != null) {
            setStatAssto3(((char)statAssto3Obj));
            ws.getIndBilaTrchEstr().setStatAssto3(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setStatAssto3(((short)-1));
        }
    }

    @Override
    public char getStatTbgcAssto1() {
        return bilaTrchEstr.getB03StatTbgcAssto1();
    }

    @Override
    public void setStatTbgcAssto1(char statTbgcAssto1) {
        this.bilaTrchEstr.setB03StatTbgcAssto1(statTbgcAssto1);
    }

    @Override
    public Character getStatTbgcAssto1Obj() {
        if (ws.getIndBilaTrchEstr().getStatTbgcAssto1() >= 0) {
            return ((Character)getStatTbgcAssto1());
        }
        else {
            return null;
        }
    }

    @Override
    public void setStatTbgcAssto1Obj(Character statTbgcAssto1Obj) {
        if (statTbgcAssto1Obj != null) {
            setStatTbgcAssto1(((char)statTbgcAssto1Obj));
            ws.getIndBilaTrchEstr().setStatTbgcAssto1(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setStatTbgcAssto1(((short)-1));
        }
    }

    @Override
    public char getStatTbgcAssto2() {
        return bilaTrchEstr.getB03StatTbgcAssto2();
    }

    @Override
    public void setStatTbgcAssto2(char statTbgcAssto2) {
        this.bilaTrchEstr.setB03StatTbgcAssto2(statTbgcAssto2);
    }

    @Override
    public Character getStatTbgcAssto2Obj() {
        if (ws.getIndBilaTrchEstr().getStatTbgcAssto2() >= 0) {
            return ((Character)getStatTbgcAssto2());
        }
        else {
            return null;
        }
    }

    @Override
    public void setStatTbgcAssto2Obj(Character statTbgcAssto2Obj) {
        if (statTbgcAssto2Obj != null) {
            setStatTbgcAssto2(((char)statTbgcAssto2Obj));
            ws.getIndBilaTrchEstr().setStatTbgcAssto2(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setStatTbgcAssto2(((short)-1));
        }
    }

    @Override
    public char getStatTbgcAssto3() {
        return bilaTrchEstr.getB03StatTbgcAssto3();
    }

    @Override
    public void setStatTbgcAssto3(char statTbgcAssto3) {
        this.bilaTrchEstr.setB03StatTbgcAssto3(statTbgcAssto3);
    }

    @Override
    public Character getStatTbgcAssto3Obj() {
        if (ws.getIndBilaTrchEstr().getStatTbgcAssto3() >= 0) {
            return ((Character)getStatTbgcAssto3());
        }
        else {
            return null;
        }
    }

    @Override
    public void setStatTbgcAssto3Obj(Character statTbgcAssto3Obj) {
        if (statTbgcAssto3Obj != null) {
            setStatTbgcAssto3(((char)statTbgcAssto3Obj));
            ws.getIndBilaTrchEstr().setStatTbgcAssto3(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setStatTbgcAssto3(((short)-1));
        }
    }

    @Override
    public String getTpAccComm() {
        return bilaTrchEstr.getB03TpAccComm();
    }

    @Override
    public void setTpAccComm(String tpAccComm) {
        this.bilaTrchEstr.setB03TpAccComm(tpAccComm);
    }

    @Override
    public String getTpAccCommObj() {
        if (ws.getIndBilaTrchEstr().getTpAccComm() >= 0) {
            return getTpAccComm();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpAccCommObj(String tpAccCommObj) {
        if (tpAccCommObj != null) {
            setTpAccComm(tpAccCommObj);
            ws.getIndBilaTrchEstr().setTpAccComm(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setTpAccComm(((short)-1));
        }
    }

    @Override
    public char getTpAdegPre() {
        return bilaTrchEstr.getB03TpAdegPre();
    }

    @Override
    public void setTpAdegPre(char tpAdegPre) {
        this.bilaTrchEstr.setB03TpAdegPre(tpAdegPre);
    }

    @Override
    public Character getTpAdegPreObj() {
        if (ws.getIndBilaTrchEstr().getTpAdegPre() >= 0) {
            return ((Character)getTpAdegPre());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpAdegPreObj(Character tpAdegPreObj) {
        if (tpAdegPreObj != null) {
            setTpAdegPre(((char)tpAdegPreObj));
            ws.getIndBilaTrchEstr().setTpAdegPre(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setTpAdegPre(((short)-1));
        }
    }

    @Override
    public String getTpCalcRis() {
        return bilaTrchEstr.getB03TpCalcRis();
    }

    @Override
    public void setTpCalcRis(String tpCalcRis) {
        this.bilaTrchEstr.setB03TpCalcRis(tpCalcRis);
    }

    @Override
    public String getTpCausAdes() {
        return bilaTrchEstr.getB03TpCausAdes();
    }

    @Override
    public void setTpCausAdes(String tpCausAdes) {
        this.bilaTrchEstr.setB03TpCausAdes(tpCausAdes);
    }

    @Override
    public String getTpCausPoli() {
        return bilaTrchEstr.getB03TpCausPoli();
    }

    @Override
    public void setTpCausPoli(String tpCausPoli) {
        this.bilaTrchEstr.setB03TpCausPoli(tpCausPoli);
    }

    @Override
    public String getTpCausTrch() {
        return bilaTrchEstr.getB03TpCausTrch();
    }

    @Override
    public void setTpCausTrch(String tpCausTrch) {
        this.bilaTrchEstr.setB03TpCausTrch(tpCausTrch);
    }

    @Override
    public String getTpCoass() {
        return bilaTrchEstr.getB03TpCoass();
    }

    @Override
    public void setTpCoass(String tpCoass) {
        this.bilaTrchEstr.setB03TpCoass(tpCoass);
    }

    @Override
    public String getTpCoassObj() {
        if (ws.getIndBilaTrchEstr().getTpCoass() >= 0) {
            return getTpCoass();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpCoassObj(String tpCoassObj) {
        if (tpCoassObj != null) {
            setTpCoass(tpCoassObj);
            ws.getIndBilaTrchEstr().setTpCoass(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setTpCoass(((short)-1));
        }
    }

    @Override
    public String getTpCopCasoMor() {
        return bilaTrchEstr.getB03TpCopCasoMor();
    }

    @Override
    public void setTpCopCasoMor(String tpCopCasoMor) {
        this.bilaTrchEstr.setB03TpCopCasoMor(tpCopCasoMor);
    }

    @Override
    public String getTpCopCasoMorObj() {
        if (ws.getIndBilaTrchEstr().getTpCopCasoMor() >= 0) {
            return getTpCopCasoMor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpCopCasoMorObj(String tpCopCasoMorObj) {
        if (tpCopCasoMorObj != null) {
            setTpCopCasoMor(tpCopCasoMorObj);
            ws.getIndBilaTrchEstr().setTpCopCasoMor(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setTpCopCasoMor(((short)-1));
        }
    }

    @Override
    public String getTpFrmAssva() {
        return bilaTrchEstr.getB03TpFrmAssva();
    }

    @Override
    public void setTpFrmAssva(String tpFrmAssva) {
        this.bilaTrchEstr.setB03TpFrmAssva(tpFrmAssva);
    }

    @Override
    public String getTpIas() {
        return bilaTrchEstr.getB03TpIas();
    }

    @Override
    public void setTpIas(String tpIas) {
        this.bilaTrchEstr.setB03TpIas(tpIas);
    }

    @Override
    public String getTpIasObj() {
        if (ws.getIndBilaTrchEstr().getTpIas() >= 0) {
            return getTpIas();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpIasObj(String tpIasObj) {
        if (tpIasObj != null) {
            setTpIas(tpIasObj);
            ws.getIndBilaTrchEstr().setTpIas(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setTpIas(((short)-1));
        }
    }

    @Override
    public char getTpPre() {
        return bilaTrchEstr.getB03TpPre();
    }

    @Override
    public void setTpPre(char tpPre) {
        this.bilaTrchEstr.setB03TpPre(tpPre);
    }

    @Override
    public Character getTpPreObj() {
        if (ws.getIndBilaTrchEstr().getTpPre() >= 0) {
            return ((Character)getTpPre());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpPreObj(Character tpPreObj) {
        if (tpPreObj != null) {
            setTpPre(((char)tpPreObj));
            ws.getIndBilaTrchEstr().setTpPre(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setTpPre(((short)-1));
        }
    }

    @Override
    public String getTpPrstz() {
        return bilaTrchEstr.getB03TpPrstz();
    }

    @Override
    public void setTpPrstz(String tpPrstz) {
        this.bilaTrchEstr.setB03TpPrstz(tpPrstz);
    }

    @Override
    public String getTpPrstzObj() {
        if (ws.getIndBilaTrchEstr().getTpPrstz() >= 0) {
            return getTpPrstz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpPrstzObj(String tpPrstzObj) {
        if (tpPrstzObj != null) {
            setTpPrstz(tpPrstzObj);
            ws.getIndBilaTrchEstr().setTpPrstz(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setTpPrstz(((short)-1));
        }
    }

    @Override
    public String getTpRamoBila() {
        return bilaTrchEstr.getB03TpRamoBila();
    }

    @Override
    public void setTpRamoBila(String tpRamoBila) {
        this.bilaTrchEstr.setB03TpRamoBila(tpRamoBila);
    }

    @Override
    public String getTpRgmFisc() {
        return bilaTrchEstr.getB03TpRgmFisc();
    }

    @Override
    public void setTpRgmFisc(String tpRgmFisc) {
        this.bilaTrchEstr.setB03TpRgmFisc(tpRgmFisc);
    }

    @Override
    public String getTpRgmFiscObj() {
        if (ws.getIndBilaTrchEstr().getTpRgmFisc() >= 0) {
            return getTpRgmFisc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpRgmFiscObj(String tpRgmFiscObj) {
        if (tpRgmFiscObj != null) {
            setTpRgmFisc(tpRgmFiscObj);
            ws.getIndBilaTrchEstr().setTpRgmFisc(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setTpRgmFisc(((short)-1));
        }
    }

    @Override
    public String getTpRival() {
        return bilaTrchEstr.getB03TpRival();
    }

    @Override
    public void setTpRival(String tpRival) {
        this.bilaTrchEstr.setB03TpRival(tpRival);
    }

    @Override
    public String getTpRivalObj() {
        if (ws.getIndBilaTrchEstr().getTpRival() >= 0) {
            return getTpRival();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpRivalObj(String tpRivalObj) {
        if (tpRivalObj != null) {
            setTpRival(tpRivalObj);
            ws.getIndBilaTrchEstr().setTpRival(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setTpRival(((short)-1));
        }
    }

    @Override
    public String getTpStatBusAdes() {
        return bilaTrchEstr.getB03TpStatBusAdes();
    }

    @Override
    public void setTpStatBusAdes(String tpStatBusAdes) {
        this.bilaTrchEstr.setB03TpStatBusAdes(tpStatBusAdes);
    }

    @Override
    public String getTpStatBusPoli() {
        return bilaTrchEstr.getB03TpStatBusPoli();
    }

    @Override
    public void setTpStatBusPoli(String tpStatBusPoli) {
        this.bilaTrchEstr.setB03TpStatBusPoli(tpStatBusPoli);
    }

    @Override
    public String getTpStatBusTrch() {
        return bilaTrchEstr.getB03TpStatBusTrch();
    }

    @Override
    public void setTpStatBusTrch(String tpStatBusTrch) {
        this.bilaTrchEstr.setB03TpStatBusTrch(tpStatBusTrch);
    }

    @Override
    public String getTpStatInvst() {
        return bilaTrchEstr.getB03TpStatInvst();
    }

    @Override
    public void setTpStatInvst(String tpStatInvst) {
        this.bilaTrchEstr.setB03TpStatInvst(tpStatInvst);
    }

    @Override
    public String getTpStatInvstObj() {
        if (ws.getIndBilaTrchEstr().getTpStatInvst() >= 0) {
            return getTpStatInvst();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpStatInvstObj(String tpStatInvstObj) {
        if (tpStatInvstObj != null) {
            setTpStatInvst(tpStatInvstObj);
            ws.getIndBilaTrchEstr().setTpStatInvst(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setTpStatInvst(((short)-1));
        }
    }

    @Override
    public String getTpTari() {
        return bilaTrchEstr.getB03TpTari();
    }

    @Override
    public void setTpTari(String tpTari) {
        this.bilaTrchEstr.setB03TpTari(tpTari);
    }

    @Override
    public String getTpTariObj() {
        if (ws.getIndBilaTrchEstr().getTpTari() >= 0) {
            return getTpTari();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpTariObj(String tpTariObj) {
        if (tpTariObj != null) {
            setTpTari(tpTariObj);
            ws.getIndBilaTrchEstr().setTpTari(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setTpTari(((short)-1));
        }
    }

    @Override
    public String getTpTrasf() {
        return bilaTrchEstr.getB03TpTrasf();
    }

    @Override
    public void setTpTrasf(String tpTrasf) {
        this.bilaTrchEstr.setB03TpTrasf(tpTrasf);
    }

    @Override
    public String getTpTrasfObj() {
        if (ws.getIndBilaTrchEstr().getTpTrasf() >= 0) {
            return getTpTrasf();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpTrasfObj(String tpTrasfObj) {
        if (tpTrasfObj != null) {
            setTpTrasf(tpTrasfObj);
            ws.getIndBilaTrchEstr().setTpTrasf(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setTpTrasf(((short)-1));
        }
    }

    @Override
    public String getTpTrch() {
        return bilaTrchEstr.getB03TpTrch();
    }

    @Override
    public void setTpTrch(String tpTrch) {
        this.bilaTrchEstr.setB03TpTrch(tpTrch);
    }

    @Override
    public String getTpTrchObj() {
        if (ws.getIndBilaTrchEstr().getTpTrch() >= 0) {
            return getTpTrch();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpTrchObj(String tpTrchObj) {
        if (tpTrchObj != null) {
            setTpTrch(tpTrchObj);
            ws.getIndBilaTrchEstr().setTpTrch(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setTpTrch(((short)-1));
        }
    }

    @Override
    public String getTpTst() {
        return bilaTrchEstr.getB03TpTst();
    }

    @Override
    public void setTpTst(String tpTst) {
        this.bilaTrchEstr.setB03TpTst(tpTst);
    }

    @Override
    public String getTpTstObj() {
        if (ws.getIndBilaTrchEstr().getTpTst() >= 0) {
            return getTpTst();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpTstObj(String tpTstObj) {
        if (tpTstObj != null) {
            setTpTst(tpTstObj);
            ws.getIndBilaTrchEstr().setTpTst(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setTpTst(((short)-1));
        }
    }

    @Override
    public char getTpVers() {
        return bilaTrchEstr.getB03TpVers();
    }

    @Override
    public void setTpVers(char tpVers) {
        this.bilaTrchEstr.setB03TpVers(tpVers);
    }

    @Override
    public Character getTpVersObj() {
        if (ws.getIndBilaTrchEstr().getTpVers() >= 0) {
            return ((Character)getTpVers());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpVersObj(Character tpVersObj) {
        if (tpVersObj != null) {
            setTpVers(((char)tpVersObj));
            ws.getIndBilaTrchEstr().setTpVers(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setTpVers(((short)-1));
        }
    }

    @Override
    public String getTratRiass() {
        return bilaTrchEstr.getB03TratRiass();
    }

    @Override
    public void setTratRiass(String tratRiass) {
        this.bilaTrchEstr.setB03TratRiass(tratRiass);
    }

    @Override
    public String getTratRiassEcc() {
        return bilaTrchEstr.getB03TratRiassEcc();
    }

    @Override
    public void setTratRiassEcc(String tratRiassEcc) {
        this.bilaTrchEstr.setB03TratRiassEcc(tratRiassEcc);
    }

    @Override
    public String getTratRiassEccObj() {
        if (ws.getIndBilaTrchEstr().getTratRiassEcc() >= 0) {
            return getTratRiassEcc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTratRiassEccObj(String tratRiassEccObj) {
        if (tratRiassEccObj != null) {
            setTratRiassEcc(tratRiassEccObj);
            ws.getIndBilaTrchEstr().setTratRiassEcc(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setTratRiassEcc(((short)-1));
        }
    }

    @Override
    public String getTratRiassObj() {
        if (ws.getIndBilaTrchEstr().getTratRiass() >= 0) {
            return getTratRiass();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTratRiassObj(String tratRiassObj) {
        if (tratRiassObj != null) {
            setTratRiass(tratRiassObj);
            ws.getIndBilaTrchEstr().setTratRiass(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setTratRiass(((short)-1));
        }
    }

    @Override
    public AfDecimal getTsMedio() {
        return bilaTrchEstr.getB03TsMedio().getB03TsMedio();
    }

    @Override
    public void setTsMedio(AfDecimal tsMedio) {
        this.bilaTrchEstr.getB03TsMedio().setB03TsMedio(tsMedio.copy());
    }

    @Override
    public AfDecimal getTsMedioObj() {
        if (ws.getIndBilaTrchEstr().getTsMedio() >= 0) {
            return getTsMedio();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTsMedioObj(AfDecimal tsMedioObj) {
        if (tsMedioObj != null) {
            setTsMedio(new AfDecimal(tsMedioObj, 14, 9));
            ws.getIndBilaTrchEstr().setTsMedio(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setTsMedio(((short)-1));
        }
    }

    @Override
    public AfDecimal getTsNetT() {
        return bilaTrchEstr.getB03TsNetT().getB03TsNetT();
    }

    @Override
    public void setTsNetT(AfDecimal tsNetT) {
        this.bilaTrchEstr.getB03TsNetT().setB03TsNetT(tsNetT.copy());
    }

    @Override
    public AfDecimal getTsNetTObj() {
        if (ws.getIndBilaTrchEstr().getTsNetT() >= 0) {
            return getTsNetT();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTsNetTObj(AfDecimal tsNetTObj) {
        if (tsNetTObj != null) {
            setTsNetT(new AfDecimal(tsNetTObj, 14, 9));
            ws.getIndBilaTrchEstr().setTsNetT(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setTsNetT(((short)-1));
        }
    }

    @Override
    public AfDecimal getTsPp() {
        return bilaTrchEstr.getB03TsPp().getB03TsPp();
    }

    @Override
    public void setTsPp(AfDecimal tsPp) {
        this.bilaTrchEstr.getB03TsPp().setB03TsPp(tsPp.copy());
    }

    @Override
    public AfDecimal getTsPpObj() {
        if (ws.getIndBilaTrchEstr().getTsPp() >= 0) {
            return getTsPp();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTsPpObj(AfDecimal tsPpObj) {
        if (tsPpObj != null) {
            setTsPp(new AfDecimal(tsPpObj, 14, 9));
            ws.getIndBilaTrchEstr().setTsPp(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setTsPp(((short)-1));
        }
    }

    @Override
    public AfDecimal getTsRendtoSppr() {
        return bilaTrchEstr.getB03TsRendtoSppr().getB03TsRendtoSppr();
    }

    @Override
    public void setTsRendtoSppr(AfDecimal tsRendtoSppr) {
        this.bilaTrchEstr.getB03TsRendtoSppr().setB03TsRendtoSppr(tsRendtoSppr.copy());
    }

    @Override
    public AfDecimal getTsRendtoSpprObj() {
        if (ws.getIndBilaTrchEstr().getTsRendtoSppr() >= 0) {
            return getTsRendtoSppr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTsRendtoSpprObj(AfDecimal tsRendtoSpprObj) {
        if (tsRendtoSpprObj != null) {
            setTsRendtoSppr(new AfDecimal(tsRendtoSpprObj, 14, 9));
            ws.getIndBilaTrchEstr().setTsRendtoSppr(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setTsRendtoSppr(((short)-1));
        }
    }

    @Override
    public AfDecimal getTsRendtoT() {
        return bilaTrchEstr.getB03TsRendtoT().getB03TsRendtoT();
    }

    @Override
    public void setTsRendtoT(AfDecimal tsRendtoT) {
        this.bilaTrchEstr.getB03TsRendtoT().setB03TsRendtoT(tsRendtoT.copy());
    }

    @Override
    public AfDecimal getTsRendtoTObj() {
        if (ws.getIndBilaTrchEstr().getTsRendtoT() >= 0) {
            return getTsRendtoT();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTsRendtoTObj(AfDecimal tsRendtoTObj) {
        if (tsRendtoTObj != null) {
            setTsRendtoT(new AfDecimal(tsRendtoTObj, 14, 9));
            ws.getIndBilaTrchEstr().setTsRendtoT(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setTsRendtoT(((short)-1));
        }
    }

    @Override
    public AfDecimal getTsStabPre() {
        return bilaTrchEstr.getB03TsStabPre().getB03TsStabPre();
    }

    @Override
    public void setTsStabPre(AfDecimal tsStabPre) {
        this.bilaTrchEstr.getB03TsStabPre().setB03TsStabPre(tsStabPre.copy());
    }

    @Override
    public AfDecimal getTsStabPreObj() {
        if (ws.getIndBilaTrchEstr().getTsStabPre() >= 0) {
            return getTsStabPre();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTsStabPreObj(AfDecimal tsStabPreObj) {
        if (tsStabPreObj != null) {
            setTsStabPre(new AfDecimal(tsStabPreObj, 14, 9));
            ws.getIndBilaTrchEstr().setTsStabPre(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setTsStabPre(((short)-1));
        }
    }

    @Override
    public AfDecimal getTsTariDov() {
        return bilaTrchEstr.getB03TsTariDov().getB03TsTariDov();
    }

    @Override
    public void setTsTariDov(AfDecimal tsTariDov) {
        this.bilaTrchEstr.getB03TsTariDov().setB03TsTariDov(tsTariDov.copy());
    }

    @Override
    public AfDecimal getTsTariDovObj() {
        if (ws.getIndBilaTrchEstr().getTsTariDov() >= 0) {
            return getTsTariDov();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTsTariDovObj(AfDecimal tsTariDovObj) {
        if (tsTariDovObj != null) {
            setTsTariDov(new AfDecimal(tsTariDovObj, 14, 9));
            ws.getIndBilaTrchEstr().setTsTariDov(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setTsTariDov(((short)-1));
        }
    }

    @Override
    public AfDecimal getTsTariScon() {
        return bilaTrchEstr.getB03TsTariScon().getB03TsTariScon();
    }

    @Override
    public void setTsTariScon(AfDecimal tsTariScon) {
        this.bilaTrchEstr.getB03TsTariScon().setB03TsTariScon(tsTariScon.copy());
    }

    @Override
    public AfDecimal getTsTariSconObj() {
        if (ws.getIndBilaTrchEstr().getTsTariScon() >= 0) {
            return getTsTariScon();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTsTariSconObj(AfDecimal tsTariSconObj) {
        if (tsTariSconObj != null) {
            setTsTariScon(new AfDecimal(tsTariSconObj, 14, 9));
            ws.getIndBilaTrchEstr().setTsTariScon(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setTsTariScon(((short)-1));
        }
    }

    @Override
    public AfDecimal getUltRm() {
        return bilaTrchEstr.getB03UltRm().getB03UltRm();
    }

    @Override
    public void setUltRm(AfDecimal ultRm) {
        this.bilaTrchEstr.getB03UltRm().setB03UltRm(ultRm.copy());
    }

    @Override
    public AfDecimal getUltRmObj() {
        if (ws.getIndBilaTrchEstr().getUltRm() >= 0) {
            return getUltRm();
        }
        else {
            return null;
        }
    }

    @Override
    public void setUltRmObj(AfDecimal ultRmObj) {
        if (ultRmObj != null) {
            setUltRm(new AfDecimal(ultRmObj, 15, 3));
            ws.getIndBilaTrchEstr().setUltRm(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setUltRm(((short)-1));
        }
    }

    @Override
    public String getVlt() {
        return bilaTrchEstr.getB03Vlt();
    }

    @Override
    public void setVlt(String vlt) {
        this.bilaTrchEstr.setB03Vlt(vlt);
    }

    @Override
    public String getVltObj() {
        if (ws.getIndBilaTrchEstr().getVlt() >= 0) {
            return getVlt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setVltObj(String vltObj) {
        if (vltObj != null) {
            setVlt(vltObj);
            ws.getIndBilaTrchEstr().setVlt(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setVlt(((short)-1));
        }
    }

    @Override
    public AfDecimal getcSubrshT() {
        return bilaTrchEstr.getB03CSubrshT().getB03CSubrshT();
    }

    @Override
    public void setcSubrshT(AfDecimal cSubrshT) {
        this.bilaTrchEstr.getB03CSubrshT().setB03CSubrshT(cSubrshT.copy());
    }

    @Override
    public AfDecimal getcSubrshTObj() {
        if (ws.getIndBilaTrchEstr().getcSubrshT() >= 0) {
            return getcSubrshT();
        }
        else {
            return null;
        }
    }

    @Override
    public void setcSubrshTObj(AfDecimal cSubrshTObj) {
        if (cSubrshTObj != null) {
            setcSubrshT(new AfDecimal(cSubrshTObj, 15, 3));
            ws.getIndBilaTrchEstr().setcSubrshT(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setcSubrshT(((short)-1));
        }
    }
}
