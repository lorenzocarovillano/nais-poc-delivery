package it.accenture.jnais;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Idsv0003CampiEsito;
import it.accenture.jnais.copy.Lvvc0000DatiInput2;
import it.accenture.jnais.ws.enums.Idsv0003Sqlcode;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.InputLvvs0000;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.Lvvs0013Data;

/**Original name: LVVS0013<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2007.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *   PROGRAMMA...... LVVS0013
 *   TIPOLOGIA...... SERVIZIO
 *   PROCESSO....... XXX
 *   FUNZIONE....... XXX
 *   DESCRIZIONE.... CONVERSIONE DATA DECORRENZA POLIZZA
 * **------------------------------------------------------------***</pre>*/
public class Lvvs0013 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lvvs0013Data ws = new Lvvs0013Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS0013
    private Ivvc0213 ivvc0213;

    //==== METHODS ====
    /**Original name: PROGRAM_LVVS0013_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: PERFORM S1000-ELABORAZIONE
        //              THRU EX-S1000
        s1000Elaborazione();
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lvvs0013 getInstance() {
        return ((Lvvs0013)Programs.getInstance(Lvvs0013.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                        IX-INDICI
        //                                             IVVC0213-TAB-OUTPUT.
        initIxIndici();
        initTabOutput();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE
        //             TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1000Elaborazione() {
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE AREA-IO-POL
        //                      WK-DATA-OUTPUT
        //                      WK-DATA-X-12.
        initAreaIoPol();
        ws.setWkDataOutput(new AfDecimal(0, 11, 7));
        initWkDataX12();
        //--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
        //--> RISPETTIVE AREE DCLGEN IN WORKING
        // COB_CODE: PERFORM S1100-VALORIZZA-DCLGEN
        //              THRU S1100-VALORIZZA-DCLGEN-EX
        //           VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
        //                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //                   SPACES OR LOW-VALUE OR HIGH-VALUE
        ws.setIxDclgen(((short)1));
        while (!(ws.getIxDclgen() > ivvc0213.getEleInfoMax() || Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias()) || Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()) || Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()))) {
            s1100ValorizzaDclgen();
            ws.setIxDclgen(Trunc.toShort(ws.getIxDclgen() + 1, 4));
        }
        //--> PERFORM DI CONTROLLI SUI I CAMPI CARICATI NELLE
        //--> DCLGEN DI WORKING
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //                  THRU S1200-CONTROLLO-DATI-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM S1200-CONTROLLO-DATI
            //              THRU S1200-CONTROLLO-DATI-EX
            s1200ControlloDati();
        }
        // COB_CODE:      IF  IDSV0003-SUCCESSFUL-RC
        //                AND IDSV0003-SUCCESSFUL-SQL
        //           *--> CALL MODULO PER RECUPERO DATA
        //                    END-IF
        //                END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            //--> CALL MODULO PER RECUPERO DATA
            // COB_CODE: PERFORM S1300-CALL-LVVS0000
            //              THRU S1300-CALL-LVVS0000-EX
            s1300CallLvvs0000();
            // COB_CODE:          IF IDSV0003-SUCCESSFUL-RC
            //                      END-EVALUATE
            //                    ELSE
            //           *-->       GESTIRE ERRORE DISPATCHER
            //                      END-STRING
            //                    END-IF
            if (idsv0003.getReturnCode().isSuccessfulRc()) {
                // COB_CODE:           EVALUATE TRUE
                //                         WHEN IDSV0003-SUCCESSFUL-SQL
                //           *-->          OPERAZIONE ESEGUITA CORRETTAMENTE
                //                                TO IVVC0213-VAL-IMP-O
                //                         WHEN OTHER
                //           *--->         ERRORE DI ACCESSO AL DB
                //                              END-STRING
                //                      END-EVALUATE
                switch (idsv0003.getSqlcode().getSqlcode()) {

                    case Idsv0003Sqlcode.SUCCESSFUL_SQL://-->          OPERAZIONE ESEGUITA CORRETTAMENTE
                        // COB_CODE: MOVE LVVC0000-DATA-OUTPUT
                        //             TO IVVC0213-VAL-IMP-O
                        ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(ws.getInputLvvs0000().getDataOutput(), 18, 7));
                        break;

                    default://--->         ERRORE DI ACCESSO AL DB
                        // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED TO TRUE
                        idsv0003.getReturnCode().setFieldNotValued();
                        // COB_CODE: MOVE WK-PGM
                        //             TO IDSV0003-COD-SERVIZIO-BE
                        idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                        // COB_CODE: STRING IDSV0003-RETURN-CODE  ';'
                        //                  IDSV0003-SQLCODE
                        //           DELIMITED BY SIZE
                        //           INTO IDSV0003-DESCRIZ-ERR-DB2
                        //           END-STRING
                        concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                        idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                        break;
                }
            }
            else {
                //-->       GESTIRE ERRORE DISPATCHER
                // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
                // COB_CODE: MOVE WK-PGM
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: STRING IDSV0003-RETURN-CODE  ';'
                //                  IDSV0003-SQLCODE
                //           DELIMITED BY SIZE
                //           INTO IDSV0003-DESCRIZ-ERR-DB2
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
            }
        }
    }

    /**Original name: S1100-VALORIZZA-DCLGEN<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 *     RISPETTIVE AREE DCLGEN IN WORKING
	 * ----------------------------------------------------------------*</pre>*/
    private void s1100ValorizzaDclgen() {
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-POLI
        //                TO DPOL-AREA-POLIZZA
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias(), ws.getIvvc0218().getAliasPoli())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO DPOL-AREA-POLIZZA
            ws.setDpolAreaPolizzaFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxDclgen()).getLunghezza() - 1));
        }
    }

    /**Original name: S1200-CONTROLLO-DATI<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLO DATI DCLGEN
	 * ----------------------------------------------------------------*</pre>*/
    private void s1200ControlloDati() {
        // COB_CODE: IF DPOL-DT-SCAD NOT NUMERIC
        //                TO IDSV0003-DESCRIZ-ERR-DB2
        //           ELSE
        //              END-IF
        //           END-IF.
        if (!Functions.isNumber(ws.getLccvpol1().getDati().getWpolDtScad().getWpolDtScad())) {
            // COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
            idsv0003.getReturnCode().setFieldNotValued();
            // COB_CODE: MOVE WK-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'DATA-DECORRENZA-POLIZZA NON NUMERICA'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("DATA-DECORRENZA-POLIZZA NON NUMERICA");
        }
        else if (ws.getLccvpol1().getDati().getWpolDtScad().getWpolDtScad() == 0) {
            // COB_CODE: IF DPOL-DT-SCAD = 0
            //              TO IDSV0003-DESCRIZ-ERR-DB2
            //           END-IF
            // COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
            idsv0003.getReturnCode().setFieldNotValued();
            // COB_CODE: MOVE WK-PGM
            //           TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'DATA-DECORRENZA-POLIZZA NON VALORIZZATA'
            //           TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("DATA-DECORRENZA-POLIZZA NON VALORIZZATA");
        }
    }

    /**Original name: S1300-CALL-LVVS0000<br>
	 * <pre>----------------------------------------------------------------*
	 *    CHIAMATA LVVS0000
	 * ----------------------------------------------------------------*
	 * --> COPY LVVC0000</pre>*/
    private void s1300CallLvvs0000() {
        Lvvs0000 lvvs0000 = null;
        // COB_CODE: INITIALIZE INPUT-LVVS0000.
        initInputLvvs0000();
        // COB_CODE: MOVE DPOL-DT-SCAD              TO LVVC0000-DATA-INPUT-1.
        ws.getInputLvvs0000().setDataInput1(TruncAbs.toInt(ws.getLccvpol1().getDati().getWpolDtScad().getWpolDtScad(), 8));
        //--> INIZIALIZZA CODICE DI RITORNO
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC     TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL    TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        //
        // COB_CODE: MOVE 'LVVS0000'                TO WK-CALL-PGM.
        ws.setWkCallPgm("LVVS0000");
        //
        // COB_CODE: CALL WK-CALL-PGM USING      IDSV0003
        //                                       INPUT-LVVS0000.
        lvvs0000 = Lvvs0000.getInstance();
        lvvs0000.run(idsv0003, ws.getInputLvvs0000());
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: MOVE SPACES                     TO IVVC0213-VAL-STR-O.
        ivvc0213.getTabOutput().setValStrO("");
        // COB_CODE: MOVE 0                          TO IVVC0213-VAL-PERC-O.
        ivvc0213.getTabOutput().setValPercO(Trunc.toDecimal(0, 14, 9));
        //
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    public void initIxIndici() {
        ws.setIxDclgen(((short)0));
    }

    public void initTabOutput() {
        ivvc0213.getTabOutput().setCodVariabileO("");
        ivvc0213.getTabOutput().setTpDatoO(Types.SPACE_CHAR);
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        ivvc0213.getTabOutput().setValPercO(new AfDecimal(0, 14, 9));
        ivvc0213.getTabOutput().setValStrO("");
    }

    public void initAreaIoPol() {
        ws.setDpolElePoliMax(((short)0));
        ws.getLccvpol1().getStatus().setStatus(Types.SPACE_CHAR);
        ws.getLccvpol1().setIdPtf(0);
        ws.getLccvpol1().getDati().setWpolIdPoli(0);
        ws.getLccvpol1().getDati().setWpolIdMoviCrz(0);
        ws.getLccvpol1().getDati().getWpolIdMoviChiu().setWpolIdMoviChiu(0);
        ws.getLccvpol1().getDati().setWpolIbOgg("");
        ws.getLccvpol1().getDati().setWpolIbProp("");
        ws.getLccvpol1().getDati().getWpolDtProp().setWpolDtProp(0);
        ws.getLccvpol1().getDati().setWpolDtIniEff(0);
        ws.getLccvpol1().getDati().setWpolDtEndEff(0);
        ws.getLccvpol1().getDati().setWpolCodCompAnia(0);
        ws.getLccvpol1().getDati().setWpolDtDecor(0);
        ws.getLccvpol1().getDati().setWpolDtEmis(0);
        ws.getLccvpol1().getDati().setWpolTpPoli("");
        ws.getLccvpol1().getDati().getWpolDurAa().setWpolDurAa(0);
        ws.getLccvpol1().getDati().getWpolDurMm().setWpolDurMm(0);
        ws.getLccvpol1().getDati().getWpolDtScad().setWpolDtScad(0);
        ws.getLccvpol1().getDati().setWpolCodProd("");
        ws.getLccvpol1().getDati().setWpolDtIniVldtProd(0);
        ws.getLccvpol1().getDati().setWpolCodConv("");
        ws.getLccvpol1().getDati().setWpolCodRamo("");
        ws.getLccvpol1().getDati().getWpolDtIniVldtConv().setWpolDtIniVldtConv(0);
        ws.getLccvpol1().getDati().getWpolDtApplzConv().setWpolDtApplzConv(0);
        ws.getLccvpol1().getDati().setWpolTpFrmAssva("");
        ws.getLccvpol1().getDati().setWpolTpRgmFisc("");
        ws.getLccvpol1().getDati().setWpolFlEstas(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlRshComun(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlRshComunCond(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolTpLivGenzTit("");
        ws.getLccvpol1().getDati().setWpolFlCopFinanz(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolTpApplzDir("");
        ws.getLccvpol1().getDati().getWpolSpeMed().setWpolSpeMed(new AfDecimal(0, 15, 3));
        ws.getLccvpol1().getDati().getWpolDirEmis().setWpolDirEmis(new AfDecimal(0, 15, 3));
        ws.getLccvpol1().getDati().getWpolDir1oVers().setWpolDir1oVers(new AfDecimal(0, 15, 3));
        ws.getLccvpol1().getDati().getWpolDirVersAgg().setWpolDirVersAgg(new AfDecimal(0, 15, 3));
        ws.getLccvpol1().getDati().setWpolCodDvs("");
        ws.getLccvpol1().getDati().setWpolFlFntAz(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlFntAder(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlFntTfr(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlFntVolo(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolTpOpzAScad("");
        ws.getLccvpol1().getDati().getWpolAaDiffProrDflt().setWpolAaDiffProrDflt(0);
        ws.getLccvpol1().getDati().setWpolFlVerProd("");
        ws.getLccvpol1().getDati().getWpolDurGg().setWpolDurGg(0);
        ws.getLccvpol1().getDati().getWpolDirQuiet().setWpolDirQuiet(new AfDecimal(0, 15, 3));
        ws.getLccvpol1().getDati().setWpolTpPtfEstno("");
        ws.getLccvpol1().getDati().setWpolFlCumPreCntr(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlAmmbMovi(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolConvGeco("");
        ws.getLccvpol1().getDati().setWpolDsRiga(0);
        ws.getLccvpol1().getDati().setWpolDsOperSql(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolDsVer(0);
        ws.getLccvpol1().getDati().setWpolDsTsIniCptz(0);
        ws.getLccvpol1().getDati().setWpolDsTsEndCptz(0);
        ws.getLccvpol1().getDati().setWpolDsUtente("");
        ws.getLccvpol1().getDati().setWpolDsStatoElab(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlScudoFisc(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlTrasfe(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlTfrStrc(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().getWpolDtPresc().setWpolDtPresc(0);
        ws.getLccvpol1().getDati().setWpolCodConvAgg("");
        ws.getLccvpol1().getDati().setWpolSubcatProd("");
        ws.getLccvpol1().getDati().setWpolFlQuestAdegzAss(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolCodTpa("");
        ws.getLccvpol1().getDati().getWpolIdAccComm().setWpolIdAccComm(0);
        ws.getLccvpol1().getDati().setWpolFlPoliCpiPr(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlPoliBundling(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolIndPoliPrinColl(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlVndBundle(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolIbBs("");
        ws.getLccvpol1().getDati().setWpolFlPoliIfp(Types.SPACE_CHAR);
    }

    public void initWkDataX12() {
        ws.getWkDataX12().setxAa("");
        ws.getWkDataX12().setVirogla(Types.SPACE_CHAR);
        ws.getWkDataX12().setxGg("");
    }

    public void initInputLvvs0000() {
        ws.getInputLvvs0000().getFormatDate().setFormatDate(Types.SPACE_CHAR);
        ws.getInputLvvs0000().setDataInputFormatted("00000000");
        ws.getInputLvvs0000().setDataInput1Formatted("00000000");
        ws.getInputLvvs0000().getDatiInput2().setLvvc0000AnniInput2Formatted("00000");
        ws.getInputLvvs0000().getDatiInput2().setLvvc0000MesiInput2Formatted("00000");
        ws.getInputLvvs0000().getDatiInput2().setLvvc0000GiorniInput2Formatted("00000");
        ws.getInputLvvs0000().setAnniInput3Formatted("00000");
        ws.getInputLvvs0000().setMesiInput3Formatted("00000");
        ws.getInputLvvs0000().setDataOutput(new AfDecimal(0, 11, 7));
    }
}
