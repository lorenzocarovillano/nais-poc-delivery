package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.DfltAdesDao;
import it.accenture.jnais.commons.data.to.IDfltAdes;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.DfltAdesIdbsdad0;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idbsdad0Data;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.redefines.DadDtDecorDflt;
import it.accenture.jnais.ws.redefines.DadDtScadAdesDflt;
import it.accenture.jnais.ws.redefines.DadDurAaAdesDflt;
import it.accenture.jnais.ws.redefines.DadDurGgAdesDflt;
import it.accenture.jnais.ws.redefines.DadDurMmAdesDflt;
import it.accenture.jnais.ws.redefines.DadEtaScadFemmDflt;
import it.accenture.jnais.ws.redefines.DadEtaScadMascDflt;
import it.accenture.jnais.ws.redefines.DadFrazDflt;
import it.accenture.jnais.ws.redefines.DadImpAder;
import it.accenture.jnais.ws.redefines.DadImpAz;
import it.accenture.jnais.ws.redefines.DadImpPreDflt;
import it.accenture.jnais.ws.redefines.DadImpProvIncDflt;
import it.accenture.jnais.ws.redefines.DadImpTfr;
import it.accenture.jnais.ws.redefines.DadImpVolo;
import it.accenture.jnais.ws.redefines.DadPcAder;
import it.accenture.jnais.ws.redefines.DadPcAz;
import it.accenture.jnais.ws.redefines.DadPcProvIncDflt;
import it.accenture.jnais.ws.redefines.DadPcTfr;
import it.accenture.jnais.ws.redefines.DadPcVolo;

/**Original name: IDBSDAD0<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  04 SET 2008.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Idbsdad0 extends Program implements IDfltAdes {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private DfltAdesDao dfltAdesDao = new DfltAdesDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Idbsdad0Data ws = new Idbsdad0Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: DFLT-ADES
    private DfltAdesIdbsdad0 dfltAdes;

    //==== METHODS ====
    /**Original name: PROGRAM_IDBSDAD0_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, DfltAdesIdbsdad0 dfltAdes) {
        this.idsv0003 = idsv0003;
        this.dfltAdes = dfltAdes;
        // COB_CODE: PERFORM A000-INIZIO                    THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO          THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS          THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO          THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                        a300ElaboraIdEff();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                        a400ElaboraIdpEff();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO          THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS          THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO          THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                        b300ElaboraIdCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                        b400ElaboraIdpCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                        b500ElaboraIboCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                        b600ElaboraIbsCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                        b700ElaboraIdoCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //              SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-PRIMARY-KEY
                //                 PERFORM A200-ELABORA-PK          THRU A200-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO         THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS         THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO         THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.PRIMARY_KEY:// COB_CODE: PERFORM A200-ELABORA-PK          THRU A200-EX
                        a200ElaboraPk();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO         THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS         THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO         THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Idbsdad0 getInstance() {
        return ((Idbsdad0)Programs.getInstance(Idbsdad0.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'IDBSDAD0'   TO IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("IDBSDAD0");
        // COB_CODE: MOVE 'DFLT_ADES' TO IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("DFLT_ADES");
        // COB_CODE: MOVE '00'                     TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                   TO   IDSV0003-SQLCODE
        //                                              IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                              IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-PK<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a200ElaboraPk() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-PK          THRU A210-EX
        //              WHEN IDSV0003-INSERT
        //                 PERFORM A220-INSERT-PK          THRU A220-EX
        //              WHEN IDSV0003-UPDATE
        //                 PERFORM A230-UPDATE-PK          THRU A230-EX
        //              WHEN IDSV0003-DELETE
        //                 PERFORM A240-DELETE-PK          THRU A240-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-PK          THRU A210-EX
            a210SelectPk();
        }
        else if (idsv0003.getOperazione().isInsert()) {
            // COB_CODE: PERFORM A220-INSERT-PK          THRU A220-EX
            a220InsertPk();
        }
        else if (idsv0003.getOperazione().isUpdate()) {
            // COB_CODE: PERFORM A230-UPDATE-PK          THRU A230-EX
            a230UpdatePk();
        }
        else if (idsv0003.getOperazione().isDelete()) {
            // COB_CODE: PERFORM A240-DELETE-PK          THRU A240-EX
            a240DeletePk();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A300-ELABORA-ID-EFF<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void a300ElaboraIdEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A310-SELECT-ID-EFF          THRU A310-EX
            a310SelectIdEff();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A400-ELABORA-IDP-EFF<br>*/
    private void a400ElaboraIdpEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
            a410SelectIdpEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
            a460OpenCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
            a470CloseCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
            a480FetchFirstIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
            a490FetchNextIdpEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A500-ELABORA-IBO<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a500ElaboraIbo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A510-SELECT-IBO             THRU A510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A510-SELECT-IBO             THRU A510-EX
            a510SelectIbo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
            a560OpenCursorIbo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
            a570CloseCursorIbo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
            a580FetchFirstIbo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
            a590FetchNextIbo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A600-ELABORA-IBS<br>*/
    private void a600ElaboraIbs() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A610-SELECT-IBS             THRU A610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A610-SELECT-IBS             THRU A610-EX
            a610SelectIbs();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
            a660OpenCursorIbs();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
            a670CloseCursorIbs();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
            a680FetchFirstIbs();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
            a690FetchNextIbs();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A700-ELABORA-IDO<br>*/
    private void a700ElaboraIdo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A710-SELECT-IDO                 THRU A710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A710-SELECT-IDO                 THRU A710-EX
            a710SelectIdo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
            a760OpenCursorIdo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
            a770CloseCursorIdo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
            a780FetchFirstIdo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
            a790FetchNextIdo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B300-ELABORA-ID-CPZ<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void b300ElaboraIdCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
            b310SelectIdCpz();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B400-ELABORA-IDP-CPZ<br>*/
    private void b400ElaboraIdpCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
            b410SelectIdpCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
            b460OpenCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
            b470CloseCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
            b480FetchFirstIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
            b490FetchNextIdpCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B500-ELABORA-IBO-CPZ<br>*/
    private void b500ElaboraIboCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
            b510SelectIboCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
            b560OpenCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
            b570CloseCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
            b580FetchFirstIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B600-ELABORA-IBS-CPZ<br>*/
    private void b600ElaboraIbsCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
            b610SelectIbsCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
            b660OpenCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
            b670CloseCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
            b680FetchFirstIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B700-ELABORA-IDO-CPZ<br>*/
    private void b700ElaboraIdoCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
            b710SelectIdoCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
            b760OpenCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
            b770CloseCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
            b780FetchFirstIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A210-SELECT-PK<br>
	 * <pre>----
	 * ----  gestione PK
	 * ----</pre>*/
    private void a210SelectPk() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_DFLT_ADES
        //                ,ID_POLI
        //                ,IB_POLI
        //                ,IB_DFLT
        //                ,COD_GAR_1
        //                ,COD_GAR_2
        //                ,COD_GAR_3
        //                ,COD_GAR_4
        //                ,COD_GAR_5
        //                ,COD_GAR_6
        //                ,DT_DECOR_DFLT
        //                ,ETA_SCAD_MASC_DFLT
        //                ,ETA_SCAD_FEMM_DFLT
        //                ,DUR_AA_ADES_DFLT
        //                ,DUR_MM_ADES_DFLT
        //                ,DUR_GG_ADES_DFLT
        //                ,DT_SCAD_ADES_DFLT
        //                ,FRAZ_DFLT
        //                ,PC_PROV_INC_DFLT
        //                ,IMP_PROV_INC_DFLT
        //                ,IMP_AZ
        //                ,IMP_ADER
        //                ,IMP_TFR
        //                ,IMP_VOLO
        //                ,PC_AZ
        //                ,PC_ADER
        //                ,PC_TFR
        //                ,PC_VOLO
        //                ,TP_FNT_CNBTVA
        //                ,IMP_PRE_DFLT
        //                ,COD_COMP_ANIA
        //                ,TP_PRE
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //             INTO
        //                :DAD-ID-DFLT-ADES
        //               ,:DAD-ID-POLI
        //               ,:DAD-IB-POLI
        //                :IND-DAD-IB-POLI
        //               ,:DAD-IB-DFLT
        //                :IND-DAD-IB-DFLT
        //               ,:DAD-COD-GAR-1
        //                :IND-DAD-COD-GAR-1
        //               ,:DAD-COD-GAR-2
        //                :IND-DAD-COD-GAR-2
        //               ,:DAD-COD-GAR-3
        //                :IND-DAD-COD-GAR-3
        //               ,:DAD-COD-GAR-4
        //                :IND-DAD-COD-GAR-4
        //               ,:DAD-COD-GAR-5
        //                :IND-DAD-COD-GAR-5
        //               ,:DAD-COD-GAR-6
        //                :IND-DAD-COD-GAR-6
        //               ,:DAD-DT-DECOR-DFLT-DB
        //                :IND-DAD-DT-DECOR-DFLT
        //               ,:DAD-ETA-SCAD-MASC-DFLT
        //                :IND-DAD-ETA-SCAD-MASC-DFLT
        //               ,:DAD-ETA-SCAD-FEMM-DFLT
        //                :IND-DAD-ETA-SCAD-FEMM-DFLT
        //               ,:DAD-DUR-AA-ADES-DFLT
        //                :IND-DAD-DUR-AA-ADES-DFLT
        //               ,:DAD-DUR-MM-ADES-DFLT
        //                :IND-DAD-DUR-MM-ADES-DFLT
        //               ,:DAD-DUR-GG-ADES-DFLT
        //                :IND-DAD-DUR-GG-ADES-DFLT
        //               ,:DAD-DT-SCAD-ADES-DFLT-DB
        //                :IND-DAD-DT-SCAD-ADES-DFLT
        //               ,:DAD-FRAZ-DFLT
        //                :IND-DAD-FRAZ-DFLT
        //               ,:DAD-PC-PROV-INC-DFLT
        //                :IND-DAD-PC-PROV-INC-DFLT
        //               ,:DAD-IMP-PROV-INC-DFLT
        //                :IND-DAD-IMP-PROV-INC-DFLT
        //               ,:DAD-IMP-AZ
        //                :IND-DAD-IMP-AZ
        //               ,:DAD-IMP-ADER
        //                :IND-DAD-IMP-ADER
        //               ,:DAD-IMP-TFR
        //                :IND-DAD-IMP-TFR
        //               ,:DAD-IMP-VOLO
        //                :IND-DAD-IMP-VOLO
        //               ,:DAD-PC-AZ
        //                :IND-DAD-PC-AZ
        //               ,:DAD-PC-ADER
        //                :IND-DAD-PC-ADER
        //               ,:DAD-PC-TFR
        //                :IND-DAD-PC-TFR
        //               ,:DAD-PC-VOLO
        //                :IND-DAD-PC-VOLO
        //               ,:DAD-TP-FNT-CNBTVA
        //                :IND-DAD-TP-FNT-CNBTVA
        //               ,:DAD-IMP-PRE-DFLT
        //                :IND-DAD-IMP-PRE-DFLT
        //               ,:DAD-COD-COMP-ANIA
        //               ,:DAD-TP-PRE
        //                :IND-DAD-TP-PRE
        //               ,:DAD-DS-OPER-SQL
        //               ,:DAD-DS-VER
        //               ,:DAD-DS-TS-CPTZ
        //               ,:DAD-DS-UTENTE
        //               ,:DAD-DS-STATO-ELAB
        //             FROM DFLT_ADES
        //             WHERE     ID_DFLT_ADES = :DAD-ID-DFLT-ADES
        //           END-EXEC.
        dfltAdesDao.selectByDadIdDfltAdes(dfltAdes.getDadIdDfltAdes(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A220-INSERT-PK<br>*/
    private void a220InsertPk() {
        // COB_CODE: PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.
        z400SeqRiga();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX
            z150ValorizzaDataServicesI();
            // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX
            z200SetIndicatoriNull();
            // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX
            z900ConvertiNToX();
            // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX
            z960LengthVchar();
            // COB_CODE: EXEC SQL
            //              INSERT
            //              INTO DFLT_ADES
            //                  (
            //                     ID_DFLT_ADES
            //                    ,ID_POLI
            //                    ,IB_POLI
            //                    ,IB_DFLT
            //                    ,COD_GAR_1
            //                    ,COD_GAR_2
            //                    ,COD_GAR_3
            //                    ,COD_GAR_4
            //                    ,COD_GAR_5
            //                    ,COD_GAR_6
            //                    ,DT_DECOR_DFLT
            //                    ,ETA_SCAD_MASC_DFLT
            //                    ,ETA_SCAD_FEMM_DFLT
            //                    ,DUR_AA_ADES_DFLT
            //                    ,DUR_MM_ADES_DFLT
            //                    ,DUR_GG_ADES_DFLT
            //                    ,DT_SCAD_ADES_DFLT
            //                    ,FRAZ_DFLT
            //                    ,PC_PROV_INC_DFLT
            //                    ,IMP_PROV_INC_DFLT
            //                    ,IMP_AZ
            //                    ,IMP_ADER
            //                    ,IMP_TFR
            //                    ,IMP_VOLO
            //                    ,PC_AZ
            //                    ,PC_ADER
            //                    ,PC_TFR
            //                    ,PC_VOLO
            //                    ,TP_FNT_CNBTVA
            //                    ,IMP_PRE_DFLT
            //                    ,COD_COMP_ANIA
            //                    ,TP_PRE
            //                    ,DS_OPER_SQL
            //                    ,DS_VER
            //                    ,DS_TS_CPTZ
            //                    ,DS_UTENTE
            //                    ,DS_STATO_ELAB
            //                  )
            //              VALUES
            //                  (
            //                    :DAD-ID-DFLT-ADES
            //                    ,:DAD-ID-POLI
            //                    ,:DAD-IB-POLI
            //                     :IND-DAD-IB-POLI
            //                    ,:DAD-IB-DFLT
            //                     :IND-DAD-IB-DFLT
            //                    ,:DAD-COD-GAR-1
            //                     :IND-DAD-COD-GAR-1
            //                    ,:DAD-COD-GAR-2
            //                     :IND-DAD-COD-GAR-2
            //                    ,:DAD-COD-GAR-3
            //                     :IND-DAD-COD-GAR-3
            //                    ,:DAD-COD-GAR-4
            //                     :IND-DAD-COD-GAR-4
            //                    ,:DAD-COD-GAR-5
            //                     :IND-DAD-COD-GAR-5
            //                    ,:DAD-COD-GAR-6
            //                     :IND-DAD-COD-GAR-6
            //                    ,:DAD-DT-DECOR-DFLT-DB
            //                     :IND-DAD-DT-DECOR-DFLT
            //                    ,:DAD-ETA-SCAD-MASC-DFLT
            //                     :IND-DAD-ETA-SCAD-MASC-DFLT
            //                    ,:DAD-ETA-SCAD-FEMM-DFLT
            //                     :IND-DAD-ETA-SCAD-FEMM-DFLT
            //                    ,:DAD-DUR-AA-ADES-DFLT
            //                     :IND-DAD-DUR-AA-ADES-DFLT
            //                    ,:DAD-DUR-MM-ADES-DFLT
            //                     :IND-DAD-DUR-MM-ADES-DFLT
            //                    ,:DAD-DUR-GG-ADES-DFLT
            //                     :IND-DAD-DUR-GG-ADES-DFLT
            //                    ,:DAD-DT-SCAD-ADES-DFLT-DB
            //                     :IND-DAD-DT-SCAD-ADES-DFLT
            //                    ,:DAD-FRAZ-DFLT
            //                     :IND-DAD-FRAZ-DFLT
            //                    ,:DAD-PC-PROV-INC-DFLT
            //                     :IND-DAD-PC-PROV-INC-DFLT
            //                    ,:DAD-IMP-PROV-INC-DFLT
            //                     :IND-DAD-IMP-PROV-INC-DFLT
            //                    ,:DAD-IMP-AZ
            //                     :IND-DAD-IMP-AZ
            //                    ,:DAD-IMP-ADER
            //                     :IND-DAD-IMP-ADER
            //                    ,:DAD-IMP-TFR
            //                     :IND-DAD-IMP-TFR
            //                    ,:DAD-IMP-VOLO
            //                     :IND-DAD-IMP-VOLO
            //                    ,:DAD-PC-AZ
            //                     :IND-DAD-PC-AZ
            //                    ,:DAD-PC-ADER
            //                     :IND-DAD-PC-ADER
            //                    ,:DAD-PC-TFR
            //                     :IND-DAD-PC-TFR
            //                    ,:DAD-PC-VOLO
            //                     :IND-DAD-PC-VOLO
            //                    ,:DAD-TP-FNT-CNBTVA
            //                     :IND-DAD-TP-FNT-CNBTVA
            //                    ,:DAD-IMP-PRE-DFLT
            //                     :IND-DAD-IMP-PRE-DFLT
            //                    ,:DAD-COD-COMP-ANIA
            //                    ,:DAD-TP-PRE
            //                     :IND-DAD-TP-PRE
            //                    ,:DAD-DS-OPER-SQL
            //                    ,:DAD-DS-VER
            //                    ,:DAD-DS-TS-CPTZ
            //                    ,:DAD-DS-UTENTE
            //                    ,:DAD-DS-STATO-ELAB
            //                  )
            //           END-EXEC
            dfltAdesDao.insertRec(this);
            // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
            a100CheckReturnCode();
        }
    }

    /**Original name: A230-UPDATE-PK<br>*/
    private void a230UpdatePk() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE DFLT_ADES SET
        //                   ID_DFLT_ADES           =
        //                :DAD-ID-DFLT-ADES
        //                  ,ID_POLI                =
        //                :DAD-ID-POLI
        //                  ,IB_POLI                =
        //                :DAD-IB-POLI
        //                                       :IND-DAD-IB-POLI
        //                  ,IB_DFLT                =
        //                :DAD-IB-DFLT
        //                                       :IND-DAD-IB-DFLT
        //                  ,COD_GAR_1              =
        //                :DAD-COD-GAR-1
        //                                       :IND-DAD-COD-GAR-1
        //                  ,COD_GAR_2              =
        //                :DAD-COD-GAR-2
        //                                       :IND-DAD-COD-GAR-2
        //                  ,COD_GAR_3              =
        //                :DAD-COD-GAR-3
        //                                       :IND-DAD-COD-GAR-3
        //                  ,COD_GAR_4              =
        //                :DAD-COD-GAR-4
        //                                       :IND-DAD-COD-GAR-4
        //                  ,COD_GAR_5              =
        //                :DAD-COD-GAR-5
        //                                       :IND-DAD-COD-GAR-5
        //                  ,COD_GAR_6              =
        //                :DAD-COD-GAR-6
        //                                       :IND-DAD-COD-GAR-6
        //                  ,DT_DECOR_DFLT          =
        //           :DAD-DT-DECOR-DFLT-DB
        //                                       :IND-DAD-DT-DECOR-DFLT
        //                  ,ETA_SCAD_MASC_DFLT     =
        //                :DAD-ETA-SCAD-MASC-DFLT
        //                                       :IND-DAD-ETA-SCAD-MASC-DFLT
        //                  ,ETA_SCAD_FEMM_DFLT     =
        //                :DAD-ETA-SCAD-FEMM-DFLT
        //                                       :IND-DAD-ETA-SCAD-FEMM-DFLT
        //                  ,DUR_AA_ADES_DFLT       =
        //                :DAD-DUR-AA-ADES-DFLT
        //                                       :IND-DAD-DUR-AA-ADES-DFLT
        //                  ,DUR_MM_ADES_DFLT       =
        //                :DAD-DUR-MM-ADES-DFLT
        //                                       :IND-DAD-DUR-MM-ADES-DFLT
        //                  ,DUR_GG_ADES_DFLT       =
        //                :DAD-DUR-GG-ADES-DFLT
        //                                       :IND-DAD-DUR-GG-ADES-DFLT
        //                  ,DT_SCAD_ADES_DFLT      =
        //           :DAD-DT-SCAD-ADES-DFLT-DB
        //                                       :IND-DAD-DT-SCAD-ADES-DFLT
        //                  ,FRAZ_DFLT              =
        //                :DAD-FRAZ-DFLT
        //                                       :IND-DAD-FRAZ-DFLT
        //                  ,PC_PROV_INC_DFLT       =
        //                :DAD-PC-PROV-INC-DFLT
        //                                       :IND-DAD-PC-PROV-INC-DFLT
        //                  ,IMP_PROV_INC_DFLT      =
        //                :DAD-IMP-PROV-INC-DFLT
        //                                       :IND-DAD-IMP-PROV-INC-DFLT
        //                  ,IMP_AZ                 =
        //                :DAD-IMP-AZ
        //                                       :IND-DAD-IMP-AZ
        //                  ,IMP_ADER               =
        //                :DAD-IMP-ADER
        //                                       :IND-DAD-IMP-ADER
        //                  ,IMP_TFR                =
        //                :DAD-IMP-TFR
        //                                       :IND-DAD-IMP-TFR
        //                  ,IMP_VOLO               =
        //                :DAD-IMP-VOLO
        //                                       :IND-DAD-IMP-VOLO
        //                  ,PC_AZ                  =
        //                :DAD-PC-AZ
        //                                       :IND-DAD-PC-AZ
        //                  ,PC_ADER                =
        //                :DAD-PC-ADER
        //                                       :IND-DAD-PC-ADER
        //                  ,PC_TFR                 =
        //                :DAD-PC-TFR
        //                                       :IND-DAD-PC-TFR
        //                  ,PC_VOLO                =
        //                :DAD-PC-VOLO
        //                                       :IND-DAD-PC-VOLO
        //                  ,TP_FNT_CNBTVA          =
        //                :DAD-TP-FNT-CNBTVA
        //                                       :IND-DAD-TP-FNT-CNBTVA
        //                  ,IMP_PRE_DFLT           =
        //                :DAD-IMP-PRE-DFLT
        //                                       :IND-DAD-IMP-PRE-DFLT
        //                  ,COD_COMP_ANIA          =
        //                :DAD-COD-COMP-ANIA
        //                  ,TP_PRE                 =
        //                :DAD-TP-PRE
        //                                       :IND-DAD-TP-PRE
        //                  ,DS_OPER_SQL            =
        //                :DAD-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :DAD-DS-VER
        //                  ,DS_TS_CPTZ             =
        //                :DAD-DS-TS-CPTZ
        //                  ,DS_UTENTE              =
        //                :DAD-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :DAD-DS-STATO-ELAB
        //                WHERE     ID_DFLT_ADES = :DAD-ID-DFLT-ADES
        //           END-EXEC.
        dfltAdesDao.updateRec(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A240-DELETE-PK<br>*/
    private void a240DeletePk() {
        // COB_CODE: EXEC SQL
        //                DELETE
        //                FROM DFLT_ADES
        //                WHERE     ID_DFLT_ADES = :DAD-ID-DFLT-ADES
        //           END-EXEC.
        dfltAdesDao.deleteByDadIdDfltAdes(dfltAdes.getDadIdDfltAdes());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A310-SELECT-ID-EFF<br>*/
    private void a310SelectIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A405-DECLARE-CURSOR-IDP-EFF<br>
	 * <pre>----
	 * ----  gestione IDP Effetto
	 * ----</pre>*/
    private void a405DeclareCursorIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A410-SELECT-IDP-EFF<br>*/
    private void a410SelectIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A460-OPEN-CURSOR-IDP-EFF<br>*/
    private void a460OpenCursorIdpEff() {
        // COB_CODE: PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.
        a405DeclareCursorIdpEff();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A470-CLOSE-CURSOR-IDP-EFF<br>*/
    private void a470CloseCursorIdpEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A480-FETCH-FIRST-IDP-EFF<br>*/
    private void a480FetchFirstIdpEff() {
        // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.
        a460OpenCursorIdpEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
            a490FetchNextIdpEff();
        }
    }

    /**Original name: A490-FETCH-NEXT-IDP-EFF<br>*/
    private void a490FetchNextIdpEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A505-DECLARE-CURSOR-IBO<br>
	 * <pre>----
	 * ----  gestione IBO Effetto e non
	 * ----</pre>*/
    private void a505DeclareCursorIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A510-SELECT-IBO<br>*/
    private void a510SelectIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A560-OPEN-CURSOR-IBO<br>*/
    private void a560OpenCursorIbo() {
        // COB_CODE: PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.
        a505DeclareCursorIbo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A570-CLOSE-CURSOR-IBO<br>*/
    private void a570CloseCursorIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A580-FETCH-FIRST-IBO<br>*/
    private void a580FetchFirstIbo() {
        // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.
        a560OpenCursorIbo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
            a590FetchNextIbo();
        }
    }

    /**Original name: A590-FETCH-NEXT-IBO<br>*/
    private void a590FetchNextIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A605-DECLARE-CURSOR-IBS<br>
	 * <pre>----
	 * ----  gestione IBS Effetto e non
	 * ----</pre>*/
    private void a605DeclareCursorIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A610-SELECT-IBS<br>*/
    private void a610SelectIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A660-OPEN-CURSOR-IBS<br>*/
    private void a660OpenCursorIbs() {
        // COB_CODE: PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.
        a605DeclareCursorIbs();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A670-CLOSE-CURSOR-IBS<br>*/
    private void a670CloseCursorIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A680-FETCH-FIRST-IBS<br>*/
    private void a680FetchFirstIbs() {
        // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.
        a660OpenCursorIbs();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
            a690FetchNextIbs();
        }
    }

    /**Original name: A690-FETCH-NEXT-IBS<br>*/
    private void a690FetchNextIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A705-DECLARE-CURSOR-IDO<br>
	 * <pre>----
	 * ----  gestione IDO Effetto e non
	 * ----</pre>*/
    private void a705DeclareCursorIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A710-SELECT-IDO<br>*/
    private void a710SelectIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A760-OPEN-CURSOR-IDO<br>*/
    private void a760OpenCursorIdo() {
        // COB_CODE: PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.
        a705DeclareCursorIdo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A770-CLOSE-CURSOR-IDO<br>*/
    private void a770CloseCursorIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A780-FETCH-FIRST-IDO<br>*/
    private void a780FetchFirstIdo() {
        // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.
        a760OpenCursorIdo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
            a790FetchNextIdo();
        }
    }

    /**Original name: A790-FETCH-NEXT-IDO<br>*/
    private void a790FetchNextIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B310-SELECT-ID-CPZ<br>
	 * <pre>----
	 * ----  gestione ID Competenza
	 * ----</pre>*/
    private void b310SelectIdCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B405-DECLARE-CURSOR-IDP-CPZ<br>
	 * <pre>----
	 * ----  gestione IDP Competenza
	 * ----</pre>*/
    private void b405DeclareCursorIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B410-SELECT-IDP-CPZ<br>*/
    private void b410SelectIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B460-OPEN-CURSOR-IDP-CPZ<br>*/
    private void b460OpenCursorIdpCpz() {
        // COB_CODE: PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.
        b405DeclareCursorIdpCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B470-CLOSE-CURSOR-IDP-CPZ<br>*/
    private void b470CloseCursorIdpCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B480-FETCH-FIRST-IDP-CPZ<br>*/
    private void b480FetchFirstIdpCpz() {
        // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.
        b460OpenCursorIdpCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
            b490FetchNextIdpCpz();
        }
    }

    /**Original name: B490-FETCH-NEXT-IDP-CPZ<br>*/
    private void b490FetchNextIdpCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B505-DECLARE-CURSOR-IBO-CPZ<br>
	 * <pre>----
	 * ----  gestione IBO Competenza
	 * ----</pre>*/
    private void b505DeclareCursorIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B510-SELECT-IBO-CPZ<br>*/
    private void b510SelectIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B560-OPEN-CURSOR-IBO-CPZ<br>*/
    private void b560OpenCursorIboCpz() {
        // COB_CODE: PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.
        b505DeclareCursorIboCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B570-CLOSE-CURSOR-IBO-CPZ<br>*/
    private void b570CloseCursorIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B580-FETCH-FIRST-IBO-CPZ<br>*/
    private void b580FetchFirstIboCpz() {
        // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.
        b560OpenCursorIboCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
    }

    /**Original name: B590-FETCH-NEXT-IBO-CPZ<br>*/
    private void b590FetchNextIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B605-DECLARE-CURSOR-IBS-CPZ<br>
	 * <pre>----
	 * ----  gestione IBS Competenza
	 * ----</pre>*/
    private void b605DeclareCursorIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B610-SELECT-IBS-CPZ<br>*/
    private void b610SelectIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B660-OPEN-CURSOR-IBS-CPZ<br>*/
    private void b660OpenCursorIbsCpz() {
        // COB_CODE: PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.
        b605DeclareCursorIbsCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B670-CLOSE-CURSOR-IBS-CPZ<br>*/
    private void b670CloseCursorIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B680-FETCH-FIRST-IBS-CPZ<br>*/
    private void b680FetchFirstIbsCpz() {
        // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.
        b660OpenCursorIbsCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
    }

    /**Original name: B690-FETCH-NEXT-IBS-CPZ<br>*/
    private void b690FetchNextIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B705-DECLARE-CURSOR-IDO-CPZ<br>
	 * <pre>----
	 * ----  gestione IDO Competenza
	 * ----</pre>*/
    private void b705DeclareCursorIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B710-SELECT-IDO-CPZ<br>*/
    private void b710SelectIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B760-OPEN-CURSOR-IDO-CPZ<br>*/
    private void b760OpenCursorIdoCpz() {
        // COB_CODE: PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.
        b705DeclareCursorIdoCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B770-CLOSE-CURSOR-IDO-CPZ<br>*/
    private void b770CloseCursorIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B780-FETCH-FIRST-IDO-CPZ<br>*/
    private void b780FetchFirstIdoCpz() {
        // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.
        b760OpenCursorIdoCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
    }

    /**Original name: B790-FETCH-NEXT-IDO-CPZ<br>*/
    private void b790FetchNextIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-DAD-IB-POLI = -1
        //              MOVE HIGH-VALUES TO DAD-IB-POLI-NULL
        //           END-IF
        if (ws.getIndDfltAdes().getIbPoli() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DAD-IB-POLI-NULL
            dfltAdes.setDadIbPoli(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfltAdesIdbsdad0.Len.DAD_IB_POLI));
        }
        // COB_CODE: IF IND-DAD-IB-DFLT = -1
        //              MOVE HIGH-VALUES TO DAD-IB-DFLT-NULL
        //           END-IF
        if (ws.getIndDfltAdes().getIbDflt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DAD-IB-DFLT-NULL
            dfltAdes.setDadIbDflt(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfltAdesIdbsdad0.Len.DAD_IB_DFLT));
        }
        // COB_CODE: IF IND-DAD-COD-GAR-1 = -1
        //              MOVE HIGH-VALUES TO DAD-COD-GAR-1-NULL
        //           END-IF
        if (ws.getIndDfltAdes().getCodGar1() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DAD-COD-GAR-1-NULL
            dfltAdes.setDadCodGar1(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfltAdesIdbsdad0.Len.DAD_COD_GAR1));
        }
        // COB_CODE: IF IND-DAD-COD-GAR-2 = -1
        //              MOVE HIGH-VALUES TO DAD-COD-GAR-2-NULL
        //           END-IF
        if (ws.getIndDfltAdes().getCodGar2() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DAD-COD-GAR-2-NULL
            dfltAdes.setDadCodGar2(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfltAdesIdbsdad0.Len.DAD_COD_GAR2));
        }
        // COB_CODE: IF IND-DAD-COD-GAR-3 = -1
        //              MOVE HIGH-VALUES TO DAD-COD-GAR-3-NULL
        //           END-IF
        if (ws.getIndDfltAdes().getCodGar3() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DAD-COD-GAR-3-NULL
            dfltAdes.setDadCodGar3(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfltAdesIdbsdad0.Len.DAD_COD_GAR3));
        }
        // COB_CODE: IF IND-DAD-COD-GAR-4 = -1
        //              MOVE HIGH-VALUES TO DAD-COD-GAR-4-NULL
        //           END-IF
        if (ws.getIndDfltAdes().getCodGar4() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DAD-COD-GAR-4-NULL
            dfltAdes.setDadCodGar4(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfltAdesIdbsdad0.Len.DAD_COD_GAR4));
        }
        // COB_CODE: IF IND-DAD-COD-GAR-5 = -1
        //              MOVE HIGH-VALUES TO DAD-COD-GAR-5-NULL
        //           END-IF
        if (ws.getIndDfltAdes().getCodGar5() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DAD-COD-GAR-5-NULL
            dfltAdes.setDadCodGar5(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfltAdesIdbsdad0.Len.DAD_COD_GAR5));
        }
        // COB_CODE: IF IND-DAD-COD-GAR-6 = -1
        //              MOVE HIGH-VALUES TO DAD-COD-GAR-6-NULL
        //           END-IF
        if (ws.getIndDfltAdes().getCodGar6() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DAD-COD-GAR-6-NULL
            dfltAdes.setDadCodGar6(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfltAdesIdbsdad0.Len.DAD_COD_GAR6));
        }
        // COB_CODE: IF IND-DAD-DT-DECOR-DFLT = -1
        //              MOVE HIGH-VALUES TO DAD-DT-DECOR-DFLT-NULL
        //           END-IF
        if (ws.getIndDfltAdes().getDtDecorDflt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DAD-DT-DECOR-DFLT-NULL
            dfltAdes.getDadDtDecorDflt().setDadDtDecorDfltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DadDtDecorDflt.Len.DAD_DT_DECOR_DFLT_NULL));
        }
        // COB_CODE: IF IND-DAD-ETA-SCAD-MASC-DFLT = -1
        //              MOVE HIGH-VALUES TO DAD-ETA-SCAD-MASC-DFLT-NULL
        //           END-IF
        if (ws.getIndDfltAdes().getEtaScadMascDflt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DAD-ETA-SCAD-MASC-DFLT-NULL
            dfltAdes.getDadEtaScadMascDflt().setDadEtaScadMascDfltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DadEtaScadMascDflt.Len.DAD_ETA_SCAD_MASC_DFLT_NULL));
        }
        // COB_CODE: IF IND-DAD-ETA-SCAD-FEMM-DFLT = -1
        //              MOVE HIGH-VALUES TO DAD-ETA-SCAD-FEMM-DFLT-NULL
        //           END-IF
        if (ws.getIndDfltAdes().getEtaScadFemmDflt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DAD-ETA-SCAD-FEMM-DFLT-NULL
            dfltAdes.getDadEtaScadFemmDflt().setDadEtaScadFemmDfltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DadEtaScadFemmDflt.Len.DAD_ETA_SCAD_FEMM_DFLT_NULL));
        }
        // COB_CODE: IF IND-DAD-DUR-AA-ADES-DFLT = -1
        //              MOVE HIGH-VALUES TO DAD-DUR-AA-ADES-DFLT-NULL
        //           END-IF
        if (ws.getIndDfltAdes().getDurAaAdesDflt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DAD-DUR-AA-ADES-DFLT-NULL
            dfltAdes.getDadDurAaAdesDflt().setDadDurAaAdesDfltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DadDurAaAdesDflt.Len.DAD_DUR_AA_ADES_DFLT_NULL));
        }
        // COB_CODE: IF IND-DAD-DUR-MM-ADES-DFLT = -1
        //              MOVE HIGH-VALUES TO DAD-DUR-MM-ADES-DFLT-NULL
        //           END-IF
        if (ws.getIndDfltAdes().getDurMmAdesDflt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DAD-DUR-MM-ADES-DFLT-NULL
            dfltAdes.getDadDurMmAdesDflt().setDadDurMmAdesDfltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DadDurMmAdesDflt.Len.DAD_DUR_MM_ADES_DFLT_NULL));
        }
        // COB_CODE: IF IND-DAD-DUR-GG-ADES-DFLT = -1
        //              MOVE HIGH-VALUES TO DAD-DUR-GG-ADES-DFLT-NULL
        //           END-IF
        if (ws.getIndDfltAdes().getDurGgAdesDflt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DAD-DUR-GG-ADES-DFLT-NULL
            dfltAdes.getDadDurGgAdesDflt().setDadDurGgAdesDfltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DadDurGgAdesDflt.Len.DAD_DUR_GG_ADES_DFLT_NULL));
        }
        // COB_CODE: IF IND-DAD-DT-SCAD-ADES-DFLT = -1
        //              MOVE HIGH-VALUES TO DAD-DT-SCAD-ADES-DFLT-NULL
        //           END-IF
        if (ws.getIndDfltAdes().getDtScadAdesDflt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DAD-DT-SCAD-ADES-DFLT-NULL
            dfltAdes.getDadDtScadAdesDflt().setDadDtScadAdesDfltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DadDtScadAdesDflt.Len.DAD_DT_SCAD_ADES_DFLT_NULL));
        }
        // COB_CODE: IF IND-DAD-FRAZ-DFLT = -1
        //              MOVE HIGH-VALUES TO DAD-FRAZ-DFLT-NULL
        //           END-IF
        if (ws.getIndDfltAdes().getFrazDflt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DAD-FRAZ-DFLT-NULL
            dfltAdes.getDadFrazDflt().setDadFrazDfltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DadFrazDflt.Len.DAD_FRAZ_DFLT_NULL));
        }
        // COB_CODE: IF IND-DAD-PC-PROV-INC-DFLT = -1
        //              MOVE HIGH-VALUES TO DAD-PC-PROV-INC-DFLT-NULL
        //           END-IF
        if (ws.getIndDfltAdes().getPcProvIncDflt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DAD-PC-PROV-INC-DFLT-NULL
            dfltAdes.getDadPcProvIncDflt().setDadPcProvIncDfltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DadPcProvIncDflt.Len.DAD_PC_PROV_INC_DFLT_NULL));
        }
        // COB_CODE: IF IND-DAD-IMP-PROV-INC-DFLT = -1
        //              MOVE HIGH-VALUES TO DAD-IMP-PROV-INC-DFLT-NULL
        //           END-IF
        if (ws.getIndDfltAdes().getImpProvIncDflt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DAD-IMP-PROV-INC-DFLT-NULL
            dfltAdes.getDadImpProvIncDflt().setDadImpProvIncDfltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DadImpProvIncDflt.Len.DAD_IMP_PROV_INC_DFLT_NULL));
        }
        // COB_CODE: IF IND-DAD-IMP-AZ = -1
        //              MOVE HIGH-VALUES TO DAD-IMP-AZ-NULL
        //           END-IF
        if (ws.getIndDfltAdes().getImpAz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DAD-IMP-AZ-NULL
            dfltAdes.getDadImpAz().setDadImpAzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DadImpAz.Len.DAD_IMP_AZ_NULL));
        }
        // COB_CODE: IF IND-DAD-IMP-ADER = -1
        //              MOVE HIGH-VALUES TO DAD-IMP-ADER-NULL
        //           END-IF
        if (ws.getIndDfltAdes().getImpAder() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DAD-IMP-ADER-NULL
            dfltAdes.getDadImpAder().setDadImpAderNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DadImpAder.Len.DAD_IMP_ADER_NULL));
        }
        // COB_CODE: IF IND-DAD-IMP-TFR = -1
        //              MOVE HIGH-VALUES TO DAD-IMP-TFR-NULL
        //           END-IF
        if (ws.getIndDfltAdes().getImpTfr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DAD-IMP-TFR-NULL
            dfltAdes.getDadImpTfr().setDadImpTfrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DadImpTfr.Len.DAD_IMP_TFR_NULL));
        }
        // COB_CODE: IF IND-DAD-IMP-VOLO = -1
        //              MOVE HIGH-VALUES TO DAD-IMP-VOLO-NULL
        //           END-IF
        if (ws.getIndDfltAdes().getImpVolo() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DAD-IMP-VOLO-NULL
            dfltAdes.getDadImpVolo().setDadImpVoloNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DadImpVolo.Len.DAD_IMP_VOLO_NULL));
        }
        // COB_CODE: IF IND-DAD-PC-AZ = -1
        //              MOVE HIGH-VALUES TO DAD-PC-AZ-NULL
        //           END-IF
        if (ws.getIndDfltAdes().getPcAz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DAD-PC-AZ-NULL
            dfltAdes.getDadPcAz().setDadPcAzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DadPcAz.Len.DAD_PC_AZ_NULL));
        }
        // COB_CODE: IF IND-DAD-PC-ADER = -1
        //              MOVE HIGH-VALUES TO DAD-PC-ADER-NULL
        //           END-IF
        if (ws.getIndDfltAdes().getPcAder() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DAD-PC-ADER-NULL
            dfltAdes.getDadPcAder().setDadPcAderNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DadPcAder.Len.DAD_PC_ADER_NULL));
        }
        // COB_CODE: IF IND-DAD-PC-TFR = -1
        //              MOVE HIGH-VALUES TO DAD-PC-TFR-NULL
        //           END-IF
        if (ws.getIndDfltAdes().getPcTfr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DAD-PC-TFR-NULL
            dfltAdes.getDadPcTfr().setDadPcTfrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DadPcTfr.Len.DAD_PC_TFR_NULL));
        }
        // COB_CODE: IF IND-DAD-PC-VOLO = -1
        //              MOVE HIGH-VALUES TO DAD-PC-VOLO-NULL
        //           END-IF
        if (ws.getIndDfltAdes().getPcVolo() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DAD-PC-VOLO-NULL
            dfltAdes.getDadPcVolo().setDadPcVoloNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DadPcVolo.Len.DAD_PC_VOLO_NULL));
        }
        // COB_CODE: IF IND-DAD-TP-FNT-CNBTVA = -1
        //              MOVE HIGH-VALUES TO DAD-TP-FNT-CNBTVA-NULL
        //           END-IF
        if (ws.getIndDfltAdes().getTpFntCnbtva() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DAD-TP-FNT-CNBTVA-NULL
            dfltAdes.setDadTpFntCnbtva(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DfltAdesIdbsdad0.Len.DAD_TP_FNT_CNBTVA));
        }
        // COB_CODE: IF IND-DAD-IMP-PRE-DFLT = -1
        //              MOVE HIGH-VALUES TO DAD-IMP-PRE-DFLT-NULL
        //           END-IF
        if (ws.getIndDfltAdes().getImpPreDflt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DAD-IMP-PRE-DFLT-NULL
            dfltAdes.getDadImpPreDflt().setDadImpPreDfltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DadImpPreDflt.Len.DAD_IMP_PRE_DFLT_NULL));
        }
        // COB_CODE: IF IND-DAD-TP-PRE = -1
        //              MOVE HIGH-VALUES TO DAD-TP-PRE-NULL
        //           END-IF.
        if (ws.getIndDfltAdes().getTpPre() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DAD-TP-PRE-NULL
            dfltAdes.setDadTpPre(Types.HIGH_CHAR_VAL);
        }
    }

    /**Original name: Z150-VALORIZZA-DATA-SERVICES-I<br>*/
    private void z150ValorizzaDataServicesI() {
        // COB_CODE: MOVE 'I' TO DAD-DS-OPER-SQL
        dfltAdes.setDadDsOperSqlFormatted("I");
        // COB_CODE: MOVE 0                   TO DAD-DS-VER
        dfltAdes.setDadDsVer(0);
        // COB_CODE: MOVE IDSV0003-USER-NAME TO DAD-DS-UTENTE
        dfltAdes.setDadDsUtente(idsv0003.getUserName());
        // COB_CODE: MOVE '1'                   TO DAD-DS-STATO-ELAB
        dfltAdes.setDadDsStatoElabFormatted("1");
        // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR TO DAD-DS-TS-CPTZ.
        dfltAdes.setDadDsTsCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
    }

    /**Original name: Z160-VALORIZZA-DATA-SERVICES-U<br>*/
    private void z160ValorizzaDataServicesU() {
        // COB_CODE: MOVE 'U' TO DAD-DS-OPER-SQL
        dfltAdes.setDadDsOperSqlFormatted("U");
        // COB_CODE: MOVE IDSV0003-USER-NAME TO DAD-DS-UTENTE
        dfltAdes.setDadDsUtente(idsv0003.getUserName());
        // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR TO DAD-DS-TS-CPTZ.
        dfltAdes.setDadDsTsCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
    }

    /**Original name: Z200-SET-INDICATORI-NULL<br>*/
    private void z200SetIndicatoriNull() {
        // COB_CODE: IF DAD-IB-POLI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DAD-IB-POLI
        //           ELSE
        //              MOVE 0 TO IND-DAD-IB-POLI
        //           END-IF
        if (Characters.EQ_HIGH.test(dfltAdes.getDadIbPoli(), DfltAdesIdbsdad0.Len.DAD_IB_POLI)) {
            // COB_CODE: MOVE -1 TO IND-DAD-IB-POLI
            ws.getIndDfltAdes().setIbPoli(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DAD-IB-POLI
            ws.getIndDfltAdes().setIbPoli(((short)0));
        }
        // COB_CODE: IF DAD-IB-DFLT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DAD-IB-DFLT
        //           ELSE
        //              MOVE 0 TO IND-DAD-IB-DFLT
        //           END-IF
        if (Characters.EQ_HIGH.test(dfltAdes.getDadIbDflt(), DfltAdesIdbsdad0.Len.DAD_IB_DFLT)) {
            // COB_CODE: MOVE -1 TO IND-DAD-IB-DFLT
            ws.getIndDfltAdes().setIbDflt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DAD-IB-DFLT
            ws.getIndDfltAdes().setIbDflt(((short)0));
        }
        // COB_CODE: IF DAD-COD-GAR-1-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DAD-COD-GAR-1
        //           ELSE
        //              MOVE 0 TO IND-DAD-COD-GAR-1
        //           END-IF
        if (Characters.EQ_HIGH.test(dfltAdes.getDadCodGar1Formatted())) {
            // COB_CODE: MOVE -1 TO IND-DAD-COD-GAR-1
            ws.getIndDfltAdes().setCodGar1(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DAD-COD-GAR-1
            ws.getIndDfltAdes().setCodGar1(((short)0));
        }
        // COB_CODE: IF DAD-COD-GAR-2-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DAD-COD-GAR-2
        //           ELSE
        //              MOVE 0 TO IND-DAD-COD-GAR-2
        //           END-IF
        if (Characters.EQ_HIGH.test(dfltAdes.getDadCodGar2Formatted())) {
            // COB_CODE: MOVE -1 TO IND-DAD-COD-GAR-2
            ws.getIndDfltAdes().setCodGar2(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DAD-COD-GAR-2
            ws.getIndDfltAdes().setCodGar2(((short)0));
        }
        // COB_CODE: IF DAD-COD-GAR-3-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DAD-COD-GAR-3
        //           ELSE
        //              MOVE 0 TO IND-DAD-COD-GAR-3
        //           END-IF
        if (Characters.EQ_HIGH.test(dfltAdes.getDadCodGar3Formatted())) {
            // COB_CODE: MOVE -1 TO IND-DAD-COD-GAR-3
            ws.getIndDfltAdes().setCodGar3(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DAD-COD-GAR-3
            ws.getIndDfltAdes().setCodGar3(((short)0));
        }
        // COB_CODE: IF DAD-COD-GAR-4-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DAD-COD-GAR-4
        //           ELSE
        //              MOVE 0 TO IND-DAD-COD-GAR-4
        //           END-IF
        if (Characters.EQ_HIGH.test(dfltAdes.getDadCodGar4Formatted())) {
            // COB_CODE: MOVE -1 TO IND-DAD-COD-GAR-4
            ws.getIndDfltAdes().setCodGar4(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DAD-COD-GAR-4
            ws.getIndDfltAdes().setCodGar4(((short)0));
        }
        // COB_CODE: IF DAD-COD-GAR-5-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DAD-COD-GAR-5
        //           ELSE
        //              MOVE 0 TO IND-DAD-COD-GAR-5
        //           END-IF
        if (Characters.EQ_HIGH.test(dfltAdes.getDadCodGar5Formatted())) {
            // COB_CODE: MOVE -1 TO IND-DAD-COD-GAR-5
            ws.getIndDfltAdes().setCodGar5(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DAD-COD-GAR-5
            ws.getIndDfltAdes().setCodGar5(((short)0));
        }
        // COB_CODE: IF DAD-COD-GAR-6-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DAD-COD-GAR-6
        //           ELSE
        //              MOVE 0 TO IND-DAD-COD-GAR-6
        //           END-IF
        if (Characters.EQ_HIGH.test(dfltAdes.getDadCodGar6Formatted())) {
            // COB_CODE: MOVE -1 TO IND-DAD-COD-GAR-6
            ws.getIndDfltAdes().setCodGar6(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DAD-COD-GAR-6
            ws.getIndDfltAdes().setCodGar6(((short)0));
        }
        // COB_CODE: IF DAD-DT-DECOR-DFLT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DAD-DT-DECOR-DFLT
        //           ELSE
        //              MOVE 0 TO IND-DAD-DT-DECOR-DFLT
        //           END-IF
        if (Characters.EQ_HIGH.test(dfltAdes.getDadDtDecorDflt().getDadDtDecorDfltNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DAD-DT-DECOR-DFLT
            ws.getIndDfltAdes().setDtDecorDflt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DAD-DT-DECOR-DFLT
            ws.getIndDfltAdes().setDtDecorDflt(((short)0));
        }
        // COB_CODE: IF DAD-ETA-SCAD-MASC-DFLT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DAD-ETA-SCAD-MASC-DFLT
        //           ELSE
        //              MOVE 0 TO IND-DAD-ETA-SCAD-MASC-DFLT
        //           END-IF
        if (Characters.EQ_HIGH.test(dfltAdes.getDadEtaScadMascDflt().getDadEtaScadMascDfltNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DAD-ETA-SCAD-MASC-DFLT
            ws.getIndDfltAdes().setEtaScadMascDflt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DAD-ETA-SCAD-MASC-DFLT
            ws.getIndDfltAdes().setEtaScadMascDflt(((short)0));
        }
        // COB_CODE: IF DAD-ETA-SCAD-FEMM-DFLT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DAD-ETA-SCAD-FEMM-DFLT
        //           ELSE
        //              MOVE 0 TO IND-DAD-ETA-SCAD-FEMM-DFLT
        //           END-IF
        if (Characters.EQ_HIGH.test(dfltAdes.getDadEtaScadFemmDflt().getDadEtaScadFemmDfltNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DAD-ETA-SCAD-FEMM-DFLT
            ws.getIndDfltAdes().setEtaScadFemmDflt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DAD-ETA-SCAD-FEMM-DFLT
            ws.getIndDfltAdes().setEtaScadFemmDflt(((short)0));
        }
        // COB_CODE: IF DAD-DUR-AA-ADES-DFLT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DAD-DUR-AA-ADES-DFLT
        //           ELSE
        //              MOVE 0 TO IND-DAD-DUR-AA-ADES-DFLT
        //           END-IF
        if (Characters.EQ_HIGH.test(dfltAdes.getDadDurAaAdesDflt().getDadDurAaAdesDfltNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DAD-DUR-AA-ADES-DFLT
            ws.getIndDfltAdes().setDurAaAdesDflt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DAD-DUR-AA-ADES-DFLT
            ws.getIndDfltAdes().setDurAaAdesDflt(((short)0));
        }
        // COB_CODE: IF DAD-DUR-MM-ADES-DFLT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DAD-DUR-MM-ADES-DFLT
        //           ELSE
        //              MOVE 0 TO IND-DAD-DUR-MM-ADES-DFLT
        //           END-IF
        if (Characters.EQ_HIGH.test(dfltAdes.getDadDurMmAdesDflt().getDadDurMmAdesDfltNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DAD-DUR-MM-ADES-DFLT
            ws.getIndDfltAdes().setDurMmAdesDflt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DAD-DUR-MM-ADES-DFLT
            ws.getIndDfltAdes().setDurMmAdesDflt(((short)0));
        }
        // COB_CODE: IF DAD-DUR-GG-ADES-DFLT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DAD-DUR-GG-ADES-DFLT
        //           ELSE
        //              MOVE 0 TO IND-DAD-DUR-GG-ADES-DFLT
        //           END-IF
        if (Characters.EQ_HIGH.test(dfltAdes.getDadDurGgAdesDflt().getDadDurGgAdesDfltNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DAD-DUR-GG-ADES-DFLT
            ws.getIndDfltAdes().setDurGgAdesDflt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DAD-DUR-GG-ADES-DFLT
            ws.getIndDfltAdes().setDurGgAdesDflt(((short)0));
        }
        // COB_CODE: IF DAD-DT-SCAD-ADES-DFLT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DAD-DT-SCAD-ADES-DFLT
        //           ELSE
        //              MOVE 0 TO IND-DAD-DT-SCAD-ADES-DFLT
        //           END-IF
        if (Characters.EQ_HIGH.test(dfltAdes.getDadDtScadAdesDflt().getDadDtScadAdesDfltNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DAD-DT-SCAD-ADES-DFLT
            ws.getIndDfltAdes().setDtScadAdesDflt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DAD-DT-SCAD-ADES-DFLT
            ws.getIndDfltAdes().setDtScadAdesDflt(((short)0));
        }
        // COB_CODE: IF DAD-FRAZ-DFLT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DAD-FRAZ-DFLT
        //           ELSE
        //              MOVE 0 TO IND-DAD-FRAZ-DFLT
        //           END-IF
        if (Characters.EQ_HIGH.test(dfltAdes.getDadFrazDflt().getDadFrazDfltNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DAD-FRAZ-DFLT
            ws.getIndDfltAdes().setFrazDflt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DAD-FRAZ-DFLT
            ws.getIndDfltAdes().setFrazDflt(((short)0));
        }
        // COB_CODE: IF DAD-PC-PROV-INC-DFLT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DAD-PC-PROV-INC-DFLT
        //           ELSE
        //              MOVE 0 TO IND-DAD-PC-PROV-INC-DFLT
        //           END-IF
        if (Characters.EQ_HIGH.test(dfltAdes.getDadPcProvIncDflt().getDadPcProvIncDfltNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DAD-PC-PROV-INC-DFLT
            ws.getIndDfltAdes().setPcProvIncDflt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DAD-PC-PROV-INC-DFLT
            ws.getIndDfltAdes().setPcProvIncDflt(((short)0));
        }
        // COB_CODE: IF DAD-IMP-PROV-INC-DFLT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DAD-IMP-PROV-INC-DFLT
        //           ELSE
        //              MOVE 0 TO IND-DAD-IMP-PROV-INC-DFLT
        //           END-IF
        if (Characters.EQ_HIGH.test(dfltAdes.getDadImpProvIncDflt().getDadImpProvIncDfltNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DAD-IMP-PROV-INC-DFLT
            ws.getIndDfltAdes().setImpProvIncDflt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DAD-IMP-PROV-INC-DFLT
            ws.getIndDfltAdes().setImpProvIncDflt(((short)0));
        }
        // COB_CODE: IF DAD-IMP-AZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DAD-IMP-AZ
        //           ELSE
        //              MOVE 0 TO IND-DAD-IMP-AZ
        //           END-IF
        if (Characters.EQ_HIGH.test(dfltAdes.getDadImpAz().getDadImpAzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DAD-IMP-AZ
            ws.getIndDfltAdes().setImpAz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DAD-IMP-AZ
            ws.getIndDfltAdes().setImpAz(((short)0));
        }
        // COB_CODE: IF DAD-IMP-ADER-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DAD-IMP-ADER
        //           ELSE
        //              MOVE 0 TO IND-DAD-IMP-ADER
        //           END-IF
        if (Characters.EQ_HIGH.test(dfltAdes.getDadImpAder().getDadImpAderNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DAD-IMP-ADER
            ws.getIndDfltAdes().setImpAder(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DAD-IMP-ADER
            ws.getIndDfltAdes().setImpAder(((short)0));
        }
        // COB_CODE: IF DAD-IMP-TFR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DAD-IMP-TFR
        //           ELSE
        //              MOVE 0 TO IND-DAD-IMP-TFR
        //           END-IF
        if (Characters.EQ_HIGH.test(dfltAdes.getDadImpTfr().getDadImpTfrNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DAD-IMP-TFR
            ws.getIndDfltAdes().setImpTfr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DAD-IMP-TFR
            ws.getIndDfltAdes().setImpTfr(((short)0));
        }
        // COB_CODE: IF DAD-IMP-VOLO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DAD-IMP-VOLO
        //           ELSE
        //              MOVE 0 TO IND-DAD-IMP-VOLO
        //           END-IF
        if (Characters.EQ_HIGH.test(dfltAdes.getDadImpVolo().getDadImpVoloNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DAD-IMP-VOLO
            ws.getIndDfltAdes().setImpVolo(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DAD-IMP-VOLO
            ws.getIndDfltAdes().setImpVolo(((short)0));
        }
        // COB_CODE: IF DAD-PC-AZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DAD-PC-AZ
        //           ELSE
        //              MOVE 0 TO IND-DAD-PC-AZ
        //           END-IF
        if (Characters.EQ_HIGH.test(dfltAdes.getDadPcAz().getDadPcAzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DAD-PC-AZ
            ws.getIndDfltAdes().setPcAz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DAD-PC-AZ
            ws.getIndDfltAdes().setPcAz(((short)0));
        }
        // COB_CODE: IF DAD-PC-ADER-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DAD-PC-ADER
        //           ELSE
        //              MOVE 0 TO IND-DAD-PC-ADER
        //           END-IF
        if (Characters.EQ_HIGH.test(dfltAdes.getDadPcAder().getDadPcAderNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DAD-PC-ADER
            ws.getIndDfltAdes().setPcAder(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DAD-PC-ADER
            ws.getIndDfltAdes().setPcAder(((short)0));
        }
        // COB_CODE: IF DAD-PC-TFR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DAD-PC-TFR
        //           ELSE
        //              MOVE 0 TO IND-DAD-PC-TFR
        //           END-IF
        if (Characters.EQ_HIGH.test(dfltAdes.getDadPcTfr().getDadPcTfrNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DAD-PC-TFR
            ws.getIndDfltAdes().setPcTfr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DAD-PC-TFR
            ws.getIndDfltAdes().setPcTfr(((short)0));
        }
        // COB_CODE: IF DAD-PC-VOLO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DAD-PC-VOLO
        //           ELSE
        //              MOVE 0 TO IND-DAD-PC-VOLO
        //           END-IF
        if (Characters.EQ_HIGH.test(dfltAdes.getDadPcVolo().getDadPcVoloNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DAD-PC-VOLO
            ws.getIndDfltAdes().setPcVolo(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DAD-PC-VOLO
            ws.getIndDfltAdes().setPcVolo(((short)0));
        }
        // COB_CODE: IF DAD-TP-FNT-CNBTVA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DAD-TP-FNT-CNBTVA
        //           ELSE
        //              MOVE 0 TO IND-DAD-TP-FNT-CNBTVA
        //           END-IF
        if (Characters.EQ_HIGH.test(dfltAdes.getDadTpFntCnbtvaFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DAD-TP-FNT-CNBTVA
            ws.getIndDfltAdes().setTpFntCnbtva(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DAD-TP-FNT-CNBTVA
            ws.getIndDfltAdes().setTpFntCnbtva(((short)0));
        }
        // COB_CODE: IF DAD-IMP-PRE-DFLT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DAD-IMP-PRE-DFLT
        //           ELSE
        //              MOVE 0 TO IND-DAD-IMP-PRE-DFLT
        //           END-IF
        if (Characters.EQ_HIGH.test(dfltAdes.getDadImpPreDflt().getDadImpPreDfltNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DAD-IMP-PRE-DFLT
            ws.getIndDfltAdes().setImpPreDflt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DAD-IMP-PRE-DFLT
            ws.getIndDfltAdes().setImpPreDflt(((short)0));
        }
        // COB_CODE: IF DAD-TP-PRE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DAD-TP-PRE
        //           ELSE
        //              MOVE 0 TO IND-DAD-TP-PRE
        //           END-IF.
        if (Conditions.eq(dfltAdes.getDadTpPre(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-DAD-TP-PRE
            ws.getIndDfltAdes().setTpPre(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DAD-TP-PRE
            ws.getIndDfltAdes().setTpPre(((short)0));
        }
    }

    /**Original name: Z400-SEQ-RIGA<br>*/
    private void z400SeqRiga() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z500-AGGIORNAMENTO-STORICO<br>*/
    private void z500AggiornamentoStorico() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z900-CONVERTI-N-TO-X<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da 9(8) comp-3 a date
	 * ----</pre>*/
    private void z900ConvertiNToX() {
        // COB_CODE: IF IND-DAD-DT-DECOR-DFLT = 0
        //               MOVE WS-DATE-X      TO DAD-DT-DECOR-DFLT-DB
        //           END-IF
        if (ws.getIndDfltAdes().getDtDecorDflt() == 0) {
            // COB_CODE: MOVE DAD-DT-DECOR-DFLT TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(dfltAdes.getDadDtDecorDflt().getDadDtDecorDflt(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO DAD-DT-DECOR-DFLT-DB
            ws.getIdbvdad3().setDadDtDecorDfltDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-DAD-DT-SCAD-ADES-DFLT = 0
        //               MOVE WS-DATE-X      TO DAD-DT-SCAD-ADES-DFLT-DB
        //           END-IF.
        if (ws.getIndDfltAdes().getDtScadAdesDflt() == 0) {
            // COB_CODE: MOVE DAD-DT-SCAD-ADES-DFLT TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(dfltAdes.getDadDtScadAdesDflt().getDadDtScadAdesDflt(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO DAD-DT-SCAD-ADES-DFLT-DB
            ws.getIdbvdad3().setDadDtScadAdesDfltDb(ws.getIdsv0010().getWsDateX());
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: IF IND-DAD-DT-DECOR-DFLT = 0
        //               MOVE WS-DATE-N      TO DAD-DT-DECOR-DFLT
        //           END-IF
        if (ws.getIndDfltAdes().getDtDecorDflt() == 0) {
            // COB_CODE: MOVE DAD-DT-DECOR-DFLT-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getIdbvdad3().getDadDtDecorDfltDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO DAD-DT-DECOR-DFLT
            dfltAdes.getDadDtDecorDflt().setDadDtDecorDflt(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-DAD-DT-SCAD-ADES-DFLT = 0
        //               MOVE WS-DATE-N      TO DAD-DT-SCAD-ADES-DFLT
        //           END-IF.
        if (ws.getIndDfltAdes().getDtScadAdesDflt() == 0) {
            // COB_CODE: MOVE DAD-DT-SCAD-ADES-DFLT-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getIdbvdad3().getDadDtScadAdesDfltDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO DAD-DT-SCAD-ADES-DFLT
            dfltAdes.getDadDtScadAdesDflt().setDadDtScadAdesDflt(ws.getIdsv0010().getWsDateN());
        }
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public int getCodCompAnia() {
        return dfltAdes.getDadCodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.dfltAdes.setDadCodCompAnia(codCompAnia);
    }

    @Override
    public String getCodGar1() {
        return dfltAdes.getDadCodGar1();
    }

    @Override
    public void setCodGar1(String codGar1) {
        this.dfltAdes.setDadCodGar1(codGar1);
    }

    @Override
    public String getCodGar1Obj() {
        if (ws.getIndDfltAdes().getCodGar1() >= 0) {
            return getCodGar1();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodGar1Obj(String codGar1Obj) {
        if (codGar1Obj != null) {
            setCodGar1(codGar1Obj);
            ws.getIndDfltAdes().setCodGar1(((short)0));
        }
        else {
            ws.getIndDfltAdes().setCodGar1(((short)-1));
        }
    }

    @Override
    public String getCodGar2() {
        return dfltAdes.getDadCodGar2();
    }

    @Override
    public void setCodGar2(String codGar2) {
        this.dfltAdes.setDadCodGar2(codGar2);
    }

    @Override
    public String getCodGar2Obj() {
        if (ws.getIndDfltAdes().getCodGar2() >= 0) {
            return getCodGar2();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodGar2Obj(String codGar2Obj) {
        if (codGar2Obj != null) {
            setCodGar2(codGar2Obj);
            ws.getIndDfltAdes().setCodGar2(((short)0));
        }
        else {
            ws.getIndDfltAdes().setCodGar2(((short)-1));
        }
    }

    @Override
    public String getCodGar3() {
        return dfltAdes.getDadCodGar3();
    }

    @Override
    public void setCodGar3(String codGar3) {
        this.dfltAdes.setDadCodGar3(codGar3);
    }

    @Override
    public String getCodGar3Obj() {
        if (ws.getIndDfltAdes().getCodGar3() >= 0) {
            return getCodGar3();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodGar3Obj(String codGar3Obj) {
        if (codGar3Obj != null) {
            setCodGar3(codGar3Obj);
            ws.getIndDfltAdes().setCodGar3(((short)0));
        }
        else {
            ws.getIndDfltAdes().setCodGar3(((short)-1));
        }
    }

    @Override
    public String getCodGar4() {
        return dfltAdes.getDadCodGar4();
    }

    @Override
    public void setCodGar4(String codGar4) {
        this.dfltAdes.setDadCodGar4(codGar4);
    }

    @Override
    public String getCodGar4Obj() {
        if (ws.getIndDfltAdes().getCodGar4() >= 0) {
            return getCodGar4();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodGar4Obj(String codGar4Obj) {
        if (codGar4Obj != null) {
            setCodGar4(codGar4Obj);
            ws.getIndDfltAdes().setCodGar4(((short)0));
        }
        else {
            ws.getIndDfltAdes().setCodGar4(((short)-1));
        }
    }

    @Override
    public String getCodGar5() {
        return dfltAdes.getDadCodGar5();
    }

    @Override
    public void setCodGar5(String codGar5) {
        this.dfltAdes.setDadCodGar5(codGar5);
    }

    @Override
    public String getCodGar5Obj() {
        if (ws.getIndDfltAdes().getCodGar5() >= 0) {
            return getCodGar5();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodGar5Obj(String codGar5Obj) {
        if (codGar5Obj != null) {
            setCodGar5(codGar5Obj);
            ws.getIndDfltAdes().setCodGar5(((short)0));
        }
        else {
            ws.getIndDfltAdes().setCodGar5(((short)-1));
        }
    }

    @Override
    public String getCodGar6() {
        return dfltAdes.getDadCodGar6();
    }

    @Override
    public void setCodGar6(String codGar6) {
        this.dfltAdes.setDadCodGar6(codGar6);
    }

    @Override
    public String getCodGar6Obj() {
        if (ws.getIndDfltAdes().getCodGar6() >= 0) {
            return getCodGar6();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodGar6Obj(String codGar6Obj) {
        if (codGar6Obj != null) {
            setCodGar6(codGar6Obj);
            ws.getIndDfltAdes().setCodGar6(((short)0));
        }
        else {
            ws.getIndDfltAdes().setCodGar6(((short)-1));
        }
    }

    @Override
    public int getDadIdDfltAdes() {
        return dfltAdes.getDadIdDfltAdes();
    }

    @Override
    public void setDadIdDfltAdes(int dadIdDfltAdes) {
        this.dfltAdes.setDadIdDfltAdes(dadIdDfltAdes);
    }

    @Override
    public char getDsOperSql() {
        return dfltAdes.getDadDsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.dfltAdes.setDadDsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return dfltAdes.getDadDsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.dfltAdes.setDadDsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsCptz() {
        return dfltAdes.getDadDsTsCptz();
    }

    @Override
    public void setDsTsCptz(long dsTsCptz) {
        this.dfltAdes.setDadDsTsCptz(dsTsCptz);
    }

    @Override
    public String getDsUtente() {
        return dfltAdes.getDadDsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.dfltAdes.setDadDsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return dfltAdes.getDadDsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.dfltAdes.setDadDsVer(dsVer);
    }

    @Override
    public String getDtDecorDfltDb() {
        return ws.getIdbvdad3().getDadDtDecorDfltDb();
    }

    @Override
    public void setDtDecorDfltDb(String dtDecorDfltDb) {
        this.ws.getIdbvdad3().setDadDtDecorDfltDb(dtDecorDfltDb);
    }

    @Override
    public String getDtDecorDfltDbObj() {
        if (ws.getIndDfltAdes().getDtDecorDflt() >= 0) {
            return getDtDecorDfltDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtDecorDfltDbObj(String dtDecorDfltDbObj) {
        if (dtDecorDfltDbObj != null) {
            setDtDecorDfltDb(dtDecorDfltDbObj);
            ws.getIndDfltAdes().setDtDecorDflt(((short)0));
        }
        else {
            ws.getIndDfltAdes().setDtDecorDflt(((short)-1));
        }
    }

    @Override
    public String getDtScadAdesDfltDb() {
        return ws.getIdbvdad3().getDadDtScadAdesDfltDb();
    }

    @Override
    public void setDtScadAdesDfltDb(String dtScadAdesDfltDb) {
        this.ws.getIdbvdad3().setDadDtScadAdesDfltDb(dtScadAdesDfltDb);
    }

    @Override
    public String getDtScadAdesDfltDbObj() {
        if (ws.getIndDfltAdes().getDtScadAdesDflt() >= 0) {
            return getDtScadAdesDfltDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtScadAdesDfltDbObj(String dtScadAdesDfltDbObj) {
        if (dtScadAdesDfltDbObj != null) {
            setDtScadAdesDfltDb(dtScadAdesDfltDbObj);
            ws.getIndDfltAdes().setDtScadAdesDflt(((short)0));
        }
        else {
            ws.getIndDfltAdes().setDtScadAdesDflt(((short)-1));
        }
    }

    @Override
    public int getDurAaAdesDflt() {
        return dfltAdes.getDadDurAaAdesDflt().getDadDurAaAdesDflt();
    }

    @Override
    public void setDurAaAdesDflt(int durAaAdesDflt) {
        this.dfltAdes.getDadDurAaAdesDflt().setDadDurAaAdesDflt(durAaAdesDflt);
    }

    @Override
    public Integer getDurAaAdesDfltObj() {
        if (ws.getIndDfltAdes().getDurAaAdesDflt() >= 0) {
            return ((Integer)getDurAaAdesDflt());
        }
        else {
            return null;
        }
    }

    @Override
    public void setDurAaAdesDfltObj(Integer durAaAdesDfltObj) {
        if (durAaAdesDfltObj != null) {
            setDurAaAdesDflt(((int)durAaAdesDfltObj));
            ws.getIndDfltAdes().setDurAaAdesDflt(((short)0));
        }
        else {
            ws.getIndDfltAdes().setDurAaAdesDflt(((short)-1));
        }
    }

    @Override
    public int getDurGgAdesDflt() {
        return dfltAdes.getDadDurGgAdesDflt().getDadDurGgAdesDflt();
    }

    @Override
    public void setDurGgAdesDflt(int durGgAdesDflt) {
        this.dfltAdes.getDadDurGgAdesDflt().setDadDurGgAdesDflt(durGgAdesDflt);
    }

    @Override
    public Integer getDurGgAdesDfltObj() {
        if (ws.getIndDfltAdes().getDurGgAdesDflt() >= 0) {
            return ((Integer)getDurGgAdesDflt());
        }
        else {
            return null;
        }
    }

    @Override
    public void setDurGgAdesDfltObj(Integer durGgAdesDfltObj) {
        if (durGgAdesDfltObj != null) {
            setDurGgAdesDflt(((int)durGgAdesDfltObj));
            ws.getIndDfltAdes().setDurGgAdesDflt(((short)0));
        }
        else {
            ws.getIndDfltAdes().setDurGgAdesDflt(((short)-1));
        }
    }

    @Override
    public int getDurMmAdesDflt() {
        return dfltAdes.getDadDurMmAdesDflt().getDadDurMmAdesDflt();
    }

    @Override
    public void setDurMmAdesDflt(int durMmAdesDflt) {
        this.dfltAdes.getDadDurMmAdesDflt().setDadDurMmAdesDflt(durMmAdesDflt);
    }

    @Override
    public Integer getDurMmAdesDfltObj() {
        if (ws.getIndDfltAdes().getDurMmAdesDflt() >= 0) {
            return ((Integer)getDurMmAdesDflt());
        }
        else {
            return null;
        }
    }

    @Override
    public void setDurMmAdesDfltObj(Integer durMmAdesDfltObj) {
        if (durMmAdesDfltObj != null) {
            setDurMmAdesDflt(((int)durMmAdesDfltObj));
            ws.getIndDfltAdes().setDurMmAdesDflt(((short)0));
        }
        else {
            ws.getIndDfltAdes().setDurMmAdesDflt(((short)-1));
        }
    }

    @Override
    public int getEtaScadFemmDflt() {
        return dfltAdes.getDadEtaScadFemmDflt().getDadEtaScadFemmDflt();
    }

    @Override
    public void setEtaScadFemmDflt(int etaScadFemmDflt) {
        this.dfltAdes.getDadEtaScadFemmDflt().setDadEtaScadFemmDflt(etaScadFemmDflt);
    }

    @Override
    public Integer getEtaScadFemmDfltObj() {
        if (ws.getIndDfltAdes().getEtaScadFemmDflt() >= 0) {
            return ((Integer)getEtaScadFemmDflt());
        }
        else {
            return null;
        }
    }

    @Override
    public void setEtaScadFemmDfltObj(Integer etaScadFemmDfltObj) {
        if (etaScadFemmDfltObj != null) {
            setEtaScadFemmDflt(((int)etaScadFemmDfltObj));
            ws.getIndDfltAdes().setEtaScadFemmDflt(((short)0));
        }
        else {
            ws.getIndDfltAdes().setEtaScadFemmDflt(((short)-1));
        }
    }

    @Override
    public int getEtaScadMascDflt() {
        return dfltAdes.getDadEtaScadMascDflt().getDadEtaScadMascDflt();
    }

    @Override
    public void setEtaScadMascDflt(int etaScadMascDflt) {
        this.dfltAdes.getDadEtaScadMascDflt().setDadEtaScadMascDflt(etaScadMascDflt);
    }

    @Override
    public Integer getEtaScadMascDfltObj() {
        if (ws.getIndDfltAdes().getEtaScadMascDflt() >= 0) {
            return ((Integer)getEtaScadMascDflt());
        }
        else {
            return null;
        }
    }

    @Override
    public void setEtaScadMascDfltObj(Integer etaScadMascDfltObj) {
        if (etaScadMascDfltObj != null) {
            setEtaScadMascDflt(((int)etaScadMascDfltObj));
            ws.getIndDfltAdes().setEtaScadMascDflt(((short)0));
        }
        else {
            ws.getIndDfltAdes().setEtaScadMascDflt(((short)-1));
        }
    }

    @Override
    public int getFrazDflt() {
        return dfltAdes.getDadFrazDflt().getDadFrazDflt();
    }

    @Override
    public void setFrazDflt(int frazDflt) {
        this.dfltAdes.getDadFrazDflt().setDadFrazDflt(frazDflt);
    }

    @Override
    public Integer getFrazDfltObj() {
        if (ws.getIndDfltAdes().getFrazDflt() >= 0) {
            return ((Integer)getFrazDflt());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFrazDfltObj(Integer frazDfltObj) {
        if (frazDfltObj != null) {
            setFrazDflt(((int)frazDfltObj));
            ws.getIndDfltAdes().setFrazDflt(((short)0));
        }
        else {
            ws.getIndDfltAdes().setFrazDflt(((short)-1));
        }
    }

    @Override
    public String getIbDflt() {
        return dfltAdes.getDadIbDflt();
    }

    @Override
    public void setIbDflt(String ibDflt) {
        this.dfltAdes.setDadIbDflt(ibDflt);
    }

    @Override
    public String getIbDfltObj() {
        if (ws.getIndDfltAdes().getIbDflt() >= 0) {
            return getIbDflt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIbDfltObj(String ibDfltObj) {
        if (ibDfltObj != null) {
            setIbDflt(ibDfltObj);
            ws.getIndDfltAdes().setIbDflt(((short)0));
        }
        else {
            ws.getIndDfltAdes().setIbDflt(((short)-1));
        }
    }

    @Override
    public String getIbPoli() {
        return dfltAdes.getDadIbPoli();
    }

    @Override
    public void setIbPoli(String ibPoli) {
        this.dfltAdes.setDadIbPoli(ibPoli);
    }

    @Override
    public String getIbPoliObj() {
        if (ws.getIndDfltAdes().getIbPoli() >= 0) {
            return getIbPoli();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIbPoliObj(String ibPoliObj) {
        if (ibPoliObj != null) {
            setIbPoli(ibPoliObj);
            ws.getIndDfltAdes().setIbPoli(((short)0));
        }
        else {
            ws.getIndDfltAdes().setIbPoli(((short)-1));
        }
    }

    @Override
    public int getIdPoli() {
        return dfltAdes.getDadIdPoli();
    }

    @Override
    public void setIdPoli(int idPoli) {
        this.dfltAdes.setDadIdPoli(idPoli);
    }

    @Override
    public AfDecimal getImpAder() {
        return dfltAdes.getDadImpAder().getDadImpAder();
    }

    @Override
    public void setImpAder(AfDecimal impAder) {
        this.dfltAdes.getDadImpAder().setDadImpAder(impAder.copy());
    }

    @Override
    public AfDecimal getImpAderObj() {
        if (ws.getIndDfltAdes().getImpAder() >= 0) {
            return getImpAder();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpAderObj(AfDecimal impAderObj) {
        if (impAderObj != null) {
            setImpAder(new AfDecimal(impAderObj, 15, 3));
            ws.getIndDfltAdes().setImpAder(((short)0));
        }
        else {
            ws.getIndDfltAdes().setImpAder(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpAz() {
        return dfltAdes.getDadImpAz().getDadImpAz();
    }

    @Override
    public void setImpAz(AfDecimal impAz) {
        this.dfltAdes.getDadImpAz().setDadImpAz(impAz.copy());
    }

    @Override
    public AfDecimal getImpAzObj() {
        if (ws.getIndDfltAdes().getImpAz() >= 0) {
            return getImpAz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpAzObj(AfDecimal impAzObj) {
        if (impAzObj != null) {
            setImpAz(new AfDecimal(impAzObj, 15, 3));
            ws.getIndDfltAdes().setImpAz(((short)0));
        }
        else {
            ws.getIndDfltAdes().setImpAz(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpPreDflt() {
        return dfltAdes.getDadImpPreDflt().getDadImpPreDflt();
    }

    @Override
    public void setImpPreDflt(AfDecimal impPreDflt) {
        this.dfltAdes.getDadImpPreDflt().setDadImpPreDflt(impPreDflt.copy());
    }

    @Override
    public AfDecimal getImpPreDfltObj() {
        if (ws.getIndDfltAdes().getImpPreDflt() >= 0) {
            return getImpPreDflt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpPreDfltObj(AfDecimal impPreDfltObj) {
        if (impPreDfltObj != null) {
            setImpPreDflt(new AfDecimal(impPreDfltObj, 15, 3));
            ws.getIndDfltAdes().setImpPreDflt(((short)0));
        }
        else {
            ws.getIndDfltAdes().setImpPreDflt(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpProvIncDflt() {
        return dfltAdes.getDadImpProvIncDflt().getDadImpProvIncDflt();
    }

    @Override
    public void setImpProvIncDflt(AfDecimal impProvIncDflt) {
        this.dfltAdes.getDadImpProvIncDflt().setDadImpProvIncDflt(impProvIncDflt.copy());
    }

    @Override
    public AfDecimal getImpProvIncDfltObj() {
        if (ws.getIndDfltAdes().getImpProvIncDflt() >= 0) {
            return getImpProvIncDflt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpProvIncDfltObj(AfDecimal impProvIncDfltObj) {
        if (impProvIncDfltObj != null) {
            setImpProvIncDflt(new AfDecimal(impProvIncDfltObj, 15, 3));
            ws.getIndDfltAdes().setImpProvIncDflt(((short)0));
        }
        else {
            ws.getIndDfltAdes().setImpProvIncDflt(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpTfr() {
        return dfltAdes.getDadImpTfr().getDadImpTfr();
    }

    @Override
    public void setImpTfr(AfDecimal impTfr) {
        this.dfltAdes.getDadImpTfr().setDadImpTfr(impTfr.copy());
    }

    @Override
    public AfDecimal getImpTfrObj() {
        if (ws.getIndDfltAdes().getImpTfr() >= 0) {
            return getImpTfr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpTfrObj(AfDecimal impTfrObj) {
        if (impTfrObj != null) {
            setImpTfr(new AfDecimal(impTfrObj, 15, 3));
            ws.getIndDfltAdes().setImpTfr(((short)0));
        }
        else {
            ws.getIndDfltAdes().setImpTfr(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpVolo() {
        return dfltAdes.getDadImpVolo().getDadImpVolo();
    }

    @Override
    public void setImpVolo(AfDecimal impVolo) {
        this.dfltAdes.getDadImpVolo().setDadImpVolo(impVolo.copy());
    }

    @Override
    public AfDecimal getImpVoloObj() {
        if (ws.getIndDfltAdes().getImpVolo() >= 0) {
            return getImpVolo();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpVoloObj(AfDecimal impVoloObj) {
        if (impVoloObj != null) {
            setImpVolo(new AfDecimal(impVoloObj, 15, 3));
            ws.getIndDfltAdes().setImpVolo(((short)0));
        }
        else {
            ws.getIndDfltAdes().setImpVolo(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcAder() {
        return dfltAdes.getDadPcAder().getDadPcAder();
    }

    @Override
    public void setPcAder(AfDecimal pcAder) {
        this.dfltAdes.getDadPcAder().setDadPcAder(pcAder.copy());
    }

    @Override
    public AfDecimal getPcAderObj() {
        if (ws.getIndDfltAdes().getPcAder() >= 0) {
            return getPcAder();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcAderObj(AfDecimal pcAderObj) {
        if (pcAderObj != null) {
            setPcAder(new AfDecimal(pcAderObj, 6, 3));
            ws.getIndDfltAdes().setPcAder(((short)0));
        }
        else {
            ws.getIndDfltAdes().setPcAder(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcAz() {
        return dfltAdes.getDadPcAz().getDadPcAz();
    }

    @Override
    public void setPcAz(AfDecimal pcAz) {
        this.dfltAdes.getDadPcAz().setDadPcAz(pcAz.copy());
    }

    @Override
    public AfDecimal getPcAzObj() {
        if (ws.getIndDfltAdes().getPcAz() >= 0) {
            return getPcAz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcAzObj(AfDecimal pcAzObj) {
        if (pcAzObj != null) {
            setPcAz(new AfDecimal(pcAzObj, 6, 3));
            ws.getIndDfltAdes().setPcAz(((short)0));
        }
        else {
            ws.getIndDfltAdes().setPcAz(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcProvIncDflt() {
        return dfltAdes.getDadPcProvIncDflt().getDadPcProvIncDflt();
    }

    @Override
    public void setPcProvIncDflt(AfDecimal pcProvIncDflt) {
        this.dfltAdes.getDadPcProvIncDflt().setDadPcProvIncDflt(pcProvIncDflt.copy());
    }

    @Override
    public AfDecimal getPcProvIncDfltObj() {
        if (ws.getIndDfltAdes().getPcProvIncDflt() >= 0) {
            return getPcProvIncDflt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcProvIncDfltObj(AfDecimal pcProvIncDfltObj) {
        if (pcProvIncDfltObj != null) {
            setPcProvIncDflt(new AfDecimal(pcProvIncDfltObj, 6, 3));
            ws.getIndDfltAdes().setPcProvIncDflt(((short)0));
        }
        else {
            ws.getIndDfltAdes().setPcProvIncDflt(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcTfr() {
        return dfltAdes.getDadPcTfr().getDadPcTfr();
    }

    @Override
    public void setPcTfr(AfDecimal pcTfr) {
        this.dfltAdes.getDadPcTfr().setDadPcTfr(pcTfr.copy());
    }

    @Override
    public AfDecimal getPcTfrObj() {
        if (ws.getIndDfltAdes().getPcTfr() >= 0) {
            return getPcTfr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcTfrObj(AfDecimal pcTfrObj) {
        if (pcTfrObj != null) {
            setPcTfr(new AfDecimal(pcTfrObj, 6, 3));
            ws.getIndDfltAdes().setPcTfr(((short)0));
        }
        else {
            ws.getIndDfltAdes().setPcTfr(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcVolo() {
        return dfltAdes.getDadPcVolo().getDadPcVolo();
    }

    @Override
    public void setPcVolo(AfDecimal pcVolo) {
        this.dfltAdes.getDadPcVolo().setDadPcVolo(pcVolo.copy());
    }

    @Override
    public AfDecimal getPcVoloObj() {
        if (ws.getIndDfltAdes().getPcVolo() >= 0) {
            return getPcVolo();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcVoloObj(AfDecimal pcVoloObj) {
        if (pcVoloObj != null) {
            setPcVolo(new AfDecimal(pcVoloObj, 6, 3));
            ws.getIndDfltAdes().setPcVolo(((short)0));
        }
        else {
            ws.getIndDfltAdes().setPcVolo(((short)-1));
        }
    }

    @Override
    public String getTpFntCnbtva() {
        return dfltAdes.getDadTpFntCnbtva();
    }

    @Override
    public void setTpFntCnbtva(String tpFntCnbtva) {
        this.dfltAdes.setDadTpFntCnbtva(tpFntCnbtva);
    }

    @Override
    public String getTpFntCnbtvaObj() {
        if (ws.getIndDfltAdes().getTpFntCnbtva() >= 0) {
            return getTpFntCnbtva();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpFntCnbtvaObj(String tpFntCnbtvaObj) {
        if (tpFntCnbtvaObj != null) {
            setTpFntCnbtva(tpFntCnbtvaObj);
            ws.getIndDfltAdes().setTpFntCnbtva(((short)0));
        }
        else {
            ws.getIndDfltAdes().setTpFntCnbtva(((short)-1));
        }
    }

    @Override
    public char getTpPre() {
        return dfltAdes.getDadTpPre();
    }

    @Override
    public void setTpPre(char tpPre) {
        this.dfltAdes.setDadTpPre(tpPre);
    }

    @Override
    public Character getTpPreObj() {
        if (ws.getIndDfltAdes().getTpPre() >= 0) {
            return ((Character)getTpPre());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpPreObj(Character tpPreObj) {
        if (tpPreObj != null) {
            setTpPre(((char)tpPreObj));
            ws.getIndDfltAdes().setTpPre(((short)0));
        }
        else {
            ws.getIndDfltAdes().setTpPre(((short)-1));
        }
    }
}
