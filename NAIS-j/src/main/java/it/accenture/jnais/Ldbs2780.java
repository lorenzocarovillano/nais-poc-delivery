package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.jdbc.FieldNotMappedException;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.BilaTrchEstrDao;
import it.accenture.jnais.commons.data.to.IBilaTrchEstr;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.BilaTrchEstrIdbsb030;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ldbs2780Data;

/**Original name: LDBS2780<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  29 LUG 2008.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Ldbs2780 extends Program implements IBilaTrchEstr {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private BilaTrchEstrDao bilaTrchEstrDao = new BilaTrchEstrDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Ldbs2780Data ws = new Ldbs2780Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: BILA-TRCH-ESTR
    private BilaTrchEstrIdbsb030 bilaTrchEstr;

    //==== METHODS ====
    /**Original name: PROGRAM_LDBS2780_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, BilaTrchEstrIdbsb030 bilaTrchEstr) {
        this.idsv0003 = idsv0003;
        this.bilaTrchEstr = bilaTrchEstr;
        // COB_CODE: PERFORM A000-INIZIO              THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //                   END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
            //              END-EVALUATE
            //           ELSE
            //               SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
            //            END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                        c200ElaboraWcNst();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Ldbs2780 getInstance() {
        return ((Ldbs2780)Programs.getInstance(Ldbs2780.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'IDBSB030'       TO IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("IDBSB030");
        // COB_CODE: MOVE 'BILA_TRCH_ESTR' TO IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("BILA_TRCH_ESTR");
        // COB_CODE: MOVE '00'                     TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                   TO   IDSV0003-SQLCODE
        //                                              IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                              IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE   OR
                //              IDSV0003-UPDATE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple() || idsv0003.getOperazione().isUpdate()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: C200-ELABORA-WC-NST<br>*/
    private void c200ElaboraWcNst() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-UPDATE
        //                 PERFORM C210-UPDATE-WC-NST          THRU C210-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isUpdate()) {
            // COB_CODE: PERFORM C210-UPDATE-WC-NST          THRU C210-EX
            c210UpdateWcNst();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: C210-UPDATE-WC-NST<br>*/
    private void c210UpdateWcNst() {
        // COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES THRU Z150-EX.
        z150ValorizzaDataServices();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.
        //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LDBS2780.cbl:line=137, because the code is unreachable.
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE BILA_TRCH_ESTR
        //                SET  TP_STAT_BUS_TRCH   = :B03-TP-STAT-BUS-TRCH
        //                    ,TP_CAUS_TRCH       = :B03-TP-CAUS-TRCH
        //                    ,TP_STAT_BUS_POLI   = :B03-TP-STAT-BUS-POLI
        //                    ,TP_CAUS_POLI       = :B03-TP-CAUS-POLI
        //                    ,TP_STAT_BUS_ADES   = :B03-TP-STAT-BUS-ADES
        //                    ,TP_CAUS_ADES       = :B03-TP-CAUS-ADES
        //                    ,ID_RICH_ESTRAZ_AGG = :B03-ID-RICH-ESTRAZ-AGG
        //                                          :IND-B03-ID-RICH-ESTRAZ-AGG
        //                WHERE  ID_RICH_ESTRAZ_MAS = :B03-ID-RICH-ESTRAZ-MAS
        //                AND    ID_ADES            = :B03-ID-ADES
        //                AND    ID_POLI            = :B03-ID-POLI
        //                AND    ID_TRCH_DI_GAR     = :B03-ID-TRCH-DI-GAR
        //           END-EXEC.
        bilaTrchEstrDao.updateRec2(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: Z150-VALORIZZA-DATA-SERVICES<br>*/
    private void z150ValorizzaDataServices() {
        // COB_CODE: MOVE IDSV0003-OPERAZIONE TO B03-DS-OPER-SQL
        bilaTrchEstr.setB03DsOperSqlFormatted(idsv0003.getOperazione().getOperazioneFormatted());
        // COB_CODE: MOVE 1                   TO B03-DS-VER
        bilaTrchEstr.setB03DsVer(1);
        // COB_CODE: MOVE IDSV0003-USER-NAME TO B03-DS-UTENTE
        bilaTrchEstr.setB03DsUtente(idsv0003.getUserName());
        // COB_CODE: MOVE '1'                   TO B03-DS-STATO-ELAB
        bilaTrchEstr.setB03DsStatoElabFormatted("1");
        // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR TO B03-DS-TS-CPTZ.
        bilaTrchEstr.setB03DsTsCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
    }

    /**Original name: Z200-SET-INDICATORI-NULL<br>*/
    private void z200SetIndicatoriNull() {
        // COB_CODE: IF B03-ID-RICH-ESTRAZ-AGG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-ID-RICH-ESTRAZ-AGG
        //           ELSE
        //              MOVE 0 TO IND-B03-ID-RICH-ESTRAZ-AGG
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03IdRichEstrazAgg().getB03IdRichEstrazAggNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-ID-RICH-ESTRAZ-AGG
            ws.getIndBilaTrchEstr().setIdRichEstrazAgg(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-ID-RICH-ESTRAZ-AGG
            ws.getIndBilaTrchEstr().setIdRichEstrazAgg(((short)0));
        }
        // COB_CODE: IF B03-FL-SIMULAZIONE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-FL-SIMULAZIONE
        //           ELSE
        //              MOVE 0 TO IND-B03-FL-SIMULAZIONE
        //           END-IF
        if (Conditions.eq(bilaTrchEstr.getB03FlSimulazione(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-B03-FL-SIMULAZIONE
            ws.getIndBilaTrchEstr().setFlSimulazione(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-FL-SIMULAZIONE
            ws.getIndBilaTrchEstr().setFlSimulazione(((short)0));
        }
        // COB_CODE: IF B03-DT-INI-VAL-TAR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DT-INI-VAL-TAR
        //           ELSE
        //              MOVE 0 TO IND-B03-DT-INI-VAL-TAR
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03DtIniValTar().getB03DtIniValTarNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DT-INI-VAL-TAR
            ws.getIndBilaTrchEstr().setDtIniValTar(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DT-INI-VAL-TAR
            ws.getIndBilaTrchEstr().setDtIniValTar(((short)0));
        }
        // COB_CODE: IF B03-COD-PROD-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-COD-PROD
        //           ELSE
        //              MOVE 0 TO IND-B03-COD-PROD
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CodProdFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-COD-PROD
            ws.getIndBilaTrchEstr().setCodProd(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-COD-PROD
            ws.getIndBilaTrchEstr().setCodProd(((short)0));
        }
        // COB_CODE: IF B03-COD-TARI-ORGN-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-COD-TARI-ORGN
        //           ELSE
        //              MOVE 0 TO IND-B03-COD-TARI-ORGN
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CodTariOrgnFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-COD-TARI-ORGN
            ws.getIndBilaTrchEstr().setCodTariOrgn(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-COD-TARI-ORGN
            ws.getIndBilaTrchEstr().setCodTariOrgn(((short)0));
        }
        // COB_CODE: IF B03-MIN-GARTO-T-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-MIN-GARTO-T
        //           ELSE
        //              MOVE 0 TO IND-B03-MIN-GARTO-T
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03MinGartoT().getB03MinGartoTNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-MIN-GARTO-T
            ws.getIndBilaTrchEstr().setMinGartoT(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-MIN-GARTO-T
            ws.getIndBilaTrchEstr().setMinGartoT(((short)0));
        }
        // COB_CODE: IF B03-TP-TARI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-TP-TARI
        //           ELSE
        //              MOVE 0 TO IND-B03-TP-TARI
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03TpTariFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-TP-TARI
            ws.getIndBilaTrchEstr().setTpTari(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-TP-TARI
            ws.getIndBilaTrchEstr().setTpTari(((short)0));
        }
        // COB_CODE: IF B03-TP-PRE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-TP-PRE
        //           ELSE
        //              MOVE 0 TO IND-B03-TP-PRE
        //           END-IF
        if (Conditions.eq(bilaTrchEstr.getB03TpPre(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-B03-TP-PRE
            ws.getIndBilaTrchEstr().setTpPre(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-TP-PRE
            ws.getIndBilaTrchEstr().setTpPre(((short)0));
        }
        // COB_CODE: IF B03-TP-ADEG-PRE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-TP-ADEG-PRE
        //           ELSE
        //              MOVE 0 TO IND-B03-TP-ADEG-PRE
        //           END-IF
        if (Conditions.eq(bilaTrchEstr.getB03TpAdegPre(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-B03-TP-ADEG-PRE
            ws.getIndBilaTrchEstr().setTpAdegPre(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-TP-ADEG-PRE
            ws.getIndBilaTrchEstr().setTpAdegPre(((short)0));
        }
        // COB_CODE: IF B03-TP-RIVAL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-TP-RIVAL
        //           ELSE
        //              MOVE 0 TO IND-B03-TP-RIVAL
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03TpRivalFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-TP-RIVAL
            ws.getIndBilaTrchEstr().setTpRival(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-TP-RIVAL
            ws.getIndBilaTrchEstr().setTpRival(((short)0));
        }
        // COB_CODE: IF B03-FL-DA-TRASF-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-FL-DA-TRASF
        //           ELSE
        //              MOVE 0 TO IND-B03-FL-DA-TRASF
        //           END-IF
        if (Conditions.eq(bilaTrchEstr.getB03FlDaTrasf(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-B03-FL-DA-TRASF
            ws.getIndBilaTrchEstr().setFlDaTrasf(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-FL-DA-TRASF
            ws.getIndBilaTrchEstr().setFlDaTrasf(((short)0));
        }
        // COB_CODE: IF B03-FL-CAR-CONT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-FL-CAR-CONT
        //           ELSE
        //              MOVE 0 TO IND-B03-FL-CAR-CONT
        //           END-IF
        if (Conditions.eq(bilaTrchEstr.getB03FlCarCont(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-B03-FL-CAR-CONT
            ws.getIndBilaTrchEstr().setFlCarCont(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-FL-CAR-CONT
            ws.getIndBilaTrchEstr().setFlCarCont(((short)0));
        }
        // COB_CODE: IF B03-FL-PRE-DA-RIS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-FL-PRE-DA-RIS
        //           ELSE
        //              MOVE 0 TO IND-B03-FL-PRE-DA-RIS
        //           END-IF
        if (Conditions.eq(bilaTrchEstr.getB03FlPreDaRis(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-B03-FL-PRE-DA-RIS
            ws.getIndBilaTrchEstr().setFlPreDaRis(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-FL-PRE-DA-RIS
            ws.getIndBilaTrchEstr().setFlPreDaRis(((short)0));
        }
        // COB_CODE: IF B03-FL-PRE-AGG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-FL-PRE-AGG
        //           ELSE
        //              MOVE 0 TO IND-B03-FL-PRE-AGG
        //           END-IF
        if (Conditions.eq(bilaTrchEstr.getB03FlPreAgg(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-B03-FL-PRE-AGG
            ws.getIndBilaTrchEstr().setFlPreAgg(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-FL-PRE-AGG
            ws.getIndBilaTrchEstr().setFlPreAgg(((short)0));
        }
        // COB_CODE: IF B03-TP-TRCH-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-TP-TRCH
        //           ELSE
        //              MOVE 0 TO IND-B03-TP-TRCH
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03TpTrchFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-TP-TRCH
            ws.getIndBilaTrchEstr().setTpTrch(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-TP-TRCH
            ws.getIndBilaTrchEstr().setTpTrch(((short)0));
        }
        // COB_CODE: IF B03-TP-TST-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-TP-TST
        //           ELSE
        //              MOVE 0 TO IND-B03-TP-TST
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03TpTstFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-TP-TST
            ws.getIndBilaTrchEstr().setTpTst(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-TP-TST
            ws.getIndBilaTrchEstr().setTpTst(((short)0));
        }
        // COB_CODE: IF B03-COD-CONV-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-COD-CONV
        //           ELSE
        //              MOVE 0 TO IND-B03-COD-CONV
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CodConvFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-COD-CONV
            ws.getIndBilaTrchEstr().setCodConv(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-COD-CONV
            ws.getIndBilaTrchEstr().setCodConv(((short)0));
        }
        // COB_CODE: IF B03-DT-DECOR-ADES-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DT-DECOR-ADES
        //           ELSE
        //              MOVE 0 TO IND-B03-DT-DECOR-ADES
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03DtDecorAdes().getB03DtDecorAdesNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DT-DECOR-ADES
            ws.getIndBilaTrchEstr().setDtDecorAdes(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DT-DECOR-ADES
            ws.getIndBilaTrchEstr().setDtDecorAdes(((short)0));
        }
        // COB_CODE: IF B03-DT-EMIS-TRCH-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DT-EMIS-TRCH
        //           ELSE
        //              MOVE 0 TO IND-B03-DT-EMIS-TRCH
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03DtEmisTrch().getB03DtEmisTrchNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DT-EMIS-TRCH
            ws.getIndBilaTrchEstr().setDtEmisTrch(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DT-EMIS-TRCH
            ws.getIndBilaTrchEstr().setDtEmisTrch(((short)0));
        }
        // COB_CODE: IF B03-DT-SCAD-TRCH-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DT-SCAD-TRCH
        //           ELSE
        //              MOVE 0 TO IND-B03-DT-SCAD-TRCH
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03DtScadTrch().getB03DtScadTrchNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DT-SCAD-TRCH
            ws.getIndBilaTrchEstr().setDtScadTrch(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DT-SCAD-TRCH
            ws.getIndBilaTrchEstr().setDtScadTrch(((short)0));
        }
        // COB_CODE: IF B03-DT-SCAD-INTMD-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DT-SCAD-INTMD
        //           ELSE
        //              MOVE 0 TO IND-B03-DT-SCAD-INTMD
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03DtScadIntmd().getB03DtScadIntmdNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DT-SCAD-INTMD
            ws.getIndBilaTrchEstr().setDtScadIntmd(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DT-SCAD-INTMD
            ws.getIndBilaTrchEstr().setDtScadIntmd(((short)0));
        }
        // COB_CODE: IF B03-DT-SCAD-PAG-PRE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DT-SCAD-PAG-PRE
        //           ELSE
        //              MOVE 0 TO IND-B03-DT-SCAD-PAG-PRE
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03DtScadPagPre().getB03DtScadPagPreNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DT-SCAD-PAG-PRE
            ws.getIndBilaTrchEstr().setDtScadPagPre(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DT-SCAD-PAG-PRE
            ws.getIndBilaTrchEstr().setDtScadPagPre(((short)0));
        }
        // COB_CODE: IF B03-DT-ULT-PRE-PAG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DT-ULT-PRE-PAG
        //           ELSE
        //              MOVE 0 TO IND-B03-DT-ULT-PRE-PAG
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03DtUltPrePag().getB03DtUltPrePagNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DT-ULT-PRE-PAG
            ws.getIndBilaTrchEstr().setDtUltPrePag(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DT-ULT-PRE-PAG
            ws.getIndBilaTrchEstr().setDtUltPrePag(((short)0));
        }
        // COB_CODE: IF B03-DT-NASC-1O-ASSTO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DT-NASC-1O-ASSTO
        //           ELSE
        //              MOVE 0 TO IND-B03-DT-NASC-1O-ASSTO
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03DtNasc1oAssto().getB03DtNasc1oAsstoNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DT-NASC-1O-ASSTO
            ws.getIndBilaTrchEstr().setDtNasc1oAssto(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DT-NASC-1O-ASSTO
            ws.getIndBilaTrchEstr().setDtNasc1oAssto(((short)0));
        }
        // COB_CODE: IF B03-SEX-1O-ASSTO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-SEX-1O-ASSTO
        //           ELSE
        //              MOVE 0 TO IND-B03-SEX-1O-ASSTO
        //           END-IF
        if (Conditions.eq(bilaTrchEstr.getB03Sex1oAssto(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-B03-SEX-1O-ASSTO
            ws.getIndBilaTrchEstr().setSex1oAssto(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-SEX-1O-ASSTO
            ws.getIndBilaTrchEstr().setSex1oAssto(((short)0));
        }
        // COB_CODE: IF B03-ETA-AA-1O-ASSTO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-ETA-AA-1O-ASSTO
        //           ELSE
        //              MOVE 0 TO IND-B03-ETA-AA-1O-ASSTO
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03EtaAa1oAssto().getB03EtaAa1oAsstoNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-ETA-AA-1O-ASSTO
            ws.getIndBilaTrchEstr().setEtaAa1oAssto(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-ETA-AA-1O-ASSTO
            ws.getIndBilaTrchEstr().setEtaAa1oAssto(((short)0));
        }
        // COB_CODE: IF B03-ETA-MM-1O-ASSTO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-ETA-MM-1O-ASSTO
        //           ELSE
        //              MOVE 0 TO IND-B03-ETA-MM-1O-ASSTO
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03EtaMm1oAssto().getB03EtaMm1oAsstoNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-ETA-MM-1O-ASSTO
            ws.getIndBilaTrchEstr().setEtaMm1oAssto(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-ETA-MM-1O-ASSTO
            ws.getIndBilaTrchEstr().setEtaMm1oAssto(((short)0));
        }
        // COB_CODE: IF B03-ETA-RAGGN-DT-CALC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-ETA-RAGGN-DT-CALC
        //           ELSE
        //              MOVE 0 TO IND-B03-ETA-RAGGN-DT-CALC
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03EtaRaggnDtCalc().getB03EtaRaggnDtCalcNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-ETA-RAGGN-DT-CALC
            ws.getIndBilaTrchEstr().setEtaRaggnDtCalc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-ETA-RAGGN-DT-CALC
            ws.getIndBilaTrchEstr().setEtaRaggnDtCalc(((short)0));
        }
        // COB_CODE: IF B03-DUR-AA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DUR-AA
        //           ELSE
        //              MOVE 0 TO IND-B03-DUR-AA
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03DurAa().getB03DurAaNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DUR-AA
            ws.getIndBilaTrchEstr().setDurAa(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DUR-AA
            ws.getIndBilaTrchEstr().setDurAa(((short)0));
        }
        // COB_CODE: IF B03-DUR-MM-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DUR-MM
        //           ELSE
        //              MOVE 0 TO IND-B03-DUR-MM
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03DurMm().getB03DurMmNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DUR-MM
            ws.getIndBilaTrchEstr().setDurMm(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DUR-MM
            ws.getIndBilaTrchEstr().setDurMm(((short)0));
        }
        // COB_CODE: IF B03-DUR-GG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DUR-GG
        //           ELSE
        //              MOVE 0 TO IND-B03-DUR-GG
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03DurGg().getB03DurGgNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DUR-GG
            ws.getIndBilaTrchEstr().setDurGg(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DUR-GG
            ws.getIndBilaTrchEstr().setDurGg(((short)0));
        }
        // COB_CODE: IF B03-DUR-1O-PER-AA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DUR-1O-PER-AA
        //           ELSE
        //              MOVE 0 TO IND-B03-DUR-1O-PER-AA
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03Dur1oPerAa().getB03Dur1oPerAaNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DUR-1O-PER-AA
            ws.getIndBilaTrchEstr().setDur1oPerAa(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DUR-1O-PER-AA
            ws.getIndBilaTrchEstr().setDur1oPerAa(((short)0));
        }
        // COB_CODE: IF B03-DUR-1O-PER-MM-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DUR-1O-PER-MM
        //           ELSE
        //              MOVE 0 TO IND-B03-DUR-1O-PER-MM
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03Dur1oPerMm().getB03Dur1oPerMmNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DUR-1O-PER-MM
            ws.getIndBilaTrchEstr().setDur1oPerMm(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DUR-1O-PER-MM
            ws.getIndBilaTrchEstr().setDur1oPerMm(((short)0));
        }
        // COB_CODE: IF B03-DUR-1O-PER-GG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DUR-1O-PER-GG
        //           ELSE
        //              MOVE 0 TO IND-B03-DUR-1O-PER-GG
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03Dur1oPerGg().getB03Dur1oPerGgNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DUR-1O-PER-GG
            ws.getIndBilaTrchEstr().setDur1oPerGg(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DUR-1O-PER-GG
            ws.getIndBilaTrchEstr().setDur1oPerGg(((short)0));
        }
        // COB_CODE: IF B03-ANTIDUR-RICOR-PREC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-ANTIDUR-RICOR-PREC
        //           ELSE
        //              MOVE 0 TO IND-B03-ANTIDUR-RICOR-PREC
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03AntidurRicorPrec().getB03AntidurRicorPrecNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-ANTIDUR-RICOR-PREC
            ws.getIndBilaTrchEstr().setAntidurRicorPrec(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-ANTIDUR-RICOR-PREC
            ws.getIndBilaTrchEstr().setAntidurRicorPrec(((short)0));
        }
        // COB_CODE: IF B03-ANTIDUR-DT-CALC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-ANTIDUR-DT-CALC
        //           ELSE
        //              MOVE 0 TO IND-B03-ANTIDUR-DT-CALC
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03AntidurDtCalc().getB03AntidurDtCalcNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-ANTIDUR-DT-CALC
            ws.getIndBilaTrchEstr().setAntidurDtCalc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-ANTIDUR-DT-CALC
            ws.getIndBilaTrchEstr().setAntidurDtCalc(((short)0));
        }
        // COB_CODE: IF B03-DUR-RES-DT-CALC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DUR-RES-DT-CALC
        //           ELSE
        //              MOVE 0 TO IND-B03-DUR-RES-DT-CALC
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03DurResDtCalc().getB03DurResDtCalcNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DUR-RES-DT-CALC
            ws.getIndBilaTrchEstr().setDurResDtCalc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DUR-RES-DT-CALC
            ws.getIndBilaTrchEstr().setDurResDtCalc(((short)0));
        }
        // COB_CODE: IF B03-DT-EFF-CAMB-STAT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DT-EFF-CAMB-STAT
        //           ELSE
        //              MOVE 0 TO IND-B03-DT-EFF-CAMB-STAT
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03DtEffCambStat().getB03DtEffCambStatNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DT-EFF-CAMB-STAT
            ws.getIndBilaTrchEstr().setDtEffCambStat(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DT-EFF-CAMB-STAT
            ws.getIndBilaTrchEstr().setDtEffCambStat(((short)0));
        }
        // COB_CODE: IF B03-DT-EMIS-CAMB-STAT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DT-EMIS-CAMB-STAT
        //           ELSE
        //              MOVE 0 TO IND-B03-DT-EMIS-CAMB-STAT
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03DtEmisCambStat().getB03DtEmisCambStatNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DT-EMIS-CAMB-STAT
            ws.getIndBilaTrchEstr().setDtEmisCambStat(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DT-EMIS-CAMB-STAT
            ws.getIndBilaTrchEstr().setDtEmisCambStat(((short)0));
        }
        // COB_CODE: IF B03-DT-EFF-STAB-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DT-EFF-STAB
        //           ELSE
        //              MOVE 0 TO IND-B03-DT-EFF-STAB
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03DtEffStab().getB03DtEffStabNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DT-EFF-STAB
            ws.getIndBilaTrchEstr().setDtEffStab(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DT-EFF-STAB
            ws.getIndBilaTrchEstr().setDtEffStab(((short)0));
        }
        // COB_CODE: IF B03-CPT-DT-STAB-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-CPT-DT-STAB
        //           ELSE
        //              MOVE 0 TO IND-B03-CPT-DT-STAB
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CptDtStab().getB03CptDtStabNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-CPT-DT-STAB
            ws.getIndBilaTrchEstr().setCptDtStab(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-CPT-DT-STAB
            ws.getIndBilaTrchEstr().setCptDtStab(((short)0));
        }
        // COB_CODE: IF B03-DT-EFF-RIDZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DT-EFF-RIDZ
        //           ELSE
        //              MOVE 0 TO IND-B03-DT-EFF-RIDZ
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03DtEffRidz().getB03DtEffRidzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DT-EFF-RIDZ
            ws.getIndBilaTrchEstr().setDtEffRidz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DT-EFF-RIDZ
            ws.getIndBilaTrchEstr().setDtEffRidz(((short)0));
        }
        // COB_CODE: IF B03-DT-EMIS-RIDZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DT-EMIS-RIDZ
        //           ELSE
        //              MOVE 0 TO IND-B03-DT-EMIS-RIDZ
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03DtEmisRidz().getB03DtEmisRidzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DT-EMIS-RIDZ
            ws.getIndBilaTrchEstr().setDtEmisRidz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DT-EMIS-RIDZ
            ws.getIndBilaTrchEstr().setDtEmisRidz(((short)0));
        }
        // COB_CODE: IF B03-CPT-DT-RIDZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-CPT-DT-RIDZ
        //           ELSE
        //              MOVE 0 TO IND-B03-CPT-DT-RIDZ
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CptDtRidz().getB03CptDtRidzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-CPT-DT-RIDZ
            ws.getIndBilaTrchEstr().setCptDtRidz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-CPT-DT-RIDZ
            ws.getIndBilaTrchEstr().setCptDtRidz(((short)0));
        }
        // COB_CODE: IF B03-FRAZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-FRAZ
        //           ELSE
        //              MOVE 0 TO IND-B03-FRAZ
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03Fraz().getB03FrazNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-FRAZ
            ws.getIndBilaTrchEstr().setFraz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-FRAZ
            ws.getIndBilaTrchEstr().setFraz(((short)0));
        }
        // COB_CODE: IF B03-DUR-PAG-PRE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DUR-PAG-PRE
        //           ELSE
        //              MOVE 0 TO IND-B03-DUR-PAG-PRE
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03DurPagPre().getB03DurPagPreNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DUR-PAG-PRE
            ws.getIndBilaTrchEstr().setDurPagPre(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DUR-PAG-PRE
            ws.getIndBilaTrchEstr().setDurPagPre(((short)0));
        }
        // COB_CODE: IF B03-NUM-PRE-PATT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-NUM-PRE-PATT
        //           ELSE
        //              MOVE 0 TO IND-B03-NUM-PRE-PATT
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03NumPrePatt().getB03NumPrePattNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-NUM-PRE-PATT
            ws.getIndBilaTrchEstr().setNumPrePatt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-NUM-PRE-PATT
            ws.getIndBilaTrchEstr().setNumPrePatt(((short)0));
        }
        // COB_CODE: IF B03-FRAZ-INI-EROG-REN-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-FRAZ-INI-EROG-REN
        //           ELSE
        //              MOVE 0 TO IND-B03-FRAZ-INI-EROG-REN
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03FrazIniErogRen().getB03FrazIniErogRenNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-FRAZ-INI-EROG-REN
            ws.getIndBilaTrchEstr().setFrazIniErogRen(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-FRAZ-INI-EROG-REN
            ws.getIndBilaTrchEstr().setFrazIniErogRen(((short)0));
        }
        // COB_CODE: IF B03-AA-REN-CER-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-AA-REN-CER
        //           ELSE
        //              MOVE 0 TO IND-B03-AA-REN-CER
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03AaRenCer().getB03AaRenCerNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-AA-REN-CER
            ws.getIndBilaTrchEstr().setAaRenCer(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-AA-REN-CER
            ws.getIndBilaTrchEstr().setAaRenCer(((short)0));
        }
        // COB_CODE: IF B03-RAT-REN-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-RAT-REN
        //           ELSE
        //              MOVE 0 TO IND-B03-RAT-REN
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03RatRen().getB03RatRenNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-RAT-REN
            ws.getIndBilaTrchEstr().setRatRen(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-RAT-REN
            ws.getIndBilaTrchEstr().setRatRen(((short)0));
        }
        // COB_CODE: IF B03-COD-DIV-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-COD-DIV
        //           ELSE
        //              MOVE 0 TO IND-B03-COD-DIV
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CodDivFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-COD-DIV
            ws.getIndBilaTrchEstr().setCodDiv(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-COD-DIV
            ws.getIndBilaTrchEstr().setCodDiv(((short)0));
        }
        // COB_CODE: IF B03-RISCPAR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-RISCPAR
        //           ELSE
        //              MOVE 0 TO IND-B03-RISCPAR
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03Riscpar().getB03RiscparNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-RISCPAR
            ws.getIndBilaTrchEstr().setRiscpar(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-RISCPAR
            ws.getIndBilaTrchEstr().setRiscpar(((short)0));
        }
        // COB_CODE: IF B03-CUM-RISCPAR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-CUM-RISCPAR
        //           ELSE
        //              MOVE 0 TO IND-B03-CUM-RISCPAR
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CumRiscpar().getB03CumRiscparNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-CUM-RISCPAR
            ws.getIndBilaTrchEstr().setCumRiscpar(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-CUM-RISCPAR
            ws.getIndBilaTrchEstr().setCumRiscpar(((short)0));
        }
        // COB_CODE: IF B03-ULT-RM-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-ULT-RM
        //           ELSE
        //              MOVE 0 TO IND-B03-ULT-RM
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03UltRm().getB03UltRmNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-ULT-RM
            ws.getIndBilaTrchEstr().setUltRm(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-ULT-RM
            ws.getIndBilaTrchEstr().setUltRm(((short)0));
        }
        // COB_CODE: IF B03-TS-RENDTO-T-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-TS-RENDTO-T
        //           ELSE
        //              MOVE 0 TO IND-B03-TS-RENDTO-T
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03TsRendtoT().getB03TsRendtoTNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-TS-RENDTO-T
            ws.getIndBilaTrchEstr().setTsRendtoT(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-TS-RENDTO-T
            ws.getIndBilaTrchEstr().setTsRendtoT(((short)0));
        }
        // COB_CODE: IF B03-ALQ-RETR-T-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-ALQ-RETR-T
        //           ELSE
        //              MOVE 0 TO IND-B03-ALQ-RETR-T
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03AlqRetrT().getB03AlqRetrTNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-ALQ-RETR-T
            ws.getIndBilaTrchEstr().setAlqRetrT(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-ALQ-RETR-T
            ws.getIndBilaTrchEstr().setAlqRetrT(((short)0));
        }
        // COB_CODE: IF B03-MIN-TRNUT-T-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-MIN-TRNUT-T
        //           ELSE
        //              MOVE 0 TO IND-B03-MIN-TRNUT-T
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03MinTrnutT().getB03MinTrnutTNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-MIN-TRNUT-T
            ws.getIndBilaTrchEstr().setMinTrnutT(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-MIN-TRNUT-T
            ws.getIndBilaTrchEstr().setMinTrnutT(((short)0));
        }
        // COB_CODE: IF B03-TS-NET-T-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-TS-NET-T
        //           ELSE
        //              MOVE 0 TO IND-B03-TS-NET-T
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03TsNetT().getB03TsNetTNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-TS-NET-T
            ws.getIndBilaTrchEstr().setTsNetT(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-TS-NET-T
            ws.getIndBilaTrchEstr().setTsNetT(((short)0));
        }
        // COB_CODE: IF B03-DT-ULT-RIVAL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DT-ULT-RIVAL
        //           ELSE
        //              MOVE 0 TO IND-B03-DT-ULT-RIVAL
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03DtUltRival().getB03DtUltRivalNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DT-ULT-RIVAL
            ws.getIndBilaTrchEstr().setDtUltRival(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DT-ULT-RIVAL
            ws.getIndBilaTrchEstr().setDtUltRival(((short)0));
        }
        // COB_CODE: IF B03-PRSTZ-INI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-PRSTZ-INI
        //           ELSE
        //              MOVE 0 TO IND-B03-PRSTZ-INI
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03PrstzIni().getB03PrstzIniNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-PRSTZ-INI
            ws.getIndBilaTrchEstr().setPrstzIni(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-PRSTZ-INI
            ws.getIndBilaTrchEstr().setPrstzIni(((short)0));
        }
        // COB_CODE: IF B03-PRSTZ-AGG-INI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-PRSTZ-AGG-INI
        //           ELSE
        //              MOVE 0 TO IND-B03-PRSTZ-AGG-INI
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03PrstzAggIni().getB03PrstzAggIniNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-PRSTZ-AGG-INI
            ws.getIndBilaTrchEstr().setPrstzAggIni(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-PRSTZ-AGG-INI
            ws.getIndBilaTrchEstr().setPrstzAggIni(((short)0));
        }
        // COB_CODE: IF B03-PRSTZ-AGG-ULT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-PRSTZ-AGG-ULT
        //           ELSE
        //              MOVE 0 TO IND-B03-PRSTZ-AGG-ULT
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03PrstzAggUlt().getB03PrstzAggUltNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-PRSTZ-AGG-ULT
            ws.getIndBilaTrchEstr().setPrstzAggUlt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-PRSTZ-AGG-ULT
            ws.getIndBilaTrchEstr().setPrstzAggUlt(((short)0));
        }
        // COB_CODE: IF B03-RAPPEL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-RAPPEL
        //           ELSE
        //              MOVE 0 TO IND-B03-RAPPEL
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03Rappel().getB03RappelNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-RAPPEL
            ws.getIndBilaTrchEstr().setRappel(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-RAPPEL
            ws.getIndBilaTrchEstr().setRappel(((short)0));
        }
        // COB_CODE: IF B03-PRE-PATTUITO-INI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-PRE-PATTUITO-INI
        //           ELSE
        //              MOVE 0 TO IND-B03-PRE-PATTUITO-INI
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03PrePattuitoIni().getB03PrePattuitoIniNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-PRE-PATTUITO-INI
            ws.getIndBilaTrchEstr().setPrePattuitoIni(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-PRE-PATTUITO-INI
            ws.getIndBilaTrchEstr().setPrePattuitoIni(((short)0));
        }
        // COB_CODE: IF B03-PRE-DOV-INI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-PRE-DOV-INI
        //           ELSE
        //              MOVE 0 TO IND-B03-PRE-DOV-INI
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03PreDovIni().getB03PreDovIniNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-PRE-DOV-INI
            ws.getIndBilaTrchEstr().setPreDovIni(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-PRE-DOV-INI
            ws.getIndBilaTrchEstr().setPreDovIni(((short)0));
        }
        // COB_CODE: IF B03-PRE-DOV-RIVTO-T-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-PRE-DOV-RIVTO-T
        //           ELSE
        //              MOVE 0 TO IND-B03-PRE-DOV-RIVTO-T
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03PreDovRivtoT().getB03PreDovRivtoTNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-PRE-DOV-RIVTO-T
            ws.getIndBilaTrchEstr().setPreDovRivtoT(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-PRE-DOV-RIVTO-T
            ws.getIndBilaTrchEstr().setPreDovRivtoT(((short)0));
        }
        // COB_CODE: IF B03-PRE-ANNUALIZ-RICOR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-PRE-ANNUALIZ-RICOR
        //           ELSE
        //              MOVE 0 TO IND-B03-PRE-ANNUALIZ-RICOR
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03PreAnnualizRicor().getB03PreAnnualizRicorNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-PRE-ANNUALIZ-RICOR
            ws.getIndBilaTrchEstr().setPreAnnualizRicor(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-PRE-ANNUALIZ-RICOR
            ws.getIndBilaTrchEstr().setPreAnnualizRicor(((short)0));
        }
        // COB_CODE: IF B03-PRE-CONT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-PRE-CONT
        //           ELSE
        //              MOVE 0 TO IND-B03-PRE-CONT
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03PreCont().getB03PreContNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-PRE-CONT
            ws.getIndBilaTrchEstr().setPreCont(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-PRE-CONT
            ws.getIndBilaTrchEstr().setPreCont(((short)0));
        }
        // COB_CODE: IF B03-PRE-PP-INI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-PRE-PP-INI
        //           ELSE
        //              MOVE 0 TO IND-B03-PRE-PP-INI
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03PrePpIni().getB03PrePpIniNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-PRE-PP-INI
            ws.getIndBilaTrchEstr().setPrePpIni(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-PRE-PP-INI
            ws.getIndBilaTrchEstr().setPrePpIni(((short)0));
        }
        // COB_CODE: IF B03-RIS-PURA-T-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-RIS-PURA-T
        //           ELSE
        //              MOVE 0 TO IND-B03-RIS-PURA-T
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03RisPuraT().getB03RisPuraTNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-RIS-PURA-T
            ws.getIndBilaTrchEstr().setRisPuraT(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-RIS-PURA-T
            ws.getIndBilaTrchEstr().setRisPuraT(((short)0));
        }
        // COB_CODE: IF B03-PROV-ACQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-PROV-ACQ
        //           ELSE
        //              MOVE 0 TO IND-B03-PROV-ACQ
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03ProvAcq().getB03ProvAcqNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-PROV-ACQ
            ws.getIndBilaTrchEstr().setProvAcq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-PROV-ACQ
            ws.getIndBilaTrchEstr().setProvAcq(((short)0));
        }
        // COB_CODE: IF B03-PROV-ACQ-RICOR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-PROV-ACQ-RICOR
        //           ELSE
        //              MOVE 0 TO IND-B03-PROV-ACQ-RICOR
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03ProvAcqRicor().getB03ProvAcqRicorNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-PROV-ACQ-RICOR
            ws.getIndBilaTrchEstr().setProvAcqRicor(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-PROV-ACQ-RICOR
            ws.getIndBilaTrchEstr().setProvAcqRicor(((short)0));
        }
        // COB_CODE: IF B03-PROV-INC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-PROV-INC
        //           ELSE
        //              MOVE 0 TO IND-B03-PROV-INC
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03ProvInc().getB03ProvIncNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-PROV-INC
            ws.getIndBilaTrchEstr().setProvInc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-PROV-INC
            ws.getIndBilaTrchEstr().setProvInc(((short)0));
        }
        // COB_CODE: IF B03-CAR-ACQ-NON-SCON-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-CAR-ACQ-NON-SCON
        //           ELSE
        //              MOVE 0 TO IND-B03-CAR-ACQ-NON-SCON
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CarAcqNonScon().getB03CarAcqNonSconNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-CAR-ACQ-NON-SCON
            ws.getIndBilaTrchEstr().setCarAcqNonScon(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-CAR-ACQ-NON-SCON
            ws.getIndBilaTrchEstr().setCarAcqNonScon(((short)0));
        }
        // COB_CODE: IF B03-OVER-COMM-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-OVER-COMM
        //           ELSE
        //              MOVE 0 TO IND-B03-OVER-COMM
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03OverComm().getB03OverCommNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-OVER-COMM
            ws.getIndBilaTrchEstr().setOverComm(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-OVER-COMM
            ws.getIndBilaTrchEstr().setOverComm(((short)0));
        }
        // COB_CODE: IF B03-CAR-ACQ-PRECONTATO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-CAR-ACQ-PRECONTATO
        //           ELSE
        //              MOVE 0 TO IND-B03-CAR-ACQ-PRECONTATO
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CarAcqPrecontato().getB03CarAcqPrecontatoNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-CAR-ACQ-PRECONTATO
            ws.getIndBilaTrchEstr().setCarAcqPrecontato(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-CAR-ACQ-PRECONTATO
            ws.getIndBilaTrchEstr().setCarAcqPrecontato(((short)0));
        }
        // COB_CODE: IF B03-RIS-ACQ-T-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-RIS-ACQ-T
        //           ELSE
        //              MOVE 0 TO IND-B03-RIS-ACQ-T
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03RisAcqT().getB03RisAcqTNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-RIS-ACQ-T
            ws.getIndBilaTrchEstr().setRisAcqT(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-RIS-ACQ-T
            ws.getIndBilaTrchEstr().setRisAcqT(((short)0));
        }
        // COB_CODE: IF B03-RIS-ZIL-T-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-RIS-ZIL-T
        //           ELSE
        //              MOVE 0 TO IND-B03-RIS-ZIL-T
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03RisZilT().getB03RisZilTNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-RIS-ZIL-T
            ws.getIndBilaTrchEstr().setRisZilT(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-RIS-ZIL-T
            ws.getIndBilaTrchEstr().setRisZilT(((short)0));
        }
        // COB_CODE: IF B03-CAR-GEST-NON-SCON-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-CAR-GEST-NON-SCON
        //           ELSE
        //              MOVE 0 TO IND-B03-CAR-GEST-NON-SCON
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CarGestNonScon().getB03CarGestNonSconNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-CAR-GEST-NON-SCON
            ws.getIndBilaTrchEstr().setCarGestNonScon(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-CAR-GEST-NON-SCON
            ws.getIndBilaTrchEstr().setCarGestNonScon(((short)0));
        }
        // COB_CODE: IF B03-CAR-GEST-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-CAR-GEST
        //           ELSE
        //              MOVE 0 TO IND-B03-CAR-GEST
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CarGest().getB03CarGestNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-CAR-GEST
            ws.getIndBilaTrchEstr().setCarGest(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-CAR-GEST
            ws.getIndBilaTrchEstr().setCarGest(((short)0));
        }
        // COB_CODE: IF B03-RIS-SPE-T-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-RIS-SPE-T
        //           ELSE
        //              MOVE 0 TO IND-B03-RIS-SPE-T
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03RisSpeT().getB03RisSpeTNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-RIS-SPE-T
            ws.getIndBilaTrchEstr().setRisSpeT(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-RIS-SPE-T
            ws.getIndBilaTrchEstr().setRisSpeT(((short)0));
        }
        // COB_CODE: IF B03-CAR-INC-NON-SCON-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-CAR-INC-NON-SCON
        //           ELSE
        //              MOVE 0 TO IND-B03-CAR-INC-NON-SCON
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CarIncNonScon().getB03CarIncNonSconNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-CAR-INC-NON-SCON
            ws.getIndBilaTrchEstr().setCarIncNonScon(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-CAR-INC-NON-SCON
            ws.getIndBilaTrchEstr().setCarIncNonScon(((short)0));
        }
        // COB_CODE: IF B03-CAR-INC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-CAR-INC
        //           ELSE
        //              MOVE 0 TO IND-B03-CAR-INC
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CarInc().getB03CarIncNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-CAR-INC
            ws.getIndBilaTrchEstr().setCarInc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-CAR-INC
            ws.getIndBilaTrchEstr().setCarInc(((short)0));
        }
        // COB_CODE: IF B03-RIS-RISTORNI-CAP-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-RIS-RISTORNI-CAP
        //           ELSE
        //              MOVE 0 TO IND-B03-RIS-RISTORNI-CAP
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03RisRistorniCap().getB03RisRistorniCapNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-RIS-RISTORNI-CAP
            ws.getIndBilaTrchEstr().setRisRistorniCap(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-RIS-RISTORNI-CAP
            ws.getIndBilaTrchEstr().setRisRistorniCap(((short)0));
        }
        // COB_CODE: IF B03-INTR-TECN-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-INTR-TECN
        //           ELSE
        //              MOVE 0 TO IND-B03-INTR-TECN
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03IntrTecn().getB03IntrTecnNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-INTR-TECN
            ws.getIndBilaTrchEstr().setIntrTecn(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-INTR-TECN
            ws.getIndBilaTrchEstr().setIntrTecn(((short)0));
        }
        // COB_CODE: IF B03-CPT-RSH-MOR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-CPT-RSH-MOR
        //           ELSE
        //              MOVE 0 TO IND-B03-CPT-RSH-MOR
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CptRshMor().getB03CptRshMorNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-CPT-RSH-MOR
            ws.getIndBilaTrchEstr().setCptRshMor(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-CPT-RSH-MOR
            ws.getIndBilaTrchEstr().setCptRshMor(((short)0));
        }
        // COB_CODE: IF B03-C-SUBRSH-T-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-C-SUBRSH-T
        //           ELSE
        //              MOVE 0 TO IND-B03-C-SUBRSH-T
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CSubrshT().getB03CSubrshTNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-C-SUBRSH-T
            ws.getIndBilaTrchEstr().setcSubrshT(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-C-SUBRSH-T
            ws.getIndBilaTrchEstr().setcSubrshT(((short)0));
        }
        // COB_CODE: IF B03-PRE-RSH-T-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-PRE-RSH-T
        //           ELSE
        //              MOVE 0 TO IND-B03-PRE-RSH-T
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03PreRshT().getB03PreRshTNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-PRE-RSH-T
            ws.getIndBilaTrchEstr().setPreRshT(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-PRE-RSH-T
            ws.getIndBilaTrchEstr().setPreRshT(((short)0));
        }
        // COB_CODE: IF B03-ALQ-MARG-RIS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-ALQ-MARG-RIS
        //           ELSE
        //              MOVE 0 TO IND-B03-ALQ-MARG-RIS
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03AlqMargRis().getB03AlqMargRisNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-ALQ-MARG-RIS
            ws.getIndBilaTrchEstr().setAlqMargRis(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-ALQ-MARG-RIS
            ws.getIndBilaTrchEstr().setAlqMargRis(((short)0));
        }
        // COB_CODE: IF B03-ALQ-MARG-C-SUBRSH-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-ALQ-MARG-C-SUBRSH
        //           ELSE
        //              MOVE 0 TO IND-B03-ALQ-MARG-C-SUBRSH
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03AlqMargCSubrsh().getB03AlqMargCSubrshNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-ALQ-MARG-C-SUBRSH
            ws.getIndBilaTrchEstr().setAlqMargCSubrsh(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-ALQ-MARG-C-SUBRSH
            ws.getIndBilaTrchEstr().setAlqMargCSubrsh(((short)0));
        }
        // COB_CODE: IF B03-TS-RENDTO-SPPR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-TS-RENDTO-SPPR
        //           ELSE
        //              MOVE 0 TO IND-B03-TS-RENDTO-SPPR
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03TsRendtoSppr().getB03TsRendtoSpprNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-TS-RENDTO-SPPR
            ws.getIndBilaTrchEstr().setTsRendtoSppr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-TS-RENDTO-SPPR
            ws.getIndBilaTrchEstr().setTsRendtoSppr(((short)0));
        }
        // COB_CODE: IF B03-TP-IAS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-TP-IAS
        //           ELSE
        //              MOVE 0 TO IND-B03-TP-IAS
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03TpIasFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-TP-IAS
            ws.getIndBilaTrchEstr().setTpIas(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-TP-IAS
            ws.getIndBilaTrchEstr().setTpIas(((short)0));
        }
        // COB_CODE: IF B03-NS-QUO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-NS-QUO
        //           ELSE
        //              MOVE 0 TO IND-B03-NS-QUO
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03NsQuo().getB03NsQuoNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-NS-QUO
            ws.getIndBilaTrchEstr().setNsQuo(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-NS-QUO
            ws.getIndBilaTrchEstr().setNsQuo(((short)0));
        }
        // COB_CODE: IF B03-TS-MEDIO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-TS-MEDIO
        //           ELSE
        //              MOVE 0 TO IND-B03-TS-MEDIO
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03TsMedio().getB03TsMedioNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-TS-MEDIO
            ws.getIndBilaTrchEstr().setTsMedio(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-TS-MEDIO
            ws.getIndBilaTrchEstr().setTsMedio(((short)0));
        }
        // COB_CODE: IF B03-CPT-RIASTO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-CPT-RIASTO
        //           ELSE
        //              MOVE 0 TO IND-B03-CPT-RIASTO
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CptRiasto().getB03CptRiastoNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-CPT-RIASTO
            ws.getIndBilaTrchEstr().setCptRiasto(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-CPT-RIASTO
            ws.getIndBilaTrchEstr().setCptRiasto(((short)0));
        }
        // COB_CODE: IF B03-PRE-RIASTO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-PRE-RIASTO
        //           ELSE
        //              MOVE 0 TO IND-B03-PRE-RIASTO
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03PreRiasto().getB03PreRiastoNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-PRE-RIASTO
            ws.getIndBilaTrchEstr().setPreRiasto(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-PRE-RIASTO
            ws.getIndBilaTrchEstr().setPreRiasto(((short)0));
        }
        // COB_CODE: IF B03-RIS-RIASTA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-RIS-RIASTA
        //           ELSE
        //              MOVE 0 TO IND-B03-RIS-RIASTA
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03RisRiasta().getB03RisRiastaNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-RIS-RIASTA
            ws.getIndBilaTrchEstr().setRisRiasta(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-RIS-RIASTA
            ws.getIndBilaTrchEstr().setRisRiasta(((short)0));
        }
        // COB_CODE: IF B03-CPT-RIASTO-ECC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-CPT-RIASTO-ECC
        //           ELSE
        //              MOVE 0 TO IND-B03-CPT-RIASTO-ECC
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CptRiastoEcc().getB03CptRiastoEccNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-CPT-RIASTO-ECC
            ws.getIndBilaTrchEstr().setCptRiastoEcc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-CPT-RIASTO-ECC
            ws.getIndBilaTrchEstr().setCptRiastoEcc(((short)0));
        }
        // COB_CODE: IF B03-PRE-RIASTO-ECC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-PRE-RIASTO-ECC
        //           ELSE
        //              MOVE 0 TO IND-B03-PRE-RIASTO-ECC
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03PreRiastoEcc().getB03PreRiastoEccNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-PRE-RIASTO-ECC
            ws.getIndBilaTrchEstr().setPreRiastoEcc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-PRE-RIASTO-ECC
            ws.getIndBilaTrchEstr().setPreRiastoEcc(((short)0));
        }
        // COB_CODE: IF B03-RIS-RIASTA-ECC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-RIS-RIASTA-ECC
        //           ELSE
        //              MOVE 0 TO IND-B03-RIS-RIASTA-ECC
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03RisRiastaEcc().getB03RisRiastaEccNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-RIS-RIASTA-ECC
            ws.getIndBilaTrchEstr().setRisRiastaEcc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-RIS-RIASTA-ECC
            ws.getIndBilaTrchEstr().setRisRiastaEcc(((short)0));
        }
        // COB_CODE: IF B03-COD-AGE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-COD-AGE
        //           ELSE
        //              MOVE 0 TO IND-B03-COD-AGE
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CodAge().getB03CodAgeNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-COD-AGE
            ws.getIndBilaTrchEstr().setCodAge(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-COD-AGE
            ws.getIndBilaTrchEstr().setCodAge(((short)0));
        }
        // COB_CODE: IF B03-COD-SUBAGE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-COD-SUBAGE
        //           ELSE
        //              MOVE 0 TO IND-B03-COD-SUBAGE
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CodSubage().getB03CodSubageNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-COD-SUBAGE
            ws.getIndBilaTrchEstr().setCodSubage(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-COD-SUBAGE
            ws.getIndBilaTrchEstr().setCodSubage(((short)0));
        }
        // COB_CODE: IF B03-COD-CAN-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-COD-CAN
        //           ELSE
        //              MOVE 0 TO IND-B03-COD-CAN
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CodCan().getB03CodCanNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-COD-CAN
            ws.getIndBilaTrchEstr().setCodCan(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-COD-CAN
            ws.getIndBilaTrchEstr().setCodCan(((short)0));
        }
        // COB_CODE: IF B03-IB-POLI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-IB-POLI
        //           ELSE
        //              MOVE 0 TO IND-B03-IB-POLI
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03IbPoli(), BilaTrchEstrIdbsb030.Len.B03_IB_POLI)) {
            // COB_CODE: MOVE -1 TO IND-B03-IB-POLI
            ws.getIndBilaTrchEstr().setIbPoli(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-IB-POLI
            ws.getIndBilaTrchEstr().setIbPoli(((short)0));
        }
        // COB_CODE: IF B03-IB-ADES-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-IB-ADES
        //           ELSE
        //              MOVE 0 TO IND-B03-IB-ADES
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03IbAdes(), BilaTrchEstrIdbsb030.Len.B03_IB_ADES)) {
            // COB_CODE: MOVE -1 TO IND-B03-IB-ADES
            ws.getIndBilaTrchEstr().setIbAdes(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-IB-ADES
            ws.getIndBilaTrchEstr().setIbAdes(((short)0));
        }
        // COB_CODE: IF B03-IB-TRCH-DI-GAR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-IB-TRCH-DI-GAR
        //           ELSE
        //              MOVE 0 TO IND-B03-IB-TRCH-DI-GAR
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03IbTrchDiGar(), BilaTrchEstrIdbsb030.Len.B03_IB_TRCH_DI_GAR)) {
            // COB_CODE: MOVE -1 TO IND-B03-IB-TRCH-DI-GAR
            ws.getIndBilaTrchEstr().setIbTrchDiGar(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-IB-TRCH-DI-GAR
            ws.getIndBilaTrchEstr().setIbTrchDiGar(((short)0));
        }
        // COB_CODE: IF B03-TP-PRSTZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-TP-PRSTZ
        //           ELSE
        //              MOVE 0 TO IND-B03-TP-PRSTZ
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03TpPrstzFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-TP-PRSTZ
            ws.getIndBilaTrchEstr().setTpPrstz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-TP-PRSTZ
            ws.getIndBilaTrchEstr().setTpPrstz(((short)0));
        }
        // COB_CODE: IF B03-TP-TRASF-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-TP-TRASF
        //           ELSE
        //              MOVE 0 TO IND-B03-TP-TRASF
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03TpTrasfFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-TP-TRASF
            ws.getIndBilaTrchEstr().setTpTrasf(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-TP-TRASF
            ws.getIndBilaTrchEstr().setTpTrasf(((short)0));
        }
        // COB_CODE: IF B03-PP-INVRIO-TARI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-PP-INVRIO-TARI
        //           ELSE
        //              MOVE 0 TO IND-B03-PP-INVRIO-TARI
        //           END-IF
        if (Conditions.eq(bilaTrchEstr.getB03PpInvrioTari(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-B03-PP-INVRIO-TARI
            ws.getIndBilaTrchEstr().setPpInvrioTari(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-PP-INVRIO-TARI
            ws.getIndBilaTrchEstr().setPpInvrioTari(((short)0));
        }
        // COB_CODE: IF B03-COEFF-OPZ-REN-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-COEFF-OPZ-REN
        //           ELSE
        //              MOVE 0 TO IND-B03-COEFF-OPZ-REN
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CoeffOpzRen().getB03CoeffOpzRenNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-COEFF-OPZ-REN
            ws.getIndBilaTrchEstr().setCoeffOpzRen(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-COEFF-OPZ-REN
            ws.getIndBilaTrchEstr().setCoeffOpzRen(((short)0));
        }
        // COB_CODE: IF B03-COEFF-OPZ-CPT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-COEFF-OPZ-CPT
        //           ELSE
        //              MOVE 0 TO IND-B03-COEFF-OPZ-CPT
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CoeffOpzCpt().getB03CoeffOpzCptNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-COEFF-OPZ-CPT
            ws.getIndBilaTrchEstr().setCoeffOpzCpt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-COEFF-OPZ-CPT
            ws.getIndBilaTrchEstr().setCoeffOpzCpt(((short)0));
        }
        // COB_CODE: IF B03-DUR-PAG-REN-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DUR-PAG-REN
        //           ELSE
        //              MOVE 0 TO IND-B03-DUR-PAG-REN
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03DurPagRen().getB03DurPagRenNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DUR-PAG-REN
            ws.getIndBilaTrchEstr().setDurPagRen(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DUR-PAG-REN
            ws.getIndBilaTrchEstr().setDurPagRen(((short)0));
        }
        // COB_CODE: IF B03-VLT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-VLT
        //           ELSE
        //              MOVE 0 TO IND-B03-VLT
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03VltFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-VLT
            ws.getIndBilaTrchEstr().setVlt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-VLT
            ws.getIndBilaTrchEstr().setVlt(((short)0));
        }
        // COB_CODE: IF B03-RIS-MAT-CHIU-PREC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-RIS-MAT-CHIU-PREC
        //           ELSE
        //              MOVE 0 TO IND-B03-RIS-MAT-CHIU-PREC
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03RisMatChiuPrec().getB03RisMatChiuPrecNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-RIS-MAT-CHIU-PREC
            ws.getIndBilaTrchEstr().setRisMatChiuPrec(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-RIS-MAT-CHIU-PREC
            ws.getIndBilaTrchEstr().setRisMatChiuPrec(((short)0));
        }
        // COB_CODE: IF B03-COD-FND-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-COD-FND
        //           ELSE
        //              MOVE 0 TO IND-B03-COD-FND
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CodFndFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-COD-FND
            ws.getIndBilaTrchEstr().setCodFnd(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-COD-FND
            ws.getIndBilaTrchEstr().setCodFnd(((short)0));
        }
        // COB_CODE: IF B03-PRSTZ-T-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-PRSTZ-T
        //           ELSE
        //              MOVE 0 TO IND-B03-PRSTZ-T
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03PrstzT().getB03PrstzTNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-PRSTZ-T
            ws.getIndBilaTrchEstr().setPrstzT(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-PRSTZ-T
            ws.getIndBilaTrchEstr().setPrstzT(((short)0));
        }
        // COB_CODE: IF B03-TS-TARI-DOV-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-TS-TARI-DOV
        //           ELSE
        //              MOVE 0 TO IND-B03-TS-TARI-DOV
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03TsTariDov().getB03TsTariDovNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-TS-TARI-DOV
            ws.getIndBilaTrchEstr().setTsTariDov(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-TS-TARI-DOV
            ws.getIndBilaTrchEstr().setTsTariDov(((short)0));
        }
        // COB_CODE: IF B03-TS-TARI-SCON-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-TS-TARI-SCON
        //           ELSE
        //              MOVE 0 TO IND-B03-TS-TARI-SCON
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03TsTariScon().getB03TsTariSconNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-TS-TARI-SCON
            ws.getIndBilaTrchEstr().setTsTariScon(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-TS-TARI-SCON
            ws.getIndBilaTrchEstr().setTsTariScon(((short)0));
        }
        // COB_CODE: IF B03-TS-PP-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-TS-PP
        //           ELSE
        //              MOVE 0 TO IND-B03-TS-PP
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03TsPp().getB03TsPpNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-TS-PP
            ws.getIndBilaTrchEstr().setTsPp(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-TS-PP
            ws.getIndBilaTrchEstr().setTsPp(((short)0));
        }
        // COB_CODE: IF B03-COEFF-RIS-1-T-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-COEFF-RIS-1-T
        //           ELSE
        //              MOVE 0 TO IND-B03-COEFF-RIS-1-T
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CoeffRis1T().getB03CoeffRis1TNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-COEFF-RIS-1-T
            ws.getIndBilaTrchEstr().setCoeffRis1T(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-COEFF-RIS-1-T
            ws.getIndBilaTrchEstr().setCoeffRis1T(((short)0));
        }
        // COB_CODE: IF B03-COEFF-RIS-2-T-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-COEFF-RIS-2-T
        //           ELSE
        //              MOVE 0 TO IND-B03-COEFF-RIS-2-T
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CoeffRis2T().getB03CoeffRis2TNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-COEFF-RIS-2-T
            ws.getIndBilaTrchEstr().setCoeffRis2T(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-COEFF-RIS-2-T
            ws.getIndBilaTrchEstr().setCoeffRis2T(((short)0));
        }
        // COB_CODE: IF B03-ABB-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-ABB
        //           ELSE
        //              MOVE 0 TO IND-B03-ABB
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03Abb().getB03AbbNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-ABB
            ws.getIndBilaTrchEstr().setAbb(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-ABB
            ws.getIndBilaTrchEstr().setAbb(((short)0));
        }
        // COB_CODE: IF B03-TP-COASS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-TP-COASS
        //           ELSE
        //              MOVE 0 TO IND-B03-TP-COASS
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03TpCoassFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-TP-COASS
            ws.getIndBilaTrchEstr().setTpCoass(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-TP-COASS
            ws.getIndBilaTrchEstr().setTpCoass(((short)0));
        }
        // COB_CODE: IF B03-TRAT-RIASS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-TRAT-RIASS
        //           ELSE
        //              MOVE 0 TO IND-B03-TRAT-RIASS
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03TratRiassFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-TRAT-RIASS
            ws.getIndBilaTrchEstr().setTratRiass(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-TRAT-RIASS
            ws.getIndBilaTrchEstr().setTratRiass(((short)0));
        }
        // COB_CODE: IF B03-TRAT-RIASS-ECC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-TRAT-RIASS-ECC
        //           ELSE
        //              MOVE 0 TO IND-B03-TRAT-RIASS-ECC
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03TratRiassEccFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-TRAT-RIASS-ECC
            ws.getIndBilaTrchEstr().setTratRiassEcc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-TRAT-RIASS-ECC
            ws.getIndBilaTrchEstr().setTratRiassEcc(((short)0));
        }
        // COB_CODE: IF B03-TP-RGM-FISC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-TP-RGM-FISC
        //           ELSE
        //              MOVE 0 TO IND-B03-TP-RGM-FISC
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03TpRgmFiscFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-TP-RGM-FISC
            ws.getIndBilaTrchEstr().setTpRgmFisc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-TP-RGM-FISC
            ws.getIndBilaTrchEstr().setTpRgmFisc(((short)0));
        }
        // COB_CODE: IF B03-DUR-GAR-AA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DUR-GAR-AA
        //           ELSE
        //              MOVE 0 TO IND-B03-DUR-GAR-AA
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03DurGarAa().getB03DurGarAaNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DUR-GAR-AA
            ws.getIndBilaTrchEstr().setDurGarAa(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DUR-GAR-AA
            ws.getIndBilaTrchEstr().setDurGarAa(((short)0));
        }
        // COB_CODE: IF B03-DUR-GAR-MM-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DUR-GAR-MM
        //           ELSE
        //              MOVE 0 TO IND-B03-DUR-GAR-MM
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03DurGarMm().getB03DurGarMmNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DUR-GAR-MM
            ws.getIndBilaTrchEstr().setDurGarMm(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DUR-GAR-MM
            ws.getIndBilaTrchEstr().setDurGarMm(((short)0));
        }
        // COB_CODE: IF B03-DUR-GAR-GG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DUR-GAR-GG
        //           ELSE
        //              MOVE 0 TO IND-B03-DUR-GAR-GG
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03DurGarGg().getB03DurGarGgNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DUR-GAR-GG
            ws.getIndBilaTrchEstr().setDurGarGg(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DUR-GAR-GG
            ws.getIndBilaTrchEstr().setDurGarGg(((short)0));
        }
        // COB_CODE: IF B03-ANTIDUR-CALC-365-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-ANTIDUR-CALC-365
        //           ELSE
        //              MOVE 0 TO IND-B03-ANTIDUR-CALC-365
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03AntidurCalc365().getB03AntidurCalc365NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-ANTIDUR-CALC-365
            ws.getIndBilaTrchEstr().setAntidurCalc365(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-ANTIDUR-CALC-365
            ws.getIndBilaTrchEstr().setAntidurCalc365(((short)0));
        }
        // COB_CODE: IF B03-COD-FISC-CNTR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-COD-FISC-CNTR
        //           ELSE
        //              MOVE 0 TO IND-B03-COD-FISC-CNTR
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CodFiscCntrFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-COD-FISC-CNTR
            ws.getIndBilaTrchEstr().setCodFiscCntr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-COD-FISC-CNTR
            ws.getIndBilaTrchEstr().setCodFiscCntr(((short)0));
        }
        // COB_CODE: IF B03-COD-FISC-ASSTO1-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-COD-FISC-ASSTO1
        //           ELSE
        //              MOVE 0 TO IND-B03-COD-FISC-ASSTO1
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CodFiscAssto1Formatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-COD-FISC-ASSTO1
            ws.getIndBilaTrchEstr().setCodFiscAssto1(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-COD-FISC-ASSTO1
            ws.getIndBilaTrchEstr().setCodFiscAssto1(((short)0));
        }
        // COB_CODE: IF B03-COD-FISC-ASSTO2-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-COD-FISC-ASSTO2
        //           ELSE
        //              MOVE 0 TO IND-B03-COD-FISC-ASSTO2
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CodFiscAssto2Formatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-COD-FISC-ASSTO2
            ws.getIndBilaTrchEstr().setCodFiscAssto2(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-COD-FISC-ASSTO2
            ws.getIndBilaTrchEstr().setCodFiscAssto2(((short)0));
        }
        // COB_CODE: IF B03-COD-FISC-ASSTO3-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-COD-FISC-ASSTO3
        //           ELSE
        //              MOVE 0 TO IND-B03-COD-FISC-ASSTO3
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CodFiscAssto3Formatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-COD-FISC-ASSTO3
            ws.getIndBilaTrchEstr().setCodFiscAssto3(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-COD-FISC-ASSTO3
            ws.getIndBilaTrchEstr().setCodFiscAssto3(((short)0));
        }
        // COB_CODE: IF B03-CAUS-SCON = HIGH-VALUES
        //              MOVE -1 TO IND-B03-CAUS-SCON
        //           ELSE
        //              MOVE 0 TO IND-B03-CAUS-SCON
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CausScon(), BilaTrchEstrIdbsb030.Len.B03_CAUS_SCON)) {
            // COB_CODE: MOVE -1 TO IND-B03-CAUS-SCON
            ws.getIndBilaTrchEstr().setCausScon(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-CAUS-SCON
            ws.getIndBilaTrchEstr().setCausScon(((short)0));
        }
        // COB_CODE: IF B03-EMIT-TIT-OPZ = HIGH-VALUES
        //              MOVE -1 TO IND-B03-EMIT-TIT-OPZ
        //           ELSE
        //              MOVE 0 TO IND-B03-EMIT-TIT-OPZ
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03EmitTitOpz(), BilaTrchEstrIdbsb030.Len.B03_EMIT_TIT_OPZ)) {
            // COB_CODE: MOVE -1 TO IND-B03-EMIT-TIT-OPZ
            ws.getIndBilaTrchEstr().setEmitTitOpz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-EMIT-TIT-OPZ
            ws.getIndBilaTrchEstr().setEmitTitOpz(((short)0));
        }
        // COB_CODE: IF B03-QTZ-SP-Z-COUP-EMIS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-QTZ-SP-Z-COUP-EMIS
        //           ELSE
        //              MOVE 0 TO IND-B03-QTZ-SP-Z-COUP-EMIS
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03QtzSpZCoupEmis().getB03QtzSpZCoupEmisNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-QTZ-SP-Z-COUP-EMIS
            ws.getIndBilaTrchEstr().setQtzSpZCoupEmis(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-QTZ-SP-Z-COUP-EMIS
            ws.getIndBilaTrchEstr().setQtzSpZCoupEmis(((short)0));
        }
        // COB_CODE: IF B03-QTZ-SP-Z-OPZ-EMIS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-QTZ-SP-Z-OPZ-EMIS
        //           ELSE
        //              MOVE 0 TO IND-B03-QTZ-SP-Z-OPZ-EMIS
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03QtzSpZOpzEmis().getB03QtzSpZOpzEmisNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-QTZ-SP-Z-OPZ-EMIS
            ws.getIndBilaTrchEstr().setQtzSpZOpzEmis(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-QTZ-SP-Z-OPZ-EMIS
            ws.getIndBilaTrchEstr().setQtzSpZOpzEmis(((short)0));
        }
        // COB_CODE: IF B03-QTZ-SP-Z-COUP-DT-C-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-QTZ-SP-Z-COUP-DT-C
        //           ELSE
        //              MOVE 0 TO IND-B03-QTZ-SP-Z-COUP-DT-C
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03QtzSpZCoupDtC().getB03QtzSpZCoupDtCNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-QTZ-SP-Z-COUP-DT-C
            ws.getIndBilaTrchEstr().setQtzSpZCoupDtC(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-QTZ-SP-Z-COUP-DT-C
            ws.getIndBilaTrchEstr().setQtzSpZCoupDtC(((short)0));
        }
        // COB_CODE: IF B03-QTZ-SP-Z-OPZ-DT-CA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-QTZ-SP-Z-OPZ-DT-CA
        //           ELSE
        //              MOVE 0 TO IND-B03-QTZ-SP-Z-OPZ-DT-CA
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03QtzSpZOpzDtCa().getB03QtzSpZOpzDtCaNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-QTZ-SP-Z-OPZ-DT-CA
            ws.getIndBilaTrchEstr().setQtzSpZOpzDtCa(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-QTZ-SP-Z-OPZ-DT-CA
            ws.getIndBilaTrchEstr().setQtzSpZOpzDtCa(((short)0));
        }
        // COB_CODE: IF B03-QTZ-TOT-EMIS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-QTZ-TOT-EMIS
        //           ELSE
        //              MOVE 0 TO IND-B03-QTZ-TOT-EMIS
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03QtzTotEmis().getB03QtzTotEmisNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-QTZ-TOT-EMIS
            ws.getIndBilaTrchEstr().setQtzTotEmis(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-QTZ-TOT-EMIS
            ws.getIndBilaTrchEstr().setQtzTotEmis(((short)0));
        }
        // COB_CODE: IF B03-QTZ-TOT-DT-CALC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-QTZ-TOT-DT-CALC
        //           ELSE
        //              MOVE 0 TO IND-B03-QTZ-TOT-DT-CALC
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03QtzTotDtCalc().getB03QtzTotDtCalcNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-QTZ-TOT-DT-CALC
            ws.getIndBilaTrchEstr().setQtzTotDtCalc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-QTZ-TOT-DT-CALC
            ws.getIndBilaTrchEstr().setQtzTotDtCalc(((short)0));
        }
        // COB_CODE: IF B03-QTZ-TOT-DT-ULT-BIL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-QTZ-TOT-DT-ULT-BIL
        //           ELSE
        //              MOVE 0 TO IND-B03-QTZ-TOT-DT-ULT-BIL
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03QtzTotDtUltBil().getB03QtzTotDtUltBilNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-QTZ-TOT-DT-ULT-BIL
            ws.getIndBilaTrchEstr().setQtzTotDtUltBil(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-QTZ-TOT-DT-ULT-BIL
            ws.getIndBilaTrchEstr().setQtzTotDtUltBil(((short)0));
        }
        // COB_CODE: IF B03-DT-QTZ-EMIS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DT-QTZ-EMIS
        //           ELSE
        //              MOVE 0 TO IND-B03-DT-QTZ-EMIS
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03DtQtzEmis().getB03DtQtzEmisNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DT-QTZ-EMIS
            ws.getIndBilaTrchEstr().setDtQtzEmis(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DT-QTZ-EMIS
            ws.getIndBilaTrchEstr().setDtQtzEmis(((short)0));
        }
        // COB_CODE: IF B03-PC-CAR-GEST-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-PC-CAR-GEST
        //           ELSE
        //              MOVE 0 TO IND-B03-PC-CAR-GEST
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03PcCarGest().getB03PcCarGestNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-PC-CAR-GEST
            ws.getIndBilaTrchEstr().setPcCarGest(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-PC-CAR-GEST
            ws.getIndBilaTrchEstr().setPcCarGest(((short)0));
        }
        // COB_CODE: IF B03-PC-CAR-ACQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-PC-CAR-ACQ
        //           ELSE
        //              MOVE 0 TO IND-B03-PC-CAR-ACQ
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03PcCarAcq().getB03PcCarAcqNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-PC-CAR-ACQ
            ws.getIndBilaTrchEstr().setPcCarAcq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-PC-CAR-ACQ
            ws.getIndBilaTrchEstr().setPcCarAcq(((short)0));
        }
        // COB_CODE: IF B03-IMP-CAR-CASO-MOR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-IMP-CAR-CASO-MOR
        //           ELSE
        //              MOVE 0 TO IND-B03-IMP-CAR-CASO-MOR
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03ImpCarCasoMor().getB03ImpCarCasoMorNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-IMP-CAR-CASO-MOR
            ws.getIndBilaTrchEstr().setImpCarCasoMor(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-IMP-CAR-CASO-MOR
            ws.getIndBilaTrchEstr().setImpCarCasoMor(((short)0));
        }
        // COB_CODE: IF B03-PC-CAR-MOR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-PC-CAR-MOR
        //           ELSE
        //              MOVE 0 TO IND-B03-PC-CAR-MOR
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03PcCarMor().getB03PcCarMorNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-PC-CAR-MOR
            ws.getIndBilaTrchEstr().setPcCarMor(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-PC-CAR-MOR
            ws.getIndBilaTrchEstr().setPcCarMor(((short)0));
        }
        // COB_CODE: IF B03-TP-VERS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-TP-VERS
        //           ELSE
        //              MOVE 0 TO IND-B03-TP-VERS
        //           END-IF
        if (Conditions.eq(bilaTrchEstr.getB03TpVers(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-B03-TP-VERS
            ws.getIndBilaTrchEstr().setTpVers(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-TP-VERS
            ws.getIndBilaTrchEstr().setTpVers(((short)0));
        }
        // COB_CODE: IF B03-FL-SWITCH-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-FL-SWITCH
        //           ELSE
        //              MOVE 0 TO IND-B03-FL-SWITCH
        //           END-IF
        if (Conditions.eq(bilaTrchEstr.getB03FlSwitch(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-B03-FL-SWITCH
            ws.getIndBilaTrchEstr().setFlSwitch(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-FL-SWITCH
            ws.getIndBilaTrchEstr().setFlSwitch(((short)0));
        }
        // COB_CODE: IF B03-FL-IAS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-FL-IAS
        //           ELSE
        //              MOVE 0 TO IND-B03-FL-IAS
        //           END-IF
        if (Conditions.eq(bilaTrchEstr.getB03FlIas(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-B03-FL-IAS
            ws.getIndBilaTrchEstr().setFlIas(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-FL-IAS
            ws.getIndBilaTrchEstr().setFlIas(((short)0));
        }
        // COB_CODE: IF B03-DIR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DIR
        //           ELSE
        //              MOVE 0 TO IND-B03-DIR
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03Dir().getB03DirNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DIR
            ws.getIndBilaTrchEstr().setDir(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DIR
            ws.getIndBilaTrchEstr().setDir(((short)0));
        }
        // COB_CODE: IF B03-TP-COP-CASO-MOR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-TP-COP-CASO-MOR
        //           ELSE
        //              MOVE 0 TO IND-B03-TP-COP-CASO-MOR
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03TpCopCasoMorFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-TP-COP-CASO-MOR
            ws.getIndBilaTrchEstr().setTpCopCasoMor(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-TP-COP-CASO-MOR
            ws.getIndBilaTrchEstr().setTpCopCasoMor(((short)0));
        }
        // COB_CODE: IF B03-MET-RISC-SPCL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-MET-RISC-SPCL
        //           ELSE
        //              MOVE 0 TO IND-B03-MET-RISC-SPCL
        //           END-IF
        if (Conditions.eq(bilaTrchEstr.getB03MetRiscSpcl().getB03MetRiscSpclNull(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-B03-MET-RISC-SPCL
            ws.getIndBilaTrchEstr().setMetRiscSpcl(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-MET-RISC-SPCL
            ws.getIndBilaTrchEstr().setMetRiscSpcl(((short)0));
        }
        // COB_CODE: IF B03-TP-STAT-INVST-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-TP-STAT-INVST
        //           ELSE
        //              MOVE 0 TO IND-B03-TP-STAT-INVST
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03TpStatInvstFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-TP-STAT-INVST
            ws.getIndBilaTrchEstr().setTpStatInvst(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-TP-STAT-INVST
            ws.getIndBilaTrchEstr().setTpStatInvst(((short)0));
        }
        // COB_CODE: IF B03-COD-PRDT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-COD-PRDT
        //           ELSE
        //              MOVE 0 TO IND-B03-COD-PRDT
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CodPrdt().getB03CodPrdtNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-COD-PRDT
            ws.getIndBilaTrchEstr().setCodPrdt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-COD-PRDT
            ws.getIndBilaTrchEstr().setCodPrdt(((short)0));
        }
        // COB_CODE: IF B03-STAT-ASSTO-1-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-STAT-ASSTO-1
        //           ELSE
        //              MOVE 0 TO IND-B03-STAT-ASSTO-1
        //           END-IF
        if (Conditions.eq(bilaTrchEstr.getB03StatAssto1(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-B03-STAT-ASSTO-1
            ws.getIndBilaTrchEstr().setStatAssto1(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-STAT-ASSTO-1
            ws.getIndBilaTrchEstr().setStatAssto1(((short)0));
        }
        // COB_CODE: IF B03-STAT-ASSTO-2-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-STAT-ASSTO-2
        //           ELSE
        //              MOVE 0 TO IND-B03-STAT-ASSTO-2
        //           END-IF
        if (Conditions.eq(bilaTrchEstr.getB03StatAssto2(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-B03-STAT-ASSTO-2
            ws.getIndBilaTrchEstr().setStatAssto2(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-STAT-ASSTO-2
            ws.getIndBilaTrchEstr().setStatAssto2(((short)0));
        }
        // COB_CODE: IF B03-STAT-ASSTO-3-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-STAT-ASSTO-3
        //           ELSE
        //              MOVE 0 TO IND-B03-STAT-ASSTO-3
        //           END-IF
        if (Conditions.eq(bilaTrchEstr.getB03StatAssto3(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-B03-STAT-ASSTO-3
            ws.getIndBilaTrchEstr().setStatAssto3(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-STAT-ASSTO-3
            ws.getIndBilaTrchEstr().setStatAssto3(((short)0));
        }
        // COB_CODE: IF B03-CPT-ASSTO-INI-MOR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-CPT-ASSTO-INI-MOR
        //           ELSE
        //              MOVE 0 TO IND-B03-CPT-ASSTO-INI-MOR
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CptAsstoIniMor().getB03CptAsstoIniMorNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-CPT-ASSTO-INI-MOR
            ws.getIndBilaTrchEstr().setCptAsstoIniMor(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-CPT-ASSTO-INI-MOR
            ws.getIndBilaTrchEstr().setCptAsstoIniMor(((short)0));
        }
        // COB_CODE: IF B03-TS-STAB-PRE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-TS-STAB-PRE
        //           ELSE
        //              MOVE 0 TO IND-B03-TS-STAB-PRE
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03TsStabPre().getB03TsStabPreNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-TS-STAB-PRE
            ws.getIndBilaTrchEstr().setTsStabPre(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-TS-STAB-PRE
            ws.getIndBilaTrchEstr().setTsStabPre(((short)0));
        }
        // COB_CODE: IF B03-DIR-EMIS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DIR-EMIS
        //           ELSE
        //              MOVE 0 TO IND-B03-DIR-EMIS
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03DirEmis().getB03DirEmisNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DIR-EMIS
            ws.getIndBilaTrchEstr().setDirEmis(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DIR-EMIS
            ws.getIndBilaTrchEstr().setDirEmis(((short)0));
        }
        // COB_CODE: IF B03-DT-INC-ULT-PRE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-DT-INC-ULT-PRE
        //           ELSE
        //              MOVE 0 TO IND-B03-DT-INC-ULT-PRE
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03DtIncUltPre().getB03DtIncUltPreNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-DT-INC-ULT-PRE
            ws.getIndBilaTrchEstr().setDtIncUltPre(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-DT-INC-ULT-PRE
            ws.getIndBilaTrchEstr().setDtIncUltPre(((short)0));
        }
        // COB_CODE: IF B03-STAT-TBGC-ASSTO-1-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-STAT-TBGC-ASSTO-1
        //           ELSE
        //              MOVE 0 TO IND-B03-STAT-TBGC-ASSTO-1
        //           END-IF
        if (Conditions.eq(bilaTrchEstr.getB03StatTbgcAssto1(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-B03-STAT-TBGC-ASSTO-1
            ws.getIndBilaTrchEstr().setStatTbgcAssto1(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-STAT-TBGC-ASSTO-1
            ws.getIndBilaTrchEstr().setStatTbgcAssto1(((short)0));
        }
        // COB_CODE: IF B03-STAT-TBGC-ASSTO-2-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-STAT-TBGC-ASSTO-2
        //           ELSE
        //              MOVE 0 TO IND-B03-STAT-TBGC-ASSTO-2
        //           END-IF
        if (Conditions.eq(bilaTrchEstr.getB03StatTbgcAssto2(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-B03-STAT-TBGC-ASSTO-2
            ws.getIndBilaTrchEstr().setStatTbgcAssto2(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-STAT-TBGC-ASSTO-2
            ws.getIndBilaTrchEstr().setStatTbgcAssto2(((short)0));
        }
        // COB_CODE: IF B03-STAT-TBGC-ASSTO-3-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-STAT-TBGC-ASSTO-3
        //           ELSE
        //              MOVE 0 TO IND-B03-STAT-TBGC-ASSTO-3
        //           END-IF
        if (Conditions.eq(bilaTrchEstr.getB03StatTbgcAssto3(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-B03-STAT-TBGC-ASSTO-3
            ws.getIndBilaTrchEstr().setStatTbgcAssto3(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-STAT-TBGC-ASSTO-3
            ws.getIndBilaTrchEstr().setStatTbgcAssto3(((short)0));
        }
        // COB_CODE: IF B03-FRAZ-DECR-CPT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-FRAZ-DECR-CPT
        //           ELSE
        //              MOVE 0 TO IND-B03-FRAZ-DECR-CPT
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03FrazDecrCpt().getB03FrazDecrCptNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-FRAZ-DECR-CPT
            ws.getIndBilaTrchEstr().setFrazDecrCpt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-FRAZ-DECR-CPT
            ws.getIndBilaTrchEstr().setFrazDecrCpt(((short)0));
        }
        // COB_CODE: IF B03-PRE-PP-ULT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-PRE-PP-ULT
        //           ELSE
        //              MOVE 0 TO IND-B03-PRE-PP-ULT
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03PrePpUlt().getB03PrePpUltNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-PRE-PP-ULT
            ws.getIndBilaTrchEstr().setPrePpUlt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-PRE-PP-ULT
            ws.getIndBilaTrchEstr().setPrePpUlt(((short)0));
        }
        // COB_CODE: IF B03-ACQ-EXP-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-ACQ-EXP
        //           ELSE
        //              MOVE 0 TO IND-B03-ACQ-EXP
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03AcqExp().getB03AcqExpNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-ACQ-EXP
            ws.getIndBilaTrchEstr().setAcqExp(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-ACQ-EXP
            ws.getIndBilaTrchEstr().setAcqExp(((short)0));
        }
        // COB_CODE: IF B03-REMUN-ASS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-REMUN-ASS
        //           ELSE
        //              MOVE 0 TO IND-B03-REMUN-ASS
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03RemunAss().getB03RemunAssNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-REMUN-ASS
            ws.getIndBilaTrchEstr().setRemunAss(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-REMUN-ASS
            ws.getIndBilaTrchEstr().setRemunAss(((short)0));
        }
        // COB_CODE: IF B03-COMMIS-INTER-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-COMMIS-INTER
        //           ELSE
        //              MOVE 0 TO IND-B03-COMMIS-INTER
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03CommisInter().getB03CommisInterNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-COMMIS-INTER
            ws.getIndBilaTrchEstr().setCommisInter(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-COMMIS-INTER
            ws.getIndBilaTrchEstr().setCommisInter(((short)0));
        }
        // COB_CODE: IF B03-NUM-FINANZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-NUM-FINANZ
        //           ELSE
        //              MOVE 0 TO IND-B03-NUM-FINANZ
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03NumFinanz(), BilaTrchEstrIdbsb030.Len.B03_NUM_FINANZ)) {
            // COB_CODE: MOVE -1 TO IND-B03-NUM-FINANZ
            ws.getIndBilaTrchEstr().setNumFinanz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-NUM-FINANZ
            ws.getIndBilaTrchEstr().setNumFinanz(((short)0));
        }
        // COB_CODE: IF B03-TP-ACC-COMM-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-TP-ACC-COMM
        //           ELSE
        //              MOVE 0 TO IND-B03-TP-ACC-COMM
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03TpAccCommFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-TP-ACC-COMM
            ws.getIndBilaTrchEstr().setTpAccComm(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-TP-ACC-COMM
            ws.getIndBilaTrchEstr().setTpAccComm(((short)0));
        }
        // COB_CODE: IF B03-IB-ACC-COMM-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-IB-ACC-COMM
        //           ELSE
        //              MOVE 0 TO IND-B03-IB-ACC-COMM
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03IbAccComm(), BilaTrchEstrIdbsb030.Len.B03_IB_ACC_COMM)) {
            // COB_CODE: MOVE -1 TO IND-B03-IB-ACC-COMM
            ws.getIndBilaTrchEstr().setIbAccComm(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-IB-ACC-COMM
            ws.getIndBilaTrchEstr().setIbAccComm(((short)0));
        }
        // COB_CODE: IF B03-CARZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B03-CARZ
        //           ELSE
        //              MOVE 0 TO IND-B03-CARZ
        //           END-IF.
        if (Characters.EQ_HIGH.test(bilaTrchEstr.getB03Carz().getB03CarzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B03-CARZ
            ws.getIndBilaTrchEstr().setCarz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B03-CARZ
            ws.getIndBilaTrchEstr().setCarz(((short)0));
        }
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IDSP0003:line=25, because the code is unreachable.
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IDSP0003:line=33, because the code is unreachable.
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    @Override
    public int getAaRenCer() {
        throw new FieldNotMappedException("aaRenCer");
    }

    @Override
    public void setAaRenCer(int aaRenCer) {
        throw new FieldNotMappedException("aaRenCer");
    }

    @Override
    public Integer getAaRenCerObj() {
        return ((Integer)getAaRenCer());
    }

    @Override
    public void setAaRenCerObj(Integer aaRenCerObj) {
        setAaRenCer(((int)aaRenCerObj));
    }

    @Override
    public AfDecimal getAbb() {
        throw new FieldNotMappedException("abb");
    }

    @Override
    public void setAbb(AfDecimal abb) {
        throw new FieldNotMappedException("abb");
    }

    @Override
    public AfDecimal getAbbObj() {
        return getAbb();
    }

    @Override
    public void setAbbObj(AfDecimal abbObj) {
        setAbb(new AfDecimal(abbObj, 15, 3));
    }

    @Override
    public AfDecimal getAcqExp() {
        throw new FieldNotMappedException("acqExp");
    }

    @Override
    public void setAcqExp(AfDecimal acqExp) {
        throw new FieldNotMappedException("acqExp");
    }

    @Override
    public AfDecimal getAcqExpObj() {
        return getAcqExp();
    }

    @Override
    public void setAcqExpObj(AfDecimal acqExpObj) {
        setAcqExp(new AfDecimal(acqExpObj, 15, 3));
    }

    @Override
    public AfDecimal getAlqMargCSubrsh() {
        throw new FieldNotMappedException("alqMargCSubrsh");
    }

    @Override
    public void setAlqMargCSubrsh(AfDecimal alqMargCSubrsh) {
        throw new FieldNotMappedException("alqMargCSubrsh");
    }

    @Override
    public AfDecimal getAlqMargCSubrshObj() {
        return getAlqMargCSubrsh();
    }

    @Override
    public void setAlqMargCSubrshObj(AfDecimal alqMargCSubrshObj) {
        setAlqMargCSubrsh(new AfDecimal(alqMargCSubrshObj, 6, 3));
    }

    @Override
    public AfDecimal getAlqMargRis() {
        throw new FieldNotMappedException("alqMargRis");
    }

    @Override
    public void setAlqMargRis(AfDecimal alqMargRis) {
        throw new FieldNotMappedException("alqMargRis");
    }

    @Override
    public AfDecimal getAlqMargRisObj() {
        return getAlqMargRis();
    }

    @Override
    public void setAlqMargRisObj(AfDecimal alqMargRisObj) {
        setAlqMargRis(new AfDecimal(alqMargRisObj, 6, 3));
    }

    @Override
    public AfDecimal getAlqRetrT() {
        throw new FieldNotMappedException("alqRetrT");
    }

    @Override
    public void setAlqRetrT(AfDecimal alqRetrT) {
        throw new FieldNotMappedException("alqRetrT");
    }

    @Override
    public AfDecimal getAlqRetrTObj() {
        return getAlqRetrT();
    }

    @Override
    public void setAlqRetrTObj(AfDecimal alqRetrTObj) {
        setAlqRetrT(new AfDecimal(alqRetrTObj, 6, 3));
    }

    @Override
    public AfDecimal getAntidurCalc365() {
        throw new FieldNotMappedException("antidurCalc365");
    }

    @Override
    public void setAntidurCalc365(AfDecimal antidurCalc365) {
        throw new FieldNotMappedException("antidurCalc365");
    }

    @Override
    public AfDecimal getAntidurCalc365Obj() {
        return getAntidurCalc365();
    }

    @Override
    public void setAntidurCalc365Obj(AfDecimal antidurCalc365Obj) {
        setAntidurCalc365(new AfDecimal(antidurCalc365Obj, 11, 7));
    }

    @Override
    public AfDecimal getAntidurDtCalc() {
        throw new FieldNotMappedException("antidurDtCalc");
    }

    @Override
    public void setAntidurDtCalc(AfDecimal antidurDtCalc) {
        throw new FieldNotMappedException("antidurDtCalc");
    }

    @Override
    public AfDecimal getAntidurDtCalcObj() {
        return getAntidurDtCalc();
    }

    @Override
    public void setAntidurDtCalcObj(AfDecimal antidurDtCalcObj) {
        setAntidurDtCalc(new AfDecimal(antidurDtCalcObj, 11, 7));
    }

    @Override
    public int getAntidurRicorPrec() {
        throw new FieldNotMappedException("antidurRicorPrec");
    }

    @Override
    public void setAntidurRicorPrec(int antidurRicorPrec) {
        throw new FieldNotMappedException("antidurRicorPrec");
    }

    @Override
    public Integer getAntidurRicorPrecObj() {
        return ((Integer)getAntidurRicorPrec());
    }

    @Override
    public void setAntidurRicorPrecObj(Integer antidurRicorPrecObj) {
        setAntidurRicorPrec(((int)antidurRicorPrecObj));
    }

    @Override
    public int getB03IdAdes() {
        return bilaTrchEstr.getB03IdAdes();
    }

    @Override
    public void setB03IdAdes(int b03IdAdes) {
        this.bilaTrchEstr.setB03IdAdes(b03IdAdes);
    }

    @Override
    public int getB03IdBilaTrchEstr() {
        throw new FieldNotMappedException("b03IdBilaTrchEstr");
    }

    @Override
    public void setB03IdBilaTrchEstr(int b03IdBilaTrchEstr) {
        throw new FieldNotMappedException("b03IdBilaTrchEstr");
    }

    @Override
    public int getB03IdPoli() {
        return bilaTrchEstr.getB03IdPoli();
    }

    @Override
    public void setB03IdPoli(int b03IdPoli) {
        this.bilaTrchEstr.setB03IdPoli(b03IdPoli);
    }

    @Override
    public int getB03IdRichEstrazMas() {
        return bilaTrchEstr.getB03IdRichEstrazMas();
    }

    @Override
    public void setB03IdRichEstrazMas(int b03IdRichEstrazMas) {
        this.bilaTrchEstr.setB03IdRichEstrazMas(b03IdRichEstrazMas);
    }

    @Override
    public int getB03IdTrchDiGar() {
        return bilaTrchEstr.getB03IdTrchDiGar();
    }

    @Override
    public void setB03IdTrchDiGar(int b03IdTrchDiGar) {
        this.bilaTrchEstr.setB03IdTrchDiGar(b03IdTrchDiGar);
    }

    @Override
    public AfDecimal getCarAcqNonScon() {
        throw new FieldNotMappedException("carAcqNonScon");
    }

    @Override
    public void setCarAcqNonScon(AfDecimal carAcqNonScon) {
        throw new FieldNotMappedException("carAcqNonScon");
    }

    @Override
    public AfDecimal getCarAcqNonSconObj() {
        return getCarAcqNonScon();
    }

    @Override
    public void setCarAcqNonSconObj(AfDecimal carAcqNonSconObj) {
        setCarAcqNonScon(new AfDecimal(carAcqNonSconObj, 15, 3));
    }

    @Override
    public AfDecimal getCarAcqPrecontato() {
        throw new FieldNotMappedException("carAcqPrecontato");
    }

    @Override
    public void setCarAcqPrecontato(AfDecimal carAcqPrecontato) {
        throw new FieldNotMappedException("carAcqPrecontato");
    }

    @Override
    public AfDecimal getCarAcqPrecontatoObj() {
        return getCarAcqPrecontato();
    }

    @Override
    public void setCarAcqPrecontatoObj(AfDecimal carAcqPrecontatoObj) {
        setCarAcqPrecontato(new AfDecimal(carAcqPrecontatoObj, 15, 3));
    }

    @Override
    public AfDecimal getCarGest() {
        throw new FieldNotMappedException("carGest");
    }

    @Override
    public void setCarGest(AfDecimal carGest) {
        throw new FieldNotMappedException("carGest");
    }

    @Override
    public AfDecimal getCarGestNonScon() {
        throw new FieldNotMappedException("carGestNonScon");
    }

    @Override
    public void setCarGestNonScon(AfDecimal carGestNonScon) {
        throw new FieldNotMappedException("carGestNonScon");
    }

    @Override
    public AfDecimal getCarGestNonSconObj() {
        return getCarGestNonScon();
    }

    @Override
    public void setCarGestNonSconObj(AfDecimal carGestNonSconObj) {
        setCarGestNonScon(new AfDecimal(carGestNonSconObj, 15, 3));
    }

    @Override
    public AfDecimal getCarGestObj() {
        return getCarGest();
    }

    @Override
    public void setCarGestObj(AfDecimal carGestObj) {
        setCarGest(new AfDecimal(carGestObj, 15, 3));
    }

    @Override
    public AfDecimal getCarInc() {
        throw new FieldNotMappedException("carInc");
    }

    @Override
    public void setCarInc(AfDecimal carInc) {
        throw new FieldNotMappedException("carInc");
    }

    @Override
    public AfDecimal getCarIncNonScon() {
        throw new FieldNotMappedException("carIncNonScon");
    }

    @Override
    public void setCarIncNonScon(AfDecimal carIncNonScon) {
        throw new FieldNotMappedException("carIncNonScon");
    }

    @Override
    public AfDecimal getCarIncNonSconObj() {
        return getCarIncNonScon();
    }

    @Override
    public void setCarIncNonSconObj(AfDecimal carIncNonSconObj) {
        setCarIncNonScon(new AfDecimal(carIncNonSconObj, 15, 3));
    }

    @Override
    public AfDecimal getCarIncObj() {
        return getCarInc();
    }

    @Override
    public void setCarIncObj(AfDecimal carIncObj) {
        setCarInc(new AfDecimal(carIncObj, 15, 3));
    }

    @Override
    public int getCarz() {
        throw new FieldNotMappedException("carz");
    }

    @Override
    public void setCarz(int carz) {
        throw new FieldNotMappedException("carz");
    }

    @Override
    public Integer getCarzObj() {
        return ((Integer)getCarz());
    }

    @Override
    public void setCarzObj(Integer carzObj) {
        setCarz(((int)carzObj));
    }

    @Override
    public String getCausSconVchar() {
        throw new FieldNotMappedException("causSconVchar");
    }

    @Override
    public void setCausSconVchar(String causSconVchar) {
        throw new FieldNotMappedException("causSconVchar");
    }

    @Override
    public String getCausSconVcharObj() {
        return getCausSconVchar();
    }

    @Override
    public void setCausSconVcharObj(String causSconVcharObj) {
        setCausSconVchar(causSconVcharObj);
    }

    @Override
    public int getCodAge() {
        throw new FieldNotMappedException("codAge");
    }

    @Override
    public void setCodAge(int codAge) {
        throw new FieldNotMappedException("codAge");
    }

    @Override
    public Integer getCodAgeObj() {
        return ((Integer)getCodAge());
    }

    @Override
    public void setCodAgeObj(Integer codAgeObj) {
        setCodAge(((int)codAgeObj));
    }

    @Override
    public int getCodCan() {
        throw new FieldNotMappedException("codCan");
    }

    @Override
    public void setCodCan(int codCan) {
        throw new FieldNotMappedException("codCan");
    }

    @Override
    public Integer getCodCanObj() {
        return ((Integer)getCodCan());
    }

    @Override
    public void setCodCanObj(Integer codCanObj) {
        setCodCan(((int)codCanObj));
    }

    @Override
    public int getCodCompAnia() {
        throw new FieldNotMappedException("codCompAnia");
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        throw new FieldNotMappedException("codCompAnia");
    }

    @Override
    public String getCodConv() {
        throw new FieldNotMappedException("codConv");
    }

    @Override
    public void setCodConv(String codConv) {
        throw new FieldNotMappedException("codConv");
    }

    @Override
    public String getCodConvObj() {
        return getCodConv();
    }

    @Override
    public void setCodConvObj(String codConvObj) {
        setCodConv(codConvObj);
    }

    @Override
    public String getCodDiv() {
        throw new FieldNotMappedException("codDiv");
    }

    @Override
    public void setCodDiv(String codDiv) {
        throw new FieldNotMappedException("codDiv");
    }

    @Override
    public String getCodDivObj() {
        return getCodDiv();
    }

    @Override
    public void setCodDivObj(String codDivObj) {
        setCodDiv(codDivObj);
    }

    @Override
    public String getCodFiscAssto1() {
        throw new FieldNotMappedException("codFiscAssto1");
    }

    @Override
    public void setCodFiscAssto1(String codFiscAssto1) {
        throw new FieldNotMappedException("codFiscAssto1");
    }

    @Override
    public String getCodFiscAssto1Obj() {
        return getCodFiscAssto1();
    }

    @Override
    public void setCodFiscAssto1Obj(String codFiscAssto1Obj) {
        setCodFiscAssto1(codFiscAssto1Obj);
    }

    @Override
    public String getCodFiscAssto2() {
        throw new FieldNotMappedException("codFiscAssto2");
    }

    @Override
    public void setCodFiscAssto2(String codFiscAssto2) {
        throw new FieldNotMappedException("codFiscAssto2");
    }

    @Override
    public String getCodFiscAssto2Obj() {
        return getCodFiscAssto2();
    }

    @Override
    public void setCodFiscAssto2Obj(String codFiscAssto2Obj) {
        setCodFiscAssto2(codFiscAssto2Obj);
    }

    @Override
    public String getCodFiscAssto3() {
        throw new FieldNotMappedException("codFiscAssto3");
    }

    @Override
    public void setCodFiscAssto3(String codFiscAssto3) {
        throw new FieldNotMappedException("codFiscAssto3");
    }

    @Override
    public String getCodFiscAssto3Obj() {
        return getCodFiscAssto3();
    }

    @Override
    public void setCodFiscAssto3Obj(String codFiscAssto3Obj) {
        setCodFiscAssto3(codFiscAssto3Obj);
    }

    @Override
    public String getCodFiscCntr() {
        throw new FieldNotMappedException("codFiscCntr");
    }

    @Override
    public void setCodFiscCntr(String codFiscCntr) {
        throw new FieldNotMappedException("codFiscCntr");
    }

    @Override
    public String getCodFiscCntrObj() {
        return getCodFiscCntr();
    }

    @Override
    public void setCodFiscCntrObj(String codFiscCntrObj) {
        setCodFiscCntr(codFiscCntrObj);
    }

    @Override
    public String getCodFnd() {
        throw new FieldNotMappedException("codFnd");
    }

    @Override
    public void setCodFnd(String codFnd) {
        throw new FieldNotMappedException("codFnd");
    }

    @Override
    public String getCodFndObj() {
        return getCodFnd();
    }

    @Override
    public void setCodFndObj(String codFndObj) {
        setCodFnd(codFndObj);
    }

    @Override
    public int getCodPrdt() {
        throw new FieldNotMappedException("codPrdt");
    }

    @Override
    public void setCodPrdt(int codPrdt) {
        throw new FieldNotMappedException("codPrdt");
    }

    @Override
    public Integer getCodPrdtObj() {
        return ((Integer)getCodPrdt());
    }

    @Override
    public void setCodPrdtObj(Integer codPrdtObj) {
        setCodPrdt(((int)codPrdtObj));
    }

    @Override
    public String getCodProd() {
        throw new FieldNotMappedException("codProd");
    }

    @Override
    public void setCodProd(String codProd) {
        throw new FieldNotMappedException("codProd");
    }

    @Override
    public String getCodProdObj() {
        return getCodProd();
    }

    @Override
    public void setCodProdObj(String codProdObj) {
        setCodProd(codProdObj);
    }

    @Override
    public String getCodRamo() {
        throw new FieldNotMappedException("codRamo");
    }

    @Override
    public void setCodRamo(String codRamo) {
        throw new FieldNotMappedException("codRamo");
    }

    @Override
    public int getCodSubage() {
        throw new FieldNotMappedException("codSubage");
    }

    @Override
    public void setCodSubage(int codSubage) {
        throw new FieldNotMappedException("codSubage");
    }

    @Override
    public Integer getCodSubageObj() {
        return ((Integer)getCodSubage());
    }

    @Override
    public void setCodSubageObj(Integer codSubageObj) {
        setCodSubage(((int)codSubageObj));
    }

    @Override
    public String getCodTari() {
        throw new FieldNotMappedException("codTari");
    }

    @Override
    public void setCodTari(String codTari) {
        throw new FieldNotMappedException("codTari");
    }

    @Override
    public String getCodTariOrgn() {
        throw new FieldNotMappedException("codTariOrgn");
    }

    @Override
    public void setCodTariOrgn(String codTariOrgn) {
        throw new FieldNotMappedException("codTariOrgn");
    }

    @Override
    public String getCodTariOrgnObj() {
        return getCodTariOrgn();
    }

    @Override
    public void setCodTariOrgnObj(String codTariOrgnObj) {
        setCodTariOrgn(codTariOrgnObj);
    }

    @Override
    public AfDecimal getCoeffOpzCpt() {
        throw new FieldNotMappedException("coeffOpzCpt");
    }

    @Override
    public void setCoeffOpzCpt(AfDecimal coeffOpzCpt) {
        throw new FieldNotMappedException("coeffOpzCpt");
    }

    @Override
    public AfDecimal getCoeffOpzCptObj() {
        return getCoeffOpzCpt();
    }

    @Override
    public void setCoeffOpzCptObj(AfDecimal coeffOpzCptObj) {
        setCoeffOpzCpt(new AfDecimal(coeffOpzCptObj, 6, 3));
    }

    @Override
    public AfDecimal getCoeffOpzRen() {
        throw new FieldNotMappedException("coeffOpzRen");
    }

    @Override
    public void setCoeffOpzRen(AfDecimal coeffOpzRen) {
        throw new FieldNotMappedException("coeffOpzRen");
    }

    @Override
    public AfDecimal getCoeffOpzRenObj() {
        return getCoeffOpzRen();
    }

    @Override
    public void setCoeffOpzRenObj(AfDecimal coeffOpzRenObj) {
        setCoeffOpzRen(new AfDecimal(coeffOpzRenObj, 6, 3));
    }

    @Override
    public AfDecimal getCoeffRis1T() {
        throw new FieldNotMappedException("coeffRis1T");
    }

    @Override
    public void setCoeffRis1T(AfDecimal coeffRis1T) {
        throw new FieldNotMappedException("coeffRis1T");
    }

    @Override
    public AfDecimal getCoeffRis1TObj() {
        return getCoeffRis1T();
    }

    @Override
    public void setCoeffRis1TObj(AfDecimal coeffRis1TObj) {
        setCoeffRis1T(new AfDecimal(coeffRis1TObj, 14, 9));
    }

    @Override
    public AfDecimal getCoeffRis2T() {
        throw new FieldNotMappedException("coeffRis2T");
    }

    @Override
    public void setCoeffRis2T(AfDecimal coeffRis2T) {
        throw new FieldNotMappedException("coeffRis2T");
    }

    @Override
    public AfDecimal getCoeffRis2TObj() {
        return getCoeffRis2T();
    }

    @Override
    public void setCoeffRis2TObj(AfDecimal coeffRis2TObj) {
        setCoeffRis2T(new AfDecimal(coeffRis2TObj, 14, 9));
    }

    @Override
    public AfDecimal getCommisInter() {
        throw new FieldNotMappedException("commisInter");
    }

    @Override
    public void setCommisInter(AfDecimal commisInter) {
        throw new FieldNotMappedException("commisInter");
    }

    @Override
    public AfDecimal getCommisInterObj() {
        return getCommisInter();
    }

    @Override
    public void setCommisInterObj(AfDecimal commisInterObj) {
        setCommisInter(new AfDecimal(commisInterObj, 15, 3));
    }

    @Override
    public AfDecimal getCptAsstoIniMor() {
        throw new FieldNotMappedException("cptAsstoIniMor");
    }

    @Override
    public void setCptAsstoIniMor(AfDecimal cptAsstoIniMor) {
        throw new FieldNotMappedException("cptAsstoIniMor");
    }

    @Override
    public AfDecimal getCptAsstoIniMorObj() {
        return getCptAsstoIniMor();
    }

    @Override
    public void setCptAsstoIniMorObj(AfDecimal cptAsstoIniMorObj) {
        setCptAsstoIniMor(new AfDecimal(cptAsstoIniMorObj, 15, 3));
    }

    @Override
    public AfDecimal getCptDtRidz() {
        throw new FieldNotMappedException("cptDtRidz");
    }

    @Override
    public void setCptDtRidz(AfDecimal cptDtRidz) {
        throw new FieldNotMappedException("cptDtRidz");
    }

    @Override
    public AfDecimal getCptDtRidzObj() {
        return getCptDtRidz();
    }

    @Override
    public void setCptDtRidzObj(AfDecimal cptDtRidzObj) {
        setCptDtRidz(new AfDecimal(cptDtRidzObj, 15, 3));
    }

    @Override
    public AfDecimal getCptDtStab() {
        throw new FieldNotMappedException("cptDtStab");
    }

    @Override
    public void setCptDtStab(AfDecimal cptDtStab) {
        throw new FieldNotMappedException("cptDtStab");
    }

    @Override
    public AfDecimal getCptDtStabObj() {
        return getCptDtStab();
    }

    @Override
    public void setCptDtStabObj(AfDecimal cptDtStabObj) {
        setCptDtStab(new AfDecimal(cptDtStabObj, 15, 3));
    }

    @Override
    public AfDecimal getCptRiasto() {
        throw new FieldNotMappedException("cptRiasto");
    }

    @Override
    public void setCptRiasto(AfDecimal cptRiasto) {
        throw new FieldNotMappedException("cptRiasto");
    }

    @Override
    public AfDecimal getCptRiastoEcc() {
        throw new FieldNotMappedException("cptRiastoEcc");
    }

    @Override
    public void setCptRiastoEcc(AfDecimal cptRiastoEcc) {
        throw new FieldNotMappedException("cptRiastoEcc");
    }

    @Override
    public AfDecimal getCptRiastoEccObj() {
        return getCptRiastoEcc();
    }

    @Override
    public void setCptRiastoEccObj(AfDecimal cptRiastoEccObj) {
        setCptRiastoEcc(new AfDecimal(cptRiastoEccObj, 15, 3));
    }

    @Override
    public AfDecimal getCptRiastoObj() {
        return getCptRiasto();
    }

    @Override
    public void setCptRiastoObj(AfDecimal cptRiastoObj) {
        setCptRiasto(new AfDecimal(cptRiastoObj, 15, 3));
    }

    @Override
    public AfDecimal getCptRshMor() {
        throw new FieldNotMappedException("cptRshMor");
    }

    @Override
    public void setCptRshMor(AfDecimal cptRshMor) {
        throw new FieldNotMappedException("cptRshMor");
    }

    @Override
    public AfDecimal getCptRshMorObj() {
        return getCptRshMor();
    }

    @Override
    public void setCptRshMorObj(AfDecimal cptRshMorObj) {
        setCptRshMor(new AfDecimal(cptRshMorObj, 15, 3));
    }

    @Override
    public AfDecimal getCumRiscpar() {
        throw new FieldNotMappedException("cumRiscpar");
    }

    @Override
    public void setCumRiscpar(AfDecimal cumRiscpar) {
        throw new FieldNotMappedException("cumRiscpar");
    }

    @Override
    public AfDecimal getCumRiscparObj() {
        return getCumRiscpar();
    }

    @Override
    public void setCumRiscparObj(AfDecimal cumRiscparObj) {
        setCumRiscpar(new AfDecimal(cumRiscparObj, 15, 3));
    }

    @Override
    public AfDecimal getDir() {
        throw new FieldNotMappedException("dir");
    }

    @Override
    public void setDir(AfDecimal dir) {
        throw new FieldNotMappedException("dir");
    }

    @Override
    public AfDecimal getDirEmis() {
        throw new FieldNotMappedException("dirEmis");
    }

    @Override
    public void setDirEmis(AfDecimal dirEmis) {
        throw new FieldNotMappedException("dirEmis");
    }

    @Override
    public AfDecimal getDirEmisObj() {
        return getDirEmis();
    }

    @Override
    public void setDirEmisObj(AfDecimal dirEmisObj) {
        setDirEmis(new AfDecimal(dirEmisObj, 15, 3));
    }

    @Override
    public AfDecimal getDirObj() {
        return getDir();
    }

    @Override
    public void setDirObj(AfDecimal dirObj) {
        setDir(new AfDecimal(dirObj, 15, 3));
    }

    @Override
    public char getDsOperSql() {
        throw new FieldNotMappedException("dsOperSql");
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        throw new FieldNotMappedException("dsOperSql");
    }

    @Override
    public char getDsStatoElab() {
        throw new FieldNotMappedException("dsStatoElab");
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        throw new FieldNotMappedException("dsStatoElab");
    }

    @Override
    public long getDsTsCptz() {
        throw new FieldNotMappedException("dsTsCptz");
    }

    @Override
    public void setDsTsCptz(long dsTsCptz) {
        throw new FieldNotMappedException("dsTsCptz");
    }

    @Override
    public String getDsUtente() {
        throw new FieldNotMappedException("dsUtente");
    }

    @Override
    public void setDsUtente(String dsUtente) {
        throw new FieldNotMappedException("dsUtente");
    }

    @Override
    public int getDsVer() {
        throw new FieldNotMappedException("dsVer");
    }

    @Override
    public void setDsVer(int dsVer) {
        throw new FieldNotMappedException("dsVer");
    }

    @Override
    public String getDtDecorAdesDb() {
        throw new FieldNotMappedException("dtDecorAdesDb");
    }

    @Override
    public void setDtDecorAdesDb(String dtDecorAdesDb) {
        throw new FieldNotMappedException("dtDecorAdesDb");
    }

    @Override
    public String getDtDecorAdesDbObj() {
        return getDtDecorAdesDb();
    }

    @Override
    public void setDtDecorAdesDbObj(String dtDecorAdesDbObj) {
        setDtDecorAdesDb(dtDecorAdesDbObj);
    }

    @Override
    public String getDtDecorPoliDb() {
        throw new FieldNotMappedException("dtDecorPoliDb");
    }

    @Override
    public void setDtDecorPoliDb(String dtDecorPoliDb) {
        throw new FieldNotMappedException("dtDecorPoliDb");
    }

    @Override
    public String getDtDecorTrchDb() {
        throw new FieldNotMappedException("dtDecorTrchDb");
    }

    @Override
    public void setDtDecorTrchDb(String dtDecorTrchDb) {
        throw new FieldNotMappedException("dtDecorTrchDb");
    }

    @Override
    public String getDtEffCambStatDb() {
        throw new FieldNotMappedException("dtEffCambStatDb");
    }

    @Override
    public void setDtEffCambStatDb(String dtEffCambStatDb) {
        throw new FieldNotMappedException("dtEffCambStatDb");
    }

    @Override
    public String getDtEffCambStatDbObj() {
        return getDtEffCambStatDb();
    }

    @Override
    public void setDtEffCambStatDbObj(String dtEffCambStatDbObj) {
        setDtEffCambStatDb(dtEffCambStatDbObj);
    }

    @Override
    public String getDtEffRidzDb() {
        throw new FieldNotMappedException("dtEffRidzDb");
    }

    @Override
    public void setDtEffRidzDb(String dtEffRidzDb) {
        throw new FieldNotMappedException("dtEffRidzDb");
    }

    @Override
    public String getDtEffRidzDbObj() {
        return getDtEffRidzDb();
    }

    @Override
    public void setDtEffRidzDbObj(String dtEffRidzDbObj) {
        setDtEffRidzDb(dtEffRidzDbObj);
    }

    @Override
    public String getDtEffStabDb() {
        throw new FieldNotMappedException("dtEffStabDb");
    }

    @Override
    public void setDtEffStabDb(String dtEffStabDb) {
        throw new FieldNotMappedException("dtEffStabDb");
    }

    @Override
    public String getDtEffStabDbObj() {
        return getDtEffStabDb();
    }

    @Override
    public void setDtEffStabDbObj(String dtEffStabDbObj) {
        setDtEffStabDb(dtEffStabDbObj);
    }

    @Override
    public String getDtEmisCambStatDb() {
        throw new FieldNotMappedException("dtEmisCambStatDb");
    }

    @Override
    public void setDtEmisCambStatDb(String dtEmisCambStatDb) {
        throw new FieldNotMappedException("dtEmisCambStatDb");
    }

    @Override
    public String getDtEmisCambStatDbObj() {
        return getDtEmisCambStatDb();
    }

    @Override
    public void setDtEmisCambStatDbObj(String dtEmisCambStatDbObj) {
        setDtEmisCambStatDb(dtEmisCambStatDbObj);
    }

    @Override
    public String getDtEmisPoliDb() {
        throw new FieldNotMappedException("dtEmisPoliDb");
    }

    @Override
    public void setDtEmisPoliDb(String dtEmisPoliDb) {
        throw new FieldNotMappedException("dtEmisPoliDb");
    }

    @Override
    public String getDtEmisRidzDb() {
        throw new FieldNotMappedException("dtEmisRidzDb");
    }

    @Override
    public void setDtEmisRidzDb(String dtEmisRidzDb) {
        throw new FieldNotMappedException("dtEmisRidzDb");
    }

    @Override
    public String getDtEmisRidzDbObj() {
        return getDtEmisRidzDb();
    }

    @Override
    public void setDtEmisRidzDbObj(String dtEmisRidzDbObj) {
        setDtEmisRidzDb(dtEmisRidzDbObj);
    }

    @Override
    public String getDtEmisTrchDb() {
        throw new FieldNotMappedException("dtEmisTrchDb");
    }

    @Override
    public void setDtEmisTrchDb(String dtEmisTrchDb) {
        throw new FieldNotMappedException("dtEmisTrchDb");
    }

    @Override
    public String getDtEmisTrchDbObj() {
        return getDtEmisTrchDb();
    }

    @Override
    public void setDtEmisTrchDbObj(String dtEmisTrchDbObj) {
        setDtEmisTrchDb(dtEmisTrchDbObj);
    }

    @Override
    public String getDtIncUltPreDb() {
        throw new FieldNotMappedException("dtIncUltPreDb");
    }

    @Override
    public void setDtIncUltPreDb(String dtIncUltPreDb) {
        throw new FieldNotMappedException("dtIncUltPreDb");
    }

    @Override
    public String getDtIncUltPreDbObj() {
        return getDtIncUltPreDb();
    }

    @Override
    public void setDtIncUltPreDbObj(String dtIncUltPreDbObj) {
        setDtIncUltPreDb(dtIncUltPreDbObj);
    }

    @Override
    public String getDtIniValTarDb() {
        throw new FieldNotMappedException("dtIniValTarDb");
    }

    @Override
    public void setDtIniValTarDb(String dtIniValTarDb) {
        throw new FieldNotMappedException("dtIniValTarDb");
    }

    @Override
    public String getDtIniValTarDbObj() {
        return getDtIniValTarDb();
    }

    @Override
    public void setDtIniValTarDbObj(String dtIniValTarDbObj) {
        setDtIniValTarDb(dtIniValTarDbObj);
    }

    @Override
    public String getDtIniVldtProdDb() {
        throw new FieldNotMappedException("dtIniVldtProdDb");
    }

    @Override
    public void setDtIniVldtProdDb(String dtIniVldtProdDb) {
        throw new FieldNotMappedException("dtIniVldtProdDb");
    }

    @Override
    public String getDtNasc1oAsstoDb() {
        throw new FieldNotMappedException("dtNasc1oAsstoDb");
    }

    @Override
    public void setDtNasc1oAsstoDb(String dtNasc1oAsstoDb) {
        throw new FieldNotMappedException("dtNasc1oAsstoDb");
    }

    @Override
    public String getDtNasc1oAsstoDbObj() {
        return getDtNasc1oAsstoDb();
    }

    @Override
    public void setDtNasc1oAsstoDbObj(String dtNasc1oAsstoDbObj) {
        setDtNasc1oAsstoDb(dtNasc1oAsstoDbObj);
    }

    @Override
    public String getDtProduzioneDb() {
        throw new FieldNotMappedException("dtProduzioneDb");
    }

    @Override
    public void setDtProduzioneDb(String dtProduzioneDb) {
        throw new FieldNotMappedException("dtProduzioneDb");
    }

    @Override
    public String getDtQtzEmisDb() {
        throw new FieldNotMappedException("dtQtzEmisDb");
    }

    @Override
    public void setDtQtzEmisDb(String dtQtzEmisDb) {
        throw new FieldNotMappedException("dtQtzEmisDb");
    }

    @Override
    public String getDtQtzEmisDbObj() {
        return getDtQtzEmisDb();
    }

    @Override
    public void setDtQtzEmisDbObj(String dtQtzEmisDbObj) {
        setDtQtzEmisDb(dtQtzEmisDbObj);
    }

    @Override
    public String getDtRisDb() {
        throw new FieldNotMappedException("dtRisDb");
    }

    @Override
    public void setDtRisDb(String dtRisDb) {
        throw new FieldNotMappedException("dtRisDb");
    }

    @Override
    public String getDtScadIntmdDb() {
        throw new FieldNotMappedException("dtScadIntmdDb");
    }

    @Override
    public void setDtScadIntmdDb(String dtScadIntmdDb) {
        throw new FieldNotMappedException("dtScadIntmdDb");
    }

    @Override
    public String getDtScadIntmdDbObj() {
        return getDtScadIntmdDb();
    }

    @Override
    public void setDtScadIntmdDbObj(String dtScadIntmdDbObj) {
        setDtScadIntmdDb(dtScadIntmdDbObj);
    }

    @Override
    public String getDtScadPagPreDb() {
        throw new FieldNotMappedException("dtScadPagPreDb");
    }

    @Override
    public void setDtScadPagPreDb(String dtScadPagPreDb) {
        throw new FieldNotMappedException("dtScadPagPreDb");
    }

    @Override
    public String getDtScadPagPreDbObj() {
        return getDtScadPagPreDb();
    }

    @Override
    public void setDtScadPagPreDbObj(String dtScadPagPreDbObj) {
        setDtScadPagPreDb(dtScadPagPreDbObj);
    }

    @Override
    public String getDtScadTrchDb() {
        throw new FieldNotMappedException("dtScadTrchDb");
    }

    @Override
    public void setDtScadTrchDb(String dtScadTrchDb) {
        throw new FieldNotMappedException("dtScadTrchDb");
    }

    @Override
    public String getDtScadTrchDbObj() {
        return getDtScadTrchDb();
    }

    @Override
    public void setDtScadTrchDbObj(String dtScadTrchDbObj) {
        setDtScadTrchDb(dtScadTrchDbObj);
    }

    @Override
    public String getDtUltPrePagDb() {
        throw new FieldNotMappedException("dtUltPrePagDb");
    }

    @Override
    public void setDtUltPrePagDb(String dtUltPrePagDb) {
        throw new FieldNotMappedException("dtUltPrePagDb");
    }

    @Override
    public String getDtUltPrePagDbObj() {
        return getDtUltPrePagDb();
    }

    @Override
    public void setDtUltPrePagDbObj(String dtUltPrePagDbObj) {
        setDtUltPrePagDb(dtUltPrePagDbObj);
    }

    @Override
    public String getDtUltRivalDb() {
        throw new FieldNotMappedException("dtUltRivalDb");
    }

    @Override
    public void setDtUltRivalDb(String dtUltRivalDb) {
        throw new FieldNotMappedException("dtUltRivalDb");
    }

    @Override
    public String getDtUltRivalDbObj() {
        return getDtUltRivalDb();
    }

    @Override
    public void setDtUltRivalDbObj(String dtUltRivalDbObj) {
        setDtUltRivalDb(dtUltRivalDbObj);
    }

    @Override
    public int getDur1oPerAa() {
        throw new FieldNotMappedException("dur1oPerAa");
    }

    @Override
    public void setDur1oPerAa(int dur1oPerAa) {
        throw new FieldNotMappedException("dur1oPerAa");
    }

    @Override
    public Integer getDur1oPerAaObj() {
        return ((Integer)getDur1oPerAa());
    }

    @Override
    public void setDur1oPerAaObj(Integer dur1oPerAaObj) {
        setDur1oPerAa(((int)dur1oPerAaObj));
    }

    @Override
    public int getDur1oPerGg() {
        throw new FieldNotMappedException("dur1oPerGg");
    }

    @Override
    public void setDur1oPerGg(int dur1oPerGg) {
        throw new FieldNotMappedException("dur1oPerGg");
    }

    @Override
    public Integer getDur1oPerGgObj() {
        return ((Integer)getDur1oPerGg());
    }

    @Override
    public void setDur1oPerGgObj(Integer dur1oPerGgObj) {
        setDur1oPerGg(((int)dur1oPerGgObj));
    }

    @Override
    public int getDur1oPerMm() {
        throw new FieldNotMappedException("dur1oPerMm");
    }

    @Override
    public void setDur1oPerMm(int dur1oPerMm) {
        throw new FieldNotMappedException("dur1oPerMm");
    }

    @Override
    public Integer getDur1oPerMmObj() {
        return ((Integer)getDur1oPerMm());
    }

    @Override
    public void setDur1oPerMmObj(Integer dur1oPerMmObj) {
        setDur1oPerMm(((int)dur1oPerMmObj));
    }

    @Override
    public int getDurAa() {
        throw new FieldNotMappedException("durAa");
    }

    @Override
    public void setDurAa(int durAa) {
        throw new FieldNotMappedException("durAa");
    }

    @Override
    public Integer getDurAaObj() {
        return ((Integer)getDurAa());
    }

    @Override
    public void setDurAaObj(Integer durAaObj) {
        setDurAa(((int)durAaObj));
    }

    @Override
    public int getDurGarAa() {
        throw new FieldNotMappedException("durGarAa");
    }

    @Override
    public void setDurGarAa(int durGarAa) {
        throw new FieldNotMappedException("durGarAa");
    }

    @Override
    public Integer getDurGarAaObj() {
        return ((Integer)getDurGarAa());
    }

    @Override
    public void setDurGarAaObj(Integer durGarAaObj) {
        setDurGarAa(((int)durGarAaObj));
    }

    @Override
    public int getDurGarGg() {
        throw new FieldNotMappedException("durGarGg");
    }

    @Override
    public void setDurGarGg(int durGarGg) {
        throw new FieldNotMappedException("durGarGg");
    }

    @Override
    public Integer getDurGarGgObj() {
        return ((Integer)getDurGarGg());
    }

    @Override
    public void setDurGarGgObj(Integer durGarGgObj) {
        setDurGarGg(((int)durGarGgObj));
    }

    @Override
    public int getDurGarMm() {
        throw new FieldNotMappedException("durGarMm");
    }

    @Override
    public void setDurGarMm(int durGarMm) {
        throw new FieldNotMappedException("durGarMm");
    }

    @Override
    public Integer getDurGarMmObj() {
        return ((Integer)getDurGarMm());
    }

    @Override
    public void setDurGarMmObj(Integer durGarMmObj) {
        setDurGarMm(((int)durGarMmObj));
    }

    @Override
    public int getDurGg() {
        throw new FieldNotMappedException("durGg");
    }

    @Override
    public void setDurGg(int durGg) {
        throw new FieldNotMappedException("durGg");
    }

    @Override
    public Integer getDurGgObj() {
        return ((Integer)getDurGg());
    }

    @Override
    public void setDurGgObj(Integer durGgObj) {
        setDurGg(((int)durGgObj));
    }

    @Override
    public int getDurMm() {
        throw new FieldNotMappedException("durMm");
    }

    @Override
    public void setDurMm(int durMm) {
        throw new FieldNotMappedException("durMm");
    }

    @Override
    public Integer getDurMmObj() {
        return ((Integer)getDurMm());
    }

    @Override
    public void setDurMmObj(Integer durMmObj) {
        setDurMm(((int)durMmObj));
    }

    @Override
    public int getDurPagPre() {
        throw new FieldNotMappedException("durPagPre");
    }

    @Override
    public void setDurPagPre(int durPagPre) {
        throw new FieldNotMappedException("durPagPre");
    }

    @Override
    public Integer getDurPagPreObj() {
        return ((Integer)getDurPagPre());
    }

    @Override
    public void setDurPagPreObj(Integer durPagPreObj) {
        setDurPagPre(((int)durPagPreObj));
    }

    @Override
    public int getDurPagRen() {
        throw new FieldNotMappedException("durPagRen");
    }

    @Override
    public void setDurPagRen(int durPagRen) {
        throw new FieldNotMappedException("durPagRen");
    }

    @Override
    public Integer getDurPagRenObj() {
        return ((Integer)getDurPagRen());
    }

    @Override
    public void setDurPagRenObj(Integer durPagRenObj) {
        setDurPagRen(((int)durPagRenObj));
    }

    @Override
    public AfDecimal getDurResDtCalc() {
        throw new FieldNotMappedException("durResDtCalc");
    }

    @Override
    public void setDurResDtCalc(AfDecimal durResDtCalc) {
        throw new FieldNotMappedException("durResDtCalc");
    }

    @Override
    public AfDecimal getDurResDtCalcObj() {
        return getDurResDtCalc();
    }

    @Override
    public void setDurResDtCalcObj(AfDecimal durResDtCalcObj) {
        setDurResDtCalc(new AfDecimal(durResDtCalcObj, 11, 7));
    }

    @Override
    public String getEmitTitOpzVchar() {
        throw new FieldNotMappedException("emitTitOpzVchar");
    }

    @Override
    public void setEmitTitOpzVchar(String emitTitOpzVchar) {
        throw new FieldNotMappedException("emitTitOpzVchar");
    }

    @Override
    public String getEmitTitOpzVcharObj() {
        return getEmitTitOpzVchar();
    }

    @Override
    public void setEmitTitOpzVcharObj(String emitTitOpzVcharObj) {
        setEmitTitOpzVchar(emitTitOpzVcharObj);
    }

    @Override
    public int getEtaAa1oAssto() {
        throw new FieldNotMappedException("etaAa1oAssto");
    }

    @Override
    public void setEtaAa1oAssto(int etaAa1oAssto) {
        throw new FieldNotMappedException("etaAa1oAssto");
    }

    @Override
    public Integer getEtaAa1oAsstoObj() {
        return ((Integer)getEtaAa1oAssto());
    }

    @Override
    public void setEtaAa1oAsstoObj(Integer etaAa1oAsstoObj) {
        setEtaAa1oAssto(((int)etaAa1oAsstoObj));
    }

    @Override
    public int getEtaMm1oAssto() {
        throw new FieldNotMappedException("etaMm1oAssto");
    }

    @Override
    public void setEtaMm1oAssto(int etaMm1oAssto) {
        throw new FieldNotMappedException("etaMm1oAssto");
    }

    @Override
    public Integer getEtaMm1oAsstoObj() {
        return ((Integer)getEtaMm1oAssto());
    }

    @Override
    public void setEtaMm1oAsstoObj(Integer etaMm1oAsstoObj) {
        setEtaMm1oAssto(((int)etaMm1oAsstoObj));
    }

    @Override
    public AfDecimal getEtaRaggnDtCalc() {
        throw new FieldNotMappedException("etaRaggnDtCalc");
    }

    @Override
    public void setEtaRaggnDtCalc(AfDecimal etaRaggnDtCalc) {
        throw new FieldNotMappedException("etaRaggnDtCalc");
    }

    @Override
    public AfDecimal getEtaRaggnDtCalcObj() {
        return getEtaRaggnDtCalc();
    }

    @Override
    public void setEtaRaggnDtCalcObj(AfDecimal etaRaggnDtCalcObj) {
        setEtaRaggnDtCalc(new AfDecimal(etaRaggnDtCalcObj, 7, 3));
    }

    @Override
    public char getFlCarCont() {
        throw new FieldNotMappedException("flCarCont");
    }

    @Override
    public void setFlCarCont(char flCarCont) {
        throw new FieldNotMappedException("flCarCont");
    }

    @Override
    public Character getFlCarContObj() {
        return ((Character)getFlCarCont());
    }

    @Override
    public void setFlCarContObj(Character flCarContObj) {
        setFlCarCont(((char)flCarContObj));
    }

    @Override
    public char getFlDaTrasf() {
        throw new FieldNotMappedException("flDaTrasf");
    }

    @Override
    public void setFlDaTrasf(char flDaTrasf) {
        throw new FieldNotMappedException("flDaTrasf");
    }

    @Override
    public Character getFlDaTrasfObj() {
        return ((Character)getFlDaTrasf());
    }

    @Override
    public void setFlDaTrasfObj(Character flDaTrasfObj) {
        setFlDaTrasf(((char)flDaTrasfObj));
    }

    @Override
    public char getFlIas() {
        throw new FieldNotMappedException("flIas");
    }

    @Override
    public void setFlIas(char flIas) {
        throw new FieldNotMappedException("flIas");
    }

    @Override
    public Character getFlIasObj() {
        return ((Character)getFlIas());
    }

    @Override
    public void setFlIasObj(Character flIasObj) {
        setFlIas(((char)flIasObj));
    }

    @Override
    public char getFlPreAgg() {
        throw new FieldNotMappedException("flPreAgg");
    }

    @Override
    public void setFlPreAgg(char flPreAgg) {
        throw new FieldNotMappedException("flPreAgg");
    }

    @Override
    public Character getFlPreAggObj() {
        return ((Character)getFlPreAgg());
    }

    @Override
    public void setFlPreAggObj(Character flPreAggObj) {
        setFlPreAgg(((char)flPreAggObj));
    }

    @Override
    public char getFlPreDaRis() {
        throw new FieldNotMappedException("flPreDaRis");
    }

    @Override
    public void setFlPreDaRis(char flPreDaRis) {
        throw new FieldNotMappedException("flPreDaRis");
    }

    @Override
    public Character getFlPreDaRisObj() {
        return ((Character)getFlPreDaRis());
    }

    @Override
    public void setFlPreDaRisObj(Character flPreDaRisObj) {
        setFlPreDaRis(((char)flPreDaRisObj));
    }

    @Override
    public char getFlSimulazione() {
        throw new FieldNotMappedException("flSimulazione");
    }

    @Override
    public void setFlSimulazione(char flSimulazione) {
        throw new FieldNotMappedException("flSimulazione");
    }

    @Override
    public Character getFlSimulazioneObj() {
        return ((Character)getFlSimulazione());
    }

    @Override
    public void setFlSimulazioneObj(Character flSimulazioneObj) {
        setFlSimulazione(((char)flSimulazioneObj));
    }

    @Override
    public char getFlSwitch() {
        throw new FieldNotMappedException("flSwitch");
    }

    @Override
    public void setFlSwitch(char flSwitch) {
        throw new FieldNotMappedException("flSwitch");
    }

    @Override
    public Character getFlSwitchObj() {
        return ((Character)getFlSwitch());
    }

    @Override
    public void setFlSwitchObj(Character flSwitchObj) {
        setFlSwitch(((char)flSwitchObj));
    }

    @Override
    public int getFraz() {
        throw new FieldNotMappedException("fraz");
    }

    @Override
    public void setFraz(int fraz) {
        throw new FieldNotMappedException("fraz");
    }

    @Override
    public int getFrazDecrCpt() {
        throw new FieldNotMappedException("frazDecrCpt");
    }

    @Override
    public void setFrazDecrCpt(int frazDecrCpt) {
        throw new FieldNotMappedException("frazDecrCpt");
    }

    @Override
    public Integer getFrazDecrCptObj() {
        return ((Integer)getFrazDecrCpt());
    }

    @Override
    public void setFrazDecrCptObj(Integer frazDecrCptObj) {
        setFrazDecrCpt(((int)frazDecrCptObj));
    }

    @Override
    public int getFrazIniErogRen() {
        throw new FieldNotMappedException("frazIniErogRen");
    }

    @Override
    public void setFrazIniErogRen(int frazIniErogRen) {
        throw new FieldNotMappedException("frazIniErogRen");
    }

    @Override
    public Integer getFrazIniErogRenObj() {
        return ((Integer)getFrazIniErogRen());
    }

    @Override
    public void setFrazIniErogRenObj(Integer frazIniErogRenObj) {
        setFrazIniErogRen(((int)frazIniErogRenObj));
    }

    @Override
    public Integer getFrazObj() {
        return ((Integer)getFraz());
    }

    @Override
    public void setFrazObj(Integer frazObj) {
        setFraz(((int)frazObj));
    }

    @Override
    public String getIbAccComm() {
        throw new FieldNotMappedException("ibAccComm");
    }

    @Override
    public void setIbAccComm(String ibAccComm) {
        throw new FieldNotMappedException("ibAccComm");
    }

    @Override
    public String getIbAccCommObj() {
        return getIbAccComm();
    }

    @Override
    public void setIbAccCommObj(String ibAccCommObj) {
        setIbAccComm(ibAccCommObj);
    }

    @Override
    public String getIbAdes() {
        throw new FieldNotMappedException("ibAdes");
    }

    @Override
    public void setIbAdes(String ibAdes) {
        throw new FieldNotMappedException("ibAdes");
    }

    @Override
    public String getIbAdesObj() {
        return getIbAdes();
    }

    @Override
    public void setIbAdesObj(String ibAdesObj) {
        setIbAdes(ibAdesObj);
    }

    @Override
    public String getIbPoli() {
        throw new FieldNotMappedException("ibPoli");
    }

    @Override
    public void setIbPoli(String ibPoli) {
        throw new FieldNotMappedException("ibPoli");
    }

    @Override
    public String getIbPoliObj() {
        return getIbPoli();
    }

    @Override
    public void setIbPoliObj(String ibPoliObj) {
        setIbPoli(ibPoliObj);
    }

    @Override
    public String getIbTrchDiGar() {
        throw new FieldNotMappedException("ibTrchDiGar");
    }

    @Override
    public void setIbTrchDiGar(String ibTrchDiGar) {
        throw new FieldNotMappedException("ibTrchDiGar");
    }

    @Override
    public String getIbTrchDiGarObj() {
        return getIbTrchDiGar();
    }

    @Override
    public void setIbTrchDiGarObj(String ibTrchDiGarObj) {
        setIbTrchDiGar(ibTrchDiGarObj);
    }

    @Override
    public int getIdGar() {
        throw new FieldNotMappedException("idGar");
    }

    @Override
    public void setIdGar(int idGar) {
        throw new FieldNotMappedException("idGar");
    }

    @Override
    public int getIdRichEstrazAgg() {
        return bilaTrchEstr.getB03IdRichEstrazAgg().getB03IdRichEstrazAgg();
    }

    @Override
    public void setIdRichEstrazAgg(int idRichEstrazAgg) {
        this.bilaTrchEstr.getB03IdRichEstrazAgg().setB03IdRichEstrazAgg(idRichEstrazAgg);
    }

    @Override
    public Integer getIdRichEstrazAggObj() {
        if (ws.getIndBilaTrchEstr().getIdRichEstrazAgg() >= 0) {
            return ((Integer)getIdRichEstrazAgg());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdRichEstrazAggObj(Integer idRichEstrazAggObj) {
        if (idRichEstrazAggObj != null) {
            setIdRichEstrazAgg(((int)idRichEstrazAggObj));
            ws.getIndBilaTrchEstr().setIdRichEstrazAgg(((short)0));
        }
        else {
            ws.getIndBilaTrchEstr().setIdRichEstrazAgg(((short)-1));
        }
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        throw new FieldNotMappedException("idsv0003CodiceCompagniaAnia");
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        throw new FieldNotMappedException("idsv0003CodiceCompagniaAnia");
    }

    @Override
    public AfDecimal getImpCarCasoMor() {
        throw new FieldNotMappedException("impCarCasoMor");
    }

    @Override
    public void setImpCarCasoMor(AfDecimal impCarCasoMor) {
        throw new FieldNotMappedException("impCarCasoMor");
    }

    @Override
    public AfDecimal getImpCarCasoMorObj() {
        return getImpCarCasoMor();
    }

    @Override
    public void setImpCarCasoMorObj(AfDecimal impCarCasoMorObj) {
        setImpCarCasoMor(new AfDecimal(impCarCasoMorObj, 15, 3));
    }

    @Override
    public AfDecimal getIntrTecn() {
        throw new FieldNotMappedException("intrTecn");
    }

    @Override
    public void setIntrTecn(AfDecimal intrTecn) {
        throw new FieldNotMappedException("intrTecn");
    }

    @Override
    public AfDecimal getIntrTecnObj() {
        return getIntrTecn();
    }

    @Override
    public void setIntrTecnObj(AfDecimal intrTecnObj) {
        setIntrTecn(new AfDecimal(intrTecnObj, 6, 3));
    }

    @Override
    public short getMetRiscSpcl() {
        throw new FieldNotMappedException("metRiscSpcl");
    }

    @Override
    public void setMetRiscSpcl(short metRiscSpcl) {
        throw new FieldNotMappedException("metRiscSpcl");
    }

    @Override
    public Short getMetRiscSpclObj() {
        return ((Short)getMetRiscSpcl());
    }

    @Override
    public void setMetRiscSpclObj(Short metRiscSpclObj) {
        setMetRiscSpcl(((short)metRiscSpclObj));
    }

    @Override
    public AfDecimal getMinGartoT() {
        throw new FieldNotMappedException("minGartoT");
    }

    @Override
    public void setMinGartoT(AfDecimal minGartoT) {
        throw new FieldNotMappedException("minGartoT");
    }

    @Override
    public AfDecimal getMinGartoTObj() {
        return getMinGartoT();
    }

    @Override
    public void setMinGartoTObj(AfDecimal minGartoTObj) {
        setMinGartoT(new AfDecimal(minGartoTObj, 15, 3));
    }

    @Override
    public AfDecimal getMinTrnutT() {
        throw new FieldNotMappedException("minTrnutT");
    }

    @Override
    public void setMinTrnutT(AfDecimal minTrnutT) {
        throw new FieldNotMappedException("minTrnutT");
    }

    @Override
    public AfDecimal getMinTrnutTObj() {
        return getMinTrnutT();
    }

    @Override
    public void setMinTrnutTObj(AfDecimal minTrnutTObj) {
        setMinTrnutT(new AfDecimal(minTrnutTObj, 15, 3));
    }

    @Override
    public AfDecimal getNsQuo() {
        throw new FieldNotMappedException("nsQuo");
    }

    @Override
    public void setNsQuo(AfDecimal nsQuo) {
        throw new FieldNotMappedException("nsQuo");
    }

    @Override
    public AfDecimal getNsQuoObj() {
        return getNsQuo();
    }

    @Override
    public void setNsQuoObj(AfDecimal nsQuoObj) {
        setNsQuo(new AfDecimal(nsQuoObj, 6, 3));
    }

    @Override
    public String getNumFinanz() {
        throw new FieldNotMappedException("numFinanz");
    }

    @Override
    public void setNumFinanz(String numFinanz) {
        throw new FieldNotMappedException("numFinanz");
    }

    @Override
    public String getNumFinanzObj() {
        return getNumFinanz();
    }

    @Override
    public void setNumFinanzObj(String numFinanzObj) {
        setNumFinanz(numFinanzObj);
    }

    @Override
    public int getNumPrePatt() {
        throw new FieldNotMappedException("numPrePatt");
    }

    @Override
    public void setNumPrePatt(int numPrePatt) {
        throw new FieldNotMappedException("numPrePatt");
    }

    @Override
    public Integer getNumPrePattObj() {
        return ((Integer)getNumPrePatt());
    }

    @Override
    public void setNumPrePattObj(Integer numPrePattObj) {
        setNumPrePatt(((int)numPrePattObj));
    }

    @Override
    public AfDecimal getOverComm() {
        throw new FieldNotMappedException("overComm");
    }

    @Override
    public void setOverComm(AfDecimal overComm) {
        throw new FieldNotMappedException("overComm");
    }

    @Override
    public AfDecimal getOverCommObj() {
        return getOverComm();
    }

    @Override
    public void setOverCommObj(AfDecimal overCommObj) {
        setOverComm(new AfDecimal(overCommObj, 15, 3));
    }

    @Override
    public AfDecimal getPcCarAcq() {
        throw new FieldNotMappedException("pcCarAcq");
    }

    @Override
    public void setPcCarAcq(AfDecimal pcCarAcq) {
        throw new FieldNotMappedException("pcCarAcq");
    }

    @Override
    public AfDecimal getPcCarAcqObj() {
        return getPcCarAcq();
    }

    @Override
    public void setPcCarAcqObj(AfDecimal pcCarAcqObj) {
        setPcCarAcq(new AfDecimal(pcCarAcqObj, 6, 3));
    }

    @Override
    public AfDecimal getPcCarGest() {
        throw new FieldNotMappedException("pcCarGest");
    }

    @Override
    public void setPcCarGest(AfDecimal pcCarGest) {
        throw new FieldNotMappedException("pcCarGest");
    }

    @Override
    public AfDecimal getPcCarGestObj() {
        return getPcCarGest();
    }

    @Override
    public void setPcCarGestObj(AfDecimal pcCarGestObj) {
        setPcCarGest(new AfDecimal(pcCarGestObj, 6, 3));
    }

    @Override
    public AfDecimal getPcCarMor() {
        throw new FieldNotMappedException("pcCarMor");
    }

    @Override
    public void setPcCarMor(AfDecimal pcCarMor) {
        throw new FieldNotMappedException("pcCarMor");
    }

    @Override
    public AfDecimal getPcCarMorObj() {
        return getPcCarMor();
    }

    @Override
    public void setPcCarMorObj(AfDecimal pcCarMorObj) {
        setPcCarMor(new AfDecimal(pcCarMorObj, 6, 3));
    }

    @Override
    public char getPpInvrioTari() {
        throw new FieldNotMappedException("ppInvrioTari");
    }

    @Override
    public void setPpInvrioTari(char ppInvrioTari) {
        throw new FieldNotMappedException("ppInvrioTari");
    }

    @Override
    public Character getPpInvrioTariObj() {
        return ((Character)getPpInvrioTari());
    }

    @Override
    public void setPpInvrioTariObj(Character ppInvrioTariObj) {
        setPpInvrioTari(((char)ppInvrioTariObj));
    }

    @Override
    public AfDecimal getPreAnnualizRicor() {
        throw new FieldNotMappedException("preAnnualizRicor");
    }

    @Override
    public void setPreAnnualizRicor(AfDecimal preAnnualizRicor) {
        throw new FieldNotMappedException("preAnnualizRicor");
    }

    @Override
    public AfDecimal getPreAnnualizRicorObj() {
        return getPreAnnualizRicor();
    }

    @Override
    public void setPreAnnualizRicorObj(AfDecimal preAnnualizRicorObj) {
        setPreAnnualizRicor(new AfDecimal(preAnnualizRicorObj, 15, 3));
    }

    @Override
    public AfDecimal getPreCont() {
        throw new FieldNotMappedException("preCont");
    }

    @Override
    public void setPreCont(AfDecimal preCont) {
        throw new FieldNotMappedException("preCont");
    }

    @Override
    public AfDecimal getPreContObj() {
        return getPreCont();
    }

    @Override
    public void setPreContObj(AfDecimal preContObj) {
        setPreCont(new AfDecimal(preContObj, 15, 3));
    }

    @Override
    public AfDecimal getPreDovIni() {
        throw new FieldNotMappedException("preDovIni");
    }

    @Override
    public void setPreDovIni(AfDecimal preDovIni) {
        throw new FieldNotMappedException("preDovIni");
    }

    @Override
    public AfDecimal getPreDovIniObj() {
        return getPreDovIni();
    }

    @Override
    public void setPreDovIniObj(AfDecimal preDovIniObj) {
        setPreDovIni(new AfDecimal(preDovIniObj, 15, 3));
    }

    @Override
    public AfDecimal getPreDovRivtoT() {
        throw new FieldNotMappedException("preDovRivtoT");
    }

    @Override
    public void setPreDovRivtoT(AfDecimal preDovRivtoT) {
        throw new FieldNotMappedException("preDovRivtoT");
    }

    @Override
    public AfDecimal getPreDovRivtoTObj() {
        return getPreDovRivtoT();
    }

    @Override
    public void setPreDovRivtoTObj(AfDecimal preDovRivtoTObj) {
        setPreDovRivtoT(new AfDecimal(preDovRivtoTObj, 15, 3));
    }

    @Override
    public AfDecimal getPrePattuitoIni() {
        throw new FieldNotMappedException("prePattuitoIni");
    }

    @Override
    public void setPrePattuitoIni(AfDecimal prePattuitoIni) {
        throw new FieldNotMappedException("prePattuitoIni");
    }

    @Override
    public AfDecimal getPrePattuitoIniObj() {
        return getPrePattuitoIni();
    }

    @Override
    public void setPrePattuitoIniObj(AfDecimal prePattuitoIniObj) {
        setPrePattuitoIni(new AfDecimal(prePattuitoIniObj, 15, 3));
    }

    @Override
    public AfDecimal getPrePpIni() {
        throw new FieldNotMappedException("prePpIni");
    }

    @Override
    public void setPrePpIni(AfDecimal prePpIni) {
        throw new FieldNotMappedException("prePpIni");
    }

    @Override
    public AfDecimal getPrePpIniObj() {
        return getPrePpIni();
    }

    @Override
    public void setPrePpIniObj(AfDecimal prePpIniObj) {
        setPrePpIni(new AfDecimal(prePpIniObj, 15, 3));
    }

    @Override
    public AfDecimal getPrePpUlt() {
        throw new FieldNotMappedException("prePpUlt");
    }

    @Override
    public void setPrePpUlt(AfDecimal prePpUlt) {
        throw new FieldNotMappedException("prePpUlt");
    }

    @Override
    public AfDecimal getPrePpUltObj() {
        return getPrePpUlt();
    }

    @Override
    public void setPrePpUltObj(AfDecimal prePpUltObj) {
        setPrePpUlt(new AfDecimal(prePpUltObj, 15, 3));
    }

    @Override
    public AfDecimal getPreRiasto() {
        throw new FieldNotMappedException("preRiasto");
    }

    @Override
    public void setPreRiasto(AfDecimal preRiasto) {
        throw new FieldNotMappedException("preRiasto");
    }

    @Override
    public AfDecimal getPreRiastoEcc() {
        throw new FieldNotMappedException("preRiastoEcc");
    }

    @Override
    public void setPreRiastoEcc(AfDecimal preRiastoEcc) {
        throw new FieldNotMappedException("preRiastoEcc");
    }

    @Override
    public AfDecimal getPreRiastoEccObj() {
        return getPreRiastoEcc();
    }

    @Override
    public void setPreRiastoEccObj(AfDecimal preRiastoEccObj) {
        setPreRiastoEcc(new AfDecimal(preRiastoEccObj, 15, 3));
    }

    @Override
    public AfDecimal getPreRiastoObj() {
        return getPreRiasto();
    }

    @Override
    public void setPreRiastoObj(AfDecimal preRiastoObj) {
        setPreRiasto(new AfDecimal(preRiastoObj, 15, 3));
    }

    @Override
    public AfDecimal getPreRshT() {
        throw new FieldNotMappedException("preRshT");
    }

    @Override
    public void setPreRshT(AfDecimal preRshT) {
        throw new FieldNotMappedException("preRshT");
    }

    @Override
    public AfDecimal getPreRshTObj() {
        return getPreRshT();
    }

    @Override
    public void setPreRshTObj(AfDecimal preRshTObj) {
        setPreRshT(new AfDecimal(preRshTObj, 15, 3));
    }

    @Override
    public AfDecimal getProvAcq() {
        throw new FieldNotMappedException("provAcq");
    }

    @Override
    public void setProvAcq(AfDecimal provAcq) {
        throw new FieldNotMappedException("provAcq");
    }

    @Override
    public AfDecimal getProvAcqObj() {
        return getProvAcq();
    }

    @Override
    public void setProvAcqObj(AfDecimal provAcqObj) {
        setProvAcq(new AfDecimal(provAcqObj, 15, 3));
    }

    @Override
    public AfDecimal getProvAcqRicor() {
        throw new FieldNotMappedException("provAcqRicor");
    }

    @Override
    public void setProvAcqRicor(AfDecimal provAcqRicor) {
        throw new FieldNotMappedException("provAcqRicor");
    }

    @Override
    public AfDecimal getProvAcqRicorObj() {
        return getProvAcqRicor();
    }

    @Override
    public void setProvAcqRicorObj(AfDecimal provAcqRicorObj) {
        setProvAcqRicor(new AfDecimal(provAcqRicorObj, 15, 3));
    }

    @Override
    public AfDecimal getProvInc() {
        throw new FieldNotMappedException("provInc");
    }

    @Override
    public void setProvInc(AfDecimal provInc) {
        throw new FieldNotMappedException("provInc");
    }

    @Override
    public AfDecimal getProvIncObj() {
        return getProvInc();
    }

    @Override
    public void setProvIncObj(AfDecimal provIncObj) {
        setProvInc(new AfDecimal(provIncObj, 15, 3));
    }

    @Override
    public AfDecimal getPrstzAggIni() {
        throw new FieldNotMappedException("prstzAggIni");
    }

    @Override
    public void setPrstzAggIni(AfDecimal prstzAggIni) {
        throw new FieldNotMappedException("prstzAggIni");
    }

    @Override
    public AfDecimal getPrstzAggIniObj() {
        return getPrstzAggIni();
    }

    @Override
    public void setPrstzAggIniObj(AfDecimal prstzAggIniObj) {
        setPrstzAggIni(new AfDecimal(prstzAggIniObj, 15, 3));
    }

    @Override
    public AfDecimal getPrstzAggUlt() {
        throw new FieldNotMappedException("prstzAggUlt");
    }

    @Override
    public void setPrstzAggUlt(AfDecimal prstzAggUlt) {
        throw new FieldNotMappedException("prstzAggUlt");
    }

    @Override
    public AfDecimal getPrstzAggUltObj() {
        return getPrstzAggUlt();
    }

    @Override
    public void setPrstzAggUltObj(AfDecimal prstzAggUltObj) {
        setPrstzAggUlt(new AfDecimal(prstzAggUltObj, 15, 3));
    }

    @Override
    public AfDecimal getPrstzIni() {
        throw new FieldNotMappedException("prstzIni");
    }

    @Override
    public void setPrstzIni(AfDecimal prstzIni) {
        throw new FieldNotMappedException("prstzIni");
    }

    @Override
    public AfDecimal getPrstzIniObj() {
        return getPrstzIni();
    }

    @Override
    public void setPrstzIniObj(AfDecimal prstzIniObj) {
        setPrstzIni(new AfDecimal(prstzIniObj, 15, 3));
    }

    @Override
    public AfDecimal getPrstzT() {
        throw new FieldNotMappedException("prstzT");
    }

    @Override
    public void setPrstzT(AfDecimal prstzT) {
        throw new FieldNotMappedException("prstzT");
    }

    @Override
    public AfDecimal getPrstzTObj() {
        return getPrstzT();
    }

    @Override
    public void setPrstzTObj(AfDecimal prstzTObj) {
        setPrstzT(new AfDecimal(prstzTObj, 15, 3));
    }

    @Override
    public AfDecimal getQtzSpZCoupDtC() {
        throw new FieldNotMappedException("qtzSpZCoupDtC");
    }

    @Override
    public void setQtzSpZCoupDtC(AfDecimal qtzSpZCoupDtC) {
        throw new FieldNotMappedException("qtzSpZCoupDtC");
    }

    @Override
    public AfDecimal getQtzSpZCoupDtCObj() {
        return getQtzSpZCoupDtC();
    }

    @Override
    public void setQtzSpZCoupDtCObj(AfDecimal qtzSpZCoupDtCObj) {
        setQtzSpZCoupDtC(new AfDecimal(qtzSpZCoupDtCObj, 12, 5));
    }

    @Override
    public AfDecimal getQtzSpZCoupEmis() {
        throw new FieldNotMappedException("qtzSpZCoupEmis");
    }

    @Override
    public void setQtzSpZCoupEmis(AfDecimal qtzSpZCoupEmis) {
        throw new FieldNotMappedException("qtzSpZCoupEmis");
    }

    @Override
    public AfDecimal getQtzSpZCoupEmisObj() {
        return getQtzSpZCoupEmis();
    }

    @Override
    public void setQtzSpZCoupEmisObj(AfDecimal qtzSpZCoupEmisObj) {
        setQtzSpZCoupEmis(new AfDecimal(qtzSpZCoupEmisObj, 12, 5));
    }

    @Override
    public AfDecimal getQtzSpZOpzDtCa() {
        throw new FieldNotMappedException("qtzSpZOpzDtCa");
    }

    @Override
    public void setQtzSpZOpzDtCa(AfDecimal qtzSpZOpzDtCa) {
        throw new FieldNotMappedException("qtzSpZOpzDtCa");
    }

    @Override
    public AfDecimal getQtzSpZOpzDtCaObj() {
        return getQtzSpZOpzDtCa();
    }

    @Override
    public void setQtzSpZOpzDtCaObj(AfDecimal qtzSpZOpzDtCaObj) {
        setQtzSpZOpzDtCa(new AfDecimal(qtzSpZOpzDtCaObj, 12, 5));
    }

    @Override
    public AfDecimal getQtzSpZOpzEmis() {
        throw new FieldNotMappedException("qtzSpZOpzEmis");
    }

    @Override
    public void setQtzSpZOpzEmis(AfDecimal qtzSpZOpzEmis) {
        throw new FieldNotMappedException("qtzSpZOpzEmis");
    }

    @Override
    public AfDecimal getQtzSpZOpzEmisObj() {
        return getQtzSpZOpzEmis();
    }

    @Override
    public void setQtzSpZOpzEmisObj(AfDecimal qtzSpZOpzEmisObj) {
        setQtzSpZOpzEmis(new AfDecimal(qtzSpZOpzEmisObj, 12, 5));
    }

    @Override
    public AfDecimal getQtzTotDtCalc() {
        throw new FieldNotMappedException("qtzTotDtCalc");
    }

    @Override
    public void setQtzTotDtCalc(AfDecimal qtzTotDtCalc) {
        throw new FieldNotMappedException("qtzTotDtCalc");
    }

    @Override
    public AfDecimal getQtzTotDtCalcObj() {
        return getQtzTotDtCalc();
    }

    @Override
    public void setQtzTotDtCalcObj(AfDecimal qtzTotDtCalcObj) {
        setQtzTotDtCalc(new AfDecimal(qtzTotDtCalcObj, 12, 5));
    }

    @Override
    public AfDecimal getQtzTotDtUltBil() {
        throw new FieldNotMappedException("qtzTotDtUltBil");
    }

    @Override
    public void setQtzTotDtUltBil(AfDecimal qtzTotDtUltBil) {
        throw new FieldNotMappedException("qtzTotDtUltBil");
    }

    @Override
    public AfDecimal getQtzTotDtUltBilObj() {
        return getQtzTotDtUltBil();
    }

    @Override
    public void setQtzTotDtUltBilObj(AfDecimal qtzTotDtUltBilObj) {
        setQtzTotDtUltBil(new AfDecimal(qtzTotDtUltBilObj, 12, 5));
    }

    @Override
    public AfDecimal getQtzTotEmis() {
        throw new FieldNotMappedException("qtzTotEmis");
    }

    @Override
    public void setQtzTotEmis(AfDecimal qtzTotEmis) {
        throw new FieldNotMappedException("qtzTotEmis");
    }

    @Override
    public AfDecimal getQtzTotEmisObj() {
        return getQtzTotEmis();
    }

    @Override
    public void setQtzTotEmisObj(AfDecimal qtzTotEmisObj) {
        setQtzTotEmis(new AfDecimal(qtzTotEmisObj, 12, 5));
    }

    @Override
    public String getRamoBila() {
        throw new FieldNotMappedException("ramoBila");
    }

    @Override
    public void setRamoBila(String ramoBila) {
        throw new FieldNotMappedException("ramoBila");
    }

    @Override
    public AfDecimal getRappel() {
        throw new FieldNotMappedException("rappel");
    }

    @Override
    public void setRappel(AfDecimal rappel) {
        throw new FieldNotMappedException("rappel");
    }

    @Override
    public AfDecimal getRappelObj() {
        return getRappel();
    }

    @Override
    public void setRappelObj(AfDecimal rappelObj) {
        setRappel(new AfDecimal(rappelObj, 15, 3));
    }

    @Override
    public AfDecimal getRatRen() {
        throw new FieldNotMappedException("ratRen");
    }

    @Override
    public void setRatRen(AfDecimal ratRen) {
        throw new FieldNotMappedException("ratRen");
    }

    @Override
    public AfDecimal getRatRenObj() {
        return getRatRen();
    }

    @Override
    public void setRatRenObj(AfDecimal ratRenObj) {
        setRatRen(new AfDecimal(ratRenObj, 15, 3));
    }

    @Override
    public AfDecimal getRemunAss() {
        throw new FieldNotMappedException("remunAss");
    }

    @Override
    public void setRemunAss(AfDecimal remunAss) {
        throw new FieldNotMappedException("remunAss");
    }

    @Override
    public AfDecimal getRemunAssObj() {
        return getRemunAss();
    }

    @Override
    public void setRemunAssObj(AfDecimal remunAssObj) {
        setRemunAss(new AfDecimal(remunAssObj, 15, 3));
    }

    @Override
    public AfDecimal getRisAcqT() {
        throw new FieldNotMappedException("risAcqT");
    }

    @Override
    public void setRisAcqT(AfDecimal risAcqT) {
        throw new FieldNotMappedException("risAcqT");
    }

    @Override
    public AfDecimal getRisAcqTObj() {
        return getRisAcqT();
    }

    @Override
    public void setRisAcqTObj(AfDecimal risAcqTObj) {
        setRisAcqT(new AfDecimal(risAcqTObj, 15, 3));
    }

    @Override
    public AfDecimal getRisMatChiuPrec() {
        throw new FieldNotMappedException("risMatChiuPrec");
    }

    @Override
    public void setRisMatChiuPrec(AfDecimal risMatChiuPrec) {
        throw new FieldNotMappedException("risMatChiuPrec");
    }

    @Override
    public AfDecimal getRisMatChiuPrecObj() {
        return getRisMatChiuPrec();
    }

    @Override
    public void setRisMatChiuPrecObj(AfDecimal risMatChiuPrecObj) {
        setRisMatChiuPrec(new AfDecimal(risMatChiuPrecObj, 15, 3));
    }

    @Override
    public AfDecimal getRisPuraT() {
        throw new FieldNotMappedException("risPuraT");
    }

    @Override
    public void setRisPuraT(AfDecimal risPuraT) {
        throw new FieldNotMappedException("risPuraT");
    }

    @Override
    public AfDecimal getRisPuraTObj() {
        return getRisPuraT();
    }

    @Override
    public void setRisPuraTObj(AfDecimal risPuraTObj) {
        setRisPuraT(new AfDecimal(risPuraTObj, 15, 3));
    }

    @Override
    public AfDecimal getRisRiasta() {
        throw new FieldNotMappedException("risRiasta");
    }

    @Override
    public void setRisRiasta(AfDecimal risRiasta) {
        throw new FieldNotMappedException("risRiasta");
    }

    @Override
    public AfDecimal getRisRiastaEcc() {
        throw new FieldNotMappedException("risRiastaEcc");
    }

    @Override
    public void setRisRiastaEcc(AfDecimal risRiastaEcc) {
        throw new FieldNotMappedException("risRiastaEcc");
    }

    @Override
    public AfDecimal getRisRiastaEccObj() {
        return getRisRiastaEcc();
    }

    @Override
    public void setRisRiastaEccObj(AfDecimal risRiastaEccObj) {
        setRisRiastaEcc(new AfDecimal(risRiastaEccObj, 15, 3));
    }

    @Override
    public AfDecimal getRisRiastaObj() {
        return getRisRiasta();
    }

    @Override
    public void setRisRiastaObj(AfDecimal risRiastaObj) {
        setRisRiasta(new AfDecimal(risRiastaObj, 15, 3));
    }

    @Override
    public AfDecimal getRisRistorniCap() {
        throw new FieldNotMappedException("risRistorniCap");
    }

    @Override
    public void setRisRistorniCap(AfDecimal risRistorniCap) {
        throw new FieldNotMappedException("risRistorniCap");
    }

    @Override
    public AfDecimal getRisRistorniCapObj() {
        return getRisRistorniCap();
    }

    @Override
    public void setRisRistorniCapObj(AfDecimal risRistorniCapObj) {
        setRisRistorniCap(new AfDecimal(risRistorniCapObj, 15, 3));
    }

    @Override
    public AfDecimal getRisSpeT() {
        throw new FieldNotMappedException("risSpeT");
    }

    @Override
    public void setRisSpeT(AfDecimal risSpeT) {
        throw new FieldNotMappedException("risSpeT");
    }

    @Override
    public AfDecimal getRisSpeTObj() {
        return getRisSpeT();
    }

    @Override
    public void setRisSpeTObj(AfDecimal risSpeTObj) {
        setRisSpeT(new AfDecimal(risSpeTObj, 15, 3));
    }

    @Override
    public AfDecimal getRisZilT() {
        throw new FieldNotMappedException("risZilT");
    }

    @Override
    public void setRisZilT(AfDecimal risZilT) {
        throw new FieldNotMappedException("risZilT");
    }

    @Override
    public AfDecimal getRisZilTObj() {
        return getRisZilT();
    }

    @Override
    public void setRisZilTObj(AfDecimal risZilTObj) {
        setRisZilT(new AfDecimal(risZilTObj, 15, 3));
    }

    @Override
    public AfDecimal getRiscpar() {
        throw new FieldNotMappedException("riscpar");
    }

    @Override
    public void setRiscpar(AfDecimal riscpar) {
        throw new FieldNotMappedException("riscpar");
    }

    @Override
    public AfDecimal getRiscparObj() {
        return getRiscpar();
    }

    @Override
    public void setRiscparObj(AfDecimal riscparObj) {
        setRiscpar(new AfDecimal(riscparObj, 15, 3));
    }

    @Override
    public char getSex1oAssto() {
        throw new FieldNotMappedException("sex1oAssto");
    }

    @Override
    public void setSex1oAssto(char sex1oAssto) {
        throw new FieldNotMappedException("sex1oAssto");
    }

    @Override
    public Character getSex1oAsstoObj() {
        return ((Character)getSex1oAssto());
    }

    @Override
    public void setSex1oAsstoObj(Character sex1oAsstoObj) {
        setSex1oAssto(((char)sex1oAsstoObj));
    }

    @Override
    public char getStatAssto1() {
        throw new FieldNotMappedException("statAssto1");
    }

    @Override
    public void setStatAssto1(char statAssto1) {
        throw new FieldNotMappedException("statAssto1");
    }

    @Override
    public Character getStatAssto1Obj() {
        return ((Character)getStatAssto1());
    }

    @Override
    public void setStatAssto1Obj(Character statAssto1Obj) {
        setStatAssto1(((char)statAssto1Obj));
    }

    @Override
    public char getStatAssto2() {
        throw new FieldNotMappedException("statAssto2");
    }

    @Override
    public void setStatAssto2(char statAssto2) {
        throw new FieldNotMappedException("statAssto2");
    }

    @Override
    public Character getStatAssto2Obj() {
        return ((Character)getStatAssto2());
    }

    @Override
    public void setStatAssto2Obj(Character statAssto2Obj) {
        setStatAssto2(((char)statAssto2Obj));
    }

    @Override
    public char getStatAssto3() {
        throw new FieldNotMappedException("statAssto3");
    }

    @Override
    public void setStatAssto3(char statAssto3) {
        throw new FieldNotMappedException("statAssto3");
    }

    @Override
    public Character getStatAssto3Obj() {
        return ((Character)getStatAssto3());
    }

    @Override
    public void setStatAssto3Obj(Character statAssto3Obj) {
        setStatAssto3(((char)statAssto3Obj));
    }

    @Override
    public char getStatTbgcAssto1() {
        throw new FieldNotMappedException("statTbgcAssto1");
    }

    @Override
    public void setStatTbgcAssto1(char statTbgcAssto1) {
        throw new FieldNotMappedException("statTbgcAssto1");
    }

    @Override
    public Character getStatTbgcAssto1Obj() {
        return ((Character)getStatTbgcAssto1());
    }

    @Override
    public void setStatTbgcAssto1Obj(Character statTbgcAssto1Obj) {
        setStatTbgcAssto1(((char)statTbgcAssto1Obj));
    }

    @Override
    public char getStatTbgcAssto2() {
        throw new FieldNotMappedException("statTbgcAssto2");
    }

    @Override
    public void setStatTbgcAssto2(char statTbgcAssto2) {
        throw new FieldNotMappedException("statTbgcAssto2");
    }

    @Override
    public Character getStatTbgcAssto2Obj() {
        return ((Character)getStatTbgcAssto2());
    }

    @Override
    public void setStatTbgcAssto2Obj(Character statTbgcAssto2Obj) {
        setStatTbgcAssto2(((char)statTbgcAssto2Obj));
    }

    @Override
    public char getStatTbgcAssto3() {
        throw new FieldNotMappedException("statTbgcAssto3");
    }

    @Override
    public void setStatTbgcAssto3(char statTbgcAssto3) {
        throw new FieldNotMappedException("statTbgcAssto3");
    }

    @Override
    public Character getStatTbgcAssto3Obj() {
        return ((Character)getStatTbgcAssto3());
    }

    @Override
    public void setStatTbgcAssto3Obj(Character statTbgcAssto3Obj) {
        setStatTbgcAssto3(((char)statTbgcAssto3Obj));
    }

    @Override
    public String getTpAccComm() {
        throw new FieldNotMappedException("tpAccComm");
    }

    @Override
    public void setTpAccComm(String tpAccComm) {
        throw new FieldNotMappedException("tpAccComm");
    }

    @Override
    public String getTpAccCommObj() {
        return getTpAccComm();
    }

    @Override
    public void setTpAccCommObj(String tpAccCommObj) {
        setTpAccComm(tpAccCommObj);
    }

    @Override
    public char getTpAdegPre() {
        throw new FieldNotMappedException("tpAdegPre");
    }

    @Override
    public void setTpAdegPre(char tpAdegPre) {
        throw new FieldNotMappedException("tpAdegPre");
    }

    @Override
    public Character getTpAdegPreObj() {
        return ((Character)getTpAdegPre());
    }

    @Override
    public void setTpAdegPreObj(Character tpAdegPreObj) {
        setTpAdegPre(((char)tpAdegPreObj));
    }

    @Override
    public String getTpCalcRis() {
        throw new FieldNotMappedException("tpCalcRis");
    }

    @Override
    public void setTpCalcRis(String tpCalcRis) {
        throw new FieldNotMappedException("tpCalcRis");
    }

    @Override
    public String getTpCausAdes() {
        return bilaTrchEstr.getB03TpCausAdes();
    }

    @Override
    public void setTpCausAdes(String tpCausAdes) {
        this.bilaTrchEstr.setB03TpCausAdes(tpCausAdes);
    }

    @Override
    public String getTpCausPoli() {
        return bilaTrchEstr.getB03TpCausPoli();
    }

    @Override
    public void setTpCausPoli(String tpCausPoli) {
        this.bilaTrchEstr.setB03TpCausPoli(tpCausPoli);
    }

    @Override
    public String getTpCausTrch() {
        return bilaTrchEstr.getB03TpCausTrch();
    }

    @Override
    public void setTpCausTrch(String tpCausTrch) {
        this.bilaTrchEstr.setB03TpCausTrch(tpCausTrch);
    }

    @Override
    public String getTpCoass() {
        throw new FieldNotMappedException("tpCoass");
    }

    @Override
    public void setTpCoass(String tpCoass) {
        throw new FieldNotMappedException("tpCoass");
    }

    @Override
    public String getTpCoassObj() {
        return getTpCoass();
    }

    @Override
    public void setTpCoassObj(String tpCoassObj) {
        setTpCoass(tpCoassObj);
    }

    @Override
    public String getTpCopCasoMor() {
        throw new FieldNotMappedException("tpCopCasoMor");
    }

    @Override
    public void setTpCopCasoMor(String tpCopCasoMor) {
        throw new FieldNotMappedException("tpCopCasoMor");
    }

    @Override
    public String getTpCopCasoMorObj() {
        return getTpCopCasoMor();
    }

    @Override
    public void setTpCopCasoMorObj(String tpCopCasoMorObj) {
        setTpCopCasoMor(tpCopCasoMorObj);
    }

    @Override
    public String getTpFrmAssva() {
        throw new FieldNotMappedException("tpFrmAssva");
    }

    @Override
    public void setTpFrmAssva(String tpFrmAssva) {
        throw new FieldNotMappedException("tpFrmAssva");
    }

    @Override
    public String getTpIas() {
        throw new FieldNotMappedException("tpIas");
    }

    @Override
    public void setTpIas(String tpIas) {
        throw new FieldNotMappedException("tpIas");
    }

    @Override
    public String getTpIasObj() {
        return getTpIas();
    }

    @Override
    public void setTpIasObj(String tpIasObj) {
        setTpIas(tpIasObj);
    }

    @Override
    public char getTpPre() {
        throw new FieldNotMappedException("tpPre");
    }

    @Override
    public void setTpPre(char tpPre) {
        throw new FieldNotMappedException("tpPre");
    }

    @Override
    public Character getTpPreObj() {
        return ((Character)getTpPre());
    }

    @Override
    public void setTpPreObj(Character tpPreObj) {
        setTpPre(((char)tpPreObj));
    }

    @Override
    public String getTpPrstz() {
        throw new FieldNotMappedException("tpPrstz");
    }

    @Override
    public void setTpPrstz(String tpPrstz) {
        throw new FieldNotMappedException("tpPrstz");
    }

    @Override
    public String getTpPrstzObj() {
        return getTpPrstz();
    }

    @Override
    public void setTpPrstzObj(String tpPrstzObj) {
        setTpPrstz(tpPrstzObj);
    }

    @Override
    public String getTpRamoBila() {
        throw new FieldNotMappedException("tpRamoBila");
    }

    @Override
    public void setTpRamoBila(String tpRamoBila) {
        throw new FieldNotMappedException("tpRamoBila");
    }

    @Override
    public String getTpRgmFisc() {
        throw new FieldNotMappedException("tpRgmFisc");
    }

    @Override
    public void setTpRgmFisc(String tpRgmFisc) {
        throw new FieldNotMappedException("tpRgmFisc");
    }

    @Override
    public String getTpRgmFiscObj() {
        return getTpRgmFisc();
    }

    @Override
    public void setTpRgmFiscObj(String tpRgmFiscObj) {
        setTpRgmFisc(tpRgmFiscObj);
    }

    @Override
    public String getTpRival() {
        throw new FieldNotMappedException("tpRival");
    }

    @Override
    public void setTpRival(String tpRival) {
        throw new FieldNotMappedException("tpRival");
    }

    @Override
    public String getTpRivalObj() {
        return getTpRival();
    }

    @Override
    public void setTpRivalObj(String tpRivalObj) {
        setTpRival(tpRivalObj);
    }

    @Override
    public String getTpStatBusAdes() {
        return bilaTrchEstr.getB03TpStatBusAdes();
    }

    @Override
    public void setTpStatBusAdes(String tpStatBusAdes) {
        this.bilaTrchEstr.setB03TpStatBusAdes(tpStatBusAdes);
    }

    @Override
    public String getTpStatBusPoli() {
        return bilaTrchEstr.getB03TpStatBusPoli();
    }

    @Override
    public void setTpStatBusPoli(String tpStatBusPoli) {
        this.bilaTrchEstr.setB03TpStatBusPoli(tpStatBusPoli);
    }

    @Override
    public String getTpStatBusTrch() {
        return bilaTrchEstr.getB03TpStatBusTrch();
    }

    @Override
    public void setTpStatBusTrch(String tpStatBusTrch) {
        this.bilaTrchEstr.setB03TpStatBusTrch(tpStatBusTrch);
    }

    @Override
    public String getTpStatInvst() {
        throw new FieldNotMappedException("tpStatInvst");
    }

    @Override
    public void setTpStatInvst(String tpStatInvst) {
        throw new FieldNotMappedException("tpStatInvst");
    }

    @Override
    public String getTpStatInvstObj() {
        return getTpStatInvst();
    }

    @Override
    public void setTpStatInvstObj(String tpStatInvstObj) {
        setTpStatInvst(tpStatInvstObj);
    }

    @Override
    public String getTpTari() {
        throw new FieldNotMappedException("tpTari");
    }

    @Override
    public void setTpTari(String tpTari) {
        throw new FieldNotMappedException("tpTari");
    }

    @Override
    public String getTpTariObj() {
        return getTpTari();
    }

    @Override
    public void setTpTariObj(String tpTariObj) {
        setTpTari(tpTariObj);
    }

    @Override
    public String getTpTrasf() {
        throw new FieldNotMappedException("tpTrasf");
    }

    @Override
    public void setTpTrasf(String tpTrasf) {
        throw new FieldNotMappedException("tpTrasf");
    }

    @Override
    public String getTpTrasfObj() {
        return getTpTrasf();
    }

    @Override
    public void setTpTrasfObj(String tpTrasfObj) {
        setTpTrasf(tpTrasfObj);
    }

    @Override
    public String getTpTrch() {
        throw new FieldNotMappedException("tpTrch");
    }

    @Override
    public void setTpTrch(String tpTrch) {
        throw new FieldNotMappedException("tpTrch");
    }

    @Override
    public String getTpTrchObj() {
        return getTpTrch();
    }

    @Override
    public void setTpTrchObj(String tpTrchObj) {
        setTpTrch(tpTrchObj);
    }

    @Override
    public String getTpTst() {
        throw new FieldNotMappedException("tpTst");
    }

    @Override
    public void setTpTst(String tpTst) {
        throw new FieldNotMappedException("tpTst");
    }

    @Override
    public String getTpTstObj() {
        return getTpTst();
    }

    @Override
    public void setTpTstObj(String tpTstObj) {
        setTpTst(tpTstObj);
    }

    @Override
    public char getTpVers() {
        throw new FieldNotMappedException("tpVers");
    }

    @Override
    public void setTpVers(char tpVers) {
        throw new FieldNotMappedException("tpVers");
    }

    @Override
    public Character getTpVersObj() {
        return ((Character)getTpVers());
    }

    @Override
    public void setTpVersObj(Character tpVersObj) {
        setTpVers(((char)tpVersObj));
    }

    @Override
    public String getTratRiass() {
        throw new FieldNotMappedException("tratRiass");
    }

    @Override
    public void setTratRiass(String tratRiass) {
        throw new FieldNotMappedException("tratRiass");
    }

    @Override
    public String getTratRiassEcc() {
        throw new FieldNotMappedException("tratRiassEcc");
    }

    @Override
    public void setTratRiassEcc(String tratRiassEcc) {
        throw new FieldNotMappedException("tratRiassEcc");
    }

    @Override
    public String getTratRiassEccObj() {
        return getTratRiassEcc();
    }

    @Override
    public void setTratRiassEccObj(String tratRiassEccObj) {
        setTratRiassEcc(tratRiassEccObj);
    }

    @Override
    public String getTratRiassObj() {
        return getTratRiass();
    }

    @Override
    public void setTratRiassObj(String tratRiassObj) {
        setTratRiass(tratRiassObj);
    }

    @Override
    public AfDecimal getTsMedio() {
        throw new FieldNotMappedException("tsMedio");
    }

    @Override
    public void setTsMedio(AfDecimal tsMedio) {
        throw new FieldNotMappedException("tsMedio");
    }

    @Override
    public AfDecimal getTsMedioObj() {
        return getTsMedio();
    }

    @Override
    public void setTsMedioObj(AfDecimal tsMedioObj) {
        setTsMedio(new AfDecimal(tsMedioObj, 14, 9));
    }

    @Override
    public AfDecimal getTsNetT() {
        throw new FieldNotMappedException("tsNetT");
    }

    @Override
    public void setTsNetT(AfDecimal tsNetT) {
        throw new FieldNotMappedException("tsNetT");
    }

    @Override
    public AfDecimal getTsNetTObj() {
        return getTsNetT();
    }

    @Override
    public void setTsNetTObj(AfDecimal tsNetTObj) {
        setTsNetT(new AfDecimal(tsNetTObj, 14, 9));
    }

    @Override
    public AfDecimal getTsPp() {
        throw new FieldNotMappedException("tsPp");
    }

    @Override
    public void setTsPp(AfDecimal tsPp) {
        throw new FieldNotMappedException("tsPp");
    }

    @Override
    public AfDecimal getTsPpObj() {
        return getTsPp();
    }

    @Override
    public void setTsPpObj(AfDecimal tsPpObj) {
        setTsPp(new AfDecimal(tsPpObj, 14, 9));
    }

    @Override
    public AfDecimal getTsRendtoSppr() {
        throw new FieldNotMappedException("tsRendtoSppr");
    }

    @Override
    public void setTsRendtoSppr(AfDecimal tsRendtoSppr) {
        throw new FieldNotMappedException("tsRendtoSppr");
    }

    @Override
    public AfDecimal getTsRendtoSpprObj() {
        return getTsRendtoSppr();
    }

    @Override
    public void setTsRendtoSpprObj(AfDecimal tsRendtoSpprObj) {
        setTsRendtoSppr(new AfDecimal(tsRendtoSpprObj, 14, 9));
    }

    @Override
    public AfDecimal getTsRendtoT() {
        throw new FieldNotMappedException("tsRendtoT");
    }

    @Override
    public void setTsRendtoT(AfDecimal tsRendtoT) {
        throw new FieldNotMappedException("tsRendtoT");
    }

    @Override
    public AfDecimal getTsRendtoTObj() {
        return getTsRendtoT();
    }

    @Override
    public void setTsRendtoTObj(AfDecimal tsRendtoTObj) {
        setTsRendtoT(new AfDecimal(tsRendtoTObj, 14, 9));
    }

    @Override
    public AfDecimal getTsStabPre() {
        throw new FieldNotMappedException("tsStabPre");
    }

    @Override
    public void setTsStabPre(AfDecimal tsStabPre) {
        throw new FieldNotMappedException("tsStabPre");
    }

    @Override
    public AfDecimal getTsStabPreObj() {
        return getTsStabPre();
    }

    @Override
    public void setTsStabPreObj(AfDecimal tsStabPreObj) {
        setTsStabPre(new AfDecimal(tsStabPreObj, 14, 9));
    }

    @Override
    public AfDecimal getTsTariDov() {
        throw new FieldNotMappedException("tsTariDov");
    }

    @Override
    public void setTsTariDov(AfDecimal tsTariDov) {
        throw new FieldNotMappedException("tsTariDov");
    }

    @Override
    public AfDecimal getTsTariDovObj() {
        return getTsTariDov();
    }

    @Override
    public void setTsTariDovObj(AfDecimal tsTariDovObj) {
        setTsTariDov(new AfDecimal(tsTariDovObj, 14, 9));
    }

    @Override
    public AfDecimal getTsTariScon() {
        throw new FieldNotMappedException("tsTariScon");
    }

    @Override
    public void setTsTariScon(AfDecimal tsTariScon) {
        throw new FieldNotMappedException("tsTariScon");
    }

    @Override
    public AfDecimal getTsTariSconObj() {
        return getTsTariScon();
    }

    @Override
    public void setTsTariSconObj(AfDecimal tsTariSconObj) {
        setTsTariScon(new AfDecimal(tsTariSconObj, 14, 9));
    }

    @Override
    public AfDecimal getUltRm() {
        throw new FieldNotMappedException("ultRm");
    }

    @Override
    public void setUltRm(AfDecimal ultRm) {
        throw new FieldNotMappedException("ultRm");
    }

    @Override
    public AfDecimal getUltRmObj() {
        return getUltRm();
    }

    @Override
    public void setUltRmObj(AfDecimal ultRmObj) {
        setUltRm(new AfDecimal(ultRmObj, 15, 3));
    }

    @Override
    public String getVlt() {
        throw new FieldNotMappedException("vlt");
    }

    @Override
    public void setVlt(String vlt) {
        throw new FieldNotMappedException("vlt");
    }

    @Override
    public String getVltObj() {
        return getVlt();
    }

    @Override
    public void setVltObj(String vltObj) {
        setVlt(vltObj);
    }

    @Override
    public AfDecimal getcSubrshT() {
        throw new FieldNotMappedException("cSubrshT");
    }

    @Override
    public void setcSubrshT(AfDecimal cSubrshT) {
        throw new FieldNotMappedException("cSubrshT");
    }

    @Override
    public AfDecimal getcSubrshTObj() {
        return getcSubrshT();
    }

    @Override
    public void setcSubrshTObj(AfDecimal cSubrshTObj) {
        setcSubrshT(new AfDecimal(cSubrshTObj, 15, 3));
    }
}
