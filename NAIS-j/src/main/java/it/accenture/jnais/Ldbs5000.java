package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.DettQuest1Dao;
import it.accenture.jnais.commons.data.to.IDettQuest1;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.DettQuestIdbsdeq0;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ldbs5000Data;
import it.accenture.jnais.ws.redefines.DeqIdCompQuest;
import it.accenture.jnais.ws.redefines.DeqIdMoviChiu;
import it.accenture.jnais.ws.redefines.DeqRispDt;
import it.accenture.jnais.ws.redefines.DeqRispImp;
import it.accenture.jnais.ws.redefines.DeqRispNum;
import it.accenture.jnais.ws.redefines.DeqRispPc;
import it.accenture.jnais.ws.redefines.DeqRispTs;

/**Original name: LDBS5000<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  04 GIU 2019.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO AD HOC PER ACCESSO RISORSE DB        *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Ldbs5000 extends Program implements IDettQuest1 {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private DettQuest1Dao dettQuest1Dao = new DettQuest1Dao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Ldbs5000Data ws = new Ldbs5000Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: DETT-QUEST
    private DettQuestIdbsdeq0 dettQuest;

    //==== METHODS ====
    /**Original name: PROGRAM_LDBS5000_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, DettQuestIdbsdeq0 dettQuest) {
        this.idsv0003 = idsv0003;
        this.dettQuest = dettQuest;
        // COB_CODE: MOVE IDSV0003-BUFFER-WHERE-COND TO LDBV5001.
        ws.getLdbv5001().setLdbv5001Formatted(this.idsv0003.getBufferWhereCondFormatted());
        // COB_CODE: PERFORM A000-INIZIO              THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                        a200ElaboraWcEff();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                        b200ElaboraWcCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //               SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                        c200ElaboraWcNst();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: MOVE LDBV5001 TO IDSV0003-BUFFER-WHERE-COND.
        this.idsv0003.setBufferWhereCond(ws.getLdbv5001().getLdbv5001Formatted());
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Ldbs5000 getInstance() {
        return ((Ldbs5000)Programs.getInstance(Ldbs5000.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'LDBS5000'              TO   IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("LDBS5000");
        // COB_CODE: MOVE 'DETT-QUEST' TO   IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("DETT-QUEST");
        // COB_CODE: MOVE '00'                      TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                    TO   IDSV0003-SQLCODE
        //                                               IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                    TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                               IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-WC-EFF<br>*/
    private void a200ElaboraWcEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-WC-EFF          THRU A210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-WC-EFF          THRU A210-EX
            a210SelectWcEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
            a260OpenCursorWcEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
            a270CloseCursorWcEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
            a280FetchFirstWcEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
            a290FetchNextWcEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B200-ELABORA-WC-CPZ<br>*/
    private void b200ElaboraWcCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
            b210SelectWcCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
            b260OpenCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
            b270CloseCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
            b280FetchFirstWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
            b290FetchNextWcCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: C200-ELABORA-WC-NST<br>*/
    private void c200ElaboraWcNst() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM C210-SELECT-WC-NST          THRU C210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM C210-SELECT-WC-NST          THRU C210-EX
            c210SelectWcNst();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
            c260OpenCursorWcNst();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
            c270CloseCursorWcNst();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
            c280FetchFirstWcNst();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
            c290FetchNextWcNst();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A205-DECLARE-CURSOR-WC-EFF<br>
	 * <pre>----
	 * ----  gestione WC Effetto
	 * ----</pre>*/
    private void a205DeclareCursorWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //              DECLARE C-EFF CURSOR FOR
        //              SELECT
        //                     A.ID_DETT_QUEST
        //                    ,A.ID_QUEST
        //                    ,A.ID_MOVI_CRZ
        //                    ,A.ID_MOVI_CHIU
        //                    ,A.DT_INI_EFF
        //                    ,A.DT_END_EFF
        //                    ,A.COD_COMP_ANIA
        //                    ,A.COD_QUEST
        //                    ,A.COD_DOM
        //                    ,A.COD_DOM_COLLG
        //                    ,A.RISP_NUM
        //                    ,A.RISP_FL
        //                    ,A.RISP_TXT
        //                    ,A.RISP_TS
        //                    ,A.RISP_IMP
        //                    ,A.RISP_PC
        //                    ,A.RISP_DT
        //                    ,A.RISP_KEY
        //                    ,A.TP_RISP
        //                    ,A.ID_COMP_QUEST
        //                    ,A.VAL_RISP
        //                    ,A.DS_RIGA
        //                    ,A.DS_OPER_SQL
        //                    ,A.DS_VER
        //                    ,A.DS_TS_INI_CPTZ
        //                    ,A.DS_TS_END_CPTZ
        //                    ,A.DS_UTENTE
        //                    ,A.DS_STATO_ELAB
        //              FROM DETT_QUEST A,
        //                   QUEST B
        //              WHERE  A.ID_QUEST = B.ID_QUEST
        //                    AND B.ID_RAPP_ANA = :LDBV5001-ID-RAPP-ANA
        //                    AND A.ID_COMP_QUEST = :LDBV5001-ID-COMP-QUEST
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.ID_MOVI_CHIU IS NULL
        //                    AND B.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A210-SELECT-WC-EFF<br>*/
    private void a210SelectWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                     A.ID_DETT_QUEST
        //                    ,A.ID_QUEST
        //                    ,A.ID_MOVI_CRZ
        //                    ,A.ID_MOVI_CHIU
        //                    ,A.DT_INI_EFF
        //                    ,A.DT_END_EFF
        //                    ,A.COD_COMP_ANIA
        //                    ,A.COD_QUEST
        //                    ,A.COD_DOM
        //                    ,A.COD_DOM_COLLG
        //                    ,A.RISP_NUM
        //                    ,A.RISP_FL
        //                    ,A.RISP_TXT
        //                    ,A.RISP_TS
        //                    ,A.RISP_IMP
        //                    ,A.RISP_PC
        //                    ,A.RISP_DT
        //                    ,A.RISP_KEY
        //                    ,A.TP_RISP
        //                    ,A.ID_COMP_QUEST
        //                    ,A.VAL_RISP
        //                    ,A.DS_RIGA
        //                    ,A.DS_OPER_SQL
        //                    ,A.DS_VER
        //                    ,A.DS_TS_INI_CPTZ
        //                    ,A.DS_TS_END_CPTZ
        //                    ,A.DS_UTENTE
        //                    ,A.DS_STATO_ELAB
        //             INTO
        //                :DEQ-ID-DETT-QUEST
        //               ,:DEQ-ID-QUEST
        //               ,:DEQ-ID-MOVI-CRZ
        //               ,:DEQ-ID-MOVI-CHIU
        //                :IND-DEQ-ID-MOVI-CHIU
        //               ,:DEQ-DT-INI-EFF-DB
        //               ,:DEQ-DT-END-EFF-DB
        //               ,:DEQ-COD-COMP-ANIA
        //               ,:DEQ-COD-QUEST
        //                :IND-DEQ-COD-QUEST
        //               ,:DEQ-COD-DOM
        //                :IND-DEQ-COD-DOM
        //               ,:DEQ-COD-DOM-COLLG
        //                :IND-DEQ-COD-DOM-COLLG
        //               ,:DEQ-RISP-NUM
        //                :IND-DEQ-RISP-NUM
        //               ,:DEQ-RISP-FL
        //                :IND-DEQ-RISP-FL
        //               ,:DEQ-RISP-TXT-VCHAR
        //                :IND-DEQ-RISP-TXT
        //               ,:DEQ-RISP-TS
        //                :IND-DEQ-RISP-TS
        //               ,:DEQ-RISP-IMP
        //                :IND-DEQ-RISP-IMP
        //               ,:DEQ-RISP-PC
        //                :IND-DEQ-RISP-PC
        //               ,:DEQ-RISP-DT-DB
        //                :IND-DEQ-RISP-DT
        //               ,:DEQ-RISP-KEY
        //                :IND-DEQ-RISP-KEY
        //               ,:DEQ-TP-RISP
        //                :IND-DEQ-TP-RISP
        //               ,:DEQ-ID-COMP-QUEST
        //                :IND-DEQ-ID-COMP-QUEST
        //               ,:DEQ-VAL-RISP-VCHAR
        //                :IND-DEQ-VAL-RISP
        //               ,:DEQ-DS-RIGA
        //               ,:DEQ-DS-OPER-SQL
        //               ,:DEQ-DS-VER
        //               ,:DEQ-DS-TS-INI-CPTZ
        //               ,:DEQ-DS-TS-END-CPTZ
        //               ,:DEQ-DS-UTENTE
        //               ,:DEQ-DS-STATO-ELAB
        //             FROM DETT_QUEST A,
        //                  QUEST B
        //             WHERE  A.ID_QUEST = B.ID_QUEST
        //                    AND B.ID_RAPP_ANA = :LDBV5001-ID-RAPP-ANA
        //                    AND A.ID_COMP_QUEST = :LDBV5001-ID-COMP-QUEST
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.ID_MOVI_CHIU IS NULL
        //                    AND B.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        dettQuest1Dao.selectRec(ws.getLdbv5001().getLdbv5001IdRappAna(), ws.getLdbv5001().getLdbv5001IdCompQuest(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: A260-OPEN-CURSOR-WC-EFF<br>*/
    private void a260OpenCursorWcEff() {
        // COB_CODE: PERFORM A205-DECLARE-CURSOR-WC-EFF THRU A205-EX.
        a205DeclareCursorWcEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-EFF
        //           END-EXEC.
        dettQuest1Dao.openCEff25(ws.getLdbv5001().getLdbv5001IdRappAna(), ws.getLdbv5001().getLdbv5001IdCompQuest(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A270-CLOSE-CURSOR-WC-EFF<br>*/
    private void a270CloseCursorWcEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-EFF
        //           END-EXEC.
        dettQuest1Dao.closeCEff25();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A280-FETCH-FIRST-WC-EFF<br>*/
    private void a280FetchFirstWcEff() {
        // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF    THRU A260-EX.
        a260OpenCursorWcEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
            a290FetchNextWcEff();
        }
    }

    /**Original name: A290-FETCH-NEXT-WC-EFF<br>*/
    private void a290FetchNextWcEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-EFF
        //           INTO
        //                :DEQ-ID-DETT-QUEST
        //               ,:DEQ-ID-QUEST
        //               ,:DEQ-ID-MOVI-CRZ
        //               ,:DEQ-ID-MOVI-CHIU
        //                :IND-DEQ-ID-MOVI-CHIU
        //               ,:DEQ-DT-INI-EFF-DB
        //               ,:DEQ-DT-END-EFF-DB
        //               ,:DEQ-COD-COMP-ANIA
        //               ,:DEQ-COD-QUEST
        //                :IND-DEQ-COD-QUEST
        //               ,:DEQ-COD-DOM
        //                :IND-DEQ-COD-DOM
        //               ,:DEQ-COD-DOM-COLLG
        //                :IND-DEQ-COD-DOM-COLLG
        //               ,:DEQ-RISP-NUM
        //                :IND-DEQ-RISP-NUM
        //               ,:DEQ-RISP-FL
        //                :IND-DEQ-RISP-FL
        //               ,:DEQ-RISP-TXT-VCHAR
        //                :IND-DEQ-RISP-TXT
        //               ,:DEQ-RISP-TS
        //                :IND-DEQ-RISP-TS
        //               ,:DEQ-RISP-IMP
        //                :IND-DEQ-RISP-IMP
        //               ,:DEQ-RISP-PC
        //                :IND-DEQ-RISP-PC
        //               ,:DEQ-RISP-DT-DB
        //                :IND-DEQ-RISP-DT
        //               ,:DEQ-RISP-KEY
        //                :IND-DEQ-RISP-KEY
        //               ,:DEQ-TP-RISP
        //                :IND-DEQ-TP-RISP
        //               ,:DEQ-ID-COMP-QUEST
        //                :IND-DEQ-ID-COMP-QUEST
        //               ,:DEQ-VAL-RISP-VCHAR
        //                :IND-DEQ-VAL-RISP
        //               ,:DEQ-DS-RIGA
        //               ,:DEQ-DS-OPER-SQL
        //               ,:DEQ-DS-VER
        //               ,:DEQ-DS-TS-INI-CPTZ
        //               ,:DEQ-DS-TS-END-CPTZ
        //               ,:DEQ-DS-UTENTE
        //               ,:DEQ-DS-STATO-ELAB
        //           END-EXEC.
        dettQuest1Dao.fetchCEff25(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF THRU A270-EX
            a270CloseCursorWcEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B205-DECLARE-CURSOR-WC-CPZ<br>
	 * <pre>----
	 * ----  gestione WC Competenza
	 * ----</pre>*/
    private void b205DeclareCursorWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //              DECLARE C-CPZ CURSOR FOR
        //              SELECT
        //                     A.ID_DETT_QUEST
        //                    ,A.ID_QUEST
        //                    ,A.ID_MOVI_CRZ
        //                    ,A.ID_MOVI_CHIU
        //                    ,A.DT_INI_EFF
        //                    ,A.DT_END_EFF
        //                    ,A.COD_COMP_ANIA
        //                    ,A.COD_QUEST
        //                    ,A.COD_DOM
        //                    ,A.COD_DOM_COLLG
        //                    ,A.RISP_NUM
        //                    ,A.RISP_FL
        //                    ,A.RISP_TXT
        //                    ,A.RISP_TS
        //                    ,A.RISP_IMP
        //                    ,A.RISP_PC
        //                    ,A.RISP_DT
        //                    ,A.RISP_KEY
        //                    ,A.TP_RISP
        //                    ,A.ID_COMP_QUEST
        //                    ,A.VAL_RISP
        //                    ,A.DS_RIGA
        //                    ,A.DS_OPER_SQL
        //                    ,A.DS_VER
        //                    ,A.DS_TS_INI_CPTZ
        //                    ,A.DS_TS_END_CPTZ
        //                    ,A.DS_UTENTE
        //                    ,A.DS_STATO_ELAB
        //              FROM DETT_QUEST A,
        //                   QUEST B
        //              WHERE  A.ID_QUEST = B.ID_QUEST
        //                        AND B.ID_RAPP_ANA = :LDBV5001-ID-RAPP-ANA
        //                        AND A.ID_COMP_QUEST = :LDBV5001-ID-COMP-QUEST
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND A.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //                    AND B.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND B.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B210-SELECT-WC-CPZ<br>*/
    private void b210SelectWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                     A.ID_DETT_QUEST
        //                    ,A.ID_QUEST
        //                    ,A.ID_MOVI_CRZ
        //                    ,A.ID_MOVI_CHIU
        //                    ,A.DT_INI_EFF
        //                    ,A.DT_END_EFF
        //                    ,A.COD_COMP_ANIA
        //                    ,A.COD_QUEST
        //                    ,A.COD_DOM
        //                    ,A.COD_DOM_COLLG
        //                    ,A.RISP_NUM
        //                    ,A.RISP_FL
        //                    ,A.RISP_TXT
        //                    ,A.RISP_TS
        //                    ,A.RISP_IMP
        //                    ,A.RISP_PC
        //                    ,A.RISP_DT
        //                    ,A.RISP_KEY
        //                    ,A.TP_RISP
        //                    ,A.ID_COMP_QUEST
        //                    ,A.VAL_RISP
        //                    ,A.DS_RIGA
        //                    ,A.DS_OPER_SQL
        //                    ,A.DS_VER
        //                    ,A.DS_TS_INI_CPTZ
        //                    ,A.DS_TS_END_CPTZ
        //                    ,A.DS_UTENTE
        //                    ,A.DS_STATO_ELAB
        //             INTO
        //                :DEQ-ID-DETT-QUEST
        //               ,:DEQ-ID-QUEST
        //               ,:DEQ-ID-MOVI-CRZ
        //               ,:DEQ-ID-MOVI-CHIU
        //                :IND-DEQ-ID-MOVI-CHIU
        //               ,:DEQ-DT-INI-EFF-DB
        //               ,:DEQ-DT-END-EFF-DB
        //               ,:DEQ-COD-COMP-ANIA
        //               ,:DEQ-COD-QUEST
        //                :IND-DEQ-COD-QUEST
        //               ,:DEQ-COD-DOM
        //                :IND-DEQ-COD-DOM
        //               ,:DEQ-COD-DOM-COLLG
        //                :IND-DEQ-COD-DOM-COLLG
        //               ,:DEQ-RISP-NUM
        //                :IND-DEQ-RISP-NUM
        //               ,:DEQ-RISP-FL
        //                :IND-DEQ-RISP-FL
        //               ,:DEQ-RISP-TXT-VCHAR
        //                :IND-DEQ-RISP-TXT
        //               ,:DEQ-RISP-TS
        //                :IND-DEQ-RISP-TS
        //               ,:DEQ-RISP-IMP
        //                :IND-DEQ-RISP-IMP
        //               ,:DEQ-RISP-PC
        //                :IND-DEQ-RISP-PC
        //               ,:DEQ-RISP-DT-DB
        //                :IND-DEQ-RISP-DT
        //               ,:DEQ-RISP-KEY
        //                :IND-DEQ-RISP-KEY
        //               ,:DEQ-TP-RISP
        //                :IND-DEQ-TP-RISP
        //               ,:DEQ-ID-COMP-QUEST
        //                :IND-DEQ-ID-COMP-QUEST
        //               ,:DEQ-VAL-RISP-VCHAR
        //                :IND-DEQ-VAL-RISP
        //               ,:DEQ-DS-RIGA
        //               ,:DEQ-DS-OPER-SQL
        //               ,:DEQ-DS-VER
        //               ,:DEQ-DS-TS-INI-CPTZ
        //               ,:DEQ-DS-TS-END-CPTZ
        //               ,:DEQ-DS-UTENTE
        //               ,:DEQ-DS-STATO-ELAB
        //             FROM DETT_QUEST A,
        //                  QUEST B
        //             WHERE  A.ID_QUEST = B.ID_QUEST
        //                    AND B.ID_RAPP_ANA = :LDBV5001-ID-RAPP-ANA
        //                    AND A.ID_COMP_QUEST = :LDBV5001-ID-COMP-QUEST
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND A.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //                    AND B.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND B.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        dettQuest1Dao.selectRec1(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: B260-OPEN-CURSOR-WC-CPZ<br>*/
    private void b260OpenCursorWcCpz() {
        // COB_CODE: PERFORM B205-DECLARE-CURSOR-WC-CPZ THRU B205-EX.
        b205DeclareCursorWcCpz();
        // COB_CODE: EXEC SQL
        //                OPEN C-CPZ
        //           END-EXEC.
        dettQuest1Dao.openCCpz25(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B270-CLOSE-CURSOR-WC-CPZ<br>*/
    private void b270CloseCursorWcCpz() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-CPZ
        //           END-EXEC.
        dettQuest1Dao.closeCCpz25();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B280-FETCH-FIRST-WC-CPZ<br>*/
    private void b280FetchFirstWcCpz() {
        // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ    THRU B260-EX.
        b260OpenCursorWcCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
            b290FetchNextWcCpz();
        }
    }

    /**Original name: B290-FETCH-NEXT-WC-CPZ<br>*/
    private void b290FetchNextWcCpz() {
        // COB_CODE: EXEC SQL
        //                FETCH C-CPZ
        //           INTO
        //                :DEQ-ID-DETT-QUEST
        //               ,:DEQ-ID-QUEST
        //               ,:DEQ-ID-MOVI-CRZ
        //               ,:DEQ-ID-MOVI-CHIU
        //                :IND-DEQ-ID-MOVI-CHIU
        //               ,:DEQ-DT-INI-EFF-DB
        //               ,:DEQ-DT-END-EFF-DB
        //               ,:DEQ-COD-COMP-ANIA
        //               ,:DEQ-COD-QUEST
        //                :IND-DEQ-COD-QUEST
        //               ,:DEQ-COD-DOM
        //                :IND-DEQ-COD-DOM
        //               ,:DEQ-COD-DOM-COLLG
        //                :IND-DEQ-COD-DOM-COLLG
        //               ,:DEQ-RISP-NUM
        //                :IND-DEQ-RISP-NUM
        //               ,:DEQ-RISP-FL
        //                :IND-DEQ-RISP-FL
        //               ,:DEQ-RISP-TXT-VCHAR
        //                :IND-DEQ-RISP-TXT
        //               ,:DEQ-RISP-TS
        //                :IND-DEQ-RISP-TS
        //               ,:DEQ-RISP-IMP
        //                :IND-DEQ-RISP-IMP
        //               ,:DEQ-RISP-PC
        //                :IND-DEQ-RISP-PC
        //               ,:DEQ-RISP-DT-DB
        //                :IND-DEQ-RISP-DT
        //               ,:DEQ-RISP-KEY
        //                :IND-DEQ-RISP-KEY
        //               ,:DEQ-TP-RISP
        //                :IND-DEQ-TP-RISP
        //               ,:DEQ-ID-COMP-QUEST
        //                :IND-DEQ-ID-COMP-QUEST
        //               ,:DEQ-VAL-RISP-VCHAR
        //                :IND-DEQ-VAL-RISP
        //               ,:DEQ-DS-RIGA
        //               ,:DEQ-DS-OPER-SQL
        //               ,:DEQ-DS-VER
        //               ,:DEQ-DS-TS-INI-CPTZ
        //               ,:DEQ-DS-TS-END-CPTZ
        //               ,:DEQ-DS-UTENTE
        //               ,:DEQ-DS-STATO-ELAB
        //           END-EXEC.
        dettQuest1Dao.fetchCCpz25(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ THRU B270-EX
            b270CloseCursorWcCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: C205-DECLARE-CURSOR-WC-NST<br>
	 * <pre>----
	 * ----  gestione WC Senza Storicità
	 * ----</pre>*/
    private void c205DeclareCursorWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C210-SELECT-WC-NST<br>*/
    private void c210SelectWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C260-OPEN-CURSOR-WC-NST<br>*/
    private void c260OpenCursorWcNst() {
        // COB_CODE: PERFORM C205-DECLARE-CURSOR-WC-NST THRU C205-EX.
        c205DeclareCursorWcNst();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C270-CLOSE-CURSOR-WC-NST<br>*/
    private void c270CloseCursorWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C280-FETCH-FIRST-WC-NST<br>*/
    private void c280FetchFirstWcNst() {
        // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST    THRU C260-EX.
        c260OpenCursorWcNst();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
            c290FetchNextWcNst();
        }
    }

    /**Original name: C290-FETCH-NEXT-WC-NST<br>*/
    private void c290FetchNextWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>----
	 * ----  utilità comuni a tutti i livelli operazione
	 * ----</pre>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-DEQ-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO DEQ-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndDettQuest().getIdOgg() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DEQ-ID-MOVI-CHIU-NULL
            dettQuest.getDeqIdMoviChiu().setDeqIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DeqIdMoviChiu.Len.DEQ_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-DEQ-COD-QUEST = -1
        //              MOVE HIGH-VALUES TO DEQ-COD-QUEST-NULL
        //           END-IF
        if (ws.getIndDettQuest().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DEQ-COD-QUEST-NULL
            dettQuest.setDeqCodQuest(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DettQuestIdbsdeq0.Len.DEQ_COD_QUEST));
        }
        // COB_CODE: IF IND-DEQ-COD-DOM = -1
        //              MOVE HIGH-VALUES TO DEQ-COD-DOM-NULL
        //           END-IF
        if (ws.getIndDettQuest().getDtIniPer() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DEQ-COD-DOM-NULL
            dettQuest.setDeqCodDom(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DettQuestIdbsdeq0.Len.DEQ_COD_DOM));
        }
        // COB_CODE: IF IND-DEQ-COD-DOM-COLLG = -1
        //              MOVE HIGH-VALUES TO DEQ-COD-DOM-COLLG-NULL
        //           END-IF
        if (ws.getIndDettQuest().getDtEndPer() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DEQ-COD-DOM-COLLG-NULL
            dettQuest.setDeqCodDomCollg(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DettQuestIdbsdeq0.Len.DEQ_COD_DOM_COLLG));
        }
        // COB_CODE: IF IND-DEQ-RISP-NUM = -1
        //              MOVE HIGH-VALUES TO DEQ-RISP-NUM-NULL
        //           END-IF
        if (ws.getIndDettQuest().getImpstSost() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DEQ-RISP-NUM-NULL
            dettQuest.getDeqRispNum().setDeqRispNumNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DeqRispNum.Len.DEQ_RISP_NUM_NULL));
        }
        // COB_CODE: IF IND-DEQ-RISP-FL = -1
        //              MOVE HIGH-VALUES TO DEQ-RISP-FL-NULL
        //           END-IF
        if (ws.getIndDettQuest().getImpbIs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DEQ-RISP-FL-NULL
            dettQuest.setDeqRispFl(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-DEQ-RISP-TXT = -1
        //              MOVE HIGH-VALUES TO DEQ-RISP-TXT
        //           END-IF
        if (ws.getIndDettQuest().getAlqIs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DEQ-RISP-TXT
            dettQuest.setDeqRispTxt(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DettQuestIdbsdeq0.Len.DEQ_RISP_TXT));
        }
        // COB_CODE: IF IND-DEQ-RISP-TS = -1
        //              MOVE HIGH-VALUES TO DEQ-RISP-TS-NULL
        //           END-IF
        if (ws.getIndDettQuest().getCodTrb() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DEQ-RISP-TS-NULL
            dettQuest.getDeqRispTs().setDeqRispTsNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DeqRispTs.Len.DEQ_RISP_TS_NULL));
        }
        // COB_CODE: IF IND-DEQ-RISP-IMP = -1
        //              MOVE HIGH-VALUES TO DEQ-RISP-IMP-NULL
        //           END-IF
        if (ws.getIndDettQuest().getPrstzLrdAnteIs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DEQ-RISP-IMP-NULL
            dettQuest.getDeqRispImp().setDeqRispImpNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DeqRispImp.Len.DEQ_RISP_IMP_NULL));
        }
        // COB_CODE: IF IND-DEQ-RISP-PC = -1
        //              MOVE HIGH-VALUES TO DEQ-RISP-PC-NULL
        //           END-IF
        if (ws.getIndDettQuest().getRisMatNetPrec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DEQ-RISP-PC-NULL
            dettQuest.getDeqRispPc().setDeqRispPcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DeqRispPc.Len.DEQ_RISP_PC_NULL));
        }
        // COB_CODE: IF IND-DEQ-RISP-DT = -1
        //              MOVE HIGH-VALUES TO DEQ-RISP-DT-NULL
        //           END-IF
        if (ws.getIndDettQuest().getRisMatAnteTax() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DEQ-RISP-DT-NULL
            dettQuest.getDeqRispDt().setDeqRispDtNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DeqRispDt.Len.DEQ_RISP_DT_NULL));
        }
        // COB_CODE: IF IND-DEQ-RISP-KEY = -1
        //              MOVE HIGH-VALUES TO DEQ-RISP-KEY-NULL
        //           END-IF
        if (ws.getIndDettQuest().getRisMatPostTax() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DEQ-RISP-KEY-NULL
            dettQuest.setDeqRispKey(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DettQuestIdbsdeq0.Len.DEQ_RISP_KEY));
        }
        // COB_CODE: IF IND-DEQ-TP-RISP = -1
        //              MOVE HIGH-VALUES TO DEQ-TP-RISP-NULL
        //           END-IF
        if (ws.getIndDettQuest().getPrstzNet() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DEQ-TP-RISP-NULL
            dettQuest.setDeqTpRisp(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DettQuestIdbsdeq0.Len.DEQ_TP_RISP));
        }
        // COB_CODE: IF IND-DEQ-ID-COMP-QUEST = -1
        //              MOVE HIGH-VALUES TO DEQ-ID-COMP-QUEST-NULL
        //           END-IF
        if (ws.getIndDettQuest().getPrstzPrec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DEQ-ID-COMP-QUEST-NULL
            dettQuest.getDeqIdCompQuest().setDeqIdCompQuestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DeqIdCompQuest.Len.DEQ_ID_COMP_QUEST_NULL));
        }
        // COB_CODE: IF IND-DEQ-VAL-RISP = -1
        //              MOVE HIGH-VALUES TO DEQ-VAL-RISP
        //           END-IF.
        if (ws.getIndDettQuest().getCumPreVers() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DEQ-VAL-RISP
            dettQuest.setDeqValRisp(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DettQuestIdbsdeq0.Len.DEQ_VAL_RISP));
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE DEQ-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getDettQuestDb().getDtIniEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO DEQ-DT-INI-EFF
        dettQuest.setDeqDtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE DEQ-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getDettQuestDb().getDtEndEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO DEQ-DT-END-EFF
        dettQuest.setDeqDtEndEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-DEQ-RISP-DT = 0
        //               MOVE WS-DATE-N      TO DEQ-RISP-DT
        //           END-IF.
        if (ws.getIndDettQuest().getRisMatAnteTax() == 0) {
            // COB_CODE: MOVE DEQ-RISP-DT-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getDettQuestDb().getRispDtDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO DEQ-RISP-DT
            dettQuest.getDeqRispDt().setDeqRispDt(ws.getIdsv0010().getWsDateN());
        }
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
        // COB_CODE: MOVE LENGTH OF DEQ-RISP-TXT
        //                       TO DEQ-RISP-TXT-LEN
        dettQuest.setDeqRispTxtLen(((short)DettQuestIdbsdeq0.Len.DEQ_RISP_TXT));
        // COB_CODE: MOVE LENGTH OF DEQ-VAL-RISP
        //                       TO DEQ-VAL-RISP-LEN.
        dettQuest.setDeqValRispLen(((short)DettQuestIdbsdeq0.Len.DEQ_VAL_RISP));
    }

    /**Original name: Z970-CODICE-ADHOC-PRE<br>
	 * <pre>----
	 * ----  prevede statements AD HOC PRE Query
	 * ----</pre>*/
    private void z970CodiceAdhocPre() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z980-CODICE-ADHOC-POST<br>
	 * <pre>----
	 * ----  prevede statements AD HOC POST Query
	 * ----</pre>*/
    private void z980CodiceAdhocPost() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public int getCodCompAnia() {
        return dettQuest.getDeqCodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.dettQuest.setDeqCodCompAnia(codCompAnia);
    }

    @Override
    public String getCodDom() {
        return dettQuest.getDeqCodDom();
    }

    @Override
    public void setCodDom(String codDom) {
        this.dettQuest.setDeqCodDom(codDom);
    }

    @Override
    public String getCodDomCollg() {
        return dettQuest.getDeqCodDomCollg();
    }

    @Override
    public void setCodDomCollg(String codDomCollg) {
        this.dettQuest.setDeqCodDomCollg(codDomCollg);
    }

    @Override
    public String getCodDomCollgObj() {
        if (ws.getIndDettQuest().getDtEndPer() >= 0) {
            return getCodDomCollg();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodDomCollgObj(String codDomCollgObj) {
        if (codDomCollgObj != null) {
            setCodDomCollg(codDomCollgObj);
            ws.getIndDettQuest().setDtEndPer(((short)0));
        }
        else {
            ws.getIndDettQuest().setDtEndPer(((short)-1));
        }
    }

    @Override
    public String getCodDomObj() {
        if (ws.getIndDettQuest().getDtIniPer() >= 0) {
            return getCodDom();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodDomObj(String codDomObj) {
        if (codDomObj != null) {
            setCodDom(codDomObj);
            ws.getIndDettQuest().setDtIniPer(((short)0));
        }
        else {
            ws.getIndDettQuest().setDtIniPer(((short)-1));
        }
    }

    @Override
    public String getCodQuest() {
        return dettQuest.getDeqCodQuest();
    }

    @Override
    public void setCodQuest(String codQuest) {
        this.dettQuest.setDeqCodQuest(codQuest);
    }

    @Override
    public String getCodQuestObj() {
        if (ws.getIndDettQuest().getIdMoviChiu() >= 0) {
            return getCodQuest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodQuestObj(String codQuestObj) {
        if (codQuestObj != null) {
            setCodQuest(codQuestObj);
            ws.getIndDettQuest().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndDettQuest().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public char getDsOperSql() {
        return dettQuest.getDeqDsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.dettQuest.setDeqDsOperSql(dsOperSql);
    }

    @Override
    public long getDsRiga() {
        return dettQuest.getDeqDsRiga();
    }

    @Override
    public void setDsRiga(long dsRiga) {
        this.dettQuest.setDeqDsRiga(dsRiga);
    }

    @Override
    public char getDsStatoElab() {
        return dettQuest.getDeqDsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.dettQuest.setDeqDsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsEndCptz() {
        return dettQuest.getDeqDsTsEndCptz();
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.dettQuest.setDeqDsTsEndCptz(dsTsEndCptz);
    }

    @Override
    public long getDsTsIniCptz() {
        return dettQuest.getDeqDsTsIniCptz();
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.dettQuest.setDeqDsTsIniCptz(dsTsIniCptz);
    }

    @Override
    public String getDsUtente() {
        return dettQuest.getDeqDsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.dettQuest.setDeqDsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return dettQuest.getDeqDsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.dettQuest.setDeqDsVer(dsVer);
    }

    @Override
    public String getDtEndEffDb() {
        return ws.getDettQuestDb().getDtEndEffDb();
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        this.ws.getDettQuestDb().setDtEndEffDb(dtEndEffDb);
    }

    @Override
    public String getDtIniEffDb() {
        return ws.getDettQuestDb().getDtIniEffDb();
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        this.ws.getDettQuestDb().setDtIniEffDb(dtIniEffDb);
    }

    @Override
    public int getIdCompQuest() {
        return dettQuest.getDeqIdCompQuest().getDeqIdCompQuest();
    }

    @Override
    public void setIdCompQuest(int idCompQuest) {
        this.dettQuest.getDeqIdCompQuest().setDeqIdCompQuest(idCompQuest);
    }

    @Override
    public Integer getIdCompQuestObj() {
        if (ws.getIndDettQuest().getPrstzPrec() >= 0) {
            return ((Integer)getIdCompQuest());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdCompQuestObj(Integer idCompQuestObj) {
        if (idCompQuestObj != null) {
            setIdCompQuest(((int)idCompQuestObj));
            ws.getIndDettQuest().setPrstzPrec(((short)0));
        }
        else {
            ws.getIndDettQuest().setPrstzPrec(((short)-1));
        }
    }

    @Override
    public int getIdDettQuest() {
        return dettQuest.getDeqIdDettQuest();
    }

    @Override
    public void setIdDettQuest(int idDettQuest) {
        this.dettQuest.setDeqIdDettQuest(idDettQuest);
    }

    @Override
    public int getIdMoviChiu() {
        return dettQuest.getDeqIdMoviChiu().getDeqIdMoviChiu();
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        this.dettQuest.getDeqIdMoviChiu().setDeqIdMoviChiu(idMoviChiu);
    }

    @Override
    public Integer getIdMoviChiuObj() {
        if (ws.getIndDettQuest().getIdOgg() >= 0) {
            return ((Integer)getIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        if (idMoviChiuObj != null) {
            setIdMoviChiu(((int)idMoviChiuObj));
            ws.getIndDettQuest().setIdOgg(((short)0));
        }
        else {
            ws.getIndDettQuest().setIdOgg(((short)-1));
        }
    }

    @Override
    public int getIdMoviCrz() {
        return dettQuest.getDeqIdMoviCrz();
    }

    @Override
    public void setIdMoviCrz(int idMoviCrz) {
        this.dettQuest.setDeqIdMoviCrz(idMoviCrz);
    }

    @Override
    public int getIdQuest() {
        return dettQuest.getDeqIdQuest();
    }

    @Override
    public void setIdQuest(int idQuest) {
        this.dettQuest.setDeqIdQuest(idQuest);
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        return idsv0003.getCodiceCompagniaAnia();
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        this.idsv0003.setCodiceCompagniaAnia(idsv0003CodiceCompagniaAnia);
    }

    @Override
    public int getLdbv5001IdCompQuest() {
        return ws.getLdbv5001().getLdbv5001IdCompQuest();
    }

    @Override
    public void setLdbv5001IdCompQuest(int ldbv5001IdCompQuest) {
        this.ws.getLdbv5001().setLdbv5001IdCompQuest(ldbv5001IdCompQuest);
    }

    @Override
    public int getLdbv5001IdRappAna() {
        return ws.getLdbv5001().getLdbv5001IdRappAna();
    }

    @Override
    public void setLdbv5001IdRappAna(int ldbv5001IdRappAna) {
        this.ws.getLdbv5001().setLdbv5001IdRappAna(ldbv5001IdRappAna);
    }

    @Override
    public String getRispDtDb() {
        return ws.getDettQuestDb().getRispDtDb();
    }

    @Override
    public void setRispDtDb(String rispDtDb) {
        this.ws.getDettQuestDb().setRispDtDb(rispDtDb);
    }

    @Override
    public String getRispDtDbObj() {
        if (ws.getIndDettQuest().getRisMatAnteTax() >= 0) {
            return getRispDtDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRispDtDbObj(String rispDtDbObj) {
        if (rispDtDbObj != null) {
            setRispDtDb(rispDtDbObj);
            ws.getIndDettQuest().setRisMatAnteTax(((short)0));
        }
        else {
            ws.getIndDettQuest().setRisMatAnteTax(((short)-1));
        }
    }

    @Override
    public char getRispFl() {
        return dettQuest.getDeqRispFl();
    }

    @Override
    public void setRispFl(char rispFl) {
        this.dettQuest.setDeqRispFl(rispFl);
    }

    @Override
    public Character getRispFlObj() {
        if (ws.getIndDettQuest().getImpbIs() >= 0) {
            return ((Character)getRispFl());
        }
        else {
            return null;
        }
    }

    @Override
    public void setRispFlObj(Character rispFlObj) {
        if (rispFlObj != null) {
            setRispFl(((char)rispFlObj));
            ws.getIndDettQuest().setImpbIs(((short)0));
        }
        else {
            ws.getIndDettQuest().setImpbIs(((short)-1));
        }
    }

    @Override
    public AfDecimal getRispImp() {
        return dettQuest.getDeqRispImp().getDeqRispImp();
    }

    @Override
    public void setRispImp(AfDecimal rispImp) {
        this.dettQuest.getDeqRispImp().setDeqRispImp(rispImp.copy());
    }

    @Override
    public AfDecimal getRispImpObj() {
        if (ws.getIndDettQuest().getPrstzLrdAnteIs() >= 0) {
            return getRispImp();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRispImpObj(AfDecimal rispImpObj) {
        if (rispImpObj != null) {
            setRispImp(new AfDecimal(rispImpObj, 15, 3));
            ws.getIndDettQuest().setPrstzLrdAnteIs(((short)0));
        }
        else {
            ws.getIndDettQuest().setPrstzLrdAnteIs(((short)-1));
        }
    }

    @Override
    public String getRispKey() {
        return dettQuest.getDeqRispKey();
    }

    @Override
    public void setRispKey(String rispKey) {
        this.dettQuest.setDeqRispKey(rispKey);
    }

    @Override
    public String getRispKeyObj() {
        if (ws.getIndDettQuest().getRisMatPostTax() >= 0) {
            return getRispKey();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRispKeyObj(String rispKeyObj) {
        if (rispKeyObj != null) {
            setRispKey(rispKeyObj);
            ws.getIndDettQuest().setRisMatPostTax(((short)0));
        }
        else {
            ws.getIndDettQuest().setRisMatPostTax(((short)-1));
        }
    }

    @Override
    public int getRispNum() {
        return dettQuest.getDeqRispNum().getDeqRispNum();
    }

    @Override
    public void setRispNum(int rispNum) {
        this.dettQuest.getDeqRispNum().setDeqRispNum(rispNum);
    }

    @Override
    public Integer getRispNumObj() {
        if (ws.getIndDettQuest().getImpstSost() >= 0) {
            return ((Integer)getRispNum());
        }
        else {
            return null;
        }
    }

    @Override
    public void setRispNumObj(Integer rispNumObj) {
        if (rispNumObj != null) {
            setRispNum(((int)rispNumObj));
            ws.getIndDettQuest().setImpstSost(((short)0));
        }
        else {
            ws.getIndDettQuest().setImpstSost(((short)-1));
        }
    }

    @Override
    public AfDecimal getRispPc() {
        return dettQuest.getDeqRispPc().getDeqRispPc();
    }

    @Override
    public void setRispPc(AfDecimal rispPc) {
        this.dettQuest.getDeqRispPc().setDeqRispPc(rispPc.copy());
    }

    @Override
    public AfDecimal getRispPcObj() {
        if (ws.getIndDettQuest().getRisMatNetPrec() >= 0) {
            return getRispPc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRispPcObj(AfDecimal rispPcObj) {
        if (rispPcObj != null) {
            setRispPc(new AfDecimal(rispPcObj, 6, 3));
            ws.getIndDettQuest().setRisMatNetPrec(((short)0));
        }
        else {
            ws.getIndDettQuest().setRisMatNetPrec(((short)-1));
        }
    }

    @Override
    public AfDecimal getRispTs() {
        return dettQuest.getDeqRispTs().getDeqRispTs();
    }

    @Override
    public void setRispTs(AfDecimal rispTs) {
        this.dettQuest.getDeqRispTs().setDeqRispTs(rispTs.copy());
    }

    @Override
    public AfDecimal getRispTsObj() {
        if (ws.getIndDettQuest().getCodTrb() >= 0) {
            return getRispTs();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRispTsObj(AfDecimal rispTsObj) {
        if (rispTsObj != null) {
            setRispTs(new AfDecimal(rispTsObj, 14, 9));
            ws.getIndDettQuest().setCodTrb(((short)0));
        }
        else {
            ws.getIndDettQuest().setCodTrb(((short)-1));
        }
    }

    @Override
    public String getRispTxtVchar() {
        return dettQuest.getDeqRispTxtVcharFormatted();
    }

    @Override
    public void setRispTxtVchar(String rispTxtVchar) {
        this.dettQuest.setDeqRispTxtVcharFormatted(rispTxtVchar);
    }

    @Override
    public String getRispTxtVcharObj() {
        if (ws.getIndDettQuest().getAlqIs() >= 0) {
            return getRispTxtVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRispTxtVcharObj(String rispTxtVcharObj) {
        if (rispTxtVcharObj != null) {
            setRispTxtVchar(rispTxtVcharObj);
            ws.getIndDettQuest().setAlqIs(((short)0));
        }
        else {
            ws.getIndDettQuest().setAlqIs(((short)-1));
        }
    }

    @Override
    public String getTpRisp() {
        return dettQuest.getDeqTpRisp();
    }

    @Override
    public void setTpRisp(String tpRisp) {
        this.dettQuest.setDeqTpRisp(tpRisp);
    }

    @Override
    public String getTpRispObj() {
        if (ws.getIndDettQuest().getPrstzNet() >= 0) {
            return getTpRisp();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpRispObj(String tpRispObj) {
        if (tpRispObj != null) {
            setTpRisp(tpRispObj);
            ws.getIndDettQuest().setPrstzNet(((short)0));
        }
        else {
            ws.getIndDettQuest().setPrstzNet(((short)-1));
        }
    }

    @Override
    public String getValRispVchar() {
        return dettQuest.getDeqValRispVcharFormatted();
    }

    @Override
    public void setValRispVchar(String valRispVchar) {
        this.dettQuest.setDeqValRispVcharFormatted(valRispVchar);
    }

    @Override
    public String getValRispVcharObj() {
        if (ws.getIndDettQuest().getCumPreVers() >= 0) {
            return getValRispVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setValRispVcharObj(String valRispVcharObj) {
        if (valRispVcharObj != null) {
            setValRispVchar(valRispVcharObj);
            ws.getIndDettQuest().setCumPreVers(((short)0));
        }
        else {
            ws.getIndDettQuest().setCumPreVers(((short)-1));
        }
    }

    @Override
    public String getWsDataInizioEffettoDb() {
        return ws.getIdsv0010().getWsDataInizioEffettoDb();
    }

    @Override
    public void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb) {
        this.ws.getIdsv0010().setWsDataInizioEffettoDb(wsDataInizioEffettoDb);
    }

    @Override
    public long getWsTsCompetenza() {
        return ws.getIdsv0010().getWsTsCompetenza();
    }

    @Override
    public void setWsTsCompetenza(long wsTsCompetenza) {
        this.ws.getIdsv0010().setWsTsCompetenza(wsTsCompetenza);
    }
}
