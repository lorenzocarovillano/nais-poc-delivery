package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.jdbc.FieldNotMappedException;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.ImpstBolloDao;
import it.accenture.jnais.commons.data.to.IImpstBollo;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idbsp580Data;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.ImpstBollo;
import it.accenture.jnais.ws.redefines.P58IdMoviChiu;
import it.accenture.jnais.ws.redefines.P58ImpstBolloDettV;
import it.accenture.jnais.ws.redefines.P58ImpstBolloTotV;

/**Original name: IDBSP580<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  09 AGO 2013.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Idbsp580 extends Program implements IImpstBollo {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private ImpstBolloDao impstBolloDao = new ImpstBolloDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Idbsp580Data ws = new Idbsp580Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: IMPST-BOLLO
    private ImpstBollo impstBollo;

    //==== METHODS ====
    /**Original name: PROGRAM_IDBSP580_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, ImpstBollo impstBollo) {
        this.idsv0003 = idsv0003;
        this.impstBollo = impstBollo;
        // COB_CODE: PERFORM A000-INIZIO                    THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO          THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS          THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO          THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                        a300ElaboraIdEff();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                        a400ElaboraIdpEff();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO          THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS          THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO          THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                        b300ElaboraIdCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                        b400ElaboraIdpCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                        b500ElaboraIboCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                        b600ElaboraIbsCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                        b700ElaboraIdoCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //              SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-PRIMARY-KEY
                //                 PERFORM A200-ELABORA-PK          THRU A200-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO         THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS         THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO         THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.PRIMARY_KEY:// COB_CODE: PERFORM A200-ELABORA-PK          THRU A200-EX
                        a200ElaboraPk();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO         THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS         THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO         THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Idbsp580 getInstance() {
        return ((Idbsp580)Programs.getInstance(Idbsp580.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'IDBSP580'   TO IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("IDBSP580");
        // COB_CODE: MOVE 'IMPST_BOLLO' TO IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("IMPST_BOLLO");
        // COB_CODE: MOVE '00'                     TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                   TO   IDSV0003-SQLCODE
        //                                              IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                              IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-AGG-STORICO-SOLO-INS  OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isAggStoricoSoloIns() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-PK<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a200ElaboraPk() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-PK          THRU A210-EX
        //              WHEN IDSV0003-INSERT
        //                 PERFORM A220-INSERT-PK          THRU A220-EX
        //              WHEN IDSV0003-UPDATE
        //                 PERFORM A230-UPDATE-PK          THRU A230-EX
        //              WHEN IDSV0003-DELETE
        //                 PERFORM A240-DELETE-PK          THRU A240-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-PK          THRU A210-EX
            a210SelectPk();
        }
        else if (idsv0003.getOperazione().isInsert()) {
            // COB_CODE: PERFORM A220-INSERT-PK          THRU A220-EX
            a220InsertPk();
        }
        else if (idsv0003.getOperazione().isUpdate()) {
            // COB_CODE: PERFORM A230-UPDATE-PK          THRU A230-EX
            a230UpdatePk();
        }
        else if (idsv0003.getOperazione().isDelete()) {
            // COB_CODE: PERFORM A240-DELETE-PK          THRU A240-EX
            a240DeletePk();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A300-ELABORA-ID-EFF<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void a300ElaboraIdEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A310-SELECT-ID-EFF          THRU A310-EX
            a310SelectIdEff();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A400-ELABORA-IDP-EFF<br>*/
    private void a400ElaboraIdpEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
            a410SelectIdpEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
            a460OpenCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
            a470CloseCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
            a480FetchFirstIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
            a490FetchNextIdpEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A500-ELABORA-IBO<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a500ElaboraIbo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A510-SELECT-IBO             THRU A510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A510-SELECT-IBO             THRU A510-EX
            a510SelectIbo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
            a560OpenCursorIbo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
            a570CloseCursorIbo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
            a580FetchFirstIbo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
            a590FetchNextIbo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A600-ELABORA-IBS<br>*/
    private void a600ElaboraIbs() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A610-SELECT-IBS             THRU A610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A610-SELECT-IBS             THRU A610-EX
            a610SelectIbs();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
            a660OpenCursorIbs();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
            a670CloseCursorIbs();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
            a680FetchFirstIbs();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
            a690FetchNextIbs();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A700-ELABORA-IDO<br>*/
    private void a700ElaboraIdo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A710-SELECT-IDO                 THRU A710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A710-SELECT-IDO                 THRU A710-EX
            a710SelectIdo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
            a760OpenCursorIdo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
            a770CloseCursorIdo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
            a780FetchFirstIdo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
            a790FetchNextIdo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B300-ELABORA-ID-CPZ<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void b300ElaboraIdCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
            b310SelectIdCpz();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B400-ELABORA-IDP-CPZ<br>*/
    private void b400ElaboraIdpCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
            b410SelectIdpCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
            b460OpenCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
            b470CloseCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
            b480FetchFirstIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
            b490FetchNextIdpCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B500-ELABORA-IBO-CPZ<br>*/
    private void b500ElaboraIboCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
            b510SelectIboCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
            b560OpenCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
            b570CloseCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
            b580FetchFirstIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B600-ELABORA-IBS-CPZ<br>*/
    private void b600ElaboraIbsCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
            b610SelectIbsCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
            b660OpenCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
            b670CloseCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
            b680FetchFirstIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B700-ELABORA-IDO-CPZ<br>*/
    private void b700ElaboraIdoCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
            b710SelectIdoCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
            b760OpenCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
            b770CloseCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
            b780FetchFirstIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A210-SELECT-PK<br>
	 * <pre>----
	 * ----  gestione PK
	 * ----</pre>*/
    private void a210SelectPk() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_IMPST_BOLLO
        //                ,COD_COMP_ANIA
        //                ,ID_POLI
        //                ,IB_POLI
        //                ,COD_FISC
        //                ,COD_PART_IVA
        //                ,ID_RAPP_ANA
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,DT_INI_CALC
        //                ,DT_END_CALC
        //                ,TP_CAUS_BOLLO
        //                ,IMPST_BOLLO_DETT_C
        //                ,IMPST_BOLLO_DETT_V
        //                ,IMPST_BOLLO_TOT_V
        //                ,IMPST_BOLLO_TOT_R
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //             INTO
        //                :P58-ID-IMPST-BOLLO
        //               ,:P58-COD-COMP-ANIA
        //               ,:P58-ID-POLI
        //               ,:P58-IB-POLI
        //               ,:P58-COD-FISC
        //                :IND-P58-COD-FISC
        //               ,:P58-COD-PART-IVA
        //                :IND-P58-COD-PART-IVA
        //               ,:P58-ID-RAPP-ANA
        //               ,:P58-ID-MOVI-CRZ
        //               ,:P58-ID-MOVI-CHIU
        //                :IND-P58-ID-MOVI-CHIU
        //               ,:P58-DT-INI-EFF-DB
        //               ,:P58-DT-END-EFF-DB
        //               ,:P58-DT-INI-CALC-DB
        //               ,:P58-DT-END-CALC-DB
        //               ,:P58-TP-CAUS-BOLLO
        //               ,:P58-IMPST-BOLLO-DETT-C
        //               ,:P58-IMPST-BOLLO-DETT-V
        //                :IND-P58-IMPST-BOLLO-DETT-V
        //               ,:P58-IMPST-BOLLO-TOT-V
        //                :IND-P58-IMPST-BOLLO-TOT-V
        //               ,:P58-IMPST-BOLLO-TOT-R
        //               ,:P58-DS-RIGA
        //               ,:P58-DS-OPER-SQL
        //               ,:P58-DS-VER
        //               ,:P58-DS-TS-INI-CPTZ
        //               ,:P58-DS-TS-END-CPTZ
        //               ,:P58-DS-UTENTE
        //               ,:P58-DS-STATO-ELAB
        //             FROM IMPST_BOLLO
        //             WHERE     DS_RIGA = :P58-DS-RIGA
        //           END-EXEC.
        impstBolloDao.selectByP58DsRiga(impstBollo.getP58DsRiga(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A220-INSERT-PK<br>*/
    private void a220InsertPk() {
        // COB_CODE: PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.
        z400SeqRiga();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX
            z150ValorizzaDataServicesI();
            // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX
            z200SetIndicatoriNull();
            // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX
            z900ConvertiNToX();
            // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX
            z960LengthVchar();
            // COB_CODE: EXEC SQL
            //              INSERT
            //              INTO IMPST_BOLLO
            //                  (
            //                     ID_IMPST_BOLLO
            //                    ,COD_COMP_ANIA
            //                    ,ID_POLI
            //                    ,IB_POLI
            //                    ,COD_FISC
            //                    ,COD_PART_IVA
            //                    ,ID_RAPP_ANA
            //                    ,ID_MOVI_CRZ
            //                    ,ID_MOVI_CHIU
            //                    ,DT_INI_EFF
            //                    ,DT_END_EFF
            //                    ,DT_INI_CALC
            //                    ,DT_END_CALC
            //                    ,TP_CAUS_BOLLO
            //                    ,IMPST_BOLLO_DETT_C
            //                    ,IMPST_BOLLO_DETT_V
            //                    ,IMPST_BOLLO_TOT_V
            //                    ,IMPST_BOLLO_TOT_R
            //                    ,DS_RIGA
            //                    ,DS_OPER_SQL
            //                    ,DS_VER
            //                    ,DS_TS_INI_CPTZ
            //                    ,DS_TS_END_CPTZ
            //                    ,DS_UTENTE
            //                    ,DS_STATO_ELAB
            //                  )
            //              VALUES
            //                  (
            //                    :P58-ID-IMPST-BOLLO
            //                    ,:P58-COD-COMP-ANIA
            //                    ,:P58-ID-POLI
            //                    ,:P58-IB-POLI
            //                    ,:P58-COD-FISC
            //                     :IND-P58-COD-FISC
            //                    ,:P58-COD-PART-IVA
            //                     :IND-P58-COD-PART-IVA
            //                    ,:P58-ID-RAPP-ANA
            //                    ,:P58-ID-MOVI-CRZ
            //                    ,:P58-ID-MOVI-CHIU
            //                     :IND-P58-ID-MOVI-CHIU
            //                    ,:P58-DT-INI-EFF-DB
            //                    ,:P58-DT-END-EFF-DB
            //                    ,:P58-DT-INI-CALC-DB
            //                    ,:P58-DT-END-CALC-DB
            //                    ,:P58-TP-CAUS-BOLLO
            //                    ,:P58-IMPST-BOLLO-DETT-C
            //                    ,:P58-IMPST-BOLLO-DETT-V
            //                     :IND-P58-IMPST-BOLLO-DETT-V
            //                    ,:P58-IMPST-BOLLO-TOT-V
            //                     :IND-P58-IMPST-BOLLO-TOT-V
            //                    ,:P58-IMPST-BOLLO-TOT-R
            //                    ,:P58-DS-RIGA
            //                    ,:P58-DS-OPER-SQL
            //                    ,:P58-DS-VER
            //                    ,:P58-DS-TS-INI-CPTZ
            //                    ,:P58-DS-TS-END-CPTZ
            //                    ,:P58-DS-UTENTE
            //                    ,:P58-DS-STATO-ELAB
            //                  )
            //           END-EXEC
            impstBolloDao.insertRec(this);
            // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
            a100CheckReturnCode();
        }
    }

    /**Original name: A230-UPDATE-PK<br>*/
    private void a230UpdatePk() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE IMPST_BOLLO SET
        //                   ID_IMPST_BOLLO         =
        //                :P58-ID-IMPST-BOLLO
        //                  ,COD_COMP_ANIA          =
        //                :P58-COD-COMP-ANIA
        //                  ,ID_POLI                =
        //                :P58-ID-POLI
        //                  ,IB_POLI                =
        //                :P58-IB-POLI
        //                  ,COD_FISC               =
        //                :P58-COD-FISC
        //                                       :IND-P58-COD-FISC
        //                  ,COD_PART_IVA           =
        //                :P58-COD-PART-IVA
        //                                       :IND-P58-COD-PART-IVA
        //                  ,ID_RAPP_ANA            =
        //                :P58-ID-RAPP-ANA
        //                  ,ID_MOVI_CRZ            =
        //                :P58-ID-MOVI-CRZ
        //                  ,ID_MOVI_CHIU           =
        //                :P58-ID-MOVI-CHIU
        //                                       :IND-P58-ID-MOVI-CHIU
        //                  ,DT_INI_EFF             =
        //           :P58-DT-INI-EFF-DB
        //                  ,DT_END_EFF             =
        //           :P58-DT-END-EFF-DB
        //                  ,DT_INI_CALC            =
        //           :P58-DT-INI-CALC-DB
        //                  ,DT_END_CALC            =
        //           :P58-DT-END-CALC-DB
        //                  ,TP_CAUS_BOLLO          =
        //                :P58-TP-CAUS-BOLLO
        //                  ,IMPST_BOLLO_DETT_C     =
        //                :P58-IMPST-BOLLO-DETT-C
        //                  ,IMPST_BOLLO_DETT_V     =
        //                :P58-IMPST-BOLLO-DETT-V
        //                                       :IND-P58-IMPST-BOLLO-DETT-V
        //                  ,IMPST_BOLLO_TOT_V      =
        //                :P58-IMPST-BOLLO-TOT-V
        //                                       :IND-P58-IMPST-BOLLO-TOT-V
        //                  ,IMPST_BOLLO_TOT_R      =
        //                :P58-IMPST-BOLLO-TOT-R
        //                  ,DS_RIGA                =
        //                :P58-DS-RIGA
        //                  ,DS_OPER_SQL            =
        //                :P58-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :P58-DS-VER
        //                  ,DS_TS_INI_CPTZ         =
        //                :P58-DS-TS-INI-CPTZ
        //                  ,DS_TS_END_CPTZ         =
        //                :P58-DS-TS-END-CPTZ
        //                  ,DS_UTENTE              =
        //                :P58-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :P58-DS-STATO-ELAB
        //                WHERE     DS_RIGA = :P58-DS-RIGA
        //           END-EXEC.
        impstBolloDao.updateRec(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A240-DELETE-PK<br>*/
    private void a240DeletePk() {
        // COB_CODE: EXEC SQL
        //                DELETE
        //                FROM IMPST_BOLLO
        //                WHERE     DS_RIGA = :P58-DS-RIGA
        //           END-EXEC.
        impstBolloDao.deleteByP58DsRiga(impstBollo.getP58DsRiga());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A305-DECLARE-CURSOR-ID-EFF<br>
	 * <pre>----
	 * ----  gestione ID Effetto
	 * ----</pre>*/
    private void a305DeclareCursorIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-ID-UPD-EFF-P58 CURSOR FOR
        //              SELECT
        //                     ID_IMPST_BOLLO
        //                    ,COD_COMP_ANIA
        //                    ,ID_POLI
        //                    ,IB_POLI
        //                    ,COD_FISC
        //                    ,COD_PART_IVA
        //                    ,ID_RAPP_ANA
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,DT_INI_CALC
        //                    ,DT_END_CALC
        //                    ,TP_CAUS_BOLLO
        //                    ,IMPST_BOLLO_DETT_C
        //                    ,IMPST_BOLLO_DETT_V
        //                    ,IMPST_BOLLO_TOT_V
        //                    ,IMPST_BOLLO_TOT_R
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //              FROM IMPST_BOLLO
        //              WHERE     ID_IMPST_BOLLO = :P58-ID-IMPST-BOLLO
        //                    AND DS_TS_END_CPTZ = :WS-TS-INFINITO
        //                    AND DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //              ORDER BY DT_INI_EFF ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A310-SELECT-ID-EFF<br>*/
    private void a310SelectIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_IMPST_BOLLO
        //                ,COD_COMP_ANIA
        //                ,ID_POLI
        //                ,IB_POLI
        //                ,COD_FISC
        //                ,COD_PART_IVA
        //                ,ID_RAPP_ANA
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,DT_INI_CALC
        //                ,DT_END_CALC
        //                ,TP_CAUS_BOLLO
        //                ,IMPST_BOLLO_DETT_C
        //                ,IMPST_BOLLO_DETT_V
        //                ,IMPST_BOLLO_TOT_V
        //                ,IMPST_BOLLO_TOT_R
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //             INTO
        //                :P58-ID-IMPST-BOLLO
        //               ,:P58-COD-COMP-ANIA
        //               ,:P58-ID-POLI
        //               ,:P58-IB-POLI
        //               ,:P58-COD-FISC
        //                :IND-P58-COD-FISC
        //               ,:P58-COD-PART-IVA
        //                :IND-P58-COD-PART-IVA
        //               ,:P58-ID-RAPP-ANA
        //               ,:P58-ID-MOVI-CRZ
        //               ,:P58-ID-MOVI-CHIU
        //                :IND-P58-ID-MOVI-CHIU
        //               ,:P58-DT-INI-EFF-DB
        //               ,:P58-DT-END-EFF-DB
        //               ,:P58-DT-INI-CALC-DB
        //               ,:P58-DT-END-CALC-DB
        //               ,:P58-TP-CAUS-BOLLO
        //               ,:P58-IMPST-BOLLO-DETT-C
        //               ,:P58-IMPST-BOLLO-DETT-V
        //                :IND-P58-IMPST-BOLLO-DETT-V
        //               ,:P58-IMPST-BOLLO-TOT-V
        //                :IND-P58-IMPST-BOLLO-TOT-V
        //               ,:P58-IMPST-BOLLO-TOT-R
        //               ,:P58-DS-RIGA
        //               ,:P58-DS-OPER-SQL
        //               ,:P58-DS-VER
        //               ,:P58-DS-TS-INI-CPTZ
        //               ,:P58-DS-TS-END-CPTZ
        //               ,:P58-DS-UTENTE
        //               ,:P58-DS-STATO-ELAB
        //             FROM IMPST_BOLLO
        //             WHERE     ID_IMPST_BOLLO = :P58-ID-IMPST-BOLLO
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        impstBolloDao.selectRec(impstBollo.getP58IdImpstBollo(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A330-UPDATE-ID-EFF<br>*/
    private void a330UpdateIdEff() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE IMPST_BOLLO SET
        //                   ID_IMPST_BOLLO         =
        //                :P58-ID-IMPST-BOLLO
        //                  ,COD_COMP_ANIA          =
        //                :P58-COD-COMP-ANIA
        //                  ,ID_POLI                =
        //                :P58-ID-POLI
        //                  ,IB_POLI                =
        //                :P58-IB-POLI
        //                  ,COD_FISC               =
        //                :P58-COD-FISC
        //                                       :IND-P58-COD-FISC
        //                  ,COD_PART_IVA           =
        //                :P58-COD-PART-IVA
        //                                       :IND-P58-COD-PART-IVA
        //                  ,ID_RAPP_ANA            =
        //                :P58-ID-RAPP-ANA
        //                  ,ID_MOVI_CRZ            =
        //                :P58-ID-MOVI-CRZ
        //                  ,ID_MOVI_CHIU           =
        //                :P58-ID-MOVI-CHIU
        //                                       :IND-P58-ID-MOVI-CHIU
        //                  ,DT_INI_EFF             =
        //           :P58-DT-INI-EFF-DB
        //                  ,DT_END_EFF             =
        //           :P58-DT-END-EFF-DB
        //                  ,DT_INI_CALC            =
        //           :P58-DT-INI-CALC-DB
        //                  ,DT_END_CALC            =
        //           :P58-DT-END-CALC-DB
        //                  ,TP_CAUS_BOLLO          =
        //                :P58-TP-CAUS-BOLLO
        //                  ,IMPST_BOLLO_DETT_C     =
        //                :P58-IMPST-BOLLO-DETT-C
        //                  ,IMPST_BOLLO_DETT_V     =
        //                :P58-IMPST-BOLLO-DETT-V
        //                                       :IND-P58-IMPST-BOLLO-DETT-V
        //                  ,IMPST_BOLLO_TOT_V      =
        //                :P58-IMPST-BOLLO-TOT-V
        //                                       :IND-P58-IMPST-BOLLO-TOT-V
        //                  ,IMPST_BOLLO_TOT_R      =
        //                :P58-IMPST-BOLLO-TOT-R
        //                  ,DS_RIGA                =
        //                :P58-DS-RIGA
        //                  ,DS_OPER_SQL            =
        //                :P58-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :P58-DS-VER
        //                  ,DS_TS_INI_CPTZ         =
        //                :P58-DS-TS-INI-CPTZ
        //                  ,DS_TS_END_CPTZ         =
        //                :P58-DS-TS-END-CPTZ
        //                  ,DS_UTENTE              =
        //                :P58-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :P58-DS-STATO-ELAB
        //                WHERE     DS_RIGA = :P58-DS-RIGA
        //                   AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        impstBolloDao.updateRec1(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A360-OPEN-CURSOR-ID-EFF<br>*/
    private void a360OpenCursorIdEff() {
        // COB_CODE: PERFORM A305-DECLARE-CURSOR-ID-EFF THRU A305-EX.
        a305DeclareCursorIdEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-ID-UPD-EFF-P58
        //           END-EXEC.
        impstBolloDao.openCIdUpdEffP58(impstBollo.getP58IdImpstBollo(), ws.getIdsv0010().getWsTsInfinito(), ws.getIdsv0010().getWsDataInizioEffettoDb(), idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A370-CLOSE-CURSOR-ID-EFF<br>*/
    private void a370CloseCursorIdEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-ID-UPD-EFF-P58
        //           END-EXEC.
        impstBolloDao.closeCIdUpdEffP58();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A390-FETCH-NEXT-ID-EFF<br>*/
    private void a390FetchNextIdEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-ID-UPD-EFF-P58
        //           INTO
        //                :P58-ID-IMPST-BOLLO
        //               ,:P58-COD-COMP-ANIA
        //               ,:P58-ID-POLI
        //               ,:P58-IB-POLI
        //               ,:P58-COD-FISC
        //                :IND-P58-COD-FISC
        //               ,:P58-COD-PART-IVA
        //                :IND-P58-COD-PART-IVA
        //               ,:P58-ID-RAPP-ANA
        //               ,:P58-ID-MOVI-CRZ
        //               ,:P58-ID-MOVI-CHIU
        //                :IND-P58-ID-MOVI-CHIU
        //               ,:P58-DT-INI-EFF-DB
        //               ,:P58-DT-END-EFF-DB
        //               ,:P58-DT-INI-CALC-DB
        //               ,:P58-DT-END-CALC-DB
        //               ,:P58-TP-CAUS-BOLLO
        //               ,:P58-IMPST-BOLLO-DETT-C
        //               ,:P58-IMPST-BOLLO-DETT-V
        //                :IND-P58-IMPST-BOLLO-DETT-V
        //               ,:P58-IMPST-BOLLO-TOT-V
        //                :IND-P58-IMPST-BOLLO-TOT-V
        //               ,:P58-IMPST-BOLLO-TOT-R
        //               ,:P58-DS-RIGA
        //               ,:P58-DS-OPER-SQL
        //               ,:P58-DS-VER
        //               ,:P58-DS-TS-INI-CPTZ
        //               ,:P58-DS-TS-END-CPTZ
        //               ,:P58-DS-UTENTE
        //               ,:P58-DS-STATO-ELAB
        //           END-EXEC.
        impstBolloDao.fetchCIdUpdEffP58(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A370-CLOSE-CURSOR-ID-EFF THRU A370-EX
            a370CloseCursorIdEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A405-DECLARE-CURSOR-IDP-EFF<br>
	 * <pre>----
	 * ----  gestione IDP Effetto
	 * ----</pre>*/
    private void a405DeclareCursorIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IDP-EFF-P58 CURSOR FOR
        //              SELECT
        //                     ID_IMPST_BOLLO
        //                    ,COD_COMP_ANIA
        //                    ,ID_POLI
        //                    ,IB_POLI
        //                    ,COD_FISC
        //                    ,COD_PART_IVA
        //                    ,ID_RAPP_ANA
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,DT_INI_CALC
        //                    ,DT_END_CALC
        //                    ,TP_CAUS_BOLLO
        //                    ,IMPST_BOLLO_DETT_C
        //                    ,IMPST_BOLLO_DETT_V
        //                    ,IMPST_BOLLO_TOT_V
        //                    ,IMPST_BOLLO_TOT_R
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //              FROM IMPST_BOLLO
        //              WHERE     ID_RAPP_ANA = :P58-ID-RAPP-ANA
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //              ORDER BY ID_IMPST_BOLLO ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A410-SELECT-IDP-EFF<br>*/
    private void a410SelectIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_IMPST_BOLLO
        //                ,COD_COMP_ANIA
        //                ,ID_POLI
        //                ,IB_POLI
        //                ,COD_FISC
        //                ,COD_PART_IVA
        //                ,ID_RAPP_ANA
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,DT_INI_CALC
        //                ,DT_END_CALC
        //                ,TP_CAUS_BOLLO
        //                ,IMPST_BOLLO_DETT_C
        //                ,IMPST_BOLLO_DETT_V
        //                ,IMPST_BOLLO_TOT_V
        //                ,IMPST_BOLLO_TOT_R
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //             INTO
        //                :P58-ID-IMPST-BOLLO
        //               ,:P58-COD-COMP-ANIA
        //               ,:P58-ID-POLI
        //               ,:P58-IB-POLI
        //               ,:P58-COD-FISC
        //                :IND-P58-COD-FISC
        //               ,:P58-COD-PART-IVA
        //                :IND-P58-COD-PART-IVA
        //               ,:P58-ID-RAPP-ANA
        //               ,:P58-ID-MOVI-CRZ
        //               ,:P58-ID-MOVI-CHIU
        //                :IND-P58-ID-MOVI-CHIU
        //               ,:P58-DT-INI-EFF-DB
        //               ,:P58-DT-END-EFF-DB
        //               ,:P58-DT-INI-CALC-DB
        //               ,:P58-DT-END-CALC-DB
        //               ,:P58-TP-CAUS-BOLLO
        //               ,:P58-IMPST-BOLLO-DETT-C
        //               ,:P58-IMPST-BOLLO-DETT-V
        //                :IND-P58-IMPST-BOLLO-DETT-V
        //               ,:P58-IMPST-BOLLO-TOT-V
        //                :IND-P58-IMPST-BOLLO-TOT-V
        //               ,:P58-IMPST-BOLLO-TOT-R
        //               ,:P58-DS-RIGA
        //               ,:P58-DS-OPER-SQL
        //               ,:P58-DS-VER
        //               ,:P58-DS-TS-INI-CPTZ
        //               ,:P58-DS-TS-END-CPTZ
        //               ,:P58-DS-UTENTE
        //               ,:P58-DS-STATO-ELAB
        //             FROM IMPST_BOLLO
        //             WHERE     ID_RAPP_ANA = :P58-ID-RAPP-ANA
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        impstBolloDao.selectRec1(impstBollo.getP58IdRappAna(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A460-OPEN-CURSOR-IDP-EFF<br>*/
    private void a460OpenCursorIdpEff() {
        // COB_CODE: PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.
        a405DeclareCursorIdpEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-IDP-EFF-P58
        //           END-EXEC.
        impstBolloDao.openCIdpEffP58(impstBollo.getP58IdRappAna(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A470-CLOSE-CURSOR-IDP-EFF<br>*/
    private void a470CloseCursorIdpEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IDP-EFF-P58
        //           END-EXEC.
        impstBolloDao.closeCIdpEffP58();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A480-FETCH-FIRST-IDP-EFF<br>*/
    private void a480FetchFirstIdpEff() {
        // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.
        a460OpenCursorIdpEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
            a490FetchNextIdpEff();
        }
    }

    /**Original name: A490-FETCH-NEXT-IDP-EFF<br>*/
    private void a490FetchNextIdpEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IDP-EFF-P58
        //           INTO
        //                :P58-ID-IMPST-BOLLO
        //               ,:P58-COD-COMP-ANIA
        //               ,:P58-ID-POLI
        //               ,:P58-IB-POLI
        //               ,:P58-COD-FISC
        //                :IND-P58-COD-FISC
        //               ,:P58-COD-PART-IVA
        //                :IND-P58-COD-PART-IVA
        //               ,:P58-ID-RAPP-ANA
        //               ,:P58-ID-MOVI-CRZ
        //               ,:P58-ID-MOVI-CHIU
        //                :IND-P58-ID-MOVI-CHIU
        //               ,:P58-DT-INI-EFF-DB
        //               ,:P58-DT-END-EFF-DB
        //               ,:P58-DT-INI-CALC-DB
        //               ,:P58-DT-END-CALC-DB
        //               ,:P58-TP-CAUS-BOLLO
        //               ,:P58-IMPST-BOLLO-DETT-C
        //               ,:P58-IMPST-BOLLO-DETT-V
        //                :IND-P58-IMPST-BOLLO-DETT-V
        //               ,:P58-IMPST-BOLLO-TOT-V
        //                :IND-P58-IMPST-BOLLO-TOT-V
        //               ,:P58-IMPST-BOLLO-TOT-R
        //               ,:P58-DS-RIGA
        //               ,:P58-DS-OPER-SQL
        //               ,:P58-DS-VER
        //               ,:P58-DS-TS-INI-CPTZ
        //               ,:P58-DS-TS-END-CPTZ
        //               ,:P58-DS-UTENTE
        //               ,:P58-DS-STATO-ELAB
        //           END-EXEC.
        impstBolloDao.fetchCIdpEffP58(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
            a470CloseCursorIdpEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A505-DECLARE-CURSOR-IBO<br>
	 * <pre>----
	 * ----  gestione IBO Effetto e non
	 * ----</pre>*/
    private void a505DeclareCursorIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A510-SELECT-IBO<br>*/
    private void a510SelectIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A560-OPEN-CURSOR-IBO<br>*/
    private void a560OpenCursorIbo() {
        // COB_CODE: PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.
        a505DeclareCursorIbo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A570-CLOSE-CURSOR-IBO<br>*/
    private void a570CloseCursorIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A580-FETCH-FIRST-IBO<br>*/
    private void a580FetchFirstIbo() {
        // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.
        a560OpenCursorIbo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
            a590FetchNextIbo();
        }
    }

    /**Original name: A590-FETCH-NEXT-IBO<br>*/
    private void a590FetchNextIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A605-DCL-CUR-IBS-POLI<br>
	 * <pre>----
	 * ----  gestione IBS Effetto e non
	 * ----</pre>*/
    private void a605DclCurIbsPoli() {
    // COB_CODE: EXEC SQL
    //                DECLARE C-IBS-EFF-P58-0 CURSOR FOR
    //              SELECT
    //                     ID_IMPST_BOLLO
    //                    ,COD_COMP_ANIA
    //                    ,ID_POLI
    //                    ,IB_POLI
    //                    ,COD_FISC
    //                    ,COD_PART_IVA
    //                    ,ID_RAPP_ANA
    //                    ,ID_MOVI_CRZ
    //                    ,ID_MOVI_CHIU
    //                    ,DT_INI_EFF
    //                    ,DT_END_EFF
    //                    ,DT_INI_CALC
    //                    ,DT_END_CALC
    //                    ,TP_CAUS_BOLLO
    //                    ,IMPST_BOLLO_DETT_C
    //                    ,IMPST_BOLLO_DETT_V
    //                    ,IMPST_BOLLO_TOT_V
    //                    ,IMPST_BOLLO_TOT_R
    //                    ,DS_RIGA
    //                    ,DS_OPER_SQL
    //                    ,DS_VER
    //                    ,DS_TS_INI_CPTZ
    //                    ,DS_TS_END_CPTZ
    //                    ,DS_UTENTE
    //                    ,DS_STATO_ELAB
    //              FROM IMPST_BOLLO
    //              WHERE     IB_POLI = :P58-IB-POLI
    //                    AND COD_COMP_ANIA =
    //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
    //                    AND DT_INI_EFF <=
    //                        :WS-DATA-INIZIO-EFFETTO-DB
    //                    AND DT_END_EFF >
    //                        :WS-DATA-INIZIO-EFFETTO-DB
    //                    AND ID_MOVI_CHIU IS NULL
    //              ORDER BY ID_IMPST_BOLLO ASC
    //           END-EXEC.
    // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A605-DECLARE-CURSOR-IBS<br>*/
    private void a605DeclareCursorIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: IF P58-IB-POLI NOT = HIGH-VALUES
        //                  THRU A605-POLI-EX
        //           END-IF.
        if (!Characters.EQ_HIGH.test(impstBollo.getP58IbPoli(), ImpstBollo.Len.P58_IB_POLI)) {
            // COB_CODE: PERFORM A605-DCL-CUR-IBS-POLI
            //              THRU A605-POLI-EX
            a605DclCurIbsPoli();
        }
    }

    /**Original name: A610-SELECT-IBS-POLI<br>*/
    private void a610SelectIbsPoli() {
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_IMPST_BOLLO
        //                ,COD_COMP_ANIA
        //                ,ID_POLI
        //                ,IB_POLI
        //                ,COD_FISC
        //                ,COD_PART_IVA
        //                ,ID_RAPP_ANA
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,DT_INI_CALC
        //                ,DT_END_CALC
        //                ,TP_CAUS_BOLLO
        //                ,IMPST_BOLLO_DETT_C
        //                ,IMPST_BOLLO_DETT_V
        //                ,IMPST_BOLLO_TOT_V
        //                ,IMPST_BOLLO_TOT_R
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //             INTO
        //                :P58-ID-IMPST-BOLLO
        //               ,:P58-COD-COMP-ANIA
        //               ,:P58-ID-POLI
        //               ,:P58-IB-POLI
        //               ,:P58-COD-FISC
        //                :IND-P58-COD-FISC
        //               ,:P58-COD-PART-IVA
        //                :IND-P58-COD-PART-IVA
        //               ,:P58-ID-RAPP-ANA
        //               ,:P58-ID-MOVI-CRZ
        //               ,:P58-ID-MOVI-CHIU
        //                :IND-P58-ID-MOVI-CHIU
        //               ,:P58-DT-INI-EFF-DB
        //               ,:P58-DT-END-EFF-DB
        //               ,:P58-DT-INI-CALC-DB
        //               ,:P58-DT-END-CALC-DB
        //               ,:P58-TP-CAUS-BOLLO
        //               ,:P58-IMPST-BOLLO-DETT-C
        //               ,:P58-IMPST-BOLLO-DETT-V
        //                :IND-P58-IMPST-BOLLO-DETT-V
        //               ,:P58-IMPST-BOLLO-TOT-V
        //                :IND-P58-IMPST-BOLLO-TOT-V
        //               ,:P58-IMPST-BOLLO-TOT-R
        //               ,:P58-DS-RIGA
        //               ,:P58-DS-OPER-SQL
        //               ,:P58-DS-VER
        //               ,:P58-DS-TS-INI-CPTZ
        //               ,:P58-DS-TS-END-CPTZ
        //               ,:P58-DS-UTENTE
        //               ,:P58-DS-STATO-ELAB
        //             FROM IMPST_BOLLO
        //             WHERE     IB_POLI = :P58-IB-POLI
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        impstBolloDao.selectRec2(impstBollo.getP58IbPoli(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
    }

    /**Original name: A610-SELECT-IBS<br>*/
    private void a610SelectIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: IF P58-IB-POLI NOT = HIGH-VALUES
        //                  THRU A610-POLI-EX
        //           END-IF.
        if (!Characters.EQ_HIGH.test(impstBollo.getP58IbPoli(), ImpstBollo.Len.P58_IB_POLI)) {
            // COB_CODE: PERFORM A610-SELECT-IBS-POLI
            //              THRU A610-POLI-EX
            a610SelectIbsPoli();
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A660-OPEN-CURSOR-IBS<br>*/
    private void a660OpenCursorIbs() {
        // COB_CODE: PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.
        a605DeclareCursorIbs();
        // COB_CODE: IF P58-IB-POLI NOT = HIGH-VALUES
        //              END-EXEC
        //           END-IF.
        if (!Characters.EQ_HIGH.test(impstBollo.getP58IbPoli(), ImpstBollo.Len.P58_IB_POLI)) {
            // COB_CODE: EXEC SQL
            //                OPEN C-IBS-EFF-P58-0
            //           END-EXEC
            impstBolloDao.openCIbsEffP580(impstBollo.getP58IbPoli(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb());
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A670-CLOSE-CURSOR-IBS<br>*/
    private void a670CloseCursorIbs() {
        // COB_CODE: IF P58-IB-POLI NOT = HIGH-VALUES
        //              END-EXEC
        //           END-IF.
        if (!Characters.EQ_HIGH.test(impstBollo.getP58IbPoli(), ImpstBollo.Len.P58_IB_POLI)) {
            // COB_CODE: EXEC SQL
            //                CLOSE C-IBS-EFF-P58-0
            //           END-EXEC
            impstBolloDao.closeCIbsEffP580();
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A680-FETCH-FIRST-IBS<br>*/
    private void a680FetchFirstIbs() {
        // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.
        a660OpenCursorIbs();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
            a690FetchNextIbs();
        }
    }

    /**Original name: A690-FN-IBS-POLI<br>*/
    private void a690FnIbsPoli() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IBS-EFF-P58-0
        //           INTO
        //                :P58-ID-IMPST-BOLLO
        //               ,:P58-COD-COMP-ANIA
        //               ,:P58-ID-POLI
        //               ,:P58-IB-POLI
        //               ,:P58-COD-FISC
        //                :IND-P58-COD-FISC
        //               ,:P58-COD-PART-IVA
        //                :IND-P58-COD-PART-IVA
        //               ,:P58-ID-RAPP-ANA
        //               ,:P58-ID-MOVI-CRZ
        //               ,:P58-ID-MOVI-CHIU
        //                :IND-P58-ID-MOVI-CHIU
        //               ,:P58-DT-INI-EFF-DB
        //               ,:P58-DT-END-EFF-DB
        //               ,:P58-DT-INI-CALC-DB
        //               ,:P58-DT-END-CALC-DB
        //               ,:P58-TP-CAUS-BOLLO
        //               ,:P58-IMPST-BOLLO-DETT-C
        //               ,:P58-IMPST-BOLLO-DETT-V
        //                :IND-P58-IMPST-BOLLO-DETT-V
        //               ,:P58-IMPST-BOLLO-TOT-V
        //                :IND-P58-IMPST-BOLLO-TOT-V
        //               ,:P58-IMPST-BOLLO-TOT-R
        //               ,:P58-DS-RIGA
        //               ,:P58-DS-OPER-SQL
        //               ,:P58-DS-VER
        //               ,:P58-DS-TS-INI-CPTZ
        //               ,:P58-DS-TS-END-CPTZ
        //               ,:P58-DS-UTENTE
        //               ,:P58-DS-STATO-ELAB
        //           END-EXEC.
        impstBolloDao.fetchCIbsEffP580(this);
    }

    /**Original name: A690-FETCH-NEXT-IBS<br>*/
    private void a690FetchNextIbs() {
        // COB_CODE: IF P58-IB-POLI NOT = HIGH-VALUES
        //                  THRU A690-POLI-EX
        //           END-IF.
        if (!Characters.EQ_HIGH.test(impstBollo.getP58IbPoli(), ImpstBollo.Len.P58_IB_POLI)) {
            // COB_CODE: PERFORM A690-FN-IBS-POLI
            //              THRU A690-POLI-EX
            a690FnIbsPoli();
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A670-CLOSE-CURSOR-IBS     THRU A670-EX
            a670CloseCursorIbs();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A705-DECLARE-CURSOR-IDO<br>
	 * <pre>----
	 * ----  gestione IDO Effetto e non
	 * ----</pre>*/
    private void a705DeclareCursorIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A710-SELECT-IDO<br>*/
    private void a710SelectIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A760-OPEN-CURSOR-IDO<br>*/
    private void a760OpenCursorIdo() {
        // COB_CODE: PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.
        a705DeclareCursorIdo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A770-CLOSE-CURSOR-IDO<br>*/
    private void a770CloseCursorIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A780-FETCH-FIRST-IDO<br>*/
    private void a780FetchFirstIdo() {
        // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.
        a760OpenCursorIdo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
            a790FetchNextIdo();
        }
    }

    /**Original name: A790-FETCH-NEXT-IDO<br>*/
    private void a790FetchNextIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B310-SELECT-ID-CPZ<br>
	 * <pre>----
	 * ----  gestione ID Competenza
	 * ----</pre>*/
    private void b310SelectIdCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_IMPST_BOLLO
        //                ,COD_COMP_ANIA
        //                ,ID_POLI
        //                ,IB_POLI
        //                ,COD_FISC
        //                ,COD_PART_IVA
        //                ,ID_RAPP_ANA
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,DT_INI_CALC
        //                ,DT_END_CALC
        //                ,TP_CAUS_BOLLO
        //                ,IMPST_BOLLO_DETT_C
        //                ,IMPST_BOLLO_DETT_V
        //                ,IMPST_BOLLO_TOT_V
        //                ,IMPST_BOLLO_TOT_R
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //             INTO
        //                :P58-ID-IMPST-BOLLO
        //               ,:P58-COD-COMP-ANIA
        //               ,:P58-ID-POLI
        //               ,:P58-IB-POLI
        //               ,:P58-COD-FISC
        //                :IND-P58-COD-FISC
        //               ,:P58-COD-PART-IVA
        //                :IND-P58-COD-PART-IVA
        //               ,:P58-ID-RAPP-ANA
        //               ,:P58-ID-MOVI-CRZ
        //               ,:P58-ID-MOVI-CHIU
        //                :IND-P58-ID-MOVI-CHIU
        //               ,:P58-DT-INI-EFF-DB
        //               ,:P58-DT-END-EFF-DB
        //               ,:P58-DT-INI-CALC-DB
        //               ,:P58-DT-END-CALC-DB
        //               ,:P58-TP-CAUS-BOLLO
        //               ,:P58-IMPST-BOLLO-DETT-C
        //               ,:P58-IMPST-BOLLO-DETT-V
        //                :IND-P58-IMPST-BOLLO-DETT-V
        //               ,:P58-IMPST-BOLLO-TOT-V
        //                :IND-P58-IMPST-BOLLO-TOT-V
        //               ,:P58-IMPST-BOLLO-TOT-R
        //               ,:P58-DS-RIGA
        //               ,:P58-DS-OPER-SQL
        //               ,:P58-DS-VER
        //               ,:P58-DS-TS-INI-CPTZ
        //               ,:P58-DS-TS-END-CPTZ
        //               ,:P58-DS-UTENTE
        //               ,:P58-DS-STATO-ELAB
        //             FROM IMPST_BOLLO
        //             WHERE     ID_IMPST_BOLLO = :P58-ID-IMPST-BOLLO
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        impstBolloDao.selectRec3(impstBollo.getP58IdImpstBollo(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B405-DECLARE-CURSOR-IDP-CPZ<br>
	 * <pre>----
	 * ----  gestione IDP Competenza
	 * ----</pre>*/
    private void b405DeclareCursorIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IDP-CPZ-P58 CURSOR FOR
        //              SELECT
        //                     ID_IMPST_BOLLO
        //                    ,COD_COMP_ANIA
        //                    ,ID_POLI
        //                    ,IB_POLI
        //                    ,COD_FISC
        //                    ,COD_PART_IVA
        //                    ,ID_RAPP_ANA
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,DT_INI_CALC
        //                    ,DT_END_CALC
        //                    ,TP_CAUS_BOLLO
        //                    ,IMPST_BOLLO_DETT_C
        //                    ,IMPST_BOLLO_DETT_V
        //                    ,IMPST_BOLLO_TOT_V
        //                    ,IMPST_BOLLO_TOT_R
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //              FROM IMPST_BOLLO
        //              WHERE     ID_RAPP_ANA = :P58-ID-RAPP-ANA
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //              ORDER BY ID_IMPST_BOLLO ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B410-SELECT-IDP-CPZ<br>*/
    private void b410SelectIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_IMPST_BOLLO
        //                ,COD_COMP_ANIA
        //                ,ID_POLI
        //                ,IB_POLI
        //                ,COD_FISC
        //                ,COD_PART_IVA
        //                ,ID_RAPP_ANA
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,DT_INI_CALC
        //                ,DT_END_CALC
        //                ,TP_CAUS_BOLLO
        //                ,IMPST_BOLLO_DETT_C
        //                ,IMPST_BOLLO_DETT_V
        //                ,IMPST_BOLLO_TOT_V
        //                ,IMPST_BOLLO_TOT_R
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //             INTO
        //                :P58-ID-IMPST-BOLLO
        //               ,:P58-COD-COMP-ANIA
        //               ,:P58-ID-POLI
        //               ,:P58-IB-POLI
        //               ,:P58-COD-FISC
        //                :IND-P58-COD-FISC
        //               ,:P58-COD-PART-IVA
        //                :IND-P58-COD-PART-IVA
        //               ,:P58-ID-RAPP-ANA
        //               ,:P58-ID-MOVI-CRZ
        //               ,:P58-ID-MOVI-CHIU
        //                :IND-P58-ID-MOVI-CHIU
        //               ,:P58-DT-INI-EFF-DB
        //               ,:P58-DT-END-EFF-DB
        //               ,:P58-DT-INI-CALC-DB
        //               ,:P58-DT-END-CALC-DB
        //               ,:P58-TP-CAUS-BOLLO
        //               ,:P58-IMPST-BOLLO-DETT-C
        //               ,:P58-IMPST-BOLLO-DETT-V
        //                :IND-P58-IMPST-BOLLO-DETT-V
        //               ,:P58-IMPST-BOLLO-TOT-V
        //                :IND-P58-IMPST-BOLLO-TOT-V
        //               ,:P58-IMPST-BOLLO-TOT-R
        //               ,:P58-DS-RIGA
        //               ,:P58-DS-OPER-SQL
        //               ,:P58-DS-VER
        //               ,:P58-DS-TS-INI-CPTZ
        //               ,:P58-DS-TS-END-CPTZ
        //               ,:P58-DS-UTENTE
        //               ,:P58-DS-STATO-ELAB
        //             FROM IMPST_BOLLO
        //             WHERE     ID_RAPP_ANA = :P58-ID-RAPP-ANA
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        impstBolloDao.selectRec4(impstBollo.getP58IdRappAna(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B460-OPEN-CURSOR-IDP-CPZ<br>*/
    private void b460OpenCursorIdpCpz() {
        // COB_CODE: PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.
        b405DeclareCursorIdpCpz();
        // COB_CODE: EXEC SQL
        //                OPEN C-IDP-CPZ-P58
        //           END-EXEC.
        impstBolloDao.openCIdpCpzP58(impstBollo.getP58IdRappAna(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B470-CLOSE-CURSOR-IDP-CPZ<br>*/
    private void b470CloseCursorIdpCpz() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IDP-CPZ-P58
        //           END-EXEC.
        impstBolloDao.closeCIdpCpzP58();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B480-FETCH-FIRST-IDP-CPZ<br>*/
    private void b480FetchFirstIdpCpz() {
        // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.
        b460OpenCursorIdpCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
            b490FetchNextIdpCpz();
        }
    }

    /**Original name: B490-FETCH-NEXT-IDP-CPZ<br>*/
    private void b490FetchNextIdpCpz() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IDP-CPZ-P58
        //           INTO
        //                :P58-ID-IMPST-BOLLO
        //               ,:P58-COD-COMP-ANIA
        //               ,:P58-ID-POLI
        //               ,:P58-IB-POLI
        //               ,:P58-COD-FISC
        //                :IND-P58-COD-FISC
        //               ,:P58-COD-PART-IVA
        //                :IND-P58-COD-PART-IVA
        //               ,:P58-ID-RAPP-ANA
        //               ,:P58-ID-MOVI-CRZ
        //               ,:P58-ID-MOVI-CHIU
        //                :IND-P58-ID-MOVI-CHIU
        //               ,:P58-DT-INI-EFF-DB
        //               ,:P58-DT-END-EFF-DB
        //               ,:P58-DT-INI-CALC-DB
        //               ,:P58-DT-END-CALC-DB
        //               ,:P58-TP-CAUS-BOLLO
        //               ,:P58-IMPST-BOLLO-DETT-C
        //               ,:P58-IMPST-BOLLO-DETT-V
        //                :IND-P58-IMPST-BOLLO-DETT-V
        //               ,:P58-IMPST-BOLLO-TOT-V
        //                :IND-P58-IMPST-BOLLO-TOT-V
        //               ,:P58-IMPST-BOLLO-TOT-R
        //               ,:P58-DS-RIGA
        //               ,:P58-DS-OPER-SQL
        //               ,:P58-DS-VER
        //               ,:P58-DS-TS-INI-CPTZ
        //               ,:P58-DS-TS-END-CPTZ
        //               ,:P58-DS-UTENTE
        //               ,:P58-DS-STATO-ELAB
        //           END-EXEC.
        impstBolloDao.fetchCIdpCpzP58(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
            b470CloseCursorIdpCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B505-DECLARE-CURSOR-IBO-CPZ<br>
	 * <pre>----
	 * ----  gestione IBO Competenza
	 * ----</pre>*/
    private void b505DeclareCursorIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B510-SELECT-IBO-CPZ<br>*/
    private void b510SelectIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B560-OPEN-CURSOR-IBO-CPZ<br>*/
    private void b560OpenCursorIboCpz() {
        // COB_CODE: PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.
        b505DeclareCursorIboCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B570-CLOSE-CURSOR-IBO-CPZ<br>*/
    private void b570CloseCursorIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B580-FETCH-FIRST-IBO-CPZ<br>*/
    private void b580FetchFirstIboCpz() {
        // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.
        b560OpenCursorIboCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
    }

    /**Original name: B590-FETCH-NEXT-IBO-CPZ<br>*/
    private void b590FetchNextIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B605-DCL-CUR-IBS-POLI<br>
	 * <pre>----
	 * ----  gestione IBS Competenza
	 * ----</pre>*/
    private void b605DclCurIbsPoli() {
    // COB_CODE: EXEC SQL
    //                DECLARE C-IBS-CPZ-P58-0 CURSOR FOR
    //              SELECT
    //                     ID_IMPST_BOLLO
    //                    ,COD_COMP_ANIA
    //                    ,ID_POLI
    //                    ,IB_POLI
    //                    ,COD_FISC
    //                    ,COD_PART_IVA
    //                    ,ID_RAPP_ANA
    //                    ,ID_MOVI_CRZ
    //                    ,ID_MOVI_CHIU
    //                    ,DT_INI_EFF
    //                    ,DT_END_EFF
    //                    ,DT_INI_CALC
    //                    ,DT_END_CALC
    //                    ,TP_CAUS_BOLLO
    //                    ,IMPST_BOLLO_DETT_C
    //                    ,IMPST_BOLLO_DETT_V
    //                    ,IMPST_BOLLO_TOT_V
    //                    ,IMPST_BOLLO_TOT_R
    //                    ,DS_RIGA
    //                    ,DS_OPER_SQL
    //                    ,DS_VER
    //                    ,DS_TS_INI_CPTZ
    //                    ,DS_TS_END_CPTZ
    //                    ,DS_UTENTE
    //                    ,DS_STATO_ELAB
    //              FROM IMPST_BOLLO
    //              WHERE     IB_POLI = :P58-IB-POLI
    //                    AND COD_COMP_ANIA =
    //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
    //                    AND DT_INI_EFF <=
    //                        :WS-DATA-INIZIO-EFFETTO-DB
    //                    AND DT_END_EFF >
    //                        :WS-DATA-INIZIO-EFFETTO-DB
    //                    AND DS_TS_INI_CPTZ <=
    //                         :WS-TS-COMPETENZA
    //                    AND DS_TS_END_CPTZ >
    //                         :WS-TS-COMPETENZA
    //              ORDER BY ID_IMPST_BOLLO ASC
    //           END-EXEC.
    // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B605-DECLARE-CURSOR-IBS-CPZ<br>*/
    private void b605DeclareCursorIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: IF P58-IB-POLI NOT = HIGH-VALUES
        //                  THRU B605-POLI-EX
        //           END-IF.
        if (!Characters.EQ_HIGH.test(impstBollo.getP58IbPoli(), ImpstBollo.Len.P58_IB_POLI)) {
            // COB_CODE: PERFORM B605-DCL-CUR-IBS-POLI
            //              THRU B605-POLI-EX
            b605DclCurIbsPoli();
        }
    }

    /**Original name: B610-SELECT-IBS-POLI<br>*/
    private void b610SelectIbsPoli() {
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_IMPST_BOLLO
        //                ,COD_COMP_ANIA
        //                ,ID_POLI
        //                ,IB_POLI
        //                ,COD_FISC
        //                ,COD_PART_IVA
        //                ,ID_RAPP_ANA
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,DT_INI_CALC
        //                ,DT_END_CALC
        //                ,TP_CAUS_BOLLO
        //                ,IMPST_BOLLO_DETT_C
        //                ,IMPST_BOLLO_DETT_V
        //                ,IMPST_BOLLO_TOT_V
        //                ,IMPST_BOLLO_TOT_R
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //             INTO
        //                :P58-ID-IMPST-BOLLO
        //               ,:P58-COD-COMP-ANIA
        //               ,:P58-ID-POLI
        //               ,:P58-IB-POLI
        //               ,:P58-COD-FISC
        //                :IND-P58-COD-FISC
        //               ,:P58-COD-PART-IVA
        //                :IND-P58-COD-PART-IVA
        //               ,:P58-ID-RAPP-ANA
        //               ,:P58-ID-MOVI-CRZ
        //               ,:P58-ID-MOVI-CHIU
        //                :IND-P58-ID-MOVI-CHIU
        //               ,:P58-DT-INI-EFF-DB
        //               ,:P58-DT-END-EFF-DB
        //               ,:P58-DT-INI-CALC-DB
        //               ,:P58-DT-END-CALC-DB
        //               ,:P58-TP-CAUS-BOLLO
        //               ,:P58-IMPST-BOLLO-DETT-C
        //               ,:P58-IMPST-BOLLO-DETT-V
        //                :IND-P58-IMPST-BOLLO-DETT-V
        //               ,:P58-IMPST-BOLLO-TOT-V
        //                :IND-P58-IMPST-BOLLO-TOT-V
        //               ,:P58-IMPST-BOLLO-TOT-R
        //               ,:P58-DS-RIGA
        //               ,:P58-DS-OPER-SQL
        //               ,:P58-DS-VER
        //               ,:P58-DS-TS-INI-CPTZ
        //               ,:P58-DS-TS-END-CPTZ
        //               ,:P58-DS-UTENTE
        //               ,:P58-DS-STATO-ELAB
        //             FROM IMPST_BOLLO
        //             WHERE     IB_POLI = :P58-IB-POLI
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        impstBolloDao.selectRec5(impstBollo.getP58IbPoli(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
    }

    /**Original name: B610-SELECT-IBS-CPZ<br>*/
    private void b610SelectIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: IF P58-IB-POLI NOT = HIGH-VALUES
        //                  THRU B610-POLI-EX
        //           END-IF.
        if (!Characters.EQ_HIGH.test(impstBollo.getP58IbPoli(), ImpstBollo.Len.P58_IB_POLI)) {
            // COB_CODE: PERFORM B610-SELECT-IBS-POLI
            //              THRU B610-POLI-EX
            b610SelectIbsPoli();
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B660-OPEN-CURSOR-IBS-CPZ<br>*/
    private void b660OpenCursorIbsCpz() {
        // COB_CODE: PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.
        b605DeclareCursorIbsCpz();
        // COB_CODE: IF P58-IB-POLI NOT = HIGH-VALUES
        //              END-EXEC
        //           END-IF.
        if (!Characters.EQ_HIGH.test(impstBollo.getP58IbPoli(), ImpstBollo.Len.P58_IB_POLI)) {
            // COB_CODE: EXEC SQL
            //                OPEN C-IBS-CPZ-P58-0
            //           END-EXEC
            impstBolloDao.openCIbsCpzP580(impstBollo.getP58IbPoli(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza());
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B670-CLOSE-CURSOR-IBS-CPZ<br>*/
    private void b670CloseCursorIbsCpz() {
        // COB_CODE: IF P58-IB-POLI NOT = HIGH-VALUES
        //              END-EXEC
        //           END-IF.
        if (!Characters.EQ_HIGH.test(impstBollo.getP58IbPoli(), ImpstBollo.Len.P58_IB_POLI)) {
            // COB_CODE: EXEC SQL
            //                CLOSE C-IBS-CPZ-P58-0
            //           END-EXEC
            impstBolloDao.closeCIbsCpzP580();
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B680-FETCH-FIRST-IBS-CPZ<br>*/
    private void b680FetchFirstIbsCpz() {
        // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.
        b660OpenCursorIbsCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
    }

    /**Original name: B690-FN-IBS-POLI<br>*/
    private void b690FnIbsPoli() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IBS-CPZ-P58-0
        //           INTO
        //                :P58-ID-IMPST-BOLLO
        //               ,:P58-COD-COMP-ANIA
        //               ,:P58-ID-POLI
        //               ,:P58-IB-POLI
        //               ,:P58-COD-FISC
        //                :IND-P58-COD-FISC
        //               ,:P58-COD-PART-IVA
        //                :IND-P58-COD-PART-IVA
        //               ,:P58-ID-RAPP-ANA
        //               ,:P58-ID-MOVI-CRZ
        //               ,:P58-ID-MOVI-CHIU
        //                :IND-P58-ID-MOVI-CHIU
        //               ,:P58-DT-INI-EFF-DB
        //               ,:P58-DT-END-EFF-DB
        //               ,:P58-DT-INI-CALC-DB
        //               ,:P58-DT-END-CALC-DB
        //               ,:P58-TP-CAUS-BOLLO
        //               ,:P58-IMPST-BOLLO-DETT-C
        //               ,:P58-IMPST-BOLLO-DETT-V
        //                :IND-P58-IMPST-BOLLO-DETT-V
        //               ,:P58-IMPST-BOLLO-TOT-V
        //                :IND-P58-IMPST-BOLLO-TOT-V
        //               ,:P58-IMPST-BOLLO-TOT-R
        //               ,:P58-DS-RIGA
        //               ,:P58-DS-OPER-SQL
        //               ,:P58-DS-VER
        //               ,:P58-DS-TS-INI-CPTZ
        //               ,:P58-DS-TS-END-CPTZ
        //               ,:P58-DS-UTENTE
        //               ,:P58-DS-STATO-ELAB
        //           END-EXEC.
        impstBolloDao.fetchCIbsCpzP580(this);
    }

    /**Original name: B690-FETCH-NEXT-IBS-CPZ<br>*/
    private void b690FetchNextIbsCpz() {
        // COB_CODE: IF P58-IB-POLI NOT = HIGH-VALUES
        //                  THRU B690-POLI-EX
        //           END-IF.
        if (!Characters.EQ_HIGH.test(impstBollo.getP58IbPoli(), ImpstBollo.Len.P58_IB_POLI)) {
            // COB_CODE: PERFORM B690-FN-IBS-POLI
            //              THRU B690-POLI-EX
            b690FnIbsPoli();
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B670-CLOSE-CURSOR-IBS-CPZ     THRU B670-EX
            b670CloseCursorIbsCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B705-DECLARE-CURSOR-IDO-CPZ<br>
	 * <pre>----
	 * ----  gestione IDO Competenza
	 * ----</pre>*/
    private void b705DeclareCursorIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B710-SELECT-IDO-CPZ<br>*/
    private void b710SelectIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B760-OPEN-CURSOR-IDO-CPZ<br>*/
    private void b760OpenCursorIdoCpz() {
        // COB_CODE: PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.
        b705DeclareCursorIdoCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B770-CLOSE-CURSOR-IDO-CPZ<br>*/
    private void b770CloseCursorIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B780-FETCH-FIRST-IDO-CPZ<br>*/
    private void b780FetchFirstIdoCpz() {
        // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.
        b760OpenCursorIdoCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
    }

    /**Original name: B790-FETCH-NEXT-IDO-CPZ<br>*/
    private void b790FetchNextIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-P58-COD-FISC = -1
        //              MOVE HIGH-VALUES TO P58-COD-FISC-NULL
        //           END-IF
        if (ws.getIndImpstBollo().getValQuo() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P58-COD-FISC-NULL
            impstBollo.setP58CodFisc(LiteralGenerator.create(Types.HIGH_CHAR_VAL, ImpstBollo.Len.P58_COD_FISC));
        }
        // COB_CODE: IF IND-P58-COD-PART-IVA = -1
        //              MOVE HIGH-VALUES TO P58-COD-PART-IVA-NULL
        //           END-IF
        if (ws.getIndImpstBollo().getValQuoManfee() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P58-COD-PART-IVA-NULL
            impstBollo.setP58CodPartIva(LiteralGenerator.create(Types.HIGH_CHAR_VAL, ImpstBollo.Len.P58_COD_PART_IVA));
        }
        // COB_CODE: IF IND-P58-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO P58-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndImpstBollo().getDtRilevazioneNav() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P58-ID-MOVI-CHIU-NULL
            impstBollo.getP58IdMoviChiu().setP58IdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P58IdMoviChiu.Len.P58_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-P58-IMPST-BOLLO-DETT-V = -1
        //              MOVE HIGH-VALUES TO P58-IMPST-BOLLO-DETT-V-NULL
        //           END-IF
        if (ws.getIndImpstBollo().getValQuoAcq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P58-IMPST-BOLLO-DETT-V-NULL
            impstBollo.getP58ImpstBolloDettV().setP58ImpstBolloDettVNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P58ImpstBolloDettV.Len.P58_IMPST_BOLLO_DETT_V_NULL));
        }
        // COB_CODE: IF IND-P58-IMPST-BOLLO-TOT-V = -1
        //              MOVE HIGH-VALUES TO P58-IMPST-BOLLO-TOT-V-NULL
        //           END-IF.
        if (ws.getIndImpstBollo().getFlNoNav() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P58-IMPST-BOLLO-TOT-V-NULL
            impstBollo.getP58ImpstBolloTotV().setP58ImpstBolloTotVNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P58ImpstBolloTotV.Len.P58_IMPST_BOLLO_TOT_V_NULL));
        }
    }

    /**Original name: Z150-VALORIZZA-DATA-SERVICES-I<br>*/
    private void z150ValorizzaDataServicesI() {
        // COB_CODE: MOVE 'I' TO P58-DS-OPER-SQL
        impstBollo.setP58DsOperSqlFormatted("I");
        // COB_CODE: MOVE 0                   TO P58-DS-VER
        impstBollo.setP58DsVer(0);
        // COB_CODE: MOVE IDSV0003-USER-NAME TO P58-DS-UTENTE
        impstBollo.setP58DsUtente(idsv0003.getUserName());
        // COB_CODE: MOVE '1'                   TO P58-DS-STATO-ELAB.
        impstBollo.setP58DsStatoElabFormatted("1");
    }

    /**Original name: Z160-VALORIZZA-DATA-SERVICES-U<br>*/
    private void z160ValorizzaDataServicesU() {
        // COB_CODE: MOVE 'U' TO P58-DS-OPER-SQL
        impstBollo.setP58DsOperSqlFormatted("U");
        // COB_CODE: MOVE IDSV0003-USER-NAME TO P58-DS-UTENTE.
        impstBollo.setP58DsUtente(idsv0003.getUserName());
    }

    /**Original name: Z200-SET-INDICATORI-NULL<br>*/
    private void z200SetIndicatoriNull() {
        // COB_CODE: IF P58-COD-FISC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P58-COD-FISC
        //           ELSE
        //              MOVE 0 TO IND-P58-COD-FISC
        //           END-IF
        if (Characters.EQ_HIGH.test(impstBollo.getP58CodFiscFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P58-COD-FISC
            ws.getIndImpstBollo().setValQuo(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P58-COD-FISC
            ws.getIndImpstBollo().setValQuo(((short)0));
        }
        // COB_CODE: IF P58-COD-PART-IVA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P58-COD-PART-IVA
        //           ELSE
        //              MOVE 0 TO IND-P58-COD-PART-IVA
        //           END-IF
        if (Characters.EQ_HIGH.test(impstBollo.getP58CodPartIvaFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P58-COD-PART-IVA
            ws.getIndImpstBollo().setValQuoManfee(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P58-COD-PART-IVA
            ws.getIndImpstBollo().setValQuoManfee(((short)0));
        }
        // COB_CODE: IF P58-ID-MOVI-CHIU-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P58-ID-MOVI-CHIU
        //           ELSE
        //              MOVE 0 TO IND-P58-ID-MOVI-CHIU
        //           END-IF
        if (Characters.EQ_HIGH.test(impstBollo.getP58IdMoviChiu().getP58IdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P58-ID-MOVI-CHIU
            ws.getIndImpstBollo().setDtRilevazioneNav(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P58-ID-MOVI-CHIU
            ws.getIndImpstBollo().setDtRilevazioneNav(((short)0));
        }
        // COB_CODE: IF P58-IMPST-BOLLO-DETT-V-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P58-IMPST-BOLLO-DETT-V
        //           ELSE
        //              MOVE 0 TO IND-P58-IMPST-BOLLO-DETT-V
        //           END-IF
        if (Characters.EQ_HIGH.test(impstBollo.getP58ImpstBolloDettV().getP58ImpstBolloDettVNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P58-IMPST-BOLLO-DETT-V
            ws.getIndImpstBollo().setValQuoAcq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P58-IMPST-BOLLO-DETT-V
            ws.getIndImpstBollo().setValQuoAcq(((short)0));
        }
        // COB_CODE: IF P58-IMPST-BOLLO-TOT-V-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P58-IMPST-BOLLO-TOT-V
        //           ELSE
        //              MOVE 0 TO IND-P58-IMPST-BOLLO-TOT-V
        //           END-IF.
        if (Characters.EQ_HIGH.test(impstBollo.getP58ImpstBolloTotV().getP58ImpstBolloTotVNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P58-IMPST-BOLLO-TOT-V
            ws.getIndImpstBollo().setFlNoNav(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P58-IMPST-BOLLO-TOT-V
            ws.getIndImpstBollo().setFlNoNav(((short)0));
        }
    }

    /**Original name: Z400-SEQ-RIGA<br>*/
    private void z400SeqRiga() {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_RIGA
        //              INTO : P58-DS-RIGA
        //           END-EXEC.
        //TODO: Implement: valuesStmt;
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: Z500-AGGIORNAMENTO-STORICO<br>*/
    private void z500AggiornamentoStorico() {
        // COB_CODE: MOVE IMPST-BOLLO TO WS-BUFFER-TABLE.
        ws.setWsBufferTable(impstBollo.getImpstBolloFormatted());
        // COB_CODE: MOVE P58-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.
        ws.setWsIdMoviCrz(TruncAbs.toInt(impstBollo.getP58IdMoviCrz(), 9));
        // COB_CODE: PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.
        a360OpenCursorIdEff();
        // COB_CODE: PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-SQL
        //              END-IF
        //           END-PERFORM.
        while (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX
            a390FetchNextIdEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              END-IF
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: MOVE WS-ID-MOVI-CRZ   TO P58-ID-MOVI-CHIU
                impstBollo.getP58IdMoviChiu().setP58IdMoviChiu(ws.getWsIdMoviCrz());
                // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
                //                                 TO P58-DS-TS-END-CPTZ
                impstBollo.setP58DsTsEndCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
                // COB_CODE: PERFORM A330-UPDATE-ID-EFF THRU A330-EX
                a330UpdateIdEff();
                // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
                //                 END-IF
                //           END-IF
                if (idsv0003.getSqlcode().isSuccessfulSql()) {
                    // COB_CODE: MOVE WS-ID-MOVI-CRZ TO P58-ID-MOVI-CRZ
                    impstBollo.setP58IdMoviCrz(ws.getWsIdMoviCrz());
                    // COB_CODE: MOVE HIGH-VALUES    TO P58-ID-MOVI-CHIU-NULL
                    impstBollo.getP58IdMoviChiu().setP58IdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P58IdMoviChiu.Len.P58_ID_MOVI_CHIU_NULL));
                    // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
                    //                               TO P58-DT-END-EFF
                    impstBollo.setP58DtEndEff(idsv0003.getDataInizioEffetto());
                    // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
                    //                               TO P58-DS-TS-INI-CPTZ
                    impstBollo.setP58DsTsIniCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
                    // COB_CODE: MOVE WS-TS-INFINITO
                    //                               TO P58-DS-TS-END-CPTZ
                    impstBollo.setP58DsTsEndCptz(ws.getIdsv0010().getWsTsInfinito());
                    // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
                    //              PERFORM A220-INSERT-PK THRU A220-EX
                    //           END-IF
                    if (idsv0003.getSqlcode().isSuccessfulSql()) {
                        // COB_CODE: PERFORM A220-INSERT-PK THRU A220-EX
                        a220InsertPk();
                    }
                }
            }
        }
        // COB_CODE: IF IDSV0003-NOT-FOUND
        //              END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF NOT IDSV0003-DELETE-LOGICA
            //              PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX
            //           ELSE
            //              SET IDSV0003-SUCCESSFUL-SQL TO TRUE
            //           END-IF
            if (!idsv0003.getOperazione().isDeleteLogica()) {
                // COB_CODE: PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX
                z600InsertNuovaRigaStorica();
            }
            else {
                // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL TO TRUE
                idsv0003.getSqlcode().setSuccessfulSql();
            }
        }
    }

    /**Original name: Z550-AGG-STORICO-SOLO-INS<br>*/
    private void z550AggStoricoSoloIns() {
        // COB_CODE: MOVE IMPST-BOLLO TO WS-BUFFER-TABLE.
        ws.setWsBufferTable(impstBollo.getImpstBolloFormatted());
        // COB_CODE: MOVE P58-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.
        ws.setWsIdMoviCrz(TruncAbs.toInt(impstBollo.getP58IdMoviCrz(), 9));
        // COB_CODE: PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX.
        z600InsertNuovaRigaStorica();
    }

    /**Original name: Z600-INSERT-NUOVA-RIGA-STORICA<br>*/
    private void z600InsertNuovaRigaStorica() {
        // COB_CODE: MOVE WS-BUFFER-TABLE TO IMPST-BOLLO.
        impstBollo.setImpstBolloFormatted(ws.getWsBufferTableFormatted());
        // COB_CODE: MOVE WS-ID-MOVI-CRZ  TO P58-ID-MOVI-CRZ.
        impstBollo.setP58IdMoviCrz(ws.getWsIdMoviCrz());
        // COB_CODE: MOVE HIGH-VALUES     TO P58-ID-MOVI-CHIU-NULL.
        impstBollo.getP58IdMoviChiu().setP58IdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P58IdMoviChiu.Len.P58_ID_MOVI_CHIU_NULL));
        // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
        //                                TO P58-DT-INI-EFF.
        impstBollo.setP58DtIniEff(idsv0003.getDataInizioEffetto());
        // COB_CODE: MOVE WS-DT-INFINITO
        //                                TO P58-DT-END-EFF.
        impstBollo.setP58DtEndEff(ws.getIdsv0010().getWsDtInfinito());
        // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
        //                                TO P58-DS-TS-INI-CPTZ.
        impstBollo.setP58DsTsIniCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
        // COB_CODE: MOVE WS-TS-INFINITO
        //                                TO P58-DS-TS-END-CPTZ.
        impstBollo.setP58DsTsEndCptz(ws.getIdsv0010().getWsTsInfinito());
        // COB_CODE: MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
        //                                TO P58-COD-COMP-ANIA.
        impstBollo.setP58CodCompAnia(idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A220-INSERT-PK THRU A220-EX.
        a220InsertPk();
    }

    /**Original name: Z900-CONVERTI-N-TO-X<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da 9(8) comp-3 a date
	 * ----</pre>*/
    private void z900ConvertiNToX() {
        // COB_CODE: MOVE P58-DT-INI-EFF TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(impstBollo.getP58DtIniEff(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO P58-DT-INI-EFF-DB
        ws.getImpstBolloDb().setEffDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: MOVE P58-DT-END-EFF TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(impstBollo.getP58DtEndEff(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO P58-DT-END-EFF-DB
        ws.getImpstBolloDb().setRgstrzRichDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: MOVE P58-DT-INI-CALC TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(impstBollo.getP58DtIniCalc(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO P58-DT-INI-CALC-DB
        ws.getImpstBolloDb().setPervRichDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: MOVE P58-DT-END-CALC TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(impstBollo.getP58DtEndCalc(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO P58-DT-END-CALC-DB.
        ws.getImpstBolloDb().setEsecRichDb(ws.getIdsv0010().getWsDateX());
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE P58-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getImpstBolloDb().getEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO P58-DT-INI-EFF
        impstBollo.setP58DtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE P58-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getImpstBolloDb().getRgstrzRichDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO P58-DT-END-EFF
        impstBollo.setP58DtEndEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE P58-DT-INI-CALC-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getImpstBolloDb().getPervRichDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO P58-DT-INI-CALC
        impstBollo.setP58DtIniCalc(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE P58-DT-END-CALC-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getImpstBolloDb().getEsecRichDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO P58-DT-END-CALC.
        impstBollo.setP58DtEndCalc(ws.getIdsv0010().getWsDateN());
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public int getCodCompAnia() {
        return impstBollo.getP58CodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.impstBollo.setP58CodCompAnia(codCompAnia);
    }

    @Override
    public String getCodFisc() {
        return impstBollo.getP58CodFisc();
    }

    @Override
    public void setCodFisc(String codFisc) {
        this.impstBollo.setP58CodFisc(codFisc);
    }

    @Override
    public String getCodFiscObj() {
        if (ws.getIndImpstBollo().getValQuo() >= 0) {
            return getCodFisc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodFiscObj(String codFiscObj) {
        if (codFiscObj != null) {
            setCodFisc(codFiscObj);
            ws.getIndImpstBollo().setValQuo(((short)0));
        }
        else {
            ws.getIndImpstBollo().setValQuo(((short)-1));
        }
    }

    @Override
    public String getCodPartIva() {
        return impstBollo.getP58CodPartIva();
    }

    @Override
    public void setCodPartIva(String codPartIva) {
        this.impstBollo.setP58CodPartIva(codPartIva);
    }

    @Override
    public String getCodPartIvaObj() {
        if (ws.getIndImpstBollo().getValQuoManfee() >= 0) {
            return getCodPartIva();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodPartIvaObj(String codPartIvaObj) {
        if (codPartIvaObj != null) {
            setCodPartIva(codPartIvaObj);
            ws.getIndImpstBollo().setValQuoManfee(((short)0));
        }
        else {
            ws.getIndImpstBollo().setValQuoManfee(((short)-1));
        }
    }

    @Override
    public AfDecimal getDettC() {
        throw new FieldNotMappedException("dettC");
    }

    @Override
    public void setDettC(AfDecimal dettC) {
        throw new FieldNotMappedException("dettC");
    }

    @Override
    public AfDecimal getDettV() {
        throw new FieldNotMappedException("dettV");
    }

    @Override
    public void setDettV(AfDecimal dettV) {
        throw new FieldNotMappedException("dettV");
    }

    @Override
    public AfDecimal getDettVObj() {
        return getDettV();
    }

    @Override
    public void setDettVObj(AfDecimal dettVObj) {
        setDettV(new AfDecimal(dettVObj, 15, 3));
    }

    @Override
    public char getDsOperSql() {
        return impstBollo.getP58DsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.impstBollo.setP58DsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return impstBollo.getP58DsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.impstBollo.setP58DsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsEndCptz() {
        return impstBollo.getP58DsTsEndCptz();
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.impstBollo.setP58DsTsEndCptz(dsTsEndCptz);
    }

    @Override
    public long getDsTsIniCptz() {
        return impstBollo.getP58DsTsIniCptz();
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.impstBollo.setP58DsTsIniCptz(dsTsIniCptz);
    }

    @Override
    public String getDsUtente() {
        return impstBollo.getP58DsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.impstBollo.setP58DsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return impstBollo.getP58DsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.impstBollo.setP58DsVer(dsVer);
    }

    @Override
    public String getDtEndCalcDb() {
        return ws.getImpstBolloDb().getEsecRichDb();
    }

    @Override
    public void setDtEndCalcDb(String dtEndCalcDb) {
        this.ws.getImpstBolloDb().setEsecRichDb(dtEndCalcDb);
    }

    @Override
    public String getDtEndEffDb() {
        return ws.getImpstBolloDb().getRgstrzRichDb();
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        this.ws.getImpstBolloDb().setRgstrzRichDb(dtEndEffDb);
    }

    @Override
    public String getDtIniCalcDb() {
        return ws.getImpstBolloDb().getPervRichDb();
    }

    @Override
    public void setDtIniCalcDb(String dtIniCalcDb) {
        this.ws.getImpstBolloDb().setPervRichDb(dtIniCalcDb);
    }

    @Override
    public String getDtIniEffDb() {
        return ws.getImpstBolloDb().getEffDb();
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        this.ws.getImpstBolloDb().setEffDb(dtIniEffDb);
    }

    @Override
    public String getIbPoli() {
        return impstBollo.getP58IbPoli();
    }

    @Override
    public void setIbPoli(String ibPoli) {
        this.impstBollo.setP58IbPoli(ibPoli);
    }

    @Override
    public int getIdImpstBollo() {
        return impstBollo.getP58IdImpstBollo();
    }

    @Override
    public void setIdImpstBollo(int idImpstBollo) {
        this.impstBollo.setP58IdImpstBollo(idImpstBollo);
    }

    @Override
    public int getIdMoviChiu() {
        return impstBollo.getP58IdMoviChiu().getP58IdMoviChiu();
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        this.impstBollo.getP58IdMoviChiu().setP58IdMoviChiu(idMoviChiu);
    }

    @Override
    public Integer getIdMoviChiuObj() {
        if (ws.getIndImpstBollo().getDtRilevazioneNav() >= 0) {
            return ((Integer)getIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        if (idMoviChiuObj != null) {
            setIdMoviChiu(((int)idMoviChiuObj));
            ws.getIndImpstBollo().setDtRilevazioneNav(((short)0));
        }
        else {
            ws.getIndImpstBollo().setDtRilevazioneNav(((short)-1));
        }
    }

    @Override
    public int getIdMoviCrz() {
        return impstBollo.getP58IdMoviCrz();
    }

    @Override
    public void setIdMoviCrz(int idMoviCrz) {
        this.impstBollo.setP58IdMoviCrz(idMoviCrz);
    }

    @Override
    public int getIdPoli() {
        return impstBollo.getP58IdPoli();
    }

    @Override
    public void setIdPoli(int idPoli) {
        this.impstBollo.setP58IdPoli(idPoli);
    }

    @Override
    public int getIdRappAna() {
        return impstBollo.getP58IdRappAna();
    }

    @Override
    public void setIdRappAna(int idRappAna) {
        this.impstBollo.setP58IdRappAna(idRappAna);
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        throw new FieldNotMappedException("idsv0003CodiceCompagniaAnia");
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        throw new FieldNotMappedException("idsv0003CodiceCompagniaAnia");
    }

    @Override
    public AfDecimal getImpstBolloDettC() {
        return impstBollo.getP58ImpstBolloDettC();
    }

    @Override
    public void setImpstBolloDettC(AfDecimal impstBolloDettC) {
        this.impstBollo.setP58ImpstBolloDettC(impstBolloDettC.copy());
    }

    @Override
    public AfDecimal getImpstBolloDettV() {
        return impstBollo.getP58ImpstBolloDettV().getP58ImpstBolloDettV();
    }

    @Override
    public void setImpstBolloDettV(AfDecimal impstBolloDettV) {
        this.impstBollo.getP58ImpstBolloDettV().setP58ImpstBolloDettV(impstBolloDettV.copy());
    }

    @Override
    public AfDecimal getImpstBolloDettVObj() {
        if (ws.getIndImpstBollo().getValQuoAcq() >= 0) {
            return getImpstBolloDettV();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstBolloDettVObj(AfDecimal impstBolloDettVObj) {
        if (impstBolloDettVObj != null) {
            setImpstBolloDettV(new AfDecimal(impstBolloDettVObj, 15, 3));
            ws.getIndImpstBollo().setValQuoAcq(((short)0));
        }
        else {
            ws.getIndImpstBollo().setValQuoAcq(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstBolloTotR() {
        return impstBollo.getP58ImpstBolloTotR();
    }

    @Override
    public void setImpstBolloTotR(AfDecimal impstBolloTotR) {
        this.impstBollo.setP58ImpstBolloTotR(impstBolloTotR.copy());
    }

    @Override
    public AfDecimal getImpstBolloTotV() {
        return impstBollo.getP58ImpstBolloTotV().getP58ImpstBolloTotV();
    }

    @Override
    public void setImpstBolloTotV(AfDecimal impstBolloTotV) {
        this.impstBollo.getP58ImpstBolloTotV().setP58ImpstBolloTotV(impstBolloTotV.copy());
    }

    @Override
    public AfDecimal getImpstBolloTotVObj() {
        if (ws.getIndImpstBollo().getFlNoNav() >= 0) {
            return getImpstBolloTotV();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstBolloTotVObj(AfDecimal impstBolloTotVObj) {
        if (impstBolloTotVObj != null) {
            setImpstBolloTotV(new AfDecimal(impstBolloTotVObj, 15, 3));
            ws.getIndImpstBollo().setFlNoNav(((short)0));
        }
        else {
            ws.getIndImpstBollo().setFlNoNav(((short)-1));
        }
    }

    @Override
    public String getLdbve391CodFisc() {
        throw new FieldNotMappedException("ldbve391CodFisc");
    }

    @Override
    public void setLdbve391CodFisc(String ldbve391CodFisc) {
        throw new FieldNotMappedException("ldbve391CodFisc");
    }

    @Override
    public String getLdbve391DtEndCalcDb() {
        throw new FieldNotMappedException("ldbve391DtEndCalcDb");
    }

    @Override
    public void setLdbve391DtEndCalcDb(String ldbve391DtEndCalcDb) {
        throw new FieldNotMappedException("ldbve391DtEndCalcDb");
    }

    @Override
    public String getLdbve391DtIniCalcDb() {
        throw new FieldNotMappedException("ldbve391DtIniCalcDb");
    }

    @Override
    public void setLdbve391DtIniCalcDb(String ldbve391DtIniCalcDb) {
        throw new FieldNotMappedException("ldbve391DtIniCalcDb");
    }

    @Override
    public String getLdbve391TpCausBollo1() {
        throw new FieldNotMappedException("ldbve391TpCausBollo1");
    }

    @Override
    public void setLdbve391TpCausBollo1(String ldbve391TpCausBollo1) {
        throw new FieldNotMappedException("ldbve391TpCausBollo1");
    }

    @Override
    public String getLdbve391TpCausBollo2() {
        throw new FieldNotMappedException("ldbve391TpCausBollo2");
    }

    @Override
    public void setLdbve391TpCausBollo2(String ldbve391TpCausBollo2) {
        throw new FieldNotMappedException("ldbve391TpCausBollo2");
    }

    @Override
    public String getLdbve391TpCausBollo3() {
        throw new FieldNotMappedException("ldbve391TpCausBollo3");
    }

    @Override
    public void setLdbve391TpCausBollo3(String ldbve391TpCausBollo3) {
        throw new FieldNotMappedException("ldbve391TpCausBollo3");
    }

    @Override
    public String getLdbve391TpCausBollo4() {
        throw new FieldNotMappedException("ldbve391TpCausBollo4");
    }

    @Override
    public void setLdbve391TpCausBollo4(String ldbve391TpCausBollo4) {
        throw new FieldNotMappedException("ldbve391TpCausBollo4");
    }

    @Override
    public String getLdbve391TpCausBollo5() {
        throw new FieldNotMappedException("ldbve391TpCausBollo5");
    }

    @Override
    public void setLdbve391TpCausBollo5(String ldbve391TpCausBollo5) {
        throw new FieldNotMappedException("ldbve391TpCausBollo5");
    }

    @Override
    public String getLdbve391TpCausBollo6() {
        throw new FieldNotMappedException("ldbve391TpCausBollo6");
    }

    @Override
    public void setLdbve391TpCausBollo6(String ldbve391TpCausBollo6) {
        throw new FieldNotMappedException("ldbve391TpCausBollo6");
    }

    @Override
    public String getLdbve391TpCausBollo7() {
        throw new FieldNotMappedException("ldbve391TpCausBollo7");
    }

    @Override
    public void setLdbve391TpCausBollo7(String ldbve391TpCausBollo7) {
        throw new FieldNotMappedException("ldbve391TpCausBollo7");
    }

    @Override
    public String getLdbve421CodPartIva() {
        throw new FieldNotMappedException("ldbve421CodPartIva");
    }

    @Override
    public void setLdbve421CodPartIva(String ldbve421CodPartIva) {
        throw new FieldNotMappedException("ldbve421CodPartIva");
    }

    @Override
    public String getLdbve421DtEndCalcDb() {
        throw new FieldNotMappedException("ldbve421DtEndCalcDb");
    }

    @Override
    public void setLdbve421DtEndCalcDb(String ldbve421DtEndCalcDb) {
        throw new FieldNotMappedException("ldbve421DtEndCalcDb");
    }

    @Override
    public String getLdbve421DtIniCalcDb() {
        throw new FieldNotMappedException("ldbve421DtIniCalcDb");
    }

    @Override
    public void setLdbve421DtIniCalcDb(String ldbve421DtIniCalcDb) {
        throw new FieldNotMappedException("ldbve421DtIniCalcDb");
    }

    @Override
    public String getLdbve421TpCausBollo1() {
        throw new FieldNotMappedException("ldbve421TpCausBollo1");
    }

    @Override
    public void setLdbve421TpCausBollo1(String ldbve421TpCausBollo1) {
        throw new FieldNotMappedException("ldbve421TpCausBollo1");
    }

    @Override
    public String getLdbve421TpCausBollo2() {
        throw new FieldNotMappedException("ldbve421TpCausBollo2");
    }

    @Override
    public void setLdbve421TpCausBollo2(String ldbve421TpCausBollo2) {
        throw new FieldNotMappedException("ldbve421TpCausBollo2");
    }

    @Override
    public String getLdbve421TpCausBollo3() {
        throw new FieldNotMappedException("ldbve421TpCausBollo3");
    }

    @Override
    public void setLdbve421TpCausBollo3(String ldbve421TpCausBollo3) {
        throw new FieldNotMappedException("ldbve421TpCausBollo3");
    }

    @Override
    public String getLdbve421TpCausBollo4() {
        throw new FieldNotMappedException("ldbve421TpCausBollo4");
    }

    @Override
    public void setLdbve421TpCausBollo4(String ldbve421TpCausBollo4) {
        throw new FieldNotMappedException("ldbve421TpCausBollo4");
    }

    @Override
    public String getLdbve421TpCausBollo5() {
        throw new FieldNotMappedException("ldbve421TpCausBollo5");
    }

    @Override
    public void setLdbve421TpCausBollo5(String ldbve421TpCausBollo5) {
        throw new FieldNotMappedException("ldbve421TpCausBollo5");
    }

    @Override
    public String getLdbve421TpCausBollo6() {
        throw new FieldNotMappedException("ldbve421TpCausBollo6");
    }

    @Override
    public void setLdbve421TpCausBollo6(String ldbve421TpCausBollo6) {
        throw new FieldNotMappedException("ldbve421TpCausBollo6");
    }

    @Override
    public String getLdbve421TpCausBollo7() {
        throw new FieldNotMappedException("ldbve421TpCausBollo7");
    }

    @Override
    public void setLdbve421TpCausBollo7(String ldbve421TpCausBollo7) {
        throw new FieldNotMappedException("ldbve421TpCausBollo7");
    }

    @Override
    public long getP58DsRiga() {
        return impstBollo.getP58DsRiga();
    }

    @Override
    public void setP58DsRiga(long p58DsRiga) {
        this.impstBollo.setP58DsRiga(p58DsRiga);
    }

    @Override
    public AfDecimal getTotR() {
        throw new FieldNotMappedException("totR");
    }

    @Override
    public void setTotR(AfDecimal totR) {
        throw new FieldNotMappedException("totR");
    }

    @Override
    public AfDecimal getTotV() {
        throw new FieldNotMappedException("totV");
    }

    @Override
    public void setTotV(AfDecimal totV) {
        throw new FieldNotMappedException("totV");
    }

    @Override
    public AfDecimal getTotVObj() {
        return getTotV();
    }

    @Override
    public void setTotVObj(AfDecimal totVObj) {
        setTotV(new AfDecimal(totVObj, 15, 3));
    }

    @Override
    public String getTpCausBollo() {
        return impstBollo.getP58TpCausBollo();
    }

    @Override
    public void setTpCausBollo(String tpCausBollo) {
        this.impstBollo.setP58TpCausBollo(tpCausBollo);
    }

    @Override
    public String getWsDataInizioEffettoDb() {
        throw new FieldNotMappedException("wsDataInizioEffettoDb");
    }

    @Override
    public void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb) {
        throw new FieldNotMappedException("wsDataInizioEffettoDb");
    }

    @Override
    public long getWsTsCompetenza() {
        throw new FieldNotMappedException("wsTsCompetenza");
    }

    @Override
    public void setWsTsCompetenza(long wsTsCompetenza) {
        throw new FieldNotMappedException("wsTsCompetenza");
    }
}
