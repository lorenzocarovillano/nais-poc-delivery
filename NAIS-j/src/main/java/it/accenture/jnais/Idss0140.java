package it.accenture.jnais;

import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.ws.CampiNumerici;
import it.accenture.jnais.ws.Idss0140Link;
import it.accenture.jnais.ws.WsCampiAppoggio;
import static java.lang.Math.abs;

/**Original name: IDSS0140<br>
 * <pre>****************************************************************
 *                                                                *
 *                    IDENTIFICATION DIVISION                     *
 *                                                                *
 * ****************************************************************
 * AUTHOR.        ACCENTURE.
 * DATE-WRITTEN.  04/12/2006.
 * ****************************************************************
 *                                                                *
 *     NOME :           IDSS0140                                  *
 *     TIPO :           ROUTINE COBOL                             *
 *     DESCRIZIONE :    ROUTINE PER LA CONVERSIONE DEI DATI       *
 *                      TRA IL DB NAIS E I FILE VSAM LIFE         *
 *                      NUMERO --> NUMERO                         *
 *                                                                *
 *     AREE DI PASSAGGIO DATI                                     *
 *                                                                *
 *     DATI DI INPUT : IDSV0141                                   *
 *     DATI DI OUTPUT: IDSV0141                                   *
 *                                                                *
 * ****************************************************************
 *  --- TABELLINA DI DECODIFICA DATI DA VSAM A DB2 --- *
 *  --- operazione di PRESA IN CARICO
 *  -- ALFA NUMERICI
 *  PIC  X(001)               PIC S9(002)V     COMP-3
 *  PIC  X(003)               PIC  X(020)
 *  PIC  X(005)               PIC S9(005)V     COMP-3
 *  -- NUMERICI INTERI NON SEGNATI
 *  PIC  9(002)               PIC S9(003)V9(3) COMP-3
 *  PIC  9(002)               PIC S9(005)V     COMP-3
 *  PIC  9(002)               PIC S9(006)V     COMP-3
 *  PIC  9(002)V9(3)          PIC S9(003)V9(3) COMP-3
 *  PIC  9(003)               PIC S9(005)V     COMP-3
 *  PIC  9(004)               PIC S9(003)V     COMP-3
 *  PIC  9(004)               PIC S9(005)V     COMP-3
 *  PIC  9(004)               PIC  X(040)
 *  PIC  9(008)               PIC S9(008)V     COMP-3
 *  -- NUMERICI COMP-3
 *  PIC  9(002)V9(2) COMP-3   PIC S9(003)V9(3) COMP-3
 *  PIC  9(002)V9(3) COMP-3   PIC S9(003)V9(3) COMP-3
 *  PIC  9(003)      COMP-3   PIC  X(020)
 *  PIC  9(003)      COMP-3   PIC S9(005)V     COMP-3
 *  PIC  9(003)V9(2) COMP-3   PIC S9(003)V9(3) COMP-3
 *  PIC  9(003)V9(3) COMP-3   PIC S9(003)V9(2) COMP-3
 *  PIC  9(005)      COMP-3   PIC  X(012)
 *  PIC  9(005)      COMP-3   PIC  X(020)
 *  PIC  9(005)      COMP-3   PIC S9(003)V9(3) COMP-3
 *  PIC  9(005)V9(3) COMP-3   PIC S9(012)V9(3) COMP-3
 *  PIC  9(007)V9(3) COMP-3   PIC S9(012)V9(3) COMP-3
 *  PIC  9(007)      COMP-3   PIC S9(009)V     COMP-3
 *  PIC  9(008)V9(3) COMP-3   PIC S9(012)V9(3) COMP-3
 *  PIC S9(008)      COMP-3   PIC  X(020)
 *  PIC  9(009)      COMP-3   PIC S9(005)V     COMP-3
 *  PIC  9(009)      COMP-3   PIC  X(040)
 *  PIC  9(009)V9(3) COMP-3   PIC S9(012)V9(3) COMP-3
 *  PIC  9(011)V9(3) COMP-3   PIC S9(012)V9(3) COMP-3
 *  -------------------------------------------------- *
 *  -------------------------------------------------- *
 *  --- TABELLINA DI DECODIFICA DATI DA DB2 A VSAM --- *
 *  -- ALFA NUMERICI
 *  PIC  X(012)               PIC  9(005)      COMP-3
 *  PIC  X(020)               PIC  X(003)
 *  PIC  X(020)               PIC  9(003)      COMP-3
 *  PIC  X(020)               PIC  9(005)      COMP-3
 *  PIC  X(020)               PIC S9(008)      COMP-3
 *  PIC  X(040)               PIC  9(004)
 *  PIC  X(040)               PIC  9(009)      COMP-3
 *  -- NUMERICI COMP-3
 *  PIC S9(002)V     COMP-3   PIC  X(001)
 *  PIC S9(003)V9(2) COMP-3   PIC  9(003)V9(3) COMP-3
 *  PIC S9(003)V9(3) COMP-3   PIC  9(002)
 *  PIC S9(003)V9(3) COMP-3   PIC  9(002)V9(3)
 *  PIC S9(003)V9(3) COMP-3   PIC  9(002)V9(2) COMP-3
 *  PIC S9(003)V9(3) COMP-3   PIC  9(002)V9(3) COMP-3
 *  PIC S9(003)V9(3) COMP-3   PIC  9(003)V9(2) COMP-3
 *  PIC S9(003)V9(3) COMP-3   PIC  9(004)      COMP-3
 *  PIC S9(003)V9(3) COMP-3   PIC  9(005)      COMP-3
 *  PIC S9(005)V     COMP-3   PIC  X(005)
 *  PIC S9(005)V     COMP-3   PIC  9(002)
 *  PIC S9(005)V     COMP-3   PIC  9(003)
 *  PIC S9(005)V     COMP-3   PIC  9(003)      COMP-3
 *  PIC S9(005)V     COMP-3   PIC  9(004)
 *  PIC S9(005)V     COMP-3   PIC  9(009)      COMP-3
 *  PIC S9(006)V     COMP-3   PIC  9(002)
 *  PIC S9(008)V     COMP-3   PIC  9(008)
 *  PIC S9(009)V     COMP-3   PIC  9(007)      COMP-3
 *  PIC S9(012)V9(3) COMP-3   PIC  9(005)V9(3) COMP-3
 *  PIC S9(012)V9(3) COMP-3   PIC  9(007)V9(3) COMP-3
 *  PIC S9(012)V9(3) COMP-3   PIC  9(008)V9(3) COMP-3
 *  PIC S9(012)V9(3) COMP-3   PIC  9(009)V9(3) COMP-3
 *  PIC S9(012)V9(3) COMP-3   PIC  9(011)V9(3) COMP-3
 *  -------------------------------------------------- *
 * ****************************************************************
 * ****************************************************************
 *                                                                *
 *                    ENVIRONMENT  DIVISION                       *
 *                                                                *
 * ****************************************************************
 * SOURCE-COMPUTER. IBM-370 WITH DEBUGGING MODE.
 * OBJECT-COMPUTER. IBM-370.</pre>*/
public class Idss0140 extends Program {

    //==== PROPERTIES ====
    //Original name: WS-CAMPI-APPOGGIO
    private WsCampiAppoggio wsCampiAppoggio = new WsCampiAppoggio();
    //Original name: CAMPI-NUMERICI
    private CampiNumerici campiNumerici = new CampiNumerici();
    //Original name: IDSS0140-LINK
    private Idss0140Link idss0140Link;

    //==== METHODS ====
    /**Original name: MAIN_SUBROUTINE<br>*/
    public long execute(Idss0140Link idss0140Link) {
        this.idss0140Link = idss0140Link;
        principale();
        fine();
        return 0;
    }

    public static Idss0140 getInstance() {
        return ((Idss0140)Programs.getInstance(Idss0140.class));
    }

    /**Original name: 000-PRINCIPALE<br>
	 * <pre>****************************************************************
	 *                                                                *
	 *                    000-PRINCIPALE                              *
	 *                                                                *
	 * ****************************************************************
	 * -- INIZIALIZZO IL FLAG PER L'ESITO DELL'ELABORAZIONE</pre>*/
    private void principale() {
        // COB_CODE: SET IDSV0141-SUCCESSFUL-RC   TO TRUE.
        idss0140Link.getReturnCode().setSuccessfulRc();
        // COB_CODE: INITIALIZE                      IDSV0141-CAMPO-DEST
        //                                           CAMPI-NUMERICI
        //                                           WS-CAMPI-APPOGGIO.
        idss0140Link.setCampoDest(new AfDecimal(0, 18, 7));
        initCampiNumerici();
        initWsCampiAppoggio();
        // COB_CODE: SET  IDSV0141-SUCCESSFUL-RC  TO TRUE
        idss0140Link.getReturnCode().setSuccessfulRc();
        // COB_CODE: IF IDSV0141-CAMPO-MITT(1:IDSV0141-LUNGHEZZA-DATO-STR)
        //              = HIGH-VALUE OR SPACES OR LOW-VALUE
        //              END-IF
        //           END-IF.
        if (Conditions.eq(idss0140Link.getCampoMittFormatted().substring((1) - 1, idss0140Link.getLunghezzaDatoStr()), LiteralGenerator.create(Types.HIGH_CHAR_VAL, idss0140Link.getLunghezzaDatoStr())) || Conditions.eq(idss0140Link.getCampoMittFormatted().substring((1) - 1, idss0140Link.getLunghezzaDatoStr()), "") || Conditions.eq(idss0140Link.getCampoMittFormatted().substring((1) - 1, idss0140Link.getLunghezzaDatoStr()), LiteralGenerator.create(Types.LOW_CHAR_VAL, idss0140Link.getLunghezzaDatoStr()))) {
            // COB_CODE: IF IDSV0141-MITT-ALFANUM
            //              MOVE ALL ZERO          TO IDSV0141-CAMPO-MITT
            //           ELSE
            //              SET IDSV0141-UNSUCCESSFUL-RC         TO TRUE
            //           END-IF
            if (idss0140Link.getTipoDatoMitt().isAlfanum()) {
                // COB_CODE: MOVE ALL ZERO          TO IDSV0141-CAMPO-MITT
                idss0140Link.setCampoMitt(LiteralGenerator.create('0', Idss0140Link.Len.CAMPO_MITT));
            }
            else {
                // COB_CODE: MOVE 'FIELD-NOT-VALUED' TO IDSV0141-DESCRIZ-ERR-DB2
                idss0140Link.setDescrizErrDb2("FIELD-NOT-VALUED");
                // COB_CODE: MOVE 'IDSS0140'         TO IDSV0141-COD-SERVIZIO-BE
                idss0140Link.setCodServizioBe("IDSS0140");
                // COB_CODE: SET IDSV0141-UNSUCCESSFUL-RC         TO TRUE
                idss0140Link.getReturnCode().setUnsuccessfulRc();
            }
        }
        //
        // COB_CODE: IF IDSV0141-SUCCESSFUL-RC
        //                 THRU 200-VALORIZZA-CAMPI-EX
        //           END-IF.
        if (idss0140Link.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: COMPUTE PARTE-INTERA-MITT = IDSV0141-LUNGHEZZA-DATO-MITT
            //                                    - IDSV0141-PRECISIONE-DATO-MITT
            wsCampiAppoggio.setParteInteraMitt(Trunc.toShort(abs(idss0140Link.getLunghezzaDatoMitt() - idss0140Link.getPrecisioneDatoMitt()), 3));
            //
            // COB_CODE: PERFORM 200-VALORIZZA-CAMPI
            //              THRU 200-VALORIZZA-CAMPI-EX
            valorizzaCampi();
        }
    }

    /**Original name: 999-FINE<br>*/
    private void fine() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    /**Original name: 200-VALORIZZA-CAMPI<br>
	 * <pre>-- TRASFORMAZIONE DA NAIS A LIFE</pre>*/
    private void valorizzaCampi() {
        // COB_CODE: MOVE 'IDSS0140'             TO IDSV0141-COD-SERVIZIO-BE
        idss0140Link.setCodServizioBe("IDSS0140");
        // COB_CODE: SET IDSV0141-UNSUCCESSFUL-RC              TO TRUE
        idss0140Link.getReturnCode().setUnsuccessfulRc();
        // COB_CODE: IF IDSV0141-MITT-COMP-3
        //              END-EVALUATE
        //           ELSE
        //              END-EVALUATE
        //           END-IF.
        if (idss0140Link.getTipoDatoMitt().isComp3()) {
            // COB_CODE: EVALUATE PARTE-INTERA-MITT
            //                WHEN 1
            //                   END-IF
            //               WHEN 2
            //                   END-IF
            //               WHEN 3
            //               WHEN 4
            //                   END-IF
            //               WHEN 5
            //                   END-IF
            //               WHEN 6
            //                   END-IF
            //               WHEN 7
            //                  END-IF
            //               WHEN 8
            //                   END-IF
            //               WHEN 9
            //                   END-IF
            //               WHEN 10
            //                   END-IF
            //               WHEN 12
            //                   END-IF
            //           END-EVALUATE
            switch (wsCampiAppoggio.getParteInteraMitt()) {

                case ((short)1):// COB_CODE: IF IDSV0141-PRECISIONE-DATO-MITT = ZERO
                    //                             TO TRUE
                    //           END-IF
                    if (idss0140Link.getPrecisioneDatoMitt() == 0) {
                        // COB_CODE: MOVE IDSV0141-CAMPO-MITT
                        //                          TO S-NOVE-DI-1-V-COMP
                        campiNumerici.setsNoveDi1VCompFormatted(idss0140Link.getCampoMittFormatted());
                        // COB_CODE: MOVE NUM-S-NOVE-DI-1-V-COMP
                        //                          TO IDSV0141-CAMPO-DEST
                        idss0140Link.setCampoDest(Trunc.toDecimal(campiNumerici.getNumSNoveDi1VComp(), 18, 7));
                        // COB_CODE: SET  IDSV0141-SUCCESSFUL-RC
                        //                          TO TRUE
                        idss0140Link.getReturnCode().setSuccessfulRc();
                    }
                    break;

                case ((short)2):// COB_CODE: IF IDSV0141-PRECISIONE-DATO-MITT = ZERO
                    //                             TO TRUE
                    //           END-IF
                    if (idss0140Link.getPrecisioneDatoMitt() == 0) {
                        // COB_CODE: MOVE IDSV0141-CAMPO-MITT
                        //                          TO S-NOVE-DI-2-V-COMP
                        campiNumerici.setsNoveDi2VCompFormatted(idss0140Link.getCampoMittFormatted());
                        // COB_CODE: MOVE NUM-S-NOVE-DI-2-V-COMP
                        //                          TO IDSV0141-CAMPO-DEST
                        idss0140Link.setCampoDest(Trunc.toDecimal(campiNumerici.getNumSNoveDi2VComp(), 18, 7));
                        // COB_CODE: SET  IDSV0141-SUCCESSFUL-RC
                        //                          TO TRUE
                        idss0140Link.getReturnCode().setSuccessfulRc();
                    }
                    break;

                case ((short)3):
                case ((short)4):// COB_CODE: IF IDSV0141-PRECISIONE-DATO-MITT = 3
                    //                             TO TRUE
                    //           END-IF
                    if (idss0140Link.getPrecisioneDatoMitt() == 3) {
                        // COB_CODE: MOVE IDSV0141-CAMPO-MITT
                        //             TO S-NOVE-DI-3-V3-COMP
                        campiNumerici.setsNoveDi3V3CompFormatted(idss0140Link.getCampoMittFormatted());
                        // COB_CODE: MOVE NUM-S-NOVE-DI-3-V3-COMP
                        //             TO IDSV0141-CAMPO-DEST
                        idss0140Link.setCampoDest(Trunc.toDecimal(campiNumerici.getNumSNoveDi3V3Comp(), 18, 7));
                        // COB_CODE: SET  IDSV0141-SUCCESSFUL-RC
                        //                          TO TRUE
                        idss0140Link.getReturnCode().setSuccessfulRc();
                    }
                    // COB_CODE: IF IDSV0141-PRECISIONE-DATO-MITT = 0
                    //                              TO TRUE
                    //           END-IF
                    if (idss0140Link.getPrecisioneDatoMitt() == 0) {
                        // COB_CODE: MOVE IDSV0141-CAMPO-MITT
                        //                          TO S-NOVE-DI-3-V-COMP
                        campiNumerici.setsNoveDi3VCompFormatted(idss0140Link.getCampoMittFormatted());
                        // COB_CODE: MOVE NUM-S-NOVE-DI-3-V-COMP
                        //                          TO IDSV0141-CAMPO-DEST
                        idss0140Link.setCampoDest(Trunc.toDecimal(campiNumerici.getNumSNoveDi3VComp(), 18, 7));
                        // COB_CODE: SET  IDSV0141-SUCCESSFUL-RC
                        //                           TO TRUE
                        idss0140Link.getReturnCode().setSuccessfulRc();
                    }
                    break;

                case ((short)5):// COB_CODE: IF IDSV0141-PRECISIONE-DATO-MITT = ZERO
                    //                             TO TRUE
                    //           END-IF
                    if (idss0140Link.getPrecisioneDatoMitt() == 0) {
                        // COB_CODE: MOVE IDSV0141-CAMPO-MITT
                        //             TO S-NOVE-DI-5-V-COMP
                        campiNumerici.setsNoveDi5VCompFormatted(idss0140Link.getCampoMittFormatted());
                        // COB_CODE: MOVE NUM-S-NOVE-DI-5-V-COMP
                        //             TO IDSV0141-CAMPO-DEST
                        idss0140Link.setCampoDest(Trunc.toDecimal(campiNumerici.getNumSNoveDi5VComp(), 18, 7));
                        // COB_CODE: SET  IDSV0141-SUCCESSFUL-RC
                        //                          TO TRUE
                        idss0140Link.getReturnCode().setSuccessfulRc();
                    }
                    // COB_CODE: IF IDSV0141-PRECISIONE-DATO-MITT = 9
                    //                             TO TRUE
                    //           END-IF
                    if (idss0140Link.getPrecisioneDatoMitt() == 9) {
                        // COB_CODE: MOVE IDSV0141-CAMPO-MITT
                        //             TO S-NOVE-DI-5-V9-COMP
                        campiNumerici.setsNoveDi5V9CompFormatted(idss0140Link.getCampoMittFormatted());
                        // COB_CODE: MOVE NUM-S-NOVE-DI-5-V9-COMP
                        //             TO IDSV0141-CAMPO-DEST
                        idss0140Link.setCampoDest(Trunc.toDecimal(campiNumerici.getNumSNoveDi5V9Comp(), 18, 7));
                        // COB_CODE: SET  IDSV0141-SUCCESSFUL-RC
                        //                          TO TRUE
                        idss0140Link.getReturnCode().setSuccessfulRc();
                    }
                    break;

                case ((short)6):// COB_CODE: IF IDSV0141-PRECISIONE-DATO-MITT = ZERO
                    //                             TO TRUE
                    //           END-IF
                    if (idss0140Link.getPrecisioneDatoMitt() == 0) {
                        // COB_CODE: MOVE IDSV0141-CAMPO-MITT
                        //             TO S-NOVE-DI-6-V-COMP
                        campiNumerici.setsNoveDi6VCompFormatted(idss0140Link.getCampoMittFormatted());
                        // COB_CODE: MOVE NUM-S-NOVE-DI-6-V-COMP
                        //                          TO IDSV0141-CAMPO-DEST
                        idss0140Link.setCampoDest(Trunc.toDecimal(campiNumerici.getNumSNoveDi6VComp(), 18, 7));
                        // COB_CODE: SET  IDSV0141-SUCCESSFUL-RC
                        //                          TO TRUE
                        idss0140Link.getReturnCode().setSuccessfulRc();
                    }
                    break;

                case ((short)7):// COB_CODE: IF IDSV0141-PRECISIONE-DATO-MITT = 5
                    //                             TO TRUE
                    //           END-IF
                    if (idss0140Link.getPrecisioneDatoMitt() == 5) {
                        // COB_CODE: MOVE IDSV0141-CAMPO-MITT
                        //             TO S-NOVE-DI-7-V5-COMP
                        campiNumerici.setsNoveDi7V5CompFormatted(idss0140Link.getCampoMittFormatted());
                        // COB_CODE: MOVE NUM-S-NOVE-DI-7-V5-COMP
                        //                          TO IDSV0141-CAMPO-DEST
                        idss0140Link.setCampoDest(Trunc.toDecimal(campiNumerici.getNumSNoveDi7V5Comp(), 18, 7));
                        // COB_CODE: SET  IDSV0141-SUCCESSFUL-RC
                        //                          TO TRUE
                        idss0140Link.getReturnCode().setSuccessfulRc();
                    }
                    break;

                case ((short)8):// COB_CODE: IF IDSV0141-PRECISIONE-DATO-MITT = ZERO
                    //                             TO TRUE
                    //           END-IF
                    if (idss0140Link.getPrecisioneDatoMitt() == 0) {
                        // COB_CODE: MOVE IDSV0141-CAMPO-MITT
                        //                          TO S-NOVE-DI-8-V-COMP
                        campiNumerici.setsNoveDi8VCompFormatted(idss0140Link.getCampoMittFormatted());
                        // COB_CODE: MOVE NUM-S-NOVE-DI-8-V-COMP
                        //                          TO IDSV0141-CAMPO-DEST
                        idss0140Link.setCampoDest(Trunc.toDecimal(campiNumerici.getNumSNoveDi8VComp(), 18, 7));
                        // COB_CODE: SET  IDSV0141-SUCCESSFUL-RC
                        //                          TO TRUE
                        idss0140Link.getReturnCode().setSuccessfulRc();
                    }
                    break;

                case ((short)9):// COB_CODE: IF IDSV0141-PRECISIONE-DATO-MITT = ZERO
                    //                             TO TRUE
                    //           END-IF
                    if (idss0140Link.getPrecisioneDatoMitt() == 0) {
                        // COB_CODE: MOVE IDSV0141-CAMPO-MITT
                        //                          TO S-NOVE-DI-9-V-COMP
                        campiNumerici.setsNoveDi9VCompFormatted(idss0140Link.getCampoMittFormatted());
                        // COB_CODE: MOVE NUM-S-NOVE-DI-9-V-COMP
                        //                          TO IDSV0141-CAMPO-DEST
                        idss0140Link.setCampoDest(Trunc.toDecimal(campiNumerici.getNumSNoveDi9VComp(), 18, 7));
                        // COB_CODE: SET IDSV0141-SUCCESSFUL-RC
                        //                          TO TRUE
                        idss0140Link.getReturnCode().setSuccessfulRc();
                    }
                    break;

                case ((short)10):// COB_CODE: IF IDSV0141-PRECISIONE-DATO-MITT = ZERO
                    //                             TO TRUE
                    //           END-IF
                    if (idss0140Link.getPrecisioneDatoMitt() == 0) {
                        // COB_CODE: MOVE IDSV0141-CAMPO-MITT
                        //                          TO S-NOVE-DI-10-V-COMP
                        campiNumerici.setsNoveDi10VCompFormatted(idss0140Link.getCampoMittFormatted());
                        // COB_CODE: MOVE NUM-S-NOVE-DI-10-V-COMP
                        //                          TO IDSV0141-CAMPO-DEST
                        idss0140Link.setCampoDest(Trunc.toDecimal(campiNumerici.getNumSNoveDi10VComp(), 18, 7));
                        // COB_CODE: SET  IDSV0141-SUCCESSFUL-RC
                        //                          TO TRUE
                        idss0140Link.getReturnCode().setSuccessfulRc();
                    }
                    break;

                case ((short)12):// COB_CODE: IF IDSV0141-PRECISIONE-DATO-MITT = 3
                    //                             TO TRUE
                    //           END-IF
                    if (idss0140Link.getPrecisioneDatoMitt() == 3) {
                        // COB_CODE: MOVE IDSV0141-CAMPO-MITT
                        //             TO S-NOVE-DI-12-V3-COMP
                        campiNumerici.setsNoveDi12V3CompFormatted(idss0140Link.getCampoMittFormatted());
                        // COB_CODE: MOVE NUM-S-NOVE-DI-12-V3-COMP
                        //             TO IDSV0141-CAMPO-DEST
                        idss0140Link.setCampoDest(Trunc.toDecimal(campiNumerici.getNumSNoveDi12V3Comp(), 18, 7));
                        // COB_CODE: SET  IDSV0141-SUCCESSFUL-RC
                        //                          TO TRUE
                        idss0140Link.getReturnCode().setSuccessfulRc();
                    }
                    break;

                default:break;
            }
        }
        else {
            // COB_CODE: EVALUATE PARTE-INTERA-MITT
            //               WHEN 1
            //                   END-IF
            //               WHEN 2
            //                   END-IF
            //               WHEN 3
            //                   END-EVALUATE
            //               WHEN 5
            //                   END-IF
            //               WHEN 6
            //                   END-IF
            //               WHEN 8
            //               WHEN 10
            //                   END-IF
            //               WHEN 9
            //                   END-IF
            //               WHEN 12
            //                   END-IF
            //           END-EVALUATE
            switch (wsCampiAppoggio.getParteInteraMitt()) {

                case ((short)1):// COB_CODE: IF IDSV0141-PRECISIONE-DATO-MITT = ZERO
                    //                             TO TRUE
                    //           END-IF
                    if (idss0140Link.getPrecisioneDatoMitt() == 0) {
                        // COB_CODE: MOVE IDSV0141-CAMPO-MITT
                        //                          TO S-NOVE-DI-1-V
                        campiNumerici.setsNoveDi1VFormatted(idss0140Link.getCampoMittFormatted());
                        // COB_CODE: MOVE NUM-S-NOVE-DI-1-V
                        //                          TO IDSV0141-CAMPO-DEST
                        idss0140Link.setCampoDest(Trunc.toDecimal(campiNumerici.getNumSNoveDi1V(), 18, 7));
                        // COB_CODE: SET  IDSV0141-SUCCESSFUL-RC
                        //                          TO TRUE
                        idss0140Link.getReturnCode().setSuccessfulRc();
                    }
                    break;

                case ((short)2):// COB_CODE: IF IDSV0141-PRECISIONE-DATO-MITT = ZERO
                    //                             TO TRUE
                    //           END-IF
                    if (idss0140Link.getPrecisioneDatoMitt() == 0) {
                        // COB_CODE: MOVE IDSV0141-CAMPO-MITT
                        //                          TO S-NOVE-DI-2-V
                        campiNumerici.setsNoveDi2VFormatted(idss0140Link.getCampoMittFormatted());
                        // COB_CODE: MOVE NUM-S-NOVE-DI-2-V
                        //                          TO IDSV0141-CAMPO-DEST
                        idss0140Link.setCampoDest(Trunc.toDecimal(campiNumerici.getNumSNoveDi2V(), 18, 7));
                        // COB_CODE: SET  IDSV0141-SUCCESSFUL-RC
                        //                          TO TRUE
                        idss0140Link.getReturnCode().setSuccessfulRc();
                    }
                    break;

                case ((short)3):// COB_CODE: EVALUATE IDSV0141-PRECISIONE-DATO-MITT
                    //            WHEN 3
                    //                             TO TRUE
                    //            WHEN 0
                    //                              TO TRUE
                    //           WHEN 7
                    //                             TO TRUE
                    //           END-EVALUATE
                    switch (idss0140Link.getPrecisioneDatoMitt()) {

                        case ((short)3):// COB_CODE: MOVE IDSV0141-CAMPO-MITT
                            //             TO S-NOVE-DI-3-V3
                            campiNumerici.setsNoveDi3V3Formatted(idss0140Link.getCampoMittFormatted());
                            // COB_CODE: MOVE NUM-S-NOVE-DI-3-V3
                            //             TO IDSV0141-CAMPO-DEST
                            idss0140Link.setCampoDest(Trunc.toDecimal(campiNumerici.getNumSNoveDi3V3(), 18, 7));
                            // COB_CODE: SET  IDSV0141-SUCCESSFUL-RC
                            //                          TO TRUE
                            idss0140Link.getReturnCode().setSuccessfulRc();
                            break;

                        case ((short)0):// COB_CODE: MOVE IDSV0141-CAMPO-MITT
                            //                          TO S-NOVE-DI-3-V
                            campiNumerici.setsNoveDi3VFormatted(idss0140Link.getCampoMittFormatted());
                            // COB_CODE: MOVE NUM-S-NOVE-DI-3-V-COMP
                            //                          TO IDSV0141-CAMPO-DEST
                            idss0140Link.setCampoDest(Trunc.toDecimal(campiNumerici.getNumSNoveDi3VComp(), 18, 7));
                            // COB_CODE: SET  IDSV0141-SUCCESSFUL-RC
                            //                           TO TRUE
                            idss0140Link.getReturnCode().setSuccessfulRc();
                            break;

                        case ((short)7):// COB_CODE: MOVE IDSV0141-CAMPO-MITT
                            //             TO S-NOVE-DI-3-V7
                            campiNumerici.setsNoveDi3V7Formatted(idss0140Link.getCampoMittFormatted());
                            // COB_CODE: MOVE NUM-S-NOVE-DI-3-V7
                            //             TO IDSV0141-CAMPO-DEST
                            idss0140Link.setCampoDest(Trunc.toDecimal(campiNumerici.getNumSNoveDi3V7(), 18, 7));
                            // COB_CODE: SET  IDSV0141-SUCCESSFUL-RC
                            //                          TO TRUE
                            idss0140Link.getReturnCode().setSuccessfulRc();
                            break;

                        default:break;
                    }
                    break;

                case ((short)5):// COB_CODE: IF IDSV0141-PRECISIONE-DATO-MITT = ZERO
                    //                             TO TRUE
                    //           END-IF
                    if (idss0140Link.getPrecisioneDatoMitt() == 0) {
                        // COB_CODE: MOVE IDSV0141-CAMPO-MITT
                        //             TO S-NOVE-DI-5-V
                        campiNumerici.setsNoveDi5VFormatted(idss0140Link.getCampoMittFormatted());
                        // COB_CODE: MOVE NUM-S-NOVE-DI-5-V
                        //             TO IDSV0141-CAMPO-DEST
                        idss0140Link.setCampoDest(Trunc.toDecimal(campiNumerici.getNumSNoveDi5V(), 18, 7));
                        // COB_CODE: SET  IDSV0141-SUCCESSFUL-RC
                        //                          TO TRUE
                        idss0140Link.getReturnCode().setSuccessfulRc();
                    }
                    // COB_CODE: IF IDSV0141-PRECISIONE-DATO-MITT = 9
                    //                             TO TRUE
                    //           END-IF
                    if (idss0140Link.getPrecisioneDatoMitt() == 9) {
                        // COB_CODE: MOVE IDSV0141-CAMPO-MITT
                        //             TO S-NOVE-DI-5-V9
                        campiNumerici.setsNoveDi5V9Formatted(idss0140Link.getCampoMittFormatted());
                        // COB_CODE: MOVE NUM-S-NOVE-DI-5-V9
                        //             TO IDSV0141-CAMPO-DEST
                        idss0140Link.setCampoDest(Trunc.toDecimal(campiNumerici.getNumSNoveDi5V9(), 18, 7));
                        // COB_CODE: SET  IDSV0141-SUCCESSFUL-RC
                        //                          TO TRUE
                        idss0140Link.getReturnCode().setSuccessfulRc();
                    }
                    break;

                case ((short)6):// COB_CODE: IF IDSV0141-PRECISIONE-DATO-MITT = ZERO
                    //                             TO TRUE
                    //           END-IF
                    if (idss0140Link.getPrecisioneDatoMitt() == 0) {
                        // COB_CODE: MOVE IDSV0141-CAMPO-MITT
                        //             TO S-NOVE-DI-6-V
                        campiNumerici.setsNoveDi6VFormatted(idss0140Link.getCampoMittFormatted());
                        // COB_CODE: MOVE NUM-S-NOVE-DI-6-V
                        //                          TO IDSV0141-CAMPO-DEST
                        idss0140Link.setCampoDest(Trunc.toDecimal(campiNumerici.getNumSNoveDi6V(), 18, 7));
                        // COB_CODE: SET  IDSV0141-SUCCESSFUL-RC
                        //                          TO TRUE
                        idss0140Link.getReturnCode().setSuccessfulRc();
                    }
                    break;

                case ((short)8):
                case ((short)10):// COB_CODE: IF IDSV0141-PRECISIONE-DATO-MITT = ZERO
                    //                             TO TRUE
                    //           END-IF
                    if (idss0140Link.getPrecisioneDatoMitt() == 0) {
                        // COB_CODE: MOVE IDSV0141-CAMPO-MITT
                        //                          TO S-NOVE-DI-8-V
                        campiNumerici.setsNoveDi8VFormatted(idss0140Link.getCampoMittFormatted());
                        // COB_CODE: MOVE NUM-S-NOVE-DI-8-V
                        //                          TO IDSV0141-CAMPO-DEST
                        idss0140Link.setCampoDest(Trunc.toDecimal(campiNumerici.getNumSNoveDi8V(), 18, 7));
                        // COB_CODE: SET  IDSV0141-SUCCESSFUL-RC
                        //                          TO TRUE
                        idss0140Link.getReturnCode().setSuccessfulRc();
                    }
                    break;

                case ((short)9):// COB_CODE: IF IDSV0141-PRECISIONE-DATO-MITT = ZERO
                    //                             TO TRUE
                    //           END-IF
                    if (idss0140Link.getPrecisioneDatoMitt() == 0) {
                        // COB_CODE: MOVE IDSV0141-CAMPO-MITT
                        //                          TO S-NOVE-DI-9-V
                        campiNumerici.setsNoveDi9VFormatted(idss0140Link.getCampoMittFormatted());
                        // COB_CODE: MOVE NUM-S-NOVE-DI-9-V
                        //                          TO IDSV0141-CAMPO-DEST
                        idss0140Link.setCampoDest(Trunc.toDecimal(campiNumerici.getNumSNoveDi9V(), 18, 7));
                        // COB_CODE: SET IDSV0141-SUCCESSFUL-RC
                        //                          TO TRUE
                        idss0140Link.getReturnCode().setSuccessfulRc();
                    }
                    break;

                case ((short)12):// COB_CODE: IF IDSV0141-PRECISIONE-DATO-MITT = 3
                    //                             TO TRUE
                    //           END-IF
                    if (idss0140Link.getPrecisioneDatoMitt() == 3) {
                        // COB_CODE: MOVE IDSV0141-CAMPO-MITT
                        //             TO S-NOVE-DI-12-V3
                        campiNumerici.setsNoveDi12V3Formatted(idss0140Link.getCampoMittFormatted());
                        // COB_CODE: MOVE NUM-S-NOVE-DI-12-V3
                        //             TO IDSV0141-CAMPO-DEST
                        idss0140Link.setCampoDest(Trunc.toDecimal(campiNumerici.getNumSNoveDi12V3(), 18, 7));
                        // COB_CODE: SET  IDSV0141-SUCCESSFUL-RC
                        //                          TO TRUE
                        idss0140Link.getReturnCode().setSuccessfulRc();
                    }
                    break;

                default:break;
            }
        }
    }

    public void initCampiNumerici() {
        campiNumerici.setChrXDi1(Types.SPACE_CHAR);
        campiNumerici.setChrXDi3("");
        campiNumerici.setChrXDi20("");
        campiNumerici.setChrXDi40("");
        campiNumerici.setNumNoveDi2Formatted("00");
        campiNumerici.setNumSNoveDi1VComp(((short)0));
        campiNumerici.setNumNoveDi2V2Comp(new AfDecimal(0, 4, 2));
        campiNumerici.setNumNoveDi2V3Comp(new AfDecimal(0, 5, 3));
        campiNumerici.setNumNoveDi3Comp(((short)0));
        campiNumerici.setNumNoveDi3V2Comp(new AfDecimal(0, 5, 2));
        campiNumerici.setNumNoveDi3V3Comp(new AfDecimal(0, 6, 3));
        campiNumerici.setNumNoveDi5Comp(0);
        campiNumerici.setNumNoveDi5V3Comp(new AfDecimal(0, 8, 3));
        campiNumerici.setNumNoveDi7V3Comp(new AfDecimal(0, 10, 3));
        campiNumerici.setNumNoveDi7Comp(0);
        campiNumerici.setNumNoveDi8V3Comp(new AfDecimal(0, 11, 3));
        campiNumerici.setNumNoveDi9Comp(0);
        campiNumerici.setNumNoveDi9V3Comp(new AfDecimal(0, 12, 3));
        campiNumerici.setNumNoveDi11V3Comp(new AfDecimal(0, 14, 3));
        campiNumerici.setNumSNoveDi2VComp(((short)0));
        campiNumerici.setNumSNoveDi2V2Comp(new AfDecimal(0, 4, 2));
        campiNumerici.setNumSDi2V3Comp(new AfDecimal(0, 5, 3));
        campiNumerici.setNumSNoveDi3VComp(((short)0));
        campiNumerici.setNumSNoveDi3V2Comp(new AfDecimal(0, 5, 2));
        campiNumerici.setNumSNoveDi3V3Comp(new AfDecimal(0, 6, 3));
        campiNumerici.setNumSNoveDi4VComp(((short)0));
        campiNumerici.setNumSNoveDi5VComp(0);
        campiNumerici.setNumSNoveDi5V9Comp(new AfDecimal(0, 14, 9));
        campiNumerici.setNumSNoveDi6VComp(0);
        campiNumerici.setNumSNoveDi5V3Comp(new AfDecimal(0, 8, 3));
        campiNumerici.setNumSNoveDi7V3Comp(new AfDecimal(0, 10, 3));
        campiNumerici.setNumSNoveDi7V5Comp(new AfDecimal(0, 12, 5));
        campiNumerici.setNumSNoveDi7Comp(0);
        campiNumerici.setNumSNoveDi8V3Comp(new AfDecimal(0, 11, 3));
        campiNumerici.setNumSNoveDi8VComp(0);
        campiNumerici.setNumSNoveDi10VComp(0);
        campiNumerici.setNumSNoveDi9VComp(0);
        campiNumerici.setNumSNoveDi11V3Comp(new AfDecimal(0, 14, 3));
        campiNumerici.setNumSNoveDi12V3Comp(new AfDecimal(0, 15, 3));
        campiNumerici.setNumSNoveDi1V(((short)0));
        campiNumerici.setNumSNoveDi2V(((short)0));
        campiNumerici.setNumSNoveDi2V2(new AfDecimal(0, 4, 2));
        campiNumerici.setNumSNoveDi3V(((short)0));
        campiNumerici.setNumSNoveDi3V21(new AfDecimal(0, 5, 2));
        campiNumerici.setNumSNoveDi5V(0);
        campiNumerici.setNumSNoveDi5V9(new AfDecimal(0, 14, 9));
        campiNumerici.setNumSNoveDi3V22(new AfDecimal(0, 5, 2));
        campiNumerici.setNumSNoveDi3V3(new AfDecimal(0, 6, 3));
        campiNumerici.setNumSNoveDi6V(0);
        campiNumerici.setNumSNoveDi8V(0);
        campiNumerici.setNumSNoveDi9V(0);
        campiNumerici.setNumSNoveDi8V3(new AfDecimal(0, 11, 3));
        campiNumerici.setNumSNoveDi11Comp(0);
        campiNumerici.setNumSNoveDi12V3(new AfDecimal(0, 15, 3));
        campiNumerici.setNumSNoveDi3V7(new AfDecimal(0, 10, 7));
    }

    public void initWsCampiAppoggio() {
        wsCampiAppoggio.setParteInteraMittFormatted("000");
        wsCampiAppoggio.setParteInteraDestFormatted("000");
        wsCampiAppoggio.setWsNumDi1Formatted("0");
        wsCampiAppoggio.setWsNumDi3Formatted("000");
        wsCampiAppoggio.setWsNumDi5Formatted("00000");
        wsCampiAppoggio.setWsNumDi8Formatted("00000000");
        wsCampiAppoggio.setWsNumDi9Formatted("000000000");
    }
}
