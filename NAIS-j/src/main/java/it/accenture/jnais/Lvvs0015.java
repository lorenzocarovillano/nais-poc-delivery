package it.accenture.jnais;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Idsv0003CampiEsito;
import it.accenture.jnais.copy.Lvvc0000DatiInput2;
import it.accenture.jnais.ws.enums.Idsv0003Sqlcode;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.InputLvvs0000;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.Lvvs0015Data;

/**Original name: LVVS0015<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2007.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *   PROGRAMMA...... LVVS0015
 *   TIPOLOGIA...... SERVIZIO
 *   PROCESSO....... XXX
 *   FUNZIONE....... XXX
 *   DESCRIZIONE.... CONVERSIONE DATA NASCITA RAPP-ANA
 * **------------------------------------------------------------***</pre>*/
public class Lvvs0015 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lvvs0015Data ws = new Lvvs0015Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS0015
    private Ivvc0213 ivvc0213;

    //==== METHODS ====
    /**Original name: PROGRAM_LVVS0015_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: PERFORM S1000-ELABORAZIONE
        //              THRU EX-S1000
        s1000Elaborazione();
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lvvs0015 getInstance() {
        return ((Lvvs0015)Programs.getInstance(Lvvs0015.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                        IX-INDICI
        //                                             IVVC0213-TAB-OUTPUT.
        initIxIndici();
        initTabOutput();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE
        //             TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1000Elaborazione() {
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE AREA-IO-RAPP-ANA
        //                      WK-DATA-OUTPUT
        //                      WK-DATA-X-12.
        initAreaIoRappAna();
        ws.setWkDataOutput(new AfDecimal(0, 11, 7));
        initWkDataX12();
        //--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
        //--> RISPETTIVE AREE DCLGEN IN WORKING
        // COB_CODE: PERFORM S1100-VALORIZZA-DCLGEN
        //              THRU S1100-VALORIZZA-DCLGEN-EX
        //           VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
        //                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //                   SPACES OR LOW-VALUE OR HIGH-VALUE
        ws.setIxDclgen(((short)1));
        while (!(ws.getIxDclgen() > ivvc0213.getEleInfoMax() || Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias()) || Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()) || Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()))) {
            s1100ValorizzaDclgen();
            ws.setIxDclgen(Trunc.toShort(ws.getIxDclgen() + 1, 4));
        }
        //--> PERFORM DI CONTROLLI SUI I CAMPI CARICATI NELLE
        //--> DCLGEN DI WORKING
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //                  THRU S1200-CONTROLLO-DATI-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM S1200-CONTROLLO-DATI
            //              THRU S1200-CONTROLLO-DATI-EX
            s1200ControlloDati();
        }
        // COB_CODE:      IF  IDSV0003-SUCCESSFUL-RC
        //                AND IDSV0003-SUCCESSFUL-SQL
        //           *--> CALL MODULO PER RECUPERO DATA
        //                    END-IF
        //                END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            //--> CALL MODULO PER RECUPERO DATA
            // COB_CODE: PERFORM S1300-CALL-LVVS0000
            //              THRU S1300-CALL-LVVS0000-EX
            s1300CallLvvs0000();
            // COB_CODE:          IF IDSV0003-SUCCESSFUL-RC
            //                      END-EVALUATE
            //                    ELSE
            //           *-->       GESTIRE ERRORE DISPATCHER
            //                      END-STRING
            //                    END-IF
            if (idsv0003.getReturnCode().isSuccessfulRc()) {
                // COB_CODE:           EVALUATE TRUE
                //                         WHEN IDSV0003-SUCCESSFUL-SQL
                //           *-->          OPERAZIONE ESEGUITA CORRETTAMENTE
                //                                TO IVVC0213-VAL-IMP-O
                //                         WHEN OTHER
                //           *--->         ERRORE DI ACCESSO AL DB
                //                              END-STRING
                //                      END-EVALUATE
                switch (idsv0003.getSqlcode().getSqlcode()) {

                    case Idsv0003Sqlcode.SUCCESSFUL_SQL://-->          OPERAZIONE ESEGUITA CORRETTAMENTE
                        // COB_CODE: MOVE LVVC0000-DATA-OUTPUT
                        //             TO IVVC0213-VAL-IMP-O
                        ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(ws.getInputLvvs0000().getDataOutput(), 18, 7));
                        break;

                    default://--->         ERRORE DI ACCESSO AL DB
                        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
                        idsv0003.getReturnCode().setInvalidOper();
                        // COB_CODE: MOVE WK-PGM
                        //             TO IDSV0003-COD-SERVIZIO-BE
                        idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                        // COB_CODE: STRING IDSV0003-RETURN-CODE  ';'
                        //                  IDSV0003-SQLCODE
                        //           DELIMITED BY SIZE
                        //           INTO IDSV0003-DESCRIZ-ERR-DB2
                        //           END-STRING
                        concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                        idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                        break;
                }
            }
            else {
                //-->       GESTIRE ERRORE DISPATCHER
                // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
                // COB_CODE: MOVE WK-PGM
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: STRING IDSV0003-RETURN-CODE  ';'
                //                  IDSV0003-SQLCODE
                //           DELIMITED BY SIZE
                //           INTO IDSV0003-DESCRIZ-ERR-DB2
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
            }
        }
    }

    /**Original name: S1100-VALORIZZA-DCLGEN<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 *     RISPETTIVE AREE DCLGEN IN WORKING
	 * ----------------------------------------------------------------*</pre>*/
    private void s1100ValorizzaDclgen() {
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-RAPP-ANAG
        //                TO DRAN-AREA-RAPP-ANAG
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias(), ws.getIvvc0218().getAliasRappAnag())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO DRAN-AREA-RAPP-ANAG
            ws.setDranAreaRappAnagFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxDclgen()).getLunghezza() - 1));
        }
    }

    /**Original name: S1200-CONTROLLO-DATI<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLO DATI DCLGEN
	 * ----------------------------------------------------------------*</pre>*/
    private void s1200ControlloDati() {
        // COB_CODE: IF DRAN-DT-NASC(IVVC0213-IX-TABB) NOT NUMERIC
        //                TO IDSV0003-DESCRIZ-ERR-DB2
        //           ELSE
        //              END-IF
        //           END-IF.
        if (!Functions.isNumber(ws.getDranTabRappAnag(ivvc0213.getIxTabb()).getLccvran1().getDati().getWranDtNasc().getWranDtNasc())) {
            // COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
            idsv0003.getReturnCode().setFieldNotValued();
            // COB_CODE: MOVE WK-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'DATA-NASCITA-RAPP-ANA NON NUMERICA'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("DATA-NASCITA-RAPP-ANA NON NUMERICA");
        }
        else if (ws.getDranTabRappAnag(ivvc0213.getIxTabb()).getLccvran1().getDati().getWranDtNasc().getWranDtNasc() == 0) {
            // COB_CODE: IF DRAN-DT-NASC(IVVC0213-IX-TABB) = 0
            //                TO IDSV0003-DESCRIZ-ERR-DB2
            //           END-IF
            // COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
            idsv0003.getReturnCode().setFieldNotValued();
            // COB_CODE: MOVE WK-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'DATA-DATA-NASCITA-RAPP-ANA NON VALORIZZATA'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("DATA-DATA-NASCITA-RAPP-ANA NON VALORIZZATA");
        }
    }

    /**Original name: S1300-CALL-LVVS0000<br>
	 * <pre>----------------------------------------------------------------*
	 *    CHIAMATA LVVS0000
	 * ----------------------------------------------------------------*</pre>*/
    private void s1300CallLvvs0000() {
        Lvvs0000 lvvs0000 = null;
        // COB_CODE: INITIALIZE INPUT-LVVS0000.
        initInputLvvs0000();
        // COB_CODE: SET LVVC0000-AAA-V-GGG         TO TRUE.
        ws.getInputLvvs0000().getFormatDate().setAaaVGgg();
        // COB_CODE: MOVE DRAN-DT-NASC(IVVC0213-IX-TABB)
        //                                          TO LVVC0000-DATA-INPUT-1.
        ws.getInputLvvs0000().setDataInput1(TruncAbs.toInt(ws.getDranTabRappAnag(ivvc0213.getIxTabb()).getLccvran1().getDati().getWranDtNasc().getWranDtNasc(), 8));
        //--> INIZIALIZZA CODICE DI RITORNO
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC     TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL    TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        //
        // COB_CODE: MOVE 'LVVS0000'                TO WK-CALL-PGM.
        ws.setWkCallPgm("LVVS0000");
        //
        // COB_CODE: CALL WK-CALL-PGM USING         IDSV0003
        //                                          INPUT-LVVS0000.
        lvvs0000 = Lvvs0000.getInstance();
        lvvs0000.run(idsv0003, ws.getInputLvvs0000());
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: MOVE SPACES                     TO IVVC0213-VAL-STR-O.
        ivvc0213.getTabOutput().setValStrO("");
        // COB_CODE: MOVE 0                          TO IVVC0213-VAL-PERC-O.
        ivvc0213.getTabOutput().setValPercO(Trunc.toDecimal(0, 14, 9));
        //
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    public void initIxIndici() {
        ws.setIxDclgen(((short)0));
        ws.setIxTabRan(((short)0));
    }

    public void initTabOutput() {
        ivvc0213.getTabOutput().setCodVariabileO("");
        ivvc0213.getTabOutput().setTpDatoO(Types.SPACE_CHAR);
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        ivvc0213.getTabOutput().setValPercO(new AfDecimal(0, 14, 9));
        ivvc0213.getTabOutput().setValStrO("");
    }

    public void initAreaIoRappAna() {
        ws.setDranEleRappAnagMax(((short)0));
        for (int idx0 = 1; idx0 <= Lvvs0015Data.DRAN_TAB_RAPP_ANAG_MAXOCCURS; idx0++) {
            ws.getDranTabRappAnag(idx0).getLccvran1().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getDranTabRappAnag(idx0).getLccvran1().setIdPtf(0);
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranIdRappAna(0);
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().getWranIdRappAnaCollg().setWranIdRappAnaCollg(0);
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranIdOgg(0);
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranTpOgg("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranIdMoviCrz(0);
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().getWranIdMoviChiu().setWranIdMoviChiu(0);
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranDtIniEff(0);
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranDtEndEff(0);
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranCodCompAnia(0);
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranCodSogg("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranTpRappAna("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranTpPers(Types.SPACE_CHAR);
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranSex(Types.SPACE_CHAR);
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().getWranDtNasc().setWranDtNasc(0);
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranFlEstas(Types.SPACE_CHAR);
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranIndir1("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranIndir2("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranIndir3("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranTpUtlzIndir1("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranTpUtlzIndir2("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranTpUtlzIndir3("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranEstrCntCorrAccr("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranEstrCntCorrAdd("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranEstrDocto("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().getWranPcNelRapp().setWranPcNelRapp(new AfDecimal(0, 6, 3));
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranTpMezPagAdd("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranTpMezPagAccr("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranCodMatr("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranTpAdegz("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranFlTstRsh(Types.SPACE_CHAR);
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranCodAz("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranIndPrinc("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().getWranDtDeliberaCda().setWranDtDeliberaCda(0);
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranDlgAlRisc(Types.SPACE_CHAR);
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranLegaleRapprPrinc(Types.SPACE_CHAR);
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranTpLegaleRappr("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranTpIndPrinc("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranTpStatRid("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranNomeIntRid("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranCognIntRid("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranCognIntTratt("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranNomeIntTratt("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranCfIntRid("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranFlCoincDipCntr(Types.SPACE_CHAR);
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranFlCoincIntCntr(Types.SPACE_CHAR);
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().getWranDtDeces().setWranDtDeces(0);
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranFlFumatore(Types.SPACE_CHAR);
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranDsRiga(0);
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranDsOperSql(Types.SPACE_CHAR);
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranDsVer(0);
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranDsTsIniCptz(0);
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranDsTsEndCptz(0);
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranDsUtente("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranDsStatoElab(Types.SPACE_CHAR);
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranFlLavDip(Types.SPACE_CHAR);
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranTpVarzPagat("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranCodRid("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranTpCausRid("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranIndMassaCorp("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranCatRshProf("");
        }
    }

    public void initWkDataX12() {
        ws.getWkDataX12().setxAa("");
        ws.getWkDataX12().setVirogla(Types.SPACE_CHAR);
        ws.getWkDataX12().setxGg("");
    }

    public void initInputLvvs0000() {
        ws.getInputLvvs0000().getFormatDate().setFormatDate(Types.SPACE_CHAR);
        ws.getInputLvvs0000().setDataInputFormatted("00000000");
        ws.getInputLvvs0000().setDataInput1Formatted("00000000");
        ws.getInputLvvs0000().getDatiInput2().setLvvc0000AnniInput2Formatted("00000");
        ws.getInputLvvs0000().getDatiInput2().setLvvc0000MesiInput2Formatted("00000");
        ws.getInputLvvs0000().getDatiInput2().setLvvc0000GiorniInput2Formatted("00000");
        ws.getInputLvvs0000().setAnniInput3Formatted("00000");
        ws.getInputLvvs0000().setMesiInput3Formatted("00000");
        ws.getInputLvvs0000().setDataOutput(new AfDecimal(0, 11, 7));
    }
}
