package it.accenture.jnais;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Idsv0003CampiEsito;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.Lvvs0052Data;

/**Original name: LVVS0052<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2007.
 * DATE-COMPILED.
 * **------------------------------------------------------------***</pre>*/
public class Lvvs0052 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lvvs0052Data ws = new Lvvs0052Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS0007
    private Ivvc0213 ivvc0213;

    //==== METHODS ====
    /**Original name: PROGRAM_LVVS0052_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        //
        // COB_CODE: PERFORM S1000-ELABORAZIONE
        //              THRU EX-S1000
        s1000Elaborazione();
        //
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lvvs0052 getInstance() {
        return ((Lvvs0052)Programs.getInstance(Lvvs0052.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                        IX-INDICI
        //                                             IVVC0213-TAB-OUTPUT.
        initIxIndici();
        initTabOutput();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: INITIALIZE LDBV1701.
        initLdbv1701();
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE
        //             TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
        // COB_CODE: MOVE IVVC0213-WHERE-COND(1:2)     TO WK-TP-OGG.
        ws.setWkTpOgg(ivvc0213.getWhereCondFormatted().substring((1) - 1, 2));
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*
	 * --> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 * --> RISPETTIVE AREE DCLGEN IN WORKING</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: PERFORM S1100-VALORIZZA-DCLGEN
        //              THRU S1100-VALORIZZA-DCLGEN-EX
        //           VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
        //                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //                   SPACES OR LOW-VALUE OR HIGH-VALUE.
        ws.setIxDclgen(((short)1));
        while (!(ws.getIxDclgen() > ivvc0213.getEleInfoMax() || Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias()) || Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()) || Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()))) {
            s1100ValorizzaDclgen();
            ws.setIxDclgen(Trunc.toShort(ws.getIxDclgen() + 1, 4));
        }
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //                  THRU S1200-CONTROLLO-DATI-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM S1200-CONTROLLO-DATI
            //              THRU S1200-CONTROLLO-DATI-EX
            s1200ControlloDati();
        }
        //
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //               END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: IF (WK-TP-OGG = 'PO' AND IVVC0213-ID-POL-AUTENTICO) OR
            //              (WK-TP-OGG = 'AD' AND IVVC0213-ID-ADE-AUTENTICO) OR
            //              (WK-TP-OGG = 'GA' AND IVVC0213-ID-GRZ-AUTENTICO) OR
            //              (WK-TP-OGG = 'TG' AND IVVC0213-ID-TGA-AUTENTICO)
            //              PERFORM S1250-CALCOLA-DATA1   THRU S1250-EX
            //           ELSE
            //              MOVE 'TR'                     TO IVVC0213-VAL-STR-O
            //           END-IF
            if (Conditions.eq(ws.getWkTpOgg(), "PO") && ivvc0213.getModalitaIdPol().isIvvc0213IdPolAutentico() || Conditions.eq(ws.getWkTpOgg(), "AD") && ivvc0213.getModalitaIdAde().isIvvc0213IdAdeAutentico() || Conditions.eq(ws.getWkTpOgg(), "GA") && ivvc0213.getModalitaIdGrz().isIvvc0213IdGrzAutentico() || Conditions.eq(ws.getWkTpOgg(), "TG") && ivvc0213.getModalitaIdTga().isIvvc0213IdTgaAutentico()) {
                // COB_CODE: EVALUATE WK-TP-OGG
                //             WHEN 'PO'
                //               MOVE WK-TP-OGG            TO LDBV1701-TP-OGG
                //             WHEN 'AD'
                //               MOVE WK-TP-OGG            TO LDBV1701-TP-OGG
                //             WHEN 'GA'
                //               MOVE WK-TP-OGG            TO LDBV1701-TP-OGG
                //             WHEN 'TG'
                //               MOVE WK-TP-OGG            TO LDBV1701-TP-OGG
                //           END-EVALUATE
                switch (ws.getWkTpOgg()) {

                    case "PO":// COB_CODE: MOVE IVVC0213-ID-POLIZZA  TO LDBV1701-ID-OGG
                        ws.getLdbv1701().setIdOgg(ivvc0213.getIdPolizza());
                        // COB_CODE: MOVE WK-TP-OGG            TO LDBV1701-TP-OGG
                        ws.getLdbv1701().setTpOgg(ws.getWkTpOgg());
                        break;

                    case "AD":// COB_CODE: MOVE IVVC0213-ID-ADESIONE TO LDBV1701-ID-OGG
                        ws.getLdbv1701().setIdOgg(ivvc0213.getIdAdesione());
                        // COB_CODE: MOVE WK-TP-OGG            TO LDBV1701-TP-OGG
                        ws.getLdbv1701().setTpOgg(ws.getWkTpOgg());
                        break;

                    case "GA":// COB_CODE: MOVE IVVC0213-ID-GARANZIA TO LDBV1701-ID-OGG
                        ws.getLdbv1701().setIdOgg(ivvc0213.getIdGaranzia());
                        // COB_CODE: MOVE WK-TP-OGG            TO LDBV1701-TP-OGG
                        ws.getLdbv1701().setTpOgg(ws.getWkTpOgg());
                        break;

                    case "TG":// COB_CODE: MOVE IVVC0213-ID-TRANCHE  TO LDBV1701-ID-OGG
                        ws.getLdbv1701().setIdOgg(ivvc0213.getIdTranche());
                        // COB_CODE: MOVE WK-TP-OGG            TO LDBV1701-TP-OGG
                        ws.getLdbv1701().setTpOgg(ws.getWkTpOgg());
                        break;

                    default:break;
                }
                // COB_CODE: PERFORM S1250-CALCOLA-DATA1   THRU S1250-EX
                s1250CalcolaData1();
            }
            else {
                // COB_CODE: MOVE 'TR'                     TO IVVC0213-VAL-STR-O
                ivvc0213.getTabOutput().setValStrO("TR");
            }
        }
    }

    /**Original name: S1100-VALORIZZA-DCLGEN<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 *     RISPETTIVE AREE DCLGEN IN WORKING
	 * ----------------------------------------------------------------*</pre>*/
    private void s1100ValorizzaDclgen() {
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-POLI
        //                TO DPOL-AREA-POL
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias(), ws.getIvvc0218().getAliasPoli())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO DPOL-AREA-POL
            ws.setDpolAreaPolFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxDclgen()).getLunghezza() - 1));
        }
        //
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-TRCH-GAR
        //                TO DTGA-AREA-TGA
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias(), ws.getIvvc0218().getAliasTrchGar())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO DTGA-AREA-TGA
            ws.setDtgaAreaTgaFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxDclgen()).getLunghezza() - 1));
        }
        //
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-GARANZIA
        //                TO DGRZ-AREA-GRA
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias(), ws.getIvvc0218().getAliasGaranzia())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO DGRZ-AREA-GRA
            ws.setDgrzAreaGraFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxDclgen()).getLunghezza() - 1));
        }
        //
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-ADES
        //                TO DADE-AREA-ADES
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias(), ws.getIvvc0218().getAliasAdes())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO DADE-AREA-ADES
            ws.setDadeAreaAdesFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxDclgen()).getLunghezza() - 1));
        }
    }

    /**Original name: S1250-CALCOLA-DATA1<br>
	 * <pre>----------------------------------------------------------------*
	 *   CALCOLA DATA 1
	 * ----------------------------------------------------------------*</pre>*/
    private void s1250CalcolaData1() {
        Ldbs1700 ldbs1700 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: SET IDSV0003-WHERE-CONDITION        TO TRUE.
        idsv0003.getLivelloOperazione().setWhereCondition();
        //
        // COB_CODE: MOVE 'LDBS1700'                     TO WK-CALL-PGM.
        ws.setWkCallPgm("LDBS1700");
        //
        //
        // COB_CODE:      CALL WK-CALL-PGM  USING  IDSV0003 LDBV1701
        //           *
        //                ON EXCEPTION
        //                     SET IDSV0003-INVALID-OPER  TO TRUE
        //                END-CALL.
        try {
            ldbs1700 = Ldbs1700.getInstance();
            ldbs1700.run(idsv0003, ws.getLdbv1701());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-CALL-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: MOVE 'CALL-LDBS1700 ERRORE CHIAMATA - T0000-TRATTA-MATRICE'
            //              TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBS1700 ERRORE CHIAMATA - T0000-TRATTA-MATRICE");
            // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              MOVE LDBV1701-TP-STAT        TO IVVC0213-VAL-STR-O
        //           ELSE
        //              END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: MOVE LDBV1701-TP-STAT        TO IVVC0213-VAL-STR-O
            ivvc0213.getTabOutput().setValStrO(ws.getLdbv1701().getTpStat());
        }
        else {
            // COB_CODE: MOVE WK-CALL-PGM             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: STRING 'CHIAMATA LDBS1700 ;'
            //                  IDSV0003-RETURN-CODE ';'
            //                  IDSV0003-SQLCODE
            //              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LDBS1700 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
            //           ELSE
            //              SET IDSV0003-INVALID-OPER            TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isNotFound()) {
                // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-OPER            TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
        }
    }

    /**Original name: S1200-CONTROLLO-DATI<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLO DATI DCLGEN
	 * ----------------------------------------------------------------*
	 *     MOVE DPOL-TP-FRM-ASSVA            TO WS-TP-FRM-ASSVA</pre>*/
    private void s1200ControlloDati() {
        // COB_CODE: IF WK-TP-OGG = 'GA' OR 'TG'
        //              END-IF
        //           END-IF.
        if (Conditions.eq(ws.getWkTpOgg(), "GA") || Conditions.eq(ws.getWkTpOgg(), "TG")) {
            // COB_CODE: IF DTGA-ELE-TGA-MAX GREATER ZERO
            //              END-IF
            //           END-IF
            if (ws.getDtgaEleTgaMax() > 0) {
                // COB_CODE: IF IVVC0213-ID-TRANCHE IS NUMERIC
                //              END-IF
                //           ELSE
                //                TO IDSV0003-DESCRIZ-ERR-DB2
                //           END-IF
                if (Functions.isNumber(ivvc0213.getIdTrancheFormatted())) {
                    // COB_CODE: IF IVVC0213-ID-TRANCHE = 0
                    //               TO IDSV0003-DESCRIZ-ERR-DB2
                    //           END-IF
                    if (ivvc0213.getIdTranche() == 0) {
                        // COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
                        idsv0003.getReturnCode().setFieldNotValued();
                        // COB_CODE: MOVE WK-PGM
                        //             TO IDSV0003-COD-SERVIZIO-BE
                        idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                        // COB_CODE: MOVE 'ID-POLIZZA NON VALORIZZATO'
                        //             TO IDSV0003-DESCRIZ-ERR-DB2
                        idsv0003.getCampiEsito().setDescrizErrDb2("ID-POLIZZA NON VALORIZZATO");
                    }
                }
                else {
                    // COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
                    idsv0003.getReturnCode().setFieldNotValued();
                    // COB_CODE: MOVE WK-PGM
                    //             TO IDSV0003-COD-SERVIZIO-BE
                    idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'ID-TRCH NON VALORIZZATO'
                    //             TO IDSV0003-DESCRIZ-ERR-DB2
                    idsv0003.getCampiEsito().setDescrizErrDb2("ID-TRCH NON VALORIZZATO");
                }
            }
        }
        // COB_CODE: IF WK-TP-OGG = 'PO'
        //              END-IF
        //           END-IF.
        if (Conditions.eq(ws.getWkTpOgg(), "PO")) {
            // COB_CODE: IF IVVC0213-ID-POLIZZA IS NUMERIC
            //              END-IF
            //           ELSE
            //                TO IDSV0003-DESCRIZ-ERR-DB2
            //           END-IF
            if (Functions.isNumber(ivvc0213.getIdPolizzaFormatted())) {
                // COB_CODE: IF IVVC0213-ID-POLIZZA = 0
                //               TO IDSV0003-DESCRIZ-ERR-DB2
                //           END-IF
                if (ivvc0213.getIdPolizza() == 0) {
                    // COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
                    idsv0003.getReturnCode().setFieldNotValued();
                    // COB_CODE: MOVE WK-PGM
                    //             TO IDSV0003-COD-SERVIZIO-BE
                    idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'ID-POLIZZA NON VALORIZZATO'
                    //             TO IDSV0003-DESCRIZ-ERR-DB2
                    idsv0003.getCampiEsito().setDescrizErrDb2("ID-POLIZZA NON VALORIZZATO");
                }
            }
            else {
                // COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
                // COB_CODE: MOVE WK-PGM
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE 'ID-POLIZZA NON VALORIZZATO'
                //             TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("ID-POLIZZA NON VALORIZZATO");
            }
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    public void initIxIndici() {
        ws.setIxDclgen(((short)0));
    }

    public void initTabOutput() {
        ivvc0213.getTabOutput().setCodVariabileO("");
        ivvc0213.getTabOutput().setTpDatoO(Types.SPACE_CHAR);
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        ivvc0213.getTabOutput().setValPercO(new AfDecimal(0, 14, 9));
        ivvc0213.getTabOutput().setValStrO("");
    }

    public void initLdbv1701() {
        ws.getLdbv1701().setIdOgg(0);
        ws.getLdbv1701().setTpOgg("");
        ws.getLdbv1701().setTpCaus("");
        ws.getLdbv1701().setTpStat("");
    }
}
