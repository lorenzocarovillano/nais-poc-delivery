package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.QuestAdegVerDao;
import it.accenture.jnais.commons.data.to.IQuestAdegVer;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idbsp560Data;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.QuestAdegVer;
import it.accenture.jnais.ws.redefines.P56CodCan;
import it.accenture.jnais.ws.redefines.P56DtIniFntRedd;
import it.accenture.jnais.ws.redefines.P56DtIniFntRedd2;
import it.accenture.jnais.ws.redefines.P56DtIniFntRedd3;
import it.accenture.jnais.ws.redefines.P56IdAssicurati;
import it.accenture.jnais.ws.redefines.P56IdRappAna;
import it.accenture.jnais.ws.redefines.P56ImpAfi;
import it.accenture.jnais.ws.redefines.P56ImpPaEspMsc1;
import it.accenture.jnais.ws.redefines.P56ImpPaEspMsc2;
import it.accenture.jnais.ws.redefines.P56ImpPaEspMsc3;
import it.accenture.jnais.ws.redefines.P56ImpPaEspMsc4;
import it.accenture.jnais.ws.redefines.P56ImpPaEspMsc5;
import it.accenture.jnais.ws.redefines.P56ImpTotAffAcc;
import it.accenture.jnais.ws.redefines.P56ImpTotAffUtil;
import it.accenture.jnais.ws.redefines.P56ImpTotFinAcc;
import it.accenture.jnais.ws.redefines.P56ImpTotFinUtil;
import it.accenture.jnais.ws.redefines.P56NumDip;
import it.accenture.jnais.ws.redefines.P56PcEspAgPaMsc;
import it.accenture.jnais.ws.redefines.P56PcQuoDetTitEff;
import it.accenture.jnais.ws.redefines.P56PcRipPatAsVita;
import it.accenture.jnais.ws.redefines.P56PcRipPatIm;
import it.accenture.jnais.ws.redefines.P56PcRipPatSetIm;
import it.accenture.jnais.ws.redefines.P56ReddCon;
import it.accenture.jnais.ws.redefines.P56ReddFattAnnu;
import it.accenture.jnais.ws.redefines.P56TpMovi;
import it.accenture.jnais.ws.redefines.P56UltFattAnnu;

/**Original name: IDBSP560<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  26 MAR 2019.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Idbsp560 extends Program implements IQuestAdegVer {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private QuestAdegVerDao questAdegVerDao = new QuestAdegVerDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Idbsp560Data ws = new Idbsp560Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: QUEST-ADEG-VER
    private QuestAdegVer questAdegVer;

    //==== METHODS ====
    /**Original name: PROGRAM_IDBSP560_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, QuestAdegVer questAdegVer) {
        this.idsv0003 = idsv0003;
        this.questAdegVer = questAdegVer;
        // COB_CODE: PERFORM A000-INIZIO                    THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO          THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS          THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO          THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                        a300ElaboraIdEff();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                        a400ElaboraIdpEff();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO          THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS          THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO          THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                        b300ElaboraIdCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                        b400ElaboraIdpCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                        b500ElaboraIboCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                        b600ElaboraIbsCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                        b700ElaboraIdoCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //              SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-PRIMARY-KEY
                //                 PERFORM A200-ELABORA-PK          THRU A200-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO         THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS         THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO         THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.PRIMARY_KEY:// COB_CODE: PERFORM A200-ELABORA-PK          THRU A200-EX
                        a200ElaboraPk();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO         THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS         THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO         THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Idbsp560 getInstance() {
        return ((Idbsp560)Programs.getInstance(Idbsp560.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'IDBSP560'   TO IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("IDBSP560");
        // COB_CODE: MOVE 'QUEST_ADEG_VER' TO IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("QUEST_ADEG_VER");
        // COB_CODE: MOVE '00'                     TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                   TO   IDSV0003-SQLCODE
        //                                              IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                              IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-AGG-STORICO-SOLO-INS  OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isAggStoricoSoloIns() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-PK<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a200ElaboraPk() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-PK          THRU A210-EX
        //              WHEN IDSV0003-INSERT
        //                 PERFORM A220-INSERT-PK          THRU A220-EX
        //              WHEN IDSV0003-UPDATE
        //                 PERFORM A230-UPDATE-PK          THRU A230-EX
        //              WHEN IDSV0003-DELETE
        //                 PERFORM A240-DELETE-PK          THRU A240-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-PK          THRU A210-EX
            a210SelectPk();
        }
        else if (idsv0003.getOperazione().isInsert()) {
            // COB_CODE: PERFORM A220-INSERT-PK          THRU A220-EX
            a220InsertPk();
        }
        else if (idsv0003.getOperazione().isUpdate()) {
            // COB_CODE: PERFORM A230-UPDATE-PK          THRU A230-EX
            a230UpdatePk();
        }
        else if (idsv0003.getOperazione().isDelete()) {
            // COB_CODE: PERFORM A240-DELETE-PK          THRU A240-EX
            a240DeletePk();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A300-ELABORA-ID-EFF<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void a300ElaboraIdEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A310-SELECT-ID-EFF          THRU A310-EX
            a310SelectIdEff();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A400-ELABORA-IDP-EFF<br>*/
    private void a400ElaboraIdpEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
            a410SelectIdpEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
            a460OpenCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
            a470CloseCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
            a480FetchFirstIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
            a490FetchNextIdpEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A500-ELABORA-IBO<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a500ElaboraIbo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A510-SELECT-IBO             THRU A510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A510-SELECT-IBO             THRU A510-EX
            a510SelectIbo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
            a560OpenCursorIbo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
            a570CloseCursorIbo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
            a580FetchFirstIbo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
            a590FetchNextIbo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A600-ELABORA-IBS<br>*/
    private void a600ElaboraIbs() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A610-SELECT-IBS             THRU A610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A610-SELECT-IBS             THRU A610-EX
            a610SelectIbs();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
            a660OpenCursorIbs();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
            a670CloseCursorIbs();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
            a680FetchFirstIbs();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
            a690FetchNextIbs();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A700-ELABORA-IDO<br>*/
    private void a700ElaboraIdo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A710-SELECT-IDO                 THRU A710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A710-SELECT-IDO                 THRU A710-EX
            a710SelectIdo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
            a760OpenCursorIdo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
            a770CloseCursorIdo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
            a780FetchFirstIdo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
            a790FetchNextIdo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B300-ELABORA-ID-CPZ<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void b300ElaboraIdCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
            b310SelectIdCpz();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B400-ELABORA-IDP-CPZ<br>*/
    private void b400ElaboraIdpCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
            b410SelectIdpCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
            b460OpenCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
            b470CloseCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
            b480FetchFirstIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
            b490FetchNextIdpCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B500-ELABORA-IBO-CPZ<br>*/
    private void b500ElaboraIboCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
            b510SelectIboCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
            b560OpenCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
            b570CloseCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
            b580FetchFirstIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B600-ELABORA-IBS-CPZ<br>*/
    private void b600ElaboraIbsCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
            b610SelectIbsCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
            b660OpenCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
            b670CloseCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
            b680FetchFirstIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B700-ELABORA-IDO-CPZ<br>*/
    private void b700ElaboraIdoCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
            b710SelectIdoCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
            b760OpenCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
            b770CloseCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
            b780FetchFirstIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A210-SELECT-PK<br>
	 * <pre>----
	 * ----  gestione PK
	 * ----</pre>*/
    private void a210SelectPk() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_QUEST_ADEG_VER
        //                ,COD_COMP_ANIA
        //                ,ID_MOVI_CRZ
        //                ,ID_RAPP_ANA
        //                ,ID_POLI
        //                ,NATURA_OPRZ
        //                ,ORGN_FND
        //                ,COD_QLFC_PROF
        //                ,COD_NAZ_QLFC_PROF
        //                ,COD_PRV_QLFC_PROF
        //                ,FNT_REDD
        //                ,REDD_FATT_ANNU
        //                ,COD_ATECO
        //                ,VALUT_COLL
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,LUOGO_COSTITUZIONE
        //                ,TP_MOVI
        //                ,FL_RAG_RAPP
        //                ,FL_PRSZ_TIT_EFF
        //                ,TP_MOT_RISC
        //                ,TP_PNT_VND
        //                ,TP_ADEG_VER
        //                ,TP_RELA_ESEC
        //                ,TP_SCO_FIN_RAPP
        //                ,FL_PRSZ_3O_PAGAT
        //                ,AREA_GEO_PROV_FND
        //                ,TP_DEST_FND
        //                ,FL_PAESE_RESID_AUT
        //                ,FL_PAESE_CIT_AUT
        //                ,FL_PAESE_NAZ_AUT
        //                ,COD_PROF_PREC
        //                ,FL_AUT_PEP
        //                ,FL_IMP_CAR_PUB
        //                ,FL_LIS_TERR_SORV
        //                ,TP_SIT_FIN_PAT
        //                ,TP_SIT_FIN_PAT_CON
        //                ,IMP_TOT_AFF_UTIL
        //                ,IMP_TOT_FIN_UTIL
        //                ,IMP_TOT_AFF_ACC
        //                ,IMP_TOT_FIN_ACC
        //                ,TP_FRM_GIUR_SAV
        //                ,REG_COLL_POLI
        //                ,NUM_TEL
        //                ,NUM_DIP
        //                ,TP_SIT_FAM_CONV
        //                ,COD_PROF_CON
        //                ,FL_ES_PROC_PEN
        //                ,TP_COND_CLIENTE
        //                ,COD_SAE
        //                ,TP_OPER_ESTERO
        //                ,STAT_OPER_ESTERO
        //                ,COD_PRV_SVOL_ATT
        //                ,COD_STAT_SVOL_ATT
        //                ,TP_SOC
        //                ,FL_IND_SOC_QUOT
        //                ,TP_SIT_GIUR
        //                ,PC_QUO_DET_TIT_EFF
        //                ,TP_PRFL_RSH_PEP
        //                ,TP_PEP
        //                ,FL_NOT_PREG
        //                ,DT_INI_FNT_REDD
        //                ,FNT_REDD_2
        //                ,DT_INI_FNT_REDD_2
        //                ,FNT_REDD_3
        //                ,DT_INI_FNT_REDD_3
        //                ,MOT_ASS_TIT_EFF
        //                ,FIN_COSTITUZIONE
        //                ,DESC_IMP_CAR_PUB
        //                ,DESC_SCO_FIN_RAPP
        //                ,DESC_PROC_PNL
        //                ,DESC_NOT_PREG
        //                ,ID_ASSICURATI
        //                ,REDD_CON
        //                ,DESC_LIB_MOT_RISC
        //                ,TP_MOT_ASS_TIT_EFF
        //                ,TP_RAG_RAPP
        //                ,COD_CAN
        //                ,TP_FIN_COST
        //                ,NAZ_DEST_FND
        //                ,FL_AU_FATCA_AEOI
        //                ,TP_CAR_FIN_GIUR
        //                ,TP_CAR_FIN_GIUR_AT
        //                ,TP_CAR_FIN_GIUR_PA
        //                ,FL_ISTITUZ_FIN
        //                ,TP_ORI_FND_TIT_EFF
        //                ,PC_ESP_AG_PA_MSC
        //                ,FL_PR_TR_USA
        //                ,FL_PR_TR_NO_USA
        //                ,PC_RIP_PAT_AS_VITA
        //                ,PC_RIP_PAT_IM
        //                ,PC_RIP_PAT_SET_IM
        //                ,TP_STATUS_AEOI
        //                ,TP_STATUS_FATCA
        //                ,FL_RAPP_PA_MSC
        //                ,COD_COMUN_SVOL_ATT
        //                ,TP_DT_1O_CON_CLI
        //                ,TP_MOD_EN_RELA_INT
        //                ,TP_REDD_ANNU_LRD
        //                ,TP_REDD_CON
        //                ,TP_OPER_SOC_FID
        //                ,COD_PA_ESP_MSC_1
        //                ,IMP_PA_ESP_MSC_1
        //                ,COD_PA_ESP_MSC_2
        //                ,IMP_PA_ESP_MSC_2
        //                ,COD_PA_ESP_MSC_3
        //                ,IMP_PA_ESP_MSC_3
        //                ,COD_PA_ESP_MSC_4
        //                ,IMP_PA_ESP_MSC_4
        //                ,COD_PA_ESP_MSC_5
        //                ,IMP_PA_ESP_MSC_5
        //                ,DESC_ORGN_FND
        //                ,COD_AUT_DUE_DIL
        //                ,FL_PR_QUEST_FATCA
        //                ,FL_PR_QUEST_AEOI
        //                ,FL_PR_QUEST_OFAC
        //                ,FL_PR_QUEST_KYC
        //                ,FL_PR_QUEST_MSCQ
        //                ,TP_NOT_PREG
        //                ,TP_PROC_PNL
        //                ,COD_IMP_CAR_PUB
        //                ,OPRZ_SOSPETTE
        //                ,ULT_FATT_ANNU
        //                ,DESC_PEP
        //                ,NUM_TEL_2
        //                ,IMP_AFI
        //                ,FL_NEW_PRO
        //                ,TP_MOT_CAMBIO_CNTR
        //                ,DESC_MOT_CAMBIO_CN
        //                ,COD_SOGG
        //             INTO
        //                :P56-ID-QUEST-ADEG-VER
        //               ,:P56-COD-COMP-ANIA
        //               ,:P56-ID-MOVI-CRZ
        //               ,:P56-ID-RAPP-ANA
        //                :IND-P56-ID-RAPP-ANA
        //               ,:P56-ID-POLI
        //               ,:P56-NATURA-OPRZ
        //                :IND-P56-NATURA-OPRZ
        //               ,:P56-ORGN-FND
        //                :IND-P56-ORGN-FND
        //               ,:P56-COD-QLFC-PROF
        //                :IND-P56-COD-QLFC-PROF
        //               ,:P56-COD-NAZ-QLFC-PROF
        //                :IND-P56-COD-NAZ-QLFC-PROF
        //               ,:P56-COD-PRV-QLFC-PROF
        //                :IND-P56-COD-PRV-QLFC-PROF
        //               ,:P56-FNT-REDD
        //                :IND-P56-FNT-REDD
        //               ,:P56-REDD-FATT-ANNU
        //                :IND-P56-REDD-FATT-ANNU
        //               ,:P56-COD-ATECO
        //                :IND-P56-COD-ATECO
        //               ,:P56-VALUT-COLL
        //                :IND-P56-VALUT-COLL
        //               ,:P56-DS-OPER-SQL
        //               ,:P56-DS-VER
        //               ,:P56-DS-TS-CPTZ
        //               ,:P56-DS-UTENTE
        //               ,:P56-DS-STATO-ELAB
        //               ,:P56-LUOGO-COSTITUZIONE-VCHAR
        //                :IND-P56-LUOGO-COSTITUZIONE
        //               ,:P56-TP-MOVI
        //                :IND-P56-TP-MOVI
        //               ,:P56-FL-RAG-RAPP
        //                :IND-P56-FL-RAG-RAPP
        //               ,:P56-FL-PRSZ-TIT-EFF
        //                :IND-P56-FL-PRSZ-TIT-EFF
        //               ,:P56-TP-MOT-RISC
        //                :IND-P56-TP-MOT-RISC
        //               ,:P56-TP-PNT-VND
        //                :IND-P56-TP-PNT-VND
        //               ,:P56-TP-ADEG-VER
        //                :IND-P56-TP-ADEG-VER
        //               ,:P56-TP-RELA-ESEC
        //                :IND-P56-TP-RELA-ESEC
        //               ,:P56-TP-SCO-FIN-RAPP
        //                :IND-P56-TP-SCO-FIN-RAPP
        //               ,:P56-FL-PRSZ-3O-PAGAT
        //                :IND-P56-FL-PRSZ-3O-PAGAT
        //               ,:P56-AREA-GEO-PROV-FND
        //                :IND-P56-AREA-GEO-PROV-FND
        //               ,:P56-TP-DEST-FND
        //                :IND-P56-TP-DEST-FND
        //               ,:P56-FL-PAESE-RESID-AUT
        //                :IND-P56-FL-PAESE-RESID-AUT
        //               ,:P56-FL-PAESE-CIT-AUT
        //                :IND-P56-FL-PAESE-CIT-AUT
        //               ,:P56-FL-PAESE-NAZ-AUT
        //                :IND-P56-FL-PAESE-NAZ-AUT
        //               ,:P56-COD-PROF-PREC
        //                :IND-P56-COD-PROF-PREC
        //               ,:P56-FL-AUT-PEP
        //                :IND-P56-FL-AUT-PEP
        //               ,:P56-FL-IMP-CAR-PUB
        //                :IND-P56-FL-IMP-CAR-PUB
        //               ,:P56-FL-LIS-TERR-SORV
        //                :IND-P56-FL-LIS-TERR-SORV
        //               ,:P56-TP-SIT-FIN-PAT
        //                :IND-P56-TP-SIT-FIN-PAT
        //               ,:P56-TP-SIT-FIN-PAT-CON
        //                :IND-P56-TP-SIT-FIN-PAT-CON
        //               ,:P56-IMP-TOT-AFF-UTIL
        //                :IND-P56-IMP-TOT-AFF-UTIL
        //               ,:P56-IMP-TOT-FIN-UTIL
        //                :IND-P56-IMP-TOT-FIN-UTIL
        //               ,:P56-IMP-TOT-AFF-ACC
        //                :IND-P56-IMP-TOT-AFF-ACC
        //               ,:P56-IMP-TOT-FIN-ACC
        //                :IND-P56-IMP-TOT-FIN-ACC
        //               ,:P56-TP-FRM-GIUR-SAV
        //                :IND-P56-TP-FRM-GIUR-SAV
        //               ,:P56-REG-COLL-POLI
        //                :IND-P56-REG-COLL-POLI
        //               ,:P56-NUM-TEL
        //                :IND-P56-NUM-TEL
        //               ,:P56-NUM-DIP
        //                :IND-P56-NUM-DIP
        //               ,:P56-TP-SIT-FAM-CONV
        //                :IND-P56-TP-SIT-FAM-CONV
        //               ,:P56-COD-PROF-CON
        //                :IND-P56-COD-PROF-CON
        //               ,:P56-FL-ES-PROC-PEN
        //                :IND-P56-FL-ES-PROC-PEN
        //               ,:P56-TP-COND-CLIENTE
        //                :IND-P56-TP-COND-CLIENTE
        //               ,:P56-COD-SAE
        //                :IND-P56-COD-SAE
        //               ,:P56-TP-OPER-ESTERO
        //                :IND-P56-TP-OPER-ESTERO
        //               ,:P56-STAT-OPER-ESTERO
        //                :IND-P56-STAT-OPER-ESTERO
        //               ,:P56-COD-PRV-SVOL-ATT
        //                :IND-P56-COD-PRV-SVOL-ATT
        //               ,:P56-COD-STAT-SVOL-ATT
        //                :IND-P56-COD-STAT-SVOL-ATT
        //               ,:P56-TP-SOC
        //                :IND-P56-TP-SOC
        //               ,:P56-FL-IND-SOC-QUOT
        //                :IND-P56-FL-IND-SOC-QUOT
        //               ,:P56-TP-SIT-GIUR
        //                :IND-P56-TP-SIT-GIUR
        //               ,:P56-PC-QUO-DET-TIT-EFF
        //                :IND-P56-PC-QUO-DET-TIT-EFF
        //               ,:P56-TP-PRFL-RSH-PEP
        //                :IND-P56-TP-PRFL-RSH-PEP
        //               ,:P56-TP-PEP
        //                :IND-P56-TP-PEP
        //               ,:P56-FL-NOT-PREG
        //                :IND-P56-FL-NOT-PREG
        //               ,:P56-DT-INI-FNT-REDD-DB
        //                :IND-P56-DT-INI-FNT-REDD
        //               ,:P56-FNT-REDD-2
        //                :IND-P56-FNT-REDD-2
        //               ,:P56-DT-INI-FNT-REDD-2-DB
        //                :IND-P56-DT-INI-FNT-REDD-2
        //               ,:P56-FNT-REDD-3
        //                :IND-P56-FNT-REDD-3
        //               ,:P56-DT-INI-FNT-REDD-3-DB
        //                :IND-P56-DT-INI-FNT-REDD-3
        //               ,:P56-MOT-ASS-TIT-EFF-VCHAR
        //                :IND-P56-MOT-ASS-TIT-EFF
        //               ,:P56-FIN-COSTITUZIONE-VCHAR
        //                :IND-P56-FIN-COSTITUZIONE
        //               ,:P56-DESC-IMP-CAR-PUB-VCHAR
        //                :IND-P56-DESC-IMP-CAR-PUB
        //               ,:P56-DESC-SCO-FIN-RAPP-VCHAR
        //                :IND-P56-DESC-SCO-FIN-RAPP
        //               ,:P56-DESC-PROC-PNL-VCHAR
        //                :IND-P56-DESC-PROC-PNL
        //               ,:P56-DESC-NOT-PREG-VCHAR
        //                :IND-P56-DESC-NOT-PREG
        //               ,:P56-ID-ASSICURATI
        //                :IND-P56-ID-ASSICURATI
        //               ,:P56-REDD-CON
        //                :IND-P56-REDD-CON
        //               ,:P56-DESC-LIB-MOT-RISC-VCHAR
        //                :IND-P56-DESC-LIB-MOT-RISC
        //               ,:P56-TP-MOT-ASS-TIT-EFF
        //                :IND-P56-TP-MOT-ASS-TIT-EFF
        //               ,:P56-TP-RAG-RAPP
        //                :IND-P56-TP-RAG-RAPP
        //               ,:P56-COD-CAN
        //                :IND-P56-COD-CAN
        //               ,:P56-TP-FIN-COST
        //                :IND-P56-TP-FIN-COST
        //               ,:P56-NAZ-DEST-FND
        //                :IND-P56-NAZ-DEST-FND
        //               ,:P56-FL-AU-FATCA-AEOI
        //                :IND-P56-FL-AU-FATCA-AEOI
        //               ,:P56-TP-CAR-FIN-GIUR
        //                :IND-P56-TP-CAR-FIN-GIUR
        //               ,:P56-TP-CAR-FIN-GIUR-AT
        //                :IND-P56-TP-CAR-FIN-GIUR-AT
        //               ,:P56-TP-CAR-FIN-GIUR-PA
        //                :IND-P56-TP-CAR-FIN-GIUR-PA
        //               ,:P56-FL-ISTITUZ-FIN
        //                :IND-P56-FL-ISTITUZ-FIN
        //               ,:P56-TP-ORI-FND-TIT-EFF
        //                :IND-P56-TP-ORI-FND-TIT-EFF
        //               ,:P56-PC-ESP-AG-PA-MSC
        //                :IND-P56-PC-ESP-AG-PA-MSC
        //               ,:P56-FL-PR-TR-USA
        //                :IND-P56-FL-PR-TR-USA
        //               ,:P56-FL-PR-TR-NO-USA
        //                :IND-P56-FL-PR-TR-NO-USA
        //               ,:P56-PC-RIP-PAT-AS-VITA
        //                :IND-P56-PC-RIP-PAT-AS-VITA
        //               ,:P56-PC-RIP-PAT-IM
        //                :IND-P56-PC-RIP-PAT-IM
        //               ,:P56-PC-RIP-PAT-SET-IM
        //                :IND-P56-PC-RIP-PAT-SET-IM
        //               ,:P56-TP-STATUS-AEOI
        //                :IND-P56-TP-STATUS-AEOI
        //               ,:P56-TP-STATUS-FATCA
        //                :IND-P56-TP-STATUS-FATCA
        //               ,:P56-FL-RAPP-PA-MSC
        //                :IND-P56-FL-RAPP-PA-MSC
        //               ,:P56-COD-COMUN-SVOL-ATT
        //                :IND-P56-COD-COMUN-SVOL-ATT
        //               ,:P56-TP-DT-1O-CON-CLI
        //                :IND-P56-TP-DT-1O-CON-CLI
        //               ,:P56-TP-MOD-EN-RELA-INT
        //                :IND-P56-TP-MOD-EN-RELA-INT
        //               ,:P56-TP-REDD-ANNU-LRD
        //                :IND-P56-TP-REDD-ANNU-LRD
        //               ,:P56-TP-REDD-CON
        //                :IND-P56-TP-REDD-CON
        //               ,:P56-TP-OPER-SOC-FID
        //                :IND-P56-TP-OPER-SOC-FID
        //               ,:P56-COD-PA-ESP-MSC-1
        //                :IND-P56-COD-PA-ESP-MSC-1
        //               ,:P56-IMP-PA-ESP-MSC-1
        //                :IND-P56-IMP-PA-ESP-MSC-1
        //               ,:P56-COD-PA-ESP-MSC-2
        //                :IND-P56-COD-PA-ESP-MSC-2
        //               ,:P56-IMP-PA-ESP-MSC-2
        //                :IND-P56-IMP-PA-ESP-MSC-2
        //               ,:P56-COD-PA-ESP-MSC-3
        //                :IND-P56-COD-PA-ESP-MSC-3
        //               ,:P56-IMP-PA-ESP-MSC-3
        //                :IND-P56-IMP-PA-ESP-MSC-3
        //               ,:P56-COD-PA-ESP-MSC-4
        //                :IND-P56-COD-PA-ESP-MSC-4
        //               ,:P56-IMP-PA-ESP-MSC-4
        //                :IND-P56-IMP-PA-ESP-MSC-4
        //               ,:P56-COD-PA-ESP-MSC-5
        //                :IND-P56-COD-PA-ESP-MSC-5
        //               ,:P56-IMP-PA-ESP-MSC-5
        //                :IND-P56-IMP-PA-ESP-MSC-5
        //               ,:P56-DESC-ORGN-FND-VCHAR
        //                :IND-P56-DESC-ORGN-FND
        //               ,:P56-COD-AUT-DUE-DIL
        //                :IND-P56-COD-AUT-DUE-DIL
        //               ,:P56-FL-PR-QUEST-FATCA
        //                :IND-P56-FL-PR-QUEST-FATCA
        //               ,:P56-FL-PR-QUEST-AEOI
        //                :IND-P56-FL-PR-QUEST-AEOI
        //               ,:P56-FL-PR-QUEST-OFAC
        //                :IND-P56-FL-PR-QUEST-OFAC
        //               ,:P56-FL-PR-QUEST-KYC
        //                :IND-P56-FL-PR-QUEST-KYC
        //               ,:P56-FL-PR-QUEST-MSCQ
        //                :IND-P56-FL-PR-QUEST-MSCQ
        //               ,:P56-TP-NOT-PREG
        //                :IND-P56-TP-NOT-PREG
        //               ,:P56-TP-PROC-PNL
        //                :IND-P56-TP-PROC-PNL
        //               ,:P56-COD-IMP-CAR-PUB
        //                :IND-P56-COD-IMP-CAR-PUB
        //               ,:P56-OPRZ-SOSPETTE
        //                :IND-P56-OPRZ-SOSPETTE
        //               ,:P56-ULT-FATT-ANNU
        //                :IND-P56-ULT-FATT-ANNU
        //               ,:P56-DESC-PEP-VCHAR
        //                :IND-P56-DESC-PEP
        //               ,:P56-NUM-TEL-2
        //                :IND-P56-NUM-TEL-2
        //               ,:P56-IMP-AFI
        //                :IND-P56-IMP-AFI
        //               ,:P56-FL-NEW-PRO
        //                :IND-P56-FL-NEW-PRO
        //               ,:P56-TP-MOT-CAMBIO-CNTR
        //                :IND-P56-TP-MOT-CAMBIO-CNTR
        //               ,:P56-DESC-MOT-CAMBIO-CN-VCHAR
        //                :IND-P56-DESC-MOT-CAMBIO-CN
        //               ,:P56-COD-SOGG
        //                :IND-P56-COD-SOGG
        //             FROM QUEST_ADEG_VER
        //             WHERE     ID_QUEST_ADEG_VER = :P56-ID-QUEST-ADEG-VER
        //           END-EXEC.
        questAdegVerDao.selectByP56IdQuestAdegVer(questAdegVer.getP56IdQuestAdegVer(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A220-INSERT-PK<br>*/
    private void a220InsertPk() {
        // COB_CODE: PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.
        z400SeqRiga();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX
            z150ValorizzaDataServicesI();
            // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX
            z200SetIndicatoriNull();
            // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX
            z900ConvertiNToX();
            // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX
            z960LengthVchar();
            // COB_CODE: EXEC SQL
            //              INSERT
            //              INTO QUEST_ADEG_VER
            //                  (
            //                     ID_QUEST_ADEG_VER
            //                    ,COD_COMP_ANIA
            //                    ,ID_MOVI_CRZ
            //                    ,ID_RAPP_ANA
            //                    ,ID_POLI
            //                    ,NATURA_OPRZ
            //                    ,ORGN_FND
            //                    ,COD_QLFC_PROF
            //                    ,COD_NAZ_QLFC_PROF
            //                    ,COD_PRV_QLFC_PROF
            //                    ,FNT_REDD
            //                    ,REDD_FATT_ANNU
            //                    ,COD_ATECO
            //                    ,VALUT_COLL
            //                    ,DS_OPER_SQL
            //                    ,DS_VER
            //                    ,DS_TS_CPTZ
            //                    ,DS_UTENTE
            //                    ,DS_STATO_ELAB
            //                    ,LUOGO_COSTITUZIONE
            //                    ,TP_MOVI
            //                    ,FL_RAG_RAPP
            //                    ,FL_PRSZ_TIT_EFF
            //                    ,TP_MOT_RISC
            //                    ,TP_PNT_VND
            //                    ,TP_ADEG_VER
            //                    ,TP_RELA_ESEC
            //                    ,TP_SCO_FIN_RAPP
            //                    ,FL_PRSZ_3O_PAGAT
            //                    ,AREA_GEO_PROV_FND
            //                    ,TP_DEST_FND
            //                    ,FL_PAESE_RESID_AUT
            //                    ,FL_PAESE_CIT_AUT
            //                    ,FL_PAESE_NAZ_AUT
            //                    ,COD_PROF_PREC
            //                    ,FL_AUT_PEP
            //                    ,FL_IMP_CAR_PUB
            //                    ,FL_LIS_TERR_SORV
            //                    ,TP_SIT_FIN_PAT
            //                    ,TP_SIT_FIN_PAT_CON
            //                    ,IMP_TOT_AFF_UTIL
            //                    ,IMP_TOT_FIN_UTIL
            //                    ,IMP_TOT_AFF_ACC
            //                    ,IMP_TOT_FIN_ACC
            //                    ,TP_FRM_GIUR_SAV
            //                    ,REG_COLL_POLI
            //                    ,NUM_TEL
            //                    ,NUM_DIP
            //                    ,TP_SIT_FAM_CONV
            //                    ,COD_PROF_CON
            //                    ,FL_ES_PROC_PEN
            //                    ,TP_COND_CLIENTE
            //                    ,COD_SAE
            //                    ,TP_OPER_ESTERO
            //                    ,STAT_OPER_ESTERO
            //                    ,COD_PRV_SVOL_ATT
            //                    ,COD_STAT_SVOL_ATT
            //                    ,TP_SOC
            //                    ,FL_IND_SOC_QUOT
            //                    ,TP_SIT_GIUR
            //                    ,PC_QUO_DET_TIT_EFF
            //                    ,TP_PRFL_RSH_PEP
            //                    ,TP_PEP
            //                    ,FL_NOT_PREG
            //                    ,DT_INI_FNT_REDD
            //                    ,FNT_REDD_2
            //                    ,DT_INI_FNT_REDD_2
            //                    ,FNT_REDD_3
            //                    ,DT_INI_FNT_REDD_3
            //                    ,MOT_ASS_TIT_EFF
            //                    ,FIN_COSTITUZIONE
            //                    ,DESC_IMP_CAR_PUB
            //                    ,DESC_SCO_FIN_RAPP
            //                    ,DESC_PROC_PNL
            //                    ,DESC_NOT_PREG
            //                    ,ID_ASSICURATI
            //                    ,REDD_CON
            //                    ,DESC_LIB_MOT_RISC
            //                    ,TP_MOT_ASS_TIT_EFF
            //                    ,TP_RAG_RAPP
            //                    ,COD_CAN
            //                    ,TP_FIN_COST
            //                    ,NAZ_DEST_FND
            //                    ,FL_AU_FATCA_AEOI
            //                    ,TP_CAR_FIN_GIUR
            //                    ,TP_CAR_FIN_GIUR_AT
            //                    ,TP_CAR_FIN_GIUR_PA
            //                    ,FL_ISTITUZ_FIN
            //                    ,TP_ORI_FND_TIT_EFF
            //                    ,PC_ESP_AG_PA_MSC
            //                    ,FL_PR_TR_USA
            //                    ,FL_PR_TR_NO_USA
            //                    ,PC_RIP_PAT_AS_VITA
            //                    ,PC_RIP_PAT_IM
            //                    ,PC_RIP_PAT_SET_IM
            //                    ,TP_STATUS_AEOI
            //                    ,TP_STATUS_FATCA
            //                    ,FL_RAPP_PA_MSC
            //                    ,COD_COMUN_SVOL_ATT
            //                    ,TP_DT_1O_CON_CLI
            //                    ,TP_MOD_EN_RELA_INT
            //                    ,TP_REDD_ANNU_LRD
            //                    ,TP_REDD_CON
            //                    ,TP_OPER_SOC_FID
            //                    ,COD_PA_ESP_MSC_1
            //                    ,IMP_PA_ESP_MSC_1
            //                    ,COD_PA_ESP_MSC_2
            //                    ,IMP_PA_ESP_MSC_2
            //                    ,COD_PA_ESP_MSC_3
            //                    ,IMP_PA_ESP_MSC_3
            //                    ,COD_PA_ESP_MSC_4
            //                    ,IMP_PA_ESP_MSC_4
            //                    ,COD_PA_ESP_MSC_5
            //                    ,IMP_PA_ESP_MSC_5
            //                    ,DESC_ORGN_FND
            //                    ,COD_AUT_DUE_DIL
            //                    ,FL_PR_QUEST_FATCA
            //                    ,FL_PR_QUEST_AEOI
            //                    ,FL_PR_QUEST_OFAC
            //                    ,FL_PR_QUEST_KYC
            //                    ,FL_PR_QUEST_MSCQ
            //                    ,TP_NOT_PREG
            //                    ,TP_PROC_PNL
            //                    ,COD_IMP_CAR_PUB
            //                    ,OPRZ_SOSPETTE
            //                    ,ULT_FATT_ANNU
            //                    ,DESC_PEP
            //                    ,NUM_TEL_2
            //                    ,IMP_AFI
            //                    ,FL_NEW_PRO
            //                    ,TP_MOT_CAMBIO_CNTR
            //                    ,DESC_MOT_CAMBIO_CN
            //                    ,COD_SOGG
            //                  )
            //              VALUES
            //                  (
            //                    :P56-ID-QUEST-ADEG-VER
            //                    ,:P56-COD-COMP-ANIA
            //                    ,:P56-ID-MOVI-CRZ
            //                    ,:P56-ID-RAPP-ANA
            //                     :IND-P56-ID-RAPP-ANA
            //                    ,:P56-ID-POLI
            //                    ,:P56-NATURA-OPRZ
            //                     :IND-P56-NATURA-OPRZ
            //                    ,:P56-ORGN-FND
            //                     :IND-P56-ORGN-FND
            //                    ,:P56-COD-QLFC-PROF
            //                     :IND-P56-COD-QLFC-PROF
            //                    ,:P56-COD-NAZ-QLFC-PROF
            //                     :IND-P56-COD-NAZ-QLFC-PROF
            //                    ,:P56-COD-PRV-QLFC-PROF
            //                     :IND-P56-COD-PRV-QLFC-PROF
            //                    ,:P56-FNT-REDD
            //                     :IND-P56-FNT-REDD
            //                    ,:P56-REDD-FATT-ANNU
            //                     :IND-P56-REDD-FATT-ANNU
            //                    ,:P56-COD-ATECO
            //                     :IND-P56-COD-ATECO
            //                    ,:P56-VALUT-COLL
            //                     :IND-P56-VALUT-COLL
            //                    ,:P56-DS-OPER-SQL
            //                    ,:P56-DS-VER
            //                    ,:P56-DS-TS-CPTZ
            //                    ,:P56-DS-UTENTE
            //                    ,:P56-DS-STATO-ELAB
            //                    ,:P56-LUOGO-COSTITUZIONE-VCHAR
            //                     :IND-P56-LUOGO-COSTITUZIONE
            //                    ,:P56-TP-MOVI
            //                     :IND-P56-TP-MOVI
            //                    ,:P56-FL-RAG-RAPP
            //                     :IND-P56-FL-RAG-RAPP
            //                    ,:P56-FL-PRSZ-TIT-EFF
            //                     :IND-P56-FL-PRSZ-TIT-EFF
            //                    ,:P56-TP-MOT-RISC
            //                     :IND-P56-TP-MOT-RISC
            //                    ,:P56-TP-PNT-VND
            //                     :IND-P56-TP-PNT-VND
            //                    ,:P56-TP-ADEG-VER
            //                     :IND-P56-TP-ADEG-VER
            //                    ,:P56-TP-RELA-ESEC
            //                     :IND-P56-TP-RELA-ESEC
            //                    ,:P56-TP-SCO-FIN-RAPP
            //                     :IND-P56-TP-SCO-FIN-RAPP
            //                    ,:P56-FL-PRSZ-3O-PAGAT
            //                     :IND-P56-FL-PRSZ-3O-PAGAT
            //                    ,:P56-AREA-GEO-PROV-FND
            //                     :IND-P56-AREA-GEO-PROV-FND
            //                    ,:P56-TP-DEST-FND
            //                     :IND-P56-TP-DEST-FND
            //                    ,:P56-FL-PAESE-RESID-AUT
            //                     :IND-P56-FL-PAESE-RESID-AUT
            //                    ,:P56-FL-PAESE-CIT-AUT
            //                     :IND-P56-FL-PAESE-CIT-AUT
            //                    ,:P56-FL-PAESE-NAZ-AUT
            //                     :IND-P56-FL-PAESE-NAZ-AUT
            //                    ,:P56-COD-PROF-PREC
            //                     :IND-P56-COD-PROF-PREC
            //                    ,:P56-FL-AUT-PEP
            //                     :IND-P56-FL-AUT-PEP
            //                    ,:P56-FL-IMP-CAR-PUB
            //                     :IND-P56-FL-IMP-CAR-PUB
            //                    ,:P56-FL-LIS-TERR-SORV
            //                     :IND-P56-FL-LIS-TERR-SORV
            //                    ,:P56-TP-SIT-FIN-PAT
            //                     :IND-P56-TP-SIT-FIN-PAT
            //                    ,:P56-TP-SIT-FIN-PAT-CON
            //                     :IND-P56-TP-SIT-FIN-PAT-CON
            //                    ,:P56-IMP-TOT-AFF-UTIL
            //                     :IND-P56-IMP-TOT-AFF-UTIL
            //                    ,:P56-IMP-TOT-FIN-UTIL
            //                     :IND-P56-IMP-TOT-FIN-UTIL
            //                    ,:P56-IMP-TOT-AFF-ACC
            //                     :IND-P56-IMP-TOT-AFF-ACC
            //                    ,:P56-IMP-TOT-FIN-ACC
            //                     :IND-P56-IMP-TOT-FIN-ACC
            //                    ,:P56-TP-FRM-GIUR-SAV
            //                     :IND-P56-TP-FRM-GIUR-SAV
            //                    ,:P56-REG-COLL-POLI
            //                     :IND-P56-REG-COLL-POLI
            //                    ,:P56-NUM-TEL
            //                     :IND-P56-NUM-TEL
            //                    ,:P56-NUM-DIP
            //                     :IND-P56-NUM-DIP
            //                    ,:P56-TP-SIT-FAM-CONV
            //                     :IND-P56-TP-SIT-FAM-CONV
            //                    ,:P56-COD-PROF-CON
            //                     :IND-P56-COD-PROF-CON
            //                    ,:P56-FL-ES-PROC-PEN
            //                     :IND-P56-FL-ES-PROC-PEN
            //                    ,:P56-TP-COND-CLIENTE
            //                     :IND-P56-TP-COND-CLIENTE
            //                    ,:P56-COD-SAE
            //                     :IND-P56-COD-SAE
            //                    ,:P56-TP-OPER-ESTERO
            //                     :IND-P56-TP-OPER-ESTERO
            //                    ,:P56-STAT-OPER-ESTERO
            //                     :IND-P56-STAT-OPER-ESTERO
            //                    ,:P56-COD-PRV-SVOL-ATT
            //                     :IND-P56-COD-PRV-SVOL-ATT
            //                    ,:P56-COD-STAT-SVOL-ATT
            //                     :IND-P56-COD-STAT-SVOL-ATT
            //                    ,:P56-TP-SOC
            //                     :IND-P56-TP-SOC
            //                    ,:P56-FL-IND-SOC-QUOT
            //                     :IND-P56-FL-IND-SOC-QUOT
            //                    ,:P56-TP-SIT-GIUR
            //                     :IND-P56-TP-SIT-GIUR
            //                    ,:P56-PC-QUO-DET-TIT-EFF
            //                     :IND-P56-PC-QUO-DET-TIT-EFF
            //                    ,:P56-TP-PRFL-RSH-PEP
            //                     :IND-P56-TP-PRFL-RSH-PEP
            //                    ,:P56-TP-PEP
            //                     :IND-P56-TP-PEP
            //                    ,:P56-FL-NOT-PREG
            //                     :IND-P56-FL-NOT-PREG
            //                    ,:P56-DT-INI-FNT-REDD-DB
            //                     :IND-P56-DT-INI-FNT-REDD
            //                    ,:P56-FNT-REDD-2
            //                     :IND-P56-FNT-REDD-2
            //                    ,:P56-DT-INI-FNT-REDD-2-DB
            //                     :IND-P56-DT-INI-FNT-REDD-2
            //                    ,:P56-FNT-REDD-3
            //                     :IND-P56-FNT-REDD-3
            //                    ,:P56-DT-INI-FNT-REDD-3-DB
            //                     :IND-P56-DT-INI-FNT-REDD-3
            //                    ,:P56-MOT-ASS-TIT-EFF-VCHAR
            //                     :IND-P56-MOT-ASS-TIT-EFF
            //                    ,:P56-FIN-COSTITUZIONE-VCHAR
            //                     :IND-P56-FIN-COSTITUZIONE
            //                    ,:P56-DESC-IMP-CAR-PUB-VCHAR
            //                     :IND-P56-DESC-IMP-CAR-PUB
            //                    ,:P56-DESC-SCO-FIN-RAPP-VCHAR
            //                     :IND-P56-DESC-SCO-FIN-RAPP
            //                    ,:P56-DESC-PROC-PNL-VCHAR
            //                     :IND-P56-DESC-PROC-PNL
            //                    ,:P56-DESC-NOT-PREG-VCHAR
            //                     :IND-P56-DESC-NOT-PREG
            //                    ,:P56-ID-ASSICURATI
            //                     :IND-P56-ID-ASSICURATI
            //                    ,:P56-REDD-CON
            //                     :IND-P56-REDD-CON
            //                    ,:P56-DESC-LIB-MOT-RISC-VCHAR
            //                     :IND-P56-DESC-LIB-MOT-RISC
            //                    ,:P56-TP-MOT-ASS-TIT-EFF
            //                     :IND-P56-TP-MOT-ASS-TIT-EFF
            //                    ,:P56-TP-RAG-RAPP
            //                     :IND-P56-TP-RAG-RAPP
            //                    ,:P56-COD-CAN
            //                     :IND-P56-COD-CAN
            //                    ,:P56-TP-FIN-COST
            //                     :IND-P56-TP-FIN-COST
            //                    ,:P56-NAZ-DEST-FND
            //                     :IND-P56-NAZ-DEST-FND
            //                    ,:P56-FL-AU-FATCA-AEOI
            //                     :IND-P56-FL-AU-FATCA-AEOI
            //                    ,:P56-TP-CAR-FIN-GIUR
            //                     :IND-P56-TP-CAR-FIN-GIUR
            //                    ,:P56-TP-CAR-FIN-GIUR-AT
            //                     :IND-P56-TP-CAR-FIN-GIUR-AT
            //                    ,:P56-TP-CAR-FIN-GIUR-PA
            //                     :IND-P56-TP-CAR-FIN-GIUR-PA
            //                    ,:P56-FL-ISTITUZ-FIN
            //                     :IND-P56-FL-ISTITUZ-FIN
            //                    ,:P56-TP-ORI-FND-TIT-EFF
            //                     :IND-P56-TP-ORI-FND-TIT-EFF
            //                    ,:P56-PC-ESP-AG-PA-MSC
            //                     :IND-P56-PC-ESP-AG-PA-MSC
            //                    ,:P56-FL-PR-TR-USA
            //                     :IND-P56-FL-PR-TR-USA
            //                    ,:P56-FL-PR-TR-NO-USA
            //                     :IND-P56-FL-PR-TR-NO-USA
            //                    ,:P56-PC-RIP-PAT-AS-VITA
            //                     :IND-P56-PC-RIP-PAT-AS-VITA
            //                    ,:P56-PC-RIP-PAT-IM
            //                     :IND-P56-PC-RIP-PAT-IM
            //                    ,:P56-PC-RIP-PAT-SET-IM
            //                     :IND-P56-PC-RIP-PAT-SET-IM
            //                    ,:P56-TP-STATUS-AEOI
            //                     :IND-P56-TP-STATUS-AEOI
            //                    ,:P56-TP-STATUS-FATCA
            //                     :IND-P56-TP-STATUS-FATCA
            //                    ,:P56-FL-RAPP-PA-MSC
            //                     :IND-P56-FL-RAPP-PA-MSC
            //                    ,:P56-COD-COMUN-SVOL-ATT
            //                     :IND-P56-COD-COMUN-SVOL-ATT
            //                    ,:P56-TP-DT-1O-CON-CLI
            //                     :IND-P56-TP-DT-1O-CON-CLI
            //                    ,:P56-TP-MOD-EN-RELA-INT
            //                     :IND-P56-TP-MOD-EN-RELA-INT
            //                    ,:P56-TP-REDD-ANNU-LRD
            //                     :IND-P56-TP-REDD-ANNU-LRD
            //                    ,:P56-TP-REDD-CON
            //                     :IND-P56-TP-REDD-CON
            //                    ,:P56-TP-OPER-SOC-FID
            //                     :IND-P56-TP-OPER-SOC-FID
            //                    ,:P56-COD-PA-ESP-MSC-1
            //                     :IND-P56-COD-PA-ESP-MSC-1
            //                    ,:P56-IMP-PA-ESP-MSC-1
            //                     :IND-P56-IMP-PA-ESP-MSC-1
            //                    ,:P56-COD-PA-ESP-MSC-2
            //                     :IND-P56-COD-PA-ESP-MSC-2
            //                    ,:P56-IMP-PA-ESP-MSC-2
            //                     :IND-P56-IMP-PA-ESP-MSC-2
            //                    ,:P56-COD-PA-ESP-MSC-3
            //                     :IND-P56-COD-PA-ESP-MSC-3
            //                    ,:P56-IMP-PA-ESP-MSC-3
            //                     :IND-P56-IMP-PA-ESP-MSC-3
            //                    ,:P56-COD-PA-ESP-MSC-4
            //                     :IND-P56-COD-PA-ESP-MSC-4
            //                    ,:P56-IMP-PA-ESP-MSC-4
            //                     :IND-P56-IMP-PA-ESP-MSC-4
            //                    ,:P56-COD-PA-ESP-MSC-5
            //                     :IND-P56-COD-PA-ESP-MSC-5
            //                    ,:P56-IMP-PA-ESP-MSC-5
            //                     :IND-P56-IMP-PA-ESP-MSC-5
            //                    ,:P56-DESC-ORGN-FND-VCHAR
            //                     :IND-P56-DESC-ORGN-FND
            //                    ,:P56-COD-AUT-DUE-DIL
            //                     :IND-P56-COD-AUT-DUE-DIL
            //                    ,:P56-FL-PR-QUEST-FATCA
            //                     :IND-P56-FL-PR-QUEST-FATCA
            //                    ,:P56-FL-PR-QUEST-AEOI
            //                     :IND-P56-FL-PR-QUEST-AEOI
            //                    ,:P56-FL-PR-QUEST-OFAC
            //                     :IND-P56-FL-PR-QUEST-OFAC
            //                    ,:P56-FL-PR-QUEST-KYC
            //                     :IND-P56-FL-PR-QUEST-KYC
            //                    ,:P56-FL-PR-QUEST-MSCQ
            //                     :IND-P56-FL-PR-QUEST-MSCQ
            //                    ,:P56-TP-NOT-PREG
            //                     :IND-P56-TP-NOT-PREG
            //                    ,:P56-TP-PROC-PNL
            //                     :IND-P56-TP-PROC-PNL
            //                    ,:P56-COD-IMP-CAR-PUB
            //                     :IND-P56-COD-IMP-CAR-PUB
            //                    ,:P56-OPRZ-SOSPETTE
            //                     :IND-P56-OPRZ-SOSPETTE
            //                    ,:P56-ULT-FATT-ANNU
            //                     :IND-P56-ULT-FATT-ANNU
            //                    ,:P56-DESC-PEP-VCHAR
            //                     :IND-P56-DESC-PEP
            //                    ,:P56-NUM-TEL-2
            //                     :IND-P56-NUM-TEL-2
            //                    ,:P56-IMP-AFI
            //                     :IND-P56-IMP-AFI
            //                    ,:P56-FL-NEW-PRO
            //                     :IND-P56-FL-NEW-PRO
            //                    ,:P56-TP-MOT-CAMBIO-CNTR
            //                     :IND-P56-TP-MOT-CAMBIO-CNTR
            //                    ,:P56-DESC-MOT-CAMBIO-CN-VCHAR
            //                     :IND-P56-DESC-MOT-CAMBIO-CN
            //                    ,:P56-COD-SOGG
            //                     :IND-P56-COD-SOGG
            //                  )
            //           END-EXEC
            questAdegVerDao.insertRec(this);
            // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
            a100CheckReturnCode();
        }
    }

    /**Original name: A230-UPDATE-PK<br>*/
    private void a230UpdatePk() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE QUEST_ADEG_VER SET
        //                   ID_QUEST_ADEG_VER      =
        //                :P56-ID-QUEST-ADEG-VER
        //                  ,COD_COMP_ANIA          =
        //                :P56-COD-COMP-ANIA
        //                  ,ID_MOVI_CRZ            =
        //                :P56-ID-MOVI-CRZ
        //                  ,ID_RAPP_ANA            =
        //                :P56-ID-RAPP-ANA
        //                                       :IND-P56-ID-RAPP-ANA
        //                  ,ID_POLI                =
        //                :P56-ID-POLI
        //                  ,NATURA_OPRZ            =
        //                :P56-NATURA-OPRZ
        //                                       :IND-P56-NATURA-OPRZ
        //                  ,ORGN_FND               =
        //                :P56-ORGN-FND
        //                                       :IND-P56-ORGN-FND
        //                  ,COD_QLFC_PROF          =
        //                :P56-COD-QLFC-PROF
        //                                       :IND-P56-COD-QLFC-PROF
        //                  ,COD_NAZ_QLFC_PROF      =
        //                :P56-COD-NAZ-QLFC-PROF
        //                                       :IND-P56-COD-NAZ-QLFC-PROF
        //                  ,COD_PRV_QLFC_PROF      =
        //                :P56-COD-PRV-QLFC-PROF
        //                                       :IND-P56-COD-PRV-QLFC-PROF
        //                  ,FNT_REDD               =
        //                :P56-FNT-REDD
        //                                       :IND-P56-FNT-REDD
        //                  ,REDD_FATT_ANNU         =
        //                :P56-REDD-FATT-ANNU
        //                                       :IND-P56-REDD-FATT-ANNU
        //                  ,COD_ATECO              =
        //                :P56-COD-ATECO
        //                                       :IND-P56-COD-ATECO
        //                  ,VALUT_COLL             =
        //                :P56-VALUT-COLL
        //                                       :IND-P56-VALUT-COLL
        //                  ,DS_OPER_SQL            =
        //                :P56-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :P56-DS-VER
        //                  ,DS_TS_CPTZ             =
        //                :P56-DS-TS-CPTZ
        //                  ,DS_UTENTE              =
        //                :P56-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :P56-DS-STATO-ELAB
        //                  ,LUOGO_COSTITUZIONE     =
        //                :P56-LUOGO-COSTITUZIONE-VCHAR
        //                                       :IND-P56-LUOGO-COSTITUZIONE
        //                  ,TP_MOVI                =
        //                :P56-TP-MOVI
        //                                       :IND-P56-TP-MOVI
        //                  ,FL_RAG_RAPP            =
        //                :P56-FL-RAG-RAPP
        //                                       :IND-P56-FL-RAG-RAPP
        //                  ,FL_PRSZ_TIT_EFF        =
        //                :P56-FL-PRSZ-TIT-EFF
        //                                       :IND-P56-FL-PRSZ-TIT-EFF
        //                  ,TP_MOT_RISC            =
        //                :P56-TP-MOT-RISC
        //                                       :IND-P56-TP-MOT-RISC
        //                  ,TP_PNT_VND             =
        //                :P56-TP-PNT-VND
        //                                       :IND-P56-TP-PNT-VND
        //                  ,TP_ADEG_VER            =
        //                :P56-TP-ADEG-VER
        //                                       :IND-P56-TP-ADEG-VER
        //                  ,TP_RELA_ESEC           =
        //                :P56-TP-RELA-ESEC
        //                                       :IND-P56-TP-RELA-ESEC
        //                  ,TP_SCO_FIN_RAPP        =
        //                :P56-TP-SCO-FIN-RAPP
        //                                       :IND-P56-TP-SCO-FIN-RAPP
        //                  ,FL_PRSZ_3O_PAGAT       =
        //                :P56-FL-PRSZ-3O-PAGAT
        //                                       :IND-P56-FL-PRSZ-3O-PAGAT
        //                  ,AREA_GEO_PROV_FND      =
        //                :P56-AREA-GEO-PROV-FND
        //                                       :IND-P56-AREA-GEO-PROV-FND
        //                  ,TP_DEST_FND            =
        //                :P56-TP-DEST-FND
        //                                       :IND-P56-TP-DEST-FND
        //                  ,FL_PAESE_RESID_AUT     =
        //                :P56-FL-PAESE-RESID-AUT
        //                                       :IND-P56-FL-PAESE-RESID-AUT
        //                  ,FL_PAESE_CIT_AUT       =
        //                :P56-FL-PAESE-CIT-AUT
        //                                       :IND-P56-FL-PAESE-CIT-AUT
        //                  ,FL_PAESE_NAZ_AUT       =
        //                :P56-FL-PAESE-NAZ-AUT
        //                                       :IND-P56-FL-PAESE-NAZ-AUT
        //                  ,COD_PROF_PREC          =
        //                :P56-COD-PROF-PREC
        //                                       :IND-P56-COD-PROF-PREC
        //                  ,FL_AUT_PEP             =
        //                :P56-FL-AUT-PEP
        //                                       :IND-P56-FL-AUT-PEP
        //                  ,FL_IMP_CAR_PUB         =
        //                :P56-FL-IMP-CAR-PUB
        //                                       :IND-P56-FL-IMP-CAR-PUB
        //                  ,FL_LIS_TERR_SORV       =
        //                :P56-FL-LIS-TERR-SORV
        //                                       :IND-P56-FL-LIS-TERR-SORV
        //                  ,TP_SIT_FIN_PAT         =
        //                :P56-TP-SIT-FIN-PAT
        //                                       :IND-P56-TP-SIT-FIN-PAT
        //                  ,TP_SIT_FIN_PAT_CON     =
        //                :P56-TP-SIT-FIN-PAT-CON
        //                                       :IND-P56-TP-SIT-FIN-PAT-CON
        //                  ,IMP_TOT_AFF_UTIL       =
        //                :P56-IMP-TOT-AFF-UTIL
        //                                       :IND-P56-IMP-TOT-AFF-UTIL
        //                  ,IMP_TOT_FIN_UTIL       =
        //                :P56-IMP-TOT-FIN-UTIL
        //                                       :IND-P56-IMP-TOT-FIN-UTIL
        //                  ,IMP_TOT_AFF_ACC        =
        //                :P56-IMP-TOT-AFF-ACC
        //                                       :IND-P56-IMP-TOT-AFF-ACC
        //                  ,IMP_TOT_FIN_ACC        =
        //                :P56-IMP-TOT-FIN-ACC
        //                                       :IND-P56-IMP-TOT-FIN-ACC
        //                  ,TP_FRM_GIUR_SAV        =
        //                :P56-TP-FRM-GIUR-SAV
        //                                       :IND-P56-TP-FRM-GIUR-SAV
        //                  ,REG_COLL_POLI          =
        //                :P56-REG-COLL-POLI
        //                                       :IND-P56-REG-COLL-POLI
        //                  ,NUM_TEL                =
        //                :P56-NUM-TEL
        //                                       :IND-P56-NUM-TEL
        //                  ,NUM_DIP                =
        //                :P56-NUM-DIP
        //                                       :IND-P56-NUM-DIP
        //                  ,TP_SIT_FAM_CONV        =
        //                :P56-TP-SIT-FAM-CONV
        //                                       :IND-P56-TP-SIT-FAM-CONV
        //                  ,COD_PROF_CON           =
        //                :P56-COD-PROF-CON
        //                                       :IND-P56-COD-PROF-CON
        //                  ,FL_ES_PROC_PEN         =
        //                :P56-FL-ES-PROC-PEN
        //                                       :IND-P56-FL-ES-PROC-PEN
        //                  ,TP_COND_CLIENTE        =
        //                :P56-TP-COND-CLIENTE
        //                                       :IND-P56-TP-COND-CLIENTE
        //                  ,COD_SAE                =
        //                :P56-COD-SAE
        //                                       :IND-P56-COD-SAE
        //                  ,TP_OPER_ESTERO         =
        //                :P56-TP-OPER-ESTERO
        //                                       :IND-P56-TP-OPER-ESTERO
        //                  ,STAT_OPER_ESTERO       =
        //                :P56-STAT-OPER-ESTERO
        //                                       :IND-P56-STAT-OPER-ESTERO
        //                  ,COD_PRV_SVOL_ATT       =
        //                :P56-COD-PRV-SVOL-ATT
        //                                       :IND-P56-COD-PRV-SVOL-ATT
        //                  ,COD_STAT_SVOL_ATT      =
        //                :P56-COD-STAT-SVOL-ATT
        //                                       :IND-P56-COD-STAT-SVOL-ATT
        //                  ,TP_SOC                 =
        //                :P56-TP-SOC
        //                                       :IND-P56-TP-SOC
        //                  ,FL_IND_SOC_QUOT        =
        //                :P56-FL-IND-SOC-QUOT
        //                                       :IND-P56-FL-IND-SOC-QUOT
        //                  ,TP_SIT_GIUR            =
        //                :P56-TP-SIT-GIUR
        //                                       :IND-P56-TP-SIT-GIUR
        //                  ,PC_QUO_DET_TIT_EFF     =
        //                :P56-PC-QUO-DET-TIT-EFF
        //                                       :IND-P56-PC-QUO-DET-TIT-EFF
        //                  ,TP_PRFL_RSH_PEP        =
        //                :P56-TP-PRFL-RSH-PEP
        //                                       :IND-P56-TP-PRFL-RSH-PEP
        //                  ,TP_PEP                 =
        //                :P56-TP-PEP
        //                                       :IND-P56-TP-PEP
        //                  ,FL_NOT_PREG            =
        //                :P56-FL-NOT-PREG
        //                                       :IND-P56-FL-NOT-PREG
        //                  ,DT_INI_FNT_REDD        =
        //           :P56-DT-INI-FNT-REDD-DB
        //                                       :IND-P56-DT-INI-FNT-REDD
        //                  ,FNT_REDD_2             =
        //                :P56-FNT-REDD-2
        //                                       :IND-P56-FNT-REDD-2
        //                  ,DT_INI_FNT_REDD_2      =
        //           :P56-DT-INI-FNT-REDD-2-DB
        //                                       :IND-P56-DT-INI-FNT-REDD-2
        //                  ,FNT_REDD_3             =
        //                :P56-FNT-REDD-3
        //                                       :IND-P56-FNT-REDD-3
        //                  ,DT_INI_FNT_REDD_3      =
        //           :P56-DT-INI-FNT-REDD-3-DB
        //                                       :IND-P56-DT-INI-FNT-REDD-3
        //                  ,MOT_ASS_TIT_EFF        =
        //                :P56-MOT-ASS-TIT-EFF-VCHAR
        //                                       :IND-P56-MOT-ASS-TIT-EFF
        //                  ,FIN_COSTITUZIONE       =
        //                :P56-FIN-COSTITUZIONE-VCHAR
        //                                       :IND-P56-FIN-COSTITUZIONE
        //                  ,DESC_IMP_CAR_PUB       =
        //                :P56-DESC-IMP-CAR-PUB-VCHAR
        //                                       :IND-P56-DESC-IMP-CAR-PUB
        //                  ,DESC_SCO_FIN_RAPP      =
        //                :P56-DESC-SCO-FIN-RAPP-VCHAR
        //                                       :IND-P56-DESC-SCO-FIN-RAPP
        //                  ,DESC_PROC_PNL          =
        //                :P56-DESC-PROC-PNL-VCHAR
        //                                       :IND-P56-DESC-PROC-PNL
        //                  ,DESC_NOT_PREG          =
        //                :P56-DESC-NOT-PREG-VCHAR
        //                                       :IND-P56-DESC-NOT-PREG
        //                  ,ID_ASSICURATI          =
        //                :P56-ID-ASSICURATI
        //                                       :IND-P56-ID-ASSICURATI
        //                  ,REDD_CON               =
        //                :P56-REDD-CON
        //                                       :IND-P56-REDD-CON
        //                  ,DESC_LIB_MOT_RISC      =
        //                :P56-DESC-LIB-MOT-RISC-VCHAR
        //                                       :IND-P56-DESC-LIB-MOT-RISC
        //                  ,TP_MOT_ASS_TIT_EFF     =
        //                :P56-TP-MOT-ASS-TIT-EFF
        //                                       :IND-P56-TP-MOT-ASS-TIT-EFF
        //                  ,TP_RAG_RAPP            =
        //                :P56-TP-RAG-RAPP
        //                                       :IND-P56-TP-RAG-RAPP
        //                  ,COD_CAN                =
        //                :P56-COD-CAN
        //                                       :IND-P56-COD-CAN
        //                  ,TP_FIN_COST            =
        //                :P56-TP-FIN-COST
        //                                       :IND-P56-TP-FIN-COST
        //                  ,NAZ_DEST_FND           =
        //                :P56-NAZ-DEST-FND
        //                                       :IND-P56-NAZ-DEST-FND
        //                  ,FL_AU_FATCA_AEOI       =
        //                :P56-FL-AU-FATCA-AEOI
        //                                       :IND-P56-FL-AU-FATCA-AEOI
        //                  ,TP_CAR_FIN_GIUR        =
        //                :P56-TP-CAR-FIN-GIUR
        //                                       :IND-P56-TP-CAR-FIN-GIUR
        //                  ,TP_CAR_FIN_GIUR_AT     =
        //                :P56-TP-CAR-FIN-GIUR-AT
        //                                       :IND-P56-TP-CAR-FIN-GIUR-AT
        //                  ,TP_CAR_FIN_GIUR_PA     =
        //                :P56-TP-CAR-FIN-GIUR-PA
        //                                       :IND-P56-TP-CAR-FIN-GIUR-PA
        //                  ,FL_ISTITUZ_FIN         =
        //                :P56-FL-ISTITUZ-FIN
        //                                       :IND-P56-FL-ISTITUZ-FIN
        //                  ,TP_ORI_FND_TIT_EFF     =
        //                :P56-TP-ORI-FND-TIT-EFF
        //                                       :IND-P56-TP-ORI-FND-TIT-EFF
        //                  ,PC_ESP_AG_PA_MSC       =
        //                :P56-PC-ESP-AG-PA-MSC
        //                                       :IND-P56-PC-ESP-AG-PA-MSC
        //                  ,FL_PR_TR_USA           =
        //                :P56-FL-PR-TR-USA
        //                                       :IND-P56-FL-PR-TR-USA
        //                  ,FL_PR_TR_NO_USA        =
        //                :P56-FL-PR-TR-NO-USA
        //                                       :IND-P56-FL-PR-TR-NO-USA
        //                  ,PC_RIP_PAT_AS_VITA     =
        //                :P56-PC-RIP-PAT-AS-VITA
        //                                       :IND-P56-PC-RIP-PAT-AS-VITA
        //                  ,PC_RIP_PAT_IM          =
        //                :P56-PC-RIP-PAT-IM
        //                                       :IND-P56-PC-RIP-PAT-IM
        //                  ,PC_RIP_PAT_SET_IM      =
        //                :P56-PC-RIP-PAT-SET-IM
        //                                       :IND-P56-PC-RIP-PAT-SET-IM
        //                  ,TP_STATUS_AEOI         =
        //                :P56-TP-STATUS-AEOI
        //                                       :IND-P56-TP-STATUS-AEOI
        //                  ,TP_STATUS_FATCA        =
        //                :P56-TP-STATUS-FATCA
        //                                       :IND-P56-TP-STATUS-FATCA
        //                  ,FL_RAPP_PA_MSC         =
        //                :P56-FL-RAPP-PA-MSC
        //                                       :IND-P56-FL-RAPP-PA-MSC
        //                  ,COD_COMUN_SVOL_ATT     =
        //                :P56-COD-COMUN-SVOL-ATT
        //                                       :IND-P56-COD-COMUN-SVOL-ATT
        //                  ,TP_DT_1O_CON_CLI       =
        //                :P56-TP-DT-1O-CON-CLI
        //                                       :IND-P56-TP-DT-1O-CON-CLI
        //                  ,TP_MOD_EN_RELA_INT     =
        //                :P56-TP-MOD-EN-RELA-INT
        //                                       :IND-P56-TP-MOD-EN-RELA-INT
        //                  ,TP_REDD_ANNU_LRD       =
        //                :P56-TP-REDD-ANNU-LRD
        //                                       :IND-P56-TP-REDD-ANNU-LRD
        //                  ,TP_REDD_CON            =
        //                :P56-TP-REDD-CON
        //                                       :IND-P56-TP-REDD-CON
        //                  ,TP_OPER_SOC_FID        =
        //                :P56-TP-OPER-SOC-FID
        //                                       :IND-P56-TP-OPER-SOC-FID
        //                  ,COD_PA_ESP_MSC_1       =
        //                :P56-COD-PA-ESP-MSC-1
        //                                       :IND-P56-COD-PA-ESP-MSC-1
        //                  ,IMP_PA_ESP_MSC_1       =
        //                :P56-IMP-PA-ESP-MSC-1
        //                                       :IND-P56-IMP-PA-ESP-MSC-1
        //                  ,COD_PA_ESP_MSC_2       =
        //                :P56-COD-PA-ESP-MSC-2
        //                                       :IND-P56-COD-PA-ESP-MSC-2
        //                  ,IMP_PA_ESP_MSC_2       =
        //                :P56-IMP-PA-ESP-MSC-2
        //                                       :IND-P56-IMP-PA-ESP-MSC-2
        //                  ,COD_PA_ESP_MSC_3       =
        //                :P56-COD-PA-ESP-MSC-3
        //                                       :IND-P56-COD-PA-ESP-MSC-3
        //                  ,IMP_PA_ESP_MSC_3       =
        //                :P56-IMP-PA-ESP-MSC-3
        //                                       :IND-P56-IMP-PA-ESP-MSC-3
        //                  ,COD_PA_ESP_MSC_4       =
        //                :P56-COD-PA-ESP-MSC-4
        //                                       :IND-P56-COD-PA-ESP-MSC-4
        //                  ,IMP_PA_ESP_MSC_4       =
        //                :P56-IMP-PA-ESP-MSC-4
        //                                       :IND-P56-IMP-PA-ESP-MSC-4
        //                  ,COD_PA_ESP_MSC_5       =
        //                :P56-COD-PA-ESP-MSC-5
        //                                       :IND-P56-COD-PA-ESP-MSC-5
        //                  ,IMP_PA_ESP_MSC_5       =
        //                :P56-IMP-PA-ESP-MSC-5
        //                                       :IND-P56-IMP-PA-ESP-MSC-5
        //                  ,DESC_ORGN_FND          =
        //                :P56-DESC-ORGN-FND-VCHAR
        //                                       :IND-P56-DESC-ORGN-FND
        //                  ,COD_AUT_DUE_DIL        =
        //                :P56-COD-AUT-DUE-DIL
        //                                       :IND-P56-COD-AUT-DUE-DIL
        //                  ,FL_PR_QUEST_FATCA      =
        //                :P56-FL-PR-QUEST-FATCA
        //                                       :IND-P56-FL-PR-QUEST-FATCA
        //                  ,FL_PR_QUEST_AEOI       =
        //                :P56-FL-PR-QUEST-AEOI
        //                                       :IND-P56-FL-PR-QUEST-AEOI
        //                  ,FL_PR_QUEST_OFAC       =
        //                :P56-FL-PR-QUEST-OFAC
        //                                       :IND-P56-FL-PR-QUEST-OFAC
        //                  ,FL_PR_QUEST_KYC        =
        //                :P56-FL-PR-QUEST-KYC
        //                                       :IND-P56-FL-PR-QUEST-KYC
        //                  ,FL_PR_QUEST_MSCQ       =
        //                :P56-FL-PR-QUEST-MSCQ
        //                                       :IND-P56-FL-PR-QUEST-MSCQ
        //                  ,TP_NOT_PREG            =
        //                :P56-TP-NOT-PREG
        //                                       :IND-P56-TP-NOT-PREG
        //                  ,TP_PROC_PNL            =
        //                :P56-TP-PROC-PNL
        //                                       :IND-P56-TP-PROC-PNL
        //                  ,COD_IMP_CAR_PUB        =
        //                :P56-COD-IMP-CAR-PUB
        //                                       :IND-P56-COD-IMP-CAR-PUB
        //                  ,OPRZ_SOSPETTE          =
        //                :P56-OPRZ-SOSPETTE
        //                                       :IND-P56-OPRZ-SOSPETTE
        //                  ,ULT_FATT_ANNU          =
        //                :P56-ULT-FATT-ANNU
        //                                       :IND-P56-ULT-FATT-ANNU
        //                  ,DESC_PEP               =
        //                :P56-DESC-PEP-VCHAR
        //                                       :IND-P56-DESC-PEP
        //                  ,NUM_TEL_2              =
        //                :P56-NUM-TEL-2
        //                                       :IND-P56-NUM-TEL-2
        //                  ,IMP_AFI                =
        //                :P56-IMP-AFI
        //                                       :IND-P56-IMP-AFI
        //                  ,FL_NEW_PRO             =
        //                :P56-FL-NEW-PRO
        //                                       :IND-P56-FL-NEW-PRO
        //                  ,TP_MOT_CAMBIO_CNTR     =
        //                :P56-TP-MOT-CAMBIO-CNTR
        //                                       :IND-P56-TP-MOT-CAMBIO-CNTR
        //                  ,DESC_MOT_CAMBIO_CN     =
        //                :P56-DESC-MOT-CAMBIO-CN-VCHAR
        //                                       :IND-P56-DESC-MOT-CAMBIO-CN
        //                  ,COD_SOGG               =
        //                :P56-COD-SOGG
        //                                       :IND-P56-COD-SOGG
        //                WHERE     ID_QUEST_ADEG_VER = :P56-ID-QUEST-ADEG-VER
        //           END-EXEC.
        questAdegVerDao.updateRec(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A240-DELETE-PK<br>*/
    private void a240DeletePk() {
        // COB_CODE: EXEC SQL
        //                DELETE
        //                FROM QUEST_ADEG_VER
        //                WHERE     ID_QUEST_ADEG_VER = :P56-ID-QUEST-ADEG-VER
        //           END-EXEC.
        questAdegVerDao.deleteByP56IdQuestAdegVer(questAdegVer.getP56IdQuestAdegVer());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A310-SELECT-ID-EFF<br>*/
    private void a310SelectIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A405-DECLARE-CURSOR-IDP-EFF<br>
	 * <pre>----
	 * ----  gestione IDP Effetto
	 * ----</pre>*/
    private void a405DeclareCursorIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A410-SELECT-IDP-EFF<br>*/
    private void a410SelectIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A460-OPEN-CURSOR-IDP-EFF<br>*/
    private void a460OpenCursorIdpEff() {
        // COB_CODE: PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.
        a405DeclareCursorIdpEff();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A470-CLOSE-CURSOR-IDP-EFF<br>*/
    private void a470CloseCursorIdpEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A480-FETCH-FIRST-IDP-EFF<br>*/
    private void a480FetchFirstIdpEff() {
        // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.
        a460OpenCursorIdpEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
            a490FetchNextIdpEff();
        }
    }

    /**Original name: A490-FETCH-NEXT-IDP-EFF<br>*/
    private void a490FetchNextIdpEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A505-DECLARE-CURSOR-IBO<br>
	 * <pre>----
	 * ----  gestione IBO Effetto e non
	 * ----</pre>*/
    private void a505DeclareCursorIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A510-SELECT-IBO<br>*/
    private void a510SelectIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A560-OPEN-CURSOR-IBO<br>*/
    private void a560OpenCursorIbo() {
        // COB_CODE: PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.
        a505DeclareCursorIbo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A570-CLOSE-CURSOR-IBO<br>*/
    private void a570CloseCursorIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A580-FETCH-FIRST-IBO<br>*/
    private void a580FetchFirstIbo() {
        // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.
        a560OpenCursorIbo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
            a590FetchNextIbo();
        }
    }

    /**Original name: A590-FETCH-NEXT-IBO<br>*/
    private void a590FetchNextIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A605-DECLARE-CURSOR-IBS<br>
	 * <pre>----
	 * ----  gestione IBS Effetto e non
	 * ----</pre>*/
    private void a605DeclareCursorIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A610-SELECT-IBS<br>*/
    private void a610SelectIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A660-OPEN-CURSOR-IBS<br>*/
    private void a660OpenCursorIbs() {
        // COB_CODE: PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.
        a605DeclareCursorIbs();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A670-CLOSE-CURSOR-IBS<br>*/
    private void a670CloseCursorIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A680-FETCH-FIRST-IBS<br>*/
    private void a680FetchFirstIbs() {
        // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.
        a660OpenCursorIbs();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
            a690FetchNextIbs();
        }
    }

    /**Original name: A690-FETCH-NEXT-IBS<br>*/
    private void a690FetchNextIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A705-DECLARE-CURSOR-IDO<br>
	 * <pre>----
	 * ----  gestione IDO Effetto e non
	 * ----</pre>*/
    private void a705DeclareCursorIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A710-SELECT-IDO<br>*/
    private void a710SelectIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A760-OPEN-CURSOR-IDO<br>*/
    private void a760OpenCursorIdo() {
        // COB_CODE: PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.
        a705DeclareCursorIdo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A770-CLOSE-CURSOR-IDO<br>*/
    private void a770CloseCursorIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A780-FETCH-FIRST-IDO<br>*/
    private void a780FetchFirstIdo() {
        // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.
        a760OpenCursorIdo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
            a790FetchNextIdo();
        }
    }

    /**Original name: A790-FETCH-NEXT-IDO<br>*/
    private void a790FetchNextIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B310-SELECT-ID-CPZ<br>
	 * <pre>----
	 * ----  gestione ID Competenza
	 * ----</pre>*/
    private void b310SelectIdCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B405-DECLARE-CURSOR-IDP-CPZ<br>
	 * <pre>----
	 * ----  gestione IDP Competenza
	 * ----</pre>*/
    private void b405DeclareCursorIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B410-SELECT-IDP-CPZ<br>*/
    private void b410SelectIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B460-OPEN-CURSOR-IDP-CPZ<br>*/
    private void b460OpenCursorIdpCpz() {
        // COB_CODE: PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.
        b405DeclareCursorIdpCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B470-CLOSE-CURSOR-IDP-CPZ<br>*/
    private void b470CloseCursorIdpCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B480-FETCH-FIRST-IDP-CPZ<br>*/
    private void b480FetchFirstIdpCpz() {
        // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.
        b460OpenCursorIdpCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
            b490FetchNextIdpCpz();
        }
    }

    /**Original name: B490-FETCH-NEXT-IDP-CPZ<br>*/
    private void b490FetchNextIdpCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B505-DECLARE-CURSOR-IBO-CPZ<br>
	 * <pre>----
	 * ----  gestione IBO Competenza
	 * ----</pre>*/
    private void b505DeclareCursorIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B510-SELECT-IBO-CPZ<br>*/
    private void b510SelectIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B560-OPEN-CURSOR-IBO-CPZ<br>*/
    private void b560OpenCursorIboCpz() {
        // COB_CODE: PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.
        b505DeclareCursorIboCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B570-CLOSE-CURSOR-IBO-CPZ<br>*/
    private void b570CloseCursorIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B580-FETCH-FIRST-IBO-CPZ<br>*/
    private void b580FetchFirstIboCpz() {
        // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.
        b560OpenCursorIboCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
    }

    /**Original name: B590-FETCH-NEXT-IBO-CPZ<br>*/
    private void b590FetchNextIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B605-DECLARE-CURSOR-IBS-CPZ<br>
	 * <pre>----
	 * ----  gestione IBS Competenza
	 * ----</pre>*/
    private void b605DeclareCursorIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B610-SELECT-IBS-CPZ<br>*/
    private void b610SelectIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B660-OPEN-CURSOR-IBS-CPZ<br>*/
    private void b660OpenCursorIbsCpz() {
        // COB_CODE: PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.
        b605DeclareCursorIbsCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B670-CLOSE-CURSOR-IBS-CPZ<br>*/
    private void b670CloseCursorIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B680-FETCH-FIRST-IBS-CPZ<br>*/
    private void b680FetchFirstIbsCpz() {
        // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.
        b660OpenCursorIbsCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
    }

    /**Original name: B690-FETCH-NEXT-IBS-CPZ<br>*/
    private void b690FetchNextIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B705-DECLARE-CURSOR-IDO-CPZ<br>
	 * <pre>----
	 * ----  gestione IDO Competenza
	 * ----</pre>*/
    private void b705DeclareCursorIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B710-SELECT-IDO-CPZ<br>*/
    private void b710SelectIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B760-OPEN-CURSOR-IDO-CPZ<br>*/
    private void b760OpenCursorIdoCpz() {
        // COB_CODE: PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.
        b705DeclareCursorIdoCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B770-CLOSE-CURSOR-IDO-CPZ<br>*/
    private void b770CloseCursorIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B780-FETCH-FIRST-IDO-CPZ<br>*/
    private void b780FetchFirstIdoCpz() {
        // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.
        b760OpenCursorIdoCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
    }

    /**Original name: B790-FETCH-NEXT-IDO-CPZ<br>*/
    private void b790FetchNextIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-P56-ID-RAPP-ANA = -1
        //              MOVE HIGH-VALUES TO P56-ID-RAPP-ANA-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getIdRappAna() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-ID-RAPP-ANA-NULL
            questAdegVer.getP56IdRappAna().setP56IdRappAnaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P56IdRappAna.Len.P56_ID_RAPP_ANA_NULL));
        }
        // COB_CODE: IF IND-P56-NATURA-OPRZ = -1
        //              MOVE HIGH-VALUES TO P56-NATURA-OPRZ-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getNaturaOprz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-NATURA-OPRZ-NULL
            questAdegVer.setP56NaturaOprz(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_NATURA_OPRZ));
        }
        // COB_CODE: IF IND-P56-ORGN-FND = -1
        //              MOVE HIGH-VALUES TO P56-ORGN-FND-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getOrgnFnd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-ORGN-FND-NULL
            questAdegVer.setP56OrgnFnd(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_ORGN_FND));
        }
        // COB_CODE: IF IND-P56-COD-QLFC-PROF = -1
        //              MOVE HIGH-VALUES TO P56-COD-QLFC-PROF-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getCodQlfcProf() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-COD-QLFC-PROF-NULL
            questAdegVer.setP56CodQlfcProf(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_COD_QLFC_PROF));
        }
        // COB_CODE: IF IND-P56-COD-NAZ-QLFC-PROF = -1
        //              MOVE HIGH-VALUES TO P56-COD-NAZ-QLFC-PROF-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getCodNazQlfcProf() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-COD-NAZ-QLFC-PROF-NULL
            questAdegVer.setP56CodNazQlfcProf(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_COD_NAZ_QLFC_PROF));
        }
        // COB_CODE: IF IND-P56-COD-PRV-QLFC-PROF = -1
        //              MOVE HIGH-VALUES TO P56-COD-PRV-QLFC-PROF-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getCodPrvQlfcProf() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-COD-PRV-QLFC-PROF-NULL
            questAdegVer.setP56CodPrvQlfcProf(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_COD_PRV_QLFC_PROF));
        }
        // COB_CODE: IF IND-P56-FNT-REDD = -1
        //              MOVE HIGH-VALUES TO P56-FNT-REDD-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getFntRedd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-FNT-REDD-NULL
            questAdegVer.setP56FntRedd(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_FNT_REDD));
        }
        // COB_CODE: IF IND-P56-REDD-FATT-ANNU = -1
        //              MOVE HIGH-VALUES TO P56-REDD-FATT-ANNU-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getReddFattAnnu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-REDD-FATT-ANNU-NULL
            questAdegVer.getP56ReddFattAnnu().setP56ReddFattAnnuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P56ReddFattAnnu.Len.P56_REDD_FATT_ANNU_NULL));
        }
        // COB_CODE: IF IND-P56-COD-ATECO = -1
        //              MOVE HIGH-VALUES TO P56-COD-ATECO-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getCodAteco() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-COD-ATECO-NULL
            questAdegVer.setP56CodAteco(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_COD_ATECO));
        }
        // COB_CODE: IF IND-P56-VALUT-COLL = -1
        //              MOVE HIGH-VALUES TO P56-VALUT-COLL-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getValutColl() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-VALUT-COLL-NULL
            questAdegVer.setP56ValutColl(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_VALUT_COLL));
        }
        // COB_CODE: IF IND-P56-LUOGO-COSTITUZIONE = -1
        //              MOVE HIGH-VALUES TO P56-LUOGO-COSTITUZIONE
        //           END-IF
        if (ws.getIndQuestAdegVer().getLuogoCostituzione() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-LUOGO-COSTITUZIONE
            questAdegVer.setP56LuogoCostituzione(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_LUOGO_COSTITUZIONE));
        }
        // COB_CODE: IF IND-P56-TP-MOVI = -1
        //              MOVE HIGH-VALUES TO P56-TP-MOVI-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getTpMovi() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-TP-MOVI-NULL
            questAdegVer.getP56TpMovi().setP56TpMoviNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P56TpMovi.Len.P56_TP_MOVI_NULL));
        }
        // COB_CODE: IF IND-P56-FL-RAG-RAPP = -1
        //              MOVE HIGH-VALUES TO P56-FL-RAG-RAPP-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getFlRagRapp() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-FL-RAG-RAPP-NULL
            questAdegVer.setP56FlRagRapp(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-P56-FL-PRSZ-TIT-EFF = -1
        //              MOVE HIGH-VALUES TO P56-FL-PRSZ-TIT-EFF-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getFlPrszTitEff() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-FL-PRSZ-TIT-EFF-NULL
            questAdegVer.setP56FlPrszTitEff(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-P56-TP-MOT-RISC = -1
        //              MOVE HIGH-VALUES TO P56-TP-MOT-RISC-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getTpMotRisc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-TP-MOT-RISC-NULL
            questAdegVer.setP56TpMotRisc(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_TP_MOT_RISC));
        }
        // COB_CODE: IF IND-P56-TP-PNT-VND = -1
        //              MOVE HIGH-VALUES TO P56-TP-PNT-VND-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getTpPntVnd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-TP-PNT-VND-NULL
            questAdegVer.setP56TpPntVnd(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_TP_PNT_VND));
        }
        // COB_CODE: IF IND-P56-TP-ADEG-VER = -1
        //              MOVE HIGH-VALUES TO P56-TP-ADEG-VER-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getTpAdegVer() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-TP-ADEG-VER-NULL
            questAdegVer.setP56TpAdegVer(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_TP_ADEG_VER));
        }
        // COB_CODE: IF IND-P56-TP-RELA-ESEC = -1
        //              MOVE HIGH-VALUES TO P56-TP-RELA-ESEC-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getTpRelaEsec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-TP-RELA-ESEC-NULL
            questAdegVer.setP56TpRelaEsec(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_TP_RELA_ESEC));
        }
        // COB_CODE: IF IND-P56-TP-SCO-FIN-RAPP = -1
        //              MOVE HIGH-VALUES TO P56-TP-SCO-FIN-RAPP-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getTpScoFinRapp() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-TP-SCO-FIN-RAPP-NULL
            questAdegVer.setP56TpScoFinRapp(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_TP_SCO_FIN_RAPP));
        }
        // COB_CODE: IF IND-P56-FL-PRSZ-3O-PAGAT = -1
        //              MOVE HIGH-VALUES TO P56-FL-PRSZ-3O-PAGAT-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getFlPrsz3oPagat() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-FL-PRSZ-3O-PAGAT-NULL
            questAdegVer.setP56FlPrsz3oPagat(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-P56-AREA-GEO-PROV-FND = -1
        //              MOVE HIGH-VALUES TO P56-AREA-GEO-PROV-FND-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getAreaGeoProvFnd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-AREA-GEO-PROV-FND-NULL
            questAdegVer.setP56AreaGeoProvFnd(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_AREA_GEO_PROV_FND));
        }
        // COB_CODE: IF IND-P56-TP-DEST-FND = -1
        //              MOVE HIGH-VALUES TO P56-TP-DEST-FND-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getTpDestFnd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-TP-DEST-FND-NULL
            questAdegVer.setP56TpDestFnd(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_TP_DEST_FND));
        }
        // COB_CODE: IF IND-P56-FL-PAESE-RESID-AUT = -1
        //              MOVE HIGH-VALUES TO P56-FL-PAESE-RESID-AUT-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getFlPaeseResidAut() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-FL-PAESE-RESID-AUT-NULL
            questAdegVer.setP56FlPaeseResidAut(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-P56-FL-PAESE-CIT-AUT = -1
        //              MOVE HIGH-VALUES TO P56-FL-PAESE-CIT-AUT-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getFlPaeseCitAut() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-FL-PAESE-CIT-AUT-NULL
            questAdegVer.setP56FlPaeseCitAut(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-P56-FL-PAESE-NAZ-AUT = -1
        //              MOVE HIGH-VALUES TO P56-FL-PAESE-NAZ-AUT-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getFlPaeseNazAut() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-FL-PAESE-NAZ-AUT-NULL
            questAdegVer.setP56FlPaeseNazAut(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-P56-COD-PROF-PREC = -1
        //              MOVE HIGH-VALUES TO P56-COD-PROF-PREC-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getCodProfPrec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-COD-PROF-PREC-NULL
            questAdegVer.setP56CodProfPrec(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_COD_PROF_PREC));
        }
        // COB_CODE: IF IND-P56-FL-AUT-PEP = -1
        //              MOVE HIGH-VALUES TO P56-FL-AUT-PEP-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getFlAutPep() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-FL-AUT-PEP-NULL
            questAdegVer.setP56FlAutPep(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-P56-FL-IMP-CAR-PUB = -1
        //              MOVE HIGH-VALUES TO P56-FL-IMP-CAR-PUB-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getFlImpCarPub() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-FL-IMP-CAR-PUB-NULL
            questAdegVer.setP56FlImpCarPub(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-P56-FL-LIS-TERR-SORV = -1
        //              MOVE HIGH-VALUES TO P56-FL-LIS-TERR-SORV-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getFlLisTerrSorv() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-FL-LIS-TERR-SORV-NULL
            questAdegVer.setP56FlLisTerrSorv(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-P56-TP-SIT-FIN-PAT = -1
        //              MOVE HIGH-VALUES TO P56-TP-SIT-FIN-PAT-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getTpSitFinPat() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-TP-SIT-FIN-PAT-NULL
            questAdegVer.setP56TpSitFinPat(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_TP_SIT_FIN_PAT));
        }
        // COB_CODE: IF IND-P56-TP-SIT-FIN-PAT-CON = -1
        //              MOVE HIGH-VALUES TO P56-TP-SIT-FIN-PAT-CON-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getTpSitFinPatCon() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-TP-SIT-FIN-PAT-CON-NULL
            questAdegVer.setP56TpSitFinPatCon(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_TP_SIT_FIN_PAT_CON));
        }
        // COB_CODE: IF IND-P56-IMP-TOT-AFF-UTIL = -1
        //              MOVE HIGH-VALUES TO P56-IMP-TOT-AFF-UTIL-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getImpTotAffUtil() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-IMP-TOT-AFF-UTIL-NULL
            questAdegVer.getP56ImpTotAffUtil().setP56ImpTotAffUtilNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P56ImpTotAffUtil.Len.P56_IMP_TOT_AFF_UTIL_NULL));
        }
        // COB_CODE: IF IND-P56-IMP-TOT-FIN-UTIL = -1
        //              MOVE HIGH-VALUES TO P56-IMP-TOT-FIN-UTIL-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getImpTotFinUtil() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-IMP-TOT-FIN-UTIL-NULL
            questAdegVer.getP56ImpTotFinUtil().setP56ImpTotFinUtilNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P56ImpTotFinUtil.Len.P56_IMP_TOT_FIN_UTIL_NULL));
        }
        // COB_CODE: IF IND-P56-IMP-TOT-AFF-ACC = -1
        //              MOVE HIGH-VALUES TO P56-IMP-TOT-AFF-ACC-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getImpTotAffAcc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-IMP-TOT-AFF-ACC-NULL
            questAdegVer.getP56ImpTotAffAcc().setP56ImpTotAffAccNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P56ImpTotAffAcc.Len.P56_IMP_TOT_AFF_ACC_NULL));
        }
        // COB_CODE: IF IND-P56-IMP-TOT-FIN-ACC = -1
        //              MOVE HIGH-VALUES TO P56-IMP-TOT-FIN-ACC-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getImpTotFinAcc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-IMP-TOT-FIN-ACC-NULL
            questAdegVer.getP56ImpTotFinAcc().setP56ImpTotFinAccNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P56ImpTotFinAcc.Len.P56_IMP_TOT_FIN_ACC_NULL));
        }
        // COB_CODE: IF IND-P56-TP-FRM-GIUR-SAV = -1
        //              MOVE HIGH-VALUES TO P56-TP-FRM-GIUR-SAV-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getTpFrmGiurSav() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-TP-FRM-GIUR-SAV-NULL
            questAdegVer.setP56TpFrmGiurSav(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_TP_FRM_GIUR_SAV));
        }
        // COB_CODE: IF IND-P56-REG-COLL-POLI = -1
        //              MOVE HIGH-VALUES TO P56-REG-COLL-POLI-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getRegCollPoli() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-REG-COLL-POLI-NULL
            questAdegVer.setP56RegCollPoli(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_REG_COLL_POLI));
        }
        // COB_CODE: IF IND-P56-NUM-TEL = -1
        //              MOVE HIGH-VALUES TO P56-NUM-TEL-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getNumTel() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-NUM-TEL-NULL
            questAdegVer.setP56NumTel(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_NUM_TEL));
        }
        // COB_CODE: IF IND-P56-NUM-DIP = -1
        //              MOVE HIGH-VALUES TO P56-NUM-DIP-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getNumDip() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-NUM-DIP-NULL
            questAdegVer.getP56NumDip().setP56NumDipNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P56NumDip.Len.P56_NUM_DIP_NULL));
        }
        // COB_CODE: IF IND-P56-TP-SIT-FAM-CONV = -1
        //              MOVE HIGH-VALUES TO P56-TP-SIT-FAM-CONV-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getTpSitFamConv() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-TP-SIT-FAM-CONV-NULL
            questAdegVer.setP56TpSitFamConv(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_TP_SIT_FAM_CONV));
        }
        // COB_CODE: IF IND-P56-COD-PROF-CON = -1
        //              MOVE HIGH-VALUES TO P56-COD-PROF-CON-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getCodProfCon() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-COD-PROF-CON-NULL
            questAdegVer.setP56CodProfCon(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_COD_PROF_CON));
        }
        // COB_CODE: IF IND-P56-FL-ES-PROC-PEN = -1
        //              MOVE HIGH-VALUES TO P56-FL-ES-PROC-PEN-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getFlEsProcPen() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-FL-ES-PROC-PEN-NULL
            questAdegVer.setP56FlEsProcPen(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-P56-TP-COND-CLIENTE = -1
        //              MOVE HIGH-VALUES TO P56-TP-COND-CLIENTE-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getTpCondCliente() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-TP-COND-CLIENTE-NULL
            questAdegVer.setP56TpCondCliente(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_TP_COND_CLIENTE));
        }
        // COB_CODE: IF IND-P56-COD-SAE = -1
        //              MOVE HIGH-VALUES TO P56-COD-SAE-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getCodSae() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-COD-SAE-NULL
            questAdegVer.setP56CodSae(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_COD_SAE));
        }
        // COB_CODE: IF IND-P56-TP-OPER-ESTERO = -1
        //              MOVE HIGH-VALUES TO P56-TP-OPER-ESTERO-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getTpOperEstero() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-TP-OPER-ESTERO-NULL
            questAdegVer.setP56TpOperEstero(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_TP_OPER_ESTERO));
        }
        // COB_CODE: IF IND-P56-STAT-OPER-ESTERO = -1
        //              MOVE HIGH-VALUES TO P56-STAT-OPER-ESTERO-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getStatOperEstero() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-STAT-OPER-ESTERO-NULL
            questAdegVer.setP56StatOperEstero(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_STAT_OPER_ESTERO));
        }
        // COB_CODE: IF IND-P56-COD-PRV-SVOL-ATT = -1
        //              MOVE HIGH-VALUES TO P56-COD-PRV-SVOL-ATT-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getCodPrvSvolAtt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-COD-PRV-SVOL-ATT-NULL
            questAdegVer.setP56CodPrvSvolAtt(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_COD_PRV_SVOL_ATT));
        }
        // COB_CODE: IF IND-P56-COD-STAT-SVOL-ATT = -1
        //              MOVE HIGH-VALUES TO P56-COD-STAT-SVOL-ATT-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getCodStatSvolAtt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-COD-STAT-SVOL-ATT-NULL
            questAdegVer.setP56CodStatSvolAtt(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_COD_STAT_SVOL_ATT));
        }
        // COB_CODE: IF IND-P56-TP-SOC = -1
        //              MOVE HIGH-VALUES TO P56-TP-SOC-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getTpSoc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-TP-SOC-NULL
            questAdegVer.setP56TpSoc(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_TP_SOC));
        }
        // COB_CODE: IF IND-P56-FL-IND-SOC-QUOT = -1
        //              MOVE HIGH-VALUES TO P56-FL-IND-SOC-QUOT-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getFlIndSocQuot() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-FL-IND-SOC-QUOT-NULL
            questAdegVer.setP56FlIndSocQuot(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-P56-TP-SIT-GIUR = -1
        //              MOVE HIGH-VALUES TO P56-TP-SIT-GIUR-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getTpSitGiur() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-TP-SIT-GIUR-NULL
            questAdegVer.setP56TpSitGiur(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_TP_SIT_GIUR));
        }
        // COB_CODE: IF IND-P56-PC-QUO-DET-TIT-EFF = -1
        //              MOVE HIGH-VALUES TO P56-PC-QUO-DET-TIT-EFF-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getPcQuoDetTitEff() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-PC-QUO-DET-TIT-EFF-NULL
            questAdegVer.getP56PcQuoDetTitEff().setP56PcQuoDetTitEffNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P56PcQuoDetTitEff.Len.P56_PC_QUO_DET_TIT_EFF_NULL));
        }
        // COB_CODE: IF IND-P56-TP-PRFL-RSH-PEP = -1
        //              MOVE HIGH-VALUES TO P56-TP-PRFL-RSH-PEP-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getTpPrflRshPep() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-TP-PRFL-RSH-PEP-NULL
            questAdegVer.setP56TpPrflRshPep(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_TP_PRFL_RSH_PEP));
        }
        // COB_CODE: IF IND-P56-TP-PEP = -1
        //              MOVE HIGH-VALUES TO P56-TP-PEP-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getTpPep() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-TP-PEP-NULL
            questAdegVer.setP56TpPep(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_TP_PEP));
        }
        // COB_CODE: IF IND-P56-FL-NOT-PREG = -1
        //              MOVE HIGH-VALUES TO P56-FL-NOT-PREG-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getFlNotPreg() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-FL-NOT-PREG-NULL
            questAdegVer.setP56FlNotPreg(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-P56-DT-INI-FNT-REDD = -1
        //              MOVE HIGH-VALUES TO P56-DT-INI-FNT-REDD-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getDtIniFntRedd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-DT-INI-FNT-REDD-NULL
            questAdegVer.getP56DtIniFntRedd().setP56DtIniFntReddNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P56DtIniFntRedd.Len.P56_DT_INI_FNT_REDD_NULL));
        }
        // COB_CODE: IF IND-P56-FNT-REDD-2 = -1
        //              MOVE HIGH-VALUES TO P56-FNT-REDD-2-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getFntRedd2() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-FNT-REDD-2-NULL
            questAdegVer.setP56FntRedd2(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_FNT_REDD2));
        }
        // COB_CODE: IF IND-P56-DT-INI-FNT-REDD-2 = -1
        //              MOVE HIGH-VALUES TO P56-DT-INI-FNT-REDD-2-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getDtIniFntRedd2() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-DT-INI-FNT-REDD-2-NULL
            questAdegVer.getP56DtIniFntRedd2().setP56DtIniFntRedd2Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P56DtIniFntRedd2.Len.P56_DT_INI_FNT_REDD2_NULL));
        }
        // COB_CODE: IF IND-P56-FNT-REDD-3 = -1
        //              MOVE HIGH-VALUES TO P56-FNT-REDD-3-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getFntRedd3() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-FNT-REDD-3-NULL
            questAdegVer.setP56FntRedd3(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_FNT_REDD3));
        }
        // COB_CODE: IF IND-P56-DT-INI-FNT-REDD-3 = -1
        //              MOVE HIGH-VALUES TO P56-DT-INI-FNT-REDD-3-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getDtIniFntRedd3() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-DT-INI-FNT-REDD-3-NULL
            questAdegVer.getP56DtIniFntRedd3().setP56DtIniFntRedd3Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P56DtIniFntRedd3.Len.P56_DT_INI_FNT_REDD3_NULL));
        }
        // COB_CODE: IF IND-P56-MOT-ASS-TIT-EFF = -1
        //              MOVE HIGH-VALUES TO P56-MOT-ASS-TIT-EFF
        //           END-IF
        if (ws.getIndQuestAdegVer().getMotAssTitEff() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-MOT-ASS-TIT-EFF
            questAdegVer.setP56MotAssTitEff(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_MOT_ASS_TIT_EFF));
        }
        // COB_CODE: IF IND-P56-FIN-COSTITUZIONE = -1
        //              MOVE HIGH-VALUES TO P56-FIN-COSTITUZIONE
        //           END-IF
        if (ws.getIndQuestAdegVer().getFinCostituzione() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-FIN-COSTITUZIONE
            questAdegVer.setP56FinCostituzione(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_FIN_COSTITUZIONE));
        }
        // COB_CODE: IF IND-P56-DESC-IMP-CAR-PUB = -1
        //              MOVE HIGH-VALUES TO P56-DESC-IMP-CAR-PUB
        //           END-IF
        if (ws.getIndQuestAdegVer().getDescImpCarPub() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-DESC-IMP-CAR-PUB
            questAdegVer.setP56DescImpCarPub(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_DESC_IMP_CAR_PUB));
        }
        // COB_CODE: IF IND-P56-DESC-SCO-FIN-RAPP = -1
        //              MOVE HIGH-VALUES TO P56-DESC-SCO-FIN-RAPP
        //           END-IF
        if (ws.getIndQuestAdegVer().getDescScoFinRapp() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-DESC-SCO-FIN-RAPP
            questAdegVer.setP56DescScoFinRapp(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_DESC_SCO_FIN_RAPP));
        }
        // COB_CODE: IF IND-P56-DESC-PROC-PNL = -1
        //              MOVE HIGH-VALUES TO P56-DESC-PROC-PNL
        //           END-IF
        if (ws.getIndQuestAdegVer().getDescProcPnl() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-DESC-PROC-PNL
            questAdegVer.setP56DescProcPnl(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_DESC_PROC_PNL));
        }
        // COB_CODE: IF IND-P56-DESC-NOT-PREG = -1
        //              MOVE HIGH-VALUES TO P56-DESC-NOT-PREG
        //           END-IF
        if (ws.getIndQuestAdegVer().getDescNotPreg() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-DESC-NOT-PREG
            questAdegVer.setP56DescNotPreg(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_DESC_NOT_PREG));
        }
        // COB_CODE: IF IND-P56-ID-ASSICURATI = -1
        //              MOVE HIGH-VALUES TO P56-ID-ASSICURATI-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getIdAssicurati() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-ID-ASSICURATI-NULL
            questAdegVer.getP56IdAssicurati().setP56IdAssicuratiNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P56IdAssicurati.Len.P56_ID_ASSICURATI_NULL));
        }
        // COB_CODE: IF IND-P56-REDD-CON = -1
        //              MOVE HIGH-VALUES TO P56-REDD-CON-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getReddCon() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-REDD-CON-NULL
            questAdegVer.getP56ReddCon().setP56ReddConNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P56ReddCon.Len.P56_REDD_CON_NULL));
        }
        // COB_CODE: IF IND-P56-DESC-LIB-MOT-RISC = -1
        //              MOVE HIGH-VALUES TO P56-DESC-LIB-MOT-RISC
        //           END-IF
        if (ws.getIndQuestAdegVer().getDescLibMotRisc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-DESC-LIB-MOT-RISC
            questAdegVer.setP56DescLibMotRisc(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_DESC_LIB_MOT_RISC));
        }
        // COB_CODE: IF IND-P56-TP-MOT-ASS-TIT-EFF = -1
        //              MOVE HIGH-VALUES TO P56-TP-MOT-ASS-TIT-EFF-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getTpMotAssTitEff() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-TP-MOT-ASS-TIT-EFF-NULL
            questAdegVer.setP56TpMotAssTitEff(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_TP_MOT_ASS_TIT_EFF));
        }
        // COB_CODE: IF IND-P56-TP-RAG-RAPP = -1
        //              MOVE HIGH-VALUES TO P56-TP-RAG-RAPP-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getTpRagRapp() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-TP-RAG-RAPP-NULL
            questAdegVer.setP56TpRagRapp(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_TP_RAG_RAPP));
        }
        // COB_CODE: IF IND-P56-COD-CAN = -1
        //              MOVE HIGH-VALUES TO P56-COD-CAN-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getCodCan() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-COD-CAN-NULL
            questAdegVer.getP56CodCan().setP56CodCanNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P56CodCan.Len.P56_COD_CAN_NULL));
        }
        // COB_CODE: IF IND-P56-TP-FIN-COST = -1
        //              MOVE HIGH-VALUES TO P56-TP-FIN-COST-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getTpFinCost() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-TP-FIN-COST-NULL
            questAdegVer.setP56TpFinCost(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_TP_FIN_COST));
        }
        // COB_CODE: IF IND-P56-NAZ-DEST-FND = -1
        //              MOVE HIGH-VALUES TO P56-NAZ-DEST-FND-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getNazDestFnd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-NAZ-DEST-FND-NULL
            questAdegVer.setP56NazDestFnd(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_NAZ_DEST_FND));
        }
        // COB_CODE: IF IND-P56-FL-AU-FATCA-AEOI = -1
        //              MOVE HIGH-VALUES TO P56-FL-AU-FATCA-AEOI-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getFlAuFatcaAeoi() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-FL-AU-FATCA-AEOI-NULL
            questAdegVer.setP56FlAuFatcaAeoi(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-P56-TP-CAR-FIN-GIUR = -1
        //              MOVE HIGH-VALUES TO P56-TP-CAR-FIN-GIUR-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getTpCarFinGiur() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-TP-CAR-FIN-GIUR-NULL
            questAdegVer.setP56TpCarFinGiur(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_TP_CAR_FIN_GIUR));
        }
        // COB_CODE: IF IND-P56-TP-CAR-FIN-GIUR-AT = -1
        //              MOVE HIGH-VALUES TO P56-TP-CAR-FIN-GIUR-AT-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getTpCarFinGiurAt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-TP-CAR-FIN-GIUR-AT-NULL
            questAdegVer.setP56TpCarFinGiurAt(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_TP_CAR_FIN_GIUR_AT));
        }
        // COB_CODE: IF IND-P56-TP-CAR-FIN-GIUR-PA = -1
        //              MOVE HIGH-VALUES TO P56-TP-CAR-FIN-GIUR-PA-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getTpCarFinGiurPa() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-TP-CAR-FIN-GIUR-PA-NULL
            questAdegVer.setP56TpCarFinGiurPa(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_TP_CAR_FIN_GIUR_PA));
        }
        // COB_CODE: IF IND-P56-FL-ISTITUZ-FIN = -1
        //              MOVE HIGH-VALUES TO P56-FL-ISTITUZ-FIN-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getFlIstituzFin() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-FL-ISTITUZ-FIN-NULL
            questAdegVer.setP56FlIstituzFin(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-P56-TP-ORI-FND-TIT-EFF = -1
        //              MOVE HIGH-VALUES TO P56-TP-ORI-FND-TIT-EFF-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getTpOriFndTitEff() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-TP-ORI-FND-TIT-EFF-NULL
            questAdegVer.setP56TpOriFndTitEff(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_TP_ORI_FND_TIT_EFF));
        }
        // COB_CODE: IF IND-P56-PC-ESP-AG-PA-MSC = -1
        //              MOVE HIGH-VALUES TO P56-PC-ESP-AG-PA-MSC-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getPcEspAgPaMsc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-PC-ESP-AG-PA-MSC-NULL
            questAdegVer.getP56PcEspAgPaMsc().setP56PcEspAgPaMscNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P56PcEspAgPaMsc.Len.P56_PC_ESP_AG_PA_MSC_NULL));
        }
        // COB_CODE: IF IND-P56-FL-PR-TR-USA = -1
        //              MOVE HIGH-VALUES TO P56-FL-PR-TR-USA-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getFlPrTrUsa() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-FL-PR-TR-USA-NULL
            questAdegVer.setP56FlPrTrUsa(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-P56-FL-PR-TR-NO-USA = -1
        //              MOVE HIGH-VALUES TO P56-FL-PR-TR-NO-USA-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getFlPrTrNoUsa() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-FL-PR-TR-NO-USA-NULL
            questAdegVer.setP56FlPrTrNoUsa(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-P56-PC-RIP-PAT-AS-VITA = -1
        //              MOVE HIGH-VALUES TO P56-PC-RIP-PAT-AS-VITA-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getPcRipPatAsVita() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-PC-RIP-PAT-AS-VITA-NULL
            questAdegVer.getP56PcRipPatAsVita().setP56PcRipPatAsVitaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P56PcRipPatAsVita.Len.P56_PC_RIP_PAT_AS_VITA_NULL));
        }
        // COB_CODE: IF IND-P56-PC-RIP-PAT-IM = -1
        //              MOVE HIGH-VALUES TO P56-PC-RIP-PAT-IM-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getPcRipPatIm() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-PC-RIP-PAT-IM-NULL
            questAdegVer.getP56PcRipPatIm().setP56PcRipPatImNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P56PcRipPatIm.Len.P56_PC_RIP_PAT_IM_NULL));
        }
        // COB_CODE: IF IND-P56-PC-RIP-PAT-SET-IM = -1
        //              MOVE HIGH-VALUES TO P56-PC-RIP-PAT-SET-IM-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getPcRipPatSetIm() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-PC-RIP-PAT-SET-IM-NULL
            questAdegVer.getP56PcRipPatSetIm().setP56PcRipPatSetImNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P56PcRipPatSetIm.Len.P56_PC_RIP_PAT_SET_IM_NULL));
        }
        // COB_CODE: IF IND-P56-TP-STATUS-AEOI = -1
        //              MOVE HIGH-VALUES TO P56-TP-STATUS-AEOI-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getTpStatusAeoi() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-TP-STATUS-AEOI-NULL
            questAdegVer.setP56TpStatusAeoi(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_TP_STATUS_AEOI));
        }
        // COB_CODE: IF IND-P56-TP-STATUS-FATCA = -1
        //              MOVE HIGH-VALUES TO P56-TP-STATUS-FATCA-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getTpStatusFatca() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-TP-STATUS-FATCA-NULL
            questAdegVer.setP56TpStatusFatca(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_TP_STATUS_FATCA));
        }
        // COB_CODE: IF IND-P56-FL-RAPP-PA-MSC = -1
        //              MOVE HIGH-VALUES TO P56-FL-RAPP-PA-MSC-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getFlRappPaMsc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-FL-RAPP-PA-MSC-NULL
            questAdegVer.setP56FlRappPaMsc(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-P56-COD-COMUN-SVOL-ATT = -1
        //              MOVE HIGH-VALUES TO P56-COD-COMUN-SVOL-ATT-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getCodComunSvolAtt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-COD-COMUN-SVOL-ATT-NULL
            questAdegVer.setP56CodComunSvolAtt(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_COD_COMUN_SVOL_ATT));
        }
        // COB_CODE: IF IND-P56-TP-DT-1O-CON-CLI = -1
        //              MOVE HIGH-VALUES TO P56-TP-DT-1O-CON-CLI-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getTpDt1oConCli() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-TP-DT-1O-CON-CLI-NULL
            questAdegVer.setP56TpDt1oConCli(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_TP_DT1O_CON_CLI));
        }
        // COB_CODE: IF IND-P56-TP-MOD-EN-RELA-INT = -1
        //              MOVE HIGH-VALUES TO P56-TP-MOD-EN-RELA-INT-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getTpModEnRelaInt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-TP-MOD-EN-RELA-INT-NULL
            questAdegVer.setP56TpModEnRelaInt(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_TP_MOD_EN_RELA_INT));
        }
        // COB_CODE: IF IND-P56-TP-REDD-ANNU-LRD = -1
        //              MOVE HIGH-VALUES TO P56-TP-REDD-ANNU-LRD-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getTpReddAnnuLrd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-TP-REDD-ANNU-LRD-NULL
            questAdegVer.setP56TpReddAnnuLrd(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_TP_REDD_ANNU_LRD));
        }
        // COB_CODE: IF IND-P56-TP-REDD-CON = -1
        //              MOVE HIGH-VALUES TO P56-TP-REDD-CON-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getTpReddCon() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-TP-REDD-CON-NULL
            questAdegVer.setP56TpReddCon(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_TP_REDD_CON));
        }
        // COB_CODE: IF IND-P56-TP-OPER-SOC-FID = -1
        //              MOVE HIGH-VALUES TO P56-TP-OPER-SOC-FID-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getTpOperSocFid() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-TP-OPER-SOC-FID-NULL
            questAdegVer.setP56TpOperSocFid(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_TP_OPER_SOC_FID));
        }
        // COB_CODE: IF IND-P56-COD-PA-ESP-MSC-1 = -1
        //              MOVE HIGH-VALUES TO P56-COD-PA-ESP-MSC-1-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getCodPaEspMsc1() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-COD-PA-ESP-MSC-1-NULL
            questAdegVer.setP56CodPaEspMsc1(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_COD_PA_ESP_MSC1));
        }
        // COB_CODE: IF IND-P56-IMP-PA-ESP-MSC-1 = -1
        //              MOVE HIGH-VALUES TO P56-IMP-PA-ESP-MSC-1-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getImpPaEspMsc1() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-IMP-PA-ESP-MSC-1-NULL
            questAdegVer.getP56ImpPaEspMsc1().setP56ImpPaEspMsc1Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P56ImpPaEspMsc1.Len.P56_IMP_PA_ESP_MSC1_NULL));
        }
        // COB_CODE: IF IND-P56-COD-PA-ESP-MSC-2 = -1
        //              MOVE HIGH-VALUES TO P56-COD-PA-ESP-MSC-2-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getCodPaEspMsc2() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-COD-PA-ESP-MSC-2-NULL
            questAdegVer.setP56CodPaEspMsc2(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_COD_PA_ESP_MSC2));
        }
        // COB_CODE: IF IND-P56-IMP-PA-ESP-MSC-2 = -1
        //              MOVE HIGH-VALUES TO P56-IMP-PA-ESP-MSC-2-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getImpPaEspMsc2() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-IMP-PA-ESP-MSC-2-NULL
            questAdegVer.getP56ImpPaEspMsc2().setP56ImpPaEspMsc2Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P56ImpPaEspMsc2.Len.P56_IMP_PA_ESP_MSC2_NULL));
        }
        // COB_CODE: IF IND-P56-COD-PA-ESP-MSC-3 = -1
        //              MOVE HIGH-VALUES TO P56-COD-PA-ESP-MSC-3-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getCodPaEspMsc3() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-COD-PA-ESP-MSC-3-NULL
            questAdegVer.setP56CodPaEspMsc3(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_COD_PA_ESP_MSC3));
        }
        // COB_CODE: IF IND-P56-IMP-PA-ESP-MSC-3 = -1
        //              MOVE HIGH-VALUES TO P56-IMP-PA-ESP-MSC-3-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getImpPaEspMsc3() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-IMP-PA-ESP-MSC-3-NULL
            questAdegVer.getP56ImpPaEspMsc3().setP56ImpPaEspMsc3Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P56ImpPaEspMsc3.Len.P56_IMP_PA_ESP_MSC3_NULL));
        }
        // COB_CODE: IF IND-P56-COD-PA-ESP-MSC-4 = -1
        //              MOVE HIGH-VALUES TO P56-COD-PA-ESP-MSC-4-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getCodPaEspMsc4() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-COD-PA-ESP-MSC-4-NULL
            questAdegVer.setP56CodPaEspMsc4(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_COD_PA_ESP_MSC4));
        }
        // COB_CODE: IF IND-P56-IMP-PA-ESP-MSC-4 = -1
        //              MOVE HIGH-VALUES TO P56-IMP-PA-ESP-MSC-4-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getImpPaEspMsc4() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-IMP-PA-ESP-MSC-4-NULL
            questAdegVer.getP56ImpPaEspMsc4().setP56ImpPaEspMsc4Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P56ImpPaEspMsc4.Len.P56_IMP_PA_ESP_MSC4_NULL));
        }
        // COB_CODE: IF IND-P56-COD-PA-ESP-MSC-5 = -1
        //              MOVE HIGH-VALUES TO P56-COD-PA-ESP-MSC-5-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getCodPaEspMsc5() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-COD-PA-ESP-MSC-5-NULL
            questAdegVer.setP56CodPaEspMsc5(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_COD_PA_ESP_MSC5));
        }
        // COB_CODE: IF IND-P56-IMP-PA-ESP-MSC-5 = -1
        //              MOVE HIGH-VALUES TO P56-IMP-PA-ESP-MSC-5-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getImpPaEspMsc5() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-IMP-PA-ESP-MSC-5-NULL
            questAdegVer.getP56ImpPaEspMsc5().setP56ImpPaEspMsc5Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P56ImpPaEspMsc5.Len.P56_IMP_PA_ESP_MSC5_NULL));
        }
        // COB_CODE: IF IND-P56-DESC-ORGN-FND = -1
        //              MOVE HIGH-VALUES TO P56-DESC-ORGN-FND
        //           END-IF
        if (ws.getIndQuestAdegVer().getDescOrgnFnd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-DESC-ORGN-FND
            questAdegVer.setP56DescOrgnFnd(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_DESC_ORGN_FND));
        }
        // COB_CODE: IF IND-P56-COD-AUT-DUE-DIL = -1
        //              MOVE HIGH-VALUES TO P56-COD-AUT-DUE-DIL-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getCodAutDueDil() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-COD-AUT-DUE-DIL-NULL
            questAdegVer.setP56CodAutDueDil(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_COD_AUT_DUE_DIL));
        }
        // COB_CODE: IF IND-P56-FL-PR-QUEST-FATCA = -1
        //              MOVE HIGH-VALUES TO P56-FL-PR-QUEST-FATCA-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getFlPrQuestFatca() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-FL-PR-QUEST-FATCA-NULL
            questAdegVer.setP56FlPrQuestFatca(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-P56-FL-PR-QUEST-AEOI = -1
        //              MOVE HIGH-VALUES TO P56-FL-PR-QUEST-AEOI-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getFlPrQuestAeoi() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-FL-PR-QUEST-AEOI-NULL
            questAdegVer.setP56FlPrQuestAeoi(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-P56-FL-PR-QUEST-OFAC = -1
        //              MOVE HIGH-VALUES TO P56-FL-PR-QUEST-OFAC-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getFlPrQuestOfac() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-FL-PR-QUEST-OFAC-NULL
            questAdegVer.setP56FlPrQuestOfac(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-P56-FL-PR-QUEST-KYC = -1
        //              MOVE HIGH-VALUES TO P56-FL-PR-QUEST-KYC-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getFlPrQuestKyc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-FL-PR-QUEST-KYC-NULL
            questAdegVer.setP56FlPrQuestKyc(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-P56-FL-PR-QUEST-MSCQ = -1
        //              MOVE HIGH-VALUES TO P56-FL-PR-QUEST-MSCQ-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getFlPrQuestMscq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-FL-PR-QUEST-MSCQ-NULL
            questAdegVer.setP56FlPrQuestMscq(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-P56-TP-NOT-PREG = -1
        //              MOVE HIGH-VALUES TO P56-TP-NOT-PREG-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getTpNotPreg() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-TP-NOT-PREG-NULL
            questAdegVer.setP56TpNotPreg(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_TP_NOT_PREG));
        }
        // COB_CODE: IF IND-P56-TP-PROC-PNL = -1
        //              MOVE HIGH-VALUES TO P56-TP-PROC-PNL-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getTpProcPnl() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-TP-PROC-PNL-NULL
            questAdegVer.setP56TpProcPnl(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_TP_PROC_PNL));
        }
        // COB_CODE: IF IND-P56-COD-IMP-CAR-PUB = -1
        //              MOVE HIGH-VALUES TO P56-COD-IMP-CAR-PUB-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getCodImpCarPub() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-COD-IMP-CAR-PUB-NULL
            questAdegVer.setP56CodImpCarPub(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_COD_IMP_CAR_PUB));
        }
        // COB_CODE: IF IND-P56-OPRZ-SOSPETTE = -1
        //              MOVE HIGH-VALUES TO P56-OPRZ-SOSPETTE-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getOprzSospette() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-OPRZ-SOSPETTE-NULL
            questAdegVer.setP56OprzSospette(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-P56-ULT-FATT-ANNU = -1
        //              MOVE HIGH-VALUES TO P56-ULT-FATT-ANNU-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getUltFattAnnu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-ULT-FATT-ANNU-NULL
            questAdegVer.getP56UltFattAnnu().setP56UltFattAnnuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P56UltFattAnnu.Len.P56_ULT_FATT_ANNU_NULL));
        }
        // COB_CODE: IF IND-P56-DESC-PEP = -1
        //              MOVE HIGH-VALUES TO P56-DESC-PEP
        //           END-IF
        if (ws.getIndQuestAdegVer().getDescPep() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-DESC-PEP
            questAdegVer.setP56DescPep(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_DESC_PEP));
        }
        // COB_CODE: IF IND-P56-NUM-TEL-2 = -1
        //              MOVE HIGH-VALUES TO P56-NUM-TEL-2-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getNumTel2() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-NUM-TEL-2-NULL
            questAdegVer.setP56NumTel2(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_NUM_TEL2));
        }
        // COB_CODE: IF IND-P56-IMP-AFI = -1
        //              MOVE HIGH-VALUES TO P56-IMP-AFI-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getImpAfi() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-IMP-AFI-NULL
            questAdegVer.getP56ImpAfi().setP56ImpAfiNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P56ImpAfi.Len.P56_IMP_AFI_NULL));
        }
        // COB_CODE: IF IND-P56-FL-NEW-PRO = -1
        //              MOVE HIGH-VALUES TO P56-FL-NEW-PRO-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getFlNewPro() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-FL-NEW-PRO-NULL
            questAdegVer.setP56FlNewPro(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-P56-TP-MOT-CAMBIO-CNTR = -1
        //              MOVE HIGH-VALUES TO P56-TP-MOT-CAMBIO-CNTR-NULL
        //           END-IF
        if (ws.getIndQuestAdegVer().getTpMotCambioCntr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-TP-MOT-CAMBIO-CNTR-NULL
            questAdegVer.setP56TpMotCambioCntr(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_TP_MOT_CAMBIO_CNTR));
        }
        // COB_CODE: IF IND-P56-DESC-MOT-CAMBIO-CN = -1
        //              MOVE HIGH-VALUES TO P56-DESC-MOT-CAMBIO-CN
        //           END-IF
        if (ws.getIndQuestAdegVer().getDescMotCambioCn() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-DESC-MOT-CAMBIO-CN
            questAdegVer.setP56DescMotCambioCn(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_DESC_MOT_CAMBIO_CN));
        }
        // COB_CODE: IF IND-P56-COD-SOGG = -1
        //              MOVE HIGH-VALUES TO P56-COD-SOGG-NULL
        //           END-IF.
        if (ws.getIndQuestAdegVer().getCodSogg() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P56-COD-SOGG-NULL
            questAdegVer.setP56CodSogg(LiteralGenerator.create(Types.HIGH_CHAR_VAL, QuestAdegVer.Len.P56_COD_SOGG));
        }
    }

    /**Original name: Z150-VALORIZZA-DATA-SERVICES-I<br>*/
    private void z150ValorizzaDataServicesI() {
        // COB_CODE: MOVE 'I' TO P56-DS-OPER-SQL
        questAdegVer.setP56DsOperSqlFormatted("I");
        // COB_CODE: MOVE 0                   TO P56-DS-VER
        questAdegVer.setP56DsVer(0);
        // COB_CODE: MOVE IDSV0003-USER-NAME TO P56-DS-UTENTE
        questAdegVer.setP56DsUtente(idsv0003.getUserName());
        // COB_CODE: MOVE '1'                   TO P56-DS-STATO-ELAB
        questAdegVer.setP56DsStatoElabFormatted("1");
        // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR TO P56-DS-TS-CPTZ.
        questAdegVer.setP56DsTsCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
    }

    /**Original name: Z160-VALORIZZA-DATA-SERVICES-U<br>*/
    private void z160ValorizzaDataServicesU() {
        // COB_CODE: MOVE 'U' TO P56-DS-OPER-SQL
        questAdegVer.setP56DsOperSqlFormatted("U");
        // COB_CODE: MOVE IDSV0003-USER-NAME TO P56-DS-UTENTE
        questAdegVer.setP56DsUtente(idsv0003.getUserName());
        // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR TO P56-DS-TS-CPTZ.
        questAdegVer.setP56DsTsCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
    }

    /**Original name: Z200-SET-INDICATORI-NULL<br>*/
    private void z200SetIndicatoriNull() {
        // COB_CODE: IF P56-ID-RAPP-ANA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-ID-RAPP-ANA
        //           ELSE
        //              MOVE 0 TO IND-P56-ID-RAPP-ANA
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56IdRappAna().getP56IdRappAnaNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-ID-RAPP-ANA
            ws.getIndQuestAdegVer().setIdRappAna(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-ID-RAPP-ANA
            ws.getIndQuestAdegVer().setIdRappAna(((short)0));
        }
        // COB_CODE: IF P56-NATURA-OPRZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-NATURA-OPRZ
        //           ELSE
        //              MOVE 0 TO IND-P56-NATURA-OPRZ
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56NaturaOprzFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-NATURA-OPRZ
            ws.getIndQuestAdegVer().setNaturaOprz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-NATURA-OPRZ
            ws.getIndQuestAdegVer().setNaturaOprz(((short)0));
        }
        // COB_CODE: IF P56-ORGN-FND-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-ORGN-FND
        //           ELSE
        //              MOVE 0 TO IND-P56-ORGN-FND
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56OrgnFndFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-ORGN-FND
            ws.getIndQuestAdegVer().setOrgnFnd(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-ORGN-FND
            ws.getIndQuestAdegVer().setOrgnFnd(((short)0));
        }
        // COB_CODE: IF P56-COD-QLFC-PROF-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-COD-QLFC-PROF
        //           ELSE
        //              MOVE 0 TO IND-P56-COD-QLFC-PROF
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56CodQlfcProfFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-COD-QLFC-PROF
            ws.getIndQuestAdegVer().setCodQlfcProf(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-COD-QLFC-PROF
            ws.getIndQuestAdegVer().setCodQlfcProf(((short)0));
        }
        // COB_CODE: IF P56-COD-NAZ-QLFC-PROF-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-COD-NAZ-QLFC-PROF
        //           ELSE
        //              MOVE 0 TO IND-P56-COD-NAZ-QLFC-PROF
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56CodNazQlfcProfFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-COD-NAZ-QLFC-PROF
            ws.getIndQuestAdegVer().setCodNazQlfcProf(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-COD-NAZ-QLFC-PROF
            ws.getIndQuestAdegVer().setCodNazQlfcProf(((short)0));
        }
        // COB_CODE: IF P56-COD-PRV-QLFC-PROF-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-COD-PRV-QLFC-PROF
        //           ELSE
        //              MOVE 0 TO IND-P56-COD-PRV-QLFC-PROF
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56CodPrvQlfcProfFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-COD-PRV-QLFC-PROF
            ws.getIndQuestAdegVer().setCodPrvQlfcProf(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-COD-PRV-QLFC-PROF
            ws.getIndQuestAdegVer().setCodPrvQlfcProf(((short)0));
        }
        // COB_CODE: IF P56-FNT-REDD-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-FNT-REDD
        //           ELSE
        //              MOVE 0 TO IND-P56-FNT-REDD
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56FntReddFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-FNT-REDD
            ws.getIndQuestAdegVer().setFntRedd(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-FNT-REDD
            ws.getIndQuestAdegVer().setFntRedd(((short)0));
        }
        // COB_CODE: IF P56-REDD-FATT-ANNU-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-REDD-FATT-ANNU
        //           ELSE
        //              MOVE 0 TO IND-P56-REDD-FATT-ANNU
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56ReddFattAnnu().getP56ReddFattAnnuNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-REDD-FATT-ANNU
            ws.getIndQuestAdegVer().setReddFattAnnu(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-REDD-FATT-ANNU
            ws.getIndQuestAdegVer().setReddFattAnnu(((short)0));
        }
        // COB_CODE: IF P56-COD-ATECO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-COD-ATECO
        //           ELSE
        //              MOVE 0 TO IND-P56-COD-ATECO
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56CodAtecoFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-COD-ATECO
            ws.getIndQuestAdegVer().setCodAteco(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-COD-ATECO
            ws.getIndQuestAdegVer().setCodAteco(((short)0));
        }
        // COB_CODE: IF P56-VALUT-COLL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-VALUT-COLL
        //           ELSE
        //              MOVE 0 TO IND-P56-VALUT-COLL
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56ValutCollFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-VALUT-COLL
            ws.getIndQuestAdegVer().setValutColl(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-VALUT-COLL
            ws.getIndQuestAdegVer().setValutColl(((short)0));
        }
        // COB_CODE: IF P56-LUOGO-COSTITUZIONE = HIGH-VALUES
        //              MOVE -1 TO IND-P56-LUOGO-COSTITUZIONE
        //           ELSE
        //              MOVE 0 TO IND-P56-LUOGO-COSTITUZIONE
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56LuogoCostituzione(), QuestAdegVer.Len.P56_LUOGO_COSTITUZIONE)) {
            // COB_CODE: MOVE -1 TO IND-P56-LUOGO-COSTITUZIONE
            ws.getIndQuestAdegVer().setLuogoCostituzione(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-LUOGO-COSTITUZIONE
            ws.getIndQuestAdegVer().setLuogoCostituzione(((short)0));
        }
        // COB_CODE: IF P56-TP-MOVI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-TP-MOVI
        //           ELSE
        //              MOVE 0 TO IND-P56-TP-MOVI
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56TpMovi().getP56TpMoviNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-TP-MOVI
            ws.getIndQuestAdegVer().setTpMovi(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-TP-MOVI
            ws.getIndQuestAdegVer().setTpMovi(((short)0));
        }
        // COB_CODE: IF P56-FL-RAG-RAPP-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-FL-RAG-RAPP
        //           ELSE
        //              MOVE 0 TO IND-P56-FL-RAG-RAPP
        //           END-IF
        if (Conditions.eq(questAdegVer.getP56FlRagRapp(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-P56-FL-RAG-RAPP
            ws.getIndQuestAdegVer().setFlRagRapp(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-FL-RAG-RAPP
            ws.getIndQuestAdegVer().setFlRagRapp(((short)0));
        }
        // COB_CODE: IF P56-FL-PRSZ-TIT-EFF-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-FL-PRSZ-TIT-EFF
        //           ELSE
        //              MOVE 0 TO IND-P56-FL-PRSZ-TIT-EFF
        //           END-IF
        if (Conditions.eq(questAdegVer.getP56FlPrszTitEff(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-P56-FL-PRSZ-TIT-EFF
            ws.getIndQuestAdegVer().setFlPrszTitEff(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-FL-PRSZ-TIT-EFF
            ws.getIndQuestAdegVer().setFlPrszTitEff(((short)0));
        }
        // COB_CODE: IF P56-TP-MOT-RISC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-TP-MOT-RISC
        //           ELSE
        //              MOVE 0 TO IND-P56-TP-MOT-RISC
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56TpMotRiscFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-TP-MOT-RISC
            ws.getIndQuestAdegVer().setTpMotRisc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-TP-MOT-RISC
            ws.getIndQuestAdegVer().setTpMotRisc(((short)0));
        }
        // COB_CODE: IF P56-TP-PNT-VND-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-TP-PNT-VND
        //           ELSE
        //              MOVE 0 TO IND-P56-TP-PNT-VND
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56TpPntVndFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-TP-PNT-VND
            ws.getIndQuestAdegVer().setTpPntVnd(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-TP-PNT-VND
            ws.getIndQuestAdegVer().setTpPntVnd(((short)0));
        }
        // COB_CODE: IF P56-TP-ADEG-VER-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-TP-ADEG-VER
        //           ELSE
        //              MOVE 0 TO IND-P56-TP-ADEG-VER
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56TpAdegVerFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-TP-ADEG-VER
            ws.getIndQuestAdegVer().setTpAdegVer(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-TP-ADEG-VER
            ws.getIndQuestAdegVer().setTpAdegVer(((short)0));
        }
        // COB_CODE: IF P56-TP-RELA-ESEC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-TP-RELA-ESEC
        //           ELSE
        //              MOVE 0 TO IND-P56-TP-RELA-ESEC
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56TpRelaEsecFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-TP-RELA-ESEC
            ws.getIndQuestAdegVer().setTpRelaEsec(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-TP-RELA-ESEC
            ws.getIndQuestAdegVer().setTpRelaEsec(((short)0));
        }
        // COB_CODE: IF P56-TP-SCO-FIN-RAPP-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-TP-SCO-FIN-RAPP
        //           ELSE
        //              MOVE 0 TO IND-P56-TP-SCO-FIN-RAPP
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56TpScoFinRappFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-TP-SCO-FIN-RAPP
            ws.getIndQuestAdegVer().setTpScoFinRapp(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-TP-SCO-FIN-RAPP
            ws.getIndQuestAdegVer().setTpScoFinRapp(((short)0));
        }
        // COB_CODE: IF P56-FL-PRSZ-3O-PAGAT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-FL-PRSZ-3O-PAGAT
        //           ELSE
        //              MOVE 0 TO IND-P56-FL-PRSZ-3O-PAGAT
        //           END-IF
        if (Conditions.eq(questAdegVer.getP56FlPrsz3oPagat(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-P56-FL-PRSZ-3O-PAGAT
            ws.getIndQuestAdegVer().setFlPrsz3oPagat(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-FL-PRSZ-3O-PAGAT
            ws.getIndQuestAdegVer().setFlPrsz3oPagat(((short)0));
        }
        // COB_CODE: IF P56-AREA-GEO-PROV-FND-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-AREA-GEO-PROV-FND
        //           ELSE
        //              MOVE 0 TO IND-P56-AREA-GEO-PROV-FND
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56AreaGeoProvFndFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-AREA-GEO-PROV-FND
            ws.getIndQuestAdegVer().setAreaGeoProvFnd(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-AREA-GEO-PROV-FND
            ws.getIndQuestAdegVer().setAreaGeoProvFnd(((short)0));
        }
        // COB_CODE: IF P56-TP-DEST-FND-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-TP-DEST-FND
        //           ELSE
        //              MOVE 0 TO IND-P56-TP-DEST-FND
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56TpDestFndFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-TP-DEST-FND
            ws.getIndQuestAdegVer().setTpDestFnd(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-TP-DEST-FND
            ws.getIndQuestAdegVer().setTpDestFnd(((short)0));
        }
        // COB_CODE: IF P56-FL-PAESE-RESID-AUT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-FL-PAESE-RESID-AUT
        //           ELSE
        //              MOVE 0 TO IND-P56-FL-PAESE-RESID-AUT
        //           END-IF
        if (Conditions.eq(questAdegVer.getP56FlPaeseResidAut(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-P56-FL-PAESE-RESID-AUT
            ws.getIndQuestAdegVer().setFlPaeseResidAut(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-FL-PAESE-RESID-AUT
            ws.getIndQuestAdegVer().setFlPaeseResidAut(((short)0));
        }
        // COB_CODE: IF P56-FL-PAESE-CIT-AUT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-FL-PAESE-CIT-AUT
        //           ELSE
        //              MOVE 0 TO IND-P56-FL-PAESE-CIT-AUT
        //           END-IF
        if (Conditions.eq(questAdegVer.getP56FlPaeseCitAut(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-P56-FL-PAESE-CIT-AUT
            ws.getIndQuestAdegVer().setFlPaeseCitAut(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-FL-PAESE-CIT-AUT
            ws.getIndQuestAdegVer().setFlPaeseCitAut(((short)0));
        }
        // COB_CODE: IF P56-FL-PAESE-NAZ-AUT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-FL-PAESE-NAZ-AUT
        //           ELSE
        //              MOVE 0 TO IND-P56-FL-PAESE-NAZ-AUT
        //           END-IF
        if (Conditions.eq(questAdegVer.getP56FlPaeseNazAut(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-P56-FL-PAESE-NAZ-AUT
            ws.getIndQuestAdegVer().setFlPaeseNazAut(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-FL-PAESE-NAZ-AUT
            ws.getIndQuestAdegVer().setFlPaeseNazAut(((short)0));
        }
        // COB_CODE: IF P56-COD-PROF-PREC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-COD-PROF-PREC
        //           ELSE
        //              MOVE 0 TO IND-P56-COD-PROF-PREC
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56CodProfPrecFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-COD-PROF-PREC
            ws.getIndQuestAdegVer().setCodProfPrec(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-COD-PROF-PREC
            ws.getIndQuestAdegVer().setCodProfPrec(((short)0));
        }
        // COB_CODE: IF P56-FL-AUT-PEP-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-FL-AUT-PEP
        //           ELSE
        //              MOVE 0 TO IND-P56-FL-AUT-PEP
        //           END-IF
        if (Conditions.eq(questAdegVer.getP56FlAutPep(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-P56-FL-AUT-PEP
            ws.getIndQuestAdegVer().setFlAutPep(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-FL-AUT-PEP
            ws.getIndQuestAdegVer().setFlAutPep(((short)0));
        }
        // COB_CODE: IF P56-FL-IMP-CAR-PUB-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-FL-IMP-CAR-PUB
        //           ELSE
        //              MOVE 0 TO IND-P56-FL-IMP-CAR-PUB
        //           END-IF
        if (Conditions.eq(questAdegVer.getP56FlImpCarPub(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-P56-FL-IMP-CAR-PUB
            ws.getIndQuestAdegVer().setFlImpCarPub(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-FL-IMP-CAR-PUB
            ws.getIndQuestAdegVer().setFlImpCarPub(((short)0));
        }
        // COB_CODE: IF P56-FL-LIS-TERR-SORV-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-FL-LIS-TERR-SORV
        //           ELSE
        //              MOVE 0 TO IND-P56-FL-LIS-TERR-SORV
        //           END-IF
        if (Conditions.eq(questAdegVer.getP56FlLisTerrSorv(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-P56-FL-LIS-TERR-SORV
            ws.getIndQuestAdegVer().setFlLisTerrSorv(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-FL-LIS-TERR-SORV
            ws.getIndQuestAdegVer().setFlLisTerrSorv(((short)0));
        }
        // COB_CODE: IF P56-TP-SIT-FIN-PAT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-TP-SIT-FIN-PAT
        //           ELSE
        //              MOVE 0 TO IND-P56-TP-SIT-FIN-PAT
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56TpSitFinPatFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-TP-SIT-FIN-PAT
            ws.getIndQuestAdegVer().setTpSitFinPat(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-TP-SIT-FIN-PAT
            ws.getIndQuestAdegVer().setTpSitFinPat(((short)0));
        }
        // COB_CODE: IF P56-TP-SIT-FIN-PAT-CON-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-TP-SIT-FIN-PAT-CON
        //           ELSE
        //              MOVE 0 TO IND-P56-TP-SIT-FIN-PAT-CON
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56TpSitFinPatConFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-TP-SIT-FIN-PAT-CON
            ws.getIndQuestAdegVer().setTpSitFinPatCon(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-TP-SIT-FIN-PAT-CON
            ws.getIndQuestAdegVer().setTpSitFinPatCon(((short)0));
        }
        // COB_CODE: IF P56-IMP-TOT-AFF-UTIL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-IMP-TOT-AFF-UTIL
        //           ELSE
        //              MOVE 0 TO IND-P56-IMP-TOT-AFF-UTIL
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56ImpTotAffUtil().getP56ImpTotAffUtilNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-IMP-TOT-AFF-UTIL
            ws.getIndQuestAdegVer().setImpTotAffUtil(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-IMP-TOT-AFF-UTIL
            ws.getIndQuestAdegVer().setImpTotAffUtil(((short)0));
        }
        // COB_CODE: IF P56-IMP-TOT-FIN-UTIL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-IMP-TOT-FIN-UTIL
        //           ELSE
        //              MOVE 0 TO IND-P56-IMP-TOT-FIN-UTIL
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56ImpTotFinUtil().getP56ImpTotFinUtilNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-IMP-TOT-FIN-UTIL
            ws.getIndQuestAdegVer().setImpTotFinUtil(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-IMP-TOT-FIN-UTIL
            ws.getIndQuestAdegVer().setImpTotFinUtil(((short)0));
        }
        // COB_CODE: IF P56-IMP-TOT-AFF-ACC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-IMP-TOT-AFF-ACC
        //           ELSE
        //              MOVE 0 TO IND-P56-IMP-TOT-AFF-ACC
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56ImpTotAffAcc().getP56ImpTotAffAccNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-IMP-TOT-AFF-ACC
            ws.getIndQuestAdegVer().setImpTotAffAcc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-IMP-TOT-AFF-ACC
            ws.getIndQuestAdegVer().setImpTotAffAcc(((short)0));
        }
        // COB_CODE: IF P56-IMP-TOT-FIN-ACC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-IMP-TOT-FIN-ACC
        //           ELSE
        //              MOVE 0 TO IND-P56-IMP-TOT-FIN-ACC
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56ImpTotFinAcc().getP56ImpTotFinAccNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-IMP-TOT-FIN-ACC
            ws.getIndQuestAdegVer().setImpTotFinAcc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-IMP-TOT-FIN-ACC
            ws.getIndQuestAdegVer().setImpTotFinAcc(((short)0));
        }
        // COB_CODE: IF P56-TP-FRM-GIUR-SAV-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-TP-FRM-GIUR-SAV
        //           ELSE
        //              MOVE 0 TO IND-P56-TP-FRM-GIUR-SAV
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56TpFrmGiurSavFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-TP-FRM-GIUR-SAV
            ws.getIndQuestAdegVer().setTpFrmGiurSav(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-TP-FRM-GIUR-SAV
            ws.getIndQuestAdegVer().setTpFrmGiurSav(((short)0));
        }
        // COB_CODE: IF P56-REG-COLL-POLI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-REG-COLL-POLI
        //           ELSE
        //              MOVE 0 TO IND-P56-REG-COLL-POLI
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56RegCollPoliFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-REG-COLL-POLI
            ws.getIndQuestAdegVer().setRegCollPoli(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-REG-COLL-POLI
            ws.getIndQuestAdegVer().setRegCollPoli(((short)0));
        }
        // COB_CODE: IF P56-NUM-TEL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-NUM-TEL
        //           ELSE
        //              MOVE 0 TO IND-P56-NUM-TEL
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56NumTelFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-NUM-TEL
            ws.getIndQuestAdegVer().setNumTel(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-NUM-TEL
            ws.getIndQuestAdegVer().setNumTel(((short)0));
        }
        // COB_CODE: IF P56-NUM-DIP-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-NUM-DIP
        //           ELSE
        //              MOVE 0 TO IND-P56-NUM-DIP
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56NumDip().getP56NumDipNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-NUM-DIP
            ws.getIndQuestAdegVer().setNumDip(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-NUM-DIP
            ws.getIndQuestAdegVer().setNumDip(((short)0));
        }
        // COB_CODE: IF P56-TP-SIT-FAM-CONV-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-TP-SIT-FAM-CONV
        //           ELSE
        //              MOVE 0 TO IND-P56-TP-SIT-FAM-CONV
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56TpSitFamConvFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-TP-SIT-FAM-CONV
            ws.getIndQuestAdegVer().setTpSitFamConv(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-TP-SIT-FAM-CONV
            ws.getIndQuestAdegVer().setTpSitFamConv(((short)0));
        }
        // COB_CODE: IF P56-COD-PROF-CON-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-COD-PROF-CON
        //           ELSE
        //              MOVE 0 TO IND-P56-COD-PROF-CON
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56CodProfConFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-COD-PROF-CON
            ws.getIndQuestAdegVer().setCodProfCon(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-COD-PROF-CON
            ws.getIndQuestAdegVer().setCodProfCon(((short)0));
        }
        // COB_CODE: IF P56-FL-ES-PROC-PEN-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-FL-ES-PROC-PEN
        //           ELSE
        //              MOVE 0 TO IND-P56-FL-ES-PROC-PEN
        //           END-IF
        if (Conditions.eq(questAdegVer.getP56FlEsProcPen(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-P56-FL-ES-PROC-PEN
            ws.getIndQuestAdegVer().setFlEsProcPen(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-FL-ES-PROC-PEN
            ws.getIndQuestAdegVer().setFlEsProcPen(((short)0));
        }
        // COB_CODE: IF P56-TP-COND-CLIENTE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-TP-COND-CLIENTE
        //           ELSE
        //              MOVE 0 TO IND-P56-TP-COND-CLIENTE
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56TpCondClienteFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-TP-COND-CLIENTE
            ws.getIndQuestAdegVer().setTpCondCliente(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-TP-COND-CLIENTE
            ws.getIndQuestAdegVer().setTpCondCliente(((short)0));
        }
        // COB_CODE: IF P56-COD-SAE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-COD-SAE
        //           ELSE
        //              MOVE 0 TO IND-P56-COD-SAE
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56CodSaeFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-COD-SAE
            ws.getIndQuestAdegVer().setCodSae(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-COD-SAE
            ws.getIndQuestAdegVer().setCodSae(((short)0));
        }
        // COB_CODE: IF P56-TP-OPER-ESTERO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-TP-OPER-ESTERO
        //           ELSE
        //              MOVE 0 TO IND-P56-TP-OPER-ESTERO
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56TpOperEsteroFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-TP-OPER-ESTERO
            ws.getIndQuestAdegVer().setTpOperEstero(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-TP-OPER-ESTERO
            ws.getIndQuestAdegVer().setTpOperEstero(((short)0));
        }
        // COB_CODE: IF P56-STAT-OPER-ESTERO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-STAT-OPER-ESTERO
        //           ELSE
        //              MOVE 0 TO IND-P56-STAT-OPER-ESTERO
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56StatOperEsteroFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-STAT-OPER-ESTERO
            ws.getIndQuestAdegVer().setStatOperEstero(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-STAT-OPER-ESTERO
            ws.getIndQuestAdegVer().setStatOperEstero(((short)0));
        }
        // COB_CODE: IF P56-COD-PRV-SVOL-ATT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-COD-PRV-SVOL-ATT
        //           ELSE
        //              MOVE 0 TO IND-P56-COD-PRV-SVOL-ATT
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56CodPrvSvolAttFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-COD-PRV-SVOL-ATT
            ws.getIndQuestAdegVer().setCodPrvSvolAtt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-COD-PRV-SVOL-ATT
            ws.getIndQuestAdegVer().setCodPrvSvolAtt(((short)0));
        }
        // COB_CODE: IF P56-COD-STAT-SVOL-ATT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-COD-STAT-SVOL-ATT
        //           ELSE
        //              MOVE 0 TO IND-P56-COD-STAT-SVOL-ATT
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56CodStatSvolAttFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-COD-STAT-SVOL-ATT
            ws.getIndQuestAdegVer().setCodStatSvolAtt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-COD-STAT-SVOL-ATT
            ws.getIndQuestAdegVer().setCodStatSvolAtt(((short)0));
        }
        // COB_CODE: IF P56-TP-SOC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-TP-SOC
        //           ELSE
        //              MOVE 0 TO IND-P56-TP-SOC
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56TpSocFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-TP-SOC
            ws.getIndQuestAdegVer().setTpSoc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-TP-SOC
            ws.getIndQuestAdegVer().setTpSoc(((short)0));
        }
        // COB_CODE: IF P56-FL-IND-SOC-QUOT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-FL-IND-SOC-QUOT
        //           ELSE
        //              MOVE 0 TO IND-P56-FL-IND-SOC-QUOT
        //           END-IF
        if (Conditions.eq(questAdegVer.getP56FlIndSocQuot(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-P56-FL-IND-SOC-QUOT
            ws.getIndQuestAdegVer().setFlIndSocQuot(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-FL-IND-SOC-QUOT
            ws.getIndQuestAdegVer().setFlIndSocQuot(((short)0));
        }
        // COB_CODE: IF P56-TP-SIT-GIUR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-TP-SIT-GIUR
        //           ELSE
        //              MOVE 0 TO IND-P56-TP-SIT-GIUR
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56TpSitGiurFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-TP-SIT-GIUR
            ws.getIndQuestAdegVer().setTpSitGiur(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-TP-SIT-GIUR
            ws.getIndQuestAdegVer().setTpSitGiur(((short)0));
        }
        // COB_CODE: IF P56-PC-QUO-DET-TIT-EFF-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-PC-QUO-DET-TIT-EFF
        //           ELSE
        //              MOVE 0 TO IND-P56-PC-QUO-DET-TIT-EFF
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56PcQuoDetTitEff().getP56PcQuoDetTitEffNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-PC-QUO-DET-TIT-EFF
            ws.getIndQuestAdegVer().setPcQuoDetTitEff(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-PC-QUO-DET-TIT-EFF
            ws.getIndQuestAdegVer().setPcQuoDetTitEff(((short)0));
        }
        // COB_CODE: IF P56-TP-PRFL-RSH-PEP-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-TP-PRFL-RSH-PEP
        //           ELSE
        //              MOVE 0 TO IND-P56-TP-PRFL-RSH-PEP
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56TpPrflRshPepFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-TP-PRFL-RSH-PEP
            ws.getIndQuestAdegVer().setTpPrflRshPep(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-TP-PRFL-RSH-PEP
            ws.getIndQuestAdegVer().setTpPrflRshPep(((short)0));
        }
        // COB_CODE: IF P56-TP-PEP-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-TP-PEP
        //           ELSE
        //              MOVE 0 TO IND-P56-TP-PEP
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56TpPepFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-TP-PEP
            ws.getIndQuestAdegVer().setTpPep(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-TP-PEP
            ws.getIndQuestAdegVer().setTpPep(((short)0));
        }
        // COB_CODE: IF P56-FL-NOT-PREG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-FL-NOT-PREG
        //           ELSE
        //              MOVE 0 TO IND-P56-FL-NOT-PREG
        //           END-IF
        if (Conditions.eq(questAdegVer.getP56FlNotPreg(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-P56-FL-NOT-PREG
            ws.getIndQuestAdegVer().setFlNotPreg(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-FL-NOT-PREG
            ws.getIndQuestAdegVer().setFlNotPreg(((short)0));
        }
        // COB_CODE: IF P56-DT-INI-FNT-REDD-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-DT-INI-FNT-REDD
        //           ELSE
        //              MOVE 0 TO IND-P56-DT-INI-FNT-REDD
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56DtIniFntRedd().getP56DtIniFntReddNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-DT-INI-FNT-REDD
            ws.getIndQuestAdegVer().setDtIniFntRedd(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-DT-INI-FNT-REDD
            ws.getIndQuestAdegVer().setDtIniFntRedd(((short)0));
        }
        // COB_CODE: IF P56-FNT-REDD-2-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-FNT-REDD-2
        //           ELSE
        //              MOVE 0 TO IND-P56-FNT-REDD-2
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56FntRedd2Formatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-FNT-REDD-2
            ws.getIndQuestAdegVer().setFntRedd2(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-FNT-REDD-2
            ws.getIndQuestAdegVer().setFntRedd2(((short)0));
        }
        // COB_CODE: IF P56-DT-INI-FNT-REDD-2-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-DT-INI-FNT-REDD-2
        //           ELSE
        //              MOVE 0 TO IND-P56-DT-INI-FNT-REDD-2
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56DtIniFntRedd2().getP56DtIniFntRedd2NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-DT-INI-FNT-REDD-2
            ws.getIndQuestAdegVer().setDtIniFntRedd2(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-DT-INI-FNT-REDD-2
            ws.getIndQuestAdegVer().setDtIniFntRedd2(((short)0));
        }
        // COB_CODE: IF P56-FNT-REDD-3-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-FNT-REDD-3
        //           ELSE
        //              MOVE 0 TO IND-P56-FNT-REDD-3
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56FntRedd3Formatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-FNT-REDD-3
            ws.getIndQuestAdegVer().setFntRedd3(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-FNT-REDD-3
            ws.getIndQuestAdegVer().setFntRedd3(((short)0));
        }
        // COB_CODE: IF P56-DT-INI-FNT-REDD-3-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-DT-INI-FNT-REDD-3
        //           ELSE
        //              MOVE 0 TO IND-P56-DT-INI-FNT-REDD-3
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56DtIniFntRedd3().getP56DtIniFntRedd3NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-DT-INI-FNT-REDD-3
            ws.getIndQuestAdegVer().setDtIniFntRedd3(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-DT-INI-FNT-REDD-3
            ws.getIndQuestAdegVer().setDtIniFntRedd3(((short)0));
        }
        // COB_CODE: IF P56-MOT-ASS-TIT-EFF = HIGH-VALUES
        //              MOVE -1 TO IND-P56-MOT-ASS-TIT-EFF
        //           ELSE
        //              MOVE 0 TO IND-P56-MOT-ASS-TIT-EFF
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56MotAssTitEff(), QuestAdegVer.Len.P56_MOT_ASS_TIT_EFF)) {
            // COB_CODE: MOVE -1 TO IND-P56-MOT-ASS-TIT-EFF
            ws.getIndQuestAdegVer().setMotAssTitEff(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-MOT-ASS-TIT-EFF
            ws.getIndQuestAdegVer().setMotAssTitEff(((short)0));
        }
        // COB_CODE: IF P56-FIN-COSTITUZIONE = HIGH-VALUES
        //              MOVE -1 TO IND-P56-FIN-COSTITUZIONE
        //           ELSE
        //              MOVE 0 TO IND-P56-FIN-COSTITUZIONE
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56FinCostituzione(), QuestAdegVer.Len.P56_FIN_COSTITUZIONE)) {
            // COB_CODE: MOVE -1 TO IND-P56-FIN-COSTITUZIONE
            ws.getIndQuestAdegVer().setFinCostituzione(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-FIN-COSTITUZIONE
            ws.getIndQuestAdegVer().setFinCostituzione(((short)0));
        }
        // COB_CODE: IF P56-DESC-IMP-CAR-PUB = HIGH-VALUES
        //              MOVE -1 TO IND-P56-DESC-IMP-CAR-PUB
        //           ELSE
        //              MOVE 0 TO IND-P56-DESC-IMP-CAR-PUB
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56DescImpCarPub(), QuestAdegVer.Len.P56_DESC_IMP_CAR_PUB)) {
            // COB_CODE: MOVE -1 TO IND-P56-DESC-IMP-CAR-PUB
            ws.getIndQuestAdegVer().setDescImpCarPub(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-DESC-IMP-CAR-PUB
            ws.getIndQuestAdegVer().setDescImpCarPub(((short)0));
        }
        // COB_CODE: IF P56-DESC-SCO-FIN-RAPP = HIGH-VALUES
        //              MOVE -1 TO IND-P56-DESC-SCO-FIN-RAPP
        //           ELSE
        //              MOVE 0 TO IND-P56-DESC-SCO-FIN-RAPP
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56DescScoFinRapp(), QuestAdegVer.Len.P56_DESC_SCO_FIN_RAPP)) {
            // COB_CODE: MOVE -1 TO IND-P56-DESC-SCO-FIN-RAPP
            ws.getIndQuestAdegVer().setDescScoFinRapp(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-DESC-SCO-FIN-RAPP
            ws.getIndQuestAdegVer().setDescScoFinRapp(((short)0));
        }
        // COB_CODE: IF P56-DESC-PROC-PNL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-DESC-PROC-PNL
        //           ELSE
        //              MOVE 0 TO IND-P56-DESC-PROC-PNL
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56DescProcPnl(), QuestAdegVer.Len.P56_DESC_PROC_PNL)) {
            // COB_CODE: MOVE -1 TO IND-P56-DESC-PROC-PNL
            ws.getIndQuestAdegVer().setDescProcPnl(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-DESC-PROC-PNL
            ws.getIndQuestAdegVer().setDescProcPnl(((short)0));
        }
        // COB_CODE: IF P56-DESC-NOT-PREG = HIGH-VALUES
        //              MOVE -1 TO IND-P56-DESC-NOT-PREG
        //           ELSE
        //              MOVE 0 TO IND-P56-DESC-NOT-PREG
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56DescNotPreg(), QuestAdegVer.Len.P56_DESC_NOT_PREG)) {
            // COB_CODE: MOVE -1 TO IND-P56-DESC-NOT-PREG
            ws.getIndQuestAdegVer().setDescNotPreg(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-DESC-NOT-PREG
            ws.getIndQuestAdegVer().setDescNotPreg(((short)0));
        }
        // COB_CODE: IF P56-ID-ASSICURATI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-ID-ASSICURATI
        //           ELSE
        //              MOVE 0 TO IND-P56-ID-ASSICURATI
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56IdAssicurati().getP56IdAssicuratiNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-ID-ASSICURATI
            ws.getIndQuestAdegVer().setIdAssicurati(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-ID-ASSICURATI
            ws.getIndQuestAdegVer().setIdAssicurati(((short)0));
        }
        // COB_CODE: IF P56-REDD-CON-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-REDD-CON
        //           ELSE
        //              MOVE 0 TO IND-P56-REDD-CON
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56ReddCon().getP56ReddConNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-REDD-CON
            ws.getIndQuestAdegVer().setReddCon(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-REDD-CON
            ws.getIndQuestAdegVer().setReddCon(((short)0));
        }
        // COB_CODE: IF P56-DESC-LIB-MOT-RISC = HIGH-VALUES
        //              MOVE -1 TO IND-P56-DESC-LIB-MOT-RISC
        //           ELSE
        //              MOVE 0 TO IND-P56-DESC-LIB-MOT-RISC
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56DescLibMotRisc(), QuestAdegVer.Len.P56_DESC_LIB_MOT_RISC)) {
            // COB_CODE: MOVE -1 TO IND-P56-DESC-LIB-MOT-RISC
            ws.getIndQuestAdegVer().setDescLibMotRisc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-DESC-LIB-MOT-RISC
            ws.getIndQuestAdegVer().setDescLibMotRisc(((short)0));
        }
        // COB_CODE: IF P56-TP-MOT-ASS-TIT-EFF-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-TP-MOT-ASS-TIT-EFF
        //           ELSE
        //              MOVE 0 TO IND-P56-TP-MOT-ASS-TIT-EFF
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56TpMotAssTitEffFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-TP-MOT-ASS-TIT-EFF
            ws.getIndQuestAdegVer().setTpMotAssTitEff(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-TP-MOT-ASS-TIT-EFF
            ws.getIndQuestAdegVer().setTpMotAssTitEff(((short)0));
        }
        // COB_CODE: IF P56-TP-RAG-RAPP-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-TP-RAG-RAPP
        //           ELSE
        //              MOVE 0 TO IND-P56-TP-RAG-RAPP
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56TpRagRappFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-TP-RAG-RAPP
            ws.getIndQuestAdegVer().setTpRagRapp(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-TP-RAG-RAPP
            ws.getIndQuestAdegVer().setTpRagRapp(((short)0));
        }
        // COB_CODE: IF P56-COD-CAN-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-COD-CAN
        //           ELSE
        //              MOVE 0 TO IND-P56-COD-CAN
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56CodCan().getP56CodCanNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-COD-CAN
            ws.getIndQuestAdegVer().setCodCan(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-COD-CAN
            ws.getIndQuestAdegVer().setCodCan(((short)0));
        }
        // COB_CODE: IF P56-TP-FIN-COST-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-TP-FIN-COST
        //           ELSE
        //              MOVE 0 TO IND-P56-TP-FIN-COST
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56TpFinCostFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-TP-FIN-COST
            ws.getIndQuestAdegVer().setTpFinCost(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-TP-FIN-COST
            ws.getIndQuestAdegVer().setTpFinCost(((short)0));
        }
        // COB_CODE: IF P56-NAZ-DEST-FND-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-NAZ-DEST-FND
        //           ELSE
        //              MOVE 0 TO IND-P56-NAZ-DEST-FND
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56NazDestFndFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-NAZ-DEST-FND
            ws.getIndQuestAdegVer().setNazDestFnd(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-NAZ-DEST-FND
            ws.getIndQuestAdegVer().setNazDestFnd(((short)0));
        }
        // COB_CODE: IF P56-FL-AU-FATCA-AEOI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-FL-AU-FATCA-AEOI
        //           ELSE
        //              MOVE 0 TO IND-P56-FL-AU-FATCA-AEOI
        //           END-IF
        if (Conditions.eq(questAdegVer.getP56FlAuFatcaAeoi(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-P56-FL-AU-FATCA-AEOI
            ws.getIndQuestAdegVer().setFlAuFatcaAeoi(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-FL-AU-FATCA-AEOI
            ws.getIndQuestAdegVer().setFlAuFatcaAeoi(((short)0));
        }
        // COB_CODE: IF P56-TP-CAR-FIN-GIUR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-TP-CAR-FIN-GIUR
        //           ELSE
        //              MOVE 0 TO IND-P56-TP-CAR-FIN-GIUR
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56TpCarFinGiurFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-TP-CAR-FIN-GIUR
            ws.getIndQuestAdegVer().setTpCarFinGiur(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-TP-CAR-FIN-GIUR
            ws.getIndQuestAdegVer().setTpCarFinGiur(((short)0));
        }
        // COB_CODE: IF P56-TP-CAR-FIN-GIUR-AT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-TP-CAR-FIN-GIUR-AT
        //           ELSE
        //              MOVE 0 TO IND-P56-TP-CAR-FIN-GIUR-AT
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56TpCarFinGiurAtFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-TP-CAR-FIN-GIUR-AT
            ws.getIndQuestAdegVer().setTpCarFinGiurAt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-TP-CAR-FIN-GIUR-AT
            ws.getIndQuestAdegVer().setTpCarFinGiurAt(((short)0));
        }
        // COB_CODE: IF P56-TP-CAR-FIN-GIUR-PA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-TP-CAR-FIN-GIUR-PA
        //           ELSE
        //              MOVE 0 TO IND-P56-TP-CAR-FIN-GIUR-PA
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56TpCarFinGiurPaFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-TP-CAR-FIN-GIUR-PA
            ws.getIndQuestAdegVer().setTpCarFinGiurPa(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-TP-CAR-FIN-GIUR-PA
            ws.getIndQuestAdegVer().setTpCarFinGiurPa(((short)0));
        }
        // COB_CODE: IF P56-FL-ISTITUZ-FIN-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-FL-ISTITUZ-FIN
        //           ELSE
        //              MOVE 0 TO IND-P56-FL-ISTITUZ-FIN
        //           END-IF
        if (Conditions.eq(questAdegVer.getP56FlIstituzFin(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-P56-FL-ISTITUZ-FIN
            ws.getIndQuestAdegVer().setFlIstituzFin(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-FL-ISTITUZ-FIN
            ws.getIndQuestAdegVer().setFlIstituzFin(((short)0));
        }
        // COB_CODE: IF P56-TP-ORI-FND-TIT-EFF-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-TP-ORI-FND-TIT-EFF
        //           ELSE
        //              MOVE 0 TO IND-P56-TP-ORI-FND-TIT-EFF
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56TpOriFndTitEffFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-TP-ORI-FND-TIT-EFF
            ws.getIndQuestAdegVer().setTpOriFndTitEff(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-TP-ORI-FND-TIT-EFF
            ws.getIndQuestAdegVer().setTpOriFndTitEff(((short)0));
        }
        // COB_CODE: IF P56-PC-ESP-AG-PA-MSC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-PC-ESP-AG-PA-MSC
        //           ELSE
        //              MOVE 0 TO IND-P56-PC-ESP-AG-PA-MSC
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56PcEspAgPaMsc().getP56PcEspAgPaMscNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-PC-ESP-AG-PA-MSC
            ws.getIndQuestAdegVer().setPcEspAgPaMsc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-PC-ESP-AG-PA-MSC
            ws.getIndQuestAdegVer().setPcEspAgPaMsc(((short)0));
        }
        // COB_CODE: IF P56-FL-PR-TR-USA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-FL-PR-TR-USA
        //           ELSE
        //              MOVE 0 TO IND-P56-FL-PR-TR-USA
        //           END-IF
        if (Conditions.eq(questAdegVer.getP56FlPrTrUsa(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-P56-FL-PR-TR-USA
            ws.getIndQuestAdegVer().setFlPrTrUsa(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-FL-PR-TR-USA
            ws.getIndQuestAdegVer().setFlPrTrUsa(((short)0));
        }
        // COB_CODE: IF P56-FL-PR-TR-NO-USA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-FL-PR-TR-NO-USA
        //           ELSE
        //              MOVE 0 TO IND-P56-FL-PR-TR-NO-USA
        //           END-IF
        if (Conditions.eq(questAdegVer.getP56FlPrTrNoUsa(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-P56-FL-PR-TR-NO-USA
            ws.getIndQuestAdegVer().setFlPrTrNoUsa(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-FL-PR-TR-NO-USA
            ws.getIndQuestAdegVer().setFlPrTrNoUsa(((short)0));
        }
        // COB_CODE: IF P56-PC-RIP-PAT-AS-VITA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-PC-RIP-PAT-AS-VITA
        //           ELSE
        //              MOVE 0 TO IND-P56-PC-RIP-PAT-AS-VITA
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56PcRipPatAsVita().getP56PcRipPatAsVitaNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-PC-RIP-PAT-AS-VITA
            ws.getIndQuestAdegVer().setPcRipPatAsVita(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-PC-RIP-PAT-AS-VITA
            ws.getIndQuestAdegVer().setPcRipPatAsVita(((short)0));
        }
        // COB_CODE: IF P56-PC-RIP-PAT-IM-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-PC-RIP-PAT-IM
        //           ELSE
        //              MOVE 0 TO IND-P56-PC-RIP-PAT-IM
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56PcRipPatIm().getP56PcRipPatImNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-PC-RIP-PAT-IM
            ws.getIndQuestAdegVer().setPcRipPatIm(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-PC-RIP-PAT-IM
            ws.getIndQuestAdegVer().setPcRipPatIm(((short)0));
        }
        // COB_CODE: IF P56-PC-RIP-PAT-SET-IM-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-PC-RIP-PAT-SET-IM
        //           ELSE
        //              MOVE 0 TO IND-P56-PC-RIP-PAT-SET-IM
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56PcRipPatSetIm().getP56PcRipPatSetImNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-PC-RIP-PAT-SET-IM
            ws.getIndQuestAdegVer().setPcRipPatSetIm(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-PC-RIP-PAT-SET-IM
            ws.getIndQuestAdegVer().setPcRipPatSetIm(((short)0));
        }
        // COB_CODE: IF P56-TP-STATUS-AEOI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-TP-STATUS-AEOI
        //           ELSE
        //              MOVE 0 TO IND-P56-TP-STATUS-AEOI
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56TpStatusAeoiFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-TP-STATUS-AEOI
            ws.getIndQuestAdegVer().setTpStatusAeoi(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-TP-STATUS-AEOI
            ws.getIndQuestAdegVer().setTpStatusAeoi(((short)0));
        }
        // COB_CODE: IF P56-TP-STATUS-FATCA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-TP-STATUS-FATCA
        //           ELSE
        //              MOVE 0 TO IND-P56-TP-STATUS-FATCA
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56TpStatusFatcaFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-TP-STATUS-FATCA
            ws.getIndQuestAdegVer().setTpStatusFatca(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-TP-STATUS-FATCA
            ws.getIndQuestAdegVer().setTpStatusFatca(((short)0));
        }
        // COB_CODE: IF P56-FL-RAPP-PA-MSC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-FL-RAPP-PA-MSC
        //           ELSE
        //              MOVE 0 TO IND-P56-FL-RAPP-PA-MSC
        //           END-IF
        if (Conditions.eq(questAdegVer.getP56FlRappPaMsc(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-P56-FL-RAPP-PA-MSC
            ws.getIndQuestAdegVer().setFlRappPaMsc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-FL-RAPP-PA-MSC
            ws.getIndQuestAdegVer().setFlRappPaMsc(((short)0));
        }
        // COB_CODE: IF P56-COD-COMUN-SVOL-ATT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-COD-COMUN-SVOL-ATT
        //           ELSE
        //              MOVE 0 TO IND-P56-COD-COMUN-SVOL-ATT
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56CodComunSvolAttFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-COD-COMUN-SVOL-ATT
            ws.getIndQuestAdegVer().setCodComunSvolAtt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-COD-COMUN-SVOL-ATT
            ws.getIndQuestAdegVer().setCodComunSvolAtt(((short)0));
        }
        // COB_CODE: IF P56-TP-DT-1O-CON-CLI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-TP-DT-1O-CON-CLI
        //           ELSE
        //              MOVE 0 TO IND-P56-TP-DT-1O-CON-CLI
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56TpDt1oConCliFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-TP-DT-1O-CON-CLI
            ws.getIndQuestAdegVer().setTpDt1oConCli(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-TP-DT-1O-CON-CLI
            ws.getIndQuestAdegVer().setTpDt1oConCli(((short)0));
        }
        // COB_CODE: IF P56-TP-MOD-EN-RELA-INT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-TP-MOD-EN-RELA-INT
        //           ELSE
        //              MOVE 0 TO IND-P56-TP-MOD-EN-RELA-INT
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56TpModEnRelaIntFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-TP-MOD-EN-RELA-INT
            ws.getIndQuestAdegVer().setTpModEnRelaInt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-TP-MOD-EN-RELA-INT
            ws.getIndQuestAdegVer().setTpModEnRelaInt(((short)0));
        }
        // COB_CODE: IF P56-TP-REDD-ANNU-LRD-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-TP-REDD-ANNU-LRD
        //           ELSE
        //              MOVE 0 TO IND-P56-TP-REDD-ANNU-LRD
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56TpReddAnnuLrdFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-TP-REDD-ANNU-LRD
            ws.getIndQuestAdegVer().setTpReddAnnuLrd(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-TP-REDD-ANNU-LRD
            ws.getIndQuestAdegVer().setTpReddAnnuLrd(((short)0));
        }
        // COB_CODE: IF P56-TP-REDD-CON-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-TP-REDD-CON
        //           ELSE
        //              MOVE 0 TO IND-P56-TP-REDD-CON
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56TpReddConFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-TP-REDD-CON
            ws.getIndQuestAdegVer().setTpReddCon(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-TP-REDD-CON
            ws.getIndQuestAdegVer().setTpReddCon(((short)0));
        }
        // COB_CODE: IF P56-TP-OPER-SOC-FID-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-TP-OPER-SOC-FID
        //           ELSE
        //              MOVE 0 TO IND-P56-TP-OPER-SOC-FID
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56TpOperSocFidFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-TP-OPER-SOC-FID
            ws.getIndQuestAdegVer().setTpOperSocFid(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-TP-OPER-SOC-FID
            ws.getIndQuestAdegVer().setTpOperSocFid(((short)0));
        }
        // COB_CODE: IF P56-COD-PA-ESP-MSC-1-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-COD-PA-ESP-MSC-1
        //           ELSE
        //              MOVE 0 TO IND-P56-COD-PA-ESP-MSC-1
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56CodPaEspMsc1Formatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-COD-PA-ESP-MSC-1
            ws.getIndQuestAdegVer().setCodPaEspMsc1(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-COD-PA-ESP-MSC-1
            ws.getIndQuestAdegVer().setCodPaEspMsc1(((short)0));
        }
        // COB_CODE: IF P56-IMP-PA-ESP-MSC-1-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-IMP-PA-ESP-MSC-1
        //           ELSE
        //              MOVE 0 TO IND-P56-IMP-PA-ESP-MSC-1
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56ImpPaEspMsc1().getP56ImpPaEspMsc1NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-IMP-PA-ESP-MSC-1
            ws.getIndQuestAdegVer().setImpPaEspMsc1(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-IMP-PA-ESP-MSC-1
            ws.getIndQuestAdegVer().setImpPaEspMsc1(((short)0));
        }
        // COB_CODE: IF P56-COD-PA-ESP-MSC-2-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-COD-PA-ESP-MSC-2
        //           ELSE
        //              MOVE 0 TO IND-P56-COD-PA-ESP-MSC-2
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56CodPaEspMsc2Formatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-COD-PA-ESP-MSC-2
            ws.getIndQuestAdegVer().setCodPaEspMsc2(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-COD-PA-ESP-MSC-2
            ws.getIndQuestAdegVer().setCodPaEspMsc2(((short)0));
        }
        // COB_CODE: IF P56-IMP-PA-ESP-MSC-2-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-IMP-PA-ESP-MSC-2
        //           ELSE
        //              MOVE 0 TO IND-P56-IMP-PA-ESP-MSC-2
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56ImpPaEspMsc2().getP56ImpPaEspMsc2NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-IMP-PA-ESP-MSC-2
            ws.getIndQuestAdegVer().setImpPaEspMsc2(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-IMP-PA-ESP-MSC-2
            ws.getIndQuestAdegVer().setImpPaEspMsc2(((short)0));
        }
        // COB_CODE: IF P56-COD-PA-ESP-MSC-3-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-COD-PA-ESP-MSC-3
        //           ELSE
        //              MOVE 0 TO IND-P56-COD-PA-ESP-MSC-3
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56CodPaEspMsc3Formatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-COD-PA-ESP-MSC-3
            ws.getIndQuestAdegVer().setCodPaEspMsc3(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-COD-PA-ESP-MSC-3
            ws.getIndQuestAdegVer().setCodPaEspMsc3(((short)0));
        }
        // COB_CODE: IF P56-IMP-PA-ESP-MSC-3-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-IMP-PA-ESP-MSC-3
        //           ELSE
        //              MOVE 0 TO IND-P56-IMP-PA-ESP-MSC-3
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56ImpPaEspMsc3().getP56ImpPaEspMsc3NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-IMP-PA-ESP-MSC-3
            ws.getIndQuestAdegVer().setImpPaEspMsc3(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-IMP-PA-ESP-MSC-3
            ws.getIndQuestAdegVer().setImpPaEspMsc3(((short)0));
        }
        // COB_CODE: IF P56-COD-PA-ESP-MSC-4-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-COD-PA-ESP-MSC-4
        //           ELSE
        //              MOVE 0 TO IND-P56-COD-PA-ESP-MSC-4
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56CodPaEspMsc4Formatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-COD-PA-ESP-MSC-4
            ws.getIndQuestAdegVer().setCodPaEspMsc4(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-COD-PA-ESP-MSC-4
            ws.getIndQuestAdegVer().setCodPaEspMsc4(((short)0));
        }
        // COB_CODE: IF P56-IMP-PA-ESP-MSC-4-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-IMP-PA-ESP-MSC-4
        //           ELSE
        //              MOVE 0 TO IND-P56-IMP-PA-ESP-MSC-4
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56ImpPaEspMsc4().getP56ImpPaEspMsc4NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-IMP-PA-ESP-MSC-4
            ws.getIndQuestAdegVer().setImpPaEspMsc4(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-IMP-PA-ESP-MSC-4
            ws.getIndQuestAdegVer().setImpPaEspMsc4(((short)0));
        }
        // COB_CODE: IF P56-COD-PA-ESP-MSC-5-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-COD-PA-ESP-MSC-5
        //           ELSE
        //              MOVE 0 TO IND-P56-COD-PA-ESP-MSC-5
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56CodPaEspMsc5Formatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-COD-PA-ESP-MSC-5
            ws.getIndQuestAdegVer().setCodPaEspMsc5(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-COD-PA-ESP-MSC-5
            ws.getIndQuestAdegVer().setCodPaEspMsc5(((short)0));
        }
        // COB_CODE: IF P56-IMP-PA-ESP-MSC-5-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-IMP-PA-ESP-MSC-5
        //           ELSE
        //              MOVE 0 TO IND-P56-IMP-PA-ESP-MSC-5
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56ImpPaEspMsc5().getP56ImpPaEspMsc5NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-IMP-PA-ESP-MSC-5
            ws.getIndQuestAdegVer().setImpPaEspMsc5(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-IMP-PA-ESP-MSC-5
            ws.getIndQuestAdegVer().setImpPaEspMsc5(((short)0));
        }
        // COB_CODE: IF P56-DESC-ORGN-FND = HIGH-VALUES
        //              MOVE -1 TO IND-P56-DESC-ORGN-FND
        //           ELSE
        //              MOVE 0 TO IND-P56-DESC-ORGN-FND
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56DescOrgnFnd(), QuestAdegVer.Len.P56_DESC_ORGN_FND)) {
            // COB_CODE: MOVE -1 TO IND-P56-DESC-ORGN-FND
            ws.getIndQuestAdegVer().setDescOrgnFnd(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-DESC-ORGN-FND
            ws.getIndQuestAdegVer().setDescOrgnFnd(((short)0));
        }
        // COB_CODE: IF P56-COD-AUT-DUE-DIL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-COD-AUT-DUE-DIL
        //           ELSE
        //              MOVE 0 TO IND-P56-COD-AUT-DUE-DIL
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56CodAutDueDilFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-COD-AUT-DUE-DIL
            ws.getIndQuestAdegVer().setCodAutDueDil(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-COD-AUT-DUE-DIL
            ws.getIndQuestAdegVer().setCodAutDueDil(((short)0));
        }
        // COB_CODE: IF P56-FL-PR-QUEST-FATCA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-FL-PR-QUEST-FATCA
        //           ELSE
        //              MOVE 0 TO IND-P56-FL-PR-QUEST-FATCA
        //           END-IF
        if (Conditions.eq(questAdegVer.getP56FlPrQuestFatca(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-P56-FL-PR-QUEST-FATCA
            ws.getIndQuestAdegVer().setFlPrQuestFatca(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-FL-PR-QUEST-FATCA
            ws.getIndQuestAdegVer().setFlPrQuestFatca(((short)0));
        }
        // COB_CODE: IF P56-FL-PR-QUEST-AEOI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-FL-PR-QUEST-AEOI
        //           ELSE
        //              MOVE 0 TO IND-P56-FL-PR-QUEST-AEOI
        //           END-IF
        if (Conditions.eq(questAdegVer.getP56FlPrQuestAeoi(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-P56-FL-PR-QUEST-AEOI
            ws.getIndQuestAdegVer().setFlPrQuestAeoi(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-FL-PR-QUEST-AEOI
            ws.getIndQuestAdegVer().setFlPrQuestAeoi(((short)0));
        }
        // COB_CODE: IF P56-FL-PR-QUEST-OFAC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-FL-PR-QUEST-OFAC
        //           ELSE
        //              MOVE 0 TO IND-P56-FL-PR-QUEST-OFAC
        //           END-IF
        if (Conditions.eq(questAdegVer.getP56FlPrQuestOfac(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-P56-FL-PR-QUEST-OFAC
            ws.getIndQuestAdegVer().setFlPrQuestOfac(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-FL-PR-QUEST-OFAC
            ws.getIndQuestAdegVer().setFlPrQuestOfac(((short)0));
        }
        // COB_CODE: IF P56-FL-PR-QUEST-KYC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-FL-PR-QUEST-KYC
        //           ELSE
        //              MOVE 0 TO IND-P56-FL-PR-QUEST-KYC
        //           END-IF
        if (Conditions.eq(questAdegVer.getP56FlPrQuestKyc(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-P56-FL-PR-QUEST-KYC
            ws.getIndQuestAdegVer().setFlPrQuestKyc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-FL-PR-QUEST-KYC
            ws.getIndQuestAdegVer().setFlPrQuestKyc(((short)0));
        }
        // COB_CODE: IF P56-FL-PR-QUEST-MSCQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-FL-PR-QUEST-MSCQ
        //           ELSE
        //              MOVE 0 TO IND-P56-FL-PR-QUEST-MSCQ
        //           END-IF
        if (Conditions.eq(questAdegVer.getP56FlPrQuestMscq(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-P56-FL-PR-QUEST-MSCQ
            ws.getIndQuestAdegVer().setFlPrQuestMscq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-FL-PR-QUEST-MSCQ
            ws.getIndQuestAdegVer().setFlPrQuestMscq(((short)0));
        }
        // COB_CODE: IF P56-TP-NOT-PREG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-TP-NOT-PREG
        //           ELSE
        //              MOVE 0 TO IND-P56-TP-NOT-PREG
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56TpNotPregFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-TP-NOT-PREG
            ws.getIndQuestAdegVer().setTpNotPreg(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-TP-NOT-PREG
            ws.getIndQuestAdegVer().setTpNotPreg(((short)0));
        }
        // COB_CODE: IF P56-TP-PROC-PNL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-TP-PROC-PNL
        //           ELSE
        //              MOVE 0 TO IND-P56-TP-PROC-PNL
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56TpProcPnlFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-TP-PROC-PNL
            ws.getIndQuestAdegVer().setTpProcPnl(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-TP-PROC-PNL
            ws.getIndQuestAdegVer().setTpProcPnl(((short)0));
        }
        // COB_CODE: IF P56-COD-IMP-CAR-PUB-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-COD-IMP-CAR-PUB
        //           ELSE
        //              MOVE 0 TO IND-P56-COD-IMP-CAR-PUB
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56CodImpCarPubFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-COD-IMP-CAR-PUB
            ws.getIndQuestAdegVer().setCodImpCarPub(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-COD-IMP-CAR-PUB
            ws.getIndQuestAdegVer().setCodImpCarPub(((short)0));
        }
        // COB_CODE: IF P56-OPRZ-SOSPETTE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-OPRZ-SOSPETTE
        //           ELSE
        //              MOVE 0 TO IND-P56-OPRZ-SOSPETTE
        //           END-IF
        if (Conditions.eq(questAdegVer.getP56OprzSospette(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-P56-OPRZ-SOSPETTE
            ws.getIndQuestAdegVer().setOprzSospette(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-OPRZ-SOSPETTE
            ws.getIndQuestAdegVer().setOprzSospette(((short)0));
        }
        // COB_CODE: IF P56-ULT-FATT-ANNU-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-ULT-FATT-ANNU
        //           ELSE
        //              MOVE 0 TO IND-P56-ULT-FATT-ANNU
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56UltFattAnnu().getP56UltFattAnnuNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-ULT-FATT-ANNU
            ws.getIndQuestAdegVer().setUltFattAnnu(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-ULT-FATT-ANNU
            ws.getIndQuestAdegVer().setUltFattAnnu(((short)0));
        }
        // COB_CODE: IF P56-DESC-PEP = HIGH-VALUES
        //              MOVE -1 TO IND-P56-DESC-PEP
        //           ELSE
        //              MOVE 0 TO IND-P56-DESC-PEP
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56DescPep(), QuestAdegVer.Len.P56_DESC_PEP)) {
            // COB_CODE: MOVE -1 TO IND-P56-DESC-PEP
            ws.getIndQuestAdegVer().setDescPep(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-DESC-PEP
            ws.getIndQuestAdegVer().setDescPep(((short)0));
        }
        // COB_CODE: IF P56-NUM-TEL-2-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-NUM-TEL-2
        //           ELSE
        //              MOVE 0 TO IND-P56-NUM-TEL-2
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56NumTel2Formatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-NUM-TEL-2
            ws.getIndQuestAdegVer().setNumTel2(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-NUM-TEL-2
            ws.getIndQuestAdegVer().setNumTel2(((short)0));
        }
        // COB_CODE: IF P56-IMP-AFI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-IMP-AFI
        //           ELSE
        //              MOVE 0 TO IND-P56-IMP-AFI
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56ImpAfi().getP56ImpAfiNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-IMP-AFI
            ws.getIndQuestAdegVer().setImpAfi(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-IMP-AFI
            ws.getIndQuestAdegVer().setImpAfi(((short)0));
        }
        // COB_CODE: IF P56-FL-NEW-PRO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-FL-NEW-PRO
        //           ELSE
        //              MOVE 0 TO IND-P56-FL-NEW-PRO
        //           END-IF
        if (Conditions.eq(questAdegVer.getP56FlNewPro(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-P56-FL-NEW-PRO
            ws.getIndQuestAdegVer().setFlNewPro(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-FL-NEW-PRO
            ws.getIndQuestAdegVer().setFlNewPro(((short)0));
        }
        // COB_CODE: IF P56-TP-MOT-CAMBIO-CNTR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-TP-MOT-CAMBIO-CNTR
        //           ELSE
        //              MOVE 0 TO IND-P56-TP-MOT-CAMBIO-CNTR
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56TpMotCambioCntrFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-TP-MOT-CAMBIO-CNTR
            ws.getIndQuestAdegVer().setTpMotCambioCntr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-TP-MOT-CAMBIO-CNTR
            ws.getIndQuestAdegVer().setTpMotCambioCntr(((short)0));
        }
        // COB_CODE: IF P56-DESC-MOT-CAMBIO-CN = HIGH-VALUES
        //              MOVE -1 TO IND-P56-DESC-MOT-CAMBIO-CN
        //           ELSE
        //              MOVE 0 TO IND-P56-DESC-MOT-CAMBIO-CN
        //           END-IF
        if (Characters.EQ_HIGH.test(questAdegVer.getP56DescMotCambioCn(), QuestAdegVer.Len.P56_DESC_MOT_CAMBIO_CN)) {
            // COB_CODE: MOVE -1 TO IND-P56-DESC-MOT-CAMBIO-CN
            ws.getIndQuestAdegVer().setDescMotCambioCn(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-DESC-MOT-CAMBIO-CN
            ws.getIndQuestAdegVer().setDescMotCambioCn(((short)0));
        }
        // COB_CODE: IF P56-COD-SOGG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P56-COD-SOGG
        //           ELSE
        //              MOVE 0 TO IND-P56-COD-SOGG
        //           END-IF.
        if (Characters.EQ_HIGH.test(questAdegVer.getP56CodSoggFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P56-COD-SOGG
            ws.getIndQuestAdegVer().setCodSogg(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P56-COD-SOGG
            ws.getIndQuestAdegVer().setCodSogg(((short)0));
        }
    }

    /**Original name: Z400-SEQ-RIGA<br>*/
    private void z400SeqRiga() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z500-AGGIORNAMENTO-STORICO<br>*/
    private void z500AggiornamentoStorico() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z550-AGG-STORICO-SOLO-INS<br>*/
    private void z550AggStoricoSoloIns() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z900-CONVERTI-N-TO-X<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da 9(8) comp-3 a date
	 * ----</pre>*/
    private void z900ConvertiNToX() {
        // COB_CODE: IF IND-P56-DT-INI-FNT-REDD = 0
        //               MOVE WS-DATE-X      TO P56-DT-INI-FNT-REDD-DB
        //           END-IF
        if (ws.getIndQuestAdegVer().getDtIniFntRedd() == 0) {
            // COB_CODE: MOVE P56-DT-INI-FNT-REDD TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(questAdegVer.getP56DtIniFntRedd().getP56DtIniFntRedd(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO P56-DT-INI-FNT-REDD-DB
            ws.getQuestAdegVerDb().setDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-P56-DT-INI-FNT-REDD-2 = 0
        //               MOVE WS-DATE-X      TO P56-DT-INI-FNT-REDD-2-DB
        //           END-IF
        if (ws.getIndQuestAdegVer().getDtIniFntRedd2() == 0) {
            // COB_CODE: MOVE P56-DT-INI-FNT-REDD-2 TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(questAdegVer.getP56DtIniFntRedd2().getP56DtIniFntRedd2(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO P56-DT-INI-FNT-REDD-2-DB
            ws.getQuestAdegVerDb().setRedd2Db(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-P56-DT-INI-FNT-REDD-3 = 0
        //               MOVE WS-DATE-X      TO P56-DT-INI-FNT-REDD-3-DB
        //           END-IF.
        if (ws.getIndQuestAdegVer().getDtIniFntRedd3() == 0) {
            // COB_CODE: MOVE P56-DT-INI-FNT-REDD-3 TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(questAdegVer.getP56DtIniFntRedd3().getP56DtIniFntRedd3(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO P56-DT-INI-FNT-REDD-3-DB
            ws.getQuestAdegVerDb().setRedd3Db(ws.getIdsv0010().getWsDateX());
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: IF IND-P56-DT-INI-FNT-REDD = 0
        //               MOVE WS-DATE-N      TO P56-DT-INI-FNT-REDD
        //           END-IF
        if (ws.getIndQuestAdegVer().getDtIniFntRedd() == 0) {
            // COB_CODE: MOVE P56-DT-INI-FNT-REDD-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getQuestAdegVerDb().getDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO P56-DT-INI-FNT-REDD
            questAdegVer.getP56DtIniFntRedd().setP56DtIniFntRedd(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-P56-DT-INI-FNT-REDD-2 = 0
        //               MOVE WS-DATE-N      TO P56-DT-INI-FNT-REDD-2
        //           END-IF
        if (ws.getIndQuestAdegVer().getDtIniFntRedd2() == 0) {
            // COB_CODE: MOVE P56-DT-INI-FNT-REDD-2-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getQuestAdegVerDb().getRedd2Db());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO P56-DT-INI-FNT-REDD-2
            questAdegVer.getP56DtIniFntRedd2().setP56DtIniFntRedd2(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-P56-DT-INI-FNT-REDD-3 = 0
        //               MOVE WS-DATE-N      TO P56-DT-INI-FNT-REDD-3
        //           END-IF.
        if (ws.getIndQuestAdegVer().getDtIniFntRedd3() == 0) {
            // COB_CODE: MOVE P56-DT-INI-FNT-REDD-3-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getQuestAdegVerDb().getRedd3Db());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO P56-DT-INI-FNT-REDD-3
            questAdegVer.getP56DtIniFntRedd3().setP56DtIniFntRedd3(ws.getIdsv0010().getWsDateN());
        }
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
        // COB_CODE: MOVE LENGTH OF P56-LUOGO-COSTITUZIONE
        //                       TO P56-LUOGO-COSTITUZIONE-LEN
        questAdegVer.setP56LuogoCostituzioneLen(((short)QuestAdegVer.Len.P56_LUOGO_COSTITUZIONE));
        // COB_CODE: MOVE LENGTH OF P56-MOT-ASS-TIT-EFF
        //                       TO P56-MOT-ASS-TIT-EFF-LEN
        questAdegVer.setP56MotAssTitEffLen(((short)QuestAdegVer.Len.P56_MOT_ASS_TIT_EFF));
        // COB_CODE: MOVE LENGTH OF P56-FIN-COSTITUZIONE
        //                       TO P56-FIN-COSTITUZIONE-LEN
        questAdegVer.setP56FinCostituzioneLen(((short)QuestAdegVer.Len.P56_FIN_COSTITUZIONE));
        // COB_CODE: MOVE LENGTH OF P56-DESC-IMP-CAR-PUB
        //                       TO P56-DESC-IMP-CAR-PUB-LEN
        questAdegVer.setP56DescImpCarPubLen(((short)QuestAdegVer.Len.P56_DESC_IMP_CAR_PUB));
        // COB_CODE: MOVE LENGTH OF P56-DESC-SCO-FIN-RAPP
        //                       TO P56-DESC-SCO-FIN-RAPP-LEN
        questAdegVer.setP56DescScoFinRappLen(((short)QuestAdegVer.Len.P56_DESC_SCO_FIN_RAPP));
        // COB_CODE: MOVE LENGTH OF P56-DESC-PROC-PNL
        //                       TO P56-DESC-PROC-PNL-LEN
        questAdegVer.setP56DescProcPnlLen(((short)QuestAdegVer.Len.P56_DESC_PROC_PNL));
        // COB_CODE: MOVE LENGTH OF P56-DESC-NOT-PREG
        //                       TO P56-DESC-NOT-PREG-LEN
        questAdegVer.setP56DescNotPregLen(((short)QuestAdegVer.Len.P56_DESC_NOT_PREG));
        // COB_CODE: MOVE LENGTH OF P56-DESC-LIB-MOT-RISC
        //                       TO P56-DESC-LIB-MOT-RISC-LEN
        questAdegVer.setP56DescLibMotRiscLen(((short)QuestAdegVer.Len.P56_DESC_LIB_MOT_RISC));
        // COB_CODE: MOVE LENGTH OF P56-DESC-ORGN-FND
        //                       TO P56-DESC-ORGN-FND-LEN
        questAdegVer.setP56DescOrgnFndLen(((short)QuestAdegVer.Len.P56_DESC_ORGN_FND));
        // COB_CODE: MOVE LENGTH OF P56-DESC-PEP
        //                       TO P56-DESC-PEP-LEN
        questAdegVer.setP56DescPepLen(((short)QuestAdegVer.Len.P56_DESC_PEP));
        // COB_CODE: MOVE LENGTH OF P56-DESC-MOT-CAMBIO-CN
        //                       TO P56-DESC-MOT-CAMBIO-CN-LEN.
        questAdegVer.setP56DescMotCambioCnLen(((short)QuestAdegVer.Len.P56_DESC_MOT_CAMBIO_CN));
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public String getAreaGeoProvFnd() {
        return questAdegVer.getP56AreaGeoProvFnd();
    }

    @Override
    public void setAreaGeoProvFnd(String areaGeoProvFnd) {
        this.questAdegVer.setP56AreaGeoProvFnd(areaGeoProvFnd);
    }

    @Override
    public String getAreaGeoProvFndObj() {
        if (ws.getIndQuestAdegVer().getAreaGeoProvFnd() >= 0) {
            return getAreaGeoProvFnd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAreaGeoProvFndObj(String areaGeoProvFndObj) {
        if (areaGeoProvFndObj != null) {
            setAreaGeoProvFnd(areaGeoProvFndObj);
            ws.getIndQuestAdegVer().setAreaGeoProvFnd(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setAreaGeoProvFnd(((short)-1));
        }
    }

    @Override
    public String getCodAteco() {
        return questAdegVer.getP56CodAteco();
    }

    @Override
    public void setCodAteco(String codAteco) {
        this.questAdegVer.setP56CodAteco(codAteco);
    }

    @Override
    public String getCodAtecoObj() {
        if (ws.getIndQuestAdegVer().getCodAteco() >= 0) {
            return getCodAteco();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodAtecoObj(String codAtecoObj) {
        if (codAtecoObj != null) {
            setCodAteco(codAtecoObj);
            ws.getIndQuestAdegVer().setCodAteco(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setCodAteco(((short)-1));
        }
    }

    @Override
    public String getCodAutDueDil() {
        return questAdegVer.getP56CodAutDueDil();
    }

    @Override
    public void setCodAutDueDil(String codAutDueDil) {
        this.questAdegVer.setP56CodAutDueDil(codAutDueDil);
    }

    @Override
    public String getCodAutDueDilObj() {
        if (ws.getIndQuestAdegVer().getCodAutDueDil() >= 0) {
            return getCodAutDueDil();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodAutDueDilObj(String codAutDueDilObj) {
        if (codAutDueDilObj != null) {
            setCodAutDueDil(codAutDueDilObj);
            ws.getIndQuestAdegVer().setCodAutDueDil(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setCodAutDueDil(((short)-1));
        }
    }

    @Override
    public int getCodCan() {
        return questAdegVer.getP56CodCan().getP56CodCan();
    }

    @Override
    public void setCodCan(int codCan) {
        this.questAdegVer.getP56CodCan().setP56CodCan(codCan);
    }

    @Override
    public Integer getCodCanObj() {
        if (ws.getIndQuestAdegVer().getCodCan() >= 0) {
            return ((Integer)getCodCan());
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodCanObj(Integer codCanObj) {
        if (codCanObj != null) {
            setCodCan(((int)codCanObj));
            ws.getIndQuestAdegVer().setCodCan(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setCodCan(((short)-1));
        }
    }

    @Override
    public int getCodCompAnia() {
        return questAdegVer.getP56CodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.questAdegVer.setP56CodCompAnia(codCompAnia);
    }

    @Override
    public String getCodComunSvolAtt() {
        return questAdegVer.getP56CodComunSvolAtt();
    }

    @Override
    public void setCodComunSvolAtt(String codComunSvolAtt) {
        this.questAdegVer.setP56CodComunSvolAtt(codComunSvolAtt);
    }

    @Override
    public String getCodComunSvolAttObj() {
        if (ws.getIndQuestAdegVer().getCodComunSvolAtt() >= 0) {
            return getCodComunSvolAtt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodComunSvolAttObj(String codComunSvolAttObj) {
        if (codComunSvolAttObj != null) {
            setCodComunSvolAtt(codComunSvolAttObj);
            ws.getIndQuestAdegVer().setCodComunSvolAtt(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setCodComunSvolAtt(((short)-1));
        }
    }

    @Override
    public String getCodImpCarPub() {
        return questAdegVer.getP56CodImpCarPub();
    }

    @Override
    public void setCodImpCarPub(String codImpCarPub) {
        this.questAdegVer.setP56CodImpCarPub(codImpCarPub);
    }

    @Override
    public String getCodImpCarPubObj() {
        if (ws.getIndQuestAdegVer().getCodImpCarPub() >= 0) {
            return getCodImpCarPub();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodImpCarPubObj(String codImpCarPubObj) {
        if (codImpCarPubObj != null) {
            setCodImpCarPub(codImpCarPubObj);
            ws.getIndQuestAdegVer().setCodImpCarPub(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setCodImpCarPub(((short)-1));
        }
    }

    @Override
    public String getCodNazQlfcProf() {
        return questAdegVer.getP56CodNazQlfcProf();
    }

    @Override
    public void setCodNazQlfcProf(String codNazQlfcProf) {
        this.questAdegVer.setP56CodNazQlfcProf(codNazQlfcProf);
    }

    @Override
    public String getCodNazQlfcProfObj() {
        if (ws.getIndQuestAdegVer().getCodNazQlfcProf() >= 0) {
            return getCodNazQlfcProf();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodNazQlfcProfObj(String codNazQlfcProfObj) {
        if (codNazQlfcProfObj != null) {
            setCodNazQlfcProf(codNazQlfcProfObj);
            ws.getIndQuestAdegVer().setCodNazQlfcProf(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setCodNazQlfcProf(((short)-1));
        }
    }

    @Override
    public String getCodPaEspMsc1() {
        return questAdegVer.getP56CodPaEspMsc1();
    }

    @Override
    public void setCodPaEspMsc1(String codPaEspMsc1) {
        this.questAdegVer.setP56CodPaEspMsc1(codPaEspMsc1);
    }

    @Override
    public String getCodPaEspMsc1Obj() {
        if (ws.getIndQuestAdegVer().getCodPaEspMsc1() >= 0) {
            return getCodPaEspMsc1();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodPaEspMsc1Obj(String codPaEspMsc1Obj) {
        if (codPaEspMsc1Obj != null) {
            setCodPaEspMsc1(codPaEspMsc1Obj);
            ws.getIndQuestAdegVer().setCodPaEspMsc1(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setCodPaEspMsc1(((short)-1));
        }
    }

    @Override
    public String getCodPaEspMsc2() {
        return questAdegVer.getP56CodPaEspMsc2();
    }

    @Override
    public void setCodPaEspMsc2(String codPaEspMsc2) {
        this.questAdegVer.setP56CodPaEspMsc2(codPaEspMsc2);
    }

    @Override
    public String getCodPaEspMsc2Obj() {
        if (ws.getIndQuestAdegVer().getCodPaEspMsc2() >= 0) {
            return getCodPaEspMsc2();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodPaEspMsc2Obj(String codPaEspMsc2Obj) {
        if (codPaEspMsc2Obj != null) {
            setCodPaEspMsc2(codPaEspMsc2Obj);
            ws.getIndQuestAdegVer().setCodPaEspMsc2(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setCodPaEspMsc2(((short)-1));
        }
    }

    @Override
    public String getCodPaEspMsc3() {
        return questAdegVer.getP56CodPaEspMsc3();
    }

    @Override
    public void setCodPaEspMsc3(String codPaEspMsc3) {
        this.questAdegVer.setP56CodPaEspMsc3(codPaEspMsc3);
    }

    @Override
    public String getCodPaEspMsc3Obj() {
        if (ws.getIndQuestAdegVer().getCodPaEspMsc3() >= 0) {
            return getCodPaEspMsc3();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodPaEspMsc3Obj(String codPaEspMsc3Obj) {
        if (codPaEspMsc3Obj != null) {
            setCodPaEspMsc3(codPaEspMsc3Obj);
            ws.getIndQuestAdegVer().setCodPaEspMsc3(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setCodPaEspMsc3(((short)-1));
        }
    }

    @Override
    public String getCodPaEspMsc4() {
        return questAdegVer.getP56CodPaEspMsc4();
    }

    @Override
    public void setCodPaEspMsc4(String codPaEspMsc4) {
        this.questAdegVer.setP56CodPaEspMsc4(codPaEspMsc4);
    }

    @Override
    public String getCodPaEspMsc4Obj() {
        if (ws.getIndQuestAdegVer().getCodPaEspMsc4() >= 0) {
            return getCodPaEspMsc4();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodPaEspMsc4Obj(String codPaEspMsc4Obj) {
        if (codPaEspMsc4Obj != null) {
            setCodPaEspMsc4(codPaEspMsc4Obj);
            ws.getIndQuestAdegVer().setCodPaEspMsc4(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setCodPaEspMsc4(((short)-1));
        }
    }

    @Override
    public String getCodPaEspMsc5() {
        return questAdegVer.getP56CodPaEspMsc5();
    }

    @Override
    public void setCodPaEspMsc5(String codPaEspMsc5) {
        this.questAdegVer.setP56CodPaEspMsc5(codPaEspMsc5);
    }

    @Override
    public String getCodPaEspMsc5Obj() {
        if (ws.getIndQuestAdegVer().getCodPaEspMsc5() >= 0) {
            return getCodPaEspMsc5();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodPaEspMsc5Obj(String codPaEspMsc5Obj) {
        if (codPaEspMsc5Obj != null) {
            setCodPaEspMsc5(codPaEspMsc5Obj);
            ws.getIndQuestAdegVer().setCodPaEspMsc5(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setCodPaEspMsc5(((short)-1));
        }
    }

    @Override
    public String getCodProfCon() {
        return questAdegVer.getP56CodProfCon();
    }

    @Override
    public void setCodProfCon(String codProfCon) {
        this.questAdegVer.setP56CodProfCon(codProfCon);
    }

    @Override
    public String getCodProfConObj() {
        if (ws.getIndQuestAdegVer().getCodProfCon() >= 0) {
            return getCodProfCon();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodProfConObj(String codProfConObj) {
        if (codProfConObj != null) {
            setCodProfCon(codProfConObj);
            ws.getIndQuestAdegVer().setCodProfCon(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setCodProfCon(((short)-1));
        }
    }

    @Override
    public String getCodProfPrec() {
        return questAdegVer.getP56CodProfPrec();
    }

    @Override
    public void setCodProfPrec(String codProfPrec) {
        this.questAdegVer.setP56CodProfPrec(codProfPrec);
    }

    @Override
    public String getCodProfPrecObj() {
        if (ws.getIndQuestAdegVer().getCodProfPrec() >= 0) {
            return getCodProfPrec();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodProfPrecObj(String codProfPrecObj) {
        if (codProfPrecObj != null) {
            setCodProfPrec(codProfPrecObj);
            ws.getIndQuestAdegVer().setCodProfPrec(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setCodProfPrec(((short)-1));
        }
    }

    @Override
    public String getCodPrvQlfcProf() {
        return questAdegVer.getP56CodPrvQlfcProf();
    }

    @Override
    public void setCodPrvQlfcProf(String codPrvQlfcProf) {
        this.questAdegVer.setP56CodPrvQlfcProf(codPrvQlfcProf);
    }

    @Override
    public String getCodPrvQlfcProfObj() {
        if (ws.getIndQuestAdegVer().getCodPrvQlfcProf() >= 0) {
            return getCodPrvQlfcProf();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodPrvQlfcProfObj(String codPrvQlfcProfObj) {
        if (codPrvQlfcProfObj != null) {
            setCodPrvQlfcProf(codPrvQlfcProfObj);
            ws.getIndQuestAdegVer().setCodPrvQlfcProf(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setCodPrvQlfcProf(((short)-1));
        }
    }

    @Override
    public String getCodPrvSvolAtt() {
        return questAdegVer.getP56CodPrvSvolAtt();
    }

    @Override
    public void setCodPrvSvolAtt(String codPrvSvolAtt) {
        this.questAdegVer.setP56CodPrvSvolAtt(codPrvSvolAtt);
    }

    @Override
    public String getCodPrvSvolAttObj() {
        if (ws.getIndQuestAdegVer().getCodPrvSvolAtt() >= 0) {
            return getCodPrvSvolAtt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodPrvSvolAttObj(String codPrvSvolAttObj) {
        if (codPrvSvolAttObj != null) {
            setCodPrvSvolAtt(codPrvSvolAttObj);
            ws.getIndQuestAdegVer().setCodPrvSvolAtt(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setCodPrvSvolAtt(((short)-1));
        }
    }

    @Override
    public String getCodQlfcProf() {
        return questAdegVer.getP56CodQlfcProf();
    }

    @Override
    public void setCodQlfcProf(String codQlfcProf) {
        this.questAdegVer.setP56CodQlfcProf(codQlfcProf);
    }

    @Override
    public String getCodQlfcProfObj() {
        if (ws.getIndQuestAdegVer().getCodQlfcProf() >= 0) {
            return getCodQlfcProf();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodQlfcProfObj(String codQlfcProfObj) {
        if (codQlfcProfObj != null) {
            setCodQlfcProf(codQlfcProfObj);
            ws.getIndQuestAdegVer().setCodQlfcProf(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setCodQlfcProf(((short)-1));
        }
    }

    @Override
    public String getCodSae() {
        return questAdegVer.getP56CodSae();
    }

    @Override
    public void setCodSae(String codSae) {
        this.questAdegVer.setP56CodSae(codSae);
    }

    @Override
    public String getCodSaeObj() {
        if (ws.getIndQuestAdegVer().getCodSae() >= 0) {
            return getCodSae();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodSaeObj(String codSaeObj) {
        if (codSaeObj != null) {
            setCodSae(codSaeObj);
            ws.getIndQuestAdegVer().setCodSae(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setCodSae(((short)-1));
        }
    }

    @Override
    public String getCodSogg() {
        return questAdegVer.getP56CodSogg();
    }

    @Override
    public void setCodSogg(String codSogg) {
        this.questAdegVer.setP56CodSogg(codSogg);
    }

    @Override
    public String getCodSoggObj() {
        if (ws.getIndQuestAdegVer().getCodSogg() >= 0) {
            return getCodSogg();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodSoggObj(String codSoggObj) {
        if (codSoggObj != null) {
            setCodSogg(codSoggObj);
            ws.getIndQuestAdegVer().setCodSogg(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setCodSogg(((short)-1));
        }
    }

    @Override
    public String getCodStatSvolAtt() {
        return questAdegVer.getP56CodStatSvolAtt();
    }

    @Override
    public void setCodStatSvolAtt(String codStatSvolAtt) {
        this.questAdegVer.setP56CodStatSvolAtt(codStatSvolAtt);
    }

    @Override
    public String getCodStatSvolAttObj() {
        if (ws.getIndQuestAdegVer().getCodStatSvolAtt() >= 0) {
            return getCodStatSvolAtt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodStatSvolAttObj(String codStatSvolAttObj) {
        if (codStatSvolAttObj != null) {
            setCodStatSvolAtt(codStatSvolAttObj);
            ws.getIndQuestAdegVer().setCodStatSvolAtt(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setCodStatSvolAtt(((short)-1));
        }
    }

    @Override
    public String getDescImpCarPubVchar() {
        return questAdegVer.getP56DescImpCarPubVcharFormatted();
    }

    @Override
    public void setDescImpCarPubVchar(String descImpCarPubVchar) {
        this.questAdegVer.setP56DescImpCarPubVcharFormatted(descImpCarPubVchar);
    }

    @Override
    public String getDescImpCarPubVcharObj() {
        if (ws.getIndQuestAdegVer().getDescImpCarPub() >= 0) {
            return getDescImpCarPubVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDescImpCarPubVcharObj(String descImpCarPubVcharObj) {
        if (descImpCarPubVcharObj != null) {
            setDescImpCarPubVchar(descImpCarPubVcharObj);
            ws.getIndQuestAdegVer().setDescImpCarPub(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setDescImpCarPub(((short)-1));
        }
    }

    @Override
    public String getDescLibMotRiscVchar() {
        return questAdegVer.getP56DescLibMotRiscVcharFormatted();
    }

    @Override
    public void setDescLibMotRiscVchar(String descLibMotRiscVchar) {
        this.questAdegVer.setP56DescLibMotRiscVcharFormatted(descLibMotRiscVchar);
    }

    @Override
    public String getDescLibMotRiscVcharObj() {
        if (ws.getIndQuestAdegVer().getDescLibMotRisc() >= 0) {
            return getDescLibMotRiscVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDescLibMotRiscVcharObj(String descLibMotRiscVcharObj) {
        if (descLibMotRiscVcharObj != null) {
            setDescLibMotRiscVchar(descLibMotRiscVcharObj);
            ws.getIndQuestAdegVer().setDescLibMotRisc(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setDescLibMotRisc(((short)-1));
        }
    }

    @Override
    public String getDescMotCambioCnVchar() {
        return questAdegVer.getP56DescMotCambioCnVcharFormatted();
    }

    @Override
    public void setDescMotCambioCnVchar(String descMotCambioCnVchar) {
        this.questAdegVer.setP56DescMotCambioCnVcharFormatted(descMotCambioCnVchar);
    }

    @Override
    public String getDescMotCambioCnVcharObj() {
        if (ws.getIndQuestAdegVer().getDescMotCambioCn() >= 0) {
            return getDescMotCambioCnVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDescMotCambioCnVcharObj(String descMotCambioCnVcharObj) {
        if (descMotCambioCnVcharObj != null) {
            setDescMotCambioCnVchar(descMotCambioCnVcharObj);
            ws.getIndQuestAdegVer().setDescMotCambioCn(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setDescMotCambioCn(((short)-1));
        }
    }

    @Override
    public String getDescNotPregVchar() {
        return questAdegVer.getP56DescNotPregVcharFormatted();
    }

    @Override
    public void setDescNotPregVchar(String descNotPregVchar) {
        this.questAdegVer.setP56DescNotPregVcharFormatted(descNotPregVchar);
    }

    @Override
    public String getDescNotPregVcharObj() {
        if (ws.getIndQuestAdegVer().getDescNotPreg() >= 0) {
            return getDescNotPregVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDescNotPregVcharObj(String descNotPregVcharObj) {
        if (descNotPregVcharObj != null) {
            setDescNotPregVchar(descNotPregVcharObj);
            ws.getIndQuestAdegVer().setDescNotPreg(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setDescNotPreg(((short)-1));
        }
    }

    @Override
    public String getDescOrgnFndVchar() {
        return questAdegVer.getP56DescOrgnFndVcharFormatted();
    }

    @Override
    public void setDescOrgnFndVchar(String descOrgnFndVchar) {
        this.questAdegVer.setP56DescOrgnFndVcharFormatted(descOrgnFndVchar);
    }

    @Override
    public String getDescOrgnFndVcharObj() {
        if (ws.getIndQuestAdegVer().getDescOrgnFnd() >= 0) {
            return getDescOrgnFndVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDescOrgnFndVcharObj(String descOrgnFndVcharObj) {
        if (descOrgnFndVcharObj != null) {
            setDescOrgnFndVchar(descOrgnFndVcharObj);
            ws.getIndQuestAdegVer().setDescOrgnFnd(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setDescOrgnFnd(((short)-1));
        }
    }

    @Override
    public String getDescPepVchar() {
        return questAdegVer.getP56DescPepVcharFormatted();
    }

    @Override
    public void setDescPepVchar(String descPepVchar) {
        this.questAdegVer.setP56DescPepVcharFormatted(descPepVchar);
    }

    @Override
    public String getDescPepVcharObj() {
        if (ws.getIndQuestAdegVer().getDescPep() >= 0) {
            return getDescPepVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDescPepVcharObj(String descPepVcharObj) {
        if (descPepVcharObj != null) {
            setDescPepVchar(descPepVcharObj);
            ws.getIndQuestAdegVer().setDescPep(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setDescPep(((short)-1));
        }
    }

    @Override
    public String getDescProcPnlVchar() {
        return questAdegVer.getP56DescProcPnlVcharFormatted();
    }

    @Override
    public void setDescProcPnlVchar(String descProcPnlVchar) {
        this.questAdegVer.setP56DescProcPnlVcharFormatted(descProcPnlVchar);
    }

    @Override
    public String getDescProcPnlVcharObj() {
        if (ws.getIndQuestAdegVer().getDescProcPnl() >= 0) {
            return getDescProcPnlVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDescProcPnlVcharObj(String descProcPnlVcharObj) {
        if (descProcPnlVcharObj != null) {
            setDescProcPnlVchar(descProcPnlVcharObj);
            ws.getIndQuestAdegVer().setDescProcPnl(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setDescProcPnl(((short)-1));
        }
    }

    @Override
    public String getDescScoFinRappVchar() {
        return questAdegVer.getP56DescScoFinRappVcharFormatted();
    }

    @Override
    public void setDescScoFinRappVchar(String descScoFinRappVchar) {
        this.questAdegVer.setP56DescScoFinRappVcharFormatted(descScoFinRappVchar);
    }

    @Override
    public String getDescScoFinRappVcharObj() {
        if (ws.getIndQuestAdegVer().getDescScoFinRapp() >= 0) {
            return getDescScoFinRappVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDescScoFinRappVcharObj(String descScoFinRappVcharObj) {
        if (descScoFinRappVcharObj != null) {
            setDescScoFinRappVchar(descScoFinRappVcharObj);
            ws.getIndQuestAdegVer().setDescScoFinRapp(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setDescScoFinRapp(((short)-1));
        }
    }

    @Override
    public char getDsOperSql() {
        return questAdegVer.getP56DsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.questAdegVer.setP56DsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return questAdegVer.getP56DsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.questAdegVer.setP56DsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsCptz() {
        return questAdegVer.getP56DsTsCptz();
    }

    @Override
    public void setDsTsCptz(long dsTsCptz) {
        this.questAdegVer.setP56DsTsCptz(dsTsCptz);
    }

    @Override
    public String getDsUtente() {
        return questAdegVer.getP56DsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.questAdegVer.setP56DsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return questAdegVer.getP56DsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.questAdegVer.setP56DsVer(dsVer);
    }

    @Override
    public String getDtIniFntRedd2Db() {
        return ws.getQuestAdegVerDb().getRedd2Db();
    }

    @Override
    public void setDtIniFntRedd2Db(String dtIniFntRedd2Db) {
        this.ws.getQuestAdegVerDb().setRedd2Db(dtIniFntRedd2Db);
    }

    @Override
    public String getDtIniFntRedd2DbObj() {
        if (ws.getIndQuestAdegVer().getDtIniFntRedd2() >= 0) {
            return getDtIniFntRedd2Db();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtIniFntRedd2DbObj(String dtIniFntRedd2DbObj) {
        if (dtIniFntRedd2DbObj != null) {
            setDtIniFntRedd2Db(dtIniFntRedd2DbObj);
            ws.getIndQuestAdegVer().setDtIniFntRedd2(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setDtIniFntRedd2(((short)-1));
        }
    }

    @Override
    public String getDtIniFntRedd3Db() {
        return ws.getQuestAdegVerDb().getRedd3Db();
    }

    @Override
    public void setDtIniFntRedd3Db(String dtIniFntRedd3Db) {
        this.ws.getQuestAdegVerDb().setRedd3Db(dtIniFntRedd3Db);
    }

    @Override
    public String getDtIniFntRedd3DbObj() {
        if (ws.getIndQuestAdegVer().getDtIniFntRedd3() >= 0) {
            return getDtIniFntRedd3Db();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtIniFntRedd3DbObj(String dtIniFntRedd3DbObj) {
        if (dtIniFntRedd3DbObj != null) {
            setDtIniFntRedd3Db(dtIniFntRedd3DbObj);
            ws.getIndQuestAdegVer().setDtIniFntRedd3(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setDtIniFntRedd3(((short)-1));
        }
    }

    @Override
    public String getDtIniFntReddDb() {
        return ws.getQuestAdegVerDb().getDb();
    }

    @Override
    public void setDtIniFntReddDb(String dtIniFntReddDb) {
        this.ws.getQuestAdegVerDb().setDb(dtIniFntReddDb);
    }

    @Override
    public String getDtIniFntReddDbObj() {
        if (ws.getIndQuestAdegVer().getDtIniFntRedd() >= 0) {
            return getDtIniFntReddDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtIniFntReddDbObj(String dtIniFntReddDbObj) {
        if (dtIniFntReddDbObj != null) {
            setDtIniFntReddDb(dtIniFntReddDbObj);
            ws.getIndQuestAdegVer().setDtIniFntRedd(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setDtIniFntRedd(((short)-1));
        }
    }

    @Override
    public String getFinCostituzioneVchar() {
        return questAdegVer.getP56FinCostituzioneVcharFormatted();
    }

    @Override
    public void setFinCostituzioneVchar(String finCostituzioneVchar) {
        this.questAdegVer.setP56FinCostituzioneVcharFormatted(finCostituzioneVchar);
    }

    @Override
    public String getFinCostituzioneVcharObj() {
        if (ws.getIndQuestAdegVer().getFinCostituzione() >= 0) {
            return getFinCostituzioneVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setFinCostituzioneVcharObj(String finCostituzioneVcharObj) {
        if (finCostituzioneVcharObj != null) {
            setFinCostituzioneVchar(finCostituzioneVcharObj);
            ws.getIndQuestAdegVer().setFinCostituzione(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setFinCostituzione(((short)-1));
        }
    }

    @Override
    public char getFlAuFatcaAeoi() {
        return questAdegVer.getP56FlAuFatcaAeoi();
    }

    @Override
    public void setFlAuFatcaAeoi(char flAuFatcaAeoi) {
        this.questAdegVer.setP56FlAuFatcaAeoi(flAuFatcaAeoi);
    }

    @Override
    public Character getFlAuFatcaAeoiObj() {
        if (ws.getIndQuestAdegVer().getFlAuFatcaAeoi() >= 0) {
            return ((Character)getFlAuFatcaAeoi());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlAuFatcaAeoiObj(Character flAuFatcaAeoiObj) {
        if (flAuFatcaAeoiObj != null) {
            setFlAuFatcaAeoi(((char)flAuFatcaAeoiObj));
            ws.getIndQuestAdegVer().setFlAuFatcaAeoi(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setFlAuFatcaAeoi(((short)-1));
        }
    }

    @Override
    public char getFlAutPep() {
        return questAdegVer.getP56FlAutPep();
    }

    @Override
    public void setFlAutPep(char flAutPep) {
        this.questAdegVer.setP56FlAutPep(flAutPep);
    }

    @Override
    public Character getFlAutPepObj() {
        if (ws.getIndQuestAdegVer().getFlAutPep() >= 0) {
            return ((Character)getFlAutPep());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlAutPepObj(Character flAutPepObj) {
        if (flAutPepObj != null) {
            setFlAutPep(((char)flAutPepObj));
            ws.getIndQuestAdegVer().setFlAutPep(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setFlAutPep(((short)-1));
        }
    }

    @Override
    public char getFlEsProcPen() {
        return questAdegVer.getP56FlEsProcPen();
    }

    @Override
    public void setFlEsProcPen(char flEsProcPen) {
        this.questAdegVer.setP56FlEsProcPen(flEsProcPen);
    }

    @Override
    public Character getFlEsProcPenObj() {
        if (ws.getIndQuestAdegVer().getFlEsProcPen() >= 0) {
            return ((Character)getFlEsProcPen());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlEsProcPenObj(Character flEsProcPenObj) {
        if (flEsProcPenObj != null) {
            setFlEsProcPen(((char)flEsProcPenObj));
            ws.getIndQuestAdegVer().setFlEsProcPen(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setFlEsProcPen(((short)-1));
        }
    }

    @Override
    public char getFlImpCarPub() {
        return questAdegVer.getP56FlImpCarPub();
    }

    @Override
    public void setFlImpCarPub(char flImpCarPub) {
        this.questAdegVer.setP56FlImpCarPub(flImpCarPub);
    }

    @Override
    public Character getFlImpCarPubObj() {
        if (ws.getIndQuestAdegVer().getFlImpCarPub() >= 0) {
            return ((Character)getFlImpCarPub());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlImpCarPubObj(Character flImpCarPubObj) {
        if (flImpCarPubObj != null) {
            setFlImpCarPub(((char)flImpCarPubObj));
            ws.getIndQuestAdegVer().setFlImpCarPub(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setFlImpCarPub(((short)-1));
        }
    }

    @Override
    public char getFlIndSocQuot() {
        return questAdegVer.getP56FlIndSocQuot();
    }

    @Override
    public void setFlIndSocQuot(char flIndSocQuot) {
        this.questAdegVer.setP56FlIndSocQuot(flIndSocQuot);
    }

    @Override
    public Character getFlIndSocQuotObj() {
        if (ws.getIndQuestAdegVer().getFlIndSocQuot() >= 0) {
            return ((Character)getFlIndSocQuot());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlIndSocQuotObj(Character flIndSocQuotObj) {
        if (flIndSocQuotObj != null) {
            setFlIndSocQuot(((char)flIndSocQuotObj));
            ws.getIndQuestAdegVer().setFlIndSocQuot(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setFlIndSocQuot(((short)-1));
        }
    }

    @Override
    public char getFlIstituzFin() {
        return questAdegVer.getP56FlIstituzFin();
    }

    @Override
    public void setFlIstituzFin(char flIstituzFin) {
        this.questAdegVer.setP56FlIstituzFin(flIstituzFin);
    }

    @Override
    public Character getFlIstituzFinObj() {
        if (ws.getIndQuestAdegVer().getFlIstituzFin() >= 0) {
            return ((Character)getFlIstituzFin());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlIstituzFinObj(Character flIstituzFinObj) {
        if (flIstituzFinObj != null) {
            setFlIstituzFin(((char)flIstituzFinObj));
            ws.getIndQuestAdegVer().setFlIstituzFin(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setFlIstituzFin(((short)-1));
        }
    }

    @Override
    public char getFlLisTerrSorv() {
        return questAdegVer.getP56FlLisTerrSorv();
    }

    @Override
    public void setFlLisTerrSorv(char flLisTerrSorv) {
        this.questAdegVer.setP56FlLisTerrSorv(flLisTerrSorv);
    }

    @Override
    public Character getFlLisTerrSorvObj() {
        if (ws.getIndQuestAdegVer().getFlLisTerrSorv() >= 0) {
            return ((Character)getFlLisTerrSorv());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlLisTerrSorvObj(Character flLisTerrSorvObj) {
        if (flLisTerrSorvObj != null) {
            setFlLisTerrSorv(((char)flLisTerrSorvObj));
            ws.getIndQuestAdegVer().setFlLisTerrSorv(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setFlLisTerrSorv(((short)-1));
        }
    }

    @Override
    public char getFlNewPro() {
        return questAdegVer.getP56FlNewPro();
    }

    @Override
    public void setFlNewPro(char flNewPro) {
        this.questAdegVer.setP56FlNewPro(flNewPro);
    }

    @Override
    public Character getFlNewProObj() {
        if (ws.getIndQuestAdegVer().getFlNewPro() >= 0) {
            return ((Character)getFlNewPro());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlNewProObj(Character flNewProObj) {
        if (flNewProObj != null) {
            setFlNewPro(((char)flNewProObj));
            ws.getIndQuestAdegVer().setFlNewPro(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setFlNewPro(((short)-1));
        }
    }

    @Override
    public char getFlNotPreg() {
        return questAdegVer.getP56FlNotPreg();
    }

    @Override
    public void setFlNotPreg(char flNotPreg) {
        this.questAdegVer.setP56FlNotPreg(flNotPreg);
    }

    @Override
    public Character getFlNotPregObj() {
        if (ws.getIndQuestAdegVer().getFlNotPreg() >= 0) {
            return ((Character)getFlNotPreg());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlNotPregObj(Character flNotPregObj) {
        if (flNotPregObj != null) {
            setFlNotPreg(((char)flNotPregObj));
            ws.getIndQuestAdegVer().setFlNotPreg(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setFlNotPreg(((short)-1));
        }
    }

    @Override
    public char getFlPaeseCitAut() {
        return questAdegVer.getP56FlPaeseCitAut();
    }

    @Override
    public void setFlPaeseCitAut(char flPaeseCitAut) {
        this.questAdegVer.setP56FlPaeseCitAut(flPaeseCitAut);
    }

    @Override
    public Character getFlPaeseCitAutObj() {
        if (ws.getIndQuestAdegVer().getFlPaeseCitAut() >= 0) {
            return ((Character)getFlPaeseCitAut());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlPaeseCitAutObj(Character flPaeseCitAutObj) {
        if (flPaeseCitAutObj != null) {
            setFlPaeseCitAut(((char)flPaeseCitAutObj));
            ws.getIndQuestAdegVer().setFlPaeseCitAut(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setFlPaeseCitAut(((short)-1));
        }
    }

    @Override
    public char getFlPaeseNazAut() {
        return questAdegVer.getP56FlPaeseNazAut();
    }

    @Override
    public void setFlPaeseNazAut(char flPaeseNazAut) {
        this.questAdegVer.setP56FlPaeseNazAut(flPaeseNazAut);
    }

    @Override
    public Character getFlPaeseNazAutObj() {
        if (ws.getIndQuestAdegVer().getFlPaeseNazAut() >= 0) {
            return ((Character)getFlPaeseNazAut());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlPaeseNazAutObj(Character flPaeseNazAutObj) {
        if (flPaeseNazAutObj != null) {
            setFlPaeseNazAut(((char)flPaeseNazAutObj));
            ws.getIndQuestAdegVer().setFlPaeseNazAut(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setFlPaeseNazAut(((short)-1));
        }
    }

    @Override
    public char getFlPaeseResidAut() {
        return questAdegVer.getP56FlPaeseResidAut();
    }

    @Override
    public void setFlPaeseResidAut(char flPaeseResidAut) {
        this.questAdegVer.setP56FlPaeseResidAut(flPaeseResidAut);
    }

    @Override
    public Character getFlPaeseResidAutObj() {
        if (ws.getIndQuestAdegVer().getFlPaeseResidAut() >= 0) {
            return ((Character)getFlPaeseResidAut());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlPaeseResidAutObj(Character flPaeseResidAutObj) {
        if (flPaeseResidAutObj != null) {
            setFlPaeseResidAut(((char)flPaeseResidAutObj));
            ws.getIndQuestAdegVer().setFlPaeseResidAut(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setFlPaeseResidAut(((short)-1));
        }
    }

    @Override
    public char getFlPrQuestAeoi() {
        return questAdegVer.getP56FlPrQuestAeoi();
    }

    @Override
    public void setFlPrQuestAeoi(char flPrQuestAeoi) {
        this.questAdegVer.setP56FlPrQuestAeoi(flPrQuestAeoi);
    }

    @Override
    public Character getFlPrQuestAeoiObj() {
        if (ws.getIndQuestAdegVer().getFlPrQuestAeoi() >= 0) {
            return ((Character)getFlPrQuestAeoi());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlPrQuestAeoiObj(Character flPrQuestAeoiObj) {
        if (flPrQuestAeoiObj != null) {
            setFlPrQuestAeoi(((char)flPrQuestAeoiObj));
            ws.getIndQuestAdegVer().setFlPrQuestAeoi(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setFlPrQuestAeoi(((short)-1));
        }
    }

    @Override
    public char getFlPrQuestFatca() {
        return questAdegVer.getP56FlPrQuestFatca();
    }

    @Override
    public void setFlPrQuestFatca(char flPrQuestFatca) {
        this.questAdegVer.setP56FlPrQuestFatca(flPrQuestFatca);
    }

    @Override
    public Character getFlPrQuestFatcaObj() {
        if (ws.getIndQuestAdegVer().getFlPrQuestFatca() >= 0) {
            return ((Character)getFlPrQuestFatca());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlPrQuestFatcaObj(Character flPrQuestFatcaObj) {
        if (flPrQuestFatcaObj != null) {
            setFlPrQuestFatca(((char)flPrQuestFatcaObj));
            ws.getIndQuestAdegVer().setFlPrQuestFatca(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setFlPrQuestFatca(((short)-1));
        }
    }

    @Override
    public char getFlPrQuestKyc() {
        return questAdegVer.getP56FlPrQuestKyc();
    }

    @Override
    public void setFlPrQuestKyc(char flPrQuestKyc) {
        this.questAdegVer.setP56FlPrQuestKyc(flPrQuestKyc);
    }

    @Override
    public Character getFlPrQuestKycObj() {
        if (ws.getIndQuestAdegVer().getFlPrQuestKyc() >= 0) {
            return ((Character)getFlPrQuestKyc());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlPrQuestKycObj(Character flPrQuestKycObj) {
        if (flPrQuestKycObj != null) {
            setFlPrQuestKyc(((char)flPrQuestKycObj));
            ws.getIndQuestAdegVer().setFlPrQuestKyc(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setFlPrQuestKyc(((short)-1));
        }
    }

    @Override
    public char getFlPrQuestMscq() {
        return questAdegVer.getP56FlPrQuestMscq();
    }

    @Override
    public void setFlPrQuestMscq(char flPrQuestMscq) {
        this.questAdegVer.setP56FlPrQuestMscq(flPrQuestMscq);
    }

    @Override
    public Character getFlPrQuestMscqObj() {
        if (ws.getIndQuestAdegVer().getFlPrQuestMscq() >= 0) {
            return ((Character)getFlPrQuestMscq());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlPrQuestMscqObj(Character flPrQuestMscqObj) {
        if (flPrQuestMscqObj != null) {
            setFlPrQuestMscq(((char)flPrQuestMscqObj));
            ws.getIndQuestAdegVer().setFlPrQuestMscq(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setFlPrQuestMscq(((short)-1));
        }
    }

    @Override
    public char getFlPrQuestOfac() {
        return questAdegVer.getP56FlPrQuestOfac();
    }

    @Override
    public void setFlPrQuestOfac(char flPrQuestOfac) {
        this.questAdegVer.setP56FlPrQuestOfac(flPrQuestOfac);
    }

    @Override
    public Character getFlPrQuestOfacObj() {
        if (ws.getIndQuestAdegVer().getFlPrQuestOfac() >= 0) {
            return ((Character)getFlPrQuestOfac());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlPrQuestOfacObj(Character flPrQuestOfacObj) {
        if (flPrQuestOfacObj != null) {
            setFlPrQuestOfac(((char)flPrQuestOfacObj));
            ws.getIndQuestAdegVer().setFlPrQuestOfac(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setFlPrQuestOfac(((short)-1));
        }
    }

    @Override
    public char getFlPrTrNoUsa() {
        return questAdegVer.getP56FlPrTrNoUsa();
    }

    @Override
    public void setFlPrTrNoUsa(char flPrTrNoUsa) {
        this.questAdegVer.setP56FlPrTrNoUsa(flPrTrNoUsa);
    }

    @Override
    public Character getFlPrTrNoUsaObj() {
        if (ws.getIndQuestAdegVer().getFlPrTrNoUsa() >= 0) {
            return ((Character)getFlPrTrNoUsa());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlPrTrNoUsaObj(Character flPrTrNoUsaObj) {
        if (flPrTrNoUsaObj != null) {
            setFlPrTrNoUsa(((char)flPrTrNoUsaObj));
            ws.getIndQuestAdegVer().setFlPrTrNoUsa(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setFlPrTrNoUsa(((short)-1));
        }
    }

    @Override
    public char getFlPrTrUsa() {
        return questAdegVer.getP56FlPrTrUsa();
    }

    @Override
    public void setFlPrTrUsa(char flPrTrUsa) {
        this.questAdegVer.setP56FlPrTrUsa(flPrTrUsa);
    }

    @Override
    public Character getFlPrTrUsaObj() {
        if (ws.getIndQuestAdegVer().getFlPrTrUsa() >= 0) {
            return ((Character)getFlPrTrUsa());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlPrTrUsaObj(Character flPrTrUsaObj) {
        if (flPrTrUsaObj != null) {
            setFlPrTrUsa(((char)flPrTrUsaObj));
            ws.getIndQuestAdegVer().setFlPrTrUsa(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setFlPrTrUsa(((short)-1));
        }
    }

    @Override
    public char getFlPrsz3oPagat() {
        return questAdegVer.getP56FlPrsz3oPagat();
    }

    @Override
    public void setFlPrsz3oPagat(char flPrsz3oPagat) {
        this.questAdegVer.setP56FlPrsz3oPagat(flPrsz3oPagat);
    }

    @Override
    public Character getFlPrsz3oPagatObj() {
        if (ws.getIndQuestAdegVer().getFlPrsz3oPagat() >= 0) {
            return ((Character)getFlPrsz3oPagat());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlPrsz3oPagatObj(Character flPrsz3oPagatObj) {
        if (flPrsz3oPagatObj != null) {
            setFlPrsz3oPagat(((char)flPrsz3oPagatObj));
            ws.getIndQuestAdegVer().setFlPrsz3oPagat(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setFlPrsz3oPagat(((short)-1));
        }
    }

    @Override
    public char getFlPrszTitEff() {
        return questAdegVer.getP56FlPrszTitEff();
    }

    @Override
    public void setFlPrszTitEff(char flPrszTitEff) {
        this.questAdegVer.setP56FlPrszTitEff(flPrszTitEff);
    }

    @Override
    public Character getFlPrszTitEffObj() {
        if (ws.getIndQuestAdegVer().getFlPrszTitEff() >= 0) {
            return ((Character)getFlPrszTitEff());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlPrszTitEffObj(Character flPrszTitEffObj) {
        if (flPrszTitEffObj != null) {
            setFlPrszTitEff(((char)flPrszTitEffObj));
            ws.getIndQuestAdegVer().setFlPrszTitEff(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setFlPrszTitEff(((short)-1));
        }
    }

    @Override
    public char getFlRagRapp() {
        return questAdegVer.getP56FlRagRapp();
    }

    @Override
    public void setFlRagRapp(char flRagRapp) {
        this.questAdegVer.setP56FlRagRapp(flRagRapp);
    }

    @Override
    public Character getFlRagRappObj() {
        if (ws.getIndQuestAdegVer().getFlRagRapp() >= 0) {
            return ((Character)getFlRagRapp());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlRagRappObj(Character flRagRappObj) {
        if (flRagRappObj != null) {
            setFlRagRapp(((char)flRagRappObj));
            ws.getIndQuestAdegVer().setFlRagRapp(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setFlRagRapp(((short)-1));
        }
    }

    @Override
    public char getFlRappPaMsc() {
        return questAdegVer.getP56FlRappPaMsc();
    }

    @Override
    public void setFlRappPaMsc(char flRappPaMsc) {
        this.questAdegVer.setP56FlRappPaMsc(flRappPaMsc);
    }

    @Override
    public Character getFlRappPaMscObj() {
        if (ws.getIndQuestAdegVer().getFlRappPaMsc() >= 0) {
            return ((Character)getFlRappPaMsc());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlRappPaMscObj(Character flRappPaMscObj) {
        if (flRappPaMscObj != null) {
            setFlRappPaMsc(((char)flRappPaMscObj));
            ws.getIndQuestAdegVer().setFlRappPaMsc(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setFlRappPaMsc(((short)-1));
        }
    }

    @Override
    public String getFntRedd2() {
        return questAdegVer.getP56FntRedd2();
    }

    @Override
    public void setFntRedd2(String fntRedd2) {
        this.questAdegVer.setP56FntRedd2(fntRedd2);
    }

    @Override
    public String getFntRedd2Obj() {
        if (ws.getIndQuestAdegVer().getFntRedd2() >= 0) {
            return getFntRedd2();
        }
        else {
            return null;
        }
    }

    @Override
    public void setFntRedd2Obj(String fntRedd2Obj) {
        if (fntRedd2Obj != null) {
            setFntRedd2(fntRedd2Obj);
            ws.getIndQuestAdegVer().setFntRedd2(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setFntRedd2(((short)-1));
        }
    }

    @Override
    public String getFntRedd3() {
        return questAdegVer.getP56FntRedd3();
    }

    @Override
    public void setFntRedd3(String fntRedd3) {
        this.questAdegVer.setP56FntRedd3(fntRedd3);
    }

    @Override
    public String getFntRedd3Obj() {
        if (ws.getIndQuestAdegVer().getFntRedd3() >= 0) {
            return getFntRedd3();
        }
        else {
            return null;
        }
    }

    @Override
    public void setFntRedd3Obj(String fntRedd3Obj) {
        if (fntRedd3Obj != null) {
            setFntRedd3(fntRedd3Obj);
            ws.getIndQuestAdegVer().setFntRedd3(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setFntRedd3(((short)-1));
        }
    }

    @Override
    public String getFntRedd() {
        return questAdegVer.getP56FntRedd();
    }

    @Override
    public void setFntRedd(String fntRedd) {
        this.questAdegVer.setP56FntRedd(fntRedd);
    }

    @Override
    public String getFntReddObj() {
        if (ws.getIndQuestAdegVer().getFntRedd() >= 0) {
            return getFntRedd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setFntReddObj(String fntReddObj) {
        if (fntReddObj != null) {
            setFntRedd(fntReddObj);
            ws.getIndQuestAdegVer().setFntRedd(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setFntRedd(((short)-1));
        }
    }

    @Override
    public int getIdAssicurati() {
        return questAdegVer.getP56IdAssicurati().getP56IdAssicurati();
    }

    @Override
    public void setIdAssicurati(int idAssicurati) {
        this.questAdegVer.getP56IdAssicurati().setP56IdAssicurati(idAssicurati);
    }

    @Override
    public Integer getIdAssicuratiObj() {
        if (ws.getIndQuestAdegVer().getIdAssicurati() >= 0) {
            return ((Integer)getIdAssicurati());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdAssicuratiObj(Integer idAssicuratiObj) {
        if (idAssicuratiObj != null) {
            setIdAssicurati(((int)idAssicuratiObj));
            ws.getIndQuestAdegVer().setIdAssicurati(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setIdAssicurati(((short)-1));
        }
    }

    @Override
    public int getIdMoviCrz() {
        return questAdegVer.getP56IdMoviCrz();
    }

    @Override
    public void setIdMoviCrz(int idMoviCrz) {
        this.questAdegVer.setP56IdMoviCrz(idMoviCrz);
    }

    @Override
    public int getIdPoli() {
        return questAdegVer.getP56IdPoli();
    }

    @Override
    public void setIdPoli(int idPoli) {
        this.questAdegVer.setP56IdPoli(idPoli);
    }

    @Override
    public int getIdRappAna() {
        return questAdegVer.getP56IdRappAna().getP56IdRappAna();
    }

    @Override
    public void setIdRappAna(int idRappAna) {
        this.questAdegVer.getP56IdRappAna().setP56IdRappAna(idRappAna);
    }

    @Override
    public Integer getIdRappAnaObj() {
        if (ws.getIndQuestAdegVer().getIdRappAna() >= 0) {
            return ((Integer)getIdRappAna());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdRappAnaObj(Integer idRappAnaObj) {
        if (idRappAnaObj != null) {
            setIdRappAna(((int)idRappAnaObj));
            ws.getIndQuestAdegVer().setIdRappAna(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setIdRappAna(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpAfi() {
        return questAdegVer.getP56ImpAfi().getP56ImpAfi();
    }

    @Override
    public void setImpAfi(AfDecimal impAfi) {
        this.questAdegVer.getP56ImpAfi().setP56ImpAfi(impAfi.copy());
    }

    @Override
    public AfDecimal getImpAfiObj() {
        if (ws.getIndQuestAdegVer().getImpAfi() >= 0) {
            return getImpAfi();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpAfiObj(AfDecimal impAfiObj) {
        if (impAfiObj != null) {
            setImpAfi(new AfDecimal(impAfiObj, 15, 3));
            ws.getIndQuestAdegVer().setImpAfi(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setImpAfi(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpPaEspMsc1() {
        return questAdegVer.getP56ImpPaEspMsc1().getP56ImpPaEspMsc1();
    }

    @Override
    public void setImpPaEspMsc1(AfDecimal impPaEspMsc1) {
        this.questAdegVer.getP56ImpPaEspMsc1().setP56ImpPaEspMsc1(impPaEspMsc1.copy());
    }

    @Override
    public AfDecimal getImpPaEspMsc1Obj() {
        if (ws.getIndQuestAdegVer().getImpPaEspMsc1() >= 0) {
            return getImpPaEspMsc1();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpPaEspMsc1Obj(AfDecimal impPaEspMsc1Obj) {
        if (impPaEspMsc1Obj != null) {
            setImpPaEspMsc1(new AfDecimal(impPaEspMsc1Obj, 15, 3));
            ws.getIndQuestAdegVer().setImpPaEspMsc1(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setImpPaEspMsc1(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpPaEspMsc2() {
        return questAdegVer.getP56ImpPaEspMsc2().getP56ImpPaEspMsc2();
    }

    @Override
    public void setImpPaEspMsc2(AfDecimal impPaEspMsc2) {
        this.questAdegVer.getP56ImpPaEspMsc2().setP56ImpPaEspMsc2(impPaEspMsc2.copy());
    }

    @Override
    public AfDecimal getImpPaEspMsc2Obj() {
        if (ws.getIndQuestAdegVer().getImpPaEspMsc2() >= 0) {
            return getImpPaEspMsc2();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpPaEspMsc2Obj(AfDecimal impPaEspMsc2Obj) {
        if (impPaEspMsc2Obj != null) {
            setImpPaEspMsc2(new AfDecimal(impPaEspMsc2Obj, 15, 3));
            ws.getIndQuestAdegVer().setImpPaEspMsc2(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setImpPaEspMsc2(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpPaEspMsc3() {
        return questAdegVer.getP56ImpPaEspMsc3().getP56ImpPaEspMsc3();
    }

    @Override
    public void setImpPaEspMsc3(AfDecimal impPaEspMsc3) {
        this.questAdegVer.getP56ImpPaEspMsc3().setP56ImpPaEspMsc3(impPaEspMsc3.copy());
    }

    @Override
    public AfDecimal getImpPaEspMsc3Obj() {
        if (ws.getIndQuestAdegVer().getImpPaEspMsc3() >= 0) {
            return getImpPaEspMsc3();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpPaEspMsc3Obj(AfDecimal impPaEspMsc3Obj) {
        if (impPaEspMsc3Obj != null) {
            setImpPaEspMsc3(new AfDecimal(impPaEspMsc3Obj, 15, 3));
            ws.getIndQuestAdegVer().setImpPaEspMsc3(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setImpPaEspMsc3(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpPaEspMsc4() {
        return questAdegVer.getP56ImpPaEspMsc4().getP56ImpPaEspMsc4();
    }

    @Override
    public void setImpPaEspMsc4(AfDecimal impPaEspMsc4) {
        this.questAdegVer.getP56ImpPaEspMsc4().setP56ImpPaEspMsc4(impPaEspMsc4.copy());
    }

    @Override
    public AfDecimal getImpPaEspMsc4Obj() {
        if (ws.getIndQuestAdegVer().getImpPaEspMsc4() >= 0) {
            return getImpPaEspMsc4();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpPaEspMsc4Obj(AfDecimal impPaEspMsc4Obj) {
        if (impPaEspMsc4Obj != null) {
            setImpPaEspMsc4(new AfDecimal(impPaEspMsc4Obj, 15, 3));
            ws.getIndQuestAdegVer().setImpPaEspMsc4(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setImpPaEspMsc4(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpPaEspMsc5() {
        return questAdegVer.getP56ImpPaEspMsc5().getP56ImpPaEspMsc5();
    }

    @Override
    public void setImpPaEspMsc5(AfDecimal impPaEspMsc5) {
        this.questAdegVer.getP56ImpPaEspMsc5().setP56ImpPaEspMsc5(impPaEspMsc5.copy());
    }

    @Override
    public AfDecimal getImpPaEspMsc5Obj() {
        if (ws.getIndQuestAdegVer().getImpPaEspMsc5() >= 0) {
            return getImpPaEspMsc5();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpPaEspMsc5Obj(AfDecimal impPaEspMsc5Obj) {
        if (impPaEspMsc5Obj != null) {
            setImpPaEspMsc5(new AfDecimal(impPaEspMsc5Obj, 15, 3));
            ws.getIndQuestAdegVer().setImpPaEspMsc5(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setImpPaEspMsc5(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpTotAffAcc() {
        return questAdegVer.getP56ImpTotAffAcc().getP56ImpTotAffAcc();
    }

    @Override
    public void setImpTotAffAcc(AfDecimal impTotAffAcc) {
        this.questAdegVer.getP56ImpTotAffAcc().setP56ImpTotAffAcc(impTotAffAcc.copy());
    }

    @Override
    public AfDecimal getImpTotAffAccObj() {
        if (ws.getIndQuestAdegVer().getImpTotAffAcc() >= 0) {
            return getImpTotAffAcc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpTotAffAccObj(AfDecimal impTotAffAccObj) {
        if (impTotAffAccObj != null) {
            setImpTotAffAcc(new AfDecimal(impTotAffAccObj, 15, 3));
            ws.getIndQuestAdegVer().setImpTotAffAcc(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setImpTotAffAcc(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpTotAffUtil() {
        return questAdegVer.getP56ImpTotAffUtil().getP56ImpTotAffUtil();
    }

    @Override
    public void setImpTotAffUtil(AfDecimal impTotAffUtil) {
        this.questAdegVer.getP56ImpTotAffUtil().setP56ImpTotAffUtil(impTotAffUtil.copy());
    }

    @Override
    public AfDecimal getImpTotAffUtilObj() {
        if (ws.getIndQuestAdegVer().getImpTotAffUtil() >= 0) {
            return getImpTotAffUtil();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpTotAffUtilObj(AfDecimal impTotAffUtilObj) {
        if (impTotAffUtilObj != null) {
            setImpTotAffUtil(new AfDecimal(impTotAffUtilObj, 15, 3));
            ws.getIndQuestAdegVer().setImpTotAffUtil(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setImpTotAffUtil(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpTotFinAcc() {
        return questAdegVer.getP56ImpTotFinAcc().getP56ImpTotFinAcc();
    }

    @Override
    public void setImpTotFinAcc(AfDecimal impTotFinAcc) {
        this.questAdegVer.getP56ImpTotFinAcc().setP56ImpTotFinAcc(impTotFinAcc.copy());
    }

    @Override
    public AfDecimal getImpTotFinAccObj() {
        if (ws.getIndQuestAdegVer().getImpTotFinAcc() >= 0) {
            return getImpTotFinAcc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpTotFinAccObj(AfDecimal impTotFinAccObj) {
        if (impTotFinAccObj != null) {
            setImpTotFinAcc(new AfDecimal(impTotFinAccObj, 15, 3));
            ws.getIndQuestAdegVer().setImpTotFinAcc(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setImpTotFinAcc(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpTotFinUtil() {
        return questAdegVer.getP56ImpTotFinUtil().getP56ImpTotFinUtil();
    }

    @Override
    public void setImpTotFinUtil(AfDecimal impTotFinUtil) {
        this.questAdegVer.getP56ImpTotFinUtil().setP56ImpTotFinUtil(impTotFinUtil.copy());
    }

    @Override
    public AfDecimal getImpTotFinUtilObj() {
        if (ws.getIndQuestAdegVer().getImpTotFinUtil() >= 0) {
            return getImpTotFinUtil();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpTotFinUtilObj(AfDecimal impTotFinUtilObj) {
        if (impTotFinUtilObj != null) {
            setImpTotFinUtil(new AfDecimal(impTotFinUtilObj, 15, 3));
            ws.getIndQuestAdegVer().setImpTotFinUtil(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setImpTotFinUtil(((short)-1));
        }
    }

    @Override
    public String getLuogoCostituzioneVchar() {
        return questAdegVer.getP56LuogoCostituzioneVcharFormatted();
    }

    @Override
    public void setLuogoCostituzioneVchar(String luogoCostituzioneVchar) {
        this.questAdegVer.setP56LuogoCostituzioneVcharFormatted(luogoCostituzioneVchar);
    }

    @Override
    public String getLuogoCostituzioneVcharObj() {
        if (ws.getIndQuestAdegVer().getLuogoCostituzione() >= 0) {
            return getLuogoCostituzioneVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setLuogoCostituzioneVcharObj(String luogoCostituzioneVcharObj) {
        if (luogoCostituzioneVcharObj != null) {
            setLuogoCostituzioneVchar(luogoCostituzioneVcharObj);
            ws.getIndQuestAdegVer().setLuogoCostituzione(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setLuogoCostituzione(((short)-1));
        }
    }

    @Override
    public String getMotAssTitEffVchar() {
        return questAdegVer.getP56MotAssTitEffVcharFormatted();
    }

    @Override
    public void setMotAssTitEffVchar(String motAssTitEffVchar) {
        this.questAdegVer.setP56MotAssTitEffVcharFormatted(motAssTitEffVchar);
    }

    @Override
    public String getMotAssTitEffVcharObj() {
        if (ws.getIndQuestAdegVer().getMotAssTitEff() >= 0) {
            return getMotAssTitEffVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setMotAssTitEffVcharObj(String motAssTitEffVcharObj) {
        if (motAssTitEffVcharObj != null) {
            setMotAssTitEffVchar(motAssTitEffVcharObj);
            ws.getIndQuestAdegVer().setMotAssTitEff(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setMotAssTitEff(((short)-1));
        }
    }

    @Override
    public String getNaturaOprz() {
        return questAdegVer.getP56NaturaOprz();
    }

    @Override
    public void setNaturaOprz(String naturaOprz) {
        this.questAdegVer.setP56NaturaOprz(naturaOprz);
    }

    @Override
    public String getNaturaOprzObj() {
        if (ws.getIndQuestAdegVer().getNaturaOprz() >= 0) {
            return getNaturaOprz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setNaturaOprzObj(String naturaOprzObj) {
        if (naturaOprzObj != null) {
            setNaturaOprz(naturaOprzObj);
            ws.getIndQuestAdegVer().setNaturaOprz(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setNaturaOprz(((short)-1));
        }
    }

    @Override
    public String getNazDestFnd() {
        return questAdegVer.getP56NazDestFnd();
    }

    @Override
    public void setNazDestFnd(String nazDestFnd) {
        this.questAdegVer.setP56NazDestFnd(nazDestFnd);
    }

    @Override
    public String getNazDestFndObj() {
        if (ws.getIndQuestAdegVer().getNazDestFnd() >= 0) {
            return getNazDestFnd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setNazDestFndObj(String nazDestFndObj) {
        if (nazDestFndObj != null) {
            setNazDestFnd(nazDestFndObj);
            ws.getIndQuestAdegVer().setNazDestFnd(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setNazDestFnd(((short)-1));
        }
    }

    @Override
    public int getNumDip() {
        return questAdegVer.getP56NumDip().getP56NumDip();
    }

    @Override
    public void setNumDip(int numDip) {
        this.questAdegVer.getP56NumDip().setP56NumDip(numDip);
    }

    @Override
    public Integer getNumDipObj() {
        if (ws.getIndQuestAdegVer().getNumDip() >= 0) {
            return ((Integer)getNumDip());
        }
        else {
            return null;
        }
    }

    @Override
    public void setNumDipObj(Integer numDipObj) {
        if (numDipObj != null) {
            setNumDip(((int)numDipObj));
            ws.getIndQuestAdegVer().setNumDip(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setNumDip(((short)-1));
        }
    }

    @Override
    public String getNumTel2() {
        return questAdegVer.getP56NumTel2();
    }

    @Override
    public void setNumTel2(String numTel2) {
        this.questAdegVer.setP56NumTel2(numTel2);
    }

    @Override
    public String getNumTel2Obj() {
        if (ws.getIndQuestAdegVer().getNumTel2() >= 0) {
            return getNumTel2();
        }
        else {
            return null;
        }
    }

    @Override
    public void setNumTel2Obj(String numTel2Obj) {
        if (numTel2Obj != null) {
            setNumTel2(numTel2Obj);
            ws.getIndQuestAdegVer().setNumTel2(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setNumTel2(((short)-1));
        }
    }

    @Override
    public String getNumTel() {
        return questAdegVer.getP56NumTel();
    }

    @Override
    public void setNumTel(String numTel) {
        this.questAdegVer.setP56NumTel(numTel);
    }

    @Override
    public String getNumTelObj() {
        if (ws.getIndQuestAdegVer().getNumTel() >= 0) {
            return getNumTel();
        }
        else {
            return null;
        }
    }

    @Override
    public void setNumTelObj(String numTelObj) {
        if (numTelObj != null) {
            setNumTel(numTelObj);
            ws.getIndQuestAdegVer().setNumTel(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setNumTel(((short)-1));
        }
    }

    @Override
    public char getOprzSospette() {
        return questAdegVer.getP56OprzSospette();
    }

    @Override
    public void setOprzSospette(char oprzSospette) {
        this.questAdegVer.setP56OprzSospette(oprzSospette);
    }

    @Override
    public Character getOprzSospetteObj() {
        if (ws.getIndQuestAdegVer().getOprzSospette() >= 0) {
            return ((Character)getOprzSospette());
        }
        else {
            return null;
        }
    }

    @Override
    public void setOprzSospetteObj(Character oprzSospetteObj) {
        if (oprzSospetteObj != null) {
            setOprzSospette(((char)oprzSospetteObj));
            ws.getIndQuestAdegVer().setOprzSospette(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setOprzSospette(((short)-1));
        }
    }

    @Override
    public String getOrgnFnd() {
        return questAdegVer.getP56OrgnFnd();
    }

    @Override
    public void setOrgnFnd(String orgnFnd) {
        this.questAdegVer.setP56OrgnFnd(orgnFnd);
    }

    @Override
    public String getOrgnFndObj() {
        if (ws.getIndQuestAdegVer().getOrgnFnd() >= 0) {
            return getOrgnFnd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setOrgnFndObj(String orgnFndObj) {
        if (orgnFndObj != null) {
            setOrgnFnd(orgnFndObj);
            ws.getIndQuestAdegVer().setOrgnFnd(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setOrgnFnd(((short)-1));
        }
    }

    @Override
    public int getP56IdQuestAdegVer() {
        return questAdegVer.getP56IdQuestAdegVer();
    }

    @Override
    public void setP56IdQuestAdegVer(int p56IdQuestAdegVer) {
        this.questAdegVer.setP56IdQuestAdegVer(p56IdQuestAdegVer);
    }

    @Override
    public AfDecimal getPcEspAgPaMsc() {
        return questAdegVer.getP56PcEspAgPaMsc().getP56PcEspAgPaMsc();
    }

    @Override
    public void setPcEspAgPaMsc(AfDecimal pcEspAgPaMsc) {
        this.questAdegVer.getP56PcEspAgPaMsc().setP56PcEspAgPaMsc(pcEspAgPaMsc.copy());
    }

    @Override
    public AfDecimal getPcEspAgPaMscObj() {
        if (ws.getIndQuestAdegVer().getPcEspAgPaMsc() >= 0) {
            return getPcEspAgPaMsc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcEspAgPaMscObj(AfDecimal pcEspAgPaMscObj) {
        if (pcEspAgPaMscObj != null) {
            setPcEspAgPaMsc(new AfDecimal(pcEspAgPaMscObj, 6, 3));
            ws.getIndQuestAdegVer().setPcEspAgPaMsc(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setPcEspAgPaMsc(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcQuoDetTitEff() {
        return questAdegVer.getP56PcQuoDetTitEff().getP56PcQuoDetTitEff();
    }

    @Override
    public void setPcQuoDetTitEff(AfDecimal pcQuoDetTitEff) {
        this.questAdegVer.getP56PcQuoDetTitEff().setP56PcQuoDetTitEff(pcQuoDetTitEff.copy());
    }

    @Override
    public AfDecimal getPcQuoDetTitEffObj() {
        if (ws.getIndQuestAdegVer().getPcQuoDetTitEff() >= 0) {
            return getPcQuoDetTitEff();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcQuoDetTitEffObj(AfDecimal pcQuoDetTitEffObj) {
        if (pcQuoDetTitEffObj != null) {
            setPcQuoDetTitEff(new AfDecimal(pcQuoDetTitEffObj, 6, 3));
            ws.getIndQuestAdegVer().setPcQuoDetTitEff(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setPcQuoDetTitEff(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcRipPatAsVita() {
        return questAdegVer.getP56PcRipPatAsVita().getP56PcRipPatAsVita();
    }

    @Override
    public void setPcRipPatAsVita(AfDecimal pcRipPatAsVita) {
        this.questAdegVer.getP56PcRipPatAsVita().setP56PcRipPatAsVita(pcRipPatAsVita.copy());
    }

    @Override
    public AfDecimal getPcRipPatAsVitaObj() {
        if (ws.getIndQuestAdegVer().getPcRipPatAsVita() >= 0) {
            return getPcRipPatAsVita();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcRipPatAsVitaObj(AfDecimal pcRipPatAsVitaObj) {
        if (pcRipPatAsVitaObj != null) {
            setPcRipPatAsVita(new AfDecimal(pcRipPatAsVitaObj, 6, 3));
            ws.getIndQuestAdegVer().setPcRipPatAsVita(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setPcRipPatAsVita(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcRipPatIm() {
        return questAdegVer.getP56PcRipPatIm().getP56PcRipPatIm();
    }

    @Override
    public void setPcRipPatIm(AfDecimal pcRipPatIm) {
        this.questAdegVer.getP56PcRipPatIm().setP56PcRipPatIm(pcRipPatIm.copy());
    }

    @Override
    public AfDecimal getPcRipPatImObj() {
        if (ws.getIndQuestAdegVer().getPcRipPatIm() >= 0) {
            return getPcRipPatIm();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcRipPatImObj(AfDecimal pcRipPatImObj) {
        if (pcRipPatImObj != null) {
            setPcRipPatIm(new AfDecimal(pcRipPatImObj, 6, 3));
            ws.getIndQuestAdegVer().setPcRipPatIm(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setPcRipPatIm(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcRipPatSetIm() {
        return questAdegVer.getP56PcRipPatSetIm().getP56PcRipPatSetIm();
    }

    @Override
    public void setPcRipPatSetIm(AfDecimal pcRipPatSetIm) {
        this.questAdegVer.getP56PcRipPatSetIm().setP56PcRipPatSetIm(pcRipPatSetIm.copy());
    }

    @Override
    public AfDecimal getPcRipPatSetImObj() {
        if (ws.getIndQuestAdegVer().getPcRipPatSetIm() >= 0) {
            return getPcRipPatSetIm();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcRipPatSetImObj(AfDecimal pcRipPatSetImObj) {
        if (pcRipPatSetImObj != null) {
            setPcRipPatSetIm(new AfDecimal(pcRipPatSetImObj, 6, 3));
            ws.getIndQuestAdegVer().setPcRipPatSetIm(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setPcRipPatSetIm(((short)-1));
        }
    }

    @Override
    public AfDecimal getReddCon() {
        return questAdegVer.getP56ReddCon().getP56ReddCon();
    }

    @Override
    public void setReddCon(AfDecimal reddCon) {
        this.questAdegVer.getP56ReddCon().setP56ReddCon(reddCon.copy());
    }

    @Override
    public AfDecimal getReddConObj() {
        if (ws.getIndQuestAdegVer().getReddCon() >= 0) {
            return getReddCon();
        }
        else {
            return null;
        }
    }

    @Override
    public void setReddConObj(AfDecimal reddConObj) {
        if (reddConObj != null) {
            setReddCon(new AfDecimal(reddConObj, 15, 3));
            ws.getIndQuestAdegVer().setReddCon(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setReddCon(((short)-1));
        }
    }

    @Override
    public AfDecimal getReddFattAnnu() {
        return questAdegVer.getP56ReddFattAnnu().getP56ReddFattAnnu();
    }

    @Override
    public void setReddFattAnnu(AfDecimal reddFattAnnu) {
        this.questAdegVer.getP56ReddFattAnnu().setP56ReddFattAnnu(reddFattAnnu.copy());
    }

    @Override
    public AfDecimal getReddFattAnnuObj() {
        if (ws.getIndQuestAdegVer().getReddFattAnnu() >= 0) {
            return getReddFattAnnu();
        }
        else {
            return null;
        }
    }

    @Override
    public void setReddFattAnnuObj(AfDecimal reddFattAnnuObj) {
        if (reddFattAnnuObj != null) {
            setReddFattAnnu(new AfDecimal(reddFattAnnuObj, 15, 3));
            ws.getIndQuestAdegVer().setReddFattAnnu(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setReddFattAnnu(((short)-1));
        }
    }

    @Override
    public String getRegCollPoli() {
        return questAdegVer.getP56RegCollPoli();
    }

    @Override
    public void setRegCollPoli(String regCollPoli) {
        this.questAdegVer.setP56RegCollPoli(regCollPoli);
    }

    @Override
    public String getRegCollPoliObj() {
        if (ws.getIndQuestAdegVer().getRegCollPoli() >= 0) {
            return getRegCollPoli();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRegCollPoliObj(String regCollPoliObj) {
        if (regCollPoliObj != null) {
            setRegCollPoli(regCollPoliObj);
            ws.getIndQuestAdegVer().setRegCollPoli(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setRegCollPoli(((short)-1));
        }
    }

    @Override
    public String getStatOperEstero() {
        return questAdegVer.getP56StatOperEstero();
    }

    @Override
    public void setStatOperEstero(String statOperEstero) {
        this.questAdegVer.setP56StatOperEstero(statOperEstero);
    }

    @Override
    public String getStatOperEsteroObj() {
        if (ws.getIndQuestAdegVer().getStatOperEstero() >= 0) {
            return getStatOperEstero();
        }
        else {
            return null;
        }
    }

    @Override
    public void setStatOperEsteroObj(String statOperEsteroObj) {
        if (statOperEsteroObj != null) {
            setStatOperEstero(statOperEsteroObj);
            ws.getIndQuestAdegVer().setStatOperEstero(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setStatOperEstero(((short)-1));
        }
    }

    @Override
    public String getTpAdegVer() {
        return questAdegVer.getP56TpAdegVer();
    }

    @Override
    public void setTpAdegVer(String tpAdegVer) {
        this.questAdegVer.setP56TpAdegVer(tpAdegVer);
    }

    @Override
    public String getTpAdegVerObj() {
        if (ws.getIndQuestAdegVer().getTpAdegVer() >= 0) {
            return getTpAdegVer();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpAdegVerObj(String tpAdegVerObj) {
        if (tpAdegVerObj != null) {
            setTpAdegVer(tpAdegVerObj);
            ws.getIndQuestAdegVer().setTpAdegVer(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setTpAdegVer(((short)-1));
        }
    }

    @Override
    public String getTpCarFinGiur() {
        return questAdegVer.getP56TpCarFinGiur();
    }

    @Override
    public String getTpCarFinGiurAt() {
        return questAdegVer.getP56TpCarFinGiurAt();
    }

    @Override
    public void setTpCarFinGiurAt(String tpCarFinGiurAt) {
        this.questAdegVer.setP56TpCarFinGiurAt(tpCarFinGiurAt);
    }

    @Override
    public String getTpCarFinGiurAtObj() {
        if (ws.getIndQuestAdegVer().getTpCarFinGiurAt() >= 0) {
            return getTpCarFinGiurAt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpCarFinGiurAtObj(String tpCarFinGiurAtObj) {
        if (tpCarFinGiurAtObj != null) {
            setTpCarFinGiurAt(tpCarFinGiurAtObj);
            ws.getIndQuestAdegVer().setTpCarFinGiurAt(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setTpCarFinGiurAt(((short)-1));
        }
    }

    @Override
    public void setTpCarFinGiur(String tpCarFinGiur) {
        this.questAdegVer.setP56TpCarFinGiur(tpCarFinGiur);
    }

    @Override
    public String getTpCarFinGiurObj() {
        if (ws.getIndQuestAdegVer().getTpCarFinGiur() >= 0) {
            return getTpCarFinGiur();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpCarFinGiurObj(String tpCarFinGiurObj) {
        if (tpCarFinGiurObj != null) {
            setTpCarFinGiur(tpCarFinGiurObj);
            ws.getIndQuestAdegVer().setTpCarFinGiur(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setTpCarFinGiur(((short)-1));
        }
    }

    @Override
    public String getTpCarFinGiurPa() {
        return questAdegVer.getP56TpCarFinGiurPa();
    }

    @Override
    public void setTpCarFinGiurPa(String tpCarFinGiurPa) {
        this.questAdegVer.setP56TpCarFinGiurPa(tpCarFinGiurPa);
    }

    @Override
    public String getTpCarFinGiurPaObj() {
        if (ws.getIndQuestAdegVer().getTpCarFinGiurPa() >= 0) {
            return getTpCarFinGiurPa();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpCarFinGiurPaObj(String tpCarFinGiurPaObj) {
        if (tpCarFinGiurPaObj != null) {
            setTpCarFinGiurPa(tpCarFinGiurPaObj);
            ws.getIndQuestAdegVer().setTpCarFinGiurPa(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setTpCarFinGiurPa(((short)-1));
        }
    }

    @Override
    public String getTpCondCliente() {
        return questAdegVer.getP56TpCondCliente();
    }

    @Override
    public void setTpCondCliente(String tpCondCliente) {
        this.questAdegVer.setP56TpCondCliente(tpCondCliente);
    }

    @Override
    public String getTpCondClienteObj() {
        if (ws.getIndQuestAdegVer().getTpCondCliente() >= 0) {
            return getTpCondCliente();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpCondClienteObj(String tpCondClienteObj) {
        if (tpCondClienteObj != null) {
            setTpCondCliente(tpCondClienteObj);
            ws.getIndQuestAdegVer().setTpCondCliente(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setTpCondCliente(((short)-1));
        }
    }

    @Override
    public String getTpDestFnd() {
        return questAdegVer.getP56TpDestFnd();
    }

    @Override
    public void setTpDestFnd(String tpDestFnd) {
        this.questAdegVer.setP56TpDestFnd(tpDestFnd);
    }

    @Override
    public String getTpDestFndObj() {
        if (ws.getIndQuestAdegVer().getTpDestFnd() >= 0) {
            return getTpDestFnd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpDestFndObj(String tpDestFndObj) {
        if (tpDestFndObj != null) {
            setTpDestFnd(tpDestFndObj);
            ws.getIndQuestAdegVer().setTpDestFnd(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setTpDestFnd(((short)-1));
        }
    }

    @Override
    public String getTpDt1oConCli() {
        return questAdegVer.getP56TpDt1oConCli();
    }

    @Override
    public void setTpDt1oConCli(String tpDt1oConCli) {
        this.questAdegVer.setP56TpDt1oConCli(tpDt1oConCli);
    }

    @Override
    public String getTpDt1oConCliObj() {
        if (ws.getIndQuestAdegVer().getTpDt1oConCli() >= 0) {
            return getTpDt1oConCli();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpDt1oConCliObj(String tpDt1oConCliObj) {
        if (tpDt1oConCliObj != null) {
            setTpDt1oConCli(tpDt1oConCliObj);
            ws.getIndQuestAdegVer().setTpDt1oConCli(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setTpDt1oConCli(((short)-1));
        }
    }

    @Override
    public String getTpFinCost() {
        return questAdegVer.getP56TpFinCost();
    }

    @Override
    public void setTpFinCost(String tpFinCost) {
        this.questAdegVer.setP56TpFinCost(tpFinCost);
    }

    @Override
    public String getTpFinCostObj() {
        if (ws.getIndQuestAdegVer().getTpFinCost() >= 0) {
            return getTpFinCost();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpFinCostObj(String tpFinCostObj) {
        if (tpFinCostObj != null) {
            setTpFinCost(tpFinCostObj);
            ws.getIndQuestAdegVer().setTpFinCost(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setTpFinCost(((short)-1));
        }
    }

    @Override
    public String getTpFrmGiurSav() {
        return questAdegVer.getP56TpFrmGiurSav();
    }

    @Override
    public void setTpFrmGiurSav(String tpFrmGiurSav) {
        this.questAdegVer.setP56TpFrmGiurSav(tpFrmGiurSav);
    }

    @Override
    public String getTpFrmGiurSavObj() {
        if (ws.getIndQuestAdegVer().getTpFrmGiurSav() >= 0) {
            return getTpFrmGiurSav();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpFrmGiurSavObj(String tpFrmGiurSavObj) {
        if (tpFrmGiurSavObj != null) {
            setTpFrmGiurSav(tpFrmGiurSavObj);
            ws.getIndQuestAdegVer().setTpFrmGiurSav(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setTpFrmGiurSav(((short)-1));
        }
    }

    @Override
    public String getTpModEnRelaInt() {
        return questAdegVer.getP56TpModEnRelaInt();
    }

    @Override
    public void setTpModEnRelaInt(String tpModEnRelaInt) {
        this.questAdegVer.setP56TpModEnRelaInt(tpModEnRelaInt);
    }

    @Override
    public String getTpModEnRelaIntObj() {
        if (ws.getIndQuestAdegVer().getTpModEnRelaInt() >= 0) {
            return getTpModEnRelaInt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpModEnRelaIntObj(String tpModEnRelaIntObj) {
        if (tpModEnRelaIntObj != null) {
            setTpModEnRelaInt(tpModEnRelaIntObj);
            ws.getIndQuestAdegVer().setTpModEnRelaInt(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setTpModEnRelaInt(((short)-1));
        }
    }

    @Override
    public String getTpMotAssTitEff() {
        return questAdegVer.getP56TpMotAssTitEff();
    }

    @Override
    public void setTpMotAssTitEff(String tpMotAssTitEff) {
        this.questAdegVer.setP56TpMotAssTitEff(tpMotAssTitEff);
    }

    @Override
    public String getTpMotAssTitEffObj() {
        if (ws.getIndQuestAdegVer().getTpMotAssTitEff() >= 0) {
            return getTpMotAssTitEff();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpMotAssTitEffObj(String tpMotAssTitEffObj) {
        if (tpMotAssTitEffObj != null) {
            setTpMotAssTitEff(tpMotAssTitEffObj);
            ws.getIndQuestAdegVer().setTpMotAssTitEff(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setTpMotAssTitEff(((short)-1));
        }
    }

    @Override
    public String getTpMotCambioCntr() {
        return questAdegVer.getP56TpMotCambioCntr();
    }

    @Override
    public void setTpMotCambioCntr(String tpMotCambioCntr) {
        this.questAdegVer.setP56TpMotCambioCntr(tpMotCambioCntr);
    }

    @Override
    public String getTpMotCambioCntrObj() {
        if (ws.getIndQuestAdegVer().getTpMotCambioCntr() >= 0) {
            return getTpMotCambioCntr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpMotCambioCntrObj(String tpMotCambioCntrObj) {
        if (tpMotCambioCntrObj != null) {
            setTpMotCambioCntr(tpMotCambioCntrObj);
            ws.getIndQuestAdegVer().setTpMotCambioCntr(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setTpMotCambioCntr(((short)-1));
        }
    }

    @Override
    public String getTpMotRisc() {
        return questAdegVer.getP56TpMotRisc();
    }

    @Override
    public void setTpMotRisc(String tpMotRisc) {
        this.questAdegVer.setP56TpMotRisc(tpMotRisc);
    }

    @Override
    public String getTpMotRiscObj() {
        if (ws.getIndQuestAdegVer().getTpMotRisc() >= 0) {
            return getTpMotRisc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpMotRiscObj(String tpMotRiscObj) {
        if (tpMotRiscObj != null) {
            setTpMotRisc(tpMotRiscObj);
            ws.getIndQuestAdegVer().setTpMotRisc(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setTpMotRisc(((short)-1));
        }
    }

    @Override
    public int getTpMovi() {
        return questAdegVer.getP56TpMovi().getP56TpMovi();
    }

    @Override
    public void setTpMovi(int tpMovi) {
        this.questAdegVer.getP56TpMovi().setP56TpMovi(tpMovi);
    }

    @Override
    public Integer getTpMoviObj() {
        if (ws.getIndQuestAdegVer().getTpMovi() >= 0) {
            return ((Integer)getTpMovi());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpMoviObj(Integer tpMoviObj) {
        if (tpMoviObj != null) {
            setTpMovi(((int)tpMoviObj));
            ws.getIndQuestAdegVer().setTpMovi(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setTpMovi(((short)-1));
        }
    }

    @Override
    public String getTpNotPreg() {
        return questAdegVer.getP56TpNotPreg();
    }

    @Override
    public void setTpNotPreg(String tpNotPreg) {
        this.questAdegVer.setP56TpNotPreg(tpNotPreg);
    }

    @Override
    public String getTpNotPregObj() {
        if (ws.getIndQuestAdegVer().getTpNotPreg() >= 0) {
            return getTpNotPreg();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpNotPregObj(String tpNotPregObj) {
        if (tpNotPregObj != null) {
            setTpNotPreg(tpNotPregObj);
            ws.getIndQuestAdegVer().setTpNotPreg(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setTpNotPreg(((short)-1));
        }
    }

    @Override
    public String getTpOperEstero() {
        return questAdegVer.getP56TpOperEstero();
    }

    @Override
    public void setTpOperEstero(String tpOperEstero) {
        this.questAdegVer.setP56TpOperEstero(tpOperEstero);
    }

    @Override
    public String getTpOperEsteroObj() {
        if (ws.getIndQuestAdegVer().getTpOperEstero() >= 0) {
            return getTpOperEstero();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpOperEsteroObj(String tpOperEsteroObj) {
        if (tpOperEsteroObj != null) {
            setTpOperEstero(tpOperEsteroObj);
            ws.getIndQuestAdegVer().setTpOperEstero(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setTpOperEstero(((short)-1));
        }
    }

    @Override
    public String getTpOperSocFid() {
        return questAdegVer.getP56TpOperSocFid();
    }

    @Override
    public void setTpOperSocFid(String tpOperSocFid) {
        this.questAdegVer.setP56TpOperSocFid(tpOperSocFid);
    }

    @Override
    public String getTpOperSocFidObj() {
        if (ws.getIndQuestAdegVer().getTpOperSocFid() >= 0) {
            return getTpOperSocFid();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpOperSocFidObj(String tpOperSocFidObj) {
        if (tpOperSocFidObj != null) {
            setTpOperSocFid(tpOperSocFidObj);
            ws.getIndQuestAdegVer().setTpOperSocFid(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setTpOperSocFid(((short)-1));
        }
    }

    @Override
    public String getTpOriFndTitEff() {
        return questAdegVer.getP56TpOriFndTitEff();
    }

    @Override
    public void setTpOriFndTitEff(String tpOriFndTitEff) {
        this.questAdegVer.setP56TpOriFndTitEff(tpOriFndTitEff);
    }

    @Override
    public String getTpOriFndTitEffObj() {
        if (ws.getIndQuestAdegVer().getTpOriFndTitEff() >= 0) {
            return getTpOriFndTitEff();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpOriFndTitEffObj(String tpOriFndTitEffObj) {
        if (tpOriFndTitEffObj != null) {
            setTpOriFndTitEff(tpOriFndTitEffObj);
            ws.getIndQuestAdegVer().setTpOriFndTitEff(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setTpOriFndTitEff(((short)-1));
        }
    }

    @Override
    public String getTpPep() {
        return questAdegVer.getP56TpPep();
    }

    @Override
    public void setTpPep(String tpPep) {
        this.questAdegVer.setP56TpPep(tpPep);
    }

    @Override
    public String getTpPepObj() {
        if (ws.getIndQuestAdegVer().getTpPep() >= 0) {
            return getTpPep();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpPepObj(String tpPepObj) {
        if (tpPepObj != null) {
            setTpPep(tpPepObj);
            ws.getIndQuestAdegVer().setTpPep(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setTpPep(((short)-1));
        }
    }

    @Override
    public String getTpPntVnd() {
        return questAdegVer.getP56TpPntVnd();
    }

    @Override
    public void setTpPntVnd(String tpPntVnd) {
        this.questAdegVer.setP56TpPntVnd(tpPntVnd);
    }

    @Override
    public String getTpPntVndObj() {
        if (ws.getIndQuestAdegVer().getTpPntVnd() >= 0) {
            return getTpPntVnd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpPntVndObj(String tpPntVndObj) {
        if (tpPntVndObj != null) {
            setTpPntVnd(tpPntVndObj);
            ws.getIndQuestAdegVer().setTpPntVnd(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setTpPntVnd(((short)-1));
        }
    }

    @Override
    public String getTpPrflRshPep() {
        return questAdegVer.getP56TpPrflRshPep();
    }

    @Override
    public void setTpPrflRshPep(String tpPrflRshPep) {
        this.questAdegVer.setP56TpPrflRshPep(tpPrflRshPep);
    }

    @Override
    public String getTpPrflRshPepObj() {
        if (ws.getIndQuestAdegVer().getTpPrflRshPep() >= 0) {
            return getTpPrflRshPep();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpPrflRshPepObj(String tpPrflRshPepObj) {
        if (tpPrflRshPepObj != null) {
            setTpPrflRshPep(tpPrflRshPepObj);
            ws.getIndQuestAdegVer().setTpPrflRshPep(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setTpPrflRshPep(((short)-1));
        }
    }

    @Override
    public String getTpProcPnl() {
        return questAdegVer.getP56TpProcPnl();
    }

    @Override
    public void setTpProcPnl(String tpProcPnl) {
        this.questAdegVer.setP56TpProcPnl(tpProcPnl);
    }

    @Override
    public String getTpProcPnlObj() {
        if (ws.getIndQuestAdegVer().getTpProcPnl() >= 0) {
            return getTpProcPnl();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpProcPnlObj(String tpProcPnlObj) {
        if (tpProcPnlObj != null) {
            setTpProcPnl(tpProcPnlObj);
            ws.getIndQuestAdegVer().setTpProcPnl(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setTpProcPnl(((short)-1));
        }
    }

    @Override
    public String getTpRagRapp() {
        return questAdegVer.getP56TpRagRapp();
    }

    @Override
    public void setTpRagRapp(String tpRagRapp) {
        this.questAdegVer.setP56TpRagRapp(tpRagRapp);
    }

    @Override
    public String getTpRagRappObj() {
        if (ws.getIndQuestAdegVer().getTpRagRapp() >= 0) {
            return getTpRagRapp();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpRagRappObj(String tpRagRappObj) {
        if (tpRagRappObj != null) {
            setTpRagRapp(tpRagRappObj);
            ws.getIndQuestAdegVer().setTpRagRapp(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setTpRagRapp(((short)-1));
        }
    }

    @Override
    public String getTpReddAnnuLrd() {
        return questAdegVer.getP56TpReddAnnuLrd();
    }

    @Override
    public void setTpReddAnnuLrd(String tpReddAnnuLrd) {
        this.questAdegVer.setP56TpReddAnnuLrd(tpReddAnnuLrd);
    }

    @Override
    public String getTpReddAnnuLrdObj() {
        if (ws.getIndQuestAdegVer().getTpReddAnnuLrd() >= 0) {
            return getTpReddAnnuLrd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpReddAnnuLrdObj(String tpReddAnnuLrdObj) {
        if (tpReddAnnuLrdObj != null) {
            setTpReddAnnuLrd(tpReddAnnuLrdObj);
            ws.getIndQuestAdegVer().setTpReddAnnuLrd(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setTpReddAnnuLrd(((short)-1));
        }
    }

    @Override
    public String getTpReddCon() {
        return questAdegVer.getP56TpReddCon();
    }

    @Override
    public void setTpReddCon(String tpReddCon) {
        this.questAdegVer.setP56TpReddCon(tpReddCon);
    }

    @Override
    public String getTpReddConObj() {
        if (ws.getIndQuestAdegVer().getTpReddCon() >= 0) {
            return getTpReddCon();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpReddConObj(String tpReddConObj) {
        if (tpReddConObj != null) {
            setTpReddCon(tpReddConObj);
            ws.getIndQuestAdegVer().setTpReddCon(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setTpReddCon(((short)-1));
        }
    }

    @Override
    public String getTpRelaEsec() {
        return questAdegVer.getP56TpRelaEsec();
    }

    @Override
    public void setTpRelaEsec(String tpRelaEsec) {
        this.questAdegVer.setP56TpRelaEsec(tpRelaEsec);
    }

    @Override
    public String getTpRelaEsecObj() {
        if (ws.getIndQuestAdegVer().getTpRelaEsec() >= 0) {
            return getTpRelaEsec();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpRelaEsecObj(String tpRelaEsecObj) {
        if (tpRelaEsecObj != null) {
            setTpRelaEsec(tpRelaEsecObj);
            ws.getIndQuestAdegVer().setTpRelaEsec(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setTpRelaEsec(((short)-1));
        }
    }

    @Override
    public String getTpScoFinRapp() {
        return questAdegVer.getP56TpScoFinRapp();
    }

    @Override
    public void setTpScoFinRapp(String tpScoFinRapp) {
        this.questAdegVer.setP56TpScoFinRapp(tpScoFinRapp);
    }

    @Override
    public String getTpScoFinRappObj() {
        if (ws.getIndQuestAdegVer().getTpScoFinRapp() >= 0) {
            return getTpScoFinRapp();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpScoFinRappObj(String tpScoFinRappObj) {
        if (tpScoFinRappObj != null) {
            setTpScoFinRapp(tpScoFinRappObj);
            ws.getIndQuestAdegVer().setTpScoFinRapp(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setTpScoFinRapp(((short)-1));
        }
    }

    @Override
    public String getTpSitFamConv() {
        return questAdegVer.getP56TpSitFamConv();
    }

    @Override
    public void setTpSitFamConv(String tpSitFamConv) {
        this.questAdegVer.setP56TpSitFamConv(tpSitFamConv);
    }

    @Override
    public String getTpSitFamConvObj() {
        if (ws.getIndQuestAdegVer().getTpSitFamConv() >= 0) {
            return getTpSitFamConv();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpSitFamConvObj(String tpSitFamConvObj) {
        if (tpSitFamConvObj != null) {
            setTpSitFamConv(tpSitFamConvObj);
            ws.getIndQuestAdegVer().setTpSitFamConv(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setTpSitFamConv(((short)-1));
        }
    }

    @Override
    public String getTpSitFinPat() {
        return questAdegVer.getP56TpSitFinPat();
    }

    @Override
    public void setTpSitFinPat(String tpSitFinPat) {
        this.questAdegVer.setP56TpSitFinPat(tpSitFinPat);
    }

    @Override
    public String getTpSitFinPatCon() {
        return questAdegVer.getP56TpSitFinPatCon();
    }

    @Override
    public void setTpSitFinPatCon(String tpSitFinPatCon) {
        this.questAdegVer.setP56TpSitFinPatCon(tpSitFinPatCon);
    }

    @Override
    public String getTpSitFinPatConObj() {
        if (ws.getIndQuestAdegVer().getTpSitFinPatCon() >= 0) {
            return getTpSitFinPatCon();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpSitFinPatConObj(String tpSitFinPatConObj) {
        if (tpSitFinPatConObj != null) {
            setTpSitFinPatCon(tpSitFinPatConObj);
            ws.getIndQuestAdegVer().setTpSitFinPatCon(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setTpSitFinPatCon(((short)-1));
        }
    }

    @Override
    public String getTpSitFinPatObj() {
        if (ws.getIndQuestAdegVer().getTpSitFinPat() >= 0) {
            return getTpSitFinPat();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpSitFinPatObj(String tpSitFinPatObj) {
        if (tpSitFinPatObj != null) {
            setTpSitFinPat(tpSitFinPatObj);
            ws.getIndQuestAdegVer().setTpSitFinPat(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setTpSitFinPat(((short)-1));
        }
    }

    @Override
    public String getTpSitGiur() {
        return questAdegVer.getP56TpSitGiur();
    }

    @Override
    public void setTpSitGiur(String tpSitGiur) {
        this.questAdegVer.setP56TpSitGiur(tpSitGiur);
    }

    @Override
    public String getTpSitGiurObj() {
        if (ws.getIndQuestAdegVer().getTpSitGiur() >= 0) {
            return getTpSitGiur();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpSitGiurObj(String tpSitGiurObj) {
        if (tpSitGiurObj != null) {
            setTpSitGiur(tpSitGiurObj);
            ws.getIndQuestAdegVer().setTpSitGiur(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setTpSitGiur(((short)-1));
        }
    }

    @Override
    public String getTpSoc() {
        return questAdegVer.getP56TpSoc();
    }

    @Override
    public void setTpSoc(String tpSoc) {
        this.questAdegVer.setP56TpSoc(tpSoc);
    }

    @Override
    public String getTpSocObj() {
        if (ws.getIndQuestAdegVer().getTpSoc() >= 0) {
            return getTpSoc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpSocObj(String tpSocObj) {
        if (tpSocObj != null) {
            setTpSoc(tpSocObj);
            ws.getIndQuestAdegVer().setTpSoc(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setTpSoc(((short)-1));
        }
    }

    @Override
    public String getTpStatusAeoi() {
        return questAdegVer.getP56TpStatusAeoi();
    }

    @Override
    public void setTpStatusAeoi(String tpStatusAeoi) {
        this.questAdegVer.setP56TpStatusAeoi(tpStatusAeoi);
    }

    @Override
    public String getTpStatusAeoiObj() {
        if (ws.getIndQuestAdegVer().getTpStatusAeoi() >= 0) {
            return getTpStatusAeoi();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpStatusAeoiObj(String tpStatusAeoiObj) {
        if (tpStatusAeoiObj != null) {
            setTpStatusAeoi(tpStatusAeoiObj);
            ws.getIndQuestAdegVer().setTpStatusAeoi(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setTpStatusAeoi(((short)-1));
        }
    }

    @Override
    public String getTpStatusFatca() {
        return questAdegVer.getP56TpStatusFatca();
    }

    @Override
    public void setTpStatusFatca(String tpStatusFatca) {
        this.questAdegVer.setP56TpStatusFatca(tpStatusFatca);
    }

    @Override
    public String getTpStatusFatcaObj() {
        if (ws.getIndQuestAdegVer().getTpStatusFatca() >= 0) {
            return getTpStatusFatca();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpStatusFatcaObj(String tpStatusFatcaObj) {
        if (tpStatusFatcaObj != null) {
            setTpStatusFatca(tpStatusFatcaObj);
            ws.getIndQuestAdegVer().setTpStatusFatca(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setTpStatusFatca(((short)-1));
        }
    }

    @Override
    public AfDecimal getUltFattAnnu() {
        return questAdegVer.getP56UltFattAnnu().getP56UltFattAnnu();
    }

    @Override
    public void setUltFattAnnu(AfDecimal ultFattAnnu) {
        this.questAdegVer.getP56UltFattAnnu().setP56UltFattAnnu(ultFattAnnu.copy());
    }

    @Override
    public AfDecimal getUltFattAnnuObj() {
        if (ws.getIndQuestAdegVer().getUltFattAnnu() >= 0) {
            return getUltFattAnnu();
        }
        else {
            return null;
        }
    }

    @Override
    public void setUltFattAnnuObj(AfDecimal ultFattAnnuObj) {
        if (ultFattAnnuObj != null) {
            setUltFattAnnu(new AfDecimal(ultFattAnnuObj, 15, 3));
            ws.getIndQuestAdegVer().setUltFattAnnu(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setUltFattAnnu(((short)-1));
        }
    }

    @Override
    public String getValutColl() {
        return questAdegVer.getP56ValutColl();
    }

    @Override
    public void setValutColl(String valutColl) {
        this.questAdegVer.setP56ValutColl(valutColl);
    }

    @Override
    public String getValutCollObj() {
        if (ws.getIndQuestAdegVer().getValutColl() >= 0) {
            return getValutColl();
        }
        else {
            return null;
        }
    }

    @Override
    public void setValutCollObj(String valutCollObj) {
        if (valutCollObj != null) {
            setValutColl(valutCollObj);
            ws.getIndQuestAdegVer().setValutColl(((short)0));
        }
        else {
            ws.getIndQuestAdegVer().setValutColl(((short)-1));
        }
    }
}
