package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.jdbc.FieldNotMappedException;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.DettTitContDao;
import it.accenture.jnais.commons.data.to.IDettTitCont;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ldbs2130Data;
import it.accenture.jnais.ws.Ldbv2091;

/**Original name: LDBS2130<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  07 DIC 2010.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO AD HOC PER ACCESSO RISORSE DB        *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Ldbs2130 extends Program implements IDettTitCont {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private DettTitContDao dettTitContDao = new DettTitContDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Ldbs2130Data ws = new Ldbs2130Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: LDBV2131
    private Ldbv2091 ldbv2131;

    //==== METHODS ====
    /**Original name: PROGRAM_LDBS2130_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, Ldbv2091 ldbv2131) {
        this.idsv0003 = idsv0003;
        this.ldbv2131 = ldbv2131;
        // COB_CODE: PERFORM A000-INIZIO              THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                        a200ElaboraWcEff();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                        b200ElaboraWcCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //               SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                        c200ElaboraWcNst();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Ldbs2130 getInstance() {
        return ((Ldbs2130)Programs.getInstance(Ldbs2130.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'LDBS2130'              TO   IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("LDBS2130");
        // COB_CODE: MOVE 'LDBV2131' TO   IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("LDBV2131");
        // COB_CODE: MOVE '00'                      TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                    TO   IDSV0003-SQLCODE
        //                                               IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                    TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                               IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-WC-EFF<br>*/
    private void a200ElaboraWcEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-WC-EFF          THRU A210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-WC-EFF          THRU A210-EX
            a210SelectWcEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
            a260OpenCursorWcEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
            a270CloseCursorWcEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
            a280FetchFirstWcEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
            a290FetchNextWcEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B200-ELABORA-WC-CPZ<br>*/
    private void b200ElaboraWcCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
            b210SelectWcCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
            b260OpenCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
            b270CloseCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
            b280FetchFirstWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
            b290FetchNextWcCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: C200-ELABORA-WC-NST<br>*/
    private void c200ElaboraWcNst() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM C210-SELECT-WC-NST          THRU C210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM C210-SELECT-WC-NST          THRU C210-EX
            c210SelectWcNst();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
            c260OpenCursorWcNst();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
            c270CloseCursorWcNst();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
            c280FetchFirstWcNst();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
            c290FetchNextWcNst();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A205-DECLARE-CURSOR-WC-EFF<br>
	 * <pre>----
	 * ----  gestione WC Effetto
	 * ----</pre>*/
    private void a205DeclareCursorWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //              DECLARE C-EFF CURSOR FOR
        //            SELECT PRE_TOT
        //            FROM DETT_TIT_CONT
        //            WHERE ID_OGG = :LDBV2131-ID-OGG
        //              AND TP_OGG = :LDBV2131-TP-OGG
        //              AND TP_STAT_TIT IN (:LDBV2131-TP-STAT-TIT-01,
        //                                :LDBV2131-TP-STAT-TIT-02,
        //                                :LDBV2131-TP-STAT-TIT-03,
        //                                :LDBV2131-TP-STAT-TIT-04,
        //                                :LDBV2131-TP-STAT-TIT-05)
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A210-SELECT-WC-EFF<br>*/
    private void a210SelectWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A260-OPEN-CURSOR-WC-EFF<br>*/
    private void a260OpenCursorWcEff() {
        // COB_CODE: PERFORM A205-DECLARE-CURSOR-WC-EFF THRU A205-EX.
        a205DeclareCursorWcEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-EFF
        //           END-EXEC.
        dettTitContDao.openCEff10(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A270-CLOSE-CURSOR-WC-EFF<br>*/
    private void a270CloseCursorWcEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-EFF
        //           END-EXEC.
        dettTitContDao.closeCEff10();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A280-FETCH-FIRST-WC-EFF<br>*/
    private void a280FetchFirstWcEff() {
        // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF    THRU A260-EX.
        a260OpenCursorWcEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
            a290FetchNextWcEff();
        }
    }

    /**Original name: A290-FETCH-NEXT-WC-EFF<br>*/
    private void a290FetchNextWcEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-EFF
        //           INTO
        //            :LDBV2131-TOT-PREMI
        //            :IND-DTC-PRE-TOT
        //           END-EXEC.
        dettTitContDao.fetchCEff10(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF THRU A270-EX
            a270CloseCursorWcEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B205-DECLARE-CURSOR-WC-CPZ<br>
	 * <pre>----
	 * ----  gestione WC Competenza
	 * ----</pre>*/
    private void b205DeclareCursorWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //              DECLARE C-CPZ CURSOR FOR
        //            SELECT PRE_TOT
        //            FROM DETT_TIT_CONT
        //            WHERE ID_OGG = :LDBV2131-ID-OGG
        //              AND TP_OGG = :LDBV2131-TP-OGG
        //              AND TP_STAT_TIT IN (:LDBV2131-TP-STAT-TIT-01,
        //                                :LDBV2131-TP-STAT-TIT-02,
        //                                :LDBV2131-TP-STAT-TIT-03,
        //                                :LDBV2131-TP-STAT-TIT-04,
        //                                :LDBV2131-TP-STAT-TIT-05)
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B210-SELECT-WC-CPZ<br>*/
    private void b210SelectWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B260-OPEN-CURSOR-WC-CPZ<br>*/
    private void b260OpenCursorWcCpz() {
        // COB_CODE: PERFORM B205-DECLARE-CURSOR-WC-CPZ THRU B205-EX.
        b205DeclareCursorWcCpz();
        // COB_CODE: EXEC SQL
        //                OPEN C-CPZ
        //           END-EXEC.
        dettTitContDao.openCCpz10(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B270-CLOSE-CURSOR-WC-CPZ<br>*/
    private void b270CloseCursorWcCpz() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-CPZ
        //           END-EXEC.
        dettTitContDao.closeCCpz10();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B280-FETCH-FIRST-WC-CPZ<br>*/
    private void b280FetchFirstWcCpz() {
        // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ    THRU B260-EX.
        b260OpenCursorWcCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
            b290FetchNextWcCpz();
        }
    }

    /**Original name: B290-FETCH-NEXT-WC-CPZ<br>*/
    private void b290FetchNextWcCpz() {
        // COB_CODE: EXEC SQL
        //                FETCH C-CPZ
        //           INTO
        //            :LDBV2131-TOT-PREMI
        //            :IND-DTC-PRE-TOT
        //           END-EXEC.
        dettTitContDao.fetchCCpz10(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ THRU B270-EX
            b270CloseCursorWcCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: C205-DECLARE-CURSOR-WC-NST<br>
	 * <pre>----
	 * ----  gestione WC Senza Storicità
	 * ----</pre>*/
    private void c205DeclareCursorWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C210-SELECT-WC-NST<br>*/
    private void c210SelectWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C260-OPEN-CURSOR-WC-NST<br>*/
    private void c260OpenCursorWcNst() {
        // COB_CODE: PERFORM C205-DECLARE-CURSOR-WC-NST THRU C205-EX.
        c205DeclareCursorWcNst();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C270-CLOSE-CURSOR-WC-NST<br>*/
    private void c270CloseCursorWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C280-FETCH-FIRST-WC-NST<br>*/
    private void c280FetchFirstWcNst() {
        // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST    THRU C260-EX.
        c260OpenCursorWcNst();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
            c290FetchNextWcNst();
        }
    }

    /**Original name: C290-FETCH-NEXT-WC-NST<br>*/
    private void c290FetchNextWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>----
	 * ----  utilità comuni a tutti i livelli operazione
	 * ----</pre>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: CONTINUE.
        //continue
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z970-CODICE-ADHOC-PRE<br>
	 * <pre>----
	 * ----  prevede statements AD HOC PRE Query
	 * ----</pre>*/
    private void z970CodiceAdhocPre() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z980-CODICE-ADHOC-POST<br>
	 * <pre>----
	 * ----  prevede statements AD HOC POST Query
	 * ----</pre>*/
    private void z980CodiceAdhocPost() {
        // COB_CODE: IF IND-DTC-PRE-TOT = -1
        //              MOVE ZERO  TO LDBV2131-TOT-PREMI
        //           END-IF.
        if (ws.getIndDettTitCont().getPreTot() == -1) {
            // COB_CODE: MOVE ZERO  TO LDBV2131-TOT-PREMI
            ldbv2131.setTotPremi(new AfDecimal(0, 18, 3));
        }
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    @Override
    public AfDecimal getAcqExp() {
        throw new FieldNotMappedException("acqExp");
    }

    @Override
    public void setAcqExp(AfDecimal acqExp) {
        throw new FieldNotMappedException("acqExp");
    }

    @Override
    public AfDecimal getAcqExpObj() {
        return getAcqExp();
    }

    @Override
    public void setAcqExpObj(AfDecimal acqExpObj) {
        setAcqExp(new AfDecimal(acqExpObj, 15, 3));
    }

    @Override
    public AfDecimal getCarAcq() {
        throw new FieldNotMappedException("carAcq");
    }

    @Override
    public void setCarAcq(AfDecimal carAcq) {
        throw new FieldNotMappedException("carAcq");
    }

    @Override
    public AfDecimal getCarAcqObj() {
        return getCarAcq();
    }

    @Override
    public void setCarAcqObj(AfDecimal carAcqObj) {
        setCarAcq(new AfDecimal(carAcqObj, 15, 3));
    }

    @Override
    public AfDecimal getCarGest() {
        throw new FieldNotMappedException("carGest");
    }

    @Override
    public void setCarGest(AfDecimal carGest) {
        throw new FieldNotMappedException("carGest");
    }

    @Override
    public AfDecimal getCarGestObj() {
        return getCarGest();
    }

    @Override
    public void setCarGestObj(AfDecimal carGestObj) {
        setCarGest(new AfDecimal(carGestObj, 15, 3));
    }

    @Override
    public AfDecimal getCarIas() {
        throw new FieldNotMappedException("carIas");
    }

    @Override
    public void setCarIas(AfDecimal carIas) {
        throw new FieldNotMappedException("carIas");
    }

    @Override
    public AfDecimal getCarIasObj() {
        return getCarIas();
    }

    @Override
    public void setCarIasObj(AfDecimal carIasObj) {
        setCarIas(new AfDecimal(carIasObj, 15, 3));
    }

    @Override
    public AfDecimal getCarInc() {
        throw new FieldNotMappedException("carInc");
    }

    @Override
    public void setCarInc(AfDecimal carInc) {
        throw new FieldNotMappedException("carInc");
    }

    @Override
    public AfDecimal getCarIncObj() {
        return getCarInc();
    }

    @Override
    public void setCarIncObj(AfDecimal carIncObj) {
        setCarInc(new AfDecimal(carIncObj, 15, 3));
    }

    @Override
    public AfDecimal getCnbtAntirac() {
        throw new FieldNotMappedException("cnbtAntirac");
    }

    @Override
    public void setCnbtAntirac(AfDecimal cnbtAntirac) {
        throw new FieldNotMappedException("cnbtAntirac");
    }

    @Override
    public AfDecimal getCnbtAntiracObj() {
        return getCnbtAntirac();
    }

    @Override
    public void setCnbtAntiracObj(AfDecimal cnbtAntiracObj) {
        setCnbtAntirac(new AfDecimal(cnbtAntiracObj, 15, 3));
    }

    @Override
    public int getCodCompAnia() {
        throw new FieldNotMappedException("codCompAnia");
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        throw new FieldNotMappedException("codCompAnia");
    }

    @Override
    public String getCodDvs() {
        throw new FieldNotMappedException("codDvs");
    }

    @Override
    public void setCodDvs(String codDvs) {
        throw new FieldNotMappedException("codDvs");
    }

    @Override
    public String getCodDvsObj() {
        return getCodDvs();
    }

    @Override
    public void setCodDvsObj(String codDvsObj) {
        setCodDvs(codDvsObj);
    }

    @Override
    public String getCodTari() {
        throw new FieldNotMappedException("codTari");
    }

    @Override
    public void setCodTari(String codTari) {
        throw new FieldNotMappedException("codTari");
    }

    @Override
    public String getCodTariObj() {
        return getCodTari();
    }

    @Override
    public void setCodTariObj(String codTariObj) {
        setCodTari(codTariObj);
    }

    @Override
    public AfDecimal getCommisInter() {
        throw new FieldNotMappedException("commisInter");
    }

    @Override
    public void setCommisInter(AfDecimal commisInter) {
        throw new FieldNotMappedException("commisInter");
    }

    @Override
    public AfDecimal getCommisInterObj() {
        return getCommisInter();
    }

    @Override
    public void setCommisInterObj(AfDecimal commisInterObj) {
        setCommisInter(new AfDecimal(commisInterObj, 15, 3));
    }

    @Override
    public AfDecimal getDir() {
        throw new FieldNotMappedException("dir");
    }

    @Override
    public void setDir(AfDecimal dir) {
        throw new FieldNotMappedException("dir");
    }

    @Override
    public AfDecimal getDirObj() {
        return getDir();
    }

    @Override
    public void setDirObj(AfDecimal dirObj) {
        setDir(new AfDecimal(dirObj, 15, 3));
    }

    @Override
    public char getDsOperSql() {
        throw new FieldNotMappedException("dsOperSql");
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        throw new FieldNotMappedException("dsOperSql");
    }

    @Override
    public char getDsStatoElab() {
        throw new FieldNotMappedException("dsStatoElab");
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        throw new FieldNotMappedException("dsStatoElab");
    }

    @Override
    public long getDsTsEndCptz() {
        throw new FieldNotMappedException("dsTsEndCptz");
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        throw new FieldNotMappedException("dsTsEndCptz");
    }

    @Override
    public long getDsTsIniCptz() {
        throw new FieldNotMappedException("dsTsIniCptz");
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        throw new FieldNotMappedException("dsTsIniCptz");
    }

    @Override
    public String getDsUtente() {
        throw new FieldNotMappedException("dsUtente");
    }

    @Override
    public void setDsUtente(String dsUtente) {
        throw new FieldNotMappedException("dsUtente");
    }

    @Override
    public int getDsVer() {
        throw new FieldNotMappedException("dsVer");
    }

    @Override
    public void setDsVer(int dsVer) {
        throw new FieldNotMappedException("dsVer");
    }

    @Override
    public String getDtEndCopDb() {
        throw new FieldNotMappedException("dtEndCopDb");
    }

    @Override
    public void setDtEndCopDb(String dtEndCopDb) {
        throw new FieldNotMappedException("dtEndCopDb");
    }

    @Override
    public String getDtEndCopDbObj() {
        return getDtEndCopDb();
    }

    @Override
    public void setDtEndCopDbObj(String dtEndCopDbObj) {
        setDtEndCopDb(dtEndCopDbObj);
    }

    @Override
    public String getDtEndEffDb() {
        throw new FieldNotMappedException("dtEndEffDb");
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        throw new FieldNotMappedException("dtEndEffDb");
    }

    @Override
    public String getDtEsiTitDb() {
        throw new FieldNotMappedException("dtEsiTitDb");
    }

    @Override
    public void setDtEsiTitDb(String dtEsiTitDb) {
        throw new FieldNotMappedException("dtEsiTitDb");
    }

    @Override
    public String getDtEsiTitDbObj() {
        return getDtEsiTitDb();
    }

    @Override
    public void setDtEsiTitDbObj(String dtEsiTitDbObj) {
        setDtEsiTitDb(dtEsiTitDbObj);
    }

    @Override
    public String getDtIniEffDb() {
        throw new FieldNotMappedException("dtIniEffDb");
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        throw new FieldNotMappedException("dtIniEffDb");
    }

    @Override
    public long getDtcDsRiga() {
        throw new FieldNotMappedException("dtcDsRiga");
    }

    @Override
    public void setDtcDsRiga(long dtcDsRiga) {
        throw new FieldNotMappedException("dtcDsRiga");
    }

    @Override
    public String getDtcDtIniCopDb() {
        throw new FieldNotMappedException("dtcDtIniCopDb");
    }

    @Override
    public void setDtcDtIniCopDb(String dtcDtIniCopDb) {
        throw new FieldNotMappedException("dtcDtIniCopDb");
    }

    @Override
    public String getDtcDtIniCopDbObj() {
        return getDtcDtIniCopDb();
    }

    @Override
    public void setDtcDtIniCopDbObj(String dtcDtIniCopDbObj) {
        setDtcDtIniCopDb(dtcDtIniCopDbObj);
    }

    @Override
    public int getDtcIdOgg() {
        throw new FieldNotMappedException("dtcIdOgg");
    }

    @Override
    public void setDtcIdOgg(int dtcIdOgg) {
        throw new FieldNotMappedException("dtcIdOgg");
    }

    @Override
    public String getDtcTpOgg() {
        throw new FieldNotMappedException("dtcTpOgg");
    }

    @Override
    public void setDtcTpOgg(String dtcTpOgg) {
        throw new FieldNotMappedException("dtcTpOgg");
    }

    @Override
    public String getDtcTpStatTit() {
        throw new FieldNotMappedException("dtcTpStatTit");
    }

    @Override
    public void setDtcTpStatTit(String dtcTpStatTit) {
        throw new FieldNotMappedException("dtcTpStatTit");
    }

    @Override
    public int getFrqMovi() {
        throw new FieldNotMappedException("frqMovi");
    }

    @Override
    public void setFrqMovi(int frqMovi) {
        throw new FieldNotMappedException("frqMovi");
    }

    @Override
    public Integer getFrqMoviObj() {
        return ((Integer)getFrqMovi());
    }

    @Override
    public void setFrqMoviObj(Integer frqMoviObj) {
        setFrqMovi(((int)frqMoviObj));
    }

    @Override
    public int getIdDettTitCont() {
        throw new FieldNotMappedException("idDettTitCont");
    }

    @Override
    public void setIdDettTitCont(int idDettTitCont) {
        throw new FieldNotMappedException("idDettTitCont");
    }

    @Override
    public int getIdMoviChiu() {
        throw new FieldNotMappedException("idMoviChiu");
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        throw new FieldNotMappedException("idMoviChiu");
    }

    @Override
    public Integer getIdMoviChiuObj() {
        return ((Integer)getIdMoviChiu());
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        setIdMoviChiu(((int)idMoviChiuObj));
    }

    @Override
    public int getIdMoviCrz() {
        throw new FieldNotMappedException("idMoviCrz");
    }

    @Override
    public void setIdMoviCrz(int idMoviCrz) {
        throw new FieldNotMappedException("idMoviCrz");
    }

    @Override
    public int getIdTitCont() {
        throw new FieldNotMappedException("idTitCont");
    }

    @Override
    public void setIdTitCont(int idTitCont) {
        throw new FieldNotMappedException("idTitCont");
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        return idsv0003.getCodiceCompagniaAnia();
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        this.idsv0003.setCodiceCompagniaAnia(idsv0003CodiceCompagniaAnia);
    }

    @Override
    public AfDecimal getImpAder() {
        throw new FieldNotMappedException("impAder");
    }

    @Override
    public void setImpAder(AfDecimal impAder) {
        throw new FieldNotMappedException("impAder");
    }

    @Override
    public AfDecimal getImpAderObj() {
        return getImpAder();
    }

    @Override
    public void setImpAderObj(AfDecimal impAderObj) {
        setImpAder(new AfDecimal(impAderObj, 15, 3));
    }

    @Override
    public AfDecimal getImpAz() {
        throw new FieldNotMappedException("impAz");
    }

    @Override
    public void setImpAz(AfDecimal impAz) {
        throw new FieldNotMappedException("impAz");
    }

    @Override
    public AfDecimal getImpAzObj() {
        return getImpAz();
    }

    @Override
    public void setImpAzObj(AfDecimal impAzObj) {
        setImpAz(new AfDecimal(impAzObj, 15, 3));
    }

    @Override
    public AfDecimal getImpTfr() {
        throw new FieldNotMappedException("impTfr");
    }

    @Override
    public void setImpTfr(AfDecimal impTfr) {
        throw new FieldNotMappedException("impTfr");
    }

    @Override
    public AfDecimal getImpTfrObj() {
        return getImpTfr();
    }

    @Override
    public void setImpTfrObj(AfDecimal impTfrObj) {
        setImpTfr(new AfDecimal(impTfrObj, 15, 3));
    }

    @Override
    public AfDecimal getImpTfrStrc() {
        throw new FieldNotMappedException("impTfrStrc");
    }

    @Override
    public void setImpTfrStrc(AfDecimal impTfrStrc) {
        throw new FieldNotMappedException("impTfrStrc");
    }

    @Override
    public AfDecimal getImpTfrStrcObj() {
        return getImpTfrStrc();
    }

    @Override
    public void setImpTfrStrcObj(AfDecimal impTfrStrcObj) {
        setImpTfrStrc(new AfDecimal(impTfrStrcObj, 15, 3));
    }

    @Override
    public AfDecimal getImpTrasfe() {
        throw new FieldNotMappedException("impTrasfe");
    }

    @Override
    public void setImpTrasfe(AfDecimal impTrasfe) {
        throw new FieldNotMappedException("impTrasfe");
    }

    @Override
    public AfDecimal getImpTrasfeObj() {
        return getImpTrasfe();
    }

    @Override
    public void setImpTrasfeObj(AfDecimal impTrasfeObj) {
        setImpTrasfe(new AfDecimal(impTrasfeObj, 15, 3));
    }

    @Override
    public AfDecimal getImpVolo() {
        throw new FieldNotMappedException("impVolo");
    }

    @Override
    public void setImpVolo(AfDecimal impVolo) {
        throw new FieldNotMappedException("impVolo");
    }

    @Override
    public AfDecimal getImpVoloObj() {
        return getImpVolo();
    }

    @Override
    public void setImpVoloObj(AfDecimal impVoloObj) {
        setImpVolo(new AfDecimal(impVoloObj, 15, 3));
    }

    @Override
    public AfDecimal getIntrFraz() {
        throw new FieldNotMappedException("intrFraz");
    }

    @Override
    public void setIntrFraz(AfDecimal intrFraz) {
        throw new FieldNotMappedException("intrFraz");
    }

    @Override
    public AfDecimal getIntrFrazObj() {
        return getIntrFraz();
    }

    @Override
    public void setIntrFrazObj(AfDecimal intrFrazObj) {
        setIntrFraz(new AfDecimal(intrFrazObj, 15, 3));
    }

    @Override
    public AfDecimal getIntrMora() {
        throw new FieldNotMappedException("intrMora");
    }

    @Override
    public void setIntrMora(AfDecimal intrMora) {
        throw new FieldNotMappedException("intrMora");
    }

    @Override
    public AfDecimal getIntrMoraObj() {
        return getIntrMora();
    }

    @Override
    public void setIntrMoraObj(AfDecimal intrMoraObj) {
        setIntrMora(new AfDecimal(intrMoraObj, 15, 3));
    }

    @Override
    public AfDecimal getIntrRetdt() {
        throw new FieldNotMappedException("intrRetdt");
    }

    @Override
    public void setIntrRetdt(AfDecimal intrRetdt) {
        throw new FieldNotMappedException("intrRetdt");
    }

    @Override
    public AfDecimal getIntrRetdtObj() {
        return getIntrRetdt();
    }

    @Override
    public void setIntrRetdtObj(AfDecimal intrRetdtObj) {
        setIntrRetdt(new AfDecimal(intrRetdtObj, 15, 3));
    }

    @Override
    public AfDecimal getIntrRiat() {
        throw new FieldNotMappedException("intrRiat");
    }

    @Override
    public void setIntrRiat(AfDecimal intrRiat) {
        throw new FieldNotMappedException("intrRiat");
    }

    @Override
    public AfDecimal getIntrRiatObj() {
        return getIntrRiat();
    }

    @Override
    public void setIntrRiatObj(AfDecimal intrRiatObj) {
        setIntrRiat(new AfDecimal(intrRiatObj, 15, 3));
    }

    @Override
    public int getLdbv2131IdOgg() {
        return ldbv2131.getIdOgg();
    }

    @Override
    public void setLdbv2131IdOgg(int ldbv2131IdOgg) {
        this.ldbv2131.setIdOgg(ldbv2131IdOgg);
    }

    @Override
    public AfDecimal getLdbv2131TotPremi() {
        return ldbv2131.getTotPremi();
    }

    @Override
    public void setLdbv2131TotPremi(AfDecimal ldbv2131TotPremi) {
        this.ldbv2131.setTotPremi(ldbv2131TotPremi.copy());
    }

    @Override
    public AfDecimal getLdbv2131TotPremiObj() {
        if (ws.getIndDettTitCont().getPreTot() >= 0) {
            return getLdbv2131TotPremi();
        }
        else {
            return null;
        }
    }

    @Override
    public void setLdbv2131TotPremiObj(AfDecimal ldbv2131TotPremiObj) {
        if (ldbv2131TotPremiObj != null) {
            setLdbv2131TotPremi(new AfDecimal(ldbv2131TotPremiObj, 18, 3));
            ws.getIndDettTitCont().setPreTot(((short)0));
        }
        else {
            ws.getIndDettTitCont().setPreTot(((short)-1));
        }
    }

    @Override
    public String getLdbv2131TpOgg() {
        return ldbv2131.getTpOgg();
    }

    @Override
    public void setLdbv2131TpOgg(String ldbv2131TpOgg) {
        this.ldbv2131.setTpOgg(ldbv2131TpOgg);
    }

    @Override
    public String getLdbv2131TpStatTit01() {
        return ldbv2131.getTpStatTit01();
    }

    @Override
    public void setLdbv2131TpStatTit01(String ldbv2131TpStatTit01) {
        this.ldbv2131.setTpStatTit01(ldbv2131TpStatTit01);
    }

    @Override
    public String getLdbv2131TpStatTit02() {
        return ldbv2131.getTpStatTit02();
    }

    @Override
    public void setLdbv2131TpStatTit02(String ldbv2131TpStatTit02) {
        this.ldbv2131.setTpStatTit02(ldbv2131TpStatTit02);
    }

    @Override
    public String getLdbv2131TpStatTit03() {
        return ldbv2131.getTpStatTit03();
    }

    @Override
    public void setLdbv2131TpStatTit03(String ldbv2131TpStatTit03) {
        this.ldbv2131.setTpStatTit03(ldbv2131TpStatTit03);
    }

    @Override
    public String getLdbv2131TpStatTit04() {
        return ldbv2131.getTpStatTit04();
    }

    @Override
    public void setLdbv2131TpStatTit04(String ldbv2131TpStatTit04) {
        this.ldbv2131.setTpStatTit04(ldbv2131TpStatTit04);
    }

    @Override
    public String getLdbv2131TpStatTit05() {
        return ldbv2131.getTpStatTit05();
    }

    @Override
    public void setLdbv2131TpStatTit05(String ldbv2131TpStatTit05) {
        this.ldbv2131.setTpStatTit05(ldbv2131TpStatTit05);
    }

    @Override
    public String getLdbv4151DataInizPeriodoDb() {
        throw new FieldNotMappedException("ldbv4151DataInizPeriodoDb");
    }

    @Override
    public void setLdbv4151DataInizPeriodoDb(String ldbv4151DataInizPeriodoDb) {
        throw new FieldNotMappedException("ldbv4151DataInizPeriodoDb");
    }

    @Override
    public int getLdbv4151IdOgg() {
        throw new FieldNotMappedException("ldbv4151IdOgg");
    }

    @Override
    public void setLdbv4151IdOgg(int ldbv4151IdOgg) {
        throw new FieldNotMappedException("ldbv4151IdOgg");
    }

    @Override
    public AfDecimal getLdbv4151PreTot() {
        throw new FieldNotMappedException("ldbv4151PreTot");
    }

    @Override
    public void setLdbv4151PreTot(AfDecimal ldbv4151PreTot) {
        throw new FieldNotMappedException("ldbv4151PreTot");
    }

    @Override
    public String getLdbv4151TpOgg() {
        throw new FieldNotMappedException("ldbv4151TpOgg");
    }

    @Override
    public void setLdbv4151TpOgg(String ldbv4151TpOgg) {
        throw new FieldNotMappedException("ldbv4151TpOgg");
    }

    @Override
    public String getLdbv4151TpStatTit() {
        throw new FieldNotMappedException("ldbv4151TpStatTit");
    }

    @Override
    public void setLdbv4151TpStatTit(String ldbv4151TpStatTit) {
        throw new FieldNotMappedException("ldbv4151TpStatTit");
    }

    @Override
    public AfDecimal getManfeeAntic() {
        throw new FieldNotMappedException("manfeeAntic");
    }

    @Override
    public void setManfeeAntic(AfDecimal manfeeAntic) {
        throw new FieldNotMappedException("manfeeAntic");
    }

    @Override
    public AfDecimal getManfeeAnticObj() {
        return getManfeeAntic();
    }

    @Override
    public void setManfeeAnticObj(AfDecimal manfeeAnticObj) {
        setManfeeAntic(new AfDecimal(manfeeAnticObj, 15, 3));
    }

    @Override
    public AfDecimal getManfeeRec() {
        throw new FieldNotMappedException("manfeeRec");
    }

    @Override
    public void setManfeeRec(AfDecimal manfeeRec) {
        throw new FieldNotMappedException("manfeeRec");
    }

    @Override
    public AfDecimal getManfeeRecObj() {
        return getManfeeRec();
    }

    @Override
    public void setManfeeRecObj(AfDecimal manfeeRecObj) {
        setManfeeRec(new AfDecimal(manfeeRecObj, 15, 3));
    }

    @Override
    public AfDecimal getManfeeRicor() {
        throw new FieldNotMappedException("manfeeRicor");
    }

    @Override
    public void setManfeeRicor(AfDecimal manfeeRicor) {
        throw new FieldNotMappedException("manfeeRicor");
    }

    @Override
    public AfDecimal getManfeeRicorObj() {
        return getManfeeRicor();
    }

    @Override
    public void setManfeeRicorObj(AfDecimal manfeeRicorObj) {
        setManfeeRicor(new AfDecimal(manfeeRicorObj, 15, 3));
    }

    @Override
    public int getNumGgRitardoPag() {
        throw new FieldNotMappedException("numGgRitardoPag");
    }

    @Override
    public void setNumGgRitardoPag(int numGgRitardoPag) {
        throw new FieldNotMappedException("numGgRitardoPag");
    }

    @Override
    public Integer getNumGgRitardoPagObj() {
        return ((Integer)getNumGgRitardoPag());
    }

    @Override
    public void setNumGgRitardoPagObj(Integer numGgRitardoPagObj) {
        setNumGgRitardoPag(((int)numGgRitardoPagObj));
    }

    @Override
    public int getNumGgRival() {
        throw new FieldNotMappedException("numGgRival");
    }

    @Override
    public void setNumGgRival(int numGgRival) {
        throw new FieldNotMappedException("numGgRival");
    }

    @Override
    public Integer getNumGgRivalObj() {
        return ((Integer)getNumGgRival());
    }

    @Override
    public void setNumGgRivalObj(Integer numGgRivalObj) {
        setNumGgRival(((int)numGgRivalObj));
    }

    @Override
    public AfDecimal getPreNet() {
        throw new FieldNotMappedException("preNet");
    }

    @Override
    public void setPreNet(AfDecimal preNet) {
        throw new FieldNotMappedException("preNet");
    }

    @Override
    public AfDecimal getPreNetObj() {
        return getPreNet();
    }

    @Override
    public void setPreNetObj(AfDecimal preNetObj) {
        setPreNet(new AfDecimal(preNetObj, 15, 3));
    }

    @Override
    public AfDecimal getPrePpIas() {
        throw new FieldNotMappedException("prePpIas");
    }

    @Override
    public void setPrePpIas(AfDecimal prePpIas) {
        throw new FieldNotMappedException("prePpIas");
    }

    @Override
    public AfDecimal getPrePpIasObj() {
        return getPrePpIas();
    }

    @Override
    public void setPrePpIasObj(AfDecimal prePpIasObj) {
        setPrePpIas(new AfDecimal(prePpIasObj, 15, 3));
    }

    @Override
    public AfDecimal getPreSoloRsh() {
        throw new FieldNotMappedException("preSoloRsh");
    }

    @Override
    public void setPreSoloRsh(AfDecimal preSoloRsh) {
        throw new FieldNotMappedException("preSoloRsh");
    }

    @Override
    public AfDecimal getPreSoloRshObj() {
        return getPreSoloRsh();
    }

    @Override
    public void setPreSoloRshObj(AfDecimal preSoloRshObj) {
        setPreSoloRsh(new AfDecimal(preSoloRshObj, 15, 3));
    }

    @Override
    public AfDecimal getPreTot() {
        throw new FieldNotMappedException("preTot");
    }

    @Override
    public void setPreTot(AfDecimal preTot) {
        throw new FieldNotMappedException("preTot");
    }

    @Override
    public AfDecimal getPreTotObj() {
        return getPreTot();
    }

    @Override
    public void setPreTotObj(AfDecimal preTotObj) {
        setPreTot(new AfDecimal(preTotObj, 15, 3));
    }

    @Override
    public AfDecimal getProvAcq1aa() {
        throw new FieldNotMappedException("provAcq1aa");
    }

    @Override
    public void setProvAcq1aa(AfDecimal provAcq1aa) {
        throw new FieldNotMappedException("provAcq1aa");
    }

    @Override
    public AfDecimal getProvAcq1aaObj() {
        return getProvAcq1aa();
    }

    @Override
    public void setProvAcq1aaObj(AfDecimal provAcq1aaObj) {
        setProvAcq1aa(new AfDecimal(provAcq1aaObj, 15, 3));
    }

    @Override
    public AfDecimal getProvAcq2aa() {
        throw new FieldNotMappedException("provAcq2aa");
    }

    @Override
    public void setProvAcq2aa(AfDecimal provAcq2aa) {
        throw new FieldNotMappedException("provAcq2aa");
    }

    @Override
    public AfDecimal getProvAcq2aaObj() {
        return getProvAcq2aa();
    }

    @Override
    public void setProvAcq2aaObj(AfDecimal provAcq2aaObj) {
        setProvAcq2aa(new AfDecimal(provAcq2aaObj, 15, 3));
    }

    @Override
    public AfDecimal getProvDaRec() {
        throw new FieldNotMappedException("provDaRec");
    }

    @Override
    public void setProvDaRec(AfDecimal provDaRec) {
        throw new FieldNotMappedException("provDaRec");
    }

    @Override
    public AfDecimal getProvDaRecObj() {
        return getProvDaRec();
    }

    @Override
    public void setProvDaRecObj(AfDecimal provDaRecObj) {
        setProvDaRec(new AfDecimal(provDaRecObj, 15, 3));
    }

    @Override
    public AfDecimal getProvInc() {
        throw new FieldNotMappedException("provInc");
    }

    @Override
    public void setProvInc(AfDecimal provInc) {
        throw new FieldNotMappedException("provInc");
    }

    @Override
    public AfDecimal getProvIncObj() {
        return getProvInc();
    }

    @Override
    public void setProvIncObj(AfDecimal provIncObj) {
        setProvInc(new AfDecimal(provIncObj, 15, 3));
    }

    @Override
    public AfDecimal getProvRicor() {
        throw new FieldNotMappedException("provRicor");
    }

    @Override
    public void setProvRicor(AfDecimal provRicor) {
        throw new FieldNotMappedException("provRicor");
    }

    @Override
    public AfDecimal getProvRicorObj() {
        return getProvRicor();
    }

    @Override
    public void setProvRicorObj(AfDecimal provRicorObj) {
        setProvRicor(new AfDecimal(provRicorObj, 15, 3));
    }

    @Override
    public AfDecimal getRemunAss() {
        throw new FieldNotMappedException("remunAss");
    }

    @Override
    public void setRemunAss(AfDecimal remunAss) {
        throw new FieldNotMappedException("remunAss");
    }

    @Override
    public AfDecimal getRemunAssObj() {
        return getRemunAss();
    }

    @Override
    public void setRemunAssObj(AfDecimal remunAssObj) {
        setRemunAss(new AfDecimal(remunAssObj, 15, 3));
    }

    @Override
    public AfDecimal getSoprAlt() {
        throw new FieldNotMappedException("soprAlt");
    }

    @Override
    public void setSoprAlt(AfDecimal soprAlt) {
        throw new FieldNotMappedException("soprAlt");
    }

    @Override
    public AfDecimal getSoprAltObj() {
        return getSoprAlt();
    }

    @Override
    public void setSoprAltObj(AfDecimal soprAltObj) {
        setSoprAlt(new AfDecimal(soprAltObj, 15, 3));
    }

    @Override
    public AfDecimal getSoprProf() {
        throw new FieldNotMappedException("soprProf");
    }

    @Override
    public void setSoprProf(AfDecimal soprProf) {
        throw new FieldNotMappedException("soprProf");
    }

    @Override
    public AfDecimal getSoprProfObj() {
        return getSoprProf();
    }

    @Override
    public void setSoprProfObj(AfDecimal soprProfObj) {
        setSoprProf(new AfDecimal(soprProfObj, 15, 3));
    }

    @Override
    public AfDecimal getSoprSan() {
        throw new FieldNotMappedException("soprSan");
    }

    @Override
    public void setSoprSan(AfDecimal soprSan) {
        throw new FieldNotMappedException("soprSan");
    }

    @Override
    public AfDecimal getSoprSanObj() {
        return getSoprSan();
    }

    @Override
    public void setSoprSanObj(AfDecimal soprSanObj) {
        setSoprSan(new AfDecimal(soprSanObj, 15, 3));
    }

    @Override
    public AfDecimal getSoprSpo() {
        throw new FieldNotMappedException("soprSpo");
    }

    @Override
    public void setSoprSpo(AfDecimal soprSpo) {
        throw new FieldNotMappedException("soprSpo");
    }

    @Override
    public AfDecimal getSoprSpoObj() {
        return getSoprSpo();
    }

    @Override
    public void setSoprSpoObj(AfDecimal soprSpoObj) {
        setSoprSpo(new AfDecimal(soprSpoObj, 15, 3));
    }

    @Override
    public AfDecimal getSoprTec() {
        throw new FieldNotMappedException("soprTec");
    }

    @Override
    public void setSoprTec(AfDecimal soprTec) {
        throw new FieldNotMappedException("soprTec");
    }

    @Override
    public AfDecimal getSoprTecObj() {
        return getSoprTec();
    }

    @Override
    public void setSoprTecObj(AfDecimal soprTecObj) {
        setSoprTec(new AfDecimal(soprTecObj, 15, 3));
    }

    @Override
    public AfDecimal getSpeAge() {
        throw new FieldNotMappedException("speAge");
    }

    @Override
    public void setSpeAge(AfDecimal speAge) {
        throw new FieldNotMappedException("speAge");
    }

    @Override
    public AfDecimal getSpeAgeObj() {
        return getSpeAge();
    }

    @Override
    public void setSpeAgeObj(AfDecimal speAgeObj) {
        setSpeAge(new AfDecimal(speAgeObj, 15, 3));
    }

    @Override
    public AfDecimal getSpeMed() {
        throw new FieldNotMappedException("speMed");
    }

    @Override
    public void setSpeMed(AfDecimal speMed) {
        throw new FieldNotMappedException("speMed");
    }

    @Override
    public AfDecimal getSpeMedObj() {
        return getSpeMed();
    }

    @Override
    public void setSpeMedObj(AfDecimal speMedObj) {
        setSpeMed(new AfDecimal(speMedObj, 15, 3));
    }

    @Override
    public AfDecimal getTax() {
        throw new FieldNotMappedException("tax");
    }

    @Override
    public void setTax(AfDecimal tax) {
        throw new FieldNotMappedException("tax");
    }

    @Override
    public AfDecimal getTaxObj() {
        return getTax();
    }

    @Override
    public void setTaxObj(AfDecimal taxObj) {
        setTax(new AfDecimal(taxObj, 15, 3));
    }

    @Override
    public AfDecimal getTotIntrPrest() {
        throw new FieldNotMappedException("totIntrPrest");
    }

    @Override
    public void setTotIntrPrest(AfDecimal totIntrPrest) {
        throw new FieldNotMappedException("totIntrPrest");
    }

    @Override
    public AfDecimal getTotIntrPrestObj() {
        return getTotIntrPrest();
    }

    @Override
    public void setTotIntrPrestObj(AfDecimal totIntrPrestObj) {
        setTotIntrPrest(new AfDecimal(totIntrPrestObj, 15, 3));
    }

    @Override
    public String getTpRgmFisc() {
        throw new FieldNotMappedException("tpRgmFisc");
    }

    @Override
    public void setTpRgmFisc(String tpRgmFisc) {
        throw new FieldNotMappedException("tpRgmFisc");
    }

    @Override
    public String getWsDataInizioEffettoDb() {
        return ws.getIdsv0010().getWsDataInizioEffettoDb();
    }

    @Override
    public void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb) {
        this.ws.getIdsv0010().setWsDataInizioEffettoDb(wsDataInizioEffettoDb);
    }

    @Override
    public long getWsTsCompetenza() {
        return ws.getIdsv0010().getWsTsCompetenza();
    }

    @Override
    public void setWsTsCompetenza(long wsTsCompetenza) {
        this.ws.getIdsv0010().setWsTsCompetenza(wsTsCompetenza);
    }
}
