package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.CompStrDatoDao;
import it.accenture.jnais.commons.data.dao.RidefDatoStrDao;
import it.accenture.jnais.commons.data.dao.SinonimoStrDao;
import it.accenture.jnais.copy.CompStrDato;
import it.accenture.jnais.copy.Idso0021AreaErrori;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.AnagDato;
import it.accenture.jnais.ws.AreaCall;
import it.accenture.jnais.ws.ConversionVariables;
import it.accenture.jnais.ws.enums.Idsv0003Sqlcode;
import it.accenture.jnais.ws.Idss0020Data;
import it.accenture.jnais.ws.occurs.ScrElementsStrDato;
import it.accenture.jnais.ws.occurs.WkElementiPadre;
import it.accenture.jnais.ws.occurs.WkElementiPadreRed;
import it.accenture.jnais.ws.redefines.AdaLunghezzaDato;
import it.accenture.jnais.ws.redefines.AdaPrecisioneDato;
import it.accenture.jnais.ws.redefines.CsdLunghezzaDato;
import it.accenture.jnais.ws.redefines.CsdPrecisioneDato;
import it.accenture.jnais.ws.redefines.CsdRicorrenza;
import it.accenture.jnais.ws.redefines.Idso0021StrutturaDato;
import static java.lang.Math.abs;

/**Original name: IDSS0020<br>
 * <pre>AUTHOR. ATS NAPOLI.</pre>*/
public class Idss0020 extends Program {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private CompStrDatoDao compStrDatoDao = new CompStrDatoDao(dbAccessStatus);
    private RidefDatoStrDao ridefDatoStrDao = new RidefDatoStrDao(dbAccessStatus);
    private SinonimoStrDao sinonimoStrDao = new SinonimoStrDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Idss0020Data ws = new Idss0020Data();
    //Original name: AREA-CALL
    private AreaCall areaCall;

    //==== CONSTRUCTORS ====
    public Idss0020() {
        registerListeners();
    }

    //==== METHODS ====
    /**Original name: MAIN<br>
	 * <pre>************************************************************************
	 *      MOVE SPACES               TO INPUT-IDSS0020</pre>*/
    public long execute(AreaCall areaCall) {
        this.areaCall = areaCall;
        // COB_CODE: MOVE AREA-CALL            TO INPUT-IDSS0020.
        ws.setInputIdss0020Bytes(this.areaCall.getAreaCallBytes());
        // COB_CODE: MOVE 'COMP_STR_DATO'      TO IDSO0021-NOME-TABELLA.
        ws.getIdso0021().getIdso0021AreaErrori().setNomeTabella("COMP_STR_DATO");
        // COB_CODE: PERFORM A000-INIZIALIZZA-OUTPUT.
        a000InizializzaOutput();
        // COB_CODE: IF IDSO0021-SUCCESSFUL-RC
        //              END-IF
        //           END-IF
        if (ws.getIdso0021().getIdso0021AreaErrori().getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE IDSI0021-CODICE-COMPAGNIA-ANIA
            //                                         TO CSD-COD-COMPAGNIA-ANIA
            ws.getCompStrDato().setCsdCodCompagniaAnia(ws.getIdsi0021Area().getCodiceCompagniaAnia());
            // COB_CODE: MOVE IDSI0021-CODICE-STR-DATO TO CSD-COD-STR-DATO
            ws.getCompStrDato().setCsdCodStrDato(ws.getIdsi0021Area().getCodiceStrDato());
            // COB_CODE: ADD  1                        TO WK-NUMERO-LIVELLO
            ws.setWkNumeroLivello(Trunc.toShort(1 + ws.getWkNumeroLivello(), 3));
            // COB_CODE: IF WK-NUMERO-LIVELLO > WK-LIMITE-LIVELLI
            //              SET IDSO0021-EXCESS-OF-RECURSION TO TRUE
            //           END-IF
            if (ws.getWkNumeroLivello() > ws.getWkLimiteLivelli()) {
                // COB_CODE: MOVE 'SUPERATO LIMITE MASSIMO DI RICORSIONI'
                //                          TO IDSO0021-DESCRIZ-ERR-DB2
                ws.getIdso0021().getIdso0021AreaErrori().setDescrizErrDb2("SUPERATO LIMITE MASSIMO DI RICORSIONI");
                // COB_CODE: PERFORM 2000-DBERROR
                dberror();
                // COB_CODE: SET IDSO0021-EXCESS-OF-RECURSION TO TRUE
                ws.getIdso0021().getIdso0021AreaErrori().getReturnCode().setExcessOfRecursion();
            }
            // COB_CODE: IF IDSO0021-SUCCESSFUL-RC
            //              END-IF
            //           END-IF
            if (ws.getIdso0021().getIdso0021AreaErrori().getReturnCode().isSuccessfulRc()) {
                // COB_CODE: PERFORM A100-DECLARE
                a100Declare();
                // COB_CODE: PERFORM A200-OPEN
                a200Open();
                // COB_CODE: IF IDSO0021-SUCCESSFUL-RC
                //              PERFORM A300-FETCH
                //           END-IF
                if (ws.getIdso0021().getIdso0021AreaErrori().getReturnCode().isSuccessfulRc()) {
                    // COB_CODE: PERFORM A300-FETCH
                    a300Fetch();
                }
                // COB_CODE: IF IDSO0021-SUCCESSFUL-RC
                //              PERFORM 4000-FINE
                //           END-IF
                if (ws.getIdso0021().getIdso0021AreaErrori().getReturnCode().isSuccessfulRc()) {
                    // COB_CODE: PERFORM A400-CLOSE
                    a400Close();
                    // COB_CODE: PERFORM A500-OPERAZIONI-FINALI
                    a500OperazioniFinali();
                    // COB_CODE: PERFORM 4000-FINE
                    fine();
                }
            }
        }
        // COB_CODE: MOVE SPACES                    TO AREA-CALL
        this.areaCall.initAreaCallSpaces();
        // COB_CODE: MOVE OUTPUT-IDSS0020           TO AREA-CALL.
        this.areaCall.setAreaCallBytes(ws.getOutputIdss0020Bytes());
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Idss0020 getInstance() {
        return ((Idss0020)Programs.getInstance(Idss0020.class));
    }

    /**Original name: A000-INIZIALIZZA-OUTPUT_FIRST_SENTENCES<br>*/
    private void a000InizializzaOutput() {
        // COB_CODE: IF IDSI0021-INITIALIZE-SI
        //              MOVE PGM-NAME-IDSS0020     TO IDSO0021-COD-SERVIZIO-BE
        //           END-IF.
        if (ws.getIdsi0021Area().getInitialize().isSi()) {
            // COB_CODE: MOVE HIGH-VALUE           TO OUTPUT-IDSS0020
            ws.initOutputIdss0020HighValues();
            // COB_CODE: INITIALIZE
            //                                     WK-NUMERO-LIVELLO
            //                                     INDICE-ANAGR-DATO
            //                                     WK-CALCOLO-PADRE
            //                                     WK-CALCOLO-PADRE-RED
            //                                     WK-INIZIO-POSIZIONE-RED
            //                                     WK-INIZIO-POSIZIONE-RIC
            //                                     WK-INIZIO-POSIZIONE-MOL
            //                                     WK-NUMERO-RICORRENZA
            //                                     IND-SINONIM
            ws.setWkNumeroLivello(((short)0));
            ws.setIndiceAnagrDato(((short)0));
            initWkCalcoloPadre();
            initWkCalcoloPadreRed();
            ws.setWkInizioPosizioneRed(0);
            ws.setWkInizioPosizioneRic(0);
            ws.setWkInizioPosizioneMol(0);
            ws.setWkNumeroRicorrenza(0);
            ws.setIndSinonimFormatted("0");
            // COB_CODE: MOVE 'N'               TO FLAG-REDEFINES
            ws.setFlagRedefines(false);
            // COB_CODE: MOVE 1                 TO WK-INIZIO-POSIZIONE
            ws.setWkInizioPosizione(1);
            // COB_CODE: SET IDSI0021-INITIALIZE-NO TO TRUE
            ws.getIdsi0021Area().getInitialize().setNo();
            // COB_CODE: SET IDSO0021-SUCCESSFUL-RC TO TRUE
            ws.getIdso0021().getIdso0021AreaErrori().getReturnCode().setSuccessfulRc();
            // COB_CODE: PERFORM S000-ELABORA-SST
            s000ElaboraSst();
            // COB_CODE: MOVE PGM-NAME-IDSS0020     TO IDSO0021-COD-SERVIZIO-BE
            ws.getIdso0021().getIdso0021AreaErrori().setCodServizioBe(ws.getPgmNameIdss0020());
        }
    }

    /**Original name: A100-DECLARE_FIRST_SENTENCES<br>*/
    private void a100Declare() {
        // COB_CODE: EVALUATE WK-NUMERO-LIVELLO
        //           WHEN 1
        //              CONTINUE
        //           WHEN 2
        //              CONTINUE
        //           WHEN 3
        //              CONTINUE
        //           WHEN 4
        //              CONTINUE
        //           WHEN 5
        //              CONTINUE
        //           WHEN 6
        //              CONTINUE
        //           WHEN 7
        //              CONTINUE
        //           WHEN 8
        //              CONTINUE
        //           WHEN 9
        //              CONTINUE
        //           WHEN 10
        //              CONTINUE
        //           END-EVALUATE.
        switch (ws.getWkNumeroLivello()) {

            case ((short)1):// COB_CODE: EXEC SQL DECLARE CUR1 CURSOR FOR
            //             SELECT
            //             COD_COMPAGNIA_ANIA
            //             ,COD_STR_DATO
            //             ,POSIZIONE
            //             ,COD_STR_DATO2
            //             ,COD_DATO
            //             ,FLAG_KEY
            //             ,FLAG_RETURN_CODE
            //             ,FLAG_CALL_USING
            //             ,FLAG_WHERE_COND
            //             ,FLAG_REDEFINES
            //             ,TIPO_DATO
            //             ,LUNGHEZZA_DATO
            //             ,PRECISIONE_DATO
            //             ,COD_DOMINIO
            //             ,FORMATTAZIONE_DATO
            //             ,SERVIZIO_CONVERS
            //             ,AREA_CONV_STANDARD
            //             ,RICORRENZA
            //             ,OBBLIGATORIETA
            //             ,VALORE_DEFAULT
            //             ,DS_UTENTE
            //              FROM COMP_STR_DATO
            //            WHERE COD_COMPAGNIA_ANIA = :CSD-COD-COMPAGNIA-ANIA
            //              AND COD_STR_DATO       = :CSD-COD-STR-DATO
            //              ORDER BY POSIZIONE
            //           END-EXEC
            // DECLARE CURSOR doesn't need a translation;
            // COB_CODE: CONTINUE
            //continue
                break;

            case ((short)2):// COB_CODE: EXEC SQL DECLARE CUR2 CURSOR FOR
            //               SELECT
            //               COD_COMPAGNIA_ANIA
            //               ,COD_STR_DATO
            //               ,POSIZIONE
            //               ,COD_STR_DATO2
            //               ,COD_DATO
            //               ,FLAG_KEY
            //               ,FLAG_RETURN_CODE
            //               ,FLAG_CALL_USING
            //               ,FLAG_WHERE_COND
            //               ,FLAG_REDEFINES
            //               ,TIPO_DATO
            //               ,LUNGHEZZA_DATO
            //               ,PRECISIONE_DATO
            //               ,COD_DOMINIO
            //               ,FORMATTAZIONE_DATO
            //               ,SERVIZIO_CONVERS
            //               ,AREA_CONV_STANDARD
            //               ,RICORRENZA
            //               ,OBBLIGATORIETA
            //               ,VALORE_DEFAULT
            //               ,DS_UTENTE
            //                FROM COMP_STR_DATO
            //              WHERE COD_COMPAGNIA_ANIA = :CSD-COD-COMPAGNIA-ANIA
            //                AND COD_STR_DATO       = :CSD-COD-STR-DATO
            //                ORDER BY POSIZIONE
            //             END-EXEC
            // DECLARE CURSOR doesn't need a translation;
            // COB_CODE: CONTINUE
            //continue
                break;

            case ((short)3):// COB_CODE:  EXEC SQL DECLARE CUR3 CURSOR FOR
            //             SELECT
            //             COD_COMPAGNIA_ANIA
            //             ,COD_STR_DATO
            //             ,POSIZIONE
            //             ,COD_STR_DATO2
            //             ,COD_DATO
            //             ,FLAG_KEY
            //             ,FLAG_RETURN_CODE
            //             ,FLAG_CALL_USING
            //             ,FLAG_WHERE_COND
            //             ,FLAG_REDEFINES
            //             ,TIPO_DATO
            //             ,LUNGHEZZA_DATO
            //             ,PRECISIONE_DATO
            //             ,COD_DOMINIO
            //             ,FORMATTAZIONE_DATO
            //             ,SERVIZIO_CONVERS
            //             ,AREA_CONV_STANDARD
            //             ,RICORRENZA
            //             ,OBBLIGATORIETA
            //             ,VALORE_DEFAULT
            //             ,DS_UTENTE
            //              FROM COMP_STR_DATO
            //            WHERE COD_COMPAGNIA_ANIA = :CSD-COD-COMPAGNIA-ANIA
            //              AND COD_STR_DATO       = :CSD-COD-STR-DATO
            //              ORDER BY POSIZIONE
            //           END-EXEC
            // DECLARE CURSOR doesn't need a translation;
            // COB_CODE: CONTINUE
            //continue
                break;

            case ((short)4):// COB_CODE:  EXEC SQL DECLARE CUR4 CURSOR FOR
            //             SELECT
            //             COD_COMPAGNIA_ANIA
            //             ,COD_STR_DATO
            //             ,POSIZIONE
            //             ,COD_STR_DATO2
            //             ,COD_DATO
            //             ,FLAG_KEY
            //             ,FLAG_RETURN_CODE
            //             ,FLAG_CALL_USING
            //             ,FLAG_WHERE_COND
            //             ,FLAG_REDEFINES
            //             ,TIPO_DATO
            //             ,LUNGHEZZA_DATO
            //             ,PRECISIONE_DATO
            //             ,COD_DOMINIO
            //             ,FORMATTAZIONE_DATO
            //             ,SERVIZIO_CONVERS
            //             ,AREA_CONV_STANDARD
            //             ,RICORRENZA
            //             ,OBBLIGATORIETA
            //             ,VALORE_DEFAULT
            //             ,DS_UTENTE
            //              FROM COMP_STR_DATO
            //            WHERE COD_COMPAGNIA_ANIA = :CSD-COD-COMPAGNIA-ANIA
            //              AND COD_STR_DATO       = :CSD-COD-STR-DATO
            //              ORDER BY POSIZIONE
            //           END-EXEC
            // DECLARE CURSOR doesn't need a translation;
            // COB_CODE: CONTINUE
            //continue
                break;

            case ((short)5):// COB_CODE:  EXEC SQL DECLARE CUR5 CURSOR FOR
            //             SELECT
            //             COD_COMPAGNIA_ANIA
            //             ,COD_STR_DATO
            //             ,POSIZIONE
            //             ,COD_STR_DATO2
            //             ,COD_DATO
            //             ,FLAG_KEY
            //             ,FLAG_RETURN_CODE
            //             ,FLAG_CALL_USING
            //             ,FLAG_WHERE_COND
            //             ,FLAG_REDEFINES
            //             ,TIPO_DATO
            //             ,LUNGHEZZA_DATO
            //             ,PRECISIONE_DATO
            //             ,COD_DOMINIO
            //             ,FORMATTAZIONE_DATO
            //             ,SERVIZIO_CONVERS
            //             ,AREA_CONV_STANDARD
            //             ,RICORRENZA
            //             ,OBBLIGATORIETA
            //             ,VALORE_DEFAULT
            //             ,DS_UTENTE
            //              FROM COMP_STR_DATO
            //            WHERE COD_COMPAGNIA_ANIA = :CSD-COD-COMPAGNIA-ANIA
            //              AND COD_STR_DATO       = :CSD-COD-STR-DATO
            //              ORDER BY POSIZIONE
            //           END-EXEC
            // DECLARE CURSOR doesn't need a translation;
            // COB_CODE: CONTINUE
            //continue
                break;

            case ((short)6):// COB_CODE: EXEC SQL DECLARE CUR6 CURSOR FOR
            //             SELECT
            //             COD_COMPAGNIA_ANIA
            //             ,COD_STR_DATO
            //             ,POSIZIONE
            //             ,COD_STR_DATO2
            //             ,COD_DATO
            //             ,FLAG_KEY
            //             ,FLAG_RETURN_CODE
            //             ,FLAG_CALL_USING
            //             ,FLAG_WHERE_COND
            //             ,FLAG_REDEFINES
            //             ,TIPO_DATO
            //             ,LUNGHEZZA_DATO
            //             ,PRECISIONE_DATO
            //             ,COD_DOMINIO
            //             ,FORMATTAZIONE_DATO
            //             ,SERVIZIO_CONVERS
            //             ,AREA_CONV_STANDARD
            //             ,RICORRENZA
            //             ,OBBLIGATORIETA
            //             ,VALORE_DEFAULT
            //             ,DS_UTENTE
            //              FROM COMP_STR_DATO
            //            WHERE COD_COMPAGNIA_ANIA = :CSD-COD-COMPAGNIA-ANIA
            //              AND COD_STR_DATO       = :CSD-COD-STR-DATO
            //              ORDER BY POSIZIONE
            //           END-EXEC
            // DECLARE CURSOR doesn't need a translation;
            // COB_CODE: CONTINUE
            //continue
                break;

            case ((short)7):// COB_CODE: EXEC SQL DECLARE CUR7 CURSOR FOR
            //             SELECT
            //             COD_COMPAGNIA_ANIA
            //             ,COD_STR_DATO
            //             ,POSIZIONE
            //             ,COD_STR_DATO2
            //             ,COD_DATO
            //             ,FLAG_KEY
            //             ,FLAG_RETURN_CODE
            //             ,FLAG_CALL_USING
            //             ,FLAG_WHERE_COND
            //             ,FLAG_REDEFINES
            //             ,TIPO_DATO
            //             ,LUNGHEZZA_DATO
            //             ,PRECISIONE_DATO
            //             ,COD_DOMINIO
            //             ,FORMATTAZIONE_DATO
            //             ,SERVIZIO_CONVERS
            //             ,AREA_CONV_STANDARD
            //             ,RICORRENZA
            //             ,OBBLIGATORIETA
            //             ,VALORE_DEFAULT
            //             ,DS_UTENTE
            //              FROM COMP_STR_DATO
            //            WHERE COD_COMPAGNIA_ANIA = :CSD-COD-COMPAGNIA-ANIA
            //              AND COD_STR_DATO       = :CSD-COD-STR-DATO
            //              ORDER BY POSIZIONE
            //           END-EXEC
            // DECLARE CURSOR doesn't need a translation;
            // COB_CODE: CONTINUE
            //continue
                break;

            case ((short)8):// COB_CODE: EXEC SQL DECLARE CUR8 CURSOR FOR
            //             SELECT
            //             COD_COMPAGNIA_ANIA
            //             ,COD_STR_DATO
            //             ,POSIZIONE
            //             ,COD_STR_DATO2
            //             ,COD_DATO
            //             ,FLAG_KEY
            //             ,FLAG_RETURN_CODE
            //             ,FLAG_CALL_USING
            //             ,FLAG_WHERE_COND
            //             ,FLAG_REDEFINES
            //             ,TIPO_DATO
            //             ,LUNGHEZZA_DATO
            //             ,PRECISIONE_DATO
            //             ,COD_DOMINIO
            //             ,FORMATTAZIONE_DATO
            //             ,SERVIZIO_CONVERS
            //             ,AREA_CONV_STANDARD
            //             ,RICORRENZA
            //             ,OBBLIGATORIETA
            //             ,VALORE_DEFAULT
            //             ,DS_UTENTE
            //              FROM COMP_STR_DATO
            //            WHERE COD_COMPAGNIA_ANIA = :CSD-COD-COMPAGNIA-ANIA
            //              AND COD_STR_DATO       = :CSD-COD-STR-DATO
            //              ORDER BY POSIZIONE
            //           END-EXEC
            // DECLARE CURSOR doesn't need a translation;
            // COB_CODE: CONTINUE
            //continue
                break;

            case ((short)9):// COB_CODE: EXEC SQL DECLARE CUR9 CURSOR FOR
            //             SELECT
            //             COD_COMPAGNIA_ANIA
            //             ,COD_STR_DATO
            //             ,POSIZIONE
            //             ,COD_STR_DATO2
            //             ,COD_DATO
            //             ,FLAG_KEY
            //             ,FLAG_RETURN_CODE
            //             ,FLAG_CALL_USING
            //             ,FLAG_WHERE_COND
            //             ,FLAG_REDEFINES
            //             ,TIPO_DATO
            //             ,LUNGHEZZA_DATO
            //             ,PRECISIONE_DATO
            //             ,COD_DOMINIO
            //             ,FORMATTAZIONE_DATO
            //             ,SERVIZIO_CONVERS
            //             ,AREA_CONV_STANDARD
            //             ,RICORRENZA
            //             ,OBBLIGATORIETA
            //             ,VALORE_DEFAULT
            //             ,DS_UTENTE
            //              FROM COMP_STR_DATO
            //            WHERE COD_COMPAGNIA_ANIA = :CSD-COD-COMPAGNIA-ANIA
            //              AND     COD_STR_DATO   = :CSD-COD-STR-DATO
            //              ORDER BY POSIZIONE
            //           END-EXEC
            // DECLARE CURSOR doesn't need a translation;
            // COB_CODE: CONTINUE
            //continue
                break;

            case ((short)10):// COB_CODE: EXEC SQL DECLARE CUR10 CURSOR FOR
            //             SELECT
            //             COD_COMPAGNIA_ANIA
            //             ,COD_STR_DATO
            //             ,POSIZIONE
            //             ,COD_STR_DATO2
            //             ,COD_DATO
            //             ,FLAG_KEY
            //             ,FLAG_RETURN_CODE
            //             ,FLAG_CALL_USING
            //             ,FLAG_WHERE_COND
            //             ,FLAG_REDEFINES
            //             ,TIPO_DATO
            //             ,LUNGHEZZA_DATO
            //             ,PRECISIONE_DATO
            //             ,COD_DOMINIO
            //             ,FORMATTAZIONE_DATO
            //             ,SERVIZIO_CONVERS
            //             ,AREA_CONV_STANDARD
            //             ,RICORRENZA
            //             ,OBBLIGATORIETA
            //             ,VALORE_DEFAULT
            //             ,DS_UTENTE
            //              FROM COMP_STR_DATO
            //            WHERE COD_COMPAGNIA_ANIA = :CSD-COD-COMPAGNIA-ANIA
            //              AND COD_STR_DATO       = :CSD-COD-STR-DATO
            //              ORDER BY POSIZIONE
            //           END-EXEC
            // DECLARE CURSOR doesn't need a translation;
            // COB_CODE: CONTINUE
            //continue
                break;

            default:break;
        }
    }

    /**Original name: A200-OPEN_FIRST_SENTENCES<br>*/
    private void a200Open() {
        // COB_CODE: EVALUATE WK-NUMERO-LIVELLO
        //           WHEN 1
        //              CONTINUE
        //           WHEN 2
        //              CONTINUE
        //           WHEN 3
        //              CONTINUE
        //           WHEN 4
        //              CONTINUE
        //           WHEN 5
        //              CONTINUE
        //           WHEN 6
        //              CONTINUE
        //           WHEN 7
        //              CONTINUE
        //           WHEN 8
        //              CONTINUE
        //           WHEN 9
        //              CONTINUE
        //           WHEN 10
        //              CONTINUE
        //           END-EVALUATE.
        switch (ws.getWkNumeroLivello()) {

            case ((short)1):// COB_CODE: EXEC SQL OPEN CUR1  END-EXEC
                compStrDatoDao.openCur1(ws.getCompStrDato().getCsdCodCompagniaAnia(), ws.getCompStrDato().getCsdCodStrDato());
                // COB_CODE: CONTINUE
                //continue
                break;

            case ((short)2):// COB_CODE: EXEC SQL OPEN CUR2  END-EXEC
                compStrDatoDao.openCur1(ws.getCompStrDato().getCsdCodCompagniaAnia(), ws.getCompStrDato().getCsdCodStrDato());
                // COB_CODE: CONTINUE
                //continue
                break;

            case ((short)3):// COB_CODE: EXEC SQL OPEN CUR3  END-EXEC
                compStrDatoDao.openCur1(ws.getCompStrDato().getCsdCodCompagniaAnia(), ws.getCompStrDato().getCsdCodStrDato());
                // COB_CODE: CONTINUE
                //continue
                break;

            case ((short)4):// COB_CODE: EXEC SQL OPEN CUR4  END-EXEC
                compStrDatoDao.openCur1(ws.getCompStrDato().getCsdCodCompagniaAnia(), ws.getCompStrDato().getCsdCodStrDato());
                // COB_CODE: CONTINUE
                //continue
                break;

            case ((short)5):// COB_CODE: EXEC SQL OPEN CUR5  END-EXEC
                compStrDatoDao.openCur1(ws.getCompStrDato().getCsdCodCompagniaAnia(), ws.getCompStrDato().getCsdCodStrDato());
                // COB_CODE: CONTINUE
                //continue
                break;

            case ((short)6):// COB_CODE: EXEC SQL OPEN CUR6  END-EXEC
                compStrDatoDao.openCur1(ws.getCompStrDato().getCsdCodCompagniaAnia(), ws.getCompStrDato().getCsdCodStrDato());
                // COB_CODE: CONTINUE
                //continue
                break;

            case ((short)7):// COB_CODE: EXEC SQL OPEN CUR7  END-EXEC
                compStrDatoDao.openCur1(ws.getCompStrDato().getCsdCodCompagniaAnia(), ws.getCompStrDato().getCsdCodStrDato());
                // COB_CODE: CONTINUE
                //continue
                break;

            case ((short)8):// COB_CODE: EXEC SQL OPEN CUR8  END-EXEC
                compStrDatoDao.openCur1(ws.getCompStrDato().getCsdCodCompagniaAnia(), ws.getCompStrDato().getCsdCodStrDato());
                // COB_CODE: CONTINUE
                //continue
                break;

            case ((short)9):// COB_CODE: EXEC SQL OPEN CUR9  END-EXEC
                compStrDatoDao.openCur1(ws.getCompStrDato().getCsdCodCompagniaAnia(), ws.getCompStrDato().getCsdCodStrDato());
                // COB_CODE: CONTINUE
                //continue
                break;

            case ((short)10):// COB_CODE: EXEC SQL OPEN CUR10 END-EXEC
                compStrDatoDao.openCur1(ws.getCompStrDato().getCsdCodCompagniaAnia(), ws.getCompStrDato().getCsdCodStrDato());
                // COB_CODE: CONTINUE
                //continue
                break;

            default:break;
        }
        // COB_CODE: MOVE SQLCODE         TO IDSO0021-SQLCODE
        ws.getIdso0021().getIdso0021AreaErrori().getSqlcode().setSqlcodeSigned(sqlca.getSqlcode());
        // COB_CODE: MOVE DESCRIZ-ERR-DB2 TO IDSO0021-DESCRIZ-ERR-DB2
        ws.getIdso0021().getIdso0021AreaErrori().setDescrizErrDb2(ws.getDescrizErrDb2());
        // COB_CODE: IF NOT IDSO0021-SUCCESSFUL-SQL
        //              PERFORM 2000-DBERROR
        //           END-IF.
        if (!ws.getIdso0021().getIdso0021AreaErrori().getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM 2000-DBERROR
            dberror();
        }
    }

    /**Original name: A300-FETCH_FIRST_SENTENCES<br>*/
    private void a300Fetch() {
        // COB_CODE: MOVE SPACES               TO COMP-STR-DATO
        ws.getCompStrDato().initCompStrDatoSpaces();
        // COB_CODE: EVALUATE WK-NUMERO-LIVELLO
        //           WHEN 1
        //              END-PERFORM
        //           WHEN 2
        //              END-PERFORM
        //           WHEN 3
        //              END-PERFORM
        //           WHEN 4
        //              END-PERFORM
        //           WHEN 5
        //              END-PERFORM
        //              WHEN 6
        //              END-PERFORM
        //              WHEN 7
        //              END-PERFORM
        //              WHEN 8
        //              END-PERFORM
        //              WHEN 9
        //              END-PERFORM
        //              WHEN 10
        //              END-PERFORM
        //           END-EVALUATE.
        switch (ws.getWkNumeroLivello()) {

            case ((short)1):// COB_CODE: PERFORM UNTIL IDSO0021-NOT-FOUND
                //           OR NOT IDSO0021-SUCCESSFUL-RC
                //              PERFORM C000-CONTROLLO-FIGLIO
                //           END-PERFORM
                while (!(ws.getIdso0021().getIdso0021AreaErrori().getSqlcode().isNotFound() || !ws.getIdso0021().getIdso0021AreaErrori().getReturnCode().isSuccessfulRc())) {
                    // COB_CODE: EXEC SQL FETCH CUR1
                    //           INTO
                    //           :CSD-COD-COMPAGNIA-ANIA
                    //           ,:CSD-COD-STR-DATO
                    //           ,:CSD-POSIZIONE
                    //           ,:CSD-COD-STR-DATO2
                    //            :IND-CSD-COD-STR-DATO2
                    //           ,:CSD-COD-DATO
                    //            :IND-CSD-COD-DATO
                    //           ,:CSD-FLAG-KEY
                    //            :IND-CSD-FLAG-KEY
                    //           ,:CSD-FLAG-RETURN-CODE
                    //            :IND-CSD-FLAG-RETURN-CODE
                    //           ,:CSD-FLAG-CALL-USING
                    //            :IND-CSD-FLAG-CALL-USING
                    //           ,:CSD-FLAG-WHERE-COND
                    //            :IND-CSD-FLAG-WHERE-COND
                    //           ,:CSD-FLAG-REDEFINES
                    //            :IND-CSD-FLAG-REDEFINES
                    //           ,:CSD-TIPO-DATO
                    //            :IND-CSD-TIPO-DATO
                    //           ,:CSD-LUNGHEZZA-DATO
                    //            :IND-CSD-LUNGHEZZA-DATO
                    //           ,:CSD-PRECISIONE-DATO
                    //            :IND-CSD-PRECISIONE-DATO
                    //           ,:CSD-COD-DOMINIO
                    //            :IND-CSD-COD-DOMINIO
                    //           ,:CSD-FORMATTAZIONE-DATO
                    //            :IND-CSD-FORMATTAZIONE-DATO
                    //           ,:CSD-SERVIZIO-CONVERS
                    //            :IND-CSD-SERVIZIO-CONVERS
                    //           ,:CSD-AREA-CONV-STANDARD
                    //            :IND-CSD-AREA-CONV-STANDARD
                    //           ,:CSD-RICORRENZA
                    //            :IND-CSD-RICORRENZA
                    //           ,:CSD-OBBLIGATORIETA
                    //            :IND-CSD-OBBLIGATORIETA
                    //           ,:CSD-VALORE-DEFAULT
                    //            :IND-CSD-VALORE-DEFAULT
                    //           ,:CSD-DS-UTENTE
                    //           END-EXEC
                    compStrDatoDao.fetchCur1(ws);
                    // COB_CODE: PERFORM C000-CONTROLLO-FIGLIO
                    c000ControlloFiglio();
                }
                break;

            case ((short)2):// COB_CODE: PERFORM UNTIL IDSO0021-NOT-FOUND
                //           OR NOT IDSO0021-SUCCESSFUL-RC
                //              PERFORM C000-CONTROLLO-FIGLIO
                //           END-PERFORM
                while (!(ws.getIdso0021().getIdso0021AreaErrori().getSqlcode().isNotFound() || !ws.getIdso0021().getIdso0021AreaErrori().getReturnCode().isSuccessfulRc())) {
                    // COB_CODE: EXEC SQL FETCH CUR2
                    //            INTO
                    //           :CSD-COD-COMPAGNIA-ANIA
                    //           ,:CSD-COD-STR-DATO
                    //           ,:CSD-POSIZIONE
                    //           ,:CSD-COD-STR-DATO2
                    //            :IND-CSD-COD-STR-DATO2
                    //           ,:CSD-COD-DATO
                    //            :IND-CSD-COD-DATO
                    //           ,:CSD-FLAG-KEY
                    //            :IND-CSD-FLAG-KEY
                    //           ,:CSD-FLAG-RETURN-CODE
                    //            :IND-CSD-FLAG-RETURN-CODE
                    //           ,:CSD-FLAG-CALL-USING
                    //            :IND-CSD-FLAG-CALL-USING
                    //           ,:CSD-FLAG-WHERE-COND
                    //            :IND-CSD-FLAG-WHERE-COND
                    //           ,:CSD-FLAG-REDEFINES
                    //            :IND-CSD-FLAG-REDEFINES
                    //           ,:CSD-TIPO-DATO
                    //            :IND-CSD-TIPO-DATO
                    //           ,:CSD-LUNGHEZZA-DATO
                    //            :IND-CSD-LUNGHEZZA-DATO
                    //           ,:CSD-PRECISIONE-DATO
                    //            :IND-CSD-PRECISIONE-DATO
                    //           ,:CSD-COD-DOMINIO
                    //            :IND-CSD-COD-DOMINIO
                    //           ,:CSD-FORMATTAZIONE-DATO
                    //            :IND-CSD-FORMATTAZIONE-DATO
                    //           ,:CSD-SERVIZIO-CONVERS
                    //            :IND-CSD-SERVIZIO-CONVERS
                    //           ,:CSD-AREA-CONV-STANDARD
                    //            :IND-CSD-AREA-CONV-STANDARD
                    //           ,:CSD-RICORRENZA
                    //            :IND-CSD-RICORRENZA
                    //           ,:CSD-OBBLIGATORIETA
                    //            :IND-CSD-OBBLIGATORIETA
                    //           ,:CSD-VALORE-DEFAULT
                    //            :IND-CSD-VALORE-DEFAULT
                    //           ,:CSD-DS-UTENTE
                    //           END-EXEC
                    compStrDatoDao.fetchCur1(ws);
                    // COB_CODE: PERFORM C000-CONTROLLO-FIGLIO
                    c000ControlloFiglio();
                }
                break;

            case ((short)3):// COB_CODE: PERFORM UNTIL IDSO0021-NOT-FOUND
                //           OR NOT IDSO0021-SUCCESSFUL-RC
                //              PERFORM C000-CONTROLLO-FIGLIO
                //           END-PERFORM
                while (!(ws.getIdso0021().getIdso0021AreaErrori().getSqlcode().isNotFound() || !ws.getIdso0021().getIdso0021AreaErrori().getReturnCode().isSuccessfulRc())) {
                    // COB_CODE: EXEC SQL FETCH CUR3
                    //            INTO
                    //           :CSD-COD-COMPAGNIA-ANIA
                    //           ,:CSD-COD-STR-DATO
                    //           ,:CSD-POSIZIONE
                    //           ,:CSD-COD-STR-DATO2
                    //            :IND-CSD-COD-STR-DATO2
                    //           ,:CSD-COD-DATO
                    //            :IND-CSD-COD-DATO
                    //           ,:CSD-FLAG-KEY
                    //            :IND-CSD-FLAG-KEY
                    //           ,:CSD-FLAG-RETURN-CODE
                    //            :IND-CSD-FLAG-RETURN-CODE
                    //           ,:CSD-FLAG-CALL-USING
                    //            :IND-CSD-FLAG-CALL-USING
                    //           ,:CSD-FLAG-WHERE-COND
                    //            :IND-CSD-FLAG-WHERE-COND
                    //           ,:CSD-FLAG-REDEFINES
                    //            :IND-CSD-FLAG-REDEFINES
                    //           ,:CSD-TIPO-DATO
                    //            :IND-CSD-TIPO-DATO
                    //           ,:CSD-LUNGHEZZA-DATO
                    //            :IND-CSD-LUNGHEZZA-DATO
                    //           ,:CSD-PRECISIONE-DATO
                    //            :IND-CSD-PRECISIONE-DATO
                    //           ,:CSD-COD-DOMINIO
                    //            :IND-CSD-COD-DOMINIO
                    //           ,:CSD-FORMATTAZIONE-DATO
                    //            :IND-CSD-FORMATTAZIONE-DATO
                    //           ,:CSD-SERVIZIO-CONVERS
                    //            :IND-CSD-SERVIZIO-CONVERS
                    //           ,:CSD-AREA-CONV-STANDARD
                    //            :IND-CSD-AREA-CONV-STANDARD
                    //           ,:CSD-RICORRENZA
                    //            :IND-CSD-RICORRENZA
                    //           ,:CSD-OBBLIGATORIETA
                    //            :IND-CSD-OBBLIGATORIETA
                    //           ,:CSD-VALORE-DEFAULT
                    //            :IND-CSD-VALORE-DEFAULT
                    //           ,:CSD-DS-UTENTE
                    //           END-EXEC
                    compStrDatoDao.fetchCur1(ws);
                    // COB_CODE: PERFORM C000-CONTROLLO-FIGLIO
                    c000ControlloFiglio();
                }
                break;

            case ((short)4):// COB_CODE: PERFORM UNTIL IDSO0021-NOT-FOUND
                //           OR NOT IDSO0021-SUCCESSFUL-RC
                //              PERFORM C000-CONTROLLO-FIGLIO
                //           END-PERFORM
                while (!(ws.getIdso0021().getIdso0021AreaErrori().getSqlcode().isNotFound() || !ws.getIdso0021().getIdso0021AreaErrori().getReturnCode().isSuccessfulRc())) {
                    // COB_CODE: EXEC SQL FETCH CUR4
                    //            INTO
                    //           :CSD-COD-COMPAGNIA-ANIA
                    //           ,:CSD-COD-STR-DATO
                    //           ,:CSD-POSIZIONE
                    //           ,:CSD-COD-STR-DATO2
                    //            :IND-CSD-COD-STR-DATO2
                    //           ,:CSD-COD-DATO
                    //            :IND-CSD-COD-DATO
                    //           ,:CSD-FLAG-KEY
                    //            :IND-CSD-FLAG-KEY
                    //           ,:CSD-FLAG-RETURN-CODE
                    //            :IND-CSD-FLAG-RETURN-CODE
                    //           ,:CSD-FLAG-CALL-USING
                    //            :IND-CSD-FLAG-CALL-USING
                    //           ,:CSD-FLAG-WHERE-COND
                    //            :IND-CSD-FLAG-WHERE-COND
                    //           ,:CSD-FLAG-REDEFINES
                    //            :IND-CSD-FLAG-REDEFINES
                    //           ,:CSD-TIPO-DATO
                    //            :IND-CSD-TIPO-DATO
                    //           ,:CSD-LUNGHEZZA-DATO
                    //            :IND-CSD-LUNGHEZZA-DATO
                    //           ,:CSD-PRECISIONE-DATO
                    //            :IND-CSD-PRECISIONE-DATO
                    //           ,:CSD-COD-DOMINIO
                    //            :IND-CSD-COD-DOMINIO
                    //           ,:CSD-FORMATTAZIONE-DATO
                    //            :IND-CSD-FORMATTAZIONE-DATO
                    //           ,:CSD-SERVIZIO-CONVERS
                    //            :IND-CSD-SERVIZIO-CONVERS
                    //           ,:CSD-AREA-CONV-STANDARD
                    //            :IND-CSD-AREA-CONV-STANDARD
                    //           ,:CSD-RICORRENZA
                    //            :IND-CSD-RICORRENZA
                    //           ,:CSD-OBBLIGATORIETA
                    //            :IND-CSD-OBBLIGATORIETA
                    //           ,:CSD-VALORE-DEFAULT
                    //            :IND-CSD-VALORE-DEFAULT
                    //           ,:CSD-DS-UTENTE
                    //           END-EXEC
                    compStrDatoDao.fetchCur1(ws);
                    // COB_CODE: PERFORM C000-CONTROLLO-FIGLIO
                    c000ControlloFiglio();
                }
                break;

            case ((short)5):// COB_CODE: PERFORM UNTIL IDSO0021-NOT-FOUND
                //           OR NOT IDSO0021-SUCCESSFUL-RC
                //              PERFORM C000-CONTROLLO-FIGLIO
                //           END-PERFORM
                while (!(ws.getIdso0021().getIdso0021AreaErrori().getSqlcode().isNotFound() || !ws.getIdso0021().getIdso0021AreaErrori().getReturnCode().isSuccessfulRc())) {
                    // COB_CODE: EXEC SQL FETCH CUR5
                    //            INTO
                    //           :CSD-COD-COMPAGNIA-ANIA
                    //           ,:CSD-COD-STR-DATO
                    //           ,:CSD-POSIZIONE
                    //           ,:CSD-COD-STR-DATO2
                    //            :IND-CSD-COD-STR-DATO2
                    //           ,:CSD-COD-DATO
                    //            :IND-CSD-COD-DATO
                    //           ,:CSD-FLAG-KEY
                    //            :IND-CSD-FLAG-KEY
                    //           ,:CSD-FLAG-RETURN-CODE
                    //            :IND-CSD-FLAG-RETURN-CODE
                    //           ,:CSD-FLAG-CALL-USING
                    //            :IND-CSD-FLAG-CALL-USING
                    //           ,:CSD-FLAG-WHERE-COND
                    //            :IND-CSD-FLAG-WHERE-COND
                    //           ,:CSD-FLAG-REDEFINES
                    //            :IND-CSD-FLAG-REDEFINES
                    //           ,:CSD-TIPO-DATO
                    //            :IND-CSD-TIPO-DATO
                    //           ,:CSD-LUNGHEZZA-DATO
                    //            :IND-CSD-LUNGHEZZA-DATO
                    //           ,:CSD-PRECISIONE-DATO
                    //            :IND-CSD-PRECISIONE-DATO
                    //           ,:CSD-COD-DOMINIO
                    //            :IND-CSD-COD-DOMINIO
                    //           ,:CSD-FORMATTAZIONE-DATO
                    //            :IND-CSD-FORMATTAZIONE-DATO
                    //           ,:CSD-SERVIZIO-CONVERS
                    //            :IND-CSD-SERVIZIO-CONVERS
                    //           ,:CSD-AREA-CONV-STANDARD
                    //            :IND-CSD-AREA-CONV-STANDARD
                    //           ,:CSD-RICORRENZA
                    //            :IND-CSD-RICORRENZA
                    //           ,:CSD-OBBLIGATORIETA
                    //            :IND-CSD-OBBLIGATORIETA
                    //           ,:CSD-VALORE-DEFAULT
                    //            :IND-CSD-VALORE-DEFAULT
                    //           ,:CSD-DS-UTENTE
                    //           END-EXEC
                    compStrDatoDao.fetchCur1(ws);
                    // COB_CODE: PERFORM C000-CONTROLLO-FIGLIO
                    c000ControlloFiglio();
                }
                break;

            case ((short)6):// COB_CODE: PERFORM UNTIL IDSO0021-NOT-FOUND
                //           OR NOT IDSO0021-SUCCESSFUL-RC
                //              PERFORM C000-CONTROLLO-FIGLIO
                //           END-PERFORM
                while (!(ws.getIdso0021().getIdso0021AreaErrori().getSqlcode().isNotFound() || !ws.getIdso0021().getIdso0021AreaErrori().getReturnCode().isSuccessfulRc())) {
                    // COB_CODE: EXEC SQL FETCH CUR6
                    //           INTO
                    //           :CSD-COD-COMPAGNIA-ANIA
                    //           ,:CSD-COD-STR-DATO
                    //           ,:CSD-POSIZIONE
                    //           ,:CSD-COD-STR-DATO2
                    //            :IND-CSD-COD-STR-DATO2
                    //           ,:CSD-COD-DATO
                    //            :IND-CSD-COD-DATO
                    //           ,:CSD-FLAG-KEY
                    //            :IND-CSD-FLAG-KEY
                    //           ,:CSD-FLAG-RETURN-CODE
                    //            :IND-CSD-FLAG-RETURN-CODE
                    //           ,:CSD-FLAG-CALL-USING
                    //            :IND-CSD-FLAG-CALL-USING
                    //           ,:CSD-FLAG-WHERE-COND
                    //            :IND-CSD-FLAG-WHERE-COND
                    //           ,:CSD-FLAG-REDEFINES
                    //            :IND-CSD-FLAG-REDEFINES
                    //           ,:CSD-TIPO-DATO
                    //            :IND-CSD-TIPO-DATO
                    //           ,:CSD-LUNGHEZZA-DATO
                    //            :IND-CSD-LUNGHEZZA-DATO
                    //           ,:CSD-PRECISIONE-DATO
                    //            :IND-CSD-PRECISIONE-DATO
                    //           ,:CSD-COD-DOMINIO
                    //            :IND-CSD-COD-DOMINIO
                    //           ,:CSD-FORMATTAZIONE-DATO
                    //            :IND-CSD-FORMATTAZIONE-DATO
                    //           ,:CSD-SERVIZIO-CONVERS
                    //            :IND-CSD-SERVIZIO-CONVERS
                    //           ,:CSD-AREA-CONV-STANDARD
                    //            :IND-CSD-AREA-CONV-STANDARD
                    //           ,:CSD-RICORRENZA
                    //            :IND-CSD-RICORRENZA
                    //           ,:CSD-OBBLIGATORIETA
                    //            :IND-CSD-OBBLIGATORIETA
                    //           ,:CSD-VALORE-DEFAULT
                    //            :IND-CSD-VALORE-DEFAULT
                    //           ,:CSD-DS-UTENTE
                    //           END-EXEC
                    compStrDatoDao.fetchCur1(ws);
                    // COB_CODE: PERFORM C000-CONTROLLO-FIGLIO
                    c000ControlloFiglio();
                }
                break;

            case ((short)7):// COB_CODE: PERFORM UNTIL IDSO0021-NOT-FOUND
                //           OR NOT IDSO0021-SUCCESSFUL-RC
                //              PERFORM C000-CONTROLLO-FIGLIO
                //           END-PERFORM
                while (!(ws.getIdso0021().getIdso0021AreaErrori().getSqlcode().isNotFound() || !ws.getIdso0021().getIdso0021AreaErrori().getReturnCode().isSuccessfulRc())) {
                    // COB_CODE: EXEC SQL FETCH CUR7
                    //           INTO
                    //           :CSD-COD-COMPAGNIA-ANIA
                    //           ,:CSD-COD-STR-DATO
                    //           ,:CSD-POSIZIONE
                    //           ,:CSD-COD-STR-DATO2
                    //            :IND-CSD-COD-STR-DATO2
                    //           ,:CSD-COD-DATO
                    //            :IND-CSD-COD-DATO
                    //           ,:CSD-FLAG-KEY
                    //            :IND-CSD-FLAG-KEY
                    //           ,:CSD-FLAG-RETURN-CODE
                    //            :IND-CSD-FLAG-RETURN-CODE
                    //           ,:CSD-FLAG-CALL-USING
                    //            :IND-CSD-FLAG-CALL-USING
                    //           ,:CSD-FLAG-WHERE-COND
                    //            :IND-CSD-FLAG-WHERE-COND
                    //           ,:CSD-FLAG-REDEFINES
                    //            :IND-CSD-FLAG-REDEFINES
                    //           ,:CSD-TIPO-DATO
                    //            :IND-CSD-TIPO-DATO
                    //           ,:CSD-LUNGHEZZA-DATO
                    //            :IND-CSD-LUNGHEZZA-DATO
                    //           ,:CSD-PRECISIONE-DATO
                    //            :IND-CSD-PRECISIONE-DATO
                    //           ,:CSD-COD-DOMINIO
                    //            :IND-CSD-COD-DOMINIO
                    //           ,:CSD-FORMATTAZIONE-DATO
                    //            :IND-CSD-FORMATTAZIONE-DATO
                    //           ,:CSD-SERVIZIO-CONVERS
                    //            :IND-CSD-SERVIZIO-CONVERS
                    //           ,:CSD-AREA-CONV-STANDARD
                    //            :IND-CSD-AREA-CONV-STANDARD
                    //           ,:CSD-RICORRENZA
                    //            :IND-CSD-RICORRENZA
                    //           ,:CSD-OBBLIGATORIETA
                    //            :IND-CSD-OBBLIGATORIETA
                    //           ,:CSD-VALORE-DEFAULT
                    //            :IND-CSD-VALORE-DEFAULT
                    //           ,:CSD-DS-UTENTE
                    //           END-EXEC
                    compStrDatoDao.fetchCur1(ws);
                    // COB_CODE: PERFORM C000-CONTROLLO-FIGLIO
                    c000ControlloFiglio();
                }
                break;

            case ((short)8):// COB_CODE: PERFORM UNTIL IDSO0021-NOT-FOUND
                //           OR NOT IDSO0021-SUCCESSFUL-RC
                //              PERFORM C000-CONTROLLO-FIGLIO
                //           END-PERFORM
                while (!(ws.getIdso0021().getIdso0021AreaErrori().getSqlcode().isNotFound() || !ws.getIdso0021().getIdso0021AreaErrori().getReturnCode().isSuccessfulRc())) {
                    // COB_CODE: EXEC SQL FETCH CUR8
                    //           INTO
                    //           :CSD-COD-COMPAGNIA-ANIA
                    //           ,:CSD-COD-STR-DATO
                    //           ,:CSD-POSIZIONE
                    //           ,:CSD-COD-STR-DATO2
                    //            :IND-CSD-COD-STR-DATO2
                    //           ,:CSD-COD-DATO
                    //            :IND-CSD-COD-DATO
                    //           ,:CSD-FLAG-KEY
                    //            :IND-CSD-FLAG-KEY
                    //           ,:CSD-FLAG-RETURN-CODE
                    //            :IND-CSD-FLAG-RETURN-CODE
                    //           ,:CSD-FLAG-CALL-USING
                    //            :IND-CSD-FLAG-CALL-USING
                    //           ,:CSD-FLAG-WHERE-COND
                    //            :IND-CSD-FLAG-WHERE-COND
                    //           ,:CSD-FLAG-REDEFINES
                    //            :IND-CSD-FLAG-REDEFINES
                    //           ,:CSD-TIPO-DATO
                    //            :IND-CSD-TIPO-DATO
                    //           ,:CSD-LUNGHEZZA-DATO
                    //            :IND-CSD-LUNGHEZZA-DATO
                    //           ,:CSD-PRECISIONE-DATO
                    //            :IND-CSD-PRECISIONE-DATO
                    //           ,:CSD-COD-DOMINIO
                    //            :IND-CSD-COD-DOMINIO
                    //           ,:CSD-FORMATTAZIONE-DATO
                    //            :IND-CSD-FORMATTAZIONE-DATO
                    //           ,:CSD-SERVIZIO-CONVERS
                    //            :IND-CSD-SERVIZIO-CONVERS
                    //           ,:CSD-AREA-CONV-STANDARD
                    //            :IND-CSD-AREA-CONV-STANDARD
                    //           ,:CSD-RICORRENZA
                    //            :IND-CSD-RICORRENZA
                    //           ,:CSD-OBBLIGATORIETA
                    //            :IND-CSD-OBBLIGATORIETA
                    //           ,:CSD-VALORE-DEFAULT
                    //            :IND-CSD-VALORE-DEFAULT
                    //           ,:CSD-DS-UTENTE
                    //           END-EXEC
                    compStrDatoDao.fetchCur1(ws);
                    // COB_CODE: PERFORM C000-CONTROLLO-FIGLIO
                    c000ControlloFiglio();
                }
                break;

            case ((short)9):// COB_CODE: PERFORM UNTIL IDSO0021-NOT-FOUND
                //           OR NOT IDSO0021-SUCCESSFUL-RC
                //              PERFORM C000-CONTROLLO-FIGLIO
                //           END-PERFORM
                while (!(ws.getIdso0021().getIdso0021AreaErrori().getSqlcode().isNotFound() || !ws.getIdso0021().getIdso0021AreaErrori().getReturnCode().isSuccessfulRc())) {
                    // COB_CODE: EXEC SQL FETCH CUR9
                    //           INTO
                    //           :CSD-COD-COMPAGNIA-ANIA
                    //           ,:CSD-COD-STR-DATO
                    //           ,:CSD-POSIZIONE
                    //           ,:CSD-COD-STR-DATO2
                    //            :IND-CSD-COD-STR-DATO2
                    //           ,:CSD-COD-DATO
                    //            :IND-CSD-COD-DATO
                    //           ,:CSD-FLAG-KEY
                    //            :IND-CSD-FLAG-KEY
                    //           ,:CSD-FLAG-RETURN-CODE
                    //            :IND-CSD-FLAG-RETURN-CODE
                    //           ,:CSD-FLAG-CALL-USING
                    //            :IND-CSD-FLAG-CALL-USING
                    //           ,:CSD-FLAG-WHERE-COND
                    //            :IND-CSD-FLAG-WHERE-COND
                    //           ,:CSD-FLAG-REDEFINES
                    //            :IND-CSD-FLAG-REDEFINES
                    //           ,:CSD-TIPO-DATO
                    //            :IND-CSD-TIPO-DATO
                    //           ,:CSD-LUNGHEZZA-DATO
                    //            :IND-CSD-LUNGHEZZA-DATO
                    //           ,:CSD-PRECISIONE-DATO
                    //            :IND-CSD-PRECISIONE-DATO
                    //           ,:CSD-COD-DOMINIO
                    //            :IND-CSD-COD-DOMINIO
                    //           ,:CSD-FORMATTAZIONE-DATO
                    //            :IND-CSD-FORMATTAZIONE-DATO
                    //           ,:CSD-SERVIZIO-CONVERS
                    //            :IND-CSD-SERVIZIO-CONVERS
                    //           ,:CSD-AREA-CONV-STANDARD
                    //            :IND-CSD-AREA-CONV-STANDARD
                    //           ,:CSD-RICORRENZA
                    //            :IND-CSD-RICORRENZA
                    //           ,:CSD-OBBLIGATORIETA
                    //            :IND-CSD-OBBLIGATORIETA
                    //           ,:CSD-VALORE-DEFAULT
                    //            :IND-CSD-VALORE-DEFAULT
                    //           ,:CSD-DS-UTENTE
                    //           END-EXEC
                    compStrDatoDao.fetchCur1(ws);
                    // COB_CODE: PERFORM C000-CONTROLLO-FIGLIO
                    c000ControlloFiglio();
                }
                break;

            case ((short)10):// COB_CODE: PERFORM UNTIL IDSO0021-NOT-FOUND
                //           OR NOT IDSO0021-SUCCESSFUL-RC
                //              PERFORM C000-CONTROLLO-FIGLIO
                //           END-PERFORM
                while (!(ws.getIdso0021().getIdso0021AreaErrori().getSqlcode().isNotFound() || !ws.getIdso0021().getIdso0021AreaErrori().getReturnCode().isSuccessfulRc())) {
                    // COB_CODE: EXEC SQL FETCH CUR10
                    //           INTO
                    //           :CSD-COD-COMPAGNIA-ANIA
                    //           ,:CSD-COD-STR-DATO
                    //           ,:CSD-POSIZIONE
                    //           ,:CSD-COD-STR-DATO2
                    //            :IND-CSD-COD-STR-DATO2
                    //           ,:CSD-COD-DATO
                    //            :IND-CSD-COD-DATO
                    //           ,:CSD-FLAG-KEY
                    //            :IND-CSD-FLAG-KEY
                    //           ,:CSD-FLAG-RETURN-CODE
                    //            :IND-CSD-FLAG-RETURN-CODE
                    //           ,:CSD-FLAG-CALL-USING
                    //            :IND-CSD-FLAG-CALL-USING
                    //           ,:CSD-FLAG-WHERE-COND
                    //            :IND-CSD-FLAG-WHERE-COND
                    //           ,:CSD-FLAG-REDEFINES
                    //            :IND-CSD-FLAG-REDEFINES
                    //           ,:CSD-TIPO-DATO
                    //            :IND-CSD-TIPO-DATO
                    //           ,:CSD-LUNGHEZZA-DATO
                    //            :IND-CSD-LUNGHEZZA-DATO
                    //           ,:CSD-PRECISIONE-DATO
                    //            :IND-CSD-PRECISIONE-DATO
                    //           ,:CSD-COD-DOMINIO
                    //            :IND-CSD-COD-DOMINIO
                    //           ,:CSD-FORMATTAZIONE-DATO
                    //            :IND-CSD-FORMATTAZIONE-DATO
                    //           ,:CSD-SERVIZIO-CONVERS
                    //            :IND-CSD-SERVIZIO-CONVERS
                    //           ,:CSD-AREA-CONV-STANDARD
                    //            :IND-CSD-AREA-CONV-STANDARD
                    //           ,:CSD-RICORRENZA
                    //            :IND-CSD-RICORRENZA
                    //           ,:CSD-OBBLIGATORIETA
                    //            :IND-CSD-OBBLIGATORIETA
                    //           ,:CSD-VALORE-DEFAULT
                    //            :IND-CSD-VALORE-DEFAULT
                    //           ,:CSD-DS-UTENTE
                    //           END-EXEC
                    compStrDatoDao.fetchCur1(ws);
                    // COB_CODE: PERFORM C000-CONTROLLO-FIGLIO
                    c000ControlloFiglio();
                }
                break;

            default:break;
        }
    }

    /**Original name: A400-CLOSE_FIRST_SENTENCES<br>*/
    private void a400Close() {
        // COB_CODE: EVALUATE WK-NUMERO-LIVELLO
        //           WHEN 1
        //              CONTINUE
        //           WHEN 2
        //              CONTINUE
        //           WHEN 3
        //              CONTINUE
        //           WHEN 4
        //              CONTINUE
        //           WHEN 5
        //              CONTINUE
        //           WHEN 6
        //              CONTINUE
        //           WHEN 7
        //              CONTINUE
        //           WHEN 8
        //              CONTINUE
        //           WHEN 9
        //              CONTINUE
        //           WHEN 10
        //              CONTINUE
        //           END-EVALUATE.
        switch (ws.getWkNumeroLivello()) {

            case ((short)1):// COB_CODE: EXEC SQL CLOSE  CUR1  END-EXEC
                compStrDatoDao.closeCur1();
                // COB_CODE: CONTINUE
                //continue
                break;

            case ((short)2):// COB_CODE: EXEC SQL CLOSE  CUR2  END-EXEC
                compStrDatoDao.closeCur1();
                // COB_CODE: CONTINUE
                //continue
                break;

            case ((short)3):// COB_CODE: EXEC SQL CLOSE  CUR3  END-EXEC
                compStrDatoDao.closeCur1();
                // COB_CODE: CONTINUE
                //continue
                break;

            case ((short)4):// COB_CODE: EXEC SQL CLOSE  CUR4  END-EXEC
                compStrDatoDao.closeCur1();
                // COB_CODE: CONTINUE
                //continue
                break;

            case ((short)5):// COB_CODE: EXEC SQL CLOSE  CUR5  END-EXEC
                compStrDatoDao.closeCur1();
                // COB_CODE: CONTINUE
                //continue
                break;

            case ((short)6):// COB_CODE: EXEC SQL CLOSE  CUR6  END-EXEC
                compStrDatoDao.closeCur1();
                // COB_CODE: CONTINUE
                //continue
                break;

            case ((short)7):// COB_CODE: EXEC SQL CLOSE  CUR7  END-EXEC
                compStrDatoDao.closeCur1();
                // COB_CODE: CONTINUE
                //continue
                break;

            case ((short)8):// COB_CODE: EXEC SQL CLOSE  CUR8  END-EXEC
                compStrDatoDao.closeCur1();
                // COB_CODE: CONTINUE
                //continue
                break;

            case ((short)9):// COB_CODE: EXEC SQL CLOSE  CUR9  END-EXEC
                compStrDatoDao.closeCur1();
                // COB_CODE: CONTINUE
                //continue
                break;

            case ((short)10):// COB_CODE: EXEC SQL CLOSE  CUR10 END-EXEC
                compStrDatoDao.closeCur1();
                // COB_CODE: CONTINUE
                //continue
                break;

            default:break;
        }
        // COB_CODE: IF SQLCODE NOT = ZEROES
        //              PERFORM 2000-DBERROR
        //           END-IF.
        if (sqlca.getSqlcode() != 0) {
            // COB_CODE: MOVE SQLCODE         TO IDSO0021-SQLCODE
            ws.getIdso0021().getIdso0021AreaErrori().getSqlcode().setSqlcodeSigned(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2 TO IDSO0021-DESCRIZ-ERR-DB2
            ws.getIdso0021().getIdso0021AreaErrori().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: PERFORM 2000-DBERROR
            dberror();
        }
    }

    /**Original name: A500-OPERAZIONI-FINALI_FIRST_SENTENCES<br>*/
    private void a500OperazioniFinali() {
        // COB_CODE: SUBTRACT 1 FROM WK-NUMERO-LIVELLO.
        ws.setWkNumeroLivello(Trunc.toShort(abs(ws.getWkNumeroLivello() - 1), 3));
        // COB_CODE: PERFORM C300-CTRL-STRUTTURA-PADRE.
        c300CtrlStrutturaPadre();
    }

    /**Original name: C000-CONTROLLO-FIGLIO_FIRST_SENTENCES<br>*/
    private void c000ControlloFiglio() {
        // COB_CODE: MOVE SQLCODE         TO IDSO0021-SQLCODE
        ws.getIdso0021().getIdso0021AreaErrori().getSqlcode().setSqlcodeSigned(sqlca.getSqlcode());
        // COB_CODE: MOVE DESCRIZ-ERR-DB2 TO IDSO0021-DESCRIZ-ERR-DB2
        ws.getIdso0021().getIdso0021AreaErrori().setDescrizErrDb2(ws.getDescrizErrDb2());
        // COB_CODE: IF  NOT IDSO0021-SUCCESSFUL-SQL
        //           AND NOT IDSO0021-NOT-FOUND
        //              PERFORM A400-CLOSE
        //           END-IF.
        if (!ws.getIdso0021().getIdso0021AreaErrori().getSqlcode().isSuccessfulSql() && !ws.getIdso0021().getIdso0021AreaErrori().getSqlcode().isNotFound()) {
            // COB_CODE: PERFORM 2000-DBERROR
            dberror();
            // COB_CODE: PERFORM A400-CLOSE
            a400Close();
        }
        // COB_CODE: IF IDSO0021-SUCCESSFUL-RC AND
        //              IDSO0021-SUCCESSFUL-SQL
        //              END-IF
        //           END-IF.
        if (ws.getIdso0021().getIdso0021AreaErrori().getReturnCode().isSuccessfulRc() && ws.getIdso0021().getIdso0021AreaErrori().getSqlcode().isSuccessfulSql()) {
            // COB_CODE: MOVE 'S'                         TO WK-COUNTER
            ws.setWkCounterFormatted("S");
            // COB_CODE: PERFORM N000-CNTL-CAMPI-NULL-CSD
            n000CntlCampiNullCsd();
            // COB_CODE: PERFORM C050-VALORIZZA-BASE
            c050ValorizzaBase();
            // COB_CODE: IF CSD-COD-STR-DATO2 =
            //              SPACES OR LOW-VALUES OR HIGH-VALUES
            //              END-IF
            //           ELSE
            //              END-IF
            //           END-IF
            if (Characters.EQ_SPACE.test(ws.getCompStrDato().getCsdCodStrDato2()) || Characters.EQ_LOW.test(ws.getCompStrDato().getCsdCodStrDato2(), CompStrDato.Len.CSD_COD_STR_DATO2) || Characters.EQ_HIGH.test(ws.getCompStrDato().getCsdCodStrDato2(), CompStrDato.Len.CSD_COD_STR_DATO2)) {
                // COB_CODE: IF CSD-COD-DATO  = SPACES OR LOW-VALUES OR HIGH-VALUES
                //                                       TO IDSO0021-DESCRIZ-ERR-DB2
                //           ELSE
                //              PERFORM C100-CONTROLLO-COMPOSIZIONE
                //           END-IF
                if (Characters.EQ_SPACE.test(ws.getCompStrDato().getCsdCodDato()) || Characters.EQ_LOW.test(ws.getCompStrDato().getCsdCodDato(), CompStrDato.Len.CSD_COD_DATO) || Characters.EQ_HIGH.test(ws.getCompStrDato().getCsdCodDato(), CompStrDato.Len.CSD_COD_DATO)) {
                    // COB_CODE: SET IDSO0021-FIELD-NOT-VALUED TO TRUE
                    ws.getIdso0021().getIdso0021AreaErrori().getReturnCode().setFieldNotValued();
                    // COB_CODE: MOVE 'CODICE DATO NON VALORIZZATO'
                    //                                    TO IDSO0021-DESCRIZ-ERR-DB2
                    ws.getIdso0021().getIdso0021AreaErrori().setDescrizErrDb2("CODICE DATO NON VALORIZZATO");
                }
                else {
                    // COB_CODE: PERFORM C100-CONTROLLO-COMPOSIZIONE
                    c100ControlloComposizione();
                }
            }
            else {
                // COB_CODE: PERFORM C150-COMPOSIZIONE-PADRE
                c150ComposizionePadre();
                // COB_CODE: IF CSD-LUNGHEZZA-DATO IS NUMERIC AND
                //              CSD-LUNGHEZZA-DATO NOT = 0
                //              PERFORM C130-VALORIZZA-DATO-COMPLETO
                //           ELSE
                //              END-IF
                //           END-IF
                if (Functions.isNumber(ws.getCompStrDato().getCsdLunghezzaDato().getCsdLunghezzaDato()) && ws.getCompStrDato().getCsdLunghezzaDato().getCsdLunghezzaDato() != 0) {
                    // COB_CODE: PERFORM C130-VALORIZZA-DATO-COMPLETO
                    c130ValorizzaDatoCompleto();
                }
                else {
                    // COB_CODE: PERFORM R300-CNTL-RICORRENZA
                    r300CntlRicorrenza();
                    // COB_CODE: PERFORM R000-RICORSIONE
                    r000Ricorsione();
                    // COB_CODE: IF CAMPI-REDEFINES
                    //              END-IF
                    //           ELSE
                    //              MOVE 0  TO WK-LUNG-PADRE(WK-NUMERO-LIVELLO)
                    //           END-IF
                    if (ws.isFlagRedefines()) {
                        // COB_CODE: IF WK-NUMERO-RICORRENZE-RED
                        //                         (WK-NUMERO-LIVELLO) > 0
                        //                         WK-INIZIO-POSIZIONE-RIC
                        //           END-IF
                        if (ws.getWkElementiPadreRed(ws.getWkNumeroLivello()).getNumeroRicorrenzeRed() > 0) {
                            // COB_CODE: MOVE 0  TO WK-NUMERO-RICORRENZE-RED
                            //                        (WK-NUMERO-LIVELLO)
                            //                      WK-NUMERO-RICORRENZA
                            //                      WK-INIZIO-POSIZIONE-RIC
                            ws.getWkElementiPadreRed(ws.getWkNumeroLivello()).setNumeroRicorrenzeRed(0);
                            ws.setWkNumeroRicorrenza(0);
                            ws.setWkInizioPosizioneRic(0);
                        }
                        // COB_CODE: MOVE WK-INDICE-PADRE-RED(WK-NUMERO-LIVELLO)
                        //                TO COMODO-INDICE-PADRE
                        ws.setComodoIndicePadre(ws.getWkElementiPadreRed(ws.getWkNumeroLivello()).getIndicePadreRed());
                        // COB_CODE: MOVE WK-LUNG-PADRE-RED  (WK-NUMERO-LIVELLO)
                        //                                TO IDSO0021-LUNGHEZZA-DATO
                        //                                   (COMODO-INDICE-PADRE)
                        ws.getIdso0021().getIdso0021StrutturaDato().setLunghezzaDato(ws.getComodoIndicePadre(), ws.getWkElementiPadreRed(ws.getWkNumeroLivello()).getLungPadreRed());
                        // COB_CODE: MOVE 0 TO WK-LUNG-PADRE-RED(WK-NUMERO-LIVELLO)
                        ws.getWkElementiPadreRed(ws.getWkNumeroLivello()).setLungPadreRed(0);
                        // COB_CODE: IF WK-NUMERO-LIVELLO =  WK-NUMERO-LIVELLO-RED
                        //               SET CAMPI-NON-REDEFINES TO TRUE
                        //           END-IF
                        if (ws.getWkNumeroLivello() == ws.getWkNumeroLivelloRed()) {
                            // COB_CODE: SET CAMPI-NON-REDEFINES TO TRUE
                            ws.setFlagRedefines(false);
                        }
                    }
                    else {
                        // COB_CODE: IF WK-NUMERO-RICORRENZE(WK-NUMERO-LIVELLO) > 0
                        //                   WK-INIZIO-POSIZIONE-RIC
                        //           END-IF
                        if (ws.getWkElementiPadre(ws.getWkNumeroLivello()).getNumeroRicorrenze() > 0) {
                            // COB_CODE: ADD  WK-INIZIO-POSIZIONE-RIC
                            //                                 TO WK-INIZIO-POSIZIONE
                            ws.setWkInizioPosizione(Trunc.toInt(ws.getWkInizioPosizioneRic() + ws.getWkInizioPosizione(), 5));
                            // COB_CODE: MOVE 0
                            //             TO WK-NUMERO-RICORRENZE (WK-NUMERO-LIVELLO)
                            //                WK-NUMERO-RICORRENZA
                            //                WK-INIZIO-POSIZIONE-RIC
                            ws.getWkElementiPadre(ws.getWkNumeroLivello()).setNumeroRicorrenze(0);
                            ws.setWkNumeroRicorrenza(0);
                            ws.setWkInizioPosizioneRic(0);
                        }
                        // COB_CODE: MOVE WK-INDICE-PADRE(WK-NUMERO-LIVELLO)
                        //                TO COMODO-INDICE-PADRE
                        ws.setComodoIndicePadre(ws.getWkElementiPadre(ws.getWkNumeroLivello()).getIndicePadre());
                        // COB_CODE: MOVE WK-LUNG-PADRE(WK-NUMERO-LIVELLO)
                        //                TO IDSO0021-LUNGHEZZA-DATO
                        //             (COMODO-INDICE-PADRE)
                        ws.getIdso0021().getIdso0021StrutturaDato().setLunghezzaDato(ws.getComodoIndicePadre(), ws.getWkElementiPadre(ws.getWkNumeroLivello()).getLungPadre());
                        // COB_CODE: MOVE 0  TO WK-LUNG-PADRE(WK-NUMERO-LIVELLO)
                        ws.getWkElementiPadre(ws.getWkNumeroLivello()).setLungPadre(0);
                    }
                }
            }
            // COB_CODE: IF WK-COUNTER = 'N'
            //              PERFORM 2000-DBERROR
            //           END-IF
            if (ws.getWkCounter() == 'N') {
                // COB_CODE: MOVE 'NESSUN CAMPO DI STRUTTURA TROVATO'
                //                       TO IDSO0021-DESCRIZ-ERR-DB2
                ws.getIdso0021().getIdso0021AreaErrori().setDescrizErrDb2("NESSUN CAMPO DI STRUTTURA TROVATO");
                // COB_CODE: PERFORM 2000-DBERROR
                dberror();
            }
            // COB_CODE: IF IDSO0021-SUCCESSFUL-RC AND
            //              IDSO0021-SUCCESSFUL-SQL
            //              MOVE SPACES              TO COMP-STR-DATO
            //           END-IF
            if (ws.getIdso0021().getIdso0021AreaErrori().getReturnCode().isSuccessfulRc() && ws.getIdso0021().getIdso0021AreaErrori().getSqlcode().isSuccessfulSql()) {
                // COB_CODE: PERFORM R600-CNTL-REDEFINES
                r600CntlRedefines();
                // COB_CODE: MOVE SPACES              TO COMP-STR-DATO
                ws.getCompStrDato().initCompStrDatoSpaces();
            }
        }
    }

    /**Original name: C050-VALORIZZA-BASE_FIRST_SENTENCES<br>*/
    private void c050ValorizzaBase() {
        // COB_CODE: MOVE SPACES                 TO WK-CTRL-LUNG-CAMPO-TIPO
        ws.getConversionVariables().setCtrlLungCampoTipo("");
        // COB_CODE: MOVE 0                      TO WK-CTRL-LUNG-CAMPO-LUNG
        ws.getConversionVariables().setCtrlLungCampoLung(0);
        // COB_CODE: ADD 1                       TO INDICE-ANAGR-DATO
        ws.setIndiceAnagrDato(Trunc.toShort(1 + ws.getIndiceAnagrDato(), 3));
        // COB_CODE: MOVE CSD-FLAG-KEY           TO IDSO0021-FLAG-KEY
        //                                          (INDICE-ANAGR-DATO)
        ws.getIdso0021().getIdso0021StrutturaDato().setFlagKey(ws.getIndiceAnagrDato(), ws.getCompStrDato().getCsdFlagKey());
        // COB_CODE: MOVE CSD-FLAG-RETURN-CODE
        //                                       TO IDSO0021-FLAG-RETURN-CODE
        //                                          (INDICE-ANAGR-DATO)
        ws.getIdso0021().getIdso0021StrutturaDato().setFlagReturnCode(ws.getIndiceAnagrDato(), ws.getCompStrDato().getCsdFlagReturnCode());
        // COB_CODE: MOVE CSD-FLAG-CALL-USING    TO IDSO0021-FLAG-CALL-USING
        //                                          (INDICE-ANAGR-DATO)
        ws.getIdso0021().getIdso0021StrutturaDato().setFlagCallUsing(ws.getIndiceAnagrDato(), ws.getCompStrDato().getCsdFlagCallUsing());
        // COB_CODE: MOVE CSD-FLAG-WHERE-COND    TO IDSO0021-FLAG-WHERE-COND
        //                                          (INDICE-ANAGR-DATO)
        ws.getIdso0021().getIdso0021StrutturaDato().setFlagWhereCond(ws.getIndiceAnagrDato(), ws.getCompStrDato().getCsdFlagWhereCond());
        // COB_CODE: MOVE CSD-FLAG-REDEFINES     TO IDSO0021-FLAG-REDEFINES
        //                                          (INDICE-ANAGR-DATO)
        ws.getIdso0021().getIdso0021StrutturaDato().setFlagRedefines(ws.getIndiceAnagrDato(), ws.getCompStrDato().getCsdFlagRedefines());
        // COB_CODE: MOVE CSD-SERVIZIO-CONVERS   TO IDSO0021-SERV-CONVERSIONE
        //                                          (INDICE-ANAGR-DATO)
        ws.getIdso0021().getIdso0021StrutturaDato().setServConversione(ws.getIndiceAnagrDato(), ws.getCompStrDato().getCsdServizioConvers());
        // COB_CODE: MOVE CSD-AREA-CONV-STANDARD TO IDSO0021-AREA-CONV-STANDARD
        //                                          (INDICE-ANAGR-DATO)
        ws.getIdso0021().getIdso0021StrutturaDato().setAreaConvStandard(ws.getIndiceAnagrDato(), ws.getCompStrDato().getCsdAreaConvStandard());
        // COB_CODE: IF CSD-RICORRENZA-NULL NOT = HIGH-VALUE
        //                                          (INDICE-ANAGR-DATO)
        //           END-IF
        if (!Characters.EQ_HIGH.test(ws.getCompStrDato().getCsdRicorrenza().getCsdRicorrenzaNullFormatted())) {
            // COB_CODE: MOVE CSD-RICORRENZA      TO IDSO0021-RICORRENZA
            //                                       (INDICE-ANAGR-DATO)
            ws.getIdso0021().getIdso0021StrutturaDato().setRicorrenza(ws.getIndiceAnagrDato(), ws.getCompStrDato().getCsdRicorrenza().getCsdRicorrenza());
        }
        // COB_CODE: IF WK-NUMERO-RICORRENZA > 0
        //                                          (INDICE-ANAGR-DATO)
        //           END-IF
        if (ws.getWkNumeroRicorrenza() > 0) {
            // COB_CODE: MOVE WK-CAMPO-ATTIVO     TO IDSO0021-CLONE
            //                                       (INDICE-ANAGR-DATO)
            ws.getIdso0021().getIdso0021StrutturaDato().setClone(ws.getIndiceAnagrDato(), ws.getWkCampoAttivo());
        }
        // COB_CODE: MOVE CSD-OBBLIGATORIETA     TO IDSO0021-OBBLIGATORIETA
        //                                          (INDICE-ANAGR-DATO)
        ws.getIdso0021().getIdso0021StrutturaDato().setObbligatorieta(ws.getIndiceAnagrDato(), ws.getCompStrDato().getCsdObbligatorieta());
        // COB_CODE: MOVE CSD-VALORE-DEFAULT     TO IDSO0021-VALORE-DEFAULT
        //                                          (INDICE-ANAGR-DATO).
        ws.getIdso0021().getIdso0021StrutturaDato().setValoreDefault(ws.getIndiceAnagrDato(), ws.getCompStrDato().getCsdValoreDefault());
    }

    /**Original name: C100-CONTROLLO-COMPOSIZIONE_FIRST_SENTENCES<br>*/
    private void c100ControlloComposizione() {
        // COB_CODE: IF CSD-TIPO-DATO  = SPACES OR LOW-VALUES OR HIGH-VALUES
        //              END-IF
        //           ELSE
        //              PERFORM C130-VALORIZZA-DATO-COMPLETO
        //           END-IF.
        if (Characters.EQ_SPACE.test(ws.getCompStrDato().getCsdTipoDato()) || Characters.EQ_LOW.test(ws.getCompStrDato().getCsdTipoDatoFormatted()) || Characters.EQ_HIGH.test(ws.getCompStrDato().getCsdTipoDatoFormatted())) {
            // COB_CODE: MOVE CSD-COD-DATO              TO ADA-COD-DATO
            ws.getAnagDato().setAdaCodDato(ws.getCompStrDato().getCsdCodDato());
            // COB_CODE: MOVE IDSI0021-CODICE-COMPAGNIA-ANIA
            //                                          TO ADA-COD-COMPAGNIA-ANIA
            ws.getAnagDato().setAdaCodCompagniaAnia(ws.getIdsi0021Area().getCodiceCompagniaAnia());
            // COB_CODE: PERFORM R100-ACCEDI-ANAGR-DATO
            r100AccediAnagrDato();
            // COB_CODE: IF IDSO0021-SUCCESSFUL-RC
            //                                          (INDICE-ANAGR-DATO)
            //           END-IF
            if (ws.getIdso0021().getIdso0021AreaErrori().getReturnCode().isSuccessfulRc()) {
                // COB_CODE: MOVE ADA-COD-DATO           TO IDSO0021-CODICE-DATO
                //                                      (INDICE-ANAGR-DATO)
                ws.getIdso0021().getIdso0021StrutturaDato().setCodiceDato(ws.getIndiceAnagrDato(), ws.getAnagDato().getAdaCodDato());
                // COB_CODE: IF CAMPI-REDEFINES
                //                                      (INDICE-ANAGR-DATO)
                //           ELSE
                //                                      (INDICE-ANAGR-DATO)
                //           END-IF
                if (ws.isFlagRedefines()) {
                    // COB_CODE: MOVE WK-INIZIO-POSIZIONE-RED
                    //                TO IDSO0021-INIZIO-POSIZIONE
                    //                                   (INDICE-ANAGR-DATO)
                    ws.getIdso0021().getIdso0021StrutturaDato().setInizioPosizione(ws.getIndiceAnagrDato(), ws.getWkInizioPosizioneRed());
                }
                else {
                    // COB_CODE: MOVE WK-INIZIO-POSIZIONE
                    //                TO IDSO0021-INIZIO-POSIZIONE
                    //                                   (INDICE-ANAGR-DATO)
                    ws.getIdso0021().getIdso0021StrutturaDato().setInizioPosizione(ws.getIndiceAnagrDato(), ws.getWkInizioPosizione());
                }
                // COB_CODE: MOVE ADA-TIPO-DATO       TO IDSO0021-TIPO-DATO
                //                                       (INDICE-ANAGR-DATO)
                //                                       WK-CTRL-LUNG-CAMPO-TIPO
                ws.getIdso0021().getIdso0021StrutturaDato().setTipoDato(ws.getIndiceAnagrDato(), ws.getAnagDato().getAdaTipoDato());
                ws.getConversionVariables().setCtrlLungCampoTipo(ws.getAnagDato().getAdaTipoDato());
                // COB_CODE: MOVE ADA-LUNGHEZZA-DATO  TO WK-CTRL-LUNG-CAMPO-LUNG
                //                                       IDSO0021-LUNGHEZZA-DATO-NOM
                //                                       (INDICE-ANAGR-DATO)
                ws.getConversionVariables().setCtrlLungCampoLung(ws.getAnagDato().getAdaLunghezzaDato().getAdaLunghezzaDato());
                ws.getIdso0021().getIdso0021StrutturaDato().setLunghezzaDatoNom(ws.getIndiceAnagrDato(), ws.getAnagDato().getAdaLunghezzaDato().getAdaLunghezzaDato());
                // COB_CODE: IF WK-CTRL-LUNG-CAMPO-TIPO =
                //              WK-CAMPO-TIPO-COMP  OR WK-CAMPO-TIPO-COMP1 OR
                //              WK-CAMPO-TIPO-COMP2 OR WK-CAMPO-TIPO-COMP3 OR
                //              WK-CAMPO-TIPO-COMP4 OR WK-CAMPO-TIPO-COMP5
                //              PERFORM R200-CTRL-LUNGHEZZA-CAMPO
                //           END-IF
                if (Conditions.eq(ws.getConversionVariables().getCtrlLungCampoTipo(), ws.getConversionVariables().getCampoTipoComp()) || Conditions.eq(ws.getConversionVariables().getCtrlLungCampoTipo(), ws.getConversionVariables().getCampoTipoComp1()) || Conditions.eq(ws.getConversionVariables().getCtrlLungCampoTipo(), ws.getConversionVariables().getCampoTipoComp2()) || Conditions.eq(ws.getConversionVariables().getCtrlLungCampoTipo(), ws.getConversionVariables().getCampoTipoComp3()) || Conditions.eq(ws.getConversionVariables().getCtrlLungCampoTipo(), ws.getConversionVariables().getCampoTipoComp4()) || Conditions.eq(ws.getConversionVariables().getCtrlLungCampoTipo(), ws.getConversionVariables().getCampoTipoComp5())) {
                    // COB_CODE: PERFORM R200-CTRL-LUNGHEZZA-CAMPO
                    r200CtrlLunghezzaCampo();
                }
                // COB_CODE: MOVE WK-CTRL-LUNG-CAMPO-LUNG
                //                                    TO IDSO0021-LUNGHEZZA-DATO
                //                                       (INDICE-ANAGR-DATO)
                ws.getIdso0021().getIdso0021StrutturaDato().setLunghezzaDato(ws.getIndiceAnagrDato(), ws.getConversionVariables().getCtrlLungCampoLung());
                // COB_CODE: PERFORM C200-INCREMENTA-LUNGHEZZA
                c200IncrementaLunghezza();
                // COB_CODE: IF CAMPI-REDEFINES
                //                                    TO WK-INIZIO-POSIZIONE-RED
                //           ELSE
                //                                    TO WK-INIZIO-POSIZIONE
                //           END-IF
                if (ws.isFlagRedefines()) {
                    // COB_CODE: ADD  WK-CTRL-LUNG-CAMPO-LUNG
                    //                                 TO WK-INIZIO-POSIZIONE-RED
                    ws.setWkInizioPosizioneRed(Trunc.toInt(ws.getConversionVariables().getCtrlLungCampoLung() + ws.getWkInizioPosizioneRed(), 5));
                }
                else {
                    // COB_CODE: MOVE WK-INIZIO-POSIZIONE
                    //                                 TO WK-INIZIO-POSIZIONE-RED
                    ws.setWkInizioPosizioneRed(ws.getWkInizioPosizione());
                    // COB_CODE: ADD  WK-CTRL-LUNG-CAMPO-LUNG
                    //                                 TO WK-INIZIO-POSIZIONE
                    ws.setWkInizioPosizione(Trunc.toInt(ws.getConversionVariables().getCtrlLungCampoLung() + ws.getWkInizioPosizione(), 5));
                }
                // COB_CODE: MOVE ADA-PRECISIONE-DATO TO IDSO0021-PRECISIONE-DATO
                //                                       (INDICE-ANAGR-DATO)
                ws.getIdso0021().getIdso0021StrutturaDato().setPrecisioneDato(ws.getIndiceAnagrDato(), ws.getAnagDato().getAdaPrecisioneDato().getAdaPrecisioneDato());
                // COB_CODE: MOVE ADA-FORMATTAZIONE-DATO
                //                                    TO IDSO0021-FORMATTAZIONE-DATO
                //                                       (INDICE-ANAGR-DATO)
                ws.getIdso0021().getIdso0021StrutturaDato().setFormattazioneDato(ws.getIndiceAnagrDato(), ws.getAnagDato().getAdaFormattazioneDato());
                // COB_CODE: MOVE ADA-COD-DOMINIO     TO IDSO0021-CODICE-DOMINIO
                //                                       (INDICE-ANAGR-DATO)
                ws.getIdso0021().getIdso0021StrutturaDato().setCodiceDominio(ws.getIndiceAnagrDato(), ws.getAnagDato().getAdaCodDominio());
            }
        }
        else {
            // COB_CODE: PERFORM C130-VALORIZZA-DATO-COMPLETO
            c130ValorizzaDatoCompleto();
        }
    }

    /**Original name: C130-VALORIZZA-DATO-COMPLETO_FIRST_SENTENCES<br>*/
    private void c130ValorizzaDatoCompleto() {
        // COB_CODE: IF CSD-COD-STR-DATO2 =
        //              SPACES OR LOW-VALUES OR HIGH-VALUES
        //                                         (INDICE-ANAGR-DATO)
        //           ELSE
        //                                          (INDICE-ANAGR-DATO)
        //           END-IF
        if (Characters.EQ_SPACE.test(ws.getCompStrDato().getCsdCodStrDato2()) || Characters.EQ_LOW.test(ws.getCompStrDato().getCsdCodStrDato2(), CompStrDato.Len.CSD_COD_STR_DATO2) || Characters.EQ_HIGH.test(ws.getCompStrDato().getCsdCodStrDato2(), CompStrDato.Len.CSD_COD_STR_DATO2)) {
            // COB_CODE: MOVE CSD-COD-DATO        TO IDSO0021-CODICE-DATO
            //                                      (INDICE-ANAGR-DATO)
            ws.getIdso0021().getIdso0021StrutturaDato().setCodiceDato(ws.getIndiceAnagrDato(), ws.getCompStrDato().getCsdCodDato());
        }
        else {
            // COB_CODE: MOVE CSD-COD-STR-DATO2   TO IDSO0021-CODICE-DATO
            //                                       (INDICE-ANAGR-DATO)
            ws.getIdso0021().getIdso0021StrutturaDato().setCodiceDato(ws.getIndiceAnagrDato(), ws.getCompStrDato().getCsdCodStrDato2());
        }
        // COB_CODE: MOVE CSD-TIPO-DATO          TO IDSO0021-TIPO-DATO
        //                                          (INDICE-ANAGR-DATO)
        //                                          WK-CTRL-LUNG-CAMPO-TIPO
        ws.getIdso0021().getIdso0021StrutturaDato().setTipoDato(ws.getIndiceAnagrDato(), ws.getCompStrDato().getCsdTipoDato());
        ws.getConversionVariables().setCtrlLungCampoTipo(ws.getCompStrDato().getCsdTipoDato());
        // COB_CODE: IF CAMPI-REDEFINES
        //                TO IDSO0021-INIZIO-POSIZIONE(INDICE-ANAGR-DATO)
        //           ELSE
        //                TO IDSO0021-INIZIO-POSIZIONE(INDICE-ANAGR-DATO)
        //           END-IF
        if (ws.isFlagRedefines()) {
            // COB_CODE: MOVE WK-INIZIO-POSIZIONE-RED
            //             TO IDSO0021-INIZIO-POSIZIONE(INDICE-ANAGR-DATO)
            ws.getIdso0021().getIdso0021StrutturaDato().setInizioPosizione(ws.getIndiceAnagrDato(), ws.getWkInizioPosizioneRed());
        }
        else {
            // COB_CODE: MOVE WK-INIZIO-POSIZIONE
            //             TO IDSO0021-INIZIO-POSIZIONE(INDICE-ANAGR-DATO)
            ws.getIdso0021().getIdso0021StrutturaDato().setInizioPosizione(ws.getIndiceAnagrDato(), ws.getWkInizioPosizione());
        }
        // COB_CODE: MOVE CSD-LUNGHEZZA-DATO
        //                TO WK-CTRL-LUNG-CAMPO-LUNG
        //                   IDSO0021-LUNGHEZZA-DATO-NOM
        //                    (INDICE-ANAGR-DATO)
        ws.getConversionVariables().setCtrlLungCampoLung(ws.getCompStrDato().getCsdLunghezzaDato().getCsdLunghezzaDato());
        ws.getIdso0021().getIdso0021StrutturaDato().setLunghezzaDatoNom(ws.getIndiceAnagrDato(), ws.getCompStrDato().getCsdLunghezzaDato().getCsdLunghezzaDato());
        // COB_CODE: IF WK-CTRL-LUNG-CAMPO-TIPO =
        //              WK-CAMPO-TIPO-COMP  OR WK-CAMPO-TIPO-COMP1 OR
        //              WK-CAMPO-TIPO-COMP2 OR WK-CAMPO-TIPO-COMP3 OR
        //              WK-CAMPO-TIPO-COMP4 OR WK-CAMPO-TIPO-COMP5
        //              PERFORM R200-CTRL-LUNGHEZZA-CAMPO
        //           END-IF
        if (Conditions.eq(ws.getConversionVariables().getCtrlLungCampoTipo(), ws.getConversionVariables().getCampoTipoComp()) || Conditions.eq(ws.getConversionVariables().getCtrlLungCampoTipo(), ws.getConversionVariables().getCampoTipoComp1()) || Conditions.eq(ws.getConversionVariables().getCtrlLungCampoTipo(), ws.getConversionVariables().getCampoTipoComp2()) || Conditions.eq(ws.getConversionVariables().getCtrlLungCampoTipo(), ws.getConversionVariables().getCampoTipoComp3()) || Conditions.eq(ws.getConversionVariables().getCtrlLungCampoTipo(), ws.getConversionVariables().getCampoTipoComp4()) || Conditions.eq(ws.getConversionVariables().getCtrlLungCampoTipo(), ws.getConversionVariables().getCampoTipoComp5())) {
            // COB_CODE: PERFORM R200-CTRL-LUNGHEZZA-CAMPO
            r200CtrlLunghezzaCampo();
        }
        // COB_CODE: MOVE WK-CTRL-LUNG-CAMPO-LUNG
        //                TO IDSO0021-LUNGHEZZA-DATO
        //                    (INDICE-ANAGR-DATO)
        ws.getIdso0021().getIdso0021StrutturaDato().setLunghezzaDato(ws.getIndiceAnagrDato(), ws.getConversionVariables().getCtrlLungCampoLung());
        // COB_CODE: PERFORM C200-INCREMENTA-LUNGHEZZA
        c200IncrementaLunghezza();
        // COB_CODE: IF CAMPI-REDEFINES
        //              ADD WK-CTRL-LUNG-CAMPO-LUNG TO WK-INIZIO-POSIZIONE-RED
        //           ELSE
        //              ADD WK-CTRL-LUNG-CAMPO-LUNG TO WK-INIZIO-POSIZIONE
        //           END-IF
        if (ws.isFlagRedefines()) {
            // COB_CODE: ADD WK-CTRL-LUNG-CAMPO-LUNG TO WK-INIZIO-POSIZIONE-RED
            ws.setWkInizioPosizioneRed(Trunc.toInt(ws.getConversionVariables().getCtrlLungCampoLung() + ws.getWkInizioPosizioneRed(), 5));
        }
        else {
            // COB_CODE: MOVE WK-INIZIO-POSIZIONE   TO WK-INIZIO-POSIZIONE-RED
            ws.setWkInizioPosizioneRed(ws.getWkInizioPosizione());
            // COB_CODE: ADD WK-CTRL-LUNG-CAMPO-LUNG TO WK-INIZIO-POSIZIONE
            ws.setWkInizioPosizione(Trunc.toInt(ws.getConversionVariables().getCtrlLungCampoLung() + ws.getWkInizioPosizione(), 5));
        }
        // COB_CODE: IF CSD-PRECISIONE-DATO-NULL NOT = HIGH-VALUES
        //                   TO IDSO0021-PRECISIONE-DATO(INDICE-ANAGR-DATO)
        //           END-IF.
        if (!Characters.EQ_HIGH.test(ws.getCompStrDato().getCsdPrecisioneDato().getCsdPrecisioneDatoNullFormatted())) {
            // COB_CODE: MOVE CSD-PRECISIONE-DATO
            //                TO IDSO0021-PRECISIONE-DATO(INDICE-ANAGR-DATO)
            ws.getIdso0021().getIdso0021StrutturaDato().setPrecisioneDato(ws.getIndiceAnagrDato(), ws.getCompStrDato().getCsdPrecisioneDato().getCsdPrecisioneDato());
        }
    }

    /**Original name: C150-COMPOSIZIONE-PADRE_FIRST_SENTENCES<br>*/
    private void c150ComposizionePadre() {
        // COB_CODE: MOVE CSD-COD-STR-DATO2
        //                   TO IDSO0021-CODICE-DATO(INDICE-ANAGR-DATO)
        ws.getIdso0021().getIdso0021StrutturaDato().setCodiceDato(ws.getIndiceAnagrDato(), ws.getCompStrDato().getCsdCodStrDato2());
        // COB_CODE: IF CAMPI-REDEFINES
        //                      WK-CTRL-LUNG-CAMPO-TIPO
        //           ELSE
        //                      WK-CTRL-LUNG-CAMPO-TIPO
        //           END-IF.
        if (ws.isFlagRedefines()) {
            // COB_CODE: MOVE WK-INIZIO-POSIZIONE-RED
            //                TO IDSO0021-INIZIO-POSIZIONE(INDICE-ANAGR-DATO)
            ws.getIdso0021().getIdso0021StrutturaDato().setInizioPosizione(ws.getIndiceAnagrDato(), ws.getWkInizioPosizioneRed());
            // COB_CODE: MOVE HIGH-VALUES
            //                TO IDSO0021-TIPO-DATO(INDICE-ANAGR-DATO)
            //                   WK-CTRL-LUNG-CAMPO-TIPO
            ws.getIdso0021().getIdso0021StrutturaDato().setTipoDato(ws.getIndiceAnagrDato(), LiteralGenerator.create(Types.HIGH_CHAR_VAL, Idso0021StrutturaDato.Len.TIPO_DATO));
            ws.getConversionVariables().setCtrlLungCampoTipo(LiteralGenerator.create(Types.HIGH_CHAR_VAL, ConversionVariables.Len.CTRL_LUNG_CAMPO_TIPO));
        }
        else {
            // COB_CODE: MOVE WK-INIZIO-POSIZIONE
            //                TO IDSO0021-INIZIO-POSIZIONE(INDICE-ANAGR-DATO)
            ws.getIdso0021().getIdso0021StrutturaDato().setInizioPosizione(ws.getIndiceAnagrDato(), ws.getWkInizioPosizione());
            // COB_CODE: MOVE CSD-TIPO-DATO
            //                TO IDSO0021-TIPO-DATO(INDICE-ANAGR-DATO)
            //                   WK-CTRL-LUNG-CAMPO-TIPO
            ws.getIdso0021().getIdso0021StrutturaDato().setTipoDato(ws.getIndiceAnagrDato(), ws.getCompStrDato().getCsdTipoDato());
            ws.getConversionVariables().setCtrlLungCampoTipo(ws.getCompStrDato().getCsdTipoDato());
        }
    }

    /**Original name: C180-COMPOSIZIONE-RDS_FIRST_SENTENCES<br>*/
    private void c180ComposizioneRds() {
        // COB_CODE: MOVE CSD-COD-COMPAGNIA-ANIA  TO RDS-COD-COMPAGNIA-ANIA
        ws.getRidefDatoStr().setCodCompagniaAnia(ws.getCompStrDato().getCsdCodCompagniaAnia());
        // COB_CODE: MOVE IDSI0021-TIPO-MOVIMENTO TO RDS-TIPO-MOVIMENTO
        ws.getRidefDatoStr().setTipoMovimento(ws.getIdsi0021Area().getTipoMovimento());
        // COB_CODE: MOVE CSD-COD-DATO            TO RDS-COD-DATO.
        ws.getRidefDatoStr().setCodDato(ws.getCompStrDato().getCsdCodDato());
    }

    /**Original name: C200-INCREMENTA-LUNGHEZZA_FIRST_SENTENCES<br>*/
    private void c200IncrementaLunghezza() {
        // COB_CODE: IF WK-NUMERO-RICORRENZA > 0
        //                      TO WK-INIZIO-POSIZIONE-RIC
        //           ELSE
        //                      TO WK-CTRL-LUNG-CAMPO-LUNG-EFF
        //           END-IF
        if (ws.getWkNumeroRicorrenza() > 0) {
            // COB_CODE: MOVE 0   TO WK-CTRL-LUNG-CAMPO-LUNG-EFF
            //                       WK-INIZIO-POSIZIONE-MOL
            ws.getConversionVariables().setCtrlLungCampoLungEff(0);
            ws.setWkInizioPosizioneMol(0);
            // COB_CODE: COMPUTE WK-CTRL-LUNG-CAMPO-LUNG-EFF  =
            //                   WK-CTRL-LUNG-CAMPO-LUNG *
            //                   WK-NUMERO-RICORRENZA
            ws.getConversionVariables().setCtrlLungCampoLungEff(Trunc.toInt(ws.getConversionVariables().getCtrlLungCampoLung() * ws.getWkNumeroRicorrenza(), 5));
            // COB_CODE: COMPUTE WK-INIZIO-POSIZIONE-MOL =
            //                   WK-CTRL-LUNG-CAMPO-LUNG *
            //                  (WK-NUMERO-RICORRENZA - 1)
            ws.setWkInizioPosizioneMol(Trunc.toInt(ws.getConversionVariables().getCtrlLungCampoLung() * (ws.getWkNumeroRicorrenza() - 1), 5));
            // COB_CODE: ADD     WK-INIZIO-POSIZIONE-MOL
            //                   TO WK-INIZIO-POSIZIONE-RIC
            ws.setWkInizioPosizioneRic(Trunc.toInt(ws.getWkInizioPosizioneMol() + ws.getWkInizioPosizioneRic(), 5));
        }
        else {
            // COB_CODE: MOVE WK-CTRL-LUNG-CAMPO-LUNG
            //                   TO WK-CTRL-LUNG-CAMPO-LUNG-EFF
            ws.getConversionVariables().setCtrlLungCampoLungEff(ws.getConversionVariables().getCtrlLungCampoLung());
        }
        // COB_CODE: PERFORM VARYING INDEX-PADRE FROM 1 BY 1
        //                   UNTIL   INDEX-PADRE > WK-LIMITE-LIVELLI
        //                   OR      INDEX-PADRE >= WK-NUMERO-LIVELLO
        //                   END-IF
        //           END-PERFORM.
        ws.setIndexPadre(((short)1));
        while (!(ws.getIndexPadre() > ws.getWkLimiteLivelli() || ws.getIndexPadre() >= ws.getWkNumeroLivello())) {
            // COB_CODE: IF CAMPI-REDEFINES
            //                    TO WK-LUNG-PADRE-RED(INDEX-PADRE)
            //           ELSE
            //                  TO WK-LUNG-PADRE(INDEX-PADRE)
            //           END-IF
            if (ws.isFlagRedefines()) {
                // COB_CODE: ADD WK-CTRL-LUNG-CAMPO-LUNG-EFF
                //                 TO WK-LUNG-PADRE-RED(INDEX-PADRE)
                ws.getWkElementiPadreRed(ws.getIndexPadre()).setLungPadreRed(Trunc.toInt(ws.getConversionVariables().getCtrlLungCampoLungEff() + ws.getWkElementiPadreRed(ws.getIndexPadre()).getLungPadreRed(), 5));
            }
            else {
                // COB_CODE: ADD WK-CTRL-LUNG-CAMPO-LUNG-EFF
                //               TO WK-LUNG-PADRE(INDEX-PADRE)
                ws.getWkElementiPadre(ws.getIndexPadre()).setLungPadre(Trunc.toInt(ws.getConversionVariables().getCtrlLungCampoLungEff() + ws.getWkElementiPadre(ws.getIndexPadre()).getLungPadre(), 5));
            }
            ws.setIndexPadre(Trunc.toShort(ws.getIndexPadre() + 1, 3));
        }
    }

    /**Original name: C300-CTRL-STRUTTURA-PADRE_FIRST_SENTENCES<br>*/
    private void c300CtrlStrutturaPadre() {
        // COB_CODE: IF IDSI0021-PTF-NEWLIFE AND
        //              WK-NUMERO-LIVELLO < WK-LIVELLO-PADRE
        //              PERFORM C330-RICERCA-FLAG-CALL-USING
        //           END-IF.
        if (ws.getIdsi0021Area().getIdentitaChiamante().isPtfNewlife() && ws.getWkNumeroLivello() < ws.getWkLivelloPadre()) {
            // COB_CODE: PERFORM C330-RICERCA-FLAG-CALL-USING
            c330RicercaFlagCallUsing();
        }
        // COB_CODE: MOVE INDICE-ANAGR-DATO    TO IDSO0021-NUM-ELEM.
        ws.getIdso0021().setIdso0021NumElem(ws.getIndiceAnagrDato());
    }

    /**Original name: C330-RICERCA-FLAG-CALL-USING_FIRST_SENTENCES<br>*/
    private void c330RicercaFlagCallUsing() {
        boolean endOfTable = false;
        // COB_CODE: SET IDSO0021-IND               TO 1
        ws.setIdso0021Ind(1);
        // COB_CODE: SEARCH  IDSO0021-ELEMENTS-STR-DATO
        //             WHEN IDSO0021-FLAG-CALL-USING(IDSO0021-IND)
        //                           = WK-CAMPO-ATTIVO
        //                  PERFORM C350-SCHIACCIA-STRUTTURA
        //           END-SEARCH.
        endOfTable = true;
        while (ws.getIdso0021Ind() <= 250) {
            if (ws.getIdso0021().getIdso0021StrutturaDato().getFlagCallUsing(ws.getIdso0021Ind()) == ws.getWkCampoAttivo()) {
                endOfTable = false;
                // COB_CODE: PERFORM C350-SCHIACCIA-STRUTTURA
                c350SchiacciaStruttura();
                break;
            }
            ws.setIdso0021Ind(Trunc.toInt(ws.getIdso0021Ind() + 1, 9));
        }
        //NO AT END GROUP;
    }

    /**Original name: C350-SCHIACCIA-STRUTTURA_FIRST_SENTENCES<br>*/
    private void c350SchiacciaStruttura() {
        // COB_CODE: MOVE 0                    TO INDICE-ANAGR-DATO
        ws.setIndiceAnagrDato(((short)0));
        // COB_CODE: SET SCR-IND               TO 1
        ws.setScrInd(1);
        // COB_CODE: MOVE IDSO0021-STRUTTURA-DATO
        //                                     TO AREA-SCREMATURA
        ws.setAreaScrematuraBytes(ws.getIdso0021().getIdso0021StrutturaDato().getIdso0021StrutturaDatoBytes());
        // COB_CODE: MOVE SPACES               TO IDSO0021-STRUTTURA-DATO
        ws.getIdso0021().getIdso0021StrutturaDato().initIdso0021StrutturaDatoSpaces();
        // COB_CODE: PERFORM VARYING SCR-IND FROM 1 BY 1
        //                   UNTIL   SCR-IND > WK-LIMITE-STR-DATO
        //                   OR      SCR-CODICE-DATO(SCR-IND) =
        //                           SPACES OR LOW-VALUES OR HIGH-VALUES
        //                   END-IF
        //           END-PERFORM.
        ws.setScrInd(1);
        while (!(ws.getScrInd() > ws.getWkLimiteStrDato0() || Characters.EQ_SPACE.test(ws.getScrElementsStrDato(ws.getScrInd()).getCodiceDato()) || Characters.EQ_LOW.test(ws.getScrElementsStrDato(ws.getScrInd()).getCodiceDato(), ScrElementsStrDato.Len.CODICE_DATO) || Characters.EQ_HIGH.test(ws.getScrElementsStrDato(ws.getScrInd()).getCodiceDato(), ScrElementsStrDato.Len.CODICE_DATO))) {
            // COB_CODE: IF SCR-FLAG-CALL-USING(SCR-IND) =
            //                                 WK-CAMPO-ATTIVO
            //                                        (INDICE-ANAGR-DATO)
            //           END-IF
            if (ws.getScrElementsStrDato(ws.getScrInd()).getFlagCallUsing() == ws.getWkCampoAttivo()) {
                // COB_CODE: ADD 1           TO INDICE-ANAGR-DATO
                ws.setIndiceAnagrDato(Trunc.toShort(1 + ws.getIndiceAnagrDato(), 3));
                // COB_CODE: MOVE SCR-ELEMENTS-STR-DATO(SCR-IND)
                //                           TO IDSO0021-ELEMENTS-STR-DATO
                //                                     (INDICE-ANAGR-DATO)
                ws.getIdso0021().getIdso0021StrutturaDato().setElementsStrDatoBytes(ws.getIndiceAnagrDato(), ws.getScrElementsStrDato(ws.getScrInd()).getScrElementsStrDatoBytes());
            }
            ws.setScrInd(Trunc.toInt(ws.getScrInd() + 1, 9));
        }
    }

    /**Original name: N000-CNTL-CAMPI-NULL-CSD_FIRST_SENTENCES<br>*/
    private void n000CntlCampiNullCsd() {
        // COB_CODE: IF IND-CSD-COD-STR-DATO2 = -1
        //              MOVE HIGH-VALUES TO CSD-COD-STR-DATO2
        //           END-IF
        if (ws.getIndCompStrDato().getCodStrDato2() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO CSD-COD-STR-DATO2
            ws.getCompStrDato().setCsdCodStrDato2(LiteralGenerator.create(Types.HIGH_CHAR_VAL, CompStrDato.Len.CSD_COD_STR_DATO2));
        }
        // COB_CODE: IF IND-CSD-COD-DATO = -1
        //              MOVE HIGH-VALUES TO CSD-COD-DATO
        //           END-IF
        if (ws.getIndCompStrDato().getCodDato() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO CSD-COD-DATO
            ws.getCompStrDato().setCsdCodDato(LiteralGenerator.create(Types.HIGH_CHAR_VAL, CompStrDato.Len.CSD_COD_DATO));
        }
        // COB_CODE: IF IND-CSD-FLAG-KEY = -1
        //              MOVE HIGH-VALUES TO CSD-FLAG-KEY-NULL
        //           END-IF
        if (ws.getIndCompStrDato().getFlagKey() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO CSD-FLAG-KEY-NULL
            ws.getCompStrDato().setCsdFlagKey(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-CSD-FLAG-RETURN-CODE = -1
        //              MOVE HIGH-VALUES TO CSD-FLAG-RETURN-CODE-NULL
        //           END-IF
        if (ws.getIndCompStrDato().getFlagReturnCode() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO CSD-FLAG-RETURN-CODE-NULL
            ws.getCompStrDato().setCsdFlagReturnCode(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-CSD-FLAG-CALL-USING = -1
        //              MOVE HIGH-VALUES TO CSD-FLAG-CALL-USING-NULL
        //           END-IF
        if (ws.getIndCompStrDato().getFlagCallUsing() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO CSD-FLAG-CALL-USING-NULL
            ws.getCompStrDato().setCsdFlagCallUsing(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-CSD-FLAG-WHERE-COND = -1
        //              MOVE HIGH-VALUES TO CSD-FLAG-WHERE-COND-NULL
        //           END-IF
        if (ws.getIndCompStrDato().getFlagWhereCond() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO CSD-FLAG-WHERE-COND-NULL
            ws.getCompStrDato().setCsdFlagWhereCond(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-CSD-FLAG-REDEFINES = -1
        //              MOVE HIGH-VALUES TO CSD-FLAG-REDEFINES-NULL
        //           END-IF
        if (ws.getIndCompStrDato().getFlagRedefines() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO CSD-FLAG-REDEFINES-NULL
            ws.getCompStrDato().setCsdFlagRedefines(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-CSD-TIPO-DATO = -1
        //              MOVE HIGH-VALUES TO CSD-TIPO-DATO-NULL
        //           END-IF
        if (ws.getIndCompStrDato().getTipoDato() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO CSD-TIPO-DATO-NULL
            ws.getCompStrDato().setCsdTipoDato(LiteralGenerator.create(Types.HIGH_CHAR_VAL, CompStrDato.Len.CSD_TIPO_DATO));
        }
        // COB_CODE: IF IND-CSD-LUNGHEZZA-DATO = -1
        //              MOVE HIGH-VALUES TO CSD-LUNGHEZZA-DATO-NULL
        //           END-IF
        if (ws.getIndCompStrDato().getLunghezzaDato() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO CSD-LUNGHEZZA-DATO-NULL
            ws.getCompStrDato().getCsdLunghezzaDato().setCsdLunghezzaDatoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, CsdLunghezzaDato.Len.CSD_LUNGHEZZA_DATO_NULL));
        }
        // COB_CODE: IF IND-CSD-PRECISIONE-DATO = -1
        //              MOVE HIGH-VALUES TO CSD-PRECISIONE-DATO-NULL
        //           END-IF
        if (ws.getIndCompStrDato().getPrecisioneDato() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO CSD-PRECISIONE-DATO-NULL
            ws.getCompStrDato().getCsdPrecisioneDato().setCsdPrecisioneDatoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, CsdPrecisioneDato.Len.CSD_PRECISIONE_DATO_NULL));
        }
        // COB_CODE: IF IND-CSD-COD-DOMINIO = -1
        //              MOVE HIGH-VALUES TO CSD-COD-DOMINIO
        //           END-IF
        if (ws.getIndCompStrDato().getCodDominio() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO CSD-COD-DOMINIO
            ws.getCompStrDato().setCsdCodDominio(LiteralGenerator.create(Types.HIGH_CHAR_VAL, CompStrDato.Len.CSD_COD_DOMINIO));
        }
        // COB_CODE: IF IND-CSD-FORMATTAZIONE-DATO = -1
        //              MOVE HIGH-VALUES TO CSD-FORMATTAZIONE-DATO
        //           END-IF
        if (ws.getIndCompStrDato().getFormattazioneDato() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO CSD-FORMATTAZIONE-DATO
            ws.getCompStrDato().setCsdFormattazioneDato(LiteralGenerator.create(Types.HIGH_CHAR_VAL, CompStrDato.Len.CSD_FORMATTAZIONE_DATO));
        }
        // COB_CODE: IF IND-CSD-SERVIZIO-CONVERS = -1
        //              MOVE HIGH-VALUES TO CSD-SERVIZIO-CONVERS-NULL
        //           END-IF
        if (ws.getIndCompStrDato().getServizioConvers() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO CSD-SERVIZIO-CONVERS-NULL
            ws.getCompStrDato().setCsdServizioConvers(LiteralGenerator.create(Types.HIGH_CHAR_VAL, CompStrDato.Len.CSD_SERVIZIO_CONVERS));
        }
        // COB_CODE: IF IND-CSD-AREA-CONV-STANDARD = -1
        //              MOVE HIGH-VALUES TO CSD-AREA-CONV-STANDARD-NULL
        //           END-IF
        if (ws.getIndCompStrDato().getAreaConvStandard() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO CSD-AREA-CONV-STANDARD-NULL
            ws.getCompStrDato().setCsdAreaConvStandard(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-CSD-RICORRENZA = -1
        //              MOVE HIGH-VALUES TO CSD-RICORRENZA-NULL
        //           END-IF
        if (ws.getIndCompStrDato().getRicorrenza() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO CSD-RICORRENZA-NULL
            ws.getCompStrDato().getCsdRicorrenza().setCsdRicorrenzaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, CsdRicorrenza.Len.CSD_RICORRENZA_NULL));
        }
        // COB_CODE: IF IND-CSD-OBBLIGATORIETA = -1
        //              MOVE HIGH-VALUES TO CSD-OBBLIGATORIETA-NULL
        //           END-IF
        if (ws.getIndCompStrDato().getObbligatorieta() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO CSD-OBBLIGATORIETA-NULL
            ws.getCompStrDato().setCsdObbligatorieta(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-CSD-VALORE-DEFAULT = -1
        //              MOVE HIGH-VALUES TO CSD-VALORE-DEFAULT
        //           END-IF.
        if (ws.getIndCompStrDato().getValoreDefault() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO CSD-VALORE-DEFAULT
            ws.getCompStrDato().setCsdValoreDefault(LiteralGenerator.create(Types.HIGH_CHAR_VAL, CompStrDato.Len.CSD_VALORE_DEFAULT));
        }
    }

    /**Original name: N100-CNTL-CAMPI-NULL-ADA_FIRST_SENTENCES<br>*/
    private void n100CntlCampiNullAda() {
        // COB_CODE: IF IND-ADA-DESC-DATO = -1
        //              MOVE HIGH-VALUES TO ADA-DESC-DATO
        //           END-IF
        if (ws.getIndAnagDato().getDescDato() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADA-DESC-DATO
            ws.getAnagDato().setAdaDescDato(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AnagDato.Len.ADA_DESC_DATO));
        }
        // COB_CODE: IF IND-ADA-TIPO-DATO = -1
        //              MOVE HIGH-VALUES TO ADA-TIPO-DATO-NULL
        //           END-IF
        if (ws.getIndAnagDato().getTipoDato() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADA-TIPO-DATO-NULL
            ws.getAnagDato().setAdaTipoDato(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AnagDato.Len.ADA_TIPO_DATO));
        }
        // COB_CODE: IF IND-ADA-LUNGHEZZA-DATO = -1
        //              MOVE HIGH-VALUES TO ADA-LUNGHEZZA-DATO-NULL
        //           END-IF
        if (ws.getIndAnagDato().getLunghezzaDato() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADA-LUNGHEZZA-DATO-NULL
            ws.getAnagDato().getAdaLunghezzaDato().setAdaLunghezzaDatoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdaLunghezzaDato.Len.ADA_LUNGHEZZA_DATO_NULL));
        }
        // COB_CODE: IF IND-ADA-PRECISIONE-DATO = -1
        //              MOVE HIGH-VALUES TO ADA-PRECISIONE-DATO-NULL
        //           END-IF
        if (ws.getIndAnagDato().getPrecisioneDato() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADA-PRECISIONE-DATO-NULL
            ws.getAnagDato().getAdaPrecisioneDato().setAdaPrecisioneDatoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdaPrecisioneDato.Len.ADA_PRECISIONE_DATO_NULL));
        }
        // COB_CODE: IF IND-ADA-COD-DOMINIO = -1
        //              MOVE HIGH-VALUES TO ADA-COD-DOMINIO
        //           END-IF
        if (ws.getIndAnagDato().getCodDominio() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADA-COD-DOMINIO
            ws.getAnagDato().setAdaCodDominio(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AnagDato.Len.ADA_COD_DOMINIO));
        }
        // COB_CODE: IF IND-ADA-FORMATTAZIONE-DATO = -1
        //              MOVE HIGH-VALUES TO ADA-FORMATTAZIONE-DATO
        //           END-IF.
        if (ws.getIndAnagDato().getFormattazioneDato() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADA-FORMATTAZIONE-DATO
            ws.getAnagDato().setAdaFormattazioneDato(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AnagDato.Len.ADA_FORMATTAZIONE_DATO));
        }
    }

    /**Original name: R000-RICORSIONE_FIRST_SENTENCES<br>*/
    private void r000Ricorsione() {
        Idss0020 idss0020 = null;
        // COB_CODE: MOVE CSD-COD-STR-DATO2
        //                                     TO IDSI0021-CODICE-STR-DATO
        ws.getIdsi0021Area().setCodiceStrDato(ws.getCompStrDato().getCsdCodStrDato2());
        // COB_CODE: IF CAMPI-REDEFINES
        //                                 (WK-NUMERO-LIVELLO)
        //           ELSE
        //                                (WK-NUMERO-LIVELLO)
        //           END-IF
        if (ws.isFlagRedefines()) {
            // COB_CODE: MOVE INDICE-ANAGR-DATO
            //                            TO WK-INDICE-PADRE-RED
            //                              (WK-NUMERO-LIVELLO)
            ws.getWkElementiPadreRed(ws.getWkNumeroLivello()).setIndicePadreRed(ws.getIndiceAnagrDato());
        }
        else {
            // COB_CODE: MOVE INDICE-ANAGR-DATO
            //                            TO WK-INDICE-PADRE
            //                             (WK-NUMERO-LIVELLO)
            ws.getWkElementiPadre(ws.getWkNumeroLivello()).setIndicePadre(ws.getIndiceAnagrDato());
        }
        // COB_CODE: MOVE SPACES               TO AREA-CALL
        areaCall.initAreaCallSpaces();
        // COB_CODE: MOVE INPUT-IDSS0020       TO AREA-CALL.
        areaCall.setAreaCallBytes(ws.getInputIdss0020Bytes());
        // COB_CODE: CALL PGM-NAME-IDSS0020 USING AREA-CALL.
        idss0020 = Idss0020.getInstance();
        idss0020.run(areaCall);
        //     MOVE SPACES               TO OUTPUT-IDSS0020
        // COB_CODE: MOVE AREA-CALL            TO OUTPUT-IDSS0020
        ws.setOutputIdss0020Bytes(areaCall.getAreaCallBytes());
        // COB_CODE: IF IDSO0021-RETURN-CODE NOT = '00'
        //              GOBACK
        //           END-IF.
        if (!Conditions.eq(ws.getIdso0021().getIdso0021AreaErrori().getReturnCode().getReturnCode(), "00")) {
            // COB_CODE: GOBACK
            throw new ReturnException();
        }
    }

    /**Original name: R100-ACCEDI-ANAGR-DATO_FIRST_SENTENCES<br>
	 * <pre>--> TIPO OPERAZIONE</pre>*/
    private void r100AccediAnagrDato() {
        Idss0080 idss0080 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: SET IDSV0003-SELECT            TO TRUE.
        ws.getIdsv0003().getOperazione().setSelect();
        //--> INIZIALIZZA CODICE DI RITORNO
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC    TO TRUE.
        ws.getIdsv0003().getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE.
        ws.getIdsv0003().getSqlcode().setSuccessfulSql();
        // COB_CODE: CALL PGM-NAME-IDSS0080               USING IDSV0003
        //                                                 ANAG-DATO
        idss0080 = Idss0080.getInstance();
        idss0080.run(ws.getIdsv0003(), ws.getAnagDato());
        // COB_CODE:      IF IDSV0003-SUCCESSFUL-RC
        //                   END-EVALUATE
        //                ELSE
        //           *-->    GESTIRE ERRORE DISPATCHER
        //                   MOVE 'ANAG_DATO' TO IDSO0021-NOME-TABELLA
        //                END-IF.
        if (ws.getIdsv0003().getReturnCode().isSuccessfulRc()) {
            // COB_CODE:         EVALUATE TRUE
            //                       WHEN IDSV0003-SUCCESSFUL-SQL
            //           *-->        OPERAZIONE ESEGUITA CORRETTAMENTE
            //                          PERFORM N100-CNTL-CAMPI-NULL-ADA
            //                       WHEN OTHER
            //           *--->       ERRORE DI ACCESSO AL DB
            //                        MOVE 'ANAG_DATO' TO IDSO0021-NOME-TABELLA
            //                   END-EVALUATE
            switch (ws.getIdsv0003().getSqlcode().getSqlcode()) {

                case Idsv0003Sqlcode.SUCCESSFUL_SQL://-->        OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: PERFORM N100-CNTL-CAMPI-NULL-ADA
                    n100CntlCampiNullAda();
                    break;

                default://--->       ERRORE DI ACCESSO AL DB
                    // COB_CODE: MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                    ws.setWkSqlcode(ws.getIdsv0003().getSqlcode().getSqlcode());
                    // COB_CODE: STRING 'ANAG_DATO'
                    //                  ' - '
                    //                  'RC : '
                    //                  IDSV0003-RETURN-CODE
                    //                  'SQLCODE : '
                    //                  WK-SQLCODE
                    //                  DELIMITED BY SIZE INTO
                    //                  IDSO0021-DESCRIZ-ERR-DB2
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Idso0021AreaErrori.Len.DESCRIZ_ERR_DB2, new String[] {"ANAG_DATO", " - ", "RC : ", ws.getIdsv0003().getReturnCode().getReturnCodeFormatted(), "SQLCODE : ", ws.getWkSqlcodeAsString()});
                    ws.getIdso0021().getIdso0021AreaErrori().setDescrizErrDb2(concatUtil.replaceInString(ws.getIdso0021().getIdso0021AreaErrori().getDescrizErrDb2Formatted()));
                    // COB_CODE: PERFORM 2000-DBERROR
                    dberror();
                    // COB_CODE: MOVE 'ANAG_DATO' TO IDSO0021-NOME-TABELLA
                    ws.getIdso0021().getIdso0021AreaErrori().setNomeTabella("ANAG_DATO");
                    break;
            }
        }
        else {
            //-->    GESTIRE ERRORE DISPATCHER
            // COB_CODE: STRING 'ERRORE CHIAMATA MODULO '
            //                   PGM-NAME-IDSS0080
            //                   ' - '
            //                   'RC : '
            //                   IDSV0003-RETURN-CODE
            //                   DELIMITED BY SIZE INTO
            //                   IDSO0021-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idso0021AreaErrori.Len.DESCRIZ_ERR_DB2, "ERRORE CHIAMATA MODULO ", ws.getPgmNameIdss0080Formatted(), " - ", "RC : ", ws.getIdsv0003().getReturnCode().getReturnCodeFormatted());
            ws.getIdso0021().getIdso0021AreaErrori().setDescrizErrDb2(concatUtil.replaceInString(ws.getIdso0021().getIdso0021AreaErrori().getDescrizErrDb2Formatted()));
            // COB_CODE: PERFORM 2000-DBERROR
            dberror();
            // COB_CODE: MOVE 'ANAG_DATO' TO IDSO0021-NOME-TABELLA
            ws.getIdso0021().getIdso0021AreaErrori().setNomeTabella("ANAG_DATO");
        }
    }

    /**Original name: R200-CTRL-LUNGHEZZA-CAMPO_FIRST_SENTENCES<br>*/
    private void r200CtrlLunghezzaCampo() {
        // COB_CODE: IF WK-CTRL-LUNG-CAMPO-TIPO =
        //                WK-CAMPO-TIPO-COMP
        //                END-EVALUATE
        //           END-IF
        if (Conditions.eq(ws.getConversionVariables().getCtrlLungCampoTipo(), ws.getConversionVariables().getCampoTipoComp())) {
            // COB_CODE: EVALUATE WK-CTRL-LUNG-CAMPO-LUNG
            //             WHEN 1 THRU 4
            //                MOVE 2      TO WK-CTRL-LUNG-CAMPO-LUNG
            //             WHEN 5 THRU 9
            //                MOVE 4      TO WK-CTRL-LUNG-CAMPO-LUNG
            //             WHEN 10 THRU 18
            //                MOVE 8      TO WK-CTRL-LUNG-CAMPO-LUNG
            //           END-EVALUATE
            if (ws.getConversionVariables().getCtrlLungCampoLung() >= 1 && ws.getConversionVariables().getCtrlLungCampoLung() <= 4) {
                // COB_CODE: MOVE 2      TO WK-CTRL-LUNG-CAMPO-LUNG
                ws.getConversionVariables().setCtrlLungCampoLung(2);
            }
            else if (ws.getConversionVariables().getCtrlLungCampoLung() >= 5 && ws.getConversionVariables().getCtrlLungCampoLung() <= 9) {
                // COB_CODE: MOVE 4      TO WK-CTRL-LUNG-CAMPO-LUNG
                ws.getConversionVariables().setCtrlLungCampoLung(4);
            }
            else if (ws.getConversionVariables().getCtrlLungCampoLung() >= 10 && ws.getConversionVariables().getCtrlLungCampoLung() <= 18) {
                // COB_CODE: MOVE 8      TO WK-CTRL-LUNG-CAMPO-LUNG
                ws.getConversionVariables().setCtrlLungCampoLung(8);
            }
        }
        // COB_CODE: IF WK-CTRL-LUNG-CAMPO-TIPO =
        //              WK-CAMPO-TIPO-COMP3
        //                       WK-CTRL-LUNG-CAMPO-LUNG + 1
        //           END-IF.
        if (Conditions.eq(ws.getConversionVariables().getCtrlLungCampoTipo(), ws.getConversionVariables().getCampoTipoComp3())) {
            // COB_CODE: COMPUTE WK-CTRL-LUNG-CAMPO-LUNG =
            //                   WK-CTRL-LUNG-CAMPO-LUNG / 2
            ws.getConversionVariables().setCtrlLungCampoLung((new AfDecimal(((((double)(ws.getConversionVariables().getCtrlLungCampoLung()))) / 2), 5, 0)).toInt());
            // COB_CODE: COMPUTE WK-CTRL-LUNG-CAMPO-LUNG =
            //                   WK-CTRL-LUNG-CAMPO-LUNG + 1
            ws.getConversionVariables().setCtrlLungCampoLung(Trunc.toInt(ws.getConversionVariables().getCtrlLungCampoLung() + 1, 5));
        }
    }

    /**Original name: R300-CNTL-RICORRENZA_FIRST_SENTENCES<br>*/
    private void r300CntlRicorrenza() {
        // COB_CODE: IF CSD-RICORRENZA IS NUMERIC AND
        //              CSD-RICORRENZA > 0
        //              END-IF
        //           END-IF.
        if (Functions.isNumber(ws.getCompStrDato().getCsdRicorrenza().getCsdRicorrenza()) && ws.getCompStrDato().getCsdRicorrenza().getCsdRicorrenza() > 0) {
            // COB_CODE: IF CAMPI-REDEFINES
            //                      WK-NUMERO-RICORRENZA
            //           ELSE
            //                      WK-NUMERO-RICORRENZA
            //           END-IF
            if (ws.isFlagRedefines()) {
                // COB_CODE: MOVE CSD-RICORRENZA
                //                TO WK-NUMERO-RICORRENZE-RED(WK-NUMERO-LIVELLO)
                //                   WK-NUMERO-RICORRENZA
                ws.getWkElementiPadreRed(ws.getWkNumeroLivello()).setNumeroRicorrenzeRed(ws.getCompStrDato().getCsdRicorrenza().getCsdRicorrenza());
                ws.setWkNumeroRicorrenza(ws.getCompStrDato().getCsdRicorrenza().getCsdRicorrenza());
            }
            else {
                // COB_CODE: MOVE CSD-RICORRENZA
                //                TO WK-NUMERO-RICORRENZE    (WK-NUMERO-LIVELLO)
                //                   WK-NUMERO-RICORRENZA
                ws.getWkElementiPadre(ws.getWkNumeroLivello()).setNumeroRicorrenze(ws.getCompStrDato().getCsdRicorrenza().getCsdRicorrenza());
                ws.setWkNumeroRicorrenza(ws.getCompStrDato().getCsdRicorrenza().getCsdRicorrenza());
            }
        }
    }

    /**Original name: R500-ACCEDI-SU-RDS_FIRST_SENTENCES<br>*/
    private void r500AccediSuRds() {
        // COB_CODE: EXEC SQL
        //              SELECT
        //                  COD_STR_DATO
        //              INTO
        //                  :RDS-COD-DATO
        //              FROM RIDEF_DATO_STR
        //              WHERE COD_COMPAGNIA_ANIA = :RDS-COD-COMPAGNIA-ANIA
        //              AND   TIPO_MOVIMENTO     = :RDS-TIPO-MOVIMENTO
        //              AND   COD_DATO           = :RDS-COD-DATO
        //           END-EXEC.
        ws.getRidefDatoStr().setCodDato(ridefDatoStrDao.selectRec(ws.getRidefDatoStr().getCodCompagniaAnia(), ws.getRidefDatoStr().getTipoMovimento(), ws.getRidefDatoStr().getCodDato(), ws.getRidefDatoStr().getCodDato()));
        // COB_CODE: MOVE SQLCODE         TO IDSO0021-SQLCODE
        ws.getIdso0021().getIdso0021AreaErrori().getSqlcode().setSqlcodeSigned(sqlca.getSqlcode());
        // COB_CODE: MOVE DESCRIZ-ERR-DB2 TO IDSO0021-DESCRIZ-ERR-DB2.
        ws.getIdso0021().getIdso0021AreaErrori().setDescrizErrDb2(ws.getDescrizErrDb2());
    }

    /**Original name: R600-CNTL-REDEFINES_FIRST_SENTENCES<br>*/
    private void r600CntlRedefines() {
        // COB_CODE: IF CSD-FLAG-REDEFINES = WK-CAMPO-ATTIVO
        //              END-IF
        //           END-IF.
        if (ws.getCompStrDato().getCsdFlagRedefines() == ws.getWkCampoAttivo()) {
            // COB_CODE: PERFORM C180-COMPOSIZIONE-RDS
            c180ComposizioneRds();
            // COB_CODE: PERFORM R500-ACCEDI-SU-RDS
            r500AccediSuRds();
            // COB_CODE: IF NOT IDSO0021-SUCCESSFUL-SQL
            //              END-IF
            //           END-IF
            if (!ws.getIdso0021().getIdso0021AreaErrori().getSqlcode().isSuccessfulSql()) {
                // COB_CODE: IF IDSO0021-NOT-FOUND
                //              PERFORM R500-ACCEDI-SU-RDS
                //           ELSE
                //                             TO IDSO0021-NOME-TABELLA
                //           END-IF
                if (ws.getIdso0021().getIdso0021AreaErrori().getSqlcode().isNotFound()) {
                    // COB_CODE: MOVE 99999  TO RDS-TIPO-MOVIMENTO
                    ws.getRidefDatoStr().setTipoMovimento(99999);
                    // COB_CODE: SET IDSO0021-SUCCESSFUL-RC  TO TRUE
                    ws.getIdso0021().getIdso0021AreaErrori().getReturnCode().setSuccessfulRc();
                    // COB_CODE: SET IDSO0021-SUCCESSFUL-SQL TO TRUE
                    ws.getIdso0021().getIdso0021AreaErrori().getSqlcode().setSuccessfulSql();
                    // COB_CODE: PERFORM R500-ACCEDI-SU-RDS
                    r500AccediSuRds();
                }
                else {
                    // COB_CODE: PERFORM 2000-DBERROR
                    dberror();
                    // COB_CODE: MOVE 'RIDEF_DATO_STR'
                    //                          TO IDSO0021-NOME-TABELLA
                    ws.getIdso0021().getIdso0021AreaErrori().setNomeTabella("RIDEF_DATO_STR");
                }
            }
            // COB_CODE: IF IDSO0021-SUCCESSFUL-SQL
            //              PERFORM C000-CONTROLLO-FIGLIO
            //           ELSE
            //              END-IF
            //           END-IF
            if (ws.getIdso0021().getIdso0021AreaErrori().getSqlcode().isSuccessfulSql()) {
                // COB_CODE: MOVE WK-NUMERO-LIVELLO TO WK-NUMERO-LIVELLO-RED
                ws.setWkNumeroLivelloRed(ws.getWkNumeroLivello());
                // COB_CODE: SET CAMPI-REDEFINES    TO TRUE
                ws.setFlagRedefines(true);
                // COB_CODE: MOVE RDS-COD-STR-DATO  TO CSD-COD-STR-DATO2
                ws.getCompStrDato().setCsdCodStrDato2(ws.getRidefDatoStr().getCodStrDato());
                // COB_CODE: MOVE 0                 TO IND-CSD-COD-STR-DATO2
                ws.getIndCompStrDato().setCodStrDato2(((short)0));
                // COB_CODE: PERFORM C000-CONTROLLO-FIGLIO
                c000ControlloFiglio();
            }
            else if (ws.getIdso0021().getIdso0021AreaErrori().getSqlcode().isNotFound()) {
                // COB_CODE: IF IDSO0021-NOT-FOUND
                //              SET IDSO0021-SUCCESSFUL-SQL TO TRUE
                //           ELSE
                //              MOVE 'RIDEF_DATO_STR' TO IDSO0021-NOME-TABELLA
                //           END-IF
                // COB_CODE: SET IDSO0021-SUCCESSFUL-RC  TO TRUE
                ws.getIdso0021().getIdso0021AreaErrori().getReturnCode().setSuccessfulRc();
                // COB_CODE: SET IDSO0021-SUCCESSFUL-SQL TO TRUE
                ws.getIdso0021().getIdso0021AreaErrori().getSqlcode().setSuccessfulSql();
            }
            else {
                // COB_CODE: PERFORM 2000-DBERROR
                dberror();
                // COB_CODE: MOVE 'RIDEF_DATO_STR' TO IDSO0021-NOME-TABELLA
                ws.getIdso0021().getIdso0021AreaErrori().setNomeTabella("RIDEF_DATO_STR");
            }
        }
    }

    /**Original name: S000-ELABORA-SST_FIRST_SENTENCES<br>*/
    private void s000ElaboraSst() {
        // COB_CODE: PERFORM S210-VALORIZZA-CAMPI
        s210ValorizzaCampi();
        //     PERFORM S200-ACCEDI-SU-SST
        // COB_CODE: IF IDSO0021-SUCCESSFUL-RC AND
        //              IND-SINONIM = 0
        //              END-IF
        //           END-IF.
        if (ws.getIdso0021().getIdso0021AreaErrori().getReturnCode().isSuccessfulRc() && ws.getIndSinonim() == 0) {
            // COB_CODE: PERFORM S210-VALORIZZA-CAMPI
            s210ValorizzaCampi();
            // COB_CODE: MOVE '*'              TO  SST-OPERAZIONE
            ws.getSinonimoStr().setOperazione("*");
            // COB_CODE: PERFORM S200-ACCEDI-SU-SST
            s200AccediSuSst();
            // COB_CODE: IF IDSO0021-SUCCESSFUL-RC AND
            //              IND-SINONIM = 0
            //              END-IF
            //           END-IF
            if (ws.getIdso0021().getIdso0021AreaErrori().getReturnCode().isSuccessfulRc() && ws.getIndSinonim() == 0) {
                // COB_CODE: PERFORM S210-VALORIZZA-CAMPI
                s210ValorizzaCampi();
                // COB_CODE: MOVE '*'            TO  SST-FLAG-MOD-ESECUTIVA
                ws.getSinonimoStr().setFlagModEsecutivaFormatted("*");
                // COB_CODE: PERFORM S200-ACCEDI-SU-SST
                s200AccediSuSst();
                // COB_CODE: IF IDSO0021-SUCCESSFUL-RC AND
                //              IND-SINONIM = 0
                //              PERFORM S200-ACCEDI-SU-SST
                //           END-IF
                if (ws.getIdso0021().getIdso0021AreaErrori().getReturnCode().isSuccessfulRc() && ws.getIndSinonim() == 0) {
                    // COB_CODE: PERFORM S210-VALORIZZA-CAMPI
                    s210ValorizzaCampi();
                    // COB_CODE: MOVE '*'         TO  SST-OPERAZIONE
                    ws.getSinonimoStr().setOperazione("*");
                    // COB_CODE: MOVE '*'         TO  SST-FLAG-MOD-ESECUTIVA
                    ws.getSinonimoStr().setFlagModEsecutivaFormatted("*");
                    // COB_CODE: PERFORM S200-ACCEDI-SU-SST
                    s200AccediSuSst();
                }
            }
        }
    }

    /**Original name: S210-VALORIZZA-CAMPI_FIRST_SENTENCES<br>*/
    private void s210ValorizzaCampi() {
        // COB_CODE: MOVE IDSI0021-CODICE-COMPAGNIA-ANIA TO SST-COD-COMPAGNIA-ANIA
        ws.getSinonimoStr().setCodCompagniaAnia(ws.getIdsi0021Area().getCodiceCompagniaAnia());
        // COB_CODE: MOVE IDSI0021-CODICE-STR-DATO       TO SST-COD-STR-DATO
        ws.getSinonimoStr().setCodStrDato(ws.getIdsi0021Area().getCodiceStrDato());
        // COB_CODE: MOVE IDSI0021-MODALITA-ESECUTIVA    TO SST-FLAG-MOD-ESECUTIVA
        ws.getSinonimoStr().setFlagModEsecutiva(ws.getIdsi0021Area().getModalitaEsecutiva().getModalitaEsecutiva());
        // COB_CODE: MOVE IDSI0021-OPERAZIONE            TO SST-OPERAZIONE.
        ws.getSinonimoStr().setOperazione(ws.getIdsi0021Area().getOperazione().getOperazione());
    }

    /**Original name: S200-ACCEDI-SU-SST_FIRST_SENTENCES<br>*/
    private void s200AccediSuSst() {
        // COB_CODE: EXEC SQL
        //                SELECT COD_SIN_STR_DATO
        //                   INTO :SST-COD-STR-DATO
        //                FROM SINONIMO_STR
        //                WHERE COD_COMPAGNIA_ANIA = :SST-COD-COMPAGNIA-ANIA
        //                AND   COD_STR_DATO       = :SST-COD-STR-DATO
        //                AND   OPERAZIONE         = :SST-OPERAZIONE
        //                AND   FLAG_MOD_ESECUTIVA = :SST-FLAG-MOD-ESECUTIVA
        //           END-EXEC.
        ws.getSinonimoStr().setCodStrDato(sinonimoStrDao.selectRec(ws.getSinonimoStr().getCodCompagniaAnia(), ws.getSinonimoStr().getCodStrDato(), ws.getSinonimoStr().getOperazione(), ws.getSinonimoStr().getFlagModEsecutiva(), ws.getSinonimoStr().getCodStrDato()));
        // COB_CODE: MOVE SQLCODE         TO IDSO0021-SQLCODE
        ws.getIdso0021().getIdso0021AreaErrori().getSqlcode().setSqlcodeSigned(sqlca.getSqlcode());
        // COB_CODE: MOVE DESCRIZ-ERR-DB2 TO IDSO0021-DESCRIZ-ERR-DB2
        ws.getIdso0021().getIdso0021AreaErrori().setDescrizErrDb2(ws.getDescrizErrDb2());
        // COB_CODE: IF IDSO0021-SUCCESSFUL-SQL
        //              ADD 1                      TO IND-SINONIM
        //           ELSE
        //              END-IF
        //           END-IF.
        if (ws.getIdso0021().getIdso0021AreaErrori().getSqlcode().isSuccessfulSql()) {
            // COB_CODE: MOVE SST-COD-SIN-STR-DATO  TO IDSI0021-CODICE-STR-DATO
            ws.getIdsi0021Area().setCodiceStrDato(ws.getSinonimoStr().getCodSinStrDato());
            // COB_CODE: ADD 1                      TO IND-SINONIM
            ws.setIndSinonim(Trunc.toShort(1 + ws.getIndSinonim(), 1));
        }
        else if (!ws.getIdso0021().getIdso0021AreaErrori().getSqlcode().isNotFound()) {
            // COB_CODE: IF NOT IDSO0021-NOT-FOUND
            //              MOVE 'SINONIMO_STR'     TO IDSO0021-NOME-TABELLA
            //           END-IF
            // COB_CODE: PERFORM 2000-DBERROR
            dberror();
            // COB_CODE: MOVE 'SINONIMO_STR'     TO IDSO0021-NOME-TABELLA
            ws.getIdso0021().getIdso0021AreaErrori().setNomeTabella("SINONIMO_STR");
        }
    }

    /**Original name: 2000-DBERROR_FIRST_SENTENCES<br>*/
    private void dberror() {
        // COB_CODE: MOVE 'D3'                TO IDSO0021-RETURN-CODE.
        ws.getIdso0021().getIdso0021AreaErrori().getReturnCode().setReturnCode("D3");
        // COB_CODE: MOVE PGM-NAME-IDSS0020   TO IDSO0021-COD-SERVIZIO-BE.
        ws.getIdso0021().getIdso0021AreaErrori().setCodServizioBe(ws.getPgmNameIdss0020());
    }

    /**Original name: 4000-FINE_FIRST_SENTENCES<br>*/
    private void fine() {
        // COB_CODE: IF  INDICE-ANAGR-DATO = 0 AND WK-NUMERO-LIVELLO = 0
        //              PERFORM 2000-DBERROR
        //           END-IF.
        if (ws.getIndiceAnagrDato() == 0 && ws.getWkNumeroLivello() == 0) {
            // COB_CODE: PERFORM 2000-DBERROR
            dberror();
        }
    }

    public void initWkCalcoloPadre() {
        for (int idx0 = 1; idx0 <= Idss0020Data.WK_ELEMENTI_PADRE_MAXOCCURS; idx0++) {
            ws.getWkElementiPadre(idx0).setIndicePadreFormatted("000");
            ws.getWkElementiPadre(idx0).setLungPadre(0);
            ws.getWkElementiPadre(idx0).setNumeroRicorrenze(0);
        }
    }

    public void initWkCalcoloPadreRed() {
        for (int idx0 = 1; idx0 <= Idss0020Data.WK_ELEMENTI_PADRE_RED_MAXOCCURS; idx0++) {
            ws.getWkElementiPadreRed(idx0).setIndicePadreRedFormatted("000");
            ws.getWkElementiPadreRed(idx0).setLungPadreRed(0);
            ws.getWkElementiPadreRed(idx0).setNumeroRicorrenzeRed(0);
        }
    }

    public void registerListeners() {
        ws.getWkLimiteStrDato().addListener(ws.getFlagTrattamentoListener());
    }
}
