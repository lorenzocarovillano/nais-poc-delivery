package it.accenture.jnais;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Idsv0003CampiEsito;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.Lvvs3270Data;

/**Original name: LVVS3270<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2017.
 * DATE-COMPILED.
 * **------------------------------------------------------------***</pre>*/
public class Lvvs3270 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lvvs3270Data ws = new Lvvs3270Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS3270
    private Ivvc0213 ivvc0213;

    //==== METHODS ====
    /**Original name: PROGRAM_LVVS3270_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        //
        // COB_CODE: PERFORM S1000-ELABORAZIONE
        //              THRU EX-S1000
        s1000Elaborazione();
        //
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lvvs3270 getInstance() {
        return ((Lvvs3270)Programs.getInstance(Lvvs3270.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                        IX-INDICI
        //                                             IVVC0213-TAB-OUTPUT.
        initIxIndici();
        initTabOutput();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE
        //             TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
        // COB_CODE: MOVE IVVC0213-TIPO-MOVI-ORIG
        //             TO WS-MOVIMENTO.
        ws.getWsMovimento().setWsMovimentoFormatted(ivvc0213.getTipoMoviOrigFormatted());
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*
	 * --> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 * --> RISPETTIVE AREE DCLGEN IN WORKING</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: PERFORM S1100-VALORIZZA-DCLGEN
        //              THRU S1100-VALORIZZA-DCLGEN-EX
        //           VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
        //                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //                   SPACES OR LOW-VALUE OR HIGH-VALUE.
        ws.setIxDclgen(((short)1));
        while (!(ws.getIxDclgen() > ivvc0213.getEleInfoMax() || Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias()) || Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()) || Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()))) {
            s1100ValorizzaDclgen();
            ws.setIxDclgen(Trunc.toShort(ws.getIxDclgen() + 1, 4));
        }
        //
        // COB_CODE:      IF  IDSV0003-SUCCESSFUL-RC
        //                AND IDSV0003-SUCCESSFUL-SQL
        //           * --     LETTURA STRATEGIA D'INVESTIMENTO
        //                    END-IF
        //                END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // --     LETTURA STRATEGIA D'INVESTIMENTO
            // COB_CODE: PERFORM A400-LEGGI-SDI
            //              THRU A400-LEGGI-SDI-EX
            a400LeggiSdi();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
            //                TO IVVC0213-VAL-STR-O
            //           END-IF
            if (idsv0003.getReturnCode().isSuccessfulRc()) {
                // COB_CODE: MOVE SDI-TP-STRA
                //             TO IVVC0213-VAL-STR-O
                ivvc0213.getTabOutput().setValStrO(ws.getStraDiInvst().getSdiTpStra());
            }
        }
    }

    /**Original name: A400-LEGGI-SDI<br>
	 * <pre>----------------------------------------------------------------*
	 *    LETTURA STRATEGIA DI INVESTIMENTO
	 * ----------------------------------------------------------------*</pre>*/
    private void a400LeggiSdi() {
        Idbssdi0 idbssdi0 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE STRA-DI-INVST.
        initStraDiInvst();
        // COB_CODE: MOVE DADE-ID-ADES             TO SDI-ID-OGG
        ws.getStraDiInvst().setSdiIdOgg(ws.getLccvade1().getDati().getWadeIdAdes());
        // COB_CODE: SET ADESIONE                  TO TRUE
        ws.getWsTpOgg().setAdesione();
        // COB_CODE: MOVE  WS-TP-OGG               TO SDI-TP-OGG
        ws.getStraDiInvst().setSdiTpOgg(ws.getWsTpOgg().getWsTpOgg());
        // COB_CODE: MOVE SPACES                   TO IDSV0003-BUFFER-WHERE-COND.
        idsv0003.setBufferWhereCond("");
        // COB_CODE: SET IDSV0003-TRATT-X-COMPETENZA TO TRUE.
        idsv0003.getTrattamentoStoricita().setTrattXCompetenza();
        // COB_CODE: SET IDSV0003-ID-OGGETTO         TO TRUE.
        idsv0003.getLivelloOperazione().setIdsi0011IdOggetto();
        // COB_CODE: SET IDSV0003-SELECT             TO TRUE.
        idsv0003.getOperazione().setSelect();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC      TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL     TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: CALL PGM-IDBSSDI0  USING  IDSV0003 STRA-DI-INVST
        //           ON EXCEPTION
        //              SET IDSV0003-INVALID-OPER TO TRUE
        //           END-CALL.
        try {
            idbssdi0 = Idbssdi0.getInstance();
            idbssdi0.run(idsv0003, ws.getStraDiInvst());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE PGM-IDBSSDI0
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getPgmIdbssdi0());
            // COB_CODE: MOVE 'CALL LDBSF110 ERRORE CHIAMATA'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("CALL LDBSF110 ERRORE CHIAMATA");
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE:      IF IDSV0003-SUCCESSFUL-SQL
        //                   CONTINUE
        //                ELSE
        //           * SIR RISERVE DI NOVEMBRE 2018
        //                  END-IF
        //                END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
        // COB_CODE: CONTINUE
        //continue
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // SIR RISERVE DI NOVEMBRE 2018
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              MOVE 'STRATEGIA NON TROVATA' TO IDSV0003-DESCRIZ-ERR-DB2
            //           ELSE
            //                     END-STRING
            //           END-IF
            // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED TO TRUE
            idsv0003.getReturnCode().setFieldNotValued();
            // COB_CODE: MOVE WK-PGM   TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'STRATEGIA NON TROVATA' TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("STRATEGIA NON TROVATA");
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER     TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
            // COB_CODE: MOVE PGM-IDBSSDI0             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getPgmIdbssdi0());
            // COB_CODE: STRING 'CHIAMATA IDBSSDI0 ;'
            //                 IDSV0003-RETURN-CODE ';'
            //                 IDSV0003-SQLCODE
            //                 DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //                   END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA IDBSSDI0 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
        }
    }

    /**Original name: S1100-VALORIZZA-DCLGEN<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 *     RISPETTIVE AREE DCLGEN IN WORKING
	 * ----------------------------------------------------------------*</pre>*/
    private void s1100ValorizzaDclgen() {
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-ADES
        //                TO DADE-AREA-ADES
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias(), ws.getIvvc0218().getAliasAdes())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO DADE-AREA-ADES
            ws.setDadeAreaAdesFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxDclgen()).getLunghezza() - 1));
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    public void initIxIndici() {
        ws.setIxDclgen(((short)0));
    }

    public void initTabOutput() {
        ivvc0213.getTabOutput().setCodVariabileO("");
        ivvc0213.getTabOutput().setTpDatoO(Types.SPACE_CHAR);
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        ivvc0213.getTabOutput().setValPercO(new AfDecimal(0, 14, 9));
        ivvc0213.getTabOutput().setValStrO("");
    }

    public void initStraDiInvst() {
        ws.getStraDiInvst().setSdiIdStraDiInvst(0);
        ws.getStraDiInvst().setSdiIdOgg(0);
        ws.getStraDiInvst().setSdiTpOgg("");
        ws.getStraDiInvst().setSdiIdMoviCrz(0);
        ws.getStraDiInvst().getSdiIdMoviChiu().setSdiIdMoviChiu(0);
        ws.getStraDiInvst().setSdiDtIniEff(0);
        ws.getStraDiInvst().setSdiDtEndEff(0);
        ws.getStraDiInvst().setSdiCodCompAnia(0);
        ws.getStraDiInvst().setSdiCodStra("");
        ws.getStraDiInvst().getSdiModGest().setSdiModGest(((short)0));
        ws.getStraDiInvst().setSdiVarRifto("");
        ws.getStraDiInvst().setSdiValVarRifto("");
        ws.getStraDiInvst().getSdiDtFis().setSdiDtFis(((short)0));
        ws.getStraDiInvst().getSdiFrqValut().setSdiFrqValut(0);
        ws.getStraDiInvst().setSdiFlIniEndPer(Types.SPACE_CHAR);
        ws.getStraDiInvst().setSdiDsRiga(0);
        ws.getStraDiInvst().setSdiDsOperSql(Types.SPACE_CHAR);
        ws.getStraDiInvst().setSdiDsVer(0);
        ws.getStraDiInvst().setSdiDsTsIniCptz(0);
        ws.getStraDiInvst().setSdiDsTsEndCptz(0);
        ws.getStraDiInvst().setSdiDsUtente("");
        ws.getStraDiInvst().setSdiDsStatoElab(Types.SPACE_CHAR);
        ws.getStraDiInvst().setSdiTpStra("");
        ws.getStraDiInvst().setSdiTpProvzaStra("");
        ws.getStraDiInvst().getSdiPcProtezione().setSdiPcProtezione(new AfDecimal(0, 6, 3));
    }
}
