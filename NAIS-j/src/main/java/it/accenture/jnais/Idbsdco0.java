package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.DCollDao;
import it.accenture.jnais.commons.data.to.IDColl;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.DCollIdbsdco0;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idbsdco0Data;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.redefines.DcoDtScadAdesDflt;
import it.accenture.jnais.ws.redefines.DcoDtUltRinnTac;
import it.accenture.jnais.ws.redefines.DcoDurAaAdesDflt;
import it.accenture.jnais.ws.redefines.DcoDurGgAdesDflt;
import it.accenture.jnais.ws.redefines.DcoDurMmAdesDflt;
import it.accenture.jnais.ws.redefines.DcoEtaScadFemmDflt;
import it.accenture.jnais.ws.redefines.DcoEtaScadMascDflt;
import it.accenture.jnais.ws.redefines.DcoFrazDflt;
import it.accenture.jnais.ws.redefines.DcoIdMoviChiu;
import it.accenture.jnais.ws.redefines.DcoImpArrotPre;
import it.accenture.jnais.ws.redefines.DcoImpScon;
import it.accenture.jnais.ws.redefines.DcoPcScon;

/**Original name: IDBSDCO0<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  04 SET 2008.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Idbsdco0 extends Program implements IDColl {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private DCollDao dCollDao = new DCollDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Idbsdco0Data ws = new Idbsdco0Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: D-COLL
    private DCollIdbsdco0 dColl;

    //==== METHODS ====
    /**Original name: PROGRAM_IDBSDCO0_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, DCollIdbsdco0 dColl) {
        this.idsv0003 = idsv0003;
        this.dColl = dColl;
        // COB_CODE: PERFORM A000-INIZIO                    THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO          THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS          THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO          THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                        a300ElaboraIdEff();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                        a400ElaboraIdpEff();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO          THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS          THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO          THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                        b300ElaboraIdCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                        b400ElaboraIdpCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                        b500ElaboraIboCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                        b600ElaboraIbsCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                        b700ElaboraIdoCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //              SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-PRIMARY-KEY
                //                 PERFORM A200-ELABORA-PK          THRU A200-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO         THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS         THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO         THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.PRIMARY_KEY:// COB_CODE: PERFORM A200-ELABORA-PK          THRU A200-EX
                        a200ElaboraPk();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO         THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS         THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO         THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Idbsdco0 getInstance() {
        return ((Idbsdco0)Programs.getInstance(Idbsdco0.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'IDBSDCO0'   TO IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("IDBSDCO0");
        // COB_CODE: MOVE 'D_COLL' TO IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("D_COLL");
        // COB_CODE: MOVE '00'                     TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                   TO   IDSV0003-SQLCODE
        //                                              IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                              IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-PK<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a200ElaboraPk() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-PK          THRU A210-EX
        //              WHEN IDSV0003-INSERT
        //                 PERFORM A220-INSERT-PK          THRU A220-EX
        //              WHEN IDSV0003-UPDATE
        //                 PERFORM A230-UPDATE-PK          THRU A230-EX
        //              WHEN IDSV0003-DELETE
        //                 PERFORM A240-DELETE-PK          THRU A240-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-PK          THRU A210-EX
            a210SelectPk();
        }
        else if (idsv0003.getOperazione().isInsert()) {
            // COB_CODE: PERFORM A220-INSERT-PK          THRU A220-EX
            a220InsertPk();
        }
        else if (idsv0003.getOperazione().isUpdate()) {
            // COB_CODE: PERFORM A230-UPDATE-PK          THRU A230-EX
            a230UpdatePk();
        }
        else if (idsv0003.getOperazione().isDelete()) {
            // COB_CODE: PERFORM A240-DELETE-PK          THRU A240-EX
            a240DeletePk();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A300-ELABORA-ID-EFF<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void a300ElaboraIdEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A310-SELECT-ID-EFF          THRU A310-EX
            a310SelectIdEff();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A400-ELABORA-IDP-EFF<br>*/
    private void a400ElaboraIdpEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
            a410SelectIdpEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
            a460OpenCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
            a470CloseCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
            a480FetchFirstIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
            a490FetchNextIdpEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A500-ELABORA-IBO<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a500ElaboraIbo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A510-SELECT-IBO             THRU A510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A510-SELECT-IBO             THRU A510-EX
            a510SelectIbo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
            a560OpenCursorIbo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
            a570CloseCursorIbo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
            a580FetchFirstIbo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
            a590FetchNextIbo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A600-ELABORA-IBS<br>*/
    private void a600ElaboraIbs() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A610-SELECT-IBS             THRU A610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A610-SELECT-IBS             THRU A610-EX
            a610SelectIbs();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
            a660OpenCursorIbs();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
            a670CloseCursorIbs();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
            a680FetchFirstIbs();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
            a690FetchNextIbs();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A700-ELABORA-IDO<br>*/
    private void a700ElaboraIdo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A710-SELECT-IDO                 THRU A710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A710-SELECT-IDO                 THRU A710-EX
            a710SelectIdo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
            a760OpenCursorIdo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
            a770CloseCursorIdo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
            a780FetchFirstIdo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
            a790FetchNextIdo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B300-ELABORA-ID-CPZ<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void b300ElaboraIdCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
            b310SelectIdCpz();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B400-ELABORA-IDP-CPZ<br>*/
    private void b400ElaboraIdpCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
            b410SelectIdpCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
            b460OpenCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
            b470CloseCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
            b480FetchFirstIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
            b490FetchNextIdpCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B500-ELABORA-IBO-CPZ<br>*/
    private void b500ElaboraIboCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
            b510SelectIboCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
            b560OpenCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
            b570CloseCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
            b580FetchFirstIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B600-ELABORA-IBS-CPZ<br>*/
    private void b600ElaboraIbsCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
            b610SelectIbsCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
            b660OpenCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
            b670CloseCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
            b680FetchFirstIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B700-ELABORA-IDO-CPZ<br>*/
    private void b700ElaboraIdoCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
            b710SelectIdoCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
            b760OpenCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
            b770CloseCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
            b780FetchFirstIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A210-SELECT-PK<br>
	 * <pre>----
	 * ----  gestione PK
	 * ----</pre>*/
    private void a210SelectPk() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_D_COLL
        //                ,ID_POLI
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,IMP_ARROT_PRE
        //                ,PC_SCON
        //                ,FL_ADES_SING
        //                ,TP_IMP
        //                ,FL_RICL_PRE_DA_CPT
        //                ,TP_ADES
        //                ,DT_ULT_RINN_TAC
        //                ,IMP_SCON
        //                ,FRAZ_DFLT
        //                ,ETA_SCAD_MASC_DFLT
        //                ,ETA_SCAD_FEMM_DFLT
        //                ,TP_DFLT_DUR
        //                ,DUR_AA_ADES_DFLT
        //                ,DUR_MM_ADES_DFLT
        //                ,DUR_GG_ADES_DFLT
        //                ,DT_SCAD_ADES_DFLT
        //                ,COD_FND_DFLT
        //                ,TP_DUR
        //                ,TP_CALC_DUR
        //                ,FL_NO_ADERENTI
        //                ,FL_DISTINTA_CNBTVA
        //                ,FL_CNBT_AUTES
        //                ,FL_QTZ_POST_EMIS
        //                ,FL_COMNZ_FND_IS
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //             INTO
        //                :DCO-ID-D-COLL
        //               ,:DCO-ID-POLI
        //               ,:DCO-ID-MOVI-CRZ
        //               ,:DCO-ID-MOVI-CHIU
        //                :IND-DCO-ID-MOVI-CHIU
        //               ,:DCO-DT-INI-EFF-DB
        //               ,:DCO-DT-END-EFF-DB
        //               ,:DCO-COD-COMP-ANIA
        //               ,:DCO-IMP-ARROT-PRE
        //                :IND-DCO-IMP-ARROT-PRE
        //               ,:DCO-PC-SCON
        //                :IND-DCO-PC-SCON
        //               ,:DCO-FL-ADES-SING
        //                :IND-DCO-FL-ADES-SING
        //               ,:DCO-TP-IMP
        //                :IND-DCO-TP-IMP
        //               ,:DCO-FL-RICL-PRE-DA-CPT
        //                :IND-DCO-FL-RICL-PRE-DA-CPT
        //               ,:DCO-TP-ADES
        //                :IND-DCO-TP-ADES
        //               ,:DCO-DT-ULT-RINN-TAC-DB
        //                :IND-DCO-DT-ULT-RINN-TAC
        //               ,:DCO-IMP-SCON
        //                :IND-DCO-IMP-SCON
        //               ,:DCO-FRAZ-DFLT
        //                :IND-DCO-FRAZ-DFLT
        //               ,:DCO-ETA-SCAD-MASC-DFLT
        //                :IND-DCO-ETA-SCAD-MASC-DFLT
        //               ,:DCO-ETA-SCAD-FEMM-DFLT
        //                :IND-DCO-ETA-SCAD-FEMM-DFLT
        //               ,:DCO-TP-DFLT-DUR
        //                :IND-DCO-TP-DFLT-DUR
        //               ,:DCO-DUR-AA-ADES-DFLT
        //                :IND-DCO-DUR-AA-ADES-DFLT
        //               ,:DCO-DUR-MM-ADES-DFLT
        //                :IND-DCO-DUR-MM-ADES-DFLT
        //               ,:DCO-DUR-GG-ADES-DFLT
        //                :IND-DCO-DUR-GG-ADES-DFLT
        //               ,:DCO-DT-SCAD-ADES-DFLT-DB
        //                :IND-DCO-DT-SCAD-ADES-DFLT
        //               ,:DCO-COD-FND-DFLT
        //                :IND-DCO-COD-FND-DFLT
        //               ,:DCO-TP-DUR
        //                :IND-DCO-TP-DUR
        //               ,:DCO-TP-CALC-DUR
        //                :IND-DCO-TP-CALC-DUR
        //               ,:DCO-FL-NO-ADERENTI
        //                :IND-DCO-FL-NO-ADERENTI
        //               ,:DCO-FL-DISTINTA-CNBTVA
        //                :IND-DCO-FL-DISTINTA-CNBTVA
        //               ,:DCO-FL-CNBT-AUTES
        //                :IND-DCO-FL-CNBT-AUTES
        //               ,:DCO-FL-QTZ-POST-EMIS
        //                :IND-DCO-FL-QTZ-POST-EMIS
        //               ,:DCO-FL-COMNZ-FND-IS
        //                :IND-DCO-FL-COMNZ-FND-IS
        //               ,:DCO-DS-RIGA
        //               ,:DCO-DS-OPER-SQL
        //               ,:DCO-DS-VER
        //               ,:DCO-DS-TS-INI-CPTZ
        //               ,:DCO-DS-TS-END-CPTZ
        //               ,:DCO-DS-UTENTE
        //               ,:DCO-DS-STATO-ELAB
        //             FROM D_COLL
        //             WHERE     DS_RIGA = :DCO-DS-RIGA
        //           END-EXEC.
        dCollDao.selectByDcoDsRiga(dColl.getDcoDsRiga(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A220-INSERT-PK<br>*/
    private void a220InsertPk() {
        // COB_CODE: PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.
        z400SeqRiga();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX
            z150ValorizzaDataServicesI();
            // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX
            z200SetIndicatoriNull();
            // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX
            z900ConvertiNToX();
            // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX
            z960LengthVchar();
            // COB_CODE: EXEC SQL
            //              INSERT
            //              INTO D_COLL
            //                  (
            //                     ID_D_COLL
            //                    ,ID_POLI
            //                    ,ID_MOVI_CRZ
            //                    ,ID_MOVI_CHIU
            //                    ,DT_INI_EFF
            //                    ,DT_END_EFF
            //                    ,COD_COMP_ANIA
            //                    ,IMP_ARROT_PRE
            //                    ,PC_SCON
            //                    ,FL_ADES_SING
            //                    ,TP_IMP
            //                    ,FL_RICL_PRE_DA_CPT
            //                    ,TP_ADES
            //                    ,DT_ULT_RINN_TAC
            //                    ,IMP_SCON
            //                    ,FRAZ_DFLT
            //                    ,ETA_SCAD_MASC_DFLT
            //                    ,ETA_SCAD_FEMM_DFLT
            //                    ,TP_DFLT_DUR
            //                    ,DUR_AA_ADES_DFLT
            //                    ,DUR_MM_ADES_DFLT
            //                    ,DUR_GG_ADES_DFLT
            //                    ,DT_SCAD_ADES_DFLT
            //                    ,COD_FND_DFLT
            //                    ,TP_DUR
            //                    ,TP_CALC_DUR
            //                    ,FL_NO_ADERENTI
            //                    ,FL_DISTINTA_CNBTVA
            //                    ,FL_CNBT_AUTES
            //                    ,FL_QTZ_POST_EMIS
            //                    ,FL_COMNZ_FND_IS
            //                    ,DS_RIGA
            //                    ,DS_OPER_SQL
            //                    ,DS_VER
            //                    ,DS_TS_INI_CPTZ
            //                    ,DS_TS_END_CPTZ
            //                    ,DS_UTENTE
            //                    ,DS_STATO_ELAB
            //                  )
            //              VALUES
            //                  (
            //                    :DCO-ID-D-COLL
            //                    ,:DCO-ID-POLI
            //                    ,:DCO-ID-MOVI-CRZ
            //                    ,:DCO-ID-MOVI-CHIU
            //                     :IND-DCO-ID-MOVI-CHIU
            //                    ,:DCO-DT-INI-EFF-DB
            //                    ,:DCO-DT-END-EFF-DB
            //                    ,:DCO-COD-COMP-ANIA
            //                    ,:DCO-IMP-ARROT-PRE
            //                     :IND-DCO-IMP-ARROT-PRE
            //                    ,:DCO-PC-SCON
            //                     :IND-DCO-PC-SCON
            //                    ,:DCO-FL-ADES-SING
            //                     :IND-DCO-FL-ADES-SING
            //                    ,:DCO-TP-IMP
            //                     :IND-DCO-TP-IMP
            //                    ,:DCO-FL-RICL-PRE-DA-CPT
            //                     :IND-DCO-FL-RICL-PRE-DA-CPT
            //                    ,:DCO-TP-ADES
            //                     :IND-DCO-TP-ADES
            //                    ,:DCO-DT-ULT-RINN-TAC-DB
            //                     :IND-DCO-DT-ULT-RINN-TAC
            //                    ,:DCO-IMP-SCON
            //                     :IND-DCO-IMP-SCON
            //                    ,:DCO-FRAZ-DFLT
            //                     :IND-DCO-FRAZ-DFLT
            //                    ,:DCO-ETA-SCAD-MASC-DFLT
            //                     :IND-DCO-ETA-SCAD-MASC-DFLT
            //                    ,:DCO-ETA-SCAD-FEMM-DFLT
            //                     :IND-DCO-ETA-SCAD-FEMM-DFLT
            //                    ,:DCO-TP-DFLT-DUR
            //                     :IND-DCO-TP-DFLT-DUR
            //                    ,:DCO-DUR-AA-ADES-DFLT
            //                     :IND-DCO-DUR-AA-ADES-DFLT
            //                    ,:DCO-DUR-MM-ADES-DFLT
            //                     :IND-DCO-DUR-MM-ADES-DFLT
            //                    ,:DCO-DUR-GG-ADES-DFLT
            //                     :IND-DCO-DUR-GG-ADES-DFLT
            //                    ,:DCO-DT-SCAD-ADES-DFLT-DB
            //                     :IND-DCO-DT-SCAD-ADES-DFLT
            //                    ,:DCO-COD-FND-DFLT
            //                     :IND-DCO-COD-FND-DFLT
            //                    ,:DCO-TP-DUR
            //                     :IND-DCO-TP-DUR
            //                    ,:DCO-TP-CALC-DUR
            //                     :IND-DCO-TP-CALC-DUR
            //                    ,:DCO-FL-NO-ADERENTI
            //                     :IND-DCO-FL-NO-ADERENTI
            //                    ,:DCO-FL-DISTINTA-CNBTVA
            //                     :IND-DCO-FL-DISTINTA-CNBTVA
            //                    ,:DCO-FL-CNBT-AUTES
            //                     :IND-DCO-FL-CNBT-AUTES
            //                    ,:DCO-FL-QTZ-POST-EMIS
            //                     :IND-DCO-FL-QTZ-POST-EMIS
            //                    ,:DCO-FL-COMNZ-FND-IS
            //                     :IND-DCO-FL-COMNZ-FND-IS
            //                    ,:DCO-DS-RIGA
            //                    ,:DCO-DS-OPER-SQL
            //                    ,:DCO-DS-VER
            //                    ,:DCO-DS-TS-INI-CPTZ
            //                    ,:DCO-DS-TS-END-CPTZ
            //                    ,:DCO-DS-UTENTE
            //                    ,:DCO-DS-STATO-ELAB
            //                  )
            //           END-EXEC
            dCollDao.insertRec(this);
            // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
            a100CheckReturnCode();
        }
    }

    /**Original name: A230-UPDATE-PK<br>*/
    private void a230UpdatePk() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE D_COLL SET
        //                   ID_D_COLL              =
        //                :DCO-ID-D-COLL
        //                  ,ID_POLI                =
        //                :DCO-ID-POLI
        //                  ,ID_MOVI_CRZ            =
        //                :DCO-ID-MOVI-CRZ
        //                  ,ID_MOVI_CHIU           =
        //                :DCO-ID-MOVI-CHIU
        //                                       :IND-DCO-ID-MOVI-CHIU
        //                  ,DT_INI_EFF             =
        //           :DCO-DT-INI-EFF-DB
        //                  ,DT_END_EFF             =
        //           :DCO-DT-END-EFF-DB
        //                  ,COD_COMP_ANIA          =
        //                :DCO-COD-COMP-ANIA
        //                  ,IMP_ARROT_PRE          =
        //                :DCO-IMP-ARROT-PRE
        //                                       :IND-DCO-IMP-ARROT-PRE
        //                  ,PC_SCON                =
        //                :DCO-PC-SCON
        //                                       :IND-DCO-PC-SCON
        //                  ,FL_ADES_SING           =
        //                :DCO-FL-ADES-SING
        //                                       :IND-DCO-FL-ADES-SING
        //                  ,TP_IMP                 =
        //                :DCO-TP-IMP
        //                                       :IND-DCO-TP-IMP
        //                  ,FL_RICL_PRE_DA_CPT     =
        //                :DCO-FL-RICL-PRE-DA-CPT
        //                                       :IND-DCO-FL-RICL-PRE-DA-CPT
        //                  ,TP_ADES                =
        //                :DCO-TP-ADES
        //                                       :IND-DCO-TP-ADES
        //                  ,DT_ULT_RINN_TAC        =
        //           :DCO-DT-ULT-RINN-TAC-DB
        //                                       :IND-DCO-DT-ULT-RINN-TAC
        //                  ,IMP_SCON               =
        //                :DCO-IMP-SCON
        //                                       :IND-DCO-IMP-SCON
        //                  ,FRAZ_DFLT              =
        //                :DCO-FRAZ-DFLT
        //                                       :IND-DCO-FRAZ-DFLT
        //                  ,ETA_SCAD_MASC_DFLT     =
        //                :DCO-ETA-SCAD-MASC-DFLT
        //                                       :IND-DCO-ETA-SCAD-MASC-DFLT
        //                  ,ETA_SCAD_FEMM_DFLT     =
        //                :DCO-ETA-SCAD-FEMM-DFLT
        //                                       :IND-DCO-ETA-SCAD-FEMM-DFLT
        //                  ,TP_DFLT_DUR            =
        //                :DCO-TP-DFLT-DUR
        //                                       :IND-DCO-TP-DFLT-DUR
        //                  ,DUR_AA_ADES_DFLT       =
        //                :DCO-DUR-AA-ADES-DFLT
        //                                       :IND-DCO-DUR-AA-ADES-DFLT
        //                  ,DUR_MM_ADES_DFLT       =
        //                :DCO-DUR-MM-ADES-DFLT
        //                                       :IND-DCO-DUR-MM-ADES-DFLT
        //                  ,DUR_GG_ADES_DFLT       =
        //                :DCO-DUR-GG-ADES-DFLT
        //                                       :IND-DCO-DUR-GG-ADES-DFLT
        //                  ,DT_SCAD_ADES_DFLT      =
        //           :DCO-DT-SCAD-ADES-DFLT-DB
        //                                       :IND-DCO-DT-SCAD-ADES-DFLT
        //                  ,COD_FND_DFLT           =
        //                :DCO-COD-FND-DFLT
        //                                       :IND-DCO-COD-FND-DFLT
        //                  ,TP_DUR                 =
        //                :DCO-TP-DUR
        //                                       :IND-DCO-TP-DUR
        //                  ,TP_CALC_DUR            =
        //                :DCO-TP-CALC-DUR
        //                                       :IND-DCO-TP-CALC-DUR
        //                  ,FL_NO_ADERENTI         =
        //                :DCO-FL-NO-ADERENTI
        //                                       :IND-DCO-FL-NO-ADERENTI
        //                  ,FL_DISTINTA_CNBTVA     =
        //                :DCO-FL-DISTINTA-CNBTVA
        //                                       :IND-DCO-FL-DISTINTA-CNBTVA
        //                  ,FL_CNBT_AUTES          =
        //                :DCO-FL-CNBT-AUTES
        //                                       :IND-DCO-FL-CNBT-AUTES
        //                  ,FL_QTZ_POST_EMIS       =
        //                :DCO-FL-QTZ-POST-EMIS
        //                                       :IND-DCO-FL-QTZ-POST-EMIS
        //                  ,FL_COMNZ_FND_IS        =
        //                :DCO-FL-COMNZ-FND-IS
        //                                       :IND-DCO-FL-COMNZ-FND-IS
        //                  ,DS_RIGA                =
        //                :DCO-DS-RIGA
        //                  ,DS_OPER_SQL            =
        //                :DCO-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :DCO-DS-VER
        //                  ,DS_TS_INI_CPTZ         =
        //                :DCO-DS-TS-INI-CPTZ
        //                  ,DS_TS_END_CPTZ         =
        //                :DCO-DS-TS-END-CPTZ
        //                  ,DS_UTENTE              =
        //                :DCO-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :DCO-DS-STATO-ELAB
        //                WHERE     DS_RIGA = :DCO-DS-RIGA
        //           END-EXEC.
        dCollDao.updateRec(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A240-DELETE-PK<br>*/
    private void a240DeletePk() {
        // COB_CODE: EXEC SQL
        //                DELETE
        //                FROM D_COLL
        //                WHERE     DS_RIGA = :DCO-DS-RIGA
        //           END-EXEC.
        dCollDao.deleteByDcoDsRiga(dColl.getDcoDsRiga());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A305-DECLARE-CURSOR-ID-EFF<br>
	 * <pre>----
	 * ----  gestione ID Effetto
	 * ----</pre>*/
    private void a305DeclareCursorIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-ID-UPD-EFF-DCO CURSOR FOR
        //              SELECT
        //                     ID_D_COLL
        //                    ,ID_POLI
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,IMP_ARROT_PRE
        //                    ,PC_SCON
        //                    ,FL_ADES_SING
        //                    ,TP_IMP
        //                    ,FL_RICL_PRE_DA_CPT
        //                    ,TP_ADES
        //                    ,DT_ULT_RINN_TAC
        //                    ,IMP_SCON
        //                    ,FRAZ_DFLT
        //                    ,ETA_SCAD_MASC_DFLT
        //                    ,ETA_SCAD_FEMM_DFLT
        //                    ,TP_DFLT_DUR
        //                    ,DUR_AA_ADES_DFLT
        //                    ,DUR_MM_ADES_DFLT
        //                    ,DUR_GG_ADES_DFLT
        //                    ,DT_SCAD_ADES_DFLT
        //                    ,COD_FND_DFLT
        //                    ,TP_DUR
        //                    ,TP_CALC_DUR
        //                    ,FL_NO_ADERENTI
        //                    ,FL_DISTINTA_CNBTVA
        //                    ,FL_CNBT_AUTES
        //                    ,FL_QTZ_POST_EMIS
        //                    ,FL_COMNZ_FND_IS
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //              FROM D_COLL
        //              WHERE     ID_D_COLL = :DCO-ID-D-COLL
        //                    AND DS_TS_END_CPTZ = :WS-TS-INFINITO
        //                    AND DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //              ORDER BY DT_INI_EFF ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A310-SELECT-ID-EFF<br>*/
    private void a310SelectIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_D_COLL
        //                ,ID_POLI
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,IMP_ARROT_PRE
        //                ,PC_SCON
        //                ,FL_ADES_SING
        //                ,TP_IMP
        //                ,FL_RICL_PRE_DA_CPT
        //                ,TP_ADES
        //                ,DT_ULT_RINN_TAC
        //                ,IMP_SCON
        //                ,FRAZ_DFLT
        //                ,ETA_SCAD_MASC_DFLT
        //                ,ETA_SCAD_FEMM_DFLT
        //                ,TP_DFLT_DUR
        //                ,DUR_AA_ADES_DFLT
        //                ,DUR_MM_ADES_DFLT
        //                ,DUR_GG_ADES_DFLT
        //                ,DT_SCAD_ADES_DFLT
        //                ,COD_FND_DFLT
        //                ,TP_DUR
        //                ,TP_CALC_DUR
        //                ,FL_NO_ADERENTI
        //                ,FL_DISTINTA_CNBTVA
        //                ,FL_CNBT_AUTES
        //                ,FL_QTZ_POST_EMIS
        //                ,FL_COMNZ_FND_IS
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //             INTO
        //                :DCO-ID-D-COLL
        //               ,:DCO-ID-POLI
        //               ,:DCO-ID-MOVI-CRZ
        //               ,:DCO-ID-MOVI-CHIU
        //                :IND-DCO-ID-MOVI-CHIU
        //               ,:DCO-DT-INI-EFF-DB
        //               ,:DCO-DT-END-EFF-DB
        //               ,:DCO-COD-COMP-ANIA
        //               ,:DCO-IMP-ARROT-PRE
        //                :IND-DCO-IMP-ARROT-PRE
        //               ,:DCO-PC-SCON
        //                :IND-DCO-PC-SCON
        //               ,:DCO-FL-ADES-SING
        //                :IND-DCO-FL-ADES-SING
        //               ,:DCO-TP-IMP
        //                :IND-DCO-TP-IMP
        //               ,:DCO-FL-RICL-PRE-DA-CPT
        //                :IND-DCO-FL-RICL-PRE-DA-CPT
        //               ,:DCO-TP-ADES
        //                :IND-DCO-TP-ADES
        //               ,:DCO-DT-ULT-RINN-TAC-DB
        //                :IND-DCO-DT-ULT-RINN-TAC
        //               ,:DCO-IMP-SCON
        //                :IND-DCO-IMP-SCON
        //               ,:DCO-FRAZ-DFLT
        //                :IND-DCO-FRAZ-DFLT
        //               ,:DCO-ETA-SCAD-MASC-DFLT
        //                :IND-DCO-ETA-SCAD-MASC-DFLT
        //               ,:DCO-ETA-SCAD-FEMM-DFLT
        //                :IND-DCO-ETA-SCAD-FEMM-DFLT
        //               ,:DCO-TP-DFLT-DUR
        //                :IND-DCO-TP-DFLT-DUR
        //               ,:DCO-DUR-AA-ADES-DFLT
        //                :IND-DCO-DUR-AA-ADES-DFLT
        //               ,:DCO-DUR-MM-ADES-DFLT
        //                :IND-DCO-DUR-MM-ADES-DFLT
        //               ,:DCO-DUR-GG-ADES-DFLT
        //                :IND-DCO-DUR-GG-ADES-DFLT
        //               ,:DCO-DT-SCAD-ADES-DFLT-DB
        //                :IND-DCO-DT-SCAD-ADES-DFLT
        //               ,:DCO-COD-FND-DFLT
        //                :IND-DCO-COD-FND-DFLT
        //               ,:DCO-TP-DUR
        //                :IND-DCO-TP-DUR
        //               ,:DCO-TP-CALC-DUR
        //                :IND-DCO-TP-CALC-DUR
        //               ,:DCO-FL-NO-ADERENTI
        //                :IND-DCO-FL-NO-ADERENTI
        //               ,:DCO-FL-DISTINTA-CNBTVA
        //                :IND-DCO-FL-DISTINTA-CNBTVA
        //               ,:DCO-FL-CNBT-AUTES
        //                :IND-DCO-FL-CNBT-AUTES
        //               ,:DCO-FL-QTZ-POST-EMIS
        //                :IND-DCO-FL-QTZ-POST-EMIS
        //               ,:DCO-FL-COMNZ-FND-IS
        //                :IND-DCO-FL-COMNZ-FND-IS
        //               ,:DCO-DS-RIGA
        //               ,:DCO-DS-OPER-SQL
        //               ,:DCO-DS-VER
        //               ,:DCO-DS-TS-INI-CPTZ
        //               ,:DCO-DS-TS-END-CPTZ
        //               ,:DCO-DS-UTENTE
        //               ,:DCO-DS-STATO-ELAB
        //             FROM D_COLL
        //             WHERE     ID_D_COLL = :DCO-ID-D-COLL
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        dCollDao.selectRec(dColl.getDcoIdDColl(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A330-UPDATE-ID-EFF<br>*/
    private void a330UpdateIdEff() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE D_COLL SET
        //                   ID_D_COLL              =
        //                :DCO-ID-D-COLL
        //                  ,ID_POLI                =
        //                :DCO-ID-POLI
        //                  ,ID_MOVI_CRZ            =
        //                :DCO-ID-MOVI-CRZ
        //                  ,ID_MOVI_CHIU           =
        //                :DCO-ID-MOVI-CHIU
        //                                       :IND-DCO-ID-MOVI-CHIU
        //                  ,DT_INI_EFF             =
        //           :DCO-DT-INI-EFF-DB
        //                  ,DT_END_EFF             =
        //           :DCO-DT-END-EFF-DB
        //                  ,COD_COMP_ANIA          =
        //                :DCO-COD-COMP-ANIA
        //                  ,IMP_ARROT_PRE          =
        //                :DCO-IMP-ARROT-PRE
        //                                       :IND-DCO-IMP-ARROT-PRE
        //                  ,PC_SCON                =
        //                :DCO-PC-SCON
        //                                       :IND-DCO-PC-SCON
        //                  ,FL_ADES_SING           =
        //                :DCO-FL-ADES-SING
        //                                       :IND-DCO-FL-ADES-SING
        //                  ,TP_IMP                 =
        //                :DCO-TP-IMP
        //                                       :IND-DCO-TP-IMP
        //                  ,FL_RICL_PRE_DA_CPT     =
        //                :DCO-FL-RICL-PRE-DA-CPT
        //                                       :IND-DCO-FL-RICL-PRE-DA-CPT
        //                  ,TP_ADES                =
        //                :DCO-TP-ADES
        //                                       :IND-DCO-TP-ADES
        //                  ,DT_ULT_RINN_TAC        =
        //           :DCO-DT-ULT-RINN-TAC-DB
        //                                       :IND-DCO-DT-ULT-RINN-TAC
        //                  ,IMP_SCON               =
        //                :DCO-IMP-SCON
        //                                       :IND-DCO-IMP-SCON
        //                  ,FRAZ_DFLT              =
        //                :DCO-FRAZ-DFLT
        //                                       :IND-DCO-FRAZ-DFLT
        //                  ,ETA_SCAD_MASC_DFLT     =
        //                :DCO-ETA-SCAD-MASC-DFLT
        //                                       :IND-DCO-ETA-SCAD-MASC-DFLT
        //                  ,ETA_SCAD_FEMM_DFLT     =
        //                :DCO-ETA-SCAD-FEMM-DFLT
        //                                       :IND-DCO-ETA-SCAD-FEMM-DFLT
        //                  ,TP_DFLT_DUR            =
        //                :DCO-TP-DFLT-DUR
        //                                       :IND-DCO-TP-DFLT-DUR
        //                  ,DUR_AA_ADES_DFLT       =
        //                :DCO-DUR-AA-ADES-DFLT
        //                                       :IND-DCO-DUR-AA-ADES-DFLT
        //                  ,DUR_MM_ADES_DFLT       =
        //                :DCO-DUR-MM-ADES-DFLT
        //                                       :IND-DCO-DUR-MM-ADES-DFLT
        //                  ,DUR_GG_ADES_DFLT       =
        //                :DCO-DUR-GG-ADES-DFLT
        //                                       :IND-DCO-DUR-GG-ADES-DFLT
        //                  ,DT_SCAD_ADES_DFLT      =
        //           :DCO-DT-SCAD-ADES-DFLT-DB
        //                                       :IND-DCO-DT-SCAD-ADES-DFLT
        //                  ,COD_FND_DFLT           =
        //                :DCO-COD-FND-DFLT
        //                                       :IND-DCO-COD-FND-DFLT
        //                  ,TP_DUR                 =
        //                :DCO-TP-DUR
        //                                       :IND-DCO-TP-DUR
        //                  ,TP_CALC_DUR            =
        //                :DCO-TP-CALC-DUR
        //                                       :IND-DCO-TP-CALC-DUR
        //                  ,FL_NO_ADERENTI         =
        //                :DCO-FL-NO-ADERENTI
        //                                       :IND-DCO-FL-NO-ADERENTI
        //                  ,FL_DISTINTA_CNBTVA     =
        //                :DCO-FL-DISTINTA-CNBTVA
        //                                       :IND-DCO-FL-DISTINTA-CNBTVA
        //                  ,FL_CNBT_AUTES          =
        //                :DCO-FL-CNBT-AUTES
        //                                       :IND-DCO-FL-CNBT-AUTES
        //                  ,FL_QTZ_POST_EMIS       =
        //                :DCO-FL-QTZ-POST-EMIS
        //                                       :IND-DCO-FL-QTZ-POST-EMIS
        //                  ,FL_COMNZ_FND_IS        =
        //                :DCO-FL-COMNZ-FND-IS
        //                                       :IND-DCO-FL-COMNZ-FND-IS
        //                  ,DS_RIGA                =
        //                :DCO-DS-RIGA
        //                  ,DS_OPER_SQL            =
        //                :DCO-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :DCO-DS-VER
        //                  ,DS_TS_INI_CPTZ         =
        //                :DCO-DS-TS-INI-CPTZ
        //                  ,DS_TS_END_CPTZ         =
        //                :DCO-DS-TS-END-CPTZ
        //                  ,DS_UTENTE              =
        //                :DCO-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :DCO-DS-STATO-ELAB
        //                WHERE     DS_RIGA = :DCO-DS-RIGA
        //                   AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        dCollDao.updateRec1(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A360-OPEN-CURSOR-ID-EFF<br>*/
    private void a360OpenCursorIdEff() {
        // COB_CODE: PERFORM A305-DECLARE-CURSOR-ID-EFF THRU A305-EX.
        a305DeclareCursorIdEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-ID-UPD-EFF-DCO
        //           END-EXEC.
        dCollDao.openCIdUpdEffDco(dColl.getDcoIdDColl(), ws.getIdsv0010().getWsTsInfinito(), ws.getIdsv0010().getWsDataInizioEffettoDb(), idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A370-CLOSE-CURSOR-ID-EFF<br>*/
    private void a370CloseCursorIdEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-ID-UPD-EFF-DCO
        //           END-EXEC.
        dCollDao.closeCIdUpdEffDco();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A390-FETCH-NEXT-ID-EFF<br>*/
    private void a390FetchNextIdEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-ID-UPD-EFF-DCO
        //           INTO
        //                :DCO-ID-D-COLL
        //               ,:DCO-ID-POLI
        //               ,:DCO-ID-MOVI-CRZ
        //               ,:DCO-ID-MOVI-CHIU
        //                :IND-DCO-ID-MOVI-CHIU
        //               ,:DCO-DT-INI-EFF-DB
        //               ,:DCO-DT-END-EFF-DB
        //               ,:DCO-COD-COMP-ANIA
        //               ,:DCO-IMP-ARROT-PRE
        //                :IND-DCO-IMP-ARROT-PRE
        //               ,:DCO-PC-SCON
        //                :IND-DCO-PC-SCON
        //               ,:DCO-FL-ADES-SING
        //                :IND-DCO-FL-ADES-SING
        //               ,:DCO-TP-IMP
        //                :IND-DCO-TP-IMP
        //               ,:DCO-FL-RICL-PRE-DA-CPT
        //                :IND-DCO-FL-RICL-PRE-DA-CPT
        //               ,:DCO-TP-ADES
        //                :IND-DCO-TP-ADES
        //               ,:DCO-DT-ULT-RINN-TAC-DB
        //                :IND-DCO-DT-ULT-RINN-TAC
        //               ,:DCO-IMP-SCON
        //                :IND-DCO-IMP-SCON
        //               ,:DCO-FRAZ-DFLT
        //                :IND-DCO-FRAZ-DFLT
        //               ,:DCO-ETA-SCAD-MASC-DFLT
        //                :IND-DCO-ETA-SCAD-MASC-DFLT
        //               ,:DCO-ETA-SCAD-FEMM-DFLT
        //                :IND-DCO-ETA-SCAD-FEMM-DFLT
        //               ,:DCO-TP-DFLT-DUR
        //                :IND-DCO-TP-DFLT-DUR
        //               ,:DCO-DUR-AA-ADES-DFLT
        //                :IND-DCO-DUR-AA-ADES-DFLT
        //               ,:DCO-DUR-MM-ADES-DFLT
        //                :IND-DCO-DUR-MM-ADES-DFLT
        //               ,:DCO-DUR-GG-ADES-DFLT
        //                :IND-DCO-DUR-GG-ADES-DFLT
        //               ,:DCO-DT-SCAD-ADES-DFLT-DB
        //                :IND-DCO-DT-SCAD-ADES-DFLT
        //               ,:DCO-COD-FND-DFLT
        //                :IND-DCO-COD-FND-DFLT
        //               ,:DCO-TP-DUR
        //                :IND-DCO-TP-DUR
        //               ,:DCO-TP-CALC-DUR
        //                :IND-DCO-TP-CALC-DUR
        //               ,:DCO-FL-NO-ADERENTI
        //                :IND-DCO-FL-NO-ADERENTI
        //               ,:DCO-FL-DISTINTA-CNBTVA
        //                :IND-DCO-FL-DISTINTA-CNBTVA
        //               ,:DCO-FL-CNBT-AUTES
        //                :IND-DCO-FL-CNBT-AUTES
        //               ,:DCO-FL-QTZ-POST-EMIS
        //                :IND-DCO-FL-QTZ-POST-EMIS
        //               ,:DCO-FL-COMNZ-FND-IS
        //                :IND-DCO-FL-COMNZ-FND-IS
        //               ,:DCO-DS-RIGA
        //               ,:DCO-DS-OPER-SQL
        //               ,:DCO-DS-VER
        //               ,:DCO-DS-TS-INI-CPTZ
        //               ,:DCO-DS-TS-END-CPTZ
        //               ,:DCO-DS-UTENTE
        //               ,:DCO-DS-STATO-ELAB
        //           END-EXEC.
        dCollDao.fetchCIdUpdEffDco(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A370-CLOSE-CURSOR-ID-EFF THRU A370-EX
            a370CloseCursorIdEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A405-DECLARE-CURSOR-IDP-EFF<br>
	 * <pre>----
	 * ----  gestione IDP Effetto
	 * ----</pre>*/
    private void a405DeclareCursorIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IDP-EFF-DCO CURSOR FOR
        //              SELECT
        //                     ID_D_COLL
        //                    ,ID_POLI
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,IMP_ARROT_PRE
        //                    ,PC_SCON
        //                    ,FL_ADES_SING
        //                    ,TP_IMP
        //                    ,FL_RICL_PRE_DA_CPT
        //                    ,TP_ADES
        //                    ,DT_ULT_RINN_TAC
        //                    ,IMP_SCON
        //                    ,FRAZ_DFLT
        //                    ,ETA_SCAD_MASC_DFLT
        //                    ,ETA_SCAD_FEMM_DFLT
        //                    ,TP_DFLT_DUR
        //                    ,DUR_AA_ADES_DFLT
        //                    ,DUR_MM_ADES_DFLT
        //                    ,DUR_GG_ADES_DFLT
        //                    ,DT_SCAD_ADES_DFLT
        //                    ,COD_FND_DFLT
        //                    ,TP_DUR
        //                    ,TP_CALC_DUR
        //                    ,FL_NO_ADERENTI
        //                    ,FL_DISTINTA_CNBTVA
        //                    ,FL_CNBT_AUTES
        //                    ,FL_QTZ_POST_EMIS
        //                    ,FL_COMNZ_FND_IS
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //              FROM D_COLL
        //              WHERE     ID_POLI = :DCO-ID-POLI
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //              ORDER BY ID_D_COLL ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A410-SELECT-IDP-EFF<br>*/
    private void a410SelectIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_D_COLL
        //                ,ID_POLI
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,IMP_ARROT_PRE
        //                ,PC_SCON
        //                ,FL_ADES_SING
        //                ,TP_IMP
        //                ,FL_RICL_PRE_DA_CPT
        //                ,TP_ADES
        //                ,DT_ULT_RINN_TAC
        //                ,IMP_SCON
        //                ,FRAZ_DFLT
        //                ,ETA_SCAD_MASC_DFLT
        //                ,ETA_SCAD_FEMM_DFLT
        //                ,TP_DFLT_DUR
        //                ,DUR_AA_ADES_DFLT
        //                ,DUR_MM_ADES_DFLT
        //                ,DUR_GG_ADES_DFLT
        //                ,DT_SCAD_ADES_DFLT
        //                ,COD_FND_DFLT
        //                ,TP_DUR
        //                ,TP_CALC_DUR
        //                ,FL_NO_ADERENTI
        //                ,FL_DISTINTA_CNBTVA
        //                ,FL_CNBT_AUTES
        //                ,FL_QTZ_POST_EMIS
        //                ,FL_COMNZ_FND_IS
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //             INTO
        //                :DCO-ID-D-COLL
        //               ,:DCO-ID-POLI
        //               ,:DCO-ID-MOVI-CRZ
        //               ,:DCO-ID-MOVI-CHIU
        //                :IND-DCO-ID-MOVI-CHIU
        //               ,:DCO-DT-INI-EFF-DB
        //               ,:DCO-DT-END-EFF-DB
        //               ,:DCO-COD-COMP-ANIA
        //               ,:DCO-IMP-ARROT-PRE
        //                :IND-DCO-IMP-ARROT-PRE
        //               ,:DCO-PC-SCON
        //                :IND-DCO-PC-SCON
        //               ,:DCO-FL-ADES-SING
        //                :IND-DCO-FL-ADES-SING
        //               ,:DCO-TP-IMP
        //                :IND-DCO-TP-IMP
        //               ,:DCO-FL-RICL-PRE-DA-CPT
        //                :IND-DCO-FL-RICL-PRE-DA-CPT
        //               ,:DCO-TP-ADES
        //                :IND-DCO-TP-ADES
        //               ,:DCO-DT-ULT-RINN-TAC-DB
        //                :IND-DCO-DT-ULT-RINN-TAC
        //               ,:DCO-IMP-SCON
        //                :IND-DCO-IMP-SCON
        //               ,:DCO-FRAZ-DFLT
        //                :IND-DCO-FRAZ-DFLT
        //               ,:DCO-ETA-SCAD-MASC-DFLT
        //                :IND-DCO-ETA-SCAD-MASC-DFLT
        //               ,:DCO-ETA-SCAD-FEMM-DFLT
        //                :IND-DCO-ETA-SCAD-FEMM-DFLT
        //               ,:DCO-TP-DFLT-DUR
        //                :IND-DCO-TP-DFLT-DUR
        //               ,:DCO-DUR-AA-ADES-DFLT
        //                :IND-DCO-DUR-AA-ADES-DFLT
        //               ,:DCO-DUR-MM-ADES-DFLT
        //                :IND-DCO-DUR-MM-ADES-DFLT
        //               ,:DCO-DUR-GG-ADES-DFLT
        //                :IND-DCO-DUR-GG-ADES-DFLT
        //               ,:DCO-DT-SCAD-ADES-DFLT-DB
        //                :IND-DCO-DT-SCAD-ADES-DFLT
        //               ,:DCO-COD-FND-DFLT
        //                :IND-DCO-COD-FND-DFLT
        //               ,:DCO-TP-DUR
        //                :IND-DCO-TP-DUR
        //               ,:DCO-TP-CALC-DUR
        //                :IND-DCO-TP-CALC-DUR
        //               ,:DCO-FL-NO-ADERENTI
        //                :IND-DCO-FL-NO-ADERENTI
        //               ,:DCO-FL-DISTINTA-CNBTVA
        //                :IND-DCO-FL-DISTINTA-CNBTVA
        //               ,:DCO-FL-CNBT-AUTES
        //                :IND-DCO-FL-CNBT-AUTES
        //               ,:DCO-FL-QTZ-POST-EMIS
        //                :IND-DCO-FL-QTZ-POST-EMIS
        //               ,:DCO-FL-COMNZ-FND-IS
        //                :IND-DCO-FL-COMNZ-FND-IS
        //               ,:DCO-DS-RIGA
        //               ,:DCO-DS-OPER-SQL
        //               ,:DCO-DS-VER
        //               ,:DCO-DS-TS-INI-CPTZ
        //               ,:DCO-DS-TS-END-CPTZ
        //               ,:DCO-DS-UTENTE
        //               ,:DCO-DS-STATO-ELAB
        //             FROM D_COLL
        //             WHERE     ID_POLI = :DCO-ID-POLI
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        dCollDao.selectRec1(dColl.getDcoIdPoli(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A460-OPEN-CURSOR-IDP-EFF<br>*/
    private void a460OpenCursorIdpEff() {
        // COB_CODE: PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.
        a405DeclareCursorIdpEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-IDP-EFF-DCO
        //           END-EXEC.
        dCollDao.openCIdpEffDco(dColl.getDcoIdPoli(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A470-CLOSE-CURSOR-IDP-EFF<br>*/
    private void a470CloseCursorIdpEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IDP-EFF-DCO
        //           END-EXEC.
        dCollDao.closeCIdpEffDco();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A480-FETCH-FIRST-IDP-EFF<br>*/
    private void a480FetchFirstIdpEff() {
        // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.
        a460OpenCursorIdpEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
            a490FetchNextIdpEff();
        }
    }

    /**Original name: A490-FETCH-NEXT-IDP-EFF<br>*/
    private void a490FetchNextIdpEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IDP-EFF-DCO
        //           INTO
        //                :DCO-ID-D-COLL
        //               ,:DCO-ID-POLI
        //               ,:DCO-ID-MOVI-CRZ
        //               ,:DCO-ID-MOVI-CHIU
        //                :IND-DCO-ID-MOVI-CHIU
        //               ,:DCO-DT-INI-EFF-DB
        //               ,:DCO-DT-END-EFF-DB
        //               ,:DCO-COD-COMP-ANIA
        //               ,:DCO-IMP-ARROT-PRE
        //                :IND-DCO-IMP-ARROT-PRE
        //               ,:DCO-PC-SCON
        //                :IND-DCO-PC-SCON
        //               ,:DCO-FL-ADES-SING
        //                :IND-DCO-FL-ADES-SING
        //               ,:DCO-TP-IMP
        //                :IND-DCO-TP-IMP
        //               ,:DCO-FL-RICL-PRE-DA-CPT
        //                :IND-DCO-FL-RICL-PRE-DA-CPT
        //               ,:DCO-TP-ADES
        //                :IND-DCO-TP-ADES
        //               ,:DCO-DT-ULT-RINN-TAC-DB
        //                :IND-DCO-DT-ULT-RINN-TAC
        //               ,:DCO-IMP-SCON
        //                :IND-DCO-IMP-SCON
        //               ,:DCO-FRAZ-DFLT
        //                :IND-DCO-FRAZ-DFLT
        //               ,:DCO-ETA-SCAD-MASC-DFLT
        //                :IND-DCO-ETA-SCAD-MASC-DFLT
        //               ,:DCO-ETA-SCAD-FEMM-DFLT
        //                :IND-DCO-ETA-SCAD-FEMM-DFLT
        //               ,:DCO-TP-DFLT-DUR
        //                :IND-DCO-TP-DFLT-DUR
        //               ,:DCO-DUR-AA-ADES-DFLT
        //                :IND-DCO-DUR-AA-ADES-DFLT
        //               ,:DCO-DUR-MM-ADES-DFLT
        //                :IND-DCO-DUR-MM-ADES-DFLT
        //               ,:DCO-DUR-GG-ADES-DFLT
        //                :IND-DCO-DUR-GG-ADES-DFLT
        //               ,:DCO-DT-SCAD-ADES-DFLT-DB
        //                :IND-DCO-DT-SCAD-ADES-DFLT
        //               ,:DCO-COD-FND-DFLT
        //                :IND-DCO-COD-FND-DFLT
        //               ,:DCO-TP-DUR
        //                :IND-DCO-TP-DUR
        //               ,:DCO-TP-CALC-DUR
        //                :IND-DCO-TP-CALC-DUR
        //               ,:DCO-FL-NO-ADERENTI
        //                :IND-DCO-FL-NO-ADERENTI
        //               ,:DCO-FL-DISTINTA-CNBTVA
        //                :IND-DCO-FL-DISTINTA-CNBTVA
        //               ,:DCO-FL-CNBT-AUTES
        //                :IND-DCO-FL-CNBT-AUTES
        //               ,:DCO-FL-QTZ-POST-EMIS
        //                :IND-DCO-FL-QTZ-POST-EMIS
        //               ,:DCO-FL-COMNZ-FND-IS
        //                :IND-DCO-FL-COMNZ-FND-IS
        //               ,:DCO-DS-RIGA
        //               ,:DCO-DS-OPER-SQL
        //               ,:DCO-DS-VER
        //               ,:DCO-DS-TS-INI-CPTZ
        //               ,:DCO-DS-TS-END-CPTZ
        //               ,:DCO-DS-UTENTE
        //               ,:DCO-DS-STATO-ELAB
        //           END-EXEC.
        dCollDao.fetchCIdpEffDco(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
            a470CloseCursorIdpEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A505-DECLARE-CURSOR-IBO<br>
	 * <pre>----
	 * ----  gestione IBO Effetto e non
	 * ----</pre>*/
    private void a505DeclareCursorIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A510-SELECT-IBO<br>*/
    private void a510SelectIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A560-OPEN-CURSOR-IBO<br>*/
    private void a560OpenCursorIbo() {
        // COB_CODE: PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.
        a505DeclareCursorIbo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A570-CLOSE-CURSOR-IBO<br>*/
    private void a570CloseCursorIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A580-FETCH-FIRST-IBO<br>*/
    private void a580FetchFirstIbo() {
        // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.
        a560OpenCursorIbo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
            a590FetchNextIbo();
        }
    }

    /**Original name: A590-FETCH-NEXT-IBO<br>*/
    private void a590FetchNextIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A605-DECLARE-CURSOR-IBS<br>
	 * <pre>----
	 * ----  gestione IBS Effetto e non
	 * ----</pre>*/
    private void a605DeclareCursorIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A610-SELECT-IBS<br>*/
    private void a610SelectIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A660-OPEN-CURSOR-IBS<br>*/
    private void a660OpenCursorIbs() {
        // COB_CODE: PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.
        a605DeclareCursorIbs();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A670-CLOSE-CURSOR-IBS<br>*/
    private void a670CloseCursorIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A680-FETCH-FIRST-IBS<br>*/
    private void a680FetchFirstIbs() {
        // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.
        a660OpenCursorIbs();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
            a690FetchNextIbs();
        }
    }

    /**Original name: A690-FETCH-NEXT-IBS<br>*/
    private void a690FetchNextIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A705-DECLARE-CURSOR-IDO<br>
	 * <pre>----
	 * ----  gestione IDO Effetto e non
	 * ----</pre>*/
    private void a705DeclareCursorIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A710-SELECT-IDO<br>*/
    private void a710SelectIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A760-OPEN-CURSOR-IDO<br>*/
    private void a760OpenCursorIdo() {
        // COB_CODE: PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.
        a705DeclareCursorIdo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A770-CLOSE-CURSOR-IDO<br>*/
    private void a770CloseCursorIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A780-FETCH-FIRST-IDO<br>*/
    private void a780FetchFirstIdo() {
        // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.
        a760OpenCursorIdo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
            a790FetchNextIdo();
        }
    }

    /**Original name: A790-FETCH-NEXT-IDO<br>*/
    private void a790FetchNextIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B310-SELECT-ID-CPZ<br>
	 * <pre>----
	 * ----  gestione ID Competenza
	 * ----</pre>*/
    private void b310SelectIdCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_D_COLL
        //                ,ID_POLI
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,IMP_ARROT_PRE
        //                ,PC_SCON
        //                ,FL_ADES_SING
        //                ,TP_IMP
        //                ,FL_RICL_PRE_DA_CPT
        //                ,TP_ADES
        //                ,DT_ULT_RINN_TAC
        //                ,IMP_SCON
        //                ,FRAZ_DFLT
        //                ,ETA_SCAD_MASC_DFLT
        //                ,ETA_SCAD_FEMM_DFLT
        //                ,TP_DFLT_DUR
        //                ,DUR_AA_ADES_DFLT
        //                ,DUR_MM_ADES_DFLT
        //                ,DUR_GG_ADES_DFLT
        //                ,DT_SCAD_ADES_DFLT
        //                ,COD_FND_DFLT
        //                ,TP_DUR
        //                ,TP_CALC_DUR
        //                ,FL_NO_ADERENTI
        //                ,FL_DISTINTA_CNBTVA
        //                ,FL_CNBT_AUTES
        //                ,FL_QTZ_POST_EMIS
        //                ,FL_COMNZ_FND_IS
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //             INTO
        //                :DCO-ID-D-COLL
        //               ,:DCO-ID-POLI
        //               ,:DCO-ID-MOVI-CRZ
        //               ,:DCO-ID-MOVI-CHIU
        //                :IND-DCO-ID-MOVI-CHIU
        //               ,:DCO-DT-INI-EFF-DB
        //               ,:DCO-DT-END-EFF-DB
        //               ,:DCO-COD-COMP-ANIA
        //               ,:DCO-IMP-ARROT-PRE
        //                :IND-DCO-IMP-ARROT-PRE
        //               ,:DCO-PC-SCON
        //                :IND-DCO-PC-SCON
        //               ,:DCO-FL-ADES-SING
        //                :IND-DCO-FL-ADES-SING
        //               ,:DCO-TP-IMP
        //                :IND-DCO-TP-IMP
        //               ,:DCO-FL-RICL-PRE-DA-CPT
        //                :IND-DCO-FL-RICL-PRE-DA-CPT
        //               ,:DCO-TP-ADES
        //                :IND-DCO-TP-ADES
        //               ,:DCO-DT-ULT-RINN-TAC-DB
        //                :IND-DCO-DT-ULT-RINN-TAC
        //               ,:DCO-IMP-SCON
        //                :IND-DCO-IMP-SCON
        //               ,:DCO-FRAZ-DFLT
        //                :IND-DCO-FRAZ-DFLT
        //               ,:DCO-ETA-SCAD-MASC-DFLT
        //                :IND-DCO-ETA-SCAD-MASC-DFLT
        //               ,:DCO-ETA-SCAD-FEMM-DFLT
        //                :IND-DCO-ETA-SCAD-FEMM-DFLT
        //               ,:DCO-TP-DFLT-DUR
        //                :IND-DCO-TP-DFLT-DUR
        //               ,:DCO-DUR-AA-ADES-DFLT
        //                :IND-DCO-DUR-AA-ADES-DFLT
        //               ,:DCO-DUR-MM-ADES-DFLT
        //                :IND-DCO-DUR-MM-ADES-DFLT
        //               ,:DCO-DUR-GG-ADES-DFLT
        //                :IND-DCO-DUR-GG-ADES-DFLT
        //               ,:DCO-DT-SCAD-ADES-DFLT-DB
        //                :IND-DCO-DT-SCAD-ADES-DFLT
        //               ,:DCO-COD-FND-DFLT
        //                :IND-DCO-COD-FND-DFLT
        //               ,:DCO-TP-DUR
        //                :IND-DCO-TP-DUR
        //               ,:DCO-TP-CALC-DUR
        //                :IND-DCO-TP-CALC-DUR
        //               ,:DCO-FL-NO-ADERENTI
        //                :IND-DCO-FL-NO-ADERENTI
        //               ,:DCO-FL-DISTINTA-CNBTVA
        //                :IND-DCO-FL-DISTINTA-CNBTVA
        //               ,:DCO-FL-CNBT-AUTES
        //                :IND-DCO-FL-CNBT-AUTES
        //               ,:DCO-FL-QTZ-POST-EMIS
        //                :IND-DCO-FL-QTZ-POST-EMIS
        //               ,:DCO-FL-COMNZ-FND-IS
        //                :IND-DCO-FL-COMNZ-FND-IS
        //               ,:DCO-DS-RIGA
        //               ,:DCO-DS-OPER-SQL
        //               ,:DCO-DS-VER
        //               ,:DCO-DS-TS-INI-CPTZ
        //               ,:DCO-DS-TS-END-CPTZ
        //               ,:DCO-DS-UTENTE
        //               ,:DCO-DS-STATO-ELAB
        //             FROM D_COLL
        //             WHERE     ID_D_COLL = :DCO-ID-D-COLL
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        dCollDao.selectRec2(dColl.getDcoIdDColl(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B405-DECLARE-CURSOR-IDP-CPZ<br>
	 * <pre>----
	 * ----  gestione IDP Competenza
	 * ----</pre>*/
    private void b405DeclareCursorIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IDP-CPZ-DCO CURSOR FOR
        //              SELECT
        //                     ID_D_COLL
        //                    ,ID_POLI
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,IMP_ARROT_PRE
        //                    ,PC_SCON
        //                    ,FL_ADES_SING
        //                    ,TP_IMP
        //                    ,FL_RICL_PRE_DA_CPT
        //                    ,TP_ADES
        //                    ,DT_ULT_RINN_TAC
        //                    ,IMP_SCON
        //                    ,FRAZ_DFLT
        //                    ,ETA_SCAD_MASC_DFLT
        //                    ,ETA_SCAD_FEMM_DFLT
        //                    ,TP_DFLT_DUR
        //                    ,DUR_AA_ADES_DFLT
        //                    ,DUR_MM_ADES_DFLT
        //                    ,DUR_GG_ADES_DFLT
        //                    ,DT_SCAD_ADES_DFLT
        //                    ,COD_FND_DFLT
        //                    ,TP_DUR
        //                    ,TP_CALC_DUR
        //                    ,FL_NO_ADERENTI
        //                    ,FL_DISTINTA_CNBTVA
        //                    ,FL_CNBT_AUTES
        //                    ,FL_QTZ_POST_EMIS
        //                    ,FL_COMNZ_FND_IS
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //              FROM D_COLL
        //              WHERE     ID_POLI = :DCO-ID-POLI
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //              ORDER BY ID_D_COLL ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B410-SELECT-IDP-CPZ<br>*/
    private void b410SelectIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_D_COLL
        //                ,ID_POLI
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,IMP_ARROT_PRE
        //                ,PC_SCON
        //                ,FL_ADES_SING
        //                ,TP_IMP
        //                ,FL_RICL_PRE_DA_CPT
        //                ,TP_ADES
        //                ,DT_ULT_RINN_TAC
        //                ,IMP_SCON
        //                ,FRAZ_DFLT
        //                ,ETA_SCAD_MASC_DFLT
        //                ,ETA_SCAD_FEMM_DFLT
        //                ,TP_DFLT_DUR
        //                ,DUR_AA_ADES_DFLT
        //                ,DUR_MM_ADES_DFLT
        //                ,DUR_GG_ADES_DFLT
        //                ,DT_SCAD_ADES_DFLT
        //                ,COD_FND_DFLT
        //                ,TP_DUR
        //                ,TP_CALC_DUR
        //                ,FL_NO_ADERENTI
        //                ,FL_DISTINTA_CNBTVA
        //                ,FL_CNBT_AUTES
        //                ,FL_QTZ_POST_EMIS
        //                ,FL_COMNZ_FND_IS
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //             INTO
        //                :DCO-ID-D-COLL
        //               ,:DCO-ID-POLI
        //               ,:DCO-ID-MOVI-CRZ
        //               ,:DCO-ID-MOVI-CHIU
        //                :IND-DCO-ID-MOVI-CHIU
        //               ,:DCO-DT-INI-EFF-DB
        //               ,:DCO-DT-END-EFF-DB
        //               ,:DCO-COD-COMP-ANIA
        //               ,:DCO-IMP-ARROT-PRE
        //                :IND-DCO-IMP-ARROT-PRE
        //               ,:DCO-PC-SCON
        //                :IND-DCO-PC-SCON
        //               ,:DCO-FL-ADES-SING
        //                :IND-DCO-FL-ADES-SING
        //               ,:DCO-TP-IMP
        //                :IND-DCO-TP-IMP
        //               ,:DCO-FL-RICL-PRE-DA-CPT
        //                :IND-DCO-FL-RICL-PRE-DA-CPT
        //               ,:DCO-TP-ADES
        //                :IND-DCO-TP-ADES
        //               ,:DCO-DT-ULT-RINN-TAC-DB
        //                :IND-DCO-DT-ULT-RINN-TAC
        //               ,:DCO-IMP-SCON
        //                :IND-DCO-IMP-SCON
        //               ,:DCO-FRAZ-DFLT
        //                :IND-DCO-FRAZ-DFLT
        //               ,:DCO-ETA-SCAD-MASC-DFLT
        //                :IND-DCO-ETA-SCAD-MASC-DFLT
        //               ,:DCO-ETA-SCAD-FEMM-DFLT
        //                :IND-DCO-ETA-SCAD-FEMM-DFLT
        //               ,:DCO-TP-DFLT-DUR
        //                :IND-DCO-TP-DFLT-DUR
        //               ,:DCO-DUR-AA-ADES-DFLT
        //                :IND-DCO-DUR-AA-ADES-DFLT
        //               ,:DCO-DUR-MM-ADES-DFLT
        //                :IND-DCO-DUR-MM-ADES-DFLT
        //               ,:DCO-DUR-GG-ADES-DFLT
        //                :IND-DCO-DUR-GG-ADES-DFLT
        //               ,:DCO-DT-SCAD-ADES-DFLT-DB
        //                :IND-DCO-DT-SCAD-ADES-DFLT
        //               ,:DCO-COD-FND-DFLT
        //                :IND-DCO-COD-FND-DFLT
        //               ,:DCO-TP-DUR
        //                :IND-DCO-TP-DUR
        //               ,:DCO-TP-CALC-DUR
        //                :IND-DCO-TP-CALC-DUR
        //               ,:DCO-FL-NO-ADERENTI
        //                :IND-DCO-FL-NO-ADERENTI
        //               ,:DCO-FL-DISTINTA-CNBTVA
        //                :IND-DCO-FL-DISTINTA-CNBTVA
        //               ,:DCO-FL-CNBT-AUTES
        //                :IND-DCO-FL-CNBT-AUTES
        //               ,:DCO-FL-QTZ-POST-EMIS
        //                :IND-DCO-FL-QTZ-POST-EMIS
        //               ,:DCO-FL-COMNZ-FND-IS
        //                :IND-DCO-FL-COMNZ-FND-IS
        //               ,:DCO-DS-RIGA
        //               ,:DCO-DS-OPER-SQL
        //               ,:DCO-DS-VER
        //               ,:DCO-DS-TS-INI-CPTZ
        //               ,:DCO-DS-TS-END-CPTZ
        //               ,:DCO-DS-UTENTE
        //               ,:DCO-DS-STATO-ELAB
        //             FROM D_COLL
        //             WHERE     ID_POLI = :DCO-ID-POLI
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        dCollDao.selectRec3(dColl.getDcoIdPoli(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B460-OPEN-CURSOR-IDP-CPZ<br>*/
    private void b460OpenCursorIdpCpz() {
        // COB_CODE: PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.
        b405DeclareCursorIdpCpz();
        // COB_CODE: EXEC SQL
        //                OPEN C-IDP-CPZ-DCO
        //           END-EXEC.
        dCollDao.openCIdpCpzDco(dColl.getDcoIdPoli(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B470-CLOSE-CURSOR-IDP-CPZ<br>*/
    private void b470CloseCursorIdpCpz() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IDP-CPZ-DCO
        //           END-EXEC.
        dCollDao.closeCIdpCpzDco();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B480-FETCH-FIRST-IDP-CPZ<br>*/
    private void b480FetchFirstIdpCpz() {
        // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.
        b460OpenCursorIdpCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
            b490FetchNextIdpCpz();
        }
    }

    /**Original name: B490-FETCH-NEXT-IDP-CPZ<br>*/
    private void b490FetchNextIdpCpz() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IDP-CPZ-DCO
        //           INTO
        //                :DCO-ID-D-COLL
        //               ,:DCO-ID-POLI
        //               ,:DCO-ID-MOVI-CRZ
        //               ,:DCO-ID-MOVI-CHIU
        //                :IND-DCO-ID-MOVI-CHIU
        //               ,:DCO-DT-INI-EFF-DB
        //               ,:DCO-DT-END-EFF-DB
        //               ,:DCO-COD-COMP-ANIA
        //               ,:DCO-IMP-ARROT-PRE
        //                :IND-DCO-IMP-ARROT-PRE
        //               ,:DCO-PC-SCON
        //                :IND-DCO-PC-SCON
        //               ,:DCO-FL-ADES-SING
        //                :IND-DCO-FL-ADES-SING
        //               ,:DCO-TP-IMP
        //                :IND-DCO-TP-IMP
        //               ,:DCO-FL-RICL-PRE-DA-CPT
        //                :IND-DCO-FL-RICL-PRE-DA-CPT
        //               ,:DCO-TP-ADES
        //                :IND-DCO-TP-ADES
        //               ,:DCO-DT-ULT-RINN-TAC-DB
        //                :IND-DCO-DT-ULT-RINN-TAC
        //               ,:DCO-IMP-SCON
        //                :IND-DCO-IMP-SCON
        //               ,:DCO-FRAZ-DFLT
        //                :IND-DCO-FRAZ-DFLT
        //               ,:DCO-ETA-SCAD-MASC-DFLT
        //                :IND-DCO-ETA-SCAD-MASC-DFLT
        //               ,:DCO-ETA-SCAD-FEMM-DFLT
        //                :IND-DCO-ETA-SCAD-FEMM-DFLT
        //               ,:DCO-TP-DFLT-DUR
        //                :IND-DCO-TP-DFLT-DUR
        //               ,:DCO-DUR-AA-ADES-DFLT
        //                :IND-DCO-DUR-AA-ADES-DFLT
        //               ,:DCO-DUR-MM-ADES-DFLT
        //                :IND-DCO-DUR-MM-ADES-DFLT
        //               ,:DCO-DUR-GG-ADES-DFLT
        //                :IND-DCO-DUR-GG-ADES-DFLT
        //               ,:DCO-DT-SCAD-ADES-DFLT-DB
        //                :IND-DCO-DT-SCAD-ADES-DFLT
        //               ,:DCO-COD-FND-DFLT
        //                :IND-DCO-COD-FND-DFLT
        //               ,:DCO-TP-DUR
        //                :IND-DCO-TP-DUR
        //               ,:DCO-TP-CALC-DUR
        //                :IND-DCO-TP-CALC-DUR
        //               ,:DCO-FL-NO-ADERENTI
        //                :IND-DCO-FL-NO-ADERENTI
        //               ,:DCO-FL-DISTINTA-CNBTVA
        //                :IND-DCO-FL-DISTINTA-CNBTVA
        //               ,:DCO-FL-CNBT-AUTES
        //                :IND-DCO-FL-CNBT-AUTES
        //               ,:DCO-FL-QTZ-POST-EMIS
        //                :IND-DCO-FL-QTZ-POST-EMIS
        //               ,:DCO-FL-COMNZ-FND-IS
        //                :IND-DCO-FL-COMNZ-FND-IS
        //               ,:DCO-DS-RIGA
        //               ,:DCO-DS-OPER-SQL
        //               ,:DCO-DS-VER
        //               ,:DCO-DS-TS-INI-CPTZ
        //               ,:DCO-DS-TS-END-CPTZ
        //               ,:DCO-DS-UTENTE
        //               ,:DCO-DS-STATO-ELAB
        //           END-EXEC.
        dCollDao.fetchCIdpCpzDco(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
            b470CloseCursorIdpCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B505-DECLARE-CURSOR-IBO-CPZ<br>
	 * <pre>----
	 * ----  gestione IBO Competenza
	 * ----</pre>*/
    private void b505DeclareCursorIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B510-SELECT-IBO-CPZ<br>*/
    private void b510SelectIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B560-OPEN-CURSOR-IBO-CPZ<br>*/
    private void b560OpenCursorIboCpz() {
        // COB_CODE: PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.
        b505DeclareCursorIboCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B570-CLOSE-CURSOR-IBO-CPZ<br>*/
    private void b570CloseCursorIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B580-FETCH-FIRST-IBO-CPZ<br>*/
    private void b580FetchFirstIboCpz() {
        // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.
        b560OpenCursorIboCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
    }

    /**Original name: B590-FETCH-NEXT-IBO-CPZ<br>*/
    private void b590FetchNextIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B605-DECLARE-CURSOR-IBS-CPZ<br>
	 * <pre>----
	 * ----  gestione IBS Competenza
	 * ----</pre>*/
    private void b605DeclareCursorIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B610-SELECT-IBS-CPZ<br>*/
    private void b610SelectIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B660-OPEN-CURSOR-IBS-CPZ<br>*/
    private void b660OpenCursorIbsCpz() {
        // COB_CODE: PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.
        b605DeclareCursorIbsCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B670-CLOSE-CURSOR-IBS-CPZ<br>*/
    private void b670CloseCursorIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B680-FETCH-FIRST-IBS-CPZ<br>*/
    private void b680FetchFirstIbsCpz() {
        // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.
        b660OpenCursorIbsCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
    }

    /**Original name: B690-FETCH-NEXT-IBS-CPZ<br>*/
    private void b690FetchNextIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B705-DECLARE-CURSOR-IDO-CPZ<br>
	 * <pre>----
	 * ----  gestione IDO Competenza
	 * ----</pre>*/
    private void b705DeclareCursorIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B710-SELECT-IDO-CPZ<br>*/
    private void b710SelectIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B760-OPEN-CURSOR-IDO-CPZ<br>*/
    private void b760OpenCursorIdoCpz() {
        // COB_CODE: PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.
        b705DeclareCursorIdoCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B770-CLOSE-CURSOR-IDO-CPZ<br>*/
    private void b770CloseCursorIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B780-FETCH-FIRST-IDO-CPZ<br>*/
    private void b780FetchFirstIdoCpz() {
        // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.
        b760OpenCursorIdoCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
    }

    /**Original name: B790-FETCH-NEXT-IDO-CPZ<br>*/
    private void b790FetchNextIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-DCO-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO DCO-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndDColl().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DCO-ID-MOVI-CHIU-NULL
            dColl.getDcoIdMoviChiu().setDcoIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DcoIdMoviChiu.Len.DCO_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-DCO-IMP-ARROT-PRE = -1
        //              MOVE HIGH-VALUES TO DCO-IMP-ARROT-PRE-NULL
        //           END-IF
        if (ws.getIndDColl().getImpArrotPre() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DCO-IMP-ARROT-PRE-NULL
            dColl.getDcoImpArrotPre().setDcoImpArrotPreNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DcoImpArrotPre.Len.DCO_IMP_ARROT_PRE_NULL));
        }
        // COB_CODE: IF IND-DCO-PC-SCON = -1
        //              MOVE HIGH-VALUES TO DCO-PC-SCON-NULL
        //           END-IF
        if (ws.getIndDColl().getPcScon() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DCO-PC-SCON-NULL
            dColl.getDcoPcScon().setDcoPcSconNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DcoPcScon.Len.DCO_PC_SCON_NULL));
        }
        // COB_CODE: IF IND-DCO-FL-ADES-SING = -1
        //              MOVE HIGH-VALUES TO DCO-FL-ADES-SING-NULL
        //           END-IF
        if (ws.getIndDColl().getFlAdesSing() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DCO-FL-ADES-SING-NULL
            dColl.setDcoFlAdesSing(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-DCO-TP-IMP = -1
        //              MOVE HIGH-VALUES TO DCO-TP-IMP-NULL
        //           END-IF
        if (ws.getIndDColl().getTpImp() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DCO-TP-IMP-NULL
            dColl.setDcoTpImp(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DCollIdbsdco0.Len.DCO_TP_IMP));
        }
        // COB_CODE: IF IND-DCO-FL-RICL-PRE-DA-CPT = -1
        //              MOVE HIGH-VALUES TO DCO-FL-RICL-PRE-DA-CPT-NULL
        //           END-IF
        if (ws.getIndDColl().getFlRiclPreDaCpt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DCO-FL-RICL-PRE-DA-CPT-NULL
            dColl.setDcoFlRiclPreDaCpt(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-DCO-TP-ADES = -1
        //              MOVE HIGH-VALUES TO DCO-TP-ADES-NULL
        //           END-IF
        if (ws.getIndDColl().getTpAdes() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DCO-TP-ADES-NULL
            dColl.setDcoTpAdes(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DCollIdbsdco0.Len.DCO_TP_ADES));
        }
        // COB_CODE: IF IND-DCO-DT-ULT-RINN-TAC = -1
        //              MOVE HIGH-VALUES TO DCO-DT-ULT-RINN-TAC-NULL
        //           END-IF
        if (ws.getIndDColl().getDtUltRinnTac() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DCO-DT-ULT-RINN-TAC-NULL
            dColl.getDcoDtUltRinnTac().setDcoDtUltRinnTacNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DcoDtUltRinnTac.Len.DCO_DT_ULT_RINN_TAC_NULL));
        }
        // COB_CODE: IF IND-DCO-IMP-SCON = -1
        //              MOVE HIGH-VALUES TO DCO-IMP-SCON-NULL
        //           END-IF
        if (ws.getIndDColl().getImpScon() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DCO-IMP-SCON-NULL
            dColl.getDcoImpScon().setDcoImpSconNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DcoImpScon.Len.DCO_IMP_SCON_NULL));
        }
        // COB_CODE: IF IND-DCO-FRAZ-DFLT = -1
        //              MOVE HIGH-VALUES TO DCO-FRAZ-DFLT-NULL
        //           END-IF
        if (ws.getIndDColl().getFrazDflt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DCO-FRAZ-DFLT-NULL
            dColl.getDcoFrazDflt().setDcoFrazDfltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DcoFrazDflt.Len.DCO_FRAZ_DFLT_NULL));
        }
        // COB_CODE: IF IND-DCO-ETA-SCAD-MASC-DFLT = -1
        //              MOVE HIGH-VALUES TO DCO-ETA-SCAD-MASC-DFLT-NULL
        //           END-IF
        if (ws.getIndDColl().getEtaScadMascDflt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DCO-ETA-SCAD-MASC-DFLT-NULL
            dColl.getDcoEtaScadMascDflt().setDcoEtaScadMascDfltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DcoEtaScadMascDflt.Len.DCO_ETA_SCAD_MASC_DFLT_NULL));
        }
        // COB_CODE: IF IND-DCO-ETA-SCAD-FEMM-DFLT = -1
        //              MOVE HIGH-VALUES TO DCO-ETA-SCAD-FEMM-DFLT-NULL
        //           END-IF
        if (ws.getIndDColl().getEtaScadFemmDflt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DCO-ETA-SCAD-FEMM-DFLT-NULL
            dColl.getDcoEtaScadFemmDflt().setDcoEtaScadFemmDfltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DcoEtaScadFemmDflt.Len.DCO_ETA_SCAD_FEMM_DFLT_NULL));
        }
        // COB_CODE: IF IND-DCO-TP-DFLT-DUR = -1
        //              MOVE HIGH-VALUES TO DCO-TP-DFLT-DUR-NULL
        //           END-IF
        if (ws.getIndDColl().getTpDfltDur() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DCO-TP-DFLT-DUR-NULL
            dColl.setDcoTpDfltDur(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DCollIdbsdco0.Len.DCO_TP_DFLT_DUR));
        }
        // COB_CODE: IF IND-DCO-DUR-AA-ADES-DFLT = -1
        //              MOVE HIGH-VALUES TO DCO-DUR-AA-ADES-DFLT-NULL
        //           END-IF
        if (ws.getIndDColl().getDurAaAdesDflt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DCO-DUR-AA-ADES-DFLT-NULL
            dColl.getDcoDurAaAdesDflt().setDcoDurAaAdesDfltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DcoDurAaAdesDflt.Len.DCO_DUR_AA_ADES_DFLT_NULL));
        }
        // COB_CODE: IF IND-DCO-DUR-MM-ADES-DFLT = -1
        //              MOVE HIGH-VALUES TO DCO-DUR-MM-ADES-DFLT-NULL
        //           END-IF
        if (ws.getIndDColl().getDurMmAdesDflt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DCO-DUR-MM-ADES-DFLT-NULL
            dColl.getDcoDurMmAdesDflt().setDcoDurMmAdesDfltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DcoDurMmAdesDflt.Len.DCO_DUR_MM_ADES_DFLT_NULL));
        }
        // COB_CODE: IF IND-DCO-DUR-GG-ADES-DFLT = -1
        //              MOVE HIGH-VALUES TO DCO-DUR-GG-ADES-DFLT-NULL
        //           END-IF
        if (ws.getIndDColl().getDurGgAdesDflt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DCO-DUR-GG-ADES-DFLT-NULL
            dColl.getDcoDurGgAdesDflt().setDcoDurGgAdesDfltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DcoDurGgAdesDflt.Len.DCO_DUR_GG_ADES_DFLT_NULL));
        }
        // COB_CODE: IF IND-DCO-DT-SCAD-ADES-DFLT = -1
        //              MOVE HIGH-VALUES TO DCO-DT-SCAD-ADES-DFLT-NULL
        //           END-IF
        if (ws.getIndDColl().getDtScadAdesDflt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DCO-DT-SCAD-ADES-DFLT-NULL
            dColl.getDcoDtScadAdesDflt().setDcoDtScadAdesDfltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DcoDtScadAdesDflt.Len.DCO_DT_SCAD_ADES_DFLT_NULL));
        }
        // COB_CODE: IF IND-DCO-COD-FND-DFLT = -1
        //              MOVE HIGH-VALUES TO DCO-COD-FND-DFLT-NULL
        //           END-IF
        if (ws.getIndDColl().getCodFndDflt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DCO-COD-FND-DFLT-NULL
            dColl.setDcoCodFndDflt(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DCollIdbsdco0.Len.DCO_COD_FND_DFLT));
        }
        // COB_CODE: IF IND-DCO-TP-DUR = -1
        //              MOVE HIGH-VALUES TO DCO-TP-DUR-NULL
        //           END-IF
        if (ws.getIndDColl().getTpDur() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DCO-TP-DUR-NULL
            dColl.setDcoTpDur(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DCollIdbsdco0.Len.DCO_TP_DUR));
        }
        // COB_CODE: IF IND-DCO-TP-CALC-DUR = -1
        //              MOVE HIGH-VALUES TO DCO-TP-CALC-DUR-NULL
        //           END-IF
        if (ws.getIndDColl().getTpCalcDur() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DCO-TP-CALC-DUR-NULL
            dColl.setDcoTpCalcDur(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DCollIdbsdco0.Len.DCO_TP_CALC_DUR));
        }
        // COB_CODE: IF IND-DCO-FL-NO-ADERENTI = -1
        //              MOVE HIGH-VALUES TO DCO-FL-NO-ADERENTI-NULL
        //           END-IF
        if (ws.getIndDColl().getFlNoAderenti() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DCO-FL-NO-ADERENTI-NULL
            dColl.setDcoFlNoAderenti(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-DCO-FL-DISTINTA-CNBTVA = -1
        //              MOVE HIGH-VALUES TO DCO-FL-DISTINTA-CNBTVA-NULL
        //           END-IF
        if (ws.getIndDColl().getFlDistintaCnbtva() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DCO-FL-DISTINTA-CNBTVA-NULL
            dColl.setDcoFlDistintaCnbtva(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-DCO-FL-CNBT-AUTES = -1
        //              MOVE HIGH-VALUES TO DCO-FL-CNBT-AUTES-NULL
        //           END-IF
        if (ws.getIndDColl().getFlCnbtAutes() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DCO-FL-CNBT-AUTES-NULL
            dColl.setDcoFlCnbtAutes(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-DCO-FL-QTZ-POST-EMIS = -1
        //              MOVE HIGH-VALUES TO DCO-FL-QTZ-POST-EMIS-NULL
        //           END-IF
        if (ws.getIndDColl().getFlQtzPostEmis() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DCO-FL-QTZ-POST-EMIS-NULL
            dColl.setDcoFlQtzPostEmis(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-DCO-FL-COMNZ-FND-IS = -1
        //              MOVE HIGH-VALUES TO DCO-FL-COMNZ-FND-IS-NULL
        //           END-IF.
        if (ws.getIndDColl().getFlComnzFndIs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DCO-FL-COMNZ-FND-IS-NULL
            dColl.setDcoFlComnzFndIs(Types.HIGH_CHAR_VAL);
        }
    }

    /**Original name: Z150-VALORIZZA-DATA-SERVICES-I<br>*/
    private void z150ValorizzaDataServicesI() {
        // COB_CODE: MOVE 'I' TO DCO-DS-OPER-SQL
        dColl.setDcoDsOperSqlFormatted("I");
        // COB_CODE: MOVE 0                   TO DCO-DS-VER
        dColl.setDcoDsVer(0);
        // COB_CODE: MOVE IDSV0003-USER-NAME TO DCO-DS-UTENTE
        dColl.setDcoDsUtente(idsv0003.getUserName());
        // COB_CODE: MOVE '1'                   TO DCO-DS-STATO-ELAB.
        dColl.setDcoDsStatoElabFormatted("1");
    }

    /**Original name: Z160-VALORIZZA-DATA-SERVICES-U<br>*/
    private void z160ValorizzaDataServicesU() {
        // COB_CODE: MOVE 'U' TO DCO-DS-OPER-SQL
        dColl.setDcoDsOperSqlFormatted("U");
        // COB_CODE: MOVE IDSV0003-USER-NAME TO DCO-DS-UTENTE.
        dColl.setDcoDsUtente(idsv0003.getUserName());
    }

    /**Original name: Z200-SET-INDICATORI-NULL<br>*/
    private void z200SetIndicatoriNull() {
        // COB_CODE: IF DCO-ID-MOVI-CHIU-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DCO-ID-MOVI-CHIU
        //           ELSE
        //              MOVE 0 TO IND-DCO-ID-MOVI-CHIU
        //           END-IF
        if (Characters.EQ_HIGH.test(dColl.getDcoIdMoviChiu().getDcoIdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DCO-ID-MOVI-CHIU
            ws.getIndDColl().setIdMoviChiu(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DCO-ID-MOVI-CHIU
            ws.getIndDColl().setIdMoviChiu(((short)0));
        }
        // COB_CODE: IF DCO-IMP-ARROT-PRE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DCO-IMP-ARROT-PRE
        //           ELSE
        //              MOVE 0 TO IND-DCO-IMP-ARROT-PRE
        //           END-IF
        if (Characters.EQ_HIGH.test(dColl.getDcoImpArrotPre().getDcoImpArrotPreNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DCO-IMP-ARROT-PRE
            ws.getIndDColl().setImpArrotPre(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DCO-IMP-ARROT-PRE
            ws.getIndDColl().setImpArrotPre(((short)0));
        }
        // COB_CODE: IF DCO-PC-SCON-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DCO-PC-SCON
        //           ELSE
        //              MOVE 0 TO IND-DCO-PC-SCON
        //           END-IF
        if (Characters.EQ_HIGH.test(dColl.getDcoPcScon().getDcoPcSconNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DCO-PC-SCON
            ws.getIndDColl().setPcScon(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DCO-PC-SCON
            ws.getIndDColl().setPcScon(((short)0));
        }
        // COB_CODE: IF DCO-FL-ADES-SING-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DCO-FL-ADES-SING
        //           ELSE
        //              MOVE 0 TO IND-DCO-FL-ADES-SING
        //           END-IF
        if (Conditions.eq(dColl.getDcoFlAdesSing(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-DCO-FL-ADES-SING
            ws.getIndDColl().setFlAdesSing(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DCO-FL-ADES-SING
            ws.getIndDColl().setFlAdesSing(((short)0));
        }
        // COB_CODE: IF DCO-TP-IMP-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DCO-TP-IMP
        //           ELSE
        //              MOVE 0 TO IND-DCO-TP-IMP
        //           END-IF
        if (Characters.EQ_HIGH.test(dColl.getDcoTpImpFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DCO-TP-IMP
            ws.getIndDColl().setTpImp(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DCO-TP-IMP
            ws.getIndDColl().setTpImp(((short)0));
        }
        // COB_CODE: IF DCO-FL-RICL-PRE-DA-CPT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DCO-FL-RICL-PRE-DA-CPT
        //           ELSE
        //              MOVE 0 TO IND-DCO-FL-RICL-PRE-DA-CPT
        //           END-IF
        if (Conditions.eq(dColl.getDcoFlRiclPreDaCpt(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-DCO-FL-RICL-PRE-DA-CPT
            ws.getIndDColl().setFlRiclPreDaCpt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DCO-FL-RICL-PRE-DA-CPT
            ws.getIndDColl().setFlRiclPreDaCpt(((short)0));
        }
        // COB_CODE: IF DCO-TP-ADES-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DCO-TP-ADES
        //           ELSE
        //              MOVE 0 TO IND-DCO-TP-ADES
        //           END-IF
        if (Characters.EQ_HIGH.test(dColl.getDcoTpAdesFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DCO-TP-ADES
            ws.getIndDColl().setTpAdes(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DCO-TP-ADES
            ws.getIndDColl().setTpAdes(((short)0));
        }
        // COB_CODE: IF DCO-DT-ULT-RINN-TAC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DCO-DT-ULT-RINN-TAC
        //           ELSE
        //              MOVE 0 TO IND-DCO-DT-ULT-RINN-TAC
        //           END-IF
        if (Characters.EQ_HIGH.test(dColl.getDcoDtUltRinnTac().getDcoDtUltRinnTacNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DCO-DT-ULT-RINN-TAC
            ws.getIndDColl().setDtUltRinnTac(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DCO-DT-ULT-RINN-TAC
            ws.getIndDColl().setDtUltRinnTac(((short)0));
        }
        // COB_CODE: IF DCO-IMP-SCON-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DCO-IMP-SCON
        //           ELSE
        //              MOVE 0 TO IND-DCO-IMP-SCON
        //           END-IF
        if (Characters.EQ_HIGH.test(dColl.getDcoImpScon().getDcoImpSconNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DCO-IMP-SCON
            ws.getIndDColl().setImpScon(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DCO-IMP-SCON
            ws.getIndDColl().setImpScon(((short)0));
        }
        // COB_CODE: IF DCO-FRAZ-DFLT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DCO-FRAZ-DFLT
        //           ELSE
        //              MOVE 0 TO IND-DCO-FRAZ-DFLT
        //           END-IF
        if (Characters.EQ_HIGH.test(dColl.getDcoFrazDflt().getDcoFrazDfltNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DCO-FRAZ-DFLT
            ws.getIndDColl().setFrazDflt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DCO-FRAZ-DFLT
            ws.getIndDColl().setFrazDflt(((short)0));
        }
        // COB_CODE: IF DCO-ETA-SCAD-MASC-DFLT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DCO-ETA-SCAD-MASC-DFLT
        //           ELSE
        //              MOVE 0 TO IND-DCO-ETA-SCAD-MASC-DFLT
        //           END-IF
        if (Characters.EQ_HIGH.test(dColl.getDcoEtaScadMascDflt().getDcoEtaScadMascDfltNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DCO-ETA-SCAD-MASC-DFLT
            ws.getIndDColl().setEtaScadMascDflt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DCO-ETA-SCAD-MASC-DFLT
            ws.getIndDColl().setEtaScadMascDflt(((short)0));
        }
        // COB_CODE: IF DCO-ETA-SCAD-FEMM-DFLT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DCO-ETA-SCAD-FEMM-DFLT
        //           ELSE
        //              MOVE 0 TO IND-DCO-ETA-SCAD-FEMM-DFLT
        //           END-IF
        if (Characters.EQ_HIGH.test(dColl.getDcoEtaScadFemmDflt().getDcoEtaScadFemmDfltNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DCO-ETA-SCAD-FEMM-DFLT
            ws.getIndDColl().setEtaScadFemmDflt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DCO-ETA-SCAD-FEMM-DFLT
            ws.getIndDColl().setEtaScadFemmDflt(((short)0));
        }
        // COB_CODE: IF DCO-TP-DFLT-DUR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DCO-TP-DFLT-DUR
        //           ELSE
        //              MOVE 0 TO IND-DCO-TP-DFLT-DUR
        //           END-IF
        if (Characters.EQ_HIGH.test(dColl.getDcoTpDfltDurFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DCO-TP-DFLT-DUR
            ws.getIndDColl().setTpDfltDur(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DCO-TP-DFLT-DUR
            ws.getIndDColl().setTpDfltDur(((short)0));
        }
        // COB_CODE: IF DCO-DUR-AA-ADES-DFLT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DCO-DUR-AA-ADES-DFLT
        //           ELSE
        //              MOVE 0 TO IND-DCO-DUR-AA-ADES-DFLT
        //           END-IF
        if (Characters.EQ_HIGH.test(dColl.getDcoDurAaAdesDflt().getDcoDurAaAdesDfltNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DCO-DUR-AA-ADES-DFLT
            ws.getIndDColl().setDurAaAdesDflt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DCO-DUR-AA-ADES-DFLT
            ws.getIndDColl().setDurAaAdesDflt(((short)0));
        }
        // COB_CODE: IF DCO-DUR-MM-ADES-DFLT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DCO-DUR-MM-ADES-DFLT
        //           ELSE
        //              MOVE 0 TO IND-DCO-DUR-MM-ADES-DFLT
        //           END-IF
        if (Characters.EQ_HIGH.test(dColl.getDcoDurMmAdesDflt().getDcoDurMmAdesDfltNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DCO-DUR-MM-ADES-DFLT
            ws.getIndDColl().setDurMmAdesDflt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DCO-DUR-MM-ADES-DFLT
            ws.getIndDColl().setDurMmAdesDflt(((short)0));
        }
        // COB_CODE: IF DCO-DUR-GG-ADES-DFLT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DCO-DUR-GG-ADES-DFLT
        //           ELSE
        //              MOVE 0 TO IND-DCO-DUR-GG-ADES-DFLT
        //           END-IF
        if (Characters.EQ_HIGH.test(dColl.getDcoDurGgAdesDflt().getDcoDurGgAdesDfltNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DCO-DUR-GG-ADES-DFLT
            ws.getIndDColl().setDurGgAdesDflt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DCO-DUR-GG-ADES-DFLT
            ws.getIndDColl().setDurGgAdesDflt(((short)0));
        }
        // COB_CODE: IF DCO-DT-SCAD-ADES-DFLT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DCO-DT-SCAD-ADES-DFLT
        //           ELSE
        //              MOVE 0 TO IND-DCO-DT-SCAD-ADES-DFLT
        //           END-IF
        if (Characters.EQ_HIGH.test(dColl.getDcoDtScadAdesDflt().getDcoDtScadAdesDfltNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DCO-DT-SCAD-ADES-DFLT
            ws.getIndDColl().setDtScadAdesDflt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DCO-DT-SCAD-ADES-DFLT
            ws.getIndDColl().setDtScadAdesDflt(((short)0));
        }
        // COB_CODE: IF DCO-COD-FND-DFLT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DCO-COD-FND-DFLT
        //           ELSE
        //              MOVE 0 TO IND-DCO-COD-FND-DFLT
        //           END-IF
        if (Characters.EQ_HIGH.test(dColl.getDcoCodFndDfltFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DCO-COD-FND-DFLT
            ws.getIndDColl().setCodFndDflt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DCO-COD-FND-DFLT
            ws.getIndDColl().setCodFndDflt(((short)0));
        }
        // COB_CODE: IF DCO-TP-DUR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DCO-TP-DUR
        //           ELSE
        //              MOVE 0 TO IND-DCO-TP-DUR
        //           END-IF
        if (Characters.EQ_HIGH.test(dColl.getDcoTpDurFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DCO-TP-DUR
            ws.getIndDColl().setTpDur(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DCO-TP-DUR
            ws.getIndDColl().setTpDur(((short)0));
        }
        // COB_CODE: IF DCO-TP-CALC-DUR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DCO-TP-CALC-DUR
        //           ELSE
        //              MOVE 0 TO IND-DCO-TP-CALC-DUR
        //           END-IF
        if (Characters.EQ_HIGH.test(dColl.getDcoTpCalcDurFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DCO-TP-CALC-DUR
            ws.getIndDColl().setTpCalcDur(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DCO-TP-CALC-DUR
            ws.getIndDColl().setTpCalcDur(((short)0));
        }
        // COB_CODE: IF DCO-FL-NO-ADERENTI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DCO-FL-NO-ADERENTI
        //           ELSE
        //              MOVE 0 TO IND-DCO-FL-NO-ADERENTI
        //           END-IF
        if (Conditions.eq(dColl.getDcoFlNoAderenti(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-DCO-FL-NO-ADERENTI
            ws.getIndDColl().setFlNoAderenti(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DCO-FL-NO-ADERENTI
            ws.getIndDColl().setFlNoAderenti(((short)0));
        }
        // COB_CODE: IF DCO-FL-DISTINTA-CNBTVA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DCO-FL-DISTINTA-CNBTVA
        //           ELSE
        //              MOVE 0 TO IND-DCO-FL-DISTINTA-CNBTVA
        //           END-IF
        if (Conditions.eq(dColl.getDcoFlDistintaCnbtva(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-DCO-FL-DISTINTA-CNBTVA
            ws.getIndDColl().setFlDistintaCnbtva(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DCO-FL-DISTINTA-CNBTVA
            ws.getIndDColl().setFlDistintaCnbtva(((short)0));
        }
        // COB_CODE: IF DCO-FL-CNBT-AUTES-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DCO-FL-CNBT-AUTES
        //           ELSE
        //              MOVE 0 TO IND-DCO-FL-CNBT-AUTES
        //           END-IF
        if (Conditions.eq(dColl.getDcoFlCnbtAutes(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-DCO-FL-CNBT-AUTES
            ws.getIndDColl().setFlCnbtAutes(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DCO-FL-CNBT-AUTES
            ws.getIndDColl().setFlCnbtAutes(((short)0));
        }
        // COB_CODE: IF DCO-FL-QTZ-POST-EMIS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DCO-FL-QTZ-POST-EMIS
        //           ELSE
        //              MOVE 0 TO IND-DCO-FL-QTZ-POST-EMIS
        //           END-IF
        if (Conditions.eq(dColl.getDcoFlQtzPostEmis(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-DCO-FL-QTZ-POST-EMIS
            ws.getIndDColl().setFlQtzPostEmis(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DCO-FL-QTZ-POST-EMIS
            ws.getIndDColl().setFlQtzPostEmis(((short)0));
        }
        // COB_CODE: IF DCO-FL-COMNZ-FND-IS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DCO-FL-COMNZ-FND-IS
        //           ELSE
        //              MOVE 0 TO IND-DCO-FL-COMNZ-FND-IS
        //           END-IF.
        if (Conditions.eq(dColl.getDcoFlComnzFndIs(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-DCO-FL-COMNZ-FND-IS
            ws.getIndDColl().setFlComnzFndIs(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DCO-FL-COMNZ-FND-IS
            ws.getIndDColl().setFlComnzFndIs(((short)0));
        }
    }

    /**Original name: Z400-SEQ-RIGA<br>*/
    private void z400SeqRiga() {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_RIGA
        //              INTO : DCO-DS-RIGA
        //           END-EXEC.
        //TODO: Implement: valuesStmt;
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: Z500-AGGIORNAMENTO-STORICO<br>*/
    private void z500AggiornamentoStorico() {
        // COB_CODE: MOVE D-COLL TO WS-BUFFER-TABLE.
        ws.setWsBufferTable(dColl.getdCollFormatted());
        // COB_CODE: MOVE DCO-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.
        ws.setWsIdMoviCrz(TruncAbs.toInt(dColl.getDcoIdMoviCrz(), 9));
        // COB_CODE: PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.
        a360OpenCursorIdEff();
        // COB_CODE: PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-SQL
        //              END-IF
        //           END-PERFORM.
        while (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX
            a390FetchNextIdEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              END-IF
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: MOVE WS-ID-MOVI-CRZ   TO DCO-ID-MOVI-CHIU
                dColl.getDcoIdMoviChiu().setDcoIdMoviChiu(ws.getWsIdMoviCrz());
                // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
                //                                 TO DCO-DS-TS-END-CPTZ
                dColl.setDcoDsTsEndCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
                // COB_CODE: PERFORM A330-UPDATE-ID-EFF THRU A330-EX
                a330UpdateIdEff();
                // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
                //                 END-IF
                //           END-IF
                if (idsv0003.getSqlcode().isSuccessfulSql()) {
                    // COB_CODE: MOVE WS-ID-MOVI-CRZ TO DCO-ID-MOVI-CRZ
                    dColl.setDcoIdMoviCrz(ws.getWsIdMoviCrz());
                    // COB_CODE: MOVE HIGH-VALUES    TO DCO-ID-MOVI-CHIU-NULL
                    dColl.getDcoIdMoviChiu().setDcoIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DcoIdMoviChiu.Len.DCO_ID_MOVI_CHIU_NULL));
                    // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
                    //                               TO DCO-DT-END-EFF
                    dColl.setDcoDtEndEff(idsv0003.getDataInizioEffetto());
                    // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
                    //                               TO DCO-DS-TS-INI-CPTZ
                    dColl.setDcoDsTsIniCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
                    // COB_CODE: MOVE WS-TS-INFINITO
                    //                               TO DCO-DS-TS-END-CPTZ
                    dColl.setDcoDsTsEndCptz(ws.getIdsv0010().getWsTsInfinito());
                    // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
                    //              PERFORM A220-INSERT-PK THRU A220-EX
                    //           END-IF
                    if (idsv0003.getSqlcode().isSuccessfulSql()) {
                        // COB_CODE: PERFORM A220-INSERT-PK THRU A220-EX
                        a220InsertPk();
                    }
                }
            }
        }
        // COB_CODE: IF IDSV0003-NOT-FOUND
        //              END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF NOT IDSV0003-DELETE-LOGICA
            //              PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX
            //           ELSE
            //              SET IDSV0003-SUCCESSFUL-SQL TO TRUE
            //           END-IF
            if (!idsv0003.getOperazione().isDeleteLogica()) {
                // COB_CODE: PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX
                z600InsertNuovaRigaStorica();
            }
            else {
                // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL TO TRUE
                idsv0003.getSqlcode().setSuccessfulSql();
            }
        }
    }

    /**Original name: Z600-INSERT-NUOVA-RIGA-STORICA<br>*/
    private void z600InsertNuovaRigaStorica() {
        // COB_CODE: MOVE WS-BUFFER-TABLE TO D-COLL.
        dColl.setdCollFormatted(ws.getWsBufferTableFormatted());
        // COB_CODE: MOVE WS-ID-MOVI-CRZ  TO DCO-ID-MOVI-CRZ.
        dColl.setDcoIdMoviCrz(ws.getWsIdMoviCrz());
        // COB_CODE: MOVE HIGH-VALUES     TO DCO-ID-MOVI-CHIU-NULL.
        dColl.getDcoIdMoviChiu().setDcoIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DcoIdMoviChiu.Len.DCO_ID_MOVI_CHIU_NULL));
        // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
        //                                TO DCO-DT-INI-EFF.
        dColl.setDcoDtIniEff(idsv0003.getDataInizioEffetto());
        // COB_CODE: MOVE WS-DT-INFINITO
        //                                TO DCO-DT-END-EFF.
        dColl.setDcoDtEndEff(ws.getIdsv0010().getWsDtInfinito());
        // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
        //                                TO DCO-DS-TS-INI-CPTZ.
        dColl.setDcoDsTsIniCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
        // COB_CODE: MOVE WS-TS-INFINITO
        //                                TO DCO-DS-TS-END-CPTZ.
        dColl.setDcoDsTsEndCptz(ws.getIdsv0010().getWsTsInfinito());
        // COB_CODE: MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
        //                                TO DCO-COD-COMP-ANIA.
        dColl.setDcoCodCompAnia(idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A220-INSERT-PK THRU A220-EX.
        a220InsertPk();
    }

    /**Original name: Z900-CONVERTI-N-TO-X<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da 9(8) comp-3 a date
	 * ----</pre>*/
    private void z900ConvertiNToX() {
        // COB_CODE: MOVE DCO-DT-INI-EFF TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(dColl.getDcoDtIniEff(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO DCO-DT-INI-EFF-DB
        ws.getdCollDb().setEffDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: MOVE DCO-DT-END-EFF TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(dColl.getDcoDtEndEff(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO DCO-DT-END-EFF-DB
        ws.getdCollDb().setRgstrzRichDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: IF IND-DCO-DT-ULT-RINN-TAC = 0
        //               MOVE WS-DATE-X      TO DCO-DT-ULT-RINN-TAC-DB
        //           END-IF
        if (ws.getIndDColl().getDtUltRinnTac() == 0) {
            // COB_CODE: MOVE DCO-DT-ULT-RINN-TAC TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(dColl.getDcoDtUltRinnTac().getDcoDtUltRinnTac(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO DCO-DT-ULT-RINN-TAC-DB
            ws.getdCollDb().setPervRichDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-DCO-DT-SCAD-ADES-DFLT = 0
        //               MOVE WS-DATE-X      TO DCO-DT-SCAD-ADES-DFLT-DB
        //           END-IF.
        if (ws.getIndDColl().getDtScadAdesDflt() == 0) {
            // COB_CODE: MOVE DCO-DT-SCAD-ADES-DFLT TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(dColl.getDcoDtScadAdesDflt().getDcoDtScadAdesDflt(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO DCO-DT-SCAD-ADES-DFLT-DB
            ws.getdCollDb().setEsecRichDb(ws.getIdsv0010().getWsDateX());
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE DCO-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getdCollDb().getEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO DCO-DT-INI-EFF
        dColl.setDcoDtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE DCO-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getdCollDb().getRgstrzRichDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO DCO-DT-END-EFF
        dColl.setDcoDtEndEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-DCO-DT-ULT-RINN-TAC = 0
        //               MOVE WS-DATE-N      TO DCO-DT-ULT-RINN-TAC
        //           END-IF
        if (ws.getIndDColl().getDtUltRinnTac() == 0) {
            // COB_CODE: MOVE DCO-DT-ULT-RINN-TAC-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getdCollDb().getPervRichDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO DCO-DT-ULT-RINN-TAC
            dColl.getDcoDtUltRinnTac().setDcoDtUltRinnTac(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-DCO-DT-SCAD-ADES-DFLT = 0
        //               MOVE WS-DATE-N      TO DCO-DT-SCAD-ADES-DFLT
        //           END-IF.
        if (ws.getIndDColl().getDtScadAdesDflt() == 0) {
            // COB_CODE: MOVE DCO-DT-SCAD-ADES-DFLT-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getdCollDb().getEsecRichDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO DCO-DT-SCAD-ADES-DFLT
            dColl.getDcoDtScadAdesDflt().setDcoDtScadAdesDflt(ws.getIdsv0010().getWsDateN());
        }
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public int getCodCompAnia() {
        return dColl.getDcoCodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.dColl.setDcoCodCompAnia(codCompAnia);
    }

    @Override
    public String getCodFndDflt() {
        return dColl.getDcoCodFndDflt();
    }

    @Override
    public void setCodFndDflt(String codFndDflt) {
        this.dColl.setDcoCodFndDflt(codFndDflt);
    }

    @Override
    public String getCodFndDfltObj() {
        if (ws.getIndDColl().getCodFndDflt() >= 0) {
            return getCodFndDflt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodFndDfltObj(String codFndDfltObj) {
        if (codFndDfltObj != null) {
            setCodFndDflt(codFndDfltObj);
            ws.getIndDColl().setCodFndDflt(((short)0));
        }
        else {
            ws.getIndDColl().setCodFndDflt(((short)-1));
        }
    }

    @Override
    public long getDcoDsRiga() {
        return dColl.getDcoDsRiga();
    }

    @Override
    public void setDcoDsRiga(long dcoDsRiga) {
        this.dColl.setDcoDsRiga(dcoDsRiga);
    }

    @Override
    public char getDsOperSql() {
        return dColl.getDcoDsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.dColl.setDcoDsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return dColl.getDcoDsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.dColl.setDcoDsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsEndCptz() {
        return dColl.getDcoDsTsEndCptz();
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.dColl.setDcoDsTsEndCptz(dsTsEndCptz);
    }

    @Override
    public long getDsTsIniCptz() {
        return dColl.getDcoDsTsIniCptz();
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.dColl.setDcoDsTsIniCptz(dsTsIniCptz);
    }

    @Override
    public String getDsUtente() {
        return dColl.getDcoDsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.dColl.setDcoDsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return dColl.getDcoDsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.dColl.setDcoDsVer(dsVer);
    }

    @Override
    public String getDtEndEffDb() {
        return ws.getdCollDb().getRgstrzRichDb();
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        this.ws.getdCollDb().setRgstrzRichDb(dtEndEffDb);
    }

    @Override
    public String getDtIniEffDb() {
        return ws.getdCollDb().getEffDb();
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        this.ws.getdCollDb().setEffDb(dtIniEffDb);
    }

    @Override
    public String getDtScadAdesDfltDb() {
        return ws.getdCollDb().getEsecRichDb();
    }

    @Override
    public void setDtScadAdesDfltDb(String dtScadAdesDfltDb) {
        this.ws.getdCollDb().setEsecRichDb(dtScadAdesDfltDb);
    }

    @Override
    public String getDtScadAdesDfltDbObj() {
        if (ws.getIndDColl().getDtScadAdesDflt() >= 0) {
            return getDtScadAdesDfltDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtScadAdesDfltDbObj(String dtScadAdesDfltDbObj) {
        if (dtScadAdesDfltDbObj != null) {
            setDtScadAdesDfltDb(dtScadAdesDfltDbObj);
            ws.getIndDColl().setDtScadAdesDflt(((short)0));
        }
        else {
            ws.getIndDColl().setDtScadAdesDflt(((short)-1));
        }
    }

    @Override
    public String getDtUltRinnTacDb() {
        return ws.getdCollDb().getPervRichDb();
    }

    @Override
    public void setDtUltRinnTacDb(String dtUltRinnTacDb) {
        this.ws.getdCollDb().setPervRichDb(dtUltRinnTacDb);
    }

    @Override
    public String getDtUltRinnTacDbObj() {
        if (ws.getIndDColl().getDtUltRinnTac() >= 0) {
            return getDtUltRinnTacDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltRinnTacDbObj(String dtUltRinnTacDbObj) {
        if (dtUltRinnTacDbObj != null) {
            setDtUltRinnTacDb(dtUltRinnTacDbObj);
            ws.getIndDColl().setDtUltRinnTac(((short)0));
        }
        else {
            ws.getIndDColl().setDtUltRinnTac(((short)-1));
        }
    }

    @Override
    public int getDurAaAdesDflt() {
        return dColl.getDcoDurAaAdesDflt().getDcoDurAaAdesDflt();
    }

    @Override
    public void setDurAaAdesDflt(int durAaAdesDflt) {
        this.dColl.getDcoDurAaAdesDflt().setDcoDurAaAdesDflt(durAaAdesDflt);
    }

    @Override
    public Integer getDurAaAdesDfltObj() {
        if (ws.getIndDColl().getDurAaAdesDflt() >= 0) {
            return ((Integer)getDurAaAdesDflt());
        }
        else {
            return null;
        }
    }

    @Override
    public void setDurAaAdesDfltObj(Integer durAaAdesDfltObj) {
        if (durAaAdesDfltObj != null) {
            setDurAaAdesDflt(((int)durAaAdesDfltObj));
            ws.getIndDColl().setDurAaAdesDflt(((short)0));
        }
        else {
            ws.getIndDColl().setDurAaAdesDflt(((short)-1));
        }
    }

    @Override
    public int getDurGgAdesDflt() {
        return dColl.getDcoDurGgAdesDflt().getDcoDurGgAdesDflt();
    }

    @Override
    public void setDurGgAdesDflt(int durGgAdesDflt) {
        this.dColl.getDcoDurGgAdesDflt().setDcoDurGgAdesDflt(durGgAdesDflt);
    }

    @Override
    public Integer getDurGgAdesDfltObj() {
        if (ws.getIndDColl().getDurGgAdesDflt() >= 0) {
            return ((Integer)getDurGgAdesDflt());
        }
        else {
            return null;
        }
    }

    @Override
    public void setDurGgAdesDfltObj(Integer durGgAdesDfltObj) {
        if (durGgAdesDfltObj != null) {
            setDurGgAdesDflt(((int)durGgAdesDfltObj));
            ws.getIndDColl().setDurGgAdesDflt(((short)0));
        }
        else {
            ws.getIndDColl().setDurGgAdesDflt(((short)-1));
        }
    }

    @Override
    public int getDurMmAdesDflt() {
        return dColl.getDcoDurMmAdesDflt().getDcoDurMmAdesDflt();
    }

    @Override
    public void setDurMmAdesDflt(int durMmAdesDflt) {
        this.dColl.getDcoDurMmAdesDflt().setDcoDurMmAdesDflt(durMmAdesDflt);
    }

    @Override
    public Integer getDurMmAdesDfltObj() {
        if (ws.getIndDColl().getDurMmAdesDflt() >= 0) {
            return ((Integer)getDurMmAdesDflt());
        }
        else {
            return null;
        }
    }

    @Override
    public void setDurMmAdesDfltObj(Integer durMmAdesDfltObj) {
        if (durMmAdesDfltObj != null) {
            setDurMmAdesDflt(((int)durMmAdesDfltObj));
            ws.getIndDColl().setDurMmAdesDflt(((short)0));
        }
        else {
            ws.getIndDColl().setDurMmAdesDflt(((short)-1));
        }
    }

    @Override
    public int getEtaScadFemmDflt() {
        return dColl.getDcoEtaScadFemmDflt().getDcoEtaScadFemmDflt();
    }

    @Override
    public void setEtaScadFemmDflt(int etaScadFemmDflt) {
        this.dColl.getDcoEtaScadFemmDflt().setDcoEtaScadFemmDflt(etaScadFemmDflt);
    }

    @Override
    public Integer getEtaScadFemmDfltObj() {
        if (ws.getIndDColl().getEtaScadFemmDflt() >= 0) {
            return ((Integer)getEtaScadFemmDflt());
        }
        else {
            return null;
        }
    }

    @Override
    public void setEtaScadFemmDfltObj(Integer etaScadFemmDfltObj) {
        if (etaScadFemmDfltObj != null) {
            setEtaScadFemmDflt(((int)etaScadFemmDfltObj));
            ws.getIndDColl().setEtaScadFemmDflt(((short)0));
        }
        else {
            ws.getIndDColl().setEtaScadFemmDflt(((short)-1));
        }
    }

    @Override
    public int getEtaScadMascDflt() {
        return dColl.getDcoEtaScadMascDflt().getDcoEtaScadMascDflt();
    }

    @Override
    public void setEtaScadMascDflt(int etaScadMascDflt) {
        this.dColl.getDcoEtaScadMascDflt().setDcoEtaScadMascDflt(etaScadMascDflt);
    }

    @Override
    public Integer getEtaScadMascDfltObj() {
        if (ws.getIndDColl().getEtaScadMascDflt() >= 0) {
            return ((Integer)getEtaScadMascDflt());
        }
        else {
            return null;
        }
    }

    @Override
    public void setEtaScadMascDfltObj(Integer etaScadMascDfltObj) {
        if (etaScadMascDfltObj != null) {
            setEtaScadMascDflt(((int)etaScadMascDfltObj));
            ws.getIndDColl().setEtaScadMascDflt(((short)0));
        }
        else {
            ws.getIndDColl().setEtaScadMascDflt(((short)-1));
        }
    }

    @Override
    public char getFlAdesSing() {
        return dColl.getDcoFlAdesSing();
    }

    @Override
    public void setFlAdesSing(char flAdesSing) {
        this.dColl.setDcoFlAdesSing(flAdesSing);
    }

    @Override
    public Character getFlAdesSingObj() {
        if (ws.getIndDColl().getFlAdesSing() >= 0) {
            return ((Character)getFlAdesSing());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlAdesSingObj(Character flAdesSingObj) {
        if (flAdesSingObj != null) {
            setFlAdesSing(((char)flAdesSingObj));
            ws.getIndDColl().setFlAdesSing(((short)0));
        }
        else {
            ws.getIndDColl().setFlAdesSing(((short)-1));
        }
    }

    @Override
    public char getFlCnbtAutes() {
        return dColl.getDcoFlCnbtAutes();
    }

    @Override
    public void setFlCnbtAutes(char flCnbtAutes) {
        this.dColl.setDcoFlCnbtAutes(flCnbtAutes);
    }

    @Override
    public Character getFlCnbtAutesObj() {
        if (ws.getIndDColl().getFlCnbtAutes() >= 0) {
            return ((Character)getFlCnbtAutes());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlCnbtAutesObj(Character flCnbtAutesObj) {
        if (flCnbtAutesObj != null) {
            setFlCnbtAutes(((char)flCnbtAutesObj));
            ws.getIndDColl().setFlCnbtAutes(((short)0));
        }
        else {
            ws.getIndDColl().setFlCnbtAutes(((short)-1));
        }
    }

    @Override
    public char getFlComnzFndIs() {
        return dColl.getDcoFlComnzFndIs();
    }

    @Override
    public void setFlComnzFndIs(char flComnzFndIs) {
        this.dColl.setDcoFlComnzFndIs(flComnzFndIs);
    }

    @Override
    public Character getFlComnzFndIsObj() {
        if (ws.getIndDColl().getFlComnzFndIs() >= 0) {
            return ((Character)getFlComnzFndIs());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlComnzFndIsObj(Character flComnzFndIsObj) {
        if (flComnzFndIsObj != null) {
            setFlComnzFndIs(((char)flComnzFndIsObj));
            ws.getIndDColl().setFlComnzFndIs(((short)0));
        }
        else {
            ws.getIndDColl().setFlComnzFndIs(((short)-1));
        }
    }

    @Override
    public char getFlDistintaCnbtva() {
        return dColl.getDcoFlDistintaCnbtva();
    }

    @Override
    public void setFlDistintaCnbtva(char flDistintaCnbtva) {
        this.dColl.setDcoFlDistintaCnbtva(flDistintaCnbtva);
    }

    @Override
    public Character getFlDistintaCnbtvaObj() {
        if (ws.getIndDColl().getFlDistintaCnbtva() >= 0) {
            return ((Character)getFlDistintaCnbtva());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlDistintaCnbtvaObj(Character flDistintaCnbtvaObj) {
        if (flDistintaCnbtvaObj != null) {
            setFlDistintaCnbtva(((char)flDistintaCnbtvaObj));
            ws.getIndDColl().setFlDistintaCnbtva(((short)0));
        }
        else {
            ws.getIndDColl().setFlDistintaCnbtva(((short)-1));
        }
    }

    @Override
    public char getFlNoAderenti() {
        return dColl.getDcoFlNoAderenti();
    }

    @Override
    public void setFlNoAderenti(char flNoAderenti) {
        this.dColl.setDcoFlNoAderenti(flNoAderenti);
    }

    @Override
    public Character getFlNoAderentiObj() {
        if (ws.getIndDColl().getFlNoAderenti() >= 0) {
            return ((Character)getFlNoAderenti());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlNoAderentiObj(Character flNoAderentiObj) {
        if (flNoAderentiObj != null) {
            setFlNoAderenti(((char)flNoAderentiObj));
            ws.getIndDColl().setFlNoAderenti(((short)0));
        }
        else {
            ws.getIndDColl().setFlNoAderenti(((short)-1));
        }
    }

    @Override
    public char getFlQtzPostEmis() {
        return dColl.getDcoFlQtzPostEmis();
    }

    @Override
    public void setFlQtzPostEmis(char flQtzPostEmis) {
        this.dColl.setDcoFlQtzPostEmis(flQtzPostEmis);
    }

    @Override
    public Character getFlQtzPostEmisObj() {
        if (ws.getIndDColl().getFlQtzPostEmis() >= 0) {
            return ((Character)getFlQtzPostEmis());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlQtzPostEmisObj(Character flQtzPostEmisObj) {
        if (flQtzPostEmisObj != null) {
            setFlQtzPostEmis(((char)flQtzPostEmisObj));
            ws.getIndDColl().setFlQtzPostEmis(((short)0));
        }
        else {
            ws.getIndDColl().setFlQtzPostEmis(((short)-1));
        }
    }

    @Override
    public char getFlRiclPreDaCpt() {
        return dColl.getDcoFlRiclPreDaCpt();
    }

    @Override
    public void setFlRiclPreDaCpt(char flRiclPreDaCpt) {
        this.dColl.setDcoFlRiclPreDaCpt(flRiclPreDaCpt);
    }

    @Override
    public Character getFlRiclPreDaCptObj() {
        if (ws.getIndDColl().getFlRiclPreDaCpt() >= 0) {
            return ((Character)getFlRiclPreDaCpt());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlRiclPreDaCptObj(Character flRiclPreDaCptObj) {
        if (flRiclPreDaCptObj != null) {
            setFlRiclPreDaCpt(((char)flRiclPreDaCptObj));
            ws.getIndDColl().setFlRiclPreDaCpt(((short)0));
        }
        else {
            ws.getIndDColl().setFlRiclPreDaCpt(((short)-1));
        }
    }

    @Override
    public int getFrazDflt() {
        return dColl.getDcoFrazDflt().getDcoFrazDflt();
    }

    @Override
    public void setFrazDflt(int frazDflt) {
        this.dColl.getDcoFrazDflt().setDcoFrazDflt(frazDflt);
    }

    @Override
    public Integer getFrazDfltObj() {
        if (ws.getIndDColl().getFrazDflt() >= 0) {
            return ((Integer)getFrazDflt());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFrazDfltObj(Integer frazDfltObj) {
        if (frazDfltObj != null) {
            setFrazDflt(((int)frazDfltObj));
            ws.getIndDColl().setFrazDflt(((short)0));
        }
        else {
            ws.getIndDColl().setFrazDflt(((short)-1));
        }
    }

    @Override
    public int getIdDColl() {
        return dColl.getDcoIdDColl();
    }

    @Override
    public void setIdDColl(int idDColl) {
        this.dColl.setDcoIdDColl(idDColl);
    }

    @Override
    public int getIdMoviChiu() {
        return dColl.getDcoIdMoviChiu().getDcoIdMoviChiu();
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        this.dColl.getDcoIdMoviChiu().setDcoIdMoviChiu(idMoviChiu);
    }

    @Override
    public Integer getIdMoviChiuObj() {
        if (ws.getIndDColl().getIdMoviChiu() >= 0) {
            return ((Integer)getIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        if (idMoviChiuObj != null) {
            setIdMoviChiu(((int)idMoviChiuObj));
            ws.getIndDColl().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndDColl().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getIdMoviCrz() {
        return dColl.getDcoIdMoviCrz();
    }

    @Override
    public void setIdMoviCrz(int idMoviCrz) {
        this.dColl.setDcoIdMoviCrz(idMoviCrz);
    }

    @Override
    public int getIdPoli() {
        return dColl.getDcoIdPoli();
    }

    @Override
    public void setIdPoli(int idPoli) {
        this.dColl.setDcoIdPoli(idPoli);
    }

    @Override
    public AfDecimal getImpArrotPre() {
        return dColl.getDcoImpArrotPre().getDcoImpArrotPre();
    }

    @Override
    public void setImpArrotPre(AfDecimal impArrotPre) {
        this.dColl.getDcoImpArrotPre().setDcoImpArrotPre(impArrotPre.copy());
    }

    @Override
    public AfDecimal getImpArrotPreObj() {
        if (ws.getIndDColl().getImpArrotPre() >= 0) {
            return getImpArrotPre();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpArrotPreObj(AfDecimal impArrotPreObj) {
        if (impArrotPreObj != null) {
            setImpArrotPre(new AfDecimal(impArrotPreObj, 15, 3));
            ws.getIndDColl().setImpArrotPre(((short)0));
        }
        else {
            ws.getIndDColl().setImpArrotPre(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpScon() {
        return dColl.getDcoImpScon().getDcoImpScon();
    }

    @Override
    public void setImpScon(AfDecimal impScon) {
        this.dColl.getDcoImpScon().setDcoImpScon(impScon.copy());
    }

    @Override
    public AfDecimal getImpSconObj() {
        if (ws.getIndDColl().getImpScon() >= 0) {
            return getImpScon();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpSconObj(AfDecimal impSconObj) {
        if (impSconObj != null) {
            setImpScon(new AfDecimal(impSconObj, 15, 3));
            ws.getIndDColl().setImpScon(((short)0));
        }
        else {
            ws.getIndDColl().setImpScon(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcScon() {
        return dColl.getDcoPcScon().getDcoPcScon();
    }

    @Override
    public void setPcScon(AfDecimal pcScon) {
        this.dColl.getDcoPcScon().setDcoPcScon(pcScon.copy());
    }

    @Override
    public AfDecimal getPcSconObj() {
        if (ws.getIndDColl().getPcScon() >= 0) {
            return getPcScon();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcSconObj(AfDecimal pcSconObj) {
        if (pcSconObj != null) {
            setPcScon(new AfDecimal(pcSconObj, 6, 3));
            ws.getIndDColl().setPcScon(((short)0));
        }
        else {
            ws.getIndDColl().setPcScon(((short)-1));
        }
    }

    @Override
    public String getTpAdes() {
        return dColl.getDcoTpAdes();
    }

    @Override
    public void setTpAdes(String tpAdes) {
        this.dColl.setDcoTpAdes(tpAdes);
    }

    @Override
    public String getTpAdesObj() {
        if (ws.getIndDColl().getTpAdes() >= 0) {
            return getTpAdes();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpAdesObj(String tpAdesObj) {
        if (tpAdesObj != null) {
            setTpAdes(tpAdesObj);
            ws.getIndDColl().setTpAdes(((short)0));
        }
        else {
            ws.getIndDColl().setTpAdes(((short)-1));
        }
    }

    @Override
    public String getTpCalcDur() {
        return dColl.getDcoTpCalcDur();
    }

    @Override
    public void setTpCalcDur(String tpCalcDur) {
        this.dColl.setDcoTpCalcDur(tpCalcDur);
    }

    @Override
    public String getTpCalcDurObj() {
        if (ws.getIndDColl().getTpCalcDur() >= 0) {
            return getTpCalcDur();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpCalcDurObj(String tpCalcDurObj) {
        if (tpCalcDurObj != null) {
            setTpCalcDur(tpCalcDurObj);
            ws.getIndDColl().setTpCalcDur(((short)0));
        }
        else {
            ws.getIndDColl().setTpCalcDur(((short)-1));
        }
    }

    @Override
    public String getTpDfltDur() {
        return dColl.getDcoTpDfltDur();
    }

    @Override
    public void setTpDfltDur(String tpDfltDur) {
        this.dColl.setDcoTpDfltDur(tpDfltDur);
    }

    @Override
    public String getTpDfltDurObj() {
        if (ws.getIndDColl().getTpDfltDur() >= 0) {
            return getTpDfltDur();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpDfltDurObj(String tpDfltDurObj) {
        if (tpDfltDurObj != null) {
            setTpDfltDur(tpDfltDurObj);
            ws.getIndDColl().setTpDfltDur(((short)0));
        }
        else {
            ws.getIndDColl().setTpDfltDur(((short)-1));
        }
    }

    @Override
    public String getTpDur() {
        return dColl.getDcoTpDur();
    }

    @Override
    public void setTpDur(String tpDur) {
        this.dColl.setDcoTpDur(tpDur);
    }

    @Override
    public String getTpDurObj() {
        if (ws.getIndDColl().getTpDur() >= 0) {
            return getTpDur();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpDurObj(String tpDurObj) {
        if (tpDurObj != null) {
            setTpDur(tpDurObj);
            ws.getIndDColl().setTpDur(((short)0));
        }
        else {
            ws.getIndDColl().setTpDur(((short)-1));
        }
    }

    @Override
    public String getTpImp() {
        return dColl.getDcoTpImp();
    }

    @Override
    public void setTpImp(String tpImp) {
        this.dColl.setDcoTpImp(tpImp);
    }

    @Override
    public String getTpImpObj() {
        if (ws.getIndDColl().getTpImp() >= 0) {
            return getTpImp();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpImpObj(String tpImpObj) {
        if (tpImpObj != null) {
            setTpImp(tpImpObj);
            ws.getIndDColl().setTpImp(((short)0));
        }
        else {
            ws.getIndDColl().setTpImp(((short)-1));
        }
    }
}
