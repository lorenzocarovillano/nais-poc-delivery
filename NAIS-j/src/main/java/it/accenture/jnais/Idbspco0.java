package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.ParamCompDao;
import it.accenture.jnais.commons.data.to.IParamComp;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idbspco0Data;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.ParamCompIdbspco0;
import it.accenture.jnais.ws.redefines.PcoAaUti;
import it.accenture.jnais.ws.redefines.PcoArrotPre;
import it.accenture.jnais.ws.redefines.PcoDtCont;
import it.accenture.jnais.ws.redefines.PcoDtEstrAssMag70a;
import it.accenture.jnais.ws.redefines.PcoDtEstrAssMin70a;
import it.accenture.jnais.ws.redefines.PcoDtRiatRiassComm;
import it.accenture.jnais.ws.redefines.PcoDtRiatRiassRsh;
import it.accenture.jnais.ws.redefines.PcoDtRiclRiriasCom;
import it.accenture.jnais.ws.redefines.PcoDtUltAggErogRe;
import it.accenture.jnais.ws.redefines.PcoDtUltBollCoriC;
import it.accenture.jnais.ws.redefines.PcoDtUltBollCoriI;
import it.accenture.jnais.ws.redefines.PcoDtUltBollCotrC;
import it.accenture.jnais.ws.redefines.PcoDtUltBollCotrI;
import it.accenture.jnais.ws.redefines.PcoDtUltBollEmes;
import it.accenture.jnais.ws.redefines.PcoDtUltBollEmesI;
import it.accenture.jnais.ws.redefines.PcoDtUltBollLiq;
import it.accenture.jnais.ws.redefines.PcoDtUltBollPerfC;
import it.accenture.jnais.ws.redefines.PcoDtUltBollPerfI;
import it.accenture.jnais.ws.redefines.PcoDtUltBollPreC;
import it.accenture.jnais.ws.redefines.PcoDtUltBollPreI;
import it.accenture.jnais.ws.redefines.PcoDtUltBollQuieC;
import it.accenture.jnais.ws.redefines.PcoDtUltBollQuieI;
import it.accenture.jnais.ws.redefines.PcoDtUltBollRiat;
import it.accenture.jnais.ws.redefines.PcoDtUltBollRiatI;
import it.accenture.jnais.ws.redefines.PcoDtUltBollRpCl;
import it.accenture.jnais.ws.redefines.PcoDtUltBollRpIn;
import it.accenture.jnais.ws.redefines.PcoDtUltBollRspCl;
import it.accenture.jnais.ws.redefines.PcoDtUltBollRspIn;
import it.accenture.jnais.ws.redefines.PcoDtUltBollSdI;
import it.accenture.jnais.ws.redefines.PcoDtUltBollSdnlI;
import it.accenture.jnais.ws.redefines.PcoDtUltBollSnden;
import it.accenture.jnais.ws.redefines.PcoDtUltBollSndnlq;
import it.accenture.jnais.ws.redefines.PcoDtUltBollStor;
import it.accenture.jnais.ws.redefines.PcoDtUltBollStorI;
import it.accenture.jnais.ws.redefines.PcoDtUltcBnsfdtCl;
import it.accenture.jnais.ws.redefines.PcoDtUltcBnsfdtIn;
import it.accenture.jnais.ws.redefines.PcoDtUltcBnsricCl;
import it.accenture.jnais.ws.redefines.PcoDtUltcBnsricIn;
import it.accenture.jnais.ws.redefines.PcoDtUltcIsCl;
import it.accenture.jnais.ws.redefines.PcoDtUltcIsIn;
import it.accenture.jnais.ws.redefines.PcoDtUltcMarsol;
import it.accenture.jnais.ws.redefines.PcoDtUltcPildiAaC;
import it.accenture.jnais.ws.redefines.PcoDtUltcPildiAaI;
import it.accenture.jnais.ws.redefines.PcoDtUltcPildiMmC;
import it.accenture.jnais.ws.redefines.PcoDtUltcPildiMmI;
import it.accenture.jnais.ws.redefines.PcoDtUltcPildiTrI;
import it.accenture.jnais.ws.redefines.PcoDtUltcRbCl;
import it.accenture.jnais.ws.redefines.PcoDtUltcRbIn;
import it.accenture.jnais.ws.redefines.PcoDtUltEcIlColl;
import it.accenture.jnais.ws.redefines.PcoDtUltEcIlInd;
import it.accenture.jnais.ws.redefines.PcoDtUltEcMrmColl;
import it.accenture.jnais.ws.redefines.PcoDtUltEcMrmInd;
import it.accenture.jnais.ws.redefines.PcoDtUltEcRivColl;
import it.accenture.jnais.ws.redefines.PcoDtUltEcRivInd;
import it.accenture.jnais.ws.redefines.PcoDtUltEcTcmColl;
import it.accenture.jnais.ws.redefines.PcoDtUltEcTcmInd;
import it.accenture.jnais.ws.redefines.PcoDtUltEcUlColl;
import it.accenture.jnais.ws.redefines.PcoDtUltEcUlInd;
import it.accenture.jnais.ws.redefines.PcoDtUltElabAt92C;
import it.accenture.jnais.ws.redefines.PcoDtUltElabAt92I;
import it.accenture.jnais.ws.redefines.PcoDtUltElabAt93C;
import it.accenture.jnais.ws.redefines.PcoDtUltElabAt93I;
import it.accenture.jnais.ws.redefines.PcoDtUltElabCommef;
import it.accenture.jnais.ws.redefines.PcoDtUltElabCosAt;
import it.accenture.jnais.ws.redefines.PcoDtUltElabCosSt;
import it.accenture.jnais.ws.redefines.PcoDtUltElabLiqmef;
import it.accenture.jnais.ws.redefines.PcoDtUltElabPaspas;
import it.accenture.jnais.ws.redefines.PcoDtUltElabPrAut;
import it.accenture.jnais.ws.redefines.PcoDtUltElabPrCon;
import it.accenture.jnais.ws.redefines.PcoDtUltElabPrlcos;
import it.accenture.jnais.ws.redefines.PcoDtUltElabRedpro;
import it.accenture.jnais.ws.redefines.PcoDtUltElabSpeIn;
import it.accenture.jnais.ws.redefines.PcoDtUltElabTakeP;
import it.accenture.jnais.ws.redefines.PcoDtUltelriscparPr;
import it.accenture.jnais.ws.redefines.PcoDtUltEstrazFug;
import it.accenture.jnais.ws.redefines.PcoDtUltEstrDecCo;
import it.accenture.jnais.ws.redefines.PcoDtUltgzCed;
import it.accenture.jnais.ws.redefines.PcoDtUltgzCedColl;
import it.accenture.jnais.ws.redefines.PcoDtUltgzTrchECl;
import it.accenture.jnais.ws.redefines.PcoDtUltgzTrchEIn;
import it.accenture.jnais.ws.redefines.PcoDtUltQtzoCl;
import it.accenture.jnais.ws.redefines.PcoDtUltQtzoIn;
import it.accenture.jnais.ws.redefines.PcoDtUltRiclPre;
import it.accenture.jnais.ws.redefines.PcoDtUltRiclRiass;
import it.accenture.jnais.ws.redefines.PcoDtUltRinnColl;
import it.accenture.jnais.ws.redefines.PcoDtUltRinnGarac;
import it.accenture.jnais.ws.redefines.PcoDtUltRinnTac;
import it.accenture.jnais.ws.redefines.PcoDtUltRivalCl;
import it.accenture.jnais.ws.redefines.PcoDtUltRivalIn;
import it.accenture.jnais.ws.redefines.PcoDtUltscElabCl;
import it.accenture.jnais.ws.redefines.PcoDtUltscElabIn;
import it.accenture.jnais.ws.redefines.PcoDtUltscOpzCl;
import it.accenture.jnais.ws.redefines.PcoDtUltscOpzIn;
import it.accenture.jnais.ws.redefines.PcoDtUltTabulRiass;
import it.accenture.jnais.ws.redefines.PcoFrqCostiAtt;
import it.accenture.jnais.ws.redefines.PcoFrqCostiStornati;
import it.accenture.jnais.ws.redefines.PcoGgIntrRitPag;
import it.accenture.jnais.ws.redefines.PcoGgMaxRecProv;
import it.accenture.jnais.ws.redefines.PcoImpAssSociale;
import it.accenture.jnais.ws.redefines.PcoLimVltr;
import it.accenture.jnais.ws.redefines.PcoLmCSubrshConIn;
import it.accenture.jnais.ws.redefines.PcoLmRisConInt;
import it.accenture.jnais.ws.redefines.PcoNumGgArrIntrPr;
import it.accenture.jnais.ws.redefines.PcoNumMmCalcMora;
import it.accenture.jnais.ws.redefines.PcoPcCSubrshMarsol;
import it.accenture.jnais.ws.redefines.PcoPcGarNoriskMars;
import it.accenture.jnais.ws.redefines.PcoPcProv1aaAcq;
import it.accenture.jnais.ws.redefines.PcoPcRidImp1382011;
import it.accenture.jnais.ws.redefines.PcoPcRidImp662014;
import it.accenture.jnais.ws.redefines.PcoPcRmMarsol;
import it.accenture.jnais.ws.redefines.PcoSoglAmlPrePer;
import it.accenture.jnais.ws.redefines.PcoSoglAmlPreSavR;
import it.accenture.jnais.ws.redefines.PcoSoglAmlPreUni;
import it.accenture.jnais.ws.redefines.PcoStstXRegione;

/**Original name: IDBSPCO0<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  13 NOV 2018.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Idbspco0 extends Program implements IParamComp {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private ParamCompDao paramCompDao = new ParamCompDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Idbspco0Data ws = new Idbspco0Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: PARAM-COMP
    private ParamCompIdbspco0 paramComp;

    //==== METHODS ====
    /**Original name: PROGRAM_IDBSPCO0_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, ParamCompIdbspco0 paramComp) {
        this.idsv0003 = idsv0003;
        this.paramComp = paramComp;
        // COB_CODE: PERFORM A000-INIZIO                    THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO          THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS          THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO          THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                        a300ElaboraIdEff();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                        a400ElaboraIdpEff();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO          THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS          THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO          THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                        b300ElaboraIdCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                        b400ElaboraIdpCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                        b500ElaboraIboCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                        b600ElaboraIbsCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                        b700ElaboraIdoCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //              SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-PRIMARY-KEY
                //                 PERFORM A200-ELABORA-PK          THRU A200-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO         THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS         THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO         THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.PRIMARY_KEY:// COB_CODE: PERFORM A200-ELABORA-PK          THRU A200-EX
                        a200ElaboraPk();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO         THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS         THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO         THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Idbspco0 getInstance() {
        return ((Idbspco0)Programs.getInstance(Idbspco0.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'IDBSPCO0'   TO IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("IDBSPCO0");
        // COB_CODE: MOVE 'PARAM_COMP' TO IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("PARAM_COMP");
        // COB_CODE: MOVE '00'                     TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                   TO   IDSV0003-SQLCODE
        //                                              IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                              IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-AGG-STORICO-SOLO-INS  OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isAggStoricoSoloIns() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-PK<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a200ElaboraPk() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-PK          THRU A210-EX
        //              WHEN IDSV0003-INSERT
        //                 PERFORM A220-INSERT-PK          THRU A220-EX
        //              WHEN IDSV0003-UPDATE
        //                 PERFORM A230-UPDATE-PK          THRU A230-EX
        //              WHEN IDSV0003-DELETE
        //                 PERFORM A240-DELETE-PK          THRU A240-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-PK          THRU A210-EX
            a210SelectPk();
        }
        else if (idsv0003.getOperazione().isInsert()) {
            // COB_CODE: PERFORM A220-INSERT-PK          THRU A220-EX
            a220InsertPk();
        }
        else if (idsv0003.getOperazione().isUpdate()) {
            // COB_CODE: PERFORM A230-UPDATE-PK          THRU A230-EX
            a230UpdatePk();
        }
        else if (idsv0003.getOperazione().isDelete()) {
            // COB_CODE: PERFORM A240-DELETE-PK          THRU A240-EX
            a240DeletePk();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A300-ELABORA-ID-EFF<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void a300ElaboraIdEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A310-SELECT-ID-EFF          THRU A310-EX
            a310SelectIdEff();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A400-ELABORA-IDP-EFF<br>*/
    private void a400ElaboraIdpEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
            a410SelectIdpEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
            a460OpenCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
            a470CloseCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
            a480FetchFirstIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
            a490FetchNextIdpEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A500-ELABORA-IBO<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a500ElaboraIbo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A510-SELECT-IBO             THRU A510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A510-SELECT-IBO             THRU A510-EX
            a510SelectIbo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
            a560OpenCursorIbo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
            a570CloseCursorIbo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
            a580FetchFirstIbo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
            a590FetchNextIbo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A600-ELABORA-IBS<br>*/
    private void a600ElaboraIbs() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A610-SELECT-IBS             THRU A610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A610-SELECT-IBS             THRU A610-EX
            a610SelectIbs();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
            a660OpenCursorIbs();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
            a670CloseCursorIbs();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
            a680FetchFirstIbs();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
            a690FetchNextIbs();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A700-ELABORA-IDO<br>*/
    private void a700ElaboraIdo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A710-SELECT-IDO                 THRU A710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A710-SELECT-IDO                 THRU A710-EX
            a710SelectIdo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
            a760OpenCursorIdo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
            a770CloseCursorIdo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
            a780FetchFirstIdo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
            a790FetchNextIdo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B300-ELABORA-ID-CPZ<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void b300ElaboraIdCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
            b310SelectIdCpz();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B400-ELABORA-IDP-CPZ<br>*/
    private void b400ElaboraIdpCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
            b410SelectIdpCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
            b460OpenCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
            b470CloseCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
            b480FetchFirstIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
            b490FetchNextIdpCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B500-ELABORA-IBO-CPZ<br>*/
    private void b500ElaboraIboCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
            b510SelectIboCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
            b560OpenCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
            b570CloseCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
            b580FetchFirstIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B600-ELABORA-IBS-CPZ<br>*/
    private void b600ElaboraIbsCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
            b610SelectIbsCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
            b660OpenCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
            b670CloseCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
            b680FetchFirstIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B700-ELABORA-IDO-CPZ<br>*/
    private void b700ElaboraIdoCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
            b710SelectIdoCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
            b760OpenCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
            b770CloseCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
            b780FetchFirstIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A210-SELECT-PK<br>
	 * <pre>----
	 * ----  gestione PK
	 * ----</pre>*/
    private void a210SelectPk() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                COD_COMP_ANIA
        //                ,COD_TRAT_CIRT
        //                ,LIM_VLTR
        //                ,TP_RAT_PERF
        //                ,TP_LIV_GENZ_TIT
        //                ,ARROT_PRE
        //                ,DT_CONT
        //                ,DT_ULT_RIVAL_IN
        //                ,DT_ULT_QTZO_IN
        //                ,DT_ULT_RICL_RIASS
        //                ,DT_ULT_TABUL_RIASS
        //                ,DT_ULT_BOLL_EMES
        //                ,DT_ULT_BOLL_STOR
        //                ,DT_ULT_BOLL_LIQ
        //                ,DT_ULT_BOLL_RIAT
        //                ,DT_ULTELRISCPAR_PR
        //                ,DT_ULTC_IS_IN
        //                ,DT_ULT_RICL_PRE
        //                ,DT_ULTC_MARSOL
        //                ,DT_ULTC_RB_IN
        //                ,PC_PROV_1AA_ACQ
        //                ,MOD_INTR_PREST
        //                ,GG_MAX_REC_PROV
        //                ,DT_ULTGZ_TRCH_E_IN
        //                ,DT_ULT_BOLL_SNDEN
        //                ,DT_ULT_BOLL_SNDNLQ
        //                ,DT_ULTSC_ELAB_IN
        //                ,DT_ULTSC_OPZ_IN
        //                ,DT_ULTC_BNSRIC_IN
        //                ,DT_ULTC_BNSFDT_IN
        //                ,DT_ULT_RINN_GARAC
        //                ,DT_ULTGZ_CED
        //                ,DT_ULT_ELAB_PRLCOS
        //                ,DT_ULT_RINN_COLL
        //                ,FL_RVC_PERF
        //                ,FL_RCS_POLI_NOPERF
        //                ,FL_GEST_PLUSV
        //                ,DT_ULT_RIVAL_CL
        //                ,DT_ULT_QTZO_CL
        //                ,DT_ULTC_BNSRIC_CL
        //                ,DT_ULTC_BNSFDT_CL
        //                ,DT_ULTC_IS_CL
        //                ,DT_ULTC_RB_CL
        //                ,DT_ULTGZ_TRCH_E_CL
        //                ,DT_ULTSC_ELAB_CL
        //                ,DT_ULTSC_OPZ_CL
        //                ,STST_X_REGIONE
        //                ,DT_ULTGZ_CED_COLL
        //                ,TP_MOD_RIVAL
        //                ,NUM_MM_CALC_MORA
        //                ,DT_ULT_EC_RIV_COLL
        //                ,DT_ULT_EC_RIV_IND
        //                ,DT_ULT_EC_IL_COLL
        //                ,DT_ULT_EC_IL_IND
        //                ,DT_ULT_EC_UL_COLL
        //                ,DT_ULT_EC_UL_IND
        //                ,AA_UTI
        //                ,CALC_RSH_COMUN
        //                ,FL_LIV_DEBUG
        //                ,DT_ULT_BOLL_PERF_C
        //                ,DT_ULT_BOLL_RSP_IN
        //                ,DT_ULT_BOLL_RSP_CL
        //                ,DT_ULT_BOLL_EMES_I
        //                ,DT_ULT_BOLL_STOR_I
        //                ,DT_ULT_BOLL_RIAT_I
        //                ,DT_ULT_BOLL_SD_I
        //                ,DT_ULT_BOLL_SDNL_I
        //                ,DT_ULT_BOLL_PERF_I
        //                ,DT_RICL_RIRIAS_COM
        //                ,DT_ULT_ELAB_AT92_C
        //                ,DT_ULT_ELAB_AT92_I
        //                ,DT_ULT_ELAB_AT93_C
        //                ,DT_ULT_ELAB_AT93_I
        //                ,DT_ULT_ELAB_SPE_IN
        //                ,DT_ULT_ELAB_PR_CON
        //                ,DT_ULT_BOLL_RP_CL
        //                ,DT_ULT_BOLL_RP_IN
        //                ,DT_ULT_BOLL_PRE_I
        //                ,DT_ULT_BOLL_PRE_C
        //                ,DT_ULTC_PILDI_MM_C
        //                ,DT_ULTC_PILDI_AA_C
        //                ,DT_ULTC_PILDI_MM_I
        //                ,DT_ULTC_PILDI_TR_I
        //                ,DT_ULTC_PILDI_AA_I
        //                ,DT_ULT_BOLL_QUIE_C
        //                ,DT_ULT_BOLL_QUIE_I
        //                ,DT_ULT_BOLL_COTR_I
        //                ,DT_ULT_BOLL_COTR_C
        //                ,DT_ULT_BOLL_CORI_C
        //                ,DT_ULT_BOLL_CORI_I
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,TP_VALZZ_DT_VLT
        //                ,FL_FRAZ_PROV_ACQ
        //                ,DT_ULT_AGG_EROG_RE
        //                ,PC_RM_MARSOL
        //                ,PC_C_SUBRSH_MARSOL
        //                ,COD_COMP_ISVAP
        //                ,LM_RIS_CON_INT
        //                ,LM_C_SUBRSH_CON_IN
        //                ,PC_GAR_NORISK_MARS
        //                ,CRZ_1A_RAT_INTR_PR
        //                ,NUM_GG_ARR_INTR_PR
        //                ,FL_VISUAL_VINPG
        //                ,DT_ULT_ESTRAZ_FUG
        //                ,DT_ULT_ELAB_PR_AUT
        //                ,DT_ULT_ELAB_COMMEF
        //                ,DT_ULT_ELAB_LIQMEF
        //                ,COD_FISC_MEF
        //                ,IMP_ASS_SOCIALE
        //                ,MOD_COMNZ_INVST_SW
        //                ,DT_RIAT_RIASS_RSH
        //                ,DT_RIAT_RIASS_COMM
        //                ,GG_INTR_RIT_PAG
        //                ,DT_ULT_RINN_TAC
        //                ,DESC_COMP
        //                ,DT_ULT_EC_TCM_IND
        //                ,DT_ULT_EC_TCM_COLL
        //                ,DT_ULT_EC_MRM_IND
        //                ,DT_ULT_EC_MRM_COLL
        //                ,COD_COMP_LDAP
        //                ,PC_RID_IMP_1382011
        //                ,PC_RID_IMP_662014
        //                ,SOGL_AML_PRE_UNI
        //                ,SOGL_AML_PRE_PER
        //                ,COD_SOGG_FTZ_ASSTO
        //                ,DT_ULT_ELAB_REDPRO
        //                ,DT_ULT_ELAB_TAKE_P
        //                ,DT_ULT_ELAB_PASPAS
        //                ,SOGL_AML_PRE_SAV_R
        //                ,DT_ULT_ESTR_DEC_CO
        //                ,DT_ULT_ELAB_COS_AT
        //                ,FRQ_COSTI_ATT
        //                ,DT_ULT_ELAB_COS_ST
        //                ,FRQ_COSTI_STORNATI
        //                ,DT_ESTR_ASS_MIN70A
        //                ,DT_ESTR_ASS_MAG70A
        //             INTO
        //                :PCO-COD-COMP-ANIA
        //               ,:PCO-COD-TRAT-CIRT
        //                :IND-PCO-COD-TRAT-CIRT
        //               ,:PCO-LIM-VLTR
        //                :IND-PCO-LIM-VLTR
        //               ,:PCO-TP-RAT-PERF
        //                :IND-PCO-TP-RAT-PERF
        //               ,:PCO-TP-LIV-GENZ-TIT
        //               ,:PCO-ARROT-PRE
        //                :IND-PCO-ARROT-PRE
        //               ,:PCO-DT-CONT-DB
        //                :IND-PCO-DT-CONT
        //               ,:PCO-DT-ULT-RIVAL-IN-DB
        //                :IND-PCO-DT-ULT-RIVAL-IN
        //               ,:PCO-DT-ULT-QTZO-IN-DB
        //                :IND-PCO-DT-ULT-QTZO-IN
        //               ,:PCO-DT-ULT-RICL-RIASS-DB
        //                :IND-PCO-DT-ULT-RICL-RIASS
        //               ,:PCO-DT-ULT-TABUL-RIASS-DB
        //                :IND-PCO-DT-ULT-TABUL-RIASS
        //               ,:PCO-DT-ULT-BOLL-EMES-DB
        //                :IND-PCO-DT-ULT-BOLL-EMES
        //               ,:PCO-DT-ULT-BOLL-STOR-DB
        //                :IND-PCO-DT-ULT-BOLL-STOR
        //               ,:PCO-DT-ULT-BOLL-LIQ-DB
        //                :IND-PCO-DT-ULT-BOLL-LIQ
        //               ,:PCO-DT-ULT-BOLL-RIAT-DB
        //                :IND-PCO-DT-ULT-BOLL-RIAT
        //               ,:PCO-DT-ULTELRISCPAR-PR-DB
        //                :IND-PCO-DT-ULTELRISCPAR-PR
        //               ,:PCO-DT-ULTC-IS-IN-DB
        //                :IND-PCO-DT-ULTC-IS-IN
        //               ,:PCO-DT-ULT-RICL-PRE-DB
        //                :IND-PCO-DT-ULT-RICL-PRE
        //               ,:PCO-DT-ULTC-MARSOL-DB
        //                :IND-PCO-DT-ULTC-MARSOL
        //               ,:PCO-DT-ULTC-RB-IN-DB
        //                :IND-PCO-DT-ULTC-RB-IN
        //               ,:PCO-PC-PROV-1AA-ACQ
        //                :IND-PCO-PC-PROV-1AA-ACQ
        //               ,:PCO-MOD-INTR-PREST
        //                :IND-PCO-MOD-INTR-PREST
        //               ,:PCO-GG-MAX-REC-PROV
        //                :IND-PCO-GG-MAX-REC-PROV
        //               ,:PCO-DT-ULTGZ-TRCH-E-IN-DB
        //                :IND-PCO-DT-ULTGZ-TRCH-E-IN
        //               ,:PCO-DT-ULT-BOLL-SNDEN-DB
        //                :IND-PCO-DT-ULT-BOLL-SNDEN
        //               ,:PCO-DT-ULT-BOLL-SNDNLQ-DB
        //                :IND-PCO-DT-ULT-BOLL-SNDNLQ
        //               ,:PCO-DT-ULTSC-ELAB-IN-DB
        //                :IND-PCO-DT-ULTSC-ELAB-IN
        //               ,:PCO-DT-ULTSC-OPZ-IN-DB
        //                :IND-PCO-DT-ULTSC-OPZ-IN
        //               ,:PCO-DT-ULTC-BNSRIC-IN-DB
        //                :IND-PCO-DT-ULTC-BNSRIC-IN
        //               ,:PCO-DT-ULTC-BNSFDT-IN-DB
        //                :IND-PCO-DT-ULTC-BNSFDT-IN
        //               ,:PCO-DT-ULT-RINN-GARAC-DB
        //                :IND-PCO-DT-ULT-RINN-GARAC
        //               ,:PCO-DT-ULTGZ-CED-DB
        //                :IND-PCO-DT-ULTGZ-CED
        //               ,:PCO-DT-ULT-ELAB-PRLCOS-DB
        //                :IND-PCO-DT-ULT-ELAB-PRLCOS
        //               ,:PCO-DT-ULT-RINN-COLL-DB
        //                :IND-PCO-DT-ULT-RINN-COLL
        //               ,:PCO-FL-RVC-PERF
        //                :IND-PCO-FL-RVC-PERF
        //               ,:PCO-FL-RCS-POLI-NOPERF
        //                :IND-PCO-FL-RCS-POLI-NOPERF
        //               ,:PCO-FL-GEST-PLUSV
        //                :IND-PCO-FL-GEST-PLUSV
        //               ,:PCO-DT-ULT-RIVAL-CL-DB
        //                :IND-PCO-DT-ULT-RIVAL-CL
        //               ,:PCO-DT-ULT-QTZO-CL-DB
        //                :IND-PCO-DT-ULT-QTZO-CL
        //               ,:PCO-DT-ULTC-BNSRIC-CL-DB
        //                :IND-PCO-DT-ULTC-BNSRIC-CL
        //               ,:PCO-DT-ULTC-BNSFDT-CL-DB
        //                :IND-PCO-DT-ULTC-BNSFDT-CL
        //               ,:PCO-DT-ULTC-IS-CL-DB
        //                :IND-PCO-DT-ULTC-IS-CL
        //               ,:PCO-DT-ULTC-RB-CL-DB
        //                :IND-PCO-DT-ULTC-RB-CL
        //               ,:PCO-DT-ULTGZ-TRCH-E-CL-DB
        //                :IND-PCO-DT-ULTGZ-TRCH-E-CL
        //               ,:PCO-DT-ULTSC-ELAB-CL-DB
        //                :IND-PCO-DT-ULTSC-ELAB-CL
        //               ,:PCO-DT-ULTSC-OPZ-CL-DB
        //                :IND-PCO-DT-ULTSC-OPZ-CL
        //               ,:PCO-STST-X-REGIONE-DB
        //                :IND-PCO-STST-X-REGIONE
        //               ,:PCO-DT-ULTGZ-CED-COLL-DB
        //                :IND-PCO-DT-ULTGZ-CED-COLL
        //               ,:PCO-TP-MOD-RIVAL
        //               ,:PCO-NUM-MM-CALC-MORA
        //                :IND-PCO-NUM-MM-CALC-MORA
        //               ,:PCO-DT-ULT-EC-RIV-COLL-DB
        //                :IND-PCO-DT-ULT-EC-RIV-COLL
        //               ,:PCO-DT-ULT-EC-RIV-IND-DB
        //                :IND-PCO-DT-ULT-EC-RIV-IND
        //               ,:PCO-DT-ULT-EC-IL-COLL-DB
        //                :IND-PCO-DT-ULT-EC-IL-COLL
        //               ,:PCO-DT-ULT-EC-IL-IND-DB
        //                :IND-PCO-DT-ULT-EC-IL-IND
        //               ,:PCO-DT-ULT-EC-UL-COLL-DB
        //                :IND-PCO-DT-ULT-EC-UL-COLL
        //               ,:PCO-DT-ULT-EC-UL-IND-DB
        //                :IND-PCO-DT-ULT-EC-UL-IND
        //               ,:PCO-AA-UTI
        //                :IND-PCO-AA-UTI
        //               ,:PCO-CALC-RSH-COMUN
        //               ,:PCO-FL-LIV-DEBUG
        //               ,:PCO-DT-ULT-BOLL-PERF-C-DB
        //                :IND-PCO-DT-ULT-BOLL-PERF-C
        //               ,:PCO-DT-ULT-BOLL-RSP-IN-DB
        //                :IND-PCO-DT-ULT-BOLL-RSP-IN
        //               ,:PCO-DT-ULT-BOLL-RSP-CL-DB
        //                :IND-PCO-DT-ULT-BOLL-RSP-CL
        //               ,:PCO-DT-ULT-BOLL-EMES-I-DB
        //                :IND-PCO-DT-ULT-BOLL-EMES-I
        //               ,:PCO-DT-ULT-BOLL-STOR-I-DB
        //                :IND-PCO-DT-ULT-BOLL-STOR-I
        //               ,:PCO-DT-ULT-BOLL-RIAT-I-DB
        //                :IND-PCO-DT-ULT-BOLL-RIAT-I
        //               ,:PCO-DT-ULT-BOLL-SD-I-DB
        //                :IND-PCO-DT-ULT-BOLL-SD-I
        //               ,:PCO-DT-ULT-BOLL-SDNL-I-DB
        //                :IND-PCO-DT-ULT-BOLL-SDNL-I
        //               ,:PCO-DT-ULT-BOLL-PERF-I-DB
        //                :IND-PCO-DT-ULT-BOLL-PERF-I
        //               ,:PCO-DT-RICL-RIRIAS-COM-DB
        //                :IND-PCO-DT-RICL-RIRIAS-COM
        //               ,:PCO-DT-ULT-ELAB-AT92-C-DB
        //                :IND-PCO-DT-ULT-ELAB-AT92-C
        //               ,:PCO-DT-ULT-ELAB-AT92-I-DB
        //                :IND-PCO-DT-ULT-ELAB-AT92-I
        //               ,:PCO-DT-ULT-ELAB-AT93-C-DB
        //                :IND-PCO-DT-ULT-ELAB-AT93-C
        //               ,:PCO-DT-ULT-ELAB-AT93-I-DB
        //                :IND-PCO-DT-ULT-ELAB-AT93-I
        //               ,:PCO-DT-ULT-ELAB-SPE-IN-DB
        //                :IND-PCO-DT-ULT-ELAB-SPE-IN
        //               ,:PCO-DT-ULT-ELAB-PR-CON-DB
        //                :IND-PCO-DT-ULT-ELAB-PR-CON
        //               ,:PCO-DT-ULT-BOLL-RP-CL-DB
        //                :IND-PCO-DT-ULT-BOLL-RP-CL
        //               ,:PCO-DT-ULT-BOLL-RP-IN-DB
        //                :IND-PCO-DT-ULT-BOLL-RP-IN
        //               ,:PCO-DT-ULT-BOLL-PRE-I-DB
        //                :IND-PCO-DT-ULT-BOLL-PRE-I
        //               ,:PCO-DT-ULT-BOLL-PRE-C-DB
        //                :IND-PCO-DT-ULT-BOLL-PRE-C
        //               ,:PCO-DT-ULTC-PILDI-MM-C-DB
        //                :IND-PCO-DT-ULTC-PILDI-MM-C
        //               ,:PCO-DT-ULTC-PILDI-AA-C-DB
        //                :IND-PCO-DT-ULTC-PILDI-AA-C
        //               ,:PCO-DT-ULTC-PILDI-MM-I-DB
        //                :IND-PCO-DT-ULTC-PILDI-MM-I
        //               ,:PCO-DT-ULTC-PILDI-TR-I-DB
        //                :IND-PCO-DT-ULTC-PILDI-TR-I
        //               ,:PCO-DT-ULTC-PILDI-AA-I-DB
        //                :IND-PCO-DT-ULTC-PILDI-AA-I
        //               ,:PCO-DT-ULT-BOLL-QUIE-C-DB
        //                :IND-PCO-DT-ULT-BOLL-QUIE-C
        //               ,:PCO-DT-ULT-BOLL-QUIE-I-DB
        //                :IND-PCO-DT-ULT-BOLL-QUIE-I
        //               ,:PCO-DT-ULT-BOLL-COTR-I-DB
        //                :IND-PCO-DT-ULT-BOLL-COTR-I
        //               ,:PCO-DT-ULT-BOLL-COTR-C-DB
        //                :IND-PCO-DT-ULT-BOLL-COTR-C
        //               ,:PCO-DT-ULT-BOLL-CORI-C-DB
        //                :IND-PCO-DT-ULT-BOLL-CORI-C
        //               ,:PCO-DT-ULT-BOLL-CORI-I-DB
        //                :IND-PCO-DT-ULT-BOLL-CORI-I
        //               ,:PCO-DS-OPER-SQL
        //               ,:PCO-DS-VER
        //               ,:PCO-DS-TS-CPTZ
        //               ,:PCO-DS-UTENTE
        //               ,:PCO-DS-STATO-ELAB
        //               ,:PCO-TP-VALZZ-DT-VLT
        //                :IND-PCO-TP-VALZZ-DT-VLT
        //               ,:PCO-FL-FRAZ-PROV-ACQ
        //                :IND-PCO-FL-FRAZ-PROV-ACQ
        //               ,:PCO-DT-ULT-AGG-EROG-RE-DB
        //                :IND-PCO-DT-ULT-AGG-EROG-RE
        //               ,:PCO-PC-RM-MARSOL
        //                :IND-PCO-PC-RM-MARSOL
        //               ,:PCO-PC-C-SUBRSH-MARSOL
        //                :IND-PCO-PC-C-SUBRSH-MARSOL
        //               ,:PCO-COD-COMP-ISVAP
        //                :IND-PCO-COD-COMP-ISVAP
        //               ,:PCO-LM-RIS-CON-INT
        //                :IND-PCO-LM-RIS-CON-INT
        //               ,:PCO-LM-C-SUBRSH-CON-IN
        //                :IND-PCO-LM-C-SUBRSH-CON-IN
        //               ,:PCO-PC-GAR-NORISK-MARS
        //                :IND-PCO-PC-GAR-NORISK-MARS
        //               ,:PCO-CRZ-1A-RAT-INTR-PR
        //                :IND-PCO-CRZ-1A-RAT-INTR-PR
        //               ,:PCO-NUM-GG-ARR-INTR-PR
        //                :IND-PCO-NUM-GG-ARR-INTR-PR
        //               ,:PCO-FL-VISUAL-VINPG
        //                :IND-PCO-FL-VISUAL-VINPG
        //               ,:PCO-DT-ULT-ESTRAZ-FUG-DB
        //                :IND-PCO-DT-ULT-ESTRAZ-FUG
        //               ,:PCO-DT-ULT-ELAB-PR-AUT-DB
        //                :IND-PCO-DT-ULT-ELAB-PR-AUT
        //               ,:PCO-DT-ULT-ELAB-COMMEF-DB
        //                :IND-PCO-DT-ULT-ELAB-COMMEF
        //               ,:PCO-DT-ULT-ELAB-LIQMEF-DB
        //                :IND-PCO-DT-ULT-ELAB-LIQMEF
        //               ,:PCO-COD-FISC-MEF
        //                :IND-PCO-COD-FISC-MEF
        //               ,:PCO-IMP-ASS-SOCIALE
        //                :IND-PCO-IMP-ASS-SOCIALE
        //               ,:PCO-MOD-COMNZ-INVST-SW
        //                :IND-PCO-MOD-COMNZ-INVST-SW
        //               ,:PCO-DT-RIAT-RIASS-RSH-DB
        //                :IND-PCO-DT-RIAT-RIASS-RSH
        //               ,:PCO-DT-RIAT-RIASS-COMM-DB
        //                :IND-PCO-DT-RIAT-RIASS-COMM
        //               ,:PCO-GG-INTR-RIT-PAG
        //                :IND-PCO-GG-INTR-RIT-PAG
        //               ,:PCO-DT-ULT-RINN-TAC-DB
        //                :IND-PCO-DT-ULT-RINN-TAC
        //               ,:PCO-DESC-COMP-VCHAR
        //                :IND-PCO-DESC-COMP
        //               ,:PCO-DT-ULT-EC-TCM-IND-DB
        //                :IND-PCO-DT-ULT-EC-TCM-IND
        //               ,:PCO-DT-ULT-EC-TCM-COLL-DB
        //                :IND-PCO-DT-ULT-EC-TCM-COLL
        //               ,:PCO-DT-ULT-EC-MRM-IND-DB
        //                :IND-PCO-DT-ULT-EC-MRM-IND
        //               ,:PCO-DT-ULT-EC-MRM-COLL-DB
        //                :IND-PCO-DT-ULT-EC-MRM-COLL
        //               ,:PCO-COD-COMP-LDAP
        //                :IND-PCO-COD-COMP-LDAP
        //               ,:PCO-PC-RID-IMP-1382011
        //                :IND-PCO-PC-RID-IMP-1382011
        //               ,:PCO-PC-RID-IMP-662014
        //                :IND-PCO-PC-RID-IMP-662014
        //               ,:PCO-SOGL-AML-PRE-UNI
        //                :IND-PCO-SOGL-AML-PRE-UNI
        //               ,:PCO-SOGL-AML-PRE-PER
        //                :IND-PCO-SOGL-AML-PRE-PER
        //               ,:PCO-COD-SOGG-FTZ-ASSTO
        //                :IND-PCO-COD-SOGG-FTZ-ASSTO
        //               ,:PCO-DT-ULT-ELAB-REDPRO-DB
        //                :IND-PCO-DT-ULT-ELAB-REDPRO
        //               ,:PCO-DT-ULT-ELAB-TAKE-P-DB
        //                :IND-PCO-DT-ULT-ELAB-TAKE-P
        //               ,:PCO-DT-ULT-ELAB-PASPAS-DB
        //                :IND-PCO-DT-ULT-ELAB-PASPAS
        //               ,:PCO-SOGL-AML-PRE-SAV-R
        //                :IND-PCO-SOGL-AML-PRE-SAV-R
        //               ,:PCO-DT-ULT-ESTR-DEC-CO-DB
        //                :IND-PCO-DT-ULT-ESTR-DEC-CO
        //               ,:PCO-DT-ULT-ELAB-COS-AT-DB
        //                :IND-PCO-DT-ULT-ELAB-COS-AT
        //               ,:PCO-FRQ-COSTI-ATT
        //                :IND-PCO-FRQ-COSTI-ATT
        //               ,:PCO-DT-ULT-ELAB-COS-ST-DB
        //                :IND-PCO-DT-ULT-ELAB-COS-ST
        //               ,:PCO-FRQ-COSTI-STORNATI
        //                :IND-PCO-FRQ-COSTI-STORNATI
        //               ,:PCO-DT-ESTR-ASS-MIN70A-DB
        //                :IND-PCO-DT-ESTR-ASS-MIN70A
        //               ,:PCO-DT-ESTR-ASS-MAG70A-DB
        //                :IND-PCO-DT-ESTR-ASS-MAG70A
        //             FROM PARAM_COMP
        //             WHERE     COD_COMP_ANIA = :PCO-COD-COMP-ANIA
        //           END-EXEC.
        paramCompDao.selectByPcoCodCompAnia(paramComp.getPcoCodCompAnia(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A220-INSERT-PK<br>*/
    private void a220InsertPk() {
        // COB_CODE: PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.
        z400SeqRiga();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX
            z150ValorizzaDataServicesI();
            // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX
            z200SetIndicatoriNull();
            // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX
            z900ConvertiNToX();
            // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX
            z960LengthVchar();
            // COB_CODE: EXEC SQL
            //              INSERT
            //              INTO PARAM_COMP
            //                  (
            //                     COD_COMP_ANIA
            //                    ,COD_TRAT_CIRT
            //                    ,LIM_VLTR
            //                    ,TP_RAT_PERF
            //                    ,TP_LIV_GENZ_TIT
            //                    ,ARROT_PRE
            //                    ,DT_CONT
            //                    ,DT_ULT_RIVAL_IN
            //                    ,DT_ULT_QTZO_IN
            //                    ,DT_ULT_RICL_RIASS
            //                    ,DT_ULT_TABUL_RIASS
            //                    ,DT_ULT_BOLL_EMES
            //                    ,DT_ULT_BOLL_STOR
            //                    ,DT_ULT_BOLL_LIQ
            //                    ,DT_ULT_BOLL_RIAT
            //                    ,DT_ULTELRISCPAR_PR
            //                    ,DT_ULTC_IS_IN
            //                    ,DT_ULT_RICL_PRE
            //                    ,DT_ULTC_MARSOL
            //                    ,DT_ULTC_RB_IN
            //                    ,PC_PROV_1AA_ACQ
            //                    ,MOD_INTR_PREST
            //                    ,GG_MAX_REC_PROV
            //                    ,DT_ULTGZ_TRCH_E_IN
            //                    ,DT_ULT_BOLL_SNDEN
            //                    ,DT_ULT_BOLL_SNDNLQ
            //                    ,DT_ULTSC_ELAB_IN
            //                    ,DT_ULTSC_OPZ_IN
            //                    ,DT_ULTC_BNSRIC_IN
            //                    ,DT_ULTC_BNSFDT_IN
            //                    ,DT_ULT_RINN_GARAC
            //                    ,DT_ULTGZ_CED
            //                    ,DT_ULT_ELAB_PRLCOS
            //                    ,DT_ULT_RINN_COLL
            //                    ,FL_RVC_PERF
            //                    ,FL_RCS_POLI_NOPERF
            //                    ,FL_GEST_PLUSV
            //                    ,DT_ULT_RIVAL_CL
            //                    ,DT_ULT_QTZO_CL
            //                    ,DT_ULTC_BNSRIC_CL
            //                    ,DT_ULTC_BNSFDT_CL
            //                    ,DT_ULTC_IS_CL
            //                    ,DT_ULTC_RB_CL
            //                    ,DT_ULTGZ_TRCH_E_CL
            //                    ,DT_ULTSC_ELAB_CL
            //                    ,DT_ULTSC_OPZ_CL
            //                    ,STST_X_REGIONE
            //                    ,DT_ULTGZ_CED_COLL
            //                    ,TP_MOD_RIVAL
            //                    ,NUM_MM_CALC_MORA
            //                    ,DT_ULT_EC_RIV_COLL
            //                    ,DT_ULT_EC_RIV_IND
            //                    ,DT_ULT_EC_IL_COLL
            //                    ,DT_ULT_EC_IL_IND
            //                    ,DT_ULT_EC_UL_COLL
            //                    ,DT_ULT_EC_UL_IND
            //                    ,AA_UTI
            //                    ,CALC_RSH_COMUN
            //                    ,FL_LIV_DEBUG
            //                    ,DT_ULT_BOLL_PERF_C
            //                    ,DT_ULT_BOLL_RSP_IN
            //                    ,DT_ULT_BOLL_RSP_CL
            //                    ,DT_ULT_BOLL_EMES_I
            //                    ,DT_ULT_BOLL_STOR_I
            //                    ,DT_ULT_BOLL_RIAT_I
            //                    ,DT_ULT_BOLL_SD_I
            //                    ,DT_ULT_BOLL_SDNL_I
            //                    ,DT_ULT_BOLL_PERF_I
            //                    ,DT_RICL_RIRIAS_COM
            //                    ,DT_ULT_ELAB_AT92_C
            //                    ,DT_ULT_ELAB_AT92_I
            //                    ,DT_ULT_ELAB_AT93_C
            //                    ,DT_ULT_ELAB_AT93_I
            //                    ,DT_ULT_ELAB_SPE_IN
            //                    ,DT_ULT_ELAB_PR_CON
            //                    ,DT_ULT_BOLL_RP_CL
            //                    ,DT_ULT_BOLL_RP_IN
            //                    ,DT_ULT_BOLL_PRE_I
            //                    ,DT_ULT_BOLL_PRE_C
            //                    ,DT_ULTC_PILDI_MM_C
            //                    ,DT_ULTC_PILDI_AA_C
            //                    ,DT_ULTC_PILDI_MM_I
            //                    ,DT_ULTC_PILDI_TR_I
            //                    ,DT_ULTC_PILDI_AA_I
            //                    ,DT_ULT_BOLL_QUIE_C
            //                    ,DT_ULT_BOLL_QUIE_I
            //                    ,DT_ULT_BOLL_COTR_I
            //                    ,DT_ULT_BOLL_COTR_C
            //                    ,DT_ULT_BOLL_CORI_C
            //                    ,DT_ULT_BOLL_CORI_I
            //                    ,DS_OPER_SQL
            //                    ,DS_VER
            //                    ,DS_TS_CPTZ
            //                    ,DS_UTENTE
            //                    ,DS_STATO_ELAB
            //                    ,TP_VALZZ_DT_VLT
            //                    ,FL_FRAZ_PROV_ACQ
            //                    ,DT_ULT_AGG_EROG_RE
            //                    ,PC_RM_MARSOL
            //                    ,PC_C_SUBRSH_MARSOL
            //                    ,COD_COMP_ISVAP
            //                    ,LM_RIS_CON_INT
            //                    ,LM_C_SUBRSH_CON_IN
            //                    ,PC_GAR_NORISK_MARS
            //                    ,CRZ_1A_RAT_INTR_PR
            //                    ,NUM_GG_ARR_INTR_PR
            //                    ,FL_VISUAL_VINPG
            //                    ,DT_ULT_ESTRAZ_FUG
            //                    ,DT_ULT_ELAB_PR_AUT
            //                    ,DT_ULT_ELAB_COMMEF
            //                    ,DT_ULT_ELAB_LIQMEF
            //                    ,COD_FISC_MEF
            //                    ,IMP_ASS_SOCIALE
            //                    ,MOD_COMNZ_INVST_SW
            //                    ,DT_RIAT_RIASS_RSH
            //                    ,DT_RIAT_RIASS_COMM
            //                    ,GG_INTR_RIT_PAG
            //                    ,DT_ULT_RINN_TAC
            //                    ,DESC_COMP
            //                    ,DT_ULT_EC_TCM_IND
            //                    ,DT_ULT_EC_TCM_COLL
            //                    ,DT_ULT_EC_MRM_IND
            //                    ,DT_ULT_EC_MRM_COLL
            //                    ,COD_COMP_LDAP
            //                    ,PC_RID_IMP_1382011
            //                    ,PC_RID_IMP_662014
            //                    ,SOGL_AML_PRE_UNI
            //                    ,SOGL_AML_PRE_PER
            //                    ,COD_SOGG_FTZ_ASSTO
            //                    ,DT_ULT_ELAB_REDPRO
            //                    ,DT_ULT_ELAB_TAKE_P
            //                    ,DT_ULT_ELAB_PASPAS
            //                    ,SOGL_AML_PRE_SAV_R
            //                    ,DT_ULT_ESTR_DEC_CO
            //                    ,DT_ULT_ELAB_COS_AT
            //                    ,FRQ_COSTI_ATT
            //                    ,DT_ULT_ELAB_COS_ST
            //                    ,FRQ_COSTI_STORNATI
            //                    ,DT_ESTR_ASS_MIN70A
            //                    ,DT_ESTR_ASS_MAG70A
            //                  )
            //              VALUES
            //                  (
            //                    :PCO-COD-COMP-ANIA
            //                    ,:PCO-COD-TRAT-CIRT
            //                     :IND-PCO-COD-TRAT-CIRT
            //                    ,:PCO-LIM-VLTR
            //                     :IND-PCO-LIM-VLTR
            //                    ,:PCO-TP-RAT-PERF
            //                     :IND-PCO-TP-RAT-PERF
            //                    ,:PCO-TP-LIV-GENZ-TIT
            //                    ,:PCO-ARROT-PRE
            //                     :IND-PCO-ARROT-PRE
            //                    ,:PCO-DT-CONT-DB
            //                     :IND-PCO-DT-CONT
            //                    ,:PCO-DT-ULT-RIVAL-IN-DB
            //                     :IND-PCO-DT-ULT-RIVAL-IN
            //                    ,:PCO-DT-ULT-QTZO-IN-DB
            //                     :IND-PCO-DT-ULT-QTZO-IN
            //                    ,:PCO-DT-ULT-RICL-RIASS-DB
            //                     :IND-PCO-DT-ULT-RICL-RIASS
            //                    ,:PCO-DT-ULT-TABUL-RIASS-DB
            //                     :IND-PCO-DT-ULT-TABUL-RIASS
            //                    ,:PCO-DT-ULT-BOLL-EMES-DB
            //                     :IND-PCO-DT-ULT-BOLL-EMES
            //                    ,:PCO-DT-ULT-BOLL-STOR-DB
            //                     :IND-PCO-DT-ULT-BOLL-STOR
            //                    ,:PCO-DT-ULT-BOLL-LIQ-DB
            //                     :IND-PCO-DT-ULT-BOLL-LIQ
            //                    ,:PCO-DT-ULT-BOLL-RIAT-DB
            //                     :IND-PCO-DT-ULT-BOLL-RIAT
            //                    ,:PCO-DT-ULTELRISCPAR-PR-DB
            //                     :IND-PCO-DT-ULTELRISCPAR-PR
            //                    ,:PCO-DT-ULTC-IS-IN-DB
            //                     :IND-PCO-DT-ULTC-IS-IN
            //                    ,:PCO-DT-ULT-RICL-PRE-DB
            //                     :IND-PCO-DT-ULT-RICL-PRE
            //                    ,:PCO-DT-ULTC-MARSOL-DB
            //                     :IND-PCO-DT-ULTC-MARSOL
            //                    ,:PCO-DT-ULTC-RB-IN-DB
            //                     :IND-PCO-DT-ULTC-RB-IN
            //                    ,:PCO-PC-PROV-1AA-ACQ
            //                     :IND-PCO-PC-PROV-1AA-ACQ
            //                    ,:PCO-MOD-INTR-PREST
            //                     :IND-PCO-MOD-INTR-PREST
            //                    ,:PCO-GG-MAX-REC-PROV
            //                     :IND-PCO-GG-MAX-REC-PROV
            //                    ,:PCO-DT-ULTGZ-TRCH-E-IN-DB
            //                     :IND-PCO-DT-ULTGZ-TRCH-E-IN
            //                    ,:PCO-DT-ULT-BOLL-SNDEN-DB
            //                     :IND-PCO-DT-ULT-BOLL-SNDEN
            //                    ,:PCO-DT-ULT-BOLL-SNDNLQ-DB
            //                     :IND-PCO-DT-ULT-BOLL-SNDNLQ
            //                    ,:PCO-DT-ULTSC-ELAB-IN-DB
            //                     :IND-PCO-DT-ULTSC-ELAB-IN
            //                    ,:PCO-DT-ULTSC-OPZ-IN-DB
            //                     :IND-PCO-DT-ULTSC-OPZ-IN
            //                    ,:PCO-DT-ULTC-BNSRIC-IN-DB
            //                     :IND-PCO-DT-ULTC-BNSRIC-IN
            //                    ,:PCO-DT-ULTC-BNSFDT-IN-DB
            //                     :IND-PCO-DT-ULTC-BNSFDT-IN
            //                    ,:PCO-DT-ULT-RINN-GARAC-DB
            //                     :IND-PCO-DT-ULT-RINN-GARAC
            //                    ,:PCO-DT-ULTGZ-CED-DB
            //                     :IND-PCO-DT-ULTGZ-CED
            //                    ,:PCO-DT-ULT-ELAB-PRLCOS-DB
            //                     :IND-PCO-DT-ULT-ELAB-PRLCOS
            //                    ,:PCO-DT-ULT-RINN-COLL-DB
            //                     :IND-PCO-DT-ULT-RINN-COLL
            //                    ,:PCO-FL-RVC-PERF
            //                     :IND-PCO-FL-RVC-PERF
            //                    ,:PCO-FL-RCS-POLI-NOPERF
            //                     :IND-PCO-FL-RCS-POLI-NOPERF
            //                    ,:PCO-FL-GEST-PLUSV
            //                     :IND-PCO-FL-GEST-PLUSV
            //                    ,:PCO-DT-ULT-RIVAL-CL-DB
            //                     :IND-PCO-DT-ULT-RIVAL-CL
            //                    ,:PCO-DT-ULT-QTZO-CL-DB
            //                     :IND-PCO-DT-ULT-QTZO-CL
            //                    ,:PCO-DT-ULTC-BNSRIC-CL-DB
            //                     :IND-PCO-DT-ULTC-BNSRIC-CL
            //                    ,:PCO-DT-ULTC-BNSFDT-CL-DB
            //                     :IND-PCO-DT-ULTC-BNSFDT-CL
            //                    ,:PCO-DT-ULTC-IS-CL-DB
            //                     :IND-PCO-DT-ULTC-IS-CL
            //                    ,:PCO-DT-ULTC-RB-CL-DB
            //                     :IND-PCO-DT-ULTC-RB-CL
            //                    ,:PCO-DT-ULTGZ-TRCH-E-CL-DB
            //                     :IND-PCO-DT-ULTGZ-TRCH-E-CL
            //                    ,:PCO-DT-ULTSC-ELAB-CL-DB
            //                     :IND-PCO-DT-ULTSC-ELAB-CL
            //                    ,:PCO-DT-ULTSC-OPZ-CL-DB
            //                     :IND-PCO-DT-ULTSC-OPZ-CL
            //                    ,:PCO-STST-X-REGIONE-DB
            //                     :IND-PCO-STST-X-REGIONE
            //                    ,:PCO-DT-ULTGZ-CED-COLL-DB
            //                     :IND-PCO-DT-ULTGZ-CED-COLL
            //                    ,:PCO-TP-MOD-RIVAL
            //                    ,:PCO-NUM-MM-CALC-MORA
            //                     :IND-PCO-NUM-MM-CALC-MORA
            //                    ,:PCO-DT-ULT-EC-RIV-COLL-DB
            //                     :IND-PCO-DT-ULT-EC-RIV-COLL
            //                    ,:PCO-DT-ULT-EC-RIV-IND-DB
            //                     :IND-PCO-DT-ULT-EC-RIV-IND
            //                    ,:PCO-DT-ULT-EC-IL-COLL-DB
            //                     :IND-PCO-DT-ULT-EC-IL-COLL
            //                    ,:PCO-DT-ULT-EC-IL-IND-DB
            //                     :IND-PCO-DT-ULT-EC-IL-IND
            //                    ,:PCO-DT-ULT-EC-UL-COLL-DB
            //                     :IND-PCO-DT-ULT-EC-UL-COLL
            //                    ,:PCO-DT-ULT-EC-UL-IND-DB
            //                     :IND-PCO-DT-ULT-EC-UL-IND
            //                    ,:PCO-AA-UTI
            //                     :IND-PCO-AA-UTI
            //                    ,:PCO-CALC-RSH-COMUN
            //                    ,:PCO-FL-LIV-DEBUG
            //                    ,:PCO-DT-ULT-BOLL-PERF-C-DB
            //                     :IND-PCO-DT-ULT-BOLL-PERF-C
            //                    ,:PCO-DT-ULT-BOLL-RSP-IN-DB
            //                     :IND-PCO-DT-ULT-BOLL-RSP-IN
            //                    ,:PCO-DT-ULT-BOLL-RSP-CL-DB
            //                     :IND-PCO-DT-ULT-BOLL-RSP-CL
            //                    ,:PCO-DT-ULT-BOLL-EMES-I-DB
            //                     :IND-PCO-DT-ULT-BOLL-EMES-I
            //                    ,:PCO-DT-ULT-BOLL-STOR-I-DB
            //                     :IND-PCO-DT-ULT-BOLL-STOR-I
            //                    ,:PCO-DT-ULT-BOLL-RIAT-I-DB
            //                     :IND-PCO-DT-ULT-BOLL-RIAT-I
            //                    ,:PCO-DT-ULT-BOLL-SD-I-DB
            //                     :IND-PCO-DT-ULT-BOLL-SD-I
            //                    ,:PCO-DT-ULT-BOLL-SDNL-I-DB
            //                     :IND-PCO-DT-ULT-BOLL-SDNL-I
            //                    ,:PCO-DT-ULT-BOLL-PERF-I-DB
            //                     :IND-PCO-DT-ULT-BOLL-PERF-I
            //                    ,:PCO-DT-RICL-RIRIAS-COM-DB
            //                     :IND-PCO-DT-RICL-RIRIAS-COM
            //                    ,:PCO-DT-ULT-ELAB-AT92-C-DB
            //                     :IND-PCO-DT-ULT-ELAB-AT92-C
            //                    ,:PCO-DT-ULT-ELAB-AT92-I-DB
            //                     :IND-PCO-DT-ULT-ELAB-AT92-I
            //                    ,:PCO-DT-ULT-ELAB-AT93-C-DB
            //                     :IND-PCO-DT-ULT-ELAB-AT93-C
            //                    ,:PCO-DT-ULT-ELAB-AT93-I-DB
            //                     :IND-PCO-DT-ULT-ELAB-AT93-I
            //                    ,:PCO-DT-ULT-ELAB-SPE-IN-DB
            //                     :IND-PCO-DT-ULT-ELAB-SPE-IN
            //                    ,:PCO-DT-ULT-ELAB-PR-CON-DB
            //                     :IND-PCO-DT-ULT-ELAB-PR-CON
            //                    ,:PCO-DT-ULT-BOLL-RP-CL-DB
            //                     :IND-PCO-DT-ULT-BOLL-RP-CL
            //                    ,:PCO-DT-ULT-BOLL-RP-IN-DB
            //                     :IND-PCO-DT-ULT-BOLL-RP-IN
            //                    ,:PCO-DT-ULT-BOLL-PRE-I-DB
            //                     :IND-PCO-DT-ULT-BOLL-PRE-I
            //                    ,:PCO-DT-ULT-BOLL-PRE-C-DB
            //                     :IND-PCO-DT-ULT-BOLL-PRE-C
            //                    ,:PCO-DT-ULTC-PILDI-MM-C-DB
            //                     :IND-PCO-DT-ULTC-PILDI-MM-C
            //                    ,:PCO-DT-ULTC-PILDI-AA-C-DB
            //                     :IND-PCO-DT-ULTC-PILDI-AA-C
            //                    ,:PCO-DT-ULTC-PILDI-MM-I-DB
            //                     :IND-PCO-DT-ULTC-PILDI-MM-I
            //                    ,:PCO-DT-ULTC-PILDI-TR-I-DB
            //                     :IND-PCO-DT-ULTC-PILDI-TR-I
            //                    ,:PCO-DT-ULTC-PILDI-AA-I-DB
            //                     :IND-PCO-DT-ULTC-PILDI-AA-I
            //                    ,:PCO-DT-ULT-BOLL-QUIE-C-DB
            //                     :IND-PCO-DT-ULT-BOLL-QUIE-C
            //                    ,:PCO-DT-ULT-BOLL-QUIE-I-DB
            //                     :IND-PCO-DT-ULT-BOLL-QUIE-I
            //                    ,:PCO-DT-ULT-BOLL-COTR-I-DB
            //                     :IND-PCO-DT-ULT-BOLL-COTR-I
            //                    ,:PCO-DT-ULT-BOLL-COTR-C-DB
            //                     :IND-PCO-DT-ULT-BOLL-COTR-C
            //                    ,:PCO-DT-ULT-BOLL-CORI-C-DB
            //                     :IND-PCO-DT-ULT-BOLL-CORI-C
            //                    ,:PCO-DT-ULT-BOLL-CORI-I-DB
            //                     :IND-PCO-DT-ULT-BOLL-CORI-I
            //                    ,:PCO-DS-OPER-SQL
            //                    ,:PCO-DS-VER
            //                    ,:PCO-DS-TS-CPTZ
            //                    ,:PCO-DS-UTENTE
            //                    ,:PCO-DS-STATO-ELAB
            //                    ,:PCO-TP-VALZZ-DT-VLT
            //                     :IND-PCO-TP-VALZZ-DT-VLT
            //                    ,:PCO-FL-FRAZ-PROV-ACQ
            //                     :IND-PCO-FL-FRAZ-PROV-ACQ
            //                    ,:PCO-DT-ULT-AGG-EROG-RE-DB
            //                     :IND-PCO-DT-ULT-AGG-EROG-RE
            //                    ,:PCO-PC-RM-MARSOL
            //                     :IND-PCO-PC-RM-MARSOL
            //                    ,:PCO-PC-C-SUBRSH-MARSOL
            //                     :IND-PCO-PC-C-SUBRSH-MARSOL
            //                    ,:PCO-COD-COMP-ISVAP
            //                     :IND-PCO-COD-COMP-ISVAP
            //                    ,:PCO-LM-RIS-CON-INT
            //                     :IND-PCO-LM-RIS-CON-INT
            //                    ,:PCO-LM-C-SUBRSH-CON-IN
            //                     :IND-PCO-LM-C-SUBRSH-CON-IN
            //                    ,:PCO-PC-GAR-NORISK-MARS
            //                     :IND-PCO-PC-GAR-NORISK-MARS
            //                    ,:PCO-CRZ-1A-RAT-INTR-PR
            //                     :IND-PCO-CRZ-1A-RAT-INTR-PR
            //                    ,:PCO-NUM-GG-ARR-INTR-PR
            //                     :IND-PCO-NUM-GG-ARR-INTR-PR
            //                    ,:PCO-FL-VISUAL-VINPG
            //                     :IND-PCO-FL-VISUAL-VINPG
            //                    ,:PCO-DT-ULT-ESTRAZ-FUG-DB
            //                     :IND-PCO-DT-ULT-ESTRAZ-FUG
            //                    ,:PCO-DT-ULT-ELAB-PR-AUT-DB
            //                     :IND-PCO-DT-ULT-ELAB-PR-AUT
            //                    ,:PCO-DT-ULT-ELAB-COMMEF-DB
            //                     :IND-PCO-DT-ULT-ELAB-COMMEF
            //                    ,:PCO-DT-ULT-ELAB-LIQMEF-DB
            //                     :IND-PCO-DT-ULT-ELAB-LIQMEF
            //                    ,:PCO-COD-FISC-MEF
            //                     :IND-PCO-COD-FISC-MEF
            //                    ,:PCO-IMP-ASS-SOCIALE
            //                     :IND-PCO-IMP-ASS-SOCIALE
            //                    ,:PCO-MOD-COMNZ-INVST-SW
            //                     :IND-PCO-MOD-COMNZ-INVST-SW
            //                    ,:PCO-DT-RIAT-RIASS-RSH-DB
            //                     :IND-PCO-DT-RIAT-RIASS-RSH
            //                    ,:PCO-DT-RIAT-RIASS-COMM-DB
            //                     :IND-PCO-DT-RIAT-RIASS-COMM
            //                    ,:PCO-GG-INTR-RIT-PAG
            //                     :IND-PCO-GG-INTR-RIT-PAG
            //                    ,:PCO-DT-ULT-RINN-TAC-DB
            //                     :IND-PCO-DT-ULT-RINN-TAC
            //                    ,:PCO-DESC-COMP-VCHAR
            //                     :IND-PCO-DESC-COMP
            //                    ,:PCO-DT-ULT-EC-TCM-IND-DB
            //                     :IND-PCO-DT-ULT-EC-TCM-IND
            //                    ,:PCO-DT-ULT-EC-TCM-COLL-DB
            //                     :IND-PCO-DT-ULT-EC-TCM-COLL
            //                    ,:PCO-DT-ULT-EC-MRM-IND-DB
            //                     :IND-PCO-DT-ULT-EC-MRM-IND
            //                    ,:PCO-DT-ULT-EC-MRM-COLL-DB
            //                     :IND-PCO-DT-ULT-EC-MRM-COLL
            //                    ,:PCO-COD-COMP-LDAP
            //                     :IND-PCO-COD-COMP-LDAP
            //                    ,:PCO-PC-RID-IMP-1382011
            //                     :IND-PCO-PC-RID-IMP-1382011
            //                    ,:PCO-PC-RID-IMP-662014
            //                     :IND-PCO-PC-RID-IMP-662014
            //                    ,:PCO-SOGL-AML-PRE-UNI
            //                     :IND-PCO-SOGL-AML-PRE-UNI
            //                    ,:PCO-SOGL-AML-PRE-PER
            //                     :IND-PCO-SOGL-AML-PRE-PER
            //                    ,:PCO-COD-SOGG-FTZ-ASSTO
            //                     :IND-PCO-COD-SOGG-FTZ-ASSTO
            //                    ,:PCO-DT-ULT-ELAB-REDPRO-DB
            //                     :IND-PCO-DT-ULT-ELAB-REDPRO
            //                    ,:PCO-DT-ULT-ELAB-TAKE-P-DB
            //                     :IND-PCO-DT-ULT-ELAB-TAKE-P
            //                    ,:PCO-DT-ULT-ELAB-PASPAS-DB
            //                     :IND-PCO-DT-ULT-ELAB-PASPAS
            //                    ,:PCO-SOGL-AML-PRE-SAV-R
            //                     :IND-PCO-SOGL-AML-PRE-SAV-R
            //                    ,:PCO-DT-ULT-ESTR-DEC-CO-DB
            //                     :IND-PCO-DT-ULT-ESTR-DEC-CO
            //                    ,:PCO-DT-ULT-ELAB-COS-AT-DB
            //                     :IND-PCO-DT-ULT-ELAB-COS-AT
            //                    ,:PCO-FRQ-COSTI-ATT
            //                     :IND-PCO-FRQ-COSTI-ATT
            //                    ,:PCO-DT-ULT-ELAB-COS-ST-DB
            //                     :IND-PCO-DT-ULT-ELAB-COS-ST
            //                    ,:PCO-FRQ-COSTI-STORNATI
            //                     :IND-PCO-FRQ-COSTI-STORNATI
            //                    ,:PCO-DT-ESTR-ASS-MIN70A-DB
            //                     :IND-PCO-DT-ESTR-ASS-MIN70A
            //                    ,:PCO-DT-ESTR-ASS-MAG70A-DB
            //                     :IND-PCO-DT-ESTR-ASS-MAG70A
            //                  )
            //           END-EXEC
            paramCompDao.insertRec(this);
            // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
            a100CheckReturnCode();
        }
    }

    /**Original name: A230-UPDATE-PK<br>*/
    private void a230UpdatePk() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE PARAM_COMP SET
        //                   COD_COMP_ANIA          =
        //                :PCO-COD-COMP-ANIA
        //                  ,COD_TRAT_CIRT          =
        //                :PCO-COD-TRAT-CIRT
        //                                       :IND-PCO-COD-TRAT-CIRT
        //                  ,LIM_VLTR               =
        //                :PCO-LIM-VLTR
        //                                       :IND-PCO-LIM-VLTR
        //                  ,TP_RAT_PERF            =
        //                :PCO-TP-RAT-PERF
        //                                       :IND-PCO-TP-RAT-PERF
        //                  ,TP_LIV_GENZ_TIT        =
        //                :PCO-TP-LIV-GENZ-TIT
        //                  ,ARROT_PRE              =
        //                :PCO-ARROT-PRE
        //                                       :IND-PCO-ARROT-PRE
        //                  ,DT_CONT                =
        //           :PCO-DT-CONT-DB
        //                                       :IND-PCO-DT-CONT
        //                  ,DT_ULT_RIVAL_IN        =
        //           :PCO-DT-ULT-RIVAL-IN-DB
        //                                       :IND-PCO-DT-ULT-RIVAL-IN
        //                  ,DT_ULT_QTZO_IN         =
        //           :PCO-DT-ULT-QTZO-IN-DB
        //                                       :IND-PCO-DT-ULT-QTZO-IN
        //                  ,DT_ULT_RICL_RIASS      =
        //           :PCO-DT-ULT-RICL-RIASS-DB
        //                                       :IND-PCO-DT-ULT-RICL-RIASS
        //                  ,DT_ULT_TABUL_RIASS     =
        //           :PCO-DT-ULT-TABUL-RIASS-DB
        //                                       :IND-PCO-DT-ULT-TABUL-RIASS
        //                  ,DT_ULT_BOLL_EMES       =
        //           :PCO-DT-ULT-BOLL-EMES-DB
        //                                       :IND-PCO-DT-ULT-BOLL-EMES
        //                  ,DT_ULT_BOLL_STOR       =
        //           :PCO-DT-ULT-BOLL-STOR-DB
        //                                       :IND-PCO-DT-ULT-BOLL-STOR
        //                  ,DT_ULT_BOLL_LIQ        =
        //           :PCO-DT-ULT-BOLL-LIQ-DB
        //                                       :IND-PCO-DT-ULT-BOLL-LIQ
        //                  ,DT_ULT_BOLL_RIAT       =
        //           :PCO-DT-ULT-BOLL-RIAT-DB
        //                                       :IND-PCO-DT-ULT-BOLL-RIAT
        //                  ,DT_ULTELRISCPAR_PR     =
        //           :PCO-DT-ULTELRISCPAR-PR-DB
        //                                       :IND-PCO-DT-ULTELRISCPAR-PR
        //                  ,DT_ULTC_IS_IN          =
        //           :PCO-DT-ULTC-IS-IN-DB
        //                                       :IND-PCO-DT-ULTC-IS-IN
        //                  ,DT_ULT_RICL_PRE        =
        //           :PCO-DT-ULT-RICL-PRE-DB
        //                                       :IND-PCO-DT-ULT-RICL-PRE
        //                  ,DT_ULTC_MARSOL         =
        //           :PCO-DT-ULTC-MARSOL-DB
        //                                       :IND-PCO-DT-ULTC-MARSOL
        //                  ,DT_ULTC_RB_IN          =
        //           :PCO-DT-ULTC-RB-IN-DB
        //                                       :IND-PCO-DT-ULTC-RB-IN
        //                  ,PC_PROV_1AA_ACQ        =
        //                :PCO-PC-PROV-1AA-ACQ
        //                                       :IND-PCO-PC-PROV-1AA-ACQ
        //                  ,MOD_INTR_PREST         =
        //                :PCO-MOD-INTR-PREST
        //                                       :IND-PCO-MOD-INTR-PREST
        //                  ,GG_MAX_REC_PROV        =
        //                :PCO-GG-MAX-REC-PROV
        //                                       :IND-PCO-GG-MAX-REC-PROV
        //                  ,DT_ULTGZ_TRCH_E_IN     =
        //           :PCO-DT-ULTGZ-TRCH-E-IN-DB
        //                                       :IND-PCO-DT-ULTGZ-TRCH-E-IN
        //                  ,DT_ULT_BOLL_SNDEN      =
        //           :PCO-DT-ULT-BOLL-SNDEN-DB
        //                                       :IND-PCO-DT-ULT-BOLL-SNDEN
        //                  ,DT_ULT_BOLL_SNDNLQ     =
        //           :PCO-DT-ULT-BOLL-SNDNLQ-DB
        //                                       :IND-PCO-DT-ULT-BOLL-SNDNLQ
        //                  ,DT_ULTSC_ELAB_IN       =
        //           :PCO-DT-ULTSC-ELAB-IN-DB
        //                                       :IND-PCO-DT-ULTSC-ELAB-IN
        //                  ,DT_ULTSC_OPZ_IN        =
        //           :PCO-DT-ULTSC-OPZ-IN-DB
        //                                       :IND-PCO-DT-ULTSC-OPZ-IN
        //                  ,DT_ULTC_BNSRIC_IN      =
        //           :PCO-DT-ULTC-BNSRIC-IN-DB
        //                                       :IND-PCO-DT-ULTC-BNSRIC-IN
        //                  ,DT_ULTC_BNSFDT_IN      =
        //           :PCO-DT-ULTC-BNSFDT-IN-DB
        //                                       :IND-PCO-DT-ULTC-BNSFDT-IN
        //                  ,DT_ULT_RINN_GARAC      =
        //           :PCO-DT-ULT-RINN-GARAC-DB
        //                                       :IND-PCO-DT-ULT-RINN-GARAC
        //                  ,DT_ULTGZ_CED           =
        //           :PCO-DT-ULTGZ-CED-DB
        //                                       :IND-PCO-DT-ULTGZ-CED
        //                  ,DT_ULT_ELAB_PRLCOS     =
        //           :PCO-DT-ULT-ELAB-PRLCOS-DB
        //                                       :IND-PCO-DT-ULT-ELAB-PRLCOS
        //                  ,DT_ULT_RINN_COLL       =
        //           :PCO-DT-ULT-RINN-COLL-DB
        //                                       :IND-PCO-DT-ULT-RINN-COLL
        //                  ,FL_RVC_PERF            =
        //                :PCO-FL-RVC-PERF
        //                                       :IND-PCO-FL-RVC-PERF
        //                  ,FL_RCS_POLI_NOPERF     =
        //                :PCO-FL-RCS-POLI-NOPERF
        //                                       :IND-PCO-FL-RCS-POLI-NOPERF
        //                  ,FL_GEST_PLUSV          =
        //                :PCO-FL-GEST-PLUSV
        //                                       :IND-PCO-FL-GEST-PLUSV
        //                  ,DT_ULT_RIVAL_CL        =
        //           :PCO-DT-ULT-RIVAL-CL-DB
        //                                       :IND-PCO-DT-ULT-RIVAL-CL
        //                  ,DT_ULT_QTZO_CL         =
        //           :PCO-DT-ULT-QTZO-CL-DB
        //                                       :IND-PCO-DT-ULT-QTZO-CL
        //                  ,DT_ULTC_BNSRIC_CL      =
        //           :PCO-DT-ULTC-BNSRIC-CL-DB
        //                                       :IND-PCO-DT-ULTC-BNSRIC-CL
        //                  ,DT_ULTC_BNSFDT_CL      =
        //           :PCO-DT-ULTC-BNSFDT-CL-DB
        //                                       :IND-PCO-DT-ULTC-BNSFDT-CL
        //                  ,DT_ULTC_IS_CL          =
        //           :PCO-DT-ULTC-IS-CL-DB
        //                                       :IND-PCO-DT-ULTC-IS-CL
        //                  ,DT_ULTC_RB_CL          =
        //           :PCO-DT-ULTC-RB-CL-DB
        //                                       :IND-PCO-DT-ULTC-RB-CL
        //                  ,DT_ULTGZ_TRCH_E_CL     =
        //           :PCO-DT-ULTGZ-TRCH-E-CL-DB
        //                                       :IND-PCO-DT-ULTGZ-TRCH-E-CL
        //                  ,DT_ULTSC_ELAB_CL       =
        //           :PCO-DT-ULTSC-ELAB-CL-DB
        //                                       :IND-PCO-DT-ULTSC-ELAB-CL
        //                  ,DT_ULTSC_OPZ_CL        =
        //           :PCO-DT-ULTSC-OPZ-CL-DB
        //                                       :IND-PCO-DT-ULTSC-OPZ-CL
        //                  ,STST_X_REGIONE         =
        //           :PCO-STST-X-REGIONE-DB
        //                                       :IND-PCO-STST-X-REGIONE
        //                  ,DT_ULTGZ_CED_COLL      =
        //           :PCO-DT-ULTGZ-CED-COLL-DB
        //                                       :IND-PCO-DT-ULTGZ-CED-COLL
        //                  ,TP_MOD_RIVAL           =
        //                :PCO-TP-MOD-RIVAL
        //                  ,NUM_MM_CALC_MORA       =
        //                :PCO-NUM-MM-CALC-MORA
        //                                       :IND-PCO-NUM-MM-CALC-MORA
        //                  ,DT_ULT_EC_RIV_COLL     =
        //           :PCO-DT-ULT-EC-RIV-COLL-DB
        //                                       :IND-PCO-DT-ULT-EC-RIV-COLL
        //                  ,DT_ULT_EC_RIV_IND      =
        //           :PCO-DT-ULT-EC-RIV-IND-DB
        //                                       :IND-PCO-DT-ULT-EC-RIV-IND
        //                  ,DT_ULT_EC_IL_COLL      =
        //           :PCO-DT-ULT-EC-IL-COLL-DB
        //                                       :IND-PCO-DT-ULT-EC-IL-COLL
        //                  ,DT_ULT_EC_IL_IND       =
        //           :PCO-DT-ULT-EC-IL-IND-DB
        //                                       :IND-PCO-DT-ULT-EC-IL-IND
        //                  ,DT_ULT_EC_UL_COLL      =
        //           :PCO-DT-ULT-EC-UL-COLL-DB
        //                                       :IND-PCO-DT-ULT-EC-UL-COLL
        //                  ,DT_ULT_EC_UL_IND       =
        //           :PCO-DT-ULT-EC-UL-IND-DB
        //                                       :IND-PCO-DT-ULT-EC-UL-IND
        //                  ,AA_UTI                 =
        //                :PCO-AA-UTI
        //                                       :IND-PCO-AA-UTI
        //                  ,CALC_RSH_COMUN         =
        //                :PCO-CALC-RSH-COMUN
        //                  ,FL_LIV_DEBUG           =
        //                :PCO-FL-LIV-DEBUG
        //                  ,DT_ULT_BOLL_PERF_C     =
        //           :PCO-DT-ULT-BOLL-PERF-C-DB
        //                                       :IND-PCO-DT-ULT-BOLL-PERF-C
        //                  ,DT_ULT_BOLL_RSP_IN     =
        //           :PCO-DT-ULT-BOLL-RSP-IN-DB
        //                                       :IND-PCO-DT-ULT-BOLL-RSP-IN
        //                  ,DT_ULT_BOLL_RSP_CL     =
        //           :PCO-DT-ULT-BOLL-RSP-CL-DB
        //                                       :IND-PCO-DT-ULT-BOLL-RSP-CL
        //                  ,DT_ULT_BOLL_EMES_I     =
        //           :PCO-DT-ULT-BOLL-EMES-I-DB
        //                                       :IND-PCO-DT-ULT-BOLL-EMES-I
        //                  ,DT_ULT_BOLL_STOR_I     =
        //           :PCO-DT-ULT-BOLL-STOR-I-DB
        //                                       :IND-PCO-DT-ULT-BOLL-STOR-I
        //                  ,DT_ULT_BOLL_RIAT_I     =
        //           :PCO-DT-ULT-BOLL-RIAT-I-DB
        //                                       :IND-PCO-DT-ULT-BOLL-RIAT-I
        //                  ,DT_ULT_BOLL_SD_I       =
        //           :PCO-DT-ULT-BOLL-SD-I-DB
        //                                       :IND-PCO-DT-ULT-BOLL-SD-I
        //                  ,DT_ULT_BOLL_SDNL_I     =
        //           :PCO-DT-ULT-BOLL-SDNL-I-DB
        //                                       :IND-PCO-DT-ULT-BOLL-SDNL-I
        //                  ,DT_ULT_BOLL_PERF_I     =
        //           :PCO-DT-ULT-BOLL-PERF-I-DB
        //                                       :IND-PCO-DT-ULT-BOLL-PERF-I
        //                  ,DT_RICL_RIRIAS_COM     =
        //           :PCO-DT-RICL-RIRIAS-COM-DB
        //                                       :IND-PCO-DT-RICL-RIRIAS-COM
        //                  ,DT_ULT_ELAB_AT92_C     =
        //           :PCO-DT-ULT-ELAB-AT92-C-DB
        //                                       :IND-PCO-DT-ULT-ELAB-AT92-C
        //                  ,DT_ULT_ELAB_AT92_I     =
        //           :PCO-DT-ULT-ELAB-AT92-I-DB
        //                                       :IND-PCO-DT-ULT-ELAB-AT92-I
        //                  ,DT_ULT_ELAB_AT93_C     =
        //           :PCO-DT-ULT-ELAB-AT93-C-DB
        //                                       :IND-PCO-DT-ULT-ELAB-AT93-C
        //                  ,DT_ULT_ELAB_AT93_I     =
        //           :PCO-DT-ULT-ELAB-AT93-I-DB
        //                                       :IND-PCO-DT-ULT-ELAB-AT93-I
        //                  ,DT_ULT_ELAB_SPE_IN     =
        //           :PCO-DT-ULT-ELAB-SPE-IN-DB
        //                                       :IND-PCO-DT-ULT-ELAB-SPE-IN
        //                  ,DT_ULT_ELAB_PR_CON     =
        //           :PCO-DT-ULT-ELAB-PR-CON-DB
        //                                       :IND-PCO-DT-ULT-ELAB-PR-CON
        //                  ,DT_ULT_BOLL_RP_CL      =
        //           :PCO-DT-ULT-BOLL-RP-CL-DB
        //                                       :IND-PCO-DT-ULT-BOLL-RP-CL
        //                  ,DT_ULT_BOLL_RP_IN      =
        //           :PCO-DT-ULT-BOLL-RP-IN-DB
        //                                       :IND-PCO-DT-ULT-BOLL-RP-IN
        //                  ,DT_ULT_BOLL_PRE_I      =
        //           :PCO-DT-ULT-BOLL-PRE-I-DB
        //                                       :IND-PCO-DT-ULT-BOLL-PRE-I
        //                  ,DT_ULT_BOLL_PRE_C      =
        //           :PCO-DT-ULT-BOLL-PRE-C-DB
        //                                       :IND-PCO-DT-ULT-BOLL-PRE-C
        //                  ,DT_ULTC_PILDI_MM_C     =
        //           :PCO-DT-ULTC-PILDI-MM-C-DB
        //                                       :IND-PCO-DT-ULTC-PILDI-MM-C
        //                  ,DT_ULTC_PILDI_AA_C     =
        //           :PCO-DT-ULTC-PILDI-AA-C-DB
        //                                       :IND-PCO-DT-ULTC-PILDI-AA-C
        //                  ,DT_ULTC_PILDI_MM_I     =
        //           :PCO-DT-ULTC-PILDI-MM-I-DB
        //                                       :IND-PCO-DT-ULTC-PILDI-MM-I
        //                  ,DT_ULTC_PILDI_TR_I     =
        //           :PCO-DT-ULTC-PILDI-TR-I-DB
        //                                       :IND-PCO-DT-ULTC-PILDI-TR-I
        //                  ,DT_ULTC_PILDI_AA_I     =
        //           :PCO-DT-ULTC-PILDI-AA-I-DB
        //                                       :IND-PCO-DT-ULTC-PILDI-AA-I
        //                  ,DT_ULT_BOLL_QUIE_C     =
        //           :PCO-DT-ULT-BOLL-QUIE-C-DB
        //                                       :IND-PCO-DT-ULT-BOLL-QUIE-C
        //                  ,DT_ULT_BOLL_QUIE_I     =
        //           :PCO-DT-ULT-BOLL-QUIE-I-DB
        //                                       :IND-PCO-DT-ULT-BOLL-QUIE-I
        //                  ,DT_ULT_BOLL_COTR_I     =
        //           :PCO-DT-ULT-BOLL-COTR-I-DB
        //                                       :IND-PCO-DT-ULT-BOLL-COTR-I
        //                  ,DT_ULT_BOLL_COTR_C     =
        //           :PCO-DT-ULT-BOLL-COTR-C-DB
        //                                       :IND-PCO-DT-ULT-BOLL-COTR-C
        //                  ,DT_ULT_BOLL_CORI_C     =
        //           :PCO-DT-ULT-BOLL-CORI-C-DB
        //                                       :IND-PCO-DT-ULT-BOLL-CORI-C
        //                  ,DT_ULT_BOLL_CORI_I     =
        //           :PCO-DT-ULT-BOLL-CORI-I-DB
        //                                       :IND-PCO-DT-ULT-BOLL-CORI-I
        //                  ,DS_OPER_SQL            =
        //                :PCO-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :PCO-DS-VER
        //                  ,DS_TS_CPTZ             =
        //                :PCO-DS-TS-CPTZ
        //                  ,DS_UTENTE              =
        //                :PCO-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :PCO-DS-STATO-ELAB
        //                  ,TP_VALZZ_DT_VLT        =
        //                :PCO-TP-VALZZ-DT-VLT
        //                                       :IND-PCO-TP-VALZZ-DT-VLT
        //                  ,FL_FRAZ_PROV_ACQ       =
        //                :PCO-FL-FRAZ-PROV-ACQ
        //                                       :IND-PCO-FL-FRAZ-PROV-ACQ
        //                  ,DT_ULT_AGG_EROG_RE     =
        //           :PCO-DT-ULT-AGG-EROG-RE-DB
        //                                       :IND-PCO-DT-ULT-AGG-EROG-RE
        //                  ,PC_RM_MARSOL           =
        //                :PCO-PC-RM-MARSOL
        //                                       :IND-PCO-PC-RM-MARSOL
        //                  ,PC_C_SUBRSH_MARSOL     =
        //                :PCO-PC-C-SUBRSH-MARSOL
        //                                       :IND-PCO-PC-C-SUBRSH-MARSOL
        //                  ,COD_COMP_ISVAP         =
        //                :PCO-COD-COMP-ISVAP
        //                                       :IND-PCO-COD-COMP-ISVAP
        //                  ,LM_RIS_CON_INT         =
        //                :PCO-LM-RIS-CON-INT
        //                                       :IND-PCO-LM-RIS-CON-INT
        //                  ,LM_C_SUBRSH_CON_IN     =
        //                :PCO-LM-C-SUBRSH-CON-IN
        //                                       :IND-PCO-LM-C-SUBRSH-CON-IN
        //                  ,PC_GAR_NORISK_MARS     =
        //                :PCO-PC-GAR-NORISK-MARS
        //                                       :IND-PCO-PC-GAR-NORISK-MARS
        //                  ,CRZ_1A_RAT_INTR_PR     =
        //                :PCO-CRZ-1A-RAT-INTR-PR
        //                                       :IND-PCO-CRZ-1A-RAT-INTR-PR
        //                  ,NUM_GG_ARR_INTR_PR     =
        //                :PCO-NUM-GG-ARR-INTR-PR
        //                                       :IND-PCO-NUM-GG-ARR-INTR-PR
        //                  ,FL_VISUAL_VINPG        =
        //                :PCO-FL-VISUAL-VINPG
        //                                       :IND-PCO-FL-VISUAL-VINPG
        //                  ,DT_ULT_ESTRAZ_FUG      =
        //           :PCO-DT-ULT-ESTRAZ-FUG-DB
        //                                       :IND-PCO-DT-ULT-ESTRAZ-FUG
        //                  ,DT_ULT_ELAB_PR_AUT     =
        //           :PCO-DT-ULT-ELAB-PR-AUT-DB
        //                                       :IND-PCO-DT-ULT-ELAB-PR-AUT
        //                  ,DT_ULT_ELAB_COMMEF     =
        //           :PCO-DT-ULT-ELAB-COMMEF-DB
        //                                       :IND-PCO-DT-ULT-ELAB-COMMEF
        //                  ,DT_ULT_ELAB_LIQMEF     =
        //           :PCO-DT-ULT-ELAB-LIQMEF-DB
        //                                       :IND-PCO-DT-ULT-ELAB-LIQMEF
        //                  ,COD_FISC_MEF           =
        //                :PCO-COD-FISC-MEF
        //                                       :IND-PCO-COD-FISC-MEF
        //                  ,IMP_ASS_SOCIALE        =
        //                :PCO-IMP-ASS-SOCIALE
        //                                       :IND-PCO-IMP-ASS-SOCIALE
        //                  ,MOD_COMNZ_INVST_SW     =
        //                :PCO-MOD-COMNZ-INVST-SW
        //                                       :IND-PCO-MOD-COMNZ-INVST-SW
        //                  ,DT_RIAT_RIASS_RSH      =
        //           :PCO-DT-RIAT-RIASS-RSH-DB
        //                                       :IND-PCO-DT-RIAT-RIASS-RSH
        //                  ,DT_RIAT_RIASS_COMM     =
        //           :PCO-DT-RIAT-RIASS-COMM-DB
        //                                       :IND-PCO-DT-RIAT-RIASS-COMM
        //                  ,GG_INTR_RIT_PAG        =
        //                :PCO-GG-INTR-RIT-PAG
        //                                       :IND-PCO-GG-INTR-RIT-PAG
        //                  ,DT_ULT_RINN_TAC        =
        //           :PCO-DT-ULT-RINN-TAC-DB
        //                                       :IND-PCO-DT-ULT-RINN-TAC
        //                  ,DESC_COMP              =
        //                :PCO-DESC-COMP-VCHAR
        //                                       :IND-PCO-DESC-COMP
        //                  ,DT_ULT_EC_TCM_IND      =
        //           :PCO-DT-ULT-EC-TCM-IND-DB
        //                                       :IND-PCO-DT-ULT-EC-TCM-IND
        //                  ,DT_ULT_EC_TCM_COLL     =
        //           :PCO-DT-ULT-EC-TCM-COLL-DB
        //                                       :IND-PCO-DT-ULT-EC-TCM-COLL
        //                  ,DT_ULT_EC_MRM_IND      =
        //           :PCO-DT-ULT-EC-MRM-IND-DB
        //                                       :IND-PCO-DT-ULT-EC-MRM-IND
        //                  ,DT_ULT_EC_MRM_COLL     =
        //           :PCO-DT-ULT-EC-MRM-COLL-DB
        //                                       :IND-PCO-DT-ULT-EC-MRM-COLL
        //                  ,COD_COMP_LDAP          =
        //                :PCO-COD-COMP-LDAP
        //                                       :IND-PCO-COD-COMP-LDAP
        //                  ,PC_RID_IMP_1382011     =
        //                :PCO-PC-RID-IMP-1382011
        //                                       :IND-PCO-PC-RID-IMP-1382011
        //                  ,PC_RID_IMP_662014      =
        //                :PCO-PC-RID-IMP-662014
        //                                       :IND-PCO-PC-RID-IMP-662014
        //                  ,SOGL_AML_PRE_UNI       =
        //                :PCO-SOGL-AML-PRE-UNI
        //                                       :IND-PCO-SOGL-AML-PRE-UNI
        //                  ,SOGL_AML_PRE_PER       =
        //                :PCO-SOGL-AML-PRE-PER
        //                                       :IND-PCO-SOGL-AML-PRE-PER
        //                  ,COD_SOGG_FTZ_ASSTO     =
        //                :PCO-COD-SOGG-FTZ-ASSTO
        //                                       :IND-PCO-COD-SOGG-FTZ-ASSTO
        //                  ,DT_ULT_ELAB_REDPRO     =
        //           :PCO-DT-ULT-ELAB-REDPRO-DB
        //                                       :IND-PCO-DT-ULT-ELAB-REDPRO
        //                  ,DT_ULT_ELAB_TAKE_P     =
        //           :PCO-DT-ULT-ELAB-TAKE-P-DB
        //                                       :IND-PCO-DT-ULT-ELAB-TAKE-P
        //                  ,DT_ULT_ELAB_PASPAS     =
        //           :PCO-DT-ULT-ELAB-PASPAS-DB
        //                                       :IND-PCO-DT-ULT-ELAB-PASPAS
        //                  ,SOGL_AML_PRE_SAV_R     =
        //                :PCO-SOGL-AML-PRE-SAV-R
        //                                       :IND-PCO-SOGL-AML-PRE-SAV-R
        //                  ,DT_ULT_ESTR_DEC_CO     =
        //           :PCO-DT-ULT-ESTR-DEC-CO-DB
        //                                       :IND-PCO-DT-ULT-ESTR-DEC-CO
        //                  ,DT_ULT_ELAB_COS_AT     =
        //           :PCO-DT-ULT-ELAB-COS-AT-DB
        //                                       :IND-PCO-DT-ULT-ELAB-COS-AT
        //                  ,FRQ_COSTI_ATT          =
        //                :PCO-FRQ-COSTI-ATT
        //                                       :IND-PCO-FRQ-COSTI-ATT
        //                  ,DT_ULT_ELAB_COS_ST     =
        //           :PCO-DT-ULT-ELAB-COS-ST-DB
        //                                       :IND-PCO-DT-ULT-ELAB-COS-ST
        //                  ,FRQ_COSTI_STORNATI     =
        //                :PCO-FRQ-COSTI-STORNATI
        //                                       :IND-PCO-FRQ-COSTI-STORNATI
        //                  ,DT_ESTR_ASS_MIN70A     =
        //           :PCO-DT-ESTR-ASS-MIN70A-DB
        //                                       :IND-PCO-DT-ESTR-ASS-MIN70A
        //                  ,DT_ESTR_ASS_MAG70A     =
        //           :PCO-DT-ESTR-ASS-MAG70A-DB
        //                                       :IND-PCO-DT-ESTR-ASS-MAG70A
        //                WHERE     COD_COMP_ANIA = :PCO-COD-COMP-ANIA
        //           END-EXEC.
        paramCompDao.updateRec(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A240-DELETE-PK<br>*/
    private void a240DeletePk() {
        // COB_CODE: EXEC SQL
        //                DELETE
        //                FROM PARAM_COMP
        //                WHERE     COD_COMP_ANIA = :PCO-COD-COMP-ANIA
        //           END-EXEC.
        paramCompDao.deleteByPcoCodCompAnia(paramComp.getPcoCodCompAnia());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A310-SELECT-ID-EFF<br>*/
    private void a310SelectIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A405-DECLARE-CURSOR-IDP-EFF<br>
	 * <pre>----
	 * ----  gestione IDP Effetto
	 * ----</pre>*/
    private void a405DeclareCursorIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A410-SELECT-IDP-EFF<br>*/
    private void a410SelectIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A460-OPEN-CURSOR-IDP-EFF<br>*/
    private void a460OpenCursorIdpEff() {
        // COB_CODE: PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.
        a405DeclareCursorIdpEff();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A470-CLOSE-CURSOR-IDP-EFF<br>*/
    private void a470CloseCursorIdpEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A480-FETCH-FIRST-IDP-EFF<br>*/
    private void a480FetchFirstIdpEff() {
        // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.
        a460OpenCursorIdpEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
            a490FetchNextIdpEff();
        }
    }

    /**Original name: A490-FETCH-NEXT-IDP-EFF<br>*/
    private void a490FetchNextIdpEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A505-DECLARE-CURSOR-IBO<br>
	 * <pre>----
	 * ----  gestione IBO Effetto e non
	 * ----</pre>*/
    private void a505DeclareCursorIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A510-SELECT-IBO<br>*/
    private void a510SelectIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A560-OPEN-CURSOR-IBO<br>*/
    private void a560OpenCursorIbo() {
        // COB_CODE: PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.
        a505DeclareCursorIbo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A570-CLOSE-CURSOR-IBO<br>*/
    private void a570CloseCursorIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A580-FETCH-FIRST-IBO<br>*/
    private void a580FetchFirstIbo() {
        // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.
        a560OpenCursorIbo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
            a590FetchNextIbo();
        }
    }

    /**Original name: A590-FETCH-NEXT-IBO<br>*/
    private void a590FetchNextIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A605-DECLARE-CURSOR-IBS<br>
	 * <pre>----
	 * ----  gestione IBS Effetto e non
	 * ----</pre>*/
    private void a605DeclareCursorIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A610-SELECT-IBS<br>*/
    private void a610SelectIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A660-OPEN-CURSOR-IBS<br>*/
    private void a660OpenCursorIbs() {
        // COB_CODE: PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.
        a605DeclareCursorIbs();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A670-CLOSE-CURSOR-IBS<br>*/
    private void a670CloseCursorIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A680-FETCH-FIRST-IBS<br>*/
    private void a680FetchFirstIbs() {
        // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.
        a660OpenCursorIbs();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
            a690FetchNextIbs();
        }
    }

    /**Original name: A690-FETCH-NEXT-IBS<br>*/
    private void a690FetchNextIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A705-DECLARE-CURSOR-IDO<br>
	 * <pre>----
	 * ----  gestione IDO Effetto e non
	 * ----</pre>*/
    private void a705DeclareCursorIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A710-SELECT-IDO<br>*/
    private void a710SelectIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A760-OPEN-CURSOR-IDO<br>*/
    private void a760OpenCursorIdo() {
        // COB_CODE: PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.
        a705DeclareCursorIdo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A770-CLOSE-CURSOR-IDO<br>*/
    private void a770CloseCursorIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A780-FETCH-FIRST-IDO<br>*/
    private void a780FetchFirstIdo() {
        // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.
        a760OpenCursorIdo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
            a790FetchNextIdo();
        }
    }

    /**Original name: A790-FETCH-NEXT-IDO<br>*/
    private void a790FetchNextIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B310-SELECT-ID-CPZ<br>
	 * <pre>----
	 * ----  gestione ID Competenza
	 * ----</pre>*/
    private void b310SelectIdCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B405-DECLARE-CURSOR-IDP-CPZ<br>
	 * <pre>----
	 * ----  gestione IDP Competenza
	 * ----</pre>*/
    private void b405DeclareCursorIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B410-SELECT-IDP-CPZ<br>*/
    private void b410SelectIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B460-OPEN-CURSOR-IDP-CPZ<br>*/
    private void b460OpenCursorIdpCpz() {
        // COB_CODE: PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.
        b405DeclareCursorIdpCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B470-CLOSE-CURSOR-IDP-CPZ<br>*/
    private void b470CloseCursorIdpCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B480-FETCH-FIRST-IDP-CPZ<br>*/
    private void b480FetchFirstIdpCpz() {
        // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.
        b460OpenCursorIdpCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
            b490FetchNextIdpCpz();
        }
    }

    /**Original name: B490-FETCH-NEXT-IDP-CPZ<br>*/
    private void b490FetchNextIdpCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B505-DECLARE-CURSOR-IBO-CPZ<br>
	 * <pre>----
	 * ----  gestione IBO Competenza
	 * ----</pre>*/
    private void b505DeclareCursorIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B510-SELECT-IBO-CPZ<br>*/
    private void b510SelectIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B560-OPEN-CURSOR-IBO-CPZ<br>*/
    private void b560OpenCursorIboCpz() {
        // COB_CODE: PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.
        b505DeclareCursorIboCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B570-CLOSE-CURSOR-IBO-CPZ<br>*/
    private void b570CloseCursorIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B580-FETCH-FIRST-IBO-CPZ<br>*/
    private void b580FetchFirstIboCpz() {
        // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.
        b560OpenCursorIboCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
    }

    /**Original name: B590-FETCH-NEXT-IBO-CPZ<br>*/
    private void b590FetchNextIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B605-DECLARE-CURSOR-IBS-CPZ<br>
	 * <pre>----
	 * ----  gestione IBS Competenza
	 * ----</pre>*/
    private void b605DeclareCursorIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B610-SELECT-IBS-CPZ<br>*/
    private void b610SelectIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B660-OPEN-CURSOR-IBS-CPZ<br>*/
    private void b660OpenCursorIbsCpz() {
        // COB_CODE: PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.
        b605DeclareCursorIbsCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B670-CLOSE-CURSOR-IBS-CPZ<br>*/
    private void b670CloseCursorIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B680-FETCH-FIRST-IBS-CPZ<br>*/
    private void b680FetchFirstIbsCpz() {
        // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.
        b660OpenCursorIbsCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
    }

    /**Original name: B690-FETCH-NEXT-IBS-CPZ<br>*/
    private void b690FetchNextIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B705-DECLARE-CURSOR-IDO-CPZ<br>
	 * <pre>----
	 * ----  gestione IDO Competenza
	 * ----</pre>*/
    private void b705DeclareCursorIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B710-SELECT-IDO-CPZ<br>*/
    private void b710SelectIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B760-OPEN-CURSOR-IDO-CPZ<br>*/
    private void b760OpenCursorIdoCpz() {
        // COB_CODE: PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.
        b705DeclareCursorIdoCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B770-CLOSE-CURSOR-IDO-CPZ<br>*/
    private void b770CloseCursorIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B780-FETCH-FIRST-IDO-CPZ<br>*/
    private void b780FetchFirstIdoCpz() {
        // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.
        b760OpenCursorIdoCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
    }

    /**Original name: B790-FETCH-NEXT-IDO-CPZ<br>*/
    private void b790FetchNextIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-PCO-COD-TRAT-CIRT = -1
        //              MOVE HIGH-VALUES TO PCO-COD-TRAT-CIRT-NULL
        //           END-IF
        if (ws.getIndParamComp().getCodTratCirt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-COD-TRAT-CIRT-NULL
            paramComp.setPcoCodTratCirt(LiteralGenerator.create(Types.HIGH_CHAR_VAL, ParamCompIdbspco0.Len.PCO_COD_TRAT_CIRT));
        }
        // COB_CODE: IF IND-PCO-LIM-VLTR = -1
        //              MOVE HIGH-VALUES TO PCO-LIM-VLTR-NULL
        //           END-IF
        if (ws.getIndParamComp().getLimVltr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-LIM-VLTR-NULL
            paramComp.getPcoLimVltr().setPcoLimVltrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoLimVltr.Len.PCO_LIM_VLTR_NULL));
        }
        // COB_CODE: IF IND-PCO-TP-RAT-PERF = -1
        //              MOVE HIGH-VALUES TO PCO-TP-RAT-PERF-NULL
        //           END-IF
        if (ws.getIndParamComp().getTpRatPerf() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-TP-RAT-PERF-NULL
            paramComp.setPcoTpRatPerf(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-PCO-ARROT-PRE = -1
        //              MOVE HIGH-VALUES TO PCO-ARROT-PRE-NULL
        //           END-IF
        if (ws.getIndParamComp().getArrotPre() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-ARROT-PRE-NULL
            paramComp.getPcoArrotPre().setPcoArrotPreNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoArrotPre.Len.PCO_ARROT_PRE_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-CONT = -1
        //              MOVE HIGH-VALUES TO PCO-DT-CONT-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtCont() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-CONT-NULL
            paramComp.getPcoDtCont().setPcoDtContNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtCont.Len.PCO_DT_CONT_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-RIVAL-IN = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-RIVAL-IN-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltRivalIn() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-RIVAL-IN-NULL
            paramComp.getPcoDtUltRivalIn().setPcoDtUltRivalInNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltRivalIn.Len.PCO_DT_ULT_RIVAL_IN_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-QTZO-IN = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-QTZO-IN-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltQtzoIn() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-QTZO-IN-NULL
            paramComp.getPcoDtUltQtzoIn().setPcoDtUltQtzoInNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltQtzoIn.Len.PCO_DT_ULT_QTZO_IN_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-RICL-RIASS = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-RICL-RIASS-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltRiclRiass() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-RICL-RIASS-NULL
            paramComp.getPcoDtUltRiclRiass().setPcoDtUltRiclRiassNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltRiclRiass.Len.PCO_DT_ULT_RICL_RIASS_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-TABUL-RIASS = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-TABUL-RIASS-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltTabulRiass() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-TABUL-RIASS-NULL
            paramComp.getPcoDtUltTabulRiass().setPcoDtUltTabulRiassNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltTabulRiass.Len.PCO_DT_ULT_TABUL_RIASS_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-EMES = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-EMES-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollEmes() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-EMES-NULL
            paramComp.getPcoDtUltBollEmes().setPcoDtUltBollEmesNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltBollEmes.Len.PCO_DT_ULT_BOLL_EMES_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-STOR = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-STOR-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollStor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-STOR-NULL
            paramComp.getPcoDtUltBollStor().setPcoDtUltBollStorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltBollStor.Len.PCO_DT_ULT_BOLL_STOR_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-LIQ = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-LIQ-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollLiq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-LIQ-NULL
            paramComp.getPcoDtUltBollLiq().setPcoDtUltBollLiqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltBollLiq.Len.PCO_DT_ULT_BOLL_LIQ_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-RIAT = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-RIAT-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollRiat() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-RIAT-NULL
            paramComp.getPcoDtUltBollRiat().setPcoDtUltBollRiatNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltBollRiat.Len.PCO_DT_ULT_BOLL_RIAT_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULTELRISCPAR-PR = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULTELRISCPAR-PR-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltelriscparPr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULTELRISCPAR-PR-NULL
            paramComp.getPcoDtUltelriscparPr().setPcoDtUltelriscparPrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltelriscparPr.Len.PCO_DT_ULTELRISCPAR_PR_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULTC-IS-IN = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULTC-IS-IN-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltcIsIn() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULTC-IS-IN-NULL
            paramComp.getPcoDtUltcIsIn().setPcoDtUltcIsInNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltcIsIn.Len.PCO_DT_ULTC_IS_IN_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-RICL-PRE = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-RICL-PRE-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltRiclPre() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-RICL-PRE-NULL
            paramComp.getPcoDtUltRiclPre().setPcoDtUltRiclPreNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltRiclPre.Len.PCO_DT_ULT_RICL_PRE_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULTC-MARSOL = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULTC-MARSOL-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltcMarsol() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULTC-MARSOL-NULL
            paramComp.getPcoDtUltcMarsol().setPcoDtUltcMarsolNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltcMarsol.Len.PCO_DT_ULTC_MARSOL_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULTC-RB-IN = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULTC-RB-IN-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltcRbIn() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULTC-RB-IN-NULL
            paramComp.getPcoDtUltcRbIn().setPcoDtUltcRbInNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltcRbIn.Len.PCO_DT_ULTC_RB_IN_NULL));
        }
        // COB_CODE: IF IND-PCO-PC-PROV-1AA-ACQ = -1
        //              MOVE HIGH-VALUES TO PCO-PC-PROV-1AA-ACQ-NULL
        //           END-IF
        if (ws.getIndParamComp().getPcProv1aaAcq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-PC-PROV-1AA-ACQ-NULL
            paramComp.getPcoPcProv1aaAcq().setPcoPcProv1aaAcqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoPcProv1aaAcq.Len.PCO_PC_PROV1AA_ACQ_NULL));
        }
        // COB_CODE: IF IND-PCO-MOD-INTR-PREST = -1
        //              MOVE HIGH-VALUES TO PCO-MOD-INTR-PREST-NULL
        //           END-IF
        if (ws.getIndParamComp().getModIntrPrest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-MOD-INTR-PREST-NULL
            paramComp.setPcoModIntrPrest(LiteralGenerator.create(Types.HIGH_CHAR_VAL, ParamCompIdbspco0.Len.PCO_MOD_INTR_PREST));
        }
        // COB_CODE: IF IND-PCO-GG-MAX-REC-PROV = -1
        //              MOVE HIGH-VALUES TO PCO-GG-MAX-REC-PROV-NULL
        //           END-IF
        if (ws.getIndParamComp().getGgMaxRecProv() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-GG-MAX-REC-PROV-NULL
            paramComp.getPcoGgMaxRecProv().setPcoGgMaxRecProvNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoGgMaxRecProv.Len.PCO_GG_MAX_REC_PROV_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULTGZ-TRCH-E-IN = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULTGZ-TRCH-E-IN-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltgzTrchEIn() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULTGZ-TRCH-E-IN-NULL
            paramComp.getPcoDtUltgzTrchEIn().setPcoDtUltgzTrchEInNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltgzTrchEIn.Len.PCO_DT_ULTGZ_TRCH_E_IN_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-SNDEN = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-SNDEN-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollSnden() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-SNDEN-NULL
            paramComp.getPcoDtUltBollSnden().setPcoDtUltBollSndenNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltBollSnden.Len.PCO_DT_ULT_BOLL_SNDEN_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-SNDNLQ = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-SNDNLQ-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollSndnlq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-SNDNLQ-NULL
            paramComp.getPcoDtUltBollSndnlq().setPcoDtUltBollSndnlqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltBollSndnlq.Len.PCO_DT_ULT_BOLL_SNDNLQ_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULTSC-ELAB-IN = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULTSC-ELAB-IN-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltscElabIn() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULTSC-ELAB-IN-NULL
            paramComp.getPcoDtUltscElabIn().setPcoDtUltscElabInNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltscElabIn.Len.PCO_DT_ULTSC_ELAB_IN_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULTSC-OPZ-IN = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULTSC-OPZ-IN-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltscOpzIn() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULTSC-OPZ-IN-NULL
            paramComp.getPcoDtUltscOpzIn().setPcoDtUltscOpzInNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltscOpzIn.Len.PCO_DT_ULTSC_OPZ_IN_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULTC-BNSRIC-IN = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULTC-BNSRIC-IN-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltcBnsricIn() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULTC-BNSRIC-IN-NULL
            paramComp.getPcoDtUltcBnsricIn().setPcoDtUltcBnsricInNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltcBnsricIn.Len.PCO_DT_ULTC_BNSRIC_IN_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULTC-BNSFDT-IN = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULTC-BNSFDT-IN-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltcBnsfdtIn() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULTC-BNSFDT-IN-NULL
            paramComp.getPcoDtUltcBnsfdtIn().setPcoDtUltcBnsfdtInNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltcBnsfdtIn.Len.PCO_DT_ULTC_BNSFDT_IN_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-RINN-GARAC = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-RINN-GARAC-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltRinnGarac() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-RINN-GARAC-NULL
            paramComp.getPcoDtUltRinnGarac().setPcoDtUltRinnGaracNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltRinnGarac.Len.PCO_DT_ULT_RINN_GARAC_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULTGZ-CED = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULTGZ-CED-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltgzCed() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULTGZ-CED-NULL
            paramComp.getPcoDtUltgzCed().setPcoDtUltgzCedNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltgzCed.Len.PCO_DT_ULTGZ_CED_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-ELAB-PRLCOS = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-ELAB-PRLCOS-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltElabPrlcos() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-ELAB-PRLCOS-NULL
            paramComp.getPcoDtUltElabPrlcos().setPcoDtUltElabPrlcosNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltElabPrlcos.Len.PCO_DT_ULT_ELAB_PRLCOS_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-RINN-COLL = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-RINN-COLL-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltRinnColl() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-RINN-COLL-NULL
            paramComp.getPcoDtUltRinnColl().setPcoDtUltRinnCollNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltRinnColl.Len.PCO_DT_ULT_RINN_COLL_NULL));
        }
        // COB_CODE: IF IND-PCO-FL-RVC-PERF = -1
        //              MOVE HIGH-VALUES TO PCO-FL-RVC-PERF-NULL
        //           END-IF
        if (ws.getIndParamComp().getFlRvcPerf() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-FL-RVC-PERF-NULL
            paramComp.setPcoFlRvcPerf(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-PCO-FL-RCS-POLI-NOPERF = -1
        //              MOVE HIGH-VALUES TO PCO-FL-RCS-POLI-NOPERF-NULL
        //           END-IF
        if (ws.getIndParamComp().getFlRcsPoliNoperf() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-FL-RCS-POLI-NOPERF-NULL
            paramComp.setPcoFlRcsPoliNoperf(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-PCO-FL-GEST-PLUSV = -1
        //              MOVE HIGH-VALUES TO PCO-FL-GEST-PLUSV-NULL
        //           END-IF
        if (ws.getIndParamComp().getFlGestPlusv() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-FL-GEST-PLUSV-NULL
            paramComp.setPcoFlGestPlusv(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-PCO-DT-ULT-RIVAL-CL = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-RIVAL-CL-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltRivalCl() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-RIVAL-CL-NULL
            paramComp.getPcoDtUltRivalCl().setPcoDtUltRivalClNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltRivalCl.Len.PCO_DT_ULT_RIVAL_CL_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-QTZO-CL = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-QTZO-CL-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltQtzoCl() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-QTZO-CL-NULL
            paramComp.getPcoDtUltQtzoCl().setPcoDtUltQtzoClNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltQtzoCl.Len.PCO_DT_ULT_QTZO_CL_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULTC-BNSRIC-CL = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULTC-BNSRIC-CL-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltcBnsricCl() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULTC-BNSRIC-CL-NULL
            paramComp.getPcoDtUltcBnsricCl().setPcoDtUltcBnsricClNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltcBnsricCl.Len.PCO_DT_ULTC_BNSRIC_CL_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULTC-BNSFDT-CL = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULTC-BNSFDT-CL-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltcBnsfdtCl() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULTC-BNSFDT-CL-NULL
            paramComp.getPcoDtUltcBnsfdtCl().setPcoDtUltcBnsfdtClNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltcBnsfdtCl.Len.PCO_DT_ULTC_BNSFDT_CL_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULTC-IS-CL = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULTC-IS-CL-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltcIsCl() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULTC-IS-CL-NULL
            paramComp.getPcoDtUltcIsCl().setPcoDtUltcIsClNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltcIsCl.Len.PCO_DT_ULTC_IS_CL_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULTC-RB-CL = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULTC-RB-CL-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltcRbCl() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULTC-RB-CL-NULL
            paramComp.getPcoDtUltcRbCl().setPcoDtUltcRbClNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltcRbCl.Len.PCO_DT_ULTC_RB_CL_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULTGZ-TRCH-E-CL = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULTGZ-TRCH-E-CL-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltgzTrchECl() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULTGZ-TRCH-E-CL-NULL
            paramComp.getPcoDtUltgzTrchECl().setPcoDtUltgzTrchEClNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltgzTrchECl.Len.PCO_DT_ULTGZ_TRCH_E_CL_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULTSC-ELAB-CL = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULTSC-ELAB-CL-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltscElabCl() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULTSC-ELAB-CL-NULL
            paramComp.getPcoDtUltscElabCl().setPcoDtUltscElabClNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltscElabCl.Len.PCO_DT_ULTSC_ELAB_CL_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULTSC-OPZ-CL = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULTSC-OPZ-CL-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltscOpzCl() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULTSC-OPZ-CL-NULL
            paramComp.getPcoDtUltscOpzCl().setPcoDtUltscOpzClNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltscOpzCl.Len.PCO_DT_ULTSC_OPZ_CL_NULL));
        }
        // COB_CODE: IF IND-PCO-STST-X-REGIONE = -1
        //              MOVE HIGH-VALUES TO PCO-STST-X-REGIONE-NULL
        //           END-IF
        if (ws.getIndParamComp().getStstXRegione() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-STST-X-REGIONE-NULL
            paramComp.getPcoStstXRegione().setPcoStstXRegioneNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoStstXRegione.Len.PCO_STST_X_REGIONE_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULTGZ-CED-COLL = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULTGZ-CED-COLL-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltgzCedColl() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULTGZ-CED-COLL-NULL
            paramComp.getPcoDtUltgzCedColl().setPcoDtUltgzCedCollNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltgzCedColl.Len.PCO_DT_ULTGZ_CED_COLL_NULL));
        }
        // COB_CODE: IF IND-PCO-NUM-MM-CALC-MORA = -1
        //              MOVE HIGH-VALUES TO PCO-NUM-MM-CALC-MORA-NULL
        //           END-IF
        if (ws.getIndParamComp().getNumMmCalcMora() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-NUM-MM-CALC-MORA-NULL
            paramComp.getPcoNumMmCalcMora().setPcoNumMmCalcMoraNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoNumMmCalcMora.Len.PCO_NUM_MM_CALC_MORA_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-EC-RIV-COLL = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-EC-RIV-COLL-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltEcRivColl() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-EC-RIV-COLL-NULL
            paramComp.getPcoDtUltEcRivColl().setPcoDtUltEcRivCollNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltEcRivColl.Len.PCO_DT_ULT_EC_RIV_COLL_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-EC-RIV-IND = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-EC-RIV-IND-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltEcRivInd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-EC-RIV-IND-NULL
            paramComp.getPcoDtUltEcRivInd().setPcoDtUltEcRivIndNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltEcRivInd.Len.PCO_DT_ULT_EC_RIV_IND_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-EC-IL-COLL = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-EC-IL-COLL-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltEcIlColl() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-EC-IL-COLL-NULL
            paramComp.getPcoDtUltEcIlColl().setPcoDtUltEcIlCollNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltEcIlColl.Len.PCO_DT_ULT_EC_IL_COLL_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-EC-IL-IND = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-EC-IL-IND-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltEcIlInd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-EC-IL-IND-NULL
            paramComp.getPcoDtUltEcIlInd().setPcoDtUltEcIlIndNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltEcIlInd.Len.PCO_DT_ULT_EC_IL_IND_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-EC-UL-COLL = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-EC-UL-COLL-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltEcUlColl() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-EC-UL-COLL-NULL
            paramComp.getPcoDtUltEcUlColl().setPcoDtUltEcUlCollNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltEcUlColl.Len.PCO_DT_ULT_EC_UL_COLL_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-EC-UL-IND = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-EC-UL-IND-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltEcUlInd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-EC-UL-IND-NULL
            paramComp.getPcoDtUltEcUlInd().setPcoDtUltEcUlIndNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltEcUlInd.Len.PCO_DT_ULT_EC_UL_IND_NULL));
        }
        // COB_CODE: IF IND-PCO-AA-UTI = -1
        //              MOVE HIGH-VALUES TO PCO-AA-UTI-NULL
        //           END-IF
        if (ws.getIndParamComp().getAaUti() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-AA-UTI-NULL
            paramComp.getPcoAaUti().setPcoAaUtiNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoAaUti.Len.PCO_AA_UTI_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-PERF-C = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-PERF-C-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollPerfC() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-PERF-C-NULL
            paramComp.getPcoDtUltBollPerfC().setPcoDtUltBollPerfCNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltBollPerfC.Len.PCO_DT_ULT_BOLL_PERF_C_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-RSP-IN = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-RSP-IN-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollRspIn() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-RSP-IN-NULL
            paramComp.getPcoDtUltBollRspIn().setPcoDtUltBollRspInNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltBollRspIn.Len.PCO_DT_ULT_BOLL_RSP_IN_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-RSP-CL = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-RSP-CL-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollRspCl() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-RSP-CL-NULL
            paramComp.getPcoDtUltBollRspCl().setPcoDtUltBollRspClNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltBollRspCl.Len.PCO_DT_ULT_BOLL_RSP_CL_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-EMES-I = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-EMES-I-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollEmesI() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-EMES-I-NULL
            paramComp.getPcoDtUltBollEmesI().setPcoDtUltBollEmesINull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltBollEmesI.Len.PCO_DT_ULT_BOLL_EMES_I_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-STOR-I = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-STOR-I-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollStorI() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-STOR-I-NULL
            paramComp.getPcoDtUltBollStorI().setPcoDtUltBollStorINull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltBollStorI.Len.PCO_DT_ULT_BOLL_STOR_I_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-RIAT-I = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-RIAT-I-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollRiatI() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-RIAT-I-NULL
            paramComp.getPcoDtUltBollRiatI().setPcoDtUltBollRiatINull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltBollRiatI.Len.PCO_DT_ULT_BOLL_RIAT_I_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-SD-I = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-SD-I-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollSdI() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-SD-I-NULL
            paramComp.getPcoDtUltBollSdI().setPcoDtUltBollSdINull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltBollSdI.Len.PCO_DT_ULT_BOLL_SD_I_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-SDNL-I = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-SDNL-I-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollSdnlI() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-SDNL-I-NULL
            paramComp.getPcoDtUltBollSdnlI().setPcoDtUltBollSdnlINull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltBollSdnlI.Len.PCO_DT_ULT_BOLL_SDNL_I_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-PERF-I = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-PERF-I-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollPerfI() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-PERF-I-NULL
            paramComp.getPcoDtUltBollPerfI().setPcoDtUltBollPerfINull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltBollPerfI.Len.PCO_DT_ULT_BOLL_PERF_I_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-RICL-RIRIAS-COM = -1
        //              MOVE HIGH-VALUES TO PCO-DT-RICL-RIRIAS-COM-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtRiclRiriasCom() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-RICL-RIRIAS-COM-NULL
            paramComp.getPcoDtRiclRiriasCom().setPcoDtRiclRiriasComNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtRiclRiriasCom.Len.PCO_DT_RICL_RIRIAS_COM_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-ELAB-AT92-C = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-ELAB-AT92-C-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltElabAt92C() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-ELAB-AT92-C-NULL
            paramComp.getPcoDtUltElabAt92C().setPcoDtUltElabAt92CNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltElabAt92C.Len.PCO_DT_ULT_ELAB_AT92_C_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-ELAB-AT92-I = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-ELAB-AT92-I-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltElabAt92I() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-ELAB-AT92-I-NULL
            paramComp.getPcoDtUltElabAt92I().setPcoDtUltElabAt92INull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltElabAt92I.Len.PCO_DT_ULT_ELAB_AT92_I_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-ELAB-AT93-C = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-ELAB-AT93-C-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltElabAt93C() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-ELAB-AT93-C-NULL
            paramComp.getPcoDtUltElabAt93C().setPcoDtUltElabAt93CNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltElabAt93C.Len.PCO_DT_ULT_ELAB_AT93_C_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-ELAB-AT93-I = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-ELAB-AT93-I-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltElabAt93I() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-ELAB-AT93-I-NULL
            paramComp.getPcoDtUltElabAt93I().setPcoDtUltElabAt93INull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltElabAt93I.Len.PCO_DT_ULT_ELAB_AT93_I_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-ELAB-SPE-IN = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-ELAB-SPE-IN-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltElabSpeIn() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-ELAB-SPE-IN-NULL
            paramComp.getPcoDtUltElabSpeIn().setPcoDtUltElabSpeInNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltElabSpeIn.Len.PCO_DT_ULT_ELAB_SPE_IN_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-ELAB-PR-CON = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-ELAB-PR-CON-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltElabPrCon() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-ELAB-PR-CON-NULL
            paramComp.getPcoDtUltElabPrCon().setPcoDtUltElabPrConNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltElabPrCon.Len.PCO_DT_ULT_ELAB_PR_CON_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-RP-CL = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-RP-CL-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollRpCl() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-RP-CL-NULL
            paramComp.getPcoDtUltBollRpCl().setPcoDtUltBollRpClNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltBollRpCl.Len.PCO_DT_ULT_BOLL_RP_CL_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-RP-IN = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-RP-IN-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollRpIn() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-RP-IN-NULL
            paramComp.getPcoDtUltBollRpIn().setPcoDtUltBollRpInNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltBollRpIn.Len.PCO_DT_ULT_BOLL_RP_IN_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-PRE-I = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-PRE-I-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollPreI() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-PRE-I-NULL
            paramComp.getPcoDtUltBollPreI().setPcoDtUltBollPreINull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltBollPreI.Len.PCO_DT_ULT_BOLL_PRE_I_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-PRE-C = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-PRE-C-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollPreC() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-PRE-C-NULL
            paramComp.getPcoDtUltBollPreC().setPcoDtUltBollPreCNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltBollPreC.Len.PCO_DT_ULT_BOLL_PRE_C_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULTC-PILDI-MM-C = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULTC-PILDI-MM-C-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltcPildiMmC() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULTC-PILDI-MM-C-NULL
            paramComp.getPcoDtUltcPildiMmC().setPcoDtUltcPildiMmCNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltcPildiMmC.Len.PCO_DT_ULTC_PILDI_MM_C_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULTC-PILDI-AA-C = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULTC-PILDI-AA-C-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltcPildiAaC() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULTC-PILDI-AA-C-NULL
            paramComp.getPcoDtUltcPildiAaC().setPcoDtUltcPildiAaCNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltcPildiAaC.Len.PCO_DT_ULTC_PILDI_AA_C_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULTC-PILDI-MM-I = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULTC-PILDI-MM-I-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltcPildiMmI() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULTC-PILDI-MM-I-NULL
            paramComp.getPcoDtUltcPildiMmI().setPcoDtUltcPildiMmINull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltcPildiMmI.Len.PCO_DT_ULTC_PILDI_MM_I_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULTC-PILDI-TR-I = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULTC-PILDI-TR-I-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltcPildiTrI() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULTC-PILDI-TR-I-NULL
            paramComp.getPcoDtUltcPildiTrI().setPcoDtUltcPildiTrINull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltcPildiTrI.Len.PCO_DT_ULTC_PILDI_TR_I_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULTC-PILDI-AA-I = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULTC-PILDI-AA-I-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltcPildiAaI() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULTC-PILDI-AA-I-NULL
            paramComp.getPcoDtUltcPildiAaI().setPcoDtUltcPildiAaINull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltcPildiAaI.Len.PCO_DT_ULTC_PILDI_AA_I_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-QUIE-C = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-QUIE-C-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollQuieC() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-QUIE-C-NULL
            paramComp.getPcoDtUltBollQuieC().setPcoDtUltBollQuieCNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltBollQuieC.Len.PCO_DT_ULT_BOLL_QUIE_C_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-QUIE-I = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-QUIE-I-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollQuieI() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-QUIE-I-NULL
            paramComp.getPcoDtUltBollQuieI().setPcoDtUltBollQuieINull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltBollQuieI.Len.PCO_DT_ULT_BOLL_QUIE_I_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-COTR-I = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-COTR-I-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollCotrI() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-COTR-I-NULL
            paramComp.getPcoDtUltBollCotrI().setPcoDtUltBollCotrINull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltBollCotrI.Len.PCO_DT_ULT_BOLL_COTR_I_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-COTR-C = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-COTR-C-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollCotrC() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-COTR-C-NULL
            paramComp.getPcoDtUltBollCotrC().setPcoDtUltBollCotrCNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltBollCotrC.Len.PCO_DT_ULT_BOLL_COTR_C_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-CORI-C = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-CORI-C-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollCoriC() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-CORI-C-NULL
            paramComp.getPcoDtUltBollCoriC().setPcoDtUltBollCoriCNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltBollCoriC.Len.PCO_DT_ULT_BOLL_CORI_C_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-CORI-I = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-CORI-I-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollCoriI() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-BOLL-CORI-I-NULL
            paramComp.getPcoDtUltBollCoriI().setPcoDtUltBollCoriINull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltBollCoriI.Len.PCO_DT_ULT_BOLL_CORI_I_NULL));
        }
        // COB_CODE: IF IND-PCO-TP-VALZZ-DT-VLT = -1
        //              MOVE HIGH-VALUES TO PCO-TP-VALZZ-DT-VLT-NULL
        //           END-IF
        if (ws.getIndParamComp().getTpValzzDtVlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-TP-VALZZ-DT-VLT-NULL
            paramComp.setPcoTpValzzDtVlt(LiteralGenerator.create(Types.HIGH_CHAR_VAL, ParamCompIdbspco0.Len.PCO_TP_VALZZ_DT_VLT));
        }
        // COB_CODE: IF IND-PCO-FL-FRAZ-PROV-ACQ = -1
        //              MOVE HIGH-VALUES TO PCO-FL-FRAZ-PROV-ACQ-NULL
        //           END-IF
        if (ws.getIndParamComp().getFlFrazProvAcq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-FL-FRAZ-PROV-ACQ-NULL
            paramComp.setPcoFlFrazProvAcq(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-PCO-DT-ULT-AGG-EROG-RE = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-AGG-EROG-RE-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltAggErogRe() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-AGG-EROG-RE-NULL
            paramComp.getPcoDtUltAggErogRe().setPcoDtUltAggErogReNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltAggErogRe.Len.PCO_DT_ULT_AGG_EROG_RE_NULL));
        }
        // COB_CODE: IF IND-PCO-PC-RM-MARSOL = -1
        //              MOVE HIGH-VALUES TO PCO-PC-RM-MARSOL-NULL
        //           END-IF
        if (ws.getIndParamComp().getPcRmMarsol() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-PC-RM-MARSOL-NULL
            paramComp.getPcoPcRmMarsol().setPcoPcRmMarsolNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoPcRmMarsol.Len.PCO_PC_RM_MARSOL_NULL));
        }
        // COB_CODE: IF IND-PCO-PC-C-SUBRSH-MARSOL = -1
        //              MOVE HIGH-VALUES TO PCO-PC-C-SUBRSH-MARSOL-NULL
        //           END-IF
        if (ws.getIndParamComp().getPcCSubrshMarsol() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-PC-C-SUBRSH-MARSOL-NULL
            paramComp.getPcoPcCSubrshMarsol().setPcoPcCSubrshMarsolNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoPcCSubrshMarsol.Len.PCO_PC_C_SUBRSH_MARSOL_NULL));
        }
        // COB_CODE: IF IND-PCO-COD-COMP-ISVAP = -1
        //              MOVE HIGH-VALUES TO PCO-COD-COMP-ISVAP-NULL
        //           END-IF
        if (ws.getIndParamComp().getCodCompIsvap() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-COD-COMP-ISVAP-NULL
            paramComp.setPcoCodCompIsvap(LiteralGenerator.create(Types.HIGH_CHAR_VAL, ParamCompIdbspco0.Len.PCO_COD_COMP_ISVAP));
        }
        // COB_CODE: IF IND-PCO-LM-RIS-CON-INT = -1
        //              MOVE HIGH-VALUES TO PCO-LM-RIS-CON-INT-NULL
        //           END-IF
        if (ws.getIndParamComp().getLmRisConInt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-LM-RIS-CON-INT-NULL
            paramComp.getPcoLmRisConInt().setPcoLmRisConIntNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoLmRisConInt.Len.PCO_LM_RIS_CON_INT_NULL));
        }
        // COB_CODE: IF IND-PCO-LM-C-SUBRSH-CON-IN = -1
        //              MOVE HIGH-VALUES TO PCO-LM-C-SUBRSH-CON-IN-NULL
        //           END-IF
        if (ws.getIndParamComp().getLmCSubrshConIn() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-LM-C-SUBRSH-CON-IN-NULL
            paramComp.getPcoLmCSubrshConIn().setPcoLmCSubrshConInNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoLmCSubrshConIn.Len.PCO_LM_C_SUBRSH_CON_IN_NULL));
        }
        // COB_CODE: IF IND-PCO-PC-GAR-NORISK-MARS = -1
        //              MOVE HIGH-VALUES TO PCO-PC-GAR-NORISK-MARS-NULL
        //           END-IF
        if (ws.getIndParamComp().getPcGarNoriskMars() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-PC-GAR-NORISK-MARS-NULL
            paramComp.getPcoPcGarNoriskMars().setPcoPcGarNoriskMarsNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoPcGarNoriskMars.Len.PCO_PC_GAR_NORISK_MARS_NULL));
        }
        // COB_CODE: IF IND-PCO-CRZ-1A-RAT-INTR-PR = -1
        //              MOVE HIGH-VALUES TO PCO-CRZ-1A-RAT-INTR-PR-NULL
        //           END-IF
        if (ws.getIndParamComp().getCrz1aRatIntrPr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-CRZ-1A-RAT-INTR-PR-NULL
            paramComp.setPcoCrz1aRatIntrPr(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-PCO-NUM-GG-ARR-INTR-PR = -1
        //              MOVE HIGH-VALUES TO PCO-NUM-GG-ARR-INTR-PR-NULL
        //           END-IF
        if (ws.getIndParamComp().getNumGgArrIntrPr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-NUM-GG-ARR-INTR-PR-NULL
            paramComp.getPcoNumGgArrIntrPr().setPcoNumGgArrIntrPrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoNumGgArrIntrPr.Len.PCO_NUM_GG_ARR_INTR_PR_NULL));
        }
        // COB_CODE: IF IND-PCO-FL-VISUAL-VINPG = -1
        //              MOVE HIGH-VALUES TO PCO-FL-VISUAL-VINPG-NULL
        //           END-IF
        if (ws.getIndParamComp().getFlVisualVinpg() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-FL-VISUAL-VINPG-NULL
            paramComp.setPcoFlVisualVinpg(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-PCO-DT-ULT-ESTRAZ-FUG = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-ESTRAZ-FUG-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltEstrazFug() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-ESTRAZ-FUG-NULL
            paramComp.getPcoDtUltEstrazFug().setPcoDtUltEstrazFugNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltEstrazFug.Len.PCO_DT_ULT_ESTRAZ_FUG_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-ELAB-PR-AUT = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-ELAB-PR-AUT-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltElabPrAut() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-ELAB-PR-AUT-NULL
            paramComp.getPcoDtUltElabPrAut().setPcoDtUltElabPrAutNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltElabPrAut.Len.PCO_DT_ULT_ELAB_PR_AUT_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-ELAB-COMMEF = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-ELAB-COMMEF-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltElabCommef() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-ELAB-COMMEF-NULL
            paramComp.getPcoDtUltElabCommef().setPcoDtUltElabCommefNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltElabCommef.Len.PCO_DT_ULT_ELAB_COMMEF_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-ELAB-LIQMEF = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-ELAB-LIQMEF-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltElabLiqmef() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-ELAB-LIQMEF-NULL
            paramComp.getPcoDtUltElabLiqmef().setPcoDtUltElabLiqmefNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltElabLiqmef.Len.PCO_DT_ULT_ELAB_LIQMEF_NULL));
        }
        // COB_CODE: IF IND-PCO-COD-FISC-MEF = -1
        //              MOVE HIGH-VALUES TO PCO-COD-FISC-MEF-NULL
        //           END-IF
        if (ws.getIndParamComp().getCodFiscMef() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-COD-FISC-MEF-NULL
            paramComp.setPcoCodFiscMef(LiteralGenerator.create(Types.HIGH_CHAR_VAL, ParamCompIdbspco0.Len.PCO_COD_FISC_MEF));
        }
        // COB_CODE: IF IND-PCO-IMP-ASS-SOCIALE = -1
        //              MOVE HIGH-VALUES TO PCO-IMP-ASS-SOCIALE-NULL
        //           END-IF
        if (ws.getIndParamComp().getImpAssSociale() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-IMP-ASS-SOCIALE-NULL
            paramComp.getPcoImpAssSociale().setPcoImpAssSocialeNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoImpAssSociale.Len.PCO_IMP_ASS_SOCIALE_NULL));
        }
        // COB_CODE: IF IND-PCO-MOD-COMNZ-INVST-SW = -1
        //              MOVE HIGH-VALUES TO PCO-MOD-COMNZ-INVST-SW-NULL
        //           END-IF
        if (ws.getIndParamComp().getModComnzInvstSw() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-MOD-COMNZ-INVST-SW-NULL
            paramComp.setPcoModComnzInvstSw(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-PCO-DT-RIAT-RIASS-RSH = -1
        //              MOVE HIGH-VALUES TO PCO-DT-RIAT-RIASS-RSH-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtRiatRiassRsh() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-RIAT-RIASS-RSH-NULL
            paramComp.getPcoDtRiatRiassRsh().setPcoDtRiatRiassRshNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtRiatRiassRsh.Len.PCO_DT_RIAT_RIASS_RSH_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-RIAT-RIASS-COMM = -1
        //              MOVE HIGH-VALUES TO PCO-DT-RIAT-RIASS-COMM-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtRiatRiassComm() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-RIAT-RIASS-COMM-NULL
            paramComp.getPcoDtRiatRiassComm().setPcoDtRiatRiassCommNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtRiatRiassComm.Len.PCO_DT_RIAT_RIASS_COMM_NULL));
        }
        // COB_CODE: IF IND-PCO-GG-INTR-RIT-PAG = -1
        //              MOVE HIGH-VALUES TO PCO-GG-INTR-RIT-PAG-NULL
        //           END-IF
        if (ws.getIndParamComp().getGgIntrRitPag() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-GG-INTR-RIT-PAG-NULL
            paramComp.getPcoGgIntrRitPag().setPcoGgIntrRitPagNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoGgIntrRitPag.Len.PCO_GG_INTR_RIT_PAG_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-RINN-TAC = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-RINN-TAC-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltRinnTac() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-RINN-TAC-NULL
            paramComp.getPcoDtUltRinnTac().setPcoDtUltRinnTacNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltRinnTac.Len.PCO_DT_ULT_RINN_TAC_NULL));
        }
        // COB_CODE: IF IND-PCO-DESC-COMP = -1
        //              MOVE HIGH-VALUES TO PCO-DESC-COMP
        //           END-IF
        if (ws.getIndParamComp().getDescComp() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DESC-COMP
            paramComp.setPcoDescComp(LiteralGenerator.create(Types.HIGH_CHAR_VAL, ParamCompIdbspco0.Len.PCO_DESC_COMP));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-EC-TCM-IND = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-EC-TCM-IND-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltEcTcmInd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-EC-TCM-IND-NULL
            paramComp.getPcoDtUltEcTcmInd().setPcoDtUltEcTcmIndNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltEcTcmInd.Len.PCO_DT_ULT_EC_TCM_IND_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-EC-TCM-COLL = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-EC-TCM-COLL-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltEcTcmColl() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-EC-TCM-COLL-NULL
            paramComp.getPcoDtUltEcTcmColl().setPcoDtUltEcTcmCollNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltEcTcmColl.Len.PCO_DT_ULT_EC_TCM_COLL_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-EC-MRM-IND = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-EC-MRM-IND-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltEcMrmInd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-EC-MRM-IND-NULL
            paramComp.getPcoDtUltEcMrmInd().setPcoDtUltEcMrmIndNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltEcMrmInd.Len.PCO_DT_ULT_EC_MRM_IND_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-EC-MRM-COLL = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-EC-MRM-COLL-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltEcMrmColl() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-EC-MRM-COLL-NULL
            paramComp.getPcoDtUltEcMrmColl().setPcoDtUltEcMrmCollNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltEcMrmColl.Len.PCO_DT_ULT_EC_MRM_COLL_NULL));
        }
        // COB_CODE: IF IND-PCO-COD-COMP-LDAP = -1
        //              MOVE HIGH-VALUES TO PCO-COD-COMP-LDAP-NULL
        //           END-IF
        if (ws.getIndParamComp().getCodCompLdap() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-COD-COMP-LDAP-NULL
            paramComp.setPcoCodCompLdap(LiteralGenerator.create(Types.HIGH_CHAR_VAL, ParamCompIdbspco0.Len.PCO_COD_COMP_LDAP));
        }
        // COB_CODE: IF IND-PCO-PC-RID-IMP-1382011 = -1
        //              MOVE HIGH-VALUES TO PCO-PC-RID-IMP-1382011-NULL
        //           END-IF
        if (ws.getIndParamComp().getPcRidImp1382011() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-PC-RID-IMP-1382011-NULL
            paramComp.getPcoPcRidImp1382011().setPcoPcRidImp1382011Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoPcRidImp1382011.Len.PCO_PC_RID_IMP1382011_NULL));
        }
        // COB_CODE: IF IND-PCO-PC-RID-IMP-662014 = -1
        //              MOVE HIGH-VALUES TO PCO-PC-RID-IMP-662014-NULL
        //           END-IF
        if (ws.getIndParamComp().getPcRidImp662014() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-PC-RID-IMP-662014-NULL
            paramComp.getPcoPcRidImp662014().setPcoPcRidImp662014Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoPcRidImp662014.Len.PCO_PC_RID_IMP662014_NULL));
        }
        // COB_CODE: IF IND-PCO-SOGL-AML-PRE-UNI = -1
        //              MOVE HIGH-VALUES TO PCO-SOGL-AML-PRE-UNI-NULL
        //           END-IF
        if (ws.getIndParamComp().getSoglAmlPreUni() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-SOGL-AML-PRE-UNI-NULL
            paramComp.getPcoSoglAmlPreUni().setPcoSoglAmlPreUniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoSoglAmlPreUni.Len.PCO_SOGL_AML_PRE_UNI_NULL));
        }
        // COB_CODE: IF IND-PCO-SOGL-AML-PRE-PER = -1
        //              MOVE HIGH-VALUES TO PCO-SOGL-AML-PRE-PER-NULL
        //           END-IF
        if (ws.getIndParamComp().getSoglAmlPrePer() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-SOGL-AML-PRE-PER-NULL
            paramComp.getPcoSoglAmlPrePer().setPcoSoglAmlPrePerNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoSoglAmlPrePer.Len.PCO_SOGL_AML_PRE_PER_NULL));
        }
        // COB_CODE: IF IND-PCO-COD-SOGG-FTZ-ASSTO = -1
        //              MOVE HIGH-VALUES TO PCO-COD-SOGG-FTZ-ASSTO-NULL
        //           END-IF
        if (ws.getIndParamComp().getCodSoggFtzAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-COD-SOGG-FTZ-ASSTO-NULL
            paramComp.setPcoCodSoggFtzAssto(LiteralGenerator.create(Types.HIGH_CHAR_VAL, ParamCompIdbspco0.Len.PCO_COD_SOGG_FTZ_ASSTO));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-ELAB-REDPRO = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-ELAB-REDPRO-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltElabRedpro() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-ELAB-REDPRO-NULL
            paramComp.getPcoDtUltElabRedpro().setPcoDtUltElabRedproNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltElabRedpro.Len.PCO_DT_ULT_ELAB_REDPRO_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-ELAB-TAKE-P = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-ELAB-TAKE-P-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltElabTakeP() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-ELAB-TAKE-P-NULL
            paramComp.getPcoDtUltElabTakeP().setPcoDtUltElabTakePNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltElabTakeP.Len.PCO_DT_ULT_ELAB_TAKE_P_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-ELAB-PASPAS = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-ELAB-PASPAS-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltElabPaspas() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-ELAB-PASPAS-NULL
            paramComp.getPcoDtUltElabPaspas().setPcoDtUltElabPaspasNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltElabPaspas.Len.PCO_DT_ULT_ELAB_PASPAS_NULL));
        }
        // COB_CODE: IF IND-PCO-SOGL-AML-PRE-SAV-R = -1
        //              MOVE HIGH-VALUES TO PCO-SOGL-AML-PRE-SAV-R-NULL
        //           END-IF
        if (ws.getIndParamComp().getSoglAmlPreSavR() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-SOGL-AML-PRE-SAV-R-NULL
            paramComp.getPcoSoglAmlPreSavR().setPcoSoglAmlPreSavRNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoSoglAmlPreSavR.Len.PCO_SOGL_AML_PRE_SAV_R_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-ESTR-DEC-CO = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-ESTR-DEC-CO-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltEstrDecCo() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-ESTR-DEC-CO-NULL
            paramComp.getPcoDtUltEstrDecCo().setPcoDtUltEstrDecCoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltEstrDecCo.Len.PCO_DT_ULT_ESTR_DEC_CO_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-ELAB-COS-AT = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-ELAB-COS-AT-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltElabCosAt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-ELAB-COS-AT-NULL
            paramComp.getPcoDtUltElabCosAt().setPcoDtUltElabCosAtNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltElabCosAt.Len.PCO_DT_ULT_ELAB_COS_AT_NULL));
        }
        // COB_CODE: IF IND-PCO-FRQ-COSTI-ATT = -1
        //              MOVE HIGH-VALUES TO PCO-FRQ-COSTI-ATT-NULL
        //           END-IF
        if (ws.getIndParamComp().getFrqCostiAtt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-FRQ-COSTI-ATT-NULL
            paramComp.getPcoFrqCostiAtt().setPcoFrqCostiAttNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoFrqCostiAtt.Len.PCO_FRQ_COSTI_ATT_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ULT-ELAB-COS-ST = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ULT-ELAB-COS-ST-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtUltElabCosSt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ULT-ELAB-COS-ST-NULL
            paramComp.getPcoDtUltElabCosSt().setPcoDtUltElabCosStNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtUltElabCosSt.Len.PCO_DT_ULT_ELAB_COS_ST_NULL));
        }
        // COB_CODE: IF IND-PCO-FRQ-COSTI-STORNATI = -1
        //              MOVE HIGH-VALUES TO PCO-FRQ-COSTI-STORNATI-NULL
        //           END-IF
        if (ws.getIndParamComp().getFrqCostiStornati() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-FRQ-COSTI-STORNATI-NULL
            paramComp.getPcoFrqCostiStornati().setPcoFrqCostiStornatiNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoFrqCostiStornati.Len.PCO_FRQ_COSTI_STORNATI_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ESTR-ASS-MIN70A = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ESTR-ASS-MIN70A-NULL
        //           END-IF
        if (ws.getIndParamComp().getDtEstrAssMin70a() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ESTR-ASS-MIN70A-NULL
            paramComp.getPcoDtEstrAssMin70a().setPcoDtEstrAssMin70aNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtEstrAssMin70a.Len.PCO_DT_ESTR_ASS_MIN70A_NULL));
        }
        // COB_CODE: IF IND-PCO-DT-ESTR-ASS-MAG70A = -1
        //              MOVE HIGH-VALUES TO PCO-DT-ESTR-ASS-MAG70A-NULL
        //           END-IF.
        if (ws.getIndParamComp().getDtEstrAssMag70a() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-ESTR-ASS-MAG70A-NULL
            paramComp.getPcoDtEstrAssMag70a().setPcoDtEstrAssMag70aNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtEstrAssMag70a.Len.PCO_DT_ESTR_ASS_MAG70A_NULL));
        }
    }

    /**Original name: Z150-VALORIZZA-DATA-SERVICES-I<br>*/
    private void z150ValorizzaDataServicesI() {
        // COB_CODE: MOVE 'I' TO PCO-DS-OPER-SQL
        paramComp.setPcoDsOperSqlFormatted("I");
        // COB_CODE: MOVE 0                   TO PCO-DS-VER
        paramComp.setPcoDsVer(0);
        // COB_CODE: MOVE IDSV0003-USER-NAME TO PCO-DS-UTENTE
        paramComp.setPcoDsUtente(idsv0003.getUserName());
        // COB_CODE: MOVE '1'                   TO PCO-DS-STATO-ELAB
        paramComp.setPcoDsStatoElabFormatted("1");
        // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR TO PCO-DS-TS-CPTZ.
        paramComp.setPcoDsTsCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
    }

    /**Original name: Z160-VALORIZZA-DATA-SERVICES-U<br>*/
    private void z160ValorizzaDataServicesU() {
        // COB_CODE: MOVE 'U' TO PCO-DS-OPER-SQL
        paramComp.setPcoDsOperSqlFormatted("U");
        // COB_CODE: MOVE IDSV0003-USER-NAME TO PCO-DS-UTENTE
        paramComp.setPcoDsUtente(idsv0003.getUserName());
        // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR TO PCO-DS-TS-CPTZ.
        paramComp.setPcoDsTsCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
    }

    /**Original name: Z200-SET-INDICATORI-NULL<br>*/
    private void z200SetIndicatoriNull() {
        // COB_CODE: IF PCO-COD-TRAT-CIRT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-COD-TRAT-CIRT
        //           ELSE
        //              MOVE 0 TO IND-PCO-COD-TRAT-CIRT
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoCodTratCirtFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-COD-TRAT-CIRT
            ws.getIndParamComp().setCodTratCirt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-COD-TRAT-CIRT
            ws.getIndParamComp().setCodTratCirt(((short)0));
        }
        // COB_CODE: IF PCO-LIM-VLTR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-LIM-VLTR
        //           ELSE
        //              MOVE 0 TO IND-PCO-LIM-VLTR
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoLimVltr().getPcoLimVltrNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-LIM-VLTR
            ws.getIndParamComp().setLimVltr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-LIM-VLTR
            ws.getIndParamComp().setLimVltr(((short)0));
        }
        // COB_CODE: IF PCO-TP-RAT-PERF-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-TP-RAT-PERF
        //           ELSE
        //              MOVE 0 TO IND-PCO-TP-RAT-PERF
        //           END-IF
        if (Conditions.eq(paramComp.getPcoTpRatPerf(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-PCO-TP-RAT-PERF
            ws.getIndParamComp().setTpRatPerf(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-TP-RAT-PERF
            ws.getIndParamComp().setTpRatPerf(((short)0));
        }
        // COB_CODE: IF PCO-ARROT-PRE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-ARROT-PRE
        //           ELSE
        //              MOVE 0 TO IND-PCO-ARROT-PRE
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoArrotPre().getPcoArrotPreNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-ARROT-PRE
            ws.getIndParamComp().setArrotPre(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-ARROT-PRE
            ws.getIndParamComp().setArrotPre(((short)0));
        }
        // COB_CODE: IF PCO-DT-CONT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-CONT
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-CONT
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtCont().getPcoDtContNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-CONT
            ws.getIndParamComp().setDtCont(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-CONT
            ws.getIndParamComp().setDtCont(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-RIVAL-IN-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-RIVAL-IN
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-RIVAL-IN
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltRivalIn().getPcoDtUltRivalInNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-RIVAL-IN
            ws.getIndParamComp().setDtUltRivalIn(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-RIVAL-IN
            ws.getIndParamComp().setDtUltRivalIn(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-QTZO-IN-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-QTZO-IN
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-QTZO-IN
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltQtzoIn().getPcoDtUltQtzoInNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-QTZO-IN
            ws.getIndParamComp().setDtUltQtzoIn(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-QTZO-IN
            ws.getIndParamComp().setDtUltQtzoIn(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-RICL-RIASS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-RICL-RIASS
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-RICL-RIASS
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltRiclRiass().getPcoDtUltRiclRiassNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-RICL-RIASS
            ws.getIndParamComp().setDtUltRiclRiass(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-RICL-RIASS
            ws.getIndParamComp().setDtUltRiclRiass(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-TABUL-RIASS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-TABUL-RIASS
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-TABUL-RIASS
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltTabulRiass().getPcoDtUltTabulRiassNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-TABUL-RIASS
            ws.getIndParamComp().setDtUltTabulRiass(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-TABUL-RIASS
            ws.getIndParamComp().setDtUltTabulRiass(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-BOLL-EMES-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-BOLL-EMES
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-BOLL-EMES
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltBollEmes().getPcoDtUltBollEmesNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-BOLL-EMES
            ws.getIndParamComp().setDtUltBollEmes(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-BOLL-EMES
            ws.getIndParamComp().setDtUltBollEmes(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-BOLL-STOR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-BOLL-STOR
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-BOLL-STOR
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltBollStor().getPcoDtUltBollStorNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-BOLL-STOR
            ws.getIndParamComp().setDtUltBollStor(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-BOLL-STOR
            ws.getIndParamComp().setDtUltBollStor(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-BOLL-LIQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-BOLL-LIQ
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-BOLL-LIQ
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltBollLiq().getPcoDtUltBollLiqNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-BOLL-LIQ
            ws.getIndParamComp().setDtUltBollLiq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-BOLL-LIQ
            ws.getIndParamComp().setDtUltBollLiq(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-BOLL-RIAT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-BOLL-RIAT
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-BOLL-RIAT
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltBollRiat().getPcoDtUltBollRiatNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-BOLL-RIAT
            ws.getIndParamComp().setDtUltBollRiat(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-BOLL-RIAT
            ws.getIndParamComp().setDtUltBollRiat(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULTELRISCPAR-PR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULTELRISCPAR-PR
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULTELRISCPAR-PR
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltelriscparPr().getPcoDtUltelriscparPrNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULTELRISCPAR-PR
            ws.getIndParamComp().setDtUltelriscparPr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULTELRISCPAR-PR
            ws.getIndParamComp().setDtUltelriscparPr(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULTC-IS-IN-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULTC-IS-IN
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULTC-IS-IN
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltcIsIn().getPcoDtUltcIsInNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULTC-IS-IN
            ws.getIndParamComp().setDtUltcIsIn(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULTC-IS-IN
            ws.getIndParamComp().setDtUltcIsIn(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-RICL-PRE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-RICL-PRE
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-RICL-PRE
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltRiclPre().getPcoDtUltRiclPreNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-RICL-PRE
            ws.getIndParamComp().setDtUltRiclPre(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-RICL-PRE
            ws.getIndParamComp().setDtUltRiclPre(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULTC-MARSOL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULTC-MARSOL
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULTC-MARSOL
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltcMarsol().getPcoDtUltcMarsolNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULTC-MARSOL
            ws.getIndParamComp().setDtUltcMarsol(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULTC-MARSOL
            ws.getIndParamComp().setDtUltcMarsol(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULTC-RB-IN-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULTC-RB-IN
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULTC-RB-IN
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltcRbIn().getPcoDtUltcRbInNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULTC-RB-IN
            ws.getIndParamComp().setDtUltcRbIn(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULTC-RB-IN
            ws.getIndParamComp().setDtUltcRbIn(((short)0));
        }
        // COB_CODE: IF PCO-PC-PROV-1AA-ACQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-PC-PROV-1AA-ACQ
        //           ELSE
        //              MOVE 0 TO IND-PCO-PC-PROV-1AA-ACQ
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoPcProv1aaAcq().getPcoPcProv1aaAcqNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-PC-PROV-1AA-ACQ
            ws.getIndParamComp().setPcProv1aaAcq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-PC-PROV-1AA-ACQ
            ws.getIndParamComp().setPcProv1aaAcq(((short)0));
        }
        // COB_CODE: IF PCO-MOD-INTR-PREST-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-MOD-INTR-PREST
        //           ELSE
        //              MOVE 0 TO IND-PCO-MOD-INTR-PREST
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoModIntrPrestFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-MOD-INTR-PREST
            ws.getIndParamComp().setModIntrPrest(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-MOD-INTR-PREST
            ws.getIndParamComp().setModIntrPrest(((short)0));
        }
        // COB_CODE: IF PCO-GG-MAX-REC-PROV-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-GG-MAX-REC-PROV
        //           ELSE
        //              MOVE 0 TO IND-PCO-GG-MAX-REC-PROV
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoGgMaxRecProv().getPcoGgMaxRecProvNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-GG-MAX-REC-PROV
            ws.getIndParamComp().setGgMaxRecProv(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-GG-MAX-REC-PROV
            ws.getIndParamComp().setGgMaxRecProv(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULTGZ-TRCH-E-IN-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULTGZ-TRCH-E-IN
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULTGZ-TRCH-E-IN
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltgzTrchEIn().getPcoDtUltgzTrchEInNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULTGZ-TRCH-E-IN
            ws.getIndParamComp().setDtUltgzTrchEIn(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULTGZ-TRCH-E-IN
            ws.getIndParamComp().setDtUltgzTrchEIn(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-BOLL-SNDEN-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-BOLL-SNDEN
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-BOLL-SNDEN
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltBollSnden().getPcoDtUltBollSndenNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-BOLL-SNDEN
            ws.getIndParamComp().setDtUltBollSnden(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-BOLL-SNDEN
            ws.getIndParamComp().setDtUltBollSnden(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-BOLL-SNDNLQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-BOLL-SNDNLQ
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-BOLL-SNDNLQ
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltBollSndnlq().getPcoDtUltBollSndnlqNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-BOLL-SNDNLQ
            ws.getIndParamComp().setDtUltBollSndnlq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-BOLL-SNDNLQ
            ws.getIndParamComp().setDtUltBollSndnlq(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULTSC-ELAB-IN-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULTSC-ELAB-IN
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULTSC-ELAB-IN
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltscElabIn().getPcoDtUltscElabInNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULTSC-ELAB-IN
            ws.getIndParamComp().setDtUltscElabIn(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULTSC-ELAB-IN
            ws.getIndParamComp().setDtUltscElabIn(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULTSC-OPZ-IN-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULTSC-OPZ-IN
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULTSC-OPZ-IN
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltscOpzIn().getPcoDtUltscOpzInNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULTSC-OPZ-IN
            ws.getIndParamComp().setDtUltscOpzIn(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULTSC-OPZ-IN
            ws.getIndParamComp().setDtUltscOpzIn(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULTC-BNSRIC-IN-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULTC-BNSRIC-IN
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULTC-BNSRIC-IN
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltcBnsricIn().getPcoDtUltcBnsricInNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULTC-BNSRIC-IN
            ws.getIndParamComp().setDtUltcBnsricIn(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULTC-BNSRIC-IN
            ws.getIndParamComp().setDtUltcBnsricIn(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULTC-BNSFDT-IN-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULTC-BNSFDT-IN
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULTC-BNSFDT-IN
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltcBnsfdtIn().getPcoDtUltcBnsfdtInNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULTC-BNSFDT-IN
            ws.getIndParamComp().setDtUltcBnsfdtIn(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULTC-BNSFDT-IN
            ws.getIndParamComp().setDtUltcBnsfdtIn(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-RINN-GARAC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-RINN-GARAC
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-RINN-GARAC
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltRinnGarac().getPcoDtUltRinnGaracNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-RINN-GARAC
            ws.getIndParamComp().setDtUltRinnGarac(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-RINN-GARAC
            ws.getIndParamComp().setDtUltRinnGarac(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULTGZ-CED-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULTGZ-CED
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULTGZ-CED
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltgzCed().getPcoDtUltgzCedNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULTGZ-CED
            ws.getIndParamComp().setDtUltgzCed(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULTGZ-CED
            ws.getIndParamComp().setDtUltgzCed(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-ELAB-PRLCOS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-ELAB-PRLCOS
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-ELAB-PRLCOS
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltElabPrlcos().getPcoDtUltElabPrlcosNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-ELAB-PRLCOS
            ws.getIndParamComp().setDtUltElabPrlcos(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-ELAB-PRLCOS
            ws.getIndParamComp().setDtUltElabPrlcos(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-RINN-COLL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-RINN-COLL
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-RINN-COLL
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltRinnColl().getPcoDtUltRinnCollNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-RINN-COLL
            ws.getIndParamComp().setDtUltRinnColl(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-RINN-COLL
            ws.getIndParamComp().setDtUltRinnColl(((short)0));
        }
        // COB_CODE: IF PCO-FL-RVC-PERF-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-FL-RVC-PERF
        //           ELSE
        //              MOVE 0 TO IND-PCO-FL-RVC-PERF
        //           END-IF
        if (Conditions.eq(paramComp.getPcoFlRvcPerf(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-PCO-FL-RVC-PERF
            ws.getIndParamComp().setFlRvcPerf(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-FL-RVC-PERF
            ws.getIndParamComp().setFlRvcPerf(((short)0));
        }
        // COB_CODE: IF PCO-FL-RCS-POLI-NOPERF-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-FL-RCS-POLI-NOPERF
        //           ELSE
        //              MOVE 0 TO IND-PCO-FL-RCS-POLI-NOPERF
        //           END-IF
        if (Conditions.eq(paramComp.getPcoFlRcsPoliNoperf(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-PCO-FL-RCS-POLI-NOPERF
            ws.getIndParamComp().setFlRcsPoliNoperf(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-FL-RCS-POLI-NOPERF
            ws.getIndParamComp().setFlRcsPoliNoperf(((short)0));
        }
        // COB_CODE: IF PCO-FL-GEST-PLUSV-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-FL-GEST-PLUSV
        //           ELSE
        //              MOVE 0 TO IND-PCO-FL-GEST-PLUSV
        //           END-IF
        if (Conditions.eq(paramComp.getPcoFlGestPlusv(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-PCO-FL-GEST-PLUSV
            ws.getIndParamComp().setFlGestPlusv(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-FL-GEST-PLUSV
            ws.getIndParamComp().setFlGestPlusv(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-RIVAL-CL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-RIVAL-CL
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-RIVAL-CL
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltRivalCl().getPcoDtUltRivalClNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-RIVAL-CL
            ws.getIndParamComp().setDtUltRivalCl(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-RIVAL-CL
            ws.getIndParamComp().setDtUltRivalCl(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-QTZO-CL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-QTZO-CL
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-QTZO-CL
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltQtzoCl().getPcoDtUltQtzoClNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-QTZO-CL
            ws.getIndParamComp().setDtUltQtzoCl(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-QTZO-CL
            ws.getIndParamComp().setDtUltQtzoCl(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULTC-BNSRIC-CL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULTC-BNSRIC-CL
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULTC-BNSRIC-CL
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltcBnsricCl().getPcoDtUltcBnsricClNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULTC-BNSRIC-CL
            ws.getIndParamComp().setDtUltcBnsricCl(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULTC-BNSRIC-CL
            ws.getIndParamComp().setDtUltcBnsricCl(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULTC-BNSFDT-CL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULTC-BNSFDT-CL
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULTC-BNSFDT-CL
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltcBnsfdtCl().getPcoDtUltcBnsfdtClNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULTC-BNSFDT-CL
            ws.getIndParamComp().setDtUltcBnsfdtCl(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULTC-BNSFDT-CL
            ws.getIndParamComp().setDtUltcBnsfdtCl(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULTC-IS-CL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULTC-IS-CL
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULTC-IS-CL
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltcIsCl().getPcoDtUltcIsClNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULTC-IS-CL
            ws.getIndParamComp().setDtUltcIsCl(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULTC-IS-CL
            ws.getIndParamComp().setDtUltcIsCl(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULTC-RB-CL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULTC-RB-CL
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULTC-RB-CL
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltcRbCl().getPcoDtUltcRbClNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULTC-RB-CL
            ws.getIndParamComp().setDtUltcRbCl(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULTC-RB-CL
            ws.getIndParamComp().setDtUltcRbCl(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULTGZ-TRCH-E-CL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULTGZ-TRCH-E-CL
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULTGZ-TRCH-E-CL
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltgzTrchECl().getPcoDtUltgzTrchEClNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULTGZ-TRCH-E-CL
            ws.getIndParamComp().setDtUltgzTrchECl(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULTGZ-TRCH-E-CL
            ws.getIndParamComp().setDtUltgzTrchECl(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULTSC-ELAB-CL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULTSC-ELAB-CL
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULTSC-ELAB-CL
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltscElabCl().getPcoDtUltscElabClNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULTSC-ELAB-CL
            ws.getIndParamComp().setDtUltscElabCl(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULTSC-ELAB-CL
            ws.getIndParamComp().setDtUltscElabCl(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULTSC-OPZ-CL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULTSC-OPZ-CL
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULTSC-OPZ-CL
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltscOpzCl().getPcoDtUltscOpzClNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULTSC-OPZ-CL
            ws.getIndParamComp().setDtUltscOpzCl(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULTSC-OPZ-CL
            ws.getIndParamComp().setDtUltscOpzCl(((short)0));
        }
        // COB_CODE: IF PCO-STST-X-REGIONE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-STST-X-REGIONE
        //           ELSE
        //              MOVE 0 TO IND-PCO-STST-X-REGIONE
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoStstXRegione().getPcoStstXRegioneNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-STST-X-REGIONE
            ws.getIndParamComp().setStstXRegione(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-STST-X-REGIONE
            ws.getIndParamComp().setStstXRegione(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULTGZ-CED-COLL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULTGZ-CED-COLL
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULTGZ-CED-COLL
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltgzCedColl().getPcoDtUltgzCedCollNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULTGZ-CED-COLL
            ws.getIndParamComp().setDtUltgzCedColl(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULTGZ-CED-COLL
            ws.getIndParamComp().setDtUltgzCedColl(((short)0));
        }
        // COB_CODE: IF PCO-NUM-MM-CALC-MORA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-NUM-MM-CALC-MORA
        //           ELSE
        //              MOVE 0 TO IND-PCO-NUM-MM-CALC-MORA
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoNumMmCalcMora().getPcoNumMmCalcMoraNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-NUM-MM-CALC-MORA
            ws.getIndParamComp().setNumMmCalcMora(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-NUM-MM-CALC-MORA
            ws.getIndParamComp().setNumMmCalcMora(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-EC-RIV-COLL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-EC-RIV-COLL
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-EC-RIV-COLL
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltEcRivColl().getPcoDtUltEcRivCollNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-EC-RIV-COLL
            ws.getIndParamComp().setDtUltEcRivColl(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-EC-RIV-COLL
            ws.getIndParamComp().setDtUltEcRivColl(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-EC-RIV-IND-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-EC-RIV-IND
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-EC-RIV-IND
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltEcRivInd().getPcoDtUltEcRivIndNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-EC-RIV-IND
            ws.getIndParamComp().setDtUltEcRivInd(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-EC-RIV-IND
            ws.getIndParamComp().setDtUltEcRivInd(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-EC-IL-COLL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-EC-IL-COLL
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-EC-IL-COLL
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltEcIlColl().getPcoDtUltEcIlCollNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-EC-IL-COLL
            ws.getIndParamComp().setDtUltEcIlColl(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-EC-IL-COLL
            ws.getIndParamComp().setDtUltEcIlColl(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-EC-IL-IND-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-EC-IL-IND
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-EC-IL-IND
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltEcIlInd().getPcoDtUltEcIlIndNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-EC-IL-IND
            ws.getIndParamComp().setDtUltEcIlInd(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-EC-IL-IND
            ws.getIndParamComp().setDtUltEcIlInd(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-EC-UL-COLL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-EC-UL-COLL
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-EC-UL-COLL
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltEcUlColl().getPcoDtUltEcUlCollNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-EC-UL-COLL
            ws.getIndParamComp().setDtUltEcUlColl(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-EC-UL-COLL
            ws.getIndParamComp().setDtUltEcUlColl(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-EC-UL-IND-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-EC-UL-IND
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-EC-UL-IND
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltEcUlInd().getPcoDtUltEcUlIndNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-EC-UL-IND
            ws.getIndParamComp().setDtUltEcUlInd(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-EC-UL-IND
            ws.getIndParamComp().setDtUltEcUlInd(((short)0));
        }
        // COB_CODE: IF PCO-AA-UTI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-AA-UTI
        //           ELSE
        //              MOVE 0 TO IND-PCO-AA-UTI
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoAaUti().getPcoAaUtiNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-AA-UTI
            ws.getIndParamComp().setAaUti(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-AA-UTI
            ws.getIndParamComp().setAaUti(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-BOLL-PERF-C-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-BOLL-PERF-C
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-BOLL-PERF-C
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltBollPerfC().getPcoDtUltBollPerfCNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-BOLL-PERF-C
            ws.getIndParamComp().setDtUltBollPerfC(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-BOLL-PERF-C
            ws.getIndParamComp().setDtUltBollPerfC(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-BOLL-RSP-IN-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-BOLL-RSP-IN
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-BOLL-RSP-IN
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltBollRspIn().getPcoDtUltBollRspInNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-BOLL-RSP-IN
            ws.getIndParamComp().setDtUltBollRspIn(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-BOLL-RSP-IN
            ws.getIndParamComp().setDtUltBollRspIn(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-BOLL-RSP-CL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-BOLL-RSP-CL
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-BOLL-RSP-CL
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltBollRspCl().getPcoDtUltBollRspClNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-BOLL-RSP-CL
            ws.getIndParamComp().setDtUltBollRspCl(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-BOLL-RSP-CL
            ws.getIndParamComp().setDtUltBollRspCl(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-BOLL-EMES-I-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-BOLL-EMES-I
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-BOLL-EMES-I
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltBollEmesI().getPcoDtUltBollEmesINullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-BOLL-EMES-I
            ws.getIndParamComp().setDtUltBollEmesI(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-BOLL-EMES-I
            ws.getIndParamComp().setDtUltBollEmesI(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-BOLL-STOR-I-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-BOLL-STOR-I
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-BOLL-STOR-I
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltBollStorI().getPcoDtUltBollStorINullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-BOLL-STOR-I
            ws.getIndParamComp().setDtUltBollStorI(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-BOLL-STOR-I
            ws.getIndParamComp().setDtUltBollStorI(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-BOLL-RIAT-I-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-BOLL-RIAT-I
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-BOLL-RIAT-I
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltBollRiatI().getPcoDtUltBollRiatINullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-BOLL-RIAT-I
            ws.getIndParamComp().setDtUltBollRiatI(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-BOLL-RIAT-I
            ws.getIndParamComp().setDtUltBollRiatI(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-BOLL-SD-I-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-BOLL-SD-I
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-BOLL-SD-I
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltBollSdI().getPcoDtUltBollSdINullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-BOLL-SD-I
            ws.getIndParamComp().setDtUltBollSdI(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-BOLL-SD-I
            ws.getIndParamComp().setDtUltBollSdI(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-BOLL-SDNL-I-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-BOLL-SDNL-I
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-BOLL-SDNL-I
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltBollSdnlI().getPcoDtUltBollSdnlINullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-BOLL-SDNL-I
            ws.getIndParamComp().setDtUltBollSdnlI(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-BOLL-SDNL-I
            ws.getIndParamComp().setDtUltBollSdnlI(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-BOLL-PERF-I-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-BOLL-PERF-I
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-BOLL-PERF-I
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltBollPerfI().getPcoDtUltBollPerfINullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-BOLL-PERF-I
            ws.getIndParamComp().setDtUltBollPerfI(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-BOLL-PERF-I
            ws.getIndParamComp().setDtUltBollPerfI(((short)0));
        }
        // COB_CODE: IF PCO-DT-RICL-RIRIAS-COM-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-RICL-RIRIAS-COM
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-RICL-RIRIAS-COM
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtRiclRiriasCom().getPcoDtRiclRiriasComNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-RICL-RIRIAS-COM
            ws.getIndParamComp().setDtRiclRiriasCom(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-RICL-RIRIAS-COM
            ws.getIndParamComp().setDtRiclRiriasCom(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-ELAB-AT92-C-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-ELAB-AT92-C
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-ELAB-AT92-C
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltElabAt92C().getPcoDtUltElabAt92CNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-ELAB-AT92-C
            ws.getIndParamComp().setDtUltElabAt92C(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-ELAB-AT92-C
            ws.getIndParamComp().setDtUltElabAt92C(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-ELAB-AT92-I-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-ELAB-AT92-I
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-ELAB-AT92-I
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltElabAt92I().getPcoDtUltElabAt92INullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-ELAB-AT92-I
            ws.getIndParamComp().setDtUltElabAt92I(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-ELAB-AT92-I
            ws.getIndParamComp().setDtUltElabAt92I(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-ELAB-AT93-C-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-ELAB-AT93-C
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-ELAB-AT93-C
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltElabAt93C().getPcoDtUltElabAt93CNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-ELAB-AT93-C
            ws.getIndParamComp().setDtUltElabAt93C(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-ELAB-AT93-C
            ws.getIndParamComp().setDtUltElabAt93C(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-ELAB-AT93-I-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-ELAB-AT93-I
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-ELAB-AT93-I
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltElabAt93I().getPcoDtUltElabAt93INullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-ELAB-AT93-I
            ws.getIndParamComp().setDtUltElabAt93I(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-ELAB-AT93-I
            ws.getIndParamComp().setDtUltElabAt93I(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-ELAB-SPE-IN-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-ELAB-SPE-IN
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-ELAB-SPE-IN
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltElabSpeIn().getPcoDtUltElabSpeInNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-ELAB-SPE-IN
            ws.getIndParamComp().setDtUltElabSpeIn(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-ELAB-SPE-IN
            ws.getIndParamComp().setDtUltElabSpeIn(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-ELAB-PR-CON-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-ELAB-PR-CON
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-ELAB-PR-CON
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltElabPrCon().getPcoDtUltElabPrConNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-ELAB-PR-CON
            ws.getIndParamComp().setDtUltElabPrCon(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-ELAB-PR-CON
            ws.getIndParamComp().setDtUltElabPrCon(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-BOLL-RP-CL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-BOLL-RP-CL
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-BOLL-RP-CL
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltBollRpCl().getPcoDtUltBollRpClNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-BOLL-RP-CL
            ws.getIndParamComp().setDtUltBollRpCl(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-BOLL-RP-CL
            ws.getIndParamComp().setDtUltBollRpCl(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-BOLL-RP-IN-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-BOLL-RP-IN
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-BOLL-RP-IN
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltBollRpIn().getPcoDtUltBollRpInNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-BOLL-RP-IN
            ws.getIndParamComp().setDtUltBollRpIn(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-BOLL-RP-IN
            ws.getIndParamComp().setDtUltBollRpIn(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-BOLL-PRE-I-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-BOLL-PRE-I
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-BOLL-PRE-I
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltBollPreI().getPcoDtUltBollPreINullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-BOLL-PRE-I
            ws.getIndParamComp().setDtUltBollPreI(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-BOLL-PRE-I
            ws.getIndParamComp().setDtUltBollPreI(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-BOLL-PRE-C-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-BOLL-PRE-C
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-BOLL-PRE-C
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltBollPreC().getPcoDtUltBollPreCNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-BOLL-PRE-C
            ws.getIndParamComp().setDtUltBollPreC(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-BOLL-PRE-C
            ws.getIndParamComp().setDtUltBollPreC(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULTC-PILDI-MM-C-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULTC-PILDI-MM-C
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULTC-PILDI-MM-C
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltcPildiMmC().getPcoDtUltcPildiMmCNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULTC-PILDI-MM-C
            ws.getIndParamComp().setDtUltcPildiMmC(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULTC-PILDI-MM-C
            ws.getIndParamComp().setDtUltcPildiMmC(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULTC-PILDI-AA-C-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULTC-PILDI-AA-C
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULTC-PILDI-AA-C
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltcPildiAaC().getPcoDtUltcPildiAaCNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULTC-PILDI-AA-C
            ws.getIndParamComp().setDtUltcPildiAaC(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULTC-PILDI-AA-C
            ws.getIndParamComp().setDtUltcPildiAaC(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULTC-PILDI-MM-I-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULTC-PILDI-MM-I
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULTC-PILDI-MM-I
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltcPildiMmI().getPcoDtUltcPildiMmINullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULTC-PILDI-MM-I
            ws.getIndParamComp().setDtUltcPildiMmI(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULTC-PILDI-MM-I
            ws.getIndParamComp().setDtUltcPildiMmI(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULTC-PILDI-TR-I-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULTC-PILDI-TR-I
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULTC-PILDI-TR-I
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltcPildiTrI().getPcoDtUltcPildiTrINullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULTC-PILDI-TR-I
            ws.getIndParamComp().setDtUltcPildiTrI(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULTC-PILDI-TR-I
            ws.getIndParamComp().setDtUltcPildiTrI(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULTC-PILDI-AA-I-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULTC-PILDI-AA-I
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULTC-PILDI-AA-I
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltcPildiAaI().getPcoDtUltcPildiAaINullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULTC-PILDI-AA-I
            ws.getIndParamComp().setDtUltcPildiAaI(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULTC-PILDI-AA-I
            ws.getIndParamComp().setDtUltcPildiAaI(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-BOLL-QUIE-C-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-BOLL-QUIE-C
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-BOLL-QUIE-C
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltBollQuieC().getPcoDtUltBollQuieCNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-BOLL-QUIE-C
            ws.getIndParamComp().setDtUltBollQuieC(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-BOLL-QUIE-C
            ws.getIndParamComp().setDtUltBollQuieC(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-BOLL-QUIE-I-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-BOLL-QUIE-I
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-BOLL-QUIE-I
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltBollQuieI().getPcoDtUltBollQuieINullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-BOLL-QUIE-I
            ws.getIndParamComp().setDtUltBollQuieI(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-BOLL-QUIE-I
            ws.getIndParamComp().setDtUltBollQuieI(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-BOLL-COTR-I-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-BOLL-COTR-I
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-BOLL-COTR-I
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltBollCotrI().getPcoDtUltBollCotrINullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-BOLL-COTR-I
            ws.getIndParamComp().setDtUltBollCotrI(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-BOLL-COTR-I
            ws.getIndParamComp().setDtUltBollCotrI(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-BOLL-COTR-C-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-BOLL-COTR-C
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-BOLL-COTR-C
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltBollCotrC().getPcoDtUltBollCotrCNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-BOLL-COTR-C
            ws.getIndParamComp().setDtUltBollCotrC(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-BOLL-COTR-C
            ws.getIndParamComp().setDtUltBollCotrC(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-BOLL-CORI-C-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-BOLL-CORI-C
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-BOLL-CORI-C
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltBollCoriC().getPcoDtUltBollCoriCNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-BOLL-CORI-C
            ws.getIndParamComp().setDtUltBollCoriC(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-BOLL-CORI-C
            ws.getIndParamComp().setDtUltBollCoriC(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-BOLL-CORI-I-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-BOLL-CORI-I
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-BOLL-CORI-I
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltBollCoriI().getPcoDtUltBollCoriINullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-BOLL-CORI-I
            ws.getIndParamComp().setDtUltBollCoriI(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-BOLL-CORI-I
            ws.getIndParamComp().setDtUltBollCoriI(((short)0));
        }
        // COB_CODE: IF PCO-TP-VALZZ-DT-VLT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-TP-VALZZ-DT-VLT
        //           ELSE
        //              MOVE 0 TO IND-PCO-TP-VALZZ-DT-VLT
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoTpValzzDtVltFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-TP-VALZZ-DT-VLT
            ws.getIndParamComp().setTpValzzDtVlt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-TP-VALZZ-DT-VLT
            ws.getIndParamComp().setTpValzzDtVlt(((short)0));
        }
        // COB_CODE: IF PCO-FL-FRAZ-PROV-ACQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-FL-FRAZ-PROV-ACQ
        //           ELSE
        //              MOVE 0 TO IND-PCO-FL-FRAZ-PROV-ACQ
        //           END-IF
        if (Conditions.eq(paramComp.getPcoFlFrazProvAcq(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-PCO-FL-FRAZ-PROV-ACQ
            ws.getIndParamComp().setFlFrazProvAcq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-FL-FRAZ-PROV-ACQ
            ws.getIndParamComp().setFlFrazProvAcq(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-AGG-EROG-RE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-AGG-EROG-RE
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-AGG-EROG-RE
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltAggErogRe().getPcoDtUltAggErogReNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-AGG-EROG-RE
            ws.getIndParamComp().setDtUltAggErogRe(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-AGG-EROG-RE
            ws.getIndParamComp().setDtUltAggErogRe(((short)0));
        }
        // COB_CODE: IF PCO-PC-RM-MARSOL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-PC-RM-MARSOL
        //           ELSE
        //              MOVE 0 TO IND-PCO-PC-RM-MARSOL
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoPcRmMarsol().getPcoPcRmMarsolNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-PC-RM-MARSOL
            ws.getIndParamComp().setPcRmMarsol(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-PC-RM-MARSOL
            ws.getIndParamComp().setPcRmMarsol(((short)0));
        }
        // COB_CODE: IF PCO-PC-C-SUBRSH-MARSOL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-PC-C-SUBRSH-MARSOL
        //           ELSE
        //              MOVE 0 TO IND-PCO-PC-C-SUBRSH-MARSOL
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoPcCSubrshMarsol().getPcoPcCSubrshMarsolNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-PC-C-SUBRSH-MARSOL
            ws.getIndParamComp().setPcCSubrshMarsol(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-PC-C-SUBRSH-MARSOL
            ws.getIndParamComp().setPcCSubrshMarsol(((short)0));
        }
        // COB_CODE: IF PCO-COD-COMP-ISVAP-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-COD-COMP-ISVAP
        //           ELSE
        //              MOVE 0 TO IND-PCO-COD-COMP-ISVAP
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoCodCompIsvapFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-COD-COMP-ISVAP
            ws.getIndParamComp().setCodCompIsvap(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-COD-COMP-ISVAP
            ws.getIndParamComp().setCodCompIsvap(((short)0));
        }
        // COB_CODE: IF PCO-LM-RIS-CON-INT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-LM-RIS-CON-INT
        //           ELSE
        //              MOVE 0 TO IND-PCO-LM-RIS-CON-INT
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoLmRisConInt().getPcoLmRisConIntNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-LM-RIS-CON-INT
            ws.getIndParamComp().setLmRisConInt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-LM-RIS-CON-INT
            ws.getIndParamComp().setLmRisConInt(((short)0));
        }
        // COB_CODE: IF PCO-LM-C-SUBRSH-CON-IN-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-LM-C-SUBRSH-CON-IN
        //           ELSE
        //              MOVE 0 TO IND-PCO-LM-C-SUBRSH-CON-IN
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoLmCSubrshConIn().getPcoLmCSubrshConInNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-LM-C-SUBRSH-CON-IN
            ws.getIndParamComp().setLmCSubrshConIn(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-LM-C-SUBRSH-CON-IN
            ws.getIndParamComp().setLmCSubrshConIn(((short)0));
        }
        // COB_CODE: IF PCO-PC-GAR-NORISK-MARS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-PC-GAR-NORISK-MARS
        //           ELSE
        //              MOVE 0 TO IND-PCO-PC-GAR-NORISK-MARS
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoPcGarNoriskMars().getPcoPcGarNoriskMarsNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-PC-GAR-NORISK-MARS
            ws.getIndParamComp().setPcGarNoriskMars(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-PC-GAR-NORISK-MARS
            ws.getIndParamComp().setPcGarNoriskMars(((short)0));
        }
        // COB_CODE: IF PCO-CRZ-1A-RAT-INTR-PR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-CRZ-1A-RAT-INTR-PR
        //           ELSE
        //              MOVE 0 TO IND-PCO-CRZ-1A-RAT-INTR-PR
        //           END-IF
        if (Conditions.eq(paramComp.getPcoCrz1aRatIntrPr(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-PCO-CRZ-1A-RAT-INTR-PR
            ws.getIndParamComp().setCrz1aRatIntrPr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-CRZ-1A-RAT-INTR-PR
            ws.getIndParamComp().setCrz1aRatIntrPr(((short)0));
        }
        // COB_CODE: IF PCO-NUM-GG-ARR-INTR-PR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-NUM-GG-ARR-INTR-PR
        //           ELSE
        //              MOVE 0 TO IND-PCO-NUM-GG-ARR-INTR-PR
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoNumGgArrIntrPr().getPcoNumGgArrIntrPrNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-NUM-GG-ARR-INTR-PR
            ws.getIndParamComp().setNumGgArrIntrPr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-NUM-GG-ARR-INTR-PR
            ws.getIndParamComp().setNumGgArrIntrPr(((short)0));
        }
        // COB_CODE: IF PCO-FL-VISUAL-VINPG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-FL-VISUAL-VINPG
        //           ELSE
        //              MOVE 0 TO IND-PCO-FL-VISUAL-VINPG
        //           END-IF
        if (Conditions.eq(paramComp.getPcoFlVisualVinpg(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-PCO-FL-VISUAL-VINPG
            ws.getIndParamComp().setFlVisualVinpg(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-FL-VISUAL-VINPG
            ws.getIndParamComp().setFlVisualVinpg(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-ESTRAZ-FUG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-ESTRAZ-FUG
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-ESTRAZ-FUG
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltEstrazFug().getPcoDtUltEstrazFugNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-ESTRAZ-FUG
            ws.getIndParamComp().setDtUltEstrazFug(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-ESTRAZ-FUG
            ws.getIndParamComp().setDtUltEstrazFug(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-ELAB-PR-AUT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-ELAB-PR-AUT
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-ELAB-PR-AUT
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltElabPrAut().getPcoDtUltElabPrAutNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-ELAB-PR-AUT
            ws.getIndParamComp().setDtUltElabPrAut(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-ELAB-PR-AUT
            ws.getIndParamComp().setDtUltElabPrAut(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-ELAB-COMMEF-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-ELAB-COMMEF
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-ELAB-COMMEF
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltElabCommef().getPcoDtUltElabCommefNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-ELAB-COMMEF
            ws.getIndParamComp().setDtUltElabCommef(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-ELAB-COMMEF
            ws.getIndParamComp().setDtUltElabCommef(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-ELAB-LIQMEF-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-ELAB-LIQMEF
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-ELAB-LIQMEF
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltElabLiqmef().getPcoDtUltElabLiqmefNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-ELAB-LIQMEF
            ws.getIndParamComp().setDtUltElabLiqmef(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-ELAB-LIQMEF
            ws.getIndParamComp().setDtUltElabLiqmef(((short)0));
        }
        // COB_CODE: IF PCO-COD-FISC-MEF-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-COD-FISC-MEF
        //           ELSE
        //              MOVE 0 TO IND-PCO-COD-FISC-MEF
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoCodFiscMefFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-COD-FISC-MEF
            ws.getIndParamComp().setCodFiscMef(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-COD-FISC-MEF
            ws.getIndParamComp().setCodFiscMef(((short)0));
        }
        // COB_CODE: IF PCO-IMP-ASS-SOCIALE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-IMP-ASS-SOCIALE
        //           ELSE
        //              MOVE 0 TO IND-PCO-IMP-ASS-SOCIALE
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoImpAssSociale().getPcoImpAssSocialeNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-IMP-ASS-SOCIALE
            ws.getIndParamComp().setImpAssSociale(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-IMP-ASS-SOCIALE
            ws.getIndParamComp().setImpAssSociale(((short)0));
        }
        // COB_CODE: IF PCO-MOD-COMNZ-INVST-SW-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-MOD-COMNZ-INVST-SW
        //           ELSE
        //              MOVE 0 TO IND-PCO-MOD-COMNZ-INVST-SW
        //           END-IF
        if (Conditions.eq(paramComp.getPcoModComnzInvstSw(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-PCO-MOD-COMNZ-INVST-SW
            ws.getIndParamComp().setModComnzInvstSw(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-MOD-COMNZ-INVST-SW
            ws.getIndParamComp().setModComnzInvstSw(((short)0));
        }
        // COB_CODE: IF PCO-DT-RIAT-RIASS-RSH-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-RIAT-RIASS-RSH
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-RIAT-RIASS-RSH
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtRiatRiassRsh().getPcoDtRiatRiassRshNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-RIAT-RIASS-RSH
            ws.getIndParamComp().setDtRiatRiassRsh(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-RIAT-RIASS-RSH
            ws.getIndParamComp().setDtRiatRiassRsh(((short)0));
        }
        // COB_CODE: IF PCO-DT-RIAT-RIASS-COMM-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-RIAT-RIASS-COMM
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-RIAT-RIASS-COMM
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtRiatRiassComm().getPcoDtRiatRiassCommNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-RIAT-RIASS-COMM
            ws.getIndParamComp().setDtRiatRiassComm(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-RIAT-RIASS-COMM
            ws.getIndParamComp().setDtRiatRiassComm(((short)0));
        }
        // COB_CODE: IF PCO-GG-INTR-RIT-PAG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-GG-INTR-RIT-PAG
        //           ELSE
        //              MOVE 0 TO IND-PCO-GG-INTR-RIT-PAG
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoGgIntrRitPag().getPcoGgIntrRitPagNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-GG-INTR-RIT-PAG
            ws.getIndParamComp().setGgIntrRitPag(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-GG-INTR-RIT-PAG
            ws.getIndParamComp().setGgIntrRitPag(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-RINN-TAC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-RINN-TAC
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-RINN-TAC
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltRinnTac().getPcoDtUltRinnTacNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-RINN-TAC
            ws.getIndParamComp().setDtUltRinnTac(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-RINN-TAC
            ws.getIndParamComp().setDtUltRinnTac(((short)0));
        }
        // COB_CODE: IF PCO-DESC-COMP = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DESC-COMP
        //           ELSE
        //              MOVE 0 TO IND-PCO-DESC-COMP
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDescComp(), ParamCompIdbspco0.Len.PCO_DESC_COMP)) {
            // COB_CODE: MOVE -1 TO IND-PCO-DESC-COMP
            ws.getIndParamComp().setDescComp(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DESC-COMP
            ws.getIndParamComp().setDescComp(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-EC-TCM-IND-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-EC-TCM-IND
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-EC-TCM-IND
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltEcTcmInd().getPcoDtUltEcTcmIndNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-EC-TCM-IND
            ws.getIndParamComp().setDtUltEcTcmInd(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-EC-TCM-IND
            ws.getIndParamComp().setDtUltEcTcmInd(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-EC-TCM-COLL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-EC-TCM-COLL
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-EC-TCM-COLL
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltEcTcmColl().getPcoDtUltEcTcmCollNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-EC-TCM-COLL
            ws.getIndParamComp().setDtUltEcTcmColl(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-EC-TCM-COLL
            ws.getIndParamComp().setDtUltEcTcmColl(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-EC-MRM-IND-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-EC-MRM-IND
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-EC-MRM-IND
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltEcMrmInd().getPcoDtUltEcMrmIndNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-EC-MRM-IND
            ws.getIndParamComp().setDtUltEcMrmInd(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-EC-MRM-IND
            ws.getIndParamComp().setDtUltEcMrmInd(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-EC-MRM-COLL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-EC-MRM-COLL
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-EC-MRM-COLL
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltEcMrmColl().getPcoDtUltEcMrmCollNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-EC-MRM-COLL
            ws.getIndParamComp().setDtUltEcMrmColl(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-EC-MRM-COLL
            ws.getIndParamComp().setDtUltEcMrmColl(((short)0));
        }
        // COB_CODE: IF PCO-COD-COMP-LDAP-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-COD-COMP-LDAP
        //           ELSE
        //              MOVE 0 TO IND-PCO-COD-COMP-LDAP
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoCodCompLdapFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-COD-COMP-LDAP
            ws.getIndParamComp().setCodCompLdap(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-COD-COMP-LDAP
            ws.getIndParamComp().setCodCompLdap(((short)0));
        }
        // COB_CODE: IF PCO-PC-RID-IMP-1382011-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-PC-RID-IMP-1382011
        //           ELSE
        //              MOVE 0 TO IND-PCO-PC-RID-IMP-1382011
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoPcRidImp1382011().getPcoPcRidImp1382011NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-PC-RID-IMP-1382011
            ws.getIndParamComp().setPcRidImp1382011(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-PC-RID-IMP-1382011
            ws.getIndParamComp().setPcRidImp1382011(((short)0));
        }
        // COB_CODE: IF PCO-PC-RID-IMP-662014-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-PC-RID-IMP-662014
        //           ELSE
        //              MOVE 0 TO IND-PCO-PC-RID-IMP-662014
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoPcRidImp662014().getPcoPcRidImp662014NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-PC-RID-IMP-662014
            ws.getIndParamComp().setPcRidImp662014(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-PC-RID-IMP-662014
            ws.getIndParamComp().setPcRidImp662014(((short)0));
        }
        // COB_CODE: IF PCO-SOGL-AML-PRE-UNI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-SOGL-AML-PRE-UNI
        //           ELSE
        //              MOVE 0 TO IND-PCO-SOGL-AML-PRE-UNI
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoSoglAmlPreUni().getPcoSoglAmlPreUniNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-SOGL-AML-PRE-UNI
            ws.getIndParamComp().setSoglAmlPreUni(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-SOGL-AML-PRE-UNI
            ws.getIndParamComp().setSoglAmlPreUni(((short)0));
        }
        // COB_CODE: IF PCO-SOGL-AML-PRE-PER-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-SOGL-AML-PRE-PER
        //           ELSE
        //              MOVE 0 TO IND-PCO-SOGL-AML-PRE-PER
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoSoglAmlPrePer().getPcoSoglAmlPrePerNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-SOGL-AML-PRE-PER
            ws.getIndParamComp().setSoglAmlPrePer(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-SOGL-AML-PRE-PER
            ws.getIndParamComp().setSoglAmlPrePer(((short)0));
        }
        // COB_CODE: IF PCO-COD-SOGG-FTZ-ASSTO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-COD-SOGG-FTZ-ASSTO
        //           ELSE
        //              MOVE 0 TO IND-PCO-COD-SOGG-FTZ-ASSTO
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoCodSoggFtzAsstoFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-COD-SOGG-FTZ-ASSTO
            ws.getIndParamComp().setCodSoggFtzAssto(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-COD-SOGG-FTZ-ASSTO
            ws.getIndParamComp().setCodSoggFtzAssto(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-ELAB-REDPRO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-ELAB-REDPRO
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-ELAB-REDPRO
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltElabRedpro().getPcoDtUltElabRedproNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-ELAB-REDPRO
            ws.getIndParamComp().setDtUltElabRedpro(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-ELAB-REDPRO
            ws.getIndParamComp().setDtUltElabRedpro(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-ELAB-TAKE-P-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-ELAB-TAKE-P
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-ELAB-TAKE-P
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltElabTakeP().getPcoDtUltElabTakePNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-ELAB-TAKE-P
            ws.getIndParamComp().setDtUltElabTakeP(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-ELAB-TAKE-P
            ws.getIndParamComp().setDtUltElabTakeP(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-ELAB-PASPAS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-ELAB-PASPAS
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-ELAB-PASPAS
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltElabPaspas().getPcoDtUltElabPaspasNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-ELAB-PASPAS
            ws.getIndParamComp().setDtUltElabPaspas(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-ELAB-PASPAS
            ws.getIndParamComp().setDtUltElabPaspas(((short)0));
        }
        // COB_CODE: IF PCO-SOGL-AML-PRE-SAV-R-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-SOGL-AML-PRE-SAV-R
        //           ELSE
        //              MOVE 0 TO IND-PCO-SOGL-AML-PRE-SAV-R
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoSoglAmlPreSavR().getPcoSoglAmlPreSavRNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-SOGL-AML-PRE-SAV-R
            ws.getIndParamComp().setSoglAmlPreSavR(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-SOGL-AML-PRE-SAV-R
            ws.getIndParamComp().setSoglAmlPreSavR(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-ESTR-DEC-CO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-ESTR-DEC-CO
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-ESTR-DEC-CO
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltEstrDecCo().getPcoDtUltEstrDecCoNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-ESTR-DEC-CO
            ws.getIndParamComp().setDtUltEstrDecCo(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-ESTR-DEC-CO
            ws.getIndParamComp().setDtUltEstrDecCo(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-ELAB-COS-AT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-ELAB-COS-AT
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-ELAB-COS-AT
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltElabCosAt().getPcoDtUltElabCosAtNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-ELAB-COS-AT
            ws.getIndParamComp().setDtUltElabCosAt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-ELAB-COS-AT
            ws.getIndParamComp().setDtUltElabCosAt(((short)0));
        }
        // COB_CODE: IF PCO-FRQ-COSTI-ATT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-FRQ-COSTI-ATT
        //           ELSE
        //              MOVE 0 TO IND-PCO-FRQ-COSTI-ATT
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoFrqCostiAtt().getPcoFrqCostiAttNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-FRQ-COSTI-ATT
            ws.getIndParamComp().setFrqCostiAtt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-FRQ-COSTI-ATT
            ws.getIndParamComp().setFrqCostiAtt(((short)0));
        }
        // COB_CODE: IF PCO-DT-ULT-ELAB-COS-ST-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ULT-ELAB-COS-ST
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ULT-ELAB-COS-ST
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtUltElabCosSt().getPcoDtUltElabCosStNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ULT-ELAB-COS-ST
            ws.getIndParamComp().setDtUltElabCosSt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ULT-ELAB-COS-ST
            ws.getIndParamComp().setDtUltElabCosSt(((short)0));
        }
        // COB_CODE: IF PCO-FRQ-COSTI-STORNATI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-FRQ-COSTI-STORNATI
        //           ELSE
        //              MOVE 0 TO IND-PCO-FRQ-COSTI-STORNATI
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoFrqCostiStornati().getPcoFrqCostiStornatiNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-FRQ-COSTI-STORNATI
            ws.getIndParamComp().setFrqCostiStornati(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-FRQ-COSTI-STORNATI
            ws.getIndParamComp().setFrqCostiStornati(((short)0));
        }
        // COB_CODE: IF PCO-DT-ESTR-ASS-MIN70A-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ESTR-ASS-MIN70A
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ESTR-ASS-MIN70A
        //           END-IF
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtEstrAssMin70a().getPcoDtEstrAssMin70aNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ESTR-ASS-MIN70A
            ws.getIndParamComp().setDtEstrAssMin70a(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ESTR-ASS-MIN70A
            ws.getIndParamComp().setDtEstrAssMin70a(((short)0));
        }
        // COB_CODE: IF PCO-DT-ESTR-ASS-MAG70A-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-PCO-DT-ESTR-ASS-MAG70A
        //           ELSE
        //              MOVE 0 TO IND-PCO-DT-ESTR-ASS-MAG70A
        //           END-IF.
        if (Characters.EQ_HIGH.test(paramComp.getPcoDtEstrAssMag70a().getPcoDtEstrAssMag70aNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-PCO-DT-ESTR-ASS-MAG70A
            ws.getIndParamComp().setDtEstrAssMag70a(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-PCO-DT-ESTR-ASS-MAG70A
            ws.getIndParamComp().setDtEstrAssMag70a(((short)0));
        }
    }

    /**Original name: Z400-SEQ-RIGA<br>*/
    private void z400SeqRiga() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z500-AGGIORNAMENTO-STORICO<br>*/
    private void z500AggiornamentoStorico() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z550-AGG-STORICO-SOLO-INS<br>*/
    private void z550AggStoricoSoloIns() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z900-CONVERTI-N-TO-X<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da 9(8) comp-3 a date
	 * ----</pre>*/
    private void z900ConvertiNToX() {
        // COB_CODE: IF IND-PCO-DT-CONT = 0
        //               MOVE WS-DATE-X      TO PCO-DT-CONT-DB
        //           END-IF
        if (ws.getIndParamComp().getDtCont() == 0) {
            // COB_CODE: MOVE PCO-DT-CONT TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtCont().getPcoDtCont(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-CONT-DB
            ws.getParamCompDb().setDtContDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-RIVAL-IN = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-RIVAL-IN-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltRivalIn() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-RIVAL-IN TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltRivalIn().getPcoDtUltRivalIn(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-RIVAL-IN-DB
            ws.getParamCompDb().setDtUltRivalInDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-QTZO-IN = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-QTZO-IN-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltQtzoIn() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-QTZO-IN TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltQtzoIn().getPcoDtUltQtzoIn(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-QTZO-IN-DB
            ws.getParamCompDb().setDtUltQtzoInDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-RICL-RIASS = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-RICL-RIASS-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltRiclRiass() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-RICL-RIASS TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltRiclRiass().getPcoDtUltRiclRiass(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-RICL-RIASS-DB
            ws.getParamCompDb().setDtUltRiclRiassDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-TABUL-RIASS = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-TABUL-RIASS-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltTabulRiass() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-TABUL-RIASS TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltTabulRiass().getPcoDtUltTabulRiass(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-TABUL-RIASS-DB
            ws.getParamCompDb().setDtUltTabulRiassDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-EMES = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-EMES-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollEmes() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-EMES TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltBollEmes().getPcoDtUltBollEmes(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-EMES-DB
            ws.getParamCompDb().setDtUltBollEmesDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-STOR = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-STOR-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollStor() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-STOR TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltBollStor().getPcoDtUltBollStor(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-STOR-DB
            ws.getParamCompDb().setDtUltBollStorDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-LIQ = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-LIQ-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollLiq() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-LIQ TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltBollLiq().getPcoDtUltBollLiq(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-LIQ-DB
            ws.getParamCompDb().setDtUltBollLiqDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-RIAT = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-RIAT-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollRiat() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-RIAT TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltBollRiat().getPcoDtUltBollRiat(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-RIAT-DB
            ws.getParamCompDb().setDtUltBollRiatDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULTELRISCPAR-PR = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULTELRISCPAR-PR-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltelriscparPr() == 0) {
            // COB_CODE: MOVE PCO-DT-ULTELRISCPAR-PR TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltelriscparPr().getPcoDtUltelriscparPr(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULTELRISCPAR-PR-DB
            ws.getParamCompDb().setDtUltelriscparPrDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULTC-IS-IN = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULTC-IS-IN-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltcIsIn() == 0) {
            // COB_CODE: MOVE PCO-DT-ULTC-IS-IN TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltcIsIn().getPcoDtUltcIsIn(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULTC-IS-IN-DB
            ws.getParamCompDb().setDtUltcIsInDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-RICL-PRE = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-RICL-PRE-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltRiclPre() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-RICL-PRE TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltRiclPre().getPcoDtUltRiclPre(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-RICL-PRE-DB
            ws.getParamCompDb().setDtUltRiclPreDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULTC-MARSOL = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULTC-MARSOL-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltcMarsol() == 0) {
            // COB_CODE: MOVE PCO-DT-ULTC-MARSOL TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltcMarsol().getPcoDtUltcMarsol(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULTC-MARSOL-DB
            ws.getParamCompDb().setDtUltcMarsolDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULTC-RB-IN = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULTC-RB-IN-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltcRbIn() == 0) {
            // COB_CODE: MOVE PCO-DT-ULTC-RB-IN TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltcRbIn().getPcoDtUltcRbIn(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULTC-RB-IN-DB
            ws.getParamCompDb().setDtUltcRbInDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULTGZ-TRCH-E-IN = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULTGZ-TRCH-E-IN-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltgzTrchEIn() == 0) {
            // COB_CODE: MOVE PCO-DT-ULTGZ-TRCH-E-IN TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltgzTrchEIn().getPcoDtUltgzTrchEIn(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULTGZ-TRCH-E-IN-DB
            ws.getParamCompDb().setDtUltgzTrchEInDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-SNDEN = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-SNDEN-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollSnden() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-SNDEN TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltBollSnden().getPcoDtUltBollSnden(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-SNDEN-DB
            ws.getParamCompDb().setDtUltBollSndenDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-SNDNLQ = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-SNDNLQ-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollSndnlq() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-SNDNLQ TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltBollSndnlq().getPcoDtUltBollSndnlq(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-SNDNLQ-DB
            ws.getParamCompDb().setDtUltBollSndnlqDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULTSC-ELAB-IN = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULTSC-ELAB-IN-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltscElabIn() == 0) {
            // COB_CODE: MOVE PCO-DT-ULTSC-ELAB-IN TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltscElabIn().getPcoDtUltscElabIn(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULTSC-ELAB-IN-DB
            ws.getParamCompDb().setDtUltscElabInDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULTSC-OPZ-IN = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULTSC-OPZ-IN-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltscOpzIn() == 0) {
            // COB_CODE: MOVE PCO-DT-ULTSC-OPZ-IN TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltscOpzIn().getPcoDtUltscOpzIn(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULTSC-OPZ-IN-DB
            ws.getParamCompDb().setDtUltscOpzInDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULTC-BNSRIC-IN = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULTC-BNSRIC-IN-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltcBnsricIn() == 0) {
            // COB_CODE: MOVE PCO-DT-ULTC-BNSRIC-IN TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltcBnsricIn().getPcoDtUltcBnsricIn(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULTC-BNSRIC-IN-DB
            ws.getParamCompDb().setDtUltcBnsricInDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULTC-BNSFDT-IN = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULTC-BNSFDT-IN-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltcBnsfdtIn() == 0) {
            // COB_CODE: MOVE PCO-DT-ULTC-BNSFDT-IN TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltcBnsfdtIn().getPcoDtUltcBnsfdtIn(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULTC-BNSFDT-IN-DB
            ws.getParamCompDb().setDtUltcBnsfdtInDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-RINN-GARAC = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-RINN-GARAC-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltRinnGarac() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-RINN-GARAC TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltRinnGarac().getPcoDtUltRinnGarac(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-RINN-GARAC-DB
            ws.getParamCompDb().setDtUltRinnGaracDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULTGZ-CED = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULTGZ-CED-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltgzCed() == 0) {
            // COB_CODE: MOVE PCO-DT-ULTGZ-CED TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltgzCed().getPcoDtUltgzCed(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULTGZ-CED-DB
            ws.getParamCompDb().setDtUltgzCedDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-ELAB-PRLCOS = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-ELAB-PRLCOS-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltElabPrlcos() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-PRLCOS TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltElabPrlcos().getPcoDtUltElabPrlcos(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-ELAB-PRLCOS-DB
            ws.getParamCompDb().setDtUltElabPrlcosDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-RINN-COLL = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-RINN-COLL-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltRinnColl() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-RINN-COLL TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltRinnColl().getPcoDtUltRinnColl(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-RINN-COLL-DB
            ws.getParamCompDb().setDtUltRinnCollDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-RIVAL-CL = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-RIVAL-CL-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltRivalCl() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-RIVAL-CL TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltRivalCl().getPcoDtUltRivalCl(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-RIVAL-CL-DB
            ws.getParamCompDb().setDtUltRivalClDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-QTZO-CL = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-QTZO-CL-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltQtzoCl() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-QTZO-CL TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltQtzoCl().getPcoDtUltQtzoCl(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-QTZO-CL-DB
            ws.getParamCompDb().setDtUltQtzoClDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULTC-BNSRIC-CL = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULTC-BNSRIC-CL-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltcBnsricCl() == 0) {
            // COB_CODE: MOVE PCO-DT-ULTC-BNSRIC-CL TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltcBnsricCl().getPcoDtUltcBnsricCl(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULTC-BNSRIC-CL-DB
            ws.getParamCompDb().setDtUltcBnsricClDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULTC-BNSFDT-CL = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULTC-BNSFDT-CL-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltcBnsfdtCl() == 0) {
            // COB_CODE: MOVE PCO-DT-ULTC-BNSFDT-CL TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltcBnsfdtCl().getPcoDtUltcBnsfdtCl(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULTC-BNSFDT-CL-DB
            ws.getParamCompDb().setDtUltcBnsfdtClDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULTC-IS-CL = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULTC-IS-CL-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltcIsCl() == 0) {
            // COB_CODE: MOVE PCO-DT-ULTC-IS-CL TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltcIsCl().getPcoDtUltcIsCl(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULTC-IS-CL-DB
            ws.getParamCompDb().setDtUltcIsClDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULTC-RB-CL = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULTC-RB-CL-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltcRbCl() == 0) {
            // COB_CODE: MOVE PCO-DT-ULTC-RB-CL TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltcRbCl().getPcoDtUltcRbCl(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULTC-RB-CL-DB
            ws.getParamCompDb().setDtUltcRbClDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULTGZ-TRCH-E-CL = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULTGZ-TRCH-E-CL-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltgzTrchECl() == 0) {
            // COB_CODE: MOVE PCO-DT-ULTGZ-TRCH-E-CL TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltgzTrchECl().getPcoDtUltgzTrchECl(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULTGZ-TRCH-E-CL-DB
            ws.getParamCompDb().setDtUltgzTrchEClDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULTSC-ELAB-CL = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULTSC-ELAB-CL-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltscElabCl() == 0) {
            // COB_CODE: MOVE PCO-DT-ULTSC-ELAB-CL TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltscElabCl().getPcoDtUltscElabCl(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULTSC-ELAB-CL-DB
            ws.getParamCompDb().setDtUltscElabClDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULTSC-OPZ-CL = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULTSC-OPZ-CL-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltscOpzCl() == 0) {
            // COB_CODE: MOVE PCO-DT-ULTSC-OPZ-CL TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltscOpzCl().getPcoDtUltscOpzCl(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULTSC-OPZ-CL-DB
            ws.getParamCompDb().setDtUltscOpzClDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-STST-X-REGIONE = 0
        //               MOVE WS-DATE-X      TO PCO-STST-X-REGIONE-DB
        //           END-IF
        if (ws.getIndParamComp().getStstXRegione() == 0) {
            // COB_CODE: MOVE PCO-STST-X-REGIONE TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoStstXRegione().getPcoStstXRegione(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-STST-X-REGIONE-DB
            ws.getParamCompDb().setStstXRegioneDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULTGZ-CED-COLL = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULTGZ-CED-COLL-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltgzCedColl() == 0) {
            // COB_CODE: MOVE PCO-DT-ULTGZ-CED-COLL TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltgzCedColl().getPcoDtUltgzCedColl(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULTGZ-CED-COLL-DB
            ws.getParamCompDb().setDtUltgzCedCollDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-EC-RIV-COLL = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-EC-RIV-COLL-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltEcRivColl() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-EC-RIV-COLL TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltEcRivColl().getPcoDtUltEcRivColl(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-EC-RIV-COLL-DB
            ws.getParamCompDb().setDtUltEcRivCollDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-EC-RIV-IND = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-EC-RIV-IND-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltEcRivInd() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-EC-RIV-IND TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltEcRivInd().getPcoDtUltEcRivInd(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-EC-RIV-IND-DB
            ws.getParamCompDb().setDtUltEcRivIndDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-EC-IL-COLL = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-EC-IL-COLL-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltEcIlColl() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-EC-IL-COLL TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltEcIlColl().getPcoDtUltEcIlColl(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-EC-IL-COLL-DB
            ws.getParamCompDb().setDtUltEcIlCollDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-EC-IL-IND = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-EC-IL-IND-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltEcIlInd() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-EC-IL-IND TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltEcIlInd().getPcoDtUltEcIlInd(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-EC-IL-IND-DB
            ws.getParamCompDb().setDtUltEcIlIndDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-EC-UL-COLL = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-EC-UL-COLL-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltEcUlColl() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-EC-UL-COLL TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltEcUlColl().getPcoDtUltEcUlColl(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-EC-UL-COLL-DB
            ws.getParamCompDb().setDtUltEcUlCollDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-EC-UL-IND = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-EC-UL-IND-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltEcUlInd() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-EC-UL-IND TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltEcUlInd().getPcoDtUltEcUlInd(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-EC-UL-IND-DB
            ws.getParamCompDb().setDtUltEcUlIndDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-PERF-C = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-PERF-C-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollPerfC() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-PERF-C TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltBollPerfC().getPcoDtUltBollPerfC(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-PERF-C-DB
            ws.getParamCompDb().setDtUltBollPerfCDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-RSP-IN = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-RSP-IN-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollRspIn() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-RSP-IN TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltBollRspIn().getPcoDtUltBollRspIn(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-RSP-IN-DB
            ws.getParamCompDb().setDtUltBollRspInDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-RSP-CL = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-RSP-CL-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollRspCl() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-RSP-CL TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltBollRspCl().getPcoDtUltBollRspCl(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-RSP-CL-DB
            ws.getParamCompDb().setDtUltBollRspClDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-EMES-I = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-EMES-I-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollEmesI() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-EMES-I TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltBollEmesI().getPcoDtUltBollEmesI(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-EMES-I-DB
            ws.getParamCompDb().setDtUltBollEmesIDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-STOR-I = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-STOR-I-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollStorI() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-STOR-I TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltBollStorI().getPcoDtUltBollStorI(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-STOR-I-DB
            ws.getParamCompDb().setDtUltBollStorIDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-RIAT-I = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-RIAT-I-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollRiatI() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-RIAT-I TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltBollRiatI().getPcoDtUltBollRiatI(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-RIAT-I-DB
            ws.getParamCompDb().setDtUltBollRiatIDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-SD-I = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-SD-I-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollSdI() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-SD-I TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltBollSdI().getPcoDtUltBollSdI(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-SD-I-DB
            ws.getParamCompDb().setDtUltBollSdIDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-SDNL-I = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-SDNL-I-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollSdnlI() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-SDNL-I TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltBollSdnlI().getPcoDtUltBollSdnlI(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-SDNL-I-DB
            ws.getParamCompDb().setDtUltBollSdnlIDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-PERF-I = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-PERF-I-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollPerfI() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-PERF-I TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltBollPerfI().getPcoDtUltBollPerfI(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-PERF-I-DB
            ws.getParamCompDb().setDtUltBollPerfIDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-RICL-RIRIAS-COM = 0
        //               MOVE WS-DATE-X      TO PCO-DT-RICL-RIRIAS-COM-DB
        //           END-IF
        if (ws.getIndParamComp().getDtRiclRiriasCom() == 0) {
            // COB_CODE: MOVE PCO-DT-RICL-RIRIAS-COM TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtRiclRiriasCom().getPcoDtRiclRiriasCom(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-RICL-RIRIAS-COM-DB
            ws.getParamCompDb().setDtRiclRiriasComDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-ELAB-AT92-C = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-ELAB-AT92-C-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltElabAt92C() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-AT92-C TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltElabAt92C().getPcoDtUltElabAt92C(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-ELAB-AT92-C-DB
            ws.getParamCompDb().setDtUltElabAt92CDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-ELAB-AT92-I = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-ELAB-AT92-I-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltElabAt92I() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-AT92-I TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltElabAt92I().getPcoDtUltElabAt92I(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-ELAB-AT92-I-DB
            ws.getParamCompDb().setDtUltElabAt92IDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-ELAB-AT93-C = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-ELAB-AT93-C-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltElabAt93C() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-AT93-C TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltElabAt93C().getPcoDtUltElabAt93C(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-ELAB-AT93-C-DB
            ws.getParamCompDb().setDtUltElabAt93CDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-ELAB-AT93-I = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-ELAB-AT93-I-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltElabAt93I() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-AT93-I TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltElabAt93I().getPcoDtUltElabAt93I(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-ELAB-AT93-I-DB
            ws.getParamCompDb().setDtUltElabAt93IDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-ELAB-SPE-IN = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-ELAB-SPE-IN-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltElabSpeIn() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-SPE-IN TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltElabSpeIn().getPcoDtUltElabSpeIn(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-ELAB-SPE-IN-DB
            ws.getParamCompDb().setDtUltElabSpeInDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-ELAB-PR-CON = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-ELAB-PR-CON-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltElabPrCon() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-PR-CON TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltElabPrCon().getPcoDtUltElabPrCon(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-ELAB-PR-CON-DB
            ws.getParamCompDb().setDtUltElabPrConDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-RP-CL = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-RP-CL-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollRpCl() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-RP-CL TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltBollRpCl().getPcoDtUltBollRpCl(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-RP-CL-DB
            ws.getParamCompDb().setDtUltBollRpClDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-RP-IN = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-RP-IN-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollRpIn() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-RP-IN TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltBollRpIn().getPcoDtUltBollRpIn(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-RP-IN-DB
            ws.getParamCompDb().setDtUltBollRpInDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-PRE-I = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-PRE-I-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollPreI() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-PRE-I TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltBollPreI().getPcoDtUltBollPreI(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-PRE-I-DB
            ws.getParamCompDb().setDtUltBollPreIDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-PRE-C = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-PRE-C-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollPreC() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-PRE-C TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltBollPreC().getPcoDtUltBollPreC(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-PRE-C-DB
            ws.getParamCompDb().setDtUltBollPreCDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULTC-PILDI-MM-C = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULTC-PILDI-MM-C-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltcPildiMmC() == 0) {
            // COB_CODE: MOVE PCO-DT-ULTC-PILDI-MM-C TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltcPildiMmC().getPcoDtUltcPildiMmC(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULTC-PILDI-MM-C-DB
            ws.getParamCompDb().setDtUltcPildiMmCDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULTC-PILDI-AA-C = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULTC-PILDI-AA-C-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltcPildiAaC() == 0) {
            // COB_CODE: MOVE PCO-DT-ULTC-PILDI-AA-C TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltcPildiAaC().getPcoDtUltcPildiAaC(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULTC-PILDI-AA-C-DB
            ws.getParamCompDb().setDtUltcPildiAaCDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULTC-PILDI-MM-I = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULTC-PILDI-MM-I-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltcPildiMmI() == 0) {
            // COB_CODE: MOVE PCO-DT-ULTC-PILDI-MM-I TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltcPildiMmI().getPcoDtUltcPildiMmI(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULTC-PILDI-MM-I-DB
            ws.getParamCompDb().setDtUltcPildiMmIDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULTC-PILDI-TR-I = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULTC-PILDI-TR-I-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltcPildiTrI() == 0) {
            // COB_CODE: MOVE PCO-DT-ULTC-PILDI-TR-I TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltcPildiTrI().getPcoDtUltcPildiTrI(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULTC-PILDI-TR-I-DB
            ws.getParamCompDb().setDtUltcPildiTrIDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULTC-PILDI-AA-I = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULTC-PILDI-AA-I-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltcPildiAaI() == 0) {
            // COB_CODE: MOVE PCO-DT-ULTC-PILDI-AA-I TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltcPildiAaI().getPcoDtUltcPildiAaI(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULTC-PILDI-AA-I-DB
            ws.getParamCompDb().setDtUltcPildiAaIDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-QUIE-C = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-QUIE-C-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollQuieC() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-QUIE-C TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltBollQuieC().getPcoDtUltBollQuieC(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-QUIE-C-DB
            ws.getParamCompDb().setDtUltBollQuieCDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-QUIE-I = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-QUIE-I-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollQuieI() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-QUIE-I TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltBollQuieI().getPcoDtUltBollQuieI(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-QUIE-I-DB
            ws.getParamCompDb().setDtUltBollQuieIDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-COTR-I = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-COTR-I-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollCotrI() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-COTR-I TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltBollCotrI().getPcoDtUltBollCotrI(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-COTR-I-DB
            ws.getParamCompDb().setDtUltBollCotrIDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-COTR-C = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-COTR-C-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollCotrC() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-COTR-C TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltBollCotrC().getPcoDtUltBollCotrC(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-COTR-C-DB
            ws.getParamCompDb().setDtUltBollCotrCDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-CORI-C = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-CORI-C-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollCoriC() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-CORI-C TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltBollCoriC().getPcoDtUltBollCoriC(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-CORI-C-DB
            ws.getParamCompDb().setDtUltBollCoriCDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-CORI-I = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-CORI-I-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollCoriI() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-CORI-I TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltBollCoriI().getPcoDtUltBollCoriI(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-BOLL-CORI-I-DB
            ws.getParamCompDb().setDtUltBollCoriIDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-AGG-EROG-RE = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-AGG-EROG-RE-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltAggErogRe() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-AGG-EROG-RE TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltAggErogRe().getPcoDtUltAggErogRe(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-AGG-EROG-RE-DB
            ws.getParamCompDb().setDtUltAggErogReDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-ESTRAZ-FUG = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-ESTRAZ-FUG-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltEstrazFug() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-ESTRAZ-FUG TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltEstrazFug().getPcoDtUltEstrazFug(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-ESTRAZ-FUG-DB
            ws.getParamCompDb().setDtUltEstrazFugDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-ELAB-PR-AUT = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-ELAB-PR-AUT-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltElabPrAut() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-PR-AUT TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltElabPrAut().getPcoDtUltElabPrAut(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-ELAB-PR-AUT-DB
            ws.getParamCompDb().setDtUltElabPrAutDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-ELAB-COMMEF = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-ELAB-COMMEF-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltElabCommef() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-COMMEF TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltElabCommef().getPcoDtUltElabCommef(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-ELAB-COMMEF-DB
            ws.getParamCompDb().setDtUltElabCommefDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-ELAB-LIQMEF = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-ELAB-LIQMEF-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltElabLiqmef() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-LIQMEF TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltElabLiqmef().getPcoDtUltElabLiqmef(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-ELAB-LIQMEF-DB
            ws.getParamCompDb().setDtUltElabLiqmefDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-RIAT-RIASS-RSH = 0
        //               MOVE WS-DATE-X      TO PCO-DT-RIAT-RIASS-RSH-DB
        //           END-IF
        if (ws.getIndParamComp().getDtRiatRiassRsh() == 0) {
            // COB_CODE: MOVE PCO-DT-RIAT-RIASS-RSH TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtRiatRiassRsh().getPcoDtRiatRiassRsh(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-RIAT-RIASS-RSH-DB
            ws.getParamCompDb().setDtRiatRiassRshDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-RIAT-RIASS-COMM = 0
        //               MOVE WS-DATE-X      TO PCO-DT-RIAT-RIASS-COMM-DB
        //           END-IF
        if (ws.getIndParamComp().getDtRiatRiassComm() == 0) {
            // COB_CODE: MOVE PCO-DT-RIAT-RIASS-COMM TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtRiatRiassComm().getPcoDtRiatRiassComm(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-RIAT-RIASS-COMM-DB
            ws.getParamCompDb().setDtRiatRiassCommDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-RINN-TAC = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-RINN-TAC-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltRinnTac() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-RINN-TAC TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltRinnTac().getPcoDtUltRinnTac(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-RINN-TAC-DB
            ws.getParamCompDb().setDtUltRinnTacDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-EC-TCM-IND = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-EC-TCM-IND-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltEcTcmInd() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-EC-TCM-IND TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltEcTcmInd().getPcoDtUltEcTcmInd(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-EC-TCM-IND-DB
            ws.getParamCompDb().setDtUltEcTcmIndDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-EC-TCM-COLL = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-EC-TCM-COLL-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltEcTcmColl() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-EC-TCM-COLL TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltEcTcmColl().getPcoDtUltEcTcmColl(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-EC-TCM-COLL-DB
            ws.getParamCompDb().setDtUltEcTcmCollDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-EC-MRM-IND = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-EC-MRM-IND-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltEcMrmInd() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-EC-MRM-IND TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltEcMrmInd().getPcoDtUltEcMrmInd(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-EC-MRM-IND-DB
            ws.getParamCompDb().setDtUltEcMrmIndDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-EC-MRM-COLL = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-EC-MRM-COLL-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltEcMrmColl() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-EC-MRM-COLL TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltEcMrmColl().getPcoDtUltEcMrmColl(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-EC-MRM-COLL-DB
            ws.getParamCompDb().setDtUltEcMrmCollDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-ELAB-REDPRO = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-ELAB-REDPRO-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltElabRedpro() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-REDPRO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltElabRedpro().getPcoDtUltElabRedpro(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-ELAB-REDPRO-DB
            ws.getParamCompDb().setDtUltElabRedproDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-ELAB-TAKE-P = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-ELAB-TAKE-P-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltElabTakeP() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-TAKE-P TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltElabTakeP().getPcoDtUltElabTakeP(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-ELAB-TAKE-P-DB
            ws.getParamCompDb().setDtUltElabTakePDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-ELAB-PASPAS = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-ELAB-PASPAS-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltElabPaspas() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-PASPAS TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltElabPaspas().getPcoDtUltElabPaspas(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-ELAB-PASPAS-DB
            ws.getParamCompDb().setDtUltElabPaspasDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-ESTR-DEC-CO = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-ESTR-DEC-CO-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltEstrDecCo() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-ESTR-DEC-CO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltEstrDecCo().getPcoDtUltEstrDecCo(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-ESTR-DEC-CO-DB
            ws.getParamCompDb().setDtUltEstrDecCoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-ELAB-COS-AT = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-ELAB-COS-AT-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltElabCosAt() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-COS-AT TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltElabCosAt().getPcoDtUltElabCosAt(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-ELAB-COS-AT-DB
            ws.getParamCompDb().setDtUltElabCosAtDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-ELAB-COS-ST = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ULT-ELAB-COS-ST-DB
        //           END-IF
        if (ws.getIndParamComp().getDtUltElabCosSt() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-COS-ST TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtUltElabCosSt().getPcoDtUltElabCosSt(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ULT-ELAB-COS-ST-DB
            ws.getParamCompDb().setDtUltElabCosStDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ESTR-ASS-MIN70A = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ESTR-ASS-MIN70A-DB
        //           END-IF
        if (ws.getIndParamComp().getDtEstrAssMin70a() == 0) {
            // COB_CODE: MOVE PCO-DT-ESTR-ASS-MIN70A TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtEstrAssMin70a().getPcoDtEstrAssMin70a(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ESTR-ASS-MIN70A-DB
            ws.getParamCompDb().setDtEstrAssMin70aDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-PCO-DT-ESTR-ASS-MAG70A = 0
        //               MOVE WS-DATE-X      TO PCO-DT-ESTR-ASS-MAG70A-DB
        //           END-IF.
        if (ws.getIndParamComp().getDtEstrAssMag70a() == 0) {
            // COB_CODE: MOVE PCO-DT-ESTR-ASS-MAG70A TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(paramComp.getPcoDtEstrAssMag70a().getPcoDtEstrAssMag70a(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO PCO-DT-ESTR-ASS-MAG70A-DB
            ws.getParamCompDb().setDtEstrAssMag70aDb(ws.getIdsv0010().getWsDateX());
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: IF IND-PCO-DT-CONT = 0
        //               MOVE WS-DATE-N      TO PCO-DT-CONT
        //           END-IF
        if (ws.getIndParamComp().getDtCont() == 0) {
            // COB_CODE: MOVE PCO-DT-CONT-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtContDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-CONT
            paramComp.getPcoDtCont().setPcoDtCont(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-RIVAL-IN = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-RIVAL-IN
        //           END-IF
        if (ws.getIndParamComp().getDtUltRivalIn() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-RIVAL-IN-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltRivalInDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-RIVAL-IN
            paramComp.getPcoDtUltRivalIn().setPcoDtUltRivalIn(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-QTZO-IN = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-QTZO-IN
        //           END-IF
        if (ws.getIndParamComp().getDtUltQtzoIn() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-QTZO-IN-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltQtzoInDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-QTZO-IN
            paramComp.getPcoDtUltQtzoIn().setPcoDtUltQtzoIn(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-RICL-RIASS = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-RICL-RIASS
        //           END-IF
        if (ws.getIndParamComp().getDtUltRiclRiass() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-RICL-RIASS-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltRiclRiassDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-RICL-RIASS
            paramComp.getPcoDtUltRiclRiass().setPcoDtUltRiclRiass(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-TABUL-RIASS = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-TABUL-RIASS
        //           END-IF
        if (ws.getIndParamComp().getDtUltTabulRiass() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-TABUL-RIASS-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltTabulRiassDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-TABUL-RIASS
            paramComp.getPcoDtUltTabulRiass().setPcoDtUltTabulRiass(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-EMES = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-EMES
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollEmes() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-EMES-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltBollEmesDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-EMES
            paramComp.getPcoDtUltBollEmes().setPcoDtUltBollEmes(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-STOR = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-STOR
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollStor() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-STOR-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltBollStorDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-STOR
            paramComp.getPcoDtUltBollStor().setPcoDtUltBollStor(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-LIQ = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-LIQ
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollLiq() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-LIQ-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltBollLiqDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-LIQ
            paramComp.getPcoDtUltBollLiq().setPcoDtUltBollLiq(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-RIAT = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-RIAT
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollRiat() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-RIAT-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltBollRiatDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-RIAT
            paramComp.getPcoDtUltBollRiat().setPcoDtUltBollRiat(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULTELRISCPAR-PR = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULTELRISCPAR-PR
        //           END-IF
        if (ws.getIndParamComp().getDtUltelriscparPr() == 0) {
            // COB_CODE: MOVE PCO-DT-ULTELRISCPAR-PR-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltelriscparPrDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULTELRISCPAR-PR
            paramComp.getPcoDtUltelriscparPr().setPcoDtUltelriscparPr(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULTC-IS-IN = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULTC-IS-IN
        //           END-IF
        if (ws.getIndParamComp().getDtUltcIsIn() == 0) {
            // COB_CODE: MOVE PCO-DT-ULTC-IS-IN-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltcIsInDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULTC-IS-IN
            paramComp.getPcoDtUltcIsIn().setPcoDtUltcIsIn(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-RICL-PRE = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-RICL-PRE
        //           END-IF
        if (ws.getIndParamComp().getDtUltRiclPre() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-RICL-PRE-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltRiclPreDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-RICL-PRE
            paramComp.getPcoDtUltRiclPre().setPcoDtUltRiclPre(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULTC-MARSOL = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULTC-MARSOL
        //           END-IF
        if (ws.getIndParamComp().getDtUltcMarsol() == 0) {
            // COB_CODE: MOVE PCO-DT-ULTC-MARSOL-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltcMarsolDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULTC-MARSOL
            paramComp.getPcoDtUltcMarsol().setPcoDtUltcMarsol(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULTC-RB-IN = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULTC-RB-IN
        //           END-IF
        if (ws.getIndParamComp().getDtUltcRbIn() == 0) {
            // COB_CODE: MOVE PCO-DT-ULTC-RB-IN-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltcRbInDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULTC-RB-IN
            paramComp.getPcoDtUltcRbIn().setPcoDtUltcRbIn(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULTGZ-TRCH-E-IN = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULTGZ-TRCH-E-IN
        //           END-IF
        if (ws.getIndParamComp().getDtUltgzTrchEIn() == 0) {
            // COB_CODE: MOVE PCO-DT-ULTGZ-TRCH-E-IN-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltgzTrchEInDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULTGZ-TRCH-E-IN
            paramComp.getPcoDtUltgzTrchEIn().setPcoDtUltgzTrchEIn(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-SNDEN = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-SNDEN
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollSnden() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-SNDEN-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltBollSndenDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-SNDEN
            paramComp.getPcoDtUltBollSnden().setPcoDtUltBollSnden(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-SNDNLQ = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-SNDNLQ
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollSndnlq() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-SNDNLQ-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltBollSndnlqDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-SNDNLQ
            paramComp.getPcoDtUltBollSndnlq().setPcoDtUltBollSndnlq(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULTSC-ELAB-IN = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULTSC-ELAB-IN
        //           END-IF
        if (ws.getIndParamComp().getDtUltscElabIn() == 0) {
            // COB_CODE: MOVE PCO-DT-ULTSC-ELAB-IN-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltscElabInDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULTSC-ELAB-IN
            paramComp.getPcoDtUltscElabIn().setPcoDtUltscElabIn(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULTSC-OPZ-IN = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULTSC-OPZ-IN
        //           END-IF
        if (ws.getIndParamComp().getDtUltscOpzIn() == 0) {
            // COB_CODE: MOVE PCO-DT-ULTSC-OPZ-IN-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltscOpzInDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULTSC-OPZ-IN
            paramComp.getPcoDtUltscOpzIn().setPcoDtUltscOpzIn(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULTC-BNSRIC-IN = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULTC-BNSRIC-IN
        //           END-IF
        if (ws.getIndParamComp().getDtUltcBnsricIn() == 0) {
            // COB_CODE: MOVE PCO-DT-ULTC-BNSRIC-IN-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltcBnsricInDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULTC-BNSRIC-IN
            paramComp.getPcoDtUltcBnsricIn().setPcoDtUltcBnsricIn(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULTC-BNSFDT-IN = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULTC-BNSFDT-IN
        //           END-IF
        if (ws.getIndParamComp().getDtUltcBnsfdtIn() == 0) {
            // COB_CODE: MOVE PCO-DT-ULTC-BNSFDT-IN-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltcBnsfdtInDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULTC-BNSFDT-IN
            paramComp.getPcoDtUltcBnsfdtIn().setPcoDtUltcBnsfdtIn(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-RINN-GARAC = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-RINN-GARAC
        //           END-IF
        if (ws.getIndParamComp().getDtUltRinnGarac() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-RINN-GARAC-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltRinnGaracDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-RINN-GARAC
            paramComp.getPcoDtUltRinnGarac().setPcoDtUltRinnGarac(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULTGZ-CED = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULTGZ-CED
        //           END-IF
        if (ws.getIndParamComp().getDtUltgzCed() == 0) {
            // COB_CODE: MOVE PCO-DT-ULTGZ-CED-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltgzCedDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULTGZ-CED
            paramComp.getPcoDtUltgzCed().setPcoDtUltgzCed(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-ELAB-PRLCOS = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-ELAB-PRLCOS
        //           END-IF
        if (ws.getIndParamComp().getDtUltElabPrlcos() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-PRLCOS-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltElabPrlcosDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-ELAB-PRLCOS
            paramComp.getPcoDtUltElabPrlcos().setPcoDtUltElabPrlcos(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-RINN-COLL = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-RINN-COLL
        //           END-IF
        if (ws.getIndParamComp().getDtUltRinnColl() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-RINN-COLL-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltRinnCollDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-RINN-COLL
            paramComp.getPcoDtUltRinnColl().setPcoDtUltRinnColl(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-RIVAL-CL = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-RIVAL-CL
        //           END-IF
        if (ws.getIndParamComp().getDtUltRivalCl() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-RIVAL-CL-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltRivalClDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-RIVAL-CL
            paramComp.getPcoDtUltRivalCl().setPcoDtUltRivalCl(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-QTZO-CL = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-QTZO-CL
        //           END-IF
        if (ws.getIndParamComp().getDtUltQtzoCl() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-QTZO-CL-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltQtzoClDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-QTZO-CL
            paramComp.getPcoDtUltQtzoCl().setPcoDtUltQtzoCl(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULTC-BNSRIC-CL = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULTC-BNSRIC-CL
        //           END-IF
        if (ws.getIndParamComp().getDtUltcBnsricCl() == 0) {
            // COB_CODE: MOVE PCO-DT-ULTC-BNSRIC-CL-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltcBnsricClDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULTC-BNSRIC-CL
            paramComp.getPcoDtUltcBnsricCl().setPcoDtUltcBnsricCl(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULTC-BNSFDT-CL = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULTC-BNSFDT-CL
        //           END-IF
        if (ws.getIndParamComp().getDtUltcBnsfdtCl() == 0) {
            // COB_CODE: MOVE PCO-DT-ULTC-BNSFDT-CL-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltcBnsfdtClDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULTC-BNSFDT-CL
            paramComp.getPcoDtUltcBnsfdtCl().setPcoDtUltcBnsfdtCl(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULTC-IS-CL = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULTC-IS-CL
        //           END-IF
        if (ws.getIndParamComp().getDtUltcIsCl() == 0) {
            // COB_CODE: MOVE PCO-DT-ULTC-IS-CL-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltcIsClDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULTC-IS-CL
            paramComp.getPcoDtUltcIsCl().setPcoDtUltcIsCl(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULTC-RB-CL = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULTC-RB-CL
        //           END-IF
        if (ws.getIndParamComp().getDtUltcRbCl() == 0) {
            // COB_CODE: MOVE PCO-DT-ULTC-RB-CL-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltcRbClDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULTC-RB-CL
            paramComp.getPcoDtUltcRbCl().setPcoDtUltcRbCl(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULTGZ-TRCH-E-CL = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULTGZ-TRCH-E-CL
        //           END-IF
        if (ws.getIndParamComp().getDtUltgzTrchECl() == 0) {
            // COB_CODE: MOVE PCO-DT-ULTGZ-TRCH-E-CL-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltgzTrchEClDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULTGZ-TRCH-E-CL
            paramComp.getPcoDtUltgzTrchECl().setPcoDtUltgzTrchECl(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULTSC-ELAB-CL = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULTSC-ELAB-CL
        //           END-IF
        if (ws.getIndParamComp().getDtUltscElabCl() == 0) {
            // COB_CODE: MOVE PCO-DT-ULTSC-ELAB-CL-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltscElabClDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULTSC-ELAB-CL
            paramComp.getPcoDtUltscElabCl().setPcoDtUltscElabCl(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULTSC-OPZ-CL = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULTSC-OPZ-CL
        //           END-IF
        if (ws.getIndParamComp().getDtUltscOpzCl() == 0) {
            // COB_CODE: MOVE PCO-DT-ULTSC-OPZ-CL-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltscOpzClDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULTSC-OPZ-CL
            paramComp.getPcoDtUltscOpzCl().setPcoDtUltscOpzCl(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-STST-X-REGIONE = 0
        //               MOVE WS-DATE-N      TO PCO-STST-X-REGIONE
        //           END-IF
        if (ws.getIndParamComp().getStstXRegione() == 0) {
            // COB_CODE: MOVE PCO-STST-X-REGIONE-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getStstXRegioneDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-STST-X-REGIONE
            paramComp.getPcoStstXRegione().setPcoStstXRegione(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULTGZ-CED-COLL = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULTGZ-CED-COLL
        //           END-IF
        if (ws.getIndParamComp().getDtUltgzCedColl() == 0) {
            // COB_CODE: MOVE PCO-DT-ULTGZ-CED-COLL-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltgzCedCollDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULTGZ-CED-COLL
            paramComp.getPcoDtUltgzCedColl().setPcoDtUltgzCedColl(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-EC-RIV-COLL = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-EC-RIV-COLL
        //           END-IF
        if (ws.getIndParamComp().getDtUltEcRivColl() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-EC-RIV-COLL-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltEcRivCollDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-EC-RIV-COLL
            paramComp.getPcoDtUltEcRivColl().setPcoDtUltEcRivColl(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-EC-RIV-IND = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-EC-RIV-IND
        //           END-IF
        if (ws.getIndParamComp().getDtUltEcRivInd() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-EC-RIV-IND-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltEcRivIndDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-EC-RIV-IND
            paramComp.getPcoDtUltEcRivInd().setPcoDtUltEcRivInd(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-EC-IL-COLL = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-EC-IL-COLL
        //           END-IF
        if (ws.getIndParamComp().getDtUltEcIlColl() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-EC-IL-COLL-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltEcIlCollDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-EC-IL-COLL
            paramComp.getPcoDtUltEcIlColl().setPcoDtUltEcIlColl(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-EC-IL-IND = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-EC-IL-IND
        //           END-IF
        if (ws.getIndParamComp().getDtUltEcIlInd() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-EC-IL-IND-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltEcIlIndDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-EC-IL-IND
            paramComp.getPcoDtUltEcIlInd().setPcoDtUltEcIlInd(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-EC-UL-COLL = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-EC-UL-COLL
        //           END-IF
        if (ws.getIndParamComp().getDtUltEcUlColl() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-EC-UL-COLL-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltEcUlCollDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-EC-UL-COLL
            paramComp.getPcoDtUltEcUlColl().setPcoDtUltEcUlColl(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-EC-UL-IND = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-EC-UL-IND
        //           END-IF
        if (ws.getIndParamComp().getDtUltEcUlInd() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-EC-UL-IND-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltEcUlIndDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-EC-UL-IND
            paramComp.getPcoDtUltEcUlInd().setPcoDtUltEcUlInd(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-PERF-C = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-PERF-C
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollPerfC() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-PERF-C-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltBollPerfCDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-PERF-C
            paramComp.getPcoDtUltBollPerfC().setPcoDtUltBollPerfC(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-RSP-IN = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-RSP-IN
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollRspIn() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-RSP-IN-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltBollRspInDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-RSP-IN
            paramComp.getPcoDtUltBollRspIn().setPcoDtUltBollRspIn(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-RSP-CL = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-RSP-CL
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollRspCl() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-RSP-CL-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltBollRspClDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-RSP-CL
            paramComp.getPcoDtUltBollRspCl().setPcoDtUltBollRspCl(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-EMES-I = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-EMES-I
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollEmesI() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-EMES-I-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltBollEmesIDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-EMES-I
            paramComp.getPcoDtUltBollEmesI().setPcoDtUltBollEmesI(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-STOR-I = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-STOR-I
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollStorI() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-STOR-I-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltBollStorIDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-STOR-I
            paramComp.getPcoDtUltBollStorI().setPcoDtUltBollStorI(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-RIAT-I = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-RIAT-I
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollRiatI() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-RIAT-I-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltBollRiatIDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-RIAT-I
            paramComp.getPcoDtUltBollRiatI().setPcoDtUltBollRiatI(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-SD-I = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-SD-I
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollSdI() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-SD-I-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltBollSdIDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-SD-I
            paramComp.getPcoDtUltBollSdI().setPcoDtUltBollSdI(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-SDNL-I = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-SDNL-I
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollSdnlI() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-SDNL-I-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltBollSdnlIDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-SDNL-I
            paramComp.getPcoDtUltBollSdnlI().setPcoDtUltBollSdnlI(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-PERF-I = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-PERF-I
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollPerfI() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-PERF-I-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltBollPerfIDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-PERF-I
            paramComp.getPcoDtUltBollPerfI().setPcoDtUltBollPerfI(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-RICL-RIRIAS-COM = 0
        //               MOVE WS-DATE-N      TO PCO-DT-RICL-RIRIAS-COM
        //           END-IF
        if (ws.getIndParamComp().getDtRiclRiriasCom() == 0) {
            // COB_CODE: MOVE PCO-DT-RICL-RIRIAS-COM-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtRiclRiriasComDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-RICL-RIRIAS-COM
            paramComp.getPcoDtRiclRiriasCom().setPcoDtRiclRiriasCom(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-ELAB-AT92-C = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-ELAB-AT92-C
        //           END-IF
        if (ws.getIndParamComp().getDtUltElabAt92C() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-AT92-C-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltElabAt92CDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-ELAB-AT92-C
            paramComp.getPcoDtUltElabAt92C().setPcoDtUltElabAt92C(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-ELAB-AT92-I = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-ELAB-AT92-I
        //           END-IF
        if (ws.getIndParamComp().getDtUltElabAt92I() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-AT92-I-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltElabAt92IDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-ELAB-AT92-I
            paramComp.getPcoDtUltElabAt92I().setPcoDtUltElabAt92I(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-ELAB-AT93-C = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-ELAB-AT93-C
        //           END-IF
        if (ws.getIndParamComp().getDtUltElabAt93C() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-AT93-C-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltElabAt93CDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-ELAB-AT93-C
            paramComp.getPcoDtUltElabAt93C().setPcoDtUltElabAt93C(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-ELAB-AT93-I = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-ELAB-AT93-I
        //           END-IF
        if (ws.getIndParamComp().getDtUltElabAt93I() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-AT93-I-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltElabAt93IDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-ELAB-AT93-I
            paramComp.getPcoDtUltElabAt93I().setPcoDtUltElabAt93I(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-ELAB-SPE-IN = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-ELAB-SPE-IN
        //           END-IF
        if (ws.getIndParamComp().getDtUltElabSpeIn() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-SPE-IN-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltElabSpeInDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-ELAB-SPE-IN
            paramComp.getPcoDtUltElabSpeIn().setPcoDtUltElabSpeIn(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-ELAB-PR-CON = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-ELAB-PR-CON
        //           END-IF
        if (ws.getIndParamComp().getDtUltElabPrCon() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-PR-CON-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltElabPrConDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-ELAB-PR-CON
            paramComp.getPcoDtUltElabPrCon().setPcoDtUltElabPrCon(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-RP-CL = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-RP-CL
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollRpCl() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-RP-CL-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltBollRpClDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-RP-CL
            paramComp.getPcoDtUltBollRpCl().setPcoDtUltBollRpCl(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-RP-IN = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-RP-IN
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollRpIn() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-RP-IN-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltBollRpInDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-RP-IN
            paramComp.getPcoDtUltBollRpIn().setPcoDtUltBollRpIn(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-PRE-I = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-PRE-I
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollPreI() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-PRE-I-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltBollPreIDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-PRE-I
            paramComp.getPcoDtUltBollPreI().setPcoDtUltBollPreI(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-PRE-C = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-PRE-C
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollPreC() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-PRE-C-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltBollPreCDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-PRE-C
            paramComp.getPcoDtUltBollPreC().setPcoDtUltBollPreC(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULTC-PILDI-MM-C = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULTC-PILDI-MM-C
        //           END-IF
        if (ws.getIndParamComp().getDtUltcPildiMmC() == 0) {
            // COB_CODE: MOVE PCO-DT-ULTC-PILDI-MM-C-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltcPildiMmCDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULTC-PILDI-MM-C
            paramComp.getPcoDtUltcPildiMmC().setPcoDtUltcPildiMmC(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULTC-PILDI-AA-C = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULTC-PILDI-AA-C
        //           END-IF
        if (ws.getIndParamComp().getDtUltcPildiAaC() == 0) {
            // COB_CODE: MOVE PCO-DT-ULTC-PILDI-AA-C-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltcPildiAaCDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULTC-PILDI-AA-C
            paramComp.getPcoDtUltcPildiAaC().setPcoDtUltcPildiAaC(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULTC-PILDI-MM-I = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULTC-PILDI-MM-I
        //           END-IF
        if (ws.getIndParamComp().getDtUltcPildiMmI() == 0) {
            // COB_CODE: MOVE PCO-DT-ULTC-PILDI-MM-I-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltcPildiMmIDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULTC-PILDI-MM-I
            paramComp.getPcoDtUltcPildiMmI().setPcoDtUltcPildiMmI(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULTC-PILDI-TR-I = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULTC-PILDI-TR-I
        //           END-IF
        if (ws.getIndParamComp().getDtUltcPildiTrI() == 0) {
            // COB_CODE: MOVE PCO-DT-ULTC-PILDI-TR-I-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltcPildiTrIDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULTC-PILDI-TR-I
            paramComp.getPcoDtUltcPildiTrI().setPcoDtUltcPildiTrI(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULTC-PILDI-AA-I = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULTC-PILDI-AA-I
        //           END-IF
        if (ws.getIndParamComp().getDtUltcPildiAaI() == 0) {
            // COB_CODE: MOVE PCO-DT-ULTC-PILDI-AA-I-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltcPildiAaIDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULTC-PILDI-AA-I
            paramComp.getPcoDtUltcPildiAaI().setPcoDtUltcPildiAaI(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-QUIE-C = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-QUIE-C
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollQuieC() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-QUIE-C-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltBollQuieCDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-QUIE-C
            paramComp.getPcoDtUltBollQuieC().setPcoDtUltBollQuieC(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-QUIE-I = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-QUIE-I
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollQuieI() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-QUIE-I-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltBollQuieIDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-QUIE-I
            paramComp.getPcoDtUltBollQuieI().setPcoDtUltBollQuieI(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-COTR-I = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-COTR-I
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollCotrI() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-COTR-I-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltBollCotrIDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-COTR-I
            paramComp.getPcoDtUltBollCotrI().setPcoDtUltBollCotrI(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-COTR-C = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-COTR-C
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollCotrC() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-COTR-C-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltBollCotrCDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-COTR-C
            paramComp.getPcoDtUltBollCotrC().setPcoDtUltBollCotrC(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-CORI-C = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-CORI-C
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollCoriC() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-CORI-C-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltBollCoriCDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-CORI-C
            paramComp.getPcoDtUltBollCoriC().setPcoDtUltBollCoriC(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-BOLL-CORI-I = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-CORI-I
        //           END-IF
        if (ws.getIndParamComp().getDtUltBollCoriI() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-BOLL-CORI-I-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltBollCoriIDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-BOLL-CORI-I
            paramComp.getPcoDtUltBollCoriI().setPcoDtUltBollCoriI(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-AGG-EROG-RE = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-AGG-EROG-RE
        //           END-IF
        if (ws.getIndParamComp().getDtUltAggErogRe() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-AGG-EROG-RE-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltAggErogReDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-AGG-EROG-RE
            paramComp.getPcoDtUltAggErogRe().setPcoDtUltAggErogRe(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-ESTRAZ-FUG = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-ESTRAZ-FUG
        //           END-IF
        if (ws.getIndParamComp().getDtUltEstrazFug() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-ESTRAZ-FUG-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltEstrazFugDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-ESTRAZ-FUG
            paramComp.getPcoDtUltEstrazFug().setPcoDtUltEstrazFug(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-ELAB-PR-AUT = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-ELAB-PR-AUT
        //           END-IF
        if (ws.getIndParamComp().getDtUltElabPrAut() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-PR-AUT-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltElabPrAutDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-ELAB-PR-AUT
            paramComp.getPcoDtUltElabPrAut().setPcoDtUltElabPrAut(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-ELAB-COMMEF = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-ELAB-COMMEF
        //           END-IF
        if (ws.getIndParamComp().getDtUltElabCommef() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-COMMEF-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltElabCommefDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-ELAB-COMMEF
            paramComp.getPcoDtUltElabCommef().setPcoDtUltElabCommef(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-ELAB-LIQMEF = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-ELAB-LIQMEF
        //           END-IF
        if (ws.getIndParamComp().getDtUltElabLiqmef() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-LIQMEF-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltElabLiqmefDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-ELAB-LIQMEF
            paramComp.getPcoDtUltElabLiqmef().setPcoDtUltElabLiqmef(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-RIAT-RIASS-RSH = 0
        //               MOVE WS-DATE-N      TO PCO-DT-RIAT-RIASS-RSH
        //           END-IF
        if (ws.getIndParamComp().getDtRiatRiassRsh() == 0) {
            // COB_CODE: MOVE PCO-DT-RIAT-RIASS-RSH-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtRiatRiassRshDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-RIAT-RIASS-RSH
            paramComp.getPcoDtRiatRiassRsh().setPcoDtRiatRiassRsh(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-RIAT-RIASS-COMM = 0
        //               MOVE WS-DATE-N      TO PCO-DT-RIAT-RIASS-COMM
        //           END-IF
        if (ws.getIndParamComp().getDtRiatRiassComm() == 0) {
            // COB_CODE: MOVE PCO-DT-RIAT-RIASS-COMM-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtRiatRiassCommDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-RIAT-RIASS-COMM
            paramComp.getPcoDtRiatRiassComm().setPcoDtRiatRiassComm(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-RINN-TAC = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-RINN-TAC
        //           END-IF
        if (ws.getIndParamComp().getDtUltRinnTac() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-RINN-TAC-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltRinnTacDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-RINN-TAC
            paramComp.getPcoDtUltRinnTac().setPcoDtUltRinnTac(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-EC-TCM-IND = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-EC-TCM-IND
        //           END-IF
        if (ws.getIndParamComp().getDtUltEcTcmInd() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-EC-TCM-IND-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltEcTcmIndDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-EC-TCM-IND
            paramComp.getPcoDtUltEcTcmInd().setPcoDtUltEcTcmInd(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-EC-TCM-COLL = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-EC-TCM-COLL
        //           END-IF
        if (ws.getIndParamComp().getDtUltEcTcmColl() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-EC-TCM-COLL-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltEcTcmCollDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-EC-TCM-COLL
            paramComp.getPcoDtUltEcTcmColl().setPcoDtUltEcTcmColl(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-EC-MRM-IND = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-EC-MRM-IND
        //           END-IF
        if (ws.getIndParamComp().getDtUltEcMrmInd() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-EC-MRM-IND-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltEcMrmIndDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-EC-MRM-IND
            paramComp.getPcoDtUltEcMrmInd().setPcoDtUltEcMrmInd(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-EC-MRM-COLL = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-EC-MRM-COLL
        //           END-IF
        if (ws.getIndParamComp().getDtUltEcMrmColl() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-EC-MRM-COLL-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltEcMrmCollDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-EC-MRM-COLL
            paramComp.getPcoDtUltEcMrmColl().setPcoDtUltEcMrmColl(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-ELAB-REDPRO = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-ELAB-REDPRO
        //           END-IF
        if (ws.getIndParamComp().getDtUltElabRedpro() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-REDPRO-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltElabRedproDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-ELAB-REDPRO
            paramComp.getPcoDtUltElabRedpro().setPcoDtUltElabRedpro(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-ELAB-TAKE-P = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-ELAB-TAKE-P
        //           END-IF
        if (ws.getIndParamComp().getDtUltElabTakeP() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-TAKE-P-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltElabTakePDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-ELAB-TAKE-P
            paramComp.getPcoDtUltElabTakeP().setPcoDtUltElabTakeP(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-ELAB-PASPAS = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-ELAB-PASPAS
        //           END-IF
        if (ws.getIndParamComp().getDtUltElabPaspas() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-PASPAS-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltElabPaspasDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-ELAB-PASPAS
            paramComp.getPcoDtUltElabPaspas().setPcoDtUltElabPaspas(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-ESTR-DEC-CO = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-ESTR-DEC-CO
        //           END-IF
        if (ws.getIndParamComp().getDtUltEstrDecCo() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-ESTR-DEC-CO-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltEstrDecCoDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-ESTR-DEC-CO
            paramComp.getPcoDtUltEstrDecCo().setPcoDtUltEstrDecCo(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-ELAB-COS-AT = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-ELAB-COS-AT
        //           END-IF
        if (ws.getIndParamComp().getDtUltElabCosAt() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-COS-AT-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltElabCosAtDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-ELAB-COS-AT
            paramComp.getPcoDtUltElabCosAt().setPcoDtUltElabCosAt(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ULT-ELAB-COS-ST = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ULT-ELAB-COS-ST
        //           END-IF
        if (ws.getIndParamComp().getDtUltElabCosSt() == 0) {
            // COB_CODE: MOVE PCO-DT-ULT-ELAB-COS-ST-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtUltElabCosStDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ULT-ELAB-COS-ST
            paramComp.getPcoDtUltElabCosSt().setPcoDtUltElabCosSt(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ESTR-ASS-MIN70A = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ESTR-ASS-MIN70A
        //           END-IF
        if (ws.getIndParamComp().getDtEstrAssMin70a() == 0) {
            // COB_CODE: MOVE PCO-DT-ESTR-ASS-MIN70A-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtEstrAssMin70aDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ESTR-ASS-MIN70A
            paramComp.getPcoDtEstrAssMin70a().setPcoDtEstrAssMin70a(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-PCO-DT-ESTR-ASS-MAG70A = 0
        //               MOVE WS-DATE-N      TO PCO-DT-ESTR-ASS-MAG70A
        //           END-IF.
        if (ws.getIndParamComp().getDtEstrAssMag70a() == 0) {
            // COB_CODE: MOVE PCO-DT-ESTR-ASS-MAG70A-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtEstrAssMag70aDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-ESTR-ASS-MAG70A
            paramComp.getPcoDtEstrAssMag70a().setPcoDtEstrAssMag70a(ws.getIdsv0010().getWsDateN());
        }
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public short getAaUti() {
        return paramComp.getPcoAaUti().getPcoAaUti();
    }

    @Override
    public void setAaUti(short aaUti) {
        this.paramComp.getPcoAaUti().setPcoAaUti(aaUti);
    }

    @Override
    public Short getAaUtiObj() {
        if (ws.getIndParamComp().getAaUti() >= 0) {
            return ((Short)getAaUti());
        }
        else {
            return null;
        }
    }

    @Override
    public void setAaUtiObj(Short aaUtiObj) {
        if (aaUtiObj != null) {
            setAaUti(((short)aaUtiObj));
            ws.getIndParamComp().setAaUti(((short)0));
        }
        else {
            ws.getIndParamComp().setAaUti(((short)-1));
        }
    }

    @Override
    public AfDecimal getArrotPre() {
        return paramComp.getPcoArrotPre().getPcoArrotPre();
    }

    @Override
    public void setArrotPre(AfDecimal arrotPre) {
        this.paramComp.getPcoArrotPre().setPcoArrotPre(arrotPre.copy());
    }

    @Override
    public AfDecimal getArrotPreObj() {
        if (ws.getIndParamComp().getArrotPre() >= 0) {
            return getArrotPre();
        }
        else {
            return null;
        }
    }

    @Override
    public void setArrotPreObj(AfDecimal arrotPreObj) {
        if (arrotPreObj != null) {
            setArrotPre(new AfDecimal(arrotPreObj, 15, 3));
            ws.getIndParamComp().setArrotPre(((short)0));
        }
        else {
            ws.getIndParamComp().setArrotPre(((short)-1));
        }
    }

    @Override
    public char getCalcRshComun() {
        return paramComp.getPcoCalcRshComun();
    }

    @Override
    public void setCalcRshComun(char calcRshComun) {
        this.paramComp.setPcoCalcRshComun(calcRshComun);
    }

    @Override
    public String getCodCompIsvap() {
        return paramComp.getPcoCodCompIsvap();
    }

    @Override
    public void setCodCompIsvap(String codCompIsvap) {
        this.paramComp.setPcoCodCompIsvap(codCompIsvap);
    }

    @Override
    public String getCodCompIsvapObj() {
        if (ws.getIndParamComp().getCodCompIsvap() >= 0) {
            return getCodCompIsvap();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodCompIsvapObj(String codCompIsvapObj) {
        if (codCompIsvapObj != null) {
            setCodCompIsvap(codCompIsvapObj);
            ws.getIndParamComp().setCodCompIsvap(((short)0));
        }
        else {
            ws.getIndParamComp().setCodCompIsvap(((short)-1));
        }
    }

    @Override
    public String getCodCompLdap() {
        return paramComp.getPcoCodCompLdap();
    }

    @Override
    public void setCodCompLdap(String codCompLdap) {
        this.paramComp.setPcoCodCompLdap(codCompLdap);
    }

    @Override
    public String getCodCompLdapObj() {
        if (ws.getIndParamComp().getCodCompLdap() >= 0) {
            return getCodCompLdap();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodCompLdapObj(String codCompLdapObj) {
        if (codCompLdapObj != null) {
            setCodCompLdap(codCompLdapObj);
            ws.getIndParamComp().setCodCompLdap(((short)0));
        }
        else {
            ws.getIndParamComp().setCodCompLdap(((short)-1));
        }
    }

    @Override
    public String getCodFiscMef() {
        return paramComp.getPcoCodFiscMef();
    }

    @Override
    public void setCodFiscMef(String codFiscMef) {
        this.paramComp.setPcoCodFiscMef(codFiscMef);
    }

    @Override
    public String getCodFiscMefObj() {
        if (ws.getIndParamComp().getCodFiscMef() >= 0) {
            return getCodFiscMef();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodFiscMefObj(String codFiscMefObj) {
        if (codFiscMefObj != null) {
            setCodFiscMef(codFiscMefObj);
            ws.getIndParamComp().setCodFiscMef(((short)0));
        }
        else {
            ws.getIndParamComp().setCodFiscMef(((short)-1));
        }
    }

    @Override
    public String getCodSoggFtzAssto() {
        return paramComp.getPcoCodSoggFtzAssto();
    }

    @Override
    public void setCodSoggFtzAssto(String codSoggFtzAssto) {
        this.paramComp.setPcoCodSoggFtzAssto(codSoggFtzAssto);
    }

    @Override
    public String getCodSoggFtzAsstoObj() {
        if (ws.getIndParamComp().getCodSoggFtzAssto() >= 0) {
            return getCodSoggFtzAssto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodSoggFtzAsstoObj(String codSoggFtzAsstoObj) {
        if (codSoggFtzAsstoObj != null) {
            setCodSoggFtzAssto(codSoggFtzAsstoObj);
            ws.getIndParamComp().setCodSoggFtzAssto(((short)0));
        }
        else {
            ws.getIndParamComp().setCodSoggFtzAssto(((short)-1));
        }
    }

    @Override
    public String getCodTratCirt() {
        return paramComp.getPcoCodTratCirt();
    }

    @Override
    public void setCodTratCirt(String codTratCirt) {
        this.paramComp.setPcoCodTratCirt(codTratCirt);
    }

    @Override
    public String getCodTratCirtObj() {
        if (ws.getIndParamComp().getCodTratCirt() >= 0) {
            return getCodTratCirt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodTratCirtObj(String codTratCirtObj) {
        if (codTratCirtObj != null) {
            setCodTratCirt(codTratCirtObj);
            ws.getIndParamComp().setCodTratCirt(((short)0));
        }
        else {
            ws.getIndParamComp().setCodTratCirt(((short)-1));
        }
    }

    @Override
    public char getCrz1aRatIntrPr() {
        return paramComp.getPcoCrz1aRatIntrPr();
    }

    @Override
    public void setCrz1aRatIntrPr(char crz1aRatIntrPr) {
        this.paramComp.setPcoCrz1aRatIntrPr(crz1aRatIntrPr);
    }

    @Override
    public Character getCrz1aRatIntrPrObj() {
        if (ws.getIndParamComp().getCrz1aRatIntrPr() >= 0) {
            return ((Character)getCrz1aRatIntrPr());
        }
        else {
            return null;
        }
    }

    @Override
    public void setCrz1aRatIntrPrObj(Character crz1aRatIntrPrObj) {
        if (crz1aRatIntrPrObj != null) {
            setCrz1aRatIntrPr(((char)crz1aRatIntrPrObj));
            ws.getIndParamComp().setCrz1aRatIntrPr(((short)0));
        }
        else {
            ws.getIndParamComp().setCrz1aRatIntrPr(((short)-1));
        }
    }

    @Override
    public String getDescCompVchar() {
        return paramComp.getPcoDescCompVcharFormatted();
    }

    @Override
    public void setDescCompVchar(String descCompVchar) {
        this.paramComp.setPcoDescCompVcharFormatted(descCompVchar);
    }

    @Override
    public String getDescCompVcharObj() {
        if (ws.getIndParamComp().getDescComp() >= 0) {
            return getDescCompVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDescCompVcharObj(String descCompVcharObj) {
        if (descCompVcharObj != null) {
            setDescCompVchar(descCompVcharObj);
            ws.getIndParamComp().setDescComp(((short)0));
        }
        else {
            ws.getIndParamComp().setDescComp(((short)-1));
        }
    }

    @Override
    public char getDsOperSql() {
        return paramComp.getPcoDsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.paramComp.setPcoDsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return paramComp.getPcoDsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.paramComp.setPcoDsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsCptz() {
        return paramComp.getPcoDsTsCptz();
    }

    @Override
    public void setDsTsCptz(long dsTsCptz) {
        this.paramComp.setPcoDsTsCptz(dsTsCptz);
    }

    @Override
    public String getDsUtente() {
        return paramComp.getPcoDsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.paramComp.setPcoDsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return paramComp.getPcoDsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.paramComp.setPcoDsVer(dsVer);
    }

    @Override
    public String getDtContDb() {
        return ws.getParamCompDb().getDtContDb();
    }

    @Override
    public void setDtContDb(String dtContDb) {
        this.ws.getParamCompDb().setDtContDb(dtContDb);
    }

    @Override
    public String getDtContDbObj() {
        if (ws.getIndParamComp().getDtCont() >= 0) {
            return getDtContDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtContDbObj(String dtContDbObj) {
        if (dtContDbObj != null) {
            setDtContDb(dtContDbObj);
            ws.getIndParamComp().setDtCont(((short)0));
        }
        else {
            ws.getIndParamComp().setDtCont(((short)-1));
        }
    }

    @Override
    public String getDtEstrAssMag70aDb() {
        return ws.getParamCompDb().getDtEstrAssMag70aDb();
    }

    @Override
    public void setDtEstrAssMag70aDb(String dtEstrAssMag70aDb) {
        this.ws.getParamCompDb().setDtEstrAssMag70aDb(dtEstrAssMag70aDb);
    }

    @Override
    public String getDtEstrAssMag70aDbObj() {
        if (ws.getIndParamComp().getDtEstrAssMag70a() >= 0) {
            return getDtEstrAssMag70aDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtEstrAssMag70aDbObj(String dtEstrAssMag70aDbObj) {
        if (dtEstrAssMag70aDbObj != null) {
            setDtEstrAssMag70aDb(dtEstrAssMag70aDbObj);
            ws.getIndParamComp().setDtEstrAssMag70a(((short)0));
        }
        else {
            ws.getIndParamComp().setDtEstrAssMag70a(((short)-1));
        }
    }

    @Override
    public String getDtEstrAssMin70aDb() {
        return ws.getParamCompDb().getDtEstrAssMin70aDb();
    }

    @Override
    public void setDtEstrAssMin70aDb(String dtEstrAssMin70aDb) {
        this.ws.getParamCompDb().setDtEstrAssMin70aDb(dtEstrAssMin70aDb);
    }

    @Override
    public String getDtEstrAssMin70aDbObj() {
        if (ws.getIndParamComp().getDtEstrAssMin70a() >= 0) {
            return getDtEstrAssMin70aDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtEstrAssMin70aDbObj(String dtEstrAssMin70aDbObj) {
        if (dtEstrAssMin70aDbObj != null) {
            setDtEstrAssMin70aDb(dtEstrAssMin70aDbObj);
            ws.getIndParamComp().setDtEstrAssMin70a(((short)0));
        }
        else {
            ws.getIndParamComp().setDtEstrAssMin70a(((short)-1));
        }
    }

    @Override
    public String getDtRiatRiassCommDb() {
        return ws.getParamCompDb().getDtRiatRiassCommDb();
    }

    @Override
    public void setDtRiatRiassCommDb(String dtRiatRiassCommDb) {
        this.ws.getParamCompDb().setDtRiatRiassCommDb(dtRiatRiassCommDb);
    }

    @Override
    public String getDtRiatRiassCommDbObj() {
        if (ws.getIndParamComp().getDtRiatRiassComm() >= 0) {
            return getDtRiatRiassCommDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtRiatRiassCommDbObj(String dtRiatRiassCommDbObj) {
        if (dtRiatRiassCommDbObj != null) {
            setDtRiatRiassCommDb(dtRiatRiassCommDbObj);
            ws.getIndParamComp().setDtRiatRiassComm(((short)0));
        }
        else {
            ws.getIndParamComp().setDtRiatRiassComm(((short)-1));
        }
    }

    @Override
    public String getDtRiatRiassRshDb() {
        return ws.getParamCompDb().getDtRiatRiassRshDb();
    }

    @Override
    public void setDtRiatRiassRshDb(String dtRiatRiassRshDb) {
        this.ws.getParamCompDb().setDtRiatRiassRshDb(dtRiatRiassRshDb);
    }

    @Override
    public String getDtRiatRiassRshDbObj() {
        if (ws.getIndParamComp().getDtRiatRiassRsh() >= 0) {
            return getDtRiatRiassRshDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtRiatRiassRshDbObj(String dtRiatRiassRshDbObj) {
        if (dtRiatRiassRshDbObj != null) {
            setDtRiatRiassRshDb(dtRiatRiassRshDbObj);
            ws.getIndParamComp().setDtRiatRiassRsh(((short)0));
        }
        else {
            ws.getIndParamComp().setDtRiatRiassRsh(((short)-1));
        }
    }

    @Override
    public String getDtRiclRiriasComDb() {
        return ws.getParamCompDb().getDtRiclRiriasComDb();
    }

    @Override
    public void setDtRiclRiriasComDb(String dtRiclRiriasComDb) {
        this.ws.getParamCompDb().setDtRiclRiriasComDb(dtRiclRiriasComDb);
    }

    @Override
    public String getDtRiclRiriasComDbObj() {
        if (ws.getIndParamComp().getDtRiclRiriasCom() >= 0) {
            return getDtRiclRiriasComDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtRiclRiriasComDbObj(String dtRiclRiriasComDbObj) {
        if (dtRiclRiriasComDbObj != null) {
            setDtRiclRiriasComDb(dtRiclRiriasComDbObj);
            ws.getIndParamComp().setDtRiclRiriasCom(((short)0));
        }
        else {
            ws.getIndParamComp().setDtRiclRiriasCom(((short)-1));
        }
    }

    @Override
    public String getDtUltAggErogReDb() {
        return ws.getParamCompDb().getDtUltAggErogReDb();
    }

    @Override
    public void setDtUltAggErogReDb(String dtUltAggErogReDb) {
        this.ws.getParamCompDb().setDtUltAggErogReDb(dtUltAggErogReDb);
    }

    @Override
    public String getDtUltAggErogReDbObj() {
        if (ws.getIndParamComp().getDtUltAggErogRe() >= 0) {
            return getDtUltAggErogReDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltAggErogReDbObj(String dtUltAggErogReDbObj) {
        if (dtUltAggErogReDbObj != null) {
            setDtUltAggErogReDb(dtUltAggErogReDbObj);
            ws.getIndParamComp().setDtUltAggErogRe(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltAggErogRe(((short)-1));
        }
    }

    @Override
    public String getDtUltBollCoriCDb() {
        return ws.getParamCompDb().getDtUltBollCoriCDb();
    }

    @Override
    public void setDtUltBollCoriCDb(String dtUltBollCoriCDb) {
        this.ws.getParamCompDb().setDtUltBollCoriCDb(dtUltBollCoriCDb);
    }

    @Override
    public String getDtUltBollCoriCDbObj() {
        if (ws.getIndParamComp().getDtUltBollCoriC() >= 0) {
            return getDtUltBollCoriCDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltBollCoriCDbObj(String dtUltBollCoriCDbObj) {
        if (dtUltBollCoriCDbObj != null) {
            setDtUltBollCoriCDb(dtUltBollCoriCDbObj);
            ws.getIndParamComp().setDtUltBollCoriC(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltBollCoriC(((short)-1));
        }
    }

    @Override
    public String getDtUltBollCoriIDb() {
        return ws.getParamCompDb().getDtUltBollCoriIDb();
    }

    @Override
    public void setDtUltBollCoriIDb(String dtUltBollCoriIDb) {
        this.ws.getParamCompDb().setDtUltBollCoriIDb(dtUltBollCoriIDb);
    }

    @Override
    public String getDtUltBollCoriIDbObj() {
        if (ws.getIndParamComp().getDtUltBollCoriI() >= 0) {
            return getDtUltBollCoriIDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltBollCoriIDbObj(String dtUltBollCoriIDbObj) {
        if (dtUltBollCoriIDbObj != null) {
            setDtUltBollCoriIDb(dtUltBollCoriIDbObj);
            ws.getIndParamComp().setDtUltBollCoriI(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltBollCoriI(((short)-1));
        }
    }

    @Override
    public String getDtUltBollCotrCDb() {
        return ws.getParamCompDb().getDtUltBollCotrCDb();
    }

    @Override
    public void setDtUltBollCotrCDb(String dtUltBollCotrCDb) {
        this.ws.getParamCompDb().setDtUltBollCotrCDb(dtUltBollCotrCDb);
    }

    @Override
    public String getDtUltBollCotrCDbObj() {
        if (ws.getIndParamComp().getDtUltBollCotrC() >= 0) {
            return getDtUltBollCotrCDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltBollCotrCDbObj(String dtUltBollCotrCDbObj) {
        if (dtUltBollCotrCDbObj != null) {
            setDtUltBollCotrCDb(dtUltBollCotrCDbObj);
            ws.getIndParamComp().setDtUltBollCotrC(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltBollCotrC(((short)-1));
        }
    }

    @Override
    public String getDtUltBollCotrIDb() {
        return ws.getParamCompDb().getDtUltBollCotrIDb();
    }

    @Override
    public void setDtUltBollCotrIDb(String dtUltBollCotrIDb) {
        this.ws.getParamCompDb().setDtUltBollCotrIDb(dtUltBollCotrIDb);
    }

    @Override
    public String getDtUltBollCotrIDbObj() {
        if (ws.getIndParamComp().getDtUltBollCotrI() >= 0) {
            return getDtUltBollCotrIDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltBollCotrIDbObj(String dtUltBollCotrIDbObj) {
        if (dtUltBollCotrIDbObj != null) {
            setDtUltBollCotrIDb(dtUltBollCotrIDbObj);
            ws.getIndParamComp().setDtUltBollCotrI(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltBollCotrI(((short)-1));
        }
    }

    @Override
    public String getDtUltBollEmesDb() {
        return ws.getParamCompDb().getDtUltBollEmesDb();
    }

    @Override
    public void setDtUltBollEmesDb(String dtUltBollEmesDb) {
        this.ws.getParamCompDb().setDtUltBollEmesDb(dtUltBollEmesDb);
    }

    @Override
    public String getDtUltBollEmesDbObj() {
        if (ws.getIndParamComp().getDtUltBollEmes() >= 0) {
            return getDtUltBollEmesDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltBollEmesDbObj(String dtUltBollEmesDbObj) {
        if (dtUltBollEmesDbObj != null) {
            setDtUltBollEmesDb(dtUltBollEmesDbObj);
            ws.getIndParamComp().setDtUltBollEmes(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltBollEmes(((short)-1));
        }
    }

    @Override
    public String getDtUltBollEmesIDb() {
        return ws.getParamCompDb().getDtUltBollEmesIDb();
    }

    @Override
    public void setDtUltBollEmesIDb(String dtUltBollEmesIDb) {
        this.ws.getParamCompDb().setDtUltBollEmesIDb(dtUltBollEmesIDb);
    }

    @Override
    public String getDtUltBollEmesIDbObj() {
        if (ws.getIndParamComp().getDtUltBollEmesI() >= 0) {
            return getDtUltBollEmesIDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltBollEmesIDbObj(String dtUltBollEmesIDbObj) {
        if (dtUltBollEmesIDbObj != null) {
            setDtUltBollEmesIDb(dtUltBollEmesIDbObj);
            ws.getIndParamComp().setDtUltBollEmesI(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltBollEmesI(((short)-1));
        }
    }

    @Override
    public String getDtUltBollLiqDb() {
        return ws.getParamCompDb().getDtUltBollLiqDb();
    }

    @Override
    public void setDtUltBollLiqDb(String dtUltBollLiqDb) {
        this.ws.getParamCompDb().setDtUltBollLiqDb(dtUltBollLiqDb);
    }

    @Override
    public String getDtUltBollLiqDbObj() {
        if (ws.getIndParamComp().getDtUltBollLiq() >= 0) {
            return getDtUltBollLiqDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltBollLiqDbObj(String dtUltBollLiqDbObj) {
        if (dtUltBollLiqDbObj != null) {
            setDtUltBollLiqDb(dtUltBollLiqDbObj);
            ws.getIndParamComp().setDtUltBollLiq(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltBollLiq(((short)-1));
        }
    }

    @Override
    public String getDtUltBollPerfCDb() {
        return ws.getParamCompDb().getDtUltBollPerfCDb();
    }

    @Override
    public void setDtUltBollPerfCDb(String dtUltBollPerfCDb) {
        this.ws.getParamCompDb().setDtUltBollPerfCDb(dtUltBollPerfCDb);
    }

    @Override
    public String getDtUltBollPerfCDbObj() {
        if (ws.getIndParamComp().getDtUltBollPerfC() >= 0) {
            return getDtUltBollPerfCDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltBollPerfCDbObj(String dtUltBollPerfCDbObj) {
        if (dtUltBollPerfCDbObj != null) {
            setDtUltBollPerfCDb(dtUltBollPerfCDbObj);
            ws.getIndParamComp().setDtUltBollPerfC(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltBollPerfC(((short)-1));
        }
    }

    @Override
    public String getDtUltBollPerfIDb() {
        return ws.getParamCompDb().getDtUltBollPerfIDb();
    }

    @Override
    public void setDtUltBollPerfIDb(String dtUltBollPerfIDb) {
        this.ws.getParamCompDb().setDtUltBollPerfIDb(dtUltBollPerfIDb);
    }

    @Override
    public String getDtUltBollPerfIDbObj() {
        if (ws.getIndParamComp().getDtUltBollPerfI() >= 0) {
            return getDtUltBollPerfIDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltBollPerfIDbObj(String dtUltBollPerfIDbObj) {
        if (dtUltBollPerfIDbObj != null) {
            setDtUltBollPerfIDb(dtUltBollPerfIDbObj);
            ws.getIndParamComp().setDtUltBollPerfI(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltBollPerfI(((short)-1));
        }
    }

    @Override
    public String getDtUltBollPreCDb() {
        return ws.getParamCompDb().getDtUltBollPreCDb();
    }

    @Override
    public void setDtUltBollPreCDb(String dtUltBollPreCDb) {
        this.ws.getParamCompDb().setDtUltBollPreCDb(dtUltBollPreCDb);
    }

    @Override
    public String getDtUltBollPreCDbObj() {
        if (ws.getIndParamComp().getDtUltBollPreC() >= 0) {
            return getDtUltBollPreCDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltBollPreCDbObj(String dtUltBollPreCDbObj) {
        if (dtUltBollPreCDbObj != null) {
            setDtUltBollPreCDb(dtUltBollPreCDbObj);
            ws.getIndParamComp().setDtUltBollPreC(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltBollPreC(((short)-1));
        }
    }

    @Override
    public String getDtUltBollPreIDb() {
        return ws.getParamCompDb().getDtUltBollPreIDb();
    }

    @Override
    public void setDtUltBollPreIDb(String dtUltBollPreIDb) {
        this.ws.getParamCompDb().setDtUltBollPreIDb(dtUltBollPreIDb);
    }

    @Override
    public String getDtUltBollPreIDbObj() {
        if (ws.getIndParamComp().getDtUltBollPreI() >= 0) {
            return getDtUltBollPreIDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltBollPreIDbObj(String dtUltBollPreIDbObj) {
        if (dtUltBollPreIDbObj != null) {
            setDtUltBollPreIDb(dtUltBollPreIDbObj);
            ws.getIndParamComp().setDtUltBollPreI(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltBollPreI(((short)-1));
        }
    }

    @Override
    public String getDtUltBollQuieCDb() {
        return ws.getParamCompDb().getDtUltBollQuieCDb();
    }

    @Override
    public void setDtUltBollQuieCDb(String dtUltBollQuieCDb) {
        this.ws.getParamCompDb().setDtUltBollQuieCDb(dtUltBollQuieCDb);
    }

    @Override
    public String getDtUltBollQuieCDbObj() {
        if (ws.getIndParamComp().getDtUltBollQuieC() >= 0) {
            return getDtUltBollQuieCDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltBollQuieCDbObj(String dtUltBollQuieCDbObj) {
        if (dtUltBollQuieCDbObj != null) {
            setDtUltBollQuieCDb(dtUltBollQuieCDbObj);
            ws.getIndParamComp().setDtUltBollQuieC(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltBollQuieC(((short)-1));
        }
    }

    @Override
    public String getDtUltBollQuieIDb() {
        return ws.getParamCompDb().getDtUltBollQuieIDb();
    }

    @Override
    public void setDtUltBollQuieIDb(String dtUltBollQuieIDb) {
        this.ws.getParamCompDb().setDtUltBollQuieIDb(dtUltBollQuieIDb);
    }

    @Override
    public String getDtUltBollQuieIDbObj() {
        if (ws.getIndParamComp().getDtUltBollQuieI() >= 0) {
            return getDtUltBollQuieIDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltBollQuieIDbObj(String dtUltBollQuieIDbObj) {
        if (dtUltBollQuieIDbObj != null) {
            setDtUltBollQuieIDb(dtUltBollQuieIDbObj);
            ws.getIndParamComp().setDtUltBollQuieI(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltBollQuieI(((short)-1));
        }
    }

    @Override
    public String getDtUltBollRiatDb() {
        return ws.getParamCompDb().getDtUltBollRiatDb();
    }

    @Override
    public void setDtUltBollRiatDb(String dtUltBollRiatDb) {
        this.ws.getParamCompDb().setDtUltBollRiatDb(dtUltBollRiatDb);
    }

    @Override
    public String getDtUltBollRiatDbObj() {
        if (ws.getIndParamComp().getDtUltBollRiat() >= 0) {
            return getDtUltBollRiatDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltBollRiatDbObj(String dtUltBollRiatDbObj) {
        if (dtUltBollRiatDbObj != null) {
            setDtUltBollRiatDb(dtUltBollRiatDbObj);
            ws.getIndParamComp().setDtUltBollRiat(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltBollRiat(((short)-1));
        }
    }

    @Override
    public String getDtUltBollRiatIDb() {
        return ws.getParamCompDb().getDtUltBollRiatIDb();
    }

    @Override
    public void setDtUltBollRiatIDb(String dtUltBollRiatIDb) {
        this.ws.getParamCompDb().setDtUltBollRiatIDb(dtUltBollRiatIDb);
    }

    @Override
    public String getDtUltBollRiatIDbObj() {
        if (ws.getIndParamComp().getDtUltBollRiatI() >= 0) {
            return getDtUltBollRiatIDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltBollRiatIDbObj(String dtUltBollRiatIDbObj) {
        if (dtUltBollRiatIDbObj != null) {
            setDtUltBollRiatIDb(dtUltBollRiatIDbObj);
            ws.getIndParamComp().setDtUltBollRiatI(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltBollRiatI(((short)-1));
        }
    }

    @Override
    public String getDtUltBollRpClDb() {
        return ws.getParamCompDb().getDtUltBollRpClDb();
    }

    @Override
    public void setDtUltBollRpClDb(String dtUltBollRpClDb) {
        this.ws.getParamCompDb().setDtUltBollRpClDb(dtUltBollRpClDb);
    }

    @Override
    public String getDtUltBollRpClDbObj() {
        if (ws.getIndParamComp().getDtUltBollRpCl() >= 0) {
            return getDtUltBollRpClDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltBollRpClDbObj(String dtUltBollRpClDbObj) {
        if (dtUltBollRpClDbObj != null) {
            setDtUltBollRpClDb(dtUltBollRpClDbObj);
            ws.getIndParamComp().setDtUltBollRpCl(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltBollRpCl(((short)-1));
        }
    }

    @Override
    public String getDtUltBollRpInDb() {
        return ws.getParamCompDb().getDtUltBollRpInDb();
    }

    @Override
    public void setDtUltBollRpInDb(String dtUltBollRpInDb) {
        this.ws.getParamCompDb().setDtUltBollRpInDb(dtUltBollRpInDb);
    }

    @Override
    public String getDtUltBollRpInDbObj() {
        if (ws.getIndParamComp().getDtUltBollRpIn() >= 0) {
            return getDtUltBollRpInDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltBollRpInDbObj(String dtUltBollRpInDbObj) {
        if (dtUltBollRpInDbObj != null) {
            setDtUltBollRpInDb(dtUltBollRpInDbObj);
            ws.getIndParamComp().setDtUltBollRpIn(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltBollRpIn(((short)-1));
        }
    }

    @Override
    public String getDtUltBollRspClDb() {
        return ws.getParamCompDb().getDtUltBollRspClDb();
    }

    @Override
    public void setDtUltBollRspClDb(String dtUltBollRspClDb) {
        this.ws.getParamCompDb().setDtUltBollRspClDb(dtUltBollRspClDb);
    }

    @Override
    public String getDtUltBollRspClDbObj() {
        if (ws.getIndParamComp().getDtUltBollRspCl() >= 0) {
            return getDtUltBollRspClDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltBollRspClDbObj(String dtUltBollRspClDbObj) {
        if (dtUltBollRspClDbObj != null) {
            setDtUltBollRspClDb(dtUltBollRspClDbObj);
            ws.getIndParamComp().setDtUltBollRspCl(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltBollRspCl(((short)-1));
        }
    }

    @Override
    public String getDtUltBollRspInDb() {
        return ws.getParamCompDb().getDtUltBollRspInDb();
    }

    @Override
    public void setDtUltBollRspInDb(String dtUltBollRspInDb) {
        this.ws.getParamCompDb().setDtUltBollRspInDb(dtUltBollRspInDb);
    }

    @Override
    public String getDtUltBollRspInDbObj() {
        if (ws.getIndParamComp().getDtUltBollRspIn() >= 0) {
            return getDtUltBollRspInDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltBollRspInDbObj(String dtUltBollRspInDbObj) {
        if (dtUltBollRspInDbObj != null) {
            setDtUltBollRspInDb(dtUltBollRspInDbObj);
            ws.getIndParamComp().setDtUltBollRspIn(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltBollRspIn(((short)-1));
        }
    }

    @Override
    public String getDtUltBollSdIDb() {
        return ws.getParamCompDb().getDtUltBollSdIDb();
    }

    @Override
    public void setDtUltBollSdIDb(String dtUltBollSdIDb) {
        this.ws.getParamCompDb().setDtUltBollSdIDb(dtUltBollSdIDb);
    }

    @Override
    public String getDtUltBollSdIDbObj() {
        if (ws.getIndParamComp().getDtUltBollSdI() >= 0) {
            return getDtUltBollSdIDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltBollSdIDbObj(String dtUltBollSdIDbObj) {
        if (dtUltBollSdIDbObj != null) {
            setDtUltBollSdIDb(dtUltBollSdIDbObj);
            ws.getIndParamComp().setDtUltBollSdI(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltBollSdI(((short)-1));
        }
    }

    @Override
    public String getDtUltBollSdnlIDb() {
        return ws.getParamCompDb().getDtUltBollSdnlIDb();
    }

    @Override
    public void setDtUltBollSdnlIDb(String dtUltBollSdnlIDb) {
        this.ws.getParamCompDb().setDtUltBollSdnlIDb(dtUltBollSdnlIDb);
    }

    @Override
    public String getDtUltBollSdnlIDbObj() {
        if (ws.getIndParamComp().getDtUltBollSdnlI() >= 0) {
            return getDtUltBollSdnlIDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltBollSdnlIDbObj(String dtUltBollSdnlIDbObj) {
        if (dtUltBollSdnlIDbObj != null) {
            setDtUltBollSdnlIDb(dtUltBollSdnlIDbObj);
            ws.getIndParamComp().setDtUltBollSdnlI(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltBollSdnlI(((short)-1));
        }
    }

    @Override
    public String getDtUltBollSndenDb() {
        return ws.getParamCompDb().getDtUltBollSndenDb();
    }

    @Override
    public void setDtUltBollSndenDb(String dtUltBollSndenDb) {
        this.ws.getParamCompDb().setDtUltBollSndenDb(dtUltBollSndenDb);
    }

    @Override
    public String getDtUltBollSndenDbObj() {
        if (ws.getIndParamComp().getDtUltBollSnden() >= 0) {
            return getDtUltBollSndenDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltBollSndenDbObj(String dtUltBollSndenDbObj) {
        if (dtUltBollSndenDbObj != null) {
            setDtUltBollSndenDb(dtUltBollSndenDbObj);
            ws.getIndParamComp().setDtUltBollSnden(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltBollSnden(((short)-1));
        }
    }

    @Override
    public String getDtUltBollSndnlqDb() {
        return ws.getParamCompDb().getDtUltBollSndnlqDb();
    }

    @Override
    public void setDtUltBollSndnlqDb(String dtUltBollSndnlqDb) {
        this.ws.getParamCompDb().setDtUltBollSndnlqDb(dtUltBollSndnlqDb);
    }

    @Override
    public String getDtUltBollSndnlqDbObj() {
        if (ws.getIndParamComp().getDtUltBollSndnlq() >= 0) {
            return getDtUltBollSndnlqDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltBollSndnlqDbObj(String dtUltBollSndnlqDbObj) {
        if (dtUltBollSndnlqDbObj != null) {
            setDtUltBollSndnlqDb(dtUltBollSndnlqDbObj);
            ws.getIndParamComp().setDtUltBollSndnlq(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltBollSndnlq(((short)-1));
        }
    }

    @Override
    public String getDtUltBollStorDb() {
        return ws.getParamCompDb().getDtUltBollStorDb();
    }

    @Override
    public void setDtUltBollStorDb(String dtUltBollStorDb) {
        this.ws.getParamCompDb().setDtUltBollStorDb(dtUltBollStorDb);
    }

    @Override
    public String getDtUltBollStorDbObj() {
        if (ws.getIndParamComp().getDtUltBollStor() >= 0) {
            return getDtUltBollStorDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltBollStorDbObj(String dtUltBollStorDbObj) {
        if (dtUltBollStorDbObj != null) {
            setDtUltBollStorDb(dtUltBollStorDbObj);
            ws.getIndParamComp().setDtUltBollStor(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltBollStor(((short)-1));
        }
    }

    @Override
    public String getDtUltBollStorIDb() {
        return ws.getParamCompDb().getDtUltBollStorIDb();
    }

    @Override
    public void setDtUltBollStorIDb(String dtUltBollStorIDb) {
        this.ws.getParamCompDb().setDtUltBollStorIDb(dtUltBollStorIDb);
    }

    @Override
    public String getDtUltBollStorIDbObj() {
        if (ws.getIndParamComp().getDtUltBollStorI() >= 0) {
            return getDtUltBollStorIDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltBollStorIDbObj(String dtUltBollStorIDbObj) {
        if (dtUltBollStorIDbObj != null) {
            setDtUltBollStorIDb(dtUltBollStorIDbObj);
            ws.getIndParamComp().setDtUltBollStorI(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltBollStorI(((short)-1));
        }
    }

    @Override
    public String getDtUltEcIlCollDb() {
        return ws.getParamCompDb().getDtUltEcIlCollDb();
    }

    @Override
    public void setDtUltEcIlCollDb(String dtUltEcIlCollDb) {
        this.ws.getParamCompDb().setDtUltEcIlCollDb(dtUltEcIlCollDb);
    }

    @Override
    public String getDtUltEcIlCollDbObj() {
        if (ws.getIndParamComp().getDtUltEcIlColl() >= 0) {
            return getDtUltEcIlCollDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltEcIlCollDbObj(String dtUltEcIlCollDbObj) {
        if (dtUltEcIlCollDbObj != null) {
            setDtUltEcIlCollDb(dtUltEcIlCollDbObj);
            ws.getIndParamComp().setDtUltEcIlColl(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltEcIlColl(((short)-1));
        }
    }

    @Override
    public String getDtUltEcIlIndDb() {
        return ws.getParamCompDb().getDtUltEcIlIndDb();
    }

    @Override
    public void setDtUltEcIlIndDb(String dtUltEcIlIndDb) {
        this.ws.getParamCompDb().setDtUltEcIlIndDb(dtUltEcIlIndDb);
    }

    @Override
    public String getDtUltEcIlIndDbObj() {
        if (ws.getIndParamComp().getDtUltEcIlInd() >= 0) {
            return getDtUltEcIlIndDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltEcIlIndDbObj(String dtUltEcIlIndDbObj) {
        if (dtUltEcIlIndDbObj != null) {
            setDtUltEcIlIndDb(dtUltEcIlIndDbObj);
            ws.getIndParamComp().setDtUltEcIlInd(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltEcIlInd(((short)-1));
        }
    }

    @Override
    public String getDtUltEcMrmCollDb() {
        return ws.getParamCompDb().getDtUltEcMrmCollDb();
    }

    @Override
    public void setDtUltEcMrmCollDb(String dtUltEcMrmCollDb) {
        this.ws.getParamCompDb().setDtUltEcMrmCollDb(dtUltEcMrmCollDb);
    }

    @Override
    public String getDtUltEcMrmCollDbObj() {
        if (ws.getIndParamComp().getDtUltEcMrmColl() >= 0) {
            return getDtUltEcMrmCollDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltEcMrmCollDbObj(String dtUltEcMrmCollDbObj) {
        if (dtUltEcMrmCollDbObj != null) {
            setDtUltEcMrmCollDb(dtUltEcMrmCollDbObj);
            ws.getIndParamComp().setDtUltEcMrmColl(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltEcMrmColl(((short)-1));
        }
    }

    @Override
    public String getDtUltEcMrmIndDb() {
        return ws.getParamCompDb().getDtUltEcMrmIndDb();
    }

    @Override
    public void setDtUltEcMrmIndDb(String dtUltEcMrmIndDb) {
        this.ws.getParamCompDb().setDtUltEcMrmIndDb(dtUltEcMrmIndDb);
    }

    @Override
    public String getDtUltEcMrmIndDbObj() {
        if (ws.getIndParamComp().getDtUltEcMrmInd() >= 0) {
            return getDtUltEcMrmIndDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltEcMrmIndDbObj(String dtUltEcMrmIndDbObj) {
        if (dtUltEcMrmIndDbObj != null) {
            setDtUltEcMrmIndDb(dtUltEcMrmIndDbObj);
            ws.getIndParamComp().setDtUltEcMrmInd(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltEcMrmInd(((short)-1));
        }
    }

    @Override
    public String getDtUltEcRivCollDb() {
        return ws.getParamCompDb().getDtUltEcRivCollDb();
    }

    @Override
    public void setDtUltEcRivCollDb(String dtUltEcRivCollDb) {
        this.ws.getParamCompDb().setDtUltEcRivCollDb(dtUltEcRivCollDb);
    }

    @Override
    public String getDtUltEcRivCollDbObj() {
        if (ws.getIndParamComp().getDtUltEcRivColl() >= 0) {
            return getDtUltEcRivCollDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltEcRivCollDbObj(String dtUltEcRivCollDbObj) {
        if (dtUltEcRivCollDbObj != null) {
            setDtUltEcRivCollDb(dtUltEcRivCollDbObj);
            ws.getIndParamComp().setDtUltEcRivColl(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltEcRivColl(((short)-1));
        }
    }

    @Override
    public String getDtUltEcRivIndDb() {
        return ws.getParamCompDb().getDtUltEcRivIndDb();
    }

    @Override
    public void setDtUltEcRivIndDb(String dtUltEcRivIndDb) {
        this.ws.getParamCompDb().setDtUltEcRivIndDb(dtUltEcRivIndDb);
    }

    @Override
    public String getDtUltEcRivIndDbObj() {
        if (ws.getIndParamComp().getDtUltEcRivInd() >= 0) {
            return getDtUltEcRivIndDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltEcRivIndDbObj(String dtUltEcRivIndDbObj) {
        if (dtUltEcRivIndDbObj != null) {
            setDtUltEcRivIndDb(dtUltEcRivIndDbObj);
            ws.getIndParamComp().setDtUltEcRivInd(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltEcRivInd(((short)-1));
        }
    }

    @Override
    public String getDtUltEcTcmCollDb() {
        return ws.getParamCompDb().getDtUltEcTcmCollDb();
    }

    @Override
    public void setDtUltEcTcmCollDb(String dtUltEcTcmCollDb) {
        this.ws.getParamCompDb().setDtUltEcTcmCollDb(dtUltEcTcmCollDb);
    }

    @Override
    public String getDtUltEcTcmCollDbObj() {
        if (ws.getIndParamComp().getDtUltEcTcmColl() >= 0) {
            return getDtUltEcTcmCollDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltEcTcmCollDbObj(String dtUltEcTcmCollDbObj) {
        if (dtUltEcTcmCollDbObj != null) {
            setDtUltEcTcmCollDb(dtUltEcTcmCollDbObj);
            ws.getIndParamComp().setDtUltEcTcmColl(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltEcTcmColl(((short)-1));
        }
    }

    @Override
    public String getDtUltEcTcmIndDb() {
        return ws.getParamCompDb().getDtUltEcTcmIndDb();
    }

    @Override
    public void setDtUltEcTcmIndDb(String dtUltEcTcmIndDb) {
        this.ws.getParamCompDb().setDtUltEcTcmIndDb(dtUltEcTcmIndDb);
    }

    @Override
    public String getDtUltEcTcmIndDbObj() {
        if (ws.getIndParamComp().getDtUltEcTcmInd() >= 0) {
            return getDtUltEcTcmIndDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltEcTcmIndDbObj(String dtUltEcTcmIndDbObj) {
        if (dtUltEcTcmIndDbObj != null) {
            setDtUltEcTcmIndDb(dtUltEcTcmIndDbObj);
            ws.getIndParamComp().setDtUltEcTcmInd(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltEcTcmInd(((short)-1));
        }
    }

    @Override
    public String getDtUltEcUlCollDb() {
        return ws.getParamCompDb().getDtUltEcUlCollDb();
    }

    @Override
    public void setDtUltEcUlCollDb(String dtUltEcUlCollDb) {
        this.ws.getParamCompDb().setDtUltEcUlCollDb(dtUltEcUlCollDb);
    }

    @Override
    public String getDtUltEcUlCollDbObj() {
        if (ws.getIndParamComp().getDtUltEcUlColl() >= 0) {
            return getDtUltEcUlCollDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltEcUlCollDbObj(String dtUltEcUlCollDbObj) {
        if (dtUltEcUlCollDbObj != null) {
            setDtUltEcUlCollDb(dtUltEcUlCollDbObj);
            ws.getIndParamComp().setDtUltEcUlColl(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltEcUlColl(((short)-1));
        }
    }

    @Override
    public String getDtUltEcUlIndDb() {
        return ws.getParamCompDb().getDtUltEcUlIndDb();
    }

    @Override
    public void setDtUltEcUlIndDb(String dtUltEcUlIndDb) {
        this.ws.getParamCompDb().setDtUltEcUlIndDb(dtUltEcUlIndDb);
    }

    @Override
    public String getDtUltEcUlIndDbObj() {
        if (ws.getIndParamComp().getDtUltEcUlInd() >= 0) {
            return getDtUltEcUlIndDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltEcUlIndDbObj(String dtUltEcUlIndDbObj) {
        if (dtUltEcUlIndDbObj != null) {
            setDtUltEcUlIndDb(dtUltEcUlIndDbObj);
            ws.getIndParamComp().setDtUltEcUlInd(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltEcUlInd(((short)-1));
        }
    }

    @Override
    public String getDtUltElabAt92CDb() {
        return ws.getParamCompDb().getDtUltElabAt92CDb();
    }

    @Override
    public void setDtUltElabAt92CDb(String dtUltElabAt92CDb) {
        this.ws.getParamCompDb().setDtUltElabAt92CDb(dtUltElabAt92CDb);
    }

    @Override
    public String getDtUltElabAt92CDbObj() {
        if (ws.getIndParamComp().getDtUltElabAt92C() >= 0) {
            return getDtUltElabAt92CDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltElabAt92CDbObj(String dtUltElabAt92CDbObj) {
        if (dtUltElabAt92CDbObj != null) {
            setDtUltElabAt92CDb(dtUltElabAt92CDbObj);
            ws.getIndParamComp().setDtUltElabAt92C(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltElabAt92C(((short)-1));
        }
    }

    @Override
    public String getDtUltElabAt92IDb() {
        return ws.getParamCompDb().getDtUltElabAt92IDb();
    }

    @Override
    public void setDtUltElabAt92IDb(String dtUltElabAt92IDb) {
        this.ws.getParamCompDb().setDtUltElabAt92IDb(dtUltElabAt92IDb);
    }

    @Override
    public String getDtUltElabAt92IDbObj() {
        if (ws.getIndParamComp().getDtUltElabAt92I() >= 0) {
            return getDtUltElabAt92IDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltElabAt92IDbObj(String dtUltElabAt92IDbObj) {
        if (dtUltElabAt92IDbObj != null) {
            setDtUltElabAt92IDb(dtUltElabAt92IDbObj);
            ws.getIndParamComp().setDtUltElabAt92I(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltElabAt92I(((short)-1));
        }
    }

    @Override
    public String getDtUltElabAt93CDb() {
        return ws.getParamCompDb().getDtUltElabAt93CDb();
    }

    @Override
    public void setDtUltElabAt93CDb(String dtUltElabAt93CDb) {
        this.ws.getParamCompDb().setDtUltElabAt93CDb(dtUltElabAt93CDb);
    }

    @Override
    public String getDtUltElabAt93CDbObj() {
        if (ws.getIndParamComp().getDtUltElabAt93C() >= 0) {
            return getDtUltElabAt93CDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltElabAt93CDbObj(String dtUltElabAt93CDbObj) {
        if (dtUltElabAt93CDbObj != null) {
            setDtUltElabAt93CDb(dtUltElabAt93CDbObj);
            ws.getIndParamComp().setDtUltElabAt93C(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltElabAt93C(((short)-1));
        }
    }

    @Override
    public String getDtUltElabAt93IDb() {
        return ws.getParamCompDb().getDtUltElabAt93IDb();
    }

    @Override
    public void setDtUltElabAt93IDb(String dtUltElabAt93IDb) {
        this.ws.getParamCompDb().setDtUltElabAt93IDb(dtUltElabAt93IDb);
    }

    @Override
    public String getDtUltElabAt93IDbObj() {
        if (ws.getIndParamComp().getDtUltElabAt93I() >= 0) {
            return getDtUltElabAt93IDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltElabAt93IDbObj(String dtUltElabAt93IDbObj) {
        if (dtUltElabAt93IDbObj != null) {
            setDtUltElabAt93IDb(dtUltElabAt93IDbObj);
            ws.getIndParamComp().setDtUltElabAt93I(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltElabAt93I(((short)-1));
        }
    }

    @Override
    public String getDtUltElabCommefDb() {
        return ws.getParamCompDb().getDtUltElabCommefDb();
    }

    @Override
    public void setDtUltElabCommefDb(String dtUltElabCommefDb) {
        this.ws.getParamCompDb().setDtUltElabCommefDb(dtUltElabCommefDb);
    }

    @Override
    public String getDtUltElabCommefDbObj() {
        if (ws.getIndParamComp().getDtUltElabCommef() >= 0) {
            return getDtUltElabCommefDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltElabCommefDbObj(String dtUltElabCommefDbObj) {
        if (dtUltElabCommefDbObj != null) {
            setDtUltElabCommefDb(dtUltElabCommefDbObj);
            ws.getIndParamComp().setDtUltElabCommef(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltElabCommef(((short)-1));
        }
    }

    @Override
    public String getDtUltElabCosAtDb() {
        return ws.getParamCompDb().getDtUltElabCosAtDb();
    }

    @Override
    public void setDtUltElabCosAtDb(String dtUltElabCosAtDb) {
        this.ws.getParamCompDb().setDtUltElabCosAtDb(dtUltElabCosAtDb);
    }

    @Override
    public String getDtUltElabCosAtDbObj() {
        if (ws.getIndParamComp().getDtUltElabCosAt() >= 0) {
            return getDtUltElabCosAtDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltElabCosAtDbObj(String dtUltElabCosAtDbObj) {
        if (dtUltElabCosAtDbObj != null) {
            setDtUltElabCosAtDb(dtUltElabCosAtDbObj);
            ws.getIndParamComp().setDtUltElabCosAt(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltElabCosAt(((short)-1));
        }
    }

    @Override
    public String getDtUltElabCosStDb() {
        return ws.getParamCompDb().getDtUltElabCosStDb();
    }

    @Override
    public void setDtUltElabCosStDb(String dtUltElabCosStDb) {
        this.ws.getParamCompDb().setDtUltElabCosStDb(dtUltElabCosStDb);
    }

    @Override
    public String getDtUltElabCosStDbObj() {
        if (ws.getIndParamComp().getDtUltElabCosSt() >= 0) {
            return getDtUltElabCosStDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltElabCosStDbObj(String dtUltElabCosStDbObj) {
        if (dtUltElabCosStDbObj != null) {
            setDtUltElabCosStDb(dtUltElabCosStDbObj);
            ws.getIndParamComp().setDtUltElabCosSt(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltElabCosSt(((short)-1));
        }
    }

    @Override
    public String getDtUltElabLiqmefDb() {
        return ws.getParamCompDb().getDtUltElabLiqmefDb();
    }

    @Override
    public void setDtUltElabLiqmefDb(String dtUltElabLiqmefDb) {
        this.ws.getParamCompDb().setDtUltElabLiqmefDb(dtUltElabLiqmefDb);
    }

    @Override
    public String getDtUltElabLiqmefDbObj() {
        if (ws.getIndParamComp().getDtUltElabLiqmef() >= 0) {
            return getDtUltElabLiqmefDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltElabLiqmefDbObj(String dtUltElabLiqmefDbObj) {
        if (dtUltElabLiqmefDbObj != null) {
            setDtUltElabLiqmefDb(dtUltElabLiqmefDbObj);
            ws.getIndParamComp().setDtUltElabLiqmef(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltElabLiqmef(((short)-1));
        }
    }

    @Override
    public String getDtUltElabPaspasDb() {
        return ws.getParamCompDb().getDtUltElabPaspasDb();
    }

    @Override
    public void setDtUltElabPaspasDb(String dtUltElabPaspasDb) {
        this.ws.getParamCompDb().setDtUltElabPaspasDb(dtUltElabPaspasDb);
    }

    @Override
    public String getDtUltElabPaspasDbObj() {
        if (ws.getIndParamComp().getDtUltElabPaspas() >= 0) {
            return getDtUltElabPaspasDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltElabPaspasDbObj(String dtUltElabPaspasDbObj) {
        if (dtUltElabPaspasDbObj != null) {
            setDtUltElabPaspasDb(dtUltElabPaspasDbObj);
            ws.getIndParamComp().setDtUltElabPaspas(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltElabPaspas(((short)-1));
        }
    }

    @Override
    public String getDtUltElabPrAutDb() {
        return ws.getParamCompDb().getDtUltElabPrAutDb();
    }

    @Override
    public void setDtUltElabPrAutDb(String dtUltElabPrAutDb) {
        this.ws.getParamCompDb().setDtUltElabPrAutDb(dtUltElabPrAutDb);
    }

    @Override
    public String getDtUltElabPrAutDbObj() {
        if (ws.getIndParamComp().getDtUltElabPrAut() >= 0) {
            return getDtUltElabPrAutDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltElabPrAutDbObj(String dtUltElabPrAutDbObj) {
        if (dtUltElabPrAutDbObj != null) {
            setDtUltElabPrAutDb(dtUltElabPrAutDbObj);
            ws.getIndParamComp().setDtUltElabPrAut(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltElabPrAut(((short)-1));
        }
    }

    @Override
    public String getDtUltElabPrConDb() {
        return ws.getParamCompDb().getDtUltElabPrConDb();
    }

    @Override
    public void setDtUltElabPrConDb(String dtUltElabPrConDb) {
        this.ws.getParamCompDb().setDtUltElabPrConDb(dtUltElabPrConDb);
    }

    @Override
    public String getDtUltElabPrConDbObj() {
        if (ws.getIndParamComp().getDtUltElabPrCon() >= 0) {
            return getDtUltElabPrConDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltElabPrConDbObj(String dtUltElabPrConDbObj) {
        if (dtUltElabPrConDbObj != null) {
            setDtUltElabPrConDb(dtUltElabPrConDbObj);
            ws.getIndParamComp().setDtUltElabPrCon(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltElabPrCon(((short)-1));
        }
    }

    @Override
    public String getDtUltElabPrlcosDb() {
        return ws.getParamCompDb().getDtUltElabPrlcosDb();
    }

    @Override
    public void setDtUltElabPrlcosDb(String dtUltElabPrlcosDb) {
        this.ws.getParamCompDb().setDtUltElabPrlcosDb(dtUltElabPrlcosDb);
    }

    @Override
    public String getDtUltElabPrlcosDbObj() {
        if (ws.getIndParamComp().getDtUltElabPrlcos() >= 0) {
            return getDtUltElabPrlcosDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltElabPrlcosDbObj(String dtUltElabPrlcosDbObj) {
        if (dtUltElabPrlcosDbObj != null) {
            setDtUltElabPrlcosDb(dtUltElabPrlcosDbObj);
            ws.getIndParamComp().setDtUltElabPrlcos(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltElabPrlcos(((short)-1));
        }
    }

    @Override
    public String getDtUltElabRedproDb() {
        return ws.getParamCompDb().getDtUltElabRedproDb();
    }

    @Override
    public void setDtUltElabRedproDb(String dtUltElabRedproDb) {
        this.ws.getParamCompDb().setDtUltElabRedproDb(dtUltElabRedproDb);
    }

    @Override
    public String getDtUltElabRedproDbObj() {
        if (ws.getIndParamComp().getDtUltElabRedpro() >= 0) {
            return getDtUltElabRedproDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltElabRedproDbObj(String dtUltElabRedproDbObj) {
        if (dtUltElabRedproDbObj != null) {
            setDtUltElabRedproDb(dtUltElabRedproDbObj);
            ws.getIndParamComp().setDtUltElabRedpro(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltElabRedpro(((short)-1));
        }
    }

    @Override
    public String getDtUltElabSpeInDb() {
        return ws.getParamCompDb().getDtUltElabSpeInDb();
    }

    @Override
    public void setDtUltElabSpeInDb(String dtUltElabSpeInDb) {
        this.ws.getParamCompDb().setDtUltElabSpeInDb(dtUltElabSpeInDb);
    }

    @Override
    public String getDtUltElabSpeInDbObj() {
        if (ws.getIndParamComp().getDtUltElabSpeIn() >= 0) {
            return getDtUltElabSpeInDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltElabSpeInDbObj(String dtUltElabSpeInDbObj) {
        if (dtUltElabSpeInDbObj != null) {
            setDtUltElabSpeInDb(dtUltElabSpeInDbObj);
            ws.getIndParamComp().setDtUltElabSpeIn(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltElabSpeIn(((short)-1));
        }
    }

    @Override
    public String getDtUltElabTakePDb() {
        return ws.getParamCompDb().getDtUltElabTakePDb();
    }

    @Override
    public void setDtUltElabTakePDb(String dtUltElabTakePDb) {
        this.ws.getParamCompDb().setDtUltElabTakePDb(dtUltElabTakePDb);
    }

    @Override
    public String getDtUltElabTakePDbObj() {
        if (ws.getIndParamComp().getDtUltElabTakeP() >= 0) {
            return getDtUltElabTakePDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltElabTakePDbObj(String dtUltElabTakePDbObj) {
        if (dtUltElabTakePDbObj != null) {
            setDtUltElabTakePDb(dtUltElabTakePDbObj);
            ws.getIndParamComp().setDtUltElabTakeP(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltElabTakeP(((short)-1));
        }
    }

    @Override
    public String getDtUltEstrDecCoDb() {
        return ws.getParamCompDb().getDtUltEstrDecCoDb();
    }

    @Override
    public void setDtUltEstrDecCoDb(String dtUltEstrDecCoDb) {
        this.ws.getParamCompDb().setDtUltEstrDecCoDb(dtUltEstrDecCoDb);
    }

    @Override
    public String getDtUltEstrDecCoDbObj() {
        if (ws.getIndParamComp().getDtUltEstrDecCo() >= 0) {
            return getDtUltEstrDecCoDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltEstrDecCoDbObj(String dtUltEstrDecCoDbObj) {
        if (dtUltEstrDecCoDbObj != null) {
            setDtUltEstrDecCoDb(dtUltEstrDecCoDbObj);
            ws.getIndParamComp().setDtUltEstrDecCo(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltEstrDecCo(((short)-1));
        }
    }

    @Override
    public String getDtUltEstrazFugDb() {
        return ws.getParamCompDb().getDtUltEstrazFugDb();
    }

    @Override
    public void setDtUltEstrazFugDb(String dtUltEstrazFugDb) {
        this.ws.getParamCompDb().setDtUltEstrazFugDb(dtUltEstrazFugDb);
    }

    @Override
    public String getDtUltEstrazFugDbObj() {
        if (ws.getIndParamComp().getDtUltEstrazFug() >= 0) {
            return getDtUltEstrazFugDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltEstrazFugDbObj(String dtUltEstrazFugDbObj) {
        if (dtUltEstrazFugDbObj != null) {
            setDtUltEstrazFugDb(dtUltEstrazFugDbObj);
            ws.getIndParamComp().setDtUltEstrazFug(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltEstrazFug(((short)-1));
        }
    }

    @Override
    public String getDtUltQtzoClDb() {
        return ws.getParamCompDb().getDtUltQtzoClDb();
    }

    @Override
    public void setDtUltQtzoClDb(String dtUltQtzoClDb) {
        this.ws.getParamCompDb().setDtUltQtzoClDb(dtUltQtzoClDb);
    }

    @Override
    public String getDtUltQtzoClDbObj() {
        if (ws.getIndParamComp().getDtUltQtzoCl() >= 0) {
            return getDtUltQtzoClDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltQtzoClDbObj(String dtUltQtzoClDbObj) {
        if (dtUltQtzoClDbObj != null) {
            setDtUltQtzoClDb(dtUltQtzoClDbObj);
            ws.getIndParamComp().setDtUltQtzoCl(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltQtzoCl(((short)-1));
        }
    }

    @Override
    public String getDtUltQtzoInDb() {
        return ws.getParamCompDb().getDtUltQtzoInDb();
    }

    @Override
    public void setDtUltQtzoInDb(String dtUltQtzoInDb) {
        this.ws.getParamCompDb().setDtUltQtzoInDb(dtUltQtzoInDb);
    }

    @Override
    public String getDtUltQtzoInDbObj() {
        if (ws.getIndParamComp().getDtUltQtzoIn() >= 0) {
            return getDtUltQtzoInDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltQtzoInDbObj(String dtUltQtzoInDbObj) {
        if (dtUltQtzoInDbObj != null) {
            setDtUltQtzoInDb(dtUltQtzoInDbObj);
            ws.getIndParamComp().setDtUltQtzoIn(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltQtzoIn(((short)-1));
        }
    }

    @Override
    public String getDtUltRiclPreDb() {
        return ws.getParamCompDb().getDtUltRiclPreDb();
    }

    @Override
    public void setDtUltRiclPreDb(String dtUltRiclPreDb) {
        this.ws.getParamCompDb().setDtUltRiclPreDb(dtUltRiclPreDb);
    }

    @Override
    public String getDtUltRiclPreDbObj() {
        if (ws.getIndParamComp().getDtUltRiclPre() >= 0) {
            return getDtUltRiclPreDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltRiclPreDbObj(String dtUltRiclPreDbObj) {
        if (dtUltRiclPreDbObj != null) {
            setDtUltRiclPreDb(dtUltRiclPreDbObj);
            ws.getIndParamComp().setDtUltRiclPre(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltRiclPre(((short)-1));
        }
    }

    @Override
    public String getDtUltRiclRiassDb() {
        return ws.getParamCompDb().getDtUltRiclRiassDb();
    }

    @Override
    public void setDtUltRiclRiassDb(String dtUltRiclRiassDb) {
        this.ws.getParamCompDb().setDtUltRiclRiassDb(dtUltRiclRiassDb);
    }

    @Override
    public String getDtUltRiclRiassDbObj() {
        if (ws.getIndParamComp().getDtUltRiclRiass() >= 0) {
            return getDtUltRiclRiassDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltRiclRiassDbObj(String dtUltRiclRiassDbObj) {
        if (dtUltRiclRiassDbObj != null) {
            setDtUltRiclRiassDb(dtUltRiclRiassDbObj);
            ws.getIndParamComp().setDtUltRiclRiass(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltRiclRiass(((short)-1));
        }
    }

    @Override
    public String getDtUltRinnCollDb() {
        return ws.getParamCompDb().getDtUltRinnCollDb();
    }

    @Override
    public void setDtUltRinnCollDb(String dtUltRinnCollDb) {
        this.ws.getParamCompDb().setDtUltRinnCollDb(dtUltRinnCollDb);
    }

    @Override
    public String getDtUltRinnCollDbObj() {
        if (ws.getIndParamComp().getDtUltRinnColl() >= 0) {
            return getDtUltRinnCollDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltRinnCollDbObj(String dtUltRinnCollDbObj) {
        if (dtUltRinnCollDbObj != null) {
            setDtUltRinnCollDb(dtUltRinnCollDbObj);
            ws.getIndParamComp().setDtUltRinnColl(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltRinnColl(((short)-1));
        }
    }

    @Override
    public String getDtUltRinnGaracDb() {
        return ws.getParamCompDb().getDtUltRinnGaracDb();
    }

    @Override
    public void setDtUltRinnGaracDb(String dtUltRinnGaracDb) {
        this.ws.getParamCompDb().setDtUltRinnGaracDb(dtUltRinnGaracDb);
    }

    @Override
    public String getDtUltRinnGaracDbObj() {
        if (ws.getIndParamComp().getDtUltRinnGarac() >= 0) {
            return getDtUltRinnGaracDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltRinnGaracDbObj(String dtUltRinnGaracDbObj) {
        if (dtUltRinnGaracDbObj != null) {
            setDtUltRinnGaracDb(dtUltRinnGaracDbObj);
            ws.getIndParamComp().setDtUltRinnGarac(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltRinnGarac(((short)-1));
        }
    }

    @Override
    public String getDtUltRinnTacDb() {
        return ws.getParamCompDb().getDtUltRinnTacDb();
    }

    @Override
    public void setDtUltRinnTacDb(String dtUltRinnTacDb) {
        this.ws.getParamCompDb().setDtUltRinnTacDb(dtUltRinnTacDb);
    }

    @Override
    public String getDtUltRinnTacDbObj() {
        if (ws.getIndParamComp().getDtUltRinnTac() >= 0) {
            return getDtUltRinnTacDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltRinnTacDbObj(String dtUltRinnTacDbObj) {
        if (dtUltRinnTacDbObj != null) {
            setDtUltRinnTacDb(dtUltRinnTacDbObj);
            ws.getIndParamComp().setDtUltRinnTac(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltRinnTac(((short)-1));
        }
    }

    @Override
    public String getDtUltRivalClDb() {
        return ws.getParamCompDb().getDtUltRivalClDb();
    }

    @Override
    public void setDtUltRivalClDb(String dtUltRivalClDb) {
        this.ws.getParamCompDb().setDtUltRivalClDb(dtUltRivalClDb);
    }

    @Override
    public String getDtUltRivalClDbObj() {
        if (ws.getIndParamComp().getDtUltRivalCl() >= 0) {
            return getDtUltRivalClDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltRivalClDbObj(String dtUltRivalClDbObj) {
        if (dtUltRivalClDbObj != null) {
            setDtUltRivalClDb(dtUltRivalClDbObj);
            ws.getIndParamComp().setDtUltRivalCl(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltRivalCl(((short)-1));
        }
    }

    @Override
    public String getDtUltRivalInDb() {
        return ws.getParamCompDb().getDtUltRivalInDb();
    }

    @Override
    public void setDtUltRivalInDb(String dtUltRivalInDb) {
        this.ws.getParamCompDb().setDtUltRivalInDb(dtUltRivalInDb);
    }

    @Override
    public String getDtUltRivalInDbObj() {
        if (ws.getIndParamComp().getDtUltRivalIn() >= 0) {
            return getDtUltRivalInDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltRivalInDbObj(String dtUltRivalInDbObj) {
        if (dtUltRivalInDbObj != null) {
            setDtUltRivalInDb(dtUltRivalInDbObj);
            ws.getIndParamComp().setDtUltRivalIn(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltRivalIn(((short)-1));
        }
    }

    @Override
    public String getDtUltTabulRiassDb() {
        return ws.getParamCompDb().getDtUltTabulRiassDb();
    }

    @Override
    public void setDtUltTabulRiassDb(String dtUltTabulRiassDb) {
        this.ws.getParamCompDb().setDtUltTabulRiassDb(dtUltTabulRiassDb);
    }

    @Override
    public String getDtUltTabulRiassDbObj() {
        if (ws.getIndParamComp().getDtUltTabulRiass() >= 0) {
            return getDtUltTabulRiassDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltTabulRiassDbObj(String dtUltTabulRiassDbObj) {
        if (dtUltTabulRiassDbObj != null) {
            setDtUltTabulRiassDb(dtUltTabulRiassDbObj);
            ws.getIndParamComp().setDtUltTabulRiass(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltTabulRiass(((short)-1));
        }
    }

    @Override
    public String getDtUltcBnsfdtClDb() {
        return ws.getParamCompDb().getDtUltcBnsfdtClDb();
    }

    @Override
    public void setDtUltcBnsfdtClDb(String dtUltcBnsfdtClDb) {
        this.ws.getParamCompDb().setDtUltcBnsfdtClDb(dtUltcBnsfdtClDb);
    }

    @Override
    public String getDtUltcBnsfdtClDbObj() {
        if (ws.getIndParamComp().getDtUltcBnsfdtCl() >= 0) {
            return getDtUltcBnsfdtClDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltcBnsfdtClDbObj(String dtUltcBnsfdtClDbObj) {
        if (dtUltcBnsfdtClDbObj != null) {
            setDtUltcBnsfdtClDb(dtUltcBnsfdtClDbObj);
            ws.getIndParamComp().setDtUltcBnsfdtCl(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltcBnsfdtCl(((short)-1));
        }
    }

    @Override
    public String getDtUltcBnsfdtInDb() {
        return ws.getParamCompDb().getDtUltcBnsfdtInDb();
    }

    @Override
    public void setDtUltcBnsfdtInDb(String dtUltcBnsfdtInDb) {
        this.ws.getParamCompDb().setDtUltcBnsfdtInDb(dtUltcBnsfdtInDb);
    }

    @Override
    public String getDtUltcBnsfdtInDbObj() {
        if (ws.getIndParamComp().getDtUltcBnsfdtIn() >= 0) {
            return getDtUltcBnsfdtInDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltcBnsfdtInDbObj(String dtUltcBnsfdtInDbObj) {
        if (dtUltcBnsfdtInDbObj != null) {
            setDtUltcBnsfdtInDb(dtUltcBnsfdtInDbObj);
            ws.getIndParamComp().setDtUltcBnsfdtIn(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltcBnsfdtIn(((short)-1));
        }
    }

    @Override
    public String getDtUltcBnsricClDb() {
        return ws.getParamCompDb().getDtUltcBnsricClDb();
    }

    @Override
    public void setDtUltcBnsricClDb(String dtUltcBnsricClDb) {
        this.ws.getParamCompDb().setDtUltcBnsricClDb(dtUltcBnsricClDb);
    }

    @Override
    public String getDtUltcBnsricClDbObj() {
        if (ws.getIndParamComp().getDtUltcBnsricCl() >= 0) {
            return getDtUltcBnsricClDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltcBnsricClDbObj(String dtUltcBnsricClDbObj) {
        if (dtUltcBnsricClDbObj != null) {
            setDtUltcBnsricClDb(dtUltcBnsricClDbObj);
            ws.getIndParamComp().setDtUltcBnsricCl(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltcBnsricCl(((short)-1));
        }
    }

    @Override
    public String getDtUltcBnsricInDb() {
        return ws.getParamCompDb().getDtUltcBnsricInDb();
    }

    @Override
    public void setDtUltcBnsricInDb(String dtUltcBnsricInDb) {
        this.ws.getParamCompDb().setDtUltcBnsricInDb(dtUltcBnsricInDb);
    }

    @Override
    public String getDtUltcBnsricInDbObj() {
        if (ws.getIndParamComp().getDtUltcBnsricIn() >= 0) {
            return getDtUltcBnsricInDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltcBnsricInDbObj(String dtUltcBnsricInDbObj) {
        if (dtUltcBnsricInDbObj != null) {
            setDtUltcBnsricInDb(dtUltcBnsricInDbObj);
            ws.getIndParamComp().setDtUltcBnsricIn(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltcBnsricIn(((short)-1));
        }
    }

    @Override
    public String getDtUltcIsClDb() {
        return ws.getParamCompDb().getDtUltcIsClDb();
    }

    @Override
    public void setDtUltcIsClDb(String dtUltcIsClDb) {
        this.ws.getParamCompDb().setDtUltcIsClDb(dtUltcIsClDb);
    }

    @Override
    public String getDtUltcIsClDbObj() {
        if (ws.getIndParamComp().getDtUltcIsCl() >= 0) {
            return getDtUltcIsClDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltcIsClDbObj(String dtUltcIsClDbObj) {
        if (dtUltcIsClDbObj != null) {
            setDtUltcIsClDb(dtUltcIsClDbObj);
            ws.getIndParamComp().setDtUltcIsCl(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltcIsCl(((short)-1));
        }
    }

    @Override
    public String getDtUltcIsInDb() {
        return ws.getParamCompDb().getDtUltcIsInDb();
    }

    @Override
    public void setDtUltcIsInDb(String dtUltcIsInDb) {
        this.ws.getParamCompDb().setDtUltcIsInDb(dtUltcIsInDb);
    }

    @Override
    public String getDtUltcIsInDbObj() {
        if (ws.getIndParamComp().getDtUltcIsIn() >= 0) {
            return getDtUltcIsInDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltcIsInDbObj(String dtUltcIsInDbObj) {
        if (dtUltcIsInDbObj != null) {
            setDtUltcIsInDb(dtUltcIsInDbObj);
            ws.getIndParamComp().setDtUltcIsIn(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltcIsIn(((short)-1));
        }
    }

    @Override
    public String getDtUltcMarsolDb() {
        return ws.getParamCompDb().getDtUltcMarsolDb();
    }

    @Override
    public void setDtUltcMarsolDb(String dtUltcMarsolDb) {
        this.ws.getParamCompDb().setDtUltcMarsolDb(dtUltcMarsolDb);
    }

    @Override
    public String getDtUltcMarsolDbObj() {
        if (ws.getIndParamComp().getDtUltcMarsol() >= 0) {
            return getDtUltcMarsolDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltcMarsolDbObj(String dtUltcMarsolDbObj) {
        if (dtUltcMarsolDbObj != null) {
            setDtUltcMarsolDb(dtUltcMarsolDbObj);
            ws.getIndParamComp().setDtUltcMarsol(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltcMarsol(((short)-1));
        }
    }

    @Override
    public String getDtUltcPildiAaCDb() {
        return ws.getParamCompDb().getDtUltcPildiAaCDb();
    }

    @Override
    public void setDtUltcPildiAaCDb(String dtUltcPildiAaCDb) {
        this.ws.getParamCompDb().setDtUltcPildiAaCDb(dtUltcPildiAaCDb);
    }

    @Override
    public String getDtUltcPildiAaCDbObj() {
        if (ws.getIndParamComp().getDtUltcPildiAaC() >= 0) {
            return getDtUltcPildiAaCDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltcPildiAaCDbObj(String dtUltcPildiAaCDbObj) {
        if (dtUltcPildiAaCDbObj != null) {
            setDtUltcPildiAaCDb(dtUltcPildiAaCDbObj);
            ws.getIndParamComp().setDtUltcPildiAaC(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltcPildiAaC(((short)-1));
        }
    }

    @Override
    public String getDtUltcPildiAaIDb() {
        return ws.getParamCompDb().getDtUltcPildiAaIDb();
    }

    @Override
    public void setDtUltcPildiAaIDb(String dtUltcPildiAaIDb) {
        this.ws.getParamCompDb().setDtUltcPildiAaIDb(dtUltcPildiAaIDb);
    }

    @Override
    public String getDtUltcPildiAaIDbObj() {
        if (ws.getIndParamComp().getDtUltcPildiAaI() >= 0) {
            return getDtUltcPildiAaIDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltcPildiAaIDbObj(String dtUltcPildiAaIDbObj) {
        if (dtUltcPildiAaIDbObj != null) {
            setDtUltcPildiAaIDb(dtUltcPildiAaIDbObj);
            ws.getIndParamComp().setDtUltcPildiAaI(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltcPildiAaI(((short)-1));
        }
    }

    @Override
    public String getDtUltcPildiMmCDb() {
        return ws.getParamCompDb().getDtUltcPildiMmCDb();
    }

    @Override
    public void setDtUltcPildiMmCDb(String dtUltcPildiMmCDb) {
        this.ws.getParamCompDb().setDtUltcPildiMmCDb(dtUltcPildiMmCDb);
    }

    @Override
    public String getDtUltcPildiMmCDbObj() {
        if (ws.getIndParamComp().getDtUltcPildiMmC() >= 0) {
            return getDtUltcPildiMmCDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltcPildiMmCDbObj(String dtUltcPildiMmCDbObj) {
        if (dtUltcPildiMmCDbObj != null) {
            setDtUltcPildiMmCDb(dtUltcPildiMmCDbObj);
            ws.getIndParamComp().setDtUltcPildiMmC(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltcPildiMmC(((short)-1));
        }
    }

    @Override
    public String getDtUltcPildiMmIDb() {
        return ws.getParamCompDb().getDtUltcPildiMmIDb();
    }

    @Override
    public void setDtUltcPildiMmIDb(String dtUltcPildiMmIDb) {
        this.ws.getParamCompDb().setDtUltcPildiMmIDb(dtUltcPildiMmIDb);
    }

    @Override
    public String getDtUltcPildiMmIDbObj() {
        if (ws.getIndParamComp().getDtUltcPildiMmI() >= 0) {
            return getDtUltcPildiMmIDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltcPildiMmIDbObj(String dtUltcPildiMmIDbObj) {
        if (dtUltcPildiMmIDbObj != null) {
            setDtUltcPildiMmIDb(dtUltcPildiMmIDbObj);
            ws.getIndParamComp().setDtUltcPildiMmI(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltcPildiMmI(((short)-1));
        }
    }

    @Override
    public String getDtUltcPildiTrIDb() {
        return ws.getParamCompDb().getDtUltcPildiTrIDb();
    }

    @Override
    public void setDtUltcPildiTrIDb(String dtUltcPildiTrIDb) {
        this.ws.getParamCompDb().setDtUltcPildiTrIDb(dtUltcPildiTrIDb);
    }

    @Override
    public String getDtUltcPildiTrIDbObj() {
        if (ws.getIndParamComp().getDtUltcPildiTrI() >= 0) {
            return getDtUltcPildiTrIDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltcPildiTrIDbObj(String dtUltcPildiTrIDbObj) {
        if (dtUltcPildiTrIDbObj != null) {
            setDtUltcPildiTrIDb(dtUltcPildiTrIDbObj);
            ws.getIndParamComp().setDtUltcPildiTrI(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltcPildiTrI(((short)-1));
        }
    }

    @Override
    public String getDtUltcRbClDb() {
        return ws.getParamCompDb().getDtUltcRbClDb();
    }

    @Override
    public void setDtUltcRbClDb(String dtUltcRbClDb) {
        this.ws.getParamCompDb().setDtUltcRbClDb(dtUltcRbClDb);
    }

    @Override
    public String getDtUltcRbClDbObj() {
        if (ws.getIndParamComp().getDtUltcRbCl() >= 0) {
            return getDtUltcRbClDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltcRbClDbObj(String dtUltcRbClDbObj) {
        if (dtUltcRbClDbObj != null) {
            setDtUltcRbClDb(dtUltcRbClDbObj);
            ws.getIndParamComp().setDtUltcRbCl(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltcRbCl(((short)-1));
        }
    }

    @Override
    public String getDtUltcRbInDb() {
        return ws.getParamCompDb().getDtUltcRbInDb();
    }

    @Override
    public void setDtUltcRbInDb(String dtUltcRbInDb) {
        this.ws.getParamCompDb().setDtUltcRbInDb(dtUltcRbInDb);
    }

    @Override
    public String getDtUltcRbInDbObj() {
        if (ws.getIndParamComp().getDtUltcRbIn() >= 0) {
            return getDtUltcRbInDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltcRbInDbObj(String dtUltcRbInDbObj) {
        if (dtUltcRbInDbObj != null) {
            setDtUltcRbInDb(dtUltcRbInDbObj);
            ws.getIndParamComp().setDtUltcRbIn(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltcRbIn(((short)-1));
        }
    }

    @Override
    public String getDtUltelriscparPrDb() {
        return ws.getParamCompDb().getDtUltelriscparPrDb();
    }

    @Override
    public void setDtUltelriscparPrDb(String dtUltelriscparPrDb) {
        this.ws.getParamCompDb().setDtUltelriscparPrDb(dtUltelriscparPrDb);
    }

    @Override
    public String getDtUltelriscparPrDbObj() {
        if (ws.getIndParamComp().getDtUltelriscparPr() >= 0) {
            return getDtUltelriscparPrDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltelriscparPrDbObj(String dtUltelriscparPrDbObj) {
        if (dtUltelriscparPrDbObj != null) {
            setDtUltelriscparPrDb(dtUltelriscparPrDbObj);
            ws.getIndParamComp().setDtUltelriscparPr(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltelriscparPr(((short)-1));
        }
    }

    @Override
    public String getDtUltgzCedCollDb() {
        return ws.getParamCompDb().getDtUltgzCedCollDb();
    }

    @Override
    public void setDtUltgzCedCollDb(String dtUltgzCedCollDb) {
        this.ws.getParamCompDb().setDtUltgzCedCollDb(dtUltgzCedCollDb);
    }

    @Override
    public String getDtUltgzCedCollDbObj() {
        if (ws.getIndParamComp().getDtUltgzCedColl() >= 0) {
            return getDtUltgzCedCollDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltgzCedCollDbObj(String dtUltgzCedCollDbObj) {
        if (dtUltgzCedCollDbObj != null) {
            setDtUltgzCedCollDb(dtUltgzCedCollDbObj);
            ws.getIndParamComp().setDtUltgzCedColl(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltgzCedColl(((short)-1));
        }
    }

    @Override
    public String getDtUltgzCedDb() {
        return ws.getParamCompDb().getDtUltgzCedDb();
    }

    @Override
    public void setDtUltgzCedDb(String dtUltgzCedDb) {
        this.ws.getParamCompDb().setDtUltgzCedDb(dtUltgzCedDb);
    }

    @Override
    public String getDtUltgzCedDbObj() {
        if (ws.getIndParamComp().getDtUltgzCed() >= 0) {
            return getDtUltgzCedDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltgzCedDbObj(String dtUltgzCedDbObj) {
        if (dtUltgzCedDbObj != null) {
            setDtUltgzCedDb(dtUltgzCedDbObj);
            ws.getIndParamComp().setDtUltgzCed(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltgzCed(((short)-1));
        }
    }

    @Override
    public String getDtUltgzTrchEClDb() {
        return ws.getParamCompDb().getDtUltgzTrchEClDb();
    }

    @Override
    public void setDtUltgzTrchEClDb(String dtUltgzTrchEClDb) {
        this.ws.getParamCompDb().setDtUltgzTrchEClDb(dtUltgzTrchEClDb);
    }

    @Override
    public String getDtUltgzTrchEClDbObj() {
        if (ws.getIndParamComp().getDtUltgzTrchECl() >= 0) {
            return getDtUltgzTrchEClDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltgzTrchEClDbObj(String dtUltgzTrchEClDbObj) {
        if (dtUltgzTrchEClDbObj != null) {
            setDtUltgzTrchEClDb(dtUltgzTrchEClDbObj);
            ws.getIndParamComp().setDtUltgzTrchECl(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltgzTrchECl(((short)-1));
        }
    }

    @Override
    public String getDtUltgzTrchEInDb() {
        return ws.getParamCompDb().getDtUltgzTrchEInDb();
    }

    @Override
    public void setDtUltgzTrchEInDb(String dtUltgzTrchEInDb) {
        this.ws.getParamCompDb().setDtUltgzTrchEInDb(dtUltgzTrchEInDb);
    }

    @Override
    public String getDtUltgzTrchEInDbObj() {
        if (ws.getIndParamComp().getDtUltgzTrchEIn() >= 0) {
            return getDtUltgzTrchEInDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltgzTrchEInDbObj(String dtUltgzTrchEInDbObj) {
        if (dtUltgzTrchEInDbObj != null) {
            setDtUltgzTrchEInDb(dtUltgzTrchEInDbObj);
            ws.getIndParamComp().setDtUltgzTrchEIn(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltgzTrchEIn(((short)-1));
        }
    }

    @Override
    public String getDtUltscElabClDb() {
        return ws.getParamCompDb().getDtUltscElabClDb();
    }

    @Override
    public void setDtUltscElabClDb(String dtUltscElabClDb) {
        this.ws.getParamCompDb().setDtUltscElabClDb(dtUltscElabClDb);
    }

    @Override
    public String getDtUltscElabClDbObj() {
        if (ws.getIndParamComp().getDtUltscElabCl() >= 0) {
            return getDtUltscElabClDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltscElabClDbObj(String dtUltscElabClDbObj) {
        if (dtUltscElabClDbObj != null) {
            setDtUltscElabClDb(dtUltscElabClDbObj);
            ws.getIndParamComp().setDtUltscElabCl(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltscElabCl(((short)-1));
        }
    }

    @Override
    public String getDtUltscElabInDb() {
        return ws.getParamCompDb().getDtUltscElabInDb();
    }

    @Override
    public void setDtUltscElabInDb(String dtUltscElabInDb) {
        this.ws.getParamCompDb().setDtUltscElabInDb(dtUltscElabInDb);
    }

    @Override
    public String getDtUltscElabInDbObj() {
        if (ws.getIndParamComp().getDtUltscElabIn() >= 0) {
            return getDtUltscElabInDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltscElabInDbObj(String dtUltscElabInDbObj) {
        if (dtUltscElabInDbObj != null) {
            setDtUltscElabInDb(dtUltscElabInDbObj);
            ws.getIndParamComp().setDtUltscElabIn(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltscElabIn(((short)-1));
        }
    }

    @Override
    public String getDtUltscOpzClDb() {
        return ws.getParamCompDb().getDtUltscOpzClDb();
    }

    @Override
    public void setDtUltscOpzClDb(String dtUltscOpzClDb) {
        this.ws.getParamCompDb().setDtUltscOpzClDb(dtUltscOpzClDb);
    }

    @Override
    public String getDtUltscOpzClDbObj() {
        if (ws.getIndParamComp().getDtUltscOpzCl() >= 0) {
            return getDtUltscOpzClDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltscOpzClDbObj(String dtUltscOpzClDbObj) {
        if (dtUltscOpzClDbObj != null) {
            setDtUltscOpzClDb(dtUltscOpzClDbObj);
            ws.getIndParamComp().setDtUltscOpzCl(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltscOpzCl(((short)-1));
        }
    }

    @Override
    public String getDtUltscOpzInDb() {
        return ws.getParamCompDb().getDtUltscOpzInDb();
    }

    @Override
    public void setDtUltscOpzInDb(String dtUltscOpzInDb) {
        this.ws.getParamCompDb().setDtUltscOpzInDb(dtUltscOpzInDb);
    }

    @Override
    public String getDtUltscOpzInDbObj() {
        if (ws.getIndParamComp().getDtUltscOpzIn() >= 0) {
            return getDtUltscOpzInDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltscOpzInDbObj(String dtUltscOpzInDbObj) {
        if (dtUltscOpzInDbObj != null) {
            setDtUltscOpzInDb(dtUltscOpzInDbObj);
            ws.getIndParamComp().setDtUltscOpzIn(((short)0));
        }
        else {
            ws.getIndParamComp().setDtUltscOpzIn(((short)-1));
        }
    }

    @Override
    public char getFlFrazProvAcq() {
        return paramComp.getPcoFlFrazProvAcq();
    }

    @Override
    public void setFlFrazProvAcq(char flFrazProvAcq) {
        this.paramComp.setPcoFlFrazProvAcq(flFrazProvAcq);
    }

    @Override
    public Character getFlFrazProvAcqObj() {
        if (ws.getIndParamComp().getFlFrazProvAcq() >= 0) {
            return ((Character)getFlFrazProvAcq());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlFrazProvAcqObj(Character flFrazProvAcqObj) {
        if (flFrazProvAcqObj != null) {
            setFlFrazProvAcq(((char)flFrazProvAcqObj));
            ws.getIndParamComp().setFlFrazProvAcq(((short)0));
        }
        else {
            ws.getIndParamComp().setFlFrazProvAcq(((short)-1));
        }
    }

    @Override
    public char getFlGestPlusv() {
        return paramComp.getPcoFlGestPlusv();
    }

    @Override
    public void setFlGestPlusv(char flGestPlusv) {
        this.paramComp.setPcoFlGestPlusv(flGestPlusv);
    }

    @Override
    public Character getFlGestPlusvObj() {
        if (ws.getIndParamComp().getFlGestPlusv() >= 0) {
            return ((Character)getFlGestPlusv());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlGestPlusvObj(Character flGestPlusvObj) {
        if (flGestPlusvObj != null) {
            setFlGestPlusv(((char)flGestPlusvObj));
            ws.getIndParamComp().setFlGestPlusv(((short)0));
        }
        else {
            ws.getIndParamComp().setFlGestPlusv(((short)-1));
        }
    }

    @Override
    public short getFlLivDebug() {
        return paramComp.getPcoFlLivDebug();
    }

    @Override
    public void setFlLivDebug(short flLivDebug) {
        this.paramComp.setPcoFlLivDebug(flLivDebug);
    }

    @Override
    public char getFlRcsPoliNoperf() {
        return paramComp.getPcoFlRcsPoliNoperf();
    }

    @Override
    public void setFlRcsPoliNoperf(char flRcsPoliNoperf) {
        this.paramComp.setPcoFlRcsPoliNoperf(flRcsPoliNoperf);
    }

    @Override
    public Character getFlRcsPoliNoperfObj() {
        if (ws.getIndParamComp().getFlRcsPoliNoperf() >= 0) {
            return ((Character)getFlRcsPoliNoperf());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlRcsPoliNoperfObj(Character flRcsPoliNoperfObj) {
        if (flRcsPoliNoperfObj != null) {
            setFlRcsPoliNoperf(((char)flRcsPoliNoperfObj));
            ws.getIndParamComp().setFlRcsPoliNoperf(((short)0));
        }
        else {
            ws.getIndParamComp().setFlRcsPoliNoperf(((short)-1));
        }
    }

    @Override
    public char getFlRvcPerf() {
        return paramComp.getPcoFlRvcPerf();
    }

    @Override
    public void setFlRvcPerf(char flRvcPerf) {
        this.paramComp.setPcoFlRvcPerf(flRvcPerf);
    }

    @Override
    public Character getFlRvcPerfObj() {
        if (ws.getIndParamComp().getFlRvcPerf() >= 0) {
            return ((Character)getFlRvcPerf());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlRvcPerfObj(Character flRvcPerfObj) {
        if (flRvcPerfObj != null) {
            setFlRvcPerf(((char)flRvcPerfObj));
            ws.getIndParamComp().setFlRvcPerf(((short)0));
        }
        else {
            ws.getIndParamComp().setFlRvcPerf(((short)-1));
        }
    }

    @Override
    public char getFlVisualVinpg() {
        return paramComp.getPcoFlVisualVinpg();
    }

    @Override
    public void setFlVisualVinpg(char flVisualVinpg) {
        this.paramComp.setPcoFlVisualVinpg(flVisualVinpg);
    }

    @Override
    public Character getFlVisualVinpgObj() {
        if (ws.getIndParamComp().getFlVisualVinpg() >= 0) {
            return ((Character)getFlVisualVinpg());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlVisualVinpgObj(Character flVisualVinpgObj) {
        if (flVisualVinpgObj != null) {
            setFlVisualVinpg(((char)flVisualVinpgObj));
            ws.getIndParamComp().setFlVisualVinpg(((short)0));
        }
        else {
            ws.getIndParamComp().setFlVisualVinpg(((short)-1));
        }
    }

    @Override
    public int getFrqCostiAtt() {
        return paramComp.getPcoFrqCostiAtt().getPcoFrqCostiAtt();
    }

    @Override
    public void setFrqCostiAtt(int frqCostiAtt) {
        this.paramComp.getPcoFrqCostiAtt().setPcoFrqCostiAtt(frqCostiAtt);
    }

    @Override
    public Integer getFrqCostiAttObj() {
        if (ws.getIndParamComp().getFrqCostiAtt() >= 0) {
            return ((Integer)getFrqCostiAtt());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFrqCostiAttObj(Integer frqCostiAttObj) {
        if (frqCostiAttObj != null) {
            setFrqCostiAtt(((int)frqCostiAttObj));
            ws.getIndParamComp().setFrqCostiAtt(((short)0));
        }
        else {
            ws.getIndParamComp().setFrqCostiAtt(((short)-1));
        }
    }

    @Override
    public int getFrqCostiStornati() {
        return paramComp.getPcoFrqCostiStornati().getPcoFrqCostiStornati();
    }

    @Override
    public void setFrqCostiStornati(int frqCostiStornati) {
        this.paramComp.getPcoFrqCostiStornati().setPcoFrqCostiStornati(frqCostiStornati);
    }

    @Override
    public Integer getFrqCostiStornatiObj() {
        if (ws.getIndParamComp().getFrqCostiStornati() >= 0) {
            return ((Integer)getFrqCostiStornati());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFrqCostiStornatiObj(Integer frqCostiStornatiObj) {
        if (frqCostiStornatiObj != null) {
            setFrqCostiStornati(((int)frqCostiStornatiObj));
            ws.getIndParamComp().setFrqCostiStornati(((short)0));
        }
        else {
            ws.getIndParamComp().setFrqCostiStornati(((short)-1));
        }
    }

    @Override
    public int getGgIntrRitPag() {
        return paramComp.getPcoGgIntrRitPag().getPcoGgIntrRitPag();
    }

    @Override
    public void setGgIntrRitPag(int ggIntrRitPag) {
        this.paramComp.getPcoGgIntrRitPag().setPcoGgIntrRitPag(ggIntrRitPag);
    }

    @Override
    public Integer getGgIntrRitPagObj() {
        if (ws.getIndParamComp().getGgIntrRitPag() >= 0) {
            return ((Integer)getGgIntrRitPag());
        }
        else {
            return null;
        }
    }

    @Override
    public void setGgIntrRitPagObj(Integer ggIntrRitPagObj) {
        if (ggIntrRitPagObj != null) {
            setGgIntrRitPag(((int)ggIntrRitPagObj));
            ws.getIndParamComp().setGgIntrRitPag(((short)0));
        }
        else {
            ws.getIndParamComp().setGgIntrRitPag(((short)-1));
        }
    }

    @Override
    public short getGgMaxRecProv() {
        return paramComp.getPcoGgMaxRecProv().getPcoGgMaxRecProv();
    }

    @Override
    public void setGgMaxRecProv(short ggMaxRecProv) {
        this.paramComp.getPcoGgMaxRecProv().setPcoGgMaxRecProv(ggMaxRecProv);
    }

    @Override
    public Short getGgMaxRecProvObj() {
        if (ws.getIndParamComp().getGgMaxRecProv() >= 0) {
            return ((Short)getGgMaxRecProv());
        }
        else {
            return null;
        }
    }

    @Override
    public void setGgMaxRecProvObj(Short ggMaxRecProvObj) {
        if (ggMaxRecProvObj != null) {
            setGgMaxRecProv(((short)ggMaxRecProvObj));
            ws.getIndParamComp().setGgMaxRecProv(((short)0));
        }
        else {
            ws.getIndParamComp().setGgMaxRecProv(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpAssSociale() {
        return paramComp.getPcoImpAssSociale().getPcoImpAssSociale();
    }

    @Override
    public void setImpAssSociale(AfDecimal impAssSociale) {
        this.paramComp.getPcoImpAssSociale().setPcoImpAssSociale(impAssSociale.copy());
    }

    @Override
    public AfDecimal getImpAssSocialeObj() {
        if (ws.getIndParamComp().getImpAssSociale() >= 0) {
            return getImpAssSociale();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpAssSocialeObj(AfDecimal impAssSocialeObj) {
        if (impAssSocialeObj != null) {
            setImpAssSociale(new AfDecimal(impAssSocialeObj, 15, 3));
            ws.getIndParamComp().setImpAssSociale(((short)0));
        }
        else {
            ws.getIndParamComp().setImpAssSociale(((short)-1));
        }
    }

    @Override
    public AfDecimal getLimVltr() {
        return paramComp.getPcoLimVltr().getPcoLimVltr();
    }

    @Override
    public void setLimVltr(AfDecimal limVltr) {
        this.paramComp.getPcoLimVltr().setPcoLimVltr(limVltr.copy());
    }

    @Override
    public AfDecimal getLimVltrObj() {
        if (ws.getIndParamComp().getLimVltr() >= 0) {
            return getLimVltr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setLimVltrObj(AfDecimal limVltrObj) {
        if (limVltrObj != null) {
            setLimVltr(new AfDecimal(limVltrObj, 15, 3));
            ws.getIndParamComp().setLimVltr(((short)0));
        }
        else {
            ws.getIndParamComp().setLimVltr(((short)-1));
        }
    }

    @Override
    public AfDecimal getLmCSubrshConIn() {
        return paramComp.getPcoLmCSubrshConIn().getPcoLmCSubrshConIn();
    }

    @Override
    public void setLmCSubrshConIn(AfDecimal lmCSubrshConIn) {
        this.paramComp.getPcoLmCSubrshConIn().setPcoLmCSubrshConIn(lmCSubrshConIn.copy());
    }

    @Override
    public AfDecimal getLmCSubrshConInObj() {
        if (ws.getIndParamComp().getLmCSubrshConIn() >= 0) {
            return getLmCSubrshConIn();
        }
        else {
            return null;
        }
    }

    @Override
    public void setLmCSubrshConInObj(AfDecimal lmCSubrshConInObj) {
        if (lmCSubrshConInObj != null) {
            setLmCSubrshConIn(new AfDecimal(lmCSubrshConInObj, 6, 3));
            ws.getIndParamComp().setLmCSubrshConIn(((short)0));
        }
        else {
            ws.getIndParamComp().setLmCSubrshConIn(((short)-1));
        }
    }

    @Override
    public AfDecimal getLmRisConInt() {
        return paramComp.getPcoLmRisConInt().getPcoLmRisConInt();
    }

    @Override
    public void setLmRisConInt(AfDecimal lmRisConInt) {
        this.paramComp.getPcoLmRisConInt().setPcoLmRisConInt(lmRisConInt.copy());
    }

    @Override
    public AfDecimal getLmRisConIntObj() {
        if (ws.getIndParamComp().getLmRisConInt() >= 0) {
            return getLmRisConInt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setLmRisConIntObj(AfDecimal lmRisConIntObj) {
        if (lmRisConIntObj != null) {
            setLmRisConInt(new AfDecimal(lmRisConIntObj, 6, 3));
            ws.getIndParamComp().setLmRisConInt(((short)0));
        }
        else {
            ws.getIndParamComp().setLmRisConInt(((short)-1));
        }
    }

    @Override
    public char getModComnzInvstSw() {
        return paramComp.getPcoModComnzInvstSw();
    }

    @Override
    public void setModComnzInvstSw(char modComnzInvstSw) {
        this.paramComp.setPcoModComnzInvstSw(modComnzInvstSw);
    }

    @Override
    public Character getModComnzInvstSwObj() {
        if (ws.getIndParamComp().getModComnzInvstSw() >= 0) {
            return ((Character)getModComnzInvstSw());
        }
        else {
            return null;
        }
    }

    @Override
    public void setModComnzInvstSwObj(Character modComnzInvstSwObj) {
        if (modComnzInvstSwObj != null) {
            setModComnzInvstSw(((char)modComnzInvstSwObj));
            ws.getIndParamComp().setModComnzInvstSw(((short)0));
        }
        else {
            ws.getIndParamComp().setModComnzInvstSw(((short)-1));
        }
    }

    @Override
    public String getModIntrPrest() {
        return paramComp.getPcoModIntrPrest();
    }

    @Override
    public void setModIntrPrest(String modIntrPrest) {
        this.paramComp.setPcoModIntrPrest(modIntrPrest);
    }

    @Override
    public String getModIntrPrestObj() {
        if (ws.getIndParamComp().getModIntrPrest() >= 0) {
            return getModIntrPrest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setModIntrPrestObj(String modIntrPrestObj) {
        if (modIntrPrestObj != null) {
            setModIntrPrest(modIntrPrestObj);
            ws.getIndParamComp().setModIntrPrest(((short)0));
        }
        else {
            ws.getIndParamComp().setModIntrPrest(((short)-1));
        }
    }

    @Override
    public int getNumGgArrIntrPr() {
        return paramComp.getPcoNumGgArrIntrPr().getPcoNumGgArrIntrPr();
    }

    @Override
    public void setNumGgArrIntrPr(int numGgArrIntrPr) {
        this.paramComp.getPcoNumGgArrIntrPr().setPcoNumGgArrIntrPr(numGgArrIntrPr);
    }

    @Override
    public Integer getNumGgArrIntrPrObj() {
        if (ws.getIndParamComp().getNumGgArrIntrPr() >= 0) {
            return ((Integer)getNumGgArrIntrPr());
        }
        else {
            return null;
        }
    }

    @Override
    public void setNumGgArrIntrPrObj(Integer numGgArrIntrPrObj) {
        if (numGgArrIntrPrObj != null) {
            setNumGgArrIntrPr(((int)numGgArrIntrPrObj));
            ws.getIndParamComp().setNumGgArrIntrPr(((short)0));
        }
        else {
            ws.getIndParamComp().setNumGgArrIntrPr(((short)-1));
        }
    }

    @Override
    public int getNumMmCalcMora() {
        return paramComp.getPcoNumMmCalcMora().getPcoNumMmCalcMora();
    }

    @Override
    public void setNumMmCalcMora(int numMmCalcMora) {
        this.paramComp.getPcoNumMmCalcMora().setPcoNumMmCalcMora(numMmCalcMora);
    }

    @Override
    public Integer getNumMmCalcMoraObj() {
        if (ws.getIndParamComp().getNumMmCalcMora() >= 0) {
            return ((Integer)getNumMmCalcMora());
        }
        else {
            return null;
        }
    }

    @Override
    public void setNumMmCalcMoraObj(Integer numMmCalcMoraObj) {
        if (numMmCalcMoraObj != null) {
            setNumMmCalcMora(((int)numMmCalcMoraObj));
            ws.getIndParamComp().setNumMmCalcMora(((short)0));
        }
        else {
            ws.getIndParamComp().setNumMmCalcMora(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcCSubrshMarsol() {
        return paramComp.getPcoPcCSubrshMarsol().getPcoPcCSubrshMarsol();
    }

    @Override
    public void setPcCSubrshMarsol(AfDecimal pcCSubrshMarsol) {
        this.paramComp.getPcoPcCSubrshMarsol().setPcoPcCSubrshMarsol(pcCSubrshMarsol.copy());
    }

    @Override
    public AfDecimal getPcCSubrshMarsolObj() {
        if (ws.getIndParamComp().getPcCSubrshMarsol() >= 0) {
            return getPcCSubrshMarsol();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcCSubrshMarsolObj(AfDecimal pcCSubrshMarsolObj) {
        if (pcCSubrshMarsolObj != null) {
            setPcCSubrshMarsol(new AfDecimal(pcCSubrshMarsolObj, 6, 3));
            ws.getIndParamComp().setPcCSubrshMarsol(((short)0));
        }
        else {
            ws.getIndParamComp().setPcCSubrshMarsol(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcGarNoriskMars() {
        return paramComp.getPcoPcGarNoriskMars().getPcoPcGarNoriskMars();
    }

    @Override
    public void setPcGarNoriskMars(AfDecimal pcGarNoriskMars) {
        this.paramComp.getPcoPcGarNoriskMars().setPcoPcGarNoriskMars(pcGarNoriskMars.copy());
    }

    @Override
    public AfDecimal getPcGarNoriskMarsObj() {
        if (ws.getIndParamComp().getPcGarNoriskMars() >= 0) {
            return getPcGarNoriskMars();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcGarNoriskMarsObj(AfDecimal pcGarNoriskMarsObj) {
        if (pcGarNoriskMarsObj != null) {
            setPcGarNoriskMars(new AfDecimal(pcGarNoriskMarsObj, 6, 3));
            ws.getIndParamComp().setPcGarNoriskMars(((short)0));
        }
        else {
            ws.getIndParamComp().setPcGarNoriskMars(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcProv1aaAcq() {
        return paramComp.getPcoPcProv1aaAcq().getPcoPcProv1aaAcq();
    }

    @Override
    public void setPcProv1aaAcq(AfDecimal pcProv1aaAcq) {
        this.paramComp.getPcoPcProv1aaAcq().setPcoPcProv1aaAcq(pcProv1aaAcq.copy());
    }

    @Override
    public AfDecimal getPcProv1aaAcqObj() {
        if (ws.getIndParamComp().getPcProv1aaAcq() >= 0) {
            return getPcProv1aaAcq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcProv1aaAcqObj(AfDecimal pcProv1aaAcqObj) {
        if (pcProv1aaAcqObj != null) {
            setPcProv1aaAcq(new AfDecimal(pcProv1aaAcqObj, 6, 3));
            ws.getIndParamComp().setPcProv1aaAcq(((short)0));
        }
        else {
            ws.getIndParamComp().setPcProv1aaAcq(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcRidImp1382011() {
        return paramComp.getPcoPcRidImp1382011().getPcoPcRidImp1382011();
    }

    @Override
    public void setPcRidImp1382011(AfDecimal pcRidImp1382011) {
        this.paramComp.getPcoPcRidImp1382011().setPcoPcRidImp1382011(pcRidImp1382011.copy());
    }

    @Override
    public AfDecimal getPcRidImp1382011Obj() {
        if (ws.getIndParamComp().getPcRidImp1382011() >= 0) {
            return getPcRidImp1382011();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcRidImp1382011Obj(AfDecimal pcRidImp1382011Obj) {
        if (pcRidImp1382011Obj != null) {
            setPcRidImp1382011(new AfDecimal(pcRidImp1382011Obj, 6, 3));
            ws.getIndParamComp().setPcRidImp1382011(((short)0));
        }
        else {
            ws.getIndParamComp().setPcRidImp1382011(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcRidImp662014() {
        return paramComp.getPcoPcRidImp662014().getPcoPcRidImp662014();
    }

    @Override
    public void setPcRidImp662014(AfDecimal pcRidImp662014) {
        this.paramComp.getPcoPcRidImp662014().setPcoPcRidImp662014(pcRidImp662014.copy());
    }

    @Override
    public AfDecimal getPcRidImp662014Obj() {
        if (ws.getIndParamComp().getPcRidImp662014() >= 0) {
            return getPcRidImp662014();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcRidImp662014Obj(AfDecimal pcRidImp662014Obj) {
        if (pcRidImp662014Obj != null) {
            setPcRidImp662014(new AfDecimal(pcRidImp662014Obj, 6, 3));
            ws.getIndParamComp().setPcRidImp662014(((short)0));
        }
        else {
            ws.getIndParamComp().setPcRidImp662014(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcRmMarsol() {
        return paramComp.getPcoPcRmMarsol().getPcoPcRmMarsol();
    }

    @Override
    public void setPcRmMarsol(AfDecimal pcRmMarsol) {
        this.paramComp.getPcoPcRmMarsol().setPcoPcRmMarsol(pcRmMarsol.copy());
    }

    @Override
    public AfDecimal getPcRmMarsolObj() {
        if (ws.getIndParamComp().getPcRmMarsol() >= 0) {
            return getPcRmMarsol();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcRmMarsolObj(AfDecimal pcRmMarsolObj) {
        if (pcRmMarsolObj != null) {
            setPcRmMarsol(new AfDecimal(pcRmMarsolObj, 6, 3));
            ws.getIndParamComp().setPcRmMarsol(((short)0));
        }
        else {
            ws.getIndParamComp().setPcRmMarsol(((short)-1));
        }
    }

    @Override
    public int getPcoCodCompAnia() {
        return paramComp.getPcoCodCompAnia();
    }

    @Override
    public void setPcoCodCompAnia(int pcoCodCompAnia) {
        this.paramComp.setPcoCodCompAnia(pcoCodCompAnia);
    }

    @Override
    public AfDecimal getSoglAmlPrePer() {
        return paramComp.getPcoSoglAmlPrePer().getPcoSoglAmlPrePer();
    }

    @Override
    public void setSoglAmlPrePer(AfDecimal soglAmlPrePer) {
        this.paramComp.getPcoSoglAmlPrePer().setPcoSoglAmlPrePer(soglAmlPrePer.copy());
    }

    @Override
    public AfDecimal getSoglAmlPrePerObj() {
        if (ws.getIndParamComp().getSoglAmlPrePer() >= 0) {
            return getSoglAmlPrePer();
        }
        else {
            return null;
        }
    }

    @Override
    public void setSoglAmlPrePerObj(AfDecimal soglAmlPrePerObj) {
        if (soglAmlPrePerObj != null) {
            setSoglAmlPrePer(new AfDecimal(soglAmlPrePerObj, 15, 3));
            ws.getIndParamComp().setSoglAmlPrePer(((short)0));
        }
        else {
            ws.getIndParamComp().setSoglAmlPrePer(((short)-1));
        }
    }

    @Override
    public AfDecimal getSoglAmlPreSavR() {
        return paramComp.getPcoSoglAmlPreSavR().getPcoSoglAmlPreSavR();
    }

    @Override
    public void setSoglAmlPreSavR(AfDecimal soglAmlPreSavR) {
        this.paramComp.getPcoSoglAmlPreSavR().setPcoSoglAmlPreSavR(soglAmlPreSavR.copy());
    }

    @Override
    public AfDecimal getSoglAmlPreSavRObj() {
        if (ws.getIndParamComp().getSoglAmlPreSavR() >= 0) {
            return getSoglAmlPreSavR();
        }
        else {
            return null;
        }
    }

    @Override
    public void setSoglAmlPreSavRObj(AfDecimal soglAmlPreSavRObj) {
        if (soglAmlPreSavRObj != null) {
            setSoglAmlPreSavR(new AfDecimal(soglAmlPreSavRObj, 15, 3));
            ws.getIndParamComp().setSoglAmlPreSavR(((short)0));
        }
        else {
            ws.getIndParamComp().setSoglAmlPreSavR(((short)-1));
        }
    }

    @Override
    public AfDecimal getSoglAmlPreUni() {
        return paramComp.getPcoSoglAmlPreUni().getPcoSoglAmlPreUni();
    }

    @Override
    public void setSoglAmlPreUni(AfDecimal soglAmlPreUni) {
        this.paramComp.getPcoSoglAmlPreUni().setPcoSoglAmlPreUni(soglAmlPreUni.copy());
    }

    @Override
    public AfDecimal getSoglAmlPreUniObj() {
        if (ws.getIndParamComp().getSoglAmlPreUni() >= 0) {
            return getSoglAmlPreUni();
        }
        else {
            return null;
        }
    }

    @Override
    public void setSoglAmlPreUniObj(AfDecimal soglAmlPreUniObj) {
        if (soglAmlPreUniObj != null) {
            setSoglAmlPreUni(new AfDecimal(soglAmlPreUniObj, 15, 3));
            ws.getIndParamComp().setSoglAmlPreUni(((short)0));
        }
        else {
            ws.getIndParamComp().setSoglAmlPreUni(((short)-1));
        }
    }

    @Override
    public String getStstXRegioneDb() {
        return ws.getParamCompDb().getStstXRegioneDb();
    }

    @Override
    public void setStstXRegioneDb(String ststXRegioneDb) {
        this.ws.getParamCompDb().setStstXRegioneDb(ststXRegioneDb);
    }

    @Override
    public String getStstXRegioneDbObj() {
        if (ws.getIndParamComp().getStstXRegione() >= 0) {
            return getStstXRegioneDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setStstXRegioneDbObj(String ststXRegioneDbObj) {
        if (ststXRegioneDbObj != null) {
            setStstXRegioneDb(ststXRegioneDbObj);
            ws.getIndParamComp().setStstXRegione(((short)0));
        }
        else {
            ws.getIndParamComp().setStstXRegione(((short)-1));
        }
    }

    @Override
    public String getTpLivGenzTit() {
        return paramComp.getPcoTpLivGenzTit();
    }

    @Override
    public void setTpLivGenzTit(String tpLivGenzTit) {
        this.paramComp.setPcoTpLivGenzTit(tpLivGenzTit);
    }

    @Override
    public String getTpModRival() {
        return paramComp.getPcoTpModRival();
    }

    @Override
    public void setTpModRival(String tpModRival) {
        this.paramComp.setPcoTpModRival(tpModRival);
    }

    @Override
    public char getTpRatPerf() {
        return paramComp.getPcoTpRatPerf();
    }

    @Override
    public void setTpRatPerf(char tpRatPerf) {
        this.paramComp.setPcoTpRatPerf(tpRatPerf);
    }

    @Override
    public Character getTpRatPerfObj() {
        if (ws.getIndParamComp().getTpRatPerf() >= 0) {
            return ((Character)getTpRatPerf());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpRatPerfObj(Character tpRatPerfObj) {
        if (tpRatPerfObj != null) {
            setTpRatPerf(((char)tpRatPerfObj));
            ws.getIndParamComp().setTpRatPerf(((short)0));
        }
        else {
            ws.getIndParamComp().setTpRatPerf(((short)-1));
        }
    }

    @Override
    public String getTpValzzDtVlt() {
        return paramComp.getPcoTpValzzDtVlt();
    }

    @Override
    public void setTpValzzDtVlt(String tpValzzDtVlt) {
        this.paramComp.setPcoTpValzzDtVlt(tpValzzDtVlt);
    }

    @Override
    public String getTpValzzDtVltObj() {
        if (ws.getIndParamComp().getTpValzzDtVlt() >= 0) {
            return getTpValzzDtVlt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpValzzDtVltObj(String tpValzzDtVltObj) {
        if (tpValzzDtVltObj != null) {
            setTpValzzDtVlt(tpValzzDtVltObj);
            ws.getIndParamComp().setTpValzzDtVlt(((short)0));
        }
        else {
            ws.getIndParamComp().setTpValzzDtVlt(((short)-1));
        }
    }
}
