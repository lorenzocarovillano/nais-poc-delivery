package it.accenture.jnais;

import com.modernsystems.jdbc.FieldNotMappedException;
import it.accenture.jnais.commons.data.to.IVarFunzDiCalc;
import it.accenture.jnais.ws.VarFunzDiCalcR;

/**Original name: VarFunzDiCalcIdbsvfc0<br>*/
public class VarFunzDiCalcIdbsvfc0 implements IVarFunzDiCalc {

    //==== PROPERTIES ====
    private VarFunzDiCalcR varFunzDiCalc;

    //==== CONSTRUCTORS ====
    public VarFunzDiCalcIdbsvfc0(VarFunzDiCalcR varFunzDiCalc) {
        this.varFunzDiCalc = varFunzDiCalc;
    }

    //==== METHODS ====
    @Override
    public String getCodprodVchar() {
        return varFunzDiCalc.getAc5CodprodVcharFormatted();
    }

    @Override
    public void setCodprodVchar(String codprodVchar) {
        varFunzDiCalc.setAc5CodprodVcharFormatted(codprodVchar);
    }

    @Override
    public String getCodtariVchar() {
        return varFunzDiCalc.getAc5CodtariVcharFormatted();
    }

    @Override
    public void setCodtariVchar(String codtariVchar) {
        varFunzDiCalc.setAc5CodtariVcharFormatted(codtariVchar);
    }

    @Override
    public int getDend() {
        throw new FieldNotMappedException("dend");
    }

    @Override
    public void setDend(int dend) {
        throw new FieldNotMappedException("dend");
    }

    @Override
    public int getDiniz() {
        return varFunzDiCalc.getAc5Diniz();
    }

    @Override
    public void setDiniz(int diniz) {
        varFunzDiCalc.setAc5Diniz(diniz);
    }

    @Override
    public String getGlovarlistVchar() {
        throw new FieldNotMappedException("glovarlistVchar");
    }

    @Override
    public void setGlovarlistVchar(String glovarlistVchar) {
        throw new FieldNotMappedException("glovarlistVchar");
    }

    @Override
    public String getGlovarlistVcharObj() {
        return getGlovarlistVchar();
    }

    @Override
    public void setGlovarlistVcharObj(String glovarlistVcharObj) {
        setGlovarlistVchar(glovarlistVcharObj);
    }

    @Override
    public short getIdcomp() {
        return varFunzDiCalc.getAc5Idcomp();
    }

    @Override
    public void setIdcomp(short idcomp) {
        varFunzDiCalc.setAc5Idcomp(idcomp);
    }

    @Override
    public short getIsprecalc() {
        throw new FieldNotMappedException("isprecalc");
    }

    @Override
    public void setIsprecalc(short isprecalc) {
        throw new FieldNotMappedException("isprecalc");
    }

    @Override
    public Short getIsprecalcObj() {
        return ((Short)getIsprecalc());
    }

    @Override
    public void setIsprecalcObj(Short isprecalcObj) {
        setIsprecalc(((short)isprecalcObj));
    }

    @Override
    public String getModcalcVchar() {
        throw new FieldNotMappedException("modcalcVchar");
    }

    @Override
    public void setModcalcVchar(String modcalcVchar) {
        throw new FieldNotMappedException("modcalcVchar");
    }

    @Override
    public String getModcalcVcharObj() {
        return getModcalcVchar();
    }

    @Override
    public void setModcalcVcharObj(String modcalcVcharObj) {
        setModcalcVchar(modcalcVcharObj);
    }

    @Override
    public String getNomefunzVchar() {
        return varFunzDiCalc.getAc5NomefunzVcharFormatted();
    }

    @Override
    public void setNomefunzVchar(String nomefunzVchar) {
        varFunzDiCalc.setAc5NomefunzVcharFormatted(nomefunzVchar);
    }

    @Override
    public String getNomeprogrVchar() {
        throw new FieldNotMappedException("nomeprogrVchar");
    }

    @Override
    public void setNomeprogrVchar(String nomeprogrVchar) {
        throw new FieldNotMappedException("nomeprogrVchar");
    }

    @Override
    public String getNomeprogrVcharObj() {
        return getNomeprogrVchar();
    }

    @Override
    public void setNomeprogrVcharObj(String nomeprogrVcharObj) {
        setNomeprogrVchar(nomeprogrVcharObj);
    }

    @Override
    public String getStepElabVchar() {
        return varFunzDiCalc.getAc5StepElabVcharFormatted();
    }

    @Override
    public void setStepElabVchar(String stepElabVchar) {
        varFunzDiCalc.setAc5StepElabVcharFormatted(stepElabVchar);
    }
}
