package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.BtcBatchStateDao;
import it.accenture.jnais.commons.data.to.IBtcBatchState;
import it.accenture.jnais.copy.Idbvbbs2;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.BtcElabState;
import it.accenture.jnais.ws.Iabv0002;
import it.accenture.jnais.ws.Idsv0003;

/**Original name: IABS0070<br>
 * <pre>AUTHOR.        ATS NAPOLI.
 * DATE-WRITTEN.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE
 *  F A S E         : GESTIONE TABELLA BTC_BATCH_STATE
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Iabs0070 extends Program implements IBtcBatchState {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private BtcBatchStateDao btcBatchStateDao = new BtcBatchStateDao(dbAccessStatus);
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IDBVBBS2
    private Idbvbbs2 idbvbbs2 = new Idbvbbs2();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: IABV0002
    private Iabv0002 iabv0002;
    //Original name: BTC-BATCH-STATE
    private BtcElabState btcBatchState;

    //==== METHODS ====
    /**Original name: PROGRAM_IABS0070_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, Iabv0002 iabv0002, BtcElabState btcBatchState) {
        this.idsv0003 = idsv0003;
        this.iabv0002 = iabv0002;
        this.btcBatchState = btcBatchState;
        // COB_CODE: PERFORM A000-INIZIO              THRU A000-EX.
        a000Inizio();
        // COB_CODE: PERFORM A300-ELABORA             THRU A300-EX
        a300Elabora();
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Iabs0070 getInstance() {
        return ((Iabs0070)Programs.getInstance(Iabs0070.class));
    }

    /**Original name: A000-INIZIO<br>
	 * <pre>*****************************************************************</pre>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'IABS0070'              TO   IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("IABS0070");
        // COB_CODE: MOVE 'BTC_BATCH_STATE'       TO   IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("BTC_BATCH_STATE");
        // COB_CODE: MOVE '00'                    TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                  TO   IDSV0003-SQLCODE
        //                                             IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                  TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                             IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>
	 * <pre>*****************************************************************</pre>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(descrizErrDb2);
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A300-ELABORA<br>
	 * <pre>*****************************************************************</pre>*/
    private void a300Elabora() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A310-SELECT                 THRU A310-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A360-OPEN-CURSOR            THRU A360-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A370-CLOSE-CURSOR           THRU A370-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A380-FETCH-FIRST            THRU A380-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A390-FETCH-NEXT             THRU A390-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A310-SELECT                 THRU A310-EX
            a310Select();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A360-OPEN-CURSOR            THRU A360-EX
            a360OpenCursor();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A370-CLOSE-CURSOR           THRU A370-EX
            a370CloseCursor();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A380-FETCH-FIRST            THRU A380-EX
            a380FetchFirst();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT             THRU A390-EX
            a390FetchNext();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A305-DECLARE-CURSOR<br>
	 * <pre>*****************************************************************</pre>*/
    private void a305DeclareCursor() {
    // COB_CODE: EXEC SQL
    //                DECLARE CUR-BBS CURSOR WITH HOLD FOR
    //              SELECT
    //                 COD_BATCH_STATE
    //                ,DES
    //                ,FLAG_TO_EXECUTE
    //              FROM BTC_BATCH_STATE
    //             ORDER BY COD_BATCH_STATE
    //           END-EXEC.
    // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A310-SELECT<br>
	 * <pre>*****************************************************************</pre>*/
    private void a310Select() {
        // COB_CODE: EXEC SQL
        //             SELECT
        //                 COD_BATCH_STATE
        //                ,DES
        //                ,FLAG_TO_EXECUTE
        //             INTO
        //                  :BBS-COD-BATCH-STATE
        //                 ,:BBS-DES
        //                  :IND-BBS-DES
        //                 ,:BBS-FLAG-TO-EXECUTE
        //                  :IND-BBS-FLAG-TO-EXECUTE
        //             FROM  BTC_BATCH_STATE
        //             WHERE COD_BATCH_STATE = :IABV0002-STATE-CURRENT
        //           END-EXEC.
        btcBatchStateDao.selectByIabv0002StateCurrent(iabv0002.getIabv0002StateCurrent(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A360-OPEN-CURSOR<br>
	 * <pre>*****************************************************************</pre>*/
    private void a360OpenCursor() {
        // COB_CODE: PERFORM A305-DECLARE-CURSOR THRU A305-EX.
        a305DeclareCursor();
        // COB_CODE: EXEC SQL
        //                OPEN CUR-BBS
        //           END-EXEC.
        btcBatchStateDao.openCurBbs();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A370-CLOSE-CURSOR<br>
	 * <pre>*****************************************************************</pre>*/
    private void a370CloseCursor() {
        // COB_CODE: EXEC SQL
        //                CLOSE CUR-BBS
        //           END-EXEC.
        btcBatchStateDao.closeCurBbs();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A380-FETCH-FIRST<br>
	 * <pre>*****************************************************************</pre>*/
    private void a380FetchFirst() {
        // COB_CODE: PERFORM A360-OPEN-CURSOR    THRU A360-EX.
        a360OpenCursor();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A390-FETCH-NEXT THRU A390-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT THRU A390-EX
            a390FetchNext();
        }
    }

    /**Original name: A390-FETCH-NEXT<br>
	 * <pre>*****************************************************************</pre>*/
    private void a390FetchNext() {
        // COB_CODE: EXEC SQL
        //                FETCH CUR-BBS
        //           INTO
        //                  :BBS-COD-BATCH-STATE
        //                 ,:BBS-DES
        //                  :IND-BBS-DES
        //                 ,:BBS-FLAG-TO-EXECUTE
        //                  :IND-BBS-FLAG-TO-EXECUTE
        //           END-EXEC.
        btcBatchStateDao.fetchCurBbs(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A370-CLOSE-CURSOR THRU A370-EX
            a370CloseCursor();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>*****************************************************************</pre>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-BBS-DES = -1
        //              MOVE HIGH-VALUES TO BBS-DES
        //           END-IF
        if (idbvbbs2.getBbsDes() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BBS-DES
            btcBatchState.setDes(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BtcElabState.Len.DES));
        }
        // COB_CODE: IF IND-BBS-FLAG-TO-EXECUTE = -1
        //              MOVE HIGH-VALUES TO BBS-FLAG-TO-EXECUTE-NULL
        //           END-IF.
        if (idbvbbs2.getBbsFlagToExecute() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BBS-FLAG-TO-EXECUTE-NULL
            btcBatchState.setFlagToExecute(Types.HIGH_CHAR_VAL);
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            idsv0010.setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IDSP0003:line=25, because the code is unreachable.
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            idsv0010.setWsDataInizioEffettoDb(idsv0010.getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                idsv0010.setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IDSP0003:line=33, because the code is unreachable.
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                idsv0010.setWsDataFineEffettoDb(idsv0010.getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            idsv0010.setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            idsv0010.setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    @Override
    public char getCodBatchState() {
        return btcBatchState.getCodElabState();
    }

    @Override
    public void setCodBatchState(char codBatchState) {
        this.btcBatchState.setCodElabState(codBatchState);
    }

    @Override
    public String getDes() {
        return btcBatchState.getDes();
    }

    @Override
    public void setDes(String des) {
        this.btcBatchState.setDes(des);
    }

    @Override
    public String getDesObj() {
        if (idbvbbs2.getBbsDes() >= 0) {
            return getDes();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDesObj(String desObj) {
        if (desObj != null) {
            setDes(desObj);
            idbvbbs2.setBbsDes(((short)0));
        }
        else {
            idbvbbs2.setBbsDes(((short)-1));
        }
    }

    @Override
    public char getFlagToExecute() {
        return btcBatchState.getFlagToExecute();
    }

    @Override
    public void setFlagToExecute(char flagToExecute) {
        this.btcBatchState.setFlagToExecute(flagToExecute);
    }

    @Override
    public Character getFlagToExecuteObj() {
        if (idbvbbs2.getBbsFlagToExecute() >= 0) {
            return ((Character)getFlagToExecute());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlagToExecuteObj(Character flagToExecuteObj) {
        if (flagToExecuteObj != null) {
            setFlagToExecute(((char)flagToExecuteObj));
            idbvbbs2.setBbsFlagToExecute(((short)0));
        }
        else {
            idbvbbs2.setBbsFlagToExecute(((short)-1));
        }
    }
}
