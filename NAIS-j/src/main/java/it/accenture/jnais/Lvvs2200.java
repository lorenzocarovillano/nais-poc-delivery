package it.accenture.jnais;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.Lvvs2200Data;

/**Original name: LVVS2200<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2007.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *   PROGRAMMA...... LVVS2200
 *   TIPOLOGIA...... SERVIZIO
 *   PROCESSO....... XXX
 *   FUNZIONE....... XXX
 *   DESCRIZIONE.... CALCOLO DELLA VARIABILE PRATA
 * **------------------------------------------------------------***</pre>*/
public class Lvvs2200 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lvvs2200Data ws = new Lvvs2200Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS0007
    private Ivvc0213 ivvc0213;

    //==== METHODS ====
    /**Original name: PROGRAM_LVVS2200_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: PERFORM S1000-ELABORAZIONE
        //              THRU EX-S1000
        s1000Elaborazione();
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lvvs2200 getInstance() {
        return ((Lvvs2200)Programs.getInstance(Lvvs2200.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                        IX-INDICI
        //                                             IVVC0213-TAB-OUTPUT.
        initIxIndici();
        initTabOutput();
        // COB_CODE: SET WK-TROVATO-NO                 TO TRUE.
        ws.getWkTrovato().setNo();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE
        //             TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: INITIALIZE AREA-IO-PMO
        initAreaIoPmo();
        //
        //--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
        //--> RISPETTIVE AREE DCLGEN IN WORKING
        // COB_CODE: PERFORM S1100-VALORIZZA-DCLGEN
        //              THRU S1100-VALORIZZA-DCLGEN-EX
        //           VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
        //                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //                   SPACES OR LOW-VALUE OR HIGH-VALUE.
        ws.setIxDclgen(((short)1));
        while (!(ws.getIxDclgen() > ivvc0213.getEleInfoMax() || Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias()) || Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()) || Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()))) {
            s1100ValorizzaDclgen();
            ws.setIxDclgen(Trunc.toShort(ws.getIxDclgen() + 1, 4));
        }
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //               PERFORM S1200-CALCOLA-PRATA          THRU S1200-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM S1200-CALCOLA-PRATA          THRU S1200-EX
            s1200CalcolaPrata();
        }
    }

    /**Original name: S1100-VALORIZZA-DCLGEN<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 *     RISPETTIVE AREE DCLGEN IN WORKING
	 * ----------------------------------------------------------------*</pre>*/
    private void s1100ValorizzaDclgen() {
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-PARAM-MOV
        //                TO DPMO-AREA-PMO
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias(), ws.getIvvc0218().getAliasParamMov())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO DPMO-AREA-PMO
            ws.setDpmoAreaPmoFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxDclgen()).getLunghezza() - 1));
        }
    }

    /**Original name: S1200-CALCOLA-PRATA<br>
	 * <pre>----------------------------------------------------------------*
	 *   CALCOLA PRATA
	 * ----------------------------------------------------------------*</pre>*/
    private void s1200CalcolaPrata() {
        // COB_CODE: MOVE ZEROES               TO IVVC0213-VAL-IMP-O
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        // COB_CODE: PERFORM VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > DPMO-ELE-PMO-MAX
        //                OR WK-TROVATO-SI
        //                END-EVALUATE
        //           END-PERFORM.
        ws.setIxDclgen(((short)1));
        while (!(ws.getIxDclgen() > ws.getDpmoElePmoMax() || ws.getWkTrovato().isSi())) {
            // COB_CODE: EVALUATE DPMO-TP-MOVI(IX-DCLGEN)
            //               WHEN 6101
            //               WHEN 6002
            //               WHEN 6003
            //               WHEN 1075
            //                 END-IF
            //           END-EVALUATE
            switch (ws.getDpmoTabParamMov(ws.getIxDclgen()).getLccvpmo1().getDati().getWpmoTpMovi().getWpmoTpMovi()) {

                case 6101:
                case 6002:
                case 6003:
                case 1075:// COB_CODE: IF DPMO-IMP-LRD-DI-RAT-NULL(IX-DCLGEN)
                    //              = HIGH-VALUE OR LOW-VALUE OR SPACES
                    //              CONTINUE
                    //           ELSE
                    //              SET WK-TROVATO-SI TO TRUE
                    //           END-IF
                    if (Characters.EQ_HIGH.test(ws.getDpmoTabParamMov(ws.getIxDclgen()).getLccvpmo1().getDati().getWpmoImpLrdDiRat().getWpmoImpLrdDiRatNullFormatted()) || Characters.EQ_LOW.test(ws.getDpmoTabParamMov(ws.getIxDclgen()).getLccvpmo1().getDati().getWpmoImpLrdDiRat().getWpmoImpLrdDiRatNullFormatted()) || Characters.EQ_SPACE.test(ws.getDpmoTabParamMov(ws.getIxDclgen()).getLccvpmo1().getDati().getWpmoImpLrdDiRat().getWpmoImpLrdDiRatNull())) {
                    // COB_CODE: CONTINUE
                    //continue
                    }
                    else {
                        // COB_CODE: MOVE DPMO-IMP-LRD-DI-RAT(IX-DCLGEN)
                        //             TO IVVC0213-VAL-IMP-O
                        ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(ws.getDpmoTabParamMov(ws.getIxDclgen()).getLccvpmo1().getDati().getWpmoImpLrdDiRat().getWpmoImpLrdDiRat(), 18, 7));
                        // COB_CODE: SET WK-TROVATO-SI TO TRUE
                        ws.getWkTrovato().setSi();
                    }
                    break;

                default:break;
            }
            ws.setIxDclgen(Trunc.toShort(ws.getIxDclgen() + 1, 4));
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    public void initIxIndici() {
        ws.setIxDclgen(((short)0));
    }

    public void initTabOutput() {
        ivvc0213.getTabOutput().setCodVariabileO("");
        ivvc0213.getTabOutput().setTpDatoO(Types.SPACE_CHAR);
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        ivvc0213.getTabOutput().setValPercO(new AfDecimal(0, 14, 9));
        ivvc0213.getTabOutput().setValStrO("");
    }

    public void initAreaIoPmo() {
        ws.setDpmoElePmoMax(((short)0));
        for (int idx0 = 1; idx0 <= Lvvs2200Data.DPMO_TAB_PARAM_MOV_MAXOCCURS; idx0++) {
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().setIdPtf(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoIdParamMovi(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoIdOgg(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoTpOgg("");
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoIdMoviCrz(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoIdMoviChiu().setWpmoIdMoviChiu(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoDtIniEff(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoDtEndEff(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoCodCompAnia(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoTpMovi().setWpmoTpMovi(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoFrqMovi().setWpmoFrqMovi(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoDurAa().setWpmoDurAa(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoDurMm().setWpmoDurMm(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoDurGg().setWpmoDurGg(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoDtRicorPrec().setWpmoDtRicorPrec(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoDtRicorSucc().setWpmoDtRicorSucc(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoPcIntrFraz().setWpmoPcIntrFraz(new AfDecimal(0, 6, 3));
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoImpBnsDaScoTot().setWpmoImpBnsDaScoTot(new AfDecimal(0, 15, 3));
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoImpBnsDaSco().setWpmoImpBnsDaSco(new AfDecimal(0, 15, 3));
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoPcAnticBns().setWpmoPcAnticBns(new AfDecimal(0, 6, 3));
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoTpRinnColl("");
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoTpRivalPre("");
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoTpRivalPrstz("");
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoFlEvidRival(Types.SPACE_CHAR);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoUltPcPerd().setWpmoUltPcPerd(new AfDecimal(0, 6, 3));
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoTotAaGiaPror().setWpmoTotAaGiaPror(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoTpOpz("");
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoAaRenCer().setWpmoAaRenCer(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoPcRevrsb().setWpmoPcRevrsb(new AfDecimal(0, 6, 3));
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoImpRiscParzPrgt().setWpmoImpRiscParzPrgt(new AfDecimal(0, 15, 3));
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoImpLrdDiRat().setWpmoImpLrdDiRat(new AfDecimal(0, 15, 3));
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoIbOgg("");
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoCosOner().setWpmoCosOner(new AfDecimal(0, 15, 3));
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoSpePc().setWpmoSpePc(new AfDecimal(0, 6, 3));
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoFlAttivGar(Types.SPACE_CHAR);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoCambioVerProd(Types.SPACE_CHAR);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoMmDiff().setWpmoMmDiff(((short)0));
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoImpRatManfee().setWpmoImpRatManfee(new AfDecimal(0, 15, 3));
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoDtUltErogManfee().setWpmoDtUltErogManfee(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoTpOggRival("");
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoSomAsstaGarac().setWpmoSomAsstaGarac(new AfDecimal(0, 15, 3));
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoPcApplzOpz().setWpmoPcApplzOpz(new AfDecimal(0, 6, 3));
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoIdAdes().setWpmoIdAdes(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoIdPoli(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoTpFrmAssva("");
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoDsRiga(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoDsOperSql(Types.SPACE_CHAR);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoDsVer(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoDsTsIniCptz(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoDsTsEndCptz(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoDsUtente("");
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoDsStatoElab(Types.SPACE_CHAR);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoTpEstrCnt("");
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoCodRamo("");
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoGenDaSin(Types.SPACE_CHAR);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoCodTari("");
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoNumRatPagPre().setWpmoNumRatPagPre(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoPcServVal().setWpmoPcServVal(new AfDecimal(0, 6, 3));
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoEtaAaSoglBnficr().setWpmoEtaAaSoglBnficr(((short)0));
        }
    }
}
