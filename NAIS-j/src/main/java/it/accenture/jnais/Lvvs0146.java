package it.accenture.jnais;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Idsv0003CampiEsito;
import it.accenture.jnais.ws.enums.Idsv0003Sqlcode;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.Lvvs0146Data;

/**Original name: LVVS0146<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2007.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *   PROGRAMMA...... LVVS0146
 *   TIPOLOGIA...... SERVIZIO
 *   PROCESSO....... XXX
 *   FUNZIONE....... XXX
 *   DESCRIZIONE.... ESTRAZIONE PREMIO NETTO
 * **------------------------------------------------------------***</pre>*/
public class Lvvs0146 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lvvs0146Data ws = new Lvvs0146Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS0146
    private Ivvc0213 ivvc0213;

    //==== METHODS ====
    /**Original name: PROGRAM_LVVS0146_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //                  THRU EX-S1000
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc() && this.idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM S1000-ELABORAZIONE
            //              THRU EX-S1000
            s1000Elaborazione();
        }
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lvvs0146 getInstance() {
        return ((Lvvs0146)Programs.getInstance(Lvvs0146.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                        IX-INDICI.
        initIxIndici();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE
        //             TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
        // COB_CODE: IF IVVC0213-ID-ADE-FITTZIO
        //              SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
        //           END-IF.
        if (ivvc0213.getModalitaIdAde().isIvvc0213IdAdeFittzio()) {
            // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
            idsv0003.getReturnCode().setFieldNotValued();
        }
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*
	 * --> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 * --> RISPETTIVE AREE DCLGEN IN WORKING</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: PERFORM S1100-VALORIZZA-DCLGEN
        //              THRU S1100-VALORIZZA-DCLGEN-EX
        //           VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
        //                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //                   SPACES OR LOW-VALUE OR HIGH-VALUE
        ws.setIxDclgen(((short)1));
        while (!(ws.getIxDclgen() > ivvc0213.getEleInfoMax() || Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias()) || Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()) || Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()))) {
            //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformVaryingStmtImpl @source=LVVS0146.cbl:line=179, because the code is unreachable.
            ws.setIxDclgen(Trunc.toShort(ws.getIxDclgen() + 1, 4));
        }
        //--> PERFORM DI CONTROLLI SUI I CAMPI CARICATI NELLE
        //--> DCLGEN DI WORKING
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //                  THRU S1200-CONTROLLO-DATI-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM S1200-CONTROLLO-DATI
            //              THRU S1200-CONTROLLO-DATI-EX
            s1200ControlloDati();
        }
        //--> RECUPERO IL MOVIMENTO DI COMUNICAZIONE
        // COB_CODE: MOVE IVVC0213-TIPO-MOVI-ORIG TO WS-MOVIMENTO
        ws.getWsMovimento().setWsMovimentoFormatted(ivvc0213.getTipoMoviOrigFormatted());
        // COB_CODE: IF LIQUI-RISTOT-IND OR LIQUI-RISTOT-ADE
        //           OR LIQUI-SININD OR LIQUI-SINADE
        //           OR LIQUI-RISTOT-INCAPIENZA
        //           OR LIQUI-RPP-BENEFICIO-CONTR
        //           OR LIQUI-RISTOT-INCAP
        //              THRU RECUP-MOVI-COMTOT-EX
        //           ELSE
        //              THRU RECUP-MOVI-COMUN-EX
        //           END-IF
        if (ws.getWsMovimento().isLiquiRistotInd() || ws.getWsMovimento().isLiquiRistotAde() || ws.getWsMovimento().isLiquiSinind() || ws.getWsMovimento().isLiquiSinade() || ws.getWsMovimento().isLiquiRistotIncapienza() || ws.getWsMovimento().isLiquiRppBeneficioContr() || ws.getWsMovimento().isLiquiRistotIncap()) {
            // COB_CODE: PERFORM RECUP-MOVI-COMTOT
            //             THRU RECUP-MOVI-COMTOT-EX
            recupMoviComtot();
        }
        else {
            // COB_CODE: PERFORM RECUP-MOVI-COMUN
            //             THRU RECUP-MOVI-COMUN-EX
            recupMoviComun();
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC AND
        //              COMUN-TROV-NO
        //                 TO WK-DATA-CPTZ-PREC
        //           END-IF
        if (idsv0003.getReturnCode().isSuccessfulRc() && ws.getFlagComunTrov().isNo()) {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
            //              TO WK-DATA-EFF-PREC
            ws.setWkDataEffPrec(idsv0003.getDataInizioEffetto());
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA
            //              TO WK-DATA-CPTZ-PREC
            ws.setWkDataCptzPrec(idsv0003.getDataCompetenza());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE IVVC0213-TIPO-MOVI-ORIG           TO WS-MOVIMENTO
            ws.getWsMovimento().setWsMovimentoFormatted(ivvc0213.getTipoMoviOrigFormatted());
            // COB_CODE: SET  IDSV0003-TRATT-X-COMPETENZA       TO TRUE
            idsv0003.getTrattamentoStoricita().setTrattXCompetenza();
            //-->    SE SONO IN FASE DI LIQUIDAZIONE DEVO ESCLUDERE DAL CALCOLO
            //-->    IL VALORE CALCOLATO IN FASE DI COMUNICAZIONE
            // COB_CODE:         IF LIQUI-RISPAR-POLIND
            //                   OR LIQUI-RISPAR-ADE
            //                   OR LIQUI-RISTOT-IND
            //                   OR LIQUI-RISTOT-ADE
            //                   OR LIQUI-SININD
            //                   OR LIQUI-SINADE
            //                   OR LIQUI-RPP-TAKE-PROFIT
            //                   OR LIQUI-RISTOT-INCAPIENZA
            //                   OR LIQUI-RPP-REDDITO-PROGR
            //                   OR LIQUI-RPP-BENEFICIO-CONTR
            //                   OR LIQUI-RISTOT-INCAP
            //                      END-IF
            //                   ELSE
            //           *-->       LETTURA TRCH POSITIVE
            //                      END-IF
            //                   END-IF
            if (ws.getWsMovimento().isLiquiRisparPolind() || ws.getWsMovimento().isLiquiRisparAde() || ws.getWsMovimento().isLiquiRistotInd() || ws.getWsMovimento().isLiquiRistotAde() || ws.getWsMovimento().isLiquiSinind() || ws.getWsMovimento().isLiquiSinade() || ws.getWsMovimento().isLiquiRppTakeProfit() || ws.getWsMovimento().isLiquiRistotIncapienza() || ws.getWsMovimento().isLiquiRppRedditoProgr() || ws.getWsMovimento().isLiquiRppBeneficioContr() || ws.getWsMovimento().isLiquiRistotIncap()) {
                // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WK-DT-INI-EFF-TEMP
                ws.setWkDtIniEffTemp(idsv0003.getDataInizioEffetto());
                // COB_CODE: IF IDSV0003-DATA-COMPETENZA IS NUMERIC
                //              MOVE IDSV0003-DATA-COMPETENZA     TO WK-DT-CPTZ-TEMP
                //           END-IF
                if (Functions.isNumber(idsv0003.getDataCompetenza())) {
                    // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA     TO WK-DT-CPTZ-TEMP
                    ws.setWkDtCptzTemp(idsv0003.getDataCompetenza());
                }
                // COB_CODE: MOVE WK-DATA-EFF-PREC
                //              TO IDSV0003-DATA-INIZIO-EFFETTO
                idsv0003.setDataInizioEffetto(ws.getWkDataEffPrec());
                // COB_CODE: MOVE WK-DATA-CPTZ-PREC
                //              TO IDSV0003-DATA-COMPETENZA
                idsv0003.setDataCompetenza(ws.getWkDataCptzPrec());
                //-->       LETTURA TRCH POSITIVE
                // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
                //           AND IDSV0003-SUCCESSFUL-SQL
                //               PERFORM S1301-LEGGI-TRCH-POS       THRU S1301-EX
                //           END-IF
                if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
                    // COB_CODE: PERFORM S1301-LEGGI-TRCH-POS       THRU S1301-EX
                    s1301LeggiTrchPos();
                }
                //-->       LETTURA TRCH NEGATIVE
                // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
                //           AND IDSV0003-SUCCESSFUL-SQL
                //               PERFORM S1302-LEGGI-TRCH-NEG       THRU S1302-EX
                //           END-IF
                if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
                    // COB_CODE: PERFORM S1302-LEGGI-TRCH-NEG       THRU S1302-EX
                    s1302LeggiTrchNeg();
                }
                // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
                //                 TO IDSV0003-DATA-COMPETENZA
                //           END-IF
                if (idsv0003.getReturnCode().isSuccessfulRc()) {
                    // COB_CODE: MOVE WK-DT-INI-EFF-TEMP
                    //              TO IDSV0003-DATA-INIZIO-EFFETTO
                    idsv0003.setDataInizioEffetto(ws.getWkDtIniEffTemp());
                    // COB_CODE: MOVE WK-DT-CPTZ-TEMP
                    //              TO IDSV0003-DATA-COMPETENZA
                    idsv0003.setDataCompetenza(ws.getWkDtCptzTemp());
                }
            }
            else {
                //-->       LETTURA TRCH POSITIVE
                // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
                //           AND IDSV0003-SUCCESSFUL-SQL
                //               PERFORM S1301-LEGGI-TRCH-POS       THRU S1301-EX
                //           END-IF
                if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
                    // COB_CODE: PERFORM S1301-LEGGI-TRCH-POS       THRU S1301-EX
                    s1301LeggiTrchPos();
                }
                //-->       LETTURA TRCH NEGATIVE
                // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
                //           AND IDSV0003-SUCCESSFUL-SQL
                //               PERFORM S1302-LEGGI-TRCH-NEG       THRU S1302-EX
                //           END-IF
                if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
                    // COB_CODE: PERFORM S1302-LEGGI-TRCH-NEG       THRU S1302-EX
                    s1302LeggiTrchNeg();
                }
            }
        }
    }

    /**Original name: S1200-CONTROLLO-DATI<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLO DATI DCLGEN
	 * ----------------------------------------------------------------*
	 *     MOVE DPOL-TP-FRM-ASSVA            TO WS-TP-FRM-ASSVA</pre>*/
    private void s1200ControlloDati() {
        // COB_CODE: IF IVVC0213-ID-GARANZIA IS NUMERIC
        //              END-IF
        //           ELSE
        //                TO IDSV0003-DESCRIZ-ERR-DB2
        //           END-IF.
        if (Functions.isNumber(ivvc0213.getIdGaranziaFormatted())) {
            // COB_CODE: IF IVVC0213-ID-GARANZIA = 0
            //               TO IDSV0003-DESCRIZ-ERR-DB2
            //           END-IF
            if (ivvc0213.getIdGaranzia() == 0) {
                // COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
                // COB_CODE: MOVE WK-PGM
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE 'ID-GAR NON VALORIZZATO'
                //             TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("ID-GAR NON VALORIZZATO");
            }
        }
        else {
            // COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
            idsv0003.getReturnCode().setFieldNotValued();
            // COB_CODE: MOVE WK-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'ID-GAR NON VALORIZZATO'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("ID-GAR NON VALORIZZATO");
        }
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //             END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: IF COLLETTIVA  AND
            //              IVVC0213-ID-ADESIONE  = ZEROES
            //                TO IDSV0003-DESCRIZ-ERR-DB2
            //           END-IF
            if (ws.getWsTpFrmAssva().isCollettiva() && Characters.EQ_ZERO.test(ivvc0213.getIdAdesioneFormatted())) {
                // COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
                // COB_CODE: MOVE WK-PGM
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE 'ID-ADESIONE NO VALORIZZATO PER POLIZZA COLLETTIVA'
                //             TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("ID-ADESIONE NO VALORIZZATO PER POLIZZA COLLETTIVA");
            }
        }
    }

    /**Original name: S1301-LEGGI-TRCH-POS<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI
	 * ----------------------------------------------------------------*</pre>*/
    private void s1301LeggiTrchPos() {
        Ldbs7120 ldbs7120 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: SET IDSV0003-SELECT           TO TRUE
        idsv0003.getOperazione().setSelect();
        // COB_CODE: SET IDSV0003-WHERE-CONDITION  TO TRUE
        idsv0003.getLivelloOperazione().setWhereCondition();
        //
        // COB_CODE: INITIALIZE LDBV7121.
        initLdbv7121();
        //
        // COB_CODE: MOVE IVVC0213-ID-GARANZIA     TO LDBV7121-ID-GAR.
        ws.getLdbv7121().setIdAdes(ivvc0213.getIdGaranzia());
        // COB_CODE: MOVE WS-TRCH-POS              TO LDBV7121-TP-TRCH.
        ws.getLdbv7121().getTpTrch().setTpTrchBytes(ws.getWsTrchPos().getWsTrchPosBytes());
        //
        // COB_CODE:      CALL WK-CALL-PGM  USING  IDSV0003 LDBV7121
        //           *
        //                ON EXCEPTION
        //                     SET  IDSV0003-INVALID-OPER         TO TRUE
        //                END-CALL.
        try {
            ldbs7120 = Ldbs7120.getInstance();
            ldbs7120.run(idsv0003, ws.getLdbv7121());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-CALL-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: MOVE 'CALL-LDBS7120 ERRORE CHIAMATA '
            //              TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBS7120 ERRORE CHIAMATA ");
            // COB_CODE: SET  IDSV0003-INVALID-OPER         TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        //
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //              END-IF
        //           ELSE
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: IF LDBV7121-CUM-PRE-ATT IS NUMERIC
            //                TO WS-CUM-PRE-ATT
            //           END-IF
            if (Functions.isNumber(ws.getLdbv7121().getCumPreAtt())) {
                // COB_CODE: MOVE LDBV7121-CUM-PRE-ATT
                //             TO WS-CUM-PRE-ATT
                ws.setWsCumPreAtt(Trunc.toDecimal(ws.getLdbv7121().getCumPreAtt(), 15, 3));
            }
        }
        else {
            // COB_CODE: MOVE WK-CALL-PGM             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: STRING 'CHIAMATA LDBS7120 ;'
            //                  IDSV0003-RETURN-CODE ';'
            //                  IDSV0003-SQLCODE
            //              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LDBS7120 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //           OR IDSV0003-SQLCODE = -305
            //              SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
            //           ELSE
            //              SET IDSV0003-INVALID-OPER            TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isNotFound() || idsv0003.getSqlcode().getSqlcode() == -305) {
                // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-OPER            TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
        }
    }

    /**Original name: S1302-LEGGI-TRCH-NEG<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI
	 * ----------------------------------------------------------------*</pre>*/
    private void s1302LeggiTrchNeg() {
        Ldbs7120 ldbs7120 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: SET IDSV0003-SELECT           TO TRUE
        idsv0003.getOperazione().setSelect();
        // COB_CODE: SET IDSV0003-WHERE-CONDITION  TO TRUE
        idsv0003.getLivelloOperazione().setWhereCondition();
        //
        // COB_CODE: INITIALIZE LDBV7121.
        initLdbv7121();
        //
        // COB_CODE: MOVE IVVC0213-ID-GARANZIA     TO LDBV7121-ID-GAR.
        ws.getLdbv7121().setIdAdes(ivvc0213.getIdGaranzia());
        // COB_CODE: MOVE WS-TRCH-NEG              TO LDBV7121-TP-TRCH.
        ws.getLdbv7121().getTpTrch().setTpTrchBytes(ws.getWsTrchNeg().getWsTrchNegBytes());
        //
        // COB_CODE:      CALL WK-CALL-PGM  USING  IDSV0003 LDBV7121
        //           *
        //                ON EXCEPTION
        //                     SET IDSV0003-INVALID-OPER  TO TRUE
        //                END-CALL.
        try {
            ldbs7120 = Ldbs7120.getInstance();
            ldbs7120.run(idsv0003, ws.getLdbv7121());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-CALL-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: MOVE 'CALL-LDBS7120 ERRORE CHIAMATA '
            //              TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBS7120 ERRORE CHIAMATA ");
            // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        //
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //              END-IF
        //           ELSE
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: IF LDBV7121-CUM-PRE-ATT IS NUMERIC
            //                                           LDBV7121-CUM-PRE-ATT
            //           END-IF
            if (Functions.isNumber(ws.getLdbv7121().getCumPreAtt())) {
                // COB_CODE: COMPUTE IVVC0213-VAL-IMP-O = WS-CUM-PRE-ATT -
                //                                        LDBV7121-CUM-PRE-ATT
                ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(ws.getWsCumPreAtt().subtract(ws.getLdbv7121().getCumPreAtt()), 18, 7));
            }
        }
        else if (idsv0003.getSqlcode().isNotFound() || idsv0003.getSqlcode().getSqlcode() == -305) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //           OR IDSV0003-SQLCODE = -305
            //              MOVE WS-CUM-PRE-ATT         TO IVVC0213-VAL-IMP-O
            //           ELSE
            //              END-STRING
            //           END-IF
            // COB_CODE: SET IDSV0003-SUCCESSFUL-RC  TO TRUE
            idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
            // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL TO TRUE
            idsv0003.getSqlcode().setSuccessfulSql();
            // COB_CODE: MOVE WS-CUM-PRE-ATT         TO IVVC0213-VAL-IMP-O
            ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(ws.getWsCumPreAtt(), 18, 7));
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER   TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
            // COB_CODE: MOVE WK-CALL-PGM            TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: STRING 'CHIAMATA LDBS7120 ;'
            //                  IDSV0003-RETURN-CODE ';'
            //                  IDSV0003-SQLCODE
            //              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LDBS7120 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
        }
    }

    /**Original name: RECUP-MOVI-COMUN<br>
	 * <pre>----------------------------------------------------------------*
	 *     Recupero il movimento di comunicazione
	 * ----------------------------------------------------------------*</pre>*/
    private void recupMoviComun() {
        Ldbs6040 ldbs6040 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: SET IDSV0003-FETCH-FIRST           TO TRUE
        idsv0003.getOperazione().setFetchFirst();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC         TO TRUE
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL        TO TRUE
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET NO-ULTIMA-LETTURA              TO TRUE
        ws.getFlagUltimaLettura().setNoUltimaLettura();
        // COB_CODE: SET INIT-CUR-MOV                   TO TRUE
        ws.getFlagCurMov().setInitCurMov();
        // COB_CODE: SET COMUN-TROV-NO                  TO TRUE
        ws.getFlagComunTrov().setNo();
        // COB_CODE: PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC
        //                      OR FINE-CUR-MOV
        //                      OR COMUN-TROV-SI
        //              END-IF
        //           END-PERFORM.
        while (!(!idsv0003.getReturnCode().isSuccessfulRc() || ws.getFlagCurMov().isFineCurMov() || ws.getFlagComunTrov().isSi())) {
            // COB_CODE: INITIALIZE MOVI
            initMovi();
            // COB_CODE: MOVE IVVC0213-ID-POLIZZA            TO MOV-ID-OGG
            ws.getMovi().getMovIdOgg().setMovIdOgg(ivvc0213.getIdPolizza());
            // COB_CODE: MOVE 'PO'                           TO MOV-TP-OGG
            ws.getMovi().setMovTpOgg("PO");
            // COB_CODE: SET IDSV0003-TRATT-SENZA-STOR       TO TRUE
            idsv0003.getTrattamentoStoricita().setTrattSenzaStor();
            //--> LIVELLO OPERAZIONE
            // COB_CODE: SET IDSV0003-WHERE-CONDITION       TO TRUE
            idsv0003.getLivelloOperazione().setWhereCondition();
            //--> INIZIALIZZA CODICE DI RITORNO
            // COB_CODE: SET IDSV0003-SUCCESSFUL-RC         TO TRUE
            idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
            // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL        TO TRUE
            idsv0003.getSqlcode().setSuccessfulSql();
            // COB_CODE: CALL LDBS6040   USING  IDSV0003 MOVI
            //           ON EXCEPTION
            //                SET IDSV0003-INVALID-OPER  TO TRUE
            //           END-CALL
            try {
                ldbs6040 = Ldbs6040.getInstance();
                ldbs6040.run(idsv0003, ws.getMovi());
            }
            catch (ProgramExecutionException __ex) {
                // COB_CODE: MOVE LDBS6040
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbs6040());
                // COB_CODE: MOVE 'CALL-LDBS6040 ERRORE CHIAMATA '
                //              TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBS6040 ERRORE CHIAMATA ");
                // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
            // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
            //             END-EVALUATE
            //           ELSE
            //             END-IF
            //           END-IF
            if (idsv0003.getReturnCode().isSuccessfulRc()) {
                // COB_CODE: EVALUATE TRUE
                //               WHEN IDSV0003-NOT-FOUND
                //                 END-IF
                //               WHEN IDSV0003-SUCCESSFUL-SQL
                //                 END-IF
                //               WHEN OTHER
                //                   END-STRING
                //           END-EVALUATE
                switch (idsv0003.getSqlcode().getSqlcode()) {

                    case Idsv0003Sqlcode.NOT_FOUND:// COB_CODE: SET FINE-CUR-MOV TO TRUE
                        ws.getFlagCurMov().setFineCurMov();
                        // COB_CODE: IF IDSV0003-FETCH-FIRST
                        //              END-STRING
                        //           ELSE
                        //              SET IDSV0003-SUCCESSFUL-SQL        TO TRUE
                        //           END-IF
                        if (idsv0003.getOperazione().isFetchFirst()) {
                            // COB_CODE: MOVE LDBS6040
                            //             TO IDSV0003-COD-SERVIZIO-BE
                            idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbs6040());
                            // COB_CODE: STRING 'CHIAMATA LDBS6040 ;'
                            //                  IDSV0003-RETURN-CODE ';'
                            //                  IDSV0003-SQLCODE
                            //           DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                            //           END-STRING
                            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LDBS6040 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                        }
                        else {
                            // COB_CODE: SET IDSV0003-SUCCESSFUL-RC         TO TRUE
                            idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
                            // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL        TO TRUE
                            idsv0003.getSqlcode().setSuccessfulSql();
                        }
                        break;

                    case Idsv0003Sqlcode.SUCCESSFUL_SQL:// COB_CODE: IF SI-ULTIMA-LETTURA
                        //              SET FINE-CUR-MOV   TO TRUE
                        //           ELSE
                        //              SET IDSV0003-FETCH-NEXT   TO TRUE
                        //           END-IF
                        if (ws.getFlagUltimaLettura().isSiUltimaLettura()) {
                            // COB_CODE: SET COMUN-TROV-SI   TO TRUE
                            ws.getFlagComunTrov().setSi();
                            // COB_CODE: PERFORM CLOSE-MOVI
                            //              THRU CLOSE-MOVI-EX
                            closeMovi();
                            // COB_CODE: SET FINE-CUR-MOV   TO TRUE
                            ws.getFlagCurMov().setFineCurMov();
                        }
                        else {
                            // COB_CODE: SET IDSV0003-FETCH-NEXT   TO TRUE
                            idsv0003.getOperazione().setFetchNext();
                        }
                        // COB_CODE: MOVE MOV-TP-MOVI      TO WS-MOVIMENTO
                        ws.getWsMovimento().setWsMovimento(TruncAbs.toInt(ws.getMovi().getMovTpMovi().getMovTpMovi(), 5));
                        // COB_CODE: IF COMUN-RISPAR-IND
                        //           OR COMUN-RISPAR-ADE
                        //           OR RPP-TAKE-PROFIT
                        //           OR RPP-REDDITO-PROGRAMMATO
                        //              END-IF
                        //           END-IF
                        if (ws.getWsMovimento().isComunRisparInd() || ws.getWsMovimento().isComunRisparAde() || ws.getWsMovimento().isRppTakeProfit() || ws.getWsMovimento().isRppRedditoProgrammato()) {
                            // COB_CODE: IF MOV-DT-EFF = IDSV0003-DATA-INIZIO-EFFETTO
                            //                                     - 1
                            //           ELSE
                            //                 TO WK-DATA-CPTZ-PREC
                            //           END-IF
                            if (ws.getMovi().getMovDtEff() == idsv0003.getDataInizioEffetto()) {
                                // COB_CODE: MOVE MOV-ID-MOVI    TO WK-ID-MOVI-COMUN
                                ws.setWkIdMoviComun(ws.getMovi().getMovIdMovi());
                                // COB_CODE: SET SI-ULTIMA-LETTURA  TO TRUE
                                ws.getFlagUltimaLettura().setSiUltimaLettura();
                                // COB_CODE: MOVE MOV-DT-EFF     TO WK-DATA-EFF-PREC
                                ws.setWkDataEffPrec(ws.getMovi().getMovDtEff());
                                // COB_CODE: COMPUTE WK-DATA-CPTZ-PREC =  MOV-DS-TS-CPTZ
                                //                                  - 1
                                ws.setWkDataCptzPrec(Trunc.toLong(ws.getMovi().getMovDsTsCptz() - 1, 18));
                            }
                            else {
                                // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
                                //              TO WK-DATA-EFF-PREC
                                ws.setWkDataEffPrec(idsv0003.getDataInizioEffetto());
                                // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA
                                //              TO WK-DATA-CPTZ-PREC
                                ws.setWkDataCptzPrec(idsv0003.getDataCompetenza());
                            }
                        }
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
                        idsv0003.getReturnCode().setInvalidOper();
                        // COB_CODE: MOVE LDBS6040
                        //             TO IDSV0003-COD-SERVIZIO-BE
                        idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbs6040());
                        // COB_CODE: STRING 'ERRORE RECUP MOVI COMUN ;'
                        //                  IDSV0003-RETURN-CODE ';'
                        //                  IDSV0003-SQLCODE
                        //                  DELIMITED BY SIZE INTO
                        //                  IDSV0003-DESCRIZ-ERR-DB2
                        //           END-STRING
                        concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "ERRORE RECUP MOVI COMUN ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                        idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                        break;
                }
            }
            else {
                // COB_CODE: MOVE LDBS6040
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbs6040());
                // COB_CODE: STRING 'CHIAMATA LDBS6040 ;'
                //                  IDSV0003-RETURN-CODE ';'
                //                  IDSV0003-SQLCODE
                //              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LDBS6040 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                // COB_CODE: IF IDSV0003-NOT-FOUND
                //           OR IDSV0003-SQLCODE = -305
                //              SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                //           ELSE
                //              SET IDSV0003-INVALID-OPER            TO TRUE
                //           END-IF
                if (idsv0003.getSqlcode().isNotFound() || idsv0003.getSqlcode().getSqlcode() == -305) {
                    // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                    idsv0003.getReturnCode().setFieldNotValued();
                }
                else {
                    // COB_CODE: SET IDSV0003-INVALID-OPER            TO TRUE
                    idsv0003.getReturnCode().setInvalidOper();
                }
            }
        }
    }

    /**Original name: RECUP-MOVI-COMTOT<br>
	 * <pre>----------------------------------------------------------------*
	 *     Recupero il movimento di comunicazione
	 * ----------------------------------------------------------------*</pre>*/
    private void recupMoviComtot() {
        Ldbs6040 ldbs6040 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: SET IDSV0003-FETCH-FIRST           TO TRUE
        idsv0003.getOperazione().setFetchFirst();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC         TO TRUE
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL        TO TRUE
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET NO-ULTIMA-LETTURA              TO TRUE
        ws.getFlagUltimaLettura().setNoUltimaLettura();
        // COB_CODE: SET INIT-CUR-MOV                   TO TRUE
        ws.getFlagCurMov().setInitCurMov();
        // COB_CODE: SET COMUN-TROV-NO                  TO TRUE
        ws.getFlagComunTrov().setNo();
        // COB_CODE: PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC
        //                      OR FINE-CUR-MOV
        //                      OR COMUN-TROV-SI
        //              END-IF
        //           END-PERFORM.
        while (!(!idsv0003.getReturnCode().isSuccessfulRc() || ws.getFlagCurMov().isFineCurMov() || ws.getFlagComunTrov().isSi())) {
            // COB_CODE: INITIALIZE MOVI
            initMovi();
            // COB_CODE: MOVE IVVC0213-ID-POLIZZA            TO MOV-ID-OGG
            ws.getMovi().getMovIdOgg().setMovIdOgg(ivvc0213.getIdPolizza());
            // COB_CODE: MOVE 'PO'                           TO MOV-TP-OGG
            ws.getMovi().setMovTpOgg("PO");
            // COB_CODE: SET IDSV0003-TRATT-SENZA-STOR       TO TRUE
            idsv0003.getTrattamentoStoricita().setTrattSenzaStor();
            //--> LIVELLO OPERAZIONE
            // COB_CODE: SET IDSV0003-WHERE-CONDITION       TO TRUE
            idsv0003.getLivelloOperazione().setWhereCondition();
            //--> INIZIALIZZA CODICE DI RITORNO
            // COB_CODE: SET IDSV0003-SUCCESSFUL-RC         TO TRUE
            idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
            // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL        TO TRUE
            idsv0003.getSqlcode().setSuccessfulSql();
            // COB_CODE: CALL LDBS6040   USING  IDSV0003 MOVI
            //           ON EXCEPTION
            //                SET IDSV0003-INVALID-OPER  TO TRUE
            //           END-CALL
            try {
                ldbs6040 = Ldbs6040.getInstance();
                ldbs6040.run(idsv0003, ws.getMovi());
            }
            catch (ProgramExecutionException __ex) {
                // COB_CODE: MOVE LDBS6040
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbs6040());
                // COB_CODE: MOVE 'CALL-LDBS6040 ERRORE CHIAMATA '
                //              TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBS6040 ERRORE CHIAMATA ");
                // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
            // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
            //             END-EVALUATE
            //           ELSE
            //             END-IF
            //           END-IF
            if (idsv0003.getReturnCode().isSuccessfulRc()) {
                // COB_CODE: EVALUATE TRUE
                //               WHEN IDSV0003-NOT-FOUND
                //                 END-IF
                //               WHEN IDSV0003-SUCCESSFUL-SQL
                //                 END-IF
                //               WHEN OTHER
                //                   END-STRING
                //           END-EVALUATE
                switch (idsv0003.getSqlcode().getSqlcode()) {

                    case Idsv0003Sqlcode.NOT_FOUND:// COB_CODE: SET FINE-CUR-MOV TO TRUE
                        ws.getFlagCurMov().setFineCurMov();
                        // COB_CODE: IF IDSV0003-FETCH-FIRST
                        //              END-STRING
                        //           ELSE
                        //              SET IDSV0003-SUCCESSFUL-SQL        TO TRUE
                        //           END-IF
                        if (idsv0003.getOperazione().isFetchFirst()) {
                            // COB_CODE: MOVE LDBS6040
                            //             TO IDSV0003-COD-SERVIZIO-BE
                            idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbs6040());
                            // COB_CODE: STRING 'CHIAMATA LDBS6040 ;'
                            //                  IDSV0003-RETURN-CODE ';'
                            //                  IDSV0003-SQLCODE
                            //           DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                            //           END-STRING
                            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LDBS6040 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                        }
                        else {
                            // COB_CODE: SET IDSV0003-SUCCESSFUL-RC         TO TRUE
                            idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
                            // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL        TO TRUE
                            idsv0003.getSqlcode().setSuccessfulSql();
                        }
                        break;

                    case Idsv0003Sqlcode.SUCCESSFUL_SQL:// COB_CODE: IF SI-ULTIMA-LETTURA
                        //              SET FINE-CUR-MOV   TO TRUE
                        //           ELSE
                        //              SET IDSV0003-FETCH-NEXT   TO TRUE
                        //           END-IF
                        if (ws.getFlagUltimaLettura().isSiUltimaLettura()) {
                            // COB_CODE: SET COMUN-TROV-SI   TO TRUE
                            ws.getFlagComunTrov().setSi();
                            // COB_CODE: PERFORM CLOSE-MOVI
                            //              THRU CLOSE-MOVI-EX
                            closeMovi();
                            // COB_CODE: SET FINE-CUR-MOV   TO TRUE
                            ws.getFlagCurMov().setFineCurMov();
                        }
                        else {
                            // COB_CODE: SET IDSV0003-FETCH-NEXT   TO TRUE
                            idsv0003.getOperazione().setFetchNext();
                        }
                        // COB_CODE: MOVE MOV-TP-MOVI      TO WS-MOVIMENTO
                        ws.getWsMovimento().setWsMovimento(TruncAbs.toInt(ws.getMovi().getMovTpMovi().getMovTpMovi(), 5));
                        // COB_CODE: IF RISTO-INDIVI
                        //           OR RISTO-ADESIO
                        //           OR SINIS-INDIVI
                        //           OR SINIS-ADESIO
                        //           OR COMUN-RISTOT-INCAPIENZA
                        //           OR RPP-BENEFICIO-CONTR
                        //           OR COMUN-RISTOT-INCAP
                        //              END-IF
                        //           END-IF
                        if (ws.getWsMovimento().isRistoIndivi() || ws.getWsMovimento().isRistoAdesio() || ws.getWsMovimento().isSinisIndivi() || ws.getWsMovimento().isSinisAdesio() || ws.getWsMovimento().isComunRistotIncapienza() || ws.getWsMovimento().isRppBeneficioContr() || ws.getWsMovimento().isComunRistotIncap()) {
                            // COB_CODE: IF MOV-DT-EFF = IDSV0003-DATA-INIZIO-EFFETTO
                            //                                     - 1
                            //           ELSE
                            //                 TO WK-DATA-CPTZ-PREC
                            //           END-IF
                            if (ws.getMovi().getMovDtEff() == idsv0003.getDataInizioEffetto()) {
                                // COB_CODE: MOVE MOV-ID-MOVI    TO WK-ID-MOVI-COMUN
                                ws.setWkIdMoviComun(ws.getMovi().getMovIdMovi());
                                // COB_CODE: SET SI-ULTIMA-LETTURA  TO TRUE
                                ws.getFlagUltimaLettura().setSiUltimaLettura();
                                // COB_CODE: MOVE MOV-DT-EFF     TO WK-DATA-EFF-PREC
                                ws.setWkDataEffPrec(ws.getMovi().getMovDtEff());
                                // COB_CODE: COMPUTE WK-DATA-CPTZ-PREC =  MOV-DS-TS-CPTZ
                                //                                  - 1
                                ws.setWkDataCptzPrec(Trunc.toLong(ws.getMovi().getMovDsTsCptz() - 1, 18));
                            }
                            else {
                                // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
                                //              TO WK-DATA-EFF-PREC
                                ws.setWkDataEffPrec(idsv0003.getDataInizioEffetto());
                                // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA
                                //              TO WK-DATA-CPTZ-PREC
                                ws.setWkDataCptzPrec(idsv0003.getDataCompetenza());
                            }
                        }
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
                        idsv0003.getReturnCode().setInvalidOper();
                        // COB_CODE: MOVE LDBS6040
                        //             TO IDSV0003-COD-SERVIZIO-BE
                        idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbs6040());
                        // COB_CODE: STRING 'ERRORE RECUP MOVI COMUN ;'
                        //                  IDSV0003-RETURN-CODE ';'
                        //                  IDSV0003-SQLCODE
                        //                  DELIMITED BY SIZE INTO
                        //                  IDSV0003-DESCRIZ-ERR-DB2
                        //           END-STRING
                        concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "ERRORE RECUP MOVI COMUN ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                        idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                        break;
                }
            }
            else {
                // COB_CODE: MOVE LDBS6040
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbs6040());
                // COB_CODE: STRING 'CHIAMATA LDBS6040 ;'
                //                  IDSV0003-RETURN-CODE ';'
                //                  IDSV0003-SQLCODE
                //              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LDBS6040 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                // COB_CODE: IF IDSV0003-NOT-FOUND
                //           OR IDSV0003-SQLCODE = -305
                //              SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                //           ELSE
                //              SET IDSV0003-INVALID-OPER            TO TRUE
                //           END-IF
                if (idsv0003.getSqlcode().isNotFound() || idsv0003.getSqlcode().getSqlcode() == -305) {
                    // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                    idsv0003.getReturnCode().setFieldNotValued();
                }
                else {
                    // COB_CODE: SET IDSV0003-INVALID-OPER            TO TRUE
                    idsv0003.getReturnCode().setInvalidOper();
                }
            }
        }
    }

    /**Original name: CLOSE-MOVI<br>
	 * <pre>----------------------------------------------------------------*
	 *     CHIUDO IL CURSORE SULLA TABELLA MOVIMENTO LDBS6040
	 * ----------------------------------------------------------------*</pre>*/
    private void closeMovi() {
        Ldbs6040 ldbs6040 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: SET IDSV0003-TRATT-SENZA-STOR   TO TRUE
        idsv0003.getTrattamentoStoricita().setTrattSenzaStor();
        // COB_CODE: MOVE SPACES                     TO IDSV0003-BUFFER-WHERE-COND
        idsv0003.setBufferWhereCond("");
        //
        // COB_CODE: SET IDSV0003-CLOSE-CURSOR       TO TRUE.
        idsv0003.getOperazione().setIdsv0003CloseCursor();
        // COB_CODE: SET IDSV0003-WHERE-CONDITION    TO TRUE.
        idsv0003.getLivelloOperazione().setWhereCondition();
        //
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC      TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL     TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: CALL LDBS6040    USING  IDSV0003 MOVI
        //           ON EXCEPTION
        //                SET IDSV0003-INVALID-OPER  TO TRUE
        //           END-CALL
        try {
            ldbs6040 = Ldbs6040.getInstance();
            ldbs6040.run(idsv0003, ws.getMovi());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE 'LDBS6040'
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe("LDBS6040");
            // COB_CODE: MOVE 'CALL-LDBS6040 ERRORE CHIAMATA '
            //              TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBS6040 ERRORE CHIAMATA ");
            // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           ELSE
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: EVALUATE TRUE
            //               WHEN IDSV0003-SUCCESSFUL-SQL
            //                   CONTINUE
            //               WHEN OTHER
            //                   END-IF
            //           END-EVALUATE
            switch (idsv0003.getSqlcode().getSqlcode()) {

                case Idsv0003Sqlcode.SUCCESSFUL_SQL:// COB_CODE: CONTINUE
                //continue
                    break;

                default:// COB_CODE: MOVE LDBS6040
                    //             TO IDSV0003-COD-SERVIZIO-BE
                    idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbs6040());
                    // COB_CODE: STRING 'CHIAMATA LDBS6040;'
                    //                  IDSV0003-RETURN-CODE ';'
                    //                  IDSV0003-SQLCODE
                    //              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LDBS6040;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                    idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                    // COB_CODE: IF IDSV0003-NOT-FOUND
                    //           OR IDSV0003-SQLCODE = -305
                    //              SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                    //           ELSE
                    //              SET IDSV0003-INVALID-OPER            TO TRUE
                    //           END-IF
                    if (idsv0003.getSqlcode().isNotFound() || idsv0003.getSqlcode().getSqlcode() == -305) {
                        // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                        idsv0003.getReturnCode().setFieldNotValued();
                    }
                    else {
                        // COB_CODE: SET IDSV0003-INVALID-OPER            TO TRUE
                        idsv0003.getReturnCode().setInvalidOper();
                    }
                    break;
            }
        }
        else {
            // COB_CODE: MOVE LDBS6040
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbs6040());
            // COB_CODE: STRING 'CHIAMATA LDBS6040;'
            //                  IDSV0003-RETURN-CODE ';'
            //                  IDSV0003-SQLCODE
            //              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LDBS6040;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //           OR IDSV0003-SQLCODE = -305
            //              SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
            //           ELSE
            //              SET IDSV0003-INVALID-OPER            TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isNotFound() || idsv0003.getSqlcode().getSqlcode() == -305) {
                // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-OPER            TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    public void initIxIndici() {
        ws.setIxDclgen(((short)0));
    }

    public void initLdbv7121() {
        ws.getLdbv7121().getTpTrch().setTrch1("");
        ws.getLdbv7121().getTpTrch().setTrch2("");
        ws.getLdbv7121().getTpTrch().setTrch3("");
        ws.getLdbv7121().getTpTrch().setTrch4("");
        ws.getLdbv7121().getTpTrch().setTrch5("");
        ws.getLdbv7121().getTpTrch().setTrch6("");
        ws.getLdbv7121().getTpTrch().setTrch7("");
        ws.getLdbv7121().getTpTrch().setTrch8("");
        ws.getLdbv7121().getTpTrch().setTrch9("");
        ws.getLdbv7121().getTpTrch().setTrch10("");
        ws.getLdbv7121().getTpTrch().setTrch11("");
        ws.getLdbv7121().getTpTrch().setTrch12("");
        ws.getLdbv7121().getTpTrch().setTrch13("");
        ws.getLdbv7121().getTpTrch().setTrch14("");
        ws.getLdbv7121().getTpTrch().setTrch15("");
        ws.getLdbv7121().getTpTrch().setTrch16("");
        ws.getLdbv7121().setIdAdes(0);
        ws.getLdbv7121().setCumPreAtt(new AfDecimal(0, 15, 3));
    }

    public void initMovi() {
        ws.getMovi().setMovIdMovi(0);
        ws.getMovi().setMovCodCompAnia(0);
        ws.getMovi().getMovIdOgg().setMovIdOgg(0);
        ws.getMovi().setMovIbOgg("");
        ws.getMovi().setMovIbMovi("");
        ws.getMovi().setMovTpOgg("");
        ws.getMovi().getMovIdRich().setMovIdRich(0);
        ws.getMovi().getMovTpMovi().setMovTpMovi(0);
        ws.getMovi().setMovDtEff(0);
        ws.getMovi().getMovIdMoviAnn().setMovIdMoviAnn(0);
        ws.getMovi().getMovIdMoviCollg().setMovIdMoviCollg(0);
        ws.getMovi().setMovDsOperSql(Types.SPACE_CHAR);
        ws.getMovi().setMovDsVer(0);
        ws.getMovi().setMovDsTsCptz(0);
        ws.getMovi().setMovDsUtente("");
        ws.getMovi().setMovDsStatoElab(Types.SPACE_CHAR);
    }
}
