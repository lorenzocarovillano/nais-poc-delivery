package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.BnficDao;
import it.accenture.jnais.commons.data.to.IBnfic;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.BnficIdbsbep0;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idbsbep0Data;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.redefines.BepCodBnfic;
import it.accenture.jnais.ws.redefines.BepIdBnficr;
import it.accenture.jnais.ws.redefines.BepIdMoviChiu;
import it.accenture.jnais.ws.redefines.BepPcDelBnficr;

/**Original name: IDBSBEP0<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  09 AGO 2018.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Idbsbep0 extends Program implements IBnfic {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private BnficDao bnficDao = new BnficDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Idbsbep0Data ws = new Idbsbep0Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: BNFIC
    private BnficIdbsbep0 bnfic;

    //==== METHODS ====
    /**Original name: PROGRAM_IDBSBEP0_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, BnficIdbsbep0 bnfic) {
        this.idsv0003 = idsv0003;
        this.bnfic = bnfic;
        // COB_CODE: PERFORM A000-INIZIO                    THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO          THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS          THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO          THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                        a300ElaboraIdEff();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                        a400ElaboraIdpEff();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO          THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS          THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO          THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                        b300ElaboraIdCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                        b400ElaboraIdpCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                        b500ElaboraIboCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                        b600ElaboraIbsCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                        b700ElaboraIdoCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //              SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-PRIMARY-KEY
                //                 PERFORM A200-ELABORA-PK          THRU A200-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO         THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS         THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO         THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.PRIMARY_KEY:// COB_CODE: PERFORM A200-ELABORA-PK          THRU A200-EX
                        a200ElaboraPk();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO         THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS         THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO         THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Idbsbep0 getInstance() {
        return ((Idbsbep0)Programs.getInstance(Idbsbep0.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'IDBSBEP0'   TO IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("IDBSBEP0");
        // COB_CODE: MOVE 'BNFIC' TO IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("BNFIC");
        // COB_CODE: MOVE '00'                     TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                   TO   IDSV0003-SQLCODE
        //                                              IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                              IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-AGG-STORICO-SOLO-INS  OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isAggStoricoSoloIns() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-PK<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a200ElaboraPk() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-PK          THRU A210-EX
        //              WHEN IDSV0003-INSERT
        //                 PERFORM A220-INSERT-PK          THRU A220-EX
        //              WHEN IDSV0003-UPDATE
        //                 PERFORM A230-UPDATE-PK          THRU A230-EX
        //              WHEN IDSV0003-DELETE
        //                 PERFORM A240-DELETE-PK          THRU A240-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-PK          THRU A210-EX
            a210SelectPk();
        }
        else if (idsv0003.getOperazione().isInsert()) {
            // COB_CODE: PERFORM A220-INSERT-PK          THRU A220-EX
            a220InsertPk();
        }
        else if (idsv0003.getOperazione().isUpdate()) {
            // COB_CODE: PERFORM A230-UPDATE-PK          THRU A230-EX
            a230UpdatePk();
        }
        else if (idsv0003.getOperazione().isDelete()) {
            // COB_CODE: PERFORM A240-DELETE-PK          THRU A240-EX
            a240DeletePk();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A300-ELABORA-ID-EFF<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void a300ElaboraIdEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A310-SELECT-ID-EFF          THRU A310-EX
            a310SelectIdEff();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A400-ELABORA-IDP-EFF<br>*/
    private void a400ElaboraIdpEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
            a410SelectIdpEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
            a460OpenCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
            a470CloseCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
            a480FetchFirstIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
            a490FetchNextIdpEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A500-ELABORA-IBO<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a500ElaboraIbo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A510-SELECT-IBO             THRU A510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A510-SELECT-IBO             THRU A510-EX
            a510SelectIbo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
            a560OpenCursorIbo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
            a570CloseCursorIbo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
            a580FetchFirstIbo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
            a590FetchNextIbo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A600-ELABORA-IBS<br>*/
    private void a600ElaboraIbs() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A610-SELECT-IBS             THRU A610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A610-SELECT-IBS             THRU A610-EX
            a610SelectIbs();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
            a660OpenCursorIbs();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
            a670CloseCursorIbs();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
            a680FetchFirstIbs();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
            a690FetchNextIbs();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A700-ELABORA-IDO<br>*/
    private void a700ElaboraIdo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A710-SELECT-IDO                 THRU A710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A710-SELECT-IDO                 THRU A710-EX
            a710SelectIdo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
            a760OpenCursorIdo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
            a770CloseCursorIdo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
            a780FetchFirstIdo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
            a790FetchNextIdo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B300-ELABORA-ID-CPZ<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void b300ElaboraIdCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
            b310SelectIdCpz();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B400-ELABORA-IDP-CPZ<br>*/
    private void b400ElaboraIdpCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
            b410SelectIdpCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
            b460OpenCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
            b470CloseCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
            b480FetchFirstIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
            b490FetchNextIdpCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B500-ELABORA-IBO-CPZ<br>*/
    private void b500ElaboraIboCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
            b510SelectIboCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
            b560OpenCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
            b570CloseCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
            b580FetchFirstIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B600-ELABORA-IBS-CPZ<br>*/
    private void b600ElaboraIbsCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
            b610SelectIbsCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
            b660OpenCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
            b670CloseCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
            b680FetchFirstIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B700-ELABORA-IDO-CPZ<br>*/
    private void b700ElaboraIdoCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
            b710SelectIdoCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
            b760OpenCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
            b770CloseCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
            b780FetchFirstIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A210-SELECT-PK<br>
	 * <pre>----
	 * ----  gestione PK
	 * ----</pre>*/
    private void a210SelectPk() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_BNFIC
        //                ,ID_RAPP_ANA
        //                ,ID_BNFICR
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,COD_BNFIC
        //                ,TP_IND_BNFICR
        //                ,COD_BNFICR
        //                ,DESC_BNFICR
        //                ,PC_DEL_BNFICR
        //                ,FL_ESE
        //                ,FL_IRREV
        //                ,FL_DFLT
        //                ,ESRCN_ATTVT_IMPRS
        //                ,FL_BNFICR_COLL
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,TP_NORMAL_BNFIC
        //             INTO
        //                :BEP-ID-BNFIC
        //               ,:BEP-ID-RAPP-ANA
        //               ,:BEP-ID-BNFICR
        //                :IND-BEP-ID-BNFICR
        //               ,:BEP-ID-MOVI-CRZ
        //               ,:BEP-ID-MOVI-CHIU
        //                :IND-BEP-ID-MOVI-CHIU
        //               ,:BEP-DT-INI-EFF-DB
        //               ,:BEP-DT-END-EFF-DB
        //               ,:BEP-COD-COMP-ANIA
        //               ,:BEP-COD-BNFIC
        //                :IND-BEP-COD-BNFIC
        //               ,:BEP-TP-IND-BNFICR
        //               ,:BEP-COD-BNFICR
        //                :IND-BEP-COD-BNFICR
        //               ,:BEP-DESC-BNFICR-VCHAR
        //                :IND-BEP-DESC-BNFICR
        //               ,:BEP-PC-DEL-BNFICR
        //                :IND-BEP-PC-DEL-BNFICR
        //               ,:BEP-FL-ESE
        //                :IND-BEP-FL-ESE
        //               ,:BEP-FL-IRREV
        //                :IND-BEP-FL-IRREV
        //               ,:BEP-FL-DFLT
        //                :IND-BEP-FL-DFLT
        //               ,:BEP-ESRCN-ATTVT-IMPRS
        //                :IND-BEP-ESRCN-ATTVT-IMPRS
        //               ,:BEP-FL-BNFICR-COLL
        //               ,:BEP-DS-RIGA
        //               ,:BEP-DS-OPER-SQL
        //               ,:BEP-DS-VER
        //               ,:BEP-DS-TS-INI-CPTZ
        //               ,:BEP-DS-TS-END-CPTZ
        //               ,:BEP-DS-UTENTE
        //               ,:BEP-DS-STATO-ELAB
        //               ,:BEP-TP-NORMAL-BNFIC
        //                :IND-BEP-TP-NORMAL-BNFIC
        //             FROM BNFIC
        //             WHERE     DS_RIGA = :BEP-DS-RIGA
        //           END-EXEC.
        bnficDao.selectByBepDsRiga(bnfic.getBepDsRiga(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A220-INSERT-PK<br>*/
    private void a220InsertPk() {
        // COB_CODE: PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.
        z400SeqRiga();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX
            z150ValorizzaDataServicesI();
            // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX
            z200SetIndicatoriNull();
            // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX
            z900ConvertiNToX();
            // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX
            z960LengthVchar();
            // COB_CODE: EXEC SQL
            //              INSERT
            //              INTO BNFIC
            //                  (
            //                     ID_BNFIC
            //                    ,ID_RAPP_ANA
            //                    ,ID_BNFICR
            //                    ,ID_MOVI_CRZ
            //                    ,ID_MOVI_CHIU
            //                    ,DT_INI_EFF
            //                    ,DT_END_EFF
            //                    ,COD_COMP_ANIA
            //                    ,COD_BNFIC
            //                    ,TP_IND_BNFICR
            //                    ,COD_BNFICR
            //                    ,DESC_BNFICR
            //                    ,PC_DEL_BNFICR
            //                    ,FL_ESE
            //                    ,FL_IRREV
            //                    ,FL_DFLT
            //                    ,ESRCN_ATTVT_IMPRS
            //                    ,FL_BNFICR_COLL
            //                    ,DS_RIGA
            //                    ,DS_OPER_SQL
            //                    ,DS_VER
            //                    ,DS_TS_INI_CPTZ
            //                    ,DS_TS_END_CPTZ
            //                    ,DS_UTENTE
            //                    ,DS_STATO_ELAB
            //                    ,TP_NORMAL_BNFIC
            //                  )
            //              VALUES
            //                  (
            //                    :BEP-ID-BNFIC
            //                    ,:BEP-ID-RAPP-ANA
            //                    ,:BEP-ID-BNFICR
            //                     :IND-BEP-ID-BNFICR
            //                    ,:BEP-ID-MOVI-CRZ
            //                    ,:BEP-ID-MOVI-CHIU
            //                     :IND-BEP-ID-MOVI-CHIU
            //                    ,:BEP-DT-INI-EFF-DB
            //                    ,:BEP-DT-END-EFF-DB
            //                    ,:BEP-COD-COMP-ANIA
            //                    ,:BEP-COD-BNFIC
            //                     :IND-BEP-COD-BNFIC
            //                    ,:BEP-TP-IND-BNFICR
            //                    ,:BEP-COD-BNFICR
            //                     :IND-BEP-COD-BNFICR
            //                    ,:BEP-DESC-BNFICR-VCHAR
            //                     :IND-BEP-DESC-BNFICR
            //                    ,:BEP-PC-DEL-BNFICR
            //                     :IND-BEP-PC-DEL-BNFICR
            //                    ,:BEP-FL-ESE
            //                     :IND-BEP-FL-ESE
            //                    ,:BEP-FL-IRREV
            //                     :IND-BEP-FL-IRREV
            //                    ,:BEP-FL-DFLT
            //                     :IND-BEP-FL-DFLT
            //                    ,:BEP-ESRCN-ATTVT-IMPRS
            //                     :IND-BEP-ESRCN-ATTVT-IMPRS
            //                    ,:BEP-FL-BNFICR-COLL
            //                    ,:BEP-DS-RIGA
            //                    ,:BEP-DS-OPER-SQL
            //                    ,:BEP-DS-VER
            //                    ,:BEP-DS-TS-INI-CPTZ
            //                    ,:BEP-DS-TS-END-CPTZ
            //                    ,:BEP-DS-UTENTE
            //                    ,:BEP-DS-STATO-ELAB
            //                    ,:BEP-TP-NORMAL-BNFIC
            //                     :IND-BEP-TP-NORMAL-BNFIC
            //                  )
            //           END-EXEC
            bnficDao.insertRec(this);
            // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
            a100CheckReturnCode();
        }
    }

    /**Original name: A230-UPDATE-PK<br>*/
    private void a230UpdatePk() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE BNFIC SET
        //                   ID_BNFIC               =
        //                :BEP-ID-BNFIC
        //                  ,ID_RAPP_ANA            =
        //                :BEP-ID-RAPP-ANA
        //                  ,ID_BNFICR              =
        //                :BEP-ID-BNFICR
        //                                       :IND-BEP-ID-BNFICR
        //                  ,ID_MOVI_CRZ            =
        //                :BEP-ID-MOVI-CRZ
        //                  ,ID_MOVI_CHIU           =
        //                :BEP-ID-MOVI-CHIU
        //                                       :IND-BEP-ID-MOVI-CHIU
        //                  ,DT_INI_EFF             =
        //           :BEP-DT-INI-EFF-DB
        //                  ,DT_END_EFF             =
        //           :BEP-DT-END-EFF-DB
        //                  ,COD_COMP_ANIA          =
        //                :BEP-COD-COMP-ANIA
        //                  ,COD_BNFIC              =
        //                :BEP-COD-BNFIC
        //                                       :IND-BEP-COD-BNFIC
        //                  ,TP_IND_BNFICR          =
        //                :BEP-TP-IND-BNFICR
        //                  ,COD_BNFICR             =
        //                :BEP-COD-BNFICR
        //                                       :IND-BEP-COD-BNFICR
        //                  ,DESC_BNFICR            =
        //                :BEP-DESC-BNFICR-VCHAR
        //                                       :IND-BEP-DESC-BNFICR
        //                  ,PC_DEL_BNFICR          =
        //                :BEP-PC-DEL-BNFICR
        //                                       :IND-BEP-PC-DEL-BNFICR
        //                  ,FL_ESE                 =
        //                :BEP-FL-ESE
        //                                       :IND-BEP-FL-ESE
        //                  ,FL_IRREV               =
        //                :BEP-FL-IRREV
        //                                       :IND-BEP-FL-IRREV
        //                  ,FL_DFLT                =
        //                :BEP-FL-DFLT
        //                                       :IND-BEP-FL-DFLT
        //                  ,ESRCN_ATTVT_IMPRS      =
        //                :BEP-ESRCN-ATTVT-IMPRS
        //                                       :IND-BEP-ESRCN-ATTVT-IMPRS
        //                  ,FL_BNFICR_COLL         =
        //                :BEP-FL-BNFICR-COLL
        //                  ,DS_RIGA                =
        //                :BEP-DS-RIGA
        //                  ,DS_OPER_SQL            =
        //                :BEP-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :BEP-DS-VER
        //                  ,DS_TS_INI_CPTZ         =
        //                :BEP-DS-TS-INI-CPTZ
        //                  ,DS_TS_END_CPTZ         =
        //                :BEP-DS-TS-END-CPTZ
        //                  ,DS_UTENTE              =
        //                :BEP-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :BEP-DS-STATO-ELAB
        //                  ,TP_NORMAL_BNFIC        =
        //                :BEP-TP-NORMAL-BNFIC
        //                                       :IND-BEP-TP-NORMAL-BNFIC
        //                WHERE     DS_RIGA = :BEP-DS-RIGA
        //           END-EXEC.
        bnficDao.updateRec(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A240-DELETE-PK<br>*/
    private void a240DeletePk() {
        // COB_CODE: EXEC SQL
        //                DELETE
        //                FROM BNFIC
        //                WHERE     DS_RIGA = :BEP-DS-RIGA
        //           END-EXEC.
        bnficDao.deleteByBepDsRiga(bnfic.getBepDsRiga());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A305-DECLARE-CURSOR-ID-EFF<br>
	 * <pre>----
	 * ----  gestione ID Effetto
	 * ----</pre>*/
    private void a305DeclareCursorIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-ID-UPD-EFF-BEP CURSOR FOR
        //              SELECT
        //                     ID_BNFIC
        //                    ,ID_RAPP_ANA
        //                    ,ID_BNFICR
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,COD_BNFIC
        //                    ,TP_IND_BNFICR
        //                    ,COD_BNFICR
        //                    ,DESC_BNFICR
        //                    ,PC_DEL_BNFICR
        //                    ,FL_ESE
        //                    ,FL_IRREV
        //                    ,FL_DFLT
        //                    ,ESRCN_ATTVT_IMPRS
        //                    ,FL_BNFICR_COLL
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,TP_NORMAL_BNFIC
        //              FROM BNFIC
        //              WHERE     ID_BNFIC = :BEP-ID-BNFIC
        //                    AND DS_TS_END_CPTZ = :WS-TS-INFINITO
        //                    AND DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //              ORDER BY DT_INI_EFF ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A310-SELECT-ID-EFF<br>*/
    private void a310SelectIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_BNFIC
        //                ,ID_RAPP_ANA
        //                ,ID_BNFICR
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,COD_BNFIC
        //                ,TP_IND_BNFICR
        //                ,COD_BNFICR
        //                ,DESC_BNFICR
        //                ,PC_DEL_BNFICR
        //                ,FL_ESE
        //                ,FL_IRREV
        //                ,FL_DFLT
        //                ,ESRCN_ATTVT_IMPRS
        //                ,FL_BNFICR_COLL
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,TP_NORMAL_BNFIC
        //             INTO
        //                :BEP-ID-BNFIC
        //               ,:BEP-ID-RAPP-ANA
        //               ,:BEP-ID-BNFICR
        //                :IND-BEP-ID-BNFICR
        //               ,:BEP-ID-MOVI-CRZ
        //               ,:BEP-ID-MOVI-CHIU
        //                :IND-BEP-ID-MOVI-CHIU
        //               ,:BEP-DT-INI-EFF-DB
        //               ,:BEP-DT-END-EFF-DB
        //               ,:BEP-COD-COMP-ANIA
        //               ,:BEP-COD-BNFIC
        //                :IND-BEP-COD-BNFIC
        //               ,:BEP-TP-IND-BNFICR
        //               ,:BEP-COD-BNFICR
        //                :IND-BEP-COD-BNFICR
        //               ,:BEP-DESC-BNFICR-VCHAR
        //                :IND-BEP-DESC-BNFICR
        //               ,:BEP-PC-DEL-BNFICR
        //                :IND-BEP-PC-DEL-BNFICR
        //               ,:BEP-FL-ESE
        //                :IND-BEP-FL-ESE
        //               ,:BEP-FL-IRREV
        //                :IND-BEP-FL-IRREV
        //               ,:BEP-FL-DFLT
        //                :IND-BEP-FL-DFLT
        //               ,:BEP-ESRCN-ATTVT-IMPRS
        //                :IND-BEP-ESRCN-ATTVT-IMPRS
        //               ,:BEP-FL-BNFICR-COLL
        //               ,:BEP-DS-RIGA
        //               ,:BEP-DS-OPER-SQL
        //               ,:BEP-DS-VER
        //               ,:BEP-DS-TS-INI-CPTZ
        //               ,:BEP-DS-TS-END-CPTZ
        //               ,:BEP-DS-UTENTE
        //               ,:BEP-DS-STATO-ELAB
        //               ,:BEP-TP-NORMAL-BNFIC
        //                :IND-BEP-TP-NORMAL-BNFIC
        //             FROM BNFIC
        //             WHERE     ID_BNFIC = :BEP-ID-BNFIC
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        bnficDao.selectRec(bnfic.getBepIdBnfic(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A330-UPDATE-ID-EFF<br>*/
    private void a330UpdateIdEff() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE BNFIC SET
        //                   ID_BNFIC               =
        //                :BEP-ID-BNFIC
        //                  ,ID_RAPP_ANA            =
        //                :BEP-ID-RAPP-ANA
        //                  ,ID_BNFICR              =
        //                :BEP-ID-BNFICR
        //                                       :IND-BEP-ID-BNFICR
        //                  ,ID_MOVI_CRZ            =
        //                :BEP-ID-MOVI-CRZ
        //                  ,ID_MOVI_CHIU           =
        //                :BEP-ID-MOVI-CHIU
        //                                       :IND-BEP-ID-MOVI-CHIU
        //                  ,DT_INI_EFF             =
        //           :BEP-DT-INI-EFF-DB
        //                  ,DT_END_EFF             =
        //           :BEP-DT-END-EFF-DB
        //                  ,COD_COMP_ANIA          =
        //                :BEP-COD-COMP-ANIA
        //                  ,COD_BNFIC              =
        //                :BEP-COD-BNFIC
        //                                       :IND-BEP-COD-BNFIC
        //                  ,TP_IND_BNFICR          =
        //                :BEP-TP-IND-BNFICR
        //                  ,COD_BNFICR             =
        //                :BEP-COD-BNFICR
        //                                       :IND-BEP-COD-BNFICR
        //                  ,DESC_BNFICR            =
        //                :BEP-DESC-BNFICR-VCHAR
        //                                       :IND-BEP-DESC-BNFICR
        //                  ,PC_DEL_BNFICR          =
        //                :BEP-PC-DEL-BNFICR
        //                                       :IND-BEP-PC-DEL-BNFICR
        //                  ,FL_ESE                 =
        //                :BEP-FL-ESE
        //                                       :IND-BEP-FL-ESE
        //                  ,FL_IRREV               =
        //                :BEP-FL-IRREV
        //                                       :IND-BEP-FL-IRREV
        //                  ,FL_DFLT                =
        //                :BEP-FL-DFLT
        //                                       :IND-BEP-FL-DFLT
        //                  ,ESRCN_ATTVT_IMPRS      =
        //                :BEP-ESRCN-ATTVT-IMPRS
        //                                       :IND-BEP-ESRCN-ATTVT-IMPRS
        //                  ,FL_BNFICR_COLL         =
        //                :BEP-FL-BNFICR-COLL
        //                  ,DS_RIGA                =
        //                :BEP-DS-RIGA
        //                  ,DS_OPER_SQL            =
        //                :BEP-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :BEP-DS-VER
        //                  ,DS_TS_INI_CPTZ         =
        //                :BEP-DS-TS-INI-CPTZ
        //                  ,DS_TS_END_CPTZ         =
        //                :BEP-DS-TS-END-CPTZ
        //                  ,DS_UTENTE              =
        //                :BEP-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :BEP-DS-STATO-ELAB
        //                  ,TP_NORMAL_BNFIC        =
        //                :BEP-TP-NORMAL-BNFIC
        //                                       :IND-BEP-TP-NORMAL-BNFIC
        //                WHERE     DS_RIGA = :BEP-DS-RIGA
        //                   AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        bnficDao.updateRec1(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A360-OPEN-CURSOR-ID-EFF<br>*/
    private void a360OpenCursorIdEff() {
        // COB_CODE: PERFORM A305-DECLARE-CURSOR-ID-EFF THRU A305-EX.
        a305DeclareCursorIdEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-ID-UPD-EFF-BEP
        //           END-EXEC.
        bnficDao.openCIdUpdEffBep(bnfic.getBepIdBnfic(), ws.getIdsv0010().getWsTsInfinito(), ws.getIdsv0010().getWsDataInizioEffettoDb(), idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A370-CLOSE-CURSOR-ID-EFF<br>*/
    private void a370CloseCursorIdEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-ID-UPD-EFF-BEP
        //           END-EXEC.
        bnficDao.closeCIdUpdEffBep();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A390-FETCH-NEXT-ID-EFF<br>*/
    private void a390FetchNextIdEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-ID-UPD-EFF-BEP
        //           INTO
        //                :BEP-ID-BNFIC
        //               ,:BEP-ID-RAPP-ANA
        //               ,:BEP-ID-BNFICR
        //                :IND-BEP-ID-BNFICR
        //               ,:BEP-ID-MOVI-CRZ
        //               ,:BEP-ID-MOVI-CHIU
        //                :IND-BEP-ID-MOVI-CHIU
        //               ,:BEP-DT-INI-EFF-DB
        //               ,:BEP-DT-END-EFF-DB
        //               ,:BEP-COD-COMP-ANIA
        //               ,:BEP-COD-BNFIC
        //                :IND-BEP-COD-BNFIC
        //               ,:BEP-TP-IND-BNFICR
        //               ,:BEP-COD-BNFICR
        //                :IND-BEP-COD-BNFICR
        //               ,:BEP-DESC-BNFICR-VCHAR
        //                :IND-BEP-DESC-BNFICR
        //               ,:BEP-PC-DEL-BNFICR
        //                :IND-BEP-PC-DEL-BNFICR
        //               ,:BEP-FL-ESE
        //                :IND-BEP-FL-ESE
        //               ,:BEP-FL-IRREV
        //                :IND-BEP-FL-IRREV
        //               ,:BEP-FL-DFLT
        //                :IND-BEP-FL-DFLT
        //               ,:BEP-ESRCN-ATTVT-IMPRS
        //                :IND-BEP-ESRCN-ATTVT-IMPRS
        //               ,:BEP-FL-BNFICR-COLL
        //               ,:BEP-DS-RIGA
        //               ,:BEP-DS-OPER-SQL
        //               ,:BEP-DS-VER
        //               ,:BEP-DS-TS-INI-CPTZ
        //               ,:BEP-DS-TS-END-CPTZ
        //               ,:BEP-DS-UTENTE
        //               ,:BEP-DS-STATO-ELAB
        //               ,:BEP-TP-NORMAL-BNFIC
        //                :IND-BEP-TP-NORMAL-BNFIC
        //           END-EXEC.
        bnficDao.fetchCIdUpdEffBep(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A370-CLOSE-CURSOR-ID-EFF THRU A370-EX
            a370CloseCursorIdEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A405-DECLARE-CURSOR-IDP-EFF<br>
	 * <pre>----
	 * ----  gestione IDP Effetto
	 * ----</pre>*/
    private void a405DeclareCursorIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IDP-EFF-BEP CURSOR FOR
        //              SELECT
        //                     ID_BNFIC
        //                    ,ID_RAPP_ANA
        //                    ,ID_BNFICR
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,COD_BNFIC
        //                    ,TP_IND_BNFICR
        //                    ,COD_BNFICR
        //                    ,DESC_BNFICR
        //                    ,PC_DEL_BNFICR
        //                    ,FL_ESE
        //                    ,FL_IRREV
        //                    ,FL_DFLT
        //                    ,ESRCN_ATTVT_IMPRS
        //                    ,FL_BNFICR_COLL
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,TP_NORMAL_BNFIC
        //              FROM BNFIC
        //              WHERE     ID_RAPP_ANA = :BEP-ID-RAPP-ANA
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //              ORDER BY ID_BNFIC ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A410-SELECT-IDP-EFF<br>*/
    private void a410SelectIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_BNFIC
        //                ,ID_RAPP_ANA
        //                ,ID_BNFICR
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,COD_BNFIC
        //                ,TP_IND_BNFICR
        //                ,COD_BNFICR
        //                ,DESC_BNFICR
        //                ,PC_DEL_BNFICR
        //                ,FL_ESE
        //                ,FL_IRREV
        //                ,FL_DFLT
        //                ,ESRCN_ATTVT_IMPRS
        //                ,FL_BNFICR_COLL
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,TP_NORMAL_BNFIC
        //             INTO
        //                :BEP-ID-BNFIC
        //               ,:BEP-ID-RAPP-ANA
        //               ,:BEP-ID-BNFICR
        //                :IND-BEP-ID-BNFICR
        //               ,:BEP-ID-MOVI-CRZ
        //               ,:BEP-ID-MOVI-CHIU
        //                :IND-BEP-ID-MOVI-CHIU
        //               ,:BEP-DT-INI-EFF-DB
        //               ,:BEP-DT-END-EFF-DB
        //               ,:BEP-COD-COMP-ANIA
        //               ,:BEP-COD-BNFIC
        //                :IND-BEP-COD-BNFIC
        //               ,:BEP-TP-IND-BNFICR
        //               ,:BEP-COD-BNFICR
        //                :IND-BEP-COD-BNFICR
        //               ,:BEP-DESC-BNFICR-VCHAR
        //                :IND-BEP-DESC-BNFICR
        //               ,:BEP-PC-DEL-BNFICR
        //                :IND-BEP-PC-DEL-BNFICR
        //               ,:BEP-FL-ESE
        //                :IND-BEP-FL-ESE
        //               ,:BEP-FL-IRREV
        //                :IND-BEP-FL-IRREV
        //               ,:BEP-FL-DFLT
        //                :IND-BEP-FL-DFLT
        //               ,:BEP-ESRCN-ATTVT-IMPRS
        //                :IND-BEP-ESRCN-ATTVT-IMPRS
        //               ,:BEP-FL-BNFICR-COLL
        //               ,:BEP-DS-RIGA
        //               ,:BEP-DS-OPER-SQL
        //               ,:BEP-DS-VER
        //               ,:BEP-DS-TS-INI-CPTZ
        //               ,:BEP-DS-TS-END-CPTZ
        //               ,:BEP-DS-UTENTE
        //               ,:BEP-DS-STATO-ELAB
        //               ,:BEP-TP-NORMAL-BNFIC
        //                :IND-BEP-TP-NORMAL-BNFIC
        //             FROM BNFIC
        //             WHERE     ID_RAPP_ANA = :BEP-ID-RAPP-ANA
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        bnficDao.selectRec1(bnfic.getBepIdRappAna(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A460-OPEN-CURSOR-IDP-EFF<br>*/
    private void a460OpenCursorIdpEff() {
        // COB_CODE: PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.
        a405DeclareCursorIdpEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-IDP-EFF-BEP
        //           END-EXEC.
        bnficDao.openCIdpEffBep(bnfic.getBepIdRappAna(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A470-CLOSE-CURSOR-IDP-EFF<br>*/
    private void a470CloseCursorIdpEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IDP-EFF-BEP
        //           END-EXEC.
        bnficDao.closeCIdpEffBep();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A480-FETCH-FIRST-IDP-EFF<br>*/
    private void a480FetchFirstIdpEff() {
        // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.
        a460OpenCursorIdpEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
            a490FetchNextIdpEff();
        }
    }

    /**Original name: A490-FETCH-NEXT-IDP-EFF<br>*/
    private void a490FetchNextIdpEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IDP-EFF-BEP
        //           INTO
        //                :BEP-ID-BNFIC
        //               ,:BEP-ID-RAPP-ANA
        //               ,:BEP-ID-BNFICR
        //                :IND-BEP-ID-BNFICR
        //               ,:BEP-ID-MOVI-CRZ
        //               ,:BEP-ID-MOVI-CHIU
        //                :IND-BEP-ID-MOVI-CHIU
        //               ,:BEP-DT-INI-EFF-DB
        //               ,:BEP-DT-END-EFF-DB
        //               ,:BEP-COD-COMP-ANIA
        //               ,:BEP-COD-BNFIC
        //                :IND-BEP-COD-BNFIC
        //               ,:BEP-TP-IND-BNFICR
        //               ,:BEP-COD-BNFICR
        //                :IND-BEP-COD-BNFICR
        //               ,:BEP-DESC-BNFICR-VCHAR
        //                :IND-BEP-DESC-BNFICR
        //               ,:BEP-PC-DEL-BNFICR
        //                :IND-BEP-PC-DEL-BNFICR
        //               ,:BEP-FL-ESE
        //                :IND-BEP-FL-ESE
        //               ,:BEP-FL-IRREV
        //                :IND-BEP-FL-IRREV
        //               ,:BEP-FL-DFLT
        //                :IND-BEP-FL-DFLT
        //               ,:BEP-ESRCN-ATTVT-IMPRS
        //                :IND-BEP-ESRCN-ATTVT-IMPRS
        //               ,:BEP-FL-BNFICR-COLL
        //               ,:BEP-DS-RIGA
        //               ,:BEP-DS-OPER-SQL
        //               ,:BEP-DS-VER
        //               ,:BEP-DS-TS-INI-CPTZ
        //               ,:BEP-DS-TS-END-CPTZ
        //               ,:BEP-DS-UTENTE
        //               ,:BEP-DS-STATO-ELAB
        //               ,:BEP-TP-NORMAL-BNFIC
        //                :IND-BEP-TP-NORMAL-BNFIC
        //           END-EXEC.
        bnficDao.fetchCIdpEffBep(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
            a470CloseCursorIdpEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A505-DECLARE-CURSOR-IBO<br>
	 * <pre>----
	 * ----  gestione IBO Effetto e non
	 * ----</pre>*/
    private void a505DeclareCursorIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A510-SELECT-IBO<br>*/
    private void a510SelectIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A560-OPEN-CURSOR-IBO<br>*/
    private void a560OpenCursorIbo() {
        // COB_CODE: PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.
        a505DeclareCursorIbo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A570-CLOSE-CURSOR-IBO<br>*/
    private void a570CloseCursorIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A580-FETCH-FIRST-IBO<br>*/
    private void a580FetchFirstIbo() {
        // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.
        a560OpenCursorIbo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
            a590FetchNextIbo();
        }
    }

    /**Original name: A590-FETCH-NEXT-IBO<br>*/
    private void a590FetchNextIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A605-DECLARE-CURSOR-IBS<br>
	 * <pre>----
	 * ----  gestione IBS Effetto e non
	 * ----</pre>*/
    private void a605DeclareCursorIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A610-SELECT-IBS<br>*/
    private void a610SelectIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A660-OPEN-CURSOR-IBS<br>*/
    private void a660OpenCursorIbs() {
        // COB_CODE: PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.
        a605DeclareCursorIbs();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A670-CLOSE-CURSOR-IBS<br>*/
    private void a670CloseCursorIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A680-FETCH-FIRST-IBS<br>*/
    private void a680FetchFirstIbs() {
        // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.
        a660OpenCursorIbs();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
            a690FetchNextIbs();
        }
    }

    /**Original name: A690-FETCH-NEXT-IBS<br>*/
    private void a690FetchNextIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A705-DECLARE-CURSOR-IDO<br>
	 * <pre>----
	 * ----  gestione IDO Effetto e non
	 * ----</pre>*/
    private void a705DeclareCursorIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A710-SELECT-IDO<br>*/
    private void a710SelectIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A760-OPEN-CURSOR-IDO<br>*/
    private void a760OpenCursorIdo() {
        // COB_CODE: PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.
        a705DeclareCursorIdo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A770-CLOSE-CURSOR-IDO<br>*/
    private void a770CloseCursorIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A780-FETCH-FIRST-IDO<br>*/
    private void a780FetchFirstIdo() {
        // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.
        a760OpenCursorIdo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
            a790FetchNextIdo();
        }
    }

    /**Original name: A790-FETCH-NEXT-IDO<br>*/
    private void a790FetchNextIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B310-SELECT-ID-CPZ<br>
	 * <pre>----
	 * ----  gestione ID Competenza
	 * ----</pre>*/
    private void b310SelectIdCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_BNFIC
        //                ,ID_RAPP_ANA
        //                ,ID_BNFICR
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,COD_BNFIC
        //                ,TP_IND_BNFICR
        //                ,COD_BNFICR
        //                ,DESC_BNFICR
        //                ,PC_DEL_BNFICR
        //                ,FL_ESE
        //                ,FL_IRREV
        //                ,FL_DFLT
        //                ,ESRCN_ATTVT_IMPRS
        //                ,FL_BNFICR_COLL
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,TP_NORMAL_BNFIC
        //             INTO
        //                :BEP-ID-BNFIC
        //               ,:BEP-ID-RAPP-ANA
        //               ,:BEP-ID-BNFICR
        //                :IND-BEP-ID-BNFICR
        //               ,:BEP-ID-MOVI-CRZ
        //               ,:BEP-ID-MOVI-CHIU
        //                :IND-BEP-ID-MOVI-CHIU
        //               ,:BEP-DT-INI-EFF-DB
        //               ,:BEP-DT-END-EFF-DB
        //               ,:BEP-COD-COMP-ANIA
        //               ,:BEP-COD-BNFIC
        //                :IND-BEP-COD-BNFIC
        //               ,:BEP-TP-IND-BNFICR
        //               ,:BEP-COD-BNFICR
        //                :IND-BEP-COD-BNFICR
        //               ,:BEP-DESC-BNFICR-VCHAR
        //                :IND-BEP-DESC-BNFICR
        //               ,:BEP-PC-DEL-BNFICR
        //                :IND-BEP-PC-DEL-BNFICR
        //               ,:BEP-FL-ESE
        //                :IND-BEP-FL-ESE
        //               ,:BEP-FL-IRREV
        //                :IND-BEP-FL-IRREV
        //               ,:BEP-FL-DFLT
        //                :IND-BEP-FL-DFLT
        //               ,:BEP-ESRCN-ATTVT-IMPRS
        //                :IND-BEP-ESRCN-ATTVT-IMPRS
        //               ,:BEP-FL-BNFICR-COLL
        //               ,:BEP-DS-RIGA
        //               ,:BEP-DS-OPER-SQL
        //               ,:BEP-DS-VER
        //               ,:BEP-DS-TS-INI-CPTZ
        //               ,:BEP-DS-TS-END-CPTZ
        //               ,:BEP-DS-UTENTE
        //               ,:BEP-DS-STATO-ELAB
        //               ,:BEP-TP-NORMAL-BNFIC
        //                :IND-BEP-TP-NORMAL-BNFIC
        //             FROM BNFIC
        //             WHERE     ID_BNFIC = :BEP-ID-BNFIC
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        bnficDao.selectRec2(bnfic.getBepIdBnfic(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B405-DECLARE-CURSOR-IDP-CPZ<br>
	 * <pre>----
	 * ----  gestione IDP Competenza
	 * ----</pre>*/
    private void b405DeclareCursorIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IDP-CPZ-BEP CURSOR FOR
        //              SELECT
        //                     ID_BNFIC
        //                    ,ID_RAPP_ANA
        //                    ,ID_BNFICR
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,COD_BNFIC
        //                    ,TP_IND_BNFICR
        //                    ,COD_BNFICR
        //                    ,DESC_BNFICR
        //                    ,PC_DEL_BNFICR
        //                    ,FL_ESE
        //                    ,FL_IRREV
        //                    ,FL_DFLT
        //                    ,ESRCN_ATTVT_IMPRS
        //                    ,FL_BNFICR_COLL
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,TP_NORMAL_BNFIC
        //              FROM BNFIC
        //              WHERE     ID_RAPP_ANA = :BEP-ID-RAPP-ANA
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //              ORDER BY COD_COMP_ANIA,
        //                       ID_RAPP_ANA,
        //                       ID_BNFIC ASC,
        //                       COD_BNFIC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B410-SELECT-IDP-CPZ<br>*/
    private void b410SelectIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_BNFIC
        //                ,ID_RAPP_ANA
        //                ,ID_BNFICR
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,COD_BNFIC
        //                ,TP_IND_BNFICR
        //                ,COD_BNFICR
        //                ,DESC_BNFICR
        //                ,PC_DEL_BNFICR
        //                ,FL_ESE
        //                ,FL_IRREV
        //                ,FL_DFLT
        //                ,ESRCN_ATTVT_IMPRS
        //                ,FL_BNFICR_COLL
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,TP_NORMAL_BNFIC
        //             INTO
        //                :BEP-ID-BNFIC
        //               ,:BEP-ID-RAPP-ANA
        //               ,:BEP-ID-BNFICR
        //                :IND-BEP-ID-BNFICR
        //               ,:BEP-ID-MOVI-CRZ
        //               ,:BEP-ID-MOVI-CHIU
        //                :IND-BEP-ID-MOVI-CHIU
        //               ,:BEP-DT-INI-EFF-DB
        //               ,:BEP-DT-END-EFF-DB
        //               ,:BEP-COD-COMP-ANIA
        //               ,:BEP-COD-BNFIC
        //                :IND-BEP-COD-BNFIC
        //               ,:BEP-TP-IND-BNFICR
        //               ,:BEP-COD-BNFICR
        //                :IND-BEP-COD-BNFICR
        //               ,:BEP-DESC-BNFICR-VCHAR
        //                :IND-BEP-DESC-BNFICR
        //               ,:BEP-PC-DEL-BNFICR
        //                :IND-BEP-PC-DEL-BNFICR
        //               ,:BEP-FL-ESE
        //                :IND-BEP-FL-ESE
        //               ,:BEP-FL-IRREV
        //                :IND-BEP-FL-IRREV
        //               ,:BEP-FL-DFLT
        //                :IND-BEP-FL-DFLT
        //               ,:BEP-ESRCN-ATTVT-IMPRS
        //                :IND-BEP-ESRCN-ATTVT-IMPRS
        //               ,:BEP-FL-BNFICR-COLL
        //               ,:BEP-DS-RIGA
        //               ,:BEP-DS-OPER-SQL
        //               ,:BEP-DS-VER
        //               ,:BEP-DS-TS-INI-CPTZ
        //               ,:BEP-DS-TS-END-CPTZ
        //               ,:BEP-DS-UTENTE
        //               ,:BEP-DS-STATO-ELAB
        //               ,:BEP-TP-NORMAL-BNFIC
        //                :IND-BEP-TP-NORMAL-BNFIC
        //             FROM BNFIC
        //             WHERE     ID_RAPP_ANA = :BEP-ID-RAPP-ANA
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        bnficDao.selectRec3(bnfic.getBepIdRappAna(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B460-OPEN-CURSOR-IDP-CPZ<br>*/
    private void b460OpenCursorIdpCpz() {
        // COB_CODE: PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.
        b405DeclareCursorIdpCpz();
        // COB_CODE: EXEC SQL
        //                OPEN C-IDP-CPZ-BEP
        //           END-EXEC.
        bnficDao.openCIdpCpzBep(bnfic.getBepIdRappAna(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B470-CLOSE-CURSOR-IDP-CPZ<br>*/
    private void b470CloseCursorIdpCpz() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IDP-CPZ-BEP
        //           END-EXEC.
        bnficDao.closeCIdpCpzBep();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B480-FETCH-FIRST-IDP-CPZ<br>*/
    private void b480FetchFirstIdpCpz() {
        // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.
        b460OpenCursorIdpCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
            b490FetchNextIdpCpz();
        }
    }

    /**Original name: B490-FETCH-NEXT-IDP-CPZ<br>*/
    private void b490FetchNextIdpCpz() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IDP-CPZ-BEP
        //           INTO
        //                :BEP-ID-BNFIC
        //               ,:BEP-ID-RAPP-ANA
        //               ,:BEP-ID-BNFICR
        //                :IND-BEP-ID-BNFICR
        //               ,:BEP-ID-MOVI-CRZ
        //               ,:BEP-ID-MOVI-CHIU
        //                :IND-BEP-ID-MOVI-CHIU
        //               ,:BEP-DT-INI-EFF-DB
        //               ,:BEP-DT-END-EFF-DB
        //               ,:BEP-COD-COMP-ANIA
        //               ,:BEP-COD-BNFIC
        //                :IND-BEP-COD-BNFIC
        //               ,:BEP-TP-IND-BNFICR
        //               ,:BEP-COD-BNFICR
        //                :IND-BEP-COD-BNFICR
        //               ,:BEP-DESC-BNFICR-VCHAR
        //                :IND-BEP-DESC-BNFICR
        //               ,:BEP-PC-DEL-BNFICR
        //                :IND-BEP-PC-DEL-BNFICR
        //               ,:BEP-FL-ESE
        //                :IND-BEP-FL-ESE
        //               ,:BEP-FL-IRREV
        //                :IND-BEP-FL-IRREV
        //               ,:BEP-FL-DFLT
        //                :IND-BEP-FL-DFLT
        //               ,:BEP-ESRCN-ATTVT-IMPRS
        //                :IND-BEP-ESRCN-ATTVT-IMPRS
        //               ,:BEP-FL-BNFICR-COLL
        //               ,:BEP-DS-RIGA
        //               ,:BEP-DS-OPER-SQL
        //               ,:BEP-DS-VER
        //               ,:BEP-DS-TS-INI-CPTZ
        //               ,:BEP-DS-TS-END-CPTZ
        //               ,:BEP-DS-UTENTE
        //               ,:BEP-DS-STATO-ELAB
        //               ,:BEP-TP-NORMAL-BNFIC
        //                :IND-BEP-TP-NORMAL-BNFIC
        //           END-EXEC.
        bnficDao.fetchCIdpCpzBep(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
            b470CloseCursorIdpCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B505-DECLARE-CURSOR-IBO-CPZ<br>
	 * <pre>----
	 * ----  gestione IBO Competenza
	 * ----</pre>*/
    private void b505DeclareCursorIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B510-SELECT-IBO-CPZ<br>*/
    private void b510SelectIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B560-OPEN-CURSOR-IBO-CPZ<br>*/
    private void b560OpenCursorIboCpz() {
        // COB_CODE: PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.
        b505DeclareCursorIboCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B570-CLOSE-CURSOR-IBO-CPZ<br>*/
    private void b570CloseCursorIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B580-FETCH-FIRST-IBO-CPZ<br>*/
    private void b580FetchFirstIboCpz() {
        // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.
        b560OpenCursorIboCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
    }

    /**Original name: B590-FETCH-NEXT-IBO-CPZ<br>*/
    private void b590FetchNextIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B605-DECLARE-CURSOR-IBS-CPZ<br>
	 * <pre>----
	 * ----  gestione IBS Competenza
	 * ----</pre>*/
    private void b605DeclareCursorIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B610-SELECT-IBS-CPZ<br>*/
    private void b610SelectIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B660-OPEN-CURSOR-IBS-CPZ<br>*/
    private void b660OpenCursorIbsCpz() {
        // COB_CODE: PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.
        b605DeclareCursorIbsCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B670-CLOSE-CURSOR-IBS-CPZ<br>*/
    private void b670CloseCursorIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B680-FETCH-FIRST-IBS-CPZ<br>*/
    private void b680FetchFirstIbsCpz() {
        // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.
        b660OpenCursorIbsCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
    }

    /**Original name: B690-FETCH-NEXT-IBS-CPZ<br>*/
    private void b690FetchNextIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B705-DECLARE-CURSOR-IDO-CPZ<br>
	 * <pre>----
	 * ----  gestione IDO Competenza
	 * ----</pre>*/
    private void b705DeclareCursorIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B710-SELECT-IDO-CPZ<br>*/
    private void b710SelectIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B760-OPEN-CURSOR-IDO-CPZ<br>*/
    private void b760OpenCursorIdoCpz() {
        // COB_CODE: PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.
        b705DeclareCursorIdoCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B770-CLOSE-CURSOR-IDO-CPZ<br>*/
    private void b770CloseCursorIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B780-FETCH-FIRST-IDO-CPZ<br>*/
    private void b780FetchFirstIdoCpz() {
        // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.
        b760OpenCursorIdoCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
    }

    /**Original name: B790-FETCH-NEXT-IDO-CPZ<br>*/
    private void b790FetchNextIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-BEP-ID-BNFICR = -1
        //              MOVE HIGH-VALUES TO BEP-ID-BNFICR-NULL
        //           END-IF
        if (ws.getIndBnfic().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BEP-ID-BNFICR-NULL
            bnfic.getBepIdBnficr().setBepIdBnficrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BepIdBnficr.Len.BEP_ID_BNFICR_NULL));
        }
        // COB_CODE: IF IND-BEP-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO BEP-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndBnfic().getCodParam() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BEP-ID-MOVI-CHIU-NULL
            bnfic.getBepIdMoviChiu().setBepIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BepIdMoviChiu.Len.BEP_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-BEP-COD-BNFIC = -1
        //              MOVE HIGH-VALUES TO BEP-COD-BNFIC-NULL
        //           END-IF
        if (ws.getIndBnfic().getTpParam() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BEP-COD-BNFIC-NULL
            bnfic.getBepCodBnfic().setBepCodBnficNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BepCodBnfic.Len.BEP_COD_BNFIC_NULL));
        }
        // COB_CODE: IF IND-BEP-COD-BNFICR = -1
        //              MOVE HIGH-VALUES TO BEP-COD-BNFICR-NULL
        //           END-IF
        if (ws.getIndBnfic().getTpD() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BEP-COD-BNFICR-NULL
            bnfic.setBepCodBnficr(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BnficIdbsbep0.Len.BEP_COD_BNFICR));
        }
        // COB_CODE: IF IND-BEP-DESC-BNFICR = -1
        //              MOVE HIGH-VALUES TO BEP-DESC-BNFICR
        //           END-IF
        if (ws.getIndBnfic().getValImp() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BEP-DESC-BNFICR
            bnfic.setBepDescBnficr(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BnficIdbsbep0.Len.BEP_DESC_BNFICR));
        }
        // COB_CODE: IF IND-BEP-PC-DEL-BNFICR = -1
        //              MOVE HIGH-VALUES TO BEP-PC-DEL-BNFICR-NULL
        //           END-IF
        if (ws.getIndBnfic().getValDt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BEP-PC-DEL-BNFICR-NULL
            bnfic.getBepPcDelBnficr().setBepPcDelBnficrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BepPcDelBnficr.Len.BEP_PC_DEL_BNFICR_NULL));
        }
        // COB_CODE: IF IND-BEP-FL-ESE = -1
        //              MOVE HIGH-VALUES TO BEP-FL-ESE-NULL
        //           END-IF
        if (ws.getIndBnfic().getValTs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BEP-FL-ESE-NULL
            bnfic.setBepFlEse(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-BEP-FL-IRREV = -1
        //              MOVE HIGH-VALUES TO BEP-FL-IRREV-NULL
        //           END-IF
        if (ws.getIndBnfic().getValTxt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BEP-FL-IRREV-NULL
            bnfic.setBepFlIrrev(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-BEP-FL-DFLT = -1
        //              MOVE HIGH-VALUES TO BEP-FL-DFLT-NULL
        //           END-IF
        if (ws.getIndBnfic().getValFl() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BEP-FL-DFLT-NULL
            bnfic.setBepFlDflt(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-BEP-ESRCN-ATTVT-IMPRS = -1
        //              MOVE HIGH-VALUES TO BEP-ESRCN-ATTVT-IMPRS-NULL
        //           END-IF
        if (ws.getIndBnfic().getValNum() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BEP-ESRCN-ATTVT-IMPRS-NULL
            bnfic.setBepEsrcnAttvtImprs(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-BEP-TP-NORMAL-BNFIC = -1
        //              MOVE HIGH-VALUES TO BEP-TP-NORMAL-BNFIC-NULL
        //           END-IF.
        if (ws.getIndBnfic().getValPc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BEP-TP-NORMAL-BNFIC-NULL
            bnfic.setBepTpNormalBnfic(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BnficIdbsbep0.Len.BEP_TP_NORMAL_BNFIC));
        }
    }

    /**Original name: Z150-VALORIZZA-DATA-SERVICES-I<br>*/
    private void z150ValorizzaDataServicesI() {
        // COB_CODE: MOVE 'I' TO BEP-DS-OPER-SQL
        bnfic.setBepDsOperSqlFormatted("I");
        // COB_CODE: MOVE 0                   TO BEP-DS-VER
        bnfic.setBepDsVer(0);
        // COB_CODE: MOVE IDSV0003-USER-NAME TO BEP-DS-UTENTE
        bnfic.setBepDsUtente(idsv0003.getUserName());
        // COB_CODE: MOVE '1'                   TO BEP-DS-STATO-ELAB.
        bnfic.setBepDsStatoElabFormatted("1");
    }

    /**Original name: Z160-VALORIZZA-DATA-SERVICES-U<br>*/
    private void z160ValorizzaDataServicesU() {
        // COB_CODE: MOVE 'U' TO BEP-DS-OPER-SQL
        bnfic.setBepDsOperSqlFormatted("U");
        // COB_CODE: MOVE IDSV0003-USER-NAME TO BEP-DS-UTENTE.
        bnfic.setBepDsUtente(idsv0003.getUserName());
    }

    /**Original name: Z200-SET-INDICATORI-NULL<br>*/
    private void z200SetIndicatoriNull() {
        // COB_CODE: IF BEP-ID-BNFICR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BEP-ID-BNFICR
        //           ELSE
        //              MOVE 0 TO IND-BEP-ID-BNFICR
        //           END-IF
        if (Characters.EQ_HIGH.test(bnfic.getBepIdBnficr().getBepIdBnficrNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BEP-ID-BNFICR
            ws.getIndBnfic().setIdMoviChiu(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BEP-ID-BNFICR
            ws.getIndBnfic().setIdMoviChiu(((short)0));
        }
        // COB_CODE: IF BEP-ID-MOVI-CHIU-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BEP-ID-MOVI-CHIU
        //           ELSE
        //              MOVE 0 TO IND-BEP-ID-MOVI-CHIU
        //           END-IF
        if (Characters.EQ_HIGH.test(bnfic.getBepIdMoviChiu().getBepIdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BEP-ID-MOVI-CHIU
            ws.getIndBnfic().setCodParam(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BEP-ID-MOVI-CHIU
            ws.getIndBnfic().setCodParam(((short)0));
        }
        // COB_CODE: IF BEP-COD-BNFIC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BEP-COD-BNFIC
        //           ELSE
        //              MOVE 0 TO IND-BEP-COD-BNFIC
        //           END-IF
        if (Characters.EQ_HIGH.test(bnfic.getBepCodBnfic().getBepCodBnficNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BEP-COD-BNFIC
            ws.getIndBnfic().setTpParam(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BEP-COD-BNFIC
            ws.getIndBnfic().setTpParam(((short)0));
        }
        // COB_CODE: IF BEP-COD-BNFICR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BEP-COD-BNFICR
        //           ELSE
        //              MOVE 0 TO IND-BEP-COD-BNFICR
        //           END-IF
        if (Characters.EQ_HIGH.test(bnfic.getBepCodBnficrFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BEP-COD-BNFICR
            ws.getIndBnfic().setTpD(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BEP-COD-BNFICR
            ws.getIndBnfic().setTpD(((short)0));
        }
        // COB_CODE: IF BEP-DESC-BNFICR = HIGH-VALUES
        //              MOVE -1 TO IND-BEP-DESC-BNFICR
        //           ELSE
        //              MOVE 0 TO IND-BEP-DESC-BNFICR
        //           END-IF
        if (Characters.EQ_HIGH.test(bnfic.getBepDescBnficr(), BnficIdbsbep0.Len.BEP_DESC_BNFICR)) {
            // COB_CODE: MOVE -1 TO IND-BEP-DESC-BNFICR
            ws.getIndBnfic().setValImp(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BEP-DESC-BNFICR
            ws.getIndBnfic().setValImp(((short)0));
        }
        // COB_CODE: IF BEP-PC-DEL-BNFICR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BEP-PC-DEL-BNFICR
        //           ELSE
        //              MOVE 0 TO IND-BEP-PC-DEL-BNFICR
        //           END-IF
        if (Characters.EQ_HIGH.test(bnfic.getBepPcDelBnficr().getBepPcDelBnficrNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BEP-PC-DEL-BNFICR
            ws.getIndBnfic().setValDt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BEP-PC-DEL-BNFICR
            ws.getIndBnfic().setValDt(((short)0));
        }
        // COB_CODE: IF BEP-FL-ESE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BEP-FL-ESE
        //           ELSE
        //              MOVE 0 TO IND-BEP-FL-ESE
        //           END-IF
        if (Conditions.eq(bnfic.getBepFlEse(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-BEP-FL-ESE
            ws.getIndBnfic().setValTs(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BEP-FL-ESE
            ws.getIndBnfic().setValTs(((short)0));
        }
        // COB_CODE: IF BEP-FL-IRREV-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BEP-FL-IRREV
        //           ELSE
        //              MOVE 0 TO IND-BEP-FL-IRREV
        //           END-IF
        if (Conditions.eq(bnfic.getBepFlIrrev(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-BEP-FL-IRREV
            ws.getIndBnfic().setValTxt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BEP-FL-IRREV
            ws.getIndBnfic().setValTxt(((short)0));
        }
        // COB_CODE: IF BEP-FL-DFLT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BEP-FL-DFLT
        //           ELSE
        //              MOVE 0 TO IND-BEP-FL-DFLT
        //           END-IF
        if (Conditions.eq(bnfic.getBepFlDflt(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-BEP-FL-DFLT
            ws.getIndBnfic().setValFl(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BEP-FL-DFLT
            ws.getIndBnfic().setValFl(((short)0));
        }
        // COB_CODE: IF BEP-ESRCN-ATTVT-IMPRS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BEP-ESRCN-ATTVT-IMPRS
        //           ELSE
        //              MOVE 0 TO IND-BEP-ESRCN-ATTVT-IMPRS
        //           END-IF
        if (Conditions.eq(bnfic.getBepEsrcnAttvtImprs(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-BEP-ESRCN-ATTVT-IMPRS
            ws.getIndBnfic().setValNum(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BEP-ESRCN-ATTVT-IMPRS
            ws.getIndBnfic().setValNum(((short)0));
        }
        // COB_CODE: IF BEP-TP-NORMAL-BNFIC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BEP-TP-NORMAL-BNFIC
        //           ELSE
        //              MOVE 0 TO IND-BEP-TP-NORMAL-BNFIC
        //           END-IF.
        if (Characters.EQ_HIGH.test(bnfic.getBepTpNormalBnficFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BEP-TP-NORMAL-BNFIC
            ws.getIndBnfic().setValPc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BEP-TP-NORMAL-BNFIC
            ws.getIndBnfic().setValPc(((short)0));
        }
    }

    /**Original name: Z400-SEQ-RIGA<br>*/
    private void z400SeqRiga() {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_RIGA
        //              INTO : BEP-DS-RIGA
        //           END-EXEC.
        //TODO: Implement: valuesStmt;
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: Z500-AGGIORNAMENTO-STORICO<br>*/
    private void z500AggiornamentoStorico() {
        // COB_CODE: MOVE BNFIC TO WS-BUFFER-TABLE.
        ws.setWsBufferTable(bnfic.getBnficFormatted());
        // COB_CODE: MOVE BEP-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.
        ws.setWsIdMoviCrz(TruncAbs.toInt(bnfic.getBepIdMoviCrz(), 9));
        // COB_CODE: PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.
        a360OpenCursorIdEff();
        // COB_CODE: PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-SQL
        //              END-IF
        //           END-PERFORM.
        while (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX
            a390FetchNextIdEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              END-IF
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: MOVE WS-ID-MOVI-CRZ   TO BEP-ID-MOVI-CHIU
                bnfic.getBepIdMoviChiu().setBepIdMoviChiu(ws.getWsIdMoviCrz());
                // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
                //                                 TO BEP-DS-TS-END-CPTZ
                bnfic.setBepDsTsEndCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
                // COB_CODE: PERFORM A330-UPDATE-ID-EFF THRU A330-EX
                a330UpdateIdEff();
                // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
                //                 END-IF
                //           END-IF
                if (idsv0003.getSqlcode().isSuccessfulSql()) {
                    // COB_CODE: MOVE WS-ID-MOVI-CRZ TO BEP-ID-MOVI-CRZ
                    bnfic.setBepIdMoviCrz(ws.getWsIdMoviCrz());
                    // COB_CODE: MOVE HIGH-VALUES    TO BEP-ID-MOVI-CHIU-NULL
                    bnfic.getBepIdMoviChiu().setBepIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BepIdMoviChiu.Len.BEP_ID_MOVI_CHIU_NULL));
                    // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
                    //                               TO BEP-DT-END-EFF
                    bnfic.setBepDtEndEff(idsv0003.getDataInizioEffetto());
                    // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
                    //                               TO BEP-DS-TS-INI-CPTZ
                    bnfic.setBepDsTsIniCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
                    // COB_CODE: MOVE WS-TS-INFINITO
                    //                               TO BEP-DS-TS-END-CPTZ
                    bnfic.setBepDsTsEndCptz(ws.getIdsv0010().getWsTsInfinito());
                    // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
                    //              PERFORM A220-INSERT-PK THRU A220-EX
                    //           END-IF
                    if (idsv0003.getSqlcode().isSuccessfulSql()) {
                        // COB_CODE: PERFORM A220-INSERT-PK THRU A220-EX
                        a220InsertPk();
                    }
                }
            }
        }
        // COB_CODE: IF IDSV0003-NOT-FOUND
        //              END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF NOT IDSV0003-DELETE-LOGICA
            //              PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX
            //           ELSE
            //              SET IDSV0003-SUCCESSFUL-SQL TO TRUE
            //           END-IF
            if (!idsv0003.getOperazione().isDeleteLogica()) {
                // COB_CODE: PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX
                z600InsertNuovaRigaStorica();
            }
            else {
                // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL TO TRUE
                idsv0003.getSqlcode().setSuccessfulSql();
            }
        }
    }

    /**Original name: Z600-INSERT-NUOVA-RIGA-STORICA<br>*/
    private void z600InsertNuovaRigaStorica() {
        // COB_CODE: MOVE WS-BUFFER-TABLE TO BNFIC.
        bnfic.setBnficFormatted(ws.getWsBufferTableFormatted());
        // COB_CODE: MOVE WS-ID-MOVI-CRZ  TO BEP-ID-MOVI-CRZ.
        bnfic.setBepIdMoviCrz(ws.getWsIdMoviCrz());
        // COB_CODE: MOVE HIGH-VALUES     TO BEP-ID-MOVI-CHIU-NULL.
        bnfic.getBepIdMoviChiu().setBepIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BepIdMoviChiu.Len.BEP_ID_MOVI_CHIU_NULL));
        // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
        //                                TO BEP-DT-INI-EFF.
        bnfic.setBepDtIniEff(idsv0003.getDataInizioEffetto());
        // COB_CODE: MOVE WS-DT-INFINITO
        //                                TO BEP-DT-END-EFF.
        bnfic.setBepDtEndEff(ws.getIdsv0010().getWsDtInfinito());
        // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
        //                                TO BEP-DS-TS-INI-CPTZ.
        bnfic.setBepDsTsIniCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
        // COB_CODE: MOVE WS-TS-INFINITO
        //                                TO BEP-DS-TS-END-CPTZ.
        bnfic.setBepDsTsEndCptz(ws.getIdsv0010().getWsTsInfinito());
        // COB_CODE: MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
        //                                TO BEP-COD-COMP-ANIA.
        bnfic.setBepCodCompAnia(idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A220-INSERT-PK THRU A220-EX.
        a220InsertPk();
    }

    /**Original name: Z900-CONVERTI-N-TO-X<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da 9(8) comp-3 a date
	 * ----</pre>*/
    private void z900ConvertiNToX() {
        // COB_CODE: MOVE BEP-DT-INI-EFF TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(bnfic.getBepDtIniEff(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO BEP-DT-INI-EFF-DB
        ws.getIdbvbep3().setBepDtIniEffDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: MOVE BEP-DT-END-EFF TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(bnfic.getBepDtEndEff(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO BEP-DT-END-EFF-DB.
        ws.getIdbvbep3().setBepDtEndEffDb(ws.getIdsv0010().getWsDateX());
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE BEP-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getIdbvbep3().getBepDtIniEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO BEP-DT-INI-EFF
        bnfic.setBepDtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE BEP-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getIdbvbep3().getBepDtEndEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO BEP-DT-END-EFF.
        bnfic.setBepDtEndEff(ws.getIdsv0010().getWsDateN());
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
        // COB_CODE: MOVE LENGTH OF BEP-DESC-BNFICR
        //                       TO BEP-DESC-BNFICR-LEN.
        bnfic.setBepDescBnficrLen(((short)BnficIdbsbep0.Len.BEP_DESC_BNFICR));
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public long getBepDsRiga() {
        return bnfic.getBepDsRiga();
    }

    @Override
    public void setBepDsRiga(long bepDsRiga) {
        this.bnfic.setBepDsRiga(bepDsRiga);
    }

    @Override
    public short getCodBnfic() {
        return bnfic.getBepCodBnfic().getBepCodBnfic();
    }

    @Override
    public void setCodBnfic(short codBnfic) {
        this.bnfic.getBepCodBnfic().setBepCodBnfic(codBnfic);
    }

    @Override
    public Short getCodBnficObj() {
        if (ws.getIndBnfic().getTpParam() >= 0) {
            return ((Short)getCodBnfic());
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodBnficObj(Short codBnficObj) {
        if (codBnficObj != null) {
            setCodBnfic(((short)codBnficObj));
            ws.getIndBnfic().setTpParam(((short)0));
        }
        else {
            ws.getIndBnfic().setTpParam(((short)-1));
        }
    }

    @Override
    public String getCodBnficr() {
        return bnfic.getBepCodBnficr();
    }

    @Override
    public void setCodBnficr(String codBnficr) {
        this.bnfic.setBepCodBnficr(codBnficr);
    }

    @Override
    public String getCodBnficrObj() {
        if (ws.getIndBnfic().getTpD() >= 0) {
            return getCodBnficr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodBnficrObj(String codBnficrObj) {
        if (codBnficrObj != null) {
            setCodBnficr(codBnficrObj);
            ws.getIndBnfic().setTpD(((short)0));
        }
        else {
            ws.getIndBnfic().setTpD(((short)-1));
        }
    }

    @Override
    public int getCodCompAnia() {
        return bnfic.getBepCodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.bnfic.setBepCodCompAnia(codCompAnia);
    }

    @Override
    public String getDescBnficrVchar() {
        return bnfic.getBepDescBnficrVcharFormatted();
    }

    @Override
    public void setDescBnficrVchar(String descBnficrVchar) {
        this.bnfic.setBepDescBnficrVcharFormatted(descBnficrVchar);
    }

    @Override
    public String getDescBnficrVcharObj() {
        if (ws.getIndBnfic().getValImp() >= 0) {
            return getDescBnficrVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDescBnficrVcharObj(String descBnficrVcharObj) {
        if (descBnficrVcharObj != null) {
            setDescBnficrVchar(descBnficrVcharObj);
            ws.getIndBnfic().setValImp(((short)0));
        }
        else {
            ws.getIndBnfic().setValImp(((short)-1));
        }
    }

    @Override
    public char getDsOperSql() {
        return bnfic.getBepDsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.bnfic.setBepDsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return bnfic.getBepDsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.bnfic.setBepDsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsEndCptz() {
        return bnfic.getBepDsTsEndCptz();
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.bnfic.setBepDsTsEndCptz(dsTsEndCptz);
    }

    @Override
    public long getDsTsIniCptz() {
        return bnfic.getBepDsTsIniCptz();
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.bnfic.setBepDsTsIniCptz(dsTsIniCptz);
    }

    @Override
    public String getDsUtente() {
        return bnfic.getBepDsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.bnfic.setBepDsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return bnfic.getBepDsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.bnfic.setBepDsVer(dsVer);
    }

    @Override
    public String getDtEndEffDb() {
        return ws.getIdbvbep3().getBepDtEndEffDb();
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        this.ws.getIdbvbep3().setBepDtEndEffDb(dtEndEffDb);
    }

    @Override
    public String getDtIniEffDb() {
        return ws.getIdbvbep3().getBepDtIniEffDb();
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        this.ws.getIdbvbep3().setBepDtIniEffDb(dtIniEffDb);
    }

    @Override
    public char getEsrcnAttvtImprs() {
        return bnfic.getBepEsrcnAttvtImprs();
    }

    @Override
    public void setEsrcnAttvtImprs(char esrcnAttvtImprs) {
        this.bnfic.setBepEsrcnAttvtImprs(esrcnAttvtImprs);
    }

    @Override
    public Character getEsrcnAttvtImprsObj() {
        if (ws.getIndBnfic().getValNum() >= 0) {
            return ((Character)getEsrcnAttvtImprs());
        }
        else {
            return null;
        }
    }

    @Override
    public void setEsrcnAttvtImprsObj(Character esrcnAttvtImprsObj) {
        if (esrcnAttvtImprsObj != null) {
            setEsrcnAttvtImprs(((char)esrcnAttvtImprsObj));
            ws.getIndBnfic().setValNum(((short)0));
        }
        else {
            ws.getIndBnfic().setValNum(((short)-1));
        }
    }

    @Override
    public char getFlBnficrColl() {
        return bnfic.getBepFlBnficrColl();
    }

    @Override
    public void setFlBnficrColl(char flBnficrColl) {
        this.bnfic.setBepFlBnficrColl(flBnficrColl);
    }

    @Override
    public char getFlDflt() {
        return bnfic.getBepFlDflt();
    }

    @Override
    public void setFlDflt(char flDflt) {
        this.bnfic.setBepFlDflt(flDflt);
    }

    @Override
    public Character getFlDfltObj() {
        if (ws.getIndBnfic().getValFl() >= 0) {
            return ((Character)getFlDflt());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlDfltObj(Character flDfltObj) {
        if (flDfltObj != null) {
            setFlDflt(((char)flDfltObj));
            ws.getIndBnfic().setValFl(((short)0));
        }
        else {
            ws.getIndBnfic().setValFl(((short)-1));
        }
    }

    @Override
    public char getFlEse() {
        return bnfic.getBepFlEse();
    }

    @Override
    public void setFlEse(char flEse) {
        this.bnfic.setBepFlEse(flEse);
    }

    @Override
    public Character getFlEseObj() {
        if (ws.getIndBnfic().getValTs() >= 0) {
            return ((Character)getFlEse());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlEseObj(Character flEseObj) {
        if (flEseObj != null) {
            setFlEse(((char)flEseObj));
            ws.getIndBnfic().setValTs(((short)0));
        }
        else {
            ws.getIndBnfic().setValTs(((short)-1));
        }
    }

    @Override
    public char getFlIrrev() {
        return bnfic.getBepFlIrrev();
    }

    @Override
    public void setFlIrrev(char flIrrev) {
        this.bnfic.setBepFlIrrev(flIrrev);
    }

    @Override
    public Character getFlIrrevObj() {
        if (ws.getIndBnfic().getValTxt() >= 0) {
            return ((Character)getFlIrrev());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlIrrevObj(Character flIrrevObj) {
        if (flIrrevObj != null) {
            setFlIrrev(((char)flIrrevObj));
            ws.getIndBnfic().setValTxt(((short)0));
        }
        else {
            ws.getIndBnfic().setValTxt(((short)-1));
        }
    }

    @Override
    public int getIdBnfic() {
        return bnfic.getBepIdBnfic();
    }

    @Override
    public void setIdBnfic(int idBnfic) {
        this.bnfic.setBepIdBnfic(idBnfic);
    }

    @Override
    public int getIdBnficr() {
        return bnfic.getBepIdBnficr().getBepIdBnficr();
    }

    @Override
    public void setIdBnficr(int idBnficr) {
        this.bnfic.getBepIdBnficr().setBepIdBnficr(idBnficr);
    }

    @Override
    public Integer getIdBnficrObj() {
        if (ws.getIndBnfic().getIdMoviChiu() >= 0) {
            return ((Integer)getIdBnficr());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdBnficrObj(Integer idBnficrObj) {
        if (idBnficrObj != null) {
            setIdBnficr(((int)idBnficrObj));
            ws.getIndBnfic().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndBnfic().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getIdMoviChiu() {
        return bnfic.getBepIdMoviChiu().getBepIdMoviChiu();
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        this.bnfic.getBepIdMoviChiu().setBepIdMoviChiu(idMoviChiu);
    }

    @Override
    public Integer getIdMoviChiuObj() {
        if (ws.getIndBnfic().getCodParam() >= 0) {
            return ((Integer)getIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        if (idMoviChiuObj != null) {
            setIdMoviChiu(((int)idMoviChiuObj));
            ws.getIndBnfic().setCodParam(((short)0));
        }
        else {
            ws.getIndBnfic().setCodParam(((short)-1));
        }
    }

    @Override
    public int getIdMoviCrz() {
        return bnfic.getBepIdMoviCrz();
    }

    @Override
    public void setIdMoviCrz(int idMoviCrz) {
        this.bnfic.setBepIdMoviCrz(idMoviCrz);
    }

    @Override
    public int getIdRappAna() {
        return bnfic.getBepIdRappAna();
    }

    @Override
    public void setIdRappAna(int idRappAna) {
        this.bnfic.setBepIdRappAna(idRappAna);
    }

    @Override
    public AfDecimal getPcDelBnficr() {
        return bnfic.getBepPcDelBnficr().getBepPcDelBnficr();
    }

    @Override
    public void setPcDelBnficr(AfDecimal pcDelBnficr) {
        this.bnfic.getBepPcDelBnficr().setBepPcDelBnficr(pcDelBnficr.copy());
    }

    @Override
    public AfDecimal getPcDelBnficrObj() {
        if (ws.getIndBnfic().getValDt() >= 0) {
            return getPcDelBnficr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcDelBnficrObj(AfDecimal pcDelBnficrObj) {
        if (pcDelBnficrObj != null) {
            setPcDelBnficr(new AfDecimal(pcDelBnficrObj, 6, 3));
            ws.getIndBnfic().setValDt(((short)0));
        }
        else {
            ws.getIndBnfic().setValDt(((short)-1));
        }
    }

    @Override
    public String getTpIndBnficr() {
        return bnfic.getBepTpIndBnficr();
    }

    @Override
    public void setTpIndBnficr(String tpIndBnficr) {
        this.bnfic.setBepTpIndBnficr(tpIndBnficr);
    }

    @Override
    public String getTpNormalBnfic() {
        return bnfic.getBepTpNormalBnfic();
    }

    @Override
    public void setTpNormalBnfic(String tpNormalBnfic) {
        this.bnfic.setBepTpNormalBnfic(tpNormalBnfic);
    }

    @Override
    public String getTpNormalBnficObj() {
        if (ws.getIndBnfic().getValPc() >= 0) {
            return getTpNormalBnfic();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpNormalBnficObj(String tpNormalBnficObj) {
        if (tpNormalBnficObj != null) {
            setTpNormalBnfic(tpNormalBnficObj);
            ws.getIndBnfic().setValPc(((short)0));
        }
        else {
            ws.getIndBnfic().setValPc(((short)-1));
        }
    }
}
