package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.jdbc.SqlFunctions;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.BtcParallelismDao;
import it.accenture.jnais.commons.data.to.IBtcParallelism;
import it.accenture.jnais.copy.Idsv0003CampiEsito;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.BtcParallelism;
import it.accenture.jnais.ws.enums.Idsv0003Sqlcode;
import it.accenture.jnais.ws.enums.WsFlagFase;
import it.accenture.jnais.ws.Iabs0130Data;
import it.accenture.jnais.ws.Iabv0002;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.redefines.BpaDtEnd;
import it.accenture.jnais.ws.redefines.BpaDtStart;
import it.accenture.jnais.ws.redefines.BpaIdOggA;
import it.accenture.jnais.ws.redefines.BpaIdOggDa;
import it.accenture.jnais.ws.redefines.BpaNumRowSchedule;

/**Original name: IABS0130<br>
 * <pre>AUTHOR.        ATS NAPOLI.
 * DATE-WRITTEN.  APRILE 2008.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO PER GESTIONE PARALLELISMO            *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Iabs0130 extends Program implements IBtcParallelism {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private BtcParallelismDao btcParallelismDao = new BtcParallelismDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Iabs0130Data ws = new Iabs0130Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: IABV0002
    private Iabv0002 iabv0002;
    //Original name: BTC-PARALLELISM
    private BtcParallelism btcParallelism;

    //==== METHODS ====
    /**Original name: PROGRAM_IABS0130_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, Iabv0002 iabv0002, BtcParallelism btcParallelism) {
        this.idsv0003 = idsv0003;
        this.iabv0002 = iabv0002;
        this.btcParallelism = btcParallelism;
        // COB_CODE: PERFORM A000-INIZIO                    THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A200-ELABORA                THRU A200-EX
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A200-ELABORA                THRU A200-EX
            a200Elabora();
        }
        // COB_CODE: MOVE WS-BUFFER-WHERE-COND-IABS0130
        //                         TO IDSV0003-BUFFER-WHERE-COND.
        this.idsv0003.setBufferWhereCond(ws.getWsBufferWhereCondIabs0130().getWsBufferWhereCondIabs0130Formatted());
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Iabs0130 getInstance() {
        return ((Iabs0130)Programs.getInstance(Iabs0130.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'IABS0130'   TO IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("IABS0130");
        // COB_CODE: MOVE 'BTC_PARALLELISM' TO IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("BTC_PARALLELISM");
        // COB_CODE: MOVE '00'                     TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                   TO   IDSV0003-SQLCODE
        //                                              IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                              IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: MOVE BPA-PROG-PROTOCOL        TO   WK-PROG-PROTOCOL.
        ws.setWkProgProtocol(btcParallelism.getBpaProgProtocol());
        // COB_CODE: MOVE IDSV0003-BUFFER-WHERE-COND
        //                         TO WS-BUFFER-WHERE-COND-IABS0130.
        ws.getWsBufferWhereCondIabs0130().setWsBufferWhereCondIabs0130Formatted(idsv0003.getBufferWhereCondFormatted());
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a200Elabora() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN WS-FASE-CONTROLLO-INIZIALE
        //                 PERFORM A375-CNTL                 THRU A375-EX
        //              WHEN WS-FASE-INIZIALE
        //                 END-IF
        //              WHEN WS-FASE-CONTROLLO-FINALE
        //                 PERFORM A380-CNTL                 THRU A380-EX
        //              WHEN WS-FASE-FINALE
        //                 PERFORM A230-UPDATE               THRU A230-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER         TO TRUE
        //           END-EVALUATE.
        switch (ws.getWsBufferWhereCondIabs0130().getWsFlagFase().getWsFlagFase()) {

            case WsFlagFase.CONTROLLO_INIZIALE:// COB_CODE: PERFORM A375-CNTL                 THRU A375-EX
                a375Cntl();
                break;

            case WsFlagFase.INIZIALE:// COB_CODE: PERFORM A212-PREPARA-UPD-INIZIALE THRU A212-EX
                a212PreparaUpdIniziale();
                // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
                //              PERFORM A230-UPDATE            THRU A230-EX
                //           END-IF
                if (idsv0003.getReturnCode().isSuccessfulRc()) {
                    // COB_CODE: PERFORM A230-UPDATE            THRU A230-EX
                    a230Update();
                }
                break;

            case WsFlagFase.CONTROLLO_FINALE:// COB_CODE: PERFORM A380-CNTL                 THRU A380-EX
                a380Cntl();
                break;

            case WsFlagFase.FINALE:// COB_CODE: PERFORM A213-PREPARA-UPD-FINALE   THRU A213-EX
                a213PreparaUpdFinale();
                // COB_CODE: PERFORM A230-UPDATE               THRU A230-EX
                a230Update();
                break;

            default:// COB_CODE: SET IDSV0003-INVALID-OPER         TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
                break;
        }
    }

    /**Original name: A210-SELECT-PK<br>
	 * <pre>----
	 * ----  gestione
	 * ----</pre>*/
    private void a210SelectPk() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                COD_COMP_ANIA
        //                ,PROTOCOL
        //                ,PROG_PROTOCOL
        //                ,COD_BATCH_STATE
        //                ,DT_INS
        //                ,USER_INS
        //                ,DT_START
        //                ,DT_END
        //                ,USER_START
        //                ,DESC_PARALLELISM
        //                ,ID_OGG_DA
        //                ,ID_OGG_A
        //                ,TP_OGG
        //                ,NUM_ROW_SCHEDULE
        //             INTO
        //                :BPA-COD-COMP-ANIA
        //               ,:BPA-PROTOCOL
        //               ,:BPA-PROG-PROTOCOL
        //               ,:BPA-COD-BATCH-STATE
        //               ,:BPA-DT-INS-DB
        //               ,:BPA-USER-INS
        //               ,:BPA-DT-START-DB
        //                :IND-BPA-DT-START
        //               ,:BPA-DT-END-DB
        //                :IND-BPA-DT-END
        //               ,:BPA-USER-START
        //                :IND-BPA-USER-START
        //               ,:BPA-DESC-PARALLELISM-VCHAR
        //                :IND-BPA-DESC-PARALLELISM
        //               ,:BPA-ID-OGG-DA
        //                :IND-BPA-ID-OGG-DA
        //               ,:BPA-ID-OGG-A
        //                :IND-BPA-ID-OGG-A
        //               ,:BPA-TP-OGG
        //                :IND-BPA-TP-OGG
        //               ,:BPA-NUM-ROW-SCHEDULE
        //                :IND-BPA-NUM-ROW-SCHEDULE
        //             FROM BTC_PARALLELISM
        //             WHERE     COD_COMP_ANIA = :BPA-COD-COMP-ANIA
        //                   AND PROTOCOL      = :BPA-PROTOCOL
        //                   AND PROG_PROTOCOL = :BPA-PROG-PROTOCOL
        //                   AND COD_BATCH_STATE IN (
        //                                          :IABV0002-STATE-01,
        //                                          :IABV0002-STATE-02,
        //                                          :IABV0002-STATE-03,
        //                                          :IABV0002-STATE-04,
        //                                          :IABV0002-STATE-05,
        //                                          :IABV0002-STATE-06,
        //                                          :IABV0002-STATE-07,
        //                                          :IABV0002-STATE-08,
        //                                          :IABV0002-STATE-09,
        //                                          :IABV0002-STATE-10
        //                                          )
        //           END-EXEC.
        btcParallelismDao.selectRec(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A212-PREPARA-UPD-INIZIALE<br>*/
    private void a212PreparaUpdIniziale() {
        ConcatUtil concatUtil = null;
        // COB_CODE: PERFORM A210-SELECT-PK   THRU A210-EX.
        a210SelectPk();
        // COB_CODE: IF NOT IDSV0003-SUCCESSFUL-SQL
        //              END-STRING
        //           ELSE
        //              END-IF
        //           END-IF.
        if (!idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: MOVE SPACES            TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("");
            // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
            idsv0003.getReturnCode().setSqlError();
            // COB_CODE: MOVE BPA-COD-COMP-ANIA TO COMODO-COD-COMP-ANIA
            ws.setComodoCodCompAnia(TruncAbs.toInt(btcParallelism.getBpaCodCompAnia(), 9));
            // COB_CODE: MOVE BPA-PROG-PROTOCOL TO COMODO-PROG-PROTOCOL
            ws.setComodoProgProtocol(TruncAbs.toInt(btcParallelism.getBpaProgProtocol(), 9));
            // COB_CODE: STRING 'JOB NON TROVATO SU BTC_PARALLELISM : '
            //                  DELIMITED BY SIZE
            //                  COMODO-COD-COMP-ANIA
            //                  ' / '
            //                  DELIMITED BY SIZE
            //                  BPA-PROTOCOL
            //                  DELIMITED BY SPACES
            //                  ' / '
            //                  DELIMITED BY SIZE
            //                  COMODO-PROG-PROTOCOL
            //                  DELIMITED BY SPACES
            //                  INTO
            //                  IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, new String[] {"JOB NON TROVATO SU BTC_PARALLELISM : ", ws.getComodoCodCompAniaAsString(), " / ", Functions.substringBefore(btcParallelism.getBpaProtocolFormatted(), Types.SPACE_STRING), " / ", Functions.substringBefore(ws.getComodoProgProtocolAsString(), Types.SPACE_STRING)});
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
        }
        else {
            // COB_CODE: MOVE BATCH-IN-ESECUZIONE TO BPA-COD-BATCH-STATE
            btcParallelism.setBpaCodBatchState(ws.getIabv0004().getBatchInEsecuzione());
            // COB_CODE: MOVE IDSV0003-USER-NAME  TO BPA-USER-START
            btcParallelism.setBpaUserStart(idsv0003.getUserName());
            // COB_CODE: PERFORM ESTRAI-CURRENT-TIMESTAMP
            //                   THRU ESTRAI-CURRENT-TIMESTAMP-EX
            estraiCurrentTimestamp();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
            //              MOVE WS-TIMESTAMP-N    TO BPA-DT-START
            //           END-IF
            if (idsv0003.getReturnCode().isSuccessfulRc()) {
                // COB_CODE: MOVE WS-TIMESTAMP-X    TO BPA-DT-START-DB
                ws.getBtcParallelismDb().setStartDb(ws.getIdsv0014().getTimestampX());
                // COB_CODE: MOVE WS-TIMESTAMP-N    TO BPA-DT-START
                btcParallelism.getBpaDtStart().setBpaDtStart(ws.getIdsv0014().getTimestampN());
            }
        }
    }

    /**Original name: A213-PREPARA-UPD-FINALE<br>*/
    private void a213PreparaUpdFinale() {
        // COB_CODE: MOVE IDSV0003-USER-NAME    TO BPA-USER-START
        btcParallelism.setBpaUserStart(idsv0003.getUserName());
        // COB_CODE: PERFORM ESTRAI-CURRENT-TIMESTAMP
        //                   THRU ESTRAI-CURRENT-TIMESTAMP-EX
        estraiCurrentTimestamp();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              MOVE WS-TIMESTAMP-N    TO BPA-DT-END
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE WS-TIMESTAMP-X    TO BPA-DT-END-DB
            ws.getBtcParallelismDb().setEndDb(ws.getIdsv0014().getTimestampX());
            // COB_CODE: MOVE WS-TIMESTAMP-N    TO BPA-DT-END
            btcParallelism.getBpaDtEnd().setBpaDtEnd(ws.getIdsv0014().getTimestampN());
        }
    }

    /**Original name: A230-UPDATE<br>*/
    private void a230Update() {
        // COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES THRU Z150-EX.
        z150ValorizzaDataServices();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE BTC_PARALLELISM SET
        //                   COD_BATCH_STATE  = :BPA-COD-BATCH-STATE
        //                  ,DT_START         = :BPA-DT-START-DB
        //                                      :IND-BPA-DT-START
        //                  ,DT_END           = :BPA-DT-END-DB
        //                                      :IND-BPA-DT-END
        //                  ,USER_START       = :BPA-USER-START
        //                                      :IND-BPA-USER-START
        //                WHERE COD_COMP_ANIA = :BPA-COD-COMP-ANIA
        //                  AND PROTOCOL      = :BPA-PROTOCOL
        //                  AND PROG_PROTOCOL = :BPA-PROG-PROTOCOL
        //                  AND COD_BATCH_STATE IN (
        //                                         :IABV0002-STATE-01,
        //                                         :IABV0002-STATE-02,
        //                                         :IABV0002-STATE-03,
        //                                         :IABV0002-STATE-04,
        //                                         :IABV0002-STATE-05,
        //                                         :IABV0002-STATE-06,
        //                                         :IABV0002-STATE-07,
        //                                         :IABV0002-STATE-08,
        //                                         :IABV0002-STATE-09,
        //                                         :IABV0002-STATE-10
        //                                         )
        //           END-EXEC.
        btcParallelismDao.updateRec(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A305-DECLARE-CURSOR<br>
	 * <pre>----
	 * ----  gestione FETCH
	 * ----</pre>*/
    private void a305DeclareCursor() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE CURSOR-BPA CURSOR WITH HOLD FOR
        //             SELECT
        //                COD_COMP_ANIA
        //                ,PROTOCOL
        //                ,PROG_PROTOCOL
        //                ,COD_BATCH_STATE
        //                ,DT_INS
        //                ,USER_INS
        //                ,DT_START
        //                ,DT_END
        //                ,USER_START
        //                ,DESC_PARALLELISM
        //                ,ID_OGG_DA
        //                ,ID_OGG_A
        //                ,TP_OGG
        //                ,NUM_ROW_SCHEDULE
        //             FROM BTC_PARALLELISM
        //             WHERE     COD_COMP_ANIA = :BPA-COD-COMP-ANIA
        //                   AND PROTOCOL      = :BPA-PROTOCOL
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A360-OPEN-CURSOR<br>*/
    private void a360OpenCursor() {
        // COB_CODE: PERFORM A305-DECLARE-CURSOR       THRU A305-EX.
        a305DeclareCursor();
        // COB_CODE: EXEC SQL
        //                OPEN CURSOR-BPA
        //           END-EXEC.
        btcParallelismDao.openCursorBpa(btcParallelism.getBpaCodCompAnia(), btcParallelism.getBpaProtocol());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE    THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A370-CLOSE-CURSOR<br>*/
    private void a370CloseCursor() {
        // COB_CODE: EXEC SQL
        //                CLOSE CURSOR-BPA
        //           END-EXEC.
        btcParallelismDao.closeCursorBpa();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE       THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A375-CNTL<br>*/
    private void a375Cntl() {
        ConcatUtil concatUtil = null;
        // COB_CODE: PERFORM A210-SELECT-PK   THRU A210-EX.
        a210SelectPk();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              END-IF
        //           ELSE
        //              END-STRING
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: IF BPA-COD-BATCH-STATE = BATCH-IN-ESECUZIONE
            //              END-STRING
            //           END-IF
            if (btcParallelism.getBpaCodBatchState() == ws.getIabv0004().getBatchInEsecuzione()) {
                // COB_CODE: MOVE SPACES         TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("");
                // COB_CODE: SET IDSV0003-GENERIC-ERROR TO TRUE
                idsv0003.getReturnCode().setGenericError();
                // COB_CODE: MOVE BPA-COD-COMP-ANIA TO COMODO-COD-COMP-ANIA
                ws.setComodoCodCompAnia(TruncAbs.toInt(btcParallelism.getBpaCodCompAnia(), 9));
                // COB_CODE: MOVE BPA-PROG-PROTOCOL TO COMODO-PROG-PROTOCOL
                ws.setComodoProgProtocol(TruncAbs.toInt(btcParallelism.getBpaProgProtocol(), 9));
                // COB_CODE: STRING 'JOB PARALLELO GIA'' IN ESECUZIONE'
                //               DELIMITED BY SIZE
                //               COMODO-COD-COMP-ANIA
                //               ' / '
                //               DELIMITED BY SIZE
                //               BPA-PROTOCOL
                //               DELIMITED BY SPACES
                //               ' / '
                //               DELIMITED BY SIZE
                //               COMODO-PROG-PROTOCOL
                //               DELIMITED BY SPACES
                //               INTO
                //               IDSV0003-DESCRIZ-ERR-DB2
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, new String[] {"JOB PARALLELO GIA' IN ESECUZIONE", ws.getComodoCodCompAniaAsString(), " / ", Functions.substringBefore(btcParallelism.getBpaProtocolFormatted(), Types.SPACE_STRING), " / ", Functions.substringBefore(ws.getComodoProgProtocolAsString(), Types.SPACE_STRING)});
                idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
            }
        }
        else {
            // COB_CODE: MOVE SPACES            TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("");
            // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
            idsv0003.getReturnCode().setSqlError();
            // COB_CODE: MOVE BPA-COD-COMP-ANIA TO COMODO-COD-COMP-ANIA
            ws.setComodoCodCompAnia(TruncAbs.toInt(btcParallelism.getBpaCodCompAnia(), 9));
            // COB_CODE: MOVE BPA-PROG-PROTOCOL TO COMODO-PROG-PROTOCOL
            ws.setComodoProgProtocol(TruncAbs.toInt(btcParallelism.getBpaProgProtocol(), 9));
            // COB_CODE: STRING 'JOB PARALLELO NON ESEGUIBILE : '
            //                  DELIMITED BY SIZE
            //                  COMODO-COD-COMP-ANIA
            //                  ' / '
            //                  DELIMITED BY SIZE
            //                  BPA-PROTOCOL
            //                  DELIMITED BY SPACES
            //                  ' / '
            //                  DELIMITED BY SIZE
            //                  COMODO-PROG-PROTOCOL
            //                  DELIMITED BY SPACES
            //                  INTO
            //                  IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, new String[] {"JOB PARALLELO NON ESEGUIBILE : ", ws.getComodoCodCompAniaAsString(), " / ", Functions.substringBefore(btcParallelism.getBpaProtocolFormatted(), Types.SPACE_STRING), " / ", Functions.substringBefore(ws.getComodoProgProtocolAsString(), Types.SPACE_STRING)});
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
        }
    }

    /**Original name: A380-CNTL<br>*/
    private void a380Cntl() {
        // COB_CODE: SET BATCH-COMPLETE                   TO TRUE.
        ws.getWsBufferWhereCondIabs0130().getFlagBatchComplete().setComplete();
        // COB_CODE: SET BATCH-STATE-OK                   TO TRUE.
        ws.getWsBufferWhereCondIabs0130().getFlagBatchState().setOk();
        // COB_CODE: SET IDSV0003-FETCH-FIRST             TO TRUE.
        idsv0003.getOperazione().setFetchFirst();
        // COB_CODE: MOVE BPA-PROG-PROTOCOL               TO WS-PROG-PROTOCOL.
        ws.setWsProgProtocol(btcParallelism.getBpaProgProtocol());
        // COB_CODE: PERFORM A360-OPEN-CURSOR             THRU A360-EX.
        a360OpenCursor();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              END-PERFORM
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM UNTIL
            //                   NOT IDSV0003-SUCCESSFUL-RC  OR
            //                       WK-FINE-LOOP-SI
            //              END-EVALUATE
            //           END-PERFORM
            while (!(!idsv0003.getReturnCode().isSuccessfulRc() || ws.getWkFineLoop().isSi())) {
                // COB_CODE: PERFORM A390-FETCH-NEXT                 THRU A390-EX
                a390FetchNext();
                // COB_CODE: EVALUATE TRUE
                //               WHEN IDSV0003-SUCCESSFUL-SQL
                //                    SET IDSV0003-FETCH-NEXT        TO TRUE
                //               WHEN IDSV0003-NOT-FOUND
                //                    END-IF
                //           END-EVALUATE
                switch (idsv0003.getSqlcode().getSqlcode()) {

                    case Idsv0003Sqlcode.SUCCESSFUL_SQL:// COB_CODE: PERFORM D000-CTRL-STATI-TOT
                        //                                 THRU D000-EX
                        d000CtrlStatiTot();
                        // COB_CODE: SET IDSV0003-FETCH-NEXT        TO TRUE
                        idsv0003.getOperazione().setFetchNext();
                        break;

                    case Idsv0003Sqlcode.NOT_FOUND:// COB_CODE: SET WK-FINE-LOOP-SI            TO TRUE
                        ws.getWkFineLoop().setSi();
                        // COB_CODE: IF IDSV0003-FETCH-FIRST
                        //              CONTINUE
                        //           ELSE
                        //              SET IDSV0003-SUCCESSFUL-SQL TO TRUE
                        //           END-IF
                        if (idsv0003.getOperazione().isFetchFirst()) {
                        // COB_CODE: CONTINUE
                        //continue
                        }
                        else {
                            // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL TO TRUE
                            idsv0003.getSqlcode().setSuccessfulSql();
                        }
                        break;

                    default:break;
                }
            }
        }
    }

    /**Original name: A390-FETCH-NEXT<br>*/
    private void a390FetchNext() {
        // COB_CODE: EXEC SQL
        //                FETCH CURSOR-BPA
        //             INTO
        //                :BPA-COD-COMP-ANIA
        //               ,:BPA-PROTOCOL
        //               ,:BPA-PROG-PROTOCOL
        //               ,:BPA-COD-BATCH-STATE
        //               ,:BPA-DT-INS-DB
        //               ,:BPA-USER-INS
        //               ,:BPA-DT-START-DB
        //                :IND-BPA-DT-START
        //               ,:BPA-DT-END-DB
        //                :IND-BPA-DT-END
        //               ,:BPA-USER-START
        //                :IND-BPA-USER-START
        //               ,:BPA-DESC-PARALLELISM-VCHAR
        //                :IND-BPA-DESC-PARALLELISM
        //               ,:BPA-ID-OGG-DA
        //                :IND-BPA-ID-OGG-DA
        //               ,:BPA-ID-OGG-A
        //                :IND-BPA-ID-OGG-A
        //               ,:BPA-TP-OGG
        //                :IND-BPA-TP-OGG
        //               ,:BPA-NUM-ROW-SCHEDULE
        //                :IND-BPA-NUM-ROW-SCHEDULE
        //           END-EXEC.
        btcParallelismDao.fetchCursorBpa(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A370-CLOSE-CURSOR THRU A370-EX
            a370CloseCursor();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: D000-CTRL-STATI-TOT<br>*/
    private void d000CtrlStatiTot() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF  BPA-COD-BATCH-STATE   = BATCH-IN-ESECUZIONE OR
            //               BPA-COD-BATCH-STATE   = BATCH-DA-ESEGUIRE
            //               SET BATCH-NOT-COMPLETE    TO TRUE
            //           END-IF
            if (btcParallelism.getBpaCodBatchState() == ws.getIabv0004().getBatchInEsecuzione() || btcParallelism.getBpaCodBatchState() == ws.getIabv0004().getBatchDaEseguire()) {
                // COB_CODE: SET BATCH-NOT-COMPLETE    TO TRUE
                ws.getWsBufferWhereCondIabs0130().getFlagBatchComplete().setNotComplete();
            }
            // COB_CODE: IF BPA-COD-BATCH-STATE NOT = BATCH-ESEGUITO AND
            //              BPA-COD-BATCH-STATE NOT = BATCH-SIMULATO-OK
            //              SET BATCH-STATE-KO        TO TRUE
            //           END-IF
            if (btcParallelism.getBpaCodBatchState() != ws.getIabv0004().getBatchEseguito() && btcParallelism.getBpaCodBatchState() != ws.getIabv0004().getBatchSimulatoOk()) {
                // COB_CODE: SET BATCH-STATE-KO        TO TRUE
                ws.getWsBufferWhereCondIabs0130().getFlagBatchState().setKo();
            }
        }
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-BPA-DT-START = -1
        //              MOVE HIGH-VALUES TO BPA-DT-START-NULL
        //           END-IF
        if (ws.getIndBtcParallelism().getDtStart() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BPA-DT-START-NULL
            btcParallelism.getBpaDtStart().setBpaDtStartNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BpaDtStart.Len.BPA_DT_START_NULL));
        }
        // COB_CODE: IF IND-BPA-DT-END = -1
        //              MOVE HIGH-VALUES TO BPA-DT-END-NULL
        //           END-IF
        if (ws.getIndBtcParallelism().getDtEnd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BPA-DT-END-NULL
            btcParallelism.getBpaDtEnd().setBpaDtEndNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BpaDtEnd.Len.BPA_DT_END_NULL));
        }
        // COB_CODE: IF IND-BPA-USER-START = -1
        //              MOVE HIGH-VALUES TO BPA-USER-START-NULL
        //           END-IF
        if (ws.getIndBtcParallelism().getUserStart() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BPA-USER-START-NULL
            btcParallelism.setBpaUserStart(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BtcParallelism.Len.BPA_USER_START));
        }
        // COB_CODE: IF IND-BPA-DESC-PARALLELISM = -1
        //              MOVE HIGH-VALUES TO BPA-DESC-PARALLELISM
        //           END-IF
        if (ws.getIndBtcParallelism().getDescParallelism() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BPA-DESC-PARALLELISM
            btcParallelism.setBpaDescParallelism(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BtcParallelism.Len.BPA_DESC_PARALLELISM));
        }
        // COB_CODE: IF IND-BPA-ID-OGG-DA = -1
        //              MOVE HIGH-VALUES TO BPA-ID-OGG-DA-NULL
        //           END-IF
        if (ws.getIndBtcParallelism().getIdOggDa() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BPA-ID-OGG-DA-NULL
            btcParallelism.getBpaIdOggDa().setBpaIdOggDaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BpaIdOggDa.Len.BPA_ID_OGG_DA_NULL));
        }
        // COB_CODE: IF IND-BPA-ID-OGG-A = -1
        //              MOVE HIGH-VALUES TO BPA-ID-OGG-A-NULL
        //           END-IF
        if (ws.getIndBtcParallelism().getIdOggA() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BPA-ID-OGG-A-NULL
            btcParallelism.getBpaIdOggA().setBpaIdOggANull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BpaIdOggA.Len.BPA_ID_OGG_A_NULL));
        }
        // COB_CODE: IF IND-BPA-TP-OGG = -1
        //              MOVE HIGH-VALUES TO BPA-TP-OGG-NULL
        //           END-IF
        if (ws.getIndBtcParallelism().getTpOgg() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BPA-TP-OGG-NULL
            btcParallelism.setBpaTpOgg(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BtcParallelism.Len.BPA_TP_OGG));
        }
        // COB_CODE: IF IND-BPA-NUM-ROW-SCHEDULE = -1
        //              MOVE HIGH-VALUES TO BPA-NUM-ROW-SCHEDULE-NULL
        //           END-IF.
        if (ws.getIndBtcParallelism().getNumRowSchedule() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BPA-NUM-ROW-SCHEDULE-NULL
            btcParallelism.getBpaNumRowSchedule().setBpaNumRowScheduleNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BpaNumRowSchedule.Len.BPA_NUM_ROW_SCHEDULE_NULL));
        }
    }

    /**Original name: Z150-VALORIZZA-DATA-SERVICES<br>*/
    private void z150ValorizzaDataServices() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z200-SET-INDICATORI-NULL<br>*/
    private void z200SetIndicatoriNull() {
        // COB_CODE: IF BPA-DT-START-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BPA-DT-START
        //           ELSE
        //              MOVE 0 TO IND-BPA-DT-START
        //           END-IF
        if (Characters.EQ_HIGH.test(btcParallelism.getBpaDtStart().getBpaDtStartNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BPA-DT-START
            ws.getIndBtcParallelism().setDtStart(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BPA-DT-START
            ws.getIndBtcParallelism().setDtStart(((short)0));
        }
        // COB_CODE: IF BPA-DT-END-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BPA-DT-END
        //           ELSE
        //              MOVE 0 TO IND-BPA-DT-END
        //           END-IF
        if (Characters.EQ_HIGH.test(btcParallelism.getBpaDtEnd().getBpaDtEndNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BPA-DT-END
            ws.getIndBtcParallelism().setDtEnd(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BPA-DT-END
            ws.getIndBtcParallelism().setDtEnd(((short)0));
        }
        // COB_CODE: IF BPA-USER-START-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BPA-USER-START
        //           ELSE
        //              MOVE 0 TO IND-BPA-USER-START
        //           END-IF
        if (Characters.EQ_HIGH.test(btcParallelism.getBpaUserStart(), BtcParallelism.Len.BPA_USER_START)) {
            // COB_CODE: MOVE -1 TO IND-BPA-USER-START
            ws.getIndBtcParallelism().setUserStart(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BPA-USER-START
            ws.getIndBtcParallelism().setUserStart(((short)0));
        }
        // COB_CODE: IF BPA-DESC-PARALLELISM = HIGH-VALUES
        //              MOVE -1 TO IND-BPA-DESC-PARALLELISM
        //           ELSE
        //              MOVE 0 TO IND-BPA-DESC-PARALLELISM
        //           END-IF
        if (Characters.EQ_HIGH.test(btcParallelism.getBpaDescParallelism(), BtcParallelism.Len.BPA_DESC_PARALLELISM)) {
            // COB_CODE: MOVE -1 TO IND-BPA-DESC-PARALLELISM
            ws.getIndBtcParallelism().setDescParallelism(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BPA-DESC-PARALLELISM
            ws.getIndBtcParallelism().setDescParallelism(((short)0));
        }
        // COB_CODE: IF BPA-ID-OGG-DA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BPA-ID-OGG-DA
        //           ELSE
        //              MOVE 0 TO IND-BPA-ID-OGG-DA
        //           END-IF
        if (Characters.EQ_HIGH.test(btcParallelism.getBpaIdOggDa().getBpaIdOggDaNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BPA-ID-OGG-DA
            ws.getIndBtcParallelism().setIdOggDa(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BPA-ID-OGG-DA
            ws.getIndBtcParallelism().setIdOggDa(((short)0));
        }
        // COB_CODE: IF BPA-ID-OGG-A-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BPA-ID-OGG-A
        //           ELSE
        //              MOVE 0 TO IND-BPA-ID-OGG-A
        //           END-IF
        if (Characters.EQ_HIGH.test(btcParallelism.getBpaIdOggA().getBpaIdOggANullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BPA-ID-OGG-A
            ws.getIndBtcParallelism().setIdOggA(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BPA-ID-OGG-A
            ws.getIndBtcParallelism().setIdOggA(((short)0));
        }
        // COB_CODE: IF BPA-TP-OGG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BPA-TP-OGG
        //           ELSE
        //              MOVE 0 TO IND-BPA-TP-OGG
        //           END-IF
        if (Characters.EQ_HIGH.test(btcParallelism.getBpaTpOggFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BPA-TP-OGG
            ws.getIndBtcParallelism().setTpOgg(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BPA-TP-OGG
            ws.getIndBtcParallelism().setTpOgg(((short)0));
        }
        // COB_CODE: IF BPA-NUM-ROW-SCHEDULE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BPA-NUM-ROW-SCHEDULE
        //           ELSE
        //              MOVE 0 TO IND-BPA-NUM-ROW-SCHEDULE
        //           END-IF.
        if (Characters.EQ_HIGH.test(btcParallelism.getBpaNumRowSchedule().getBpaNumRowScheduleNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BPA-NUM-ROW-SCHEDULE
            ws.getIndBtcParallelism().setNumRowSchedule(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BPA-NUM-ROW-SCHEDULE
            ws.getIndBtcParallelism().setNumRowSchedule(((short)0));
        }
    }

    /**Original name: Z900-CONVERTI-N-TO-X<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da 9(8) comp-3 a date
	 * ----</pre>*/
    private void z900ConvertiNToX() {
        // COB_CODE: MOVE BPA-DT-INS TO WS-TIMESTAMP-N
        ws.getIdsv0014().setTimestampN(TruncAbs.toLong(btcParallelism.getBpaDtIns(), 18));
        // COB_CODE: PERFORM Z701-TS-N-TO-X   THRU Z701-EX
        z701TsNToX();
        // COB_CODE: MOVE WS-TIMESTAMP-X   TO BPA-DT-INS-DB
        ws.getBtcParallelismDb().setInsDb(ws.getIdsv0014().getTimestampX());
        // COB_CODE: IF IND-BPA-DT-START = 0
        //               MOVE WS-TIMESTAMP-X      TO BPA-DT-START-DB
        //           END-IF
        if (ws.getIndBtcParallelism().getDtStart() == 0) {
            // COB_CODE: MOVE BPA-DT-START TO WS-TIMESTAMP-N
            ws.getIdsv0014().setTimestampN(TruncAbs.toLong(btcParallelism.getBpaDtStart().getBpaDtStart(), 18));
            // COB_CODE: PERFORM Z701-TS-N-TO-X   THRU Z701-EX
            z701TsNToX();
            // COB_CODE: MOVE WS-TIMESTAMP-X      TO BPA-DT-START-DB
            ws.getBtcParallelismDb().setStartDb(ws.getIdsv0014().getTimestampX());
        }
        // COB_CODE: IF IND-BPA-DT-END = 0
        //               MOVE WS-TIMESTAMP-X      TO BPA-DT-END-DB
        //           END-IF.
        if (ws.getIndBtcParallelism().getDtEnd() == 0) {
            // COB_CODE: MOVE BPA-DT-END TO WS-TIMESTAMP-N
            ws.getIdsv0014().setTimestampN(TruncAbs.toLong(btcParallelism.getBpaDtEnd().getBpaDtEnd(), 18));
            // COB_CODE: PERFORM Z701-TS-N-TO-X   THRU Z701-EX
            z701TsNToX();
            // COB_CODE: MOVE WS-TIMESTAMP-X      TO BPA-DT-END-DB
            ws.getBtcParallelismDb().setEndDb(ws.getIdsv0014().getTimestampX());
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE BPA-DT-INS-DB TO WS-TIMESTAMP-X
        ws.getIdsv0014().setTimestampX(ws.getBtcParallelismDb().getInsDb());
        // COB_CODE: PERFORM Z801-TS-X-TO-N     THRU Z801-EX
        z801TsXToN();
        // COB_CODE: MOVE WS-TIMESTAMP-N      TO BPA-DT-INS
        btcParallelism.setBpaDtIns(ws.getIdsv0014().getTimestampN());
        // COB_CODE: IF IND-BPA-DT-START = 0
        //               MOVE WS-TIMESTAMP-N      TO BPA-DT-START
        //           END-IF
        if (ws.getIndBtcParallelism().getDtStart() == 0) {
            // COB_CODE: MOVE BPA-DT-START-DB TO WS-TIMESTAMP-X
            ws.getIdsv0014().setTimestampX(ws.getBtcParallelismDb().getStartDb());
            // COB_CODE: PERFORM Z801-TS-X-TO-N     THRU Z801-EX
            z801TsXToN();
            // COB_CODE: MOVE WS-TIMESTAMP-N      TO BPA-DT-START
            btcParallelism.getBpaDtStart().setBpaDtStart(ws.getIdsv0014().getTimestampN());
        }
        // COB_CODE: IF IND-BPA-DT-END = 0
        //               MOVE WS-TIMESTAMP-N      TO BPA-DT-END
        //           END-IF.
        if (ws.getIndBtcParallelism().getDtEnd() == 0) {
            // COB_CODE: MOVE BPA-DT-END-DB TO WS-TIMESTAMP-X
            ws.getIdsv0014().setTimestampX(ws.getBtcParallelismDb().getEndDb());
            // COB_CODE: PERFORM Z801-TS-X-TO-N     THRU Z801-EX
            z801TsXToN();
            // COB_CODE: MOVE WS-TIMESTAMP-N      TO BPA-DT-END
            btcParallelism.getBpaDtEnd().setBpaDtEnd(ws.getIdsv0014().getTimestampN());
        }
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
        // COB_CODE: MOVE LENGTH OF BPA-DESC-PARALLELISM
        //                       TO BPA-DESC-PARALLELISM-LEN.
        btcParallelism.setBpaDescParallelismLen(((short)BtcParallelism.Len.BPA_DESC_PARALLELISM));
    }

    /**Original name: Z701-TS-N-TO-X<br>*/
    private void z701TsNToX() {
        // COB_CODE: MOVE WS-STR-TIMESTAMP-N(1:4)
        //                TO WS-TIMESTAMP-X(1:4)
        ws.getIdsv0014().setTimestampX(Functions.setSubstring(ws.getIdsv0014().getTimestampX(), ws.getIdsv0014().getStrTimestampNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-TIMESTAMP-N(5:2)
        //                TO WS-TIMESTAMP-X(6:2)
        ws.getIdsv0014().setTimestampX(Functions.setSubstring(ws.getIdsv0014().getTimestampX(), ws.getIdsv0014().getStrTimestampNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-TIMESTAMP-N(7:2)
        //                TO WS-TIMESTAMP-X(9:2)
        ws.getIdsv0014().setTimestampX(Functions.setSubstring(ws.getIdsv0014().getTimestampX(), ws.getIdsv0014().getStrTimestampNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE WS-STR-TIMESTAMP-N(9:2)
        //                TO WS-TIMESTAMP-X(12:2)
        ws.getIdsv0014().setTimestampX(Functions.setSubstring(ws.getIdsv0014().getTimestampX(), ws.getIdsv0014().getStrTimestampNFormatted().substring((9) - 1, 10), 12, 2));
        // COB_CODE: MOVE WS-STR-TIMESTAMP-N(11:2)
        //                TO WS-TIMESTAMP-X(15:2)
        ws.getIdsv0014().setTimestampX(Functions.setSubstring(ws.getIdsv0014().getTimestampX(), ws.getIdsv0014().getStrTimestampNFormatted().substring((11) - 1, 12), 15, 2));
        // COB_CODE: MOVE WS-STR-TIMESTAMP-N(13:2)
        //                TO WS-TIMESTAMP-X(18:2)
        ws.getIdsv0014().setTimestampX(Functions.setSubstring(ws.getIdsv0014().getTimestampX(), ws.getIdsv0014().getStrTimestampNFormatted().substring((13) - 1, 14), 18, 2));
        // COB_CODE: MOVE WS-STR-TIMESTAMP-N(15:4)
        //                TO WS-TIMESTAMP-X(21:4)
        ws.getIdsv0014().setTimestampX(Functions.setSubstring(ws.getIdsv0014().getTimestampX(), ws.getIdsv0014().getStrTimestampNFormatted().substring((15) - 1, 18), 21, 4));
        // COB_CODE: MOVE '00'
        //                TO WS-TIMESTAMP-X(25:2)
        ws.getIdsv0014().setTimestampX(Functions.setSubstring(ws.getIdsv0014().getTimestampX(), "00", 25, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-TIMESTAMP-X(5:1)
        //                   WS-TIMESTAMP-X(8:1)
        ws.getIdsv0014().setTimestampX(Functions.setSubstring(ws.getIdsv0014().getTimestampX(), "-", 5, 1));
        ws.getIdsv0014().setTimestampX(Functions.setSubstring(ws.getIdsv0014().getTimestampX(), "-", 8, 1));
        // COB_CODE: MOVE SPACE
        //                TO WS-TIMESTAMP-X(11:1)
        ws.getIdsv0014().setTimestampX(Functions.setSubstring(ws.getIdsv0014().getTimestampX(), LiteralGenerator.create(Types.SPACE_CHAR, 1), 11, 1));
        // COB_CODE: MOVE ':'
        //                TO WS-TIMESTAMP-X(14:1)
        //                   WS-TIMESTAMP-X(17:1)
        ws.getIdsv0014().setTimestampX(Functions.setSubstring(ws.getIdsv0014().getTimestampX(), ":", 14, 1));
        ws.getIdsv0014().setTimestampX(Functions.setSubstring(ws.getIdsv0014().getTimestampX(), ":", 17, 1));
        // COB_CODE: MOVE '.'
        //                TO WS-TIMESTAMP-X(20:1).
        ws.getIdsv0014().setTimestampX(Functions.setSubstring(ws.getIdsv0014().getTimestampX(), ".", 20, 1));
    }

    /**Original name: Z801-TS-X-TO-N<br>*/
    private void z801TsXToN() {
        // COB_CODE: MOVE WS-TIMESTAMP-X(1:4)
        //                TO WS-STR-TIMESTAMP-N(1:4)
        ws.getIdsv0014().setStrTimestampNFormatted(Functions.setSubstring(ws.getIdsv0014().getStrTimestampNFormatted(), ws.getIdsv0014().getTimestampXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-TIMESTAMP-X(6:2)
        //                TO WS-STR-TIMESTAMP-N(5:2)
        ws.getIdsv0014().setStrTimestampNFormatted(Functions.setSubstring(ws.getIdsv0014().getStrTimestampNFormatted(), ws.getIdsv0014().getTimestampXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-TIMESTAMP-X(9:2)
        //                TO WS-STR-TIMESTAMP-N(7:2)
        ws.getIdsv0014().setStrTimestampNFormatted(Functions.setSubstring(ws.getIdsv0014().getStrTimestampNFormatted(), ws.getIdsv0014().getTimestampXFormatted().substring((9) - 1, 10), 7, 2));
        // COB_CODE: MOVE WS-TIMESTAMP-X(12:2)
        //                TO WS-STR-TIMESTAMP-N(9:2)
        ws.getIdsv0014().setStrTimestampNFormatted(Functions.setSubstring(ws.getIdsv0014().getStrTimestampNFormatted(), ws.getIdsv0014().getTimestampXFormatted().substring((12) - 1, 13), 9, 2));
        // COB_CODE: MOVE WS-TIMESTAMP-X(15:2)
        //                TO WS-STR-TIMESTAMP-N(11:2)
        ws.getIdsv0014().setStrTimestampNFormatted(Functions.setSubstring(ws.getIdsv0014().getStrTimestampNFormatted(), ws.getIdsv0014().getTimestampXFormatted().substring((15) - 1, 16), 11, 2));
        // COB_CODE: MOVE WS-TIMESTAMP-X(18:2)
        //                TO WS-STR-TIMESTAMP-N(13:2)
        ws.getIdsv0014().setStrTimestampNFormatted(Functions.setSubstring(ws.getIdsv0014().getStrTimestampNFormatted(), ws.getIdsv0014().getTimestampXFormatted().substring((18) - 1, 19), 13, 2));
        // COB_CODE: MOVE WS-TIMESTAMP-X(21:4)
        //                TO WS-STR-TIMESTAMP-N(15:4).
        ws.getIdsv0014().setStrTimestampNFormatted(Functions.setSubstring(ws.getIdsv0014().getStrTimestampNFormatted(), ws.getIdsv0014().getTimestampXFormatted().substring((21) - 1, 24), 15, 4));
    }

    /**Original name: ESTRAI-CURRENT-TIMESTAMP<br>
	 * <pre>****************************************************************
	 *  N.B. - UTILIZZABILE SOLO INCLUDENDO LE COPY :
	 *         IDSV0014
	 *         IDSP0014
	 * ****************************************************************
	 * ----------------------------------------------------------------*
	 *  SETTA TIMESTAMP
	 * ----------------------------------------------------------------*</pre>*/
    private void estraiCurrentTimestamp() {
        // COB_CODE: EXEC SQL
        //                 VALUES CURRENT TIMESTAMP
        //                   INTO :WS-TIMESTAMP-X
        //           END-EXEC
        ws.getIdsv0014().setTimestampX(SqlFunctions.currentTimestamp());
        // COB_CODE: IF SQLCODE = 0
        //              PERFORM Z801-TS-X-TO-N   THRU Z801-EX
        //           END-IF.
        if (sqlca.getSqlcode() == 0) {
            // COB_CODE: PERFORM Z801-TS-X-TO-N   THRU Z801-EX
            z801TsXToN();
        }
    }

    @Override
    public int getBpaCodCompAnia() {
        return btcParallelism.getBpaCodCompAnia();
    }

    @Override
    public void setBpaCodCompAnia(int bpaCodCompAnia) {
        this.btcParallelism.setBpaCodCompAnia(bpaCodCompAnia);
    }

    @Override
    public int getBpaProgProtocol() {
        return btcParallelism.getBpaProgProtocol();
    }

    @Override
    public void setBpaProgProtocol(int bpaProgProtocol) {
        this.btcParallelism.setBpaProgProtocol(bpaProgProtocol);
    }

    @Override
    public String getBpaProtocol() {
        return btcParallelism.getBpaProtocol();
    }

    @Override
    public void setBpaProtocol(String bpaProtocol) {
        this.btcParallelism.setBpaProtocol(bpaProtocol);
    }

    @Override
    public char getCodBatchState() {
        return btcParallelism.getBpaCodBatchState();
    }

    @Override
    public void setCodBatchState(char codBatchState) {
        this.btcParallelism.setBpaCodBatchState(codBatchState);
    }

    @Override
    public String getDescParallelismVchar() {
        return btcParallelism.getBpaDescParallelismVcharFormatted();
    }

    @Override
    public void setDescParallelismVchar(String descParallelismVchar) {
        this.btcParallelism.setBpaDescParallelismVcharFormatted(descParallelismVchar);
    }

    @Override
    public String getDescParallelismVcharObj() {
        if (ws.getIndBtcParallelism().getDescParallelism() >= 0) {
            return getDescParallelismVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDescParallelismVcharObj(String descParallelismVcharObj) {
        if (descParallelismVcharObj != null) {
            setDescParallelismVchar(descParallelismVcharObj);
            ws.getIndBtcParallelism().setDescParallelism(((short)0));
        }
        else {
            ws.getIndBtcParallelism().setDescParallelism(((short)-1));
        }
    }

    @Override
    public String getDtEndDb() {
        return ws.getBtcParallelismDb().getEndDb();
    }

    @Override
    public void setDtEndDb(String dtEndDb) {
        this.ws.getBtcParallelismDb().setEndDb(dtEndDb);
    }

    @Override
    public String getDtEndDbObj() {
        if (ws.getIndBtcParallelism().getDtEnd() >= 0) {
            return getDtEndDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtEndDbObj(String dtEndDbObj) {
        if (dtEndDbObj != null) {
            setDtEndDb(dtEndDbObj);
            ws.getIndBtcParallelism().setDtEnd(((short)0));
        }
        else {
            ws.getIndBtcParallelism().setDtEnd(((short)-1));
        }
    }

    @Override
    public String getDtInsDb() {
        return ws.getBtcParallelismDb().getInsDb();
    }

    @Override
    public void setDtInsDb(String dtInsDb) {
        this.ws.getBtcParallelismDb().setInsDb(dtInsDb);
    }

    @Override
    public String getDtStartDb() {
        return ws.getBtcParallelismDb().getStartDb();
    }

    @Override
    public void setDtStartDb(String dtStartDb) {
        this.ws.getBtcParallelismDb().setStartDb(dtStartDb);
    }

    @Override
    public String getDtStartDbObj() {
        if (ws.getIndBtcParallelism().getDtStart() >= 0) {
            return getDtStartDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtStartDbObj(String dtStartDbObj) {
        if (dtStartDbObj != null) {
            setDtStartDb(dtStartDbObj);
            ws.getIndBtcParallelism().setDtStart(((short)0));
        }
        else {
            ws.getIndBtcParallelism().setDtStart(((short)-1));
        }
    }

    @Override
    public char getIabv0002State01() {
        return iabv0002.getIabv0002StateGlobal().getState01();
    }

    @Override
    public void setIabv0002State01(char iabv0002State01) {
        this.iabv0002.getIabv0002StateGlobal().setState01(iabv0002State01);
    }

    @Override
    public char getIabv0002State02() {
        return iabv0002.getIabv0002StateGlobal().getState02();
    }

    @Override
    public void setIabv0002State02(char iabv0002State02) {
        this.iabv0002.getIabv0002StateGlobal().setState02(iabv0002State02);
    }

    @Override
    public char getIabv0002State03() {
        return iabv0002.getIabv0002StateGlobal().getState03();
    }

    @Override
    public void setIabv0002State03(char iabv0002State03) {
        this.iabv0002.getIabv0002StateGlobal().setState03(iabv0002State03);
    }

    @Override
    public char getIabv0002State04() {
        return iabv0002.getIabv0002StateGlobal().getState04();
    }

    @Override
    public void setIabv0002State04(char iabv0002State04) {
        this.iabv0002.getIabv0002StateGlobal().setState04(iabv0002State04);
    }

    @Override
    public char getIabv0002State05() {
        return iabv0002.getIabv0002StateGlobal().getState05();
    }

    @Override
    public void setIabv0002State05(char iabv0002State05) {
        this.iabv0002.getIabv0002StateGlobal().setState05(iabv0002State05);
    }

    @Override
    public char getIabv0002State06() {
        return iabv0002.getIabv0002StateGlobal().getState06();
    }

    @Override
    public void setIabv0002State06(char iabv0002State06) {
        this.iabv0002.getIabv0002StateGlobal().setState06(iabv0002State06);
    }

    @Override
    public char getIabv0002State07() {
        return iabv0002.getIabv0002StateGlobal().getState07();
    }

    @Override
    public void setIabv0002State07(char iabv0002State07) {
        this.iabv0002.getIabv0002StateGlobal().setState07(iabv0002State07);
    }

    @Override
    public char getIabv0002State08() {
        return iabv0002.getIabv0002StateGlobal().getState08();
    }

    @Override
    public void setIabv0002State08(char iabv0002State08) {
        this.iabv0002.getIabv0002StateGlobal().setState08(iabv0002State08);
    }

    @Override
    public char getIabv0002State09() {
        return iabv0002.getIabv0002StateGlobal().getState09();
    }

    @Override
    public void setIabv0002State09(char iabv0002State09) {
        this.iabv0002.getIabv0002StateGlobal().setState09(iabv0002State09);
    }

    @Override
    public char getIabv0002State10() {
        return iabv0002.getIabv0002StateGlobal().getState10();
    }

    @Override
    public void setIabv0002State10(char iabv0002State10) {
        this.iabv0002.getIabv0002StateGlobal().setState10(iabv0002State10);
    }

    @Override
    public int getIdOggA() {
        return btcParallelism.getBpaIdOggA().getBpaIdOggA();
    }

    @Override
    public void setIdOggA(int idOggA) {
        this.btcParallelism.getBpaIdOggA().setBpaIdOggA(idOggA);
    }

    @Override
    public Integer getIdOggAObj() {
        if (ws.getIndBtcParallelism().getIdOggA() >= 0) {
            return ((Integer)getIdOggA());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdOggAObj(Integer idOggAObj) {
        if (idOggAObj != null) {
            setIdOggA(((int)idOggAObj));
            ws.getIndBtcParallelism().setIdOggA(((short)0));
        }
        else {
            ws.getIndBtcParallelism().setIdOggA(((short)-1));
        }
    }

    @Override
    public int getIdOggDa() {
        return btcParallelism.getBpaIdOggDa().getBpaIdOggDa();
    }

    @Override
    public void setIdOggDa(int idOggDa) {
        this.btcParallelism.getBpaIdOggDa().setBpaIdOggDa(idOggDa);
    }

    @Override
    public Integer getIdOggDaObj() {
        if (ws.getIndBtcParallelism().getIdOggDa() >= 0) {
            return ((Integer)getIdOggDa());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdOggDaObj(Integer idOggDaObj) {
        if (idOggDaObj != null) {
            setIdOggDa(((int)idOggDaObj));
            ws.getIndBtcParallelism().setIdOggDa(((short)0));
        }
        else {
            ws.getIndBtcParallelism().setIdOggDa(((short)-1));
        }
    }

    @Override
    public int getNumRowSchedule() {
        return btcParallelism.getBpaNumRowSchedule().getBpaNumRowSchedule();
    }

    @Override
    public void setNumRowSchedule(int numRowSchedule) {
        this.btcParallelism.getBpaNumRowSchedule().setBpaNumRowSchedule(numRowSchedule);
    }

    @Override
    public Integer getNumRowScheduleObj() {
        if (ws.getIndBtcParallelism().getNumRowSchedule() >= 0) {
            return ((Integer)getNumRowSchedule());
        }
        else {
            return null;
        }
    }

    @Override
    public void setNumRowScheduleObj(Integer numRowScheduleObj) {
        if (numRowScheduleObj != null) {
            setNumRowSchedule(((int)numRowScheduleObj));
            ws.getIndBtcParallelism().setNumRowSchedule(((short)0));
        }
        else {
            ws.getIndBtcParallelism().setNumRowSchedule(((short)-1));
        }
    }

    @Override
    public String getTpOgg() {
        return btcParallelism.getBpaTpOgg();
    }

    @Override
    public void setTpOgg(String tpOgg) {
        this.btcParallelism.setBpaTpOgg(tpOgg);
    }

    @Override
    public String getTpOggObj() {
        if (ws.getIndBtcParallelism().getTpOgg() >= 0) {
            return getTpOgg();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpOggObj(String tpOggObj) {
        if (tpOggObj != null) {
            setTpOgg(tpOggObj);
            ws.getIndBtcParallelism().setTpOgg(((short)0));
        }
        else {
            ws.getIndBtcParallelism().setTpOgg(((short)-1));
        }
    }

    @Override
    public String getUserIns() {
        return btcParallelism.getBpaUserIns();
    }

    @Override
    public void setUserIns(String userIns) {
        this.btcParallelism.setBpaUserIns(userIns);
    }

    @Override
    public String getUserStart() {
        return btcParallelism.getBpaUserStart();
    }

    @Override
    public void setUserStart(String userStart) {
        this.btcParallelism.setBpaUserStart(userStart);
    }

    @Override
    public String getUserStartObj() {
        if (ws.getIndBtcParallelism().getUserStart() >= 0) {
            return getUserStart();
        }
        else {
            return null;
        }
    }

    @Override
    public void setUserStartObj(String userStartObj) {
        if (userStartObj != null) {
            setUserStart(userStartObj);
            ws.getIndBtcParallelism().setUserStart(((short)0));
        }
        else {
            ws.getIndBtcParallelism().setUserStart(((short)-1));
        }
    }
}
