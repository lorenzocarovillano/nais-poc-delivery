package it.accenture.jnais;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Idsv0003CampiEsito;
import it.accenture.jnais.ws.enums.Idsv0003Sqlcode;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.Lvvs2780Data;

/**Original name: LVVS2780<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2013.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *   PROGRAMMA...... LVVS2780
 *   TIPOLOGIA...... SERVIZIO
 *   PROCESSO....... XXX
 *   FUNZIONE....... XXX
 *   DESCRIZIONE.... IMPOSTA DI BOLLO
 *                CALCOLO DELLA VARIABILE AMMISSIONE BOLLO (Isbollo)
 * **------------------------------------------------------------***</pre>*/
public class Lvvs2780 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lvvs2780Data ws = new Lvvs2780Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS2780
    private Ivvc0213 ivvc0213;

    //==== METHODS ====
    /**Original name: PROGRAM_LVVS2780_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: PERFORM S1000-ELABORAZIONE
        //              THRU EX-S1000
        s1000Elaborazione();
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lvvs2780 getInstance() {
        return ((Lvvs2780)Programs.getInstance(Lvvs2780.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                        IX-INDICI
        //                                             IVVC0213-TAB-OUTPUT
        //                                             WK-VAR-MOVI-COMUN.
        initIxIndici();
        initTabOutput();
        initWkVarMoviComun();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET COMUN-TROV-NO                 TO TRUE.
        ws.getFlagComunTrov().setNo();
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE
        //             TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: INITIALIZE AREA-IO-POLI.
        initAreaIoPoli();
        //--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
        //--> RISPETTIVE AREE DCLGEN IN WORKING
        // COB_CODE: PERFORM S1100-VALORIZZA-DCLGEN
        //              THRU S1100-VALORIZZA-DCLGEN-EX
        //           VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
        //                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //                   SPACES OR LOW-VALUE OR HIGH-VALUE
        ws.setIxDclgen(((short)1));
        while (!(ws.getIxDclgen() > ivvc0213.getEleInfoMax() || Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias()) || Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()) || Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()))) {
            s1100ValorizzaDclgen();
            ws.setIxDclgen(Trunc.toShort(ws.getIxDclgen() + 1, 4));
        }
        // COB_CODE: PERFORM CALCOLA-FLAG
        //              THRU CALCOLA-FLAG-EX.
        calcolaFlag();
    }

    /**Original name: CALCOLA-FLAG<br>
	 * <pre>----------------------------------------------------------------*
	 *     VERIFICA POLIZZA
	 * ----------------------------------------------------------------*</pre>*/
    private void calcolaFlag() {
        // COB_CODE: MOVE 0 TO IVVC0213-VAL-IMP-O
        ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(0, 18, 7));
        // COB_CODE: IF DPOL-TP-RGM-FISC = '01'
        //              END-IF
        //           END-IF.
        if (Conditions.eq(ws.getLccvpol1().getDati().getWpolTpRgmFisc(), "01")) {
            // COB_CODE: MOVE DPOL-TP-POLI
            //             TO ISPV0000-TP-POLI
            ws.getIspv0000().getTpPoli().setTpPoli(ws.getLccvpol1().getDati().getWpolTpPoli());
            // COB_CODE: IF ISPV0000-IND-FIP-PIP
            //              CONTINUE
            //           ELSE
            //              END-IF
            //           END-IF
            if (ws.getIspv0000().getTpPoli().isIspv0000IndFipPip()) {
            // COB_CODE: CONTINUE
            //continue
            }
            else {
                // COB_CODE: PERFORM VERIFICA-MOVIMENTO
                //              THRU VERIFICA-MOVIMENTO-EX
                verificaMovimento();
                // COB_CODE: PERFORM S1110-LETTURA-GARANZIA
                //              THRU S1110-LETTURA-GARANZIA-EX
                s1110LetturaGaranzia();
                // COB_CODE:            IF IDSV0003-SUCCESSFUL-RC
                //           *--> SE SONO PRESENTI GARANZIE DI RAMO I E III O V
                //           *--> VERIFICHIAMO LA PRESENZA DI BOLLI PREGRESSI
                //                         END-IF
                //                      END-IF
                if (idsv0003.getReturnCode().isSuccessfulRc()) {
                    //--> SE SONO PRESENTI GARANZIE DI RAMO I E III O V
                    //--> VERIFICHIAMO LA PRESENZA DI BOLLI PREGRESSI
                    // COB_CODE:               IF RAMO-I-SI AND RAMO-III-V-SI
                    //           *                PERFORM S1120-VERIFICA-BOL-PRE
                    //           *                   THRU S1120-VERIFICA-BOL-PRE-EX
                    //                            MOVE 1 TO IVVC0213-VAL-IMP-O
                    //                         ELSE
                    //                            END-IF
                    //                         END-IF
                    if (ws.getFlagRamoI().isSi() && ws.getFlagRamoIiiV().isSi()) {
                        //                PERFORM S1120-VERIFICA-BOL-PRE
                        //                   THRU S1120-VERIFICA-BOL-PRE-EX
                        // COB_CODE: MOVE 1 TO IVVC0213-VAL-IMP-O
                        ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(1, 18, 7));
                    }
                    else if (ws.getFlagRamoI().isSi() && ws.getFlagRamoIiiV().isNo()) {
                        // COB_CODE: IF RAMO-I-SI AND RAMO-III-V-NO
                        //                 THRU S1120-VERIFICA-BOL-PRE-EX
                        //           ELSE
                        //              END-IF
                        //           END-IF
                        // COB_CODE: PERFORM S1120-VERIFICA-BOL-PRE
                        //              THRU S1120-VERIFICA-BOL-PRE-EX
                        s1120VerificaBolPre();
                    }
                    else if (ws.getFlagRamoI().isNo() && ws.getFlagRamoIiiV().isSi()) {
                        // COB_CODE: IF RAMO-I-NO AND RAMO-III-V-SI
                        //              MOVE 1 TO IVVC0213-VAL-IMP-O
                        //           ELSE
                        //              MOVE 0 TO IVVC0213-VAL-IMP-O
                        //           END-IF
                        // COB_CODE: MOVE 1 TO IVVC0213-VAL-IMP-O
                        ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(1, 18, 7));
                    }
                    else {
                        // COB_CODE: MOVE 0 TO IVVC0213-VAL-IMP-O
                        ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(0, 18, 7));
                    }
                }
            }
        }
    }

    /**Original name: VERIFICA-MOVIMENTO<br>
	 * <pre>----------------------------------------------------------------*
	 *     VERIFICA MOVIMENTO
	 * ----------------------------------------------------------------*
	 * --> SE CI TROVIAMO IN FASE DI LIQUIDAZIONE DEVO RECUPERARE LE
	 * --> IMMAGINI IN FASE DI COMUNICAZIONE
	 *     MOVE IVVC0213-TIPO-MOVIMENTO
	 *       TO WS-MOVIMENTO</pre>*/
    private void verificaMovimento() {
        // COB_CODE: MOVE IVVC0213-TIPO-MOVI-ORIG
        //             TO WS-MOVIMENTO
        ws.getWsMovimento().setWsMovimentoFormatted(ivvc0213.getTipoMoviOrigFormatted());
        // COB_CODE:      IF LIQUI-RISTOT-IND OR LIQUI-RISPAR-POLIND OR
        //                   LIQUI-SCAPOL OR LIQUI-SININD OR LIQUI-RECIND OR
        //                   LIQUI-RPP-TAKE-PROFIT
        //                OR LIQUI-RISTOT-INCAPIENZA
        //                OR LIQUI-RPP-REDDITO-PROGR
        //                OR LIQUI-RPP-BENEFICIO-CONTR
        //                OR LIQUI-RISTOT-INCAP
        //           *--> RECUPERO IL MOVIMENTO DI COMUNICAZIONE
        //                   END-IF
        //                ELSE
        //                   CONTINUE
        //                END-IF.
        if (ws.getWsMovimento().isLiquiRistotInd() || ws.getWsMovimento().isLiquiRisparPolind() || ws.getWsMovimento().isLiquiScapol() || ws.getWsMovimento().isLiquiSinind() || ws.getWsMovimento().isLiquiRecind() || ws.getWsMovimento().isLiquiRppTakeProfit() || ws.getWsMovimento().isLiquiRistotIncapienza() || ws.getWsMovimento().isLiquiRppRedditoProgr() || ws.getWsMovimento().isLiquiRppBeneficioContr() || ws.getWsMovimento().isLiquiRistotIncap()) {
            //--> RECUPERO IL MOVIMENTO DI COMUNICAZIONE
            // COB_CODE: PERFORM RECUP-MOVI-COMUN
            //              THRU RECUP-MOVI-COMUN-EX
            recupMoviComun();
            // COB_CODE: IF COMUN-TROV-SI
            //                   IDSV0003-DATA-FINE-EFFETTO
            //           ELSE
            //              CONTINUE
            //           END-IF
            if (ws.getFlagComunTrov().isSi()) {
                // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA
                //             TO WK-DATA-CPTZ-RIP
                ws.getWkVarMoviComun().setDataCptzRip(idsv0003.getDataCompetenza());
                // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
                //             TO WK-DATA-EFF-RIP
                ws.getWkVarMoviComun().setDataEffRip(idsv0003.getDataInizioEffetto());
                // COB_CODE: MOVE WK-DATA-CPTZ-PREC
                //             TO IDSV0003-DATA-COMPETENZA
                idsv0003.setDataCompetenza(ws.getWkVarMoviComun().getDataCptzPrec());
                // COB_CODE: MOVE WK-DATA-EFF-PREC
                //             TO IDSV0003-DATA-INIZIO-EFFETTO
                //                IDSV0003-DATA-FINE-EFFETTO
                idsv0003.setDataInizioEffetto(ws.getWkVarMoviComun().getDataEffPrec());
                idsv0003.setDataFineEffetto(ws.getWkVarMoviComun().getDataEffPrec());
            }
            else {
            // COB_CODE: CONTINUE
            //continue
            }
        }
        else {
        // COB_CODE: CONTINUE
        //continue
        }
    }

    /**Original name: RECUP-MOVI-COMUN<br>
	 * <pre>----------------------------------------------------------------*
	 *     Recupero il movimento di comunicazione
	 * ----------------------------------------------------------------*</pre>*/
    private void recupMoviComun() {
        Ldbs6040 ldbs6040 = null;
        // COB_CODE: SET INIT-CUR-MOV               TO TRUE
        ws.getFlagCurMov().setInitCurMov();
        // COB_CODE: SET COMUN-TROV-NO              TO TRUE
        ws.getFlagComunTrov().setNo();
        // COB_CODE: INITIALIZE MOVI
        initMovi();
        //--> TIPO OPERAZIONE
        // COB_CODE: SET IDSV0003-FETCH-FIRST       TO TRUE
        idsv0003.getOperazione().setFetchFirst();
        //--> INIZIALIZZA CODICE DI RITORNO
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC    TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET  NO-ULTIMA-LETTURA         TO TRUE
        ws.getFlagUltimaLettura().setNoUltimaLettura();
        //
        // COB_CODE: PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC
        //                      OR FINE-CUR-MOV
        //                      OR COMUN-TROV-SI
        //                 END-IF
        //           END-PERFORM.
        while (!(!idsv0003.getReturnCode().isSuccessfulRc() || ws.getFlagCurMov().isFineCurMov() || ws.getFlagComunTrov().isSi())) {
            // COB_CODE: INITIALIZE MOVI
            initMovi();
            //
            // COB_CODE: MOVE IVVC0213-ID-POLIZZA    TO MOV-ID-OGG
            ws.getMovi().getMovIdOgg().setMovIdOgg(ivvc0213.getIdPolizza());
            // COB_CODE: MOVE 'PO'                   TO MOV-TP-OGG
            ws.getMovi().setMovTpOgg("PO");
            // COB_CODE: SET IDSV0003-TRATT-SENZA-STOR   TO TRUE
            idsv0003.getTrattamentoStoricita().setTrattSenzaStor();
            //--> LIVELLO OPERAZIONE
            // COB_CODE: SET IDSV0003-WHERE-CONDITION TO TRUE
            idsv0003.getLivelloOperazione().setWhereCondition();
            //--> INIZIALIZZA CODICE DI RITORNO
            // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC  TO TRUE
            idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
            // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL TO TRUE
            idsv0003.getSqlcode().setSuccessfulSql();
            // COB_CODE: CALL  LDBS6040   USING IDSV0003 MOVI
            //             ON EXCEPTION
            //                SET IDSV0003-INVALID-OPER   TO TRUE
            //           END-CALL
            try {
                ldbs6040 = Ldbs6040.getInstance();
                ldbs6040.run(idsv0003, ws.getMovi());
            }
            catch (ProgramExecutionException __ex) {
                // COB_CODE: MOVE LDBS6040          TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbs6040());
                // COB_CODE: MOVE 'CALL-LDBS6040 ERRORE CHIAMATA'
                //             TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBS6040 ERRORE CHIAMATA");
                // COB_CODE: SET IDSV0003-INVALID-OPER   TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
            // COB_CODE:           IF IDSV0003-SUCCESSFUL-RC
            //                         END-EVALUATE
            //                      ELSE
            //           *--> GESTIRE ERRORE
            //                         SET IDSV0003-INVALID-OPER   TO TRUE
            //                      END-IF
            if (idsv0003.getReturnCode().isSuccessfulRc()) {
                // COB_CODE:               EVALUATE TRUE
                //                             WHEN IDSV0003-NOT-FOUND
                //           *-->    NESSUN DATO IN TABELLA
                //           *-->     LIQUIDAZIONE CONTESTUALE - UTILIZZO L'AREA DEL
                //           *-->     BOLLO IN INPUT
                //                               SET FINE-CUR-MOV TO TRUE
                //                             WHEN IDSV0003-SUCCESSFUL-SQL
                //           *-->   OPERAZIONE ESEGUITA CORRETTAMENTE
                //           *                   MOVE IDSV0003-BUFFER-DATI TO MOVI
                //           *      TROVO IL MOVIMENTO DI COMUNICAZIONE RISCATTO PARZIALE
                //                               END-IF
                //                             WHEN OTHER
                //           *--->   ERRORE DI ACCESSO AL DB
                //                                  SET IDSV0003-INVALID-OPER   TO TRUE
                //                         END-EVALUATE
                switch (idsv0003.getSqlcode().getSqlcode()) {

                    case Idsv0003Sqlcode.NOT_FOUND://-->    NESSUN DATO IN TABELLA
                        //-->     LIQUIDAZIONE CONTESTUALE - UTILIZZO L'AREA DEL
                        //-->     BOLLO IN INPUT
                        // COB_CODE: SET FINE-CUR-MOV TO TRUE
                        ws.getFlagCurMov().setFineCurMov();
                        break;

                    case Idsv0003Sqlcode.SUCCESSFUL_SQL://-->   OPERAZIONE ESEGUITA CORRETTAMENTE
                        //                   MOVE IDSV0003-BUFFER-DATI TO MOVI
                        //      TROVO IL MOVIMENTO DI COMUNICAZIONE RISCATTO PARZIALE
                        // COB_CODE: MOVE MOV-TP-MOVI      TO WS-MOVIMENTO
                        ws.getWsMovimento().setWsMovimento(TruncAbs.toInt(ws.getMovi().getMovTpMovi().getMovTpMovi(), 5));
                        // COB_CODE: IF COMUN-RISPAR-IND
                        //           OR RISTO-INDIVI
                        //           OR VARIA-OPZION
                        //           OR SINIS-INDIVI
                        //           OR RECES-INDIVI
                        //           OR RPP-TAKE-PROFIT
                        //           OR COMUN-RISTOT-INCAPIENZA
                        //           OR RPP-REDDITO-PROGRAMMATO
                        //           OR RPP-BENEFICIO-CONTR
                        //           OR COMUN-RISTOT-INCAP
                        //              SET FINE-CUR-MOV   TO TRUE
                        //           ELSE
                        //              SET IDSV0003-FETCH-NEXT   TO TRUE
                        //           END-IF
                        if (ws.getWsMovimento().isComunRisparInd() || ws.getWsMovimento().isRistoIndivi() || ws.getWsMovimento().isVariaOpzion() || ws.getWsMovimento().isSinisIndivi() || ws.getWsMovimento().isRecesIndivi() || ws.getWsMovimento().isRppTakeProfit() || ws.getWsMovimento().isComunRistotIncapienza() || ws.getWsMovimento().isRppRedditoProgrammato() || ws.getWsMovimento().isRppBeneficioContr() || ws.getWsMovimento().isComunRistotIncap()) {
                            // COB_CODE: MOVE MOV-ID-MOVI    TO WK-ID-MOVI-COMUN
                            ws.getWkVarMoviComun().setIdMoviComun(ws.getMovi().getMovIdMovi());
                            // COB_CODE: SET SI-ULTIMA-LETTURA  TO TRUE
                            ws.getFlagUltimaLettura().setSiUltimaLettura();
                            // COB_CODE: MOVE MOV-DT-EFF     TO WK-DATA-EFF-PREC
                            ws.getWkVarMoviComun().setDataEffPrec(ws.getMovi().getMovDtEff());
                            // COB_CODE: COMPUTE WK-DATA-CPTZ-PREC =  MOV-DS-TS-CPTZ
                            //                                     - 1
                            ws.getWkVarMoviComun().setDataCptzPrec(Trunc.toLong(ws.getMovi().getMovDsTsCptz() - 1, 18));
                            // COB_CODE: SET COMUN-TROV-SI   TO TRUE
                            ws.getFlagComunTrov().setSi();
                            // COB_CODE: PERFORM CLOSE-MOVI
                            //              THRU CLOSE-MOVI-EX
                            closeMovi();
                            // COB_CODE: SET FINE-CUR-MOV   TO TRUE
                            ws.getFlagCurMov().setFineCurMov();
                        }
                        else {
                            // COB_CODE: SET IDSV0003-FETCH-NEXT   TO TRUE
                            idsv0003.getOperazione().setFetchNext();
                        }
                        break;

                    default://--->   ERRORE DI ACCESSO AL DB
                        // COB_CODE: MOVE LDBS6040
                        //             TO IDSV0003-COD-SERVIZIO-BE
                        idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbs6040());
                        // COB_CODE: MOVE 'CALL-LDBS6040 ERRORE CHIAMATA'
                        //             TO IDSV0003-DESCRIZ-ERR-DB2
                        idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBS6040 ERRORE CHIAMATA");
                        // COB_CODE: SET IDSV0003-INVALID-OPER   TO TRUE
                        idsv0003.getReturnCode().setInvalidOper();
                        break;
                }
            }
            else {
                //--> GESTIRE ERRORE
                // COB_CODE: MOVE LDBS6040
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbs6040());
                // COB_CODE: MOVE 'CALL-LDBS6040 ERRORE CHIAMATA'
                //             TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBS6040 ERRORE CHIAMATA");
                // COB_CODE: SET IDSV0003-INVALID-OPER   TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
        }
        // COB_CODE: MOVE IDSV0003-TIPO-MOVIMENTO TO WS-MOVIMENTO.
        ws.getWsMovimento().setWsMovimentoFormatted(idsv0003.getTipoMovimentoFormatted());
    }

    /**Original name: S1110-LETTURA-GARANZIA<br>
	 * <pre>----------------------------------------------------------------*
	 *     LETTURA DATI TABELLA GARANZIA
	 * ----------------------------------------------------------------*
	 * --> TIPO OPERAZIONE</pre>*/
    private void s1110LetturaGaranzia() {
        Ldbs1420 ldbs1420 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: SET IDSV0003-FETCH-FIRST       TO TRUE.
        idsv0003.getOperazione().setFetchFirst();
        //--> FLAG
        // COB_CODE: SET RAMO-III-V-NO              TO TRUE.
        ws.getFlagRamoIiiV().setNo();
        // COB_CODE: SET RAMO-I-NO                  TO TRUE.
        ws.getFlagRamoI().setNo();
        // COB_CODE: SET FINE-GAR-NO                TO TRUE.
        ws.getFineGar().setNo();
        //--> INIZIALIZZA CODICE DI RITORNO
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC     TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL    TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC
        //                      OR NOT IDSV0003-SUCCESSFUL-SQL
        //                      OR FINE-GAR-SI
        //              END-IF
        //           END-PERFORM.
        while (!(!idsv0003.getReturnCode().isSuccessfulRc() || !idsv0003.getSqlcode().isSuccessfulSql() || ws.getFineGar().isSi())) {
            // COB_CODE: INITIALIZE GAR
            initGar();
            // COB_CODE: MOVE DPOL-ID-POLI
            //             TO LDBV1421-ID-POLI
            ws.getLdbv1421().setIdPoli(ws.getLccvpol1().getDati().getWpolIdPoli());
            // COB_CODE: MOVE 5
            //             TO LDBV1421-RAMO-BILA-1
            ws.getLdbv1421().setRamoBila1("5");
            // COB_CODE: MOVE 3
            //             TO LDBV1421-RAMO-BILA-2
            ws.getLdbv1421().setRamoBila2("3");
            // COB_CODE: MOVE 1
            //             TO LDBV1421-RAMO-BILA-3
            ws.getLdbv1421().setRamoBila3("1");
            // COB_CODE: MOVE LDBV1421     TO IDSV0003-BUFFER-WHERE-COND
            idsv0003.setBufferWhereCond(ws.getLdbv1421().getLdbv1421Formatted());
            // COB_CODE: SET IDSV0003-TRATT-X-COMPETENZA TO TRUE
            idsv0003.getTrattamentoStoricita().setTrattXCompetenza();
            //--> LIVELLO OPERAZIONE
            // COB_CODE: SET IDSV0003-WHERE-CONDITION TO TRUE
            idsv0003.getLivelloOperazione().setWhereCondition();
            // COB_CODE: CALL LDBS1420 USING IDSV0003 GAR
            //           ON EXCEPTION
            //              SET IDSV0003-INVALID-OPER TO TRUE
            //           END-CALL
            try {
                ldbs1420 = Ldbs1420.getInstance();
                ldbs1420.run(idsv0003, ws.getGar());
            }
            catch (ProgramExecutionException __ex) {
                // COB_CODE: MOVE LDBS1420
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbs1420());
                // COB_CODE: MOVE 'ERRORE CALL LDBS1420 FETCH'
                //             TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("ERRORE CALL LDBS1420 FETCH");
                // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
            // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
            //              END-EVALUATE
            //           END-IF
            if (idsv0003.getReturnCode().isSuccessfulRc()) {
                // COB_CODE:            EVALUATE TRUE
                //                          WHEN IDSV0003-NOT-FOUND
                //           *-->    NESSUN DATO IN TABELLA
                //                               SET FINE-GAR-SI TO TRUE
                //                          WHEN IDSV0003-SUCCESSFUL-SQL
                //           *-->   OPERAZIONE ESEGUITA CORRETTAMENTE
                //           *-->   SE LA POLIZZA HA ALMENO UNA GARANZIA DI RAMO 5 O 3
                //           *-->   E' AMMESSO IL CALCOLO DEL BOLLO
                //                                TO TRUE
                //           *                   MOVE 1 TO IVVC0213-VAL-IMP-O
                //           *                   PERFORM S1130-CHIUSURA-CURS
                //           *                      THRU S1130-CHIUSURA-CURS-EX
                //                          WHEN OTHER
                //           *--->   ERRORE DI ACCESSO AL DB
                //                               SET FINE-GAR-SI TO TRUE
                //                      END-EVALUATE
                switch (idsv0003.getSqlcode().getSqlcode()) {

                    case Idsv0003Sqlcode.NOT_FOUND://-->    NESSUN DATO IN TABELLA
                        // COB_CODE: SET FINE-GAR-SI TO TRUE
                        ws.getFineGar().setSi();
                        break;

                    case Idsv0003Sqlcode.SUCCESSFUL_SQL://-->   OPERAZIONE ESEGUITA CORRETTAMENTE
                        //-->   SE LA POLIZZA HA ALMENO UNA GARANZIA DI RAMO 5 O 3         AD 1
                        //-->   E' AMMESSO IL CALCOLO DEL BOLLO                            AD 1
                        // COB_CODE: IF GRZ-RAMO-BILA = 3 OR 5
                        //              SET RAMO-III-V-SI TO TRUE
                        //           ELSE
                        //              END-IF
                        //           END-IF
                        if (Conditions.eq(ws.getGar().getGrzRamoBila(), "3") || Conditions.eq(ws.getGar().getGrzRamoBila(), "5")) {
                            // COB_CODE: SET RAMO-III-V-SI TO TRUE
                            ws.getFlagRamoIiiV().setSi();
                        }
                        else if (Conditions.eq(ws.getGar().getGrzRamoBila(), "1")) {
                            // COB_CODE: IF GRZ-RAMO-BILA = 1
                            //              SET RAMO-I-SI TO TRUE
                            //           END-IF
                            // COB_CODE: SET RAMO-I-SI TO TRUE
                            ws.getFlagRamoI().setSi();
                        }
                        // COB_CODE: SET IDSV0003-FETCH-NEXT
                        //            TO TRUE
                        idsv0003.getOperazione().setFetchNext();
                        //                   MOVE 1 TO IVVC0213-VAL-IMP-O
                        //                   PERFORM S1130-CHIUSURA-CURS
                        //                      THRU S1130-CHIUSURA-CURS-EX
                        break;

                    default://--->   ERRORE DI ACCESSO AL DB
                        // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
                        idsv0003.getReturnCode().setInvalidOper();
                        // COB_CODE: MOVE WK-PGM
                        //             TO IDSV0003-COD-SERVIZIO-BE
                        idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                        // COB_CODE: STRING 'ERRORE LETTURA TABELLA GARANZIA ;'
                        //                  IDSV0003-RETURN-CODE ';'
                        //                  IDSV0003-SQLCODE
                        //                  DELIMITED BY SIZE INTO
                        //                  IDSV0003-DESCRIZ-ERR-DB2
                        //           END-STRING
                        concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "ERRORE LETTURA TABELLA GARANZIA ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                        idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                        // COB_CODE: SET FINE-GAR-SI TO TRUE
                        ws.getFineGar().setSi();
                        break;
                }
            }
        }
    }

    /**Original name: S1120-VERIFICA-BOL-PRE<br>
	 * <pre>----------------------------------------------------------------*
	 *     LETTURA IMPOSTA DI BOLLO PER BOLLI PREGRESSI
	 * ----------------------------------------------------------------*</pre>*/
    private void s1120VerificaBolPre() {
        Idbsp580 idbsp580 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE IMPST-BOLLO
        initImpstBollo();
        // COB_CODE: MOVE DPOL-IB-OGG      TO P58-IB-POLI.
        ws.getImpstBollo().setP58IbPoli(ws.getLccvpol1().getDati().getWpolIbOgg());
        // COB_CODE: SET FINE-CICLO-NO     TO TRUE.
        ws.getFineCiclo().setNo();
        //  --> Tipo operazione
        // COB_CODE: SET IDSV0003-FETCH-FIRST        TO TRUE.
        idsv0003.getOperazione().setFetchFirst();
        //  --> Tipo livello
        // COB_CODE: SET IDSV0003-IB-SECONDARIO      TO TRUE.
        idsv0003.getLivelloOperazione().setIdsi0011IbSecondario();
        // COB_CODE: SET IDSV0003-TRATT-X-COMPETENZA TO TRUE.
        idsv0003.getTrattamentoStoricita().setTrattXCompetenza();
        // COB_CODE: PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC
        //                      OR FINE-CICLO-SI
        //                      OR IDSV0003-CLOSE-CURSOR
        //              END-IF
        //           END-PERFORM.
        while (!(!idsv0003.getReturnCode().isSuccessfulRc() || ws.getFineCiclo().isSi() || idsv0003.getOperazione().isCloseCursor())) {
            // COB_CODE: CALL IDBSP580    USING IDSV0003 IMPST-BOLLO
            //           ON EXCEPTION
            //              SET IDSV0003-INVALID-OPER   TO TRUE
            //           END-CALL
            try {
                idbsp580 = Idbsp580.getInstance();
                idbsp580.run(idsv0003, ws.getImpstBollo());
            }
            catch (ProgramExecutionException __ex) {
                // COB_CODE: MOVE IDBSP580               TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getIdbsp580());
                // COB_CODE: MOVE 'CALL-IDBSPOL0 ERRORE CHIAMATA'
                //             TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("CALL-IDBSPOL0 ERRORE CHIAMATA");
                // COB_CODE: SET IDSV0003-INVALID-OPER   TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
            // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
            //              END-EVALUATE
            //           END-IF
            if (idsv0003.getReturnCode().isSuccessfulRc()) {
                // COB_CODE:            EVALUATE TRUE
                //                          WHEN IDSV0003-NOT-FOUND
                //           *-->    NESSUN DATO IN TABELLA
                //                               CONTINUE
                //                          WHEN IDSV0003-SUCCESSFUL-SQL
                //           *-->   OPERAZIONE ESEGUITA CORRETTAMENTE
                //                               END-IF
                //                          WHEN OTHER
                //           *--->   ERRORE DI ACCESSO AL DB
                //                               END-STRING
                //                      END-EVALUATE
                switch (idsv0003.getSqlcode().getSqlcode()) {

                    case Idsv0003Sqlcode.NOT_FOUND://-->    NESSUN DATO IN TABELLA
                        // COB_CODE: SET FINE-CICLO-SI TO TRUE
                        ws.getFineCiclo().setSi();
                        // COB_CODE: CONTINUE
                        //continue
                        break;

                    case Idsv0003Sqlcode.SUCCESSFUL_SQL://-->   OPERAZIONE ESEGUITA CORRETTAMENTE
                        // COB_CODE: IF P58-TP-CAUS-BOLLO = 'AN' OR 'SW'
                        //                 THRU S1140-CHIUSURA-CURS-EX
                        //           ELSE
                        //              SET IDSV0003-FETCH-NEXT TO TRUE
                        //           END-IF
                        if (Conditions.eq(ws.getImpstBollo().getP58TpCausBollo(), "AN") || Conditions.eq(ws.getImpstBollo().getP58TpCausBollo(), "SW")) {
                            // COB_CODE: MOVE 1 TO IVVC0213-VAL-IMP-O
                            ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(1, 18, 7));
                            // COB_CODE: PERFORM S1140-CHIUSURA-CURS
                            //              THRU S1140-CHIUSURA-CURS-EX
                            s1140ChiusuraCurs();
                        }
                        else {
                            // COB_CODE: SET IDSV0003-FETCH-NEXT TO TRUE
                            idsv0003.getOperazione().setFetchNext();
                        }
                        break;

                    default://--->   ERRORE DI ACCESSO AL DB
                        // COB_CODE: SET FINE-CICLO-SI TO TRUE
                        ws.getFineCiclo().setSi();
                        // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
                        idsv0003.getReturnCode().setInvalidOper();
                        // COB_CODE: MOVE IDBSP580
                        //             TO IDSV0003-COD-SERVIZIO-BE
                        idsv0003.getCampiEsito().setCodServizioBe(ws.getIdbsp580());
                        // COB_CODE: STRING 'ERRORE LETTURA TABELLA IMPST-BOL;'
                        //                  IDSV0003-RETURN-CODE ';'
                        //                  IDSV0003-SQLCODE
                        //                  DELIMITED BY SIZE INTO
                        //                  IDSV0003-DESCRIZ-ERR-DB2
                        //           END-STRING
                        concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "ERRORE LETTURA TABELLA IMPST-BOL;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                        idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                        break;
                }
            }
        }
    }

    /**Original name: S1140-CHIUSURA-CURS<br>
	 * <pre>----------------------------------------------------------------*
	 *    CHIUSURA CURSORE IMPOSTA DI BOLLO
	 * ----------------------------------------------------------------*</pre>*/
    private void s1140ChiusuraCurs() {
        Idbsp580 idbsp580 = null;
        // COB_CODE: SET IDSV0003-CLOSE-CURSOR            TO TRUE.
        idsv0003.getOperazione().setIdsv0003CloseCursor();
        // COB_CODE: CALL IDBSP580 USING IDSV0003 IMPST-BOLLO
        //           ON EXCEPTION
        //              SET IDSV0003-INVALID-OPER TO TRUE
        //           END-CALL.
        try {
            idbsp580 = Idbsp580.getInstance();
            idbsp580.run(idsv0003, ws.getImpstBollo());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE LDBS1420
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbs1420());
            // COB_CODE: MOVE 'ERRORE CALL IDBSP580 CLOSE'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("ERRORE CALL IDBSP580 CLOSE");
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE: IF NOT IDSV0003-SUCCESSFUL-RC
        //           OR NOT IDSV0003-SUCCESSFUL-SQL
        //                TO TRUE
        //           END-IF.
        if (!idsv0003.getReturnCode().isSuccessfulRc() || !idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: MOVE WK-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'ERRORE CLOSE CURSORE IMPST-BOLLO'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("ERRORE CLOSE CURSORE IMPST-BOLLO");
            // COB_CODE: SET IDSV0003-INVALID-OPER
            //            TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE: SET FINE-CICLO-SI TO TRUE.
        ws.getFineCiclo().setSi();
    }

    /**Original name: S1100-VALORIZZA-DCLGEN<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 *     RISPETTIVE AREE DCLGEN IN WORKING
	 * ----------------------------------------------------------------*</pre>*/
    private void s1100ValorizzaDclgen() {
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-POLI
        //                TO DPOL-AREA-POLI
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias(), ws.getIvvc0218().getAliasPoli())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO DPOL-AREA-POLI
            ws.setDpolAreaPoliFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxDclgen()).getLunghezza() - 1));
        }
    }

    /**Original name: CLOSE-MOVI<br>
	 * <pre>----------------------------------------------------------------*
	 *     CHIUDO IL CURSORE SULLA TABELLA MOVIMENTO LDBS6040
	 * ----------------------------------------------------------------*</pre>*/
    private void closeMovi() {
        Ldbs6040 ldbs6040 = null;
        // COB_CODE: SET IDSV0003-CLOSE-CURSOR            TO TRUE.
        idsv0003.getOperazione().setIdsv0003CloseCursor();
        // COB_CODE: CALL LDBS6040 USING IDSV0003 MOVI
        //           ON EXCEPTION
        //              SET IDSV0003-INVALID-OPER TO TRUE
        //           END-CALL.
        try {
            ldbs6040 = Ldbs6040.getInstance();
            ldbs6040.run(idsv0003, ws.getMovi());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE LDBS6040
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbs6040());
            // COB_CODE: MOVE 'ERRORE CALL LDBS6040 CLOSE'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("ERRORE CALL LDBS6040 CLOSE");
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE: IF NOT IDSV0003-SUCCESSFUL-RC
        //           OR NOT IDSV0003-SUCCESSFUL-SQL
        //                TO TRUE
        //           END-IF.
        if (!idsv0003.getReturnCode().isSuccessfulRc() || !idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: MOVE WK-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'ERRORE CLOSE CURSORE LDBS6040'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("ERRORE CLOSE CURSORE LDBS6040");
            // COB_CODE: SET IDSV0003-INVALID-OPER
            //            TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: MOVE SPACES                     TO IVVC0213-VAL-STR-O.
        ivvc0213.getTabOutput().setValStrO("");
        // COB_CODE: MOVE 0                          TO IVVC0213-VAL-PERC-O.
        ivvc0213.getTabOutput().setValPercO(Trunc.toDecimal(0, 14, 9));
        //    IF LIQUI-RISTOT-IND OR LIQUI-RISPAR-POLIND OR
        //       LIQUI-SCAPOL OR LIQUI-SININD OR LIQUI-RECIND
        // COB_CODE: IF COMUN-TROV-SI
        //                   IDSV0003-DATA-FINE-EFFETTO
        //           END-IF
        if (ws.getFlagComunTrov().isSi()) {
            // COB_CODE: MOVE WK-DATA-CPTZ-RIP
            //             TO IDSV0003-DATA-COMPETENZA
            idsv0003.setDataCompetenza(ws.getWkVarMoviComun().getDataCptzRip());
            // COB_CODE: MOVE WK-DATA-EFF-RIP
            //             TO IDSV0003-DATA-INIZIO-EFFETTO
            //                IDSV0003-DATA-FINE-EFFETTO
            idsv0003.setDataInizioEffetto(ws.getWkVarMoviComun().getDataEffRip());
            idsv0003.setDataFineEffetto(ws.getWkVarMoviComun().getDataEffRip());
        }
        //
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    public void initIxIndici() {
        ws.setIxDclgen(((short)0));
        ws.setIxTabIso(((short)0));
    }

    public void initTabOutput() {
        ivvc0213.getTabOutput().setCodVariabileO("");
        ivvc0213.getTabOutput().setTpDatoO(Types.SPACE_CHAR);
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        ivvc0213.getTabOutput().setValPercO(new AfDecimal(0, 14, 9));
        ivvc0213.getTabOutput().setValStrO("");
    }

    public void initWkVarMoviComun() {
        ws.getWkVarMoviComun().setIdMoviComun(0);
        ws.getWkVarMoviComun().setDataEffPrec(0);
        ws.getWkVarMoviComun().setDataCptzPrec(0);
        ws.getWkVarMoviComun().setDataEffRip(0);
        ws.getWkVarMoviComun().setDataCptzRip(0);
    }

    public void initAreaIoPoli() {
        ws.setDpolElePoliMax(((short)0));
        ws.getLccvpol1().getStatus().setStatus(Types.SPACE_CHAR);
        ws.getLccvpol1().setIdPtf(0);
        ws.getLccvpol1().getDati().setWpolIdPoli(0);
        ws.getLccvpol1().getDati().setWpolIdMoviCrz(0);
        ws.getLccvpol1().getDati().getWpolIdMoviChiu().setWpolIdMoviChiu(0);
        ws.getLccvpol1().getDati().setWpolIbOgg("");
        ws.getLccvpol1().getDati().setWpolIbProp("");
        ws.getLccvpol1().getDati().getWpolDtProp().setWpolDtProp(0);
        ws.getLccvpol1().getDati().setWpolDtIniEff(0);
        ws.getLccvpol1().getDati().setWpolDtEndEff(0);
        ws.getLccvpol1().getDati().setWpolCodCompAnia(0);
        ws.getLccvpol1().getDati().setWpolDtDecor(0);
        ws.getLccvpol1().getDati().setWpolDtEmis(0);
        ws.getLccvpol1().getDati().setWpolTpPoli("");
        ws.getLccvpol1().getDati().getWpolDurAa().setWpolDurAa(0);
        ws.getLccvpol1().getDati().getWpolDurMm().setWpolDurMm(0);
        ws.getLccvpol1().getDati().getWpolDtScad().setWpolDtScad(0);
        ws.getLccvpol1().getDati().setWpolCodProd("");
        ws.getLccvpol1().getDati().setWpolDtIniVldtProd(0);
        ws.getLccvpol1().getDati().setWpolCodConv("");
        ws.getLccvpol1().getDati().setWpolCodRamo("");
        ws.getLccvpol1().getDati().getWpolDtIniVldtConv().setWpolDtIniVldtConv(0);
        ws.getLccvpol1().getDati().getWpolDtApplzConv().setWpolDtApplzConv(0);
        ws.getLccvpol1().getDati().setWpolTpFrmAssva("");
        ws.getLccvpol1().getDati().setWpolTpRgmFisc("");
        ws.getLccvpol1().getDati().setWpolFlEstas(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlRshComun(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlRshComunCond(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolTpLivGenzTit("");
        ws.getLccvpol1().getDati().setWpolFlCopFinanz(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolTpApplzDir("");
        ws.getLccvpol1().getDati().getWpolSpeMed().setWpolSpeMed(new AfDecimal(0, 15, 3));
        ws.getLccvpol1().getDati().getWpolDirEmis().setWpolDirEmis(new AfDecimal(0, 15, 3));
        ws.getLccvpol1().getDati().getWpolDir1oVers().setWpolDir1oVers(new AfDecimal(0, 15, 3));
        ws.getLccvpol1().getDati().getWpolDirVersAgg().setWpolDirVersAgg(new AfDecimal(0, 15, 3));
        ws.getLccvpol1().getDati().setWpolCodDvs("");
        ws.getLccvpol1().getDati().setWpolFlFntAz(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlFntAder(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlFntTfr(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlFntVolo(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolTpOpzAScad("");
        ws.getLccvpol1().getDati().getWpolAaDiffProrDflt().setWpolAaDiffProrDflt(0);
        ws.getLccvpol1().getDati().setWpolFlVerProd("");
        ws.getLccvpol1().getDati().getWpolDurGg().setWpolDurGg(0);
        ws.getLccvpol1().getDati().getWpolDirQuiet().setWpolDirQuiet(new AfDecimal(0, 15, 3));
        ws.getLccvpol1().getDati().setWpolTpPtfEstno("");
        ws.getLccvpol1().getDati().setWpolFlCumPreCntr(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlAmmbMovi(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolConvGeco("");
        ws.getLccvpol1().getDati().setWpolDsRiga(0);
        ws.getLccvpol1().getDati().setWpolDsOperSql(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolDsVer(0);
        ws.getLccvpol1().getDati().setWpolDsTsIniCptz(0);
        ws.getLccvpol1().getDati().setWpolDsTsEndCptz(0);
        ws.getLccvpol1().getDati().setWpolDsUtente("");
        ws.getLccvpol1().getDati().setWpolDsStatoElab(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlScudoFisc(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlTrasfe(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlTfrStrc(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().getWpolDtPresc().setWpolDtPresc(0);
        ws.getLccvpol1().getDati().setWpolCodConvAgg("");
        ws.getLccvpol1().getDati().setWpolSubcatProd("");
        ws.getLccvpol1().getDati().setWpolFlQuestAdegzAss(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolCodTpa("");
        ws.getLccvpol1().getDati().getWpolIdAccComm().setWpolIdAccComm(0);
        ws.getLccvpol1().getDati().setWpolFlPoliCpiPr(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlPoliBundling(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolIndPoliPrinColl(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlVndBundle(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolIbBs("");
        ws.getLccvpol1().getDati().setWpolFlPoliIfp(Types.SPACE_CHAR);
    }

    public void initMovi() {
        ws.getMovi().setMovIdMovi(0);
        ws.getMovi().setMovCodCompAnia(0);
        ws.getMovi().getMovIdOgg().setMovIdOgg(0);
        ws.getMovi().setMovIbOgg("");
        ws.getMovi().setMovIbMovi("");
        ws.getMovi().setMovTpOgg("");
        ws.getMovi().getMovIdRich().setMovIdRich(0);
        ws.getMovi().getMovTpMovi().setMovTpMovi(0);
        ws.getMovi().setMovDtEff(0);
        ws.getMovi().getMovIdMoviAnn().setMovIdMoviAnn(0);
        ws.getMovi().getMovIdMoviCollg().setMovIdMoviCollg(0);
        ws.getMovi().setMovDsOperSql(Types.SPACE_CHAR);
        ws.getMovi().setMovDsVer(0);
        ws.getMovi().setMovDsTsCptz(0);
        ws.getMovi().setMovDsUtente("");
        ws.getMovi().setMovDsStatoElab(Types.SPACE_CHAR);
    }

    public void initGar() {
        ws.getGar().setGrzIdGar(0);
        ws.getGar().getGrzIdAdes().setGrzIdAdes(0);
        ws.getGar().setGrzIdPoli(0);
        ws.getGar().setGrzIdMoviCrz(0);
        ws.getGar().getGrzIdMoviChiu().setGrzIdMoviChiu(0);
        ws.getGar().setGrzDtIniEff(0);
        ws.getGar().setGrzDtEndEff(0);
        ws.getGar().setGrzCodCompAnia(0);
        ws.getGar().setGrzIbOgg("");
        ws.getGar().getGrzDtDecor().setGrzDtDecor(0);
        ws.getGar().getGrzDtScad().setGrzDtScad(0);
        ws.getGar().setGrzCodSez("");
        ws.getGar().setGrzCodTari("");
        ws.getGar().setGrzRamoBila("");
        ws.getGar().getGrzDtIniValTar().setGrzDtIniValTar(0);
        ws.getGar().getGrzId1oAssto().setGrzId1oAssto(0);
        ws.getGar().getGrzId2oAssto().setGrzId2oAssto(0);
        ws.getGar().getGrzId3oAssto().setGrzId3oAssto(0);
        ws.getGar().getGrzTpGar().setGrzTpGar(((short)0));
        ws.getGar().setGrzTpRsh("");
        ws.getGar().getGrzTpInvst().setGrzTpInvst(((short)0));
        ws.getGar().setGrzModPagGarcol("");
        ws.getGar().setGrzTpPerPre("");
        ws.getGar().getGrzEtaAa1oAssto().setGrzEtaAa1oAssto(((short)0));
        ws.getGar().getGrzEtaMm1oAssto().setGrzEtaMm1oAssto(((short)0));
        ws.getGar().getGrzEtaAa2oAssto().setGrzEtaAa2oAssto(((short)0));
        ws.getGar().getGrzEtaMm2oAssto().setGrzEtaMm2oAssto(((short)0));
        ws.getGar().getGrzEtaAa3oAssto().setGrzEtaAa3oAssto(((short)0));
        ws.getGar().getGrzEtaMm3oAssto().setGrzEtaMm3oAssto(((short)0));
        ws.getGar().setGrzTpEmisPur(Types.SPACE_CHAR);
        ws.getGar().getGrzEtaAScad().setGrzEtaAScad(0);
        ws.getGar().setGrzTpCalcPrePrstz("");
        ws.getGar().setGrzTpPre(Types.SPACE_CHAR);
        ws.getGar().setGrzTpDur("");
        ws.getGar().getGrzDurAa().setGrzDurAa(0);
        ws.getGar().getGrzDurMm().setGrzDurMm(0);
        ws.getGar().getGrzDurGg().setGrzDurGg(0);
        ws.getGar().getGrzNumAaPagPre().setGrzNumAaPagPre(0);
        ws.getGar().getGrzAaPagPreUni().setGrzAaPagPreUni(0);
        ws.getGar().getGrzMmPagPreUni().setGrzMmPagPreUni(0);
        ws.getGar().getGrzFrazIniErogRen().setGrzFrazIniErogRen(0);
        ws.getGar().getGrzMm1oRat().setGrzMm1oRat(((short)0));
        ws.getGar().getGrzPc1oRat().setGrzPc1oRat(new AfDecimal(0, 6, 3));
        ws.getGar().setGrzTpPrstzAssta("");
        ws.getGar().getGrzDtEndCarz().setGrzDtEndCarz(0);
        ws.getGar().getGrzPcRipPre().setGrzPcRipPre(new AfDecimal(0, 6, 3));
        ws.getGar().setGrzCodFnd("");
        ws.getGar().setGrzAaRenCer("");
        ws.getGar().getGrzPcRevrsb().setGrzPcRevrsb(new AfDecimal(0, 6, 3));
        ws.getGar().setGrzTpPcRip("");
        ws.getGar().getGrzPcOpz().setGrzPcOpz(new AfDecimal(0, 6, 3));
        ws.getGar().setGrzTpIas("");
        ws.getGar().setGrzTpStab("");
        ws.getGar().setGrzTpAdegPre(Types.SPACE_CHAR);
        ws.getGar().getGrzDtVarzTpIas().setGrzDtVarzTpIas(0);
        ws.getGar().getGrzFrazDecrCpt().setGrzFrazDecrCpt(0);
        ws.getGar().setGrzCodTratRiass("");
        ws.getGar().setGrzTpDtEmisRiass("");
        ws.getGar().setGrzTpCessRiass("");
        ws.getGar().setGrzDsRiga(0);
        ws.getGar().setGrzDsOperSql(Types.SPACE_CHAR);
        ws.getGar().setGrzDsVer(0);
        ws.getGar().setGrzDsTsIniCptz(0);
        ws.getGar().setGrzDsTsEndCptz(0);
        ws.getGar().setGrzDsUtente("");
        ws.getGar().setGrzDsStatoElab(Types.SPACE_CHAR);
        ws.getGar().getGrzAaStab().setGrzAaStab(0);
        ws.getGar().getGrzTsStabLimitata().setGrzTsStabLimitata(new AfDecimal(0, 14, 9));
        ws.getGar().getGrzDtPresc().setGrzDtPresc(0);
        ws.getGar().setGrzRshInvst(Types.SPACE_CHAR);
        ws.getGar().setGrzTpRamoBila("");
    }

    public void initImpstBollo() {
        ws.getImpstBollo().setP58IdImpstBollo(0);
        ws.getImpstBollo().setP58CodCompAnia(0);
        ws.getImpstBollo().setP58IdPoli(0);
        ws.getImpstBollo().setP58IbPoli("");
        ws.getImpstBollo().setP58CodFisc("");
        ws.getImpstBollo().setP58CodPartIva("");
        ws.getImpstBollo().setP58IdRappAna(0);
        ws.getImpstBollo().setP58IdMoviCrz(0);
        ws.getImpstBollo().getP58IdMoviChiu().setP58IdMoviChiu(0);
        ws.getImpstBollo().setP58DtIniEff(0);
        ws.getImpstBollo().setP58DtEndEff(0);
        ws.getImpstBollo().setP58DtIniCalc(0);
        ws.getImpstBollo().setP58DtEndCalc(0);
        ws.getImpstBollo().setP58TpCausBollo("");
        ws.getImpstBollo().setP58ImpstBolloDettC(new AfDecimal(0, 15, 3));
        ws.getImpstBollo().getP58ImpstBolloDettV().setP58ImpstBolloDettV(new AfDecimal(0, 15, 3));
        ws.getImpstBollo().getP58ImpstBolloTotV().setP58ImpstBolloTotV(new AfDecimal(0, 15, 3));
        ws.getImpstBollo().setP58ImpstBolloTotR(new AfDecimal(0, 15, 3));
        ws.getImpstBollo().setP58DsRiga(0);
        ws.getImpstBollo().setP58DsOperSql(Types.SPACE_CHAR);
        ws.getImpstBollo().setP58DsVer(0);
        ws.getImpstBollo().setP58DsTsIniCptz(0);
        ws.getImpstBollo().setP58DsTsEndCptz(0);
        ws.getImpstBollo().setP58DsUtente("");
        ws.getImpstBollo().setP58DsStatoElab(Types.SPACE_CHAR);
    }
}
