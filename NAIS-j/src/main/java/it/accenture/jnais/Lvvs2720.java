package it.accenture.jnais;

import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Idsv0003CampiEsito;
import it.accenture.jnais.copy.Wp58Dati;
import it.accenture.jnais.ws.enums.Idsv0003Sqlcode;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.Lvvs2720Data;
import it.accenture.jnais.ws.redefines.Wp58IdMoviChiu;
import it.accenture.jnais.ws.redefines.Wp58ImpstBolloDettV;
import it.accenture.jnais.ws.redefines.Wp58ImpstBolloTotV;
import static java.lang.Math.abs;

/**Original name: LVVS2720<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2013.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *   PROGRAMMA...... LVVS2720
 *   TIPOLOGIA...... SERVIZIO
 *   PROCESSO....... XXX
 *   FUNZIONE....... XXX
 *   DESCRIZIONE.... IMPOSTA DI BOLLO
 *                   CALCOLO DELLA VARIABILE IMPPAGANNLIQ
 * **------------------------------------------------------------***</pre>*/
public class Lvvs2720 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lvvs2720Data ws = new Lvvs2720Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS2720
    private Ivvc0213 ivvc0213;

    //==== METHODS ====
    /**Original name: PROGRAM_LVVS2720_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: PERFORM S1000-ELABORAZIONE
        //              THRU EX-S1000
        s1000Elaborazione();
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lvvs2720 getInstance() {
        return ((Lvvs2720)Programs.getInstance(Lvvs2720.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                        IX-INDICI
        //                                             IVVC0213-TAB-OUTPUT.
        initIxIndici();
        initTabOutput();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET COMUN-TROV-NO                 TO TRUE.
        ws.getFlagComunTrov().setNo();
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE
        //             TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: INITIALIZE AREA-IO-P58
        //                      AREA-IO-POLI.
        initAreaIoP58();
        initAreaIoPoli();
        // COB_CODE: PERFORM VERIFICA-MOVIMENTO
        //              THRU VERIFICA-MOVIMENTO-EX
        verificaMovimento();
        //--> PERFORM DI CALCOLO ANNO CORRENTE
        // COB_CODE:      IF  IDSV0003-SUCCESSFUL-RC
        //           *    AND IDSV0003-SUCCESSFUL-SQL
        //                       THRU S1150-CALCOLO-ANNO-EX
        //                END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            //    AND IDSV0003-SUCCESSFUL-SQL
            // COB_CODE: PERFORM S1150-CALCOLO-ANNO
            //              THRU S1150-CALCOLO-ANNO-EX
            s1150CalcoloAnno();
        }
        //--> PERFORM DI SOMMATORIA
        // COB_CODE:      IF  IDSV0003-SUCCESSFUL-RC
        //           *    AND IDSV0003-SUCCESSFUL-SQL
        //                    END-IF
        //                END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            //    AND IDSV0003-SUCCESSFUL-SQL
            // COB_CODE: IF DP58-ELE-P58-MAX > 0
            //                 THRU S1300-CALCOLO-SOMMA-EX
            //           ELSE
            //              END-IF
            //           END-IF
            if (ws.getDp58EleP58Max() > 0) {
                // COB_CODE: PERFORM S1300-CALCOLO-SOMMA
                //              THRU S1300-CALCOLO-SOMMA-EX
                s1300CalcoloSomma();
            }
            else {
                // COB_CODE: PERFORM S1350-RECUPERO-INFO
                //              THRU S1350-RECUPERO-INFO-EX
                s1350RecuperoInfo();
                // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
                //           AND IDSV0003-SUCCESSFUL-SQL
                //                  THRU S1390-CALCOLO-SOMMA-2-EX
                //           END-IF
                if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
                    // COB_CODE: PERFORM S1390-CALCOLO-SOMMA-2
                    //              THRU S1390-CALCOLO-SOMMA-2-EX
                    s1390CalcoloSomma2();
                }
            }
        }
    }

    /**Original name: VERIFICA-MOVIMENTO<br>
	 * <pre>----------------------------------------------------------------*
	 *     VERIFICA MOVIMENTO
	 * ----------------------------------------------------------------*
	 * --> SE CI TROVIAMO IN FASE DI LIQUIDAZIONE DEVO RECUPERARE LE
	 * --> IMMAGINI IN FASE DI COMUNICAZIONE
	 *     MOVE IVVC0213-TIPO-MOVIMENTO
	 *       TO WS-MOVIMENTO</pre>*/
    private void verificaMovimento() {
        // COB_CODE: MOVE IVVC0213-TIPO-MOVI-ORIG
        //             TO WS-MOVIMENTO
        ws.getWsMovimento().setWsMovimentoFormatted(ivvc0213.getTipoMoviOrigFormatted());
        // COB_CODE:      IF LIQUI-RISTOT-IND OR LIQUI-RISPAR-POLIND OR
        //                   LIQUI-SCAPOL OR LIQUI-SININD OR LIQUI-RECIND OR
        //                   LIQUI-RPP-TAKE-PROFIT
        //                OR LIQUI-RISTOT-INCAPIENZA
        //                OR LIQUI-RPP-REDDITO-PROGR
        //                OR LIQUI-RPP-BENEFICIO-CONTR
        //                OR LIQUI-RISTOT-INCAP
        //           *--> RECUPERO IL MOVIMENTO DI COMUNICAZIONE
        //                   END-IF
        //                ELSE
        //           *-->    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
        //           *-->    RISPETTIVE AREE DCLGEN IN WORKING
        //                           SPACES OR LOW-VALUE OR HIGH-VALUE
        //                END-IF.
        if (ws.getWsMovimento().isLiquiRistotInd() || ws.getWsMovimento().isLiquiRisparPolind() || ws.getWsMovimento().isLiquiScapol() || ws.getWsMovimento().isLiquiSinind() || ws.getWsMovimento().isLiquiRecind() || ws.getWsMovimento().isLiquiRppTakeProfit() || ws.getWsMovimento().isLiquiRistotIncapienza() || ws.getWsMovimento().isLiquiRppRedditoProgr() || ws.getWsMovimento().isLiquiRppBeneficioContr() || ws.getWsMovimento().isLiquiRistotIncap()) {
            //--> RECUPERO IL MOVIMENTO DI COMUNICAZIONE
            // COB_CODE: PERFORM RECUP-MOVI-COMUN
            //              THRU RECUP-MOVI-COMUN-EX
            recupMoviComun();
            // COB_CODE:         IF COMUN-TROV-SI
            //                         THRU LETTURA-IMPST-BOLLO-EX
            //                   ELSE
            //           *-->       ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
            //           *-->       RISPETTIVE AREE DCLGEN IN WORKING
            //                              SPACES OR LOW-VALUE OR HIGH-VALUE
            //                   END-IF
            if (ws.getFlagComunTrov().isSi()) {
                // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA
                //             TO WK-DATA-CPTZ-RIP
                ws.getWkVarMoviComun().setDataCptzRip(idsv0003.getDataCompetenza());
                // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
                //             TO WK-DATA-EFF-RIP
                ws.getWkVarMoviComun().setDataEffRip(idsv0003.getDataInizioEffetto());
                // COB_CODE: MOVE WK-DATA-CPTZ-PREC
                //             TO IDSV0003-DATA-COMPETENZA
                idsv0003.setDataCompetenza(ws.getWkVarMoviComun().getDataCptzPrec());
                // COB_CODE: MOVE WK-DATA-EFF-PREC
                //             TO IDSV0003-DATA-INIZIO-EFFETTO
                //                IDSV0003-DATA-FINE-EFFETTO
                idsv0003.setDataInizioEffetto(ws.getWkVarMoviComun().getDataEffPrec());
                idsv0003.setDataFineEffetto(ws.getWkVarMoviComun().getDataEffPrec());
                //             UNTIL IX-TAB-P58 > 10
                // COB_CODE:            PERFORM VARYING IX-TAB-P58 FROM 1 BY 1
                //           *             UNTIL IX-TAB-P58 > 10
                //                         UNTIL IX-TAB-P58 > 75
                //                            THRU INIZIA-TOT-P58-EX
                //                      END-PERFORM
                ws.getIxIndici().setTabP58(1);
                while (!(ws.getIxIndici().getTabP58() > 75)) {
                    // COB_CODE: PERFORM INIZIA-TOT-P58
                    //              THRU INIZIA-TOT-P58-EX
                    iniziaTotP58();
                    ws.getIxIndici().setTabP58(Trunc.toInt(ws.getIxIndici().getTabP58() + 1, 9));
                }
                // COB_CODE: PERFORM LETTURA-IMPST-BOLLO
                //              THRU LETTURA-IMPST-BOLLO-EX
                letturaImpstBollo();
            }
            else {
                //-->       ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
                //-->       RISPETTIVE AREE DCLGEN IN WORKING
                // COB_CODE: PERFORM S1100-VALORIZZA-DCLGEN
                //              THRU S1100-VALORIZZA-DCLGEN-EX
                //           VARYING IX-DCLGEN FROM 1 BY 1
                //             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
                //                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
                //                   SPACES OR LOW-VALUE OR HIGH-VALUE
                ws.getIxIndici().setDclgen(((short)1));
                while (!(ws.getIxIndici().getDclgen() > ivvc0213.getEleInfoMax() || Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getTabAlias()) || Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getIvvc0213TabAliasFormatted()) || Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getIvvc0213TabAliasFormatted()))) {
                    s1100ValorizzaDclgen();
                    ws.getIxIndici().setDclgen(Trunc.toShort(ws.getIxIndici().getDclgen() + 1, 4));
                }
            }
        }
        else {
            //-->    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
            //-->    RISPETTIVE AREE DCLGEN IN WORKING
            // COB_CODE: PERFORM S1100-VALORIZZA-DCLGEN
            //              THRU S1100-VALORIZZA-DCLGEN-EX
            //           VARYING IX-DCLGEN FROM 1 BY 1
            //             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
            //                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
            //                   SPACES OR LOW-VALUE OR HIGH-VALUE
            ws.getIxIndici().setDclgen(((short)1));
            while (!(ws.getIxIndici().getDclgen() > ivvc0213.getEleInfoMax() || Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getTabAlias()) || Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getIvvc0213TabAliasFormatted()) || Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getIvvc0213TabAliasFormatted()))) {
                s1100ValorizzaDclgen();
                ws.getIxIndici().setDclgen(Trunc.toShort(ws.getIxIndici().getDclgen() + 1, 4));
            }
        }
    }

    /**Original name: RECUP-MOVI-COMUN<br>
	 * <pre>----------------------------------------------------------------*
	 *     Recupero il movimento di comunicazione
	 * ----------------------------------------------------------------*</pre>*/
    private void recupMoviComun() {
        Ldbs6040 ldbs6040 = null;
        // COB_CODE: SET INIT-CUR-MOV               TO TRUE
        ws.getFlagCurMov().setInitCurMov();
        // COB_CODE: SET COMUN-TROV-NO              TO TRUE
        ws.getFlagComunTrov().setNo();
        // COB_CODE: INITIALIZE MOVI
        initMovi();
        //--> TIPO OPERAZIONE
        // COB_CODE: SET IDSV0003-FETCH-FIRST       TO TRUE
        idsv0003.getOperazione().setFetchFirst();
        //--> INIZIALIZZA CODICE DI RITORNO
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC    TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET  NO-ULTIMA-LETTURA         TO TRUE
        ws.getFlagUltimaLettura().setNoUltimaLettura();
        //
        // COB_CODE: PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC
        //                      OR FINE-CUR-MOV
        //                      OR COMUN-TROV-SI
        //                 END-IF
        //           END-PERFORM.
        while (!(!idsv0003.getReturnCode().isSuccessfulRc() || ws.getFlagCurMov().isFineCurMov() || ws.getFlagComunTrov().isSi())) {
            // COB_CODE: INITIALIZE MOVI
            initMovi();
            //
            // COB_CODE: MOVE IVVC0213-ID-POLIZZA    TO MOV-ID-OGG
            ws.getMovi().getMovIdOgg().setMovIdOgg(ivvc0213.getIdPolizza());
            // COB_CODE: MOVE 'PO'                   TO MOV-TP-OGG
            ws.getMovi().setMovTpOgg("PO");
            // COB_CODE: SET IDSV0003-TRATT-SENZA-STOR   TO TRUE
            idsv0003.getTrattamentoStoricita().setTrattSenzaStor();
            //--> LIVELLO OPERAZIONE
            // COB_CODE: SET IDSV0003-WHERE-CONDITION TO TRUE
            idsv0003.getLivelloOperazione().setWhereCondition();
            //--> INIZIALIZZA CODICE DI RITORNO
            // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC  TO TRUE
            idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
            // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL TO TRUE
            idsv0003.getSqlcode().setSuccessfulSql();
            // COB_CODE: CALL  LDBS6040   USING IDSV0003 MOVI
            //             ON EXCEPTION
            //                SET IDSV0003-INVALID-OPER   TO TRUE
            //           END-CALL
            try {
                ldbs6040 = Ldbs6040.getInstance();
                ldbs6040.run(idsv0003, ws.getMovi());
            }
            catch (ProgramExecutionException __ex) {
                // COB_CODE: MOVE LDBS6040          TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbs6040());
                // COB_CODE: MOVE 'CALL-LDBS6040 ERRORE CHIAMATA'
                //             TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBS6040 ERRORE CHIAMATA");
                // COB_CODE: SET IDSV0003-INVALID-OPER   TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
            // COB_CODE:           IF IDSV0003-SUCCESSFUL-RC
            //                         END-EVALUATE
            //                      ELSE
            //           *--> GESTIRE ERRORE
            //                         SET IDSV0003-INVALID-OPER   TO TRUE
            //                      END-IF
            if (idsv0003.getReturnCode().isSuccessfulRc()) {
                // COB_CODE:               EVALUATE TRUE
                //                             WHEN IDSV0003-NOT-FOUND
                //           *-->    NESSUN DATO IN TABELLA
                //           *-->     LIQUIDAZIONE CONTESTUALE - UTILIZZO L'AREA DEL
                //           *-->     BOLLO IN INPUT
                //                               SET FINE-CUR-MOV TO TRUE
                //                             WHEN IDSV0003-SUCCESSFUL-SQL
                //           *-->   OPERAZIONE ESEGUITA CORRETTAMENTE
                //           *                   MOVE IDSV0003-BUFFER-DATI TO MOVI
                //           *      TROVO IL MOVIMENTO DI COMUNICAZIONE RISCATTO PARZIALE
                //                               END-IF
                //                             WHEN OTHER
                //           *--->   ERRORE DI ACCESSO AL DB
                //                                  SET IDSV0003-INVALID-OPER   TO TRUE
                //                         END-EVALUATE
                switch (idsv0003.getSqlcode().getSqlcode()) {

                    case Idsv0003Sqlcode.NOT_FOUND://-->    NESSUN DATO IN TABELLA
                        //-->     LIQUIDAZIONE CONTESTUALE - UTILIZZO L'AREA DEL
                        //-->     BOLLO IN INPUT
                        // COB_CODE: SET FINE-CUR-MOV TO TRUE
                        ws.getFlagCurMov().setFineCurMov();
                        break;

                    case Idsv0003Sqlcode.SUCCESSFUL_SQL://-->   OPERAZIONE ESEGUITA CORRETTAMENTE
                        //                   MOVE IDSV0003-BUFFER-DATI TO MOVI
                        //      TROVO IL MOVIMENTO DI COMUNICAZIONE RISCATTO PARZIALE
                        // COB_CODE: MOVE MOV-TP-MOVI      TO WS-MOVIMENTO
                        ws.getWsMovimento().setWsMovimento(TruncAbs.toInt(ws.getMovi().getMovTpMovi().getMovTpMovi(), 5));
                        // COB_CODE: IF COMUN-RISPAR-IND
                        //           OR RISTO-INDIVI
                        //           OR VARIA-OPZION
                        //           OR SINIS-INDIVI
                        //           OR RECES-INDIVI
                        //           OR RPP-TAKE-PROFIT
                        //           OR COMUN-RISTOT-INCAPIENZA
                        //           OR RPP-REDDITO-PROGRAMMATO
                        //           OR RPP-BENEFICIO-CONTR
                        //           OR COMUN-RISTOT-INCAP
                        //              SET FINE-CUR-MOV   TO TRUE
                        //           ELSE
                        //              SET IDSV0003-FETCH-NEXT   TO TRUE
                        //           END-IF
                        if (ws.getWsMovimento().isComunRisparInd() || ws.getWsMovimento().isRistoIndivi() || ws.getWsMovimento().isVariaOpzion() || ws.getWsMovimento().isSinisIndivi() || ws.getWsMovimento().isRecesIndivi() || ws.getWsMovimento().isRppTakeProfit() || ws.getWsMovimento().isComunRistotIncapienza() || ws.getWsMovimento().isRppRedditoProgrammato() || ws.getWsMovimento().isRppBeneficioContr() || ws.getWsMovimento().isComunRistotIncap()) {
                            // COB_CODE: MOVE MOV-ID-MOVI    TO WK-ID-MOVI-COMUN
                            ws.getWkVarMoviComun().setIdMoviComun(ws.getMovi().getMovIdMovi());
                            // COB_CODE: SET SI-ULTIMA-LETTURA  TO TRUE
                            ws.getFlagUltimaLettura().setSiUltimaLettura();
                            // COB_CODE: MOVE MOV-DT-EFF     TO WK-DATA-EFF-PREC
                            ws.getWkVarMoviComun().setDataEffPrec(ws.getMovi().getMovDtEff());
                            // COB_CODE: COMPUTE WK-DATA-CPTZ-PREC =  MOV-DS-TS-CPTZ
                            //                                     - 1
                            ws.getWkVarMoviComun().setDataCptzPrec(Trunc.toLong(ws.getMovi().getMovDsTsCptz() - 1, 18));
                            // COB_CODE: SET COMUN-TROV-SI   TO TRUE
                            ws.getFlagComunTrov().setSi();
                            // COB_CODE: PERFORM CLOSE-MOVI
                            //              THRU CLOSE-MOVI-EX
                            closeMovi();
                            // COB_CODE: SET FINE-CUR-MOV   TO TRUE
                            ws.getFlagCurMov().setFineCurMov();
                        }
                        else {
                            // COB_CODE: SET IDSV0003-FETCH-NEXT   TO TRUE
                            idsv0003.getOperazione().setFetchNext();
                        }
                        break;

                    default://--->   ERRORE DI ACCESSO AL DB
                        // COB_CODE: MOVE LDBS6040
                        //             TO IDSV0003-COD-SERVIZIO-BE
                        idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbs6040());
                        // COB_CODE: MOVE 'CALL-LDBS6040 ERRORE CHIAMATA'
                        //             TO IDSV0003-DESCRIZ-ERR-DB2
                        idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBS6040 ERRORE CHIAMATA");
                        // COB_CODE: SET IDSV0003-INVALID-OPER   TO TRUE
                        idsv0003.getReturnCode().setInvalidOper();
                        break;
                }
            }
            else {
                //--> GESTIRE ERRORE
                // COB_CODE: MOVE LDBS6040
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbs6040());
                // COB_CODE: MOVE 'CALL-LDBS6040 ERRORE CHIAMATA'
                //             TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBS6040 ERRORE CHIAMATA");
                // COB_CODE: SET IDSV0003-INVALID-OPER   TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
        }
        // COB_CODE: MOVE IDSV0003-TIPO-MOVIMENTO TO WS-MOVIMENTO.
        ws.getWsMovimento().setWsMovimentoFormatted(idsv0003.getTipoMovimentoFormatted());
    }

    /**Original name: CLOSE-MOVI<br>
	 * <pre>----------------------------------------------------------------*
	 *     CHIUDO IL CURSORE SULLA TABELLA MOVIMENTO LDBS6040
	 * ----------------------------------------------------------------*</pre>*/
    private void closeMovi() {
        Ldbs6040 ldbs6040 = null;
        // COB_CODE: SET IDSV0003-CLOSE-CURSOR            TO TRUE.
        idsv0003.getOperazione().setIdsv0003CloseCursor();
        // COB_CODE: CALL LDBS6040 USING IDSV0003 MOVI
        //           ON EXCEPTION
        //              SET IDSV0003-INVALID-OPER TO TRUE
        //           END-CALL.
        try {
            ldbs6040 = Ldbs6040.getInstance();
            ldbs6040.run(idsv0003, ws.getMovi());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE LDBS6040
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbs6040());
            // COB_CODE: MOVE 'ERRORE CALL LDBS6040 CLOSE'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("ERRORE CALL LDBS6040 CLOSE");
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE: IF NOT IDSV0003-SUCCESSFUL-RC
        //           OR NOT IDSV0003-SUCCESSFUL-SQL
        //                TO TRUE
        //           END-IF.
        if (!idsv0003.getReturnCode().isSuccessfulRc() || !idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: MOVE WK-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'ERRORE CLOSE CURSORE LDBS6040'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("ERRORE CLOSE CURSORE LDBS6040");
            // COB_CODE: SET IDSV0003-INVALID-OPER
            //            TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: LETTURA-IMPST-BOLLO<br>
	 * <pre>----------------------------------------------------------------*
	 *     RECUPERO IMPOSTA DI BOLLO DA MOVIMENTO DI COMUNICAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void letturaImpstBollo() {
        Ldbse590 ldbse590 = null;
        // COB_CODE: SET FINE-LETT-P58-NO           TO TRUE
        ws.getFlagFineLetturaP58().setNo();
        // COB_CODE: INITIALIZE IMPST-BOLLO
        initImpstBollo();
        // COB_CODE: MOVE 0 TO DP58-ELE-P58-MAX
        ws.setDp58EleP58Max(((short)0));
        // COB_CODE: MOVE 0 TO IX-TAB-P58
        ws.getIxIndici().setTabP58(0);
        //--> TIPO OPERAZIONE
        // COB_CODE: SET IDSV0003-FETCH-FIRST       TO TRUE
        idsv0003.getOperazione().setFetchFirst();
        //--> INIZIALIZZA CODICE DI RITORNO
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC    TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        //
        // COB_CODE: PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC
        //                      OR FINE-LETT-P58-SI
        //                 END-IF
        //           END-PERFORM.
        while (!(!idsv0003.getReturnCode().isSuccessfulRc() || ws.getFlagFineLetturaP58().isSi())) {
            // COB_CODE: INITIALIZE IMPST-BOLLO
            initImpstBollo();
            //
            // COB_CODE: MOVE IVVC0213-ID-POLIZZA    TO P58-ID-POLI
            ws.getImpstBollo().setP58IdPoli(ivvc0213.getIdPolizza());
            // COB_CODE: SET IDSV0003-TRATT-X-COMPETENZA TO TRUE
            idsv0003.getTrattamentoStoricita().setTrattXCompetenza();
            //--> LIVELLO OPERAZIONE
            // COB_CODE: SET IDSV0003-WHERE-CONDITION TO TRUE
            idsv0003.getLivelloOperazione().setWhereCondition();
            //--> INIZIALIZZA CODICE DI RITORNO
            // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC  TO TRUE
            idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
            // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL TO TRUE
            idsv0003.getSqlcode().setSuccessfulSql();
            // COB_CODE: CALL  LDBSE590   USING IDSV0003 IMPST-BOLLO
            //             ON EXCEPTION
            //                SET IDSV0003-INVALID-OPER   TO TRUE
            //           END-CALL
            try {
                ldbse590 = Ldbse590.getInstance();
                ldbse590.run(idsv0003, ws.getImpstBollo());
            }
            catch (ProgramExecutionException __ex) {
                // COB_CODE: MOVE LDBSE590          TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbse590());
                // COB_CODE: MOVE 'CALL-LDBSE590 ERRORE CHIAMATA'
                //             TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBSE590 ERRORE CHIAMATA");
                // COB_CODE: SET IDSV0003-INVALID-OPER   TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
            // COB_CODE:           IF IDSV0003-SUCCESSFUL-RC
            //                         END-EVALUATE
            //                      ELSE
            //           *--> GESTIRE ERRORE
            //                         SET FINE-LETT-P58-SI TO TRUE
            //                      END-IF
            if (idsv0003.getReturnCode().isSuccessfulRc()) {
                // COB_CODE:               EVALUATE TRUE
                //                             WHEN IDSV0003-NOT-FOUND
                //           *-->    NESSUN DATO IN TABELLA
                //                               END-IF
                //                                  SET IDSV0003-FETCH-NEXT   TO TRUE
                //                             WHEN OTHER
                //           *--->   ERRORE DI ACCESSO AL DB
                //                                  SET FINE-LETT-P58-SI TO TRUE
                //                         END-EVALUATE
                switch (idsv0003.getSqlcode().getSqlcode()) {

                    case Idsv0003Sqlcode.NOT_FOUND://-->    NESSUN DATO IN TABELLA
                        // COB_CODE: SET FINE-LETT-P58-SI           TO TRUE
                        ws.getFlagFineLetturaP58().setSi();
                        // COB_CODE: IF NOT IDSV0003-FETCH-FIRST
                        //              SET IDSV0003-SUCCESSFUL-RC  TO TRUE
                        //           END-IF
                        if (!idsv0003.getOperazione().isFetchFirst()) {
                            // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL TO TRUE
                            idsv0003.getSqlcode().setSuccessfulSql();
                            // COB_CODE: SET IDSV0003-SUCCESSFUL-RC  TO TRUE
                            idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
                        }
                        break;

                    case Idsv0003Sqlcode.SUCCESSFUL_SQL://-->   OPERAZIONE ESEGUITA CORRETTAMENTE
                        // COB_CODE: code not available
                        ws.setIntRegister1(1);
                        ws.setDp58EleP58Max(Trunc.toShort(ws.getIntRegister1() + ws.getDp58EleP58Max(), 4));
                        ws.getIxIndici().setTabP58(Trunc.toInt(abs(ws.getIntRegister1() + ws.getIxIndici().getTabP58()), 9));
                        // COB_CODE: PERFORM VALORIZZA-OUTPUT-P58
                        //              THRU VALORIZZA-OUTPUT-P58-EX
                        valorizzaOutputP58();
                        // COB_CODE: SET IDSV0003-FETCH-NEXT   TO TRUE
                        idsv0003.getOperazione().setFetchNext();
                        break;

                    default://--->   ERRORE DI ACCESSO AL DB
                        // COB_CODE: MOVE LDBSE590
                        //             TO IDSV0003-COD-SERVIZIO-BE
                        idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbse590());
                        // COB_CODE: MOVE 'CALL-LDBSE590 ERRORE CHIAMATA'
                        //             TO IDSV0003-DESCRIZ-ERR-DB2
                        idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBSE590 ERRORE CHIAMATA");
                        // COB_CODE: SET IDSV0003-INVALID-OPER   TO TRUE
                        idsv0003.getReturnCode().setInvalidOper();
                        // COB_CODE: SET FINE-LETT-P58-SI TO TRUE
                        ws.getFlagFineLetturaP58().setSi();
                        break;
                }
            }
            else {
                //--> GESTIRE ERRORE
                // COB_CODE: MOVE LDBSE590
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbse590());
                // COB_CODE: MOVE 'CALL-LDBSE590 ERRORE CHIAMATA'
                //             TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBSE590 ERRORE CHIAMATA");
                // COB_CODE: SET IDSV0003-INVALID-OPER   TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
                // COB_CODE: SET FINE-LETT-P58-SI TO TRUE
                ws.getFlagFineLetturaP58().setSi();
            }
        }
    }

    /**Original name: S1100-VALORIZZA-DCLGEN<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 *     RISPETTIVE AREE DCLGEN IN WORKING
	 * ----------------------------------------------------------------*</pre>*/
    private void s1100ValorizzaDclgen() {
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-IMPOSTA-BOLLO
        //                TO DP58-AREA-P58
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getTabAlias(), ws.getIvvc0218().getAliasImpostaBollo())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO DP58-AREA-P58
            ws.setDp58AreaP58Formatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getLunghezza() - 1));
        }
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-POLI
        //                TO DPOL-AREA-POL
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getTabAlias(), ws.getIvvc0218().getAliasPoli())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO DPOL-AREA-POL
            ws.setDpolAreaPolFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getLunghezza() - 1));
        }
    }

    /**Original name: S1150-CALCOLO-ANNO<br>
	 * <pre>----------------------------------------------------------------*
	 *    CALCOLO ANNO
	 * ----------------------------------------------------------------*</pre>*/
    private void s1150CalcoloAnno() {
        // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
        //             TO WK-DATA
        ws.getWkDataSeparata().setWkData(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
        // COB_CODE: MOVE WK-ANNO
        //             TO WK-ANNO-INI
        //                WK-ANNO-FINE
        ws.getWkDataInizio().setAnnoIniFormatted(ws.getWkDataSeparata().getAnnoFormatted());
        ws.getWkDataFine().setAnnoFineFormatted(ws.getWkDataSeparata().getAnnoFormatted());
        // COB_CODE: MOVE 01
        //             TO WK-MESE-INI
        //                WK-GG-INI
        ws.getWkDataInizio().setMeseIni(((short)1));
        ws.getWkDataInizio().setGgIni(((short)1));
        // COB_CODE: MOVE 12
        //             TO WK-MESE-FINE
        ws.getWkDataFine().setMeseFine(((short)12));
        // COB_CODE: MOVE 31
        //             TO WK-GG-FINE.
        ws.getWkDataFine().setGgFine(((short)31));
        // COB_CODE: MOVE WK-DATA-INIZIO
        //             TO WK-DATA
        ws.getWkDataSeparata().setWkDataFromBuffer(ws.getWkDataInizio().getWkDataInizioBytes());
        // COB_CODE: MOVE WK-DATA
        //             TO WK-DATA-INIZIO-D.
        ws.setWkDataInizioD(ws.getWkDataSeparata().getWkData());
        // COB_CODE: MOVE WK-DATA-FINE
        //             TO WK-DATA
        ws.getWkDataSeparata().setWkDataFromBuffer(ws.getWkDataFine().getWkDataFineBytes());
        // COB_CODE: MOVE WK-DATA
        //             TO WK-DATA-FINE-D.
        ws.setWkDataFineD(ws.getWkDataSeparata().getWkData());
    }

    /**Original name: S1300-CALCOLO-SOMMA<br>
	 * <pre>----------------------------------------------------------------*
	 *    CALCOLO DELLA SOMMAPAG
	 * ----------------------------------------------------------------*</pre>*/
    private void s1300CalcoloSomma() {
        ConcatUtil concatUtil = null;
        Ldbse420 ldbse420 = null;
        Ldbse390 ldbse390 = null;
        // COB_CODE:      IF DP58-COD-FISC-NULL(1) EQUAL HIGH-VALUES
        //                                            OR LOW-VALUES
        //           *
        //                   END-IF
        //           *
        //                ELSE
        //           *
        //                   END-IF
        //                END-IF
        if (Characters.EQ_HIGH.test(ws.getDp58TabP58(1).getLccvp581().getDati().getDp58CodFiscFormatted()) || Characters.EQ_LOW.test(ws.getDp58TabP58(1).getLccvp581().getDati().getDp58CodFiscFormatted())) {
            //
            // COB_CODE: MOVE 'LDBSE420'                     TO WK-CALL-PGM
            ws.setWkCallPgm("LDBSE420");
            // COB_CODE: INITIALIZE LDBVE421
            initLdbve421();
            //
            // COB_CODE: MOVE DP58-COD-PART-IVA(1)
            //             TO LDBVE421-COD-PART-IVA
            ws.getLdbve421().getAreaLdbve421I().setCodPartIva(ws.getDp58TabP58(1).getLccvp581().getDati().getWp58CodPartIva());
            //
            // COB_CODE: MOVE 'SW' TO   LDBVE421-TP-CAUS-BOLLO-1
            ws.getLdbve421().getAreaLdbve421I().setTpCausBollo1("SW");
            // COB_CODE: MOVE 'AN' TO   LDBVE421-TP-CAUS-BOLLO-2
            ws.getLdbve421().getAreaLdbve421I().setTpCausBollo2("AN");
            // COB_CODE: MOVE SPACES TO LDBVE421-TP-CAUS-BOLLO-3
            ws.getLdbve421().getAreaLdbve421I().setTpCausBollo3("");
            // COB_CODE: MOVE SPACES TO LDBVE421-TP-CAUS-BOLLO-4
            ws.getLdbve421().getAreaLdbve421I().setTpCausBollo4("");
            // COB_CODE: MOVE SPACES TO LDBVE421-TP-CAUS-BOLLO-5
            ws.getLdbve421().getAreaLdbve421I().setTpCausBollo5("");
            // COB_CODE: MOVE SPACES TO LDBVE421-TP-CAUS-BOLLO-6
            ws.getLdbve421().getAreaLdbve421I().setTpCausBollo6("");
            // COB_CODE: MOVE SPACES TO LDBVE421-TP-CAUS-BOLLO-7
            ws.getLdbve421().getAreaLdbve421I().setTpCausBollo7("");
            //
            // COB_CODE: MOVE WK-DATA-INIZIO-D
            //             TO LDBVE421-DT-INI-CALC
            ws.getLdbve421().getAreaLdbve421I().setDtIniCalc(ws.getWkDataInizioD());
            //
            // COB_CODE: MOVE WK-DATA-FINE-D
            //             TO LDBVE421-DT-END-CALC
            ws.getLdbve421().getAreaLdbve421I().setDtEndCalc(ws.getWkDataFineD());
            //
            // COB_CODE: SET IDSV0003-TRATT-X-COMPETENZA TO TRUE
            idsv0003.getTrattamentoStoricita().setTrattXCompetenza();
            //
            // COB_CODE: SET IDSV0003-WHERE-CONDITION    TO TRUE
            idsv0003.getLivelloOperazione().setWhereCondition();
            //
            // COB_CODE: SET IDSV0003-SELECT             TO TRUE
            idsv0003.getOperazione().setSelect();
            //
            // COB_CODE: CALL WK-CALL-PGM  USING  IDSV0003 LDBVE421
            //           ON EXCEPTION
            //           SET IDSV0003-INVALID-OPER  TO TRUE
            //           END-CALL
            try {
                ldbse420 = Ldbse420.getInstance();
                ldbse420.run(idsv0003, ws.getLdbve421());
            }
            catch (ProgramExecutionException __ex) {
                // COB_CODE: MOVE WK-CALL-PGM
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
                // COB_CODE: STRING WK-PGM ' ERRORE CALL - MODULO LVVS2720'
                //           DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                //            END-STRING
                concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, ws.getWkPgmFormatted(), " ERRORE CALL - MODULO LVVS2720");
                idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //                TO IVVC0213-VAL-IMP-O
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: MOVE LDBVE421-IMP-BOLLO-DETT-C
                //             TO IVVC0213-VAL-IMP-O
                ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(ws.getLdbve421().getLdbve421ImpBolloDettC(), 18, 7));
            }
            //
        }
        else {
            //
            // COB_CODE: MOVE 'LDBSE390'                     TO WK-CALL-PGM
            ws.setWkCallPgm("LDBSE390");
            // COB_CODE: INITIALIZE LDBVE391
            initLdbve391();
            //
            // COB_CODE: MOVE DP58-COD-FISC(1)
            //             TO LDBVE391-COD-FISC
            ws.getLdbve391().getAreaLdbve391I().setCodFisc(ws.getDp58TabP58(1).getLccvp581().getDati().getWp58CodFisc());
            //
            // COB_CODE: MOVE 'SW' TO   LDBVE391-TP-CAUS-BOLLO-1
            ws.getLdbve391().getAreaLdbve391I().setTpCausBollo1("SW");
            // COB_CODE: MOVE 'AN' TO   LDBVE391-TP-CAUS-BOLLO-2
            ws.getLdbve391().getAreaLdbve391I().setTpCausBollo2("AN");
            // COB_CODE: MOVE SPACES TO LDBVE391-TP-CAUS-BOLLO-3
            ws.getLdbve391().getAreaLdbve391I().setTpCausBollo3("");
            // COB_CODE: MOVE SPACES TO LDBVE391-TP-CAUS-BOLLO-4
            ws.getLdbve391().getAreaLdbve391I().setTpCausBollo4("");
            // COB_CODE: MOVE SPACES TO LDBVE391-TP-CAUS-BOLLO-5
            ws.getLdbve391().getAreaLdbve391I().setTpCausBollo5("");
            // COB_CODE: MOVE SPACES TO LDBVE391-TP-CAUS-BOLLO-6
            ws.getLdbve391().getAreaLdbve391I().setTpCausBollo6("");
            // COB_CODE: MOVE SPACES TO LDBVE391-TP-CAUS-BOLLO-7
            ws.getLdbve391().getAreaLdbve391I().setTpCausBollo7("");
            //
            // COB_CODE: MOVE WK-DATA-INIZIO-D
            //             TO LDBVE391-DT-INI-CALC
            ws.getLdbve391().getAreaLdbve391I().setDtIniCalc(ws.getWkDataInizioD());
            //
            // COB_CODE: MOVE WK-DATA-FINE-D
            //             TO LDBVE391-DT-END-CALC
            ws.getLdbve391().getAreaLdbve391I().setDtEndCalc(ws.getWkDataFineD());
            //
            // COB_CODE: SET IDSV0003-TRATT-X-COMPETENZA TO TRUE
            idsv0003.getTrattamentoStoricita().setTrattXCompetenza();
            //
            // COB_CODE: SET IDSV0003-WHERE-CONDITION    TO TRUE
            idsv0003.getLivelloOperazione().setWhereCondition();
            //
            // COB_CODE: SET IDSV0003-SELECT             TO TRUE
            idsv0003.getOperazione().setSelect();
            //
            // COB_CODE: CALL WK-CALL-PGM  USING  IDSV0003 LDBVE391
            //           ON EXCEPTION
            //           SET IDSV0003-INVALID-OPER  TO TRUE
            //           END-CALL
            try {
                ldbse390 = Ldbse390.getInstance();
                ldbse390.run(idsv0003, ws.getLdbve391());
            }
            catch (ProgramExecutionException __ex) {
                // COB_CODE: MOVE WK-CALL-PGM
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
                // COB_CODE: STRING 'CALL ' WK-PGM ' ERRORE CHIAMATA - MODULO LVVS2720'
                //           DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                //            END-STRING
                concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CALL ", ws.getWkPgmFormatted(), " ERRORE CHIAMATA - MODULO LVVS2720");
                idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //                TO IVVC0213-VAL-IMP-O
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: MOVE LDBVE391-IMP-BOLLO-DETT-C
                //             TO IVVC0213-VAL-IMP-O
                ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(ws.getLdbve391().getLdbve391ImpBolloDettC(), 18, 7));
            }
        }
        // COB_CODE: IF NOT IDSV0003-SUCCESSFUL-SQL
        //              END-IF
        //           END-IF.
        if (!idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: MOVE WK-CALL-PGM             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: STRING  'ERRORE CHIAMATA - MODULO LVVS2720'
            //                  IDSV0003-RETURN-CODE ';'
            //                  IDSV0003-SQLCODE
            //              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "ERRORE CHIAMATA - MODULO LVVS2720", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
            //           ELSE
            //              SET IDSV0003-INVALID-OPER            TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isNotFound()) {
                // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-OPER            TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
        }
    }

    /**Original name: S1390-CALCOLO-SOMMA-2<br>
	 * <pre>----------------------------------------------------------------*
	 *    CALCOLO DELLA SOMMAPAG
	 * ----------------------------------------------------------------*
	 *  IL RECUPERO DEL BOLLO TOTALE VARIA IN BASE AL TIPO DI PERSONA
	 *  CHE HO RECUPERATO SULLA TABELLA PERS
	 *  LA GESTIONE PARTICOLARE E' STATA NECESSARIA PER IL CASO
	 *  DITTA INDIVIDUALE.</pre>*/
    private void s1390CalcoloSomma2() {
        ConcatUtil concatUtil = null;
        Ldbse420 ldbse420 = null;
        Ldbse390 ldbse390 = null;
        // COB_CODE:      IF PERSONA-GIURIDICA-SI AND
        //                   PARTITA-IVA-SI
        //                   END-IF
        //           *
        //                END-IF
        if (ws.getFlagPersonaGiuridica().isSi() && ws.getFlagPartitaIva().isSi()) {
            // COB_CODE: MOVE 'LDBSE420'                     TO WK-CALL-PGM
            ws.setWkCallPgm("LDBSE420");
            // COB_CODE: SET IDSV0003-TRATT-X-COMPETENZA     TO TRUE
            idsv0003.getTrattamentoStoricita().setTrattXCompetenza();
            // COB_CODE: SET IDSV0003-WHERE-CONDITION        TO TRUE
            idsv0003.getLivelloOperazione().setWhereCondition();
            // COB_CODE: SET IDSV0003-SELECT                 TO TRUE
            idsv0003.getOperazione().setSelect();
            // COB_CODE: INITIALIZE LDBVE421
            initLdbve421();
            //
            // COB_CODE: MOVE WK-COD-PRT-IVA
            //             TO LDBVE421-COD-PART-IVA
            ws.getLdbve421().getAreaLdbve421I().setCodPartIva(ws.getAreaRecuperoAnagrafica().getWkCodPrtIva());
            //
            // COB_CODE: MOVE 'SW' TO   LDBVE421-TP-CAUS-BOLLO-1
            ws.getLdbve421().getAreaLdbve421I().setTpCausBollo1("SW");
            // COB_CODE: MOVE 'AN' TO   LDBVE421-TP-CAUS-BOLLO-2
            ws.getLdbve421().getAreaLdbve421I().setTpCausBollo2("AN");
            //
            // COB_CODE: MOVE WK-DATA-INIZIO-D
            //             TO LDBVE421-DT-INI-CALC
            ws.getLdbve421().getAreaLdbve421I().setDtIniCalc(ws.getWkDataInizioD());
            //
            // COB_CODE: MOVE WK-DATA-FINE-D
            //             TO LDBVE421-DT-END-CALC
            ws.getLdbve421().getAreaLdbve421I().setDtEndCalc(ws.getWkDataFineD());
            // COB_CODE: CALL WK-CALL-PGM  USING  IDSV0003 LDBVE421
            //           ON EXCEPTION
            //           SET IDSV0003-INVALID-OPER  TO TRUE
            //           END-CALL
            try {
                ldbse420 = Ldbse420.getInstance();
                ldbse420.run(idsv0003, ws.getLdbve421());
            }
            catch (ProgramExecutionException __ex) {
                // COB_CODE: MOVE WK-CALL-PGM
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
                // COB_CODE: STRING WK-PGM ' ERRORE CHIAMATA - MODULO LVVS2720'
                //           DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, ws.getWkPgmFormatted(), " ERRORE CHIAMATA - MODULO LVVS2720");
                idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //                TO IVVC0213-VAL-IMP-O
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: MOVE LDBVE421-IMP-BOLLO-DETT-C
                //             TO IVVC0213-VAL-IMP-O
                ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(ws.getLdbve421().getLdbve421ImpBolloDettC(), 18, 7));
            }
            //
        }
        //
        // COB_CODE: IF ((PERSONA-GIURIDICA-SI AND
        //                CODICE-FISCALE-SI)   OR
        //               (PERSONA-FISICA-SI AND
        //                CODICE-FISCALE-SI))
        //              END-IF
        //           END-IF
        if (ws.getFlagPersonaGiuridica().isSi() && ws.getFlagCodiceFiscale().isSi() || ws.getFlagPersonaFisica().isSi() && ws.getFlagCodiceFiscale().isSi()) {
            // COB_CODE: MOVE 'LDBSE390'                     TO WK-CALL-PGM
            ws.setWkCallPgm("LDBSE390");
            // COB_CODE: SET IDSV0003-TRATT-X-COMPETENZA     TO TRUE
            idsv0003.getTrattamentoStoricita().setTrattXCompetenza();
            // COB_CODE: SET IDSV0003-WHERE-CONDITION        TO TRUE
            idsv0003.getLivelloOperazione().setWhereCondition();
            // COB_CODE: SET IDSV0003-SELECT                 TO TRUE
            idsv0003.getOperazione().setSelect();
            // COB_CODE: INITIALIZE LDBVE391
            initLdbve391();
            //
            // COB_CODE: MOVE WK-COD-FISC
            //             TO LDBVE391-COD-FISC
            ws.getLdbve391().getAreaLdbve391I().setCodFisc(ws.getAreaRecuperoAnagrafica().getWkCodFisc());
            //
            // COB_CODE: MOVE 'SW' TO LDBVE391-TP-CAUS-BOLLO-1
            ws.getLdbve391().getAreaLdbve391I().setTpCausBollo1("SW");
            // COB_CODE: MOVE 'AN' TO LDBVE391-TP-CAUS-BOLLO-2
            ws.getLdbve391().getAreaLdbve391I().setTpCausBollo2("AN");
            //
            // COB_CODE: MOVE WK-DATA-INIZIO-D
            //             TO LDBVE391-DT-INI-CALC
            ws.getLdbve391().getAreaLdbve391I().setDtIniCalc(ws.getWkDataInizioD());
            //
            // COB_CODE: MOVE WK-DATA-FINE-D
            //             TO LDBVE391-DT-END-CALC
            ws.getLdbve391().getAreaLdbve391I().setDtEndCalc(ws.getWkDataFineD());
            // COB_CODE: CALL WK-CALL-PGM  USING  IDSV0003 LDBVE391
            //           ON EXCEPTION
            //           SET IDSV0003-INVALID-OPER  TO TRUE
            //           END-CALL
            try {
                ldbse390 = Ldbse390.getInstance();
                ldbse390.run(idsv0003, ws.getLdbve391());
            }
            catch (ProgramExecutionException __ex) {
                // COB_CODE: MOVE WK-CALL-PGM
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
                // COB_CODE: STRING 'CALL ' WK-PGM ' ERRORE CHIAMATA - MODULO LVVS2720'
                //           DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                //            END-STRING
                concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CALL ", ws.getWkPgmFormatted(), " ERRORE CHIAMATA - MODULO LVVS2720");
                idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //                TO IVVC0213-VAL-IMP-O
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: MOVE LDBVE391-IMP-BOLLO-DETT-C
                //             TO IVVC0213-VAL-IMP-O
                ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(ws.getLdbve391().getLdbve391ImpBolloDettC(), 18, 7));
            }
        }
        // COB_CODE: IF NOT IDSV0003-SUCCESSFUL-SQL
        //              END-IF
        //           END-IF.
        if (!idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: MOVE WK-CALL-PGM             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: STRING  WK-PGM ' ERRORE CHIAMATA - MODULO LVVS2720'
            //                  IDSV0003-RETURN-CODE ';'
            //                  IDSV0003-SQLCODE
            //              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, ws.getWkPgmFormatted(), " ERRORE CHIAMATA - MODULO LVVS2720", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
            //           ELSE
            //              SET IDSV0003-INVALID-OPER            TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isNotFound()) {
                // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-OPER            TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
        }
    }

    /**Original name: S1350-RECUPERO-INFO<br>
	 * <pre>----------------------------------------------------------------*
	 *    RECUPERO INFORMAZIONI
	 * ----------------------------------------------------------------*</pre>*/
    private void s1350RecuperoInfo() {
        ConcatUtil concatUtil = null;
        Ldbs1240 ldbs1240 = null;
        // COB_CODE: MOVE 'LDBS1240'                     TO WK-CALL-PGM
        ws.setWkCallPgm("LDBS1240");
        // COB_CODE: INITIALIZE LDBV1241
        //                      RAPP-ANA.
        initLdbv1241();
        initRappAna();
        //
        // INIZIALIZZA SETTAGGI
        // COB_CODE: SET PERSONA-FISICA-NO               TO TRUE
        ws.getFlagPersonaFisica().setNo();
        // COB_CODE: SET PERSONA-GIURIDICA-NO            TO TRUE
        ws.getFlagPersonaGiuridica().setNo();
        // COB_CODE: SET CODICE-FISCALE-NO               TO TRUE
        ws.getFlagCodiceFiscale().setNo();
        // COB_CODE: SET PARTITA-IVA-NO                  TO TRUE
        ws.getFlagPartitaIva().setNo();
        // COB_CODE: SET IDSV0003-TRATT-X-COMPETENZA     TO TRUE
        idsv0003.getTrattamentoStoricita().setTrattXCompetenza();
        // COB_CODE: SET IDSV0003-WHERE-CONDITION        TO TRUE
        idsv0003.getLivelloOperazione().setWhereCondition();
        // COB_CODE: SET IDSV0003-SELECT                 TO TRUE
        idsv0003.getOperazione().setSelect();
        //--> INIZIALIZZA CODICE DI RITORNO
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC         TO TRUE
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL        TO TRUE
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: MOVE IVVC0213-ID-POLIZZA
        //             TO LDBV1241-ID-OGG
        ws.getLdbv1241().setIdOgg(ivvc0213.getIdPolizza());
        //
        // COB_CODE: MOVE 'PO'
        //             TO LDBV1241-TP-OGG
        ws.getLdbv1241().setTpOgg("PO");
        //
        // COB_CODE: MOVE 'CO'
        //             TO LDBV1241-TP-RAPP-ANA
        ws.getLdbv1241().setTpRappAna("CO");
        //
        // COB_CODE: MOVE LDBV1241 TO IDSV0003-BUFFER-WHERE-COND
        idsv0003.setBufferWhereCond(ws.getLdbv1241().getLdbv1241Formatted());
        // COB_CODE: CALL WK-CALL-PGM  USING  IDSV0003 LDBV1241
        //           ON EXCEPTION
        //           SET IDSV0003-INVALID-OPER  TO TRUE
        //           END-CALL
        try {
            ldbs1240 = Ldbs1240.getInstance();
            ldbs1240.run(idsv0003, ws.getLdbv1241());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-CALL-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: STRING WK-PGM ' ERRORE CHIAMATA - MODULO LVVS2720'
            //           DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //            END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, ws.getWkPgmFormatted(), " ERRORE CHIAMATA - MODULO LVVS2720");
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
            // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //                 THRU ACCESSO-PERS-EX
        //           ELSE
        //              END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM ACCESSO-PERS
            //              THRU ACCESSO-PERS-EX
            accessoPers();
        }
        else {
            // COB_CODE: MOVE WK-CALL-PGM       TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: STRING  WK-PGM ' ERRORE CHIAMATA - MODULO LVVS2720'
            //                  IDSV0003-RETURN-CODE ';'
            //                  IDSV0003-SQLCODE
            //              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, ws.getWkPgmFormatted(), " ERRORE CHIAMATA - MODULO LVVS2720", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
            //           ELSE
            //              SET IDSV0003-INVALID-OPER            TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isNotFound()) {
                // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-OPER            TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
        }
    }

    /**Original name: ACCESSO-PERS<br>
	 * <pre>----------------------------------------------------------------*
	 *    RECUPERO DELLA PERS
	 * ----------------------------------------------------------------*</pre>*/
    private void accessoPers() {
        ConcatUtil concatUtil = null;
        Ldbs1300 ldbs1300 = null;
        // COB_CODE: MOVE RAN-COD-SOGG       TO WK-ID-STRINGA
        ws.getAreaRecuperoAnagrafica().setWkIdStringa(ws.getRappAna().getRanCodSogg());
        // COB_CODE: PERFORM E000-CONVERTI-CHAR
        //              THRU EX-E000
        e000ConvertiChar();
        // COB_CODE: MOVE WK-ID-NUM              TO WK-ID-PERS
        ws.getAreaRecuperoAnagrafica().setWkIdPers(Trunc.toInt(ws.getAreaRecuperoAnagrafica().getWkIdNum(), 9));
        // COB_CODE: MOVE 'LDBS1300'                TO WK-CALL-PGM
        ws.setWkCallPgm("LDBS1300");
        // COB_CODE: SET IDSV0003-PRIMARY-KEY            TO TRUE
        idsv0003.getLivelloOperazione().setPrimaryKey();
        // COB_CODE: SET IDSV0003-SELECT                 TO TRUE
        idsv0003.getOperazione().setSelect();
        // COB_CODE: MOVE WK-ID-PERS                TO LDBV1301-ID-TAB
        ws.getLdbv1301().setLdbv1301IdTab(ws.getAreaRecuperoAnagrafica().getWkIdPers());
        // COB_CODE: MOVE WK-TIMESTAMP-N            TO LDBV1301-TIMESTAMP
        ws.getLdbv1301().setLdbv1301Timestamp(ws.getAreaRecuperoAnagrafica().getWkTimestamp().getWkTimestampN());
        // COB_CODE: MOVE LDBV1301                  TO IDSV0003-BUFFER-WHERE-COND
        idsv0003.setBufferWhereCond(ws.getLdbv1301().getLdbv1301Formatted());
        // COB_CODE: CALL WK-CALL-PGM  USING  IDSV0003 PERS
        //           ON EXCEPTION
        //              SET IDSV0003-INVALID-OPER  TO TRUE
        //           END-CALL
        try {
            ldbs1300 = Ldbs1300.getInstance();
            ldbs1300.run(idsv0003, ws.getPers());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-CALL-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: STRING WK-PGM ' ERRORE CHIAMATA - MODULO LVVS2720'
            //           DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //            END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, ws.getWkPgmFormatted(), " ERRORE CHIAMATA - MODULO LVVS2720");
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
            // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE:    IF A25-IND-CLI = 'F'
            //              END-IF
            //           ELSE
            //              END-IF
            //           END-IF.
            if (ws.getPers().getA25IndCli() == 'F') {
                // COB_CODE: SET PERSONA-FISICA-SI     TO TRUE
                ws.getFlagPersonaFisica().setSi();
                // COB_CODE:    IF A25-COD-FISC-NULL NOT = HIGH-VALUES OR
                //                                         LOW-VALUES  OR
                //                                         SPACES
                //              MOVE A25-COD-FISC         TO WK-COD-FISC
                //           ELSE
                //              END-IF
                //           END-IF
                if (!Characters.EQ_HIGH.test(ws.getPers().getA25CodFiscFormatted()) || !Characters.EQ_LOW.test(ws.getPers().getA25CodFiscFormatted()) || !Characters.EQ_SPACE.test(ws.getPers().getA25CodFisc())) {
                    // COB_CODE: SET CODICE-FISCALE-SI     TO TRUE
                    ws.getFlagCodiceFiscale().setSi();
                    // COB_CODE: MOVE A25-COD-FISC         TO WK-COD-FISC
                    ws.getAreaRecuperoAnagrafica().setWkCodFisc(ws.getPers().getA25CodFisc());
                }
                else {
                    // COB_CODE: SET PERSONA-GIURIDICA-SI  TO TRUE
                    ws.getFlagPersonaGiuridica().setSi();
                    // COB_CODE: IF A25-COD-PRT-IVA-NULL = HIGH-VALUES OR
                    //                                     LOW-VALUES  OR
                    //                                     SPACES
                    //              MOVE A25-COD-FISC      TO WK-COD-FISC
                    //           ELSE
                    //              MOVE A25-COD-PRT-IVA   TO WK-COD-PRT-IVA
                    //           END-IF
                    if (Characters.EQ_HIGH.test(ws.getPers().getA25CodPrtIvaFormatted()) || Characters.EQ_LOW.test(ws.getPers().getA25CodPrtIvaFormatted()) || Characters.EQ_SPACE.test(ws.getPers().getA25CodPrtIva())) {
                        // COB_CODE: SET CODICE-FISCALE-SI  TO TRUE
                        ws.getFlagCodiceFiscale().setSi();
                        // COB_CODE: MOVE A25-COD-FISC      TO WK-COD-FISC
                        ws.getAreaRecuperoAnagrafica().setWkCodFisc(ws.getPers().getA25CodFisc());
                    }
                    else {
                        // COB_CODE: SET PARTITA-IVA-SI     TO TRUE
                        ws.getFlagPartitaIva().setSi();
                        // COB_CODE: MOVE A25-COD-PRT-IVA   TO WK-COD-PRT-IVA
                        ws.getAreaRecuperoAnagrafica().setWkCodPrtIva(ws.getPers().getA25CodPrtIva());
                    }
                }
            }
            else {
                // COB_CODE: MOVE WK-CALL-PGM             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
                // COB_CODE: STRING  WK-PGM ' ERRORE CHIAMATA - MODULO LVVS2720'
                //                  IDSV0003-RETURN-CODE ';'
                //                  IDSV0003-SQLCODE
                //              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, ws.getWkPgmFormatted(), " ERRORE CHIAMATA - MODULO LVVS2720", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                // COB_CODE: IF IDSV0003-NOT-FOUND
                //              SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                //           ELSE
                //              SET IDSV0003-INVALID-OPER            TO TRUE
                //           END-IF
                if (idsv0003.getSqlcode().isNotFound()) {
                    // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                    idsv0003.getReturnCode().setFieldNotValued();
                }
                else {
                    // COB_CODE: SET IDSV0003-INVALID-OPER            TO TRUE
                    idsv0003.getReturnCode().setInvalidOper();
                }
            }
        }
    }

    /**Original name: E000-CONVERTI-CHAR<br>
	 * <pre>----------------------------------------------------------------*
	 *   CONVERTI VALORE DA STRINGA IN NUMERICO
	 * ----------------------------------------------------------------*</pre>*/
    private void e000ConvertiChar() {
        Iwfs0050 iwfs0050 = null;
        // COB_CODE: MOVE ZERO                  TO WK-ID-NUM.
        ws.getAreaRecuperoAnagrafica().setWkIdNum(0);
        // COB_CODE: MOVE HIGH-VALUE            TO AREA-CALL-IWFS0050.
        ws.getAreaCallIwfs0050().initAreaCallIwfs0050HighValues();
        // COB_CODE: MOVE WK-ID-STRINGA         TO IWFI0051-ARRAY-STRINGA-INPUT.
        ws.getAreaCallIwfs0050().setIwfi0051ArrayStringaInputFormatted(ws.getAreaRecuperoAnagrafica().getWkIdStringaFormatted());
        // COB_CODE: CALL IWFS0050              USING AREA-CALL-IWFS0050
        iwfs0050 = Iwfs0050.getInstance();
        iwfs0050.run(ws.getAreaCallIwfs0050());
        //    MOVE IWFO0051-ESITO        TO IDSV0003-ESITO.
        //    MOVE IWFO0051-LOG-ERRORE   TO IDSV0003-LOG-ERRORE.
        // COB_CODE: MOVE IWFO0051-CAMPO-OUTPUT-DEFI
        //                                      TO WK-ID-NUM.
        ws.getAreaRecuperoAnagrafica().setWkIdNum(TruncAbs.toLong(ws.getAreaCallIwfs0050().getIwfo0051().getCampoOutputDefi(), 11));
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: MOVE SPACES                     TO IVVC0213-VAL-STR-O.
        ivvc0213.getTabOutput().setValStrO("");
        // COB_CODE: MOVE 0                          TO IVVC0213-VAL-PERC-O.
        ivvc0213.getTabOutput().setValPercO(Trunc.toDecimal(0, 14, 9));
        // COB_CODE: IF COMUN-TROV-SI
        //                   IDSV0003-DATA-FINE-EFFETTO
        //           END-IF
        if (ws.getFlagComunTrov().isSi()) {
            // COB_CODE: MOVE WK-DATA-CPTZ-RIP
            //             TO IDSV0003-DATA-COMPETENZA
            idsv0003.setDataCompetenza(ws.getWkVarMoviComun().getDataCptzRip());
            // COB_CODE: MOVE WK-DATA-EFF-RIP
            //             TO IDSV0003-DATA-INIZIO-EFFETTO
            //                IDSV0003-DATA-FINE-EFFETTO
            idsv0003.setDataInizioEffetto(ws.getWkVarMoviComun().getDataEffRip());
            idsv0003.setDataFineEffetto(ws.getWkVarMoviComun().getDataEffRip());
        }
        //
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    /**Original name: VALORIZZA-OUTPUT-P58<br>
	 * <pre>----------------------------------------------------------------*
	 *    COPY IMPOSTA DI BOLLO
	 * ----------------------------------------------------------------*
	 * ------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    VALORIZZA OUTPUT COPY LCCVP583
	 *    ULTIMO AGG. 09 AGO 2013
	 * ------------------------------------------------------------</pre>*/
    private void valorizzaOutputP58() {
        // COB_CODE: MOVE P58-ID-IMPST-BOLLO
        //             TO (SF)-ID-PTF(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxIndici().getTabP58()).getLccvp581().setIdPtf(ws.getImpstBollo().getP58IdImpstBollo());
        // COB_CODE: MOVE P58-ID-IMPST-BOLLO
        //             TO (SF)-ID-IMPST-BOLLO(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxIndici().getTabP58()).getLccvp581().getDati().setWp58IdImpstBollo(ws.getImpstBollo().getP58IdImpstBollo());
        // COB_CODE: MOVE P58-COD-COMP-ANIA
        //             TO (SF)-COD-COMP-ANIA(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxIndici().getTabP58()).getLccvp581().getDati().setWp58CodCompAnia(ws.getImpstBollo().getP58CodCompAnia());
        // COB_CODE: MOVE P58-ID-POLI
        //             TO (SF)-ID-POLI(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxIndici().getTabP58()).getLccvp581().getDati().setWp58IdPoli(ws.getImpstBollo().getP58IdPoli());
        // COB_CODE: MOVE P58-IB-POLI
        //             TO (SF)-IB-POLI(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxIndici().getTabP58()).getLccvp581().getDati().setWp58IbPoli(ws.getImpstBollo().getP58IbPoli());
        // COB_CODE: IF P58-COD-FISC-NULL = HIGH-VALUES
        //                TO (SF)-COD-FISC-NULL(IX-TAB-P58)
        //           ELSE
        //                TO (SF)-COD-FISC(IX-TAB-P58)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getImpstBollo().getP58CodFiscFormatted())) {
            // COB_CODE: MOVE P58-COD-FISC-NULL
            //             TO (SF)-COD-FISC-NULL(IX-TAB-P58)
            ws.getDp58TabP58(ws.getIxIndici().getTabP58()).getLccvp581().getDati().setWp58CodFisc(ws.getImpstBollo().getP58CodFisc());
        }
        else {
            // COB_CODE: MOVE P58-COD-FISC
            //             TO (SF)-COD-FISC(IX-TAB-P58)
            ws.getDp58TabP58(ws.getIxIndici().getTabP58()).getLccvp581().getDati().setWp58CodFisc(ws.getImpstBollo().getP58CodFisc());
        }
        // COB_CODE: IF P58-COD-PART-IVA-NULL = HIGH-VALUES
        //                TO (SF)-COD-PART-IVA-NULL(IX-TAB-P58)
        //           ELSE
        //                TO (SF)-COD-PART-IVA(IX-TAB-P58)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getImpstBollo().getP58CodPartIvaFormatted())) {
            // COB_CODE: MOVE P58-COD-PART-IVA-NULL
            //             TO (SF)-COD-PART-IVA-NULL(IX-TAB-P58)
            ws.getDp58TabP58(ws.getIxIndici().getTabP58()).getLccvp581().getDati().setWp58CodPartIva(ws.getImpstBollo().getP58CodPartIva());
        }
        else {
            // COB_CODE: MOVE P58-COD-PART-IVA
            //             TO (SF)-COD-PART-IVA(IX-TAB-P58)
            ws.getDp58TabP58(ws.getIxIndici().getTabP58()).getLccvp581().getDati().setWp58CodPartIva(ws.getImpstBollo().getP58CodPartIva());
        }
        // COB_CODE: MOVE P58-ID-RAPP-ANA
        //             TO (SF)-ID-RAPP-ANA(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxIndici().getTabP58()).getLccvp581().getDati().setWp58IdRappAna(ws.getImpstBollo().getP58IdRappAna());
        // COB_CODE: MOVE P58-ID-MOVI-CRZ
        //             TO (SF)-ID-MOVI-CRZ(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxIndici().getTabP58()).getLccvp581().getDati().setWp58IdMoviCrz(ws.getImpstBollo().getP58IdMoviCrz());
        // COB_CODE: IF P58-ID-MOVI-CHIU-NULL = HIGH-VALUES
        //                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-P58)
        //           ELSE
        //                TO (SF)-ID-MOVI-CHIU(IX-TAB-P58)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getImpstBollo().getP58IdMoviChiu().getP58IdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE P58-ID-MOVI-CHIU-NULL
            //             TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-P58)
            ws.getDp58TabP58(ws.getIxIndici().getTabP58()).getLccvp581().getDati().getWp58IdMoviChiu().setDp58IdMoviChiuNull(ws.getImpstBollo().getP58IdMoviChiu().getP58IdMoviChiuNull());
        }
        else {
            // COB_CODE: MOVE P58-ID-MOVI-CHIU
            //             TO (SF)-ID-MOVI-CHIU(IX-TAB-P58)
            ws.getDp58TabP58(ws.getIxIndici().getTabP58()).getLccvp581().getDati().getWp58IdMoviChiu().setWp58IdMoviChiu(ws.getImpstBollo().getP58IdMoviChiu().getP58IdMoviChiu());
        }
        // COB_CODE: MOVE P58-DT-INI-EFF
        //             TO (SF)-DT-INI-EFF(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxIndici().getTabP58()).getLccvp581().getDati().setWp58DtIniEff(ws.getImpstBollo().getP58DtIniEff());
        // COB_CODE: MOVE P58-DT-END-EFF
        //             TO (SF)-DT-END-EFF(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxIndici().getTabP58()).getLccvp581().getDati().setWp58DtEndEff(ws.getImpstBollo().getP58DtEndEff());
        // COB_CODE: MOVE P58-DT-INI-CALC
        //             TO (SF)-DT-INI-CALC(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxIndici().getTabP58()).getLccvp581().getDati().setWp58DtIniCalc(ws.getImpstBollo().getP58DtIniCalc());
        // COB_CODE: MOVE P58-DT-END-CALC
        //             TO (SF)-DT-END-CALC(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxIndici().getTabP58()).getLccvp581().getDati().setWp58DtEndCalc(ws.getImpstBollo().getP58DtEndCalc());
        // COB_CODE: MOVE P58-TP-CAUS-BOLLO
        //             TO (SF)-TP-CAUS-BOLLO(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxIndici().getTabP58()).getLccvp581().getDati().setWp58TpCausBollo(ws.getImpstBollo().getP58TpCausBollo());
        // COB_CODE: MOVE P58-IMPST-BOLLO-DETT-C
        //             TO (SF)-IMPST-BOLLO-DETT-C(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxIndici().getTabP58()).getLccvp581().getDati().setWp58ImpstBolloDettC(Trunc.toDecimal(ws.getImpstBollo().getP58ImpstBolloDettC(), 15, 3));
        // COB_CODE: IF P58-IMPST-BOLLO-DETT-V-NULL = HIGH-VALUES
        //                TO (SF)-IMPST-BOLLO-DETT-V-NULL(IX-TAB-P58)
        //           ELSE
        //                TO (SF)-IMPST-BOLLO-DETT-V(IX-TAB-P58)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getImpstBollo().getP58ImpstBolloDettV().getP58ImpstBolloDettVNullFormatted())) {
            // COB_CODE: MOVE P58-IMPST-BOLLO-DETT-V-NULL
            //             TO (SF)-IMPST-BOLLO-DETT-V-NULL(IX-TAB-P58)
            ws.getDp58TabP58(ws.getIxIndici().getTabP58()).getLccvp581().getDati().getWp58ImpstBolloDettV().setDp58ImpstBolloDettVNull(ws.getImpstBollo().getP58ImpstBolloDettV().getP58ImpstBolloDettVNull());
        }
        else {
            // COB_CODE: MOVE P58-IMPST-BOLLO-DETT-V
            //             TO (SF)-IMPST-BOLLO-DETT-V(IX-TAB-P58)
            ws.getDp58TabP58(ws.getIxIndici().getTabP58()).getLccvp581().getDati().getWp58ImpstBolloDettV().setWp58ImpstBolloDettV(Trunc.toDecimal(ws.getImpstBollo().getP58ImpstBolloDettV().getP58ImpstBolloDettV(), 15, 3));
        }
        // COB_CODE: IF P58-IMPST-BOLLO-TOT-V-NULL = HIGH-VALUES
        //                TO (SF)-IMPST-BOLLO-TOT-V-NULL(IX-TAB-P58)
        //           ELSE
        //                TO (SF)-IMPST-BOLLO-TOT-V(IX-TAB-P58)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getImpstBollo().getP58ImpstBolloTotV().getP58ImpstBolloTotVNullFormatted())) {
            // COB_CODE: MOVE P58-IMPST-BOLLO-TOT-V-NULL
            //             TO (SF)-IMPST-BOLLO-TOT-V-NULL(IX-TAB-P58)
            ws.getDp58TabP58(ws.getIxIndici().getTabP58()).getLccvp581().getDati().getWp58ImpstBolloTotV().setDp58ImpstBolloTotVNull(ws.getImpstBollo().getP58ImpstBolloTotV().getP58ImpstBolloTotVNull());
        }
        else {
            // COB_CODE: MOVE P58-IMPST-BOLLO-TOT-V
            //             TO (SF)-IMPST-BOLLO-TOT-V(IX-TAB-P58)
            ws.getDp58TabP58(ws.getIxIndici().getTabP58()).getLccvp581().getDati().getWp58ImpstBolloTotV().setWp58ImpstBolloTotV(Trunc.toDecimal(ws.getImpstBollo().getP58ImpstBolloTotV().getP58ImpstBolloTotV(), 15, 3));
        }
        // COB_CODE: MOVE P58-IMPST-BOLLO-TOT-R
        //             TO (SF)-IMPST-BOLLO-TOT-R(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxIndici().getTabP58()).getLccvp581().getDati().setWp58ImpstBolloTotR(Trunc.toDecimal(ws.getImpstBollo().getP58ImpstBolloTotR(), 15, 3));
        // COB_CODE: MOVE P58-DS-RIGA
        //             TO (SF)-DS-RIGA(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxIndici().getTabP58()).getLccvp581().getDati().setWp58DsRiga(ws.getImpstBollo().getP58DsRiga());
        // COB_CODE: MOVE P58-DS-OPER-SQL
        //             TO (SF)-DS-OPER-SQL(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxIndici().getTabP58()).getLccvp581().getDati().setWp58DsOperSql(ws.getImpstBollo().getP58DsOperSql());
        // COB_CODE: MOVE P58-DS-VER
        //             TO (SF)-DS-VER(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxIndici().getTabP58()).getLccvp581().getDati().setWp58DsVer(ws.getImpstBollo().getP58DsVer());
        // COB_CODE: MOVE P58-DS-TS-INI-CPTZ
        //             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxIndici().getTabP58()).getLccvp581().getDati().setWp58DsTsIniCptz(ws.getImpstBollo().getP58DsTsIniCptz());
        // COB_CODE: MOVE P58-DS-TS-END-CPTZ
        //             TO (SF)-DS-TS-END-CPTZ(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxIndici().getTabP58()).getLccvp581().getDati().setWp58DsTsEndCptz(ws.getImpstBollo().getP58DsTsEndCptz());
        // COB_CODE: MOVE P58-DS-UTENTE
        //             TO (SF)-DS-UTENTE(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxIndici().getTabP58()).getLccvp581().getDati().setWp58DsUtente(ws.getImpstBollo().getP58DsUtente());
        // COB_CODE: MOVE P58-DS-STATO-ELAB
        //             TO (SF)-DS-STATO-ELAB(IX-TAB-P58).
        ws.getDp58TabP58(ws.getIxIndici().getTabP58()).getLccvp581().getDati().setWp58DsStatoElab(ws.getImpstBollo().getP58DsStatoElab());
    }

    /**Original name: INIZIA-TOT-P58<br>
	 * <pre>------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    INIZIALIZZAZIONE CAMPI NULL COPY LCCVP584
	 *    ULTIMO AGG. 09 AGO 2013
	 * ------------------------------------------------------------</pre>*/
    private void iniziaTotP58() {
        // COB_CODE: PERFORM INIZIA-ZEROES-P58 THRU INIZIA-ZEROES-P58-EX
        //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LCCVP584:line=10, because the code is unreachable.
        // COB_CODE: PERFORM INIZIA-SPACES-P58 THRU INIZIA-SPACES-P58-EX
        //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LCCVP584:line=12, because the code is unreachable.
        // COB_CODE: PERFORM INIZIA-NULL-P58 THRU INIZIA-NULL-P58-EX.
        iniziaNullP58();
    }

    /**Original name: INIZIA-NULL-P58<br>*/
    private void iniziaNullP58() {
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-COD-FISC-NULL(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxIndici().getTabP58()).getLccvp581().getDati().setWp58CodFisc(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Wp58Dati.Len.WP58_COD_FISC));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-COD-PART-IVA-NULL(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxIndici().getTabP58()).getLccvp581().getDati().setWp58CodPartIva(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Wp58Dati.Len.WP58_COD_PART_IVA));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxIndici().getTabP58()).getLccvp581().getDati().getWp58IdMoviChiu().setDp58IdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Wp58IdMoviChiu.Len.DP58_ID_MOVI_CHIU_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMPST-BOLLO-DETT-V-NULL(IX-TAB-P58)
        ws.getDp58TabP58(ws.getIxIndici().getTabP58()).getLccvp581().getDati().getWp58ImpstBolloDettV().setDp58ImpstBolloDettVNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Wp58ImpstBolloDettV.Len.DP58_IMPST_BOLLO_DETT_V_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMPST-BOLLO-TOT-V-NULL(IX-TAB-P58).
        ws.getDp58TabP58(ws.getIxIndici().getTabP58()).getLccvp581().getDati().getWp58ImpstBolloTotV().setDp58ImpstBolloTotVNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Wp58ImpstBolloTotV.Len.DP58_IMPST_BOLLO_TOT_V_NULL));
    }

    public void initIxIndici() {
        ws.getIxIndici().setDclgen(((short)0));
        ws.getIxIndici().setTabRan(((short)0));
        ws.getIxIndici().setStringa(((short)0));
        ws.getIxIndici().setConvers(((short)0));
        ws.getIxIndici().setTabP58(0);
    }

    public void initTabOutput() {
        ivvc0213.getTabOutput().setCodVariabileO("");
        ivvc0213.getTabOutput().setTpDatoO(Types.SPACE_CHAR);
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        ivvc0213.getTabOutput().setValPercO(new AfDecimal(0, 14, 9));
        ivvc0213.getTabOutput().setValStrO("");
    }

    public void initAreaIoP58() {
        ws.setDp58EleP58Max(((short)0));
        for (int idx0 = 1; idx0 <= Lvvs2720Data.DP58_TAB_P58_MAXOCCURS; idx0++) {
            ws.getDp58TabP58(idx0).getLccvp581().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getDp58TabP58(idx0).getLccvp581().setIdPtf(0);
            ws.getDp58TabP58(idx0).getLccvp581().getDati().setWp58IdImpstBollo(0);
            ws.getDp58TabP58(idx0).getLccvp581().getDati().setWp58CodCompAnia(0);
            ws.getDp58TabP58(idx0).getLccvp581().getDati().setWp58IdPoli(0);
            ws.getDp58TabP58(idx0).getLccvp581().getDati().setWp58IbPoli("");
            ws.getDp58TabP58(idx0).getLccvp581().getDati().setWp58CodFisc("");
            ws.getDp58TabP58(idx0).getLccvp581().getDati().setWp58CodPartIva("");
            ws.getDp58TabP58(idx0).getLccvp581().getDati().setWp58IdRappAna(0);
            ws.getDp58TabP58(idx0).getLccvp581().getDati().setWp58IdMoviCrz(0);
            ws.getDp58TabP58(idx0).getLccvp581().getDati().getWp58IdMoviChiu().setWp58IdMoviChiu(0);
            ws.getDp58TabP58(idx0).getLccvp581().getDati().setWp58DtIniEff(0);
            ws.getDp58TabP58(idx0).getLccvp581().getDati().setWp58DtEndEff(0);
            ws.getDp58TabP58(idx0).getLccvp581().getDati().setWp58DtIniCalc(0);
            ws.getDp58TabP58(idx0).getLccvp581().getDati().setWp58DtEndCalc(0);
            ws.getDp58TabP58(idx0).getLccvp581().getDati().setWp58TpCausBollo("");
            ws.getDp58TabP58(idx0).getLccvp581().getDati().setWp58ImpstBolloDettC(new AfDecimal(0, 15, 3));
            ws.getDp58TabP58(idx0).getLccvp581().getDati().getWp58ImpstBolloDettV().setWp58ImpstBolloDettV(new AfDecimal(0, 15, 3));
            ws.getDp58TabP58(idx0).getLccvp581().getDati().getWp58ImpstBolloTotV().setWp58ImpstBolloTotV(new AfDecimal(0, 15, 3));
            ws.getDp58TabP58(idx0).getLccvp581().getDati().setWp58ImpstBolloTotR(new AfDecimal(0, 15, 3));
            ws.getDp58TabP58(idx0).getLccvp581().getDati().setWp58DsRiga(0);
            ws.getDp58TabP58(idx0).getLccvp581().getDati().setWp58DsOperSql(Types.SPACE_CHAR);
            ws.getDp58TabP58(idx0).getLccvp581().getDati().setWp58DsVer(0);
            ws.getDp58TabP58(idx0).getLccvp581().getDati().setWp58DsTsIniCptz(0);
            ws.getDp58TabP58(idx0).getLccvp581().getDati().setWp58DsTsEndCptz(0);
            ws.getDp58TabP58(idx0).getLccvp581().getDati().setWp58DsUtente("");
            ws.getDp58TabP58(idx0).getLccvp581().getDati().setWp58DsStatoElab(Types.SPACE_CHAR);
        }
    }

    public void initAreaIoPoli() {
        ws.setDpolElePolMax(((short)0));
        ws.getLccvpol1().getStatus().setStatus(Types.SPACE_CHAR);
        ws.getLccvpol1().setIdPtf(0);
        ws.getLccvpol1().getDati().setWpolIdPoli(0);
        ws.getLccvpol1().getDati().setWpolIdMoviCrz(0);
        ws.getLccvpol1().getDati().getWpolIdMoviChiu().setWpolIdMoviChiu(0);
        ws.getLccvpol1().getDati().setWpolIbOgg("");
        ws.getLccvpol1().getDati().setWpolIbProp("");
        ws.getLccvpol1().getDati().getWpolDtProp().setWpolDtProp(0);
        ws.getLccvpol1().getDati().setWpolDtIniEff(0);
        ws.getLccvpol1().getDati().setWpolDtEndEff(0);
        ws.getLccvpol1().getDati().setWpolCodCompAnia(0);
        ws.getLccvpol1().getDati().setWpolDtDecor(0);
        ws.getLccvpol1().getDati().setWpolDtEmis(0);
        ws.getLccvpol1().getDati().setWpolTpPoli("");
        ws.getLccvpol1().getDati().getWpolDurAa().setWpolDurAa(0);
        ws.getLccvpol1().getDati().getWpolDurMm().setWpolDurMm(0);
        ws.getLccvpol1().getDati().getWpolDtScad().setWpolDtScad(0);
        ws.getLccvpol1().getDati().setWpolCodProd("");
        ws.getLccvpol1().getDati().setWpolDtIniVldtProd(0);
        ws.getLccvpol1().getDati().setWpolCodConv("");
        ws.getLccvpol1().getDati().setWpolCodRamo("");
        ws.getLccvpol1().getDati().getWpolDtIniVldtConv().setWpolDtIniVldtConv(0);
        ws.getLccvpol1().getDati().getWpolDtApplzConv().setWpolDtApplzConv(0);
        ws.getLccvpol1().getDati().setWpolTpFrmAssva("");
        ws.getLccvpol1().getDati().setWpolTpRgmFisc("");
        ws.getLccvpol1().getDati().setWpolFlEstas(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlRshComun(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlRshComunCond(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolTpLivGenzTit("");
        ws.getLccvpol1().getDati().setWpolFlCopFinanz(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolTpApplzDir("");
        ws.getLccvpol1().getDati().getWpolSpeMed().setWpolSpeMed(new AfDecimal(0, 15, 3));
        ws.getLccvpol1().getDati().getWpolDirEmis().setWpolDirEmis(new AfDecimal(0, 15, 3));
        ws.getLccvpol1().getDati().getWpolDir1oVers().setWpolDir1oVers(new AfDecimal(0, 15, 3));
        ws.getLccvpol1().getDati().getWpolDirVersAgg().setWpolDirVersAgg(new AfDecimal(0, 15, 3));
        ws.getLccvpol1().getDati().setWpolCodDvs("");
        ws.getLccvpol1().getDati().setWpolFlFntAz(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlFntAder(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlFntTfr(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlFntVolo(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolTpOpzAScad("");
        ws.getLccvpol1().getDati().getWpolAaDiffProrDflt().setWpolAaDiffProrDflt(0);
        ws.getLccvpol1().getDati().setWpolFlVerProd("");
        ws.getLccvpol1().getDati().getWpolDurGg().setWpolDurGg(0);
        ws.getLccvpol1().getDati().getWpolDirQuiet().setWpolDirQuiet(new AfDecimal(0, 15, 3));
        ws.getLccvpol1().getDati().setWpolTpPtfEstno("");
        ws.getLccvpol1().getDati().setWpolFlCumPreCntr(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlAmmbMovi(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolConvGeco("");
        ws.getLccvpol1().getDati().setWpolDsRiga(0);
        ws.getLccvpol1().getDati().setWpolDsOperSql(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolDsVer(0);
        ws.getLccvpol1().getDati().setWpolDsTsIniCptz(0);
        ws.getLccvpol1().getDati().setWpolDsTsEndCptz(0);
        ws.getLccvpol1().getDati().setWpolDsUtente("");
        ws.getLccvpol1().getDati().setWpolDsStatoElab(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlScudoFisc(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlTrasfe(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlTfrStrc(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().getWpolDtPresc().setWpolDtPresc(0);
        ws.getLccvpol1().getDati().setWpolCodConvAgg("");
        ws.getLccvpol1().getDati().setWpolSubcatProd("");
        ws.getLccvpol1().getDati().setWpolFlQuestAdegzAss(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolCodTpa("");
        ws.getLccvpol1().getDati().getWpolIdAccComm().setWpolIdAccComm(0);
        ws.getLccvpol1().getDati().setWpolFlPoliCpiPr(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlPoliBundling(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolIndPoliPrinColl(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlVndBundle(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolIbBs("");
        ws.getLccvpol1().getDati().setWpolFlPoliIfp(Types.SPACE_CHAR);
    }

    public void initMovi() {
        ws.getMovi().setMovIdMovi(0);
        ws.getMovi().setMovCodCompAnia(0);
        ws.getMovi().getMovIdOgg().setMovIdOgg(0);
        ws.getMovi().setMovIbOgg("");
        ws.getMovi().setMovIbMovi("");
        ws.getMovi().setMovTpOgg("");
        ws.getMovi().getMovIdRich().setMovIdRich(0);
        ws.getMovi().getMovTpMovi().setMovTpMovi(0);
        ws.getMovi().setMovDtEff(0);
        ws.getMovi().getMovIdMoviAnn().setMovIdMoviAnn(0);
        ws.getMovi().getMovIdMoviCollg().setMovIdMoviCollg(0);
        ws.getMovi().setMovDsOperSql(Types.SPACE_CHAR);
        ws.getMovi().setMovDsVer(0);
        ws.getMovi().setMovDsTsCptz(0);
        ws.getMovi().setMovDsUtente("");
        ws.getMovi().setMovDsStatoElab(Types.SPACE_CHAR);
    }

    public void initImpstBollo() {
        ws.getImpstBollo().setP58IdImpstBollo(0);
        ws.getImpstBollo().setP58CodCompAnia(0);
        ws.getImpstBollo().setP58IdPoli(0);
        ws.getImpstBollo().setP58IbPoli("");
        ws.getImpstBollo().setP58CodFisc("");
        ws.getImpstBollo().setP58CodPartIva("");
        ws.getImpstBollo().setP58IdRappAna(0);
        ws.getImpstBollo().setP58IdMoviCrz(0);
        ws.getImpstBollo().getP58IdMoviChiu().setP58IdMoviChiu(0);
        ws.getImpstBollo().setP58DtIniEff(0);
        ws.getImpstBollo().setP58DtEndEff(0);
        ws.getImpstBollo().setP58DtIniCalc(0);
        ws.getImpstBollo().setP58DtEndCalc(0);
        ws.getImpstBollo().setP58TpCausBollo("");
        ws.getImpstBollo().setP58ImpstBolloDettC(new AfDecimal(0, 15, 3));
        ws.getImpstBollo().getP58ImpstBolloDettV().setP58ImpstBolloDettV(new AfDecimal(0, 15, 3));
        ws.getImpstBollo().getP58ImpstBolloTotV().setP58ImpstBolloTotV(new AfDecimal(0, 15, 3));
        ws.getImpstBollo().setP58ImpstBolloTotR(new AfDecimal(0, 15, 3));
        ws.getImpstBollo().setP58DsRiga(0);
        ws.getImpstBollo().setP58DsOperSql(Types.SPACE_CHAR);
        ws.getImpstBollo().setP58DsVer(0);
        ws.getImpstBollo().setP58DsTsIniCptz(0);
        ws.getImpstBollo().setP58DsTsEndCptz(0);
        ws.getImpstBollo().setP58DsUtente("");
        ws.getImpstBollo().setP58DsStatoElab(Types.SPACE_CHAR);
    }

    public void initLdbve421() {
        ws.getLdbve421().getAreaLdbve421I().setCodPartIva("");
        ws.getLdbve421().getAreaLdbve421I().setTpCausBollo1("");
        ws.getLdbve421().getAreaLdbve421I().setTpCausBollo2("");
        ws.getLdbve421().getAreaLdbve421I().setTpCausBollo3("");
        ws.getLdbve421().getAreaLdbve421I().setTpCausBollo4("");
        ws.getLdbve421().getAreaLdbve421I().setTpCausBollo5("");
        ws.getLdbve421().getAreaLdbve421I().setTpCausBollo6("");
        ws.getLdbve421().getAreaLdbve421I().setTpCausBollo7("");
        ws.getLdbve421().getAreaLdbve421I().setDtIniCalc(0);
        ws.getLdbve421().getAreaLdbve421I().setDtEndCalc(0);
        ws.getLdbve421().getAreaLdbve421I().setDtIniCalcDb("");
        ws.getLdbve421().getAreaLdbve421I().setDtEndCalcDb("");
        ws.getLdbve421().setLdbve421ImpBolloDettC(new AfDecimal(0, 15, 3));
        ws.getLdbve421().setLdbve421ImpBolloDettV(new AfDecimal(0, 15, 3));
        ws.getLdbve421().setLdbve421ImpBolloTotV(new AfDecimal(0, 15, 3));
        ws.getLdbve421().setLdbve421ImpBolloTotR(new AfDecimal(0, 15, 3));
    }

    public void initLdbve391() {
        ws.getLdbve391().getAreaLdbve391I().setCodFisc("");
        ws.getLdbve391().getAreaLdbve391I().setTpCausBollo1("");
        ws.getLdbve391().getAreaLdbve391I().setTpCausBollo2("");
        ws.getLdbve391().getAreaLdbve391I().setTpCausBollo3("");
        ws.getLdbve391().getAreaLdbve391I().setTpCausBollo4("");
        ws.getLdbve391().getAreaLdbve391I().setTpCausBollo5("");
        ws.getLdbve391().getAreaLdbve391I().setTpCausBollo6("");
        ws.getLdbve391().getAreaLdbve391I().setTpCausBollo7("");
        ws.getLdbve391().getAreaLdbve391I().setDtIniCalc(0);
        ws.getLdbve391().getAreaLdbve391I().setDtEndCalc(0);
        ws.getLdbve391().getAreaLdbve391I().setDtIniCalcDb("");
        ws.getLdbve391().getAreaLdbve391I().setDtEndCalcDb("");
        ws.getLdbve391().setLdbve391ImpBolloDettC(new AfDecimal(0, 15, 3));
        ws.getLdbve391().setLdbve391ImpBolloDettV(new AfDecimal(0, 15, 3));
        ws.getLdbve391().setLdbve391ImpBolloTotV(new AfDecimal(0, 15, 3));
        ws.getLdbve391().setLdbve391ImpBolloTotR(new AfDecimal(0, 15, 3));
    }

    public void initLdbv1241() {
        ws.getLdbv1241().setIdOgg(0);
        ws.getLdbv1241().setTpOgg("");
        ws.getLdbv1241().setTpRappAna("");
    }

    public void initRappAna() {
        ws.getRappAna().setRanIdRappAna(0);
        ws.getRappAna().getRanIdRappAnaCollg().setRanIdRappAnaCollg(0);
        ws.getRappAna().setRanIdOgg(0);
        ws.getRappAna().setRanTpOgg("");
        ws.getRappAna().setRanIdMoviCrz(0);
        ws.getRappAna().getRanIdMoviChiu().setRanIdMoviChiu(0);
        ws.getRappAna().setRanDtIniEff(0);
        ws.getRappAna().setRanDtEndEff(0);
        ws.getRappAna().setRanCodCompAnia(0);
        ws.getRappAna().setRanCodSogg("");
        ws.getRappAna().setRanTpRappAna("");
        ws.getRappAna().setRanTpPers(Types.SPACE_CHAR);
        ws.getRappAna().setRanSex(Types.SPACE_CHAR);
        ws.getRappAna().getRanDtNasc().setRanDtNasc(0);
        ws.getRappAna().setRanFlEstas(Types.SPACE_CHAR);
        ws.getRappAna().setRanIndir1("");
        ws.getRappAna().setRanIndir2("");
        ws.getRappAna().setRanIndir3("");
        ws.getRappAna().setRanTpUtlzIndir1("");
        ws.getRappAna().setRanTpUtlzIndir2("");
        ws.getRappAna().setRanTpUtlzIndir3("");
        ws.getRappAna().setRanEstrCntCorrAccr("");
        ws.getRappAna().setRanEstrCntCorrAdd("");
        ws.getRappAna().setRanEstrDocto("");
        ws.getRappAna().getRanPcNelRapp().setRanPcNelRapp(new AfDecimal(0, 6, 3));
        ws.getRappAna().setRanTpMezPagAdd("");
        ws.getRappAna().setRanTpMezPagAccr("");
        ws.getRappAna().setRanCodMatr("");
        ws.getRappAna().setRanTpAdegz("");
        ws.getRappAna().setRanFlTstRsh(Types.SPACE_CHAR);
        ws.getRappAna().setRanCodAz("");
        ws.getRappAna().setRanIndPrinc("");
        ws.getRappAna().getRanDtDeliberaCda().setRanDtDeliberaCda(0);
        ws.getRappAna().setRanDlgAlRisc(Types.SPACE_CHAR);
        ws.getRappAna().setRanLegaleRapprPrinc(Types.SPACE_CHAR);
        ws.getRappAna().setRanTpLegaleRappr("");
        ws.getRappAna().setRanTpIndPrinc("");
        ws.getRappAna().setRanTpStatRid("");
        ws.getRappAna().setRanNomeIntRidLen(((short)0));
        ws.getRappAna().setRanNomeIntRid("");
        ws.getRappAna().setRanCognIntRidLen(((short)0));
        ws.getRappAna().setRanCognIntRid("");
        ws.getRappAna().setRanCognIntTrattLen(((short)0));
        ws.getRappAna().setRanCognIntTratt("");
        ws.getRappAna().setRanNomeIntTrattLen(((short)0));
        ws.getRappAna().setRanNomeIntTratt("");
        ws.getRappAna().setRanCfIntRid("");
        ws.getRappAna().setRanFlCoincDipCntr(Types.SPACE_CHAR);
        ws.getRappAna().setRanFlCoincIntCntr(Types.SPACE_CHAR);
        ws.getRappAna().getRanDtDeces().setRanDtDeces(0);
        ws.getRappAna().setRanFlFumatore(Types.SPACE_CHAR);
        ws.getRappAna().setRanDsRiga(0);
        ws.getRappAna().setRanDsOperSql(Types.SPACE_CHAR);
        ws.getRappAna().setRanDsVer(0);
        ws.getRappAna().setRanDsTsIniCptz(0);
        ws.getRappAna().setRanDsTsEndCptz(0);
        ws.getRappAna().setRanDsUtente("");
        ws.getRappAna().setRanDsStatoElab(Types.SPACE_CHAR);
        ws.getRappAna().setRanFlLavDip(Types.SPACE_CHAR);
        ws.getRappAna().setRanTpVarzPagat("");
        ws.getRappAna().setRanCodRid("");
        ws.getRappAna().setRanTpCausRid("");
        ws.getRappAna().setRanIndMassaCorp("");
        ws.getRappAna().setRanCatRshProf("");
    }
}
