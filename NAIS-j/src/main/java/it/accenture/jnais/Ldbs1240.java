package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.jdbc.FieldNotMappedException;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.RappAnaDao;
import it.accenture.jnais.commons.data.to.IRappAna;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ldbs1240Data;
import it.accenture.jnais.ws.RappAnaLdbs1240;
import it.accenture.jnais.ws.redefines.RanDtDeces;
import it.accenture.jnais.ws.redefines.RanDtDeliberaCda;
import it.accenture.jnais.ws.redefines.RanDtNasc;
import it.accenture.jnais.ws.redefines.RanIdMoviChiu;
import it.accenture.jnais.ws.redefines.RanIdRappAnaCollg;
import it.accenture.jnais.ws.redefines.RanPcNelRapp;

/**Original name: LDBS1240<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  20 LUG 2010.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO AD HOC PER ACCESSO RISORSE DB        *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Ldbs1240 extends Program implements IRappAna {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private RappAnaDao rappAnaDao = new RappAnaDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Ldbs1240Data ws = new Ldbs1240Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: RAPP-ANA
    private RappAnaLdbs1240 rappAna;

    //==== METHODS ====
    /**Original name: PROGRAM_LDBS1240_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, RappAnaLdbs1240 rappAna) {
        this.idsv0003 = idsv0003;
        this.rappAna = rappAna;
        // COB_CODE: MOVE IDSV0003-BUFFER-WHERE-COND TO LDBV1241.
        ws.getLdbv1241().setLdbv1241Formatted(this.idsv0003.getBufferWhereCondFormatted());
        // COB_CODE: PERFORM A000-INIZIO              THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                        a200ElaboraWcEff();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                        b200ElaboraWcCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //               SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                        c200ElaboraWcNst();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: MOVE LDBV1241 TO IDSV0003-BUFFER-WHERE-COND.
        this.idsv0003.setBufferWhereCond(ws.getLdbv1241().getLdbv1241Formatted());
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Ldbs1240 getInstance() {
        return ((Ldbs1240)Programs.getInstance(Ldbs1240.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'LDBS1240'              TO   IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("LDBS1240");
        // COB_CODE: MOVE 'RAPP-ANA' TO   IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("RAPP-ANA");
        // COB_CODE: MOVE '00'                      TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                    TO   IDSV0003-SQLCODE
        //                                               IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                    TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                               IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-WC-EFF<br>*/
    private void a200ElaboraWcEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-WC-EFF          THRU A210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-WC-EFF          THRU A210-EX
            a210SelectWcEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
            a260OpenCursorWcEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
            a270CloseCursorWcEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
            a280FetchFirstWcEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
            a290FetchNextWcEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B200-ELABORA-WC-CPZ<br>*/
    private void b200ElaboraWcCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
            b210SelectWcCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
            b260OpenCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
            b270CloseCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
            b280FetchFirstWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
            b290FetchNextWcCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: C200-ELABORA-WC-NST<br>*/
    private void c200ElaboraWcNst() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM C210-SELECT-WC-NST          THRU C210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM C210-SELECT-WC-NST          THRU C210-EX
            c210SelectWcNst();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
            c260OpenCursorWcNst();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
            c270CloseCursorWcNst();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
            c280FetchFirstWcNst();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
            c290FetchNextWcNst();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A205-DECLARE-CURSOR-WC-EFF<br>
	 * <pre>----
	 * ----  gestione WC Effetto
	 * ----</pre>*/
    private void a205DeclareCursorWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //              DECLARE C-EFF CURSOR FOR
        //              SELECT
        //                     ID_RAPP_ANA
        //                    ,ID_RAPP_ANA_COLLG
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,COD_SOGG
        //                    ,TP_RAPP_ANA
        //                    ,TP_PERS
        //                    ,SEX
        //                    ,DT_NASC
        //                    ,FL_ESTAS
        //                    ,INDIR_1
        //                    ,INDIR_2
        //                    ,INDIR_3
        //                    ,TP_UTLZ_INDIR_1
        //                    ,TP_UTLZ_INDIR_2
        //                    ,TP_UTLZ_INDIR_3
        //                    ,ESTR_CNT_CORR_ACCR
        //                    ,ESTR_CNT_CORR_ADD
        //                    ,ESTR_DOCTO
        //                    ,PC_NEL_RAPP
        //                    ,TP_MEZ_PAG_ADD
        //                    ,TP_MEZ_PAG_ACCR
        //                    ,COD_MATR
        //                    ,TP_ADEGZ
        //                    ,FL_TST_RSH
        //                    ,COD_AZ
        //                    ,IND_PRINC
        //                    ,DT_DELIBERA_CDA
        //                    ,DLG_AL_RISC
        //                    ,LEGALE_RAPPR_PRINC
        //                    ,TP_LEGALE_RAPPR
        //                    ,TP_IND_PRINC
        //                    ,TP_STAT_RID
        //                    ,NOME_INT_RID
        //                    ,COGN_INT_RID
        //                    ,COGN_INT_TRATT
        //                    ,NOME_INT_TRATT
        //                    ,CF_INT_RID
        //                    ,FL_COINC_DIP_CNTR
        //                    ,FL_COINC_INT_CNTR
        //                    ,DT_DECES
        //                    ,FL_FUMATORE
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,FL_LAV_DIP
        //                    ,TP_VARZ_PAGAT
        //                    ,COD_RID
        //                    ,TP_CAUS_RID
        //                    ,IND_MASSA_CORP
        //                    ,CAT_RSH_PROF
        //              FROM RAPP_ANA
        //              WHERE      ID_OGG          = :LDBV1241-ID-OGG
        //                    AND TP_OGG          = :LDBV1241-TP-OGG
        //                    AND TP_RAPP_ANA     = :LDBV1241-TP-RAPP-ANA
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A210-SELECT-WC-EFF<br>*/
    private void a210SelectWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                     ID_RAPP_ANA
        //                    ,ID_RAPP_ANA_COLLG
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,COD_SOGG
        //                    ,TP_RAPP_ANA
        //                    ,TP_PERS
        //                    ,SEX
        //                    ,DT_NASC
        //                    ,FL_ESTAS
        //                    ,INDIR_1
        //                    ,INDIR_2
        //                    ,INDIR_3
        //                    ,TP_UTLZ_INDIR_1
        //                    ,TP_UTLZ_INDIR_2
        //                    ,TP_UTLZ_INDIR_3
        //                    ,ESTR_CNT_CORR_ACCR
        //                    ,ESTR_CNT_CORR_ADD
        //                    ,ESTR_DOCTO
        //                    ,PC_NEL_RAPP
        //                    ,TP_MEZ_PAG_ADD
        //                    ,TP_MEZ_PAG_ACCR
        //                    ,COD_MATR
        //                    ,TP_ADEGZ
        //                    ,FL_TST_RSH
        //                    ,COD_AZ
        //                    ,IND_PRINC
        //                    ,DT_DELIBERA_CDA
        //                    ,DLG_AL_RISC
        //                    ,LEGALE_RAPPR_PRINC
        //                    ,TP_LEGALE_RAPPR
        //                    ,TP_IND_PRINC
        //                    ,TP_STAT_RID
        //                    ,NOME_INT_RID
        //                    ,COGN_INT_RID
        //                    ,COGN_INT_TRATT
        //                    ,NOME_INT_TRATT
        //                    ,CF_INT_RID
        //                    ,FL_COINC_DIP_CNTR
        //                    ,FL_COINC_INT_CNTR
        //                    ,DT_DECES
        //                    ,FL_FUMATORE
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,FL_LAV_DIP
        //                    ,TP_VARZ_PAGAT
        //                    ,COD_RID
        //                    ,TP_CAUS_RID
        //                    ,IND_MASSA_CORP
        //                    ,CAT_RSH_PROF
        //             INTO
        //                :RAN-ID-RAPP-ANA
        //               ,:RAN-ID-RAPP-ANA-COLLG
        //                :IND-RAN-ID-RAPP-ANA-COLLG
        //               ,:RAN-ID-OGG
        //               ,:RAN-TP-OGG
        //               ,:RAN-ID-MOVI-CRZ
        //               ,:RAN-ID-MOVI-CHIU
        //                :IND-RAN-ID-MOVI-CHIU
        //               ,:RAN-DT-INI-EFF-DB
        //               ,:RAN-DT-END-EFF-DB
        //               ,:RAN-COD-COMP-ANIA
        //               ,:RAN-COD-SOGG
        //                :IND-RAN-COD-SOGG
        //               ,:RAN-TP-RAPP-ANA
        //               ,:RAN-TP-PERS
        //                :IND-RAN-TP-PERS
        //               ,:RAN-SEX
        //                :IND-RAN-SEX
        //               ,:RAN-DT-NASC-DB
        //                :IND-RAN-DT-NASC
        //               ,:RAN-FL-ESTAS
        //                :IND-RAN-FL-ESTAS
        //               ,:RAN-INDIR-1
        //                :IND-RAN-INDIR-1
        //               ,:RAN-INDIR-2
        //                :IND-RAN-INDIR-2
        //               ,:RAN-INDIR-3
        //                :IND-RAN-INDIR-3
        //               ,:RAN-TP-UTLZ-INDIR-1
        //                :IND-RAN-TP-UTLZ-INDIR-1
        //               ,:RAN-TP-UTLZ-INDIR-2
        //                :IND-RAN-TP-UTLZ-INDIR-2
        //               ,:RAN-TP-UTLZ-INDIR-3
        //                :IND-RAN-TP-UTLZ-INDIR-3
        //               ,:RAN-ESTR-CNT-CORR-ACCR
        //                :IND-RAN-ESTR-CNT-CORR-ACCR
        //               ,:RAN-ESTR-CNT-CORR-ADD
        //                :IND-RAN-ESTR-CNT-CORR-ADD
        //               ,:RAN-ESTR-DOCTO
        //                :IND-RAN-ESTR-DOCTO
        //               ,:RAN-PC-NEL-RAPP
        //                :IND-RAN-PC-NEL-RAPP
        //               ,:RAN-TP-MEZ-PAG-ADD
        //                :IND-RAN-TP-MEZ-PAG-ADD
        //               ,:RAN-TP-MEZ-PAG-ACCR
        //                :IND-RAN-TP-MEZ-PAG-ACCR
        //               ,:RAN-COD-MATR
        //                :IND-RAN-COD-MATR
        //               ,:RAN-TP-ADEGZ
        //                :IND-RAN-TP-ADEGZ
        //               ,:RAN-FL-TST-RSH
        //                :IND-RAN-FL-TST-RSH
        //               ,:RAN-COD-AZ
        //                :IND-RAN-COD-AZ
        //               ,:RAN-IND-PRINC
        //                :IND-RAN-IND-PRINC
        //               ,:RAN-DT-DELIBERA-CDA-DB
        //                :IND-RAN-DT-DELIBERA-CDA
        //               ,:RAN-DLG-AL-RISC
        //                :IND-RAN-DLG-AL-RISC
        //               ,:RAN-LEGALE-RAPPR-PRINC
        //                :IND-RAN-LEGALE-RAPPR-PRINC
        //               ,:RAN-TP-LEGALE-RAPPR
        //                :IND-RAN-TP-LEGALE-RAPPR
        //               ,:RAN-TP-IND-PRINC
        //                :IND-RAN-TP-IND-PRINC
        //               ,:RAN-TP-STAT-RID
        //                :IND-RAN-TP-STAT-RID
        //               ,:RAN-NOME-INT-RID-VCHAR
        //                :IND-RAN-NOME-INT-RID
        //               ,:RAN-COGN-INT-RID-VCHAR
        //                :IND-RAN-COGN-INT-RID
        //               ,:RAN-COGN-INT-TRATT-VCHAR
        //                :IND-RAN-COGN-INT-TRATT
        //               ,:RAN-NOME-INT-TRATT-VCHAR
        //                :IND-RAN-NOME-INT-TRATT
        //               ,:RAN-CF-INT-RID
        //                :IND-RAN-CF-INT-RID
        //               ,:RAN-FL-COINC-DIP-CNTR
        //                :IND-RAN-FL-COINC-DIP-CNTR
        //               ,:RAN-FL-COINC-INT-CNTR
        //                :IND-RAN-FL-COINC-INT-CNTR
        //               ,:RAN-DT-DECES-DB
        //                :IND-RAN-DT-DECES
        //               ,:RAN-FL-FUMATORE
        //                :IND-RAN-FL-FUMATORE
        //               ,:RAN-DS-RIGA
        //               ,:RAN-DS-OPER-SQL
        //               ,:RAN-DS-VER
        //               ,:RAN-DS-TS-INI-CPTZ
        //               ,:RAN-DS-TS-END-CPTZ
        //               ,:RAN-DS-UTENTE
        //               ,:RAN-DS-STATO-ELAB
        //               ,:RAN-FL-LAV-DIP
        //                :IND-RAN-FL-LAV-DIP
        //               ,:RAN-TP-VARZ-PAGAT
        //                :IND-RAN-TP-VARZ-PAGAT
        //               ,:RAN-COD-RID
        //                :IND-RAN-COD-RID
        //               ,:RAN-TP-CAUS-RID
        //                :IND-RAN-TP-CAUS-RID
        //               ,:RAN-IND-MASSA-CORP
        //                :IND-RAN-IND-MASSA-CORP
        //               ,:RAN-CAT-RSH-PROF
        //                :IND-RAN-CAT-RSH-PROF
        //             FROM RAPP_ANA
        //             WHERE      ID_OGG          = :LDBV1241-ID-OGG
        //                    AND TP_OGG          = :LDBV1241-TP-OGG
        //                    AND TP_RAPP_ANA     = :LDBV1241-TP-RAPP-ANA
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        rappAnaDao.selectRec4(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: A260-OPEN-CURSOR-WC-EFF<br>*/
    private void a260OpenCursorWcEff() {
        // COB_CODE: PERFORM A205-DECLARE-CURSOR-WC-EFF THRU A205-EX.
        a205DeclareCursorWcEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-EFF
        //           END-EXEC.
        rappAnaDao.openCEff4(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A270-CLOSE-CURSOR-WC-EFF<br>*/
    private void a270CloseCursorWcEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-EFF
        //           END-EXEC.
        rappAnaDao.closeCEff4();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A280-FETCH-FIRST-WC-EFF<br>*/
    private void a280FetchFirstWcEff() {
        // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF    THRU A260-EX.
        a260OpenCursorWcEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
            a290FetchNextWcEff();
        }
    }

    /**Original name: A290-FETCH-NEXT-WC-EFF<br>*/
    private void a290FetchNextWcEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-EFF
        //           INTO
        //                :RAN-ID-RAPP-ANA
        //               ,:RAN-ID-RAPP-ANA-COLLG
        //                :IND-RAN-ID-RAPP-ANA-COLLG
        //               ,:RAN-ID-OGG
        //               ,:RAN-TP-OGG
        //               ,:RAN-ID-MOVI-CRZ
        //               ,:RAN-ID-MOVI-CHIU
        //                :IND-RAN-ID-MOVI-CHIU
        //               ,:RAN-DT-INI-EFF-DB
        //               ,:RAN-DT-END-EFF-DB
        //               ,:RAN-COD-COMP-ANIA
        //               ,:RAN-COD-SOGG
        //                :IND-RAN-COD-SOGG
        //               ,:RAN-TP-RAPP-ANA
        //               ,:RAN-TP-PERS
        //                :IND-RAN-TP-PERS
        //               ,:RAN-SEX
        //                :IND-RAN-SEX
        //               ,:RAN-DT-NASC-DB
        //                :IND-RAN-DT-NASC
        //               ,:RAN-FL-ESTAS
        //                :IND-RAN-FL-ESTAS
        //               ,:RAN-INDIR-1
        //                :IND-RAN-INDIR-1
        //               ,:RAN-INDIR-2
        //                :IND-RAN-INDIR-2
        //               ,:RAN-INDIR-3
        //                :IND-RAN-INDIR-3
        //               ,:RAN-TP-UTLZ-INDIR-1
        //                :IND-RAN-TP-UTLZ-INDIR-1
        //               ,:RAN-TP-UTLZ-INDIR-2
        //                :IND-RAN-TP-UTLZ-INDIR-2
        //               ,:RAN-TP-UTLZ-INDIR-3
        //                :IND-RAN-TP-UTLZ-INDIR-3
        //               ,:RAN-ESTR-CNT-CORR-ACCR
        //                :IND-RAN-ESTR-CNT-CORR-ACCR
        //               ,:RAN-ESTR-CNT-CORR-ADD
        //                :IND-RAN-ESTR-CNT-CORR-ADD
        //               ,:RAN-ESTR-DOCTO
        //                :IND-RAN-ESTR-DOCTO
        //               ,:RAN-PC-NEL-RAPP
        //                :IND-RAN-PC-NEL-RAPP
        //               ,:RAN-TP-MEZ-PAG-ADD
        //                :IND-RAN-TP-MEZ-PAG-ADD
        //               ,:RAN-TP-MEZ-PAG-ACCR
        //                :IND-RAN-TP-MEZ-PAG-ACCR
        //               ,:RAN-COD-MATR
        //                :IND-RAN-COD-MATR
        //               ,:RAN-TP-ADEGZ
        //                :IND-RAN-TP-ADEGZ
        //               ,:RAN-FL-TST-RSH
        //                :IND-RAN-FL-TST-RSH
        //               ,:RAN-COD-AZ
        //                :IND-RAN-COD-AZ
        //               ,:RAN-IND-PRINC
        //                :IND-RAN-IND-PRINC
        //               ,:RAN-DT-DELIBERA-CDA-DB
        //                :IND-RAN-DT-DELIBERA-CDA
        //               ,:RAN-DLG-AL-RISC
        //                :IND-RAN-DLG-AL-RISC
        //               ,:RAN-LEGALE-RAPPR-PRINC
        //                :IND-RAN-LEGALE-RAPPR-PRINC
        //               ,:RAN-TP-LEGALE-RAPPR
        //                :IND-RAN-TP-LEGALE-RAPPR
        //               ,:RAN-TP-IND-PRINC
        //                :IND-RAN-TP-IND-PRINC
        //               ,:RAN-TP-STAT-RID
        //                :IND-RAN-TP-STAT-RID
        //               ,:RAN-NOME-INT-RID-VCHAR
        //                :IND-RAN-NOME-INT-RID
        //               ,:RAN-COGN-INT-RID-VCHAR
        //                :IND-RAN-COGN-INT-RID
        //               ,:RAN-COGN-INT-TRATT-VCHAR
        //                :IND-RAN-COGN-INT-TRATT
        //               ,:RAN-NOME-INT-TRATT-VCHAR
        //                :IND-RAN-NOME-INT-TRATT
        //               ,:RAN-CF-INT-RID
        //                :IND-RAN-CF-INT-RID
        //               ,:RAN-FL-COINC-DIP-CNTR
        //                :IND-RAN-FL-COINC-DIP-CNTR
        //               ,:RAN-FL-COINC-INT-CNTR
        //                :IND-RAN-FL-COINC-INT-CNTR
        //               ,:RAN-DT-DECES-DB
        //                :IND-RAN-DT-DECES
        //               ,:RAN-FL-FUMATORE
        //                :IND-RAN-FL-FUMATORE
        //               ,:RAN-DS-RIGA
        //               ,:RAN-DS-OPER-SQL
        //               ,:RAN-DS-VER
        //               ,:RAN-DS-TS-INI-CPTZ
        //               ,:RAN-DS-TS-END-CPTZ
        //               ,:RAN-DS-UTENTE
        //               ,:RAN-DS-STATO-ELAB
        //               ,:RAN-FL-LAV-DIP
        //                :IND-RAN-FL-LAV-DIP
        //               ,:RAN-TP-VARZ-PAGAT
        //                :IND-RAN-TP-VARZ-PAGAT
        //               ,:RAN-COD-RID
        //                :IND-RAN-COD-RID
        //               ,:RAN-TP-CAUS-RID
        //                :IND-RAN-TP-CAUS-RID
        //               ,:RAN-IND-MASSA-CORP
        //                :IND-RAN-IND-MASSA-CORP
        //               ,:RAN-CAT-RSH-PROF
        //                :IND-RAN-CAT-RSH-PROF
        //           END-EXEC.
        rappAnaDao.fetchCEff4(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF THRU A270-EX
            a270CloseCursorWcEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B205-DECLARE-CURSOR-WC-CPZ<br>
	 * <pre>----
	 * ----  gestione WC Competenza
	 * ----</pre>*/
    private void b205DeclareCursorWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //              DECLARE C-CPZ CURSOR FOR
        //              SELECT
        //                     ID_RAPP_ANA
        //                    ,ID_RAPP_ANA_COLLG
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,COD_SOGG
        //                    ,TP_RAPP_ANA
        //                    ,TP_PERS
        //                    ,SEX
        //                    ,DT_NASC
        //                    ,FL_ESTAS
        //                    ,INDIR_1
        //                    ,INDIR_2
        //                    ,INDIR_3
        //                    ,TP_UTLZ_INDIR_1
        //                    ,TP_UTLZ_INDIR_2
        //                    ,TP_UTLZ_INDIR_3
        //                    ,ESTR_CNT_CORR_ACCR
        //                    ,ESTR_CNT_CORR_ADD
        //                    ,ESTR_DOCTO
        //                    ,PC_NEL_RAPP
        //                    ,TP_MEZ_PAG_ADD
        //                    ,TP_MEZ_PAG_ACCR
        //                    ,COD_MATR
        //                    ,TP_ADEGZ
        //                    ,FL_TST_RSH
        //                    ,COD_AZ
        //                    ,IND_PRINC
        //                    ,DT_DELIBERA_CDA
        //                    ,DLG_AL_RISC
        //                    ,LEGALE_RAPPR_PRINC
        //                    ,TP_LEGALE_RAPPR
        //                    ,TP_IND_PRINC
        //                    ,TP_STAT_RID
        //                    ,NOME_INT_RID
        //                    ,COGN_INT_RID
        //                    ,COGN_INT_TRATT
        //                    ,NOME_INT_TRATT
        //                    ,CF_INT_RID
        //                    ,FL_COINC_DIP_CNTR
        //                    ,FL_COINC_INT_CNTR
        //                    ,DT_DECES
        //                    ,FL_FUMATORE
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,FL_LAV_DIP
        //                    ,TP_VARZ_PAGAT
        //                    ,COD_RID
        //                    ,TP_CAUS_RID
        //                    ,IND_MASSA_CORP
        //                    ,CAT_RSH_PROF
        //              FROM RAPP_ANA
        //              WHERE      ID_OGG          = :LDBV1241-ID-OGG
        //                        AND TP_OGG          = :LDBV1241-TP-OGG
        //                        AND TP_RAPP_ANA     = :LDBV1241-TP-RAPP-ANA
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B210-SELECT-WC-CPZ<br>*/
    private void b210SelectWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                     ID_RAPP_ANA
        //                    ,ID_RAPP_ANA_COLLG
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,COD_SOGG
        //                    ,TP_RAPP_ANA
        //                    ,TP_PERS
        //                    ,SEX
        //                    ,DT_NASC
        //                    ,FL_ESTAS
        //                    ,INDIR_1
        //                    ,INDIR_2
        //                    ,INDIR_3
        //                    ,TP_UTLZ_INDIR_1
        //                    ,TP_UTLZ_INDIR_2
        //                    ,TP_UTLZ_INDIR_3
        //                    ,ESTR_CNT_CORR_ACCR
        //                    ,ESTR_CNT_CORR_ADD
        //                    ,ESTR_DOCTO
        //                    ,PC_NEL_RAPP
        //                    ,TP_MEZ_PAG_ADD
        //                    ,TP_MEZ_PAG_ACCR
        //                    ,COD_MATR
        //                    ,TP_ADEGZ
        //                    ,FL_TST_RSH
        //                    ,COD_AZ
        //                    ,IND_PRINC
        //                    ,DT_DELIBERA_CDA
        //                    ,DLG_AL_RISC
        //                    ,LEGALE_RAPPR_PRINC
        //                    ,TP_LEGALE_RAPPR
        //                    ,TP_IND_PRINC
        //                    ,TP_STAT_RID
        //                    ,NOME_INT_RID
        //                    ,COGN_INT_RID
        //                    ,COGN_INT_TRATT
        //                    ,NOME_INT_TRATT
        //                    ,CF_INT_RID
        //                    ,FL_COINC_DIP_CNTR
        //                    ,FL_COINC_INT_CNTR
        //                    ,DT_DECES
        //                    ,FL_FUMATORE
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,FL_LAV_DIP
        //                    ,TP_VARZ_PAGAT
        //                    ,COD_RID
        //                    ,TP_CAUS_RID
        //                    ,IND_MASSA_CORP
        //                    ,CAT_RSH_PROF
        //             INTO
        //                :RAN-ID-RAPP-ANA
        //               ,:RAN-ID-RAPP-ANA-COLLG
        //                :IND-RAN-ID-RAPP-ANA-COLLG
        //               ,:RAN-ID-OGG
        //               ,:RAN-TP-OGG
        //               ,:RAN-ID-MOVI-CRZ
        //               ,:RAN-ID-MOVI-CHIU
        //                :IND-RAN-ID-MOVI-CHIU
        //               ,:RAN-DT-INI-EFF-DB
        //               ,:RAN-DT-END-EFF-DB
        //               ,:RAN-COD-COMP-ANIA
        //               ,:RAN-COD-SOGG
        //                :IND-RAN-COD-SOGG
        //               ,:RAN-TP-RAPP-ANA
        //               ,:RAN-TP-PERS
        //                :IND-RAN-TP-PERS
        //               ,:RAN-SEX
        //                :IND-RAN-SEX
        //               ,:RAN-DT-NASC-DB
        //                :IND-RAN-DT-NASC
        //               ,:RAN-FL-ESTAS
        //                :IND-RAN-FL-ESTAS
        //               ,:RAN-INDIR-1
        //                :IND-RAN-INDIR-1
        //               ,:RAN-INDIR-2
        //                :IND-RAN-INDIR-2
        //               ,:RAN-INDIR-3
        //                :IND-RAN-INDIR-3
        //               ,:RAN-TP-UTLZ-INDIR-1
        //                :IND-RAN-TP-UTLZ-INDIR-1
        //               ,:RAN-TP-UTLZ-INDIR-2
        //                :IND-RAN-TP-UTLZ-INDIR-2
        //               ,:RAN-TP-UTLZ-INDIR-3
        //                :IND-RAN-TP-UTLZ-INDIR-3
        //               ,:RAN-ESTR-CNT-CORR-ACCR
        //                :IND-RAN-ESTR-CNT-CORR-ACCR
        //               ,:RAN-ESTR-CNT-CORR-ADD
        //                :IND-RAN-ESTR-CNT-CORR-ADD
        //               ,:RAN-ESTR-DOCTO
        //                :IND-RAN-ESTR-DOCTO
        //               ,:RAN-PC-NEL-RAPP
        //                :IND-RAN-PC-NEL-RAPP
        //               ,:RAN-TP-MEZ-PAG-ADD
        //                :IND-RAN-TP-MEZ-PAG-ADD
        //               ,:RAN-TP-MEZ-PAG-ACCR
        //                :IND-RAN-TP-MEZ-PAG-ACCR
        //               ,:RAN-COD-MATR
        //                :IND-RAN-COD-MATR
        //               ,:RAN-TP-ADEGZ
        //                :IND-RAN-TP-ADEGZ
        //               ,:RAN-FL-TST-RSH
        //                :IND-RAN-FL-TST-RSH
        //               ,:RAN-COD-AZ
        //                :IND-RAN-COD-AZ
        //               ,:RAN-IND-PRINC
        //                :IND-RAN-IND-PRINC
        //               ,:RAN-DT-DELIBERA-CDA-DB
        //                :IND-RAN-DT-DELIBERA-CDA
        //               ,:RAN-DLG-AL-RISC
        //                :IND-RAN-DLG-AL-RISC
        //               ,:RAN-LEGALE-RAPPR-PRINC
        //                :IND-RAN-LEGALE-RAPPR-PRINC
        //               ,:RAN-TP-LEGALE-RAPPR
        //                :IND-RAN-TP-LEGALE-RAPPR
        //               ,:RAN-TP-IND-PRINC
        //                :IND-RAN-TP-IND-PRINC
        //               ,:RAN-TP-STAT-RID
        //                :IND-RAN-TP-STAT-RID
        //               ,:RAN-NOME-INT-RID-VCHAR
        //                :IND-RAN-NOME-INT-RID
        //               ,:RAN-COGN-INT-RID-VCHAR
        //                :IND-RAN-COGN-INT-RID
        //               ,:RAN-COGN-INT-TRATT-VCHAR
        //                :IND-RAN-COGN-INT-TRATT
        //               ,:RAN-NOME-INT-TRATT-VCHAR
        //                :IND-RAN-NOME-INT-TRATT
        //               ,:RAN-CF-INT-RID
        //                :IND-RAN-CF-INT-RID
        //               ,:RAN-FL-COINC-DIP-CNTR
        //                :IND-RAN-FL-COINC-DIP-CNTR
        //               ,:RAN-FL-COINC-INT-CNTR
        //                :IND-RAN-FL-COINC-INT-CNTR
        //               ,:RAN-DT-DECES-DB
        //                :IND-RAN-DT-DECES
        //               ,:RAN-FL-FUMATORE
        //                :IND-RAN-FL-FUMATORE
        //               ,:RAN-DS-RIGA
        //               ,:RAN-DS-OPER-SQL
        //               ,:RAN-DS-VER
        //               ,:RAN-DS-TS-INI-CPTZ
        //               ,:RAN-DS-TS-END-CPTZ
        //               ,:RAN-DS-UTENTE
        //               ,:RAN-DS-STATO-ELAB
        //               ,:RAN-FL-LAV-DIP
        //                :IND-RAN-FL-LAV-DIP
        //               ,:RAN-TP-VARZ-PAGAT
        //                :IND-RAN-TP-VARZ-PAGAT
        //               ,:RAN-COD-RID
        //                :IND-RAN-COD-RID
        //               ,:RAN-TP-CAUS-RID
        //                :IND-RAN-TP-CAUS-RID
        //               ,:RAN-IND-MASSA-CORP
        //                :IND-RAN-IND-MASSA-CORP
        //               ,:RAN-CAT-RSH-PROF
        //                :IND-RAN-CAT-RSH-PROF
        //             FROM RAPP_ANA
        //             WHERE      ID_OGG          = :LDBV1241-ID-OGG
        //                    AND TP_OGG          = :LDBV1241-TP-OGG
        //                    AND TP_RAPP_ANA     = :LDBV1241-TP-RAPP-ANA
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        rappAnaDao.selectRec5(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: B260-OPEN-CURSOR-WC-CPZ<br>*/
    private void b260OpenCursorWcCpz() {
        // COB_CODE: PERFORM B205-DECLARE-CURSOR-WC-CPZ THRU B205-EX.
        b205DeclareCursorWcCpz();
        // COB_CODE: EXEC SQL
        //                OPEN C-CPZ
        //           END-EXEC.
        rappAnaDao.openCCpz4(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B270-CLOSE-CURSOR-WC-CPZ<br>*/
    private void b270CloseCursorWcCpz() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-CPZ
        //           END-EXEC.
        rappAnaDao.closeCCpz4();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B280-FETCH-FIRST-WC-CPZ<br>*/
    private void b280FetchFirstWcCpz() {
        // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ    THRU B260-EX.
        b260OpenCursorWcCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
            b290FetchNextWcCpz();
        }
    }

    /**Original name: B290-FETCH-NEXT-WC-CPZ<br>*/
    private void b290FetchNextWcCpz() {
        // COB_CODE: EXEC SQL
        //                FETCH C-CPZ
        //           INTO
        //                :RAN-ID-RAPP-ANA
        //               ,:RAN-ID-RAPP-ANA-COLLG
        //                :IND-RAN-ID-RAPP-ANA-COLLG
        //               ,:RAN-ID-OGG
        //               ,:RAN-TP-OGG
        //               ,:RAN-ID-MOVI-CRZ
        //               ,:RAN-ID-MOVI-CHIU
        //                :IND-RAN-ID-MOVI-CHIU
        //               ,:RAN-DT-INI-EFF-DB
        //               ,:RAN-DT-END-EFF-DB
        //               ,:RAN-COD-COMP-ANIA
        //               ,:RAN-COD-SOGG
        //                :IND-RAN-COD-SOGG
        //               ,:RAN-TP-RAPP-ANA
        //               ,:RAN-TP-PERS
        //                :IND-RAN-TP-PERS
        //               ,:RAN-SEX
        //                :IND-RAN-SEX
        //               ,:RAN-DT-NASC-DB
        //                :IND-RAN-DT-NASC
        //               ,:RAN-FL-ESTAS
        //                :IND-RAN-FL-ESTAS
        //               ,:RAN-INDIR-1
        //                :IND-RAN-INDIR-1
        //               ,:RAN-INDIR-2
        //                :IND-RAN-INDIR-2
        //               ,:RAN-INDIR-3
        //                :IND-RAN-INDIR-3
        //               ,:RAN-TP-UTLZ-INDIR-1
        //                :IND-RAN-TP-UTLZ-INDIR-1
        //               ,:RAN-TP-UTLZ-INDIR-2
        //                :IND-RAN-TP-UTLZ-INDIR-2
        //               ,:RAN-TP-UTLZ-INDIR-3
        //                :IND-RAN-TP-UTLZ-INDIR-3
        //               ,:RAN-ESTR-CNT-CORR-ACCR
        //                :IND-RAN-ESTR-CNT-CORR-ACCR
        //               ,:RAN-ESTR-CNT-CORR-ADD
        //                :IND-RAN-ESTR-CNT-CORR-ADD
        //               ,:RAN-ESTR-DOCTO
        //                :IND-RAN-ESTR-DOCTO
        //               ,:RAN-PC-NEL-RAPP
        //                :IND-RAN-PC-NEL-RAPP
        //               ,:RAN-TP-MEZ-PAG-ADD
        //                :IND-RAN-TP-MEZ-PAG-ADD
        //               ,:RAN-TP-MEZ-PAG-ACCR
        //                :IND-RAN-TP-MEZ-PAG-ACCR
        //               ,:RAN-COD-MATR
        //                :IND-RAN-COD-MATR
        //               ,:RAN-TP-ADEGZ
        //                :IND-RAN-TP-ADEGZ
        //               ,:RAN-FL-TST-RSH
        //                :IND-RAN-FL-TST-RSH
        //               ,:RAN-COD-AZ
        //                :IND-RAN-COD-AZ
        //               ,:RAN-IND-PRINC
        //                :IND-RAN-IND-PRINC
        //               ,:RAN-DT-DELIBERA-CDA-DB
        //                :IND-RAN-DT-DELIBERA-CDA
        //               ,:RAN-DLG-AL-RISC
        //                :IND-RAN-DLG-AL-RISC
        //               ,:RAN-LEGALE-RAPPR-PRINC
        //                :IND-RAN-LEGALE-RAPPR-PRINC
        //               ,:RAN-TP-LEGALE-RAPPR
        //                :IND-RAN-TP-LEGALE-RAPPR
        //               ,:RAN-TP-IND-PRINC
        //                :IND-RAN-TP-IND-PRINC
        //               ,:RAN-TP-STAT-RID
        //                :IND-RAN-TP-STAT-RID
        //               ,:RAN-NOME-INT-RID-VCHAR
        //                :IND-RAN-NOME-INT-RID
        //               ,:RAN-COGN-INT-RID-VCHAR
        //                :IND-RAN-COGN-INT-RID
        //               ,:RAN-COGN-INT-TRATT-VCHAR
        //                :IND-RAN-COGN-INT-TRATT
        //               ,:RAN-NOME-INT-TRATT-VCHAR
        //                :IND-RAN-NOME-INT-TRATT
        //               ,:RAN-CF-INT-RID
        //                :IND-RAN-CF-INT-RID
        //               ,:RAN-FL-COINC-DIP-CNTR
        //                :IND-RAN-FL-COINC-DIP-CNTR
        //               ,:RAN-FL-COINC-INT-CNTR
        //                :IND-RAN-FL-COINC-INT-CNTR
        //               ,:RAN-DT-DECES-DB
        //                :IND-RAN-DT-DECES
        //               ,:RAN-FL-FUMATORE
        //                :IND-RAN-FL-FUMATORE
        //               ,:RAN-DS-RIGA
        //               ,:RAN-DS-OPER-SQL
        //               ,:RAN-DS-VER
        //               ,:RAN-DS-TS-INI-CPTZ
        //               ,:RAN-DS-TS-END-CPTZ
        //               ,:RAN-DS-UTENTE
        //               ,:RAN-DS-STATO-ELAB
        //               ,:RAN-FL-LAV-DIP
        //                :IND-RAN-FL-LAV-DIP
        //               ,:RAN-TP-VARZ-PAGAT
        //                :IND-RAN-TP-VARZ-PAGAT
        //               ,:RAN-COD-RID
        //                :IND-RAN-COD-RID
        //               ,:RAN-TP-CAUS-RID
        //                :IND-RAN-TP-CAUS-RID
        //               ,:RAN-IND-MASSA-CORP
        //                :IND-RAN-IND-MASSA-CORP
        //               ,:RAN-CAT-RSH-PROF
        //                :IND-RAN-CAT-RSH-PROF
        //           END-EXEC.
        rappAnaDao.fetchCCpz4(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ THRU B270-EX
            b270CloseCursorWcCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: C205-DECLARE-CURSOR-WC-NST<br>
	 * <pre>----
	 * ----  gestione WC Senza Storicità
	 * ----</pre>*/
    private void c205DeclareCursorWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C210-SELECT-WC-NST<br>*/
    private void c210SelectWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C260-OPEN-CURSOR-WC-NST<br>*/
    private void c260OpenCursorWcNst() {
        // COB_CODE: PERFORM C205-DECLARE-CURSOR-WC-NST THRU C205-EX.
        c205DeclareCursorWcNst();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C270-CLOSE-CURSOR-WC-NST<br>*/
    private void c270CloseCursorWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C280-FETCH-FIRST-WC-NST<br>*/
    private void c280FetchFirstWcNst() {
        // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST    THRU C260-EX.
        c260OpenCursorWcNst();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
            c290FetchNextWcNst();
        }
    }

    /**Original name: C290-FETCH-NEXT-WC-NST<br>*/
    private void c290FetchNextWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>----
	 * ----  utilità comuni a tutti i livelli operazione
	 * ----</pre>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-RAN-ID-RAPP-ANA-COLLG = -1
        //              MOVE HIGH-VALUES TO RAN-ID-RAPP-ANA-COLLG-NULL
        //           END-IF
        if (ws.getIndRappAna().getIdRappAnaCollg() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-ID-RAPP-ANA-COLLG-NULL
            rappAna.getRanIdRappAnaCollg().setRanIdRappAnaCollgNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RanIdRappAnaCollg.Len.RAN_ID_RAPP_ANA_COLLG_NULL));
        }
        // COB_CODE: IF IND-RAN-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO RAN-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndRappAna().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-ID-MOVI-CHIU-NULL
            rappAna.getRanIdMoviChiu().setRanIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RanIdMoviChiu.Len.RAN_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-RAN-COD-SOGG = -1
        //              MOVE HIGH-VALUES TO RAN-COD-SOGG-NULL
        //           END-IF
        if (ws.getIndRappAna().getCodSogg() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-COD-SOGG-NULL
            rappAna.setRanCodSogg(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_COD_SOGG));
        }
        // COB_CODE: IF IND-RAN-TP-PERS = -1
        //              MOVE HIGH-VALUES TO RAN-TP-PERS-NULL
        //           END-IF
        if (ws.getIndRappAna().getTpPers() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-TP-PERS-NULL
            rappAna.setRanTpPers(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-RAN-SEX = -1
        //              MOVE HIGH-VALUES TO RAN-SEX-NULL
        //           END-IF
        if (ws.getIndRappAna().getSex() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-SEX-NULL
            rappAna.setRanSex(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-RAN-DT-NASC = -1
        //              MOVE HIGH-VALUES TO RAN-DT-NASC-NULL
        //           END-IF
        if (ws.getIndRappAna().getDtNasc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-DT-NASC-NULL
            rappAna.getRanDtNasc().setRanDtNascNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RanDtNasc.Len.RAN_DT_NASC_NULL));
        }
        // COB_CODE: IF IND-RAN-FL-ESTAS = -1
        //              MOVE HIGH-VALUES TO RAN-FL-ESTAS-NULL
        //           END-IF
        if (ws.getIndRappAna().getFlEstas() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-FL-ESTAS-NULL
            rappAna.setRanFlEstas(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-RAN-INDIR-1 = -1
        //              MOVE HIGH-VALUES TO RAN-INDIR-1-NULL
        //           END-IF
        if (ws.getIndRappAna().getIndir1() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-INDIR-1-NULL
            rappAna.setRanIndir1(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_INDIR1));
        }
        // COB_CODE: IF IND-RAN-INDIR-2 = -1
        //              MOVE HIGH-VALUES TO RAN-INDIR-2-NULL
        //           END-IF
        if (ws.getIndRappAna().getIndir2() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-INDIR-2-NULL
            rappAna.setRanIndir2(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_INDIR2));
        }
        // COB_CODE: IF IND-RAN-INDIR-3 = -1
        //              MOVE HIGH-VALUES TO RAN-INDIR-3-NULL
        //           END-IF
        if (ws.getIndRappAna().getIndir3() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-INDIR-3-NULL
            rappAna.setRanIndir3(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_INDIR3));
        }
        // COB_CODE: IF IND-RAN-TP-UTLZ-INDIR-1 = -1
        //              MOVE HIGH-VALUES TO RAN-TP-UTLZ-INDIR-1-NULL
        //           END-IF
        if (ws.getIndRappAna().getTpUtlzIndir1() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-TP-UTLZ-INDIR-1-NULL
            rappAna.setRanTpUtlzIndir1(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_TP_UTLZ_INDIR1));
        }
        // COB_CODE: IF IND-RAN-TP-UTLZ-INDIR-2 = -1
        //              MOVE HIGH-VALUES TO RAN-TP-UTLZ-INDIR-2-NULL
        //           END-IF
        if (ws.getIndRappAna().getTpUtlzIndir2() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-TP-UTLZ-INDIR-2-NULL
            rappAna.setRanTpUtlzIndir2(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_TP_UTLZ_INDIR2));
        }
        // COB_CODE: IF IND-RAN-TP-UTLZ-INDIR-3 = -1
        //              MOVE HIGH-VALUES TO RAN-TP-UTLZ-INDIR-3-NULL
        //           END-IF
        if (ws.getIndRappAna().getTpUtlzIndir3() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-TP-UTLZ-INDIR-3-NULL
            rappAna.setRanTpUtlzIndir3(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_TP_UTLZ_INDIR3));
        }
        // COB_CODE: IF IND-RAN-ESTR-CNT-CORR-ACCR = -1
        //              MOVE HIGH-VALUES TO RAN-ESTR-CNT-CORR-ACCR-NULL
        //           END-IF
        if (ws.getIndRappAna().getEstrCntCorrAccr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-ESTR-CNT-CORR-ACCR-NULL
            rappAna.setRanEstrCntCorrAccr(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_ESTR_CNT_CORR_ACCR));
        }
        // COB_CODE: IF IND-RAN-ESTR-CNT-CORR-ADD = -1
        //              MOVE HIGH-VALUES TO RAN-ESTR-CNT-CORR-ADD-NULL
        //           END-IF
        if (ws.getIndRappAna().getEstrCntCorrAdd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-ESTR-CNT-CORR-ADD-NULL
            rappAna.setRanEstrCntCorrAdd(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_ESTR_CNT_CORR_ADD));
        }
        // COB_CODE: IF IND-RAN-ESTR-DOCTO = -1
        //              MOVE HIGH-VALUES TO RAN-ESTR-DOCTO-NULL
        //           END-IF
        if (ws.getIndRappAna().getEstrDocto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-ESTR-DOCTO-NULL
            rappAna.setRanEstrDocto(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_ESTR_DOCTO));
        }
        // COB_CODE: IF IND-RAN-PC-NEL-RAPP = -1
        //              MOVE HIGH-VALUES TO RAN-PC-NEL-RAPP-NULL
        //           END-IF
        if (ws.getIndRappAna().getPcNelRapp() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-PC-NEL-RAPP-NULL
            rappAna.getRanPcNelRapp().setRanPcNelRappNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RanPcNelRapp.Len.RAN_PC_NEL_RAPP_NULL));
        }
        // COB_CODE: IF IND-RAN-TP-MEZ-PAG-ADD = -1
        //              MOVE HIGH-VALUES TO RAN-TP-MEZ-PAG-ADD-NULL
        //           END-IF
        if (ws.getIndRappAna().getTpMezPagAdd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-TP-MEZ-PAG-ADD-NULL
            rappAna.setRanTpMezPagAdd(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_TP_MEZ_PAG_ADD));
        }
        // COB_CODE: IF IND-RAN-TP-MEZ-PAG-ACCR = -1
        //              MOVE HIGH-VALUES TO RAN-TP-MEZ-PAG-ACCR-NULL
        //           END-IF
        if (ws.getIndRappAna().getTpMezPagAccr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-TP-MEZ-PAG-ACCR-NULL
            rappAna.setRanTpMezPagAccr(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_TP_MEZ_PAG_ACCR));
        }
        // COB_CODE: IF IND-RAN-COD-MATR = -1
        //              MOVE HIGH-VALUES TO RAN-COD-MATR-NULL
        //           END-IF
        if (ws.getIndRappAna().getCodMatr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-COD-MATR-NULL
            rappAna.setRanCodMatr(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_COD_MATR));
        }
        // COB_CODE: IF IND-RAN-TP-ADEGZ = -1
        //              MOVE HIGH-VALUES TO RAN-TP-ADEGZ-NULL
        //           END-IF
        if (ws.getIndRappAna().getTpAdegz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-TP-ADEGZ-NULL
            rappAna.setRanTpAdegz(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_TP_ADEGZ));
        }
        // COB_CODE: IF IND-RAN-FL-TST-RSH = -1
        //              MOVE HIGH-VALUES TO RAN-FL-TST-RSH-NULL
        //           END-IF
        if (ws.getIndRappAna().getFlTstRsh() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-FL-TST-RSH-NULL
            rappAna.setRanFlTstRsh(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-RAN-COD-AZ = -1
        //              MOVE HIGH-VALUES TO RAN-COD-AZ-NULL
        //           END-IF
        if (ws.getIndRappAna().getCodAz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-COD-AZ-NULL
            rappAna.setRanCodAz(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_COD_AZ));
        }
        // COB_CODE: IF IND-RAN-IND-PRINC = -1
        //              MOVE HIGH-VALUES TO RAN-IND-PRINC-NULL
        //           END-IF
        if (ws.getIndRappAna().getIndPrinc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-IND-PRINC-NULL
            rappAna.setRanIndPrinc(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_IND_PRINC));
        }
        // COB_CODE: IF IND-RAN-DT-DELIBERA-CDA = -1
        //              MOVE HIGH-VALUES TO RAN-DT-DELIBERA-CDA-NULL
        //           END-IF
        if (ws.getIndRappAna().getDtDeliberaCda() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-DT-DELIBERA-CDA-NULL
            rappAna.getRanDtDeliberaCda().setRanDtDeliberaCdaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RanDtDeliberaCda.Len.RAN_DT_DELIBERA_CDA_NULL));
        }
        // COB_CODE: IF IND-RAN-DLG-AL-RISC = -1
        //              MOVE HIGH-VALUES TO RAN-DLG-AL-RISC-NULL
        //           END-IF
        if (ws.getIndRappAna().getDlgAlRisc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-DLG-AL-RISC-NULL
            rappAna.setRanDlgAlRisc(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-RAN-LEGALE-RAPPR-PRINC = -1
        //              MOVE HIGH-VALUES TO RAN-LEGALE-RAPPR-PRINC-NULL
        //           END-IF
        if (ws.getIndRappAna().getLegaleRapprPrinc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-LEGALE-RAPPR-PRINC-NULL
            rappAna.setRanLegaleRapprPrinc(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-RAN-TP-LEGALE-RAPPR = -1
        //              MOVE HIGH-VALUES TO RAN-TP-LEGALE-RAPPR-NULL
        //           END-IF
        if (ws.getIndRappAna().getTpLegaleRappr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-TP-LEGALE-RAPPR-NULL
            rappAna.setRanTpLegaleRappr(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_TP_LEGALE_RAPPR));
        }
        // COB_CODE: IF IND-RAN-TP-IND-PRINC = -1
        //              MOVE HIGH-VALUES TO RAN-TP-IND-PRINC-NULL
        //           END-IF
        if (ws.getIndRappAna().getTpIndPrinc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-TP-IND-PRINC-NULL
            rappAna.setRanTpIndPrinc(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_TP_IND_PRINC));
        }
        // COB_CODE: IF IND-RAN-TP-STAT-RID = -1
        //              MOVE HIGH-VALUES TO RAN-TP-STAT-RID-NULL
        //           END-IF
        if (ws.getIndRappAna().getTpStatRid() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-TP-STAT-RID-NULL
            rappAna.setRanTpStatRid(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_TP_STAT_RID));
        }
        // COB_CODE: IF IND-RAN-NOME-INT-RID = -1
        //              MOVE HIGH-VALUES TO RAN-NOME-INT-RID
        //           END-IF
        if (ws.getIndRappAna().getNomeIntRid() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-NOME-INT-RID
            rappAna.setRanNomeIntRid(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_NOME_INT_RID));
        }
        // COB_CODE: IF IND-RAN-COGN-INT-RID = -1
        //              MOVE HIGH-VALUES TO RAN-COGN-INT-RID
        //           END-IF
        if (ws.getIndRappAna().getCognIntRid() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-COGN-INT-RID
            rappAna.setRanCognIntRid(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_COGN_INT_RID));
        }
        // COB_CODE: IF IND-RAN-COGN-INT-TRATT = -1
        //              MOVE HIGH-VALUES TO RAN-COGN-INT-TRATT
        //           END-IF
        if (ws.getIndRappAna().getCognIntTratt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-COGN-INT-TRATT
            rappAna.setRanCognIntTratt(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_COGN_INT_TRATT));
        }
        // COB_CODE: IF IND-RAN-NOME-INT-TRATT = -1
        //              MOVE HIGH-VALUES TO RAN-NOME-INT-TRATT
        //           END-IF
        if (ws.getIndRappAna().getNomeIntTratt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-NOME-INT-TRATT
            rappAna.setRanNomeIntTratt(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_NOME_INT_TRATT));
        }
        // COB_CODE: IF IND-RAN-CF-INT-RID = -1
        //              MOVE HIGH-VALUES TO RAN-CF-INT-RID-NULL
        //           END-IF
        if (ws.getIndRappAna().getCfIntRid() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-CF-INT-RID-NULL
            rappAna.setRanCfIntRid(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_CF_INT_RID));
        }
        // COB_CODE: IF IND-RAN-FL-COINC-DIP-CNTR = -1
        //              MOVE HIGH-VALUES TO RAN-FL-COINC-DIP-CNTR-NULL
        //           END-IF
        if (ws.getIndRappAna().getFlCoincDipCntr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-FL-COINC-DIP-CNTR-NULL
            rappAna.setRanFlCoincDipCntr(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-RAN-FL-COINC-INT-CNTR = -1
        //              MOVE HIGH-VALUES TO RAN-FL-COINC-INT-CNTR-NULL
        //           END-IF
        if (ws.getIndRappAna().getFlCoincIntCntr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-FL-COINC-INT-CNTR-NULL
            rappAna.setRanFlCoincIntCntr(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-RAN-DT-DECES = -1
        //              MOVE HIGH-VALUES TO RAN-DT-DECES-NULL
        //           END-IF
        if (ws.getIndRappAna().getDtDeces() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-DT-DECES-NULL
            rappAna.getRanDtDeces().setRanDtDecesNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RanDtDeces.Len.RAN_DT_DECES_NULL));
        }
        // COB_CODE: IF IND-RAN-FL-FUMATORE = -1
        //              MOVE HIGH-VALUES TO RAN-FL-FUMATORE-NULL
        //           END-IF
        if (ws.getIndRappAna().getFlFumatore() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-FL-FUMATORE-NULL
            rappAna.setRanFlFumatore(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-RAN-FL-LAV-DIP = -1
        //              MOVE HIGH-VALUES TO RAN-FL-LAV-DIP-NULL
        //           END-IF
        if (ws.getIndRappAna().getFlLavDip() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-FL-LAV-DIP-NULL
            rappAna.setRanFlLavDip(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-RAN-TP-VARZ-PAGAT = -1
        //              MOVE HIGH-VALUES TO RAN-TP-VARZ-PAGAT-NULL
        //           END-IF
        if (ws.getIndRappAna().getTpVarzPagat() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-TP-VARZ-PAGAT-NULL
            rappAna.setRanTpVarzPagat(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_TP_VARZ_PAGAT));
        }
        // COB_CODE: IF IND-RAN-COD-RID = -1
        //              MOVE HIGH-VALUES TO RAN-COD-RID-NULL
        //           END-IF
        if (ws.getIndRappAna().getCodRid() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-COD-RID-NULL
            rappAna.setRanCodRid(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_COD_RID));
        }
        // COB_CODE: IF IND-RAN-TP-CAUS-RID = -1
        //              MOVE HIGH-VALUES TO RAN-TP-CAUS-RID-NULL
        //           END-IF
        if (ws.getIndRappAna().getTpCausRid() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-TP-CAUS-RID-NULL
            rappAna.setRanTpCausRid(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_TP_CAUS_RID));
        }
        // COB_CODE: IF IND-RAN-IND-MASSA-CORP = -1
        //              MOVE HIGH-VALUES TO RAN-IND-MASSA-CORP-NULL
        //           END-IF
        if (ws.getIndRappAna().getIndMassaCorp() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-IND-MASSA-CORP-NULL
            rappAna.setRanIndMassaCorp(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_IND_MASSA_CORP));
        }
        // COB_CODE: IF IND-RAN-CAT-RSH-PROF = -1
        //              MOVE HIGH-VALUES TO RAN-CAT-RSH-PROF-NULL
        //           END-IF.
        if (ws.getIndRappAna().getCatRshProf() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-CAT-RSH-PROF-NULL
            rappAna.setRanCatRshProf(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_CAT_RSH_PROF));
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE RAN-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getRappAnaDb().getIniEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO RAN-DT-INI-EFF
        rappAna.setRanDtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE RAN-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getRappAnaDb().getEndEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO RAN-DT-END-EFF
        rappAna.setRanDtEndEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-RAN-DT-NASC = 0
        //               MOVE WS-DATE-N      TO RAN-DT-NASC
        //           END-IF
        if (ws.getIndRappAna().getDtNasc() == 0) {
            // COB_CODE: MOVE RAN-DT-NASC-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getRappAnaDb().getIniCopDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO RAN-DT-NASC
            rappAna.getRanDtNasc().setRanDtNasc(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-RAN-DT-DELIBERA-CDA = 0
        //               MOVE WS-DATE-N      TO RAN-DT-DELIBERA-CDA
        //           END-IF
        if (ws.getIndRappAna().getDtDeliberaCda() == 0) {
            // COB_CODE: MOVE RAN-DT-DELIBERA-CDA-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getRappAnaDb().getEndCopDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO RAN-DT-DELIBERA-CDA
            rappAna.getRanDtDeliberaCda().setRanDtDeliberaCda(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-RAN-DT-DECES = 0
        //               MOVE WS-DATE-N      TO RAN-DT-DECES
        //           END-IF.
        if (ws.getIndRappAna().getDtDeces() == 0) {
            // COB_CODE: MOVE RAN-DT-DECES-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getRappAnaDb().getEsiTitDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO RAN-DT-DECES
            rappAna.getRanDtDeces().setRanDtDeces(ws.getIdsv0010().getWsDateN());
        }
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
        // COB_CODE: MOVE LENGTH OF RAN-NOME-INT-RID
        //                       TO RAN-NOME-INT-RID-LEN
        rappAna.setRanNomeIntRidLen(((short)RappAnaLdbs1240.Len.RAN_NOME_INT_RID));
        // COB_CODE: MOVE LENGTH OF RAN-COGN-INT-RID
        //                       TO RAN-COGN-INT-RID-LEN
        rappAna.setRanCognIntRidLen(((short)RappAnaLdbs1240.Len.RAN_COGN_INT_RID));
        // COB_CODE: MOVE LENGTH OF RAN-COGN-INT-TRATT
        //                       TO RAN-COGN-INT-TRATT-LEN
        rappAna.setRanCognIntTrattLen(((short)RappAnaLdbs1240.Len.RAN_COGN_INT_TRATT));
        // COB_CODE: MOVE LENGTH OF RAN-NOME-INT-TRATT
        //                       TO RAN-NOME-INT-TRATT-LEN.
        rappAna.setRanNomeIntTrattLen(((short)RappAnaLdbs1240.Len.RAN_NOME_INT_TRATT));
    }

    /**Original name: Z970-CODICE-ADHOC-PRE<br>
	 * <pre>----
	 * ----  prevede statements AD HOC PRE Query
	 * ----</pre>*/
    private void z970CodiceAdhocPre() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z980-CODICE-ADHOC-POST<br>
	 * <pre>----
	 * ----  prevede statements AD HOC POST Query
	 * ----</pre>*/
    private void z980CodiceAdhocPost() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public String getCatRshProf() {
        return rappAna.getRanCatRshProf();
    }

    @Override
    public void setCatRshProf(String catRshProf) {
        this.rappAna.setRanCatRshProf(catRshProf);
    }

    @Override
    public String getCatRshProfObj() {
        if (ws.getIndRappAna().getCatRshProf() >= 0) {
            return getCatRshProf();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCatRshProfObj(String catRshProfObj) {
        if (catRshProfObj != null) {
            setCatRshProf(catRshProfObj);
            ws.getIndRappAna().setCatRshProf(((short)0));
        }
        else {
            ws.getIndRappAna().setCatRshProf(((short)-1));
        }
    }

    @Override
    public String getCfIntRid() {
        return rappAna.getRanCfIntRid();
    }

    @Override
    public void setCfIntRid(String cfIntRid) {
        this.rappAna.setRanCfIntRid(cfIntRid);
    }

    @Override
    public String getCfIntRidObj() {
        if (ws.getIndRappAna().getCfIntRid() >= 0) {
            return getCfIntRid();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCfIntRidObj(String cfIntRidObj) {
        if (cfIntRidObj != null) {
            setCfIntRid(cfIntRidObj);
            ws.getIndRappAna().setCfIntRid(((short)0));
        }
        else {
            ws.getIndRappAna().setCfIntRid(((short)-1));
        }
    }

    @Override
    public String getCodAz() {
        return rappAna.getRanCodAz();
    }

    @Override
    public void setCodAz(String codAz) {
        this.rappAna.setRanCodAz(codAz);
    }

    @Override
    public String getCodAzObj() {
        if (ws.getIndRappAna().getCodAz() >= 0) {
            return getCodAz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodAzObj(String codAzObj) {
        if (codAzObj != null) {
            setCodAz(codAzObj);
            ws.getIndRappAna().setCodAz(((short)0));
        }
        else {
            ws.getIndRappAna().setCodAz(((short)-1));
        }
    }

    @Override
    public int getCodCompAnia() {
        return rappAna.getRanCodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.rappAna.setRanCodCompAnia(codCompAnia);
    }

    @Override
    public String getCodMatr() {
        return rappAna.getRanCodMatr();
    }

    @Override
    public void setCodMatr(String codMatr) {
        this.rappAna.setRanCodMatr(codMatr);
    }

    @Override
    public String getCodMatrObj() {
        if (ws.getIndRappAna().getCodMatr() >= 0) {
            return getCodMatr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodMatrObj(String codMatrObj) {
        if (codMatrObj != null) {
            setCodMatr(codMatrObj);
            ws.getIndRappAna().setCodMatr(((short)0));
        }
        else {
            ws.getIndRappAna().setCodMatr(((short)-1));
        }
    }

    @Override
    public String getCodRid() {
        return rappAna.getRanCodRid();
    }

    @Override
    public void setCodRid(String codRid) {
        this.rappAna.setRanCodRid(codRid);
    }

    @Override
    public String getCodRidObj() {
        if (ws.getIndRappAna().getCodRid() >= 0) {
            return getCodRid();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodRidObj(String codRidObj) {
        if (codRidObj != null) {
            setCodRid(codRidObj);
            ws.getIndRappAna().setCodRid(((short)0));
        }
        else {
            ws.getIndRappAna().setCodRid(((short)-1));
        }
    }

    @Override
    public String getCognIntRidVchar() {
        return rappAna.getRanCognIntRidVcharFormatted();
    }

    @Override
    public void setCognIntRidVchar(String cognIntRidVchar) {
        this.rappAna.setRanCognIntRidVcharFormatted(cognIntRidVchar);
    }

    @Override
    public String getCognIntRidVcharObj() {
        if (ws.getIndRappAna().getCognIntRid() >= 0) {
            return getCognIntRidVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCognIntRidVcharObj(String cognIntRidVcharObj) {
        if (cognIntRidVcharObj != null) {
            setCognIntRidVchar(cognIntRidVcharObj);
            ws.getIndRappAna().setCognIntRid(((short)0));
        }
        else {
            ws.getIndRappAna().setCognIntRid(((short)-1));
        }
    }

    @Override
    public String getCognIntTrattVchar() {
        return rappAna.getRanCognIntTrattVcharFormatted();
    }

    @Override
    public void setCognIntTrattVchar(String cognIntTrattVchar) {
        this.rappAna.setRanCognIntTrattVcharFormatted(cognIntTrattVchar);
    }

    @Override
    public String getCognIntTrattVcharObj() {
        if (ws.getIndRappAna().getCognIntTratt() >= 0) {
            return getCognIntTrattVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCognIntTrattVcharObj(String cognIntTrattVcharObj) {
        if (cognIntTrattVcharObj != null) {
            setCognIntTrattVchar(cognIntTrattVcharObj);
            ws.getIndRappAna().setCognIntTratt(((short)0));
        }
        else {
            ws.getIndRappAna().setCognIntTratt(((short)-1));
        }
    }

    @Override
    public char getDlgAlRisc() {
        return rappAna.getRanDlgAlRisc();
    }

    @Override
    public void setDlgAlRisc(char dlgAlRisc) {
        this.rappAna.setRanDlgAlRisc(dlgAlRisc);
    }

    @Override
    public Character getDlgAlRiscObj() {
        if (ws.getIndRappAna().getDlgAlRisc() >= 0) {
            return ((Character)getDlgAlRisc());
        }
        else {
            return null;
        }
    }

    @Override
    public void setDlgAlRiscObj(Character dlgAlRiscObj) {
        if (dlgAlRiscObj != null) {
            setDlgAlRisc(((char)dlgAlRiscObj));
            ws.getIndRappAna().setDlgAlRisc(((short)0));
        }
        else {
            ws.getIndRappAna().setDlgAlRisc(((short)-1));
        }
    }

    @Override
    public char getDsOperSql() {
        return rappAna.getRanDsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.rappAna.setRanDsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return rappAna.getRanDsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.rappAna.setRanDsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsEndCptz() {
        return rappAna.getRanDsTsEndCptz();
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.rappAna.setRanDsTsEndCptz(dsTsEndCptz);
    }

    @Override
    public long getDsTsIniCptz() {
        return rappAna.getRanDsTsIniCptz();
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.rappAna.setRanDsTsIniCptz(dsTsIniCptz);
    }

    @Override
    public String getDsUtente() {
        return rappAna.getRanDsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.rappAna.setRanDsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return rappAna.getRanDsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.rappAna.setRanDsVer(dsVer);
    }

    @Override
    public String getDtDecesDb() {
        return ws.getRappAnaDb().getEsiTitDb();
    }

    @Override
    public void setDtDecesDb(String dtDecesDb) {
        this.ws.getRappAnaDb().setEsiTitDb(dtDecesDb);
    }

    @Override
    public String getDtDecesDbObj() {
        if (ws.getIndRappAna().getDtDeces() >= 0) {
            return getDtDecesDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtDecesDbObj(String dtDecesDbObj) {
        if (dtDecesDbObj != null) {
            setDtDecesDb(dtDecesDbObj);
            ws.getIndRappAna().setDtDeces(((short)0));
        }
        else {
            ws.getIndRappAna().setDtDeces(((short)-1));
        }
    }

    @Override
    public String getDtDeliberaCdaDb() {
        return ws.getRappAnaDb().getEndCopDb();
    }

    @Override
    public void setDtDeliberaCdaDb(String dtDeliberaCdaDb) {
        this.ws.getRappAnaDb().setEndCopDb(dtDeliberaCdaDb);
    }

    @Override
    public String getDtDeliberaCdaDbObj() {
        if (ws.getIndRappAna().getDtDeliberaCda() >= 0) {
            return getDtDeliberaCdaDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtDeliberaCdaDbObj(String dtDeliberaCdaDbObj) {
        if (dtDeliberaCdaDbObj != null) {
            setDtDeliberaCdaDb(dtDeliberaCdaDbObj);
            ws.getIndRappAna().setDtDeliberaCda(((short)0));
        }
        else {
            ws.getIndRappAna().setDtDeliberaCda(((short)-1));
        }
    }

    @Override
    public String getDtEndEffDb() {
        return ws.getRappAnaDb().getEndEffDb();
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        this.ws.getRappAnaDb().setEndEffDb(dtEndEffDb);
    }

    @Override
    public String getDtIniEffDb() {
        return ws.getRappAnaDb().getIniEffDb();
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        this.ws.getRappAnaDb().setIniEffDb(dtIniEffDb);
    }

    @Override
    public String getDtNascDb() {
        return ws.getRappAnaDb().getIniCopDb();
    }

    @Override
    public void setDtNascDb(String dtNascDb) {
        this.ws.getRappAnaDb().setIniCopDb(dtNascDb);
    }

    @Override
    public String getDtNascDbObj() {
        if (ws.getIndRappAna().getDtNasc() >= 0) {
            return getDtNascDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtNascDbObj(String dtNascDbObj) {
        if (dtNascDbObj != null) {
            setDtNascDb(dtNascDbObj);
            ws.getIndRappAna().setDtNasc(((short)0));
        }
        else {
            ws.getIndRappAna().setDtNasc(((short)-1));
        }
    }

    @Override
    public String getEstrCntCorrAccr() {
        return rappAna.getRanEstrCntCorrAccr();
    }

    @Override
    public void setEstrCntCorrAccr(String estrCntCorrAccr) {
        this.rappAna.setRanEstrCntCorrAccr(estrCntCorrAccr);
    }

    @Override
    public String getEstrCntCorrAccrObj() {
        if (ws.getIndRappAna().getEstrCntCorrAccr() >= 0) {
            return getEstrCntCorrAccr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setEstrCntCorrAccrObj(String estrCntCorrAccrObj) {
        if (estrCntCorrAccrObj != null) {
            setEstrCntCorrAccr(estrCntCorrAccrObj);
            ws.getIndRappAna().setEstrCntCorrAccr(((short)0));
        }
        else {
            ws.getIndRappAna().setEstrCntCorrAccr(((short)-1));
        }
    }

    @Override
    public String getEstrCntCorrAdd() {
        return rappAna.getRanEstrCntCorrAdd();
    }

    @Override
    public void setEstrCntCorrAdd(String estrCntCorrAdd) {
        this.rappAna.setRanEstrCntCorrAdd(estrCntCorrAdd);
    }

    @Override
    public String getEstrCntCorrAddObj() {
        if (ws.getIndRappAna().getEstrCntCorrAdd() >= 0) {
            return getEstrCntCorrAdd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setEstrCntCorrAddObj(String estrCntCorrAddObj) {
        if (estrCntCorrAddObj != null) {
            setEstrCntCorrAdd(estrCntCorrAddObj);
            ws.getIndRappAna().setEstrCntCorrAdd(((short)0));
        }
        else {
            ws.getIndRappAna().setEstrCntCorrAdd(((short)-1));
        }
    }

    @Override
    public String getEstrDocto() {
        return rappAna.getRanEstrDocto();
    }

    @Override
    public void setEstrDocto(String estrDocto) {
        this.rappAna.setRanEstrDocto(estrDocto);
    }

    @Override
    public String getEstrDoctoObj() {
        if (ws.getIndRappAna().getEstrDocto() >= 0) {
            return getEstrDocto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setEstrDoctoObj(String estrDoctoObj) {
        if (estrDoctoObj != null) {
            setEstrDocto(estrDoctoObj);
            ws.getIndRappAna().setEstrDocto(((short)0));
        }
        else {
            ws.getIndRappAna().setEstrDocto(((short)-1));
        }
    }

    @Override
    public char getFlCoincDipCntr() {
        return rappAna.getRanFlCoincDipCntr();
    }

    @Override
    public void setFlCoincDipCntr(char flCoincDipCntr) {
        this.rappAna.setRanFlCoincDipCntr(flCoincDipCntr);
    }

    @Override
    public Character getFlCoincDipCntrObj() {
        if (ws.getIndRappAna().getFlCoincDipCntr() >= 0) {
            return ((Character)getFlCoincDipCntr());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlCoincDipCntrObj(Character flCoincDipCntrObj) {
        if (flCoincDipCntrObj != null) {
            setFlCoincDipCntr(((char)flCoincDipCntrObj));
            ws.getIndRappAna().setFlCoincDipCntr(((short)0));
        }
        else {
            ws.getIndRappAna().setFlCoincDipCntr(((short)-1));
        }
    }

    @Override
    public char getFlCoincIntCntr() {
        return rappAna.getRanFlCoincIntCntr();
    }

    @Override
    public void setFlCoincIntCntr(char flCoincIntCntr) {
        this.rappAna.setRanFlCoincIntCntr(flCoincIntCntr);
    }

    @Override
    public Character getFlCoincIntCntrObj() {
        if (ws.getIndRappAna().getFlCoincIntCntr() >= 0) {
            return ((Character)getFlCoincIntCntr());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlCoincIntCntrObj(Character flCoincIntCntrObj) {
        if (flCoincIntCntrObj != null) {
            setFlCoincIntCntr(((char)flCoincIntCntrObj));
            ws.getIndRappAna().setFlCoincIntCntr(((short)0));
        }
        else {
            ws.getIndRappAna().setFlCoincIntCntr(((short)-1));
        }
    }

    @Override
    public char getFlEstas() {
        return rappAna.getRanFlEstas();
    }

    @Override
    public void setFlEstas(char flEstas) {
        this.rappAna.setRanFlEstas(flEstas);
    }

    @Override
    public Character getFlEstasObj() {
        if (ws.getIndRappAna().getFlEstas() >= 0) {
            return ((Character)getFlEstas());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlEstasObj(Character flEstasObj) {
        if (flEstasObj != null) {
            setFlEstas(((char)flEstasObj));
            ws.getIndRappAna().setFlEstas(((short)0));
        }
        else {
            ws.getIndRappAna().setFlEstas(((short)-1));
        }
    }

    @Override
    public char getFlFumatore() {
        return rappAna.getRanFlFumatore();
    }

    @Override
    public void setFlFumatore(char flFumatore) {
        this.rappAna.setRanFlFumatore(flFumatore);
    }

    @Override
    public Character getFlFumatoreObj() {
        if (ws.getIndRappAna().getFlFumatore() >= 0) {
            return ((Character)getFlFumatore());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlFumatoreObj(Character flFumatoreObj) {
        if (flFumatoreObj != null) {
            setFlFumatore(((char)flFumatoreObj));
            ws.getIndRappAna().setFlFumatore(((short)0));
        }
        else {
            ws.getIndRappAna().setFlFumatore(((short)-1));
        }
    }

    @Override
    public char getFlLavDip() {
        return rappAna.getRanFlLavDip();
    }

    @Override
    public void setFlLavDip(char flLavDip) {
        this.rappAna.setRanFlLavDip(flLavDip);
    }

    @Override
    public Character getFlLavDipObj() {
        if (ws.getIndRappAna().getFlLavDip() >= 0) {
            return ((Character)getFlLavDip());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlLavDipObj(Character flLavDipObj) {
        if (flLavDipObj != null) {
            setFlLavDip(((char)flLavDipObj));
            ws.getIndRappAna().setFlLavDip(((short)0));
        }
        else {
            ws.getIndRappAna().setFlLavDip(((short)-1));
        }
    }

    @Override
    public char getFlTstRsh() {
        return rappAna.getRanFlTstRsh();
    }

    @Override
    public void setFlTstRsh(char flTstRsh) {
        this.rappAna.setRanFlTstRsh(flTstRsh);
    }

    @Override
    public Character getFlTstRshObj() {
        if (ws.getIndRappAna().getFlTstRsh() >= 0) {
            return ((Character)getFlTstRsh());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlTstRshObj(Character flTstRshObj) {
        if (flTstRshObj != null) {
            setFlTstRsh(((char)flTstRshObj));
            ws.getIndRappAna().setFlTstRsh(((short)0));
        }
        else {
            ws.getIndRappAna().setFlTstRsh(((short)-1));
        }
    }

    @Override
    public int getIdMoviChiu() {
        return rappAna.getRanIdMoviChiu().getRanIdMoviChiu();
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        this.rappAna.getRanIdMoviChiu().setRanIdMoviChiu(idMoviChiu);
    }

    @Override
    public Integer getIdMoviChiuObj() {
        if (ws.getIndRappAna().getIdMoviChiu() >= 0) {
            return ((Integer)getIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        if (idMoviChiuObj != null) {
            setIdMoviChiu(((int)idMoviChiuObj));
            ws.getIndRappAna().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndRappAna().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getIdMoviCrz() {
        return rappAna.getRanIdMoviCrz();
    }

    @Override
    public void setIdMoviCrz(int idMoviCrz) {
        this.rappAna.setRanIdMoviCrz(idMoviCrz);
    }

    @Override
    public int getIdRappAna() {
        return rappAna.getRanIdRappAna();
    }

    @Override
    public void setIdRappAna(int idRappAna) {
        this.rappAna.setRanIdRappAna(idRappAna);
    }

    @Override
    public int getIdRappAnaCollg() {
        return rappAna.getRanIdRappAnaCollg().getRanIdRappAnaCollg();
    }

    @Override
    public void setIdRappAnaCollg(int idRappAnaCollg) {
        this.rappAna.getRanIdRappAnaCollg().setRanIdRappAnaCollg(idRappAnaCollg);
    }

    @Override
    public Integer getIdRappAnaCollgObj() {
        if (ws.getIndRappAna().getIdRappAnaCollg() >= 0) {
            return ((Integer)getIdRappAnaCollg());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdRappAnaCollgObj(Integer idRappAnaCollgObj) {
        if (idRappAnaCollgObj != null) {
            setIdRappAnaCollg(((int)idRappAnaCollgObj));
            ws.getIndRappAna().setIdRappAnaCollg(((short)0));
        }
        else {
            ws.getIndRappAna().setIdRappAnaCollg(((short)-1));
        }
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        return idsv0003.getCodiceCompagniaAnia();
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        this.idsv0003.setCodiceCompagniaAnia(idsv0003CodiceCompagniaAnia);
    }

    @Override
    public String getIndMassaCorp() {
        return rappAna.getRanIndMassaCorp();
    }

    @Override
    public void setIndMassaCorp(String indMassaCorp) {
        this.rappAna.setRanIndMassaCorp(indMassaCorp);
    }

    @Override
    public String getIndMassaCorpObj() {
        if (ws.getIndRappAna().getIndMassaCorp() >= 0) {
            return getIndMassaCorp();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIndMassaCorpObj(String indMassaCorpObj) {
        if (indMassaCorpObj != null) {
            setIndMassaCorp(indMassaCorpObj);
            ws.getIndRappAna().setIndMassaCorp(((short)0));
        }
        else {
            ws.getIndRappAna().setIndMassaCorp(((short)-1));
        }
    }

    @Override
    public String getIndPrinc() {
        return rappAna.getRanIndPrinc();
    }

    @Override
    public void setIndPrinc(String indPrinc) {
        this.rappAna.setRanIndPrinc(indPrinc);
    }

    @Override
    public String getIndPrincObj() {
        if (ws.getIndRappAna().getIndPrinc() >= 0) {
            return getIndPrinc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIndPrincObj(String indPrincObj) {
        if (indPrincObj != null) {
            setIndPrinc(indPrincObj);
            ws.getIndRappAna().setIndPrinc(((short)0));
        }
        else {
            ws.getIndRappAna().setIndPrinc(((short)-1));
        }
    }

    @Override
    public String getIndir1() {
        return rappAna.getRanIndir1();
    }

    @Override
    public void setIndir1(String indir1) {
        this.rappAna.setRanIndir1(indir1);
    }

    @Override
    public String getIndir1Obj() {
        if (ws.getIndRappAna().getIndir1() >= 0) {
            return getIndir1();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIndir1Obj(String indir1Obj) {
        if (indir1Obj != null) {
            setIndir1(indir1Obj);
            ws.getIndRappAna().setIndir1(((short)0));
        }
        else {
            ws.getIndRappAna().setIndir1(((short)-1));
        }
    }

    @Override
    public String getIndir2() {
        return rappAna.getRanIndir2();
    }

    @Override
    public void setIndir2(String indir2) {
        this.rappAna.setRanIndir2(indir2);
    }

    @Override
    public String getIndir2Obj() {
        if (ws.getIndRappAna().getIndir2() >= 0) {
            return getIndir2();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIndir2Obj(String indir2Obj) {
        if (indir2Obj != null) {
            setIndir2(indir2Obj);
            ws.getIndRappAna().setIndir2(((short)0));
        }
        else {
            ws.getIndRappAna().setIndir2(((short)-1));
        }
    }

    @Override
    public String getIndir3() {
        return rappAna.getRanIndir3();
    }

    @Override
    public void setIndir3(String indir3) {
        this.rappAna.setRanIndir3(indir3);
    }

    @Override
    public String getIndir3Obj() {
        if (ws.getIndRappAna().getIndir3() >= 0) {
            return getIndir3();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIndir3Obj(String indir3Obj) {
        if (indir3Obj != null) {
            setIndir3(indir3Obj);
            ws.getIndRappAna().setIndir3(((short)0));
        }
        else {
            ws.getIndRappAna().setIndir3(((short)-1));
        }
    }

    @Override
    public int getLdbv1241IdOgg() {
        return ws.getLdbv1241().getIdOgg();
    }

    @Override
    public void setLdbv1241IdOgg(int ldbv1241IdOgg) {
        this.ws.getLdbv1241().setIdOgg(ldbv1241IdOgg);
    }

    @Override
    public String getLdbv1241TpOgg() {
        return ws.getLdbv1241().getTpOgg();
    }

    @Override
    public void setLdbv1241TpOgg(String ldbv1241TpOgg) {
        this.ws.getLdbv1241().setTpOgg(ldbv1241TpOgg);
    }

    @Override
    public String getLdbv1241TpRappAna() {
        return ws.getLdbv1241().getTpRappAna();
    }

    @Override
    public void setLdbv1241TpRappAna(String ldbv1241TpRappAna) {
        this.ws.getLdbv1241().setTpRappAna(ldbv1241TpRappAna);
    }

    @Override
    public int getLdbv1291IdOgg() {
        throw new FieldNotMappedException("ldbv1291IdOgg");
    }

    @Override
    public void setLdbv1291IdOgg(int ldbv1291IdOgg) {
        throw new FieldNotMappedException("ldbv1291IdOgg");
    }

    @Override
    public String getLdbv1291Tp10() {
        throw new FieldNotMappedException("ldbv1291Tp10");
    }

    @Override
    public void setLdbv1291Tp10(String ldbv1291Tp10) {
        throw new FieldNotMappedException("ldbv1291Tp10");
    }

    @Override
    public String getLdbv1291Tp11() {
        throw new FieldNotMappedException("ldbv1291Tp11");
    }

    @Override
    public void setLdbv1291Tp11(String ldbv1291Tp11) {
        throw new FieldNotMappedException("ldbv1291Tp11");
    }

    @Override
    public String getLdbv1291Tp1() {
        throw new FieldNotMappedException("ldbv1291Tp1");
    }

    @Override
    public void setLdbv1291Tp1(String ldbv1291Tp1) {
        throw new FieldNotMappedException("ldbv1291Tp1");
    }

    @Override
    public String getLdbv1291Tp2() {
        throw new FieldNotMappedException("ldbv1291Tp2");
    }

    @Override
    public void setLdbv1291Tp2(String ldbv1291Tp2) {
        throw new FieldNotMappedException("ldbv1291Tp2");
    }

    @Override
    public String getLdbv1291Tp3() {
        throw new FieldNotMappedException("ldbv1291Tp3");
    }

    @Override
    public void setLdbv1291Tp3(String ldbv1291Tp3) {
        throw new FieldNotMappedException("ldbv1291Tp3");
    }

    @Override
    public String getLdbv1291Tp4() {
        throw new FieldNotMappedException("ldbv1291Tp4");
    }

    @Override
    public void setLdbv1291Tp4(String ldbv1291Tp4) {
        throw new FieldNotMappedException("ldbv1291Tp4");
    }

    @Override
    public String getLdbv1291Tp5() {
        throw new FieldNotMappedException("ldbv1291Tp5");
    }

    @Override
    public void setLdbv1291Tp5(String ldbv1291Tp5) {
        throw new FieldNotMappedException("ldbv1291Tp5");
    }

    @Override
    public String getLdbv1291Tp6() {
        throw new FieldNotMappedException("ldbv1291Tp6");
    }

    @Override
    public void setLdbv1291Tp6(String ldbv1291Tp6) {
        throw new FieldNotMappedException("ldbv1291Tp6");
    }

    @Override
    public String getLdbv1291Tp7() {
        throw new FieldNotMappedException("ldbv1291Tp7");
    }

    @Override
    public void setLdbv1291Tp7(String ldbv1291Tp7) {
        throw new FieldNotMappedException("ldbv1291Tp7");
    }

    @Override
    public String getLdbv1291Tp8() {
        throw new FieldNotMappedException("ldbv1291Tp8");
    }

    @Override
    public void setLdbv1291Tp8(String ldbv1291Tp8) {
        throw new FieldNotMappedException("ldbv1291Tp8");
    }

    @Override
    public String getLdbv1291Tp9() {
        throw new FieldNotMappedException("ldbv1291Tp9");
    }

    @Override
    public void setLdbv1291Tp9(String ldbv1291Tp9) {
        throw new FieldNotMappedException("ldbv1291Tp9");
    }

    @Override
    public String getLdbv1291TpOgg() {
        throw new FieldNotMappedException("ldbv1291TpOgg");
    }

    @Override
    public void setLdbv1291TpOgg(String ldbv1291TpOgg) {
        throw new FieldNotMappedException("ldbv1291TpOgg");
    }

    @Override
    public char getLegaleRapprPrinc() {
        return rappAna.getRanLegaleRapprPrinc();
    }

    @Override
    public void setLegaleRapprPrinc(char legaleRapprPrinc) {
        this.rappAna.setRanLegaleRapprPrinc(legaleRapprPrinc);
    }

    @Override
    public Character getLegaleRapprPrincObj() {
        if (ws.getIndRappAna().getLegaleRapprPrinc() >= 0) {
            return ((Character)getLegaleRapprPrinc());
        }
        else {
            return null;
        }
    }

    @Override
    public void setLegaleRapprPrincObj(Character legaleRapprPrincObj) {
        if (legaleRapprPrincObj != null) {
            setLegaleRapprPrinc(((char)legaleRapprPrincObj));
            ws.getIndRappAna().setLegaleRapprPrinc(((short)0));
        }
        else {
            ws.getIndRappAna().setLegaleRapprPrinc(((short)-1));
        }
    }

    @Override
    public String getNomeIntRidVchar() {
        return rappAna.getRanNomeIntRidVcharFormatted();
    }

    @Override
    public void setNomeIntRidVchar(String nomeIntRidVchar) {
        this.rappAna.setRanNomeIntRidVcharFormatted(nomeIntRidVchar);
    }

    @Override
    public String getNomeIntRidVcharObj() {
        if (ws.getIndRappAna().getNomeIntRid() >= 0) {
            return getNomeIntRidVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setNomeIntRidVcharObj(String nomeIntRidVcharObj) {
        if (nomeIntRidVcharObj != null) {
            setNomeIntRidVchar(nomeIntRidVcharObj);
            ws.getIndRappAna().setNomeIntRid(((short)0));
        }
        else {
            ws.getIndRappAna().setNomeIntRid(((short)-1));
        }
    }

    @Override
    public String getNomeIntTrattVchar() {
        return rappAna.getRanNomeIntTrattVcharFormatted();
    }

    @Override
    public void setNomeIntTrattVchar(String nomeIntTrattVchar) {
        this.rappAna.setRanNomeIntTrattVcharFormatted(nomeIntTrattVchar);
    }

    @Override
    public String getNomeIntTrattVcharObj() {
        if (ws.getIndRappAna().getNomeIntTratt() >= 0) {
            return getNomeIntTrattVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setNomeIntTrattVcharObj(String nomeIntTrattVcharObj) {
        if (nomeIntTrattVcharObj != null) {
            setNomeIntTrattVchar(nomeIntTrattVcharObj);
            ws.getIndRappAna().setNomeIntTratt(((short)0));
        }
        else {
            ws.getIndRappAna().setNomeIntTratt(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcNelRapp() {
        return rappAna.getRanPcNelRapp().getRanPcNelRapp();
    }

    @Override
    public void setPcNelRapp(AfDecimal pcNelRapp) {
        this.rappAna.getRanPcNelRapp().setRanPcNelRapp(pcNelRapp.copy());
    }

    @Override
    public AfDecimal getPcNelRappObj() {
        if (ws.getIndRappAna().getPcNelRapp() >= 0) {
            return getPcNelRapp();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcNelRappObj(AfDecimal pcNelRappObj) {
        if (pcNelRappObj != null) {
            setPcNelRapp(new AfDecimal(pcNelRappObj, 6, 3));
            ws.getIndRappAna().setPcNelRapp(((short)0));
        }
        else {
            ws.getIndRappAna().setPcNelRapp(((short)-1));
        }
    }

    @Override
    public String getRanCodSogg() {
        return rappAna.getRanCodSogg();
    }

    @Override
    public void setRanCodSogg(String ranCodSogg) {
        this.rappAna.setRanCodSogg(ranCodSogg);
    }

    @Override
    public String getRanCodSoggObj() {
        if (ws.getIndRappAna().getCodSogg() >= 0) {
            return getRanCodSogg();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRanCodSoggObj(String ranCodSoggObj) {
        if (ranCodSoggObj != null) {
            setRanCodSogg(ranCodSoggObj);
            ws.getIndRappAna().setCodSogg(((short)0));
        }
        else {
            ws.getIndRappAna().setCodSogg(((short)-1));
        }
    }

    @Override
    public long getRanDsRiga() {
        return rappAna.getRanDsRiga();
    }

    @Override
    public void setRanDsRiga(long ranDsRiga) {
        this.rappAna.setRanDsRiga(ranDsRiga);
    }

    @Override
    public int getRanIdOgg() {
        return rappAna.getRanIdOgg();
    }

    @Override
    public void setRanIdOgg(int ranIdOgg) {
        this.rappAna.setRanIdOgg(ranIdOgg);
    }

    @Override
    public String getRanTpOgg() {
        return rappAna.getRanTpOgg();
    }

    @Override
    public void setRanTpOgg(String ranTpOgg) {
        this.rappAna.setRanTpOgg(ranTpOgg);
    }

    @Override
    public String getRanTpRappAna() {
        return rappAna.getRanTpRappAna();
    }

    @Override
    public void setRanTpRappAna(String ranTpRappAna) {
        this.rappAna.setRanTpRappAna(ranTpRappAna);
    }

    @Override
    public char getSex() {
        return rappAna.getRanSex();
    }

    @Override
    public void setSex(char sex) {
        this.rappAna.setRanSex(sex);
    }

    @Override
    public Character getSexObj() {
        if (ws.getIndRappAna().getSex() >= 0) {
            return ((Character)getSex());
        }
        else {
            return null;
        }
    }

    @Override
    public void setSexObj(Character sexObj) {
        if (sexObj != null) {
            setSex(((char)sexObj));
            ws.getIndRappAna().setSex(((short)0));
        }
        else {
            ws.getIndRappAna().setSex(((short)-1));
        }
    }

    @Override
    public String getTpAdegz() {
        return rappAna.getRanTpAdegz();
    }

    @Override
    public void setTpAdegz(String tpAdegz) {
        this.rappAna.setRanTpAdegz(tpAdegz);
    }

    @Override
    public String getTpAdegzObj() {
        if (ws.getIndRappAna().getTpAdegz() >= 0) {
            return getTpAdegz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpAdegzObj(String tpAdegzObj) {
        if (tpAdegzObj != null) {
            setTpAdegz(tpAdegzObj);
            ws.getIndRappAna().setTpAdegz(((short)0));
        }
        else {
            ws.getIndRappAna().setTpAdegz(((short)-1));
        }
    }

    @Override
    public String getTpCausRid() {
        return rappAna.getRanTpCausRid();
    }

    @Override
    public void setTpCausRid(String tpCausRid) {
        this.rappAna.setRanTpCausRid(tpCausRid);
    }

    @Override
    public String getTpCausRidObj() {
        if (ws.getIndRappAna().getTpCausRid() >= 0) {
            return getTpCausRid();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpCausRidObj(String tpCausRidObj) {
        if (tpCausRidObj != null) {
            setTpCausRid(tpCausRidObj);
            ws.getIndRappAna().setTpCausRid(((short)0));
        }
        else {
            ws.getIndRappAna().setTpCausRid(((short)-1));
        }
    }

    @Override
    public String getTpIndPrinc() {
        return rappAna.getRanTpIndPrinc();
    }

    @Override
    public void setTpIndPrinc(String tpIndPrinc) {
        this.rappAna.setRanTpIndPrinc(tpIndPrinc);
    }

    @Override
    public String getTpIndPrincObj() {
        if (ws.getIndRappAna().getTpIndPrinc() >= 0) {
            return getTpIndPrinc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpIndPrincObj(String tpIndPrincObj) {
        if (tpIndPrincObj != null) {
            setTpIndPrinc(tpIndPrincObj);
            ws.getIndRappAna().setTpIndPrinc(((short)0));
        }
        else {
            ws.getIndRappAna().setTpIndPrinc(((short)-1));
        }
    }

    @Override
    public String getTpLegaleRappr() {
        return rappAna.getRanTpLegaleRappr();
    }

    @Override
    public void setTpLegaleRappr(String tpLegaleRappr) {
        this.rappAna.setRanTpLegaleRappr(tpLegaleRappr);
    }

    @Override
    public String getTpLegaleRapprObj() {
        if (ws.getIndRappAna().getTpLegaleRappr() >= 0) {
            return getTpLegaleRappr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpLegaleRapprObj(String tpLegaleRapprObj) {
        if (tpLegaleRapprObj != null) {
            setTpLegaleRappr(tpLegaleRapprObj);
            ws.getIndRappAna().setTpLegaleRappr(((short)0));
        }
        else {
            ws.getIndRappAna().setTpLegaleRappr(((short)-1));
        }
    }

    @Override
    public String getTpMezPagAccr() {
        return rappAna.getRanTpMezPagAccr();
    }

    @Override
    public void setTpMezPagAccr(String tpMezPagAccr) {
        this.rappAna.setRanTpMezPagAccr(tpMezPagAccr);
    }

    @Override
    public String getTpMezPagAccrObj() {
        if (ws.getIndRappAna().getTpMezPagAccr() >= 0) {
            return getTpMezPagAccr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpMezPagAccrObj(String tpMezPagAccrObj) {
        if (tpMezPagAccrObj != null) {
            setTpMezPagAccr(tpMezPagAccrObj);
            ws.getIndRappAna().setTpMezPagAccr(((short)0));
        }
        else {
            ws.getIndRappAna().setTpMezPagAccr(((short)-1));
        }
    }

    @Override
    public String getTpMezPagAdd() {
        return rappAna.getRanTpMezPagAdd();
    }

    @Override
    public void setTpMezPagAdd(String tpMezPagAdd) {
        this.rappAna.setRanTpMezPagAdd(tpMezPagAdd);
    }

    @Override
    public String getTpMezPagAddObj() {
        if (ws.getIndRappAna().getTpMezPagAdd() >= 0) {
            return getTpMezPagAdd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpMezPagAddObj(String tpMezPagAddObj) {
        if (tpMezPagAddObj != null) {
            setTpMezPagAdd(tpMezPagAddObj);
            ws.getIndRappAna().setTpMezPagAdd(((short)0));
        }
        else {
            ws.getIndRappAna().setTpMezPagAdd(((short)-1));
        }
    }

    @Override
    public char getTpPers() {
        return rappAna.getRanTpPers();
    }

    @Override
    public void setTpPers(char tpPers) {
        this.rappAna.setRanTpPers(tpPers);
    }

    @Override
    public Character getTpPersObj() {
        if (ws.getIndRappAna().getTpPers() >= 0) {
            return ((Character)getTpPers());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpPersObj(Character tpPersObj) {
        if (tpPersObj != null) {
            setTpPers(((char)tpPersObj));
            ws.getIndRappAna().setTpPers(((short)0));
        }
        else {
            ws.getIndRappAna().setTpPers(((short)-1));
        }
    }

    @Override
    public String getTpStatRid() {
        return rappAna.getRanTpStatRid();
    }

    @Override
    public void setTpStatRid(String tpStatRid) {
        this.rappAna.setRanTpStatRid(tpStatRid);
    }

    @Override
    public String getTpStatRidObj() {
        if (ws.getIndRappAna().getTpStatRid() >= 0) {
            return getTpStatRid();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpStatRidObj(String tpStatRidObj) {
        if (tpStatRidObj != null) {
            setTpStatRid(tpStatRidObj);
            ws.getIndRappAna().setTpStatRid(((short)0));
        }
        else {
            ws.getIndRappAna().setTpStatRid(((short)-1));
        }
    }

    @Override
    public String getTpUtlzIndir1() {
        return rappAna.getRanTpUtlzIndir1();
    }

    @Override
    public void setTpUtlzIndir1(String tpUtlzIndir1) {
        this.rappAna.setRanTpUtlzIndir1(tpUtlzIndir1);
    }

    @Override
    public String getTpUtlzIndir1Obj() {
        if (ws.getIndRappAna().getTpUtlzIndir1() >= 0) {
            return getTpUtlzIndir1();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpUtlzIndir1Obj(String tpUtlzIndir1Obj) {
        if (tpUtlzIndir1Obj != null) {
            setTpUtlzIndir1(tpUtlzIndir1Obj);
            ws.getIndRappAna().setTpUtlzIndir1(((short)0));
        }
        else {
            ws.getIndRappAna().setTpUtlzIndir1(((short)-1));
        }
    }

    @Override
    public String getTpUtlzIndir2() {
        return rappAna.getRanTpUtlzIndir2();
    }

    @Override
    public void setTpUtlzIndir2(String tpUtlzIndir2) {
        this.rappAna.setRanTpUtlzIndir2(tpUtlzIndir2);
    }

    @Override
    public String getTpUtlzIndir2Obj() {
        if (ws.getIndRappAna().getTpUtlzIndir2() >= 0) {
            return getTpUtlzIndir2();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpUtlzIndir2Obj(String tpUtlzIndir2Obj) {
        if (tpUtlzIndir2Obj != null) {
            setTpUtlzIndir2(tpUtlzIndir2Obj);
            ws.getIndRappAna().setTpUtlzIndir2(((short)0));
        }
        else {
            ws.getIndRappAna().setTpUtlzIndir2(((short)-1));
        }
    }

    @Override
    public String getTpUtlzIndir3() {
        return rappAna.getRanTpUtlzIndir3();
    }

    @Override
    public void setTpUtlzIndir3(String tpUtlzIndir3) {
        this.rappAna.setRanTpUtlzIndir3(tpUtlzIndir3);
    }

    @Override
    public String getTpUtlzIndir3Obj() {
        if (ws.getIndRappAna().getTpUtlzIndir3() >= 0) {
            return getTpUtlzIndir3();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpUtlzIndir3Obj(String tpUtlzIndir3Obj) {
        if (tpUtlzIndir3Obj != null) {
            setTpUtlzIndir3(tpUtlzIndir3Obj);
            ws.getIndRappAna().setTpUtlzIndir3(((short)0));
        }
        else {
            ws.getIndRappAna().setTpUtlzIndir3(((short)-1));
        }
    }

    @Override
    public String getTpVarzPagat() {
        return rappAna.getRanTpVarzPagat();
    }

    @Override
    public void setTpVarzPagat(String tpVarzPagat) {
        this.rappAna.setRanTpVarzPagat(tpVarzPagat);
    }

    @Override
    public String getTpVarzPagatObj() {
        if (ws.getIndRappAna().getTpVarzPagat() >= 0) {
            return getTpVarzPagat();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpVarzPagatObj(String tpVarzPagatObj) {
        if (tpVarzPagatObj != null) {
            setTpVarzPagat(tpVarzPagatObj);
            ws.getIndRappAna().setTpVarzPagat(((short)0));
        }
        else {
            ws.getIndRappAna().setTpVarzPagat(((short)-1));
        }
    }

    @Override
    public String getWsDataInizioEffettoDb() {
        return ws.getIdsv0010().getWsDataInizioEffettoDb();
    }

    @Override
    public void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb) {
        this.ws.getIdsv0010().setWsDataInizioEffettoDb(wsDataInizioEffettoDb);
    }

    @Override
    public long getWsTsCompetenza() {
        return ws.getIdsv0010().getWsTsCompetenza();
    }

    @Override
    public void setWsTsCompetenza(long wsTsCompetenza) {
        this.ws.getIdsv0010().setWsTsCompetenza(wsTsCompetenza);
    }
}
