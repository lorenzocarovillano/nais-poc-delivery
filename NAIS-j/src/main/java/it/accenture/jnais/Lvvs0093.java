package it.accenture.jnais;

import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.program.GenericParam;
import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Idsv0003CampiEsito;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.Ldbv2921;
import it.accenture.jnais.ws.Lvvs0093Data;

/**Original name: LVVS0093<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2007.
 * DATE-COMPILED.
 * **------------------------------------------------------------***</pre>*/
public class Lvvs0093 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lvvs0093Data ws = new Lvvs0093Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS0007
    private Ivvc0213 ivvc0213;

    //==== METHODS ====
    /**Original name: PROGRAM_LVVS0093_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: PERFORM S1000-ELABORAZIONE
        //              THRU EX-S1000
        s1000Elaborazione();
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lvvs0093 getInstance() {
        return ((Lvvs0093)Programs.getInstance(Lvvs0093.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                        IX-INDICI
        //                                             IVVC0213-TAB-OUTPUT.
        initIxIndici();
        initTabOutput();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: INITIALIZE LDBV2921.
        initLdbv2921();
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE
        //             TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*
	 * --> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 * --> RISPETTIVE AREE DCLGEN IN WORKING</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: PERFORM S1100-VALORIZZA-DCLGEN
        //              THRU S1100-VALORIZZA-DCLGEN-EX
        //           VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
        //                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //                   SPACES OR LOW-VALUE OR HIGH-VALUE.
        ws.setIxDclgen(((short)1));
        while (!(ws.getIxDclgen() > ivvc0213.getEleInfoMax() || Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias()) || Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()) || Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()))) {
            s1100ValorizzaDclgen();
            ws.setIxDclgen(Trunc.toShort(ws.getIxDclgen() + 1, 4));
        }
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //               PERFORM S1250-CALCOLA-DATA1     THRU S1250-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: MOVE IVVC0213-ID-POLIZZA        TO LDBV2921-ID-POL
            ws.getLdbv2921().setIdPol(ivvc0213.getIdPolizza());
            // COB_CODE: MOVE DPOL-DT-EMIS               TO LDBV2921-DT-EMIS
            ws.getLdbv2921().setDtEmis(TruncAbs.toInt(ws.getLccvpol1().getDati().getWpolDtEmis(), 8));
            //        MOVE DPOL-DT-DECOR              TO WK-DATA-APP
            //        ADD 5                           TO WK-AA
            //        MOVE WK-DATA-APP                TO LDBV2921-DT-DECOR-MAX
            //  --> AGGIUNGI 5 ANNI ALLA DATA DECORRENZA DELLA POLIZZA
            // COB_CODE: PERFORM S60120-PREPARA-LCCS0003
            //              THRU S60120-PREPARA-LCCS0003-EX
            s60120PreparaLccs0003();
            // COB_CODE: PERFORM S60130-CALL-LCCS0003
            //              THRU S60130-CALL-LCCS0003-EX
            s60130CallLccs0003();
            //  --> TIPO OPERAZIONE
            // COB_CODE: SET IDSV0003-SELECT             TO TRUE
            idsv0003.getOperazione().setSelect();
            //  --> TIPO STORICIT`
            // COB_CODE: SET IDSV0003-TRATT-X-COMPETENZA TO TRUE
            idsv0003.getTrattamentoStoricita().setTrattXCompetenza();
            //  --> TIPO LIVELLO
            // COB_CODE: SET IDSV0003-WHERE-CONDITION    TO TRUE
            idsv0003.getLivelloOperazione().setWhereCondition();
            // COB_CODE: PERFORM S1250-CALCOLA-DATA1     THRU S1250-EX
            s1250CalcolaData1();
        }
    }

    /**Original name: S60120-PREPARA-LCCS0003<br>
	 * <pre>----------------------------------------------------------------*
	 *    VALORIZZAZIONE INPUT MODULO LCCS0003
	 * ----------------------------------------------------------------*
	 *  --> A2K-INAMG -> Data Di Partenza Alla Quale Sommare Il Delta</pre>*/
    private void s60120PreparaLccs0003() {
        // COB_CODE: MOVE DPOL-DT-DECOR
        //             TO A2K-INAMG.
        ws.getIoA2kLccc0003().getInput().getA2kIndata().setA2kInamg(TruncAbs.toInt(ws.getLccvpol1().getDati().getWpolDtDecor(), 8));
        //
        // --> A2K-TDELTA -> Tipo Delta: Mesi(M) Anni(A)
        //
        // COB_CODE: MOVE 'A'
        //             TO A2K-TDELTA
        ws.getIoA2kLccc0003().getInput().setA2kTdeltaFormatted("A");
        //
        // --> A2K-DELTA  -> Delta Da Sommare
        //
        // COB_CODE: MOVE 5
        //             TO A2K-DELTA
        ws.getIoA2kLccc0003().getInput().setA2kDelta(((short)5));
        //
        // --> A2K-FUNZ -> Tipo Funzione: Somma Delta
        //
        // COB_CODE: MOVE '02'
        //             TO A2K-FUNZ.
        ws.getIoA2kLccc0003().getInput().setA2kFunz("02");
        //
        // --> A2K-INFO -> Tipo Formato Data: AAAAMMGG
        //
        // COB_CODE: MOVE '03'
        //             TO A2K-INFO.
        ws.getIoA2kLccc0003().getInput().setA2kInfo("03");
        //
        // --> A2K-INICON -> Giorno Inizio Conteggio (Stesso Giorno)
        //
        // COB_CODE: MOVE '0'
        //             TO A2K-INICON.
        ws.getIoA2kLccc0003().getInput().setA2kIniconFormatted("0");
        //
        // --> A2K-FISLAV -> Tipo Giorni Da Sommare (Fissi)
        //
        // COB_CODE: MOVE '0'
        //             TO A2K-FISLAV.
        ws.getIoA2kLccc0003().getInput().setA2kFislavFormatted("0");
    }

    /**Original name: S60130-CALL-LCCS0003<br>
	 * <pre>----------------------------------------------------------------*
	 *    CHIAMATA AL MODULO LCCS0003
	 * ----------------------------------------------------------------*</pre>*/
    private void s60130CallLccs0003() {
        Lccs0003 lccs0003 = null;
        GenericParam inRcode = null;
        ConcatUtil concatUtil = null;
        // COB_CODE:      CALL LCCS0003         USING IO-A2K-LCCC0003
        //                                            IN-RCODE
        //                ON EXCEPTION
        //           *
        //                   SET IDSV0003-INVALID-OPER    TO TRUE
        //           *
        //                END-CALL.
        try {
            lccs0003 = Lccs0003.getInstance();
            inRcode = new GenericParam(MarshalByteExt.strToBuffer(ws.getInRcodeFormatted(), Lvvs0093Data.Len.IN_RCODE));
            lccs0003.run(ws.getIoA2kLccc0003(), inRcode);
            ws.setInRcodeFromBuffer(inRcode.getByteData());
        }
        catch (ProgramExecutionException __ex) {
            //
            // COB_CODE: MOVE WK-CALL-PGM             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: MOVE 'CALL-LCCS0003 ERRORE CHIAMATA'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LCCS0003 ERRORE CHIAMATA");
            // COB_CODE: SET IDSV0003-INVALID-OPER    TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
            //
        }
        //
        // COB_CODE:      IF IN-RCODE NOT = '00'
        //           *
        //                   SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
        //                ELSE
        //                   MOVE A2K-OUAMG         TO LDBV2921-DT-DECOR-MAX
        //                END-IF.
        if (!ws.getInRcodeFormatted().equals("00")) {
            //
            // COB_CODE: MOVE WK-CALL-PGM               TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: STRING 'ERRORE CHIAMATA LCCS0003 ;'
            //                  IN-RCODE
            //                  DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "ERRORE CHIAMATA LCCS0003 ;", ws.getInRcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
            // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
            idsv0003.getReturnCode().setFieldNotValued();
        }
        else {
            // COB_CODE: MOVE A2K-OUAMG         TO LDBV2921-DT-DECOR-MAX
            ws.getLdbv2921().setDtDecorMaxFormatted(ws.getIoA2kLccc0003().getOutput().getA2kOuamgX().getA2kOuamgFormatted());
        }
    }

    /**Original name: S1100-VALORIZZA-DCLGEN<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 *     RISPETTIVE AREE DCLGEN IN WORKING
	 * ----------------------------------------------------------------*</pre>*/
    private void s1100ValorizzaDclgen() {
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-POLI
        //                TO DPOL-AREA-POLI
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias(), ws.getIvvc0218().getAliasPoli())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO DPOL-AREA-POLI
            ws.setDpolAreaPoliFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxDclgen()).getLunghezza() - 1));
        }
    }

    /**Original name: S1250-CALCOLA-DATA1<br>
	 * <pre>----------------------------------------------------------------*
	 *   CALCOLA DATA 1
	 * ----------------------------------------------------------------*</pre>*/
    private void s1250CalcolaData1() {
        Ldbs2920 ldbs2920 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'LDBS2920'                     TO WK-CALL-PGM.
        ws.setWkCallPgm("LDBS2920");
        //
        //
        // COB_CODE:      CALL WK-CALL-PGM  USING  IDSV0003 LDBV2921
        //           *
        //                ON EXCEPTION
        //                     SET IDSV0003-INVALID-OPER  TO TRUE
        //                END-CALL.
        try {
            ldbs2920 = Ldbs2920.getInstance();
            ldbs2920.run(idsv0003, ws.getLdbv2921());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-CALL-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: MOVE 'CALL-LDBS2920 ERRORE CHIAMATA - T0000-TRATTA-MATRICE'
            //              TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBS2920 ERRORE CHIAMATA - T0000-TRATTA-MATRICE");
            // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              MOVE LDBV2921-DT-EFF         TO IVVC0213-VAL-IMP-O
        //           ELSE
        //              END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: MOVE LDBV2921-DT-EFF         TO IVVC0213-VAL-IMP-O
            ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(ws.getLdbv2921().getDtEff(), 18, 7));
        }
        else {
            // COB_CODE: MOVE WK-CALL-PGM             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: STRING 'CHIAMATA LDBS2920 ;'
            //                  IDSV0003-RETURN-CODE ';'
            //                  IDSV0003-SQLCODE
            //              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LDBS2920 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //           OR IDSV0003-SQLCODE = -305
            //              MOVE ZERO  TO IDSV0003-SQLCODE
            //           ELSE
            //              SET IDSV0003-INVALID-OPER            TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isNotFound() || idsv0003.getSqlcode().getSqlcode() == -305) {
                // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
                // COB_CODE: MOVE ZERO  TO IDSV0003-SQLCODE
                idsv0003.getSqlcode().setSqlcode(0);
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-OPER            TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    public void initIxIndici() {
        ws.setIxDclgen(((short)0));
        ws.setIxTabTga(((short)0));
    }

    public void initTabOutput() {
        ivvc0213.getTabOutput().setCodVariabileO("");
        ivvc0213.getTabOutput().setTpDatoO(Types.SPACE_CHAR);
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        ivvc0213.getTabOutput().setValPercO(new AfDecimal(0, 14, 9));
        ivvc0213.getTabOutput().setValStrO("");
    }

    public void initLdbv2921() {
        ws.getLdbv2921().setIdPol(0);
        ws.getLdbv2921().setDtEffFormatted("00000000");
        ws.getLdbv2921().setDtEmisFormatted("00000000");
        ws.getLdbv2921().setDtDecorMaxFormatted("00000000");
    }
}
