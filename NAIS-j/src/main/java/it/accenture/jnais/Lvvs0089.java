package it.accenture.jnais;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Idsv0003CampiEsito;
import it.accenture.jnais.ws.enums.Idsv0003Sqlcode;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.Lvvs0089Data;

/**Original name: LVVS0089<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2007.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *   PROGRAMMA...... LVVS0089
 *   TIPOLOGIA...... SERVIZIO
 *   PROCESSO....... XXX
 *   FUNZIONE....... XXX
 *   DESCRIZIONE.... CONVERSIONE DATA DECORRENZA POLIZZA
 * **------------------------------------------------------------***</pre>*/
public class Lvvs0089 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lvvs0089Data ws = new Lvvs0089Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS0007
    private Ivvc0213 ivvc0213;

    //==== METHODS ====
    /**Original name: PROGRAM_LVVS0089_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: PERFORM S1000-ELABORAZIONE
        //              THRU EX-S1000
        s1000Elaborazione();
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lvvs0089 getInstance() {
        return ((Lvvs0089)Programs.getInstance(Lvvs0089.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                        IX-INDICI
        //                                             IVVC0213-TAB-OUTPUT.
        initIxIndici();
        initTabOutput();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        //    INITIALIZE LDBV2891.
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE
        //             TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
        // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
        //             TO WK-DATA-EFF-RIP
        ws.setWkDataEffRip(idsv0003.getDataInizioEffetto());
        // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA
        //             TO WK-DATA-CPTZ-RIP.
        ws.setWkDataCptzRip(idsv0003.getDataCompetenza());
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: INITIALIZE WK-DATA-OUTPUT
        //                      WK-DATA-X-12.
        ws.setWkDataOutput(new AfDecimal(0, 11, 7));
        initWkDataX12();
        //--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
        //--> RISPETTIVE AREE DCLGEN IN WORKING
        //    PERFORM S1100-VALORIZZA-DCLGEN
        //       THRU S1100-VALORIZZA-DCLGEN-EX
        //    VARYING IX-DCLGEN FROM 1 BY 1
        //      UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
        //         OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //            SPACES OR LOW-VALUE OR HIGH-VALUE.
        // COB_CODE: MOVE ZEROES    TO IVVC0213-VAL-IMP-O
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        // COB_CODE:      IF  IDSV0003-SUCCESSFUL-RC
        //                AND IDSV0003-SUCCESSFUL-SQL
        //           *        PERFORM VARYING IX-TAB-TGA FROM 1 BY 1
        //           *           UNTIL IX-TAB-TGA > DTGA-ELE-TRCH-MAX
        //           *
        //           *           IF DTGA-IMPB-VIS-END2000-NULL(IX-TAB-TGA) =
        //           *                       HIGH-VALUE OR LOW-VALUE OR SPACES
        //           *              CONTINUE
        //           *           ELSE
        //           *              ADD DTGA-IMPB-VIS-END2000(IX-TAB-TGA)
        //           *               TO IVVC0213-VAL-IMP-O
        //           *           END-IF
        //           *
        //           *        END-PERFORM
        //           *--> ROUTINE PER LA RICERCA DEL MOVIMENTO ORIGINARIO
        //           *
        //                END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            //        PERFORM VARYING IX-TAB-TGA FROM 1 BY 1
            //           UNTIL IX-TAB-TGA > DTGA-ELE-TRCH-MAX
            //
            //           IF DTGA-IMPB-VIS-END2000-NULL(IX-TAB-TGA) =
            //                       HIGH-VALUE OR LOW-VALUE OR SPACES
            //              CONTINUE
            //           ELSE
            //              ADD DTGA-IMPB-VIS-END2000(IX-TAB-TGA)
            //               TO IVVC0213-VAL-IMP-O
            //           END-IF
            //
            //        END-PERFORM
            //--> ROUTINE PER LA RICERCA DEL MOVIMENTO ORIGINARIO
            //
            // COB_CODE: SET  MOV-TROV-NO           TO TRUE
            ws.getFlagMovTrov().setNo();
            // COB_CODE: IF  IVVC0213-TIPO-MOVI-ORIG IS NUMERIC
            //           AND IVVC0213-TIPO-MOVI-ORIG > ZERO
            //               SUBTRACT  1  FROM  IX-TAB
            //           END-IF.
            if (Functions.isNumber(ivvc0213.getTipoMoviOrigFormatted()) && Characters.GT_ZERO.test(ivvc0213.getTipoMoviOrigFormatted())) {
                // COB_CODE: PERFORM S1200-RICERCA-MOVI-ORIG
                //              THRU S1200-RICERCA-MOVI-ORIG-EX
                //           VARYING IX-TAB    FROM 1 BY 1
                //             UNTIL IX-TAB    > WS-TAB-MAX
                //                OR WS-MOVI-LIQUID(IX-TAB) = 0
                //                OR MOV-TROV-SI
                ws.getIxIndici().setTab(((short)1));
                while (!(ws.getIxIndici().getTab() > ws.getWsTabMax() || ws.getWsTabLiquiComun().getMoviLiquid(ws.getIxIndici().getTab()) == 0 || ws.getFlagMovTrov().isSi())) {
                    s1200RicercaMoviOrig();
                    ws.getIxIndici().setTab(Trunc.toShort(ws.getIxIndici().getTab() + 1, 4));
                }
                // COB_CODE: SUBTRACT  1  FROM  IX-TAB
                ws.getIxIndici().setTab(Trunc.toShort(ws.getIxIndici().getTab() - 1, 4));
            }
        }
        // COB_CODE: IF MOV-TROV-SI
        //              END-IF
        //           ELSE
        //              MOVE ZERO                       TO IVVC0213-VAL-IMP-O
        //           END-IF.
        if (ws.getFlagMovTrov().isSi()) {
            // COB_CODE:         IF LIQUIDAZIONE
            //                         THRU RECUP-MOVI-COMUN-EX
            //                   ELSE
            //                      MOVE IDSV0003-DATA-COMPETENZA TO WK-DS-TS-CPTZ
            //           *        MOVE IVVC0213-ID-POLIZZA        TO LDBV2891-ID-POL
            //           *        PERFORM S1250-CALCOLA-DATA1     THRU S1250-EX
            //                   END-IF
            if (ws.getWsTipoMov().isLiquidazione()) {
                // COB_CODE: MOVE WS-MOVI-LIQUID(IX-TAB)  TO WS-MOVIMENTO
                ws.getWsMovimento().setWsMovimentoFormatted(ws.getWsTabLiquiComun().getMoviLiquidFormatted(ws.getIxIndici().getTab()));
                //--> RECUPERO IL MOVIMENTO DI COMUNICAZIONE
                // COB_CODE: PERFORM RECUP-MOVI-COMUN
                //              THRU RECUP-MOVI-COMUN-EX
                recupMoviComun();
            }
            else {
                // COB_CODE: MOVE WS-MOVI-COMUN(IX-TAB)     TO WS-MOVIMENTO
                ws.getWsMovimento().setWsMovimentoFormatted(ws.getWsTabLiquiComun().getMoviComunFormatted(ws.getIxIndici().getTab()));
                // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WK-DS-TS-CPTZ
                ws.setWkDsTsCptz(idsv0003.getDataCompetenza());
                //        MOVE IVVC0213-ID-POLIZZA        TO LDBV2891-ID-POL
                //        PERFORM S1250-CALCOLA-DATA1     THRU S1250-EX
            }
            // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
            //               PERFORM S1303-LEGGI-TRCH-POS THRU S1303-EX
            //           END-IF
            if (idsv0003.getReturnCode().isSuccessfulRc()) {
                // COB_CODE: PERFORM S1303-LEGGI-TRCH-POS THRU S1303-EX
                s1303LeggiTrchPos();
            }
            //--> LETTURA TRCH NEGATIVE
            // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
            //           AND IDSV0003-SUCCESSFUL-SQL
            //               PERFORM S1304-LEGGI-TRCH-NEG THRU S1304-EX
            //           END-IF
            if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: PERFORM S1304-LEGGI-TRCH-NEG THRU S1304-EX
                s1304LeggiTrchNeg();
            }
        }
        else {
            // COB_CODE: MOVE ZERO                       TO IVVC0213-VAL-IMP-O
            ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        }
    }

    /**Original name: S1200-RICERCA-MOVI-ORIG<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE PER LA RICERCA DEL MOVIMENTO ORIGINARIO
	 *     NELLA TABELLA DEI MOVIMENTI GESTITI
	 * ----------------------------------------------------------------*</pre>*/
    private void s1200RicercaMoviOrig() {
        // COB_CODE: IF WS-MOVI-LIQUID(IX-TAB) = IVVC0213-TIPO-MOVI-ORIG
        //              SET  MOV-TROV-SI  TO TRUE
        //           END-IF.
        if (ws.getWsTabLiquiComun().getMoviLiquid(ws.getIxIndici().getTab()) == ivvc0213.getTipoMoviOrig()) {
            // COB_CODE: SET  LIQUIDAZIONE TO TRUE
            ws.getWsTipoMov().setLiquidazione();
            // COB_CODE: SET  MOV-TROV-SI  TO TRUE
            ws.getFlagMovTrov().setSi();
        }
        // COB_CODE: IF WS-MOVI-COMUN(IX-TAB) = IVVC0213-TIPO-MOVI-ORIG
        //              SET  MOV-TROV-SI  TO TRUE
        //           END-IF.
        if (ws.getWsTabLiquiComun().getMoviComun(ws.getIxIndici().getTab()) == ivvc0213.getTipoMoviOrig()) {
            // COB_CODE: SET  COMUNICAZIONE TO TRUE
            ws.getWsTipoMov().setComunicazione();
            // COB_CODE: SET  MOV-TROV-SI  TO TRUE
            ws.getFlagMovTrov().setSi();
        }
    }

    /**Original name: RECUP-MOVI-COMUN<br>
	 * <pre>----------------------------------------------------------------*
	 *     Recupero il movimento di comunicazione
	 * ----------------------------------------------------------------*</pre>*/
    private void recupMoviComun() {
        Ldbs8850 ldbs8850 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: SET IDSV0003-FETCH-FIRST           TO TRUE
        idsv0003.getOperazione().setFetchFirst();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC         TO TRUE
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL        TO TRUE
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET INIT-CUR-MOV                   TO TRUE
        ws.getFlagCurMov().setInitCurMov();
        // COB_CODE: PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC
        //                      OR FINE-CUR-MOV
        //              END-IF
        //           END-PERFORM.
        while (!(!idsv0003.getReturnCode().isSuccessfulRc() || ws.getFlagCurMov().isFineCurMov())) {
            // COB_CODE: INITIALIZE MOVI
            initMovi();
            // COB_CODE: MOVE IVVC0213-ID-POLIZZA            TO MOV-ID-OGG
            ws.getMovi().getMovIdOgg().setMovIdOgg(ivvc0213.getIdPolizza());
            // COB_CODE: MOVE 'PO'                           TO MOV-TP-OGG
            ws.getMovi().setMovTpOgg("PO");
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO   TO MOV-DT-EFF
            ws.getMovi().setMovDtEff(idsv0003.getDataInizioEffetto());
            // COB_CODE: SET IDSV0003-TRATT-X-COMPETENZA     TO TRUE
            idsv0003.getTrattamentoStoricita().setTrattXCompetenza();
            //--> LIVELLO OPERAZIONE
            // COB_CODE: SET IDSV0003-WHERE-CONDITION       TO TRUE
            idsv0003.getLivelloOperazione().setWhereCondition();
            //--> INIZIALIZZA CODICE DI RITORNO
            // COB_CODE: SET IDSV0003-SUCCESSFUL-RC         TO TRUE
            idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
            // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL        TO TRUE
            idsv0003.getSqlcode().setSuccessfulSql();
            // COB_CODE: MOVE 'LDBS8850'            TO WK-CALL-PGM
            ws.setWkCallPgm("LDBS8850");
            // COB_CODE: CALL WK-CALL-PGM   USING  IDSV0003 MOVI
            //           ON EXCEPTION
            //                SET IDSV0003-INVALID-OPER  TO TRUE
            //           END-CALL
            try {
                ldbs8850 = Ldbs8850.getInstance();
                ldbs8850.run(idsv0003, ws.getMovi());
            }
            catch (ProgramExecutionException __ex) {
                // COB_CODE: MOVE 'LDBS8850'
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe("LDBS8850");
                // COB_CODE: MOVE 'CALL-LDBS6040 ERRORE CHIAMATA '
                //              TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBS6040 ERRORE CHIAMATA ");
                // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
            // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
            //             END-EVALUATE
            //           ELSE
            //             END-IF
            //           END-IF
            if (idsv0003.getReturnCode().isSuccessfulRc()) {
                // COB_CODE:           EVALUATE TRUE
                //                         WHEN IDSV0003-NOT-FOUND
                //           *          COMUNICAZIONE E LIQUIDAZIONE CONTESTUALE
                //                           SET FINE-CUR-MOV TO TRUE
                //                         WHEN IDSV0003-SUCCESSFUL-SQL
                //                           END-IF
                //                         WHEN OTHER
                //                             END-STRING
                //                     END-EVALUATE
                switch (idsv0003.getSqlcode().getSqlcode()) {

                    case Idsv0003Sqlcode.NOT_FOUND://          COMUNICAZIONE E LIQUIDAZIONE CONTESTUALE
                        // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WK-DS-TS-CPTZ
                        ws.setWkDsTsCptz(idsv0003.getDataCompetenza());
                        // COB_CODE: SET FINE-CUR-MOV TO TRUE
                        ws.getFlagCurMov().setFineCurMov();
                        break;

                    case Idsv0003Sqlcode.SUCCESSFUL_SQL:// COB_CODE: MOVE MOV-TP-MOVI      TO WS-MOVIMENTO
                        ws.getWsMovimento().setWsMovimento(TruncAbs.toInt(ws.getMovi().getMovTpMovi().getMovTpMovi(), 5));
                        // COB_CODE: MOVE MOV-DS-TS-CPTZ   TO WK-DS-TS-CPTZ
                        ws.setWkDsTsCptz(ws.getMovi().getMovDsTsCptz());
                        // COB_CODE: IF WS-MOVI-COMUN(IX-TAB) = MOV-TP-MOVI
                        //           AND MOV-DT-EFF = IDSV0003-DATA-INIZIO-EFFETTO
                        //              SET FINE-CUR-MOV   TO TRUE
                        //           END-IF
                        if (ws.getWsTabLiquiComun().getMoviComun(ws.getIxIndici().getTab()) == ws.getMovi().getMovTpMovi().getMovTpMovi() && ws.getMovi().getMovDtEff() == idsv0003.getDataInizioEffetto()) {
                            // COB_CODE: SET FINE-CUR-MOV   TO TRUE
                            ws.getFlagCurMov().setFineCurMov();
                        }
                        // COB_CODE: IF FINE-CUR-MOV
                        //                 THRU CLOSE-MOVI-EX
                        //           ELSE
                        //              SET IDSV0003-FETCH-NEXT   TO TRUE
                        //           END-IF
                        if (ws.getFlagCurMov().isFineCurMov()) {
                            // COB_CODE: PERFORM CLOSE-MOVI
                            //              THRU CLOSE-MOVI-EX
                            closeMovi();
                        }
                        else {
                            // COB_CODE: SET IDSV0003-FETCH-NEXT   TO TRUE
                            idsv0003.getOperazione().setFetchNext();
                        }
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
                        idsv0003.getReturnCode().setInvalidOper();
                        // COB_CODE: MOVE WK-CALL-PGM
                        //             TO IDSV0003-COD-SERVIZIO-BE
                        idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
                        // COB_CODE: STRING 'ERRORE RECUP MOVI COMUN ;'
                        //                  IDSV0003-RETURN-CODE ';'
                        //                  IDSV0003-SQLCODE
                        //                  DELIMITED BY SIZE INTO
                        //                  IDSV0003-DESCRIZ-ERR-DB2
                        //           END-STRING
                        concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "ERRORE RECUP MOVI COMUN ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                        idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                        break;
                }
            }
            else {
                // COB_CODE: MOVE WK-CALL-PGM      TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
                // COB_CODE: STRING 'CHIAMATA LDBS8850 ;'
                //                  IDSV0003-RETURN-CODE ';'
                //                  IDSV0003-SQLCODE
                //              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LDBS8850 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                // COB_CODE: IF IDSV0003-NOT-FOUND
                //           OR IDSV0003-SQLCODE = -305
                //              SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                //           ELSE
                //              SET IDSV0003-INVALID-OPER            TO TRUE
                //           END-IF
                if (idsv0003.getSqlcode().isNotFound() || idsv0003.getSqlcode().getSqlcode() == -305) {
                    // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                    idsv0003.getReturnCode().setFieldNotValued();
                }
                else {
                    // COB_CODE: SET IDSV0003-INVALID-OPER            TO TRUE
                    idsv0003.getReturnCode().setInvalidOper();
                }
            }
        }
    }

    /**Original name: CLOSE-MOVI<br>
	 * <pre>----------------------------------------------------------------*
	 *     CHIUDO IL CURSORE SULLA TABELLA MOVIMENTO LDBS6040
	 * ----------------------------------------------------------------*</pre>*/
    private void closeMovi() {
        Ldbs8850 ldbs8850 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: SET IDSV0003-TRATT-X-COMPETENZA     TO TRUE
        idsv0003.getTrattamentoStoricita().setTrattXCompetenza();
        // COB_CODE: MOVE SPACES                     TO IDSV0003-BUFFER-WHERE-COND
        idsv0003.setBufferWhereCond("");
        //
        // COB_CODE: SET IDSV0003-CLOSE-CURSOR       TO TRUE.
        idsv0003.getOperazione().setIdsv0003CloseCursor();
        // COB_CODE: SET IDSV0003-WHERE-CONDITION    TO TRUE.
        idsv0003.getLivelloOperazione().setWhereCondition();
        //
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC      TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL     TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: MOVE 'LDBS8850'            TO WK-CALL-PGM
        ws.setWkCallPgm("LDBS8850");
        // COB_CODE: CALL WK-CALL-PGM    USING  IDSV0003 MOVI
        //           ON EXCEPTION
        //                SET IDSV0003-INVALID-OPER  TO TRUE
        //           END-CALL
        try {
            ldbs8850 = Ldbs8850.getInstance();
            ldbs8850.run(idsv0003, ws.getMovi());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE 'LDBS6040'
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe("LDBS6040");
            // COB_CODE: MOVE 'CALL-LDBS6040 ERRORE CHIAMATA '
            //              TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBS6040 ERRORE CHIAMATA ");
            // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           ELSE
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: EVALUATE TRUE
            //               WHEN IDSV0003-SUCCESSFUL-SQL
            //                   CONTINUE
            //               WHEN OTHER
            //                   END-IF
            //           END-EVALUATE
            switch (idsv0003.getSqlcode().getSqlcode()) {

                case Idsv0003Sqlcode.SUCCESSFUL_SQL:// COB_CODE: CONTINUE
                //continue
                    break;

                default:// COB_CODE: MOVE WK-CALL-PGM       TO IDSV0003-COD-SERVIZIO-BE
                    idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
                    // COB_CODE: STRING 'CHIAMATA LDBS6040 ;'
                    //                  IDSV0003-RETURN-CODE ';'
                    //                  IDSV0003-SQLCODE
                    //              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LDBS6040 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                    idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                    // COB_CODE: IF IDSV0003-NOT-FOUND
                    //           OR IDSV0003-SQLCODE = -305
                    //              SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                    //           ELSE
                    //              SET IDSV0003-INVALID-OPER            TO TRUE
                    //           END-IF
                    if (idsv0003.getSqlcode().isNotFound() || idsv0003.getSqlcode().getSqlcode() == -305) {
                        // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                        idsv0003.getReturnCode().setFieldNotValued();
                    }
                    else {
                        // COB_CODE: SET IDSV0003-INVALID-OPER            TO TRUE
                        idsv0003.getReturnCode().setInvalidOper();
                    }
                    break;
            }
        }
        else {
            // COB_CODE: MOVE WK-CALL-PGM             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: STRING 'CHIAMATA LDBS6040 ;'
            //                  IDSV0003-RETURN-CODE ';'
            //                  IDSV0003-SQLCODE
            //              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LDBS6040 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //           OR IDSV0003-SQLCODE = -305
            //              SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
            //           ELSE
            //              SET IDSV0003-INVALID-OPER            TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isNotFound() || idsv0003.getSqlcode().getSqlcode() == -305) {
                // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-OPER            TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
        }
    }

    /**Original name: S1303-LEGGI-TRCH-POS<br>
	 * <pre>----------------------------------------------------------------*
	 * ----------------------------------------------------------------*</pre>*/
    private void s1303LeggiTrchPos() {
        Ldbse260 ldbse260 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: SET IDSV0003-SELECT           TO TRUE
        idsv0003.getOperazione().setSelect();
        // COB_CODE: SET IDSV0003-WHERE-CONDITION  TO TRUE
        idsv0003.getLivelloOperazione().setWhereCondition();
        // COB_CODE: SET IDSV0003-TRATT-X-COMPETENZA TO TRUE
        idsv0003.getTrattamentoStoricita().setTrattXCompetenza();
        //
        // COB_CODE: INITIALIZE LDBVE261.
        initLdbve261();
        //
        // COB_CODE: MOVE WK-DS-TS-CPTZ         TO IDSV0003-DATA-COMPETENZA
        idsv0003.setDataCompetenza(ws.getWkDsTsCptz());
        // COB_CODE: MOVE IVVC0213-ID-ADESIONE     TO LDBVE261-ID-ADES.
        ws.getLdbve261().setIdAdes(ivvc0213.getIdAdesione());
        // COB_CODE: MOVE WS-TRCH-POS              TO LDBVE261-TP-TRCH.
        ws.getLdbve261().getTpTrch().setTpTrchBytes(ws.getWsTrchPos().getWsTrchPosBytes());
        // COB_CODE: MOVE 'LDBSE260'               TO WK-CALL-PGM
        ws.setWkCallPgm("LDBSE260");
        //
        // COB_CODE:      CALL WK-CALL-PGM  USING  IDSV0003 LDBVE261
        //           *
        //                ON EXCEPTION
        //                     SET IDSV0003-INVALID-OPER  TO TRUE
        //                END-CALL.
        try {
            ldbse260 = Ldbse260.getInstance();
            ldbse260.run(idsv0003, ws.getLdbve261());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-CALL-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: MOVE 'CALL-LDBSE260 ERRORE CHIAMATA '
            //              TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBSE260 ERRORE CHIAMATA ");
            // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        //
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //              END-IF
        //           ELSE
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: IF LDBVE261-IMPB-VIS-END2000 IS NUMERIC
            //                TO WS-IMPB-VIS-END2000
            //           END-IF
            if (Functions.isNumber(ws.getLdbve261().getImpbVisEnd2000())) {
                // COB_CODE: MOVE LDBVE261-IMPB-VIS-END2000
                //             TO WS-IMPB-VIS-END2000
                ws.setWsImpbVisEnd2000(Trunc.toDecimal(ws.getLdbve261().getImpbVisEnd2000(), 15, 3));
            }
        }
        else {
            // COB_CODE: MOVE WK-CALL-PGM             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: STRING 'CHIAMATA LDBSE260 ;'
            //                  IDSV0003-RETURN-CODE ';'
            //                  IDSV0003-SQLCODE
            //              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LDBSE260 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //           OR IDSV0003-SQLCODE = -305
            //              SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
            //           ELSE
            //              SET IDSV0003-INVALID-OPER            TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isNotFound() || idsv0003.getSqlcode().getSqlcode() == -305) {
                // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-OPER            TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
        }
    }

    /**Original name: S1304-LEGGI-TRCH-NEG<br>
	 * <pre>----------------------------------------------------------------*
	 * ----------------------------------------------------------------*</pre>*/
    private void s1304LeggiTrchNeg() {
        Ldbse260 ldbse260 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: SET IDSV0003-SELECT           TO TRUE
        idsv0003.getOperazione().setSelect();
        // COB_CODE: SET IDSV0003-WHERE-CONDITION  TO TRUE
        idsv0003.getLivelloOperazione().setWhereCondition();
        // COB_CODE: SET IDSV0003-TRATT-X-COMPETENZA TO TRUE
        idsv0003.getTrattamentoStoricita().setTrattXCompetenza();
        //
        // COB_CODE: INITIALIZE LDBVE261.
        initLdbve261();
        //
        // COB_CODE: MOVE WK-DS-TS-CPTZ         TO IDSV0003-DATA-COMPETENZA
        idsv0003.setDataCompetenza(ws.getWkDsTsCptz());
        // COB_CODE: MOVE IVVC0213-ID-ADESIONE     TO LDBVE261-ID-ADES.
        ws.getLdbve261().setIdAdes(ivvc0213.getIdAdesione());
        // COB_CODE: MOVE WS-TRCH-NEG              TO LDBVE261-TP-TRCH.
        ws.getLdbve261().getTpTrch().setTpTrchBytes(ws.getWsTrchNeg().getWsTrchNegBytes());
        // COB_CODE: MOVE 'LDBSE260'               TO WK-CALL-PGM
        ws.setWkCallPgm("LDBSE260");
        //
        // COB_CODE:      CALL WK-CALL-PGM  USING  IDSV0003 LDBVE261
        //           *
        //                ON EXCEPTION
        //                     SET IDSV0003-INVALID-OPER  TO TRUE
        //                END-CALL.
        try {
            ldbse260 = Ldbse260.getInstance();
            ldbse260.run(idsv0003, ws.getLdbve261());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-CALL-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: MOVE 'CALL-LDBSE260 ERRORE CHIAMATA '
            //              TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBSE260 ERRORE CHIAMATA ");
            // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        //
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //              END-IF
        //           ELSE
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: IF LDBVE261-IMPB-VIS-END2000 IS NUMERIC
            //                                           LDBVE261-IMPB-VIS-END2000
            //           END-IF
            if (Functions.isNumber(ws.getLdbve261().getImpbVisEnd2000())) {
                // COB_CODE: COMPUTE IVVC0213-VAL-IMP-O = WS-IMPB-VIS-END2000 -
                //                                        LDBVE261-IMPB-VIS-END2000
                ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(ws.getWsImpbVisEnd2000().subtract(ws.getLdbve261().getImpbVisEnd2000()), 18, 7));
            }
        }
        else if (idsv0003.getSqlcode().isNotFound() || idsv0003.getSqlcode().getSqlcode() == -305) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //           OR IDSV0003-SQLCODE = -305
            //              MOVE WS-IMPB-VIS-END2000    TO IVVC0213-VAL-IMP-O
            //           ELSE
            //              END-STRING
            //           END-IF
            // COB_CODE: SET IDSV0003-SUCCESSFUL-RC  TO TRUE
            idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
            // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL TO TRUE
            idsv0003.getSqlcode().setSuccessfulSql();
            // COB_CODE: MOVE WS-IMPB-VIS-END2000    TO IVVC0213-VAL-IMP-O
            ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(ws.getWsImpbVisEnd2000(), 18, 7));
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER            TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
            // COB_CODE: MOVE WK-CALL-PGM            TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: STRING 'CHIAMATA LDBSE260 ;'
            //                  IDSV0003-RETURN-CODE ';'
            //                  IDSV0003-SQLCODE
            //              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LDBSE260 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: MOVE WK-DATA-EFF-RIP
        //             TO IDSV0003-DATA-INIZIO-EFFETTO
        idsv0003.setDataInizioEffetto(ws.getWkDataEffRip());
        // COB_CODE: MOVE WK-DATA-CPTZ-RIP
        //             TO IDSV0003-DATA-COMPETENZA
        idsv0003.setDataCompetenza(ws.getWkDataCptzRip());
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    public void initIxIndici() {
        ws.getIxIndici().setDclgen(((short)0));
        ws.getIxIndici().setTabTga(((short)0));
        ws.getIxIndici().setTab(((short)0));
    }

    public void initTabOutput() {
        ivvc0213.getTabOutput().setCodVariabileO("");
        ivvc0213.getTabOutput().setTpDatoO(Types.SPACE_CHAR);
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        ivvc0213.getTabOutput().setValPercO(new AfDecimal(0, 14, 9));
        ivvc0213.getTabOutput().setValStrO("");
    }

    public void initWkDataX12() {
        ws.getWkDataX12().setxAa("");
        ws.getWkDataX12().setVirogla(Types.SPACE_CHAR);
        ws.getWkDataX12().setxGg("");
    }

    public void initMovi() {
        ws.getMovi().setMovIdMovi(0);
        ws.getMovi().setMovCodCompAnia(0);
        ws.getMovi().getMovIdOgg().setMovIdOgg(0);
        ws.getMovi().setMovIbOgg("");
        ws.getMovi().setMovIbMovi("");
        ws.getMovi().setMovTpOgg("");
        ws.getMovi().getMovIdRich().setMovIdRich(0);
        ws.getMovi().getMovTpMovi().setMovTpMovi(0);
        ws.getMovi().setMovDtEff(0);
        ws.getMovi().getMovIdMoviAnn().setMovIdMoviAnn(0);
        ws.getMovi().getMovIdMoviCollg().setMovIdMoviCollg(0);
        ws.getMovi().setMovDsOperSql(Types.SPACE_CHAR);
        ws.getMovi().setMovDsVer(0);
        ws.getMovi().setMovDsTsCptz(0);
        ws.getMovi().setMovDsUtente("");
        ws.getMovi().setMovDsStatoElab(Types.SPACE_CHAR);
    }

    public void initLdbve261() {
        ws.getLdbve261().getTpTrch().setTrch1("");
        ws.getLdbve261().getTpTrch().setTrch2("");
        ws.getLdbve261().getTpTrch().setTrch3("");
        ws.getLdbve261().getTpTrch().setTrch4("");
        ws.getLdbve261().getTpTrch().setTrch5("");
        ws.getLdbve261().getTpTrch().setTrch6("");
        ws.getLdbve261().getTpTrch().setTrch7("");
        ws.getLdbve261().getTpTrch().setTrch8("");
        ws.getLdbve261().getTpTrch().setTrch9("");
        ws.getLdbve261().getTpTrch().setTrch10("");
        ws.getLdbve261().getTpTrch().setTrch11("");
        ws.getLdbve261().getTpTrch().setTrch12("");
        ws.getLdbve261().getTpTrch().setTrch13("");
        ws.getLdbve261().getTpTrch().setTrch14("");
        ws.getLdbve261().getTpTrch().setTrch15("");
        ws.getLdbve261().getTpTrch().setTrch16("");
        ws.getLdbve261().setIdAdes(0);
        ws.getLdbve261().setIdMovi(0);
        ws.getLdbve261().setImpbVisEnd2000(new AfDecimal(0, 15, 3));
    }
}
