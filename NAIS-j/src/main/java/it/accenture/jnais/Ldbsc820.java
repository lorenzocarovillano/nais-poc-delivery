package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.AdesPoliStatOggBusDao;
import it.accenture.jnais.commons.data.to.IAdesPoliStatOggBus;
import it.accenture.jnais.copy.AdesIvvs0216;
import it.accenture.jnais.copy.Poli;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ldbsc820Data;
import it.accenture.jnais.ws.Ldbvc821;
import it.accenture.jnais.ws.redefines.AdeCumCnbtCap;
import it.accenture.jnais.ws.redefines.AdeDtDecor;
import it.accenture.jnais.ws.redefines.AdeDtDecorPrestBan;
import it.accenture.jnais.ws.redefines.AdeDtEffVarzStatT;
import it.accenture.jnais.ws.redefines.AdeDtNovaRgmFisc;
import it.accenture.jnais.ws.redefines.AdeDtPresc;
import it.accenture.jnais.ws.redefines.AdeDtScad;
import it.accenture.jnais.ws.redefines.AdeDtUltConsCnbt;
import it.accenture.jnais.ws.redefines.AdeDtVarzTpIas;
import it.accenture.jnais.ws.redefines.AdeDurAa;
import it.accenture.jnais.ws.redefines.AdeDurGg;
import it.accenture.jnais.ws.redefines.AdeDurMm;
import it.accenture.jnais.ws.redefines.AdeEtaAScad;
import it.accenture.jnais.ws.redefines.AdeIdMoviChiu;
import it.accenture.jnais.ws.redefines.AdeImpAder;
import it.accenture.jnais.ws.redefines.AdeImpAz;
import it.accenture.jnais.ws.redefines.AdeImpbVisDaRec;
import it.accenture.jnais.ws.redefines.AdeImpGarCnbt;
import it.accenture.jnais.ws.redefines.AdeImpRecRitAcc;
import it.accenture.jnais.ws.redefines.AdeImpRecRitVis;
import it.accenture.jnais.ws.redefines.AdeImpTfr;
import it.accenture.jnais.ws.redefines.AdeImpVolo;
import it.accenture.jnais.ws.redefines.AdeNumRatPian;
import it.accenture.jnais.ws.redefines.AdePcAder;
import it.accenture.jnais.ws.redefines.AdePcAz;
import it.accenture.jnais.ws.redefines.AdePcTfr;
import it.accenture.jnais.ws.redefines.AdePcVolo;
import it.accenture.jnais.ws.redefines.AdePreLrdInd;
import it.accenture.jnais.ws.redefines.AdePreNetInd;
import it.accenture.jnais.ws.redefines.AdePrstzIniInd;
import it.accenture.jnais.ws.redefines.AdeRatLrdInd;
import it.accenture.jnais.ws.redefines.PolAaDiffProrDflt;
import it.accenture.jnais.ws.redefines.PolDir1oVers;
import it.accenture.jnais.ws.redefines.PolDirEmis;
import it.accenture.jnais.ws.redefines.PolDirQuiet;
import it.accenture.jnais.ws.redefines.PolDirVersAgg;
import it.accenture.jnais.ws.redefines.PolDtApplzConv;
import it.accenture.jnais.ws.redefines.PolDtIniVldtConv;
import it.accenture.jnais.ws.redefines.PolDtPresc;
import it.accenture.jnais.ws.redefines.PolDtProp;
import it.accenture.jnais.ws.redefines.PolDtScad;
import it.accenture.jnais.ws.redefines.PolDurAa;
import it.accenture.jnais.ws.redefines.PolDurGg;
import it.accenture.jnais.ws.redefines.PolDurMm;
import it.accenture.jnais.ws.redefines.PolIdAccComm;
import it.accenture.jnais.ws.redefines.PolIdMoviChiu;
import it.accenture.jnais.ws.redefines.PolSpeMed;
import it.accenture.jnais.ws.redefines.StbIdMoviChiu;

/**Original name: LDBSC820<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  11 DIC 2017.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO AD HOC PER ACCESSO RISORSE DB        *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Ldbsc820 extends Program implements IAdesPoliStatOggBus {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private AdesPoliStatOggBusDao adesPoliStatOggBusDao = new AdesPoliStatOggBusDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Ldbsc820Data ws = new Ldbsc820Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: LDBVC821
    private Ldbvc821 ldbvc821;

    //==== METHODS ====
    /**Original name: PROGRAM_LDBSC820_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, Ldbvc821 ldbvc821) {
        this.idsv0003 = idsv0003;
        this.ldbvc821 = ldbvc821;
        // COB_CODE: PERFORM A000-INIZIO              THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                        a200ElaboraWcEff();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                        b200ElaboraWcCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //               SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                        c200ElaboraWcNst();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Ldbsc820 getInstance() {
        return ((Ldbsc820)Programs.getInstance(Ldbsc820.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'LDBSC820'              TO   IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("LDBSC820");
        // COB_CODE: MOVE 'LDBVC821' TO   IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("LDBVC821");
        // COB_CODE: MOVE '00'                      TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                    TO   IDSV0003-SQLCODE
        //                                               IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                    TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                               IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-WC-EFF<br>*/
    private void a200ElaboraWcEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-WC-EFF          THRU A210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-WC-EFF          THRU A210-EX
            a210SelectWcEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
            a260OpenCursorWcEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
            a270CloseCursorWcEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
            a280FetchFirstWcEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
            a290FetchNextWcEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B200-ELABORA-WC-CPZ<br>*/
    private void b200ElaboraWcCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
            b210SelectWcCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
            b260OpenCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
            b270CloseCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
            b280FetchFirstWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
            b290FetchNextWcCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: C200-ELABORA-WC-NST<br>*/
    private void c200ElaboraWcNst() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM C210-SELECT-WC-NST          THRU C210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM C210-SELECT-WC-NST          THRU C210-EX
            c210SelectWcNst();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
            c260OpenCursorWcNst();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
            c270CloseCursorWcNst();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
            c280FetchFirstWcNst();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
            c290FetchNextWcNst();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A205-DECLARE-CURSOR-WC-EFF<br>
	 * <pre>----
	 * ----  gestione WC Effetto
	 * ----</pre>*/
    private void a205DeclareCursorWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A210-SELECT-WC-EFF<br>*/
    private void a210SelectWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                     A.ID_ADES
        //                    ,A.ID_POLI
        //                    ,A.ID_MOVI_CRZ
        //                    ,A.ID_MOVI_CHIU
        //                    ,A.DT_INI_EFF
        //                    ,A.DT_END_EFF
        //                    ,A.IB_PREV
        //                    ,A.IB_OGG
        //                    ,A.COD_COMP_ANIA
        //                    ,A.DT_DECOR
        //                    ,A.DT_SCAD
        //                    ,A.ETA_A_SCAD
        //                    ,A.DUR_AA
        //                    ,A.DUR_MM
        //                    ,A.DUR_GG
        //                    ,A.TP_RGM_FISC
        //                    ,A.TP_RIAT
        //                    ,A.TP_MOD_PAG_TIT
        //                    ,A.TP_IAS
        //                    ,A.DT_VARZ_TP_IAS
        //                    ,A.PRE_NET_IND
        //                    ,A.PRE_LRD_IND
        //                    ,A.RAT_LRD_IND
        //                    ,A.PRSTZ_INI_IND
        //                    ,A.FL_COINC_ASSTO
        //                    ,A.IB_DFLT
        //                    ,A.MOD_CALC
        //                    ,A.TP_FNT_CNBTVA
        //                    ,A.IMP_AZ
        //                    ,A.IMP_ADER
        //                    ,A.IMP_TFR
        //                    ,A.IMP_VOLO
        //                    ,A.PC_AZ
        //                    ,A.PC_ADER
        //                    ,A.PC_TFR
        //                    ,A.PC_VOLO
        //                    ,A.DT_NOVA_RGM_FISC
        //                    ,A.FL_ATTIV
        //                    ,A.IMP_REC_RIT_VIS
        //                    ,A.IMP_REC_RIT_ACC
        //                    ,A.FL_VARZ_STAT_TBGC
        //                    ,A.FL_PROVZA_MIGRAZ
        //                    ,A.IMPB_VIS_DA_REC
        //                    ,A.DT_DECOR_PREST_BAN
        //                    ,A.DT_EFF_VARZ_STAT_T
        //                    ,A.DS_RIGA
        //                    ,A.DS_OPER_SQL
        //                    ,A.DS_VER
        //                    ,A.DS_TS_INI_CPTZ
        //                    ,A.DS_TS_END_CPTZ
        //                    ,A.DS_UTENTE
        //                    ,A.DS_STATO_ELAB
        //                    ,A.CUM_CNBT_CAP
        //                    ,A.IMP_GAR_CNBT
        //                    ,A.DT_ULT_CONS_CNBT
        //                    ,A.IDEN_ISC_FND
        //                    ,A.NUM_RAT_PIAN
        //                    ,A.DT_PRESC
        //                    ,A.CONCS_PREST
        //                    ,B.ID_POLI
        //                    ,B.ID_MOVI_CRZ
        //                    ,B.ID_MOVI_CHIU
        //                    ,B.IB_OGG
        //                    ,B.IB_PROP
        //                    ,B.DT_PROP
        //                    ,B.DT_INI_EFF
        //                    ,B.DT_END_EFF
        //                    ,B.COD_COMP_ANIA
        //                    ,B.DT_DECOR
        //                    ,B.DT_EMIS
        //                    ,B.TP_POLI
        //                    ,B.DUR_AA
        //                    ,B.DUR_MM
        //                    ,B.DT_SCAD
        //                    ,B.COD_PROD
        //                    ,B.DT_INI_VLDT_PROD
        //                    ,B.COD_CONV
        //                    ,B.COD_RAMO
        //                    ,B.DT_INI_VLDT_CONV
        //                    ,B.DT_APPLZ_CONV
        //                    ,B.TP_FRM_ASSVA
        //                    ,B.TP_RGM_FISC
        //                    ,B.FL_ESTAS
        //                    ,B.FL_RSH_COMUN
        //                    ,B.FL_RSH_COMUN_COND
        //                    ,B.TP_LIV_GENZ_TIT
        //                    ,B.FL_COP_FINANZ
        //                    ,B.TP_APPLZ_DIR
        //                    ,B.SPE_MED
        //                    ,B.DIR_EMIS
        //                    ,B.DIR_1O_VERS
        //                    ,B.DIR_VERS_AGG
        //                    ,B.COD_DVS
        //                    ,B.FL_FNT_AZ
        //                    ,B.FL_FNT_ADER
        //                    ,B.FL_FNT_TFR
        //                    ,B.FL_FNT_VOLO
        //                    ,B.TP_OPZ_A_SCAD
        //                    ,B.AA_DIFF_PROR_DFLT
        //                    ,B.FL_VER_PROD
        //                    ,B.DUR_GG
        //                    ,B.DIR_QUIET
        //                    ,B.TP_PTF_ESTNO
        //                    ,B.FL_CUM_PRE_CNTR
        //                    ,B.FL_AMMB_MOVI
        //                    ,B.CONV_GECO
        //                    ,B.DS_RIGA
        //                    ,B.DS_OPER_SQL
        //                    ,B.DS_VER
        //                    ,B.DS_TS_INI_CPTZ
        //                    ,B.DS_TS_END_CPTZ
        //                    ,B.DS_UTENTE
        //                    ,B.DS_STATO_ELAB
        //                    ,B.FL_SCUDO_FISC
        //                    ,B.FL_TRASFE
        //                    ,B.FL_TFR_STRC
        //                    ,B.DT_PRESC
        //                    ,B.COD_CONV_AGG
        //                    ,B.SUBCAT_PROD
        //                    ,B.FL_QUEST_ADEGZ_ASS
        //                    ,B.COD_TPA
        //                    ,B.ID_ACC_COMM
        //                    ,B.FL_POLI_CPI_PR
        //                    ,B.FL_POLI_BUNDLING
        //                    ,B.IND_POLI_PRIN_COLL
        //                    ,B.FL_VND_BUNDLE
        //                    ,B.IB_BS
        //                    ,B.FL_POLI_IFP
        //                    ,C.ID_STAT_OGG_BUS
        //                    ,C.ID_OGG
        //                    ,C.TP_OGG
        //                    ,C.ID_MOVI_CRZ
        //                    ,C.ID_MOVI_CHIU
        //                    ,C.DT_INI_EFF
        //                    ,C.DT_END_EFF
        //                    ,C.COD_COMP_ANIA
        //                    ,C.TP_STAT_BUS
        //                    ,C.TP_CAUS
        //                    ,C.DS_RIGA
        //                    ,C.DS_OPER_SQL
        //                    ,C.DS_VER
        //                    ,C.DS_TS_INI_CPTZ
        //                    ,C.DS_TS_END_CPTZ
        //                    ,C.DS_UTENTE
        //                    ,C.DS_STATO_ELAB
        //             INTO
        //                :ADE-ID-ADES
        //               ,:ADE-ID-POLI
        //               ,:ADE-ID-MOVI-CRZ
        //               ,:ADE-ID-MOVI-CHIU
        //                :IND-ADE-ID-MOVI-CHIU
        //               ,:ADE-DT-INI-EFF-DB
        //               ,:ADE-DT-END-EFF-DB
        //               ,:ADE-IB-PREV
        //                :IND-ADE-IB-PREV
        //               ,:ADE-IB-OGG
        //                :IND-ADE-IB-OGG
        //               ,:ADE-COD-COMP-ANIA
        //               ,:ADE-DT-DECOR-DB
        //                :IND-ADE-DT-DECOR
        //               ,:ADE-DT-SCAD-DB
        //                :IND-ADE-DT-SCAD
        //               ,:ADE-ETA-A-SCAD
        //                :IND-ADE-ETA-A-SCAD
        //               ,:ADE-DUR-AA
        //                :IND-ADE-DUR-AA
        //               ,:ADE-DUR-MM
        //                :IND-ADE-DUR-MM
        //               ,:ADE-DUR-GG
        //                :IND-ADE-DUR-GG
        //               ,:ADE-TP-RGM-FISC
        //               ,:ADE-TP-RIAT
        //                :IND-ADE-TP-RIAT
        //               ,:ADE-TP-MOD-PAG-TIT
        //               ,:ADE-TP-IAS
        //                :IND-ADE-TP-IAS
        //               ,:ADE-DT-VARZ-TP-IAS-DB
        //                :IND-ADE-DT-VARZ-TP-IAS
        //               ,:ADE-PRE-NET-IND
        //                :IND-ADE-PRE-NET-IND
        //               ,:ADE-PRE-LRD-IND
        //                :IND-ADE-PRE-LRD-IND
        //               ,:ADE-RAT-LRD-IND
        //                :IND-ADE-RAT-LRD-IND
        //               ,:ADE-PRSTZ-INI-IND
        //                :IND-ADE-PRSTZ-INI-IND
        //               ,:ADE-FL-COINC-ASSTO
        //                :IND-ADE-FL-COINC-ASSTO
        //               ,:ADE-IB-DFLT
        //                :IND-ADE-IB-DFLT
        //               ,:ADE-MOD-CALC
        //                :IND-ADE-MOD-CALC
        //               ,:ADE-TP-FNT-CNBTVA
        //                :IND-ADE-TP-FNT-CNBTVA
        //               ,:ADE-IMP-AZ
        //                :IND-ADE-IMP-AZ
        //               ,:ADE-IMP-ADER
        //                :IND-ADE-IMP-ADER
        //               ,:ADE-IMP-TFR
        //                :IND-ADE-IMP-TFR
        //               ,:ADE-IMP-VOLO
        //                :IND-ADE-IMP-VOLO
        //               ,:ADE-PC-AZ
        //                :IND-ADE-PC-AZ
        //               ,:ADE-PC-ADER
        //                :IND-ADE-PC-ADER
        //               ,:ADE-PC-TFR
        //                :IND-ADE-PC-TFR
        //               ,:ADE-PC-VOLO
        //                :IND-ADE-PC-VOLO
        //               ,:ADE-DT-NOVA-RGM-FISC-DB
        //                :IND-ADE-DT-NOVA-RGM-FISC
        //               ,:ADE-FL-ATTIV
        //                :IND-ADE-FL-ATTIV
        //               ,:ADE-IMP-REC-RIT-VIS
        //                :IND-ADE-IMP-REC-RIT-VIS
        //               ,:ADE-IMP-REC-RIT-ACC
        //                :IND-ADE-IMP-REC-RIT-ACC
        //               ,:ADE-FL-VARZ-STAT-TBGC
        //                :IND-ADE-FL-VARZ-STAT-TBGC
        //               ,:ADE-FL-PROVZA-MIGRAZ
        //                :IND-ADE-FL-PROVZA-MIGRAZ
        //               ,:ADE-IMPB-VIS-DA-REC
        //                :IND-ADE-IMPB-VIS-DA-REC
        //               ,:ADE-DT-DECOR-PREST-BAN-DB
        //                :IND-ADE-DT-DECOR-PREST-BAN
        //               ,:ADE-DT-EFF-VARZ-STAT-T-DB
        //                :IND-ADE-DT-EFF-VARZ-STAT-T
        //               ,:ADE-DS-RIGA
        //               ,:ADE-DS-OPER-SQL
        //               ,:ADE-DS-VER
        //               ,:ADE-DS-TS-INI-CPTZ
        //               ,:ADE-DS-TS-END-CPTZ
        //               ,:ADE-DS-UTENTE
        //               ,:ADE-DS-STATO-ELAB
        //               ,:ADE-CUM-CNBT-CAP
        //                :IND-ADE-CUM-CNBT-CAP
        //               ,:ADE-IMP-GAR-CNBT
        //                :IND-ADE-IMP-GAR-CNBT
        //               ,:ADE-DT-ULT-CONS-CNBT-DB
        //                :IND-ADE-DT-ULT-CONS-CNBT
        //               ,:ADE-IDEN-ISC-FND
        //                :IND-ADE-IDEN-ISC-FND
        //               ,:ADE-NUM-RAT-PIAN
        //                :IND-ADE-NUM-RAT-PIAN
        //               ,:ADE-DT-PRESC-DB
        //                :IND-ADE-DT-PRESC
        //               ,:ADE-CONCS-PREST
        //                :IND-ADE-CONCS-PREST
        //               ,
        //                :POL-ID-POLI
        //               ,:POL-ID-MOVI-CRZ
        //               ,:POL-ID-MOVI-CHIU
        //                :IND-POL-ID-MOVI-CHIU
        //               ,:POL-IB-OGG
        //                :IND-POL-IB-OGG
        //               ,:POL-IB-PROP
        //               ,:POL-DT-PROP-DB
        //                :IND-POL-DT-PROP
        //               ,:POL-DT-INI-EFF-DB
        //               ,:POL-DT-END-EFF-DB
        //               ,:POL-COD-COMP-ANIA
        //               ,:POL-DT-DECOR-DB
        //               ,:POL-DT-EMIS-DB
        //               ,:POL-TP-POLI
        //               ,:POL-DUR-AA
        //                :IND-POL-DUR-AA
        //               ,:POL-DUR-MM
        //                :IND-POL-DUR-MM
        //               ,:POL-DT-SCAD-DB
        //                :IND-POL-DT-SCAD
        //               ,:POL-COD-PROD
        //               ,:POL-DT-INI-VLDT-PROD-DB
        //               ,:POL-COD-CONV
        //                :IND-POL-COD-CONV
        //               ,:POL-COD-RAMO
        //                :IND-POL-COD-RAMO
        //               ,:POL-DT-INI-VLDT-CONV-DB
        //                :IND-POL-DT-INI-VLDT-CONV
        //               ,:POL-DT-APPLZ-CONV-DB
        //                :IND-POL-DT-APPLZ-CONV
        //               ,:POL-TP-FRM-ASSVA
        //               ,:POL-TP-RGM-FISC
        //                :IND-POL-TP-RGM-FISC
        //               ,:POL-FL-ESTAS
        //                :IND-POL-FL-ESTAS
        //               ,:POL-FL-RSH-COMUN
        //                :IND-POL-FL-RSH-COMUN
        //               ,:POL-FL-RSH-COMUN-COND
        //                :IND-POL-FL-RSH-COMUN-COND
        //               ,:POL-TP-LIV-GENZ-TIT
        //               ,:POL-FL-COP-FINANZ
        //                :IND-POL-FL-COP-FINANZ
        //               ,:POL-TP-APPLZ-DIR
        //                :IND-POL-TP-APPLZ-DIR
        //               ,:POL-SPE-MED
        //                :IND-POL-SPE-MED
        //               ,:POL-DIR-EMIS
        //                :IND-POL-DIR-EMIS
        //               ,:POL-DIR-1O-VERS
        //                :IND-POL-DIR-1O-VERS
        //               ,:POL-DIR-VERS-AGG
        //                :IND-POL-DIR-VERS-AGG
        //               ,:POL-COD-DVS
        //                :IND-POL-COD-DVS
        //               ,:POL-FL-FNT-AZ
        //                :IND-POL-FL-FNT-AZ
        //               ,:POL-FL-FNT-ADER
        //                :IND-POL-FL-FNT-ADER
        //               ,:POL-FL-FNT-TFR
        //                :IND-POL-FL-FNT-TFR
        //               ,:POL-FL-FNT-VOLO
        //                :IND-POL-FL-FNT-VOLO
        //               ,:POL-TP-OPZ-A-SCAD
        //                :IND-POL-TP-OPZ-A-SCAD
        //               ,:POL-AA-DIFF-PROR-DFLT
        //                :IND-POL-AA-DIFF-PROR-DFLT
        //               ,:POL-FL-VER-PROD
        //                :IND-POL-FL-VER-PROD
        //               ,:POL-DUR-GG
        //                :IND-POL-DUR-GG
        //               ,:POL-DIR-QUIET
        //                :IND-POL-DIR-QUIET
        //               ,:POL-TP-PTF-ESTNO
        //                :IND-POL-TP-PTF-ESTNO
        //               ,:POL-FL-CUM-PRE-CNTR
        //                :IND-POL-FL-CUM-PRE-CNTR
        //               ,:POL-FL-AMMB-MOVI
        //                :IND-POL-FL-AMMB-MOVI
        //               ,:POL-CONV-GECO
        //                :IND-POL-CONV-GECO
        //               ,:POL-DS-RIGA
        //               ,:POL-DS-OPER-SQL
        //               ,:POL-DS-VER
        //               ,:POL-DS-TS-INI-CPTZ
        //               ,:POL-DS-TS-END-CPTZ
        //               ,:POL-DS-UTENTE
        //               ,:POL-DS-STATO-ELAB
        //               ,:POL-FL-SCUDO-FISC
        //                :IND-POL-FL-SCUDO-FISC
        //               ,:POL-FL-TRASFE
        //                :IND-POL-FL-TRASFE
        //               ,:POL-FL-TFR-STRC
        //                :IND-POL-FL-TFR-STRC
        //               ,:POL-DT-PRESC-DB
        //                :IND-POL-DT-PRESC
        //               ,:POL-COD-CONV-AGG
        //                :IND-POL-COD-CONV-AGG
        //               ,:POL-SUBCAT-PROD
        //                :IND-POL-SUBCAT-PROD
        //               ,:POL-FL-QUEST-ADEGZ-ASS
        //                :IND-POL-FL-QUEST-ADEGZ-ASS
        //               ,:POL-COD-TPA
        //                :IND-POL-COD-TPA
        //               ,:POL-ID-ACC-COMM
        //                :IND-POL-ID-ACC-COMM
        //               ,:POL-FL-POLI-CPI-PR
        //                :IND-POL-FL-POLI-CPI-PR
        //               ,:POL-FL-POLI-BUNDLING
        //                :IND-POL-FL-POLI-BUNDLING
        //               ,:POL-IND-POLI-PRIN-COLL
        //                :IND-POL-IND-POLI-PRIN-COLL
        //               ,:POL-FL-VND-BUNDLE
        //                :IND-POL-FL-VND-BUNDLE
        //               ,:POL-IB-BS
        //                :IND-POL-IB-BS
        //               ,:POL-FL-POLI-IFP
        //                :IND-POL-FL-POLI-IFP
        //               ,
        //                :STB-ID-STAT-OGG-BUS
        //               ,:STB-ID-OGG
        //               ,:STB-TP-OGG
        //               ,:STB-ID-MOVI-CRZ
        //               ,:STB-ID-MOVI-CHIU
        //                :IND-STB-ID-MOVI-CHIU
        //               ,:STB-DT-INI-EFF-DB
        //               ,:STB-DT-END-EFF-DB
        //               ,:STB-COD-COMP-ANIA
        //               ,:STB-TP-STAT-BUS
        //               ,:STB-TP-CAUS
        //               ,:STB-DS-RIGA
        //               ,:STB-DS-OPER-SQL
        //               ,:STB-DS-VER
        //               ,:STB-DS-TS-INI-CPTZ
        //               ,:STB-DS-TS-END-CPTZ
        //               ,:STB-DS-UTENTE
        //               ,:STB-DS-STATO-ELAB
        //             FROM ADES A,
        //                  POLI B,
        //                  STAT_OGG_BUS C
        //             WHERE  B.TP_FRM_ASSVA   IN (:LC821-TP-FRM-ASSVA1,
        //                                         :LC821-TP-FRM-ASSVA2)
        //                    AND A.ID_POLI         =   B.ID_POLI
        //                    AND A.ID_POLI         = :LC821-ID-POLI
        //                    AND A.ID_ADES         =   C.ID_OGG
        //                    AND C.TP_OGG          =  'AD'
        //                    AND C.TP_STAT_BUS     =  'VI'
        //                    AND A.COD_COMP_ANIA   =   B.COD_COMP_ANIA
        //                    AND B.COD_COMP_ANIA   =   C.COD_COMP_ANIA
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.ID_MOVI_CHIU IS NULL
        //                    AND B.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.ID_MOVI_CHIU IS NULL
        //                    AND C.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND C.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND C.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND C.ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        adesPoliStatOggBusDao.selectRec(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: A260-OPEN-CURSOR-WC-EFF<br>*/
    private void a260OpenCursorWcEff() {
        // COB_CODE: PERFORM A205-DECLARE-CURSOR-WC-EFF THRU A205-EX.
        a205DeclareCursorWcEff();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A270-CLOSE-CURSOR-WC-EFF<br>*/
    private void a270CloseCursorWcEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A280-FETCH-FIRST-WC-EFF<br>*/
    private void a280FetchFirstWcEff() {
        // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF    THRU A260-EX.
        a260OpenCursorWcEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
            a290FetchNextWcEff();
        }
    }

    /**Original name: A290-FETCH-NEXT-WC-EFF<br>*/
    private void a290FetchNextWcEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B205-DECLARE-CURSOR-WC-CPZ<br>
	 * <pre>----
	 * ----  gestione WC Competenza
	 * ----</pre>*/
    private void b205DeclareCursorWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B210-SELECT-WC-CPZ<br>*/
    private void b210SelectWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                     A.ID_ADES
        //                    ,A.ID_POLI
        //                    ,A.ID_MOVI_CRZ
        //                    ,A.ID_MOVI_CHIU
        //                    ,A.DT_INI_EFF
        //                    ,A.DT_END_EFF
        //                    ,A.IB_PREV
        //                    ,A.IB_OGG
        //                    ,A.COD_COMP_ANIA
        //                    ,A.DT_DECOR
        //                    ,A.DT_SCAD
        //                    ,A.ETA_A_SCAD
        //                    ,A.DUR_AA
        //                    ,A.DUR_MM
        //                    ,A.DUR_GG
        //                    ,A.TP_RGM_FISC
        //                    ,A.TP_RIAT
        //                    ,A.TP_MOD_PAG_TIT
        //                    ,A.TP_IAS
        //                    ,A.DT_VARZ_TP_IAS
        //                    ,A.PRE_NET_IND
        //                    ,A.PRE_LRD_IND
        //                    ,A.RAT_LRD_IND
        //                    ,A.PRSTZ_INI_IND
        //                    ,A.FL_COINC_ASSTO
        //                    ,A.IB_DFLT
        //                    ,A.MOD_CALC
        //                    ,A.TP_FNT_CNBTVA
        //                    ,A.IMP_AZ
        //                    ,A.IMP_ADER
        //                    ,A.IMP_TFR
        //                    ,A.IMP_VOLO
        //                    ,A.PC_AZ
        //                    ,A.PC_ADER
        //                    ,A.PC_TFR
        //                    ,A.PC_VOLO
        //                    ,A.DT_NOVA_RGM_FISC
        //                    ,A.FL_ATTIV
        //                    ,A.IMP_REC_RIT_VIS
        //                    ,A.IMP_REC_RIT_ACC
        //                    ,A.FL_VARZ_STAT_TBGC
        //                    ,A.FL_PROVZA_MIGRAZ
        //                    ,A.IMPB_VIS_DA_REC
        //                    ,A.DT_DECOR_PREST_BAN
        //                    ,A.DT_EFF_VARZ_STAT_T
        //                    ,A.DS_RIGA
        //                    ,A.DS_OPER_SQL
        //                    ,A.DS_VER
        //                    ,A.DS_TS_INI_CPTZ
        //                    ,A.DS_TS_END_CPTZ
        //                    ,A.DS_UTENTE
        //                    ,A.DS_STATO_ELAB
        //                    ,A.CUM_CNBT_CAP
        //                    ,A.IMP_GAR_CNBT
        //                    ,A.DT_ULT_CONS_CNBT
        //                    ,A.IDEN_ISC_FND
        //                    ,A.NUM_RAT_PIAN
        //                    ,A.DT_PRESC
        //                    ,A.CONCS_PREST
        //                    ,B.ID_POLI
        //                    ,B.ID_MOVI_CRZ
        //                    ,B.ID_MOVI_CHIU
        //                    ,B.IB_OGG
        //                    ,B.IB_PROP
        //                    ,B.DT_PROP
        //                    ,B.DT_INI_EFF
        //                    ,B.DT_END_EFF
        //                    ,B.COD_COMP_ANIA
        //                    ,B.DT_DECOR
        //                    ,B.DT_EMIS
        //                    ,B.TP_POLI
        //                    ,B.DUR_AA
        //                    ,B.DUR_MM
        //                    ,B.DT_SCAD
        //                    ,B.COD_PROD
        //                    ,B.DT_INI_VLDT_PROD
        //                    ,B.COD_CONV
        //                    ,B.COD_RAMO
        //                    ,B.DT_INI_VLDT_CONV
        //                    ,B.DT_APPLZ_CONV
        //                    ,B.TP_FRM_ASSVA
        //                    ,B.TP_RGM_FISC
        //                    ,B.FL_ESTAS
        //                    ,B.FL_RSH_COMUN
        //                    ,B.FL_RSH_COMUN_COND
        //                    ,B.TP_LIV_GENZ_TIT
        //                    ,B.FL_COP_FINANZ
        //                    ,B.TP_APPLZ_DIR
        //                    ,B.SPE_MED
        //                    ,B.DIR_EMIS
        //                    ,B.DIR_1O_VERS
        //                    ,B.DIR_VERS_AGG
        //                    ,B.COD_DVS
        //                    ,B.FL_FNT_AZ
        //                    ,B.FL_FNT_ADER
        //                    ,B.FL_FNT_TFR
        //                    ,B.FL_FNT_VOLO
        //                    ,B.TP_OPZ_A_SCAD
        //                    ,B.AA_DIFF_PROR_DFLT
        //                    ,B.FL_VER_PROD
        //                    ,B.DUR_GG
        //                    ,B.DIR_QUIET
        //                    ,B.TP_PTF_ESTNO
        //                    ,B.FL_CUM_PRE_CNTR
        //                    ,B.FL_AMMB_MOVI
        //                    ,B.CONV_GECO
        //                    ,B.DS_RIGA
        //                    ,B.DS_OPER_SQL
        //                    ,B.DS_VER
        //                    ,B.DS_TS_INI_CPTZ
        //                    ,B.DS_TS_END_CPTZ
        //                    ,B.DS_UTENTE
        //                    ,B.DS_STATO_ELAB
        //                    ,B.FL_SCUDO_FISC
        //                    ,B.FL_TRASFE
        //                    ,B.FL_TFR_STRC
        //                    ,B.DT_PRESC
        //                    ,B.COD_CONV_AGG
        //                    ,B.SUBCAT_PROD
        //                    ,B.FL_QUEST_ADEGZ_ASS
        //                    ,B.COD_TPA
        //                    ,B.ID_ACC_COMM
        //                    ,B.FL_POLI_CPI_PR
        //                    ,B.FL_POLI_BUNDLING
        //                    ,B.IND_POLI_PRIN_COLL
        //                    ,B.FL_VND_BUNDLE
        //                    ,B.IB_BS
        //                    ,B.FL_POLI_IFP
        //                    ,C.ID_STAT_OGG_BUS
        //                    ,C.ID_OGG
        //                    ,C.TP_OGG
        //                    ,C.ID_MOVI_CRZ
        //                    ,C.ID_MOVI_CHIU
        //                    ,C.DT_INI_EFF
        //                    ,C.DT_END_EFF
        //                    ,C.COD_COMP_ANIA
        //                    ,C.TP_STAT_BUS
        //                    ,C.TP_CAUS
        //                    ,C.DS_RIGA
        //                    ,C.DS_OPER_SQL
        //                    ,C.DS_VER
        //                    ,C.DS_TS_INI_CPTZ
        //                    ,C.DS_TS_END_CPTZ
        //                    ,C.DS_UTENTE
        //                    ,C.DS_STATO_ELAB
        //             INTO
        //                :ADE-ID-ADES
        //               ,:ADE-ID-POLI
        //               ,:ADE-ID-MOVI-CRZ
        //               ,:ADE-ID-MOVI-CHIU
        //                :IND-ADE-ID-MOVI-CHIU
        //               ,:ADE-DT-INI-EFF-DB
        //               ,:ADE-DT-END-EFF-DB
        //               ,:ADE-IB-PREV
        //                :IND-ADE-IB-PREV
        //               ,:ADE-IB-OGG
        //                :IND-ADE-IB-OGG
        //               ,:ADE-COD-COMP-ANIA
        //               ,:ADE-DT-DECOR-DB
        //                :IND-ADE-DT-DECOR
        //               ,:ADE-DT-SCAD-DB
        //                :IND-ADE-DT-SCAD
        //               ,:ADE-ETA-A-SCAD
        //                :IND-ADE-ETA-A-SCAD
        //               ,:ADE-DUR-AA
        //                :IND-ADE-DUR-AA
        //               ,:ADE-DUR-MM
        //                :IND-ADE-DUR-MM
        //               ,:ADE-DUR-GG
        //                :IND-ADE-DUR-GG
        //               ,:ADE-TP-RGM-FISC
        //               ,:ADE-TP-RIAT
        //                :IND-ADE-TP-RIAT
        //               ,:ADE-TP-MOD-PAG-TIT
        //               ,:ADE-TP-IAS
        //                :IND-ADE-TP-IAS
        //               ,:ADE-DT-VARZ-TP-IAS-DB
        //                :IND-ADE-DT-VARZ-TP-IAS
        //               ,:ADE-PRE-NET-IND
        //                :IND-ADE-PRE-NET-IND
        //               ,:ADE-PRE-LRD-IND
        //                :IND-ADE-PRE-LRD-IND
        //               ,:ADE-RAT-LRD-IND
        //                :IND-ADE-RAT-LRD-IND
        //               ,:ADE-PRSTZ-INI-IND
        //                :IND-ADE-PRSTZ-INI-IND
        //               ,:ADE-FL-COINC-ASSTO
        //                :IND-ADE-FL-COINC-ASSTO
        //               ,:ADE-IB-DFLT
        //                :IND-ADE-IB-DFLT
        //               ,:ADE-MOD-CALC
        //                :IND-ADE-MOD-CALC
        //               ,:ADE-TP-FNT-CNBTVA
        //                :IND-ADE-TP-FNT-CNBTVA
        //               ,:ADE-IMP-AZ
        //                :IND-ADE-IMP-AZ
        //               ,:ADE-IMP-ADER
        //                :IND-ADE-IMP-ADER
        //               ,:ADE-IMP-TFR
        //                :IND-ADE-IMP-TFR
        //               ,:ADE-IMP-VOLO
        //                :IND-ADE-IMP-VOLO
        //               ,:ADE-PC-AZ
        //                :IND-ADE-PC-AZ
        //               ,:ADE-PC-ADER
        //                :IND-ADE-PC-ADER
        //               ,:ADE-PC-TFR
        //                :IND-ADE-PC-TFR
        //               ,:ADE-PC-VOLO
        //                :IND-ADE-PC-VOLO
        //               ,:ADE-DT-NOVA-RGM-FISC-DB
        //                :IND-ADE-DT-NOVA-RGM-FISC
        //               ,:ADE-FL-ATTIV
        //                :IND-ADE-FL-ATTIV
        //               ,:ADE-IMP-REC-RIT-VIS
        //                :IND-ADE-IMP-REC-RIT-VIS
        //               ,:ADE-IMP-REC-RIT-ACC
        //                :IND-ADE-IMP-REC-RIT-ACC
        //               ,:ADE-FL-VARZ-STAT-TBGC
        //                :IND-ADE-FL-VARZ-STAT-TBGC
        //               ,:ADE-FL-PROVZA-MIGRAZ
        //                :IND-ADE-FL-PROVZA-MIGRAZ
        //               ,:ADE-IMPB-VIS-DA-REC
        //                :IND-ADE-IMPB-VIS-DA-REC
        //               ,:ADE-DT-DECOR-PREST-BAN-DB
        //                :IND-ADE-DT-DECOR-PREST-BAN
        //               ,:ADE-DT-EFF-VARZ-STAT-T-DB
        //                :IND-ADE-DT-EFF-VARZ-STAT-T
        //               ,:ADE-DS-RIGA
        //               ,:ADE-DS-OPER-SQL
        //               ,:ADE-DS-VER
        //               ,:ADE-DS-TS-INI-CPTZ
        //               ,:ADE-DS-TS-END-CPTZ
        //               ,:ADE-DS-UTENTE
        //               ,:ADE-DS-STATO-ELAB
        //               ,:ADE-CUM-CNBT-CAP
        //                :IND-ADE-CUM-CNBT-CAP
        //               ,:ADE-IMP-GAR-CNBT
        //                :IND-ADE-IMP-GAR-CNBT
        //               ,:ADE-DT-ULT-CONS-CNBT-DB
        //                :IND-ADE-DT-ULT-CONS-CNBT
        //               ,:ADE-IDEN-ISC-FND
        //                :IND-ADE-IDEN-ISC-FND
        //               ,:ADE-NUM-RAT-PIAN
        //                :IND-ADE-NUM-RAT-PIAN
        //               ,:ADE-DT-PRESC-DB
        //                :IND-ADE-DT-PRESC
        //               ,:ADE-CONCS-PREST
        //                :IND-ADE-CONCS-PREST
        //               ,
        //                :POL-ID-POLI
        //               ,:POL-ID-MOVI-CRZ
        //               ,:POL-ID-MOVI-CHIU
        //                :IND-POL-ID-MOVI-CHIU
        //               ,:POL-IB-OGG
        //                :IND-POL-IB-OGG
        //               ,:POL-IB-PROP
        //               ,:POL-DT-PROP-DB
        //                :IND-POL-DT-PROP
        //               ,:POL-DT-INI-EFF-DB
        //               ,:POL-DT-END-EFF-DB
        //               ,:POL-COD-COMP-ANIA
        //               ,:POL-DT-DECOR-DB
        //               ,:POL-DT-EMIS-DB
        //               ,:POL-TP-POLI
        //               ,:POL-DUR-AA
        //                :IND-POL-DUR-AA
        //               ,:POL-DUR-MM
        //                :IND-POL-DUR-MM
        //               ,:POL-DT-SCAD-DB
        //                :IND-POL-DT-SCAD
        //               ,:POL-COD-PROD
        //               ,:POL-DT-INI-VLDT-PROD-DB
        //               ,:POL-COD-CONV
        //                :IND-POL-COD-CONV
        //               ,:POL-COD-RAMO
        //                :IND-POL-COD-RAMO
        //               ,:POL-DT-INI-VLDT-CONV-DB
        //                :IND-POL-DT-INI-VLDT-CONV
        //               ,:POL-DT-APPLZ-CONV-DB
        //                :IND-POL-DT-APPLZ-CONV
        //               ,:POL-TP-FRM-ASSVA
        //               ,:POL-TP-RGM-FISC
        //                :IND-POL-TP-RGM-FISC
        //               ,:POL-FL-ESTAS
        //                :IND-POL-FL-ESTAS
        //               ,:POL-FL-RSH-COMUN
        //                :IND-POL-FL-RSH-COMUN
        //               ,:POL-FL-RSH-COMUN-COND
        //                :IND-POL-FL-RSH-COMUN-COND
        //               ,:POL-TP-LIV-GENZ-TIT
        //               ,:POL-FL-COP-FINANZ
        //                :IND-POL-FL-COP-FINANZ
        //               ,:POL-TP-APPLZ-DIR
        //                :IND-POL-TP-APPLZ-DIR
        //               ,:POL-SPE-MED
        //                :IND-POL-SPE-MED
        //               ,:POL-DIR-EMIS
        //                :IND-POL-DIR-EMIS
        //               ,:POL-DIR-1O-VERS
        //                :IND-POL-DIR-1O-VERS
        //               ,:POL-DIR-VERS-AGG
        //                :IND-POL-DIR-VERS-AGG
        //               ,:POL-COD-DVS
        //                :IND-POL-COD-DVS
        //               ,:POL-FL-FNT-AZ
        //                :IND-POL-FL-FNT-AZ
        //               ,:POL-FL-FNT-ADER
        //                :IND-POL-FL-FNT-ADER
        //               ,:POL-FL-FNT-TFR
        //                :IND-POL-FL-FNT-TFR
        //               ,:POL-FL-FNT-VOLO
        //                :IND-POL-FL-FNT-VOLO
        //               ,:POL-TP-OPZ-A-SCAD
        //                :IND-POL-TP-OPZ-A-SCAD
        //               ,:POL-AA-DIFF-PROR-DFLT
        //                :IND-POL-AA-DIFF-PROR-DFLT
        //               ,:POL-FL-VER-PROD
        //                :IND-POL-FL-VER-PROD
        //               ,:POL-DUR-GG
        //                :IND-POL-DUR-GG
        //               ,:POL-DIR-QUIET
        //                :IND-POL-DIR-QUIET
        //               ,:POL-TP-PTF-ESTNO
        //                :IND-POL-TP-PTF-ESTNO
        //               ,:POL-FL-CUM-PRE-CNTR
        //                :IND-POL-FL-CUM-PRE-CNTR
        //               ,:POL-FL-AMMB-MOVI
        //                :IND-POL-FL-AMMB-MOVI
        //               ,:POL-CONV-GECO
        //                :IND-POL-CONV-GECO
        //               ,:POL-DS-RIGA
        //               ,:POL-DS-OPER-SQL
        //               ,:POL-DS-VER
        //               ,:POL-DS-TS-INI-CPTZ
        //               ,:POL-DS-TS-END-CPTZ
        //               ,:POL-DS-UTENTE
        //               ,:POL-DS-STATO-ELAB
        //               ,:POL-FL-SCUDO-FISC
        //                :IND-POL-FL-SCUDO-FISC
        //               ,:POL-FL-TRASFE
        //                :IND-POL-FL-TRASFE
        //               ,:POL-FL-TFR-STRC
        //                :IND-POL-FL-TFR-STRC
        //               ,:POL-DT-PRESC-DB
        //                :IND-POL-DT-PRESC
        //               ,:POL-COD-CONV-AGG
        //                :IND-POL-COD-CONV-AGG
        //               ,:POL-SUBCAT-PROD
        //                :IND-POL-SUBCAT-PROD
        //               ,:POL-FL-QUEST-ADEGZ-ASS
        //                :IND-POL-FL-QUEST-ADEGZ-ASS
        //               ,:POL-COD-TPA
        //                :IND-POL-COD-TPA
        //               ,:POL-ID-ACC-COMM
        //                :IND-POL-ID-ACC-COMM
        //               ,:POL-FL-POLI-CPI-PR
        //                :IND-POL-FL-POLI-CPI-PR
        //               ,:POL-FL-POLI-BUNDLING
        //                :IND-POL-FL-POLI-BUNDLING
        //               ,:POL-IND-POLI-PRIN-COLL
        //                :IND-POL-IND-POLI-PRIN-COLL
        //               ,:POL-FL-VND-BUNDLE
        //                :IND-POL-FL-VND-BUNDLE
        //               ,:POL-IB-BS
        //                :IND-POL-IB-BS
        //               ,:POL-FL-POLI-IFP
        //                :IND-POL-FL-POLI-IFP
        //               ,
        //                :STB-ID-STAT-OGG-BUS
        //               ,:STB-ID-OGG
        //               ,:STB-TP-OGG
        //               ,:STB-ID-MOVI-CRZ
        //               ,:STB-ID-MOVI-CHIU
        //                :IND-STB-ID-MOVI-CHIU
        //               ,:STB-DT-INI-EFF-DB
        //               ,:STB-DT-END-EFF-DB
        //               ,:STB-COD-COMP-ANIA
        //               ,:STB-TP-STAT-BUS
        //               ,:STB-TP-CAUS
        //               ,:STB-DS-RIGA
        //               ,:STB-DS-OPER-SQL
        //               ,:STB-DS-VER
        //               ,:STB-DS-TS-INI-CPTZ
        //               ,:STB-DS-TS-END-CPTZ
        //               ,:STB-DS-UTENTE
        //               ,:STB-DS-STATO-ELAB
        //             FROM ADES A,
        //                  POLI B,
        //                  STAT_OGG_BUS C
        //             WHERE  B.TP_FRM_ASSVA   IN (:LC821-TP-FRM-ASSVA1,
        //                                         :LC821-TP-FRM-ASSVA2)
        //                    AND A.ID_POLI         =   B.ID_POLI
        //                    AND A.ID_POLI         = :LC821-ID-POLI
        //                    AND A.ID_ADES         =   C.ID_OGG
        //                    AND C.TP_OGG          =  'AD'
        //                    AND C.TP_STAT_BUS     =  'VI'
        //                    AND A.COD_COMP_ANIA   =   B.COD_COMP_ANIA
        //                    AND B.COD_COMP_ANIA   =   C.COD_COMP_ANIA
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND A.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //                    AND B.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND B.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //                    AND C.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND C.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND C.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND C.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND C.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        adesPoliStatOggBusDao.selectRec1(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: B260-OPEN-CURSOR-WC-CPZ<br>*/
    private void b260OpenCursorWcCpz() {
        // COB_CODE: PERFORM B205-DECLARE-CURSOR-WC-CPZ THRU B205-EX.
        b205DeclareCursorWcCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B270-CLOSE-CURSOR-WC-CPZ<br>*/
    private void b270CloseCursorWcCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B280-FETCH-FIRST-WC-CPZ<br>*/
    private void b280FetchFirstWcCpz() {
        // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ    THRU B260-EX.
        b260OpenCursorWcCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
            b290FetchNextWcCpz();
        }
    }

    /**Original name: B290-FETCH-NEXT-WC-CPZ<br>*/
    private void b290FetchNextWcCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C205-DECLARE-CURSOR-WC-NST<br>
	 * <pre>----
	 * ----  gestione WC Senza Storicit`
	 * ----</pre>*/
    private void c205DeclareCursorWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C210-SELECT-WC-NST<br>*/
    private void c210SelectWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C260-OPEN-CURSOR-WC-NST<br>*/
    private void c260OpenCursorWcNst() {
        // COB_CODE: PERFORM C205-DECLARE-CURSOR-WC-NST THRU C205-EX.
        c205DeclareCursorWcNst();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C270-CLOSE-CURSOR-WC-NST<br>*/
    private void c270CloseCursorWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C280-FETCH-FIRST-WC-NST<br>*/
    private void c280FetchFirstWcNst() {
        // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST    THRU C260-EX.
        c260OpenCursorWcNst();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
            c290FetchNextWcNst();
        }
    }

    /**Original name: C290-FETCH-NEXT-WC-NST<br>*/
    private void c290FetchNextWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>----
	 * ----  utilit` comuni a tutti i livelli operazione
	 * ----</pre>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-ADE-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO ADE-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndAdes().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-ID-MOVI-CHIU-NULL
            ws.getAdes().getAdeIdMoviChiu().setAdeIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeIdMoviChiu.Len.ADE_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-ADE-IB-PREV = -1
        //              MOVE HIGH-VALUES TO ADE-IB-PREV-NULL
        //           END-IF
        if (ws.getIndAdes().getIbPrev() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-IB-PREV-NULL
            ws.getAdes().setAdeIbPrev(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdesIvvs0216.Len.ADE_IB_PREV));
        }
        // COB_CODE: IF IND-ADE-IB-OGG = -1
        //              MOVE HIGH-VALUES TO ADE-IB-OGG-NULL
        //           END-IF
        if (ws.getIndAdes().getIbOgg() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-IB-OGG-NULL
            ws.getAdes().setAdeIbOgg(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdesIvvs0216.Len.ADE_IB_OGG));
        }
        // COB_CODE: IF IND-ADE-DT-DECOR = -1
        //              MOVE HIGH-VALUES TO ADE-DT-DECOR-NULL
        //           END-IF
        if (ws.getIndAdes().getDtDecor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-DT-DECOR-NULL
            ws.getAdes().getAdeDtDecor().setAdeDtDecorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeDtDecor.Len.ADE_DT_DECOR_NULL));
        }
        // COB_CODE: IF IND-ADE-DT-SCAD = -1
        //              MOVE HIGH-VALUES TO ADE-DT-SCAD-NULL
        //           END-IF
        if (ws.getIndAdes().getDtScad() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-DT-SCAD-NULL
            ws.getAdes().getAdeDtScad().setAdeDtScadNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeDtScad.Len.ADE_DT_SCAD_NULL));
        }
        // COB_CODE: IF IND-ADE-ETA-A-SCAD = -1
        //              MOVE HIGH-VALUES TO ADE-ETA-A-SCAD-NULL
        //           END-IF
        if (ws.getIndAdes().getEtaAScad() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-ETA-A-SCAD-NULL
            ws.getAdes().getAdeEtaAScad().setAdeEtaAScadNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeEtaAScad.Len.ADE_ETA_A_SCAD_NULL));
        }
        // COB_CODE: IF IND-ADE-DUR-AA = -1
        //              MOVE HIGH-VALUES TO ADE-DUR-AA-NULL
        //           END-IF
        if (ws.getIndAdes().getDurAa() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-DUR-AA-NULL
            ws.getAdes().getAdeDurAa().setAdeDurAaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeDurAa.Len.ADE_DUR_AA_NULL));
        }
        // COB_CODE: IF IND-ADE-DUR-MM = -1
        //              MOVE HIGH-VALUES TO ADE-DUR-MM-NULL
        //           END-IF
        if (ws.getIndAdes().getDurMm() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-DUR-MM-NULL
            ws.getAdes().getAdeDurMm().setAdeDurMmNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeDurMm.Len.ADE_DUR_MM_NULL));
        }
        // COB_CODE: IF IND-ADE-DUR-GG = -1
        //              MOVE HIGH-VALUES TO ADE-DUR-GG-NULL
        //           END-IF
        if (ws.getIndAdes().getDurGg() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-DUR-GG-NULL
            ws.getAdes().getAdeDurGg().setAdeDurGgNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeDurGg.Len.ADE_DUR_GG_NULL));
        }
        // COB_CODE: IF IND-ADE-TP-RIAT = -1
        //              MOVE HIGH-VALUES TO ADE-TP-RIAT-NULL
        //           END-IF
        if (ws.getIndAdes().getTpRiat() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-TP-RIAT-NULL
            ws.getAdes().setAdeTpRiat(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdesIvvs0216.Len.ADE_TP_RIAT));
        }
        // COB_CODE: IF IND-ADE-TP-IAS = -1
        //              MOVE HIGH-VALUES TO ADE-TP-IAS-NULL
        //           END-IF
        if (ws.getIndAdes().getTpIas() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-TP-IAS-NULL
            ws.getAdes().setAdeTpIas(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdesIvvs0216.Len.ADE_TP_IAS));
        }
        // COB_CODE: IF IND-ADE-DT-VARZ-TP-IAS = -1
        //              MOVE HIGH-VALUES TO ADE-DT-VARZ-TP-IAS-NULL
        //           END-IF
        if (ws.getIndAdes().getDtVarzTpIas() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-DT-VARZ-TP-IAS-NULL
            ws.getAdes().getAdeDtVarzTpIas().setAdeDtVarzTpIasNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeDtVarzTpIas.Len.ADE_DT_VARZ_TP_IAS_NULL));
        }
        // COB_CODE: IF IND-ADE-PRE-NET-IND = -1
        //              MOVE HIGH-VALUES TO ADE-PRE-NET-IND-NULL
        //           END-IF
        if (ws.getIndAdes().getPreNetInd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-PRE-NET-IND-NULL
            ws.getAdes().getAdePreNetInd().setAdePreNetIndNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdePreNetInd.Len.ADE_PRE_NET_IND_NULL));
        }
        // COB_CODE: IF IND-ADE-PRE-LRD-IND = -1
        //              MOVE HIGH-VALUES TO ADE-PRE-LRD-IND-NULL
        //           END-IF
        if (ws.getIndAdes().getPreLrdInd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-PRE-LRD-IND-NULL
            ws.getAdes().getAdePreLrdInd().setAdePreLrdIndNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdePreLrdInd.Len.ADE_PRE_LRD_IND_NULL));
        }
        // COB_CODE: IF IND-ADE-RAT-LRD-IND = -1
        //              MOVE HIGH-VALUES TO ADE-RAT-LRD-IND-NULL
        //           END-IF
        if (ws.getIndAdes().getRatLrdInd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-RAT-LRD-IND-NULL
            ws.getAdes().getAdeRatLrdInd().setAdeRatLrdIndNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeRatLrdInd.Len.ADE_RAT_LRD_IND_NULL));
        }
        // COB_CODE: IF IND-ADE-PRSTZ-INI-IND = -1
        //              MOVE HIGH-VALUES TO ADE-PRSTZ-INI-IND-NULL
        //           END-IF
        if (ws.getIndAdes().getPrstzIniInd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-PRSTZ-INI-IND-NULL
            ws.getAdes().getAdePrstzIniInd().setAdePrstzIniIndNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdePrstzIniInd.Len.ADE_PRSTZ_INI_IND_NULL));
        }
        // COB_CODE: IF IND-ADE-FL-COINC-ASSTO = -1
        //              MOVE HIGH-VALUES TO ADE-FL-COINC-ASSTO-NULL
        //           END-IF
        if (ws.getIndAdes().getFlCoincAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-FL-COINC-ASSTO-NULL
            ws.getAdes().setAdeFlCoincAssto(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-ADE-IB-DFLT = -1
        //              MOVE HIGH-VALUES TO ADE-IB-DFLT-NULL
        //           END-IF
        if (ws.getIndAdes().getIbDflt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-IB-DFLT-NULL
            ws.getAdes().setAdeIbDflt(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdesIvvs0216.Len.ADE_IB_DFLT));
        }
        // COB_CODE: IF IND-ADE-MOD-CALC = -1
        //              MOVE HIGH-VALUES TO ADE-MOD-CALC-NULL
        //           END-IF
        if (ws.getIndAdes().getModCalc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-MOD-CALC-NULL
            ws.getAdes().setAdeModCalc(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdesIvvs0216.Len.ADE_MOD_CALC));
        }
        // COB_CODE: IF IND-ADE-TP-FNT-CNBTVA = -1
        //              MOVE HIGH-VALUES TO ADE-TP-FNT-CNBTVA-NULL
        //           END-IF
        if (ws.getIndAdes().getTpFntCnbtva() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-TP-FNT-CNBTVA-NULL
            ws.getAdes().setAdeTpFntCnbtva(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdesIvvs0216.Len.ADE_TP_FNT_CNBTVA));
        }
        // COB_CODE: IF IND-ADE-IMP-AZ = -1
        //              MOVE HIGH-VALUES TO ADE-IMP-AZ-NULL
        //           END-IF
        if (ws.getIndAdes().getImpAz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-IMP-AZ-NULL
            ws.getAdes().getAdeImpAz().setAdeImpAzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeImpAz.Len.ADE_IMP_AZ_NULL));
        }
        // COB_CODE: IF IND-ADE-IMP-ADER = -1
        //              MOVE HIGH-VALUES TO ADE-IMP-ADER-NULL
        //           END-IF
        if (ws.getIndAdes().getImpAder() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-IMP-ADER-NULL
            ws.getAdes().getAdeImpAder().setAdeImpAderNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeImpAder.Len.ADE_IMP_ADER_NULL));
        }
        // COB_CODE: IF IND-ADE-IMP-TFR = -1
        //              MOVE HIGH-VALUES TO ADE-IMP-TFR-NULL
        //           END-IF
        if (ws.getIndAdes().getImpTfr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-IMP-TFR-NULL
            ws.getAdes().getAdeImpTfr().setAdeImpTfrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeImpTfr.Len.ADE_IMP_TFR_NULL));
        }
        // COB_CODE: IF IND-ADE-IMP-VOLO = -1
        //              MOVE HIGH-VALUES TO ADE-IMP-VOLO-NULL
        //           END-IF
        if (ws.getIndAdes().getImpVolo() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-IMP-VOLO-NULL
            ws.getAdes().getAdeImpVolo().setAdeImpVoloNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeImpVolo.Len.ADE_IMP_VOLO_NULL));
        }
        // COB_CODE: IF IND-ADE-PC-AZ = -1
        //              MOVE HIGH-VALUES TO ADE-PC-AZ-NULL
        //           END-IF
        if (ws.getIndAdes().getPcAz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-PC-AZ-NULL
            ws.getAdes().getAdePcAz().setAdePcAzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdePcAz.Len.ADE_PC_AZ_NULL));
        }
        // COB_CODE: IF IND-ADE-PC-ADER = -1
        //              MOVE HIGH-VALUES TO ADE-PC-ADER-NULL
        //           END-IF
        if (ws.getIndAdes().getPcAder() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-PC-ADER-NULL
            ws.getAdes().getAdePcAder().setAdePcAderNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdePcAder.Len.ADE_PC_ADER_NULL));
        }
        // COB_CODE: IF IND-ADE-PC-TFR = -1
        //              MOVE HIGH-VALUES TO ADE-PC-TFR-NULL
        //           END-IF
        if (ws.getIndAdes().getPcTfr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-PC-TFR-NULL
            ws.getAdes().getAdePcTfr().setAdePcTfrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdePcTfr.Len.ADE_PC_TFR_NULL));
        }
        // COB_CODE: IF IND-ADE-PC-VOLO = -1
        //              MOVE HIGH-VALUES TO ADE-PC-VOLO-NULL
        //           END-IF
        if (ws.getIndAdes().getPcVolo() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-PC-VOLO-NULL
            ws.getAdes().getAdePcVolo().setAdePcVoloNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdePcVolo.Len.ADE_PC_VOLO_NULL));
        }
        // COB_CODE: IF IND-ADE-DT-NOVA-RGM-FISC = -1
        //              MOVE HIGH-VALUES TO ADE-DT-NOVA-RGM-FISC-NULL
        //           END-IF
        if (ws.getIndAdes().getDtNovaRgmFisc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-DT-NOVA-RGM-FISC-NULL
            ws.getAdes().getAdeDtNovaRgmFisc().setAdeDtNovaRgmFiscNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeDtNovaRgmFisc.Len.ADE_DT_NOVA_RGM_FISC_NULL));
        }
        // COB_CODE: IF IND-ADE-FL-ATTIV = -1
        //              MOVE HIGH-VALUES TO ADE-FL-ATTIV-NULL
        //           END-IF
        if (ws.getIndAdes().getFlAttiv() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-FL-ATTIV-NULL
            ws.getAdes().setAdeFlAttiv(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-ADE-IMP-REC-RIT-VIS = -1
        //              MOVE HIGH-VALUES TO ADE-IMP-REC-RIT-VIS-NULL
        //           END-IF
        if (ws.getIndAdes().getImpRecRitVis() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-IMP-REC-RIT-VIS-NULL
            ws.getAdes().getAdeImpRecRitVis().setAdeImpRecRitVisNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeImpRecRitVis.Len.ADE_IMP_REC_RIT_VIS_NULL));
        }
        // COB_CODE: IF IND-ADE-IMP-REC-RIT-ACC = -1
        //              MOVE HIGH-VALUES TO ADE-IMP-REC-RIT-ACC-NULL
        //           END-IF
        if (ws.getIndAdes().getImpRecRitAcc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-IMP-REC-RIT-ACC-NULL
            ws.getAdes().getAdeImpRecRitAcc().setAdeImpRecRitAccNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeImpRecRitAcc.Len.ADE_IMP_REC_RIT_ACC_NULL));
        }
        // COB_CODE: IF IND-ADE-FL-VARZ-STAT-TBGC = -1
        //              MOVE HIGH-VALUES TO ADE-FL-VARZ-STAT-TBGC-NULL
        //           END-IF
        if (ws.getIndAdes().getFlVarzStatTbgc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-FL-VARZ-STAT-TBGC-NULL
            ws.getAdes().setAdeFlVarzStatTbgc(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-ADE-FL-PROVZA-MIGRAZ = -1
        //              MOVE HIGH-VALUES TO ADE-FL-PROVZA-MIGRAZ-NULL
        //           END-IF
        if (ws.getIndAdes().getFlProvzaMigraz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-FL-PROVZA-MIGRAZ-NULL
            ws.getAdes().setAdeFlProvzaMigraz(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-ADE-IMPB-VIS-DA-REC = -1
        //              MOVE HIGH-VALUES TO ADE-IMPB-VIS-DA-REC-NULL
        //           END-IF
        if (ws.getIndAdes().getImpbVisDaRec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-IMPB-VIS-DA-REC-NULL
            ws.getAdes().getAdeImpbVisDaRec().setAdeImpbVisDaRecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeImpbVisDaRec.Len.ADE_IMPB_VIS_DA_REC_NULL));
        }
        // COB_CODE: IF IND-ADE-DT-DECOR-PREST-BAN = -1
        //              MOVE HIGH-VALUES TO ADE-DT-DECOR-PREST-BAN-NULL
        //           END-IF
        if (ws.getIndAdes().getDtDecorPrestBan() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-DT-DECOR-PREST-BAN-NULL
            ws.getAdes().getAdeDtDecorPrestBan().setAdeDtDecorPrestBanNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeDtDecorPrestBan.Len.ADE_DT_DECOR_PREST_BAN_NULL));
        }
        // COB_CODE: IF IND-ADE-DT-EFF-VARZ-STAT-T = -1
        //              MOVE HIGH-VALUES TO ADE-DT-EFF-VARZ-STAT-T-NULL
        //           END-IF
        if (ws.getIndAdes().getDtEffVarzStatT() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-DT-EFF-VARZ-STAT-T-NULL
            ws.getAdes().getAdeDtEffVarzStatT().setAdeDtEffVarzStatTNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeDtEffVarzStatT.Len.ADE_DT_EFF_VARZ_STAT_T_NULL));
        }
        // COB_CODE: IF IND-ADE-CUM-CNBT-CAP = -1
        //              MOVE HIGH-VALUES TO ADE-CUM-CNBT-CAP-NULL
        //           END-IF
        if (ws.getIndAdes().getCumCnbtCap() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-CUM-CNBT-CAP-NULL
            ws.getAdes().getAdeCumCnbtCap().setAdeCumCnbtCapNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeCumCnbtCap.Len.ADE_CUM_CNBT_CAP_NULL));
        }
        // COB_CODE: IF IND-ADE-IMP-GAR-CNBT = -1
        //              MOVE HIGH-VALUES TO ADE-IMP-GAR-CNBT-NULL
        //           END-IF
        if (ws.getIndAdes().getImpGarCnbt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-IMP-GAR-CNBT-NULL
            ws.getAdes().getAdeImpGarCnbt().setAdeImpGarCnbtNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeImpGarCnbt.Len.ADE_IMP_GAR_CNBT_NULL));
        }
        // COB_CODE: IF IND-ADE-DT-ULT-CONS-CNBT = -1
        //              MOVE HIGH-VALUES TO ADE-DT-ULT-CONS-CNBT-NULL
        //           END-IF
        if (ws.getIndAdes().getDtUltConsCnbt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-DT-ULT-CONS-CNBT-NULL
            ws.getAdes().getAdeDtUltConsCnbt().setAdeDtUltConsCnbtNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeDtUltConsCnbt.Len.ADE_DT_ULT_CONS_CNBT_NULL));
        }
        // COB_CODE: IF IND-ADE-IDEN-ISC-FND = -1
        //              MOVE HIGH-VALUES TO ADE-IDEN-ISC-FND-NULL
        //           END-IF
        if (ws.getIndAdes().getIdenIscFnd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-IDEN-ISC-FND-NULL
            ws.getAdes().setAdeIdenIscFnd(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdesIvvs0216.Len.ADE_IDEN_ISC_FND));
        }
        // COB_CODE: IF IND-ADE-NUM-RAT-PIAN = -1
        //              MOVE HIGH-VALUES TO ADE-NUM-RAT-PIAN-NULL
        //           END-IF
        if (ws.getIndAdes().getNumRatPian() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-NUM-RAT-PIAN-NULL
            ws.getAdes().getAdeNumRatPian().setAdeNumRatPianNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeNumRatPian.Len.ADE_NUM_RAT_PIAN_NULL));
        }
        // COB_CODE: IF IND-ADE-DT-PRESC = -1
        //              MOVE HIGH-VALUES TO ADE-DT-PRESC-NULL
        //           END-IF
        if (ws.getIndAdes().getDtPresc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-DT-PRESC-NULL
            ws.getAdes().getAdeDtPresc().setAdeDtPrescNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeDtPresc.Len.ADE_DT_PRESC_NULL));
        }
        // COB_CODE: IF IND-ADE-CONCS-PREST = -1
        //              MOVE HIGH-VALUES TO ADE-CONCS-PREST-NULL
        //           END-IF
        if (ws.getIndAdes().getConcsPrest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-CONCS-PREST-NULL
            ws.getAdes().setAdeConcsPrest(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO POL-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndPoli().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-ID-MOVI-CHIU-NULL
            ws.getPoli().getPolIdMoviChiu().setPolIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolIdMoviChiu.Len.POL_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-POL-IB-OGG = -1
        //              MOVE HIGH-VALUES TO POL-IB-OGG-NULL
        //           END-IF
        if (ws.getIndPoli().getDtIniCop() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-IB-OGG-NULL
            ws.getPoli().setPolIbOgg(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Poli.Len.POL_IB_OGG));
        }
        // COB_CODE: IF IND-POL-DT-PROP = -1
        //              MOVE HIGH-VALUES TO POL-DT-PROP-NULL
        //           END-IF
        if (ws.getIndPoli().getDtEndCop() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-DT-PROP-NULL
            ws.getPoli().getPolDtProp().setPolDtPropNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDtProp.Len.POL_DT_PROP_NULL));
        }
        // COB_CODE: IF IND-POL-DUR-AA = -1
        //              MOVE HIGH-VALUES TO POL-DUR-AA-NULL
        //           END-IF
        if (ws.getIndPoli().getPreNet() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-DUR-AA-NULL
            ws.getPoli().getPolDurAa().setPolDurAaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDurAa.Len.POL_DUR_AA_NULL));
        }
        // COB_CODE: IF IND-POL-DUR-MM = -1
        //              MOVE HIGH-VALUES TO POL-DUR-MM-NULL
        //           END-IF
        if (ws.getIndPoli().getIntrFraz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-DUR-MM-NULL
            ws.getPoli().getPolDurMm().setPolDurMmNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDurMm.Len.POL_DUR_MM_NULL));
        }
        // COB_CODE: IF IND-POL-DT-SCAD = -1
        //              MOVE HIGH-VALUES TO POL-DT-SCAD-NULL
        //           END-IF
        if (ws.getIndPoli().getIntrMora() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-DT-SCAD-NULL
            ws.getPoli().getPolDtScad().setPolDtScadNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDtScad.Len.POL_DT_SCAD_NULL));
        }
        // COB_CODE: IF IND-POL-COD-CONV = -1
        //              MOVE HIGH-VALUES TO POL-COD-CONV-NULL
        //           END-IF
        if (ws.getIndPoli().getIntrRetdt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-COD-CONV-NULL
            ws.getPoli().setPolCodConv(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Poli.Len.POL_COD_CONV));
        }
        // COB_CODE: IF IND-POL-COD-RAMO = -1
        //              MOVE HIGH-VALUES TO POL-COD-RAMO-NULL
        //           END-IF
        if (ws.getIndPoli().getIntrRiat() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-COD-RAMO-NULL
            ws.getPoli().setPolCodRamo(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Poli.Len.POL_COD_RAMO));
        }
        // COB_CODE: IF IND-POL-DT-INI-VLDT-CONV = -1
        //              MOVE HIGH-VALUES TO POL-DT-INI-VLDT-CONV-NULL
        //           END-IF
        if (ws.getIndPoli().getDir() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-DT-INI-VLDT-CONV-NULL
            ws.getPoli().getPolDtIniVldtConv().setPolDtIniVldtConvNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDtIniVldtConv.Len.POL_DT_INI_VLDT_CONV_NULL));
        }
        // COB_CODE: IF IND-POL-DT-APPLZ-CONV = -1
        //              MOVE HIGH-VALUES TO POL-DT-APPLZ-CONV-NULL
        //           END-IF
        if (ws.getIndPoli().getSpeMed() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-DT-APPLZ-CONV-NULL
            ws.getPoli().getPolDtApplzConv().setPolDtApplzConvNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDtApplzConv.Len.POL_DT_APPLZ_CONV_NULL));
        }
        // COB_CODE: IF IND-POL-TP-RGM-FISC = -1
        //              MOVE HIGH-VALUES TO POL-TP-RGM-FISC-NULL
        //           END-IF
        if (ws.getIndPoli().getTax() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-TP-RGM-FISC-NULL
            ws.getPoli().setPolTpRgmFisc(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Poli.Len.POL_TP_RGM_FISC));
        }
        // COB_CODE: IF IND-POL-FL-ESTAS = -1
        //              MOVE HIGH-VALUES TO POL-FL-ESTAS-NULL
        //           END-IF
        if (ws.getIndPoli().getSoprSan() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-ESTAS-NULL
            ws.getPoli().setPolFlEstas(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-FL-RSH-COMUN = -1
        //              MOVE HIGH-VALUES TO POL-FL-RSH-COMUN-NULL
        //           END-IF
        if (ws.getIndPoli().getSoprSpo() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-RSH-COMUN-NULL
            ws.getPoli().setPolFlRshComun(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-FL-RSH-COMUN-COND = -1
        //              MOVE HIGH-VALUES TO POL-FL-RSH-COMUN-COND-NULL
        //           END-IF
        if (ws.getIndPoli().getSoprTec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-RSH-COMUN-COND-NULL
            ws.getPoli().setPolFlRshComunCond(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-FL-COP-FINANZ = -1
        //              MOVE HIGH-VALUES TO POL-FL-COP-FINANZ-NULL
        //           END-IF
        if (ws.getIndPoli().getSoprProf() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-COP-FINANZ-NULL
            ws.getPoli().setPolFlCopFinanz(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-TP-APPLZ-DIR = -1
        //              MOVE HIGH-VALUES TO POL-TP-APPLZ-DIR-NULL
        //           END-IF
        if (ws.getIndPoli().getSoprAlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-TP-APPLZ-DIR-NULL
            ws.getPoli().setPolTpApplzDir(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Poli.Len.POL_TP_APPLZ_DIR));
        }
        // COB_CODE: IF IND-POL-SPE-MED = -1
        //              MOVE HIGH-VALUES TO POL-SPE-MED-NULL
        //           END-IF
        if (ws.getIndPoli().getPreTot() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-SPE-MED-NULL
            ws.getPoli().getPolSpeMed().setPolSpeMedNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolSpeMed.Len.POL_SPE_MED_NULL));
        }
        // COB_CODE: IF IND-POL-DIR-EMIS = -1
        //              MOVE HIGH-VALUES TO POL-DIR-EMIS-NULL
        //           END-IF
        if (ws.getIndPoli().getPrePpIas() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-DIR-EMIS-NULL
            ws.getPoli().getPolDirEmis().setPolDirEmisNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDirEmis.Len.POL_DIR_EMIS_NULL));
        }
        // COB_CODE: IF IND-POL-DIR-1O-VERS = -1
        //              MOVE HIGH-VALUES TO POL-DIR-1O-VERS-NULL
        //           END-IF
        if (ws.getIndPoli().getPreSoloRsh() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-DIR-1O-VERS-NULL
            ws.getPoli().getPolDir1oVers().setPolDir1oVersNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDir1oVers.Len.POL_DIR1O_VERS_NULL));
        }
        // COB_CODE: IF IND-POL-DIR-VERS-AGG = -1
        //              MOVE HIGH-VALUES TO POL-DIR-VERS-AGG-NULL
        //           END-IF
        if (ws.getIndPoli().getCarAcq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-DIR-VERS-AGG-NULL
            ws.getPoli().getPolDirVersAgg().setPolDirVersAggNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDirVersAgg.Len.POL_DIR_VERS_AGG_NULL));
        }
        // COB_CODE: IF IND-POL-COD-DVS = -1
        //              MOVE HIGH-VALUES TO POL-COD-DVS-NULL
        //           END-IF
        if (ws.getIndPoli().getCarGest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-COD-DVS-NULL
            ws.getPoli().setPolCodDvs(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Poli.Len.POL_COD_DVS));
        }
        // COB_CODE: IF IND-POL-FL-FNT-AZ = -1
        //              MOVE HIGH-VALUES TO POL-FL-FNT-AZ-NULL
        //           END-IF
        if (ws.getIndPoli().getCarInc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-FNT-AZ-NULL
            ws.getPoli().setPolFlFntAz(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-FL-FNT-ADER = -1
        //              MOVE HIGH-VALUES TO POL-FL-FNT-ADER-NULL
        //           END-IF
        if (ws.getIndPoli().getProvAcq1aa() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-FNT-ADER-NULL
            ws.getPoli().setPolFlFntAder(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-FL-FNT-TFR = -1
        //              MOVE HIGH-VALUES TO POL-FL-FNT-TFR-NULL
        //           END-IF
        if (ws.getIndPoli().getProvAcq2aa() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-FNT-TFR-NULL
            ws.getPoli().setPolFlFntTfr(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-FL-FNT-VOLO = -1
        //              MOVE HIGH-VALUES TO POL-FL-FNT-VOLO-NULL
        //           END-IF
        if (ws.getIndPoli().getProvRicor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-FNT-VOLO-NULL
            ws.getPoli().setPolFlFntVolo(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-TP-OPZ-A-SCAD = -1
        //              MOVE HIGH-VALUES TO POL-TP-OPZ-A-SCAD-NULL
        //           END-IF
        if (ws.getIndPoli().getProvInc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-TP-OPZ-A-SCAD-NULL
            ws.getPoli().setPolTpOpzAScad(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Poli.Len.POL_TP_OPZ_A_SCAD));
        }
        // COB_CODE: IF IND-POL-AA-DIFF-PROR-DFLT = -1
        //              MOVE HIGH-VALUES TO POL-AA-DIFF-PROR-DFLT-NULL
        //           END-IF
        if (ws.getIndPoli().getProvDaRec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-AA-DIFF-PROR-DFLT-NULL
            ws.getPoli().getPolAaDiffProrDflt().setPolAaDiffProrDfltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolAaDiffProrDflt.Len.POL_AA_DIFF_PROR_DFLT_NULL));
        }
        // COB_CODE: IF IND-POL-FL-VER-PROD = -1
        //              MOVE HIGH-VALUES TO POL-FL-VER-PROD-NULL
        //           END-IF
        if (ws.getIndPoli().getCodDvs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-VER-PROD-NULL
            ws.getPoli().setPolFlVerProd(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Poli.Len.POL_FL_VER_PROD));
        }
        // COB_CODE: IF IND-POL-DUR-GG = -1
        //              MOVE HIGH-VALUES TO POL-DUR-GG-NULL
        //           END-IF
        if (ws.getIndPoli().getFrqMovi() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-DUR-GG-NULL
            ws.getPoli().getPolDurGg().setPolDurGgNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDurGg.Len.POL_DUR_GG_NULL));
        }
        // COB_CODE: IF IND-POL-DIR-QUIET = -1
        //              MOVE HIGH-VALUES TO POL-DIR-QUIET-NULL
        //           END-IF
        if (ws.getIndPoli().getCodTari() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-DIR-QUIET-NULL
            ws.getPoli().getPolDirQuiet().setPolDirQuietNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDirQuiet.Len.POL_DIR_QUIET_NULL));
        }
        // COB_CODE: IF IND-POL-TP-PTF-ESTNO = -1
        //              MOVE HIGH-VALUES TO POL-TP-PTF-ESTNO-NULL
        //           END-IF
        if (ws.getIndPoli().getImpAz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-TP-PTF-ESTNO-NULL
            ws.getPoli().setPolTpPtfEstno(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Poli.Len.POL_TP_PTF_ESTNO));
        }
        // COB_CODE: IF IND-POL-FL-CUM-PRE-CNTR = -1
        //              MOVE HIGH-VALUES TO POL-FL-CUM-PRE-CNTR-NULL
        //           END-IF
        if (ws.getIndPoli().getImpAder() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-CUM-PRE-CNTR-NULL
            ws.getPoli().setPolFlCumPreCntr(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-FL-AMMB-MOVI = -1
        //              MOVE HIGH-VALUES TO POL-FL-AMMB-MOVI-NULL
        //           END-IF
        if (ws.getIndPoli().getImpTfr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-AMMB-MOVI-NULL
            ws.getPoli().setPolFlAmmbMovi(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-CONV-GECO = -1
        //              MOVE HIGH-VALUES TO POL-CONV-GECO-NULL
        //           END-IF
        if (ws.getIndPoli().getImpVolo() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-CONV-GECO-NULL
            ws.getPoli().setPolConvGeco(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Poli.Len.POL_CONV_GECO));
        }
        // COB_CODE: IF IND-POL-FL-SCUDO-FISC = -1
        //              MOVE HIGH-VALUES TO POL-FL-SCUDO-FISC-NULL
        //           END-IF
        if (ws.getIndPoli().getManfeeAntic() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-SCUDO-FISC-NULL
            ws.getPoli().setPolFlScudoFisc(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-FL-TRASFE = -1
        //              MOVE HIGH-VALUES TO POL-FL-TRASFE-NULL
        //           END-IF
        if (ws.getIndPoli().getManfeeRicor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-TRASFE-NULL
            ws.getPoli().setPolFlTrasfe(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-FL-TFR-STRC = -1
        //              MOVE HIGH-VALUES TO POL-FL-TFR-STRC-NULL
        //           END-IF
        if (ws.getIndPoli().getManfeeRec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-TFR-STRC-NULL
            ws.getPoli().setPolFlTfrStrc(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-DT-PRESC = -1
        //              MOVE HIGH-VALUES TO POL-DT-PRESC-NULL
        //           END-IF
        if (ws.getIndPoli().getDtEsiTit() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-DT-PRESC-NULL
            ws.getPoli().getPolDtPresc().setPolDtPrescNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDtPresc.Len.POL_DT_PRESC_NULL));
        }
        // COB_CODE: IF IND-POL-COD-CONV-AGG = -1
        //              MOVE HIGH-VALUES TO POL-COD-CONV-AGG-NULL
        //           END-IF
        if (ws.getIndPoli().getSpeAge() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-COD-CONV-AGG-NULL
            ws.getPoli().setPolCodConvAgg(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Poli.Len.POL_COD_CONV_AGG));
        }
        // COB_CODE: IF IND-POL-SUBCAT-PROD = -1
        //              MOVE HIGH-VALUES TO POL-SUBCAT-PROD-NULL
        //           END-IF
        if (ws.getIndPoli().getCarIas() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-SUBCAT-PROD-NULL
            ws.getPoli().setPolSubcatProd(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Poli.Len.POL_SUBCAT_PROD));
        }
        // COB_CODE: IF IND-POL-FL-QUEST-ADEGZ-ASS = -1
        //              MOVE HIGH-VALUES TO POL-FL-QUEST-ADEGZ-ASS-NULL
        //           END-IF
        if (ws.getIndPoli().getTotIntrPrest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-QUEST-ADEGZ-ASS-NULL
            ws.getPoli().setPolFlQuestAdegzAss(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-COD-TPA = -1
        //              MOVE HIGH-VALUES TO POL-COD-TPA-NULL
        //           END-IF
        if (ws.getIndPoli().getImpTrasfe() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-COD-TPA-NULL
            ws.getPoli().setPolCodTpa(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Poli.Len.POL_COD_TPA));
        }
        // COB_CODE: IF IND-POL-ID-ACC-COMM = -1
        //              MOVE HIGH-VALUES TO POL-ID-ACC-COMM-NULL
        //           END-IF
        if (ws.getIndPoli().getImpTfrStrc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-ID-ACC-COMM-NULL
            ws.getPoli().getPolIdAccComm().setPolIdAccCommNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolIdAccComm.Len.POL_ID_ACC_COMM_NULL));
        }
        // COB_CODE: IF IND-POL-FL-POLI-CPI-PR = -1
        //              MOVE HIGH-VALUES TO POL-FL-POLI-CPI-PR-NULL
        //           END-IF
        if (ws.getIndPoli().getNumGgRitardoPag() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-POLI-CPI-PR-NULL
            ws.getPoli().setPolFlPoliCpiPr(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-FL-POLI-BUNDLING = -1
        //              MOVE HIGH-VALUES TO POL-FL-POLI-BUNDLING-NULL
        //           END-IF
        if (ws.getIndPoli().getNumGgRival() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-POLI-BUNDLING-NULL
            ws.getPoli().setPolFlPoliBundling(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-IND-POLI-PRIN-COLL = -1
        //              MOVE HIGH-VALUES TO POL-IND-POLI-PRIN-COLL-NULL
        //           END-IF
        if (ws.getIndPoli().getAcqExp() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-IND-POLI-PRIN-COLL-NULL
            ws.getPoli().setPolIndPoliPrinColl(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-FL-VND-BUNDLE = -1
        //              MOVE HIGH-VALUES TO POL-FL-VND-BUNDLE-NULL
        //           END-IF
        if (ws.getIndPoli().getRemunAss() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-VND-BUNDLE-NULL
            ws.getPoli().setPolFlVndBundle(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-IB-BS = -1
        //              MOVE HIGH-VALUES TO POL-IB-BS-NULL
        //           END-IF
        if (ws.getIndPoli().getCommisInter() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-IB-BS-NULL
            ws.getPoli().setPolIbBs(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Poli.Len.POL_IB_BS));
        }
        // COB_CODE: IF IND-POL-FL-POLI-IFP = -1
        //              MOVE HIGH-VALUES TO POL-FL-POLI-IFP-NULL
        //           END-IF
        if (ws.getIndPoli().getCnbtAntirac() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-POLI-IFP-NULL
            ws.getPoli().setPolFlPoliIfp(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-STB-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO STB-ID-MOVI-CHIU-NULL
        //           END-IF.
        if (ws.getIndStbIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO STB-ID-MOVI-CHIU-NULL
            ws.getStatOggBus().getStbIdMoviChiu().setStbIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, StbIdMoviChiu.Len.STB_ID_MOVI_CHIU_NULL));
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE ADE-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getAdesDb().getIniEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO ADE-DT-INI-EFF
        ws.getAdes().setAdeDtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE ADE-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getAdesDb().getEndEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO ADE-DT-END-EFF
        ws.getAdes().setAdeDtEndEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-ADE-DT-DECOR = 0
        //               MOVE WS-DATE-N      TO ADE-DT-DECOR
        //           END-IF
        if (ws.getIndAdes().getDtDecor() == 0) {
            // COB_CODE: MOVE ADE-DT-DECOR-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getAdesDb().getDecorDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO ADE-DT-DECOR
            ws.getAdes().getAdeDtDecor().setAdeDtDecor(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-ADE-DT-SCAD = 0
        //               MOVE WS-DATE-N      TO ADE-DT-SCAD
        //           END-IF
        if (ws.getIndAdes().getDtScad() == 0) {
            // COB_CODE: MOVE ADE-DT-SCAD-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getAdesDb().getScadDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO ADE-DT-SCAD
            ws.getAdes().getAdeDtScad().setAdeDtScad(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-ADE-DT-VARZ-TP-IAS = 0
        //               MOVE WS-DATE-N      TO ADE-DT-VARZ-TP-IAS
        //           END-IF
        if (ws.getIndAdes().getDtVarzTpIas() == 0) {
            // COB_CODE: MOVE ADE-DT-VARZ-TP-IAS-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getAdesDb().getVarzTpIasDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO ADE-DT-VARZ-TP-IAS
            ws.getAdes().getAdeDtVarzTpIas().setAdeDtVarzTpIas(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-ADE-DT-NOVA-RGM-FISC = 0
        //               MOVE WS-DATE-N      TO ADE-DT-NOVA-RGM-FISC
        //           END-IF
        if (ws.getIndAdes().getDtNovaRgmFisc() == 0) {
            // COB_CODE: MOVE ADE-DT-NOVA-RGM-FISC-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getAdesDb().getNovaRgmFiscDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO ADE-DT-NOVA-RGM-FISC
            ws.getAdes().getAdeDtNovaRgmFisc().setAdeDtNovaRgmFisc(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-ADE-DT-DECOR-PREST-BAN = 0
        //               MOVE WS-DATE-N      TO ADE-DT-DECOR-PREST-BAN
        //           END-IF
        if (ws.getIndAdes().getDtDecorPrestBan() == 0) {
            // COB_CODE: MOVE ADE-DT-DECOR-PREST-BAN-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getAdesDb().getDecorPrestBanDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO ADE-DT-DECOR-PREST-BAN
            ws.getAdes().getAdeDtDecorPrestBan().setAdeDtDecorPrestBan(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-ADE-DT-EFF-VARZ-STAT-T = 0
        //               MOVE WS-DATE-N      TO ADE-DT-EFF-VARZ-STAT-T
        //           END-IF
        if (ws.getIndAdes().getDtEffVarzStatT() == 0) {
            // COB_CODE: MOVE ADE-DT-EFF-VARZ-STAT-T-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getAdesDb().getEffVarzStatTDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO ADE-DT-EFF-VARZ-STAT-T
            ws.getAdes().getAdeDtEffVarzStatT().setAdeDtEffVarzStatT(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-ADE-DT-ULT-CONS-CNBT = 0
        //               MOVE WS-DATE-N      TO ADE-DT-ULT-CONS-CNBT
        //           END-IF
        if (ws.getIndAdes().getDtUltConsCnbt() == 0) {
            // COB_CODE: MOVE ADE-DT-ULT-CONS-CNBT-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getAdesDb().getUltConsCnbtDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO ADE-DT-ULT-CONS-CNBT
            ws.getAdes().getAdeDtUltConsCnbt().setAdeDtUltConsCnbt(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-ADE-DT-PRESC = 0
        //               MOVE WS-DATE-N      TO ADE-DT-PRESC
        //           END-IF
        if (ws.getIndAdes().getDtPresc() == 0) {
            // COB_CODE: MOVE ADE-DT-PRESC-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getAdesDb().getPrescDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO ADE-DT-PRESC
            ws.getAdes().getAdeDtPresc().setAdeDtPresc(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-POL-DT-PROP = 0
        //               MOVE WS-DATE-N      TO POL-DT-PROP
        //           END-IF
        if (ws.getIndPoli().getDtEndCop() == 0) {
            // COB_CODE: MOVE POL-DT-PROP-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getPoliDb().getIniEffDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO POL-DT-PROP
            ws.getPoli().getPolDtProp().setPolDtProp(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: MOVE POL-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getPoliDb().getEndEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO POL-DT-INI-EFF
        ws.getPoli().setPolDtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE POL-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getPoliDb().getDecorDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO POL-DT-END-EFF
        ws.getPoli().setPolDtEndEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE POL-DT-DECOR-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getPoliDb().getScadDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO POL-DT-DECOR
        ws.getPoli().setPolDtDecor(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE POL-DT-EMIS-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getPoliDb().getVarzTpIasDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO POL-DT-EMIS
        ws.getPoli().setPolDtEmis(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-POL-DT-SCAD = 0
        //               MOVE WS-DATE-N      TO POL-DT-SCAD
        //           END-IF
        if (ws.getIndPoli().getIntrMora() == 0) {
            // COB_CODE: MOVE POL-DT-SCAD-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getPoliDb().getNovaRgmFiscDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO POL-DT-SCAD
            ws.getPoli().getPolDtScad().setPolDtScad(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: MOVE POL-DT-INI-VLDT-PROD-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getPoliDb().getDecorPrestBanDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO POL-DT-INI-VLDT-PROD
        ws.getPoli().setPolDtIniVldtProd(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-POL-DT-INI-VLDT-CONV = 0
        //               MOVE WS-DATE-N      TO POL-DT-INI-VLDT-CONV
        //           END-IF
        if (ws.getIndPoli().getDir() == 0) {
            // COB_CODE: MOVE POL-DT-INI-VLDT-CONV-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getPoliDb().getEffVarzStatTDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO POL-DT-INI-VLDT-CONV
            ws.getPoli().getPolDtIniVldtConv().setPolDtIniVldtConv(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-POL-DT-APPLZ-CONV = 0
        //               MOVE WS-DATE-N      TO POL-DT-APPLZ-CONV
        //           END-IF
        if (ws.getIndPoli().getSpeMed() == 0) {
            // COB_CODE: MOVE POL-DT-APPLZ-CONV-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getPoliDb().getUltConsCnbtDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO POL-DT-APPLZ-CONV
            ws.getPoli().getPolDtApplzConv().setPolDtApplzConv(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-POL-DT-PRESC = 0
        //               MOVE WS-DATE-N      TO POL-DT-PRESC
        //           END-IF
        if (ws.getIndPoli().getDtEsiTit() == 0) {
            // COB_CODE: MOVE POL-DT-PRESC-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getPoliDb().getPrescDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO POL-DT-PRESC
            ws.getPoli().getPolDtPresc().setPolDtPresc(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: MOVE STB-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getIdbvstb3().getStbDtIniEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO STB-DT-INI-EFF
        ws.getStatOggBus().setStbDtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE STB-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getIdbvstb3().getStbDtEndEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO STB-DT-END-EFF.
        ws.getStatOggBus().setStbDtEndEff(ws.getIdsv0010().getWsDateN());
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z970-CODICE-ADHOC-PRE<br>
	 * <pre>----
	 * ----  prevede statements AD HOC PRE Query
	 * ----</pre>*/
    private void z970CodiceAdhocPre() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z980-CODICE-ADHOC-POST<br>
	 * <pre>----
	 * ----  prevede statements AD HOC POST Query
	 * ----</pre>*/
    private void z980CodiceAdhocPost() {
        // COB_CODE: MOVE ADES          TO  LDBVC821-ADES
        ldbvc821.getLdbvc821Ades().setLdbvc821AdesBytes(ws.getAdes().getAdesBytes());
        // COB_CODE: MOVE POLI          TO  LDBVC821-POLI
        ldbvc821.getLdbvc821Poli().setLdbvc821PoliBytes(ws.getPoli().getPoliBytes());
        // COB_CODE: MOVE STAT-OGG-BUS  TO
        //                LDBVC821-STAT-OGG-BUS.
        ldbvc821.getLdbvc821StatOggBus().setLdbvc821StatOggBusBytes(ws.getStatOggBus().getStatOggBusBytes());
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public int getAdeCodCompAnia() {
        return ws.getAdes().getAdeCodCompAnia();
    }

    @Override
    public void setAdeCodCompAnia(int adeCodCompAnia) {
        this.ws.getAdes().setAdeCodCompAnia(adeCodCompAnia);
    }

    @Override
    public char getAdeConcsPrest() {
        return ws.getAdes().getAdeConcsPrest();
    }

    @Override
    public void setAdeConcsPrest(char adeConcsPrest) {
        this.ws.getAdes().setAdeConcsPrest(adeConcsPrest);
    }

    @Override
    public Character getAdeConcsPrestObj() {
        if (ws.getIndAdes().getConcsPrest() >= 0) {
            return ((Character)getAdeConcsPrest());
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeConcsPrestObj(Character adeConcsPrestObj) {
        if (adeConcsPrestObj != null) {
            setAdeConcsPrest(((char)adeConcsPrestObj));
            ws.getIndAdes().setConcsPrest(((short)0));
        }
        else {
            ws.getIndAdes().setConcsPrest(((short)-1));
        }
    }

    @Override
    public AfDecimal getAdeCumCnbtCap() {
        return ws.getAdes().getAdeCumCnbtCap().getAdeCumCnbtCap();
    }

    @Override
    public void setAdeCumCnbtCap(AfDecimal adeCumCnbtCap) {
        this.ws.getAdes().getAdeCumCnbtCap().setAdeCumCnbtCap(adeCumCnbtCap.copy());
    }

    @Override
    public AfDecimal getAdeCumCnbtCapObj() {
        if (ws.getIndAdes().getCumCnbtCap() >= 0) {
            return getAdeCumCnbtCap();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeCumCnbtCapObj(AfDecimal adeCumCnbtCapObj) {
        if (adeCumCnbtCapObj != null) {
            setAdeCumCnbtCap(new AfDecimal(adeCumCnbtCapObj, 15, 3));
            ws.getIndAdes().setCumCnbtCap(((short)0));
        }
        else {
            ws.getIndAdes().setCumCnbtCap(((short)-1));
        }
    }

    @Override
    public char getAdeDsOperSql() {
        return ws.getAdes().getAdeDsOperSql();
    }

    @Override
    public void setAdeDsOperSql(char adeDsOperSql) {
        this.ws.getAdes().setAdeDsOperSql(adeDsOperSql);
    }

    @Override
    public long getAdeDsRiga() {
        return ws.getAdes().getAdeDsRiga();
    }

    @Override
    public void setAdeDsRiga(long adeDsRiga) {
        this.ws.getAdes().setAdeDsRiga(adeDsRiga);
    }

    @Override
    public char getAdeDsStatoElab() {
        return ws.getAdes().getAdeDsStatoElab();
    }

    @Override
    public void setAdeDsStatoElab(char adeDsStatoElab) {
        this.ws.getAdes().setAdeDsStatoElab(adeDsStatoElab);
    }

    @Override
    public long getAdeDsTsEndCptz() {
        return ws.getAdes().getAdeDsTsEndCptz();
    }

    @Override
    public void setAdeDsTsEndCptz(long adeDsTsEndCptz) {
        this.ws.getAdes().setAdeDsTsEndCptz(adeDsTsEndCptz);
    }

    @Override
    public long getAdeDsTsIniCptz() {
        return ws.getAdes().getAdeDsTsIniCptz();
    }

    @Override
    public void setAdeDsTsIniCptz(long adeDsTsIniCptz) {
        this.ws.getAdes().setAdeDsTsIniCptz(adeDsTsIniCptz);
    }

    @Override
    public String getAdeDsUtente() {
        return ws.getAdes().getAdeDsUtente();
    }

    @Override
    public void setAdeDsUtente(String adeDsUtente) {
        this.ws.getAdes().setAdeDsUtente(adeDsUtente);
    }

    @Override
    public int getAdeDsVer() {
        return ws.getAdes().getAdeDsVer();
    }

    @Override
    public void setAdeDsVer(int adeDsVer) {
        this.ws.getAdes().setAdeDsVer(adeDsVer);
    }

    @Override
    public String getAdeDtDecorDb() {
        return ws.getAdesDb().getDecorDb();
    }

    @Override
    public void setAdeDtDecorDb(String adeDtDecorDb) {
        this.ws.getAdesDb().setDecorDb(adeDtDecorDb);
    }

    @Override
    public String getAdeDtDecorDbObj() {
        if (ws.getIndAdes().getDtDecor() >= 0) {
            return getAdeDtDecorDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeDtDecorDbObj(String adeDtDecorDbObj) {
        if (adeDtDecorDbObj != null) {
            setAdeDtDecorDb(adeDtDecorDbObj);
            ws.getIndAdes().setDtDecor(((short)0));
        }
        else {
            ws.getIndAdes().setDtDecor(((short)-1));
        }
    }

    @Override
    public String getAdeDtDecorPrestBanDb() {
        return ws.getAdesDb().getDecorPrestBanDb();
    }

    @Override
    public void setAdeDtDecorPrestBanDb(String adeDtDecorPrestBanDb) {
        this.ws.getAdesDb().setDecorPrestBanDb(adeDtDecorPrestBanDb);
    }

    @Override
    public String getAdeDtDecorPrestBanDbObj() {
        if (ws.getIndAdes().getDtDecorPrestBan() >= 0) {
            return getAdeDtDecorPrestBanDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeDtDecorPrestBanDbObj(String adeDtDecorPrestBanDbObj) {
        if (adeDtDecorPrestBanDbObj != null) {
            setAdeDtDecorPrestBanDb(adeDtDecorPrestBanDbObj);
            ws.getIndAdes().setDtDecorPrestBan(((short)0));
        }
        else {
            ws.getIndAdes().setDtDecorPrestBan(((short)-1));
        }
    }

    @Override
    public String getAdeDtEffVarzStatTDb() {
        return ws.getAdesDb().getEffVarzStatTDb();
    }

    @Override
    public void setAdeDtEffVarzStatTDb(String adeDtEffVarzStatTDb) {
        this.ws.getAdesDb().setEffVarzStatTDb(adeDtEffVarzStatTDb);
    }

    @Override
    public String getAdeDtEffVarzStatTDbObj() {
        if (ws.getIndAdes().getDtEffVarzStatT() >= 0) {
            return getAdeDtEffVarzStatTDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeDtEffVarzStatTDbObj(String adeDtEffVarzStatTDbObj) {
        if (adeDtEffVarzStatTDbObj != null) {
            setAdeDtEffVarzStatTDb(adeDtEffVarzStatTDbObj);
            ws.getIndAdes().setDtEffVarzStatT(((short)0));
        }
        else {
            ws.getIndAdes().setDtEffVarzStatT(((short)-1));
        }
    }

    @Override
    public String getAdeDtEndEffDb() {
        return ws.getAdesDb().getEndEffDb();
    }

    @Override
    public void setAdeDtEndEffDb(String adeDtEndEffDb) {
        this.ws.getAdesDb().setEndEffDb(adeDtEndEffDb);
    }

    @Override
    public String getAdeDtIniEffDb() {
        return ws.getAdesDb().getIniEffDb();
    }

    @Override
    public void setAdeDtIniEffDb(String adeDtIniEffDb) {
        this.ws.getAdesDb().setIniEffDb(adeDtIniEffDb);
    }

    @Override
    public String getAdeDtNovaRgmFiscDb() {
        return ws.getAdesDb().getNovaRgmFiscDb();
    }

    @Override
    public void setAdeDtNovaRgmFiscDb(String adeDtNovaRgmFiscDb) {
        this.ws.getAdesDb().setNovaRgmFiscDb(adeDtNovaRgmFiscDb);
    }

    @Override
    public String getAdeDtNovaRgmFiscDbObj() {
        if (ws.getIndAdes().getDtNovaRgmFisc() >= 0) {
            return getAdeDtNovaRgmFiscDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeDtNovaRgmFiscDbObj(String adeDtNovaRgmFiscDbObj) {
        if (adeDtNovaRgmFiscDbObj != null) {
            setAdeDtNovaRgmFiscDb(adeDtNovaRgmFiscDbObj);
            ws.getIndAdes().setDtNovaRgmFisc(((short)0));
        }
        else {
            ws.getIndAdes().setDtNovaRgmFisc(((short)-1));
        }
    }

    @Override
    public String getAdeDtPrescDb() {
        return ws.getAdesDb().getPrescDb();
    }

    @Override
    public void setAdeDtPrescDb(String adeDtPrescDb) {
        this.ws.getAdesDb().setPrescDb(adeDtPrescDb);
    }

    @Override
    public String getAdeDtPrescDbObj() {
        if (ws.getIndAdes().getDtPresc() >= 0) {
            return getAdeDtPrescDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeDtPrescDbObj(String adeDtPrescDbObj) {
        if (adeDtPrescDbObj != null) {
            setAdeDtPrescDb(adeDtPrescDbObj);
            ws.getIndAdes().setDtPresc(((short)0));
        }
        else {
            ws.getIndAdes().setDtPresc(((short)-1));
        }
    }

    @Override
    public String getAdeDtScadDb() {
        return ws.getAdesDb().getScadDb();
    }

    @Override
    public void setAdeDtScadDb(String adeDtScadDb) {
        this.ws.getAdesDb().setScadDb(adeDtScadDb);
    }

    @Override
    public String getAdeDtScadDbObj() {
        if (ws.getIndAdes().getDtScad() >= 0) {
            return getAdeDtScadDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeDtScadDbObj(String adeDtScadDbObj) {
        if (adeDtScadDbObj != null) {
            setAdeDtScadDb(adeDtScadDbObj);
            ws.getIndAdes().setDtScad(((short)0));
        }
        else {
            ws.getIndAdes().setDtScad(((short)-1));
        }
    }

    @Override
    public String getAdeDtUltConsCnbtDb() {
        return ws.getAdesDb().getUltConsCnbtDb();
    }

    @Override
    public void setAdeDtUltConsCnbtDb(String adeDtUltConsCnbtDb) {
        this.ws.getAdesDb().setUltConsCnbtDb(adeDtUltConsCnbtDb);
    }

    @Override
    public String getAdeDtUltConsCnbtDbObj() {
        if (ws.getIndAdes().getDtUltConsCnbt() >= 0) {
            return getAdeDtUltConsCnbtDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeDtUltConsCnbtDbObj(String adeDtUltConsCnbtDbObj) {
        if (adeDtUltConsCnbtDbObj != null) {
            setAdeDtUltConsCnbtDb(adeDtUltConsCnbtDbObj);
            ws.getIndAdes().setDtUltConsCnbt(((short)0));
        }
        else {
            ws.getIndAdes().setDtUltConsCnbt(((short)-1));
        }
    }

    @Override
    public String getAdeDtVarzTpIasDb() {
        return ws.getAdesDb().getVarzTpIasDb();
    }

    @Override
    public void setAdeDtVarzTpIasDb(String adeDtVarzTpIasDb) {
        this.ws.getAdesDb().setVarzTpIasDb(adeDtVarzTpIasDb);
    }

    @Override
    public String getAdeDtVarzTpIasDbObj() {
        if (ws.getIndAdes().getDtVarzTpIas() >= 0) {
            return getAdeDtVarzTpIasDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeDtVarzTpIasDbObj(String adeDtVarzTpIasDbObj) {
        if (adeDtVarzTpIasDbObj != null) {
            setAdeDtVarzTpIasDb(adeDtVarzTpIasDbObj);
            ws.getIndAdes().setDtVarzTpIas(((short)0));
        }
        else {
            ws.getIndAdes().setDtVarzTpIas(((short)-1));
        }
    }

    @Override
    public int getAdeDurAa() {
        return ws.getAdes().getAdeDurAa().getAdeDurAa();
    }

    @Override
    public void setAdeDurAa(int adeDurAa) {
        this.ws.getAdes().getAdeDurAa().setAdeDurAa(adeDurAa);
    }

    @Override
    public Integer getAdeDurAaObj() {
        if (ws.getIndAdes().getDurAa() >= 0) {
            return ((Integer)getAdeDurAa());
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeDurAaObj(Integer adeDurAaObj) {
        if (adeDurAaObj != null) {
            setAdeDurAa(((int)adeDurAaObj));
            ws.getIndAdes().setDurAa(((short)0));
        }
        else {
            ws.getIndAdes().setDurAa(((short)-1));
        }
    }

    @Override
    public int getAdeDurGg() {
        return ws.getAdes().getAdeDurGg().getAdeDurGg();
    }

    @Override
    public void setAdeDurGg(int adeDurGg) {
        this.ws.getAdes().getAdeDurGg().setAdeDurGg(adeDurGg);
    }

    @Override
    public Integer getAdeDurGgObj() {
        if (ws.getIndAdes().getDurGg() >= 0) {
            return ((Integer)getAdeDurGg());
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeDurGgObj(Integer adeDurGgObj) {
        if (adeDurGgObj != null) {
            setAdeDurGg(((int)adeDurGgObj));
            ws.getIndAdes().setDurGg(((short)0));
        }
        else {
            ws.getIndAdes().setDurGg(((short)-1));
        }
    }

    @Override
    public int getAdeDurMm() {
        return ws.getAdes().getAdeDurMm().getAdeDurMm();
    }

    @Override
    public void setAdeDurMm(int adeDurMm) {
        this.ws.getAdes().getAdeDurMm().setAdeDurMm(adeDurMm);
    }

    @Override
    public Integer getAdeDurMmObj() {
        if (ws.getIndAdes().getDurMm() >= 0) {
            return ((Integer)getAdeDurMm());
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeDurMmObj(Integer adeDurMmObj) {
        if (adeDurMmObj != null) {
            setAdeDurMm(((int)adeDurMmObj));
            ws.getIndAdes().setDurMm(((short)0));
        }
        else {
            ws.getIndAdes().setDurMm(((short)-1));
        }
    }

    @Override
    public int getAdeEtaAScad() {
        return ws.getAdes().getAdeEtaAScad().getAdeEtaAScad();
    }

    @Override
    public void setAdeEtaAScad(int adeEtaAScad) {
        this.ws.getAdes().getAdeEtaAScad().setAdeEtaAScad(adeEtaAScad);
    }

    @Override
    public Integer getAdeEtaAScadObj() {
        if (ws.getIndAdes().getEtaAScad() >= 0) {
            return ((Integer)getAdeEtaAScad());
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeEtaAScadObj(Integer adeEtaAScadObj) {
        if (adeEtaAScadObj != null) {
            setAdeEtaAScad(((int)adeEtaAScadObj));
            ws.getIndAdes().setEtaAScad(((short)0));
        }
        else {
            ws.getIndAdes().setEtaAScad(((short)-1));
        }
    }

    @Override
    public char getAdeFlAttiv() {
        return ws.getAdes().getAdeFlAttiv();
    }

    @Override
    public void setAdeFlAttiv(char adeFlAttiv) {
        this.ws.getAdes().setAdeFlAttiv(adeFlAttiv);
    }

    @Override
    public Character getAdeFlAttivObj() {
        if (ws.getIndAdes().getFlAttiv() >= 0) {
            return ((Character)getAdeFlAttiv());
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeFlAttivObj(Character adeFlAttivObj) {
        if (adeFlAttivObj != null) {
            setAdeFlAttiv(((char)adeFlAttivObj));
            ws.getIndAdes().setFlAttiv(((short)0));
        }
        else {
            ws.getIndAdes().setFlAttiv(((short)-1));
        }
    }

    @Override
    public char getAdeFlCoincAssto() {
        return ws.getAdes().getAdeFlCoincAssto();
    }

    @Override
    public void setAdeFlCoincAssto(char adeFlCoincAssto) {
        this.ws.getAdes().setAdeFlCoincAssto(adeFlCoincAssto);
    }

    @Override
    public Character getAdeFlCoincAsstoObj() {
        if (ws.getIndAdes().getFlCoincAssto() >= 0) {
            return ((Character)getAdeFlCoincAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeFlCoincAsstoObj(Character adeFlCoincAsstoObj) {
        if (adeFlCoincAsstoObj != null) {
            setAdeFlCoincAssto(((char)adeFlCoincAsstoObj));
            ws.getIndAdes().setFlCoincAssto(((short)0));
        }
        else {
            ws.getIndAdes().setFlCoincAssto(((short)-1));
        }
    }

    @Override
    public char getAdeFlProvzaMigraz() {
        return ws.getAdes().getAdeFlProvzaMigraz();
    }

    @Override
    public void setAdeFlProvzaMigraz(char adeFlProvzaMigraz) {
        this.ws.getAdes().setAdeFlProvzaMigraz(adeFlProvzaMigraz);
    }

    @Override
    public Character getAdeFlProvzaMigrazObj() {
        if (ws.getIndAdes().getFlProvzaMigraz() >= 0) {
            return ((Character)getAdeFlProvzaMigraz());
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeFlProvzaMigrazObj(Character adeFlProvzaMigrazObj) {
        if (adeFlProvzaMigrazObj != null) {
            setAdeFlProvzaMigraz(((char)adeFlProvzaMigrazObj));
            ws.getIndAdes().setFlProvzaMigraz(((short)0));
        }
        else {
            ws.getIndAdes().setFlProvzaMigraz(((short)-1));
        }
    }

    @Override
    public char getAdeFlVarzStatTbgc() {
        return ws.getAdes().getAdeFlVarzStatTbgc();
    }

    @Override
    public void setAdeFlVarzStatTbgc(char adeFlVarzStatTbgc) {
        this.ws.getAdes().setAdeFlVarzStatTbgc(adeFlVarzStatTbgc);
    }

    @Override
    public Character getAdeFlVarzStatTbgcObj() {
        if (ws.getIndAdes().getFlVarzStatTbgc() >= 0) {
            return ((Character)getAdeFlVarzStatTbgc());
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeFlVarzStatTbgcObj(Character adeFlVarzStatTbgcObj) {
        if (adeFlVarzStatTbgcObj != null) {
            setAdeFlVarzStatTbgc(((char)adeFlVarzStatTbgcObj));
            ws.getIndAdes().setFlVarzStatTbgc(((short)0));
        }
        else {
            ws.getIndAdes().setFlVarzStatTbgc(((short)-1));
        }
    }

    @Override
    public String getAdeIbDflt() {
        return ws.getAdes().getAdeIbDflt();
    }

    @Override
    public void setAdeIbDflt(String adeIbDflt) {
        this.ws.getAdes().setAdeIbDflt(adeIbDflt);
    }

    @Override
    public String getAdeIbDfltObj() {
        if (ws.getIndAdes().getIbDflt() >= 0) {
            return getAdeIbDflt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeIbDfltObj(String adeIbDfltObj) {
        if (adeIbDfltObj != null) {
            setAdeIbDflt(adeIbDfltObj);
            ws.getIndAdes().setIbDflt(((short)0));
        }
        else {
            ws.getIndAdes().setIbDflt(((short)-1));
        }
    }

    @Override
    public String getAdeIbOgg() {
        return ws.getAdes().getAdeIbOgg();
    }

    @Override
    public void setAdeIbOgg(String adeIbOgg) {
        this.ws.getAdes().setAdeIbOgg(adeIbOgg);
    }

    @Override
    public String getAdeIbOggObj() {
        if (ws.getIndAdes().getIbOgg() >= 0) {
            return getAdeIbOgg();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeIbOggObj(String adeIbOggObj) {
        if (adeIbOggObj != null) {
            setAdeIbOgg(adeIbOggObj);
            ws.getIndAdes().setIbOgg(((short)0));
        }
        else {
            ws.getIndAdes().setIbOgg(((short)-1));
        }
    }

    @Override
    public String getAdeIbPrev() {
        return ws.getAdes().getAdeIbPrev();
    }

    @Override
    public void setAdeIbPrev(String adeIbPrev) {
        this.ws.getAdes().setAdeIbPrev(adeIbPrev);
    }

    @Override
    public String getAdeIbPrevObj() {
        if (ws.getIndAdes().getIbPrev() >= 0) {
            return getAdeIbPrev();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeIbPrevObj(String adeIbPrevObj) {
        if (adeIbPrevObj != null) {
            setAdeIbPrev(adeIbPrevObj);
            ws.getIndAdes().setIbPrev(((short)0));
        }
        else {
            ws.getIndAdes().setIbPrev(((short)-1));
        }
    }

    @Override
    public int getAdeIdAdes() {
        return ws.getAdes().getAdeIdAdes();
    }

    @Override
    public void setAdeIdAdes(int adeIdAdes) {
        this.ws.getAdes().setAdeIdAdes(adeIdAdes);
    }

    @Override
    public int getAdeIdMoviChiu() {
        return ws.getAdes().getAdeIdMoviChiu().getAdeIdMoviChiu();
    }

    @Override
    public void setAdeIdMoviChiu(int adeIdMoviChiu) {
        this.ws.getAdes().getAdeIdMoviChiu().setAdeIdMoviChiu(adeIdMoviChiu);
    }

    @Override
    public Integer getAdeIdMoviChiuObj() {
        if (ws.getIndAdes().getIdMoviChiu() >= 0) {
            return ((Integer)getAdeIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeIdMoviChiuObj(Integer adeIdMoviChiuObj) {
        if (adeIdMoviChiuObj != null) {
            setAdeIdMoviChiu(((int)adeIdMoviChiuObj));
            ws.getIndAdes().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndAdes().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getAdeIdMoviCrz() {
        return ws.getAdes().getAdeIdMoviCrz();
    }

    @Override
    public void setAdeIdMoviCrz(int adeIdMoviCrz) {
        this.ws.getAdes().setAdeIdMoviCrz(adeIdMoviCrz);
    }

    @Override
    public int getAdeIdPoli() {
        return ws.getAdes().getAdeIdPoli();
    }

    @Override
    public void setAdeIdPoli(int adeIdPoli) {
        this.ws.getAdes().setAdeIdPoli(adeIdPoli);
    }

    @Override
    public String getAdeIdenIscFnd() {
        return ws.getAdes().getAdeIdenIscFnd();
    }

    @Override
    public void setAdeIdenIscFnd(String adeIdenIscFnd) {
        this.ws.getAdes().setAdeIdenIscFnd(adeIdenIscFnd);
    }

    @Override
    public String getAdeIdenIscFndObj() {
        if (ws.getIndAdes().getIdenIscFnd() >= 0) {
            return getAdeIdenIscFnd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeIdenIscFndObj(String adeIdenIscFndObj) {
        if (adeIdenIscFndObj != null) {
            setAdeIdenIscFnd(adeIdenIscFndObj);
            ws.getIndAdes().setIdenIscFnd(((short)0));
        }
        else {
            ws.getIndAdes().setIdenIscFnd(((short)-1));
        }
    }

    @Override
    public AfDecimal getAdeImpAder() {
        return ws.getAdes().getAdeImpAder().getAdeImpAder();
    }

    @Override
    public void setAdeImpAder(AfDecimal adeImpAder) {
        this.ws.getAdes().getAdeImpAder().setAdeImpAder(adeImpAder.copy());
    }

    @Override
    public AfDecimal getAdeImpAderObj() {
        if (ws.getIndAdes().getImpAder() >= 0) {
            return getAdeImpAder();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeImpAderObj(AfDecimal adeImpAderObj) {
        if (adeImpAderObj != null) {
            setAdeImpAder(new AfDecimal(adeImpAderObj, 15, 3));
            ws.getIndAdes().setImpAder(((short)0));
        }
        else {
            ws.getIndAdes().setImpAder(((short)-1));
        }
    }

    @Override
    public AfDecimal getAdeImpAz() {
        return ws.getAdes().getAdeImpAz().getAdeImpAz();
    }

    @Override
    public void setAdeImpAz(AfDecimal adeImpAz) {
        this.ws.getAdes().getAdeImpAz().setAdeImpAz(adeImpAz.copy());
    }

    @Override
    public AfDecimal getAdeImpAzObj() {
        if (ws.getIndAdes().getImpAz() >= 0) {
            return getAdeImpAz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeImpAzObj(AfDecimal adeImpAzObj) {
        if (adeImpAzObj != null) {
            setAdeImpAz(new AfDecimal(adeImpAzObj, 15, 3));
            ws.getIndAdes().setImpAz(((short)0));
        }
        else {
            ws.getIndAdes().setImpAz(((short)-1));
        }
    }

    @Override
    public AfDecimal getAdeImpGarCnbt() {
        return ws.getAdes().getAdeImpGarCnbt().getAdeImpGarCnbt();
    }

    @Override
    public void setAdeImpGarCnbt(AfDecimal adeImpGarCnbt) {
        this.ws.getAdes().getAdeImpGarCnbt().setAdeImpGarCnbt(adeImpGarCnbt.copy());
    }

    @Override
    public AfDecimal getAdeImpGarCnbtObj() {
        if (ws.getIndAdes().getImpGarCnbt() >= 0) {
            return getAdeImpGarCnbt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeImpGarCnbtObj(AfDecimal adeImpGarCnbtObj) {
        if (adeImpGarCnbtObj != null) {
            setAdeImpGarCnbt(new AfDecimal(adeImpGarCnbtObj, 15, 3));
            ws.getIndAdes().setImpGarCnbt(((short)0));
        }
        else {
            ws.getIndAdes().setImpGarCnbt(((short)-1));
        }
    }

    @Override
    public AfDecimal getAdeImpRecRitAcc() {
        return ws.getAdes().getAdeImpRecRitAcc().getAdeImpRecRitAcc();
    }

    @Override
    public void setAdeImpRecRitAcc(AfDecimal adeImpRecRitAcc) {
        this.ws.getAdes().getAdeImpRecRitAcc().setAdeImpRecRitAcc(adeImpRecRitAcc.copy());
    }

    @Override
    public AfDecimal getAdeImpRecRitAccObj() {
        if (ws.getIndAdes().getImpRecRitAcc() >= 0) {
            return getAdeImpRecRitAcc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeImpRecRitAccObj(AfDecimal adeImpRecRitAccObj) {
        if (adeImpRecRitAccObj != null) {
            setAdeImpRecRitAcc(new AfDecimal(adeImpRecRitAccObj, 15, 3));
            ws.getIndAdes().setImpRecRitAcc(((short)0));
        }
        else {
            ws.getIndAdes().setImpRecRitAcc(((short)-1));
        }
    }

    @Override
    public AfDecimal getAdeImpRecRitVis() {
        return ws.getAdes().getAdeImpRecRitVis().getAdeImpRecRitVis();
    }

    @Override
    public void setAdeImpRecRitVis(AfDecimal adeImpRecRitVis) {
        this.ws.getAdes().getAdeImpRecRitVis().setAdeImpRecRitVis(adeImpRecRitVis.copy());
    }

    @Override
    public AfDecimal getAdeImpRecRitVisObj() {
        if (ws.getIndAdes().getImpRecRitVis() >= 0) {
            return getAdeImpRecRitVis();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeImpRecRitVisObj(AfDecimal adeImpRecRitVisObj) {
        if (adeImpRecRitVisObj != null) {
            setAdeImpRecRitVis(new AfDecimal(adeImpRecRitVisObj, 15, 3));
            ws.getIndAdes().setImpRecRitVis(((short)0));
        }
        else {
            ws.getIndAdes().setImpRecRitVis(((short)-1));
        }
    }

    @Override
    public AfDecimal getAdeImpTfr() {
        return ws.getAdes().getAdeImpTfr().getAdeImpTfr();
    }

    @Override
    public void setAdeImpTfr(AfDecimal adeImpTfr) {
        this.ws.getAdes().getAdeImpTfr().setAdeImpTfr(adeImpTfr.copy());
    }

    @Override
    public AfDecimal getAdeImpTfrObj() {
        if (ws.getIndAdes().getImpTfr() >= 0) {
            return getAdeImpTfr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeImpTfrObj(AfDecimal adeImpTfrObj) {
        if (adeImpTfrObj != null) {
            setAdeImpTfr(new AfDecimal(adeImpTfrObj, 15, 3));
            ws.getIndAdes().setImpTfr(((short)0));
        }
        else {
            ws.getIndAdes().setImpTfr(((short)-1));
        }
    }

    @Override
    public AfDecimal getAdeImpVolo() {
        return ws.getAdes().getAdeImpVolo().getAdeImpVolo();
    }

    @Override
    public void setAdeImpVolo(AfDecimal adeImpVolo) {
        this.ws.getAdes().getAdeImpVolo().setAdeImpVolo(adeImpVolo.copy());
    }

    @Override
    public AfDecimal getAdeImpVoloObj() {
        if (ws.getIndAdes().getImpVolo() >= 0) {
            return getAdeImpVolo();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeImpVoloObj(AfDecimal adeImpVoloObj) {
        if (adeImpVoloObj != null) {
            setAdeImpVolo(new AfDecimal(adeImpVoloObj, 15, 3));
            ws.getIndAdes().setImpVolo(((short)0));
        }
        else {
            ws.getIndAdes().setImpVolo(((short)-1));
        }
    }

    @Override
    public AfDecimal getAdeImpbVisDaRec() {
        return ws.getAdes().getAdeImpbVisDaRec().getAdeImpbVisDaRec();
    }

    @Override
    public void setAdeImpbVisDaRec(AfDecimal adeImpbVisDaRec) {
        this.ws.getAdes().getAdeImpbVisDaRec().setAdeImpbVisDaRec(adeImpbVisDaRec.copy());
    }

    @Override
    public AfDecimal getAdeImpbVisDaRecObj() {
        if (ws.getIndAdes().getImpbVisDaRec() >= 0) {
            return getAdeImpbVisDaRec();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeImpbVisDaRecObj(AfDecimal adeImpbVisDaRecObj) {
        if (adeImpbVisDaRecObj != null) {
            setAdeImpbVisDaRec(new AfDecimal(adeImpbVisDaRecObj, 15, 3));
            ws.getIndAdes().setImpbVisDaRec(((short)0));
        }
        else {
            ws.getIndAdes().setImpbVisDaRec(((short)-1));
        }
    }

    @Override
    public String getAdeModCalc() {
        return ws.getAdes().getAdeModCalc();
    }

    @Override
    public void setAdeModCalc(String adeModCalc) {
        this.ws.getAdes().setAdeModCalc(adeModCalc);
    }

    @Override
    public String getAdeModCalcObj() {
        if (ws.getIndAdes().getModCalc() >= 0) {
            return getAdeModCalc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeModCalcObj(String adeModCalcObj) {
        if (adeModCalcObj != null) {
            setAdeModCalc(adeModCalcObj);
            ws.getIndAdes().setModCalc(((short)0));
        }
        else {
            ws.getIndAdes().setModCalc(((short)-1));
        }
    }

    @Override
    public AfDecimal getAdeNumRatPian() {
        return ws.getAdes().getAdeNumRatPian().getAdeNumRatPian();
    }

    @Override
    public void setAdeNumRatPian(AfDecimal adeNumRatPian) {
        this.ws.getAdes().getAdeNumRatPian().setAdeNumRatPian(adeNumRatPian.copy());
    }

    @Override
    public AfDecimal getAdeNumRatPianObj() {
        if (ws.getIndAdes().getNumRatPian() >= 0) {
            return getAdeNumRatPian();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeNumRatPianObj(AfDecimal adeNumRatPianObj) {
        if (adeNumRatPianObj != null) {
            setAdeNumRatPian(new AfDecimal(adeNumRatPianObj, 12, 5));
            ws.getIndAdes().setNumRatPian(((short)0));
        }
        else {
            ws.getIndAdes().setNumRatPian(((short)-1));
        }
    }

    @Override
    public AfDecimal getAdePcAder() {
        return ws.getAdes().getAdePcAder().getAdePcAder();
    }

    @Override
    public void setAdePcAder(AfDecimal adePcAder) {
        this.ws.getAdes().getAdePcAder().setAdePcAder(adePcAder.copy());
    }

    @Override
    public AfDecimal getAdePcAderObj() {
        if (ws.getIndAdes().getPcAder() >= 0) {
            return getAdePcAder();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdePcAderObj(AfDecimal adePcAderObj) {
        if (adePcAderObj != null) {
            setAdePcAder(new AfDecimal(adePcAderObj, 6, 3));
            ws.getIndAdes().setPcAder(((short)0));
        }
        else {
            ws.getIndAdes().setPcAder(((short)-1));
        }
    }

    @Override
    public AfDecimal getAdePcAz() {
        return ws.getAdes().getAdePcAz().getAdePcAz();
    }

    @Override
    public void setAdePcAz(AfDecimal adePcAz) {
        this.ws.getAdes().getAdePcAz().setAdePcAz(adePcAz.copy());
    }

    @Override
    public AfDecimal getAdePcAzObj() {
        if (ws.getIndAdes().getPcAz() >= 0) {
            return getAdePcAz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdePcAzObj(AfDecimal adePcAzObj) {
        if (adePcAzObj != null) {
            setAdePcAz(new AfDecimal(adePcAzObj, 6, 3));
            ws.getIndAdes().setPcAz(((short)0));
        }
        else {
            ws.getIndAdes().setPcAz(((short)-1));
        }
    }

    @Override
    public AfDecimal getAdePcTfr() {
        return ws.getAdes().getAdePcTfr().getAdePcTfr();
    }

    @Override
    public void setAdePcTfr(AfDecimal adePcTfr) {
        this.ws.getAdes().getAdePcTfr().setAdePcTfr(adePcTfr.copy());
    }

    @Override
    public AfDecimal getAdePcTfrObj() {
        if (ws.getIndAdes().getPcTfr() >= 0) {
            return getAdePcTfr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdePcTfrObj(AfDecimal adePcTfrObj) {
        if (adePcTfrObj != null) {
            setAdePcTfr(new AfDecimal(adePcTfrObj, 6, 3));
            ws.getIndAdes().setPcTfr(((short)0));
        }
        else {
            ws.getIndAdes().setPcTfr(((short)-1));
        }
    }

    @Override
    public AfDecimal getAdePcVolo() {
        return ws.getAdes().getAdePcVolo().getAdePcVolo();
    }

    @Override
    public void setAdePcVolo(AfDecimal adePcVolo) {
        this.ws.getAdes().getAdePcVolo().setAdePcVolo(adePcVolo.copy());
    }

    @Override
    public AfDecimal getAdePcVoloObj() {
        if (ws.getIndAdes().getPcVolo() >= 0) {
            return getAdePcVolo();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdePcVoloObj(AfDecimal adePcVoloObj) {
        if (adePcVoloObj != null) {
            setAdePcVolo(new AfDecimal(adePcVoloObj, 6, 3));
            ws.getIndAdes().setPcVolo(((short)0));
        }
        else {
            ws.getIndAdes().setPcVolo(((short)-1));
        }
    }

    @Override
    public AfDecimal getAdePreLrdInd() {
        return ws.getAdes().getAdePreLrdInd().getAdePreLrdInd();
    }

    @Override
    public void setAdePreLrdInd(AfDecimal adePreLrdInd) {
        this.ws.getAdes().getAdePreLrdInd().setAdePreLrdInd(adePreLrdInd.copy());
    }

    @Override
    public AfDecimal getAdePreLrdIndObj() {
        if (ws.getIndAdes().getPreLrdInd() >= 0) {
            return getAdePreLrdInd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdePreLrdIndObj(AfDecimal adePreLrdIndObj) {
        if (adePreLrdIndObj != null) {
            setAdePreLrdInd(new AfDecimal(adePreLrdIndObj, 15, 3));
            ws.getIndAdes().setPreLrdInd(((short)0));
        }
        else {
            ws.getIndAdes().setPreLrdInd(((short)-1));
        }
    }

    @Override
    public AfDecimal getAdePreNetInd() {
        return ws.getAdes().getAdePreNetInd().getAdePreNetInd();
    }

    @Override
    public void setAdePreNetInd(AfDecimal adePreNetInd) {
        this.ws.getAdes().getAdePreNetInd().setAdePreNetInd(adePreNetInd.copy());
    }

    @Override
    public AfDecimal getAdePreNetIndObj() {
        if (ws.getIndAdes().getPreNetInd() >= 0) {
            return getAdePreNetInd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdePreNetIndObj(AfDecimal adePreNetIndObj) {
        if (adePreNetIndObj != null) {
            setAdePreNetInd(new AfDecimal(adePreNetIndObj, 15, 3));
            ws.getIndAdes().setPreNetInd(((short)0));
        }
        else {
            ws.getIndAdes().setPreNetInd(((short)-1));
        }
    }

    @Override
    public AfDecimal getAdePrstzIniInd() {
        return ws.getAdes().getAdePrstzIniInd().getAdePrstzIniInd();
    }

    @Override
    public void setAdePrstzIniInd(AfDecimal adePrstzIniInd) {
        this.ws.getAdes().getAdePrstzIniInd().setAdePrstzIniInd(adePrstzIniInd.copy());
    }

    @Override
    public AfDecimal getAdePrstzIniIndObj() {
        if (ws.getIndAdes().getPrstzIniInd() >= 0) {
            return getAdePrstzIniInd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdePrstzIniIndObj(AfDecimal adePrstzIniIndObj) {
        if (adePrstzIniIndObj != null) {
            setAdePrstzIniInd(new AfDecimal(adePrstzIniIndObj, 15, 3));
            ws.getIndAdes().setPrstzIniInd(((short)0));
        }
        else {
            ws.getIndAdes().setPrstzIniInd(((short)-1));
        }
    }

    @Override
    public AfDecimal getAdeRatLrdInd() {
        return ws.getAdes().getAdeRatLrdInd().getAdeRatLrdInd();
    }

    @Override
    public void setAdeRatLrdInd(AfDecimal adeRatLrdInd) {
        this.ws.getAdes().getAdeRatLrdInd().setAdeRatLrdInd(adeRatLrdInd.copy());
    }

    @Override
    public AfDecimal getAdeRatLrdIndObj() {
        if (ws.getIndAdes().getRatLrdInd() >= 0) {
            return getAdeRatLrdInd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeRatLrdIndObj(AfDecimal adeRatLrdIndObj) {
        if (adeRatLrdIndObj != null) {
            setAdeRatLrdInd(new AfDecimal(adeRatLrdIndObj, 15, 3));
            ws.getIndAdes().setRatLrdInd(((short)0));
        }
        else {
            ws.getIndAdes().setRatLrdInd(((short)-1));
        }
    }

    @Override
    public String getAdeTpFntCnbtva() {
        return ws.getAdes().getAdeTpFntCnbtva();
    }

    @Override
    public void setAdeTpFntCnbtva(String adeTpFntCnbtva) {
        this.ws.getAdes().setAdeTpFntCnbtva(adeTpFntCnbtva);
    }

    @Override
    public String getAdeTpFntCnbtvaObj() {
        if (ws.getIndAdes().getTpFntCnbtva() >= 0) {
            return getAdeTpFntCnbtva();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeTpFntCnbtvaObj(String adeTpFntCnbtvaObj) {
        if (adeTpFntCnbtvaObj != null) {
            setAdeTpFntCnbtva(adeTpFntCnbtvaObj);
            ws.getIndAdes().setTpFntCnbtva(((short)0));
        }
        else {
            ws.getIndAdes().setTpFntCnbtva(((short)-1));
        }
    }

    @Override
    public String getAdeTpIas() {
        return ws.getAdes().getAdeTpIas();
    }

    @Override
    public void setAdeTpIas(String adeTpIas) {
        this.ws.getAdes().setAdeTpIas(adeTpIas);
    }

    @Override
    public String getAdeTpIasObj() {
        if (ws.getIndAdes().getTpIas() >= 0) {
            return getAdeTpIas();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeTpIasObj(String adeTpIasObj) {
        if (adeTpIasObj != null) {
            setAdeTpIas(adeTpIasObj);
            ws.getIndAdes().setTpIas(((short)0));
        }
        else {
            ws.getIndAdes().setTpIas(((short)-1));
        }
    }

    @Override
    public String getAdeTpModPagTit() {
        return ws.getAdes().getAdeTpModPagTit();
    }

    @Override
    public void setAdeTpModPagTit(String adeTpModPagTit) {
        this.ws.getAdes().setAdeTpModPagTit(adeTpModPagTit);
    }

    @Override
    public String getAdeTpRgmFisc() {
        return ws.getAdes().getAdeTpRgmFisc();
    }

    @Override
    public void setAdeTpRgmFisc(String adeTpRgmFisc) {
        this.ws.getAdes().setAdeTpRgmFisc(adeTpRgmFisc);
    }

    @Override
    public String getAdeTpRiat() {
        return ws.getAdes().getAdeTpRiat();
    }

    @Override
    public void setAdeTpRiat(String adeTpRiat) {
        this.ws.getAdes().setAdeTpRiat(adeTpRiat);
    }

    @Override
    public String getAdeTpRiatObj() {
        if (ws.getIndAdes().getTpRiat() >= 0) {
            return getAdeTpRiat();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeTpRiatObj(String adeTpRiatObj) {
        if (adeTpRiatObj != null) {
            setAdeTpRiat(adeTpRiatObj);
            ws.getIndAdes().setTpRiat(((short)0));
        }
        else {
            ws.getIndAdes().setTpRiat(((short)-1));
        }
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        return idsv0003.getCodiceCompagniaAnia();
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        this.idsv0003.setCodiceCompagniaAnia(idsv0003CodiceCompagniaAnia);
    }

    @Override
    public int getLc821IdPoli() {
        return ldbvc821.getLc821IdPoli();
    }

    @Override
    public void setLc821IdPoli(int lc821IdPoli) {
        this.ldbvc821.setLc821IdPoli(lc821IdPoli);
    }

    @Override
    public String getLc821TpFrmAssva1() {
        return ldbvc821.getLc821TpFrmAssva1();
    }

    @Override
    public void setLc821TpFrmAssva1(String lc821TpFrmAssva1) {
        this.ldbvc821.setLc821TpFrmAssva1(lc821TpFrmAssva1);
    }

    @Override
    public String getLc821TpFrmAssva2() {
        return ldbvc821.getLc821TpFrmAssva2();
    }

    @Override
    public void setLc821TpFrmAssva2(String lc821TpFrmAssva2) {
        this.ldbvc821.setLc821TpFrmAssva2(lc821TpFrmAssva2);
    }

    @Override
    public int getPolAaDiffProrDflt() {
        return ws.getPoli().getPolAaDiffProrDflt().getPolAaDiffProrDflt();
    }

    @Override
    public void setPolAaDiffProrDflt(int polAaDiffProrDflt) {
        this.ws.getPoli().getPolAaDiffProrDflt().setPolAaDiffProrDflt(polAaDiffProrDflt);
    }

    @Override
    public Integer getPolAaDiffProrDfltObj() {
        if (ws.getIndPoli().getProvDaRec() >= 0) {
            return ((Integer)getPolAaDiffProrDflt());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolAaDiffProrDfltObj(Integer polAaDiffProrDfltObj) {
        if (polAaDiffProrDfltObj != null) {
            setPolAaDiffProrDflt(((int)polAaDiffProrDfltObj));
            ws.getIndPoli().setProvDaRec(((short)0));
        }
        else {
            ws.getIndPoli().setProvDaRec(((short)-1));
        }
    }

    @Override
    public int getPolCodCompAnia() {
        return ws.getPoli().getPolCodCompAnia();
    }

    @Override
    public void setPolCodCompAnia(int polCodCompAnia) {
        this.ws.getPoli().setPolCodCompAnia(polCodCompAnia);
    }

    @Override
    public String getPolCodConv() {
        return ws.getPoli().getPolCodConv();
    }

    @Override
    public String getPolCodConvAgg() {
        return ws.getPoli().getPolCodConvAgg();
    }

    @Override
    public void setPolCodConvAgg(String polCodConvAgg) {
        this.ws.getPoli().setPolCodConvAgg(polCodConvAgg);
    }

    @Override
    public String getPolCodConvAggObj() {
        if (ws.getIndPoli().getSpeAge() >= 0) {
            return getPolCodConvAgg();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolCodConvAggObj(String polCodConvAggObj) {
        if (polCodConvAggObj != null) {
            setPolCodConvAgg(polCodConvAggObj);
            ws.getIndPoli().setSpeAge(((short)0));
        }
        else {
            ws.getIndPoli().setSpeAge(((short)-1));
        }
    }

    @Override
    public void setPolCodConv(String polCodConv) {
        this.ws.getPoli().setPolCodConv(polCodConv);
    }

    @Override
    public String getPolCodConvObj() {
        if (ws.getIndPoli().getIntrRetdt() >= 0) {
            return getPolCodConv();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolCodConvObj(String polCodConvObj) {
        if (polCodConvObj != null) {
            setPolCodConv(polCodConvObj);
            ws.getIndPoli().setIntrRetdt(((short)0));
        }
        else {
            ws.getIndPoli().setIntrRetdt(((short)-1));
        }
    }

    @Override
    public String getPolCodDvs() {
        return ws.getPoli().getPolCodDvs();
    }

    @Override
    public void setPolCodDvs(String polCodDvs) {
        this.ws.getPoli().setPolCodDvs(polCodDvs);
    }

    @Override
    public String getPolCodDvsObj() {
        if (ws.getIndPoli().getCarGest() >= 0) {
            return getPolCodDvs();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolCodDvsObj(String polCodDvsObj) {
        if (polCodDvsObj != null) {
            setPolCodDvs(polCodDvsObj);
            ws.getIndPoli().setCarGest(((short)0));
        }
        else {
            ws.getIndPoli().setCarGest(((short)-1));
        }
    }

    @Override
    public String getPolCodProd() {
        return ws.getPoli().getPolCodProd();
    }

    @Override
    public void setPolCodProd(String polCodProd) {
        this.ws.getPoli().setPolCodProd(polCodProd);
    }

    @Override
    public String getPolCodRamo() {
        return ws.getPoli().getPolCodRamo();
    }

    @Override
    public void setPolCodRamo(String polCodRamo) {
        this.ws.getPoli().setPolCodRamo(polCodRamo);
    }

    @Override
    public String getPolCodRamoObj() {
        if (ws.getIndPoli().getIntrRiat() >= 0) {
            return getPolCodRamo();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolCodRamoObj(String polCodRamoObj) {
        if (polCodRamoObj != null) {
            setPolCodRamo(polCodRamoObj);
            ws.getIndPoli().setIntrRiat(((short)0));
        }
        else {
            ws.getIndPoli().setIntrRiat(((short)-1));
        }
    }

    @Override
    public String getPolCodTpa() {
        return ws.getPoli().getPolCodTpa();
    }

    @Override
    public void setPolCodTpa(String polCodTpa) {
        this.ws.getPoli().setPolCodTpa(polCodTpa);
    }

    @Override
    public String getPolCodTpaObj() {
        if (ws.getIndPoli().getImpTrasfe() >= 0) {
            return getPolCodTpa();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolCodTpaObj(String polCodTpaObj) {
        if (polCodTpaObj != null) {
            setPolCodTpa(polCodTpaObj);
            ws.getIndPoli().setImpTrasfe(((short)0));
        }
        else {
            ws.getIndPoli().setImpTrasfe(((short)-1));
        }
    }

    @Override
    public String getPolConvGeco() {
        return ws.getPoli().getPolConvGeco();
    }

    @Override
    public void setPolConvGeco(String polConvGeco) {
        this.ws.getPoli().setPolConvGeco(polConvGeco);
    }

    @Override
    public String getPolConvGecoObj() {
        if (ws.getIndPoli().getImpVolo() >= 0) {
            return getPolConvGeco();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolConvGecoObj(String polConvGecoObj) {
        if (polConvGecoObj != null) {
            setPolConvGeco(polConvGecoObj);
            ws.getIndPoli().setImpVolo(((short)0));
        }
        else {
            ws.getIndPoli().setImpVolo(((short)-1));
        }
    }

    @Override
    public AfDecimal getPolDir1oVers() {
        return ws.getPoli().getPolDir1oVers().getPolDir1oVers();
    }

    @Override
    public void setPolDir1oVers(AfDecimal polDir1oVers) {
        this.ws.getPoli().getPolDir1oVers().setPolDir1oVers(polDir1oVers.copy());
    }

    @Override
    public AfDecimal getPolDir1oVersObj() {
        if (ws.getIndPoli().getPreSoloRsh() >= 0) {
            return getPolDir1oVers();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolDir1oVersObj(AfDecimal polDir1oVersObj) {
        if (polDir1oVersObj != null) {
            setPolDir1oVers(new AfDecimal(polDir1oVersObj, 15, 3));
            ws.getIndPoli().setPreSoloRsh(((short)0));
        }
        else {
            ws.getIndPoli().setPreSoloRsh(((short)-1));
        }
    }

    @Override
    public AfDecimal getPolDirEmis() {
        return ws.getPoli().getPolDirEmis().getPolDirEmis();
    }

    @Override
    public void setPolDirEmis(AfDecimal polDirEmis) {
        this.ws.getPoli().getPolDirEmis().setPolDirEmis(polDirEmis.copy());
    }

    @Override
    public AfDecimal getPolDirEmisObj() {
        if (ws.getIndPoli().getPrePpIas() >= 0) {
            return getPolDirEmis();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolDirEmisObj(AfDecimal polDirEmisObj) {
        if (polDirEmisObj != null) {
            setPolDirEmis(new AfDecimal(polDirEmisObj, 15, 3));
            ws.getIndPoli().setPrePpIas(((short)0));
        }
        else {
            ws.getIndPoli().setPrePpIas(((short)-1));
        }
    }

    @Override
    public AfDecimal getPolDirQuiet() {
        return ws.getPoli().getPolDirQuiet().getPolDirQuiet();
    }

    @Override
    public void setPolDirQuiet(AfDecimal polDirQuiet) {
        this.ws.getPoli().getPolDirQuiet().setPolDirQuiet(polDirQuiet.copy());
    }

    @Override
    public AfDecimal getPolDirQuietObj() {
        if (ws.getIndPoli().getCodTari() >= 0) {
            return getPolDirQuiet();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolDirQuietObj(AfDecimal polDirQuietObj) {
        if (polDirQuietObj != null) {
            setPolDirQuiet(new AfDecimal(polDirQuietObj, 15, 3));
            ws.getIndPoli().setCodTari(((short)0));
        }
        else {
            ws.getIndPoli().setCodTari(((short)-1));
        }
    }

    @Override
    public AfDecimal getPolDirVersAgg() {
        return ws.getPoli().getPolDirVersAgg().getPolDirVersAgg();
    }

    @Override
    public void setPolDirVersAgg(AfDecimal polDirVersAgg) {
        this.ws.getPoli().getPolDirVersAgg().setPolDirVersAgg(polDirVersAgg.copy());
    }

    @Override
    public AfDecimal getPolDirVersAggObj() {
        if (ws.getIndPoli().getCarAcq() >= 0) {
            return getPolDirVersAgg();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolDirVersAggObj(AfDecimal polDirVersAggObj) {
        if (polDirVersAggObj != null) {
            setPolDirVersAgg(new AfDecimal(polDirVersAggObj, 15, 3));
            ws.getIndPoli().setCarAcq(((short)0));
        }
        else {
            ws.getIndPoli().setCarAcq(((short)-1));
        }
    }

    @Override
    public char getPolDsOperSql() {
        return ws.getPoli().getPolDsOperSql();
    }

    @Override
    public void setPolDsOperSql(char polDsOperSql) {
        this.ws.getPoli().setPolDsOperSql(polDsOperSql);
    }

    @Override
    public long getPolDsRiga() {
        return ws.getPoli().getPolDsRiga();
    }

    @Override
    public void setPolDsRiga(long polDsRiga) {
        this.ws.getPoli().setPolDsRiga(polDsRiga);
    }

    @Override
    public char getPolDsStatoElab() {
        return ws.getPoli().getPolDsStatoElab();
    }

    @Override
    public void setPolDsStatoElab(char polDsStatoElab) {
        this.ws.getPoli().setPolDsStatoElab(polDsStatoElab);
    }

    @Override
    public long getPolDsTsEndCptz() {
        return ws.getPoli().getPolDsTsEndCptz();
    }

    @Override
    public void setPolDsTsEndCptz(long polDsTsEndCptz) {
        this.ws.getPoli().setPolDsTsEndCptz(polDsTsEndCptz);
    }

    @Override
    public long getPolDsTsIniCptz() {
        return ws.getPoli().getPolDsTsIniCptz();
    }

    @Override
    public void setPolDsTsIniCptz(long polDsTsIniCptz) {
        this.ws.getPoli().setPolDsTsIniCptz(polDsTsIniCptz);
    }

    @Override
    public String getPolDsUtente() {
        return ws.getPoli().getPolDsUtente();
    }

    @Override
    public void setPolDsUtente(String polDsUtente) {
        this.ws.getPoli().setPolDsUtente(polDsUtente);
    }

    @Override
    public int getPolDsVer() {
        return ws.getPoli().getPolDsVer();
    }

    @Override
    public void setPolDsVer(int polDsVer) {
        this.ws.getPoli().setPolDsVer(polDsVer);
    }

    @Override
    public String getPolDtApplzConvDb() {
        return ws.getPoliDb().getUltConsCnbtDb();
    }

    @Override
    public void setPolDtApplzConvDb(String polDtApplzConvDb) {
        this.ws.getPoliDb().setUltConsCnbtDb(polDtApplzConvDb);
    }

    @Override
    public String getPolDtApplzConvDbObj() {
        if (ws.getIndPoli().getSpeMed() >= 0) {
            return getPolDtApplzConvDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolDtApplzConvDbObj(String polDtApplzConvDbObj) {
        if (polDtApplzConvDbObj != null) {
            setPolDtApplzConvDb(polDtApplzConvDbObj);
            ws.getIndPoli().setSpeMed(((short)0));
        }
        else {
            ws.getIndPoli().setSpeMed(((short)-1));
        }
    }

    @Override
    public String getPolDtDecorDb() {
        return ws.getPoliDb().getScadDb();
    }

    @Override
    public void setPolDtDecorDb(String polDtDecorDb) {
        this.ws.getPoliDb().setScadDb(polDtDecorDb);
    }

    @Override
    public String getPolDtEmisDb() {
        return ws.getPoliDb().getVarzTpIasDb();
    }

    @Override
    public void setPolDtEmisDb(String polDtEmisDb) {
        this.ws.getPoliDb().setVarzTpIasDb(polDtEmisDb);
    }

    @Override
    public String getPolDtEndEffDb() {
        return ws.getPoliDb().getDecorDb();
    }

    @Override
    public void setPolDtEndEffDb(String polDtEndEffDb) {
        this.ws.getPoliDb().setDecorDb(polDtEndEffDb);
    }

    @Override
    public String getPolDtIniEffDb() {
        return ws.getPoliDb().getEndEffDb();
    }

    @Override
    public void setPolDtIniEffDb(String polDtIniEffDb) {
        this.ws.getPoliDb().setEndEffDb(polDtIniEffDb);
    }

    @Override
    public String getPolDtIniVldtConvDb() {
        return ws.getPoliDb().getEffVarzStatTDb();
    }

    @Override
    public void setPolDtIniVldtConvDb(String polDtIniVldtConvDb) {
        this.ws.getPoliDb().setEffVarzStatTDb(polDtIniVldtConvDb);
    }

    @Override
    public String getPolDtIniVldtConvDbObj() {
        if (ws.getIndPoli().getDir() >= 0) {
            return getPolDtIniVldtConvDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolDtIniVldtConvDbObj(String polDtIniVldtConvDbObj) {
        if (polDtIniVldtConvDbObj != null) {
            setPolDtIniVldtConvDb(polDtIniVldtConvDbObj);
            ws.getIndPoli().setDir(((short)0));
        }
        else {
            ws.getIndPoli().setDir(((short)-1));
        }
    }

    @Override
    public String getPolDtIniVldtProdDb() {
        return ws.getPoliDb().getDecorPrestBanDb();
    }

    @Override
    public void setPolDtIniVldtProdDb(String polDtIniVldtProdDb) {
        this.ws.getPoliDb().setDecorPrestBanDb(polDtIniVldtProdDb);
    }

    @Override
    public String getPolDtPrescDb() {
        return ws.getPoliDb().getPrescDb();
    }

    @Override
    public void setPolDtPrescDb(String polDtPrescDb) {
        this.ws.getPoliDb().setPrescDb(polDtPrescDb);
    }

    @Override
    public String getPolDtPrescDbObj() {
        if (ws.getIndPoli().getDtEsiTit() >= 0) {
            return getPolDtPrescDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolDtPrescDbObj(String polDtPrescDbObj) {
        if (polDtPrescDbObj != null) {
            setPolDtPrescDb(polDtPrescDbObj);
            ws.getIndPoli().setDtEsiTit(((short)0));
        }
        else {
            ws.getIndPoli().setDtEsiTit(((short)-1));
        }
    }

    @Override
    public String getPolDtPropDb() {
        return ws.getPoliDb().getIniEffDb();
    }

    @Override
    public void setPolDtPropDb(String polDtPropDb) {
        this.ws.getPoliDb().setIniEffDb(polDtPropDb);
    }

    @Override
    public String getPolDtPropDbObj() {
        if (ws.getIndPoli().getDtEndCop() >= 0) {
            return getPolDtPropDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolDtPropDbObj(String polDtPropDbObj) {
        if (polDtPropDbObj != null) {
            setPolDtPropDb(polDtPropDbObj);
            ws.getIndPoli().setDtEndCop(((short)0));
        }
        else {
            ws.getIndPoli().setDtEndCop(((short)-1));
        }
    }

    @Override
    public String getPolDtScadDb() {
        return ws.getPoliDb().getNovaRgmFiscDb();
    }

    @Override
    public void setPolDtScadDb(String polDtScadDb) {
        this.ws.getPoliDb().setNovaRgmFiscDb(polDtScadDb);
    }

    @Override
    public String getPolDtScadDbObj() {
        if (ws.getIndPoli().getIntrMora() >= 0) {
            return getPolDtScadDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolDtScadDbObj(String polDtScadDbObj) {
        if (polDtScadDbObj != null) {
            setPolDtScadDb(polDtScadDbObj);
            ws.getIndPoli().setIntrMora(((short)0));
        }
        else {
            ws.getIndPoli().setIntrMora(((short)-1));
        }
    }

    @Override
    public int getPolDurAa() {
        return ws.getPoli().getPolDurAa().getPolDurAa();
    }

    @Override
    public void setPolDurAa(int polDurAa) {
        this.ws.getPoli().getPolDurAa().setPolDurAa(polDurAa);
    }

    @Override
    public Integer getPolDurAaObj() {
        if (ws.getIndPoli().getPreNet() >= 0) {
            return ((Integer)getPolDurAa());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolDurAaObj(Integer polDurAaObj) {
        if (polDurAaObj != null) {
            setPolDurAa(((int)polDurAaObj));
            ws.getIndPoli().setPreNet(((short)0));
        }
        else {
            ws.getIndPoli().setPreNet(((short)-1));
        }
    }

    @Override
    public int getPolDurGg() {
        return ws.getPoli().getPolDurGg().getPolDurGg();
    }

    @Override
    public void setPolDurGg(int polDurGg) {
        this.ws.getPoli().getPolDurGg().setPolDurGg(polDurGg);
    }

    @Override
    public Integer getPolDurGgObj() {
        if (ws.getIndPoli().getFrqMovi() >= 0) {
            return ((Integer)getPolDurGg());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolDurGgObj(Integer polDurGgObj) {
        if (polDurGgObj != null) {
            setPolDurGg(((int)polDurGgObj));
            ws.getIndPoli().setFrqMovi(((short)0));
        }
        else {
            ws.getIndPoli().setFrqMovi(((short)-1));
        }
    }

    @Override
    public int getPolDurMm() {
        return ws.getPoli().getPolDurMm().getPolDurMm();
    }

    @Override
    public void setPolDurMm(int polDurMm) {
        this.ws.getPoli().getPolDurMm().setPolDurMm(polDurMm);
    }

    @Override
    public Integer getPolDurMmObj() {
        if (ws.getIndPoli().getIntrFraz() >= 0) {
            return ((Integer)getPolDurMm());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolDurMmObj(Integer polDurMmObj) {
        if (polDurMmObj != null) {
            setPolDurMm(((int)polDurMmObj));
            ws.getIndPoli().setIntrFraz(((short)0));
        }
        else {
            ws.getIndPoli().setIntrFraz(((short)-1));
        }
    }

    @Override
    public char getPolFlAmmbMovi() {
        return ws.getPoli().getPolFlAmmbMovi();
    }

    @Override
    public void setPolFlAmmbMovi(char polFlAmmbMovi) {
        this.ws.getPoli().setPolFlAmmbMovi(polFlAmmbMovi);
    }

    @Override
    public Character getPolFlAmmbMoviObj() {
        if (ws.getIndPoli().getImpTfr() >= 0) {
            return ((Character)getPolFlAmmbMovi());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlAmmbMoviObj(Character polFlAmmbMoviObj) {
        if (polFlAmmbMoviObj != null) {
            setPolFlAmmbMovi(((char)polFlAmmbMoviObj));
            ws.getIndPoli().setImpTfr(((short)0));
        }
        else {
            ws.getIndPoli().setImpTfr(((short)-1));
        }
    }

    @Override
    public char getPolFlCopFinanz() {
        return ws.getPoli().getPolFlCopFinanz();
    }

    @Override
    public void setPolFlCopFinanz(char polFlCopFinanz) {
        this.ws.getPoli().setPolFlCopFinanz(polFlCopFinanz);
    }

    @Override
    public Character getPolFlCopFinanzObj() {
        if (ws.getIndPoli().getSoprProf() >= 0) {
            return ((Character)getPolFlCopFinanz());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlCopFinanzObj(Character polFlCopFinanzObj) {
        if (polFlCopFinanzObj != null) {
            setPolFlCopFinanz(((char)polFlCopFinanzObj));
            ws.getIndPoli().setSoprProf(((short)0));
        }
        else {
            ws.getIndPoli().setSoprProf(((short)-1));
        }
    }

    @Override
    public char getPolFlCumPreCntr() {
        return ws.getPoli().getPolFlCumPreCntr();
    }

    @Override
    public void setPolFlCumPreCntr(char polFlCumPreCntr) {
        this.ws.getPoli().setPolFlCumPreCntr(polFlCumPreCntr);
    }

    @Override
    public Character getPolFlCumPreCntrObj() {
        if (ws.getIndPoli().getImpAder() >= 0) {
            return ((Character)getPolFlCumPreCntr());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlCumPreCntrObj(Character polFlCumPreCntrObj) {
        if (polFlCumPreCntrObj != null) {
            setPolFlCumPreCntr(((char)polFlCumPreCntrObj));
            ws.getIndPoli().setImpAder(((short)0));
        }
        else {
            ws.getIndPoli().setImpAder(((short)-1));
        }
    }

    @Override
    public char getPolFlEstas() {
        return ws.getPoli().getPolFlEstas();
    }

    @Override
    public void setPolFlEstas(char polFlEstas) {
        this.ws.getPoli().setPolFlEstas(polFlEstas);
    }

    @Override
    public Character getPolFlEstasObj() {
        if (ws.getIndPoli().getSoprSan() >= 0) {
            return ((Character)getPolFlEstas());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlEstasObj(Character polFlEstasObj) {
        if (polFlEstasObj != null) {
            setPolFlEstas(((char)polFlEstasObj));
            ws.getIndPoli().setSoprSan(((short)0));
        }
        else {
            ws.getIndPoli().setSoprSan(((short)-1));
        }
    }

    @Override
    public char getPolFlFntAder() {
        return ws.getPoli().getPolFlFntAder();
    }

    @Override
    public void setPolFlFntAder(char polFlFntAder) {
        this.ws.getPoli().setPolFlFntAder(polFlFntAder);
    }

    @Override
    public Character getPolFlFntAderObj() {
        if (ws.getIndPoli().getProvAcq1aa() >= 0) {
            return ((Character)getPolFlFntAder());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlFntAderObj(Character polFlFntAderObj) {
        if (polFlFntAderObj != null) {
            setPolFlFntAder(((char)polFlFntAderObj));
            ws.getIndPoli().setProvAcq1aa(((short)0));
        }
        else {
            ws.getIndPoli().setProvAcq1aa(((short)-1));
        }
    }

    @Override
    public char getPolFlFntAz() {
        return ws.getPoli().getPolFlFntAz();
    }

    @Override
    public void setPolFlFntAz(char polFlFntAz) {
        this.ws.getPoli().setPolFlFntAz(polFlFntAz);
    }

    @Override
    public Character getPolFlFntAzObj() {
        if (ws.getIndPoli().getCarInc() >= 0) {
            return ((Character)getPolFlFntAz());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlFntAzObj(Character polFlFntAzObj) {
        if (polFlFntAzObj != null) {
            setPolFlFntAz(((char)polFlFntAzObj));
            ws.getIndPoli().setCarInc(((short)0));
        }
        else {
            ws.getIndPoli().setCarInc(((short)-1));
        }
    }

    @Override
    public char getPolFlFntTfr() {
        return ws.getPoli().getPolFlFntTfr();
    }

    @Override
    public void setPolFlFntTfr(char polFlFntTfr) {
        this.ws.getPoli().setPolFlFntTfr(polFlFntTfr);
    }

    @Override
    public Character getPolFlFntTfrObj() {
        if (ws.getIndPoli().getProvAcq2aa() >= 0) {
            return ((Character)getPolFlFntTfr());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlFntTfrObj(Character polFlFntTfrObj) {
        if (polFlFntTfrObj != null) {
            setPolFlFntTfr(((char)polFlFntTfrObj));
            ws.getIndPoli().setProvAcq2aa(((short)0));
        }
        else {
            ws.getIndPoli().setProvAcq2aa(((short)-1));
        }
    }

    @Override
    public char getPolFlFntVolo() {
        return ws.getPoli().getPolFlFntVolo();
    }

    @Override
    public void setPolFlFntVolo(char polFlFntVolo) {
        this.ws.getPoli().setPolFlFntVolo(polFlFntVolo);
    }

    @Override
    public Character getPolFlFntVoloObj() {
        if (ws.getIndPoli().getProvRicor() >= 0) {
            return ((Character)getPolFlFntVolo());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlFntVoloObj(Character polFlFntVoloObj) {
        if (polFlFntVoloObj != null) {
            setPolFlFntVolo(((char)polFlFntVoloObj));
            ws.getIndPoli().setProvRicor(((short)0));
        }
        else {
            ws.getIndPoli().setProvRicor(((short)-1));
        }
    }

    @Override
    public char getPolFlPoliBundling() {
        return ws.getPoli().getPolFlPoliBundling();
    }

    @Override
    public void setPolFlPoliBundling(char polFlPoliBundling) {
        this.ws.getPoli().setPolFlPoliBundling(polFlPoliBundling);
    }

    @Override
    public Character getPolFlPoliBundlingObj() {
        if (ws.getIndPoli().getNumGgRival() >= 0) {
            return ((Character)getPolFlPoliBundling());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlPoliBundlingObj(Character polFlPoliBundlingObj) {
        if (polFlPoliBundlingObj != null) {
            setPolFlPoliBundling(((char)polFlPoliBundlingObj));
            ws.getIndPoli().setNumGgRival(((short)0));
        }
        else {
            ws.getIndPoli().setNumGgRival(((short)-1));
        }
    }

    @Override
    public char getPolFlPoliCpiPr() {
        return ws.getPoli().getPolFlPoliCpiPr();
    }

    @Override
    public void setPolFlPoliCpiPr(char polFlPoliCpiPr) {
        this.ws.getPoli().setPolFlPoliCpiPr(polFlPoliCpiPr);
    }

    @Override
    public Character getPolFlPoliCpiPrObj() {
        if (ws.getIndPoli().getNumGgRitardoPag() >= 0) {
            return ((Character)getPolFlPoliCpiPr());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlPoliCpiPrObj(Character polFlPoliCpiPrObj) {
        if (polFlPoliCpiPrObj != null) {
            setPolFlPoliCpiPr(((char)polFlPoliCpiPrObj));
            ws.getIndPoli().setNumGgRitardoPag(((short)0));
        }
        else {
            ws.getIndPoli().setNumGgRitardoPag(((short)-1));
        }
    }

    @Override
    public char getPolFlPoliIfp() {
        return ws.getPoli().getPolFlPoliIfp();
    }

    @Override
    public void setPolFlPoliIfp(char polFlPoliIfp) {
        this.ws.getPoli().setPolFlPoliIfp(polFlPoliIfp);
    }

    @Override
    public Character getPolFlPoliIfpObj() {
        if (ws.getIndPoli().getCnbtAntirac() >= 0) {
            return ((Character)getPolFlPoliIfp());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlPoliIfpObj(Character polFlPoliIfpObj) {
        if (polFlPoliIfpObj != null) {
            setPolFlPoliIfp(((char)polFlPoliIfpObj));
            ws.getIndPoli().setCnbtAntirac(((short)0));
        }
        else {
            ws.getIndPoli().setCnbtAntirac(((short)-1));
        }
    }

    @Override
    public char getPolFlQuestAdegzAss() {
        return ws.getPoli().getPolFlQuestAdegzAss();
    }

    @Override
    public void setPolFlQuestAdegzAss(char polFlQuestAdegzAss) {
        this.ws.getPoli().setPolFlQuestAdegzAss(polFlQuestAdegzAss);
    }

    @Override
    public Character getPolFlQuestAdegzAssObj() {
        if (ws.getIndPoli().getTotIntrPrest() >= 0) {
            return ((Character)getPolFlQuestAdegzAss());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlQuestAdegzAssObj(Character polFlQuestAdegzAssObj) {
        if (polFlQuestAdegzAssObj != null) {
            setPolFlQuestAdegzAss(((char)polFlQuestAdegzAssObj));
            ws.getIndPoli().setTotIntrPrest(((short)0));
        }
        else {
            ws.getIndPoli().setTotIntrPrest(((short)-1));
        }
    }

    @Override
    public char getPolFlRshComun() {
        return ws.getPoli().getPolFlRshComun();
    }

    @Override
    public void setPolFlRshComun(char polFlRshComun) {
        this.ws.getPoli().setPolFlRshComun(polFlRshComun);
    }

    @Override
    public char getPolFlRshComunCond() {
        return ws.getPoli().getPolFlRshComunCond();
    }

    @Override
    public void setPolFlRshComunCond(char polFlRshComunCond) {
        this.ws.getPoli().setPolFlRshComunCond(polFlRshComunCond);
    }

    @Override
    public Character getPolFlRshComunCondObj() {
        if (ws.getIndPoli().getSoprTec() >= 0) {
            return ((Character)getPolFlRshComunCond());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlRshComunCondObj(Character polFlRshComunCondObj) {
        if (polFlRshComunCondObj != null) {
            setPolFlRshComunCond(((char)polFlRshComunCondObj));
            ws.getIndPoli().setSoprTec(((short)0));
        }
        else {
            ws.getIndPoli().setSoprTec(((short)-1));
        }
    }

    @Override
    public Character getPolFlRshComunObj() {
        if (ws.getIndPoli().getSoprSpo() >= 0) {
            return ((Character)getPolFlRshComun());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlRshComunObj(Character polFlRshComunObj) {
        if (polFlRshComunObj != null) {
            setPolFlRshComun(((char)polFlRshComunObj));
            ws.getIndPoli().setSoprSpo(((short)0));
        }
        else {
            ws.getIndPoli().setSoprSpo(((short)-1));
        }
    }

    @Override
    public char getPolFlScudoFisc() {
        return ws.getPoli().getPolFlScudoFisc();
    }

    @Override
    public void setPolFlScudoFisc(char polFlScudoFisc) {
        this.ws.getPoli().setPolFlScudoFisc(polFlScudoFisc);
    }

    @Override
    public Character getPolFlScudoFiscObj() {
        if (ws.getIndPoli().getManfeeAntic() >= 0) {
            return ((Character)getPolFlScudoFisc());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlScudoFiscObj(Character polFlScudoFiscObj) {
        if (polFlScudoFiscObj != null) {
            setPolFlScudoFisc(((char)polFlScudoFiscObj));
            ws.getIndPoli().setManfeeAntic(((short)0));
        }
        else {
            ws.getIndPoli().setManfeeAntic(((short)-1));
        }
    }

    @Override
    public char getPolFlTfrStrc() {
        return ws.getPoli().getPolFlTfrStrc();
    }

    @Override
    public void setPolFlTfrStrc(char polFlTfrStrc) {
        this.ws.getPoli().setPolFlTfrStrc(polFlTfrStrc);
    }

    @Override
    public Character getPolFlTfrStrcObj() {
        if (ws.getIndPoli().getManfeeRec() >= 0) {
            return ((Character)getPolFlTfrStrc());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlTfrStrcObj(Character polFlTfrStrcObj) {
        if (polFlTfrStrcObj != null) {
            setPolFlTfrStrc(((char)polFlTfrStrcObj));
            ws.getIndPoli().setManfeeRec(((short)0));
        }
        else {
            ws.getIndPoli().setManfeeRec(((short)-1));
        }
    }

    @Override
    public char getPolFlTrasfe() {
        return ws.getPoli().getPolFlTrasfe();
    }

    @Override
    public void setPolFlTrasfe(char polFlTrasfe) {
        this.ws.getPoli().setPolFlTrasfe(polFlTrasfe);
    }

    @Override
    public Character getPolFlTrasfeObj() {
        if (ws.getIndPoli().getManfeeRicor() >= 0) {
            return ((Character)getPolFlTrasfe());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlTrasfeObj(Character polFlTrasfeObj) {
        if (polFlTrasfeObj != null) {
            setPolFlTrasfe(((char)polFlTrasfeObj));
            ws.getIndPoli().setManfeeRicor(((short)0));
        }
        else {
            ws.getIndPoli().setManfeeRicor(((short)-1));
        }
    }

    @Override
    public String getPolFlVerProd() {
        return ws.getPoli().getPolFlVerProd();
    }

    @Override
    public void setPolFlVerProd(String polFlVerProd) {
        this.ws.getPoli().setPolFlVerProd(polFlVerProd);
    }

    @Override
    public String getPolFlVerProdObj() {
        if (ws.getIndPoli().getCodDvs() >= 0) {
            return getPolFlVerProd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlVerProdObj(String polFlVerProdObj) {
        if (polFlVerProdObj != null) {
            setPolFlVerProd(polFlVerProdObj);
            ws.getIndPoli().setCodDvs(((short)0));
        }
        else {
            ws.getIndPoli().setCodDvs(((short)-1));
        }
    }

    @Override
    public char getPolFlVndBundle() {
        return ws.getPoli().getPolFlVndBundle();
    }

    @Override
    public void setPolFlVndBundle(char polFlVndBundle) {
        this.ws.getPoli().setPolFlVndBundle(polFlVndBundle);
    }

    @Override
    public Character getPolFlVndBundleObj() {
        if (ws.getIndPoli().getRemunAss() >= 0) {
            return ((Character)getPolFlVndBundle());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlVndBundleObj(Character polFlVndBundleObj) {
        if (polFlVndBundleObj != null) {
            setPolFlVndBundle(((char)polFlVndBundleObj));
            ws.getIndPoli().setRemunAss(((short)0));
        }
        else {
            ws.getIndPoli().setRemunAss(((short)-1));
        }
    }

    @Override
    public String getPolIbBs() {
        return ws.getPoli().getPolIbBs();
    }

    @Override
    public void setPolIbBs(String polIbBs) {
        this.ws.getPoli().setPolIbBs(polIbBs);
    }

    @Override
    public String getPolIbBsObj() {
        if (ws.getIndPoli().getCommisInter() >= 0) {
            return getPolIbBs();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolIbBsObj(String polIbBsObj) {
        if (polIbBsObj != null) {
            setPolIbBs(polIbBsObj);
            ws.getIndPoli().setCommisInter(((short)0));
        }
        else {
            ws.getIndPoli().setCommisInter(((short)-1));
        }
    }

    @Override
    public String getPolIbOgg() {
        return ws.getPoli().getPolIbOgg();
    }

    @Override
    public void setPolIbOgg(String polIbOgg) {
        this.ws.getPoli().setPolIbOgg(polIbOgg);
    }

    @Override
    public String getPolIbOggObj() {
        if (ws.getIndPoli().getDtIniCop() >= 0) {
            return getPolIbOgg();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolIbOggObj(String polIbOggObj) {
        if (polIbOggObj != null) {
            setPolIbOgg(polIbOggObj);
            ws.getIndPoli().setDtIniCop(((short)0));
        }
        else {
            ws.getIndPoli().setDtIniCop(((short)-1));
        }
    }

    @Override
    public String getPolIbProp() {
        return ws.getPoli().getPolIbProp();
    }

    @Override
    public void setPolIbProp(String polIbProp) {
        this.ws.getPoli().setPolIbProp(polIbProp);
    }

    @Override
    public int getPolIdAccComm() {
        return ws.getPoli().getPolIdAccComm().getPolIdAccComm();
    }

    @Override
    public void setPolIdAccComm(int polIdAccComm) {
        this.ws.getPoli().getPolIdAccComm().setPolIdAccComm(polIdAccComm);
    }

    @Override
    public Integer getPolIdAccCommObj() {
        if (ws.getIndPoli().getImpTfrStrc() >= 0) {
            return ((Integer)getPolIdAccComm());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolIdAccCommObj(Integer polIdAccCommObj) {
        if (polIdAccCommObj != null) {
            setPolIdAccComm(((int)polIdAccCommObj));
            ws.getIndPoli().setImpTfrStrc(((short)0));
        }
        else {
            ws.getIndPoli().setImpTfrStrc(((short)-1));
        }
    }

    @Override
    public int getPolIdMoviChiu() {
        return ws.getPoli().getPolIdMoviChiu().getPolIdMoviChiu();
    }

    @Override
    public void setPolIdMoviChiu(int polIdMoviChiu) {
        this.ws.getPoli().getPolIdMoviChiu().setPolIdMoviChiu(polIdMoviChiu);
    }

    @Override
    public Integer getPolIdMoviChiuObj() {
        if (ws.getIndPoli().getIdMoviChiu() >= 0) {
            return ((Integer)getPolIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolIdMoviChiuObj(Integer polIdMoviChiuObj) {
        if (polIdMoviChiuObj != null) {
            setPolIdMoviChiu(((int)polIdMoviChiuObj));
            ws.getIndPoli().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndPoli().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getPolIdMoviCrz() {
        return ws.getPoli().getPolIdMoviCrz();
    }

    @Override
    public void setPolIdMoviCrz(int polIdMoviCrz) {
        this.ws.getPoli().setPolIdMoviCrz(polIdMoviCrz);
    }

    @Override
    public int getPolIdPoli() {
        return ws.getPoli().getPolIdPoli();
    }

    @Override
    public void setPolIdPoli(int polIdPoli) {
        this.ws.getPoli().setPolIdPoli(polIdPoli);
    }

    @Override
    public char getPolIndPoliPrinColl() {
        return ws.getPoli().getPolIndPoliPrinColl();
    }

    @Override
    public void setPolIndPoliPrinColl(char polIndPoliPrinColl) {
        this.ws.getPoli().setPolIndPoliPrinColl(polIndPoliPrinColl);
    }

    @Override
    public Character getPolIndPoliPrinCollObj() {
        if (ws.getIndPoli().getAcqExp() >= 0) {
            return ((Character)getPolIndPoliPrinColl());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolIndPoliPrinCollObj(Character polIndPoliPrinCollObj) {
        if (polIndPoliPrinCollObj != null) {
            setPolIndPoliPrinColl(((char)polIndPoliPrinCollObj));
            ws.getIndPoli().setAcqExp(((short)0));
        }
        else {
            ws.getIndPoli().setAcqExp(((short)-1));
        }
    }

    @Override
    public AfDecimal getPolSpeMed() {
        return ws.getPoli().getPolSpeMed().getPolSpeMed();
    }

    @Override
    public void setPolSpeMed(AfDecimal polSpeMed) {
        this.ws.getPoli().getPolSpeMed().setPolSpeMed(polSpeMed.copy());
    }

    @Override
    public AfDecimal getPolSpeMedObj() {
        if (ws.getIndPoli().getPreTot() >= 0) {
            return getPolSpeMed();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolSpeMedObj(AfDecimal polSpeMedObj) {
        if (polSpeMedObj != null) {
            setPolSpeMed(new AfDecimal(polSpeMedObj, 15, 3));
            ws.getIndPoli().setPreTot(((short)0));
        }
        else {
            ws.getIndPoli().setPreTot(((short)-1));
        }
    }

    @Override
    public String getPolSubcatProd() {
        return ws.getPoli().getPolSubcatProd();
    }

    @Override
    public void setPolSubcatProd(String polSubcatProd) {
        this.ws.getPoli().setPolSubcatProd(polSubcatProd);
    }

    @Override
    public String getPolSubcatProdObj() {
        if (ws.getIndPoli().getCarIas() >= 0) {
            return getPolSubcatProd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolSubcatProdObj(String polSubcatProdObj) {
        if (polSubcatProdObj != null) {
            setPolSubcatProd(polSubcatProdObj);
            ws.getIndPoli().setCarIas(((short)0));
        }
        else {
            ws.getIndPoli().setCarIas(((short)-1));
        }
    }

    @Override
    public String getPolTpApplzDir() {
        return ws.getPoli().getPolTpApplzDir();
    }

    @Override
    public void setPolTpApplzDir(String polTpApplzDir) {
        this.ws.getPoli().setPolTpApplzDir(polTpApplzDir);
    }

    @Override
    public String getPolTpApplzDirObj() {
        if (ws.getIndPoli().getSoprAlt() >= 0) {
            return getPolTpApplzDir();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolTpApplzDirObj(String polTpApplzDirObj) {
        if (polTpApplzDirObj != null) {
            setPolTpApplzDir(polTpApplzDirObj);
            ws.getIndPoli().setSoprAlt(((short)0));
        }
        else {
            ws.getIndPoli().setSoprAlt(((short)-1));
        }
    }

    @Override
    public String getPolTpFrmAssva() {
        return ws.getPoli().getPolTpFrmAssva();
    }

    @Override
    public void setPolTpFrmAssva(String polTpFrmAssva) {
        this.ws.getPoli().setPolTpFrmAssva(polTpFrmAssva);
    }

    @Override
    public String getPolTpLivGenzTit() {
        return ws.getPoli().getPolTpLivGenzTit();
    }

    @Override
    public void setPolTpLivGenzTit(String polTpLivGenzTit) {
        this.ws.getPoli().setPolTpLivGenzTit(polTpLivGenzTit);
    }

    @Override
    public String getPolTpOpzAScad() {
        return ws.getPoli().getPolTpOpzAScad();
    }

    @Override
    public void setPolTpOpzAScad(String polTpOpzAScad) {
        this.ws.getPoli().setPolTpOpzAScad(polTpOpzAScad);
    }

    @Override
    public String getPolTpOpzAScadObj() {
        if (ws.getIndPoli().getProvInc() >= 0) {
            return getPolTpOpzAScad();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolTpOpzAScadObj(String polTpOpzAScadObj) {
        if (polTpOpzAScadObj != null) {
            setPolTpOpzAScad(polTpOpzAScadObj);
            ws.getIndPoli().setProvInc(((short)0));
        }
        else {
            ws.getIndPoli().setProvInc(((short)-1));
        }
    }

    @Override
    public String getPolTpPoli() {
        return ws.getPoli().getPolTpPoli();
    }

    @Override
    public void setPolTpPoli(String polTpPoli) {
        this.ws.getPoli().setPolTpPoli(polTpPoli);
    }

    @Override
    public String getPolTpPtfEstno() {
        return ws.getPoli().getPolTpPtfEstno();
    }

    @Override
    public void setPolTpPtfEstno(String polTpPtfEstno) {
        this.ws.getPoli().setPolTpPtfEstno(polTpPtfEstno);
    }

    @Override
    public String getPolTpPtfEstnoObj() {
        if (ws.getIndPoli().getImpAz() >= 0) {
            return getPolTpPtfEstno();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolTpPtfEstnoObj(String polTpPtfEstnoObj) {
        if (polTpPtfEstnoObj != null) {
            setPolTpPtfEstno(polTpPtfEstnoObj);
            ws.getIndPoli().setImpAz(((short)0));
        }
        else {
            ws.getIndPoli().setImpAz(((short)-1));
        }
    }

    @Override
    public String getPolTpRgmFisc() {
        return ws.getPoli().getPolTpRgmFisc();
    }

    @Override
    public void setPolTpRgmFisc(String polTpRgmFisc) {
        this.ws.getPoli().setPolTpRgmFisc(polTpRgmFisc);
    }

    @Override
    public String getPolTpRgmFiscObj() {
        if (ws.getIndPoli().getTax() >= 0) {
            return getPolTpRgmFisc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolTpRgmFiscObj(String polTpRgmFiscObj) {
        if (polTpRgmFiscObj != null) {
            setPolTpRgmFisc(polTpRgmFiscObj);
            ws.getIndPoli().setTax(((short)0));
        }
        else {
            ws.getIndPoli().setTax(((short)-1));
        }
    }

    @Override
    public int getStbCodCompAnia() {
        return ws.getStatOggBus().getStbCodCompAnia();
    }

    @Override
    public void setStbCodCompAnia(int stbCodCompAnia) {
        this.ws.getStatOggBus().setStbCodCompAnia(stbCodCompAnia);
    }

    @Override
    public char getStbDsOperSql() {
        return ws.getStatOggBus().getStbDsOperSql();
    }

    @Override
    public void setStbDsOperSql(char stbDsOperSql) {
        this.ws.getStatOggBus().setStbDsOperSql(stbDsOperSql);
    }

    @Override
    public long getStbDsRiga() {
        return ws.getStatOggBus().getStbDsRiga();
    }

    @Override
    public void setStbDsRiga(long stbDsRiga) {
        this.ws.getStatOggBus().setStbDsRiga(stbDsRiga);
    }

    @Override
    public char getStbDsStatoElab() {
        return ws.getStatOggBus().getStbDsStatoElab();
    }

    @Override
    public void setStbDsStatoElab(char stbDsStatoElab) {
        this.ws.getStatOggBus().setStbDsStatoElab(stbDsStatoElab);
    }

    @Override
    public long getStbDsTsEndCptz() {
        return ws.getStatOggBus().getStbDsTsEndCptz();
    }

    @Override
    public void setStbDsTsEndCptz(long stbDsTsEndCptz) {
        this.ws.getStatOggBus().setStbDsTsEndCptz(stbDsTsEndCptz);
    }

    @Override
    public long getStbDsTsIniCptz() {
        return ws.getStatOggBus().getStbDsTsIniCptz();
    }

    @Override
    public void setStbDsTsIniCptz(long stbDsTsIniCptz) {
        this.ws.getStatOggBus().setStbDsTsIniCptz(stbDsTsIniCptz);
    }

    @Override
    public String getStbDsUtente() {
        return ws.getStatOggBus().getStbDsUtente();
    }

    @Override
    public void setStbDsUtente(String stbDsUtente) {
        this.ws.getStatOggBus().setStbDsUtente(stbDsUtente);
    }

    @Override
    public int getStbDsVer() {
        return ws.getStatOggBus().getStbDsVer();
    }

    @Override
    public void setStbDsVer(int stbDsVer) {
        this.ws.getStatOggBus().setStbDsVer(stbDsVer);
    }

    @Override
    public String getStbDtEndEffDb() {
        return ws.getIdbvstb3().getStbDtEndEffDb();
    }

    @Override
    public void setStbDtEndEffDb(String stbDtEndEffDb) {
        this.ws.getIdbvstb3().setStbDtEndEffDb(stbDtEndEffDb);
    }

    @Override
    public String getStbDtIniEffDb() {
        return ws.getIdbvstb3().getStbDtIniEffDb();
    }

    @Override
    public void setStbDtIniEffDb(String stbDtIniEffDb) {
        this.ws.getIdbvstb3().setStbDtIniEffDb(stbDtIniEffDb);
    }

    @Override
    public int getStbIdMoviChiu() {
        return ws.getStatOggBus().getStbIdMoviChiu().getStbIdMoviChiu();
    }

    @Override
    public void setStbIdMoviChiu(int stbIdMoviChiu) {
        this.ws.getStatOggBus().getStbIdMoviChiu().setStbIdMoviChiu(stbIdMoviChiu);
    }

    @Override
    public Integer getStbIdMoviChiuObj() {
        if (ws.getIndStbIdMoviChiu() >= 0) {
            return ((Integer)getStbIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setStbIdMoviChiuObj(Integer stbIdMoviChiuObj) {
        if (stbIdMoviChiuObj != null) {
            setStbIdMoviChiu(((int)stbIdMoviChiuObj));
            ws.setIndStbIdMoviChiu(((short)0));
        }
        else {
            ws.setIndStbIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getStbIdMoviCrz() {
        return ws.getStatOggBus().getStbIdMoviCrz();
    }

    @Override
    public void setStbIdMoviCrz(int stbIdMoviCrz) {
        this.ws.getStatOggBus().setStbIdMoviCrz(stbIdMoviCrz);
    }

    @Override
    public int getStbIdOgg() {
        return ws.getStatOggBus().getStbIdOgg();
    }

    @Override
    public void setStbIdOgg(int stbIdOgg) {
        this.ws.getStatOggBus().setStbIdOgg(stbIdOgg);
    }

    @Override
    public int getStbIdStatOggBus() {
        return ws.getStatOggBus().getStbIdStatOggBus();
    }

    @Override
    public void setStbIdStatOggBus(int stbIdStatOggBus) {
        this.ws.getStatOggBus().setStbIdStatOggBus(stbIdStatOggBus);
    }

    @Override
    public String getStbTpCaus() {
        return ws.getStatOggBus().getStbTpCaus();
    }

    @Override
    public void setStbTpCaus(String stbTpCaus) {
        this.ws.getStatOggBus().setStbTpCaus(stbTpCaus);
    }

    @Override
    public String getStbTpOgg() {
        return ws.getStatOggBus().getStbTpOgg();
    }

    @Override
    public void setStbTpOgg(String stbTpOgg) {
        this.ws.getStatOggBus().setStbTpOgg(stbTpOgg);
    }

    @Override
    public String getStbTpStatBus() {
        return ws.getStatOggBus().getStbTpStatBus();
    }

    @Override
    public void setStbTpStatBus(String stbTpStatBus) {
        this.ws.getStatOggBus().setStbTpStatBus(stbTpStatBus);
    }

    @Override
    public String getWsDataInizioEffettoDb() {
        return ws.getIdsv0010().getWsDataInizioEffettoDb();
    }

    @Override
    public void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb) {
        this.ws.getIdsv0010().setWsDataInizioEffettoDb(wsDataInizioEffettoDb);
    }

    @Override
    public long getWsTsCompetenza() {
        return ws.getIdsv0010().getWsTsCompetenza();
    }

    @Override
    public void setWsTsCompetenza(long wsTsCompetenza) {
        this.ws.getIdsv0010().setWsTsCompetenza(wsTsCompetenza);
    }
}
