package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.jdbc.FieldNotMappedException;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.DettTitCont1Dao;
import it.accenture.jnais.commons.data.to.IDettTitCont1;
import it.accenture.jnais.copy.DettTitCont;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.copy.TitCont;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ldbs0370Data;
import it.accenture.jnais.ws.Ldbv0371;
import it.accenture.jnais.ws.redefines.DtcAcqExp;
import it.accenture.jnais.ws.redefines.DtcCarAcq;
import it.accenture.jnais.ws.redefines.DtcCarGest;
import it.accenture.jnais.ws.redefines.DtcCarIas;
import it.accenture.jnais.ws.redefines.DtcCarInc;
import it.accenture.jnais.ws.redefines.DtcCnbtAntirac;
import it.accenture.jnais.ws.redefines.DtcCommisInter;
import it.accenture.jnais.ws.redefines.DtcDir;
import it.accenture.jnais.ws.redefines.DtcDtEndCop;
import it.accenture.jnais.ws.redefines.DtcDtEsiTit;
import it.accenture.jnais.ws.redefines.DtcDtIniCop;
import it.accenture.jnais.ws.redefines.DtcFrqMovi;
import it.accenture.jnais.ws.redefines.DtcIdMoviChiu;
import it.accenture.jnais.ws.redefines.DtcImpAder;
import it.accenture.jnais.ws.redefines.DtcImpAz;
import it.accenture.jnais.ws.redefines.DtcImpTfr;
import it.accenture.jnais.ws.redefines.DtcImpTfrStrc;
import it.accenture.jnais.ws.redefines.DtcImpTrasfe;
import it.accenture.jnais.ws.redefines.DtcImpVolo;
import it.accenture.jnais.ws.redefines.DtcIntrFraz;
import it.accenture.jnais.ws.redefines.DtcIntrMora;
import it.accenture.jnais.ws.redefines.DtcIntrRetdt;
import it.accenture.jnais.ws.redefines.DtcIntrRiat;
import it.accenture.jnais.ws.redefines.DtcManfeeAntic;
import it.accenture.jnais.ws.redefines.DtcManfeeRec;
import it.accenture.jnais.ws.redefines.DtcManfeeRicor;
import it.accenture.jnais.ws.redefines.DtcNumGgRitardoPag;
import it.accenture.jnais.ws.redefines.DtcNumGgRival;
import it.accenture.jnais.ws.redefines.DtcPreNet;
import it.accenture.jnais.ws.redefines.DtcPrePpIas;
import it.accenture.jnais.ws.redefines.DtcPreSoloRsh;
import it.accenture.jnais.ws.redefines.DtcPreTot;
import it.accenture.jnais.ws.redefines.DtcProvAcq1aa;
import it.accenture.jnais.ws.redefines.DtcProvAcq2aa;
import it.accenture.jnais.ws.redefines.DtcProvDaRec;
import it.accenture.jnais.ws.redefines.DtcProvInc;
import it.accenture.jnais.ws.redefines.DtcProvRicor;
import it.accenture.jnais.ws.redefines.DtcRemunAss;
import it.accenture.jnais.ws.redefines.DtcSoprAlt;
import it.accenture.jnais.ws.redefines.DtcSoprProf;
import it.accenture.jnais.ws.redefines.DtcSoprSan;
import it.accenture.jnais.ws.redefines.DtcSoprSpo;
import it.accenture.jnais.ws.redefines.DtcSoprTec;
import it.accenture.jnais.ws.redefines.DtcSpeAge;
import it.accenture.jnais.ws.redefines.DtcSpeMed;
import it.accenture.jnais.ws.redefines.DtcTax;
import it.accenture.jnais.ws.redefines.DtcTotIntrPrest;
import it.accenture.jnais.ws.redefines.TitDtApplzMora;
import it.accenture.jnais.ws.redefines.TitDtCambioVlt;
import it.accenture.jnais.ws.redefines.TitDtCertFisc;
import it.accenture.jnais.ws.redefines.TitDtEmisTit;
import it.accenture.jnais.ws.redefines.TitDtEndCop;
import it.accenture.jnais.ws.redefines.TitDtEsiTit;
import it.accenture.jnais.ws.redefines.TitDtIniCop;
import it.accenture.jnais.ws.redefines.TitDtRichAddRid;
import it.accenture.jnais.ws.redefines.TitDtVlt;
import it.accenture.jnais.ws.redefines.TitFraz;
import it.accenture.jnais.ws.redefines.TitIdMoviChiu;
import it.accenture.jnais.ws.redefines.TitIdRappAna;
import it.accenture.jnais.ws.redefines.TitIdRappRete;
import it.accenture.jnais.ws.redefines.TitImpAder;
import it.accenture.jnais.ws.redefines.TitImpAz;
import it.accenture.jnais.ws.redefines.TitImpPag;
import it.accenture.jnais.ws.redefines.TitImpTfr;
import it.accenture.jnais.ws.redefines.TitImpTfrStrc;
import it.accenture.jnais.ws.redefines.TitImpTrasfe;
import it.accenture.jnais.ws.redefines.TitImpVolo;
import it.accenture.jnais.ws.redefines.TitNumRatAccorpate;
import it.accenture.jnais.ws.redefines.TitProgTit;
import it.accenture.jnais.ws.redefines.TitTotAcqExp;
import it.accenture.jnais.ws.redefines.TitTotCarAcq;
import it.accenture.jnais.ws.redefines.TitTotCarGest;
import it.accenture.jnais.ws.redefines.TitTotCarIas;
import it.accenture.jnais.ws.redefines.TitTotCarInc;
import it.accenture.jnais.ws.redefines.TitTotCnbtAntirac;
import it.accenture.jnais.ws.redefines.TitTotCommisInter;
import it.accenture.jnais.ws.redefines.TitTotDir;
import it.accenture.jnais.ws.redefines.TitTotIntrFraz;
import it.accenture.jnais.ws.redefines.TitTotIntrMora;
import it.accenture.jnais.ws.redefines.TitTotIntrPrest;
import it.accenture.jnais.ws.redefines.TitTotIntrRetdt;
import it.accenture.jnais.ws.redefines.TitTotIntrRiat;
import it.accenture.jnais.ws.redefines.TitTotManfeeAntic;
import it.accenture.jnais.ws.redefines.TitTotManfeeRec;
import it.accenture.jnais.ws.redefines.TitTotManfeeRicor;
import it.accenture.jnais.ws.redefines.TitTotPreNet;
import it.accenture.jnais.ws.redefines.TitTotPrePpIas;
import it.accenture.jnais.ws.redefines.TitTotPreSoloRsh;
import it.accenture.jnais.ws.redefines.TitTotPreTot;
import it.accenture.jnais.ws.redefines.TitTotProvAcq1aa;
import it.accenture.jnais.ws.redefines.TitTotProvAcq2aa;
import it.accenture.jnais.ws.redefines.TitTotProvDaRec;
import it.accenture.jnais.ws.redefines.TitTotProvInc;
import it.accenture.jnais.ws.redefines.TitTotProvRicor;
import it.accenture.jnais.ws.redefines.TitTotRemunAss;
import it.accenture.jnais.ws.redefines.TitTotSoprAlt;
import it.accenture.jnais.ws.redefines.TitTotSoprProf;
import it.accenture.jnais.ws.redefines.TitTotSoprSan;
import it.accenture.jnais.ws.redefines.TitTotSoprSpo;
import it.accenture.jnais.ws.redefines.TitTotSoprTec;
import it.accenture.jnais.ws.redefines.TitTotSpeAge;
import it.accenture.jnais.ws.redefines.TitTotSpeMed;
import it.accenture.jnais.ws.redefines.TitTotTax;
import it.accenture.jnais.ws.redefines.TitTpCausStor;

/**Original name: LDBS0370<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  16 LUG 2010.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO AD HOC PER ACCESSO RISORSE DB        *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Ldbs0370 extends Program implements IDettTitCont1 {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private DettTitCont1Dao dettTitCont1Dao = new DettTitCont1Dao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Ldbs0370Data ws = new Ldbs0370Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: LDBV0371
    private Ldbv0371 ldbv0371;

    //==== METHODS ====
    /**Original name: PROGRAM_LDBS0370_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, Ldbv0371 ldbv0371) {
        this.idsv0003 = idsv0003;
        this.ldbv0371 = ldbv0371;
        // COB_CODE: PERFORM A000-INIZIO              THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                        a200ElaboraWcEff();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                        b200ElaboraWcCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //               SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                        c200ElaboraWcNst();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Ldbs0370 getInstance() {
        return ((Ldbs0370)Programs.getInstance(Ldbs0370.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'LDBS0370'              TO   IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("LDBS0370");
        // COB_CODE: MOVE ' LDBV0371' TO   IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella(" LDBV0371");
        // COB_CODE: MOVE '00'                      TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                    TO   IDSV0003-SQLCODE
        //                                               IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                    TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                               IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-WC-EFF<br>*/
    private void a200ElaboraWcEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-WC-EFF          THRU A210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-WC-EFF          THRU A210-EX
            a210SelectWcEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
            a260OpenCursorWcEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
            a270CloseCursorWcEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
            a280FetchFirstWcEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
            a290FetchNextWcEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B200-ELABORA-WC-CPZ<br>*/
    private void b200ElaboraWcCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
            b210SelectWcCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
            b260OpenCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
            b270CloseCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
            b280FetchFirstWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
            b290FetchNextWcCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: C200-ELABORA-WC-NST<br>*/
    private void c200ElaboraWcNst() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM C210-SELECT-WC-NST          THRU C210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM C210-SELECT-WC-NST          THRU C210-EX
            c210SelectWcNst();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
            c260OpenCursorWcNst();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
            c270CloseCursorWcNst();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
            c280FetchFirstWcNst();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
            c290FetchNextWcNst();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A205-DECLARE-CURSOR-WC-EFF<br>
	 * <pre>----
	 * ----  gestione WC Effetto
	 * ----</pre>*/
    private void a205DeclareCursorWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //              DECLARE C-EFF CURSOR FOR
        //              SELECT
        //                     A.ID_DETT_TIT_CONT
        //                    ,A.ID_TIT_CONT
        //                    ,A.ID_OGG
        //                    ,A.TP_OGG
        //                    ,A.ID_MOVI_CRZ
        //                    ,A.ID_MOVI_CHIU
        //                    ,A.DT_INI_EFF
        //                    ,A.DT_END_EFF
        //                    ,A.COD_COMP_ANIA
        //                    ,A.DT_INI_COP
        //                    ,A.DT_END_COP
        //                    ,A.PRE_NET
        //                    ,A.INTR_FRAZ
        //                    ,A.INTR_MORA
        //                    ,A.INTR_RETDT
        //                    ,A.INTR_RIAT
        //                    ,A.DIR
        //                    ,A.SPE_MED
        //                    ,A.TAX
        //                    ,A.SOPR_SAN
        //                    ,A.SOPR_SPO
        //                    ,A.SOPR_TEC
        //                    ,A.SOPR_PROF
        //                    ,A.SOPR_ALT
        //                    ,A.PRE_TOT
        //                    ,A.PRE_PP_IAS
        //                    ,A.PRE_SOLO_RSH
        //                    ,A.CAR_ACQ
        //                    ,A.CAR_GEST
        //                    ,A.CAR_INC
        //                    ,A.PROV_ACQ_1AA
        //                    ,A.PROV_ACQ_2AA
        //                    ,A.PROV_RICOR
        //                    ,A.PROV_INC
        //                    ,A.PROV_DA_REC
        //                    ,A.COD_DVS
        //                    ,A.FRQ_MOVI
        //                    ,A.TP_RGM_FISC
        //                    ,A.COD_TARI
        //                    ,A.TP_STAT_TIT
        //                    ,A.IMP_AZ
        //                    ,A.IMP_ADER
        //                    ,A.IMP_TFR
        //                    ,A.IMP_VOLO
        //                    ,A.MANFEE_ANTIC
        //                    ,A.MANFEE_RICOR
        //                    ,A.MANFEE_REC
        //                    ,A.DT_ESI_TIT
        //                    ,A.SPE_AGE
        //                    ,A.CAR_IAS
        //                    ,A.TOT_INTR_PREST
        //                    ,A.DS_RIGA
        //                    ,A.DS_OPER_SQL
        //                    ,A.DS_VER
        //                    ,A.DS_TS_INI_CPTZ
        //                    ,A.DS_TS_END_CPTZ
        //                    ,A.DS_UTENTE
        //                    ,A.DS_STATO_ELAB
        //                    ,A.IMP_TRASFE
        //                    ,A.IMP_TFR_STRC
        //                    ,A.NUM_GG_RITARDO_PAG
        //                    ,A.NUM_GG_RIVAL
        //                    ,A.ACQ_EXP
        //                    ,A.REMUN_ASS
        //                    ,A.COMMIS_INTER
        //                    ,A.CNBT_ANTIRAC
        //                    ,     B.ID_TIT_CONT
        //                    ,     B.ID_OGG
        //                    ,     B.TP_OGG
        //                    ,     B.IB_RICH
        //                    ,     B.ID_MOVI_CRZ
        //                    ,     B.ID_MOVI_CHIU
        //                    ,     B.DT_INI_EFF
        //                    ,     B.DT_END_EFF
        //                    ,     B.COD_COMP_ANIA
        //                    ,     B.TP_TIT
        //                    ,     B.PROG_TIT
        //                    ,     B.TP_PRE_TIT
        //                    ,     B.TP_STAT_TIT
        //                    ,     B.DT_INI_COP
        //                    ,     B.DT_END_COP
        //                    ,     B.IMP_PAG
        //                    ,     B.FL_SOLL
        //                    ,     B.FRAZ
        //                    ,     B.DT_APPLZ_MORA
        //                    ,     B.FL_MORA
        //                    ,     B.ID_RAPP_RETE
        //                    ,     B.ID_RAPP_ANA
        //                    ,     B.COD_DVS
        //                    ,     B.DT_EMIS_TIT
        //                    ,     B.DT_ESI_TIT
        //                    ,     B.TOT_PRE_NET
        //                    ,     B.TOT_INTR_FRAZ
        //                    ,     B.TOT_INTR_MORA
        //                    ,     B.TOT_INTR_PREST
        //                    ,     B.TOT_INTR_RETDT
        //                    ,     B.TOT_INTR_RIAT
        //                    ,     B.TOT_DIR
        //                    ,     B.TOT_SPE_MED
        //                    ,     B.TOT_TAX
        //                    ,     B.TOT_SOPR_SAN
        //                    ,     B.TOT_SOPR_TEC
        //                    ,     B.TOT_SOPR_SPO
        //                    ,     B.TOT_SOPR_PROF
        //                    ,     B.TOT_SOPR_ALT
        //                    ,     B.TOT_PRE_TOT
        //                    ,     B.TOT_PRE_PP_IAS
        //                    ,     B.TOT_CAR_ACQ
        //                    ,     B.TOT_CAR_GEST
        //                    ,     B.TOT_CAR_INC
        //                    ,     B.TOT_PRE_SOLO_RSH
        //                    ,     B.TOT_PROV_ACQ_1AA
        //                    ,     B.TOT_PROV_ACQ_2AA
        //                    ,     B.TOT_PROV_RICOR
        //                    ,     B.TOT_PROV_INC
        //                    ,     B.TOT_PROV_DA_REC
        //                    ,     B.IMP_AZ
        //                    ,     B.IMP_ADER
        //                    ,     B.IMP_TFR
        //                    ,     B.IMP_VOLO
        //                    ,     B.TOT_MANFEE_ANTIC
        //                    ,     B.TOT_MANFEE_RICOR
        //                    ,     B.TOT_MANFEE_REC
        //                    ,     B.TP_MEZ_PAG_ADD
        //                    ,     B.ESTR_CNT_CORR_ADD
        //                    ,     B.DT_VLT
        //                    ,     B.FL_FORZ_DT_VLT
        //                    ,     B.DT_CAMBIO_VLT
        //                    ,     B.TOT_SPE_AGE
        //                    ,     B.TOT_CAR_IAS
        //                    ,     B.NUM_RAT_ACCORPATE
        //                    ,     B.DS_RIGA
        //                    ,     B.DS_OPER_SQL
        //                    ,     B.DS_VER
        //                    ,     B.DS_TS_INI_CPTZ
        //                    ,     B.DS_TS_END_CPTZ
        //                    ,     B.DS_UTENTE
        //                    ,     B.DS_STATO_ELAB
        //                    ,     B.FL_TIT_DA_REINVST
        //                    ,     B.DT_RICH_ADD_RID
        //                    ,     B.TP_ESI_RID
        //                    ,     B.COD_IBAN
        //                    ,     B.IMP_TRASFE
        //                    ,     B.IMP_TFR_STRC
        //                    ,     B.DT_CERT_FISC
        //                    ,     B.TP_CAUS_STOR
        //                    ,     B.TP_CAUS_DISP_STOR
        //                    ,     B.TP_TIT_MIGRAZ
        //                    ,     B.TOT_ACQ_EXP
        //                    ,     B.TOT_REMUN_ASS
        //                    ,     B.TOT_COMMIS_INTER
        //                    ,     B.TP_CAUS_RIMB
        //                    ,     B.TOT_CNBT_ANTIRAC
        //                    ,     B.FL_INC_AUTOGEN
        //              FROM DETT_TIT_CONT A,
        //                   TIT_CONT      B
        //              WHERE      A.ID_TIT_CONT   = B.ID_TIT_CONT
        //                    AND A.ID_OGG        = :LDBV0371-ID-OGG
        //                    AND A.TP_OGG        = :LDBV0371-TP-OGG
        //                    AND B.TP_PRE_TIT    = :LDBV0371-TP-PRE-TIT
        //                    AND B.TP_STAT_TIT IN (:LDBV0371-TP-STAT-TIT1 ,
        //                                          :LDBV0371-TP-STAT-TIT2)
        //                    AND A.TP_STAT_TIT IN (:LDBV0371-TP-STAT-TIT1 ,
        //                                          :LDBV0371-TP-STAT-TIT2)
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.ID_MOVI_CHIU IS NULL
        //                    AND      B.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND      B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND      B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND      B.ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A210-SELECT-WC-EFF<br>*/
    private void a210SelectWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                     A.ID_DETT_TIT_CONT
        //                    ,A.ID_TIT_CONT
        //                    ,A.ID_OGG
        //                    ,A.TP_OGG
        //                    ,A.ID_MOVI_CRZ
        //                    ,A.ID_MOVI_CHIU
        //                    ,A.DT_INI_EFF
        //                    ,A.DT_END_EFF
        //                    ,A.COD_COMP_ANIA
        //                    ,A.DT_INI_COP
        //                    ,A.DT_END_COP
        //                    ,A.PRE_NET
        //                    ,A.INTR_FRAZ
        //                    ,A.INTR_MORA
        //                    ,A.INTR_RETDT
        //                    ,A.INTR_RIAT
        //                    ,A.DIR
        //                    ,A.SPE_MED
        //                    ,A.TAX
        //                    ,A.SOPR_SAN
        //                    ,A.SOPR_SPO
        //                    ,A.SOPR_TEC
        //                    ,A.SOPR_PROF
        //                    ,A.SOPR_ALT
        //                    ,A.PRE_TOT
        //                    ,A.PRE_PP_IAS
        //                    ,A.PRE_SOLO_RSH
        //                    ,A.CAR_ACQ
        //                    ,A.CAR_GEST
        //                    ,A.CAR_INC
        //                    ,A.PROV_ACQ_1AA
        //                    ,A.PROV_ACQ_2AA
        //                    ,A.PROV_RICOR
        //                    ,A.PROV_INC
        //                    ,A.PROV_DA_REC
        //                    ,A.COD_DVS
        //                    ,A.FRQ_MOVI
        //                    ,A.TP_RGM_FISC
        //                    ,A.COD_TARI
        //                    ,A.TP_STAT_TIT
        //                    ,A.IMP_AZ
        //                    ,A.IMP_ADER
        //                    ,A.IMP_TFR
        //                    ,A.IMP_VOLO
        //                    ,A.MANFEE_ANTIC
        //                    ,A.MANFEE_RICOR
        //                    ,A.MANFEE_REC
        //                    ,A.DT_ESI_TIT
        //                    ,A.SPE_AGE
        //                    ,A.CAR_IAS
        //                    ,A.TOT_INTR_PREST
        //                    ,A.DS_RIGA
        //                    ,A.DS_OPER_SQL
        //                    ,A.DS_VER
        //                    ,A.DS_TS_INI_CPTZ
        //                    ,A.DS_TS_END_CPTZ
        //                    ,A.DS_UTENTE
        //                    ,A.DS_STATO_ELAB
        //                    ,A.IMP_TRASFE
        //                    ,A.IMP_TFR_STRC
        //                    ,A.NUM_GG_RITARDO_PAG
        //                    ,A.NUM_GG_RIVAL
        //                    ,A.ACQ_EXP
        //                    ,A.REMUN_ASS
        //                    ,A.COMMIS_INTER
        //                    ,A.CNBT_ANTIRAC
        //                    ,     B.ID_TIT_CONT
        //                    ,     B.ID_OGG
        //                    ,     B.TP_OGG
        //                    ,     B.IB_RICH
        //                    ,     B.ID_MOVI_CRZ
        //                    ,     B.ID_MOVI_CHIU
        //                    ,     B.DT_INI_EFF
        //                    ,     B.DT_END_EFF
        //                    ,     B.COD_COMP_ANIA
        //                    ,     B.TP_TIT
        //                    ,     B.PROG_TIT
        //                    ,     B.TP_PRE_TIT
        //                    ,     B.TP_STAT_TIT
        //                    ,     B.DT_INI_COP
        //                    ,     B.DT_END_COP
        //                    ,     B.IMP_PAG
        //                    ,     B.FL_SOLL
        //                    ,     B.FRAZ
        //                    ,     B.DT_APPLZ_MORA
        //                    ,     B.FL_MORA
        //                    ,     B.ID_RAPP_RETE
        //                    ,     B.ID_RAPP_ANA
        //                    ,     B.COD_DVS
        //                    ,     B.DT_EMIS_TIT
        //                    ,     B.DT_ESI_TIT
        //                    ,     B.TOT_PRE_NET
        //                    ,     B.TOT_INTR_FRAZ
        //                    ,     B.TOT_INTR_MORA
        //                    ,     B.TOT_INTR_PREST
        //                    ,     B.TOT_INTR_RETDT
        //                    ,     B.TOT_INTR_RIAT
        //                    ,     B.TOT_DIR
        //                    ,     B.TOT_SPE_MED
        //                    ,     B.TOT_TAX
        //                    ,     B.TOT_SOPR_SAN
        //                    ,     B.TOT_SOPR_TEC
        //                    ,     B.TOT_SOPR_SPO
        //                    ,     B.TOT_SOPR_PROF
        //                    ,     B.TOT_SOPR_ALT
        //                    ,     B.TOT_PRE_TOT
        //                    ,     B.TOT_PRE_PP_IAS
        //                    ,     B.TOT_CAR_ACQ
        //                    ,     B.TOT_CAR_GEST
        //                    ,     B.TOT_CAR_INC
        //                    ,     B.TOT_PRE_SOLO_RSH
        //                    ,     B.TOT_PROV_ACQ_1AA
        //                    ,     B.TOT_PROV_ACQ_2AA
        //                    ,     B.TOT_PROV_RICOR
        //                    ,     B.TOT_PROV_INC
        //                    ,     B.TOT_PROV_DA_REC
        //                    ,     B.IMP_AZ
        //                    ,     B.IMP_ADER
        //                    ,     B.IMP_TFR
        //                    ,     B.IMP_VOLO
        //                    ,     B.TOT_MANFEE_ANTIC
        //                    ,     B.TOT_MANFEE_RICOR
        //                    ,     B.TOT_MANFEE_REC
        //                    ,     B.TP_MEZ_PAG_ADD
        //                    ,     B.ESTR_CNT_CORR_ADD
        //                    ,     B.DT_VLT
        //                    ,     B.FL_FORZ_DT_VLT
        //                    ,     B.DT_CAMBIO_VLT
        //                    ,     B.TOT_SPE_AGE
        //                    ,     B.TOT_CAR_IAS
        //                    ,     B.NUM_RAT_ACCORPATE
        //                    ,     B.DS_RIGA
        //                    ,     B.DS_OPER_SQL
        //                    ,     B.DS_VER
        //                    ,     B.DS_TS_INI_CPTZ
        //                    ,     B.DS_TS_END_CPTZ
        //                    ,     B.DS_UTENTE
        //                    ,     B.DS_STATO_ELAB
        //                    ,     B.FL_TIT_DA_REINVST
        //                    ,     B.DT_RICH_ADD_RID
        //                    ,     B.TP_ESI_RID
        //                    ,     B.COD_IBAN
        //                    ,     B.IMP_TRASFE
        //                    ,     B.IMP_TFR_STRC
        //                    ,     B.DT_CERT_FISC
        //                    ,     B.TP_CAUS_STOR
        //                    ,     B.TP_CAUS_DISP_STOR
        //                    ,     B.TP_TIT_MIGRAZ
        //                    ,     B.TOT_ACQ_EXP
        //                    ,     B.TOT_REMUN_ASS
        //                    ,     B.TOT_COMMIS_INTER
        //                    ,     B.TP_CAUS_RIMB
        //                    ,     B.TOT_CNBT_ANTIRAC
        //                    ,     B.FL_INC_AUTOGEN
        //             INTO
        //                :DTC-ID-DETT-TIT-CONT
        //               ,:DTC-ID-TIT-CONT
        //               ,:DTC-ID-OGG
        //               ,:DTC-TP-OGG
        //               ,:DTC-ID-MOVI-CRZ
        //               ,:DTC-ID-MOVI-CHIU
        //                :IND-DTC-ID-MOVI-CHIU
        //               ,:DTC-DT-INI-EFF-DB
        //               ,:DTC-DT-END-EFF-DB
        //               ,:DTC-COD-COMP-ANIA
        //               ,:DTC-DT-INI-COP-DB
        //                :IND-DTC-DT-INI-COP
        //               ,:DTC-DT-END-COP-DB
        //                :IND-DTC-DT-END-COP
        //               ,:DTC-PRE-NET
        //                :IND-DTC-PRE-NET
        //               ,:DTC-INTR-FRAZ
        //                :IND-DTC-INTR-FRAZ
        //               ,:DTC-INTR-MORA
        //                :IND-DTC-INTR-MORA
        //               ,:DTC-INTR-RETDT
        //                :IND-DTC-INTR-RETDT
        //               ,:DTC-INTR-RIAT
        //                :IND-DTC-INTR-RIAT
        //               ,:DTC-DIR
        //                :IND-DTC-DIR
        //               ,:DTC-SPE-MED
        //                :IND-DTC-SPE-MED
        //               ,:DTC-TAX
        //                :IND-DTC-TAX
        //               ,:DTC-SOPR-SAN
        //                :IND-DTC-SOPR-SAN
        //               ,:DTC-SOPR-SPO
        //                :IND-DTC-SOPR-SPO
        //               ,:DTC-SOPR-TEC
        //                :IND-DTC-SOPR-TEC
        //               ,:DTC-SOPR-PROF
        //                :IND-DTC-SOPR-PROF
        //               ,:DTC-SOPR-ALT
        //                :IND-DTC-SOPR-ALT
        //               ,:DTC-PRE-TOT
        //                :IND-DTC-PRE-TOT
        //               ,:DTC-PRE-PP-IAS
        //                :IND-DTC-PRE-PP-IAS
        //               ,:DTC-PRE-SOLO-RSH
        //                :IND-DTC-PRE-SOLO-RSH
        //               ,:DTC-CAR-ACQ
        //                :IND-DTC-CAR-ACQ
        //               ,:DTC-CAR-GEST
        //                :IND-DTC-CAR-GEST
        //               ,:DTC-CAR-INC
        //                :IND-DTC-CAR-INC
        //               ,:DTC-PROV-ACQ-1AA
        //                :IND-DTC-PROV-ACQ-1AA
        //               ,:DTC-PROV-ACQ-2AA
        //                :IND-DTC-PROV-ACQ-2AA
        //               ,:DTC-PROV-RICOR
        //                :IND-DTC-PROV-RICOR
        //               ,:DTC-PROV-INC
        //                :IND-DTC-PROV-INC
        //               ,:DTC-PROV-DA-REC
        //                :IND-DTC-PROV-DA-REC
        //               ,:DTC-COD-DVS
        //                :IND-DTC-COD-DVS
        //               ,:DTC-FRQ-MOVI
        //                :IND-DTC-FRQ-MOVI
        //               ,:DTC-TP-RGM-FISC
        //               ,:DTC-COD-TARI
        //                :IND-DTC-COD-TARI
        //               ,:DTC-TP-STAT-TIT
        //               ,:DTC-IMP-AZ
        //                :IND-DTC-IMP-AZ
        //               ,:DTC-IMP-ADER
        //                :IND-DTC-IMP-ADER
        //               ,:DTC-IMP-TFR
        //                :IND-DTC-IMP-TFR
        //               ,:DTC-IMP-VOLO
        //                :IND-DTC-IMP-VOLO
        //               ,:DTC-MANFEE-ANTIC
        //                :IND-DTC-MANFEE-ANTIC
        //               ,:DTC-MANFEE-RICOR
        //                :IND-DTC-MANFEE-RICOR
        //               ,:DTC-MANFEE-REC
        //                :IND-DTC-MANFEE-REC
        //               ,:DTC-DT-ESI-TIT-DB
        //                :IND-DTC-DT-ESI-TIT
        //               ,:DTC-SPE-AGE
        //                :IND-DTC-SPE-AGE
        //               ,:DTC-CAR-IAS
        //                :IND-DTC-CAR-IAS
        //               ,:DTC-TOT-INTR-PREST
        //                :IND-DTC-TOT-INTR-PREST
        //               ,:DTC-DS-RIGA
        //               ,:DTC-DS-OPER-SQL
        //               ,:DTC-DS-VER
        //               ,:DTC-DS-TS-INI-CPTZ
        //               ,:DTC-DS-TS-END-CPTZ
        //               ,:DTC-DS-UTENTE
        //               ,:DTC-DS-STATO-ELAB
        //               ,:DTC-IMP-TRASFE
        //                :IND-DTC-IMP-TRASFE
        //               ,:DTC-IMP-TFR-STRC
        //                :IND-DTC-IMP-TFR-STRC
        //               ,:DTC-NUM-GG-RITARDO-PAG
        //                :IND-DTC-NUM-GG-RITARDO-PAG
        //               ,:DTC-NUM-GG-RIVAL
        //                :IND-DTC-NUM-GG-RIVAL
        //               ,:DTC-ACQ-EXP
        //                :IND-DTC-ACQ-EXP
        //               ,:DTC-REMUN-ASS
        //                :IND-DTC-REMUN-ASS
        //               ,:DTC-COMMIS-INTER
        //                :IND-DTC-COMMIS-INTER
        //               ,:DTC-CNBT-ANTIRAC
        //                :IND-DTC-CNBT-ANTIRAC
        //               ,:TIT-ID-TIT-CONT
        //               ,:TIT-ID-OGG
        //               ,:TIT-TP-OGG
        //               ,:TIT-IB-RICH
        //                :IND-TIT-IB-RICH
        //               ,:TIT-ID-MOVI-CRZ
        //               ,:TIT-ID-MOVI-CHIU
        //                :IND-TIT-ID-MOVI-CHIU
        //               ,:TIT-DT-INI-EFF-DB
        //               ,:TIT-DT-END-EFF-DB
        //               ,:TIT-COD-COMP-ANIA
        //               ,:TIT-TP-TIT
        //               ,:TIT-PROG-TIT
        //                :IND-TIT-PROG-TIT
        //               ,:TIT-TP-PRE-TIT
        //               ,:TIT-TP-STAT-TIT
        //               ,:TIT-DT-INI-COP-DB
        //                :IND-TIT-DT-INI-COP
        //               ,:TIT-DT-END-COP-DB
        //                :IND-TIT-DT-END-COP
        //               ,:TIT-IMP-PAG
        //                :IND-TIT-IMP-PAG
        //               ,:TIT-FL-SOLL
        //                :IND-TIT-FL-SOLL
        //               ,:TIT-FRAZ
        //                :IND-TIT-FRAZ
        //               ,:TIT-DT-APPLZ-MORA-DB
        //                :IND-TIT-DT-APPLZ-MORA
        //               ,:TIT-FL-MORA
        //                :IND-TIT-FL-MORA
        //               ,:TIT-ID-RAPP-RETE
        //                :IND-TIT-ID-RAPP-RETE
        //               ,:TIT-ID-RAPP-ANA
        //                :IND-TIT-ID-RAPP-ANA
        //               ,:TIT-COD-DVS
        //                :IND-TIT-COD-DVS
        //               ,:TIT-DT-EMIS-TIT-DB
        //                :IND-TIT-DT-EMIS-TIT
        //               ,:TIT-DT-ESI-TIT-DB
        //                :IND-TIT-DT-ESI-TIT
        //               ,:TIT-TOT-PRE-NET
        //                :IND-TIT-TOT-PRE-NET
        //               ,:TIT-TOT-INTR-FRAZ
        //                :IND-TIT-TOT-INTR-FRAZ
        //               ,:TIT-TOT-INTR-MORA
        //                :IND-TIT-TOT-INTR-MORA
        //               ,:TIT-TOT-INTR-PREST
        //                :IND-TIT-TOT-INTR-PREST
        //               ,:TIT-TOT-INTR-RETDT
        //                :IND-TIT-TOT-INTR-RETDT
        //               ,:TIT-TOT-INTR-RIAT
        //                :IND-TIT-TOT-INTR-RIAT
        //               ,:TIT-TOT-DIR
        //                :IND-TIT-TOT-DIR
        //               ,:TIT-TOT-SPE-MED
        //                :IND-TIT-TOT-SPE-MED
        //               ,:TIT-TOT-TAX
        //                :IND-TIT-TOT-TAX
        //               ,:TIT-TOT-SOPR-SAN
        //                :IND-TIT-TOT-SOPR-SAN
        //               ,:TIT-TOT-SOPR-TEC
        //                :IND-TIT-TOT-SOPR-TEC
        //               ,:TIT-TOT-SOPR-SPO
        //                :IND-TIT-TOT-SOPR-SPO
        //               ,:TIT-TOT-SOPR-PROF
        //                :IND-TIT-TOT-SOPR-PROF
        //               ,:TIT-TOT-SOPR-ALT
        //                :IND-TIT-TOT-SOPR-ALT
        //               ,:TIT-TOT-PRE-TOT
        //                :IND-TIT-TOT-PRE-TOT
        //               ,:TIT-TOT-PRE-PP-IAS
        //                :IND-TIT-TOT-PRE-PP-IAS
        //               ,:TIT-TOT-CAR-ACQ
        //                :IND-TIT-TOT-CAR-ACQ
        //               ,:TIT-TOT-CAR-GEST
        //                :IND-TIT-TOT-CAR-GEST
        //               ,:TIT-TOT-CAR-INC
        //                :IND-TIT-TOT-CAR-INC
        //               ,:TIT-TOT-PRE-SOLO-RSH
        //                :IND-TIT-TOT-PRE-SOLO-RSH
        //               ,:TIT-TOT-PROV-ACQ-1AA
        //                :IND-TIT-TOT-PROV-ACQ-1AA
        //               ,:TIT-TOT-PROV-ACQ-2AA
        //                :IND-TIT-TOT-PROV-ACQ-2AA
        //               ,:TIT-TOT-PROV-RICOR
        //                :IND-TIT-TOT-PROV-RICOR
        //               ,:TIT-TOT-PROV-INC
        //                :IND-TIT-TOT-PROV-INC
        //               ,:TIT-TOT-PROV-DA-REC
        //                :IND-TIT-TOT-PROV-DA-REC
        //               ,:TIT-IMP-AZ
        //                :IND-TIT-IMP-AZ
        //               ,:TIT-IMP-ADER
        //                :IND-TIT-IMP-ADER
        //               ,:TIT-IMP-TFR
        //                :IND-TIT-IMP-TFR
        //               ,:TIT-IMP-VOLO
        //                :IND-TIT-IMP-VOLO
        //               ,:TIT-TOT-MANFEE-ANTIC
        //                :IND-TIT-TOT-MANFEE-ANTIC
        //               ,:TIT-TOT-MANFEE-RICOR
        //                :IND-TIT-TOT-MANFEE-RICOR
        //               ,:TIT-TOT-MANFEE-REC
        //                :IND-TIT-TOT-MANFEE-REC
        //               ,:TIT-TP-MEZ-PAG-ADD
        //                :IND-TIT-TP-MEZ-PAG-ADD
        //               ,:TIT-ESTR-CNT-CORR-ADD
        //                :IND-TIT-ESTR-CNT-CORR-ADD
        //               ,:TIT-DT-VLT-DB
        //                :IND-TIT-DT-VLT
        //               ,:TIT-FL-FORZ-DT-VLT
        //                :IND-TIT-FL-FORZ-DT-VLT
        //               ,:TIT-DT-CAMBIO-VLT-DB
        //                :IND-TIT-DT-CAMBIO-VLT
        //               ,:TIT-TOT-SPE-AGE
        //                :IND-TIT-TOT-SPE-AGE
        //               ,:TIT-TOT-CAR-IAS
        //                :IND-TIT-TOT-CAR-IAS
        //               ,:TIT-NUM-RAT-ACCORPATE
        //                :IND-TIT-NUM-RAT-ACCORPATE
        //               ,:TIT-DS-RIGA
        //               ,:TIT-DS-OPER-SQL
        //               ,:TIT-DS-VER
        //               ,:TIT-DS-TS-INI-CPTZ
        //               ,:TIT-DS-TS-END-CPTZ
        //               ,:TIT-DS-UTENTE
        //               ,:TIT-DS-STATO-ELAB
        //               ,:TIT-FL-TIT-DA-REINVST
        //                :IND-TIT-FL-TIT-DA-REINVST
        //               ,:TIT-DT-RICH-ADD-RID-DB
        //                :IND-TIT-DT-RICH-ADD-RID
        //               ,:TIT-TP-ESI-RID
        //                :IND-TIT-TP-ESI-RID
        //               ,:TIT-COD-IBAN
        //                :IND-TIT-COD-IBAN
        //               ,:TIT-IMP-TRASFE
        //                :IND-TIT-IMP-TRASFE
        //               ,:TIT-IMP-TFR-STRC
        //                :IND-TIT-IMP-TFR-STRC
        //               ,:TIT-DT-CERT-FISC-DB
        //                :IND-TIT-DT-CERT-FISC
        //               ,:TIT-TP-CAUS-STOR
        //                :IND-TIT-TP-CAUS-STOR
        //               ,:TIT-TP-CAUS-DISP-STOR
        //                :IND-TIT-TP-CAUS-DISP-STOR
        //               ,:TIT-TP-TIT-MIGRAZ
        //                :IND-TIT-TP-TIT-MIGRAZ
        //               ,:TIT-TOT-ACQ-EXP
        //                :IND-TIT-TOT-ACQ-EXP
        //               ,:TIT-TOT-REMUN-ASS
        //                :IND-TIT-TOT-REMUN-ASS
        //               ,:TIT-TOT-COMMIS-INTER
        //                :IND-TIT-TOT-COMMIS-INTER
        //               ,:TIT-TP-CAUS-RIMB
        //                :IND-TIT-TP-CAUS-RIMB
        //               ,:TIT-TOT-CNBT-ANTIRAC
        //                :IND-TIT-TOT-CNBT-ANTIRAC
        //               ,:TIT-FL-INC-AUTOGEN
        //                :IND-TIT-FL-INC-AUTOGEN
        //             FROM DETT_TIT_CONT A,
        //                  TIT_CONT      B
        //             WHERE      A.ID_TIT_CONT   = B.ID_TIT_CONT
        //                    AND A.ID_OGG        = :LDBV0371-ID-OGG
        //                    AND A.TP_OGG        = :LDBV0371-TP-OGG
        //                    AND B.TP_PRE_TIT    = :LDBV0371-TP-PRE-TIT
        //                    AND B.TP_STAT_TIT IN (:LDBV0371-TP-STAT-TIT1 ,
        //                                          :LDBV0371-TP-STAT-TIT2)
        //                    AND A.TP_STAT_TIT IN (:LDBV0371-TP-STAT-TIT1 ,
        //                                          :LDBV0371-TP-STAT-TIT2)
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.ID_MOVI_CHIU IS NULL
        //                    AND      B.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND      B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND      B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND      B.ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        dettTitCont1Dao.selectRec(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LDBS0370.cbl:line=867, because the code is unreachable.
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: A260-OPEN-CURSOR-WC-EFF<br>*/
    private void a260OpenCursorWcEff() {
        // COB_CODE: PERFORM A205-DECLARE-CURSOR-WC-EFF THRU A205-EX.
        a205DeclareCursorWcEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-EFF
        //           END-EXEC.
        dettTitCont1Dao.openCEff1(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A270-CLOSE-CURSOR-WC-EFF<br>*/
    private void a270CloseCursorWcEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-EFF
        //           END-EXEC.
        dettTitCont1Dao.closeCEff1();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A280-FETCH-FIRST-WC-EFF<br>*/
    private void a280FetchFirstWcEff() {
        // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF    THRU A260-EX.
        a260OpenCursorWcEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
            a290FetchNextWcEff();
        }
    }

    /**Original name: A290-FETCH-NEXT-WC-EFF<br>*/
    private void a290FetchNextWcEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-EFF
        //           INTO
        //                :DTC-ID-DETT-TIT-CONT
        //               ,:DTC-ID-TIT-CONT
        //               ,:DTC-ID-OGG
        //               ,:DTC-TP-OGG
        //               ,:DTC-ID-MOVI-CRZ
        //               ,:DTC-ID-MOVI-CHIU
        //                :IND-DTC-ID-MOVI-CHIU
        //               ,:DTC-DT-INI-EFF-DB
        //               ,:DTC-DT-END-EFF-DB
        //               ,:DTC-COD-COMP-ANIA
        //               ,:DTC-DT-INI-COP-DB
        //                :IND-DTC-DT-INI-COP
        //               ,:DTC-DT-END-COP-DB
        //                :IND-DTC-DT-END-COP
        //               ,:DTC-PRE-NET
        //                :IND-DTC-PRE-NET
        //               ,:DTC-INTR-FRAZ
        //                :IND-DTC-INTR-FRAZ
        //               ,:DTC-INTR-MORA
        //                :IND-DTC-INTR-MORA
        //               ,:DTC-INTR-RETDT
        //                :IND-DTC-INTR-RETDT
        //               ,:DTC-INTR-RIAT
        //                :IND-DTC-INTR-RIAT
        //               ,:DTC-DIR
        //                :IND-DTC-DIR
        //               ,:DTC-SPE-MED
        //                :IND-DTC-SPE-MED
        //               ,:DTC-TAX
        //                :IND-DTC-TAX
        //               ,:DTC-SOPR-SAN
        //                :IND-DTC-SOPR-SAN
        //               ,:DTC-SOPR-SPO
        //                :IND-DTC-SOPR-SPO
        //               ,:DTC-SOPR-TEC
        //                :IND-DTC-SOPR-TEC
        //               ,:DTC-SOPR-PROF
        //                :IND-DTC-SOPR-PROF
        //               ,:DTC-SOPR-ALT
        //                :IND-DTC-SOPR-ALT
        //               ,:DTC-PRE-TOT
        //                :IND-DTC-PRE-TOT
        //               ,:DTC-PRE-PP-IAS
        //                :IND-DTC-PRE-PP-IAS
        //               ,:DTC-PRE-SOLO-RSH
        //                :IND-DTC-PRE-SOLO-RSH
        //               ,:DTC-CAR-ACQ
        //                :IND-DTC-CAR-ACQ
        //               ,:DTC-CAR-GEST
        //                :IND-DTC-CAR-GEST
        //               ,:DTC-CAR-INC
        //                :IND-DTC-CAR-INC
        //               ,:DTC-PROV-ACQ-1AA
        //                :IND-DTC-PROV-ACQ-1AA
        //               ,:DTC-PROV-ACQ-2AA
        //                :IND-DTC-PROV-ACQ-2AA
        //               ,:DTC-PROV-RICOR
        //                :IND-DTC-PROV-RICOR
        //               ,:DTC-PROV-INC
        //                :IND-DTC-PROV-INC
        //               ,:DTC-PROV-DA-REC
        //                :IND-DTC-PROV-DA-REC
        //               ,:DTC-COD-DVS
        //                :IND-DTC-COD-DVS
        //               ,:DTC-FRQ-MOVI
        //                :IND-DTC-FRQ-MOVI
        //               ,:DTC-TP-RGM-FISC
        //               ,:DTC-COD-TARI
        //                :IND-DTC-COD-TARI
        //               ,:DTC-TP-STAT-TIT
        //               ,:DTC-IMP-AZ
        //                :IND-DTC-IMP-AZ
        //               ,:DTC-IMP-ADER
        //                :IND-DTC-IMP-ADER
        //               ,:DTC-IMP-TFR
        //                :IND-DTC-IMP-TFR
        //               ,:DTC-IMP-VOLO
        //                :IND-DTC-IMP-VOLO
        //               ,:DTC-MANFEE-ANTIC
        //                :IND-DTC-MANFEE-ANTIC
        //               ,:DTC-MANFEE-RICOR
        //                :IND-DTC-MANFEE-RICOR
        //               ,:DTC-MANFEE-REC
        //                :IND-DTC-MANFEE-REC
        //               ,:DTC-DT-ESI-TIT-DB
        //                :IND-DTC-DT-ESI-TIT
        //               ,:DTC-SPE-AGE
        //                :IND-DTC-SPE-AGE
        //               ,:DTC-CAR-IAS
        //                :IND-DTC-CAR-IAS
        //               ,:DTC-TOT-INTR-PREST
        //                :IND-DTC-TOT-INTR-PREST
        //               ,:DTC-DS-RIGA
        //               ,:DTC-DS-OPER-SQL
        //               ,:DTC-DS-VER
        //               ,:DTC-DS-TS-INI-CPTZ
        //               ,:DTC-DS-TS-END-CPTZ
        //               ,:DTC-DS-UTENTE
        //               ,:DTC-DS-STATO-ELAB
        //               ,:DTC-IMP-TRASFE
        //                :IND-DTC-IMP-TRASFE
        //               ,:DTC-IMP-TFR-STRC
        //                :IND-DTC-IMP-TFR-STRC
        //               ,:DTC-NUM-GG-RITARDO-PAG
        //                :IND-DTC-NUM-GG-RITARDO-PAG
        //               ,:DTC-NUM-GG-RIVAL
        //                :IND-DTC-NUM-GG-RIVAL
        //               ,:DTC-ACQ-EXP
        //                :IND-DTC-ACQ-EXP
        //               ,:DTC-REMUN-ASS
        //                :IND-DTC-REMUN-ASS
        //               ,:DTC-COMMIS-INTER
        //                :IND-DTC-COMMIS-INTER
        //               ,:DTC-CNBT-ANTIRAC
        //                :IND-DTC-CNBT-ANTIRAC
        //               ,:TIT-ID-TIT-CONT
        //               ,:TIT-ID-OGG
        //               ,:TIT-TP-OGG
        //               ,:TIT-IB-RICH
        //                :IND-TIT-IB-RICH
        //               ,:TIT-ID-MOVI-CRZ
        //               ,:TIT-ID-MOVI-CHIU
        //                :IND-TIT-ID-MOVI-CHIU
        //               ,:TIT-DT-INI-EFF-DB
        //               ,:TIT-DT-END-EFF-DB
        //               ,:TIT-COD-COMP-ANIA
        //               ,:TIT-TP-TIT
        //               ,:TIT-PROG-TIT
        //                :IND-TIT-PROG-TIT
        //               ,:TIT-TP-PRE-TIT
        //               ,:TIT-TP-STAT-TIT
        //               ,:TIT-DT-INI-COP-DB
        //                :IND-TIT-DT-INI-COP
        //               ,:TIT-DT-END-COP-DB
        //                :IND-TIT-DT-END-COP
        //               ,:TIT-IMP-PAG
        //                :IND-TIT-IMP-PAG
        //               ,:TIT-FL-SOLL
        //                :IND-TIT-FL-SOLL
        //               ,:TIT-FRAZ
        //                :IND-TIT-FRAZ
        //               ,:TIT-DT-APPLZ-MORA-DB
        //                :IND-TIT-DT-APPLZ-MORA
        //               ,:TIT-FL-MORA
        //                :IND-TIT-FL-MORA
        //               ,:TIT-ID-RAPP-RETE
        //                :IND-TIT-ID-RAPP-RETE
        //               ,:TIT-ID-RAPP-ANA
        //                :IND-TIT-ID-RAPP-ANA
        //               ,:TIT-COD-DVS
        //                :IND-TIT-COD-DVS
        //               ,:TIT-DT-EMIS-TIT-DB
        //                :IND-TIT-DT-EMIS-TIT
        //               ,:TIT-DT-ESI-TIT-DB
        //                :IND-TIT-DT-ESI-TIT
        //               ,:TIT-TOT-PRE-NET
        //                :IND-TIT-TOT-PRE-NET
        //               ,:TIT-TOT-INTR-FRAZ
        //                :IND-TIT-TOT-INTR-FRAZ
        //               ,:TIT-TOT-INTR-MORA
        //                :IND-TIT-TOT-INTR-MORA
        //               ,:TIT-TOT-INTR-PREST
        //                :IND-TIT-TOT-INTR-PREST
        //               ,:TIT-TOT-INTR-RETDT
        //                :IND-TIT-TOT-INTR-RETDT
        //               ,:TIT-TOT-INTR-RIAT
        //                :IND-TIT-TOT-INTR-RIAT
        //               ,:TIT-TOT-DIR
        //                :IND-TIT-TOT-DIR
        //               ,:TIT-TOT-SPE-MED
        //                :IND-TIT-TOT-SPE-MED
        //               ,:TIT-TOT-TAX
        //                :IND-TIT-TOT-TAX
        //               ,:TIT-TOT-SOPR-SAN
        //                :IND-TIT-TOT-SOPR-SAN
        //               ,:TIT-TOT-SOPR-TEC
        //                :IND-TIT-TOT-SOPR-TEC
        //               ,:TIT-TOT-SOPR-SPO
        //                :IND-TIT-TOT-SOPR-SPO
        //               ,:TIT-TOT-SOPR-PROF
        //                :IND-TIT-TOT-SOPR-PROF
        //               ,:TIT-TOT-SOPR-ALT
        //                :IND-TIT-TOT-SOPR-ALT
        //               ,:TIT-TOT-PRE-TOT
        //                :IND-TIT-TOT-PRE-TOT
        //               ,:TIT-TOT-PRE-PP-IAS
        //                :IND-TIT-TOT-PRE-PP-IAS
        //               ,:TIT-TOT-CAR-ACQ
        //                :IND-TIT-TOT-CAR-ACQ
        //               ,:TIT-TOT-CAR-GEST
        //                :IND-TIT-TOT-CAR-GEST
        //               ,:TIT-TOT-CAR-INC
        //                :IND-TIT-TOT-CAR-INC
        //               ,:TIT-TOT-PRE-SOLO-RSH
        //                :IND-TIT-TOT-PRE-SOLO-RSH
        //               ,:TIT-TOT-PROV-ACQ-1AA
        //                :IND-TIT-TOT-PROV-ACQ-1AA
        //               ,:TIT-TOT-PROV-ACQ-2AA
        //                :IND-TIT-TOT-PROV-ACQ-2AA
        //               ,:TIT-TOT-PROV-RICOR
        //                :IND-TIT-TOT-PROV-RICOR
        //               ,:TIT-TOT-PROV-INC
        //                :IND-TIT-TOT-PROV-INC
        //               ,:TIT-TOT-PROV-DA-REC
        //                :IND-TIT-TOT-PROV-DA-REC
        //               ,:TIT-IMP-AZ
        //                :IND-TIT-IMP-AZ
        //               ,:TIT-IMP-ADER
        //                :IND-TIT-IMP-ADER
        //               ,:TIT-IMP-TFR
        //                :IND-TIT-IMP-TFR
        //               ,:TIT-IMP-VOLO
        //                :IND-TIT-IMP-VOLO
        //               ,:TIT-TOT-MANFEE-ANTIC
        //                :IND-TIT-TOT-MANFEE-ANTIC
        //               ,:TIT-TOT-MANFEE-RICOR
        //                :IND-TIT-TOT-MANFEE-RICOR
        //               ,:TIT-TOT-MANFEE-REC
        //                :IND-TIT-TOT-MANFEE-REC
        //               ,:TIT-TP-MEZ-PAG-ADD
        //                :IND-TIT-TP-MEZ-PAG-ADD
        //               ,:TIT-ESTR-CNT-CORR-ADD
        //                :IND-TIT-ESTR-CNT-CORR-ADD
        //               ,:TIT-DT-VLT-DB
        //                :IND-TIT-DT-VLT
        //               ,:TIT-FL-FORZ-DT-VLT
        //                :IND-TIT-FL-FORZ-DT-VLT
        //               ,:TIT-DT-CAMBIO-VLT-DB
        //                :IND-TIT-DT-CAMBIO-VLT
        //               ,:TIT-TOT-SPE-AGE
        //                :IND-TIT-TOT-SPE-AGE
        //               ,:TIT-TOT-CAR-IAS
        //                :IND-TIT-TOT-CAR-IAS
        //               ,:TIT-NUM-RAT-ACCORPATE
        //                :IND-TIT-NUM-RAT-ACCORPATE
        //               ,:TIT-DS-RIGA
        //               ,:TIT-DS-OPER-SQL
        //               ,:TIT-DS-VER
        //               ,:TIT-DS-TS-INI-CPTZ
        //               ,:TIT-DS-TS-END-CPTZ
        //               ,:TIT-DS-UTENTE
        //               ,:TIT-DS-STATO-ELAB
        //               ,:TIT-FL-TIT-DA-REINVST
        //                :IND-TIT-FL-TIT-DA-REINVST
        //               ,:TIT-DT-RICH-ADD-RID-DB
        //                :IND-TIT-DT-RICH-ADD-RID
        //               ,:TIT-TP-ESI-RID
        //                :IND-TIT-TP-ESI-RID
        //               ,:TIT-COD-IBAN
        //                :IND-TIT-COD-IBAN
        //               ,:TIT-IMP-TRASFE
        //                :IND-TIT-IMP-TRASFE
        //               ,:TIT-IMP-TFR-STRC
        //                :IND-TIT-IMP-TFR-STRC
        //               ,:TIT-DT-CERT-FISC-DB
        //                :IND-TIT-DT-CERT-FISC
        //               ,:TIT-TP-CAUS-STOR
        //                :IND-TIT-TP-CAUS-STOR
        //               ,:TIT-TP-CAUS-DISP-STOR
        //                :IND-TIT-TP-CAUS-DISP-STOR
        //               ,:TIT-TP-TIT-MIGRAZ
        //                :IND-TIT-TP-TIT-MIGRAZ
        //               ,:TIT-TOT-ACQ-EXP
        //                :IND-TIT-TOT-ACQ-EXP
        //               ,:TIT-TOT-REMUN-ASS
        //                :IND-TIT-TOT-REMUN-ASS
        //               ,:TIT-TOT-COMMIS-INTER
        //                :IND-TIT-TOT-COMMIS-INTER
        //               ,:TIT-TP-CAUS-RIMB
        //                :IND-TIT-TP-CAUS-RIMB
        //               ,:TIT-TOT-CNBT-ANTIRAC
        //                :IND-TIT-TOT-CNBT-ANTIRAC
        //               ,:TIT-FL-INC-AUTOGEN
        //                :IND-TIT-FL-INC-AUTOGEN
        //           END-EXEC.
        dettTitCont1Dao.fetchCEff1(ws);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LDBS0370.cbl:line=1191, because the code is unreachable.
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF THRU A270-EX
            a270CloseCursorWcEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B205-DECLARE-CURSOR-WC-CPZ<br>
	 * <pre>----
	 * ----  gestione WC Competenza
	 * ----</pre>*/
    private void b205DeclareCursorWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //              DECLARE C-CPZ CURSOR FOR
        //              SELECT
        //                     A.ID_DETT_TIT_CONT
        //                    ,A.ID_TIT_CONT
        //                    ,A.ID_OGG
        //                    ,A.TP_OGG
        //                    ,A.ID_MOVI_CRZ
        //                    ,A.ID_MOVI_CHIU
        //                    ,A.DT_INI_EFF
        //                    ,A.DT_END_EFF
        //                    ,A.COD_COMP_ANIA
        //                    ,A.DT_INI_COP
        //                    ,A.DT_END_COP
        //                    ,A.PRE_NET
        //                    ,A.INTR_FRAZ
        //                    ,A.INTR_MORA
        //                    ,A.INTR_RETDT
        //                    ,A.INTR_RIAT
        //                    ,A.DIR
        //                    ,A.SPE_MED
        //                    ,A.TAX
        //                    ,A.SOPR_SAN
        //                    ,A.SOPR_SPO
        //                    ,A.SOPR_TEC
        //                    ,A.SOPR_PROF
        //                    ,A.SOPR_ALT
        //                    ,A.PRE_TOT
        //                    ,A.PRE_PP_IAS
        //                    ,A.PRE_SOLO_RSH
        //                    ,A.CAR_ACQ
        //                    ,A.CAR_GEST
        //                    ,A.CAR_INC
        //                    ,A.PROV_ACQ_1AA
        //                    ,A.PROV_ACQ_2AA
        //                    ,A.PROV_RICOR
        //                    ,A.PROV_INC
        //                    ,A.PROV_DA_REC
        //                    ,A.COD_DVS
        //                    ,A.FRQ_MOVI
        //                    ,A.TP_RGM_FISC
        //                    ,A.COD_TARI
        //                    ,A.TP_STAT_TIT
        //                    ,A.IMP_AZ
        //                    ,A.IMP_ADER
        //                    ,A.IMP_TFR
        //                    ,A.IMP_VOLO
        //                    ,A.MANFEE_ANTIC
        //                    ,A.MANFEE_RICOR
        //                    ,A.MANFEE_REC
        //                    ,A.DT_ESI_TIT
        //                    ,A.SPE_AGE
        //                    ,A.CAR_IAS
        //                    ,A.TOT_INTR_PREST
        //                    ,A.DS_RIGA
        //                    ,A.DS_OPER_SQL
        //                    ,A.DS_VER
        //                    ,A.DS_TS_INI_CPTZ
        //                    ,A.DS_TS_END_CPTZ
        //                    ,A.DS_UTENTE
        //                    ,A.DS_STATO_ELAB
        //                    ,A.IMP_TRASFE
        //                    ,A.IMP_TFR_STRC
        //                    ,A.NUM_GG_RITARDO_PAG
        //                    ,A.NUM_GG_RIVAL
        //                    ,A.ACQ_EXP
        //                    ,A.REMUN_ASS
        //                    ,A.COMMIS_INTER
        //                    ,A.CNBT_ANTIRAC
        //                    ,     B.ID_TIT_CONT
        //                    ,     B.ID_OGG
        //                    ,     B.TP_OGG
        //                    ,     B.IB_RICH
        //                    ,     B.ID_MOVI_CRZ
        //                    ,     B.ID_MOVI_CHIU
        //                    ,     B.DT_INI_EFF
        //                    ,     B.DT_END_EFF
        //                    ,     B.COD_COMP_ANIA
        //                    ,     B.TP_TIT
        //                    ,     B.PROG_TIT
        //                    ,     B.TP_PRE_TIT
        //                    ,     B.TP_STAT_TIT
        //                    ,     B.DT_INI_COP
        //                    ,     B.DT_END_COP
        //                    ,     B.IMP_PAG
        //                    ,     B.FL_SOLL
        //                    ,     B.FRAZ
        //                    ,     B.DT_APPLZ_MORA
        //                    ,     B.FL_MORA
        //                    ,     B.ID_RAPP_RETE
        //                    ,     B.ID_RAPP_ANA
        //                    ,     B.COD_DVS
        //                    ,     B.DT_EMIS_TIT
        //                    ,     B.DT_ESI_TIT
        //                    ,     B.TOT_PRE_NET
        //                    ,     B.TOT_INTR_FRAZ
        //                    ,     B.TOT_INTR_MORA
        //                    ,     B.TOT_INTR_PREST
        //                    ,     B.TOT_INTR_RETDT
        //                    ,     B.TOT_INTR_RIAT
        //                    ,     B.TOT_DIR
        //                    ,     B.TOT_SPE_MED
        //                    ,     B.TOT_TAX
        //                    ,     B.TOT_SOPR_SAN
        //                    ,     B.TOT_SOPR_TEC
        //                    ,     B.TOT_SOPR_SPO
        //                    ,     B.TOT_SOPR_PROF
        //                    ,     B.TOT_SOPR_ALT
        //                    ,     B.TOT_PRE_TOT
        //                    ,     B.TOT_PRE_PP_IAS
        //                    ,     B.TOT_CAR_ACQ
        //                    ,     B.TOT_CAR_GEST
        //                    ,     B.TOT_CAR_INC
        //                    ,     B.TOT_PRE_SOLO_RSH
        //                    ,     B.TOT_PROV_ACQ_1AA
        //                    ,     B.TOT_PROV_ACQ_2AA
        //                    ,     B.TOT_PROV_RICOR
        //                    ,     B.TOT_PROV_INC
        //                    ,     B.TOT_PROV_DA_REC
        //                    ,     B.IMP_AZ
        //                    ,     B.IMP_ADER
        //                    ,     B.IMP_TFR
        //                    ,     B.IMP_VOLO
        //                    ,     B.TOT_MANFEE_ANTIC
        //                    ,     B.TOT_MANFEE_RICOR
        //                    ,     B.TOT_MANFEE_REC
        //                    ,     B.TP_MEZ_PAG_ADD
        //                    ,     B.ESTR_CNT_CORR_ADD
        //                    ,     B.DT_VLT
        //                    ,     B.FL_FORZ_DT_VLT
        //                    ,     B.DT_CAMBIO_VLT
        //                    ,     B.TOT_SPE_AGE
        //                    ,     B.TOT_CAR_IAS
        //                    ,     B.NUM_RAT_ACCORPATE
        //                    ,     B.DS_RIGA
        //                    ,     B.DS_OPER_SQL
        //                    ,     B.DS_VER
        //                    ,     B.DS_TS_INI_CPTZ
        //                    ,     B.DS_TS_END_CPTZ
        //                    ,     B.DS_UTENTE
        //                    ,     B.DS_STATO_ELAB
        //                    ,     B.FL_TIT_DA_REINVST
        //                    ,     B.DT_RICH_ADD_RID
        //                    ,     B.TP_ESI_RID
        //                    ,     B.COD_IBAN
        //                    ,     B.IMP_TRASFE
        //                    ,     B.IMP_TFR_STRC
        //                    ,     B.DT_CERT_FISC
        //                    ,     B.TP_CAUS_STOR
        //                    ,     B.TP_CAUS_DISP_STOR
        //                    ,     B.TP_TIT_MIGRAZ
        //                    ,     B.TOT_ACQ_EXP
        //                    ,     B.TOT_REMUN_ASS
        //                    ,     B.TOT_COMMIS_INTER
        //                    ,     B.TP_CAUS_RIMB
        //                    ,     B.TOT_CNBT_ANTIRAC
        //                    ,     B.FL_INC_AUTOGEN
        //              FROM DETT_TIT_CONT A,
        //                   TIT_CONT      B
        //              WHERE      A.ID_TIT_CONT   = B.ID_TIT_CONT
        //                        AND A.ID_OGG        = :LDBV0371-ID-OGG
        //                        AND A.TP_OGG        = :LDBV0371-TP-OGG
        //                        AND B.TP_PRE_TIT    = :LDBV0371-TP-PRE-TIT
        //                    AND B.TP_STAT_TIT IN (:LDBV0371-TP-STAT-TIT1 ,
        //                                          :LDBV0371-TP-STAT-TIT2)
        //                    AND A.TP_STAT_TIT IN (:LDBV0371-TP-STAT-TIT1 ,
        //                                          :LDBV0371-TP-STAT-TIT2)
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND A.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //                    AND      B.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND      B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND      B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND      B.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND      B.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B210-SELECT-WC-CPZ<br>*/
    private void b210SelectWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                     A.ID_DETT_TIT_CONT
        //                    ,A.ID_TIT_CONT
        //                    ,A.ID_OGG
        //                    ,A.TP_OGG
        //                    ,A.ID_MOVI_CRZ
        //                    ,A.ID_MOVI_CHIU
        //                    ,A.DT_INI_EFF
        //                    ,A.DT_END_EFF
        //                    ,A.COD_COMP_ANIA
        //                    ,A.DT_INI_COP
        //                    ,A.DT_END_COP
        //                    ,A.PRE_NET
        //                    ,A.INTR_FRAZ
        //                    ,A.INTR_MORA
        //                    ,A.INTR_RETDT
        //                    ,A.INTR_RIAT
        //                    ,A.DIR
        //                    ,A.SPE_MED
        //                    ,A.TAX
        //                    ,A.SOPR_SAN
        //                    ,A.SOPR_SPO
        //                    ,A.SOPR_TEC
        //                    ,A.SOPR_PROF
        //                    ,A.SOPR_ALT
        //                    ,A.PRE_TOT
        //                    ,A.PRE_PP_IAS
        //                    ,A.PRE_SOLO_RSH
        //                    ,A.CAR_ACQ
        //                    ,A.CAR_GEST
        //                    ,A.CAR_INC
        //                    ,A.PROV_ACQ_1AA
        //                    ,A.PROV_ACQ_2AA
        //                    ,A.PROV_RICOR
        //                    ,A.PROV_INC
        //                    ,A.PROV_DA_REC
        //                    ,A.COD_DVS
        //                    ,A.FRQ_MOVI
        //                    ,A.TP_RGM_FISC
        //                    ,A.COD_TARI
        //                    ,A.TP_STAT_TIT
        //                    ,A.IMP_AZ
        //                    ,A.IMP_ADER
        //                    ,A.IMP_TFR
        //                    ,A.IMP_VOLO
        //                    ,A.MANFEE_ANTIC
        //                    ,A.MANFEE_RICOR
        //                    ,A.MANFEE_REC
        //                    ,A.DT_ESI_TIT
        //                    ,A.SPE_AGE
        //                    ,A.CAR_IAS
        //                    ,A.TOT_INTR_PREST
        //                    ,A.DS_RIGA
        //                    ,A.DS_OPER_SQL
        //                    ,A.DS_VER
        //                    ,A.DS_TS_INI_CPTZ
        //                    ,A.DS_TS_END_CPTZ
        //                    ,A.DS_UTENTE
        //                    ,A.DS_STATO_ELAB
        //                    ,A.IMP_TRASFE
        //                    ,A.IMP_TFR_STRC
        //                    ,A.NUM_GG_RITARDO_PAG
        //                    ,A.NUM_GG_RIVAL
        //                    ,A.ACQ_EXP
        //                    ,A.REMUN_ASS
        //                    ,A.COMMIS_INTER
        //                    ,A.CNBT_ANTIRAC
        //                    ,     B.ID_TIT_CONT
        //                    ,     B.ID_OGG
        //                    ,     B.TP_OGG
        //                    ,     B.IB_RICH
        //                    ,     B.ID_MOVI_CRZ
        //                    ,     B.ID_MOVI_CHIU
        //                    ,     B.DT_INI_EFF
        //                    ,     B.DT_END_EFF
        //                    ,     B.COD_COMP_ANIA
        //                    ,     B.TP_TIT
        //                    ,     B.PROG_TIT
        //                    ,     B.TP_PRE_TIT
        //                    ,     B.TP_STAT_TIT
        //                    ,     B.DT_INI_COP
        //                    ,     B.DT_END_COP
        //                    ,     B.IMP_PAG
        //                    ,     B.FL_SOLL
        //                    ,     B.FRAZ
        //                    ,     B.DT_APPLZ_MORA
        //                    ,     B.FL_MORA
        //                    ,     B.ID_RAPP_RETE
        //                    ,     B.ID_RAPP_ANA
        //                    ,     B.COD_DVS
        //                    ,     B.DT_EMIS_TIT
        //                    ,     B.DT_ESI_TIT
        //                    ,     B.TOT_PRE_NET
        //                    ,     B.TOT_INTR_FRAZ
        //                    ,     B.TOT_INTR_MORA
        //                    ,     B.TOT_INTR_PREST
        //                    ,     B.TOT_INTR_RETDT
        //                    ,     B.TOT_INTR_RIAT
        //                    ,     B.TOT_DIR
        //                    ,     B.TOT_SPE_MED
        //                    ,     B.TOT_TAX
        //                    ,     B.TOT_SOPR_SAN
        //                    ,     B.TOT_SOPR_TEC
        //                    ,     B.TOT_SOPR_SPO
        //                    ,     B.TOT_SOPR_PROF
        //                    ,     B.TOT_SOPR_ALT
        //                    ,     B.TOT_PRE_TOT
        //                    ,     B.TOT_PRE_PP_IAS
        //                    ,     B.TOT_CAR_ACQ
        //                    ,     B.TOT_CAR_GEST
        //                    ,     B.TOT_CAR_INC
        //                    ,     B.TOT_PRE_SOLO_RSH
        //                    ,     B.TOT_PROV_ACQ_1AA
        //                    ,     B.TOT_PROV_ACQ_2AA
        //                    ,     B.TOT_PROV_RICOR
        //                    ,     B.TOT_PROV_INC
        //                    ,     B.TOT_PROV_DA_REC
        //                    ,     B.IMP_AZ
        //                    ,     B.IMP_ADER
        //                    ,     B.IMP_TFR
        //                    ,     B.IMP_VOLO
        //                    ,     B.TOT_MANFEE_ANTIC
        //                    ,     B.TOT_MANFEE_RICOR
        //                    ,     B.TOT_MANFEE_REC
        //                    ,     B.TP_MEZ_PAG_ADD
        //                    ,     B.ESTR_CNT_CORR_ADD
        //                    ,     B.DT_VLT
        //                    ,     B.FL_FORZ_DT_VLT
        //                    ,     B.DT_CAMBIO_VLT
        //                    ,     B.TOT_SPE_AGE
        //                    ,     B.TOT_CAR_IAS
        //                    ,     B.NUM_RAT_ACCORPATE
        //                    ,     B.DS_RIGA
        //                    ,     B.DS_OPER_SQL
        //                    ,     B.DS_VER
        //                    ,     B.DS_TS_INI_CPTZ
        //                    ,     B.DS_TS_END_CPTZ
        //                    ,     B.DS_UTENTE
        //                    ,     B.DS_STATO_ELAB
        //                    ,     B.FL_TIT_DA_REINVST
        //                    ,     B.DT_RICH_ADD_RID
        //                    ,     B.TP_ESI_RID
        //                    ,     B.COD_IBAN
        //                    ,     B.IMP_TRASFE
        //                    ,     B.IMP_TFR_STRC
        //                    ,     B.DT_CERT_FISC
        //                    ,     B.TP_CAUS_STOR
        //                    ,     B.TP_CAUS_DISP_STOR
        //                    ,     B.TP_TIT_MIGRAZ
        //                    ,     B.TOT_ACQ_EXP
        //                    ,     B.TOT_REMUN_ASS
        //                    ,     B.TOT_COMMIS_INTER
        //                    ,     B.TP_CAUS_RIMB
        //                    ,     B.TOT_CNBT_ANTIRAC
        //                    ,     B.FL_INC_AUTOGEN
        //             INTO
        //                :DTC-ID-DETT-TIT-CONT
        //               ,:DTC-ID-TIT-CONT
        //               ,:DTC-ID-OGG
        //               ,:DTC-TP-OGG
        //               ,:DTC-ID-MOVI-CRZ
        //               ,:DTC-ID-MOVI-CHIU
        //                :IND-DTC-ID-MOVI-CHIU
        //               ,:DTC-DT-INI-EFF-DB
        //               ,:DTC-DT-END-EFF-DB
        //               ,:DTC-COD-COMP-ANIA
        //               ,:DTC-DT-INI-COP-DB
        //                :IND-DTC-DT-INI-COP
        //               ,:DTC-DT-END-COP-DB
        //                :IND-DTC-DT-END-COP
        //               ,:DTC-PRE-NET
        //                :IND-DTC-PRE-NET
        //               ,:DTC-INTR-FRAZ
        //                :IND-DTC-INTR-FRAZ
        //               ,:DTC-INTR-MORA
        //                :IND-DTC-INTR-MORA
        //               ,:DTC-INTR-RETDT
        //                :IND-DTC-INTR-RETDT
        //               ,:DTC-INTR-RIAT
        //                :IND-DTC-INTR-RIAT
        //               ,:DTC-DIR
        //                :IND-DTC-DIR
        //               ,:DTC-SPE-MED
        //                :IND-DTC-SPE-MED
        //               ,:DTC-TAX
        //                :IND-DTC-TAX
        //               ,:DTC-SOPR-SAN
        //                :IND-DTC-SOPR-SAN
        //               ,:DTC-SOPR-SPO
        //                :IND-DTC-SOPR-SPO
        //               ,:DTC-SOPR-TEC
        //                :IND-DTC-SOPR-TEC
        //               ,:DTC-SOPR-PROF
        //                :IND-DTC-SOPR-PROF
        //               ,:DTC-SOPR-ALT
        //                :IND-DTC-SOPR-ALT
        //               ,:DTC-PRE-TOT
        //                :IND-DTC-PRE-TOT
        //               ,:DTC-PRE-PP-IAS
        //                :IND-DTC-PRE-PP-IAS
        //               ,:DTC-PRE-SOLO-RSH
        //                :IND-DTC-PRE-SOLO-RSH
        //               ,:DTC-CAR-ACQ
        //                :IND-DTC-CAR-ACQ
        //               ,:DTC-CAR-GEST
        //                :IND-DTC-CAR-GEST
        //               ,:DTC-CAR-INC
        //                :IND-DTC-CAR-INC
        //               ,:DTC-PROV-ACQ-1AA
        //                :IND-DTC-PROV-ACQ-1AA
        //               ,:DTC-PROV-ACQ-2AA
        //                :IND-DTC-PROV-ACQ-2AA
        //               ,:DTC-PROV-RICOR
        //                :IND-DTC-PROV-RICOR
        //               ,:DTC-PROV-INC
        //                :IND-DTC-PROV-INC
        //               ,:DTC-PROV-DA-REC
        //                :IND-DTC-PROV-DA-REC
        //               ,:DTC-COD-DVS
        //                :IND-DTC-COD-DVS
        //               ,:DTC-FRQ-MOVI
        //                :IND-DTC-FRQ-MOVI
        //               ,:DTC-TP-RGM-FISC
        //               ,:DTC-COD-TARI
        //                :IND-DTC-COD-TARI
        //               ,:DTC-TP-STAT-TIT
        //               ,:DTC-IMP-AZ
        //                :IND-DTC-IMP-AZ
        //               ,:DTC-IMP-ADER
        //                :IND-DTC-IMP-ADER
        //               ,:DTC-IMP-TFR
        //                :IND-DTC-IMP-TFR
        //               ,:DTC-IMP-VOLO
        //                :IND-DTC-IMP-VOLO
        //               ,:DTC-MANFEE-ANTIC
        //                :IND-DTC-MANFEE-ANTIC
        //               ,:DTC-MANFEE-RICOR
        //                :IND-DTC-MANFEE-RICOR
        //               ,:DTC-MANFEE-REC
        //                :IND-DTC-MANFEE-REC
        //               ,:DTC-DT-ESI-TIT-DB
        //                :IND-DTC-DT-ESI-TIT
        //               ,:DTC-SPE-AGE
        //                :IND-DTC-SPE-AGE
        //               ,:DTC-CAR-IAS
        //                :IND-DTC-CAR-IAS
        //               ,:DTC-TOT-INTR-PREST
        //                :IND-DTC-TOT-INTR-PREST
        //               ,:DTC-DS-RIGA
        //               ,:DTC-DS-OPER-SQL
        //               ,:DTC-DS-VER
        //               ,:DTC-DS-TS-INI-CPTZ
        //               ,:DTC-DS-TS-END-CPTZ
        //               ,:DTC-DS-UTENTE
        //               ,:DTC-DS-STATO-ELAB
        //               ,:DTC-IMP-TRASFE
        //                :IND-DTC-IMP-TRASFE
        //               ,:DTC-IMP-TFR-STRC
        //                :IND-DTC-IMP-TFR-STRC
        //               ,:DTC-NUM-GG-RITARDO-PAG
        //                :IND-DTC-NUM-GG-RITARDO-PAG
        //               ,:DTC-NUM-GG-RIVAL
        //                :IND-DTC-NUM-GG-RIVAL
        //               ,:DTC-ACQ-EXP
        //                :IND-DTC-ACQ-EXP
        //               ,:DTC-REMUN-ASS
        //                :IND-DTC-REMUN-ASS
        //               ,:DTC-COMMIS-INTER
        //                :IND-DTC-COMMIS-INTER
        //               ,:DTC-CNBT-ANTIRAC
        //                :IND-DTC-CNBT-ANTIRAC
        //               ,:TIT-ID-TIT-CONT
        //               ,:TIT-ID-OGG
        //               ,:TIT-TP-OGG
        //               ,:TIT-IB-RICH
        //                :IND-TIT-IB-RICH
        //               ,:TIT-ID-MOVI-CRZ
        //               ,:TIT-ID-MOVI-CHIU
        //                :IND-TIT-ID-MOVI-CHIU
        //               ,:TIT-DT-INI-EFF-DB
        //               ,:TIT-DT-END-EFF-DB
        //               ,:TIT-COD-COMP-ANIA
        //               ,:TIT-TP-TIT
        //               ,:TIT-PROG-TIT
        //                :IND-TIT-PROG-TIT
        //               ,:TIT-TP-PRE-TIT
        //               ,:TIT-TP-STAT-TIT
        //               ,:TIT-DT-INI-COP-DB
        //                :IND-TIT-DT-INI-COP
        //               ,:TIT-DT-END-COP-DB
        //                :IND-TIT-DT-END-COP
        //               ,:TIT-IMP-PAG
        //                :IND-TIT-IMP-PAG
        //               ,:TIT-FL-SOLL
        //                :IND-TIT-FL-SOLL
        //               ,:TIT-FRAZ
        //                :IND-TIT-FRAZ
        //               ,:TIT-DT-APPLZ-MORA-DB
        //                :IND-TIT-DT-APPLZ-MORA
        //               ,:TIT-FL-MORA
        //                :IND-TIT-FL-MORA
        //               ,:TIT-ID-RAPP-RETE
        //                :IND-TIT-ID-RAPP-RETE
        //               ,:TIT-ID-RAPP-ANA
        //                :IND-TIT-ID-RAPP-ANA
        //               ,:TIT-COD-DVS
        //                :IND-TIT-COD-DVS
        //               ,:TIT-DT-EMIS-TIT-DB
        //                :IND-TIT-DT-EMIS-TIT
        //               ,:TIT-DT-ESI-TIT-DB
        //                :IND-TIT-DT-ESI-TIT
        //               ,:TIT-TOT-PRE-NET
        //                :IND-TIT-TOT-PRE-NET
        //               ,:TIT-TOT-INTR-FRAZ
        //                :IND-TIT-TOT-INTR-FRAZ
        //               ,:TIT-TOT-INTR-MORA
        //                :IND-TIT-TOT-INTR-MORA
        //               ,:TIT-TOT-INTR-PREST
        //                :IND-TIT-TOT-INTR-PREST
        //               ,:TIT-TOT-INTR-RETDT
        //                :IND-TIT-TOT-INTR-RETDT
        //               ,:TIT-TOT-INTR-RIAT
        //                :IND-TIT-TOT-INTR-RIAT
        //               ,:TIT-TOT-DIR
        //                :IND-TIT-TOT-DIR
        //               ,:TIT-TOT-SPE-MED
        //                :IND-TIT-TOT-SPE-MED
        //               ,:TIT-TOT-TAX
        //                :IND-TIT-TOT-TAX
        //               ,:TIT-TOT-SOPR-SAN
        //                :IND-TIT-TOT-SOPR-SAN
        //               ,:TIT-TOT-SOPR-TEC
        //                :IND-TIT-TOT-SOPR-TEC
        //               ,:TIT-TOT-SOPR-SPO
        //                :IND-TIT-TOT-SOPR-SPO
        //               ,:TIT-TOT-SOPR-PROF
        //                :IND-TIT-TOT-SOPR-PROF
        //               ,:TIT-TOT-SOPR-ALT
        //                :IND-TIT-TOT-SOPR-ALT
        //               ,:TIT-TOT-PRE-TOT
        //                :IND-TIT-TOT-PRE-TOT
        //               ,:TIT-TOT-PRE-PP-IAS
        //                :IND-TIT-TOT-PRE-PP-IAS
        //               ,:TIT-TOT-CAR-ACQ
        //                :IND-TIT-TOT-CAR-ACQ
        //               ,:TIT-TOT-CAR-GEST
        //                :IND-TIT-TOT-CAR-GEST
        //               ,:TIT-TOT-CAR-INC
        //                :IND-TIT-TOT-CAR-INC
        //               ,:TIT-TOT-PRE-SOLO-RSH
        //                :IND-TIT-TOT-PRE-SOLO-RSH
        //               ,:TIT-TOT-PROV-ACQ-1AA
        //                :IND-TIT-TOT-PROV-ACQ-1AA
        //               ,:TIT-TOT-PROV-ACQ-2AA
        //                :IND-TIT-TOT-PROV-ACQ-2AA
        //               ,:TIT-TOT-PROV-RICOR
        //                :IND-TIT-TOT-PROV-RICOR
        //               ,:TIT-TOT-PROV-INC
        //                :IND-TIT-TOT-PROV-INC
        //               ,:TIT-TOT-PROV-DA-REC
        //                :IND-TIT-TOT-PROV-DA-REC
        //               ,:TIT-IMP-AZ
        //                :IND-TIT-IMP-AZ
        //               ,:TIT-IMP-ADER
        //                :IND-TIT-IMP-ADER
        //               ,:TIT-IMP-TFR
        //                :IND-TIT-IMP-TFR
        //               ,:TIT-IMP-VOLO
        //                :IND-TIT-IMP-VOLO
        //               ,:TIT-TOT-MANFEE-ANTIC
        //                :IND-TIT-TOT-MANFEE-ANTIC
        //               ,:TIT-TOT-MANFEE-RICOR
        //                :IND-TIT-TOT-MANFEE-RICOR
        //               ,:TIT-TOT-MANFEE-REC
        //                :IND-TIT-TOT-MANFEE-REC
        //               ,:TIT-TP-MEZ-PAG-ADD
        //                :IND-TIT-TP-MEZ-PAG-ADD
        //               ,:TIT-ESTR-CNT-CORR-ADD
        //                :IND-TIT-ESTR-CNT-CORR-ADD
        //               ,:TIT-DT-VLT-DB
        //                :IND-TIT-DT-VLT
        //               ,:TIT-FL-FORZ-DT-VLT
        //                :IND-TIT-FL-FORZ-DT-VLT
        //               ,:TIT-DT-CAMBIO-VLT-DB
        //                :IND-TIT-DT-CAMBIO-VLT
        //               ,:TIT-TOT-SPE-AGE
        //                :IND-TIT-TOT-SPE-AGE
        //               ,:TIT-TOT-CAR-IAS
        //                :IND-TIT-TOT-CAR-IAS
        //               ,:TIT-NUM-RAT-ACCORPATE
        //                :IND-TIT-NUM-RAT-ACCORPATE
        //               ,:TIT-DS-RIGA
        //               ,:TIT-DS-OPER-SQL
        //               ,:TIT-DS-VER
        //               ,:TIT-DS-TS-INI-CPTZ
        //               ,:TIT-DS-TS-END-CPTZ
        //               ,:TIT-DS-UTENTE
        //               ,:TIT-DS-STATO-ELAB
        //               ,:TIT-FL-TIT-DA-REINVST
        //                :IND-TIT-FL-TIT-DA-REINVST
        //               ,:TIT-DT-RICH-ADD-RID-DB
        //                :IND-TIT-DT-RICH-ADD-RID
        //               ,:TIT-TP-ESI-RID
        //                :IND-TIT-TP-ESI-RID
        //               ,:TIT-COD-IBAN
        //                :IND-TIT-COD-IBAN
        //               ,:TIT-IMP-TRASFE
        //                :IND-TIT-IMP-TRASFE
        //               ,:TIT-IMP-TFR-STRC
        //                :IND-TIT-IMP-TFR-STRC
        //               ,:TIT-DT-CERT-FISC-DB
        //                :IND-TIT-DT-CERT-FISC
        //               ,:TIT-TP-CAUS-STOR
        //                :IND-TIT-TP-CAUS-STOR
        //               ,:TIT-TP-CAUS-DISP-STOR
        //                :IND-TIT-TP-CAUS-DISP-STOR
        //               ,:TIT-TP-TIT-MIGRAZ
        //                :IND-TIT-TP-TIT-MIGRAZ
        //               ,:TIT-TOT-ACQ-EXP
        //                :IND-TIT-TOT-ACQ-EXP
        //               ,:TIT-TOT-REMUN-ASS
        //                :IND-TIT-TOT-REMUN-ASS
        //               ,:TIT-TOT-COMMIS-INTER
        //                :IND-TIT-TOT-COMMIS-INTER
        //               ,:TIT-TP-CAUS-RIMB
        //                :IND-TIT-TP-CAUS-RIMB
        //               ,:TIT-TOT-CNBT-ANTIRAC
        //                :IND-TIT-TOT-CNBT-ANTIRAC
        //               ,:TIT-FL-INC-AUTOGEN
        //                :IND-TIT-FL-INC-AUTOGEN
        //             FROM DETT_TIT_CONT A,
        //                  TIT_CONT      B
        //             WHERE      A.ID_TIT_CONT   = B.ID_TIT_CONT
        //                    AND A.ID_OGG        = :LDBV0371-ID-OGG
        //                    AND A.TP_OGG        = :LDBV0371-TP-OGG
        //                    AND B.TP_PRE_TIT    = :LDBV0371-TP-PRE-TIT
        //                    AND B.TP_STAT_TIT IN (:LDBV0371-TP-STAT-TIT1 ,
        //                                          :LDBV0371-TP-STAT-TIT2)
        //                    AND A.TP_STAT_TIT IN (:LDBV0371-TP-STAT-TIT1 ,
        //                                          :LDBV0371-TP-STAT-TIT2)
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND A.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //                    AND      B.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND      B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND      B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND      B.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND      B.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        dettTitCont1Dao.selectRec1(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LDBS0370.cbl:line=1887, because the code is unreachable.
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: B260-OPEN-CURSOR-WC-CPZ<br>*/
    private void b260OpenCursorWcCpz() {
        // COB_CODE: PERFORM B205-DECLARE-CURSOR-WC-CPZ THRU B205-EX.
        b205DeclareCursorWcCpz();
        // COB_CODE: EXEC SQL
        //                OPEN C-CPZ
        //           END-EXEC.
        dettTitCont1Dao.openCCpz1(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B270-CLOSE-CURSOR-WC-CPZ<br>*/
    private void b270CloseCursorWcCpz() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-CPZ
        //           END-EXEC.
        dettTitCont1Dao.closeCCpz1();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B280-FETCH-FIRST-WC-CPZ<br>*/
    private void b280FetchFirstWcCpz() {
        // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ    THRU B260-EX.
        b260OpenCursorWcCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
            b290FetchNextWcCpz();
        }
    }

    /**Original name: B290-FETCH-NEXT-WC-CPZ<br>*/
    private void b290FetchNextWcCpz() {
        // COB_CODE: EXEC SQL
        //                FETCH C-CPZ
        //           INTO
        //                :DTC-ID-DETT-TIT-CONT
        //               ,:DTC-ID-TIT-CONT
        //               ,:DTC-ID-OGG
        //               ,:DTC-TP-OGG
        //               ,:DTC-ID-MOVI-CRZ
        //               ,:DTC-ID-MOVI-CHIU
        //                :IND-DTC-ID-MOVI-CHIU
        //               ,:DTC-DT-INI-EFF-DB
        //               ,:DTC-DT-END-EFF-DB
        //               ,:DTC-COD-COMP-ANIA
        //               ,:DTC-DT-INI-COP-DB
        //                :IND-DTC-DT-INI-COP
        //               ,:DTC-DT-END-COP-DB
        //                :IND-DTC-DT-END-COP
        //               ,:DTC-PRE-NET
        //                :IND-DTC-PRE-NET
        //               ,:DTC-INTR-FRAZ
        //                :IND-DTC-INTR-FRAZ
        //               ,:DTC-INTR-MORA
        //                :IND-DTC-INTR-MORA
        //               ,:DTC-INTR-RETDT
        //                :IND-DTC-INTR-RETDT
        //               ,:DTC-INTR-RIAT
        //                :IND-DTC-INTR-RIAT
        //               ,:DTC-DIR
        //                :IND-DTC-DIR
        //               ,:DTC-SPE-MED
        //                :IND-DTC-SPE-MED
        //               ,:DTC-TAX
        //                :IND-DTC-TAX
        //               ,:DTC-SOPR-SAN
        //                :IND-DTC-SOPR-SAN
        //               ,:DTC-SOPR-SPO
        //                :IND-DTC-SOPR-SPO
        //               ,:DTC-SOPR-TEC
        //                :IND-DTC-SOPR-TEC
        //               ,:DTC-SOPR-PROF
        //                :IND-DTC-SOPR-PROF
        //               ,:DTC-SOPR-ALT
        //                :IND-DTC-SOPR-ALT
        //               ,:DTC-PRE-TOT
        //                :IND-DTC-PRE-TOT
        //               ,:DTC-PRE-PP-IAS
        //                :IND-DTC-PRE-PP-IAS
        //               ,:DTC-PRE-SOLO-RSH
        //                :IND-DTC-PRE-SOLO-RSH
        //               ,:DTC-CAR-ACQ
        //                :IND-DTC-CAR-ACQ
        //               ,:DTC-CAR-GEST
        //                :IND-DTC-CAR-GEST
        //               ,:DTC-CAR-INC
        //                :IND-DTC-CAR-INC
        //               ,:DTC-PROV-ACQ-1AA
        //                :IND-DTC-PROV-ACQ-1AA
        //               ,:DTC-PROV-ACQ-2AA
        //                :IND-DTC-PROV-ACQ-2AA
        //               ,:DTC-PROV-RICOR
        //                :IND-DTC-PROV-RICOR
        //               ,:DTC-PROV-INC
        //                :IND-DTC-PROV-INC
        //               ,:DTC-PROV-DA-REC
        //                :IND-DTC-PROV-DA-REC
        //               ,:DTC-COD-DVS
        //                :IND-DTC-COD-DVS
        //               ,:DTC-FRQ-MOVI
        //                :IND-DTC-FRQ-MOVI
        //               ,:DTC-TP-RGM-FISC
        //               ,:DTC-COD-TARI
        //                :IND-DTC-COD-TARI
        //               ,:DTC-TP-STAT-TIT
        //               ,:DTC-IMP-AZ
        //                :IND-DTC-IMP-AZ
        //               ,:DTC-IMP-ADER
        //                :IND-DTC-IMP-ADER
        //               ,:DTC-IMP-TFR
        //                :IND-DTC-IMP-TFR
        //               ,:DTC-IMP-VOLO
        //                :IND-DTC-IMP-VOLO
        //               ,:DTC-MANFEE-ANTIC
        //                :IND-DTC-MANFEE-ANTIC
        //               ,:DTC-MANFEE-RICOR
        //                :IND-DTC-MANFEE-RICOR
        //               ,:DTC-MANFEE-REC
        //                :IND-DTC-MANFEE-REC
        //               ,:DTC-DT-ESI-TIT-DB
        //                :IND-DTC-DT-ESI-TIT
        //               ,:DTC-SPE-AGE
        //                :IND-DTC-SPE-AGE
        //               ,:DTC-CAR-IAS
        //                :IND-DTC-CAR-IAS
        //               ,:DTC-TOT-INTR-PREST
        //                :IND-DTC-TOT-INTR-PREST
        //               ,:DTC-DS-RIGA
        //               ,:DTC-DS-OPER-SQL
        //               ,:DTC-DS-VER
        //               ,:DTC-DS-TS-INI-CPTZ
        //               ,:DTC-DS-TS-END-CPTZ
        //               ,:DTC-DS-UTENTE
        //               ,:DTC-DS-STATO-ELAB
        //               ,:DTC-IMP-TRASFE
        //                :IND-DTC-IMP-TRASFE
        //               ,:DTC-IMP-TFR-STRC
        //                :IND-DTC-IMP-TFR-STRC
        //               ,:DTC-NUM-GG-RITARDO-PAG
        //                :IND-DTC-NUM-GG-RITARDO-PAG
        //               ,:DTC-NUM-GG-RIVAL
        //                :IND-DTC-NUM-GG-RIVAL
        //               ,:DTC-ACQ-EXP
        //                :IND-DTC-ACQ-EXP
        //               ,:DTC-REMUN-ASS
        //                :IND-DTC-REMUN-ASS
        //               ,:DTC-COMMIS-INTER
        //                :IND-DTC-COMMIS-INTER
        //               ,:DTC-CNBT-ANTIRAC
        //                :IND-DTC-CNBT-ANTIRAC
        //               ,:TIT-ID-TIT-CONT
        //               ,:TIT-ID-OGG
        //               ,:TIT-TP-OGG
        //               ,:TIT-IB-RICH
        //                :IND-TIT-IB-RICH
        //               ,:TIT-ID-MOVI-CRZ
        //               ,:TIT-ID-MOVI-CHIU
        //                :IND-TIT-ID-MOVI-CHIU
        //               ,:TIT-DT-INI-EFF-DB
        //               ,:TIT-DT-END-EFF-DB
        //               ,:TIT-COD-COMP-ANIA
        //               ,:TIT-TP-TIT
        //               ,:TIT-PROG-TIT
        //                :IND-TIT-PROG-TIT
        //               ,:TIT-TP-PRE-TIT
        //               ,:TIT-TP-STAT-TIT
        //               ,:TIT-DT-INI-COP-DB
        //                :IND-TIT-DT-INI-COP
        //               ,:TIT-DT-END-COP-DB
        //                :IND-TIT-DT-END-COP
        //               ,:TIT-IMP-PAG
        //                :IND-TIT-IMP-PAG
        //               ,:TIT-FL-SOLL
        //                :IND-TIT-FL-SOLL
        //               ,:TIT-FRAZ
        //                :IND-TIT-FRAZ
        //               ,:TIT-DT-APPLZ-MORA-DB
        //                :IND-TIT-DT-APPLZ-MORA
        //               ,:TIT-FL-MORA
        //                :IND-TIT-FL-MORA
        //               ,:TIT-ID-RAPP-RETE
        //                :IND-TIT-ID-RAPP-RETE
        //               ,:TIT-ID-RAPP-ANA
        //                :IND-TIT-ID-RAPP-ANA
        //               ,:TIT-COD-DVS
        //                :IND-TIT-COD-DVS
        //               ,:TIT-DT-EMIS-TIT-DB
        //                :IND-TIT-DT-EMIS-TIT
        //               ,:TIT-DT-ESI-TIT-DB
        //                :IND-TIT-DT-ESI-TIT
        //               ,:TIT-TOT-PRE-NET
        //                :IND-TIT-TOT-PRE-NET
        //               ,:TIT-TOT-INTR-FRAZ
        //                :IND-TIT-TOT-INTR-FRAZ
        //               ,:TIT-TOT-INTR-MORA
        //                :IND-TIT-TOT-INTR-MORA
        //               ,:TIT-TOT-INTR-PREST
        //                :IND-TIT-TOT-INTR-PREST
        //               ,:TIT-TOT-INTR-RETDT
        //                :IND-TIT-TOT-INTR-RETDT
        //               ,:TIT-TOT-INTR-RIAT
        //                :IND-TIT-TOT-INTR-RIAT
        //               ,:TIT-TOT-DIR
        //                :IND-TIT-TOT-DIR
        //               ,:TIT-TOT-SPE-MED
        //                :IND-TIT-TOT-SPE-MED
        //               ,:TIT-TOT-TAX
        //                :IND-TIT-TOT-TAX
        //               ,:TIT-TOT-SOPR-SAN
        //                :IND-TIT-TOT-SOPR-SAN
        //               ,:TIT-TOT-SOPR-TEC
        //                :IND-TIT-TOT-SOPR-TEC
        //               ,:TIT-TOT-SOPR-SPO
        //                :IND-TIT-TOT-SOPR-SPO
        //               ,:TIT-TOT-SOPR-PROF
        //                :IND-TIT-TOT-SOPR-PROF
        //               ,:TIT-TOT-SOPR-ALT
        //                :IND-TIT-TOT-SOPR-ALT
        //               ,:TIT-TOT-PRE-TOT
        //                :IND-TIT-TOT-PRE-TOT
        //               ,:TIT-TOT-PRE-PP-IAS
        //                :IND-TIT-TOT-PRE-PP-IAS
        //               ,:TIT-TOT-CAR-ACQ
        //                :IND-TIT-TOT-CAR-ACQ
        //               ,:TIT-TOT-CAR-GEST
        //                :IND-TIT-TOT-CAR-GEST
        //               ,:TIT-TOT-CAR-INC
        //                :IND-TIT-TOT-CAR-INC
        //               ,:TIT-TOT-PRE-SOLO-RSH
        //                :IND-TIT-TOT-PRE-SOLO-RSH
        //               ,:TIT-TOT-PROV-ACQ-1AA
        //                :IND-TIT-TOT-PROV-ACQ-1AA
        //               ,:TIT-TOT-PROV-ACQ-2AA
        //                :IND-TIT-TOT-PROV-ACQ-2AA
        //               ,:TIT-TOT-PROV-RICOR
        //                :IND-TIT-TOT-PROV-RICOR
        //               ,:TIT-TOT-PROV-INC
        //                :IND-TIT-TOT-PROV-INC
        //               ,:TIT-TOT-PROV-DA-REC
        //                :IND-TIT-TOT-PROV-DA-REC
        //               ,:TIT-IMP-AZ
        //                :IND-TIT-IMP-AZ
        //               ,:TIT-IMP-ADER
        //                :IND-TIT-IMP-ADER
        //               ,:TIT-IMP-TFR
        //                :IND-TIT-IMP-TFR
        //               ,:TIT-IMP-VOLO
        //                :IND-TIT-IMP-VOLO
        //               ,:TIT-TOT-MANFEE-ANTIC
        //                :IND-TIT-TOT-MANFEE-ANTIC
        //               ,:TIT-TOT-MANFEE-RICOR
        //                :IND-TIT-TOT-MANFEE-RICOR
        //               ,:TIT-TOT-MANFEE-REC
        //                :IND-TIT-TOT-MANFEE-REC
        //               ,:TIT-TP-MEZ-PAG-ADD
        //                :IND-TIT-TP-MEZ-PAG-ADD
        //               ,:TIT-ESTR-CNT-CORR-ADD
        //                :IND-TIT-ESTR-CNT-CORR-ADD
        //               ,:TIT-DT-VLT-DB
        //                :IND-TIT-DT-VLT
        //               ,:TIT-FL-FORZ-DT-VLT
        //                :IND-TIT-FL-FORZ-DT-VLT
        //               ,:TIT-DT-CAMBIO-VLT-DB
        //                :IND-TIT-DT-CAMBIO-VLT
        //               ,:TIT-TOT-SPE-AGE
        //                :IND-TIT-TOT-SPE-AGE
        //               ,:TIT-TOT-CAR-IAS
        //                :IND-TIT-TOT-CAR-IAS
        //               ,:TIT-NUM-RAT-ACCORPATE
        //                :IND-TIT-NUM-RAT-ACCORPATE
        //               ,:TIT-DS-RIGA
        //               ,:TIT-DS-OPER-SQL
        //               ,:TIT-DS-VER
        //               ,:TIT-DS-TS-INI-CPTZ
        //               ,:TIT-DS-TS-END-CPTZ
        //               ,:TIT-DS-UTENTE
        //               ,:TIT-DS-STATO-ELAB
        //               ,:TIT-FL-TIT-DA-REINVST
        //                :IND-TIT-FL-TIT-DA-REINVST
        //               ,:TIT-DT-RICH-ADD-RID-DB
        //                :IND-TIT-DT-RICH-ADD-RID
        //               ,:TIT-TP-ESI-RID
        //                :IND-TIT-TP-ESI-RID
        //               ,:TIT-COD-IBAN
        //                :IND-TIT-COD-IBAN
        //               ,:TIT-IMP-TRASFE
        //                :IND-TIT-IMP-TRASFE
        //               ,:TIT-IMP-TFR-STRC
        //                :IND-TIT-IMP-TFR-STRC
        //               ,:TIT-DT-CERT-FISC-DB
        //                :IND-TIT-DT-CERT-FISC
        //               ,:TIT-TP-CAUS-STOR
        //                :IND-TIT-TP-CAUS-STOR
        //               ,:TIT-TP-CAUS-DISP-STOR
        //                :IND-TIT-TP-CAUS-DISP-STOR
        //               ,:TIT-TP-TIT-MIGRAZ
        //                :IND-TIT-TP-TIT-MIGRAZ
        //               ,:TIT-TOT-ACQ-EXP
        //                :IND-TIT-TOT-ACQ-EXP
        //               ,:TIT-TOT-REMUN-ASS
        //                :IND-TIT-TOT-REMUN-ASS
        //               ,:TIT-TOT-COMMIS-INTER
        //                :IND-TIT-TOT-COMMIS-INTER
        //               ,:TIT-TP-CAUS-RIMB
        //                :IND-TIT-TP-CAUS-RIMB
        //               ,:TIT-TOT-CNBT-ANTIRAC
        //                :IND-TIT-TOT-CNBT-ANTIRAC
        //               ,:TIT-FL-INC-AUTOGEN
        //                :IND-TIT-FL-INC-AUTOGEN
        //           END-EXEC.
        dettTitCont1Dao.fetchCCpz1(ws);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LDBS0370.cbl:line=2210, because the code is unreachable.
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ THRU B270-EX
            b270CloseCursorWcCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: C205-DECLARE-CURSOR-WC-NST<br>
	 * <pre>----
	 * ----  gestione WC Senza Storicità
	 * ----</pre>*/
    private void c205DeclareCursorWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C210-SELECT-WC-NST<br>*/
    private void c210SelectWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C260-OPEN-CURSOR-WC-NST<br>*/
    private void c260OpenCursorWcNst() {
        // COB_CODE: PERFORM C205-DECLARE-CURSOR-WC-NST THRU C205-EX.
        c205DeclareCursorWcNst();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C270-CLOSE-CURSOR-WC-NST<br>*/
    private void c270CloseCursorWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C280-FETCH-FIRST-WC-NST<br>*/
    private void c280FetchFirstWcNst() {
        // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST    THRU C260-EX.
        c260OpenCursorWcNst();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
            c290FetchNextWcNst();
        }
    }

    /**Original name: C290-FETCH-NEXT-WC-NST<br>*/
    private void c290FetchNextWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>----
	 * ----  utilità comuni a tutti i livelli operazione
	 * ----</pre>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-TIT-IB-RICH = -1
        //              MOVE HIGH-VALUES TO TIT-IB-RICH-NULL
        //           END-IF
        if (ws.getIndTitCont().getIbRich() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-IB-RICH-NULL
            ws.getTitCont().setTitIbRich(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitCont.Len.TIT_IB_RICH));
        }
        // COB_CODE: IF IND-TIT-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO TIT-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndTitCont().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-ID-MOVI-CHIU-NULL
            ws.getTitCont().getTitIdMoviChiu().setTitIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitIdMoviChiu.Len.TIT_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-TIT-PROG-TIT = -1
        //              MOVE HIGH-VALUES TO TIT-PROG-TIT-NULL
        //           END-IF
        if (ws.getIndTitCont().getProgTit() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-PROG-TIT-NULL
            ws.getTitCont().getTitProgTit().setTitProgTitNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitProgTit.Len.TIT_PROG_TIT_NULL));
        }
        // COB_CODE: IF IND-TIT-DT-INI-COP = -1
        //              MOVE HIGH-VALUES TO TIT-DT-INI-COP-NULL
        //           END-IF
        if (ws.getIndTitCont().getDtIniCop() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-DT-INI-COP-NULL
            ws.getTitCont().getTitDtIniCop().setTitDtIniCopNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitDtIniCop.Len.TIT_DT_INI_COP_NULL));
        }
        // COB_CODE: IF IND-TIT-DT-END-COP = -1
        //              MOVE HIGH-VALUES TO TIT-DT-END-COP-NULL
        //           END-IF
        if (ws.getIndTitCont().getDtEndCop() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-DT-END-COP-NULL
            ws.getTitCont().getTitDtEndCop().setTitDtEndCopNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitDtEndCop.Len.TIT_DT_END_COP_NULL));
        }
        // COB_CODE: IF IND-TIT-IMP-PAG = -1
        //              MOVE HIGH-VALUES TO TIT-IMP-PAG-NULL
        //           END-IF
        if (ws.getIndTitCont().getImpPag() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-IMP-PAG-NULL
            ws.getTitCont().getTitImpPag().setTitImpPagNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitImpPag.Len.TIT_IMP_PAG_NULL));
        }
        // COB_CODE: IF IND-TIT-FL-SOLL = -1
        //              MOVE HIGH-VALUES TO TIT-FL-SOLL-NULL
        //           END-IF
        if (ws.getIndTitCont().getFlSoll() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-FL-SOLL-NULL
            ws.getTitCont().setTitFlSoll(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-TIT-FRAZ = -1
        //              MOVE HIGH-VALUES TO TIT-FRAZ-NULL
        //           END-IF
        if (ws.getIndTitCont().getFraz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-FRAZ-NULL
            ws.getTitCont().getTitFraz().setTitFrazNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitFraz.Len.TIT_FRAZ_NULL));
        }
        // COB_CODE: IF IND-TIT-DT-APPLZ-MORA = -1
        //              MOVE HIGH-VALUES TO TIT-DT-APPLZ-MORA-NULL
        //           END-IF
        if (ws.getIndTitCont().getDtApplzMora() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-DT-APPLZ-MORA-NULL
            ws.getTitCont().getTitDtApplzMora().setTitDtApplzMoraNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitDtApplzMora.Len.TIT_DT_APPLZ_MORA_NULL));
        }
        // COB_CODE: IF IND-TIT-FL-MORA = -1
        //              MOVE HIGH-VALUES TO TIT-FL-MORA-NULL
        //           END-IF
        if (ws.getIndTitCont().getFlMora() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-FL-MORA-NULL
            ws.getTitCont().setTitFlMora(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-TIT-ID-RAPP-RETE = -1
        //              MOVE HIGH-VALUES TO TIT-ID-RAPP-RETE-NULL
        //           END-IF
        if (ws.getIndTitCont().getIdRappRete() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-ID-RAPP-RETE-NULL
            ws.getTitCont().getTitIdRappRete().setTitIdRappReteNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitIdRappRete.Len.TIT_ID_RAPP_RETE_NULL));
        }
        // COB_CODE: IF IND-TIT-ID-RAPP-ANA = -1
        //              MOVE HIGH-VALUES TO TIT-ID-RAPP-ANA-NULL
        //           END-IF
        if (ws.getIndTitCont().getIdRappAna() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-ID-RAPP-ANA-NULL
            ws.getTitCont().getTitIdRappAna().setTitIdRappAnaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitIdRappAna.Len.TIT_ID_RAPP_ANA_NULL));
        }
        // COB_CODE: IF IND-TIT-COD-DVS = -1
        //              MOVE HIGH-VALUES TO TIT-COD-DVS-NULL
        //           END-IF
        if (ws.getIndTitCont().getCodDvs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-COD-DVS-NULL
            ws.getTitCont().setTitCodDvs(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitCont.Len.TIT_COD_DVS));
        }
        // COB_CODE: IF IND-TIT-DT-EMIS-TIT = -1
        //              MOVE HIGH-VALUES TO TIT-DT-EMIS-TIT-NULL
        //           END-IF
        if (ws.getIndTitCont().getDtEmisTit() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-DT-EMIS-TIT-NULL
            ws.getTitCont().getTitDtEmisTit().setTitDtEmisTitNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitDtEmisTit.Len.TIT_DT_EMIS_TIT_NULL));
        }
        // COB_CODE: IF IND-TIT-DT-ESI-TIT = -1
        //              MOVE HIGH-VALUES TO TIT-DT-ESI-TIT-NULL
        //           END-IF
        if (ws.getIndTitCont().getDtEsiTit() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-DT-ESI-TIT-NULL
            ws.getTitCont().getTitDtEsiTit().setTitDtEsiTitNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitDtEsiTit.Len.TIT_DT_ESI_TIT_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-PRE-NET = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-PRE-NET-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotPreNet() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-PRE-NET-NULL
            ws.getTitCont().getTitTotPreNet().setTitTotPreNetNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotPreNet.Len.TIT_TOT_PRE_NET_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-INTR-FRAZ = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-INTR-FRAZ-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotIntrFraz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-INTR-FRAZ-NULL
            ws.getTitCont().getTitTotIntrFraz().setTitTotIntrFrazNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotIntrFraz.Len.TIT_TOT_INTR_FRAZ_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-INTR-MORA = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-INTR-MORA-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotIntrMora() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-INTR-MORA-NULL
            ws.getTitCont().getTitTotIntrMora().setTitTotIntrMoraNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotIntrMora.Len.TIT_TOT_INTR_MORA_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-INTR-PREST = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-INTR-PREST-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotIntrPrest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-INTR-PREST-NULL
            ws.getTitCont().getTitTotIntrPrest().setTitTotIntrPrestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotIntrPrest.Len.TIT_TOT_INTR_PREST_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-INTR-RETDT = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-INTR-RETDT-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotIntrRetdt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-INTR-RETDT-NULL
            ws.getTitCont().getTitTotIntrRetdt().setTitTotIntrRetdtNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotIntrRetdt.Len.TIT_TOT_INTR_RETDT_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-INTR-RIAT = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-INTR-RIAT-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotIntrRiat() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-INTR-RIAT-NULL
            ws.getTitCont().getTitTotIntrRiat().setTitTotIntrRiatNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotIntrRiat.Len.TIT_TOT_INTR_RIAT_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-DIR = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-DIR-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotDir() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-DIR-NULL
            ws.getTitCont().getTitTotDir().setTitTotDirNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotDir.Len.TIT_TOT_DIR_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-SPE-MED = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-SPE-MED-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotSpeMed() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-SPE-MED-NULL
            ws.getTitCont().getTitTotSpeMed().setTitTotSpeMedNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotSpeMed.Len.TIT_TOT_SPE_MED_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-TAX = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-TAX-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotTax() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-TAX-NULL
            ws.getTitCont().getTitTotTax().setTitTotTaxNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotTax.Len.TIT_TOT_TAX_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-SOPR-SAN = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-SOPR-SAN-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotSoprSan() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-SOPR-SAN-NULL
            ws.getTitCont().getTitTotSoprSan().setTitTotSoprSanNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotSoprSan.Len.TIT_TOT_SOPR_SAN_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-SOPR-TEC = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-SOPR-TEC-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotSoprTec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-SOPR-TEC-NULL
            ws.getTitCont().getTitTotSoprTec().setTitTotSoprTecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotSoprTec.Len.TIT_TOT_SOPR_TEC_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-SOPR-SPO = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-SOPR-SPO-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotSoprSpo() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-SOPR-SPO-NULL
            ws.getTitCont().getTitTotSoprSpo().setTitTotSoprSpoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotSoprSpo.Len.TIT_TOT_SOPR_SPO_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-SOPR-PROF = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-SOPR-PROF-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotSoprProf() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-SOPR-PROF-NULL
            ws.getTitCont().getTitTotSoprProf().setTitTotSoprProfNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotSoprProf.Len.TIT_TOT_SOPR_PROF_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-SOPR-ALT = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-SOPR-ALT-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotSoprAlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-SOPR-ALT-NULL
            ws.getTitCont().getTitTotSoprAlt().setTitTotSoprAltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotSoprAlt.Len.TIT_TOT_SOPR_ALT_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-PRE-TOT = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-PRE-TOT-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotPreTot() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-PRE-TOT-NULL
            ws.getTitCont().getTitTotPreTot().setTitTotPreTotNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotPreTot.Len.TIT_TOT_PRE_TOT_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-PRE-PP-IAS = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-PRE-PP-IAS-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotPrePpIas() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-PRE-PP-IAS-NULL
            ws.getTitCont().getTitTotPrePpIas().setTitTotPrePpIasNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotPrePpIas.Len.TIT_TOT_PRE_PP_IAS_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-CAR-ACQ = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-CAR-ACQ-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotCarAcq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-CAR-ACQ-NULL
            ws.getTitCont().getTitTotCarAcq().setTitTotCarAcqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotCarAcq.Len.TIT_TOT_CAR_ACQ_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-CAR-GEST = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-CAR-GEST-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotCarGest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-CAR-GEST-NULL
            ws.getTitCont().getTitTotCarGest().setTitTotCarGestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotCarGest.Len.TIT_TOT_CAR_GEST_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-CAR-INC = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-CAR-INC-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotCarInc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-CAR-INC-NULL
            ws.getTitCont().getTitTotCarInc().setTitTotCarIncNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotCarInc.Len.TIT_TOT_CAR_INC_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-PRE-SOLO-RSH = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-PRE-SOLO-RSH-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotPreSoloRsh() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-PRE-SOLO-RSH-NULL
            ws.getTitCont().getTitTotPreSoloRsh().setTitTotPreSoloRshNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotPreSoloRsh.Len.TIT_TOT_PRE_SOLO_RSH_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-PROV-ACQ-1AA = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-PROV-ACQ-1AA-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotProvAcq1aa() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-PROV-ACQ-1AA-NULL
            ws.getTitCont().getTitTotProvAcq1aa().setTitTotProvAcq1aaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotProvAcq1aa.Len.TIT_TOT_PROV_ACQ1AA_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-PROV-ACQ-2AA = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-PROV-ACQ-2AA-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotProvAcq2aa() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-PROV-ACQ-2AA-NULL
            ws.getTitCont().getTitTotProvAcq2aa().setTitTotProvAcq2aaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotProvAcq2aa.Len.TIT_TOT_PROV_ACQ2AA_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-PROV-RICOR = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-PROV-RICOR-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotProvRicor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-PROV-RICOR-NULL
            ws.getTitCont().getTitTotProvRicor().setTitTotProvRicorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotProvRicor.Len.TIT_TOT_PROV_RICOR_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-PROV-INC = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-PROV-INC-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotProvInc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-PROV-INC-NULL
            ws.getTitCont().getTitTotProvInc().setTitTotProvIncNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotProvInc.Len.TIT_TOT_PROV_INC_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-PROV-DA-REC = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-PROV-DA-REC-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotProvDaRec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-PROV-DA-REC-NULL
            ws.getTitCont().getTitTotProvDaRec().setTitTotProvDaRecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotProvDaRec.Len.TIT_TOT_PROV_DA_REC_NULL));
        }
        // COB_CODE: IF IND-TIT-IMP-AZ = -1
        //              MOVE HIGH-VALUES TO TIT-IMP-AZ-NULL
        //           END-IF
        if (ws.getIndTitCont().getImpAz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-IMP-AZ-NULL
            ws.getTitCont().getTitImpAz().setTitImpAzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitImpAz.Len.TIT_IMP_AZ_NULL));
        }
        // COB_CODE: IF IND-TIT-IMP-ADER = -1
        //              MOVE HIGH-VALUES TO TIT-IMP-ADER-NULL
        //           END-IF
        if (ws.getIndTitCont().getImpAder() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-IMP-ADER-NULL
            ws.getTitCont().getTitImpAder().setTitImpAderNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitImpAder.Len.TIT_IMP_ADER_NULL));
        }
        // COB_CODE: IF IND-TIT-IMP-TFR = -1
        //              MOVE HIGH-VALUES TO TIT-IMP-TFR-NULL
        //           END-IF
        if (ws.getIndTitCont().getImpTfr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-IMP-TFR-NULL
            ws.getTitCont().getTitImpTfr().setTitImpTfrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitImpTfr.Len.TIT_IMP_TFR_NULL));
        }
        // COB_CODE: IF IND-TIT-IMP-VOLO = -1
        //              MOVE HIGH-VALUES TO TIT-IMP-VOLO-NULL
        //           END-IF
        if (ws.getIndTitCont().getImpVolo() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-IMP-VOLO-NULL
            ws.getTitCont().getTitImpVolo().setTitImpVoloNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitImpVolo.Len.TIT_IMP_VOLO_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-MANFEE-ANTIC = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-MANFEE-ANTIC-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotManfeeAntic() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-MANFEE-ANTIC-NULL
            ws.getTitCont().getTitTotManfeeAntic().setTitTotManfeeAnticNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotManfeeAntic.Len.TIT_TOT_MANFEE_ANTIC_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-MANFEE-RICOR = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-MANFEE-RICOR-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotManfeeRicor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-MANFEE-RICOR-NULL
            ws.getTitCont().getTitTotManfeeRicor().setTitTotManfeeRicorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotManfeeRicor.Len.TIT_TOT_MANFEE_RICOR_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-MANFEE-REC = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-MANFEE-REC-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotManfeeRec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-MANFEE-REC-NULL
            ws.getTitCont().getTitTotManfeeRec().setTitTotManfeeRecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotManfeeRec.Len.TIT_TOT_MANFEE_REC_NULL));
        }
        // COB_CODE: IF IND-TIT-TP-MEZ-PAG-ADD = -1
        //              MOVE HIGH-VALUES TO TIT-TP-MEZ-PAG-ADD-NULL
        //           END-IF
        if (ws.getIndTitCont().getTpMezPagAdd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TP-MEZ-PAG-ADD-NULL
            ws.getTitCont().setTitTpMezPagAdd(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitCont.Len.TIT_TP_MEZ_PAG_ADD));
        }
        // COB_CODE: IF IND-TIT-ESTR-CNT-CORR-ADD = -1
        //              MOVE HIGH-VALUES TO TIT-ESTR-CNT-CORR-ADD-NULL
        //           END-IF
        if (ws.getIndTitCont().getEstrCntCorrAdd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-ESTR-CNT-CORR-ADD-NULL
            ws.getTitCont().setTitEstrCntCorrAdd(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitCont.Len.TIT_ESTR_CNT_CORR_ADD));
        }
        // COB_CODE: IF IND-TIT-DT-VLT = -1
        //              MOVE HIGH-VALUES TO TIT-DT-VLT-NULL
        //           END-IF
        if (ws.getIndTitCont().getDtVlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-DT-VLT-NULL
            ws.getTitCont().getTitDtVlt().setTitDtVltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitDtVlt.Len.TIT_DT_VLT_NULL));
        }
        // COB_CODE: IF IND-TIT-FL-FORZ-DT-VLT = -1
        //              MOVE HIGH-VALUES TO TIT-FL-FORZ-DT-VLT-NULL
        //           END-IF
        if (ws.getIndTitCont().getFlForzDtVlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-FL-FORZ-DT-VLT-NULL
            ws.getTitCont().setTitFlForzDtVlt(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-TIT-DT-CAMBIO-VLT = -1
        //              MOVE HIGH-VALUES TO TIT-DT-CAMBIO-VLT-NULL
        //           END-IF
        if (ws.getIndTitCont().getDtCambioVlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-DT-CAMBIO-VLT-NULL
            ws.getTitCont().getTitDtCambioVlt().setTitDtCambioVltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitDtCambioVlt.Len.TIT_DT_CAMBIO_VLT_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-SPE-AGE = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-SPE-AGE-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotSpeAge() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-SPE-AGE-NULL
            ws.getTitCont().getTitTotSpeAge().setTitTotSpeAgeNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotSpeAge.Len.TIT_TOT_SPE_AGE_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-CAR-IAS = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-CAR-IAS-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotCarIas() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-CAR-IAS-NULL
            ws.getTitCont().getTitTotCarIas().setTitTotCarIasNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotCarIas.Len.TIT_TOT_CAR_IAS_NULL));
        }
        // COB_CODE: IF IND-TIT-NUM-RAT-ACCORPATE = -1
        //              MOVE HIGH-VALUES TO TIT-NUM-RAT-ACCORPATE-NULL
        //           END-IF
        if (ws.getIndTitCont().getNumRatAccorpate() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-NUM-RAT-ACCORPATE-NULL
            ws.getTitCont().getTitNumRatAccorpate().setTitNumRatAccorpateNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitNumRatAccorpate.Len.TIT_NUM_RAT_ACCORPATE_NULL));
        }
        // COB_CODE: IF IND-TIT-FL-TIT-DA-REINVST = -1
        //              MOVE HIGH-VALUES TO TIT-FL-TIT-DA-REINVST-NULL
        //           END-IF
        if (ws.getIndTitCont().getFlTitDaReinvst() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-FL-TIT-DA-REINVST-NULL
            ws.getTitCont().setTitFlTitDaReinvst(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-TIT-DT-RICH-ADD-RID = -1
        //              MOVE HIGH-VALUES TO TIT-DT-RICH-ADD-RID-NULL
        //           END-IF
        if (ws.getIndTitCont().getDtRichAddRid() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-DT-RICH-ADD-RID-NULL
            ws.getTitCont().getTitDtRichAddRid().setTitDtRichAddRidNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitDtRichAddRid.Len.TIT_DT_RICH_ADD_RID_NULL));
        }
        // COB_CODE: IF IND-TIT-TP-ESI-RID = -1
        //              MOVE HIGH-VALUES TO TIT-TP-ESI-RID-NULL
        //           END-IF
        if (ws.getIndTitCont().getTpEsiRid() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TP-ESI-RID-NULL
            ws.getTitCont().setTitTpEsiRid(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitCont.Len.TIT_TP_ESI_RID));
        }
        // COB_CODE: IF IND-TIT-COD-IBAN = -1
        //              MOVE HIGH-VALUES TO TIT-COD-IBAN-NULL
        //           END-IF
        if (ws.getIndTitCont().getCodIban() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-COD-IBAN-NULL
            ws.getTitCont().setTitCodIban(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitCont.Len.TIT_COD_IBAN));
        }
        // COB_CODE: IF IND-TIT-IMP-TRASFE = -1
        //              MOVE HIGH-VALUES TO TIT-IMP-TRASFE-NULL
        //           END-IF
        if (ws.getIndTitCont().getImpTrasfe() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-IMP-TRASFE-NULL
            ws.getTitCont().getTitImpTrasfe().setTitImpTrasfeNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitImpTrasfe.Len.TIT_IMP_TRASFE_NULL));
        }
        // COB_CODE: IF IND-TIT-IMP-TFR-STRC = -1
        //              MOVE HIGH-VALUES TO TIT-IMP-TFR-STRC-NULL
        //           END-IF
        if (ws.getIndTitCont().getImpTfrStrc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-IMP-TFR-STRC-NULL
            ws.getTitCont().getTitImpTfrStrc().setTitImpTfrStrcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitImpTfrStrc.Len.TIT_IMP_TFR_STRC_NULL));
        }
        // COB_CODE: IF IND-TIT-DT-CERT-FISC = -1
        //              MOVE HIGH-VALUES TO TIT-DT-CERT-FISC-NULL
        //           END-IF
        if (ws.getIndTitCont().getDtCertFisc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-DT-CERT-FISC-NULL
            ws.getTitCont().getTitDtCertFisc().setTitDtCertFiscNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitDtCertFisc.Len.TIT_DT_CERT_FISC_NULL));
        }
        // COB_CODE: IF IND-TIT-TP-CAUS-STOR = -1
        //              MOVE HIGH-VALUES TO TIT-TP-CAUS-STOR-NULL
        //           END-IF
        if (ws.getIndTitCont().getTpCausStor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TP-CAUS-STOR-NULL
            ws.getTitCont().getTitTpCausStor().setTitTpCausStorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTpCausStor.Len.TIT_TP_CAUS_STOR_NULL));
        }
        // COB_CODE: IF IND-TIT-TP-CAUS-DISP-STOR = -1
        //              MOVE HIGH-VALUES TO TIT-TP-CAUS-DISP-STOR-NULL
        //           END-IF
        if (ws.getIndTitCont().getTpCausDispStor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TP-CAUS-DISP-STOR-NULL
            ws.getTitCont().setTitTpCausDispStor(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitCont.Len.TIT_TP_CAUS_DISP_STOR));
        }
        // COB_CODE: IF IND-TIT-TP-TIT-MIGRAZ = -1
        //              MOVE HIGH-VALUES TO TIT-TP-TIT-MIGRAZ-NULL
        //           END-IF.
        if (ws.getIndTitCont().getTpTitMigraz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TP-TIT-MIGRAZ-NULL
            ws.getTitCont().setTitTpTitMigraz(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitCont.Len.TIT_TP_TIT_MIGRAZ));
        }
        // COB_CODE: IF IND-TIT-TOT-ACQ-EXP = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-ACQ-EXP-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotAcqExp() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-ACQ-EXP-NULL
            ws.getTitCont().getTitTotAcqExp().setTitTotAcqExpNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotAcqExp.Len.TIT_TOT_ACQ_EXP_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-REMUN-ASS = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-REMUN-ASS-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotRemunAss() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-REMUN-ASS-NULL
            ws.getTitCont().getTitTotRemunAss().setTitTotRemunAssNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotRemunAss.Len.TIT_TOT_REMUN_ASS_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-COMMIS-INTER = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-COMMIS-INTER-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotCommisInter() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-COMMIS-INTER-NULL
            ws.getTitCont().getTitTotCommisInter().setTitTotCommisInterNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotCommisInter.Len.TIT_TOT_COMMIS_INTER_NULL));
        }
        // COB_CODE: IF IND-TIT-TP-CAUS-RIMB = -1
        //              MOVE HIGH-VALUES TO TIT-TP-CAUS-RIMB-NULL
        //           END-IF
        if (ws.getIndTitCont().getTpCausRimb() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TP-CAUS-RIMB-NULL
            ws.getTitCont().setTitTpCausRimb(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitCont.Len.TIT_TP_CAUS_RIMB));
        }
        // COB_CODE: IF IND-TIT-TOT-CNBT-ANTIRAC = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-CNBT-ANTIRAC-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotCnbtAntirac() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-CNBT-ANTIRAC-NULL
            ws.getTitCont().getTitTotCnbtAntirac().setTitTotCnbtAntiracNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotCnbtAntirac.Len.TIT_TOT_CNBT_ANTIRAC_NULL));
        }
        // COB_CODE: IF IND-TIT-FL-INC-AUTOGEN = -1
        //              MOVE HIGH-VALUES TO TIT-FL-INC-AUTOGEN-NULL
        //           END-IF
        if (ws.getIndTitCont().getFlIncAutogen() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-FL-INC-AUTOGEN-NULL
            ws.getTitCont().setTitFlIncAutogen(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-DTC-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO DTC-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-ID-MOVI-CHIU-NULL
            ws.getDettTitCont().getDtcIdMoviChiu().setDtcIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcIdMoviChiu.Len.DTC_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-DTC-DT-INI-COP = -1
        //              MOVE HIGH-VALUES TO DTC-DT-INI-COP-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getDtIniCop() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-DT-INI-COP-NULL
            ws.getDettTitCont().getDtcDtIniCop().setDtcDtIniCopNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcDtIniCop.Len.DTC_DT_INI_COP_NULL));
        }
        // COB_CODE: IF IND-DTC-DT-END-COP = -1
        //              MOVE HIGH-VALUES TO DTC-DT-END-COP-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getDtEndCop() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-DT-END-COP-NULL
            ws.getDettTitCont().getDtcDtEndCop().setDtcDtEndCopNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcDtEndCop.Len.DTC_DT_END_COP_NULL));
        }
        // COB_CODE: IF IND-DTC-PRE-NET = -1
        //              MOVE HIGH-VALUES TO DTC-PRE-NET-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getPreNet() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-PRE-NET-NULL
            ws.getDettTitCont().getDtcPreNet().setDtcPreNetNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcPreNet.Len.DTC_PRE_NET_NULL));
        }
        // COB_CODE: IF IND-DTC-INTR-FRAZ = -1
        //              MOVE HIGH-VALUES TO DTC-INTR-FRAZ-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getIntrFraz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-INTR-FRAZ-NULL
            ws.getDettTitCont().getDtcIntrFraz().setDtcIntrFrazNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcIntrFraz.Len.DTC_INTR_FRAZ_NULL));
        }
        // COB_CODE: IF IND-DTC-INTR-MORA = -1
        //              MOVE HIGH-VALUES TO DTC-INTR-MORA-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getIntrMora() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-INTR-MORA-NULL
            ws.getDettTitCont().getDtcIntrMora().setDtcIntrMoraNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcIntrMora.Len.DTC_INTR_MORA_NULL));
        }
        // COB_CODE: IF IND-DTC-INTR-RETDT = -1
        //              MOVE HIGH-VALUES TO DTC-INTR-RETDT-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getIntrRetdt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-INTR-RETDT-NULL
            ws.getDettTitCont().getDtcIntrRetdt().setDtcIntrRetdtNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcIntrRetdt.Len.DTC_INTR_RETDT_NULL));
        }
        // COB_CODE: IF IND-DTC-INTR-RIAT = -1
        //              MOVE HIGH-VALUES TO DTC-INTR-RIAT-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getIntrRiat() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-INTR-RIAT-NULL
            ws.getDettTitCont().getDtcIntrRiat().setDtcIntrRiatNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcIntrRiat.Len.DTC_INTR_RIAT_NULL));
        }
        // COB_CODE: IF IND-DTC-DIR = -1
        //              MOVE HIGH-VALUES TO DTC-DIR-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getDir() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-DIR-NULL
            ws.getDettTitCont().getDtcDir().setDtcDirNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcDir.Len.DTC_DIR_NULL));
        }
        // COB_CODE: IF IND-DTC-SPE-MED = -1
        //              MOVE HIGH-VALUES TO DTC-SPE-MED-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getSpeMed() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-SPE-MED-NULL
            ws.getDettTitCont().getDtcSpeMed().setDtcSpeMedNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcSpeMed.Len.DTC_SPE_MED_NULL));
        }
        // COB_CODE: IF IND-DTC-TAX = -1
        //              MOVE HIGH-VALUES TO DTC-TAX-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getTax() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-TAX-NULL
            ws.getDettTitCont().getDtcTax().setDtcTaxNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcTax.Len.DTC_TAX_NULL));
        }
        // COB_CODE: IF IND-DTC-SOPR-SAN = -1
        //              MOVE HIGH-VALUES TO DTC-SOPR-SAN-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getSoprSan() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-SOPR-SAN-NULL
            ws.getDettTitCont().getDtcSoprSan().setDtcSoprSanNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcSoprSan.Len.DTC_SOPR_SAN_NULL));
        }
        // COB_CODE: IF IND-DTC-SOPR-SPO = -1
        //              MOVE HIGH-VALUES TO DTC-SOPR-SPO-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getSoprSpo() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-SOPR-SPO-NULL
            ws.getDettTitCont().getDtcSoprSpo().setDtcSoprSpoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcSoprSpo.Len.DTC_SOPR_SPO_NULL));
        }
        // COB_CODE: IF IND-DTC-SOPR-TEC = -1
        //              MOVE HIGH-VALUES TO DTC-SOPR-TEC-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getSoprTec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-SOPR-TEC-NULL
            ws.getDettTitCont().getDtcSoprTec().setDtcSoprTecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcSoprTec.Len.DTC_SOPR_TEC_NULL));
        }
        // COB_CODE: IF IND-DTC-SOPR-PROF = -1
        //              MOVE HIGH-VALUES TO DTC-SOPR-PROF-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getSoprProf() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-SOPR-PROF-NULL
            ws.getDettTitCont().getDtcSoprProf().setDtcSoprProfNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcSoprProf.Len.DTC_SOPR_PROF_NULL));
        }
        // COB_CODE: IF IND-DTC-SOPR-ALT = -1
        //              MOVE HIGH-VALUES TO DTC-SOPR-ALT-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getSoprAlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-SOPR-ALT-NULL
            ws.getDettTitCont().getDtcSoprAlt().setDtcSoprAltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcSoprAlt.Len.DTC_SOPR_ALT_NULL));
        }
        // COB_CODE: IF IND-DTC-PRE-TOT = -1
        //              MOVE HIGH-VALUES TO DTC-PRE-TOT-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getPreTot() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-PRE-TOT-NULL
            ws.getDettTitCont().getDtcPreTot().setDtcPreTotNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcPreTot.Len.DTC_PRE_TOT_NULL));
        }
        // COB_CODE: IF IND-DTC-PRE-PP-IAS = -1
        //              MOVE HIGH-VALUES TO DTC-PRE-PP-IAS-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getPrePpIas() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-PRE-PP-IAS-NULL
            ws.getDettTitCont().getDtcPrePpIas().setDtcPrePpIasNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcPrePpIas.Len.DTC_PRE_PP_IAS_NULL));
        }
        // COB_CODE: IF IND-DTC-PRE-SOLO-RSH = -1
        //              MOVE HIGH-VALUES TO DTC-PRE-SOLO-RSH-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getPreSoloRsh() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-PRE-SOLO-RSH-NULL
            ws.getDettTitCont().getDtcPreSoloRsh().setDtcPreSoloRshNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcPreSoloRsh.Len.DTC_PRE_SOLO_RSH_NULL));
        }
        // COB_CODE: IF IND-DTC-CAR-ACQ = -1
        //              MOVE HIGH-VALUES TO DTC-CAR-ACQ-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getCarAcq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-CAR-ACQ-NULL
            ws.getDettTitCont().getDtcCarAcq().setDtcCarAcqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcCarAcq.Len.DTC_CAR_ACQ_NULL));
        }
        // COB_CODE: IF IND-DTC-CAR-GEST = -1
        //              MOVE HIGH-VALUES TO DTC-CAR-GEST-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getCarGest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-CAR-GEST-NULL
            ws.getDettTitCont().getDtcCarGest().setDtcCarGestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcCarGest.Len.DTC_CAR_GEST_NULL));
        }
        // COB_CODE: IF IND-DTC-CAR-INC = -1
        //              MOVE HIGH-VALUES TO DTC-CAR-INC-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getCarInc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-CAR-INC-NULL
            ws.getDettTitCont().getDtcCarInc().setDtcCarIncNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcCarInc.Len.DTC_CAR_INC_NULL));
        }
        // COB_CODE: IF IND-DTC-PROV-ACQ-1AA = -1
        //              MOVE HIGH-VALUES TO DTC-PROV-ACQ-1AA-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getProvAcq1aa() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-PROV-ACQ-1AA-NULL
            ws.getDettTitCont().getDtcProvAcq1aa().setDtcProvAcq1aaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcProvAcq1aa.Len.DTC_PROV_ACQ1AA_NULL));
        }
        // COB_CODE: IF IND-DTC-PROV-ACQ-2AA = -1
        //              MOVE HIGH-VALUES TO DTC-PROV-ACQ-2AA-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getProvAcq2aa() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-PROV-ACQ-2AA-NULL
            ws.getDettTitCont().getDtcProvAcq2aa().setDtcProvAcq2aaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcProvAcq2aa.Len.DTC_PROV_ACQ2AA_NULL));
        }
        // COB_CODE: IF IND-DTC-PROV-RICOR = -1
        //              MOVE HIGH-VALUES TO DTC-PROV-RICOR-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getProvRicor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-PROV-RICOR-NULL
            ws.getDettTitCont().getDtcProvRicor().setDtcProvRicorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcProvRicor.Len.DTC_PROV_RICOR_NULL));
        }
        // COB_CODE: IF IND-DTC-PROV-INC = -1
        //              MOVE HIGH-VALUES TO DTC-PROV-INC-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getProvInc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-PROV-INC-NULL
            ws.getDettTitCont().getDtcProvInc().setDtcProvIncNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcProvInc.Len.DTC_PROV_INC_NULL));
        }
        // COB_CODE: IF IND-DTC-PROV-DA-REC = -1
        //              MOVE HIGH-VALUES TO DTC-PROV-DA-REC-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getProvDaRec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-PROV-DA-REC-NULL
            ws.getDettTitCont().getDtcProvDaRec().setDtcProvDaRecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcProvDaRec.Len.DTC_PROV_DA_REC_NULL));
        }
        // COB_CODE: IF IND-DTC-COD-DVS = -1
        //              MOVE HIGH-VALUES TO DTC-COD-DVS-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getCodDvs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-COD-DVS-NULL
            ws.getDettTitCont().setDtcCodDvs(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DettTitCont.Len.DTC_COD_DVS));
        }
        // COB_CODE: IF IND-DTC-FRQ-MOVI = -1
        //              MOVE HIGH-VALUES TO DTC-FRQ-MOVI-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getFrqMovi() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-FRQ-MOVI-NULL
            ws.getDettTitCont().getDtcFrqMovi().setDtcFrqMoviNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcFrqMovi.Len.DTC_FRQ_MOVI_NULL));
        }
        // COB_CODE: IF IND-DTC-COD-TARI = -1
        //              MOVE HIGH-VALUES TO DTC-COD-TARI-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getCodTari() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-COD-TARI-NULL
            ws.getDettTitCont().setDtcCodTari(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DettTitCont.Len.DTC_COD_TARI));
        }
        // COB_CODE: IF IND-DTC-IMP-AZ = -1
        //              MOVE HIGH-VALUES TO DTC-IMP-AZ-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getImpAz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-IMP-AZ-NULL
            ws.getDettTitCont().getDtcImpAz().setDtcImpAzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcImpAz.Len.DTC_IMP_AZ_NULL));
        }
        // COB_CODE: IF IND-DTC-IMP-ADER = -1
        //              MOVE HIGH-VALUES TO DTC-IMP-ADER-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getImpAder() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-IMP-ADER-NULL
            ws.getDettTitCont().getDtcImpAder().setDtcImpAderNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcImpAder.Len.DTC_IMP_ADER_NULL));
        }
        // COB_CODE: IF IND-DTC-IMP-TFR = -1
        //              MOVE HIGH-VALUES TO DTC-IMP-TFR-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getImpTfr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-IMP-TFR-NULL
            ws.getDettTitCont().getDtcImpTfr().setDtcImpTfrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcImpTfr.Len.DTC_IMP_TFR_NULL));
        }
        // COB_CODE: IF IND-DTC-IMP-VOLO = -1
        //              MOVE HIGH-VALUES TO DTC-IMP-VOLO-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getImpVolo() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-IMP-VOLO-NULL
            ws.getDettTitCont().getDtcImpVolo().setDtcImpVoloNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcImpVolo.Len.DTC_IMP_VOLO_NULL));
        }
        // COB_CODE: IF IND-DTC-MANFEE-ANTIC = -1
        //              MOVE HIGH-VALUES TO DTC-MANFEE-ANTIC-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getManfeeAntic() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-MANFEE-ANTIC-NULL
            ws.getDettTitCont().getDtcManfeeAntic().setDtcManfeeAnticNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcManfeeAntic.Len.DTC_MANFEE_ANTIC_NULL));
        }
        // COB_CODE: IF IND-DTC-MANFEE-RICOR = -1
        //              MOVE HIGH-VALUES TO DTC-MANFEE-RICOR-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getManfeeRicor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-MANFEE-RICOR-NULL
            ws.getDettTitCont().getDtcManfeeRicor().setDtcManfeeRicorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcManfeeRicor.Len.DTC_MANFEE_RICOR_NULL));
        }
        // COB_CODE: IF IND-DTC-MANFEE-REC = -1
        //              MOVE HIGH-VALUES TO DTC-MANFEE-REC-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getManfeeRec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-MANFEE-REC-NULL
            ws.getDettTitCont().getDtcManfeeRec().setDtcManfeeRecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcManfeeRec.Len.DTC_MANFEE_REC_NULL));
        }
        // COB_CODE: IF IND-DTC-DT-ESI-TIT = -1
        //              MOVE HIGH-VALUES TO DTC-DT-ESI-TIT-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getDtEsiTit() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-DT-ESI-TIT-NULL
            ws.getDettTitCont().getDtcDtEsiTit().setDtcDtEsiTitNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcDtEsiTit.Len.DTC_DT_ESI_TIT_NULL));
        }
        // COB_CODE: IF IND-DTC-SPE-AGE = -1
        //              MOVE HIGH-VALUES TO DTC-SPE-AGE-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getSpeAge() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-SPE-AGE-NULL
            ws.getDettTitCont().getDtcSpeAge().setDtcSpeAgeNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcSpeAge.Len.DTC_SPE_AGE_NULL));
        }
        // COB_CODE: IF IND-DTC-CAR-IAS = -1
        //              MOVE HIGH-VALUES TO DTC-CAR-IAS-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getCarIas() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-CAR-IAS-NULL
            ws.getDettTitCont().getDtcCarIas().setDtcCarIasNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcCarIas.Len.DTC_CAR_IAS_NULL));
        }
        // COB_CODE: IF IND-DTC-TOT-INTR-PREST = -1
        //              MOVE HIGH-VALUES TO DTC-TOT-INTR-PREST-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getTotIntrPrest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-TOT-INTR-PREST-NULL
            ws.getDettTitCont().getDtcTotIntrPrest().setDtcTotIntrPrestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcTotIntrPrest.Len.DTC_TOT_INTR_PREST_NULL));
        }
        // COB_CODE: IF IND-DTC-IMP-TRASFE = -1
        //              MOVE HIGH-VALUES TO DTC-IMP-TRASFE-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getImpTrasfe() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-IMP-TRASFE-NULL
            ws.getDettTitCont().getDtcImpTrasfe().setDtcImpTrasfeNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcImpTrasfe.Len.DTC_IMP_TRASFE_NULL));
        }
        // COB_CODE: IF IND-DTC-IMP-TFR-STRC = -1
        //              MOVE HIGH-VALUES TO DTC-IMP-TFR-STRC-NULL
        //           END-IF.
        if (ws.getIndDettTitCont().getImpTfrStrc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-IMP-TFR-STRC-NULL
            ws.getDettTitCont().getDtcImpTfrStrc().setDtcImpTfrStrcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcImpTfrStrc.Len.DTC_IMP_TFR_STRC_NULL));
        }
        // COB_CODE: IF IND-DTC-NUM-GG-RITARDO-PAG = -1
        //              MOVE HIGH-VALUES TO DTC-NUM-GG-RITARDO-PAG-NULL
        //           END-IF
        if (ws.getIndDettTitCont().getNumGgRitardoPag() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-NUM-GG-RITARDO-PAG-NULL
            ws.getDettTitCont().getDtcNumGgRitardoPag().setDtcNumGgRitardoPagNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcNumGgRitardoPag.Len.DTC_NUM_GG_RITARDO_PAG_NULL));
        }
        // COB_CODE: IF IND-DTC-NUM-GG-RIVAL = -1
        //              MOVE HIGH-VALUES TO DTC-NUM-GG-RIVAL-NULL
        //           END-IF.
        if (ws.getIndDettTitCont().getNumGgRival() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-NUM-GG-RIVAL-NULL
            ws.getDettTitCont().getDtcNumGgRival().setDtcNumGgRivalNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcNumGgRival.Len.DTC_NUM_GG_RIVAL_NULL));
        }
        // COB_CODE: IF IND-DTC-ACQ-EXP = -1
        //               MOVE HIGH-VALUES TO DTC-ACQ-EXP-NULL
        //           END-IF.
        if (ws.getIndDettTitCont().getAcqExp() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-ACQ-EXP-NULL
            ws.getDettTitCont().getDtcAcqExp().setDtcAcqExpNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcAcqExp.Len.DTC_ACQ_EXP_NULL));
        }
        // COB_CODE: IF IND-DTC-REMUN-ASS = -1
        //              MOVE HIGH-VALUES TO DTC-REMUN-ASS-NULL
        //           END-IF.
        if (ws.getIndDettTitCont().getRemunAss() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-REMUN-ASS-NULL
            ws.getDettTitCont().getDtcRemunAss().setDtcRemunAssNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcRemunAss.Len.DTC_REMUN_ASS_NULL));
        }
        // COB_CODE: IF IND-DTC-COMMIS-INTER = -1
        //              MOVE HIGH-VALUES TO DTC-COMMIS-INTER-NULL
        //           END-IF.
        if (ws.getIndDettTitCont().getCommisInter() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-COMMIS-INTER-NULL
            ws.getDettTitCont().getDtcCommisInter().setDtcCommisInterNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcCommisInter.Len.DTC_COMMIS_INTER_NULL));
        }
        // COB_CODE: IF IND-DTC-CNBT-ANTIRAC = -1
        //              MOVE HIGH-VALUES TO DTC-CNBT-ANTIRAC-NULL
        //           END-IF.
        if (ws.getIndDettTitCont().getCnbtAntirac() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTC-CNBT-ANTIRAC-NULL
            ws.getDettTitCont().getDtcCnbtAntirac().setDtcCnbtAntiracNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtcCnbtAntirac.Len.DTC_CNBT_ANTIRAC_NULL));
        }
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z970-CODICE-ADHOC-PRE<br>
	 * <pre>----
	 * ----  prevede statements AD HOC PRE Query
	 * ----</pre>*/
    private void z970CodiceAdhocPre() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z980-CODICE-ADHOC-POST<br>
	 * <pre>----
	 * ----  prevede statements AD HOC POST Query
	 * ----</pre>*/
    private void z980CodiceAdhocPost() {
        // COB_CODE: MOVE TIT-ID-TIT-CONT          TO LDBV0371-ID-TIT-CONT
        ldbv0371.setLdbv0371IdTitCont(ws.getTitCont().getTitIdTitCont());
        // COB_CODE: MOVE DTC-ID-DETT-TIT-CONT     TO LDBV0371-ID-DETT-TIT-CONT
        ldbv0371.setLdbv0371IdDettTitCont(ws.getDettTitCont().getDtcIdDettTitCont());
        // COB_CODE: IF TIT-FRAZ-NULL = HIGH-VALUE
        //              MOVE TIT-FRAZ-NULL         TO LDBV0371-FRAZ-NULL
        //           ELSE
        //              MOVE TIT-FRAZ              TO LDBV0371-FRAZ
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitFraz().getTitFrazNullFormatted())) {
            // COB_CODE: MOVE TIT-FRAZ-NULL         TO LDBV0371-FRAZ-NULL
            ldbv0371.getLdbv0371Fraz().setLdbv0371FrazNull(ws.getTitCont().getTitFraz().getTitFrazNull());
        }
        else {
            // COB_CODE: MOVE TIT-FRAZ              TO LDBV0371-FRAZ
            ldbv0371.getLdbv0371Fraz().setLdbv0371Fraz(ws.getTitCont().getTitFraz().getTitFraz());
        }
        // COB_CODE: IF TIT-TOT-PRE-TOT-NULL = HIGH-VALUE
        //              MOVE TIT-TOT-PRE-TOT-NULL  TO LDBV0371-TIT-PRE-TOT-NULL
        //           ELSE
        //              MOVE TIT-TOT-PRE-TOT       TO LDBV0371-TIT-PRE-TOT
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotPreTot().getTitTotPreTotNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-PRE-TOT-NULL  TO LDBV0371-TIT-PRE-TOT-NULL
            ldbv0371.getLdbv0371TitPreTot().setLdbv0371TitPreTotNull(ws.getTitCont().getTitTotPreTot().getTitTotPreTotNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-PRE-TOT       TO LDBV0371-TIT-PRE-TOT
            ldbv0371.getLdbv0371TitPreTot().setLdbv0371TitPreTot(Trunc.toDecimal(ws.getTitCont().getTitTotPreTot().getTitTotPreTot(), 15, 3));
        }
        // COB_CODE: IF DTC-PRE-TOT-NULL = HIGH-VALUE
        //              MOVE DTC-PRE-TOT-NULL      TO LDBV0371-DTC-PRE-TOT-NULL
        //           ELSE
        //              MOVE DTC-PRE-TOT           TO LDBV0371-DTC-PRE-TOT
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getDettTitCont().getDtcPreTot().getDtcPreTotNullFormatted())) {
            // COB_CODE: MOVE DTC-PRE-TOT-NULL      TO LDBV0371-DTC-PRE-TOT-NULL
            ldbv0371.getLdbv0371DtcPreTot().setLdbv0371DtcPreTotNull(ws.getDettTitCont().getDtcPreTot().getDtcPreTotNull());
        }
        else {
            // COB_CODE: MOVE DTC-PRE-TOT           TO LDBV0371-DTC-PRE-TOT
            ldbv0371.getLdbv0371DtcPreTot().setLdbv0371DtcPreTot(Trunc.toDecimal(ws.getDettTitCont().getDtcPreTot().getDtcPreTot(), 15, 3));
        }
        // COB_CODE: IF DTC-DIR-NULL = HIGH-VALUE
        //              MOVE DTC-DIR-NULL      TO LDBV0371-DTC-DIR-NULL
        //           ELSE
        //              MOVE DTC-DIR           TO LDBV0371-DTC-DIR
        //           END-IF.
        if (Characters.EQ_HIGH.test(ws.getDettTitCont().getDtcDir().getDtcDirNullFormatted())) {
            // COB_CODE: MOVE DTC-DIR-NULL      TO LDBV0371-DTC-DIR-NULL
            ldbv0371.getLdbv0371DtcDir().setLdbv0371DtcDirNull(ws.getDettTitCont().getDtcDir().getDtcDirNull());
        }
        else {
            // COB_CODE: MOVE DTC-DIR           TO LDBV0371-DTC-DIR
            ldbv0371.getLdbv0371DtcDir().setLdbv0371DtcDir(Trunc.toDecimal(ws.getDettTitCont().getDtcDir().getDtcDir(), 15, 3));
        }
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    @Override
    public AfDecimal getDtcAcqExp() {
        return ws.getDettTitCont().getDtcAcqExp().getDtcAcqExp();
    }

    @Override
    public void setDtcAcqExp(AfDecimal dtcAcqExp) {
        this.ws.getDettTitCont().getDtcAcqExp().setDtcAcqExp(dtcAcqExp.copy());
    }

    @Override
    public AfDecimal getDtcAcqExpObj() {
        if (ws.getIndDettTitCont().getAcqExp() >= 0) {
            return getDtcAcqExp();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcAcqExpObj(AfDecimal dtcAcqExpObj) {
        if (dtcAcqExpObj != null) {
            setDtcAcqExp(new AfDecimal(dtcAcqExpObj, 15, 3));
            ws.getIndDettTitCont().setAcqExp(((short)0));
        }
        else {
            ws.getIndDettTitCont().setAcqExp(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcCarAcq() {
        return ws.getDettTitCont().getDtcCarAcq().getDtcCarAcq();
    }

    @Override
    public void setDtcCarAcq(AfDecimal dtcCarAcq) {
        this.ws.getDettTitCont().getDtcCarAcq().setDtcCarAcq(dtcCarAcq.copy());
    }

    @Override
    public AfDecimal getDtcCarAcqObj() {
        if (ws.getIndDettTitCont().getCarAcq() >= 0) {
            return getDtcCarAcq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcCarAcqObj(AfDecimal dtcCarAcqObj) {
        if (dtcCarAcqObj != null) {
            setDtcCarAcq(new AfDecimal(dtcCarAcqObj, 15, 3));
            ws.getIndDettTitCont().setCarAcq(((short)0));
        }
        else {
            ws.getIndDettTitCont().setCarAcq(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcCarGest() {
        return ws.getDettTitCont().getDtcCarGest().getDtcCarGest();
    }

    @Override
    public void setDtcCarGest(AfDecimal dtcCarGest) {
        this.ws.getDettTitCont().getDtcCarGest().setDtcCarGest(dtcCarGest.copy());
    }

    @Override
    public AfDecimal getDtcCarGestObj() {
        if (ws.getIndDettTitCont().getCarGest() >= 0) {
            return getDtcCarGest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcCarGestObj(AfDecimal dtcCarGestObj) {
        if (dtcCarGestObj != null) {
            setDtcCarGest(new AfDecimal(dtcCarGestObj, 15, 3));
            ws.getIndDettTitCont().setCarGest(((short)0));
        }
        else {
            ws.getIndDettTitCont().setCarGest(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcCarIas() {
        return ws.getDettTitCont().getDtcCarIas().getDtcCarIas();
    }

    @Override
    public void setDtcCarIas(AfDecimal dtcCarIas) {
        this.ws.getDettTitCont().getDtcCarIas().setDtcCarIas(dtcCarIas.copy());
    }

    @Override
    public AfDecimal getDtcCarIasObj() {
        if (ws.getIndDettTitCont().getCarIas() >= 0) {
            return getDtcCarIas();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcCarIasObj(AfDecimal dtcCarIasObj) {
        if (dtcCarIasObj != null) {
            setDtcCarIas(new AfDecimal(dtcCarIasObj, 15, 3));
            ws.getIndDettTitCont().setCarIas(((short)0));
        }
        else {
            ws.getIndDettTitCont().setCarIas(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcCarInc() {
        return ws.getDettTitCont().getDtcCarInc().getDtcCarInc();
    }

    @Override
    public void setDtcCarInc(AfDecimal dtcCarInc) {
        this.ws.getDettTitCont().getDtcCarInc().setDtcCarInc(dtcCarInc.copy());
    }

    @Override
    public AfDecimal getDtcCarIncObj() {
        if (ws.getIndDettTitCont().getCarInc() >= 0) {
            return getDtcCarInc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcCarIncObj(AfDecimal dtcCarIncObj) {
        if (dtcCarIncObj != null) {
            setDtcCarInc(new AfDecimal(dtcCarIncObj, 15, 3));
            ws.getIndDettTitCont().setCarInc(((short)0));
        }
        else {
            ws.getIndDettTitCont().setCarInc(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcCnbtAntirac() {
        return ws.getDettTitCont().getDtcCnbtAntirac().getDtcCnbtAntirac();
    }

    @Override
    public void setDtcCnbtAntirac(AfDecimal dtcCnbtAntirac) {
        this.ws.getDettTitCont().getDtcCnbtAntirac().setDtcCnbtAntirac(dtcCnbtAntirac.copy());
    }

    @Override
    public AfDecimal getDtcCnbtAntiracObj() {
        if (ws.getIndDettTitCont().getCnbtAntirac() >= 0) {
            return getDtcCnbtAntirac();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcCnbtAntiracObj(AfDecimal dtcCnbtAntiracObj) {
        if (dtcCnbtAntiracObj != null) {
            setDtcCnbtAntirac(new AfDecimal(dtcCnbtAntiracObj, 15, 3));
            ws.getIndDettTitCont().setCnbtAntirac(((short)0));
        }
        else {
            ws.getIndDettTitCont().setCnbtAntirac(((short)-1));
        }
    }

    @Override
    public int getDtcCodCompAnia() {
        return ws.getDettTitCont().getDtcCodCompAnia();
    }

    @Override
    public void setDtcCodCompAnia(int dtcCodCompAnia) {
        this.ws.getDettTitCont().setDtcCodCompAnia(dtcCodCompAnia);
    }

    @Override
    public String getDtcCodDvs() {
        return ws.getDettTitCont().getDtcCodDvs();
    }

    @Override
    public void setDtcCodDvs(String dtcCodDvs) {
        this.ws.getDettTitCont().setDtcCodDvs(dtcCodDvs);
    }

    @Override
    public String getDtcCodDvsObj() {
        if (ws.getIndDettTitCont().getCodDvs() >= 0) {
            return getDtcCodDvs();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcCodDvsObj(String dtcCodDvsObj) {
        if (dtcCodDvsObj != null) {
            setDtcCodDvs(dtcCodDvsObj);
            ws.getIndDettTitCont().setCodDvs(((short)0));
        }
        else {
            ws.getIndDettTitCont().setCodDvs(((short)-1));
        }
    }

    @Override
    public String getDtcCodTari() {
        return ws.getDettTitCont().getDtcCodTari();
    }

    @Override
    public void setDtcCodTari(String dtcCodTari) {
        this.ws.getDettTitCont().setDtcCodTari(dtcCodTari);
    }

    @Override
    public String getDtcCodTariObj() {
        if (ws.getIndDettTitCont().getCodTari() >= 0) {
            return getDtcCodTari();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcCodTariObj(String dtcCodTariObj) {
        if (dtcCodTariObj != null) {
            setDtcCodTari(dtcCodTariObj);
            ws.getIndDettTitCont().setCodTari(((short)0));
        }
        else {
            ws.getIndDettTitCont().setCodTari(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcCommisInter() {
        return ws.getDettTitCont().getDtcCommisInter().getDtcCommisInter();
    }

    @Override
    public void setDtcCommisInter(AfDecimal dtcCommisInter) {
        this.ws.getDettTitCont().getDtcCommisInter().setDtcCommisInter(dtcCommisInter.copy());
    }

    @Override
    public AfDecimal getDtcCommisInterObj() {
        if (ws.getIndDettTitCont().getCommisInter() >= 0) {
            return getDtcCommisInter();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcCommisInterObj(AfDecimal dtcCommisInterObj) {
        if (dtcCommisInterObj != null) {
            setDtcCommisInter(new AfDecimal(dtcCommisInterObj, 15, 3));
            ws.getIndDettTitCont().setCommisInter(((short)0));
        }
        else {
            ws.getIndDettTitCont().setCommisInter(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcDir() {
        return ws.getDettTitCont().getDtcDir().getDtcDir();
    }

    @Override
    public void setDtcDir(AfDecimal dtcDir) {
        this.ws.getDettTitCont().getDtcDir().setDtcDir(dtcDir.copy());
    }

    @Override
    public AfDecimal getDtcDirObj() {
        if (ws.getIndDettTitCont().getDir() >= 0) {
            return getDtcDir();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcDirObj(AfDecimal dtcDirObj) {
        if (dtcDirObj != null) {
            setDtcDir(new AfDecimal(dtcDirObj, 15, 3));
            ws.getIndDettTitCont().setDir(((short)0));
        }
        else {
            ws.getIndDettTitCont().setDir(((short)-1));
        }
    }

    @Override
    public char getDtcDsOperSql() {
        return ws.getDettTitCont().getDtcDsOperSql();
    }

    @Override
    public void setDtcDsOperSql(char dtcDsOperSql) {
        this.ws.getDettTitCont().setDtcDsOperSql(dtcDsOperSql);
    }

    @Override
    public long getDtcDsRiga() {
        return ws.getDettTitCont().getDtcDsRiga();
    }

    @Override
    public void setDtcDsRiga(long dtcDsRiga) {
        this.ws.getDettTitCont().setDtcDsRiga(dtcDsRiga);
    }

    @Override
    public char getDtcDsStatoElab() {
        return ws.getDettTitCont().getDtcDsStatoElab();
    }

    @Override
    public void setDtcDsStatoElab(char dtcDsStatoElab) {
        this.ws.getDettTitCont().setDtcDsStatoElab(dtcDsStatoElab);
    }

    @Override
    public long getDtcDsTsEndCptz() {
        return ws.getDettTitCont().getDtcDsTsEndCptz();
    }

    @Override
    public void setDtcDsTsEndCptz(long dtcDsTsEndCptz) {
        this.ws.getDettTitCont().setDtcDsTsEndCptz(dtcDsTsEndCptz);
    }

    @Override
    public long getDtcDsTsIniCptz() {
        return ws.getDettTitCont().getDtcDsTsIniCptz();
    }

    @Override
    public void setDtcDsTsIniCptz(long dtcDsTsIniCptz) {
        this.ws.getDettTitCont().setDtcDsTsIniCptz(dtcDsTsIniCptz);
    }

    @Override
    public String getDtcDsUtente() {
        return ws.getDettTitCont().getDtcDsUtente();
    }

    @Override
    public void setDtcDsUtente(String dtcDsUtente) {
        this.ws.getDettTitCont().setDtcDsUtente(dtcDsUtente);
    }

    @Override
    public int getDtcDsVer() {
        return ws.getDettTitCont().getDtcDsVer();
    }

    @Override
    public void setDtcDsVer(int dtcDsVer) {
        this.ws.getDettTitCont().setDtcDsVer(dtcDsVer);
    }

    @Override
    public String getDtcDtEndCopDb() {
        return ws.getDettTitContDb().getEndCopDb();
    }

    @Override
    public void setDtcDtEndCopDb(String dtcDtEndCopDb) {
        this.ws.getDettTitContDb().setEndCopDb(dtcDtEndCopDb);
    }

    @Override
    public String getDtcDtEndCopDbObj() {
        if (ws.getIndDettTitCont().getDtEndCop() >= 0) {
            return getDtcDtEndCopDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcDtEndCopDbObj(String dtcDtEndCopDbObj) {
        if (dtcDtEndCopDbObj != null) {
            setDtcDtEndCopDb(dtcDtEndCopDbObj);
            ws.getIndDettTitCont().setDtEndCop(((short)0));
        }
        else {
            ws.getIndDettTitCont().setDtEndCop(((short)-1));
        }
    }

    @Override
    public String getDtcDtEndEffDb() {
        return ws.getDettTitContDb().getEndEffDb();
    }

    @Override
    public void setDtcDtEndEffDb(String dtcDtEndEffDb) {
        this.ws.getDettTitContDb().setEndEffDb(dtcDtEndEffDb);
    }

    @Override
    public String getDtcDtEsiTitDb() {
        return ws.getDettTitContDb().getEsiTitDb();
    }

    @Override
    public void setDtcDtEsiTitDb(String dtcDtEsiTitDb) {
        this.ws.getDettTitContDb().setEsiTitDb(dtcDtEsiTitDb);
    }

    @Override
    public String getDtcDtEsiTitDbObj() {
        if (ws.getIndDettTitCont().getDtEsiTit() >= 0) {
            return getDtcDtEsiTitDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcDtEsiTitDbObj(String dtcDtEsiTitDbObj) {
        if (dtcDtEsiTitDbObj != null) {
            setDtcDtEsiTitDb(dtcDtEsiTitDbObj);
            ws.getIndDettTitCont().setDtEsiTit(((short)0));
        }
        else {
            ws.getIndDettTitCont().setDtEsiTit(((short)-1));
        }
    }

    @Override
    public String getDtcDtIniCopDb() {
        return ws.getDettTitContDb().getIniCopDb();
    }

    @Override
    public void setDtcDtIniCopDb(String dtcDtIniCopDb) {
        this.ws.getDettTitContDb().setIniCopDb(dtcDtIniCopDb);
    }

    @Override
    public String getDtcDtIniCopDbObj() {
        if (ws.getIndDettTitCont().getDtIniCop() >= 0) {
            return getDtcDtIniCopDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcDtIniCopDbObj(String dtcDtIniCopDbObj) {
        if (dtcDtIniCopDbObj != null) {
            setDtcDtIniCopDb(dtcDtIniCopDbObj);
            ws.getIndDettTitCont().setDtIniCop(((short)0));
        }
        else {
            ws.getIndDettTitCont().setDtIniCop(((short)-1));
        }
    }

    @Override
    public String getDtcDtIniEffDb() {
        return ws.getDettTitContDb().getIniEffDb();
    }

    @Override
    public void setDtcDtIniEffDb(String dtcDtIniEffDb) {
        this.ws.getDettTitContDb().setIniEffDb(dtcDtIniEffDb);
    }

    @Override
    public int getDtcFrqMovi() {
        return ws.getDettTitCont().getDtcFrqMovi().getDtcFrqMovi();
    }

    @Override
    public void setDtcFrqMovi(int dtcFrqMovi) {
        this.ws.getDettTitCont().getDtcFrqMovi().setDtcFrqMovi(dtcFrqMovi);
    }

    @Override
    public Integer getDtcFrqMoviObj() {
        if (ws.getIndDettTitCont().getFrqMovi() >= 0) {
            return ((Integer)getDtcFrqMovi());
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcFrqMoviObj(Integer dtcFrqMoviObj) {
        if (dtcFrqMoviObj != null) {
            setDtcFrqMovi(((int)dtcFrqMoviObj));
            ws.getIndDettTitCont().setFrqMovi(((short)0));
        }
        else {
            ws.getIndDettTitCont().setFrqMovi(((short)-1));
        }
    }

    @Override
    public int getDtcIdDettTitCont() {
        return ws.getDettTitCont().getDtcIdDettTitCont();
    }

    @Override
    public void setDtcIdDettTitCont(int dtcIdDettTitCont) {
        this.ws.getDettTitCont().setDtcIdDettTitCont(dtcIdDettTitCont);
    }

    @Override
    public int getDtcIdMoviChiu() {
        return ws.getDettTitCont().getDtcIdMoviChiu().getDtcIdMoviChiu();
    }

    @Override
    public void setDtcIdMoviChiu(int dtcIdMoviChiu) {
        this.ws.getDettTitCont().getDtcIdMoviChiu().setDtcIdMoviChiu(dtcIdMoviChiu);
    }

    @Override
    public Integer getDtcIdMoviChiuObj() {
        if (ws.getIndDettTitCont().getIdMoviChiu() >= 0) {
            return ((Integer)getDtcIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcIdMoviChiuObj(Integer dtcIdMoviChiuObj) {
        if (dtcIdMoviChiuObj != null) {
            setDtcIdMoviChiu(((int)dtcIdMoviChiuObj));
            ws.getIndDettTitCont().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndDettTitCont().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getDtcIdMoviCrz() {
        return ws.getDettTitCont().getDtcIdMoviCrz();
    }

    @Override
    public void setDtcIdMoviCrz(int dtcIdMoviCrz) {
        this.ws.getDettTitCont().setDtcIdMoviCrz(dtcIdMoviCrz);
    }

    @Override
    public int getDtcIdOgg() {
        return ws.getDettTitCont().getDtcIdOgg();
    }

    @Override
    public void setDtcIdOgg(int dtcIdOgg) {
        this.ws.getDettTitCont().setDtcIdOgg(dtcIdOgg);
    }

    @Override
    public int getDtcIdTitCont() {
        return ws.getDettTitCont().getDtcIdTitCont();
    }

    @Override
    public void setDtcIdTitCont(int dtcIdTitCont) {
        this.ws.getDettTitCont().setDtcIdTitCont(dtcIdTitCont);
    }

    @Override
    public AfDecimal getDtcImpAder() {
        return ws.getDettTitCont().getDtcImpAder().getDtcImpAder();
    }

    @Override
    public void setDtcImpAder(AfDecimal dtcImpAder) {
        this.ws.getDettTitCont().getDtcImpAder().setDtcImpAder(dtcImpAder.copy());
    }

    @Override
    public AfDecimal getDtcImpAderObj() {
        if (ws.getIndDettTitCont().getImpAder() >= 0) {
            return getDtcImpAder();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcImpAderObj(AfDecimal dtcImpAderObj) {
        if (dtcImpAderObj != null) {
            setDtcImpAder(new AfDecimal(dtcImpAderObj, 15, 3));
            ws.getIndDettTitCont().setImpAder(((short)0));
        }
        else {
            ws.getIndDettTitCont().setImpAder(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcImpAz() {
        return ws.getDettTitCont().getDtcImpAz().getDtcImpAz();
    }

    @Override
    public void setDtcImpAz(AfDecimal dtcImpAz) {
        this.ws.getDettTitCont().getDtcImpAz().setDtcImpAz(dtcImpAz.copy());
    }

    @Override
    public AfDecimal getDtcImpAzObj() {
        if (ws.getIndDettTitCont().getImpAz() >= 0) {
            return getDtcImpAz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcImpAzObj(AfDecimal dtcImpAzObj) {
        if (dtcImpAzObj != null) {
            setDtcImpAz(new AfDecimal(dtcImpAzObj, 15, 3));
            ws.getIndDettTitCont().setImpAz(((short)0));
        }
        else {
            ws.getIndDettTitCont().setImpAz(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcImpTfr() {
        return ws.getDettTitCont().getDtcImpTfr().getDtcImpTfr();
    }

    @Override
    public void setDtcImpTfr(AfDecimal dtcImpTfr) {
        this.ws.getDettTitCont().getDtcImpTfr().setDtcImpTfr(dtcImpTfr.copy());
    }

    @Override
    public AfDecimal getDtcImpTfrObj() {
        if (ws.getIndDettTitCont().getImpTfr() >= 0) {
            return getDtcImpTfr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcImpTfrObj(AfDecimal dtcImpTfrObj) {
        if (dtcImpTfrObj != null) {
            setDtcImpTfr(new AfDecimal(dtcImpTfrObj, 15, 3));
            ws.getIndDettTitCont().setImpTfr(((short)0));
        }
        else {
            ws.getIndDettTitCont().setImpTfr(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcImpTfrStrc() {
        return ws.getDettTitCont().getDtcImpTfrStrc().getDtcImpTfrStrc();
    }

    @Override
    public void setDtcImpTfrStrc(AfDecimal dtcImpTfrStrc) {
        this.ws.getDettTitCont().getDtcImpTfrStrc().setDtcImpTfrStrc(dtcImpTfrStrc.copy());
    }

    @Override
    public AfDecimal getDtcImpTfrStrcObj() {
        if (ws.getIndDettTitCont().getImpTfrStrc() >= 0) {
            return getDtcImpTfrStrc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcImpTfrStrcObj(AfDecimal dtcImpTfrStrcObj) {
        if (dtcImpTfrStrcObj != null) {
            setDtcImpTfrStrc(new AfDecimal(dtcImpTfrStrcObj, 15, 3));
            ws.getIndDettTitCont().setImpTfrStrc(((short)0));
        }
        else {
            ws.getIndDettTitCont().setImpTfrStrc(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcImpTrasfe() {
        return ws.getDettTitCont().getDtcImpTrasfe().getDtcImpTrasfe();
    }

    @Override
    public void setDtcImpTrasfe(AfDecimal dtcImpTrasfe) {
        this.ws.getDettTitCont().getDtcImpTrasfe().setDtcImpTrasfe(dtcImpTrasfe.copy());
    }

    @Override
    public AfDecimal getDtcImpTrasfeObj() {
        if (ws.getIndDettTitCont().getImpTrasfe() >= 0) {
            return getDtcImpTrasfe();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcImpTrasfeObj(AfDecimal dtcImpTrasfeObj) {
        if (dtcImpTrasfeObj != null) {
            setDtcImpTrasfe(new AfDecimal(dtcImpTrasfeObj, 15, 3));
            ws.getIndDettTitCont().setImpTrasfe(((short)0));
        }
        else {
            ws.getIndDettTitCont().setImpTrasfe(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcImpVolo() {
        return ws.getDettTitCont().getDtcImpVolo().getDtcImpVolo();
    }

    @Override
    public void setDtcImpVolo(AfDecimal dtcImpVolo) {
        this.ws.getDettTitCont().getDtcImpVolo().setDtcImpVolo(dtcImpVolo.copy());
    }

    @Override
    public AfDecimal getDtcImpVoloObj() {
        if (ws.getIndDettTitCont().getImpVolo() >= 0) {
            return getDtcImpVolo();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcImpVoloObj(AfDecimal dtcImpVoloObj) {
        if (dtcImpVoloObj != null) {
            setDtcImpVolo(new AfDecimal(dtcImpVoloObj, 15, 3));
            ws.getIndDettTitCont().setImpVolo(((short)0));
        }
        else {
            ws.getIndDettTitCont().setImpVolo(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcIntrFraz() {
        return ws.getDettTitCont().getDtcIntrFraz().getDtcIntrFraz();
    }

    @Override
    public void setDtcIntrFraz(AfDecimal dtcIntrFraz) {
        this.ws.getDettTitCont().getDtcIntrFraz().setDtcIntrFraz(dtcIntrFraz.copy());
    }

    @Override
    public AfDecimal getDtcIntrFrazObj() {
        if (ws.getIndDettTitCont().getIntrFraz() >= 0) {
            return getDtcIntrFraz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcIntrFrazObj(AfDecimal dtcIntrFrazObj) {
        if (dtcIntrFrazObj != null) {
            setDtcIntrFraz(new AfDecimal(dtcIntrFrazObj, 15, 3));
            ws.getIndDettTitCont().setIntrFraz(((short)0));
        }
        else {
            ws.getIndDettTitCont().setIntrFraz(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcIntrMora() {
        return ws.getDettTitCont().getDtcIntrMora().getDtcIntrMora();
    }

    @Override
    public void setDtcIntrMora(AfDecimal dtcIntrMora) {
        this.ws.getDettTitCont().getDtcIntrMora().setDtcIntrMora(dtcIntrMora.copy());
    }

    @Override
    public AfDecimal getDtcIntrMoraObj() {
        if (ws.getIndDettTitCont().getIntrMora() >= 0) {
            return getDtcIntrMora();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcIntrMoraObj(AfDecimal dtcIntrMoraObj) {
        if (dtcIntrMoraObj != null) {
            setDtcIntrMora(new AfDecimal(dtcIntrMoraObj, 15, 3));
            ws.getIndDettTitCont().setIntrMora(((short)0));
        }
        else {
            ws.getIndDettTitCont().setIntrMora(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcIntrRetdt() {
        return ws.getDettTitCont().getDtcIntrRetdt().getDtcIntrRetdt();
    }

    @Override
    public void setDtcIntrRetdt(AfDecimal dtcIntrRetdt) {
        this.ws.getDettTitCont().getDtcIntrRetdt().setDtcIntrRetdt(dtcIntrRetdt.copy());
    }

    @Override
    public AfDecimal getDtcIntrRetdtObj() {
        if (ws.getIndDettTitCont().getIntrRetdt() >= 0) {
            return getDtcIntrRetdt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcIntrRetdtObj(AfDecimal dtcIntrRetdtObj) {
        if (dtcIntrRetdtObj != null) {
            setDtcIntrRetdt(new AfDecimal(dtcIntrRetdtObj, 15, 3));
            ws.getIndDettTitCont().setIntrRetdt(((short)0));
        }
        else {
            ws.getIndDettTitCont().setIntrRetdt(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcIntrRiat() {
        return ws.getDettTitCont().getDtcIntrRiat().getDtcIntrRiat();
    }

    @Override
    public void setDtcIntrRiat(AfDecimal dtcIntrRiat) {
        this.ws.getDettTitCont().getDtcIntrRiat().setDtcIntrRiat(dtcIntrRiat.copy());
    }

    @Override
    public AfDecimal getDtcIntrRiatObj() {
        if (ws.getIndDettTitCont().getIntrRiat() >= 0) {
            return getDtcIntrRiat();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcIntrRiatObj(AfDecimal dtcIntrRiatObj) {
        if (dtcIntrRiatObj != null) {
            setDtcIntrRiat(new AfDecimal(dtcIntrRiatObj, 15, 3));
            ws.getIndDettTitCont().setIntrRiat(((short)0));
        }
        else {
            ws.getIndDettTitCont().setIntrRiat(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcManfeeAntic() {
        return ws.getDettTitCont().getDtcManfeeAntic().getDtcManfeeAntic();
    }

    @Override
    public void setDtcManfeeAntic(AfDecimal dtcManfeeAntic) {
        this.ws.getDettTitCont().getDtcManfeeAntic().setDtcManfeeAntic(dtcManfeeAntic.copy());
    }

    @Override
    public AfDecimal getDtcManfeeAnticObj() {
        if (ws.getIndDettTitCont().getManfeeAntic() >= 0) {
            return getDtcManfeeAntic();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcManfeeAnticObj(AfDecimal dtcManfeeAnticObj) {
        if (dtcManfeeAnticObj != null) {
            setDtcManfeeAntic(new AfDecimal(dtcManfeeAnticObj, 15, 3));
            ws.getIndDettTitCont().setManfeeAntic(((short)0));
        }
        else {
            ws.getIndDettTitCont().setManfeeAntic(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcManfeeRec() {
        return ws.getDettTitCont().getDtcManfeeRec().getDtcManfeeRec();
    }

    @Override
    public void setDtcManfeeRec(AfDecimal dtcManfeeRec) {
        this.ws.getDettTitCont().getDtcManfeeRec().setDtcManfeeRec(dtcManfeeRec.copy());
    }

    @Override
    public AfDecimal getDtcManfeeRecObj() {
        if (ws.getIndDettTitCont().getManfeeRec() >= 0) {
            return getDtcManfeeRec();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcManfeeRecObj(AfDecimal dtcManfeeRecObj) {
        if (dtcManfeeRecObj != null) {
            setDtcManfeeRec(new AfDecimal(dtcManfeeRecObj, 15, 3));
            ws.getIndDettTitCont().setManfeeRec(((short)0));
        }
        else {
            ws.getIndDettTitCont().setManfeeRec(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcManfeeRicor() {
        return ws.getDettTitCont().getDtcManfeeRicor().getDtcManfeeRicor();
    }

    @Override
    public void setDtcManfeeRicor(AfDecimal dtcManfeeRicor) {
        this.ws.getDettTitCont().getDtcManfeeRicor().setDtcManfeeRicor(dtcManfeeRicor.copy());
    }

    @Override
    public AfDecimal getDtcManfeeRicorObj() {
        if (ws.getIndDettTitCont().getManfeeRicor() >= 0) {
            return getDtcManfeeRicor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcManfeeRicorObj(AfDecimal dtcManfeeRicorObj) {
        if (dtcManfeeRicorObj != null) {
            setDtcManfeeRicor(new AfDecimal(dtcManfeeRicorObj, 15, 3));
            ws.getIndDettTitCont().setManfeeRicor(((short)0));
        }
        else {
            ws.getIndDettTitCont().setManfeeRicor(((short)-1));
        }
    }

    @Override
    public int getDtcNumGgRitardoPag() {
        return ws.getDettTitCont().getDtcNumGgRitardoPag().getDtcNumGgRitardoPag();
    }

    @Override
    public void setDtcNumGgRitardoPag(int dtcNumGgRitardoPag) {
        this.ws.getDettTitCont().getDtcNumGgRitardoPag().setDtcNumGgRitardoPag(dtcNumGgRitardoPag);
    }

    @Override
    public Integer getDtcNumGgRitardoPagObj() {
        if (ws.getIndDettTitCont().getNumGgRitardoPag() >= 0) {
            return ((Integer)getDtcNumGgRitardoPag());
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcNumGgRitardoPagObj(Integer dtcNumGgRitardoPagObj) {
        if (dtcNumGgRitardoPagObj != null) {
            setDtcNumGgRitardoPag(((int)dtcNumGgRitardoPagObj));
            ws.getIndDettTitCont().setNumGgRitardoPag(((short)0));
        }
        else {
            ws.getIndDettTitCont().setNumGgRitardoPag(((short)-1));
        }
    }

    @Override
    public int getDtcNumGgRival() {
        return ws.getDettTitCont().getDtcNumGgRival().getDtcNumGgRival();
    }

    @Override
    public void setDtcNumGgRival(int dtcNumGgRival) {
        this.ws.getDettTitCont().getDtcNumGgRival().setDtcNumGgRival(dtcNumGgRival);
    }

    @Override
    public Integer getDtcNumGgRivalObj() {
        if (ws.getIndDettTitCont().getNumGgRival() >= 0) {
            return ((Integer)getDtcNumGgRival());
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcNumGgRivalObj(Integer dtcNumGgRivalObj) {
        if (dtcNumGgRivalObj != null) {
            setDtcNumGgRival(((int)dtcNumGgRivalObj));
            ws.getIndDettTitCont().setNumGgRival(((short)0));
        }
        else {
            ws.getIndDettTitCont().setNumGgRival(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcPreNet() {
        return ws.getDettTitCont().getDtcPreNet().getDtcPreNet();
    }

    @Override
    public void setDtcPreNet(AfDecimal dtcPreNet) {
        this.ws.getDettTitCont().getDtcPreNet().setDtcPreNet(dtcPreNet.copy());
    }

    @Override
    public AfDecimal getDtcPreNetObj() {
        if (ws.getIndDettTitCont().getPreNet() >= 0) {
            return getDtcPreNet();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcPreNetObj(AfDecimal dtcPreNetObj) {
        if (dtcPreNetObj != null) {
            setDtcPreNet(new AfDecimal(dtcPreNetObj, 15, 3));
            ws.getIndDettTitCont().setPreNet(((short)0));
        }
        else {
            ws.getIndDettTitCont().setPreNet(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcPrePpIas() {
        return ws.getDettTitCont().getDtcPrePpIas().getDtcPrePpIas();
    }

    @Override
    public void setDtcPrePpIas(AfDecimal dtcPrePpIas) {
        this.ws.getDettTitCont().getDtcPrePpIas().setDtcPrePpIas(dtcPrePpIas.copy());
    }

    @Override
    public AfDecimal getDtcPrePpIasObj() {
        if (ws.getIndDettTitCont().getPrePpIas() >= 0) {
            return getDtcPrePpIas();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcPrePpIasObj(AfDecimal dtcPrePpIasObj) {
        if (dtcPrePpIasObj != null) {
            setDtcPrePpIas(new AfDecimal(dtcPrePpIasObj, 15, 3));
            ws.getIndDettTitCont().setPrePpIas(((short)0));
        }
        else {
            ws.getIndDettTitCont().setPrePpIas(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcPreSoloRsh() {
        return ws.getDettTitCont().getDtcPreSoloRsh().getDtcPreSoloRsh();
    }

    @Override
    public void setDtcPreSoloRsh(AfDecimal dtcPreSoloRsh) {
        this.ws.getDettTitCont().getDtcPreSoloRsh().setDtcPreSoloRsh(dtcPreSoloRsh.copy());
    }

    @Override
    public AfDecimal getDtcPreSoloRshObj() {
        if (ws.getIndDettTitCont().getPreSoloRsh() >= 0) {
            return getDtcPreSoloRsh();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcPreSoloRshObj(AfDecimal dtcPreSoloRshObj) {
        if (dtcPreSoloRshObj != null) {
            setDtcPreSoloRsh(new AfDecimal(dtcPreSoloRshObj, 15, 3));
            ws.getIndDettTitCont().setPreSoloRsh(((short)0));
        }
        else {
            ws.getIndDettTitCont().setPreSoloRsh(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcPreTot() {
        return ws.getDettTitCont().getDtcPreTot().getDtcPreTot();
    }

    @Override
    public void setDtcPreTot(AfDecimal dtcPreTot) {
        this.ws.getDettTitCont().getDtcPreTot().setDtcPreTot(dtcPreTot.copy());
    }

    @Override
    public AfDecimal getDtcPreTotObj() {
        if (ws.getIndDettTitCont().getPreTot() >= 0) {
            return getDtcPreTot();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcPreTotObj(AfDecimal dtcPreTotObj) {
        if (dtcPreTotObj != null) {
            setDtcPreTot(new AfDecimal(dtcPreTotObj, 15, 3));
            ws.getIndDettTitCont().setPreTot(((short)0));
        }
        else {
            ws.getIndDettTitCont().setPreTot(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcProvAcq1aa() {
        return ws.getDettTitCont().getDtcProvAcq1aa().getDtcProvAcq1aa();
    }

    @Override
    public void setDtcProvAcq1aa(AfDecimal dtcProvAcq1aa) {
        this.ws.getDettTitCont().getDtcProvAcq1aa().setDtcProvAcq1aa(dtcProvAcq1aa.copy());
    }

    @Override
    public AfDecimal getDtcProvAcq1aaObj() {
        if (ws.getIndDettTitCont().getProvAcq1aa() >= 0) {
            return getDtcProvAcq1aa();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcProvAcq1aaObj(AfDecimal dtcProvAcq1aaObj) {
        if (dtcProvAcq1aaObj != null) {
            setDtcProvAcq1aa(new AfDecimal(dtcProvAcq1aaObj, 15, 3));
            ws.getIndDettTitCont().setProvAcq1aa(((short)0));
        }
        else {
            ws.getIndDettTitCont().setProvAcq1aa(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcProvAcq2aa() {
        return ws.getDettTitCont().getDtcProvAcq2aa().getDtcProvAcq2aa();
    }

    @Override
    public void setDtcProvAcq2aa(AfDecimal dtcProvAcq2aa) {
        this.ws.getDettTitCont().getDtcProvAcq2aa().setDtcProvAcq2aa(dtcProvAcq2aa.copy());
    }

    @Override
    public AfDecimal getDtcProvAcq2aaObj() {
        if (ws.getIndDettTitCont().getProvAcq2aa() >= 0) {
            return getDtcProvAcq2aa();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcProvAcq2aaObj(AfDecimal dtcProvAcq2aaObj) {
        if (dtcProvAcq2aaObj != null) {
            setDtcProvAcq2aa(new AfDecimal(dtcProvAcq2aaObj, 15, 3));
            ws.getIndDettTitCont().setProvAcq2aa(((short)0));
        }
        else {
            ws.getIndDettTitCont().setProvAcq2aa(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcProvDaRec() {
        return ws.getDettTitCont().getDtcProvDaRec().getDtcProvDaRec();
    }

    @Override
    public void setDtcProvDaRec(AfDecimal dtcProvDaRec) {
        this.ws.getDettTitCont().getDtcProvDaRec().setDtcProvDaRec(dtcProvDaRec.copy());
    }

    @Override
    public AfDecimal getDtcProvDaRecObj() {
        if (ws.getIndDettTitCont().getProvDaRec() >= 0) {
            return getDtcProvDaRec();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcProvDaRecObj(AfDecimal dtcProvDaRecObj) {
        if (dtcProvDaRecObj != null) {
            setDtcProvDaRec(new AfDecimal(dtcProvDaRecObj, 15, 3));
            ws.getIndDettTitCont().setProvDaRec(((short)0));
        }
        else {
            ws.getIndDettTitCont().setProvDaRec(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcProvInc() {
        return ws.getDettTitCont().getDtcProvInc().getDtcProvInc();
    }

    @Override
    public void setDtcProvInc(AfDecimal dtcProvInc) {
        this.ws.getDettTitCont().getDtcProvInc().setDtcProvInc(dtcProvInc.copy());
    }

    @Override
    public AfDecimal getDtcProvIncObj() {
        if (ws.getIndDettTitCont().getProvInc() >= 0) {
            return getDtcProvInc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcProvIncObj(AfDecimal dtcProvIncObj) {
        if (dtcProvIncObj != null) {
            setDtcProvInc(new AfDecimal(dtcProvIncObj, 15, 3));
            ws.getIndDettTitCont().setProvInc(((short)0));
        }
        else {
            ws.getIndDettTitCont().setProvInc(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcProvRicor() {
        return ws.getDettTitCont().getDtcProvRicor().getDtcProvRicor();
    }

    @Override
    public void setDtcProvRicor(AfDecimal dtcProvRicor) {
        this.ws.getDettTitCont().getDtcProvRicor().setDtcProvRicor(dtcProvRicor.copy());
    }

    @Override
    public AfDecimal getDtcProvRicorObj() {
        if (ws.getIndDettTitCont().getProvRicor() >= 0) {
            return getDtcProvRicor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcProvRicorObj(AfDecimal dtcProvRicorObj) {
        if (dtcProvRicorObj != null) {
            setDtcProvRicor(new AfDecimal(dtcProvRicorObj, 15, 3));
            ws.getIndDettTitCont().setProvRicor(((short)0));
        }
        else {
            ws.getIndDettTitCont().setProvRicor(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcRemunAss() {
        return ws.getDettTitCont().getDtcRemunAss().getDtcRemunAss();
    }

    @Override
    public void setDtcRemunAss(AfDecimal dtcRemunAss) {
        this.ws.getDettTitCont().getDtcRemunAss().setDtcRemunAss(dtcRemunAss.copy());
    }

    @Override
    public AfDecimal getDtcRemunAssObj() {
        if (ws.getIndDettTitCont().getRemunAss() >= 0) {
            return getDtcRemunAss();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcRemunAssObj(AfDecimal dtcRemunAssObj) {
        if (dtcRemunAssObj != null) {
            setDtcRemunAss(new AfDecimal(dtcRemunAssObj, 15, 3));
            ws.getIndDettTitCont().setRemunAss(((short)0));
        }
        else {
            ws.getIndDettTitCont().setRemunAss(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcSoprAlt() {
        return ws.getDettTitCont().getDtcSoprAlt().getDtcSoprAlt();
    }

    @Override
    public void setDtcSoprAlt(AfDecimal dtcSoprAlt) {
        this.ws.getDettTitCont().getDtcSoprAlt().setDtcSoprAlt(dtcSoprAlt.copy());
    }

    @Override
    public AfDecimal getDtcSoprAltObj() {
        if (ws.getIndDettTitCont().getSoprAlt() >= 0) {
            return getDtcSoprAlt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcSoprAltObj(AfDecimal dtcSoprAltObj) {
        if (dtcSoprAltObj != null) {
            setDtcSoprAlt(new AfDecimal(dtcSoprAltObj, 15, 3));
            ws.getIndDettTitCont().setSoprAlt(((short)0));
        }
        else {
            ws.getIndDettTitCont().setSoprAlt(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcSoprProf() {
        return ws.getDettTitCont().getDtcSoprProf().getDtcSoprProf();
    }

    @Override
    public void setDtcSoprProf(AfDecimal dtcSoprProf) {
        this.ws.getDettTitCont().getDtcSoprProf().setDtcSoprProf(dtcSoprProf.copy());
    }

    @Override
    public AfDecimal getDtcSoprProfObj() {
        if (ws.getIndDettTitCont().getSoprProf() >= 0) {
            return getDtcSoprProf();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcSoprProfObj(AfDecimal dtcSoprProfObj) {
        if (dtcSoprProfObj != null) {
            setDtcSoprProf(new AfDecimal(dtcSoprProfObj, 15, 3));
            ws.getIndDettTitCont().setSoprProf(((short)0));
        }
        else {
            ws.getIndDettTitCont().setSoprProf(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcSoprSan() {
        return ws.getDettTitCont().getDtcSoprSan().getDtcSoprSan();
    }

    @Override
    public void setDtcSoprSan(AfDecimal dtcSoprSan) {
        this.ws.getDettTitCont().getDtcSoprSan().setDtcSoprSan(dtcSoprSan.copy());
    }

    @Override
    public AfDecimal getDtcSoprSanObj() {
        if (ws.getIndDettTitCont().getSoprSan() >= 0) {
            return getDtcSoprSan();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcSoprSanObj(AfDecimal dtcSoprSanObj) {
        if (dtcSoprSanObj != null) {
            setDtcSoprSan(new AfDecimal(dtcSoprSanObj, 15, 3));
            ws.getIndDettTitCont().setSoprSan(((short)0));
        }
        else {
            ws.getIndDettTitCont().setSoprSan(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcSoprSpo() {
        return ws.getDettTitCont().getDtcSoprSpo().getDtcSoprSpo();
    }

    @Override
    public void setDtcSoprSpo(AfDecimal dtcSoprSpo) {
        this.ws.getDettTitCont().getDtcSoprSpo().setDtcSoprSpo(dtcSoprSpo.copy());
    }

    @Override
    public AfDecimal getDtcSoprSpoObj() {
        if (ws.getIndDettTitCont().getSoprSpo() >= 0) {
            return getDtcSoprSpo();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcSoprSpoObj(AfDecimal dtcSoprSpoObj) {
        if (dtcSoprSpoObj != null) {
            setDtcSoprSpo(new AfDecimal(dtcSoprSpoObj, 15, 3));
            ws.getIndDettTitCont().setSoprSpo(((short)0));
        }
        else {
            ws.getIndDettTitCont().setSoprSpo(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcSoprTec() {
        return ws.getDettTitCont().getDtcSoprTec().getDtcSoprTec();
    }

    @Override
    public void setDtcSoprTec(AfDecimal dtcSoprTec) {
        this.ws.getDettTitCont().getDtcSoprTec().setDtcSoprTec(dtcSoprTec.copy());
    }

    @Override
    public AfDecimal getDtcSoprTecObj() {
        if (ws.getIndDettTitCont().getSoprTec() >= 0) {
            return getDtcSoprTec();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcSoprTecObj(AfDecimal dtcSoprTecObj) {
        if (dtcSoprTecObj != null) {
            setDtcSoprTec(new AfDecimal(dtcSoprTecObj, 15, 3));
            ws.getIndDettTitCont().setSoprTec(((short)0));
        }
        else {
            ws.getIndDettTitCont().setSoprTec(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcSpeAge() {
        return ws.getDettTitCont().getDtcSpeAge().getDtcSpeAge();
    }

    @Override
    public void setDtcSpeAge(AfDecimal dtcSpeAge) {
        this.ws.getDettTitCont().getDtcSpeAge().setDtcSpeAge(dtcSpeAge.copy());
    }

    @Override
    public AfDecimal getDtcSpeAgeObj() {
        if (ws.getIndDettTitCont().getSpeAge() >= 0) {
            return getDtcSpeAge();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcSpeAgeObj(AfDecimal dtcSpeAgeObj) {
        if (dtcSpeAgeObj != null) {
            setDtcSpeAge(new AfDecimal(dtcSpeAgeObj, 15, 3));
            ws.getIndDettTitCont().setSpeAge(((short)0));
        }
        else {
            ws.getIndDettTitCont().setSpeAge(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcSpeMed() {
        return ws.getDettTitCont().getDtcSpeMed().getDtcSpeMed();
    }

    @Override
    public void setDtcSpeMed(AfDecimal dtcSpeMed) {
        this.ws.getDettTitCont().getDtcSpeMed().setDtcSpeMed(dtcSpeMed.copy());
    }

    @Override
    public AfDecimal getDtcSpeMedObj() {
        if (ws.getIndDettTitCont().getSpeMed() >= 0) {
            return getDtcSpeMed();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcSpeMedObj(AfDecimal dtcSpeMedObj) {
        if (dtcSpeMedObj != null) {
            setDtcSpeMed(new AfDecimal(dtcSpeMedObj, 15, 3));
            ws.getIndDettTitCont().setSpeMed(((short)0));
        }
        else {
            ws.getIndDettTitCont().setSpeMed(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcTax() {
        return ws.getDettTitCont().getDtcTax().getDtcTax();
    }

    @Override
    public void setDtcTax(AfDecimal dtcTax) {
        this.ws.getDettTitCont().getDtcTax().setDtcTax(dtcTax.copy());
    }

    @Override
    public AfDecimal getDtcTaxObj() {
        if (ws.getIndDettTitCont().getTax() >= 0) {
            return getDtcTax();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcTaxObj(AfDecimal dtcTaxObj) {
        if (dtcTaxObj != null) {
            setDtcTax(new AfDecimal(dtcTaxObj, 15, 3));
            ws.getIndDettTitCont().setTax(((short)0));
        }
        else {
            ws.getIndDettTitCont().setTax(((short)-1));
        }
    }

    @Override
    public AfDecimal getDtcTotIntrPrest() {
        return ws.getDettTitCont().getDtcTotIntrPrest().getDtcTotIntrPrest();
    }

    @Override
    public void setDtcTotIntrPrest(AfDecimal dtcTotIntrPrest) {
        this.ws.getDettTitCont().getDtcTotIntrPrest().setDtcTotIntrPrest(dtcTotIntrPrest.copy());
    }

    @Override
    public AfDecimal getDtcTotIntrPrestObj() {
        if (ws.getIndDettTitCont().getTotIntrPrest() >= 0) {
            return getDtcTotIntrPrest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtcTotIntrPrestObj(AfDecimal dtcTotIntrPrestObj) {
        if (dtcTotIntrPrestObj != null) {
            setDtcTotIntrPrest(new AfDecimal(dtcTotIntrPrestObj, 15, 3));
            ws.getIndDettTitCont().setTotIntrPrest(((short)0));
        }
        else {
            ws.getIndDettTitCont().setTotIntrPrest(((short)-1));
        }
    }

    @Override
    public String getDtcTpOgg() {
        return ws.getDettTitCont().getDtcTpOgg();
    }

    @Override
    public void setDtcTpOgg(String dtcTpOgg) {
        this.ws.getDettTitCont().setDtcTpOgg(dtcTpOgg);
    }

    @Override
    public String getDtcTpRgmFisc() {
        return ws.getDettTitCont().getDtcTpRgmFisc();
    }

    @Override
    public void setDtcTpRgmFisc(String dtcTpRgmFisc) {
        this.ws.getDettTitCont().setDtcTpRgmFisc(dtcTpRgmFisc);
    }

    @Override
    public String getDtcTpStatTit() {
        return ws.getDettTitCont().getDtcTpStatTit();
    }

    @Override
    public void setDtcTpStatTit(String dtcTpStatTit) {
        this.ws.getDettTitCont().setDtcTpStatTit(dtcTpStatTit);
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        return idsv0003.getCodiceCompagniaAnia();
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        this.idsv0003.setCodiceCompagniaAnia(idsv0003CodiceCompagniaAnia);
    }

    @Override
    public int getLdbv0371IdOgg() {
        return ldbv0371.getLdbv0371IdOgg();
    }

    @Override
    public void setLdbv0371IdOgg(int ldbv0371IdOgg) {
        this.ldbv0371.setLdbv0371IdOgg(ldbv0371IdOgg);
    }

    @Override
    public String getLdbv0371TpOgg() {
        return ldbv0371.getLdbv0371TpOgg();
    }

    @Override
    public void setLdbv0371TpOgg(String ldbv0371TpOgg) {
        this.ldbv0371.setLdbv0371TpOgg(ldbv0371TpOgg);
    }

    @Override
    public String getLdbv0371TpPreTit() {
        return ldbv0371.getLdbv0371TpPreTit();
    }

    @Override
    public void setLdbv0371TpPreTit(String ldbv0371TpPreTit) {
        this.ldbv0371.setLdbv0371TpPreTit(ldbv0371TpPreTit);
    }

    @Override
    public String getLdbv0371TpStatTit1() {
        return ldbv0371.getLdbv0371TpStatTit1();
    }

    @Override
    public void setLdbv0371TpStatTit1(String ldbv0371TpStatTit1) {
        this.ldbv0371.setLdbv0371TpStatTit1(ldbv0371TpStatTit1);
    }

    @Override
    public String getLdbv0371TpStatTit2() {
        return ldbv0371.getLdbv0371TpStatTit2();
    }

    @Override
    public void setLdbv0371TpStatTit2(String ldbv0371TpStatTit2) {
        this.ldbv0371.setLdbv0371TpStatTit2(ldbv0371TpStatTit2);
    }

    @Override
    public int getLdbv2971IdOgg() {
        throw new FieldNotMappedException("ldbv2971IdOgg");
    }

    @Override
    public void setLdbv2971IdOgg(int ldbv2971IdOgg) {
        throw new FieldNotMappedException("ldbv2971IdOgg");
    }

    @Override
    public String getLdbv2971TpOgg() {
        throw new FieldNotMappedException("ldbv2971TpOgg");
    }

    @Override
    public void setLdbv2971TpOgg(String ldbv2971TpOgg) {
        throw new FieldNotMappedException("ldbv2971TpOgg");
    }

    @Override
    public String getLdbv3101DtADb() {
        throw new FieldNotMappedException("ldbv3101DtADb");
    }

    @Override
    public void setLdbv3101DtADb(String ldbv3101DtADb) {
        throw new FieldNotMappedException("ldbv3101DtADb");
    }

    @Override
    public String getLdbv3101DtDaDb() {
        throw new FieldNotMappedException("ldbv3101DtDaDb");
    }

    @Override
    public void setLdbv3101DtDaDb(String ldbv3101DtDaDb) {
        throw new FieldNotMappedException("ldbv3101DtDaDb");
    }

    @Override
    public int getLdbv3101IdOgg() {
        throw new FieldNotMappedException("ldbv3101IdOgg");
    }

    @Override
    public void setLdbv3101IdOgg(int ldbv3101IdOgg) {
        throw new FieldNotMappedException("ldbv3101IdOgg");
    }

    @Override
    public String getLdbv3101TpOgg() {
        throw new FieldNotMappedException("ldbv3101TpOgg");
    }

    @Override
    public void setLdbv3101TpOgg(String ldbv3101TpOgg) {
        throw new FieldNotMappedException("ldbv3101TpOgg");
    }

    @Override
    public String getLdbv3101TpStaTit() {
        throw new FieldNotMappedException("ldbv3101TpStaTit");
    }

    @Override
    public void setLdbv3101TpStaTit(String ldbv3101TpStaTit) {
        throw new FieldNotMappedException("ldbv3101TpStaTit");
    }

    @Override
    public String getLdbv3101TpTit() {
        throw new FieldNotMappedException("ldbv3101TpTit");
    }

    @Override
    public void setLdbv3101TpTit(String ldbv3101TpTit) {
        throw new FieldNotMappedException("ldbv3101TpTit");
    }

    @Override
    public String getLdbv7141DtADb() {
        throw new FieldNotMappedException("ldbv7141DtADb");
    }

    @Override
    public void setLdbv7141DtADb(String ldbv7141DtADb) {
        throw new FieldNotMappedException("ldbv7141DtADb");
    }

    @Override
    public String getLdbv7141DtDaDb() {
        throw new FieldNotMappedException("ldbv7141DtDaDb");
    }

    @Override
    public void setLdbv7141DtDaDb(String ldbv7141DtDaDb) {
        throw new FieldNotMappedException("ldbv7141DtDaDb");
    }

    @Override
    public int getLdbv7141IdOgg() {
        throw new FieldNotMappedException("ldbv7141IdOgg");
    }

    @Override
    public void setLdbv7141IdOgg(int ldbv7141IdOgg) {
        throw new FieldNotMappedException("ldbv7141IdOgg");
    }

    @Override
    public String getLdbv7141TpOgg() {
        throw new FieldNotMappedException("ldbv7141TpOgg");
    }

    @Override
    public void setLdbv7141TpOgg(String ldbv7141TpOgg) {
        throw new FieldNotMappedException("ldbv7141TpOgg");
    }

    @Override
    public String getLdbv7141TpStaTit1() {
        throw new FieldNotMappedException("ldbv7141TpStaTit1");
    }

    @Override
    public void setLdbv7141TpStaTit1(String ldbv7141TpStaTit1) {
        throw new FieldNotMappedException("ldbv7141TpStaTit1");
    }

    @Override
    public String getLdbv7141TpStaTit2() {
        throw new FieldNotMappedException("ldbv7141TpStaTit2");
    }

    @Override
    public void setLdbv7141TpStaTit2(String ldbv7141TpStaTit2) {
        throw new FieldNotMappedException("ldbv7141TpStaTit2");
    }

    @Override
    public String getLdbv7141TpTit() {
        throw new FieldNotMappedException("ldbv7141TpTit");
    }

    @Override
    public void setLdbv7141TpTit(String ldbv7141TpTit) {
        throw new FieldNotMappedException("ldbv7141TpTit");
    }

    @Override
    public int getLdbvd601IdOgg() {
        throw new FieldNotMappedException("ldbvd601IdOgg");
    }

    @Override
    public void setLdbvd601IdOgg(int ldbvd601IdOgg) {
        throw new FieldNotMappedException("ldbvd601IdOgg");
    }

    @Override
    public String getLdbvd601TpOgg() {
        throw new FieldNotMappedException("ldbvd601TpOgg");
    }

    @Override
    public void setLdbvd601TpOgg(String ldbvd601TpOgg) {
        throw new FieldNotMappedException("ldbvd601TpOgg");
    }

    @Override
    public String getLdbvd601TpStaTit() {
        throw new FieldNotMappedException("ldbvd601TpStaTit");
    }

    @Override
    public void setLdbvd601TpStaTit(String ldbvd601TpStaTit) {
        throw new FieldNotMappedException("ldbvd601TpStaTit");
    }

    @Override
    public String getLdbvd601TpTit() {
        throw new FieldNotMappedException("ldbvd601TpTit");
    }

    @Override
    public void setLdbvd601TpTit(String ldbvd601TpTit) {
        throw new FieldNotMappedException("ldbvd601TpTit");
    }

    @Override
    public int getTitCodCompAnia() {
        return ws.getTitCont().getTitCodCompAnia();
    }

    @Override
    public void setTitCodCompAnia(int titCodCompAnia) {
        this.ws.getTitCont().setTitCodCompAnia(titCodCompAnia);
    }

    @Override
    public String getTitCodDvs() {
        return ws.getTitCont().getTitCodDvs();
    }

    @Override
    public void setTitCodDvs(String titCodDvs) {
        this.ws.getTitCont().setTitCodDvs(titCodDvs);
    }

    @Override
    public String getTitCodDvsObj() {
        if (ws.getIndTitCont().getCodDvs() >= 0) {
            return getTitCodDvs();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitCodDvsObj(String titCodDvsObj) {
        if (titCodDvsObj != null) {
            setTitCodDvs(titCodDvsObj);
            ws.getIndTitCont().setCodDvs(((short)0));
        }
        else {
            ws.getIndTitCont().setCodDvs(((short)-1));
        }
    }

    @Override
    public String getTitCodIban() {
        return ws.getTitCont().getTitCodIban();
    }

    @Override
    public void setTitCodIban(String titCodIban) {
        this.ws.getTitCont().setTitCodIban(titCodIban);
    }

    @Override
    public String getTitCodIbanObj() {
        if (ws.getIndTitCont().getCodIban() >= 0) {
            return getTitCodIban();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitCodIbanObj(String titCodIbanObj) {
        if (titCodIbanObj != null) {
            setTitCodIban(titCodIbanObj);
            ws.getIndTitCont().setCodIban(((short)0));
        }
        else {
            ws.getIndTitCont().setCodIban(((short)-1));
        }
    }

    @Override
    public char getTitDsOperSql() {
        return ws.getTitCont().getTitDsOperSql();
    }

    @Override
    public void setTitDsOperSql(char titDsOperSql) {
        this.ws.getTitCont().setTitDsOperSql(titDsOperSql);
    }

    @Override
    public long getTitDsRiga() {
        return ws.getTitCont().getTitDsRiga();
    }

    @Override
    public void setTitDsRiga(long titDsRiga) {
        this.ws.getTitCont().setTitDsRiga(titDsRiga);
    }

    @Override
    public char getTitDsStatoElab() {
        return ws.getTitCont().getTitDsStatoElab();
    }

    @Override
    public void setTitDsStatoElab(char titDsStatoElab) {
        this.ws.getTitCont().setTitDsStatoElab(titDsStatoElab);
    }

    @Override
    public long getTitDsTsEndCptz() {
        return ws.getTitCont().getTitDsTsEndCptz();
    }

    @Override
    public void setTitDsTsEndCptz(long titDsTsEndCptz) {
        this.ws.getTitCont().setTitDsTsEndCptz(titDsTsEndCptz);
    }

    @Override
    public long getTitDsTsIniCptz() {
        return ws.getTitCont().getTitDsTsIniCptz();
    }

    @Override
    public void setTitDsTsIniCptz(long titDsTsIniCptz) {
        this.ws.getTitCont().setTitDsTsIniCptz(titDsTsIniCptz);
    }

    @Override
    public String getTitDsUtente() {
        return ws.getTitCont().getTitDsUtente();
    }

    @Override
    public void setTitDsUtente(String titDsUtente) {
        this.ws.getTitCont().setTitDsUtente(titDsUtente);
    }

    @Override
    public int getTitDsVer() {
        return ws.getTitCont().getTitDsVer();
    }

    @Override
    public void setTitDsVer(int titDsVer) {
        this.ws.getTitCont().setTitDsVer(titDsVer);
    }

    @Override
    public String getTitDtApplzMoraDb() {
        return ws.getTitContDb().getApplzMoraDb();
    }

    @Override
    public void setTitDtApplzMoraDb(String titDtApplzMoraDb) {
        this.ws.getTitContDb().setApplzMoraDb(titDtApplzMoraDb);
    }

    @Override
    public String getTitDtApplzMoraDbObj() {
        if (ws.getIndTitCont().getDtApplzMora() >= 0) {
            return getTitDtApplzMoraDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitDtApplzMoraDbObj(String titDtApplzMoraDbObj) {
        if (titDtApplzMoraDbObj != null) {
            setTitDtApplzMoraDb(titDtApplzMoraDbObj);
            ws.getIndTitCont().setDtApplzMora(((short)0));
        }
        else {
            ws.getIndTitCont().setDtApplzMora(((short)-1));
        }
    }

    @Override
    public String getTitDtCambioVltDb() {
        return ws.getTitContDb().getCambioVltDb();
    }

    @Override
    public void setTitDtCambioVltDb(String titDtCambioVltDb) {
        this.ws.getTitContDb().setCambioVltDb(titDtCambioVltDb);
    }

    @Override
    public String getTitDtCambioVltDbObj() {
        if (ws.getIndTitCont().getDtCambioVlt() >= 0) {
            return getTitDtCambioVltDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitDtCambioVltDbObj(String titDtCambioVltDbObj) {
        if (titDtCambioVltDbObj != null) {
            setTitDtCambioVltDb(titDtCambioVltDbObj);
            ws.getIndTitCont().setDtCambioVlt(((short)0));
        }
        else {
            ws.getIndTitCont().setDtCambioVlt(((short)-1));
        }
    }

    @Override
    public String getTitDtCertFiscDb() {
        return ws.getTitContDb().getCertFiscDb();
    }

    @Override
    public void setTitDtCertFiscDb(String titDtCertFiscDb) {
        this.ws.getTitContDb().setCertFiscDb(titDtCertFiscDb);
    }

    @Override
    public String getTitDtCertFiscDbObj() {
        if (ws.getIndTitCont().getDtCertFisc() >= 0) {
            return getTitDtCertFiscDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitDtCertFiscDbObj(String titDtCertFiscDbObj) {
        if (titDtCertFiscDbObj != null) {
            setTitDtCertFiscDb(titDtCertFiscDbObj);
            ws.getIndTitCont().setDtCertFisc(((short)0));
        }
        else {
            ws.getIndTitCont().setDtCertFisc(((short)-1));
        }
    }

    @Override
    public String getTitDtEmisTitDb() {
        return ws.getTitContDb().getEmisTitDb();
    }

    @Override
    public void setTitDtEmisTitDb(String titDtEmisTitDb) {
        this.ws.getTitContDb().setEmisTitDb(titDtEmisTitDb);
    }

    @Override
    public String getTitDtEmisTitDbObj() {
        if (ws.getIndTitCont().getDtEmisTit() >= 0) {
            return getTitDtEmisTitDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitDtEmisTitDbObj(String titDtEmisTitDbObj) {
        if (titDtEmisTitDbObj != null) {
            setTitDtEmisTitDb(titDtEmisTitDbObj);
            ws.getIndTitCont().setDtEmisTit(((short)0));
        }
        else {
            ws.getIndTitCont().setDtEmisTit(((short)-1));
        }
    }

    @Override
    public String getTitDtEndCopDb() {
        return ws.getTitContDb().getEndCopDb();
    }

    @Override
    public void setTitDtEndCopDb(String titDtEndCopDb) {
        this.ws.getTitContDb().setEndCopDb(titDtEndCopDb);
    }

    @Override
    public String getTitDtEndCopDbObj() {
        if (ws.getIndTitCont().getDtEndCop() >= 0) {
            return getTitDtEndCopDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitDtEndCopDbObj(String titDtEndCopDbObj) {
        if (titDtEndCopDbObj != null) {
            setTitDtEndCopDb(titDtEndCopDbObj);
            ws.getIndTitCont().setDtEndCop(((short)0));
        }
        else {
            ws.getIndTitCont().setDtEndCop(((short)-1));
        }
    }

    @Override
    public String getTitDtEndEffDb() {
        return ws.getTitContDb().getEndEffDb();
    }

    @Override
    public void setTitDtEndEffDb(String titDtEndEffDb) {
        this.ws.getTitContDb().setEndEffDb(titDtEndEffDb);
    }

    @Override
    public String getTitDtEsiTitDb() {
        return ws.getTitContDb().getEsiTitDb();
    }

    @Override
    public void setTitDtEsiTitDb(String titDtEsiTitDb) {
        this.ws.getTitContDb().setEsiTitDb(titDtEsiTitDb);
    }

    @Override
    public String getTitDtEsiTitDbObj() {
        if (ws.getIndTitCont().getDtEsiTit() >= 0) {
            return getTitDtEsiTitDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitDtEsiTitDbObj(String titDtEsiTitDbObj) {
        if (titDtEsiTitDbObj != null) {
            setTitDtEsiTitDb(titDtEsiTitDbObj);
            ws.getIndTitCont().setDtEsiTit(((short)0));
        }
        else {
            ws.getIndTitCont().setDtEsiTit(((short)-1));
        }
    }

    @Override
    public String getTitDtIniCopDb() {
        return ws.getTitContDb().getIniCopDb();
    }

    @Override
    public void setTitDtIniCopDb(String titDtIniCopDb) {
        this.ws.getTitContDb().setIniCopDb(titDtIniCopDb);
    }

    @Override
    public String getTitDtIniCopDbObj() {
        if (ws.getIndTitCont().getDtIniCop() >= 0) {
            return getTitDtIniCopDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitDtIniCopDbObj(String titDtIniCopDbObj) {
        if (titDtIniCopDbObj != null) {
            setTitDtIniCopDb(titDtIniCopDbObj);
            ws.getIndTitCont().setDtIniCop(((short)0));
        }
        else {
            ws.getIndTitCont().setDtIniCop(((short)-1));
        }
    }

    @Override
    public String getTitDtIniEffDb() {
        return ws.getTitContDb().getIniEffDb();
    }

    @Override
    public void setTitDtIniEffDb(String titDtIniEffDb) {
        this.ws.getTitContDb().setIniEffDb(titDtIniEffDb);
    }

    @Override
    public String getTitDtRichAddRidDb() {
        return ws.getTitContDb().getRichAddRidDb();
    }

    @Override
    public void setTitDtRichAddRidDb(String titDtRichAddRidDb) {
        this.ws.getTitContDb().setRichAddRidDb(titDtRichAddRidDb);
    }

    @Override
    public String getTitDtRichAddRidDbObj() {
        if (ws.getIndTitCont().getDtRichAddRid() >= 0) {
            return getTitDtRichAddRidDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitDtRichAddRidDbObj(String titDtRichAddRidDbObj) {
        if (titDtRichAddRidDbObj != null) {
            setTitDtRichAddRidDb(titDtRichAddRidDbObj);
            ws.getIndTitCont().setDtRichAddRid(((short)0));
        }
        else {
            ws.getIndTitCont().setDtRichAddRid(((short)-1));
        }
    }

    @Override
    public String getTitDtVltDb() {
        return ws.getTitContDb().getVltDb();
    }

    @Override
    public void setTitDtVltDb(String titDtVltDb) {
        this.ws.getTitContDb().setVltDb(titDtVltDb);
    }

    @Override
    public String getTitDtVltDbObj() {
        if (ws.getIndTitCont().getDtVlt() >= 0) {
            return getTitDtVltDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitDtVltDbObj(String titDtVltDbObj) {
        if (titDtVltDbObj != null) {
            setTitDtVltDb(titDtVltDbObj);
            ws.getIndTitCont().setDtVlt(((short)0));
        }
        else {
            ws.getIndTitCont().setDtVlt(((short)-1));
        }
    }

    @Override
    public String getTitEstrCntCorrAdd() {
        return ws.getTitCont().getTitEstrCntCorrAdd();
    }

    @Override
    public void setTitEstrCntCorrAdd(String titEstrCntCorrAdd) {
        this.ws.getTitCont().setTitEstrCntCorrAdd(titEstrCntCorrAdd);
    }

    @Override
    public String getTitEstrCntCorrAddObj() {
        if (ws.getIndTitCont().getEstrCntCorrAdd() >= 0) {
            return getTitEstrCntCorrAdd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitEstrCntCorrAddObj(String titEstrCntCorrAddObj) {
        if (titEstrCntCorrAddObj != null) {
            setTitEstrCntCorrAdd(titEstrCntCorrAddObj);
            ws.getIndTitCont().setEstrCntCorrAdd(((short)0));
        }
        else {
            ws.getIndTitCont().setEstrCntCorrAdd(((short)-1));
        }
    }

    @Override
    public char getTitFlForzDtVlt() {
        return ws.getTitCont().getTitFlForzDtVlt();
    }

    @Override
    public void setTitFlForzDtVlt(char titFlForzDtVlt) {
        this.ws.getTitCont().setTitFlForzDtVlt(titFlForzDtVlt);
    }

    @Override
    public Character getTitFlForzDtVltObj() {
        if (ws.getIndTitCont().getFlForzDtVlt() >= 0) {
            return ((Character)getTitFlForzDtVlt());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitFlForzDtVltObj(Character titFlForzDtVltObj) {
        if (titFlForzDtVltObj != null) {
            setTitFlForzDtVlt(((char)titFlForzDtVltObj));
            ws.getIndTitCont().setFlForzDtVlt(((short)0));
        }
        else {
            ws.getIndTitCont().setFlForzDtVlt(((short)-1));
        }
    }

    @Override
    public char getTitFlIncAutogen() {
        return ws.getTitCont().getTitFlIncAutogen();
    }

    @Override
    public void setTitFlIncAutogen(char titFlIncAutogen) {
        this.ws.getTitCont().setTitFlIncAutogen(titFlIncAutogen);
    }

    @Override
    public Character getTitFlIncAutogenObj() {
        if (ws.getIndTitCont().getFlIncAutogen() >= 0) {
            return ((Character)getTitFlIncAutogen());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitFlIncAutogenObj(Character titFlIncAutogenObj) {
        if (titFlIncAutogenObj != null) {
            setTitFlIncAutogen(((char)titFlIncAutogenObj));
            ws.getIndTitCont().setFlIncAutogen(((short)0));
        }
        else {
            ws.getIndTitCont().setFlIncAutogen(((short)-1));
        }
    }

    @Override
    public char getTitFlMora() {
        return ws.getTitCont().getTitFlMora();
    }

    @Override
    public void setTitFlMora(char titFlMora) {
        this.ws.getTitCont().setTitFlMora(titFlMora);
    }

    @Override
    public Character getTitFlMoraObj() {
        if (ws.getIndTitCont().getFlMora() >= 0) {
            return ((Character)getTitFlMora());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitFlMoraObj(Character titFlMoraObj) {
        if (titFlMoraObj != null) {
            setTitFlMora(((char)titFlMoraObj));
            ws.getIndTitCont().setFlMora(((short)0));
        }
        else {
            ws.getIndTitCont().setFlMora(((short)-1));
        }
    }

    @Override
    public char getTitFlSoll() {
        return ws.getTitCont().getTitFlSoll();
    }

    @Override
    public void setTitFlSoll(char titFlSoll) {
        this.ws.getTitCont().setTitFlSoll(titFlSoll);
    }

    @Override
    public Character getTitFlSollObj() {
        if (ws.getIndTitCont().getFlSoll() >= 0) {
            return ((Character)getTitFlSoll());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitFlSollObj(Character titFlSollObj) {
        if (titFlSollObj != null) {
            setTitFlSoll(((char)titFlSollObj));
            ws.getIndTitCont().setFlSoll(((short)0));
        }
        else {
            ws.getIndTitCont().setFlSoll(((short)-1));
        }
    }

    @Override
    public char getTitFlTitDaReinvst() {
        return ws.getTitCont().getTitFlTitDaReinvst();
    }

    @Override
    public void setTitFlTitDaReinvst(char titFlTitDaReinvst) {
        this.ws.getTitCont().setTitFlTitDaReinvst(titFlTitDaReinvst);
    }

    @Override
    public Character getTitFlTitDaReinvstObj() {
        if (ws.getIndTitCont().getFlTitDaReinvst() >= 0) {
            return ((Character)getTitFlTitDaReinvst());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitFlTitDaReinvstObj(Character titFlTitDaReinvstObj) {
        if (titFlTitDaReinvstObj != null) {
            setTitFlTitDaReinvst(((char)titFlTitDaReinvstObj));
            ws.getIndTitCont().setFlTitDaReinvst(((short)0));
        }
        else {
            ws.getIndTitCont().setFlTitDaReinvst(((short)-1));
        }
    }

    @Override
    public int getTitFraz() {
        return ws.getTitCont().getTitFraz().getTitFraz();
    }

    @Override
    public void setTitFraz(int titFraz) {
        this.ws.getTitCont().getTitFraz().setTitFraz(titFraz);
    }

    @Override
    public Integer getTitFrazObj() {
        if (ws.getIndTitCont().getFraz() >= 0) {
            return ((Integer)getTitFraz());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitFrazObj(Integer titFrazObj) {
        if (titFrazObj != null) {
            setTitFraz(((int)titFrazObj));
            ws.getIndTitCont().setFraz(((short)0));
        }
        else {
            ws.getIndTitCont().setFraz(((short)-1));
        }
    }

    @Override
    public String getTitIbRich() {
        return ws.getTitCont().getTitIbRich();
    }

    @Override
    public void setTitIbRich(String titIbRich) {
        this.ws.getTitCont().setTitIbRich(titIbRich);
    }

    @Override
    public String getTitIbRichObj() {
        if (ws.getIndTitCont().getIbRich() >= 0) {
            return getTitIbRich();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitIbRichObj(String titIbRichObj) {
        if (titIbRichObj != null) {
            setTitIbRich(titIbRichObj);
            ws.getIndTitCont().setIbRich(((short)0));
        }
        else {
            ws.getIndTitCont().setIbRich(((short)-1));
        }
    }

    @Override
    public int getTitIdMoviChiu() {
        return ws.getTitCont().getTitIdMoviChiu().getTitIdMoviChiu();
    }

    @Override
    public void setTitIdMoviChiu(int titIdMoviChiu) {
        this.ws.getTitCont().getTitIdMoviChiu().setTitIdMoviChiu(titIdMoviChiu);
    }

    @Override
    public Integer getTitIdMoviChiuObj() {
        if (ws.getIndTitCont().getIdMoviChiu() >= 0) {
            return ((Integer)getTitIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitIdMoviChiuObj(Integer titIdMoviChiuObj) {
        if (titIdMoviChiuObj != null) {
            setTitIdMoviChiu(((int)titIdMoviChiuObj));
            ws.getIndTitCont().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndTitCont().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getTitIdMoviCrz() {
        return ws.getTitCont().getTitIdMoviCrz();
    }

    @Override
    public void setTitIdMoviCrz(int titIdMoviCrz) {
        this.ws.getTitCont().setTitIdMoviCrz(titIdMoviCrz);
    }

    @Override
    public int getTitIdOgg() {
        return ws.getTitCont().getTitIdOgg();
    }

    @Override
    public void setTitIdOgg(int titIdOgg) {
        this.ws.getTitCont().setTitIdOgg(titIdOgg);
    }

    @Override
    public int getTitIdRappAna() {
        return ws.getTitCont().getTitIdRappAna().getTitIdRappAna();
    }

    @Override
    public void setTitIdRappAna(int titIdRappAna) {
        this.ws.getTitCont().getTitIdRappAna().setTitIdRappAna(titIdRappAna);
    }

    @Override
    public Integer getTitIdRappAnaObj() {
        if (ws.getIndTitCont().getIdRappAna() >= 0) {
            return ((Integer)getTitIdRappAna());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitIdRappAnaObj(Integer titIdRappAnaObj) {
        if (titIdRappAnaObj != null) {
            setTitIdRappAna(((int)titIdRappAnaObj));
            ws.getIndTitCont().setIdRappAna(((short)0));
        }
        else {
            ws.getIndTitCont().setIdRappAna(((short)-1));
        }
    }

    @Override
    public int getTitIdRappRete() {
        return ws.getTitCont().getTitIdRappRete().getTitIdRappRete();
    }

    @Override
    public void setTitIdRappRete(int titIdRappRete) {
        this.ws.getTitCont().getTitIdRappRete().setTitIdRappRete(titIdRappRete);
    }

    @Override
    public Integer getTitIdRappReteObj() {
        if (ws.getIndTitCont().getIdRappRete() >= 0) {
            return ((Integer)getTitIdRappRete());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitIdRappReteObj(Integer titIdRappReteObj) {
        if (titIdRappReteObj != null) {
            setTitIdRappRete(((int)titIdRappReteObj));
            ws.getIndTitCont().setIdRappRete(((short)0));
        }
        else {
            ws.getIndTitCont().setIdRappRete(((short)-1));
        }
    }

    @Override
    public int getTitIdTitCont() {
        return ws.getTitCont().getTitIdTitCont();
    }

    @Override
    public void setTitIdTitCont(int titIdTitCont) {
        this.ws.getTitCont().setTitIdTitCont(titIdTitCont);
    }

    @Override
    public AfDecimal getTitImpAder() {
        return ws.getTitCont().getTitImpAder().getTitImpAder();
    }

    @Override
    public void setTitImpAder(AfDecimal titImpAder) {
        this.ws.getTitCont().getTitImpAder().setTitImpAder(titImpAder.copy());
    }

    @Override
    public AfDecimal getTitImpAderObj() {
        if (ws.getIndTitCont().getImpAder() >= 0) {
            return getTitImpAder();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitImpAderObj(AfDecimal titImpAderObj) {
        if (titImpAderObj != null) {
            setTitImpAder(new AfDecimal(titImpAderObj, 15, 3));
            ws.getIndTitCont().setImpAder(((short)0));
        }
        else {
            ws.getIndTitCont().setImpAder(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitImpAz() {
        return ws.getTitCont().getTitImpAz().getTitImpAz();
    }

    @Override
    public void setTitImpAz(AfDecimal titImpAz) {
        this.ws.getTitCont().getTitImpAz().setTitImpAz(titImpAz.copy());
    }

    @Override
    public AfDecimal getTitImpAzObj() {
        if (ws.getIndTitCont().getImpAz() >= 0) {
            return getTitImpAz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitImpAzObj(AfDecimal titImpAzObj) {
        if (titImpAzObj != null) {
            setTitImpAz(new AfDecimal(titImpAzObj, 15, 3));
            ws.getIndTitCont().setImpAz(((short)0));
        }
        else {
            ws.getIndTitCont().setImpAz(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitImpPag() {
        return ws.getTitCont().getTitImpPag().getTitImpPag();
    }

    @Override
    public void setTitImpPag(AfDecimal titImpPag) {
        this.ws.getTitCont().getTitImpPag().setTitImpPag(titImpPag.copy());
    }

    @Override
    public AfDecimal getTitImpPagObj() {
        if (ws.getIndTitCont().getImpPag() >= 0) {
            return getTitImpPag();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitImpPagObj(AfDecimal titImpPagObj) {
        if (titImpPagObj != null) {
            setTitImpPag(new AfDecimal(titImpPagObj, 15, 3));
            ws.getIndTitCont().setImpPag(((short)0));
        }
        else {
            ws.getIndTitCont().setImpPag(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitImpTfr() {
        return ws.getTitCont().getTitImpTfr().getTitImpTfr();
    }

    @Override
    public void setTitImpTfr(AfDecimal titImpTfr) {
        this.ws.getTitCont().getTitImpTfr().setTitImpTfr(titImpTfr.copy());
    }

    @Override
    public AfDecimal getTitImpTfrObj() {
        if (ws.getIndTitCont().getImpTfr() >= 0) {
            return getTitImpTfr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitImpTfrObj(AfDecimal titImpTfrObj) {
        if (titImpTfrObj != null) {
            setTitImpTfr(new AfDecimal(titImpTfrObj, 15, 3));
            ws.getIndTitCont().setImpTfr(((short)0));
        }
        else {
            ws.getIndTitCont().setImpTfr(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitImpTfrStrc() {
        return ws.getTitCont().getTitImpTfrStrc().getTitImpTfrStrc();
    }

    @Override
    public void setTitImpTfrStrc(AfDecimal titImpTfrStrc) {
        this.ws.getTitCont().getTitImpTfrStrc().setTitImpTfrStrc(titImpTfrStrc.copy());
    }

    @Override
    public AfDecimal getTitImpTfrStrcObj() {
        if (ws.getIndTitCont().getImpTfrStrc() >= 0) {
            return getTitImpTfrStrc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitImpTfrStrcObj(AfDecimal titImpTfrStrcObj) {
        if (titImpTfrStrcObj != null) {
            setTitImpTfrStrc(new AfDecimal(titImpTfrStrcObj, 15, 3));
            ws.getIndTitCont().setImpTfrStrc(((short)0));
        }
        else {
            ws.getIndTitCont().setImpTfrStrc(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitImpTrasfe() {
        return ws.getTitCont().getTitImpTrasfe().getTitImpTrasfe();
    }

    @Override
    public void setTitImpTrasfe(AfDecimal titImpTrasfe) {
        this.ws.getTitCont().getTitImpTrasfe().setTitImpTrasfe(titImpTrasfe.copy());
    }

    @Override
    public AfDecimal getTitImpTrasfeObj() {
        if (ws.getIndTitCont().getImpTrasfe() >= 0) {
            return getTitImpTrasfe();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitImpTrasfeObj(AfDecimal titImpTrasfeObj) {
        if (titImpTrasfeObj != null) {
            setTitImpTrasfe(new AfDecimal(titImpTrasfeObj, 15, 3));
            ws.getIndTitCont().setImpTrasfe(((short)0));
        }
        else {
            ws.getIndTitCont().setImpTrasfe(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitImpVolo() {
        return ws.getTitCont().getTitImpVolo().getTitImpVolo();
    }

    @Override
    public void setTitImpVolo(AfDecimal titImpVolo) {
        this.ws.getTitCont().getTitImpVolo().setTitImpVolo(titImpVolo.copy());
    }

    @Override
    public AfDecimal getTitImpVoloObj() {
        if (ws.getIndTitCont().getImpVolo() >= 0) {
            return getTitImpVolo();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitImpVoloObj(AfDecimal titImpVoloObj) {
        if (titImpVoloObj != null) {
            setTitImpVolo(new AfDecimal(titImpVoloObj, 15, 3));
            ws.getIndTitCont().setImpVolo(((short)0));
        }
        else {
            ws.getIndTitCont().setImpVolo(((short)-1));
        }
    }

    @Override
    public int getTitNumRatAccorpate() {
        return ws.getTitCont().getTitNumRatAccorpate().getTitNumRatAccorpate();
    }

    @Override
    public void setTitNumRatAccorpate(int titNumRatAccorpate) {
        this.ws.getTitCont().getTitNumRatAccorpate().setTitNumRatAccorpate(titNumRatAccorpate);
    }

    @Override
    public Integer getTitNumRatAccorpateObj() {
        if (ws.getIndTitCont().getNumRatAccorpate() >= 0) {
            return ((Integer)getTitNumRatAccorpate());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitNumRatAccorpateObj(Integer titNumRatAccorpateObj) {
        if (titNumRatAccorpateObj != null) {
            setTitNumRatAccorpate(((int)titNumRatAccorpateObj));
            ws.getIndTitCont().setNumRatAccorpate(((short)0));
        }
        else {
            ws.getIndTitCont().setNumRatAccorpate(((short)-1));
        }
    }

    @Override
    public int getTitProgTit() {
        return ws.getTitCont().getTitProgTit().getTitProgTit();
    }

    @Override
    public void setTitProgTit(int titProgTit) {
        this.ws.getTitCont().getTitProgTit().setTitProgTit(titProgTit);
    }

    @Override
    public Integer getTitProgTitObj() {
        if (ws.getIndTitCont().getProgTit() >= 0) {
            return ((Integer)getTitProgTit());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitProgTitObj(Integer titProgTitObj) {
        if (titProgTitObj != null) {
            setTitProgTit(((int)titProgTitObj));
            ws.getIndTitCont().setProgTit(((short)0));
        }
        else {
            ws.getIndTitCont().setProgTit(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotAcqExp() {
        return ws.getTitCont().getTitTotAcqExp().getTitTotAcqExp();
    }

    @Override
    public void setTitTotAcqExp(AfDecimal titTotAcqExp) {
        this.ws.getTitCont().getTitTotAcqExp().setTitTotAcqExp(titTotAcqExp.copy());
    }

    @Override
    public AfDecimal getTitTotAcqExpObj() {
        if (ws.getIndTitCont().getTotAcqExp() >= 0) {
            return getTitTotAcqExp();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotAcqExpObj(AfDecimal titTotAcqExpObj) {
        if (titTotAcqExpObj != null) {
            setTitTotAcqExp(new AfDecimal(titTotAcqExpObj, 15, 3));
            ws.getIndTitCont().setTotAcqExp(((short)0));
        }
        else {
            ws.getIndTitCont().setTotAcqExp(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotCarAcq() {
        return ws.getTitCont().getTitTotCarAcq().getTitTotCarAcq();
    }

    @Override
    public void setTitTotCarAcq(AfDecimal titTotCarAcq) {
        this.ws.getTitCont().getTitTotCarAcq().setTitTotCarAcq(titTotCarAcq.copy());
    }

    @Override
    public AfDecimal getTitTotCarAcqObj() {
        if (ws.getIndTitCont().getTotCarAcq() >= 0) {
            return getTitTotCarAcq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotCarAcqObj(AfDecimal titTotCarAcqObj) {
        if (titTotCarAcqObj != null) {
            setTitTotCarAcq(new AfDecimal(titTotCarAcqObj, 15, 3));
            ws.getIndTitCont().setTotCarAcq(((short)0));
        }
        else {
            ws.getIndTitCont().setTotCarAcq(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotCarGest() {
        return ws.getTitCont().getTitTotCarGest().getTitTotCarGest();
    }

    @Override
    public void setTitTotCarGest(AfDecimal titTotCarGest) {
        this.ws.getTitCont().getTitTotCarGest().setTitTotCarGest(titTotCarGest.copy());
    }

    @Override
    public AfDecimal getTitTotCarGestObj() {
        if (ws.getIndTitCont().getTotCarGest() >= 0) {
            return getTitTotCarGest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotCarGestObj(AfDecimal titTotCarGestObj) {
        if (titTotCarGestObj != null) {
            setTitTotCarGest(new AfDecimal(titTotCarGestObj, 15, 3));
            ws.getIndTitCont().setTotCarGest(((short)0));
        }
        else {
            ws.getIndTitCont().setTotCarGest(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotCarIas() {
        return ws.getTitCont().getTitTotCarIas().getTitTotCarIas();
    }

    @Override
    public void setTitTotCarIas(AfDecimal titTotCarIas) {
        this.ws.getTitCont().getTitTotCarIas().setTitTotCarIas(titTotCarIas.copy());
    }

    @Override
    public AfDecimal getTitTotCarIasObj() {
        if (ws.getIndTitCont().getTotCarIas() >= 0) {
            return getTitTotCarIas();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotCarIasObj(AfDecimal titTotCarIasObj) {
        if (titTotCarIasObj != null) {
            setTitTotCarIas(new AfDecimal(titTotCarIasObj, 15, 3));
            ws.getIndTitCont().setTotCarIas(((short)0));
        }
        else {
            ws.getIndTitCont().setTotCarIas(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotCarInc() {
        return ws.getTitCont().getTitTotCarInc().getTitTotCarInc();
    }

    @Override
    public void setTitTotCarInc(AfDecimal titTotCarInc) {
        this.ws.getTitCont().getTitTotCarInc().setTitTotCarInc(titTotCarInc.copy());
    }

    @Override
    public AfDecimal getTitTotCarIncObj() {
        if (ws.getIndTitCont().getTotCarInc() >= 0) {
            return getTitTotCarInc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotCarIncObj(AfDecimal titTotCarIncObj) {
        if (titTotCarIncObj != null) {
            setTitTotCarInc(new AfDecimal(titTotCarIncObj, 15, 3));
            ws.getIndTitCont().setTotCarInc(((short)0));
        }
        else {
            ws.getIndTitCont().setTotCarInc(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotCnbtAntirac() {
        return ws.getTitCont().getTitTotCnbtAntirac().getTitTotCnbtAntirac();
    }

    @Override
    public void setTitTotCnbtAntirac(AfDecimal titTotCnbtAntirac) {
        this.ws.getTitCont().getTitTotCnbtAntirac().setTitTotCnbtAntirac(titTotCnbtAntirac.copy());
    }

    @Override
    public AfDecimal getTitTotCnbtAntiracObj() {
        if (ws.getIndTitCont().getTotCnbtAntirac() >= 0) {
            return getTitTotCnbtAntirac();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotCnbtAntiracObj(AfDecimal titTotCnbtAntiracObj) {
        if (titTotCnbtAntiracObj != null) {
            setTitTotCnbtAntirac(new AfDecimal(titTotCnbtAntiracObj, 15, 3));
            ws.getIndTitCont().setTotCnbtAntirac(((short)0));
        }
        else {
            ws.getIndTitCont().setTotCnbtAntirac(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotCommisInter() {
        return ws.getTitCont().getTitTotCommisInter().getTitTotCommisInter();
    }

    @Override
    public void setTitTotCommisInter(AfDecimal titTotCommisInter) {
        this.ws.getTitCont().getTitTotCommisInter().setTitTotCommisInter(titTotCommisInter.copy());
    }

    @Override
    public AfDecimal getTitTotCommisInterObj() {
        if (ws.getIndTitCont().getTotCommisInter() >= 0) {
            return getTitTotCommisInter();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotCommisInterObj(AfDecimal titTotCommisInterObj) {
        if (titTotCommisInterObj != null) {
            setTitTotCommisInter(new AfDecimal(titTotCommisInterObj, 15, 3));
            ws.getIndTitCont().setTotCommisInter(((short)0));
        }
        else {
            ws.getIndTitCont().setTotCommisInter(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotDir() {
        return ws.getTitCont().getTitTotDir().getTitTotDir();
    }

    @Override
    public void setTitTotDir(AfDecimal titTotDir) {
        this.ws.getTitCont().getTitTotDir().setTitTotDir(titTotDir.copy());
    }

    @Override
    public AfDecimal getTitTotDirObj() {
        if (ws.getIndTitCont().getTotDir() >= 0) {
            return getTitTotDir();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotDirObj(AfDecimal titTotDirObj) {
        if (titTotDirObj != null) {
            setTitTotDir(new AfDecimal(titTotDirObj, 15, 3));
            ws.getIndTitCont().setTotDir(((short)0));
        }
        else {
            ws.getIndTitCont().setTotDir(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotIntrFraz() {
        return ws.getTitCont().getTitTotIntrFraz().getTitTotIntrFraz();
    }

    @Override
    public void setTitTotIntrFraz(AfDecimal titTotIntrFraz) {
        this.ws.getTitCont().getTitTotIntrFraz().setTitTotIntrFraz(titTotIntrFraz.copy());
    }

    @Override
    public AfDecimal getTitTotIntrFrazObj() {
        if (ws.getIndTitCont().getTotIntrFraz() >= 0) {
            return getTitTotIntrFraz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotIntrFrazObj(AfDecimal titTotIntrFrazObj) {
        if (titTotIntrFrazObj != null) {
            setTitTotIntrFraz(new AfDecimal(titTotIntrFrazObj, 15, 3));
            ws.getIndTitCont().setTotIntrFraz(((short)0));
        }
        else {
            ws.getIndTitCont().setTotIntrFraz(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotIntrMora() {
        return ws.getTitCont().getTitTotIntrMora().getTitTotIntrMora();
    }

    @Override
    public void setTitTotIntrMora(AfDecimal titTotIntrMora) {
        this.ws.getTitCont().getTitTotIntrMora().setTitTotIntrMora(titTotIntrMora.copy());
    }

    @Override
    public AfDecimal getTitTotIntrMoraObj() {
        if (ws.getIndTitCont().getTotIntrMora() >= 0) {
            return getTitTotIntrMora();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotIntrMoraObj(AfDecimal titTotIntrMoraObj) {
        if (titTotIntrMoraObj != null) {
            setTitTotIntrMora(new AfDecimal(titTotIntrMoraObj, 15, 3));
            ws.getIndTitCont().setTotIntrMora(((short)0));
        }
        else {
            ws.getIndTitCont().setTotIntrMora(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotIntrPrest() {
        return ws.getTitCont().getTitTotIntrPrest().getTitTotIntrPrest();
    }

    @Override
    public void setTitTotIntrPrest(AfDecimal titTotIntrPrest) {
        this.ws.getTitCont().getTitTotIntrPrest().setTitTotIntrPrest(titTotIntrPrest.copy());
    }

    @Override
    public AfDecimal getTitTotIntrPrestObj() {
        if (ws.getIndTitCont().getTotIntrPrest() >= 0) {
            return getTitTotIntrPrest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotIntrPrestObj(AfDecimal titTotIntrPrestObj) {
        if (titTotIntrPrestObj != null) {
            setTitTotIntrPrest(new AfDecimal(titTotIntrPrestObj, 15, 3));
            ws.getIndTitCont().setTotIntrPrest(((short)0));
        }
        else {
            ws.getIndTitCont().setTotIntrPrest(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotIntrRetdt() {
        return ws.getTitCont().getTitTotIntrRetdt().getTitTotIntrRetdt();
    }

    @Override
    public void setTitTotIntrRetdt(AfDecimal titTotIntrRetdt) {
        this.ws.getTitCont().getTitTotIntrRetdt().setTitTotIntrRetdt(titTotIntrRetdt.copy());
    }

    @Override
    public AfDecimal getTitTotIntrRetdtObj() {
        if (ws.getIndTitCont().getTotIntrRetdt() >= 0) {
            return getTitTotIntrRetdt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotIntrRetdtObj(AfDecimal titTotIntrRetdtObj) {
        if (titTotIntrRetdtObj != null) {
            setTitTotIntrRetdt(new AfDecimal(titTotIntrRetdtObj, 15, 3));
            ws.getIndTitCont().setTotIntrRetdt(((short)0));
        }
        else {
            ws.getIndTitCont().setTotIntrRetdt(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotIntrRiat() {
        return ws.getTitCont().getTitTotIntrRiat().getTitTotIntrRiat();
    }

    @Override
    public void setTitTotIntrRiat(AfDecimal titTotIntrRiat) {
        this.ws.getTitCont().getTitTotIntrRiat().setTitTotIntrRiat(titTotIntrRiat.copy());
    }

    @Override
    public AfDecimal getTitTotIntrRiatObj() {
        if (ws.getIndTitCont().getTotIntrRiat() >= 0) {
            return getTitTotIntrRiat();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotIntrRiatObj(AfDecimal titTotIntrRiatObj) {
        if (titTotIntrRiatObj != null) {
            setTitTotIntrRiat(new AfDecimal(titTotIntrRiatObj, 15, 3));
            ws.getIndTitCont().setTotIntrRiat(((short)0));
        }
        else {
            ws.getIndTitCont().setTotIntrRiat(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotManfeeAntic() {
        return ws.getTitCont().getTitTotManfeeAntic().getTitTotManfeeAntic();
    }

    @Override
    public void setTitTotManfeeAntic(AfDecimal titTotManfeeAntic) {
        this.ws.getTitCont().getTitTotManfeeAntic().setTitTotManfeeAntic(titTotManfeeAntic.copy());
    }

    @Override
    public AfDecimal getTitTotManfeeAnticObj() {
        if (ws.getIndTitCont().getTotManfeeAntic() >= 0) {
            return getTitTotManfeeAntic();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotManfeeAnticObj(AfDecimal titTotManfeeAnticObj) {
        if (titTotManfeeAnticObj != null) {
            setTitTotManfeeAntic(new AfDecimal(titTotManfeeAnticObj, 15, 3));
            ws.getIndTitCont().setTotManfeeAntic(((short)0));
        }
        else {
            ws.getIndTitCont().setTotManfeeAntic(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotManfeeRec() {
        return ws.getTitCont().getTitTotManfeeRec().getTitTotManfeeRec();
    }

    @Override
    public void setTitTotManfeeRec(AfDecimal titTotManfeeRec) {
        this.ws.getTitCont().getTitTotManfeeRec().setTitTotManfeeRec(titTotManfeeRec.copy());
    }

    @Override
    public AfDecimal getTitTotManfeeRecObj() {
        if (ws.getIndTitCont().getTotManfeeRec() >= 0) {
            return getTitTotManfeeRec();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotManfeeRecObj(AfDecimal titTotManfeeRecObj) {
        if (titTotManfeeRecObj != null) {
            setTitTotManfeeRec(new AfDecimal(titTotManfeeRecObj, 15, 3));
            ws.getIndTitCont().setTotManfeeRec(((short)0));
        }
        else {
            ws.getIndTitCont().setTotManfeeRec(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotManfeeRicor() {
        return ws.getTitCont().getTitTotManfeeRicor().getTitTotManfeeRicor();
    }

    @Override
    public void setTitTotManfeeRicor(AfDecimal titTotManfeeRicor) {
        this.ws.getTitCont().getTitTotManfeeRicor().setTitTotManfeeRicor(titTotManfeeRicor.copy());
    }

    @Override
    public AfDecimal getTitTotManfeeRicorObj() {
        if (ws.getIndTitCont().getTotManfeeRicor() >= 0) {
            return getTitTotManfeeRicor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotManfeeRicorObj(AfDecimal titTotManfeeRicorObj) {
        if (titTotManfeeRicorObj != null) {
            setTitTotManfeeRicor(new AfDecimal(titTotManfeeRicorObj, 15, 3));
            ws.getIndTitCont().setTotManfeeRicor(((short)0));
        }
        else {
            ws.getIndTitCont().setTotManfeeRicor(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotPreNet() {
        return ws.getTitCont().getTitTotPreNet().getTitTotPreNet();
    }

    @Override
    public void setTitTotPreNet(AfDecimal titTotPreNet) {
        this.ws.getTitCont().getTitTotPreNet().setTitTotPreNet(titTotPreNet.copy());
    }

    @Override
    public AfDecimal getTitTotPreNetObj() {
        if (ws.getIndTitCont().getTotPreNet() >= 0) {
            return getTitTotPreNet();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotPreNetObj(AfDecimal titTotPreNetObj) {
        if (titTotPreNetObj != null) {
            setTitTotPreNet(new AfDecimal(titTotPreNetObj, 15, 3));
            ws.getIndTitCont().setTotPreNet(((short)0));
        }
        else {
            ws.getIndTitCont().setTotPreNet(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotPrePpIas() {
        return ws.getTitCont().getTitTotPrePpIas().getTitTotPrePpIas();
    }

    @Override
    public void setTitTotPrePpIas(AfDecimal titTotPrePpIas) {
        this.ws.getTitCont().getTitTotPrePpIas().setTitTotPrePpIas(titTotPrePpIas.copy());
    }

    @Override
    public AfDecimal getTitTotPrePpIasObj() {
        if (ws.getIndTitCont().getTotPrePpIas() >= 0) {
            return getTitTotPrePpIas();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotPrePpIasObj(AfDecimal titTotPrePpIasObj) {
        if (titTotPrePpIasObj != null) {
            setTitTotPrePpIas(new AfDecimal(titTotPrePpIasObj, 15, 3));
            ws.getIndTitCont().setTotPrePpIas(((short)0));
        }
        else {
            ws.getIndTitCont().setTotPrePpIas(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotPreSoloRsh() {
        return ws.getTitCont().getTitTotPreSoloRsh().getTitTotPreSoloRsh();
    }

    @Override
    public void setTitTotPreSoloRsh(AfDecimal titTotPreSoloRsh) {
        this.ws.getTitCont().getTitTotPreSoloRsh().setTitTotPreSoloRsh(titTotPreSoloRsh.copy());
    }

    @Override
    public AfDecimal getTitTotPreSoloRshObj() {
        if (ws.getIndTitCont().getTotPreSoloRsh() >= 0) {
            return getTitTotPreSoloRsh();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotPreSoloRshObj(AfDecimal titTotPreSoloRshObj) {
        if (titTotPreSoloRshObj != null) {
            setTitTotPreSoloRsh(new AfDecimal(titTotPreSoloRshObj, 15, 3));
            ws.getIndTitCont().setTotPreSoloRsh(((short)0));
        }
        else {
            ws.getIndTitCont().setTotPreSoloRsh(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotPreTot() {
        return ws.getTitCont().getTitTotPreTot().getTitTotPreTot();
    }

    @Override
    public void setTitTotPreTot(AfDecimal titTotPreTot) {
        this.ws.getTitCont().getTitTotPreTot().setTitTotPreTot(titTotPreTot.copy());
    }

    @Override
    public AfDecimal getTitTotPreTotObj() {
        if (ws.getIndTitCont().getTotPreTot() >= 0) {
            return getTitTotPreTot();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotPreTotObj(AfDecimal titTotPreTotObj) {
        if (titTotPreTotObj != null) {
            setTitTotPreTot(new AfDecimal(titTotPreTotObj, 15, 3));
            ws.getIndTitCont().setTotPreTot(((short)0));
        }
        else {
            ws.getIndTitCont().setTotPreTot(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotProvAcq1aa() {
        return ws.getTitCont().getTitTotProvAcq1aa().getTitTotProvAcq1aa();
    }

    @Override
    public void setTitTotProvAcq1aa(AfDecimal titTotProvAcq1aa) {
        this.ws.getTitCont().getTitTotProvAcq1aa().setTitTotProvAcq1aa(titTotProvAcq1aa.copy());
    }

    @Override
    public AfDecimal getTitTotProvAcq1aaObj() {
        if (ws.getIndTitCont().getTotProvAcq1aa() >= 0) {
            return getTitTotProvAcq1aa();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotProvAcq1aaObj(AfDecimal titTotProvAcq1aaObj) {
        if (titTotProvAcq1aaObj != null) {
            setTitTotProvAcq1aa(new AfDecimal(titTotProvAcq1aaObj, 15, 3));
            ws.getIndTitCont().setTotProvAcq1aa(((short)0));
        }
        else {
            ws.getIndTitCont().setTotProvAcq1aa(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotProvAcq2aa() {
        return ws.getTitCont().getTitTotProvAcq2aa().getTitTotProvAcq2aa();
    }

    @Override
    public void setTitTotProvAcq2aa(AfDecimal titTotProvAcq2aa) {
        this.ws.getTitCont().getTitTotProvAcq2aa().setTitTotProvAcq2aa(titTotProvAcq2aa.copy());
    }

    @Override
    public AfDecimal getTitTotProvAcq2aaObj() {
        if (ws.getIndTitCont().getTotProvAcq2aa() >= 0) {
            return getTitTotProvAcq2aa();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotProvAcq2aaObj(AfDecimal titTotProvAcq2aaObj) {
        if (titTotProvAcq2aaObj != null) {
            setTitTotProvAcq2aa(new AfDecimal(titTotProvAcq2aaObj, 15, 3));
            ws.getIndTitCont().setTotProvAcq2aa(((short)0));
        }
        else {
            ws.getIndTitCont().setTotProvAcq2aa(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotProvDaRec() {
        return ws.getTitCont().getTitTotProvDaRec().getTitTotProvDaRec();
    }

    @Override
    public void setTitTotProvDaRec(AfDecimal titTotProvDaRec) {
        this.ws.getTitCont().getTitTotProvDaRec().setTitTotProvDaRec(titTotProvDaRec.copy());
    }

    @Override
    public AfDecimal getTitTotProvDaRecObj() {
        if (ws.getIndTitCont().getTotProvDaRec() >= 0) {
            return getTitTotProvDaRec();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotProvDaRecObj(AfDecimal titTotProvDaRecObj) {
        if (titTotProvDaRecObj != null) {
            setTitTotProvDaRec(new AfDecimal(titTotProvDaRecObj, 15, 3));
            ws.getIndTitCont().setTotProvDaRec(((short)0));
        }
        else {
            ws.getIndTitCont().setTotProvDaRec(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotProvInc() {
        return ws.getTitCont().getTitTotProvInc().getTitTotProvInc();
    }

    @Override
    public void setTitTotProvInc(AfDecimal titTotProvInc) {
        this.ws.getTitCont().getTitTotProvInc().setTitTotProvInc(titTotProvInc.copy());
    }

    @Override
    public AfDecimal getTitTotProvIncObj() {
        if (ws.getIndTitCont().getTotProvInc() >= 0) {
            return getTitTotProvInc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotProvIncObj(AfDecimal titTotProvIncObj) {
        if (titTotProvIncObj != null) {
            setTitTotProvInc(new AfDecimal(titTotProvIncObj, 15, 3));
            ws.getIndTitCont().setTotProvInc(((short)0));
        }
        else {
            ws.getIndTitCont().setTotProvInc(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotProvRicor() {
        return ws.getTitCont().getTitTotProvRicor().getTitTotProvRicor();
    }

    @Override
    public void setTitTotProvRicor(AfDecimal titTotProvRicor) {
        this.ws.getTitCont().getTitTotProvRicor().setTitTotProvRicor(titTotProvRicor.copy());
    }

    @Override
    public AfDecimal getTitTotProvRicorObj() {
        if (ws.getIndTitCont().getTotProvRicor() >= 0) {
            return getTitTotProvRicor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotProvRicorObj(AfDecimal titTotProvRicorObj) {
        if (titTotProvRicorObj != null) {
            setTitTotProvRicor(new AfDecimal(titTotProvRicorObj, 15, 3));
            ws.getIndTitCont().setTotProvRicor(((short)0));
        }
        else {
            ws.getIndTitCont().setTotProvRicor(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotRemunAss() {
        return ws.getTitCont().getTitTotRemunAss().getTitTotRemunAss();
    }

    @Override
    public void setTitTotRemunAss(AfDecimal titTotRemunAss) {
        this.ws.getTitCont().getTitTotRemunAss().setTitTotRemunAss(titTotRemunAss.copy());
    }

    @Override
    public AfDecimal getTitTotRemunAssObj() {
        if (ws.getIndTitCont().getTotRemunAss() >= 0) {
            return getTitTotRemunAss();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotRemunAssObj(AfDecimal titTotRemunAssObj) {
        if (titTotRemunAssObj != null) {
            setTitTotRemunAss(new AfDecimal(titTotRemunAssObj, 15, 3));
            ws.getIndTitCont().setTotRemunAss(((short)0));
        }
        else {
            ws.getIndTitCont().setTotRemunAss(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotSoprAlt() {
        return ws.getTitCont().getTitTotSoprAlt().getTitTotSoprAlt();
    }

    @Override
    public void setTitTotSoprAlt(AfDecimal titTotSoprAlt) {
        this.ws.getTitCont().getTitTotSoprAlt().setTitTotSoprAlt(titTotSoprAlt.copy());
    }

    @Override
    public AfDecimal getTitTotSoprAltObj() {
        if (ws.getIndTitCont().getTotSoprAlt() >= 0) {
            return getTitTotSoprAlt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotSoprAltObj(AfDecimal titTotSoprAltObj) {
        if (titTotSoprAltObj != null) {
            setTitTotSoprAlt(new AfDecimal(titTotSoprAltObj, 15, 3));
            ws.getIndTitCont().setTotSoprAlt(((short)0));
        }
        else {
            ws.getIndTitCont().setTotSoprAlt(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotSoprProf() {
        return ws.getTitCont().getTitTotSoprProf().getTitTotSoprProf();
    }

    @Override
    public void setTitTotSoprProf(AfDecimal titTotSoprProf) {
        this.ws.getTitCont().getTitTotSoprProf().setTitTotSoprProf(titTotSoprProf.copy());
    }

    @Override
    public AfDecimal getTitTotSoprProfObj() {
        if (ws.getIndTitCont().getTotSoprProf() >= 0) {
            return getTitTotSoprProf();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotSoprProfObj(AfDecimal titTotSoprProfObj) {
        if (titTotSoprProfObj != null) {
            setTitTotSoprProf(new AfDecimal(titTotSoprProfObj, 15, 3));
            ws.getIndTitCont().setTotSoprProf(((short)0));
        }
        else {
            ws.getIndTitCont().setTotSoprProf(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotSoprSan() {
        return ws.getTitCont().getTitTotSoprSan().getTitTotSoprSan();
    }

    @Override
    public void setTitTotSoprSan(AfDecimal titTotSoprSan) {
        this.ws.getTitCont().getTitTotSoprSan().setTitTotSoprSan(titTotSoprSan.copy());
    }

    @Override
    public AfDecimal getTitTotSoprSanObj() {
        if (ws.getIndTitCont().getTotSoprSan() >= 0) {
            return getTitTotSoprSan();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotSoprSanObj(AfDecimal titTotSoprSanObj) {
        if (titTotSoprSanObj != null) {
            setTitTotSoprSan(new AfDecimal(titTotSoprSanObj, 15, 3));
            ws.getIndTitCont().setTotSoprSan(((short)0));
        }
        else {
            ws.getIndTitCont().setTotSoprSan(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotSoprSpo() {
        return ws.getTitCont().getTitTotSoprSpo().getTitTotSoprSpo();
    }

    @Override
    public void setTitTotSoprSpo(AfDecimal titTotSoprSpo) {
        this.ws.getTitCont().getTitTotSoprSpo().setTitTotSoprSpo(titTotSoprSpo.copy());
    }

    @Override
    public AfDecimal getTitTotSoprSpoObj() {
        if (ws.getIndTitCont().getTotSoprSpo() >= 0) {
            return getTitTotSoprSpo();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotSoprSpoObj(AfDecimal titTotSoprSpoObj) {
        if (titTotSoprSpoObj != null) {
            setTitTotSoprSpo(new AfDecimal(titTotSoprSpoObj, 15, 3));
            ws.getIndTitCont().setTotSoprSpo(((short)0));
        }
        else {
            ws.getIndTitCont().setTotSoprSpo(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotSoprTec() {
        return ws.getTitCont().getTitTotSoprTec().getTitTotSoprTec();
    }

    @Override
    public void setTitTotSoprTec(AfDecimal titTotSoprTec) {
        this.ws.getTitCont().getTitTotSoprTec().setTitTotSoprTec(titTotSoprTec.copy());
    }

    @Override
    public AfDecimal getTitTotSoprTecObj() {
        if (ws.getIndTitCont().getTotSoprTec() >= 0) {
            return getTitTotSoprTec();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotSoprTecObj(AfDecimal titTotSoprTecObj) {
        if (titTotSoprTecObj != null) {
            setTitTotSoprTec(new AfDecimal(titTotSoprTecObj, 15, 3));
            ws.getIndTitCont().setTotSoprTec(((short)0));
        }
        else {
            ws.getIndTitCont().setTotSoprTec(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotSpeAge() {
        return ws.getTitCont().getTitTotSpeAge().getTitTotSpeAge();
    }

    @Override
    public void setTitTotSpeAge(AfDecimal titTotSpeAge) {
        this.ws.getTitCont().getTitTotSpeAge().setTitTotSpeAge(titTotSpeAge.copy());
    }

    @Override
    public AfDecimal getTitTotSpeAgeObj() {
        if (ws.getIndTitCont().getTotSpeAge() >= 0) {
            return getTitTotSpeAge();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotSpeAgeObj(AfDecimal titTotSpeAgeObj) {
        if (titTotSpeAgeObj != null) {
            setTitTotSpeAge(new AfDecimal(titTotSpeAgeObj, 15, 3));
            ws.getIndTitCont().setTotSpeAge(((short)0));
        }
        else {
            ws.getIndTitCont().setTotSpeAge(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotSpeMed() {
        return ws.getTitCont().getTitTotSpeMed().getTitTotSpeMed();
    }

    @Override
    public void setTitTotSpeMed(AfDecimal titTotSpeMed) {
        this.ws.getTitCont().getTitTotSpeMed().setTitTotSpeMed(titTotSpeMed.copy());
    }

    @Override
    public AfDecimal getTitTotSpeMedObj() {
        if (ws.getIndTitCont().getTotSpeMed() >= 0) {
            return getTitTotSpeMed();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotSpeMedObj(AfDecimal titTotSpeMedObj) {
        if (titTotSpeMedObj != null) {
            setTitTotSpeMed(new AfDecimal(titTotSpeMedObj, 15, 3));
            ws.getIndTitCont().setTotSpeMed(((short)0));
        }
        else {
            ws.getIndTitCont().setTotSpeMed(((short)-1));
        }
    }

    @Override
    public AfDecimal getTitTotTax() {
        return ws.getTitCont().getTitTotTax().getTitTotTax();
    }

    @Override
    public void setTitTotTax(AfDecimal titTotTax) {
        this.ws.getTitCont().getTitTotTax().setTitTotTax(titTotTax.copy());
    }

    @Override
    public AfDecimal getTitTotTaxObj() {
        if (ws.getIndTitCont().getTotTax() >= 0) {
            return getTitTotTax();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTotTaxObj(AfDecimal titTotTaxObj) {
        if (titTotTaxObj != null) {
            setTitTotTax(new AfDecimal(titTotTaxObj, 15, 3));
            ws.getIndTitCont().setTotTax(((short)0));
        }
        else {
            ws.getIndTitCont().setTotTax(((short)-1));
        }
    }

    @Override
    public String getTitTpCausDispStor() {
        return ws.getTitCont().getTitTpCausDispStor();
    }

    @Override
    public void setTitTpCausDispStor(String titTpCausDispStor) {
        this.ws.getTitCont().setTitTpCausDispStor(titTpCausDispStor);
    }

    @Override
    public String getTitTpCausDispStorObj() {
        if (ws.getIndTitCont().getTpCausDispStor() >= 0) {
            return getTitTpCausDispStor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTpCausDispStorObj(String titTpCausDispStorObj) {
        if (titTpCausDispStorObj != null) {
            setTitTpCausDispStor(titTpCausDispStorObj);
            ws.getIndTitCont().setTpCausDispStor(((short)0));
        }
        else {
            ws.getIndTitCont().setTpCausDispStor(((short)-1));
        }
    }

    @Override
    public String getTitTpCausRimb() {
        return ws.getTitCont().getTitTpCausRimb();
    }

    @Override
    public void setTitTpCausRimb(String titTpCausRimb) {
        this.ws.getTitCont().setTitTpCausRimb(titTpCausRimb);
    }

    @Override
    public String getTitTpCausRimbObj() {
        if (ws.getIndTitCont().getTpCausRimb() >= 0) {
            return getTitTpCausRimb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTpCausRimbObj(String titTpCausRimbObj) {
        if (titTpCausRimbObj != null) {
            setTitTpCausRimb(titTpCausRimbObj);
            ws.getIndTitCont().setTpCausRimb(((short)0));
        }
        else {
            ws.getIndTitCont().setTpCausRimb(((short)-1));
        }
    }

    @Override
    public int getTitTpCausStor() {
        return ws.getTitCont().getTitTpCausStor().getTitTpCausStor();
    }

    @Override
    public void setTitTpCausStor(int titTpCausStor) {
        this.ws.getTitCont().getTitTpCausStor().setTitTpCausStor(titTpCausStor);
    }

    @Override
    public Integer getTitTpCausStorObj() {
        if (ws.getIndTitCont().getTpCausStor() >= 0) {
            return ((Integer)getTitTpCausStor());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTpCausStorObj(Integer titTpCausStorObj) {
        if (titTpCausStorObj != null) {
            setTitTpCausStor(((int)titTpCausStorObj));
            ws.getIndTitCont().setTpCausStor(((short)0));
        }
        else {
            ws.getIndTitCont().setTpCausStor(((short)-1));
        }
    }

    @Override
    public String getTitTpEsiRid() {
        return ws.getTitCont().getTitTpEsiRid();
    }

    @Override
    public void setTitTpEsiRid(String titTpEsiRid) {
        this.ws.getTitCont().setTitTpEsiRid(titTpEsiRid);
    }

    @Override
    public String getTitTpEsiRidObj() {
        if (ws.getIndTitCont().getTpEsiRid() >= 0) {
            return getTitTpEsiRid();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTpEsiRidObj(String titTpEsiRidObj) {
        if (titTpEsiRidObj != null) {
            setTitTpEsiRid(titTpEsiRidObj);
            ws.getIndTitCont().setTpEsiRid(((short)0));
        }
        else {
            ws.getIndTitCont().setTpEsiRid(((short)-1));
        }
    }

    @Override
    public String getTitTpMezPagAdd() {
        return ws.getTitCont().getTitTpMezPagAdd();
    }

    @Override
    public void setTitTpMezPagAdd(String titTpMezPagAdd) {
        this.ws.getTitCont().setTitTpMezPagAdd(titTpMezPagAdd);
    }

    @Override
    public String getTitTpMezPagAddObj() {
        if (ws.getIndTitCont().getTpMezPagAdd() >= 0) {
            return getTitTpMezPagAdd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTpMezPagAddObj(String titTpMezPagAddObj) {
        if (titTpMezPagAddObj != null) {
            setTitTpMezPagAdd(titTpMezPagAddObj);
            ws.getIndTitCont().setTpMezPagAdd(((short)0));
        }
        else {
            ws.getIndTitCont().setTpMezPagAdd(((short)-1));
        }
    }

    @Override
    public String getTitTpOgg() {
        return ws.getTitCont().getTitTpOgg();
    }

    @Override
    public void setTitTpOgg(String titTpOgg) {
        this.ws.getTitCont().setTitTpOgg(titTpOgg);
    }

    @Override
    public String getTitTpPreTit() {
        return ws.getTitCont().getTitTpPreTit();
    }

    @Override
    public void setTitTpPreTit(String titTpPreTit) {
        this.ws.getTitCont().setTitTpPreTit(titTpPreTit);
    }

    @Override
    public String getTitTpStatTit() {
        return ws.getTitCont().getTitTpStatTit();
    }

    @Override
    public void setTitTpStatTit(String titTpStatTit) {
        this.ws.getTitCont().setTitTpStatTit(titTpStatTit);
    }

    @Override
    public String getTitTpTit() {
        return ws.getTitCont().getTitTpTit();
    }

    @Override
    public void setTitTpTit(String titTpTit) {
        this.ws.getTitCont().setTitTpTit(titTpTit);
    }

    @Override
    public String getTitTpTitMigraz() {
        return ws.getTitCont().getTitTpTitMigraz();
    }

    @Override
    public void setTitTpTitMigraz(String titTpTitMigraz) {
        this.ws.getTitCont().setTitTpTitMigraz(titTpTitMigraz);
    }

    @Override
    public String getTitTpTitMigrazObj() {
        if (ws.getIndTitCont().getTpTitMigraz() >= 0) {
            return getTitTpTitMigraz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitTpTitMigrazObj(String titTpTitMigrazObj) {
        if (titTpTitMigrazObj != null) {
            setTitTpTitMigraz(titTpTitMigrazObj);
            ws.getIndTitCont().setTpTitMigraz(((short)0));
        }
        else {
            ws.getIndTitCont().setTpTitMigraz(((short)-1));
        }
    }

    @Override
    public String getWsDataInizioEffettoDb() {
        return ws.getIdsv0010().getWsDataInizioEffettoDb();
    }

    @Override
    public void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb) {
        this.ws.getIdsv0010().setWsDataInizioEffettoDb(wsDataInizioEffettoDb);
    }

    @Override
    public long getWsTsCompetenza() {
        return ws.getIdsv0010().getWsTsCompetenza();
    }

    @Override
    public void setWsTsCompetenza(long wsTsCompetenza) {
        this.ws.getIdsv0010().setWsTsCompetenza(wsTsCompetenza);
    }
}
