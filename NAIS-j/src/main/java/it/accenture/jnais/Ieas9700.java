package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.util.date.CalendarUtil;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.LogErroreDao;
import it.accenture.jnais.copy.Idsv0001LogErrore;
import it.accenture.jnais.copy.LogErrore;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.AreaIdsv0001;
import it.accenture.jnais.ws.Ieai9701Area;
import it.accenture.jnais.ws.Ieao9701Area;
import it.accenture.jnais.ws.Ieas9700Data;
import it.accenture.jnais.ws.WsVariabili;

/**Original name: IEAS9700<br>
 * <pre>****************************************************************
 *                                                                *
 *                    IDENTIFICATION DIVISION                     *
 *                                                                *
 * ****************************************************************
 * AUTHOR.        ACCENTURE.
 * DATE-WRITTEN.  18/07/06.
 * ****************************************************************
 *                                                                *
 *     NOME :           IEAS9700                                  *
 *     TIPO :           ROUTINE COBOL-DB2                         *
 *     DESCRIZIONE :    ROUTINE RICERCA GRAVITA ERRORE            *
 *                                                                *
 *     AREE DI PASSAGGIO DATI                                     *
 *                                                                *
 *     DATI DI INPUT : IDSV0001, IEAI9701                         *
 *     DATI DI OUTPUT: IEAO9701                                   *
 *                                                                *
 * ****************************************************************
 *  LOG MODIFICHE :                                               *
 *                                                                *
 *                                                                *
 * ****************************************************************
 * ****************************************************************
 *                                                                *
 *                    ENVIRONMENT  DIVISION                       *
 *                                                                *
 * ****************************************************************
 * SOURCE-COMPUTER. IBM-370 WITH DEBUGGING MODE.
 * OBJECT-COMPUTER. IBM-370.
 *     NULL-IND     Carattere usato per indicare campi NULL;</pre>*/
public class Ieas9700 extends Program {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private LogErroreDao logErroreDao = new LogErroreDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Ieas9700Data ws = new Ieas9700Data();
    //Original name: AREA-IDSV0001
    private AreaIdsv0001 areaIdsv0001;
    //Original name: IEAI9701-AREA
    private Ieai9701Area ieai9701Area;
    //Original name: IEAO9701-AREA
    private Ieao9701Area ieao9701Area;

    //==== METHODS ====
    /**Original name: MAIN_SUBROUTINE<br>*/
    public long execute(AreaIdsv0001 areaIdsv0001, Ieai9701Area ieai9701Area, Ieao9701Area ieao9701Area) {
        this.areaIdsv0001 = areaIdsv0001;
        this.ieai9701Area = ieai9701Area;
        this.ieao9701Area = ieao9701Area;
        principale();
        principaleFine();
        return 0;
    }

    public static Ieas9700 getInstance() {
        return ((Ieas9700)Programs.getInstance(Ieas9700.class));
    }

    /**Original name: 1000-PRINCIPALE<br>
	 * <pre>****************************************************************
	 *                                                                *
	 *                    1000-PRINCIPALE                             *
	 *                                                                *
	 * ****************************************************************
	 * -- INIZIALIZZO IL FLAG PER L'ESITO DELL'ELABORAZIONE</pre>*/
    private void principale() {
        // COB_CODE: SET SI-CORRETTO                         TO TRUE.
        ws.getWsEsito().setSiCorretto();
        //-- INIZIALIZZO I CAMPI DI WORKING
        // COB_CODE: MOVE SPACES                             TO WS-VARIABILI.
        ws.getWsVariabili().initWsVariabiliSpaces();
        //-- CONTROLLO COERENZA DEI DATI IN INPUT
        // COB_CODE: PERFORM 1100-CHK-FORMALI-INPUT
        //              THRU 1100-CHK-FORMALI-INPUT-EXIT.
        chkFormaliInput();
        //-- ESTRAZIONE PROGRESSIVO PER TABELLA LOG_ERRORE
        // COB_CODE: IF SI-CORRETTO
        //                 THRU 1200-ESTRAI-PROGRESSIVO-EXIT
        //           END-IF.
        if (ws.getWsEsito().isSiCorretto()) {
            // COB_CODE: PERFORM 1200-ESTRAI-PROGRESSIVO
            //              THRU 1200-ESTRAI-PROGRESSIVO-EXIT
            estraiProgressivo();
        }
        //-- VALORIZZAZIONE CAMPI DCLGEN
        // COB_CODE: IF SI-CORRETTO
        //                 THRU 1300-VALORIZZAZIONE-CAMPI-EXIT
        //           END-IF.
        if (ws.getWsEsito().isSiCorretto()) {
            // COB_CODE: PERFORM 1300-VALORIZZAZIONE-CAMPI
            //              THRU 1300-VALORIZZAZIONE-CAMPI-EXIT
            valorizzazioneCampi();
        }
        //-- INSERIMENTO DEI DATI IN TABELLA
        // COB_CODE: IF SI-CORRETTO
        //           END-IF.
        if (ws.getWsEsito().isSiCorretto()) {
            // COB_CODE: PERFORM 1400-INSERIMENTO-LOG
            //              THRU 1400-INSERIMENTO-LOG-EXIT
            inserimentoLog();
            //-- VALORIZZAZIONE CAMPI DI OUTPUT
            // COB_CODE: IF SI-CORRETTO
            //                 THRU 1500-VALORIZZA-OUTPUT-EXIT
            //           END-IF.
            if (ws.getWsEsito().isSiCorretto()) {
                // COB_CODE: PERFORM 1500-VALORIZZA-OUTPUT
                //              THRU 1500-VALORIZZA-OUTPUT-EXIT
                valorizzaOutput();
            }
        }
    }

    /**Original name: 1000-PRINCIPALE-FINE<br>*/
    private void principaleFine() {
        // COB_CODE: EXIT.
        //exit
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    /**Original name: 1100-CHK-FORMALI-INPUT<br>
	 * <pre>-- CONTROLLO DI NUMERICITA SUL CAMPO DI GRAVITA ERRORE</pre>*/
    private void chkFormaliInput() {
        // COB_CODE: IF IEAI9701-ID-GRAVITA-ERRORE IS NOT NUMERIC
        //              MOVE 04                        TO  IEAO9701-COD-ERRORE-990
        //           END-IF.
        if (!Functions.isNumber(ieai9701Area.getIeai9701IdGravitaErroreFormatted())) {
            // COB_CODE: SET NO-CORRETTO                TO TRUE
            ws.getWsEsito().setNoCorretto();
            // COB_CODE: MOVE 04                        TO  IEAO9701-COD-ERRORE-990
            ieao9701Area.setCodErrore990(4);
        }
        //-- CONTROLLO DI NUMERICITA SUL CAMPO SESSIONE
        // COB_CODE: IF SI-CORRETTO
        //              END-IF
        //           END-IF.
        if (ws.getWsEsito().isSiCorretto()) {
            // COB_CODE: MOVE IDSV0001-SESSIONE         TO WS-SESSIONE-X
            ws.getWsVariabili().setSessioneXFormatted(areaIdsv0001.getAreaComune().getSessioneFormatted());
            // COB_CODE: IF WS-SESSIONE IS NOT NUMERIC
            //              MOVE 05                     TO IEAO9701-COD-ERRORE-990
            //           END-IF
            if (!Functions.isNumber(ws.getWsVariabili().getSessioneFormatted())) {
                // COB_CODE: SET NO-CORRETTO             TO TRUE
                ws.getWsEsito().setNoCorretto();
                // COB_CODE: MOVE 05                     TO IEAO9701-COD-ERRORE-990
                ieao9701Area.setCodErrore990(5);
            }
        }
        //-- CONTROLLO CAMPI ALFANUMERICI
        // COB_CODE: IF SI-CORRETTO
        //              END-IF
        //           END-IF.
        if (ws.getWsEsito().isSiCorretto()) {
            // COB_CODE: MOVE IDSV0001-DESC-ERRORE-ESTESA  OF IDSV0001-LOG-ERRORE
            //                                          TO WS-DESCR-ERRORE
            ws.getWsVariabili().setDescrErrore(areaIdsv0001.getLogErrore().getDescErroreEstesa());
            // COB_CODE: IF WS-DESCR-ERRORE = SPACES
            //           OR WS-DESCR-ERRORE = HIGH-VALUE
            //           OR WS-DESCR-ERRORE = LOW-VALUE
            //              MOVE 06                     TO IEAO9701-COD-ERRORE-990
            //           END-IF
            if (Characters.EQ_SPACE.test(ws.getWsVariabili().getDescrErrore()) || Characters.EQ_HIGH.test(ws.getWsVariabili().getDescrErrore(), WsVariabili.Len.DESCR_ERRORE) || Characters.EQ_LOW.test(ws.getWsVariabili().getDescrErrore(), WsVariabili.Len.DESCR_ERRORE)) {
                // COB_CODE: SET NO-CORRETTO             TO TRUE
                ws.getWsEsito().setNoCorretto();
                // COB_CODE: MOVE 06                     TO IEAO9701-COD-ERRORE-990
                ieao9701Area.setCodErrore990(6);
            }
        }
        // COB_CODE: IF SI-CORRETTO
        //              END-IF
        //           END-IF.
        if (ws.getWsEsito().isSiCorretto()) {
            // COB_CODE: IF IDSV0001-COD-MAIN-BATCH = SPACES
            //           OR IDSV0001-COD-MAIN-BATCH = HIGH-VALUE
            //           OR IDSV0001-COD-MAIN-BATCH = LOW-VALUE
            //              MOVE 07                     TO IEAO9701-COD-ERRORE-990
            //           END-IF
            if (Characters.EQ_SPACE.test(areaIdsv0001.getAreaComune().getCodMainBatch()) || Characters.EQ_HIGH.test(areaIdsv0001.getAreaComune().getCodMainBatchFormatted()) || Characters.EQ_LOW.test(areaIdsv0001.getAreaComune().getCodMainBatchFormatted())) {
                // COB_CODE: SET NO-CORRETTO             TO TRUE
                ws.getWsEsito().setNoCorretto();
                // COB_CODE: MOVE 07                     TO IEAO9701-COD-ERRORE-990
                ieao9701Area.setCodErrore990(7);
            }
        }
        // COB_CODE: IF SI-CORRETTO
        //              END-IF
        //           END-IF.
        if (ws.getWsEsito().isSiCorretto()) {
            // COB_CODE: IF IDSV0001-COD-SERVIZIO-BE = SPACES
            //           OR IDSV0001-COD-SERVIZIO-BE = HIGH-VALUE
            //           OR IDSV0001-COD-SERVIZIO-BE = LOW-VALUE
            //              MOVE 08                     TO IEAO9701-COD-ERRORE-990
            //           END-IF
            if (Characters.EQ_SPACE.test(areaIdsv0001.getLogErrore().getCodServizioBe()) || Characters.EQ_HIGH.test(areaIdsv0001.getLogErrore().getCodServizioBeFormatted()) || Characters.EQ_LOW.test(areaIdsv0001.getLogErrore().getCodServizioBeFormatted())) {
                // COB_CODE: SET NO-CORRETTO             TO TRUE
                ws.getWsEsito().setNoCorretto();
                // COB_CODE: MOVE 08                     TO IEAO9701-COD-ERRORE-990
                ieao9701Area.setCodErrore990(8);
            }
        }
        // COB_CODE: IF SI-CORRETTO
        //              END-IF
        //           END-IF.
        if (ws.getWsEsito().isSiCorretto()) {
            // COB_CODE: IF IDSV0001-LABEL-ERR = SPACES
            //           OR IDSV0001-LABEL-ERR = HIGH-VALUE
            //           OR IDSV0001-LABEL-ERR = LOW-VALUE
            //              MOVE 09                     TO IEAO9701-COD-ERRORE-990
            //           END-IF
            if (Characters.EQ_SPACE.test(areaIdsv0001.getLogErrore().getLabelErr()) || Characters.EQ_HIGH.test(areaIdsv0001.getLogErrore().getLabelErr(), Idsv0001LogErrore.Len.LABEL_ERR) || Characters.EQ_LOW.test(areaIdsv0001.getLogErrore().getLabelErr(), Idsv0001LogErrore.Len.LABEL_ERR)) {
                // COB_CODE: SET NO-CORRETTO             TO TRUE
                ws.getWsEsito().setNoCorretto();
                // COB_CODE: MOVE 09                     TO IEAO9701-COD-ERRORE-990
                ieao9701Area.setCodErrore990(9);
            }
        }
        // COB_CODE: IF    IDSV0001-OPER-TABELLA = HIGH-VALUE
        //              OR IDSV0001-OPER-TABELLA = LOW-VALUE
        //              MOVE SPACE  TO IDSV0001-OPER-TABELLA.
        if (Characters.EQ_HIGH.test(areaIdsv0001.getLogErrore().getOperTabellaFormatted()) || Characters.EQ_LOW.test(areaIdsv0001.getLogErrore().getOperTabellaFormatted())) {
            // COB_CODE: MOVE SPACE  TO IDSV0001-OPER-TABELLA.
            areaIdsv0001.getLogErrore().setOperTabella("");
        }
        // COB_CODE: IF    IDSV0001-NOME-TABELLA = HIGH-VALUE
        //              OR IDSV0001-NOME-TABELLA = LOW-VALUE
        //              MOVE SPACE  TO IDSV0001-NOME-TABELLA.
        if (Characters.EQ_HIGH.test(areaIdsv0001.getLogErrore().getNomeTabellaFormatted()) || Characters.EQ_LOW.test(areaIdsv0001.getLogErrore().getNomeTabellaFormatted())) {
            // COB_CODE: MOVE SPACE  TO IDSV0001-NOME-TABELLA.
            areaIdsv0001.getLogErrore().setNomeTabella("");
        }
        // COB_CODE: IF    IDSV0001-STATUS-TABELLA = HIGH-VALUE
        //              OR IDSV0001-STATUS-TABELLA = LOW-VALUE
        //              MOVE SPACE  TO IDSV0001-STATUS-TABELLA.
        if (Characters.EQ_HIGH.test(areaIdsv0001.getLogErrore().getStatusTabellaFormatted()) || Characters.EQ_LOW.test(areaIdsv0001.getLogErrore().getStatusTabellaFormatted())) {
            // COB_CODE: MOVE SPACE  TO IDSV0001-STATUS-TABELLA.
            areaIdsv0001.getLogErrore().setStatusTabella("");
        }
        // COB_CODE: IF    IDSV0001-KEY-TABELLA = HIGH-VALUE
        //              OR IDSV0001-KEY-TABELLA = LOW-VALUE
        //              MOVE SPACE  TO IDSV0001-KEY-TABELLA.
        if (Characters.EQ_HIGH.test(areaIdsv0001.getLogErrore().getKeyTabellaFormatted()) || Characters.EQ_LOW.test(areaIdsv0001.getLogErrore().getKeyTabellaFormatted())) {
            // COB_CODE: MOVE SPACE  TO IDSV0001-KEY-TABELLA.
            areaIdsv0001.getLogErrore().setKeyTabella("");
        }
    }

    /**Original name: 1200-ESTRAI-PROGRESSIVO<br>
	 * <pre>-- ESTRAZIONE DELL'ULTIMO PROGRESSIVO DALLA TABELLA DI LOG</pre>*/
    private void estraiProgressivo() {
        // COB_CODE: MOVE WS-SESSIONE                  TO LOR-ID-LOG-ERRORE.
        ws.getLogErrore().setLorIdLogErrore(ws.getWsVariabili().getSessione());
        // COB_CODE: MOVE ZEROES                       TO WS-PRO-LOG-ERRORE.
        ws.getWsVariabili().setProLogErrore(0);
        // COB_CODE: EXEC SQL
        //                SELECT MAX(PROG_LOG_ERRORE)
        //                 INTO :WS-PRO-LOG-ERRORE
        //                 FROM LOG_ERRORE
        //                 WHERE ID_LOG_ERRORE = :LOR-ID-LOG-ERRORE
        //           END-EXEC.
        ws.getWsVariabili().setProLogErrore(logErroreDao.selectByLorIdLogErrore1(ws.getLogErrore().getLorIdLogErrore(), ws.getWsVariabili().getProLogErrore()));
        //-- CONTROLLO L'ESITO DELLA SELECT ANALIZZANDO L'SQLCODE
        // COB_CODE: EVALUATE SQLCODE
        //              WHEN 0
        //                 MOVE ZEROES                 TO IEAO9701-COD-ERRORE-990
        //              WHEN 100
        //                 MOVE ZEROES                 TO IEAO9701-COD-ERRORE-990
        //              WHEN -305
        //                 MOVE ZEROES                 TO IEAO9701-COD-ERRORE-990
        //              WHEN OTHER
        //                 MOVE 99                     TO IEAO9701-COD-ERRORE-990
        //           END-EVALUATE.
        switch (sqlca.getSqlcode()) {

            case 0:// COB_CODE: ADD  1                      TO WS-PRO-LOG-ERRORE
                ws.getWsVariabili().setProLogErrore(Trunc.toInt(1 + ws.getWsVariabili().getProLogErrore(), 5));
                // COB_CODE: MOVE ZEROES                 TO IEAO9701-COD-ERRORE-990
                ieao9701Area.setCodErrore990(0);
                break;

            case 100:// COB_CODE: MOVE 1                      TO WS-PRO-LOG-ERRORE
                ws.getWsVariabili().setProLogErrore(1);
                // COB_CODE: MOVE ZEROES                 TO IEAO9701-COD-ERRORE-990
                ieao9701Area.setCodErrore990(0);
                break;

            case -305:// COB_CODE: MOVE 1                      TO WS-PRO-LOG-ERRORE
                ws.getWsVariabili().setProLogErrore(1);
                // COB_CODE: MOVE ZEROES                 TO IEAO9701-COD-ERRORE-990
                ieao9701Area.setCodErrore990(0);
                break;

            default:// COB_CODE: SET NO-CORRETTO             TO TRUE
                ws.getWsEsito().setNoCorretto();
                // COB_CODE: MOVE 99                     TO IEAO9701-COD-ERRORE-990
                ieao9701Area.setCodErrore990(99);
                break;
        }
    }

    /**Original name: 1300-VALORIZZAZIONE-CAMPI<br>
	 * <pre>-- VALORIZZAZIONE CAMPI DELLA DCLGEN PER TABELLA DI LOG</pre>*/
    private void valorizzazioneCampi() {
        // COB_CODE: INITIALIZE LOG-ERRORE.
        initLogErrore();
        // COB_CODE: MOVE WS-SESSIONE             TO LOR-ID-LOG-ERRORE.
        ws.getLogErrore().setLorIdLogErrore(ws.getWsVariabili().getSessione());
        // COB_CODE: MOVE WS-PRO-LOG-ERRORE       TO LOR-PROG-LOG-ERRORE.
        ws.getLogErrore().setLorProgLogErrore(ws.getWsVariabili().getProLogErrore());
        // COB_CODE: MOVE IEAI9701-ID-GRAVITA-ERRORE
        //                                        TO LOR-ID-GRAVITA-ERRORE.
        ws.getLogErrore().setLorIdGravitaErrore(ieai9701Area.getIeai9701IdGravitaErrore());
        // COB_CODE: MOVE IDSV0001-DESC-ERRORE-ESTESA
        //                                        TO LOR-DESC-ERRORE-ESTESA
        ws.getLogErrore().setLorDescErroreEstesa(areaIdsv0001.getLogErrore().getDescErroreEstesa());
        // COB_CODE: MOVE IDSV0001-COD-MAIN-BATCH
        //                                        TO LOR-COD-MAIN-BATCH.
        ws.getLogErrore().setLorCodMainBatch(areaIdsv0001.getAreaComune().getCodMainBatch());
        // COB_CODE: MOVE IDSV0001-COD-SERVIZIO-BE
        //                                        TO LOR-COD-SERVIZIO-BE.
        ws.getLogErrore().setLorCodServizioBe(areaIdsv0001.getLogErrore().getCodServizioBe());
        // COB_CODE: MOVE IDSV0001-LABEL-ERR      TO LOR-LABEL-ERR
        ws.getLogErrore().setLorLabelErr(areaIdsv0001.getLogErrore().getLabelErr());
        // COB_CODE: MOVE 0                       TO IND-LOR-LABEL-ERR
        ws.getIndLogErrore().setLabelErr(((short)0));
        // COB_CODE: MOVE IDSV0001-OPER-TABELLA   TO LOR-OPER-TABELLA.
        ws.getLogErrore().setLorOperTabella(areaIdsv0001.getLogErrore().getOperTabella());
        // COB_CODE: MOVE 0                       TO IND-LOR-OPER-TABELLA
        ws.getIndLogErrore().setOperTabella(((short)0));
        // COB_CODE: MOVE IDSV0001-NOME-TABELLA   TO LOR-NOME-TABELLA
        ws.getLogErrore().setLorNomeTabella(areaIdsv0001.getLogErrore().getNomeTabella());
        // COB_CODE: MOVE 0                       TO IND-LOR-NOME-TABELLA
        ws.getIndLogErrore().setNomeTabella(((short)0));
        // COB_CODE: MOVE IDSV0001-STATUS-TABELLA TO LOR-STATUS-TABELLA
        ws.getLogErrore().setLorStatusTabella(areaIdsv0001.getLogErrore().getStatusTabella());
        // COB_CODE: MOVE 0                       TO IND-LOR-STATUS-TABELLA
        ws.getIndLogErrore().setStatusTabella(((short)0));
        // COB_CODE: MOVE IDSV0001-KEY-TABELLA    TO LOR-KEY-TABELLA
        ws.getLogErrore().setLorKeyTabella(areaIdsv0001.getLogErrore().getKeyTabella());
        // COB_CODE: MOVE 0                       TO IND-LOR-KEY-TABELLA
        ws.getIndLogErrore().setKeyTabella(((short)0));
        //---> INTERCETTO IL TIMESTAMP DI SISTEMA
        // COB_CODE: PERFORM 1305-CURRENT-TIMESTAMP
        //              THRU 1305-CURRENT-TIMESTAMP-EXIT.
        currentTimestamp();
        // COB_CODE: MOVE ZEROES                  TO LOR-TIPO-OGGETTO.
        ws.getLogErrore().getLorTipoOggetto().setLorTipoOggetto(((short)0));
        // COB_CODE: MOVE 0                       TO IND-LOR-TIPO-OGGETTO.
        ws.getIndLogErrore().setTipoOggetto(((short)0));
        // COB_CODE: MOVE SPACES                  TO LOR-IB-OGGETTO.
        ws.getLogErrore().setLorIbOggetto("");
        // COB_CODE: MOVE 0                       TO IND-LOR-IB-OGGETTO.
        ws.getIndLogErrore().setIbOggetto(((short)0));
    }

    /**Original name: 1305-CURRENT-TIMESTAMP<br>*/
    private void currentTimestamp() {
        // COB_CODE: MOVE ZEROES                   TO WS-TIMESTAMP.
        ws.setWsTimestamp(LiteralGenerator.create('0', Ieas9700Data.Len.WS_TIMESTAMP));
        // COB_CODE: MOVE '20'     TO WS-TIMESTAMP(1:2)
        ws.setWsTimestamp(Functions.setSubstring(ws.getWsTimestamp(), "20", 1, 2));
        // COB_CODE: ACCEPT WS-TIMESTAMP(3:6) FROM DATE.
        ws.setWsTimestamp(Functions.setSubstring(ws.getWsTimestamp(), CalendarUtil.getDateYYMMDD(), 3, 6));
        // COB_CODE: ACCEPT WS-TIMESTAMP(9:6) FROM TIME.
        ws.setWsTimestamp(Functions.setSubstring(ws.getWsTimestamp(), CalendarUtil.getTimeHHMMSSMM(), 9, 6));
        //---> INIZIALIZZO I CAMPI DI APPOGGIO
        // COB_CODE: MOVE ZEROES                  TO WN-TIMESTAMP-DCL
        //                                           WN-TIMESTAMP-DCL-FRM.
        ws.setWnTimestampDcl(0);
        ws.getWsTimestampDclFrm().setWnTimestampDclFrm(0);
        //----> FORMATTO IL TIMESTAM PER IL FORMATO LOG ERRORE CIOE' SENZA
        //----> CARATTERI ALFANUMERICI
        // COB_CODE: MOVE  WS-TIMESTAMP(1:14)     TO WS-TIMESTAMP-DCL-FRM(1:14).
        ws.getWsTimestampDclFrm().setWsTimestampDclFrm(Functions.setSubstring(ws.getWsTimestampDclFrm().getWsTimestampDclFrm(), ws.getWsTimestampFormatted().substring((1) - 1, 14), 1, 14));
        // COB_CODE: MOVE  '0000'                 TO WS-TIMESTAMP-DCL-FRM(15:4).
        ws.getWsTimestampDclFrm().setWsTimestampDclFrm(Functions.setSubstring(ws.getWsTimestampDclFrm().getWsTimestampDclFrm(), "0000", 15, 4));
        // COB_CODE: MOVE  WN-TIMESTAMP-DCL-FRM   TO WN-TIMESTAMP-DCL
        ws.setWnTimestampDcl(ws.getWsTimestampDclFrm().getWnTimestampDclFrm());
        // COB_CODE: MOVE  WN-TIMESTAMP-DCL       TO LOR-TIMESTAMP-REG.
        ws.getLogErrore().setLorTimestampReg(ws.getWnTimestampDcl());
    }

    /**Original name: 1400-INSERIMENTO-LOG<br>*/
    private void inserimentoLog() {
        // COB_CODE: PERFORM 1401-SET-LENGTH-VCHAR   THRU 1401-EX.
        setLengthVchar();
        //-- INSERIMENTO DATI IN TABELLA DI LOG ERRORE
        // COB_CODE: EXEC SQL
        //                INSERT
        //                  INTO LOG_ERRORE
        //                      ( ID_LOG_ERRORE
        //                      , PROG_LOG_ERRORE
        //                      , ID_GRAVITA_ERRORE
        //                      , DESC_ERRORE_ESTESA
        //                      , COD_MAIN_BATCH
        //                      , COD_SERVIZIO_BE
        //                      , LABEL_ERR
        //                      , OPER_TABELLA
        //                      , NOME_TABELLA
        //                      , STATUS_TABELLA
        //                      , KEY_TABELLA
        //                      , TIMESTAMP_REG
        //                      , TIPO_OGGETTO
        //                      , IB_OGGETTO)
        //                VALUES
        //                     ( :LOR-ID-LOG-ERRORE
        //                      ,:LOR-PROG-LOG-ERRORE
        //                      ,:LOR-ID-GRAVITA-ERRORE
        //                      ,:LOR-DESC-ERRORE-ESTESA-VCHAR
        //                      ,:LOR-COD-MAIN-BATCH
        //                      ,:LOR-COD-SERVIZIO-BE
        //                      ,:LOR-LABEL-ERR
        //                       :IND-LOR-LABEL-ERR
        //                      ,:LOR-OPER-TABELLA
        //                       :IND-LOR-OPER-TABELLA
        //                      ,:LOR-NOME-TABELLA
        //                       :IND-LOR-NOME-TABELLA
        //                      ,:LOR-STATUS-TABELLA
        //                       :IND-LOR-STATUS-TABELLA
        //                      ,:LOR-KEY-TABELLA-VCHAR
        //                       :IND-LOR-KEY-TABELLA
        //                      ,:LOR-TIMESTAMP-REG
        //                      ,:LOR-TIPO-OGGETTO
        //                       :IND-LOR-TIPO-OGGETTO
        //                      ,:LOR-IB-OGGETTO
        //                       :IND-LOR-IB-OGGETTO)
        //           END-EXEC.
        logErroreDao.insertRec(ws);
        //-- CONTROLLO L'ESITO DELLA INSERT ANALIZZANDO L'SQLCODE
        // COB_CODE: EVALUATE SQLCODE
        //              WHEN 0
        //                  MOVE ZEROES                TO IEAO9701-COD-ERRORE-990
        //              WHEN OTHER
        //                 MOVE 99                     TO IEAO9701-COD-ERRORE-990
        //           END-EVALUATE.
        switch (sqlca.getSqlcode()) {

            case 0:// COB_CODE: MOVE ZEROES                TO IEAO9701-COD-ERRORE-990
                ieao9701Area.setCodErrore990(0);
                break;

            default:// COB_CODE: SET NO-CORRETTO             TO TRUE
                ws.getWsEsito().setNoCorretto();
                // COB_CODE: MOVE 99                     TO IEAO9701-COD-ERRORE-990
                ieao9701Area.setCodErrore990(99);
                break;
        }
    }

    /**Original name: 1401-SET-LENGTH-VCHAR<br>*/
    private void setLengthVchar() {
        // COB_CODE: MOVE LENGTH OF LOR-DESC-ERRORE-ESTESA
        //                          TO LOR-DESC-ERRORE-ESTESA-LEN
        ws.getLogErrore().setLorDescErroreEstesaLen(((short)LogErrore.Len.LOR_DESC_ERRORE_ESTESA));
        // COB_CODE: MOVE LENGTH OF LOR-KEY-TABELLA
        //                          TO LOR-KEY-TABELLA-LEN.
        ws.getLogErrore().setLorKeyTabellaLen(((short)LogErrore.Len.LOR_KEY_TABELLA));
    }

    /**Original name: 1500-VALORIZZA-OUTPUT<br>
	 * <pre>-- VALORIZZAZIONE CAMPI COPY DI OUTPUT</pre>*/
    private void valorizzaOutput() {
        // COB_CODE: MOVE WS-PRO-LOG-ERRORE           TO IEAO9701-PROG-LOG-ERR.
        ieao9701Area.setProgLogErr(TruncAbs.toInt(ws.getWsVariabili().getProLogErrore(), 5));
        // COB_CODE: MOVE WS-SESSIONE                 TO IEAO9701-ID-LOG-ERR.
        ieao9701Area.setIdLogErrFormatted(ws.getWsVariabili().getSessioneFormatted());
    }

    public void initLogErrore() {
        ws.getLogErrore().setLorIdLogErrore(0);
        ws.getLogErrore().setLorProgLogErrore(0);
        ws.getLogErrore().setLorIdGravitaErrore(0);
        ws.getLogErrore().setLorDescErroreEstesaLen(((short)0));
        ws.getLogErrore().setLorDescErroreEstesa("");
        ws.getLogErrore().setLorCodMainBatch("");
        ws.getLogErrore().setLorCodServizioBe("");
        ws.getLogErrore().setLorLabelErr("");
        ws.getLogErrore().setLorOperTabella("");
        ws.getLogErrore().setLorNomeTabella("");
        ws.getLogErrore().setLorStatusTabella("");
        ws.getLogErrore().setLorKeyTabellaLen(((short)0));
        ws.getLogErrore().setLorKeyTabella("");
        ws.getLogErrore().setLorTimestampReg(0);
        ws.getLogErrore().getLorTipoOggetto().setLorTipoOggetto(((short)0));
        ws.getLogErrore().setLorIbOggetto("");
    }
}
