package it.accenture.jnais;

import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.program.GenericParam;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.A2kInput;
import it.accenture.jnais.copy.A2kOugbmba;
import it.accenture.jnais.copy.A2kOurid;
import it.accenture.jnais.copy.A2kOusta;
import it.accenture.jnais.copy.A2kOutput;
import it.accenture.jnais.copy.TitCont;
import it.accenture.jnais.ws.A2kOuamgX;
import it.accenture.jnais.ws.A2kOugl07X;
import it.accenture.jnais.ws.A2kOugmaX;
import it.accenture.jnais.ws.A2kOujulX;
import it.accenture.jnais.ws.AreaIdsv0001;
import it.accenture.jnais.ws.enums.Idso0011SqlcodeSigned;
import it.accenture.jnais.ws.Ieai9901Area;
import it.accenture.jnais.ws.Lccc0001;
import it.accenture.jnais.ws.Lccc0490;
import it.accenture.jnais.ws.Lccs0490Data;
import it.accenture.jnais.ws.WcomAreaPagina;

/**Original name: LCCS0490<br>
 * <pre>AUTHOR.        ATS NAPOLI.
 * DATE-WRITTEN.
 * DATE-COMPILED.
 * ----------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                      *
 *  F A S E         : SERVIZIO CALCOLO RATEO                       *
 *                                                                 *
 * ----------------------------------------------------------------*</pre>*/
public class Lccs0490 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lccs0490Data ws = new Lccs0490Data();
    //Original name: AREA-IDSV0001
    private AreaIdsv0001 areaIdsv0001;
    //Original name: WCOM-AREA-STATI
    private Lccc0001 lccc0001;
    //Original name: LCCC0490
    private Lccc0490 lccc0490;

    //==== METHODS ====
    /**Original name: PROGRAM_LCCS0490_FIRST_SENTENCES<br>*/
    public long execute(AreaIdsv0001 areaIdsv0001, Lccc0001 lccc0001, Lccc0490 lccc0490) {
        this.areaIdsv0001 = areaIdsv0001;
        this.lccc0001 = lccc0001;
        this.lccc0490 = lccc0490;
        // COB_CODE: PERFORM A000-INIZIO              THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0001-ESITO-OK
        //              PERFORM A100-ELABORA          THRU A100-EX
        //           END-IF.
        if (this.areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: PERFORM A100-ELABORA          THRU A100-EX
            a100Elabora();
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Lccs0490 getInstance() {
        return ((Lccs0490)Programs.getInstance(Lccs0490.class));
    }

    /**Original name: A000-INIZIO<br>
	 * <pre>-----------------------------------------------------------------
	 *   OPERAZIONI INIZIALI
	 * -----------------------------------------------------------------
	 * --> COPY PER L'INIZIALIZZAZIONE AREA ERRORI
	 * --> DELL'AREA CONTESTO.
	 *  CONTIENE UNA SEQUENZA DI ISTRUZIONI PER AZZERAR 'AREA DEGLI
	 * ERRORI DELL'AREA CONTESTO.</pre>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'OK'   TO IDSV0001-ESITO
        areaIdsv0001.getEsito().setEsito("OK");
        // COB_CODE: MOVE SPACES TO IDSV0001-LOG-ERRORE
        areaIdsv0001.getLogErrore().initLogErroreSpaces();
        // COB_CODE: SET IDSV0001-FORZ-RC-04-NO        TO TRUE
        areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
        // COB_CODE: PERFORM VARYING IDSV0001-MAX-ELE-ERRORI FROM 1 BY 1
        //                     UNTIL IDSV0001-MAX-ELE-ERRORI > 10
        //               TO IDSV0001-TIPO-TRATT-FE(IDSV0001-MAX-ELE-ERRORI)
        //           END-PERFORM
        areaIdsv0001.setMaxEleErrori(((short)1));
        while (!(areaIdsv0001.getMaxEleErrori() > 10)) {
            // COB_CODE: MOVE SPACES
            //             TO IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
            areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore("");
            // COB_CODE: MOVE ZEROES
            //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
            areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErrore(0);
            // COB_CODE: MOVE ZEROES
            //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
            areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)0));
            // COB_CODE: MOVE SPACES
            //             TO IDSV0001-TIPO-TRATT-FE(IDSV0001-MAX-ELE-ERRORI)
            areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setTipoTrattFe(Types.SPACE_CHAR);
            areaIdsv0001.setMaxEleErrori(Trunc.toShort(areaIdsv0001.getMaxEleErrori() + 1, 4));
        }
        // COB_CODE: MOVE ZEROES TO IDSV0001-MAX-ELE-ERRORI.
        areaIdsv0001.setMaxEleErrori(((short)0));
        //--  TITOLO CONTABILE
        // COB_CODE: PERFORM VARYING IX-TAB-TIT FROM 1 BY 1
        //             UNTIL IX-TAB-TIT > WK-TIT-MAX-A
        //             PERFORM INIZIA-TOT-TIT THRU INIZIA-TOT-TIT-EX
        //           END-PERFORM.
        ws.getIxIndici().setTit(((short)1));
        while (!(ws.getIxIndici().getTit() > ws.getLccvtitz().getMaxA())) {
            // COB_CODE: PERFORM INIZIA-TOT-TIT THRU INIZIA-TOT-TIT-EX
            iniziaTotTit();
            ws.getIxIndici().setTit(Trunc.toShort(ws.getIxIndici().getTit() + 1, 4));
        }
        // COB_CODE: MOVE ZEROES                        TO WTIT-ELE-TIT-MAX.
        ws.setWtitEleTitMax(((short)0));
        //--  DETTAGLIO TITOLO CONTABILE
        // COB_CODE: PERFORM VARYING IX-TAB-DTC FROM 1 BY 1
        //             UNTIL IX-TAB-DTC > WK-DTC-MAX-A
        //             PERFORM INIZIA-TOT-DTC THRU INIZIA-TOT-DTC-EX
        //           END-PERFORM.
        ws.getIxIndici().setDtc(((short)1));
        while (!(ws.getIxIndici().getDtc() > ws.getWkDtcMax().getA())) {
            // COB_CODE: PERFORM INIZIA-TOT-DTC THRU INIZIA-TOT-DTC-EX
            //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LCCS0490.cbl:line=224, because the code is unreachable.
            ws.getIxIndici().setDtc(Trunc.toShort(ws.getIxIndici().getDtc() + 1, 4));
        }
        // COB_CODE: MOVE ZEROES                        TO WDTC-ELE-DETT-TIT-MAX.
        ws.setWdtcEleDettTitMax(((short)0));
        // COB_CODE: INITIALIZE LCCC0490-AREA-OUTPUT
        //                      IX-INDICI.
        initAreaOutput();
        initIxIndici();
        // COB_CODE: SET LCCC0490-NO-TIT-EMESSI         TO TRUE.
        lccc0490.getTitoliEmessi().setNoTitEmessi();
        // COB_CODE: SET LCCC0490-SI-PRESENZA-RATEO     TO TRUE.
        lccc0490.getPresenzaRateo().setSiPresenzaRateo();
        // COB_CODE: MOVE IDSV0001-DATA-COMPETENZA(1:8) TO WK-DT-COMPETENZA.
        ws.setWkDtCompetenzaFormatted(areaIdsv0001.getAreaComune().getIdsv0001DataCompetenzaFormatted().substring((1) - 1, 8));
        // COB_CODE: SET GARANZIA-ACC-KO                TO TRUE.
        ws.getGaranziaAcc().setKo();
        // COB_CODE: SET GARANZIA-BASE-KO               TO TRUE.
        ws.getGaranziaBase().setKo();
        //--> CONTROLLI SUI CAMPI DI INPUT
        // COB_CODE: PERFORM A010-CONTROLLI-INPUT
        //              THRU A010-EX.
        a010ControlliInput();
        //--> LETTURA PARAMETRO COMPAGNIA
        // COB_CODE: IF IDSV0001-ESITO-OK
        //                 THRU A020-EX
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: PERFORM A020-LETTURA-PCO
            //              THRU A020-EX
            a020LetturaPco();
        }
    }

    /**Original name: A010-CONTROLLI-INPUT<br>
	 * <pre>-----------------------------------------------------------------
	 *   CONTROLLI DATI INPUT
	 * -----------------------------------------------------------------</pre>*/
    private void a010ControlliInput() {
        ConcatUtil concatUtil = null;
        // COB_CODE: IF LCCC0490-ID-POLI IS NUMERIC AND
        //              LCCC0490-ID-POLI > ZERO
        //              CONTINUE
        //           ELSE
        //                 THRU EX-S0300
        //           END-IF.
        if (Functions.isNumber(lccc0490.getIdPoliFormatted()) && Characters.GT_ZERO.test(lccc0490.getIdPoliFormatted())) {
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE WK-PGM     TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL   TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWkLabel());
            // COB_CODE: MOVE '005166'   TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005166");
            // COB_CODE: STRING 'IDENTIFICATIVO POLIZZA = '
            //                  LCCC0490-ID-POLI
            //                  ' NON CONSENTITO'
            //                  DELIMITED BY SIZE
            //                  INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "IDENTIFICATIVO POLIZZA = ", lccc0490.getIdPoliAsString(), " NON CONSENTITO");
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
        // COB_CODE: IF IDSV0001-ESITO-OK
        //              END-IF
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: IF LCCC0490-DT-DECOR-POLI IS NUMERIC AND
            //              LCCC0490-DT-DECOR-POLI > ZERO
            //              CONTINUE
            //           ELSE
            //                 THRU EX-S0300
            //           END-IF
            if (Functions.isNumber(lccc0490.getDtDecorPoliFormatted()) && Characters.GT_ZERO.test(lccc0490.getDtDecorPoliFormatted())) {
            // COB_CODE: CONTINUE
            //continue
            }
            else {
                // COB_CODE: MOVE WK-PGM     TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE WK-LABEL   TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr(ws.getWkLabel());
                // COB_CODE: MOVE '005166'   TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("005166");
                // COB_CODE: STRING 'DATA DECORRENZA POLIZZA = '
                //                  LCCC0490-DT-DECOR-POLI
                //                  ' NON CONSENTITA'
                //                  DELIMITED BY SIZE
                //                  INTO IEAI9901-PARAMETRI-ERR
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "DATA DECORRENZA POLIZZA = ", lccc0490.getDtDecorPoliAsString(), " NON CONSENTITA");
                ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
            }
        }
        // COB_CODE: PERFORM VARYING IX-TAB-GRZ FROM 1 BY 1
        //             UNTIL IX-TAB-GRZ > WK-GRZ-MAX-B
        //                OR IDSV0001-ESITO-KO
        //             END-IF
        //           END-PERFORM.
        ws.getIxIndici().setGrz(((short)1));
        while (!(ws.getIxIndici().getGrz() > ws.getLccvgrzz().getMaxB() || areaIdsv0001.getEsito().isIdsv0001EsitoKo())) {
            // COB_CODE: IF LCCC0490-COD-TARI(IX-TAB-GRZ) =
            //              SPACES OR HIGH-VALUE OR LOW-VALUE
            //                 THRU EX-S0300
            //           END-IF
            if (Characters.EQ_SPACE.test(lccc0490.getTabGrz(ws.getIxIndici().getGrz()).getCodTari()) || Characters.EQ_HIGH.test(lccc0490.getTabGrz(ws.getIxIndici().getGrz()).getCodTariFormatted()) || Characters.EQ_LOW.test(lccc0490.getTabGrz(ws.getIxIndici().getGrz()).getCodTariFormatted())) {
                // COB_CODE: MOVE WK-PGM     TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE WK-LABEL   TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr(ws.getWkLabel());
                // COB_CODE: MOVE '005166'   TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("005166");
                // COB_CODE: STRING 'CODICE TARIFFA = '
                //                  LCCC0490-COD-TARI(IX-TAB-GRZ)
                //                  ' NON CONSENTITO'
                //                  DELIMITED BY SIZE
                //                  INTO IEAI9901-PARAMETRI-ERR
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "CODICE TARIFFA = ", lccc0490.getTabGrz(ws.getIxIndici().getGrz()).getCodTariFormatted(), " NON CONSENTITO");
                ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
            }
            // COB_CODE: IF IDSV0001-ESITO-OK
            //              END-IF
            //           END-IF
            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                // COB_CODE: IF LCCC0490-TP-GAR(IX-TAB-GRZ) IS NUMERIC AND
                //              LCCC0490-TP-GAR(IX-TAB-GRZ) > ZERO
                //              CONTINUE
                //           ELSE
                //                 THRU EX-S0300
                //           END-IF
                if (Functions.isNumber(lccc0490.getTabGrz(ws.getIxIndici().getGrz()).getTpGarFormatted()) && Characters.GT_ZERO.test(lccc0490.getTabGrz(ws.getIxIndici().getGrz()).getTpGarFormatted())) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: MOVE WK-PGM     TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE WK-LABEL   TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr(ws.getWkLabel());
                    // COB_CODE: MOVE '005166'   TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005166");
                    // COB_CODE: STRING 'TIPO GARANZIA = '
                    //                  LCCC0490-TP-GAR(IX-TAB-GRZ)
                    //                  ' NON CONSENTITO'
                    //                  DELIMITED BY SIZE
                    //                  INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "TIPO GARANZIA = ", lccc0490.getTabGrz(ws.getIxIndici().getGrz()).getTpGarAsString(), " NON CONSENTITO");
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                }
            }
            // COB_CODE: IF IDSV0001-ESITO-OK
            //              END-IF
            //           END-IF
            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                // COB_CODE: IF LCCC0490-FRAZIONAMENTO(IX-TAB-GRZ) IS NUMERIC
                //              CONTINUE
                //           ELSE
                //                 THRU EX-S0300
                //           END-IF
                if (Functions.isNumber(lccc0490.getTabGrz(ws.getIxIndici().getGrz()).getFrazionamentoFormatted())) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: MOVE WK-PGM     TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE WK-LABEL   TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr(ws.getWkLabel());
                    // COB_CODE: MOVE '005166'   TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005166");
                    // COB_CODE: STRING 'FRAZIONAMENTO = '
                    //                  LCCC0490-FRAZIONAMENTO(IX-TAB-GRZ)
                    //                  ' NON CONSENTITO'
                    //                  DELIMITED BY SIZE
                    //                  INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "FRAZIONAMENTO = ", lccc0490.getTabGrz(ws.getIxIndici().getGrz()).getFrazionamentoAsString(), " NON CONSENTITO");
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                }
            }
            // COB_CODE: IF IDSV0001-ESITO-OK
            //              END-IF
            //           END-IF
            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                // COB_CODE: IF LCCC0490-TP-PER-PRE(IX-TAB-GRZ) =
                //              SPACES OR HIGH-VALUE OR LOW-VALUE
                //                 THRU EX-S0300
                //           END-IF
                if (Conditions.eq(lccc0490.getTabGrz(ws.getIxIndici().getGrz()).getTpPerPre(), Types.SPACE_CHAR) || Conditions.eq(lccc0490.getTabGrz(ws.getIxIndici().getGrz()).getTpPerPre(), Types.HIGH_CHAR_VAL) || Conditions.eq(lccc0490.getTabGrz(ws.getIxIndici().getGrz()).getTpPerPre(), Types.LOW_CHAR_VAL)) {
                    // COB_CODE: MOVE WK-PGM     TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE WK-LABEL   TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr(ws.getWkLabel());
                    // COB_CODE: MOVE '005166'   TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005166");
                    // COB_CODE: STRING 'TIPO PERIODO PREMIO = '
                    //                  LCCC0490-TP-PER-PRE(IX-TAB-GRZ)
                    //                  ' NON CONSENTITO'
                    //                  DELIMITED BY SIZE
                    //                  INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "TIPO PERIODO PREMIO = ", String.valueOf(lccc0490.getTabGrz(ws.getIxIndici().getGrz()).getTpPerPre()), " NON CONSENTITO");
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                }
            }
            // COB_CODE: IF IDSV0001-ESITO-OK
            //              END-IF
            //           END-IF
            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                // COB_CODE: IF LCCC0490-DT-DECOR(IX-TAB-GRZ) IS NUMERIC AND
                //              LCCC0490-DT-DECOR(IX-TAB-GRZ) > ZERO
                //              CONTINUE
                //           ELSE
                //                 THRU EX-S0300
                //           END-IF
                if (Functions.isNumber(lccc0490.getTabGrz(ws.getIxIndici().getGrz()).getDtDecorFormatted()) && Characters.GT_ZERO.test(lccc0490.getTabGrz(ws.getIxIndici().getGrz()).getDtDecorFormatted())) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: MOVE WK-PGM     TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE WK-LABEL   TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr(ws.getWkLabel());
                    // COB_CODE: MOVE '005166'   TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005166");
                    // COB_CODE: STRING 'DATA DECORRENZA = '
                    //                  LCCC0490-DT-DECOR(IX-TAB-GRZ)
                    //                  ' NON CONSENTITA'
                    //                  DELIMITED BY SIZE
                    //                  INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "DATA DECORRENZA = ", lccc0490.getTabGrz(ws.getIxIndici().getGrz()).getDtDecorAsString(), " NON CONSENTITA");
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                }
            }
            // COB_CODE: IF IDSV0001-ESITO-OK
            //              END-IF
            //           END-IF
            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                // COB_CODE: IF LCCC0490-TIPO-RICORRENZA(IX-TAB-GRZ) =
                //              SPACES OR HIGH-VALUE OR LOW-VALUE
                //                 THRU EX-S0300
                //           ELSE
                //              END-IF
                //           END-IF
                if (Conditions.eq(lccc0490.getTabGrz(ws.getIxIndici().getGrz()).getTipoRicorrenza(), Types.SPACE_CHAR) || Conditions.eq(lccc0490.getTabGrz(ws.getIxIndici().getGrz()).getTipoRicorrenza(), Types.HIGH_CHAR_VAL) || Conditions.eq(lccc0490.getTabGrz(ws.getIxIndici().getGrz()).getTipoRicorrenza(), Types.LOW_CHAR_VAL)) {
                    // COB_CODE: MOVE WK-PGM     TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE WK-LABEL   TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr(ws.getWkLabel());
                    // COB_CODE: MOVE '005166'   TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005166");
                    // COB_CODE: STRING 'TIPOLOGIA RICORRENZA = '
                    //                  LCCC0490-TIPO-RICORRENZA(IX-TAB-GRZ)
                    //                  ' NON VALORIZZATA'
                    //                  DELIMITED BY SIZE
                    //                  INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "TIPOLOGIA RICORRENZA = ", String.valueOf(lccc0490.getTabGrz(ws.getIxIndici().getGrz()).getTipoRicorrenza()), " NON VALORIZZATA");
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                }
                else if (lccc0490.getTabGrz(ws.getIxIndici().getGrz()).getTipoRicorrenza() != 'Q' && lccc0490.getTabGrz(ws.getIxIndici().getGrz()).getTipoRicorrenza() != 'G') {
                    // COB_CODE: IF LCCC0490-TIPO-RICORRENZA(IX-TAB-GRZ) NOT = 'Q' AND
                    //              LCCC0490-TIPO-RICORRENZA(IX-TAB-GRZ) NOT = 'G'
                    //                 THRU EX-S0300
                    //           END-IF
                    // COB_CODE: MOVE WK-PGM     TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE WK-LABEL   TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr(ws.getWkLabel());
                    // COB_CODE: MOVE '005166'   TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005166");
                    // COB_CODE: STRING 'TIPOLOGIA RICORRENZA = '
                    //               LCCC0490-TIPO-RICORRENZA(IX-TAB-GRZ)
                    //               ' NON CONSENTITA'
                    //               DELIMITED BY SIZE
                    //               INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "TIPOLOGIA RICORRENZA = ", String.valueOf(lccc0490.getTabGrz(ws.getIxIndici().getGrz()).getTipoRicorrenza()), " NON CONSENTITA");
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                }
            }
            ws.getIxIndici().setGrz(Trunc.toShort(ws.getIxIndici().getGrz() + 1, 4));
        }
    }

    /**Original name: A020-LETTURA-PCO<br>
	 * <pre>----------------------------------------------------------------*
	 *     LETTURA PARAMETRO COMPAGNIA
	 * ----------------------------------------------------------------*</pre>*/
    private void a020LetturaPco() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE HIGH-VALUE                  TO PARAM-COMP.
        ws.getParamComp().initParamCompHighValues();
        // COB_CODE: MOVE ZERO                     TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                            IDSI0011-DATA-FINE-EFFETTO
        //                                            IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA TO PCO-COD-COMP-ANIA.
        ws.getParamComp().setPcoCodCompAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: MOVE 'PARAM-COMP'                TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("PARAM-COMP");
        // COB_CODE: MOVE PARAM-COMP                  TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getParamComp().getParamCompFormatted());
        // COB_CODE: SET IDSI0011-SELECT              TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        // COB_CODE: SET IDSI0011-PRIMARY-KEY         TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setPrimaryKey();
        // COB_CODE: SET IDSI0011-TRATT-SENZA-STOR    TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattSenzaStor();
        // COB_CODE: PERFORM CALL-DISPATCHER          THRU CALL-DISPATCHER-EX.
        callDispatcher();
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //                   END-EVALUATE
        //                ELSE
        //           *--> GESTIRE ERRORE
        //                      THRU EX-S0300
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            // COB_CODE: EVALUATE TRUE
            //               WHEN IDSO0011-SUCCESSFUL-SQL
            //                    MOVE IDSO0011-BUFFER-DATI TO PARAM-COMP
            //               WHEN OTHER
            //                       THRU EX-S0300
            //           END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL:// COB_CODE: MOVE IDSO0011-BUFFER-DATI TO PARAM-COMP
                    ws.getParamComp().setParamCompFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    break;

                default:// COB_CODE: MOVE WK-PGM     TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE WK-LABEL   TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr(ws.getWkLabel());
                    // COB_CODE: MOVE '005016'   TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005016");
                    // COB_CODE: STRING IDSI0011-CODICE-STR-DATO ';'
                    //                  IDSO0011-RETURN-CODE     ';'
                    //                  IDSO0011-SQLCODE
                    //                  DELIMITED BY SIZE
                    //                  INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011CodiceStrDatoFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;
            }
        }
        else {
            //--> GESTIRE ERRORE
            // COB_CODE: MOVE WK-PGM           TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL         TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWkLabel());
            // COB_CODE: MOVE '005016'         TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING IDSI0011-CODICE-STR-DATO ';'
            //                  IDSO0011-RETURN-CODE     ';'
            //                  IDSO0011-SQLCODE
            //                  DELIMITED BY SIZE
            //                  INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011CodiceStrDatoFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: A100-ELABORA<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void a100Elabora() {
        // COB_CODE: PERFORM A110-VERIFICA-GARANZIE
        //              THRU A110-EX
        //           VARYING IX-TAB-GRZ FROM 1 BY 1
        //             UNTIL IX-TAB-GRZ > WK-GRZ-MAX-B
        //                OR IDSV0001-ESITO-KO.
        ws.getIxIndici().setGrz(((short)1));
        while (!(ws.getIxIndici().getGrz() > ws.getLccvgrzz().getMaxB() || areaIdsv0001.getEsito().isIdsv0001EsitoKo())) {
            a110VerificaGaranzie();
            ws.getIxIndici().setGrz(Trunc.toShort(ws.getIxIndici().getGrz() + 1, 4));
        }
        // COB_CODE: IF IDSV0001-ESITO-OK AND
        //              GARANZIA-BASE-OK  AND
        //              GARANZIA-ACC-OK
        //                 THRU A120-EX
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk() && ws.getGaranziaBase().isOk() && ws.getGaranziaAcc().isOk()) {
            // COB_CODE: PERFORM A120-ULT-TIT-INCAS
            //              THRU A120-EX
            a120UltTitIncas();
        }
    }

    /**Original name: A110-VERIFICA-GARANZIE<br>
	 * <pre>----------------------------------------------------------------*
	 *     VERIFICA GARANZIE
	 * ----------------------------------------------------------------*</pre>*/
    private void a110VerificaGaranzie() {
        // COB_CODE: MOVE LCCC0490-TP-GAR(IX-TAB-GRZ) TO ACT-TP-GARANZIA.
        ws.getLccc0006().getActTpGaranzia().setActTpGaranziaFormatted(lccc0490.getTabGrz(ws.getIxIndici().getGrz()).getTpGarFormatted());
        // COB_CODE: IF (LCCC0490-FRAZIONAMENTO(IX-TAB-GRZ) > 0 AND
        //               TP-GAR-COMPLEM)
        //                 TO WK-TIPO-RICORRENZA
        //           END-IF.
        if (lccc0490.getTabGrz(ws.getIxIndici().getGrz()).getFrazionamento() > 0 && ws.getLccc0006().getActTpGaranzia().isTpGarComplem()) {
            // COB_CODE: SET GARANZIA-ACC-OK TO TRUE
            ws.getGaranziaAcc().setOk();
            // COB_CODE: MOVE LCCC0490-DT-DECOR(IX-TAB-GRZ)
            //             TO WK-DT-DECOR-GRZ
            ws.setWkDtDecorGrzFormatted(lccc0490.getTabGrz(ws.getIxIndici().getGrz()).getDtDecorFormatted());
            // COB_CODE: MOVE LCCC0490-FRAZIONAMENTO(IX-TAB-GRZ)
            //             TO WK-FRAZIONAMENTO
            ws.setWkFrazionamentoFormatted(lccc0490.getTabGrz(ws.getIxIndici().getGrz()).getFrazionamentoFormatted());
            // COB_CODE: MOVE LCCC0490-TIPO-RICORRENZA(IX-TAB-GRZ)
            //             TO WK-TIPO-RICORRENZA
            ws.setWkTipoRicorrenza(lccc0490.getTabGrz(ws.getIxIndici().getGrz()).getTipoRicorrenza());
        }
        // COB_CODE: IF TP-GAR-BASE AND
        //             (LCCC0490-TP-PER-PRE(IX-TAB-GRZ) = 'U' OR
        //              LCCC0490-FRAZIONAMENTO(IX-TAB-GRZ) > 0)
        //              SET GARANZIA-BASE-OK TO TRUE
        //           END-IF.
        if (ws.getLccc0006().getActTpGaranzia().isBaseFld() && (lccc0490.getTabGrz(ws.getIxIndici().getGrz()).getTpPerPre() == 'U' || lccc0490.getTabGrz(ws.getIxIndici().getGrz()).getFrazionamento() > 0)) {
            // COB_CODE: SET GARANZIA-BASE-OK TO TRUE
            ws.getGaranziaBase().setOk();
        }
    }

    /**Original name: A120-ULT-TIT-INCAS<br>
	 * <pre>----------------------------------------------------------------*
	 *     LETTURA ULTIMO TITOLO CONTABILE INCASSATO
	 * ----------------------------------------------------------------*</pre>*/
    private void a120UltTitIncas() {
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE IDSI0011-BUFFER-DATI
        //                      TIT-CONT.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
        initTitCont();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC    TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
        // COB_CODE: SET IDSI0011-SELECT           TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        // COB_CODE: SET IDSI0011-WHERE-CONDITION  TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setWhereCondition();
        // COB_CODE: SET IDSI0011-TRATT-DEFAULT    TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setIdsi0011TrattDefault();
        // COB_CODE: MOVE LCCC0490-ID-POLI         TO TIT-ID-OGG.
        ws.getTitCont().setTitIdOgg(lccc0490.getIdPoli());
        // COB_CODE: SET POLIZZA                   TO TRUE.
        ws.getWsTpOgg().setPolizza();
        // COB_CODE: MOVE WS-TP-OGG                TO TIT-TP-OGG.
        ws.getTitCont().setTitTpOgg(ws.getWsTpOgg().getWsTpOgg());
        // COB_CODE: SET INCASSATO                 TO TRUE.
        ws.getWsTpStatTit().setIncassato();
        // COB_CODE: MOVE WS-TP-STAT-TIT           TO TIT-TP-STAT-TIT.
        ws.getTitCont().setTitTpStatTit(ws.getWsTpStatTit().getWsTpStatTit());
        // COB_CODE: SET TT-PREMIO                 TO TRUE.
        ws.getWsTpTit().setPremio();
        // COB_CODE: MOVE WS-TP-TIT                TO TIT-TP-TIT.
        ws.getTitCont().setTitTpTit(ws.getWsTpTit().getWsTpTit());
        // COB_CODE: MOVE WS-DT-INFINITO-1-N       TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                            IDSI0011-DATA-FINE-EFFETTO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(ws.getIdsv0015().getDtInfinito1N());
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(ws.getIdsv0015().getDtInfinito1N());
        // COB_CODE: MOVE WS-TS-INFINITO-1-N       TO IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(ws.getIdsv0015().getTsInfinito1N());
        // COB_CODE: MOVE SPACES                   TO IDSI0011-BUFFER-WHERE-COND.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferWhereCond("");
        // COB_CODE: MOVE 'LDBS5980'               TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("LDBS5980");
        // COB_CODE: MOVE TIT-CONT                 TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getTitCont().getTitContFormatted());
        // COB_CODE: PERFORM CALL-DISPATCHER       THRU CALL-DISPATCHER-EX.
        callDispatcher();
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //                   END-EVALUATE
        //                ELSE
        //           *--> GESTIRE ERRORE
        //                      THRU EX-S0300
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            // COB_CODE:         EVALUATE TRUE
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //           *-->        OPERAZIONE ESEGUITA CORRETTAMENTE
            //                            END-IF
            //                       WHEN IDSO0011-NOT-FOUND
            //           *-->        NESSUN DATO IN TABELLA
            //                            END-IF
            //                       WHEN OTHER
            //           *--->       ERRORE DI ACCESSO AL DB
            //                               THRU EX-S0300
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://-->        OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: MOVE IDSO0011-BUFFER-DATI
                    //             TO TIT-CONT
                    ws.getTitCont().setTitContFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    // COB_CODE: MOVE TIT-DT-END-COP
                    //             TO WK-DT-ULT-TIT-INC
                    ws.setWkDtUltTitInc(TruncAbs.toInt(ws.getTitCont().getTitDtEndCop().getTitDtEndCop(), 8));
                    // COB_CODE: IF WK-DT-DECOR-GRZ < TIT-DT-END-COP
                    //                 THRU A121-EX
                    //           ELSE
                    //              END-IF
                    //           END-IF
                    if (ws.getWkDtDecorGrz() < ws.getTitCont().getTitDtEndCop().getTitDtEndCop()) {
                        // COB_CODE: MOVE WK-DT-DECOR-GRZ TO WK-DT-INFERIORE
                        ws.setWkDtInferioreFormatted(ws.getWkDtDecorGrzFormatted());
                        // COB_CODE: MOVE TIT-DT-END-COP  TO WK-DT-SUPERIORE
                        //                                   LCCC0490-DT-FINE-COP
                        ws.setWkDtSuperiore(TruncAbs.toInt(ws.getTitCont().getTitDtEndCop().getTitDtEndCop(), 8));
                        lccc0490.setDtFineCop(TruncAbs.toInt(ws.getTitCont().getTitDtEndCop().getTitDtEndCop(), 8));
                        // COB_CODE: PERFORM A121-CALCOLO-DIFF-DATE
                        //              THRU A121-EX
                        a121CalcoloDiffDate();
                    }
                    else {
                        // COB_CODE: MOVE WK-DT-DECOR-GRZ TO WK-DT-INFERIORE
                        ws.setWkDtInferioreFormatted(ws.getWkDtDecorGrzFormatted());
                        //-->        LETTURA DI EVENTUALI TITOLI EMESSI
                        // COB_CODE: PERFORM A124-PREPARA-AREA-TIT
                        //              THRU A124-EX
                        a124PreparaAreaTit();
                        // COB_CODE: PERFORM A125-ESTRAZIONE-TIT
                        //              THRU A125-EX
                        a125EstrazioneTit();
                        // COB_CODE: IF IDSV0001-ESITO-OK
                        //                 THRU A127-EX
                        //           END-IF
                        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                            // COB_CODE: PERFORM A126-PREPARA-AREA-DTC
                            //              THRU A126-EX
                            a126PreparaAreaDtc();
                            // COB_CODE: PERFORM A127-ESTRAZIONE-DTC
                            //              THRU A127-EX
                            a127EstrazioneDtc();
                        }
                    }
                    break;

                case Idso0011SqlcodeSigned.NOT_FOUND://-->        NESSUN DATO IN TABELLA
                    // COB_CODE: MOVE LCCC0490-DT-DECOR-POLI  TO WK-DT-DECORRENZA
                    ws.setWkDtDecorrenzaFormatted(lccc0490.getDtDecorPoliFormatted());
                    // COB_CODE: PERFORM A122-CALL-LCCS0062
                    //              THRU A122-EX
                    a122CallLccs0062();
                    // COB_CODE: PERFORM A123-SCORRI-RATE
                    //              THRU A123-EX
                    a123ScorriRate();
                    // COB_CODE: IF TROVATO
                    //                 THRU A121-EX
                    //           END-IF
                    if (ws.getRicerca().isTrovato()) {
                        // COB_CODE: MOVE WK-DT-DECOR-GRZ
                        //             TO WK-DT-INFERIORE
                        ws.setWkDtInferioreFormatted(ws.getWkDtDecorGrzFormatted());
                        // COB_CODE: MOVE LCCC0490-DT-FINE-COP
                        //             TO WK-DT-SUPERIORE
                        ws.setWkDtSuperioreFormatted(lccc0490.getDtFineCopFormatted());
                        // COB_CODE: PERFORM A121-CALCOLO-DIFF-DATE
                        //              THRU A121-EX
                        a121CalcoloDiffDate();
                    }
                    break;

                default://--->       ERRORE DI ACCESSO AL DB
                    // COB_CODE: MOVE WK-PGM     TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE WK-LABEL   TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr(ws.getWkLabel());
                    // COB_CODE: MOVE '005016'   TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005016");
                    // COB_CODE: STRING IDSI0011-CODICE-STR-DATO ';'
                    //                  IDSO0011-RETURN-CODE     ';'
                    //                  IDSO0011-SQLCODE
                    //                  DELIMITED BY SIZE
                    //                  INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011CodiceStrDatoFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;
            }
        }
        else {
            //--> GESTIRE ERRORE
            // COB_CODE: MOVE WK-PGM           TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL         TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWkLabel());
            // COB_CODE: MOVE '005016'         TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING IDSI0011-CODICE-STR-DATO ';'
            //                  IDSO0011-RETURN-CODE     ';'
            //                  IDSO0011-SQLCODE
            //                  DELIMITED BY SIZE
            //                  INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011CodiceStrDatoFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: A121-CALCOLO-DIFF-DATE<br>
	 * <pre>----------------------------------------------------------------*
	 *  CALCOLO DELLA DIFFERENZA IN GIORNI TRA DUE DATE
	 * ----------------------------------------------------------------*</pre>*/
    private void a121CalcoloDiffDate() {
        Lccs0010 lccs0010 = null;
        GenericParam formato = null;
        GenericParam ggDiff = null;
        GenericParam codiceRitorno = null;
        // COB_CODE: MOVE 'A'                       TO FORMATO.
        ws.setFormatoFormatted("A");
        // COB_CODE: MOVE WK-DT-INFERIORE           TO DATA-INFERIORE.
        ws.getDataInferiore().setDataInferioreFormatted(ws.getWkDtInferioreFormatted());
        // COB_CODE: MOVE WK-DT-SUPERIORE           TO DATA-SUPERIORE.
        ws.getDataSuperiore().setDataSuperioreFormatted(ws.getWkDtSuperioreFormatted());
        // COB_CODE: CALL PGM-LCCS0010 USING FORMATO,
        //                                   DATA-INFERIORE,
        //                                   DATA-SUPERIORE,
        //                                   GG-DIFF,
        //                                   CODICE-RITORNO
        //           ON EXCEPTION
        //                 THRU EX-S0290
        //           END-CALL.
        try {
            lccs0010 = Lccs0010.getInstance();
            formato = new GenericParam(MarshalByteExt.chToBuffer(ws.getFormato()));
            ggDiff = new GenericParam(MarshalByteExt.strToBuffer(ws.getGgDiffFormatted(), Lccs0490Data.Len.GG_DIFF));
            codiceRitorno = new GenericParam(MarshalByteExt.chToBuffer(ws.getCodiceRitorno()));
            lccs0010.run(formato, ws.getDataInferiore(), ws.getDataSuperiore(), ggDiff, codiceRitorno);
            ws.setFormatoFromBuffer(formato.getByteData());
            ws.setGgDiffFromBuffer(ggDiff.getByteData());
            ws.setCodiceRitornoFromBuffer(codiceRitorno.getByteData());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-PGM                 TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'CALCOLO DIFFERENZA GIORNI TRA DUE DATE'
            //                                       TO CALL-DESC
            ws.getIdsv0002().setCallDesc("CALCOLO DIFFERENZA GIORNI TRA DUE DATE");
            // COB_CODE: MOVE WK-LABEL               TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWkLabel());
            // COB_CODE: PERFORM S0290-ERRORE-DI-SISTEMA
            //              THRU EX-S0290
            s0290ErroreDiSistema();
        }
        // COB_CODE: IF CODICE-RITORNO = ZERO
        //              MOVE GG-DIFF                TO LCCC0490-RATEO
        //           ELSE
        //                 THRU EX-S0300
        //           END-IF.
        if (Conditions.eq(ws.getCodiceRitorno(), '0')) {
            // COB_CODE: MOVE GG-DIFF                TO LCCC0490-RATEO
            lccc0490.setRateoFormatted(ws.getGgDiffFormatted());
        }
        else {
            // COB_CODE: MOVE WK-PGM                 TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL               TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWkLabel());
            // COB_CODE: MOVE '005016'               TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: MOVE SPACES                 TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: A122-CALL-LCCS0062<br>
	 * <pre>----------------------------------------------------------------*
	 *   ROUTINE PER IL CALCOLO DELLE RATE SUCCESSIVE
	 * ----------------------------------------------------------------*</pre>*/
    private void a122CallLccs0062() {
        Lccs0062 lccs0062 = null;
        // COB_CODE: INITIALIZE AREA-LCCC0062.
        initAreaLccc0062();
        // COB_CODE: MOVE WK-DT-DECORRENZA           TO LCCC0062-DT-DECORRENZA.
        ws.getAreaLccc0062().setDtDecorrenza(ws.getWkDtDecorrenza());
        // COB_CODE: MOVE WK-DT-COMPETENZA           TO LCCC0062-DT-COMPETENZA.
        ws.getAreaLccc0062().setDtCompetenza(ws.getWkDtCompetenza());
        // COB_CODE: IF WK-TIPO-RICORRENZA = 'Q'
        //              MOVE ZEROES                  TO LCCC0062-DT-ULTGZ-TRCH
        //           ELSE
        //              MOVE ZEROES                  TO LCCC0062-DT-ULT-QTZO
        //           END-IF.
        if (ws.getWkTipoRicorrenza() == 'Q') {
            // COB_CODE: MOVE PCO-DT-ULT-QTZO-IN      TO LCCC0062-DT-ULT-QTZO
            ws.getAreaLccc0062().setDtUltQtzo(TruncAbs.toInt(ws.getParamComp().getPcoDtUltQtzoIn().getPcoDtUltQtzoIn(), 8));
            // COB_CODE: MOVE ZEROES                  TO LCCC0062-DT-ULTGZ-TRCH
            ws.getAreaLccc0062().setDtUltgzTrch(0);
        }
        else {
            // COB_CODE: MOVE PCO-DT-ULTGZ-TRCH-E-IN  TO LCCC0062-DT-ULTGZ-TRCH
            ws.getAreaLccc0062().setDtUltgzTrch(TruncAbs.toInt(ws.getParamComp().getPcoDtUltgzTrchEIn().getPcoDtUltgzTrchEIn(), 8));
            // COB_CODE: MOVE ZEROES                  TO LCCC0062-DT-ULT-QTZO
            ws.getAreaLccc0062().setDtUltQtzo(0);
        }
        // COB_CODE: MOVE WK-FRAZIONAMENTO           TO LCCC0062-FRAZIONAMENTO.
        ws.getAreaLccc0062().setFrazionamentoFormatted(ws.getWkFrazionamentoFormatted());
        // COB_CODE: CALL PGM-LCCS0062 USING  AREA-IDSV0001
        //                                    WCOM-AREA-STATI
        //                                    AREA-LCCC0062
        //           ON EXCEPTION
        //                 THRU EX-S0290
        //           END-CALL.
        try {
            lccs0062 = Lccs0062.getInstance();
            lccs0062.run(areaIdsv0001, lccc0001, ws.getAreaLccc0062());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-PGM                    TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'SERVIZIO CALCOLO RATE'   TO CALL-DESC
            ws.getIdsv0002().setCallDesc("SERVIZIO CALCOLO RATE");
            // COB_CODE: MOVE WK-LABEL                  TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWkLabel());
            // COB_CODE: PERFORM S0290-ERRORE-DI-SISTEMA
            //              THRU EX-S0290
            s0290ErroreDiSistema();
        }
    }

    /**Original name: A123-SCORRI-RATE<br>
	 * <pre>----------------------------------------------------------------*
	 *   SCORRI RATE
	 * ----------------------------------------------------------------*</pre>*/
    private void a123ScorriRate() {
        // COB_CODE: SET NON-TROVATO                TO TRUE.
        ws.getRicerca().setNonTrovato();
        // COB_CODE: PERFORM VARYING IX-TAB-RATE FROM 1 BY 1
        //             UNTIL IX-TAB-RATE > LCCC0062-TOT-NUM-RATE
        //                OR TROVATO
        //             END-IF
        //           END-PERFORM.
        ws.getIxIndici().setRate(((short)1));
        while (!(ws.getIxIndici().getRate() > ws.getAreaLccc0062().getTotNumRate() || ws.getRicerca().isTrovato())) {
            // COB_CODE: IF WK-DT-DECOR-GRZ >= LCCC0062-DT-INF(IX-TAB-RATE) AND
            //              WK-DT-DECOR-GRZ <= LCCC0062-DT-SUP(IX-TAB-RATE)
            //                TO LCCC0490-DT-FINE-COP
            //           END-IF
            if (ws.getWkDtDecorGrz() >= ws.getAreaLccc0062().getTabRate(ws.getIxIndici().getRate()).getDtInf() && ws.getWkDtDecorGrz() <= ws.getAreaLccc0062().getTabRate(ws.getIxIndici().getRate()).getDtSup()) {
                // COB_CODE: SET TROVATO               TO TRUE
                ws.getRicerca().setTrovato();
                // COB_CODE: MOVE LCCC0062-DT-SUP(IX-TAB-RATE)
                //             TO LCCC0490-DT-FINE-COP
                lccc0490.setDtFineCop(ws.getAreaLccc0062().getTabRate(ws.getIxIndici().getRate()).getDtSup());
            }
            ws.getIxIndici().setRate(Trunc.toShort(ws.getIxIndici().getRate() + 1, 4));
        }
    }

    /**Original name: A124-PREPARA-AREA-TIT<br>
	 * <pre>----------------------------------------------------------------*
	 *   PREPARA AREA TITOLI PER ESTRARRE I TITOLI EMESSI              *
	 * ----------------------------------------------------------------*</pre>*/
    private void a124PreparaAreaTit() {
        // COB_CODE: MOVE ZERO                     TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                            IDSI0011-DATA-FINE-EFFETTO
        //                                            IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        // COB_CODE: SET IDSI0011-FETCH-FIRST      TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setFetchFirst();
        // COB_CODE: SET IDSI0011-TRATT-X-COMPETENZA TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattXCompetenza();
        // COB_CODE: SET IDSI0011-WHERE-CONDITION  TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setWhereCondition();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC    TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET  EMESSO                   TO TRUE.
        ws.getWsTpStatTit().setEmesso();
        // COB_CODE: MOVE WS-TP-STAT-TIT           TO TIT-TP-STAT-TIT.
        ws.getTitCont().setTitTpStatTit(ws.getWsTpStatTit().getWsTpStatTit());
        // COB_CODE: MOVE LCCC0490-ID-POLI         TO TIT-ID-OGG.
        ws.getTitCont().setTitIdOgg(lccc0490.getIdPoli());
        // COB_CODE: SET  POLIZZA                  TO TRUE.
        ws.getWsTpOgg().setPolizza();
        // COB_CODE: MOVE WS-TP-OGG                TO TIT-TP-OGG.
        ws.getTitCont().setTitTpOgg(ws.getWsTpOgg().getWsTpOgg());
        // COB_CODE: MOVE WK-RECUPERO-PROVV        TO TIT-TP-PRE-TIT.
        ws.getTitCont().setTitTpPreTit(ws.getWkRecuperoProvv());
        // COB_CODE: MOVE WK-PREMIO                TO TIT-TP-TIT.
        ws.getTitCont().setTitTpTit(ws.getWkPremio());
        // COB_CODE: MOVE 'LDBS0880'               TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("LDBS0880");
        // COB_CODE: MOVE  SPACES                  TO IDSI0011-BUFFER-WHERE-COND.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferWhereCond("");
        // COB_CODE: PERFORM A128-PREP-CALOLO-DT
        //              THRU A128-EX.
        a128PrepCaloloDt();
        // COB_CODE: PERFORM A129-CALOLO-DT
        //              THRU A129-EX.
        a129CaloloDt();
        // COB_CODE: IF IDSV0001-ESITO-OK
        //              MOVE TIT-CONT              TO IDSI0011-BUFFER-DATI
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: MOVE A2K-OUAMG             TO TIT-DT-INI-COP
            ws.getTitCont().getTitDtIniCop().setTitDtIniCop(ws.getIoA2kLccc0003().getOutput().getA2kOuamgX().getA2kOuamg());
            // COB_CODE: MOVE SPACES                TO IDSI0011-BUFFER-DATI
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
            // COB_CODE: MOVE TIT-CONT              TO IDSI0011-BUFFER-DATI
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getTitCont().getTitContFormatted());
        }
    }

    /**Original name: A125-ESTRAZIONE-TIT<br>
	 * <pre>----------------------------------------------------------------*
	 *     ESTRAZIONE DEI TITOLI CONTABILI
	 * ----------------------------------------------------------------*</pre>*/
    private void a125EstrazioneTit() {
        ConcatUtil concatUtil = null;
        // COB_CODE: PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC
        //                      OR NOT IDSO0011-SUCCESSFUL-SQL
        //                      OR NOT IDSV0001-ESITO-OK
        //                 END-IF
        //           END-PERFORM.
        while (!(!ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc() || !ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().isSuccessfulSql() || !areaIdsv0001.getEsito().isIdsv0001EsitoOk())) {
            // COB_CODE: PERFORM CALL-DISPATCHER
            //              THRU CALL-DISPATCHER-EX
            callDispatcher();
            // COB_CODE:            IF IDSO0011-SUCCESSFUL-RC
            //                        END-EVALUATE
            //                      ELSE
            //           *--> GESTIRE ERRORE DISPATCHER
            //                            THRU EX-S0300
            //                      END-IF
            if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
                //--> NON TROVATA
                // COB_CODE:              EVALUATE TRUE
                //           *--> NON TROVATA
                //                            WHEN IDSO0011-NOT-FOUND
                //                               END-IF
                //                            WHEN IDSO0011-SUCCESSFUL-SQL
                //           *--> OPERAZIONE ESEGUITA CORRETTAMENTE
                //                               SET IDSI0011-FETCH-NEXT   TO TRUE
                //                           WHEN OTHER
                //           *--> ERRORE DI ACCESSO AL DB
                //                                   THRU EX-S0300
                //                        END-EVALUATE
                switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                    case Idso0011SqlcodeSigned.NOT_FOUND:// COB_CODE: IF IDSI0011-FETCH-FIRST
                        //              END-IF
                        //           END-IF
                        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchFirst()) {
                            // COB_CODE: IF WK-DT-DECOR-GRZ > WK-DT-ULT-TIT-INC
                            //              SET LCCC0490-NO-PRESENZA-RATEO TO TRUE
                            //           ELSE
                            //              END-IF
                            //           END-IF
                            if (ws.getWkDtDecorGrz() > ws.getWkDtUltTitInc()) {
                                // COB_CODE: SET LCCC0490-NO-PRESENZA-RATEO TO TRUE
                                lccc0490.getPresenzaRateo().setNoPresenzaRateo();
                            }
                            else if (ws.getWkDtDecorGrz() == ws.getWkDtUltTitInc()) {
                                // COB_CODE: IF WK-DT-DECOR-GRZ = WK-DT-ULT-TIT-INC
                                //                TO LCCC0490-DT-FINE-COP
                                //           END-IF
                                // COB_CODE: MOVE WK-DT-ULT-TIT-INC
                                //             TO LCCC0490-DT-FINE-COP
                                lccc0490.setDtFineCopFormatted(ws.getWkDtUltTitIncFormatted());
                            }
                        }
                        break;

                    case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://--> OPERAZIONE ESEGUITA CORRETTAMENTE
                        // COB_CODE: MOVE IDSO0011-BUFFER-DATI TO TIT-CONT
                        ws.getTitCont().setTitContFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                        // COB_CODE: IF IDSI0011-FETCH-FIRST
                        //                 THRU A121-EX
                        //           END-IF
                        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchFirst()) {
                            // COB_CODE: MOVE TIT-DT-END-COP    TO WK-DT-SUPERIORE
                            //                LCCC0490-DT-FINE-COP
                            ws.setWkDtSuperiore(TruncAbs.toInt(ws.getTitCont().getTitDtEndCop().getTitDtEndCop(), 8));
                            lccc0490.setDtFineCop(TruncAbs.toInt(ws.getTitCont().getTitDtEndCop().getTitDtEndCop(), 8));
                            // COB_CODE: PERFORM A121-CALCOLO-DIFF-DATE
                            //              THRU A121-EX
                            a121CalcoloDiffDate();
                        }
                        // COB_CODE: ADD 1                     TO IX-TAB-TIT
                        ws.getIxIndici().setTit(Trunc.toShort(1 + ws.getIxIndici().getTit(), 4));
                        // COB_CODE: PERFORM VALORIZZA-OUTPUT-TIT
                        //              THRU VALORIZZA-OUTPUT-TIT-EX
                        valorizzaOutputTit();
                        // COB_CODE: ADD 1                     TO WTIT-ELE-TIT-MAX
                        ws.setWtitEleTitMax(Trunc.toShort(1 + ws.getWtitEleTitMax(), 4));
                        // COB_CODE: IF WTIT-ELE-TIT-MAX > 1
                        //              SET LCCC0490-SI-TIT-EMESSI TO TRUE
                        //           END-IF
                        if (ws.getWtitEleTitMax() > 1) {
                            // COB_CODE: SET LCCC0490-SI-TIT-EMESSI TO TRUE
                            lccc0490.getTitoliEmessi().setSiTitEmessi();
                        }
                        // COB_CODE: SET IDSI0011-FETCH-NEXT   TO TRUE
                        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setFetchNext();
                        break;

                    default://--> ERRORE DI ACCESSO AL DB
                        // COB_CODE: MOVE WK-PGM
                        //             TO IEAI9901-COD-SERVIZIO-BE
                        ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                        // COB_CODE: MOVE 'A125-ESTRAZIONE-TIT'
                        //             TO IEAI9901-LABEL-ERR
                        ws.getIeai9901Area().setLabelErr("A125-ESTRAZIONE-TIT");
                        // COB_CODE: MOVE '005016'
                        //             TO IEAI9901-COD-ERRORE
                        ws.getIeai9901Area().setCodErroreFormatted("005016");
                        // COB_CODE: STRING 'TABELLA TITOLO CONTABILE'     ';'
                        //                  IDSO0011-RETURN-CODE   ';'
                        //                  IDSO0011-SQLCODE
                        //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                        //           END-STRING
                        concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "TABELLA TITOLO CONTABILE", ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                        ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                        // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        //              THRU EX-S0300
                        s0300RicercaGravitaErrore();
                        break;
                }
            }
            else {
                //--> GESTIRE ERRORE DISPATCHER
                // COB_CODE: MOVE WK-PGM
                //             TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE 'A125-ESTRAZIONE-TIT'
                //             TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr("A125-ESTRAZIONE-TIT");
                // COB_CODE: MOVE '005016'
                //             TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("005016");
                // COB_CODE: STRING 'TABELLA TITOLO CONTABILE'     ';'
                //                  IDSO0011-RETURN-CODE ';'
                //                  IDSO0011-SQLCODE
                //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "TABELLA TITOLO CONTABILE", ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
            }
        }
    }

    /**Original name: A126-PREPARA-AREA-DTC<br>
	 * <pre>----------------------------------------------------------------*
	 *   PREPARA AREA DETTAGLIO TITOLI PER ESTRARRE I DETTAGLI TITOLI  *
	 *   ASSOCIATI AI TITOLI EMESSI ESTRATTI IN PRECEDENZA             *
	 * ----------------------------------------------------------------*</pre>*/
    private void a126PreparaAreaDtc() {
        // COB_CODE: MOVE WS-DT-INFINITO-1-N       TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                            IDSI0011-DATA-FINE-EFFETTO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(ws.getIdsv0015().getDtInfinito1N());
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(ws.getIdsv0015().getDtInfinito1N());
        // COB_CODE: MOVE WS-TS-INFINITO-1-N          TO IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(ws.getIdsv0015().getTsInfinito1N());
        // COB_CODE: SET IDSI0011-TRATT-X-COMPETENZA  TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattXCompetenza();
    }

    /**Original name: A127-ESTRAZIONE-DTC<br>
	 * <pre>----------------------------------------------------------------*
	 *     ESTRAZIONE DETTAGLI TITOLO CONTABILE
	 * ----------------------------------------------------------------*</pre>*/
    private void a127EstrazioneDtc() {
        // COB_CODE: PERFORM VARYING IX-TAB-TIT FROM 1 BY 1
        //           UNTIL IX-TAB-TIT > WTIT-ELE-TIT-MAX
        //              END-PERFORM
        //           END-PERFORM.
        ConcatUtil concatUtil = null;
        ws.getIxIndici().setTit(((short)1));
        while (!(ws.getIxIndici().getTit() > ws.getWtitEleTitMax())) {
            // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL       TO TRUE
            ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
            // COB_CODE: SET IDSO0011-SUCCESSFUL-RC        TO TRUE
            ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
            // COB_CODE: SET IDSI0011-FETCH-FIRST          TO TRUE
            ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setFetchFirst();
            //--> MODALITA DI ACCESSO
            // COB_CODE: SET IDSI0011-ID-PADRE             TO TRUE
            ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setIdsi0011IdPadre();
            // COB_CODE: MOVE WTIT-ID-TIT-CONT(IX-TAB-TIT) TO DTC-ID-TIT-CONT
            ws.getDettTitCont().setDtcIdTitCont(ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitIdTitCont());
            //--> NOME TABELLA FISICA DB
            // COB_CODE: MOVE 'DETT-TIT-CONT'        TO IDSI0011-CODICE-STR-DATO
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("DETT-TIT-CONT");
            //--> DCLGEN TABELLA
            // COB_CODE: MOVE DETT-TIT-CONT          TO IDSI0011-BUFFER-DATI
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getDettTitCont().getDettTitContFormatted());
            // COB_CODE: PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC
            //                      OR NOT IDSO0011-SUCCESSFUL-SQL
            //                      OR NOT IDSV0001-ESITO-OK
            //                 END-IF
            //           END-PERFORM
            while (!(!ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc() || !ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().isSuccessfulSql() || !areaIdsv0001.getEsito().isIdsv0001EsitoOk())) {
                // COB_CODE: PERFORM CALL-DISPATCHER
                //              THRU CALL-DISPATCHER-EX
                callDispatcher();
                // COB_CODE:                  IF IDSO0011-SUCCESSFUL-RC
                //                           END-EVALUATE
                //                         ELSE
                //           *--> GESTIRE ERRORE DISPATCHER
                //                               THRU EX-S0300
                //                         END-IF
                if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
                    //--> NON TROVATA
                    // COB_CODE:                    EVALUATE TRUE
                    //           *--> NON TROVATA
                    //                                  WHEN IDSO0011-NOT-FOUND
                    //                                   END-IF
                    //           *--> OPERAZIONE ESEGUITA CORRETTAMENTE
                    //                                  WHEN IDSO0011-SUCCESSFUL-SQL
                    //                                   SET IDSI0011-FETCH-NEXT   TO TRUE
                    //           *--> ERRORE DI ACCESSO AL DB
                    //                                  WHEN OTHER
                    //                                      THRU EX-S0300
                    //                           END-EVALUATE
                    switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                        case Idso0011SqlcodeSigned.NOT_FOUND:// COB_CODE: IF IDSI0011-FETCH-FIRST
                            //                 THRU EX-S0300
                            //           END-IF
                            if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchFirst()) {
                                // COB_CODE: MOVE WK-PGM
                                //             TO IEAI9901-COD-SERVIZIO-BE
                                ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                                // COB_CODE: MOVE 'A127-ESTRAZIONE-DTC'
                                //             TO IEAI9901-LABEL-ERR
                                ws.getIeai9901Area().setLabelErr("A127-ESTRAZIONE-DTC");
                                // COB_CODE: MOVE '005016'
                                //             TO IEAI9901-COD-ERRORE
                                ws.getIeai9901Area().setCodErroreFormatted("005016");
                                // COB_CODE: STRING 'DETT-TIT-CONT'      ';'
                                //                  IDSO0011-RETURN-CODE ';'
                                //                  IDSO0011-SQLCODE
                                //                  DELIMITED BY SIZE INTO
                                //                  IEAI9901-PARAMETRI-ERR
                                //           END-STRING
                                concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "DETT-TIT-CONT", ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                                ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                                //              THRU EX-S0300
                                s0300RicercaGravitaErrore();
                            }
                            //--> OPERAZIONE ESEGUITA CORRETTAMENTE
                            break;

                        case Idso0011SqlcodeSigned.SUCCESSFUL_SQL:// COB_CODE: MOVE IDSO0011-BUFFER-DATI
                            //             TO DETT-TIT-CONT
                            ws.getDettTitCont().setDettTitContFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                            // COB_CODE: ADD 1
                            //             TO IX-TAB-DTC
                            ws.getIxIndici().setDtc(Trunc.toShort(1 + ws.getIxIndici().getDtc(), 4));
                            // COB_CODE: PERFORM VALORIZZA-OUTPUT-DTC
                            //              THRU VALORIZZA-OUTPUT-DTC-EX
                            //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LCCS0490.cbl:line=941, because the code is unreachable.
                            // COB_CODE: ADD 1
                            //             TO WDTC-ELE-DETT-TIT-MAX
                            ws.setWdtcEleDettTitMax(Trunc.toShort(1 + ws.getWdtcEleDettTitMax(), 4));
                            // COB_CODE: SET IDSI0011-FETCH-NEXT   TO TRUE
                            ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setFetchNext();
                            //--> ERRORE DI ACCESSO AL DB
                            break;

                        default:// COB_CODE: MOVE WK-PGM
                            //             TO IEAI9901-COD-SERVIZIO-BE
                            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                            // COB_CODE: MOVE 'A127-ESTRAZIONE-DTC'
                            //             TO IEAI9901-LABEL-ERR
                            ws.getIeai9901Area().setLabelErr("A127-ESTRAZIONE-DTC");
                            // COB_CODE: MOVE '005016'
                            //             TO IEAI9901-COD-ERRORE
                            ws.getIeai9901Area().setCodErroreFormatted("005016");
                            // COB_CODE: STRING 'TABELLA DETT TITOLO CONTABILE' ';'
                            //                  IDSO0011-RETURN-CODE            ';'
                            //                  IDSO0011-SQLCODE
                            //                  DELIMITED BY SIZE INTO
                            //                  IEAI9901-PARAMETRI-ERR
                            //           END-STRING
                            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "TABELLA DETT TITOLO CONTABILE", ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                            //              THRU EX-S0300
                            s0300RicercaGravitaErrore();
                            break;
                    }
                }
                else {
                    //--> GESTIRE ERRORE DISPATCHER
                    // COB_CODE: MOVE WK-PGM
                    //             TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'A127-ESTRAZIONE-DTC'
                    //             TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("A127-ESTRAZIONE-DTC");
                    // COB_CODE: MOVE '005016'
                    //             TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005016");
                    // COB_CODE: STRING 'DETT-TIT-CONT'      ';'
                    //                  IDSO0011-RETURN-CODE ';'
                    //                  IDSO0011-SQLCODE
                    //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "DETT-TIT-CONT", ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                }
            }
            ws.getIxIndici().setTit(Trunc.toShort(ws.getIxIndici().getTit() + 1, 4));
        }
    }

    /**Original name: A128-PREP-CALOLO-DT<br>
	 * <pre>----------------------------------------------------------------*
	 *  VIENE SOTTRATTO UN GIORNO ALLA DATA EFFETTO                    *
	 * ----------------------------------------------------------------*</pre>*/
    private void a128PrepCaloloDt() {
        // COB_CODE: INITIALIZE IO-A2K-LCCC0003.
        initIoA2kLccc0003();
        // COB_CODE: MOVE '03'                      TO A2K-FUNZ.
        ws.getIoA2kLccc0003().getInput().setA2kFunz("03");
        // COB_CODE: MOVE '03'                      TO A2K-INFO.
        ws.getIoA2kLccc0003().getInput().setA2kInfo("03");
        // COB_CODE: MOVE 1                         TO A2K-DELTA.
        ws.getIoA2kLccc0003().getInput().setA2kDelta(((short)1));
        // COB_CODE: MOVE IDSV0001-DATA-EFFETTO     TO A2K-INDATA.
        ws.getIoA2kLccc0003().getInput().getA2kIndata().setA2kIndata(areaIdsv0001.getAreaComune().getIdsv0001DataEffettoFormatted());
        // COB_CODE: MOVE '0'                       TO A2K-FISLAV
        //                                             A2K-INICON.
        ws.getIoA2kLccc0003().getInput().setA2kFislavFormatted("0");
        ws.getIoA2kLccc0003().getInput().setA2kIniconFormatted("0");
    }

    /**Original name: A129-CALOLO-DT<br>
	 * <pre>----------------------------------------------------------------*
	 *  RICHIAMO ROUTINE CALCOLO DATA                                  *
	 * ----------------------------------------------------------------*</pre>*/
    private void a129CaloloDt() {
        Lccs0003 lccs0003 = null;
        GenericParam inRcode = null;
        // COB_CODE: CALL LCCS0003 USING IO-A2K-LCCC0003 IN-RCODE
        //           ON EXCEPTION
        //                 THRU EX-S0290
        //           END-CALL.
        try {
            lccs0003 = Lccs0003.getInstance();
            inRcode = new GenericParam(MarshalByteExt.strToBuffer(ws.getInRcodeFormatted(), Lccs0490Data.Len.IN_RCODE));
            lccs0003.run(ws.getIoA2kLccc0003(), inRcode);
            ws.setInRcodeFromBuffer(inRcode.getByteData());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-PGM                 TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'SERVIZIO OPERAZIONI SULLE DATE'
            //                                       TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("SERVIZIO OPERAZIONI SULLE DATE");
            // COB_CODE: PERFORM S0290-ERRORE-DI-SISTEMA
            //              THRU EX-S0290
            s0290ErroreDiSistema();
        }
        // COB_CODE: IF IN-RCODE NOT = '00'
        //                 THRU EX-S0300
        //           END-IF.
        if (!ws.getInRcodeFormatted().equals("00")) {
            // COB_CODE: MOVE WK-PGM                 TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'A129-CALOLO-DT'       TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("A129-CALOLO-DT");
            // COB_CODE: MOVE '005016'               TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: MOVE SPACES                 TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: S0290-ERRORE-DI-SISTEMA<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES GESTIONE ERRORE
	 * ----------------------------------------------------------------*
	 * **-------------------------------------------------------------**
	 *     PROGRAMMA ..... IERP9902
	 *     TIPOLOGIA...... COPY
	 *     DESCRIZIONE.... ROUTINE GENERALIZZATA GESTIONE ERRORI CALL
	 * **-------------------------------------------------------------**</pre>*/
    private void s0290ErroreDiSistema() {
        // COB_CODE: MOVE '005006'           TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErroreFormatted("005006");
        // COB_CODE: MOVE CALL-DESC          TO IEAI9901-PARAMETRI-ERR.
        ws.getIeai9901Area().setParametriErr(ws.getIdsv0002().getCallDesc());
        // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
        //              THRU EX-S0300.
        s0300RicercaGravitaErrore();
    }

    /**Original name: S0300-RICERCA-GRAVITA-ERRORE<br>
	 * <pre>MUOVO L'AREA DEL SERVIZIO NELL'AREA DATI DELL'AREA DI CONTESTO.
	 * ----->VALORIZZO I CAMPI DELLA IDSV0001</pre>*/
    private void s0300RicercaGravitaErrore() {
        Ieas9900 ieas9900 = null;
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR       TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE  'IEAS9900'              TO CALL-PGM
        ws.getIdsv0002().setCallPgm("IEAS9900");
        // COB_CODE: CALL CALL-PGM USING IDSV0001-AREA-CONTESTO
        //                               IEAI9901-AREA
        //                               IEAO9901-AREA
        //             ON EXCEPTION
        //                   THRU EX-S0310-ERRORE-FATALE
        //           END-CALL.
        try {
            ieas9900 = Ieas9900.getInstance();
            ieas9900.run(areaIdsv0001, ws.getIeai9901Area(), ws.getIeao9901Area());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
            areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
            // COB_CODE: PERFORM S0310-ERRORE-FATALE
            //              THRU EX-S0310-ERRORE-FATALE
            s0310ErroreFatale();
        }
        //SE LA CHIAMATA AL SERVIZIO HA ESITO POSITIVO (NESSUN ERRORE DI
        //SISTEMA, VALORIZZO L'AREA DI OUTPUT.
        //INCREMENTO L'INDICE DEGLI ERRORI
        // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
        areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
        //SE L'INDICE E' MINORE DI 10 ...
        // COB_CODE:      IF IDSV0001-MAX-ELE-ERRORI NOT GREATER 10
        //           *SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
        //                   END-IF
        //                ELSE
        //           *IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        //                       TO IDSV0001-DESC-ERRORE-ESTESA
        //                END-IF.
        if (areaIdsv0001.getMaxEleErrori() <= 10) {
            //SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
            // COB_CODE: IF IEAO9901-COD-ERRORE-990 = ZEROES
            //                      EX-S0300-IMPOSTA-ERRORE
            //           ELSE
            //                   IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
            //           END-IF
            if (Characters.EQ_ZERO.test(ws.getIeao9901Area().getCodErrore990Formatted())) {
                // COB_CODE: PERFORM S0300-IMPOSTA-ERRORE THRU
                //                   EX-S0300-IMPOSTA-ERRORE
                s0300ImpostaErrore();
            }
            else {
                // COB_CODE: MOVE 'IEAS9900'
                //             TO IDSV0001-COD-SERVIZIO-BE
                areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
                // COB_CODE: MOVE IEAO9901-LABEL-ERR-990
                //             TO IDSV0001-LABEL-ERR
                areaIdsv0001.getLogErrore().setLabelErr(ws.getIeao9901Area().getLabelErr990());
                // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
                //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
                // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
                areaIdsv0001.getEsito().setIdsv0001EsitoKo();
                // IMPOSTO IL CODICE ERRORE GESTITO ALL'INTERNO DEL PROGRAMMA
                // COB_CODE: MOVE IEAO9901-COD-ERRORE-990
                //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeao9901Area().getCodErrore990Formatted());
                // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
                //             TO IDSV0001-DESC-ERRORE-ESTESA
                //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
            }
        }
        else {
            //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
            // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
            //             TO IDSV0001-LABEL-ERR
            areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
            // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 'IEAS9900'
            //             TO IDSV0001-COD-SERVIZIO-BE
            areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
            // COB_CODE: MOVE 'OVERFLOW IMPAGINAZIONE ERRORI'
            //             TO IDSV0001-DESC-ERRORE-ESTESA
            areaIdsv0001.getLogErrore().setDescErroreEstesa("OVERFLOW IMPAGINAZIONE ERRORI");
        }
    }

    /**Original name: S0300-IMPOSTA-ERRORE<br>
	 * <pre>  se la gravit— di tutti gli errori dell'elaborazione
	 *   h minore di zero ( ad esempio -3) vuol dire che il return-code
	 *   dell'elaborazione complessiva deve essere abbassato da '08' a
	 *   '04'. Per fare cir utilizzo il flag IDSV0001-FORZ-RC-04
	 * IMPOSTO LA GRAVITA'</pre>*/
    private void s0300ImpostaErrore() {
        // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
        // COB_CODE: EVALUATE TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = -3
        //                       IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        //             WHEN  IEAO9901-LIV-GRAVITA = 0 OR 1 OR 2
        //                   SET IDSV0001-FORZ-RC-04-NO  TO TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = 3 OR 4
        //                   SET IDSV0001-ESITO-KO       TO TRUE
        //           END-EVALUATE
        if (ws.getIeao9901Area().getLivGravita() == -3) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-YES TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04Yes();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 2  TO
            //               IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
            areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)2));
        }
        else if (ws.getIeao9901Area().getLivGravita() == 0 || ws.getIeao9901Area().getLivGravita() == 1 || ws.getIeao9901Area().getLivGravita() == 2) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
        }
        else if (ws.getIeao9901Area().getLivGravita() == 3 || ws.getIeao9901Area().getLivGravita() == 4) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        }
        // IMPOSTO IL CODICE ERRORE
        // COB_CODE: MOVE IEAI9901-COD-ERRORE
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeai9901Area().getCodErroreFormatted());
        // SE IL LIVELLO DI GRAVITA' RESTITUITO DALLO IAS9900 E' MAGGIORE
        // DI 2 (ERRORE BLOCCANTE)...IMPOSTO L'ESITO E I DATI DI CONTESTO
        //RELATIVI ALL'ERRORE BLOCCANTE
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE
        //             TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR
        //             TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
        //             TO IDSV0001-DESC-ERRORE-ESTESA
        //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
        // COB_CODE: MOVE SPACES          TO IEAI9901-COD-SERVIZIO-BE
        //                                  IEAI9901-LABEL-ERR
        //                                   IEAI9901-PARAMETRI-ERR.
        ws.getIeai9901Area().setCodServizioBe("");
        ws.getIeai9901Area().setLabelErr("");
        ws.getIeai9901Area().setParametriErr("");
        // COB_CODE: MOVE ZEROES          TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErrore(0);
    }

    /**Original name: S0310-ERRORE-FATALE<br>
	 * <pre>IMPOSTO LA GRAVITA' ERRORE</pre>*/
    private void s0310ErroreFatale() {
        // COB_CODE: MOVE  3
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)3));
        // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
        areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        //IMPOSTO IL CODICE ERRORE (ERRORE FISSO)
        // COB_CODE: MOVE 900001
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErrore(900001);
        //IMPOSTO NOME DEL MODULO
        // COB_CODE: MOVE 'IEAS9900'               TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
        //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
        //              TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
        // COB_CODE: MOVE  'ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900'
        //              TO IDSV0001-DESC-ERRORE-ESTESA
        //                 IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
    }

    /**Original name: CALL-DISPATCHER<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES CALL DISPATCHER
	 * ----------------------------------------------------------------*
	 * *****************************************************************
	 *    CALL DISPATCHER
	 * *****************************************************************</pre>*/
    private void callDispatcher() {
        Idss0010 idss0010 = null;
        // COB_CODE: MOVE IDSV0001-MODALITA-ESECUTIVA
        //                              TO IDSI0011-MODALITA-ESECUTIVA
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011ModalitaEsecutiva().setIdsi0011ModalitaEsecutiva(areaIdsv0001.getAreaComune().getModalitaEsecutiva().getModalitaEsecutiva());
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //                              TO IDSI0011-CODICE-COMPAGNIA-ANIA
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceCompagniaAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: MOVE IDSV0001-COD-MAIN-BATCH
        //                              TO IDSI0011-COD-MAIN-BATCH
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodMainBatch(areaIdsv0001.getAreaComune().getCodMainBatch());
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO
        //                              TO IDSI0011-TIPO-MOVIMENTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011TipoMovimentoFormatted(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimentoFormatted());
        // COB_CODE: MOVE IDSV0001-SESSIONE
        //                              TO IDSI0011-SESSIONE
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011Sessione(areaIdsv0001.getAreaComune().getSessione());
        // COB_CODE: MOVE IDSV0001-USER-NAME
        //                              TO IDSI0011-USER-NAME
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011UserName(areaIdsv0001.getAreaComune().getUserName());
        // COB_CODE: IF IDSI0011-DATA-INIZIO-EFFETTO = 0
        //              END-IF
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataInizioEffetto() == 0) {
            // COB_CODE: IF IDSV0001-LETT-ULT-IMMAGINE-SI
            //              END-IF
            //           ELSE
            //                TO IDSI0011-DATA-INIZIO-EFFETTO
            //           END-IF
            if (areaIdsv0001.getAreaComune().getLettUltImmagine().isIdsv0001LettUltImmagineSi()) {
                // COB_CODE: IF IDSI0011-SELECT
                //           OR IDSI0011-FETCH-FIRST
                //           OR IDSI0011-FETCH-NEXT
                //                TO IDSI0011-DATA-COMPETENZA
                //           ELSE
                //                TO IDSI0011-DATA-INIZIO-EFFETTO
                //           END-IF
                if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isSelect() || ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchFirst() || ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchNext()) {
                    // COB_CODE: MOVE 99991230
                    //             TO IDSI0011-DATA-INIZIO-EFFETTO
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(99991230);
                    // COB_CODE: MOVE 999912304023595999
                    //             TO IDSI0011-DATA-COMPETENZA
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(999912304023595999L);
                }
                else {
                    // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
                    //             TO IDSI0011-DATA-INIZIO-EFFETTO
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
                }
            }
            else {
                // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
                //             TO IDSI0011-DATA-INIZIO-EFFETTO
                ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
            }
        }
        // COB_CODE: IF IDSI0011-DATA-FINE-EFFETTO = 0
        //              MOVE 99991231   TO IDSI0011-DATA-FINE-EFFETTO
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataFineEffetto() == 0) {
            // COB_CODE: MOVE 99991231   TO IDSI0011-DATA-FINE-EFFETTO
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(99991231);
        }
        // COB_CODE: IF IDSI0011-DATA-COMPETENZA = 0
        //                              TO IDSI0011-DATA-COMPETENZA
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataCompetenza() == 0) {
            // COB_CODE: MOVE IDSV0001-DATA-COMPETENZA
            //                           TO IDSI0011-DATA-COMPETENZA
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(areaIdsv0001.getAreaComune().getIdsv0001DataCompetenza());
        }
        // COB_CODE: MOVE IDSV0001-DATA-COMP-AGG-STOR
        //                              TO IDSI0011-DATA-COMP-AGG-STOR
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompAggStor(areaIdsv0001.getAreaComune().getIdsv0001DataCompAggStor());
        // COB_CODE: IF IDSI0011-TRATT-DEFAULT
        //                              TO IDSI0011-TRATTAMENTO-STORICITA
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().isTrattDefault()) {
            // COB_CODE: MOVE IDSV0001-TRATTAMENTO-STORICITA
            //                           TO IDSI0011-TRATTAMENTO-STORICITA
            ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattamentoStoricita(areaIdsv0001.getAreaComune().getTrattamentoStoricita().getTrattamentoStoricita());
        }
        // COB_CODE: MOVE IDSV0001-FORMATO-DATA-DB
        //                              TO IDSI0011-FORMATO-DATA-DB
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011FormatoDataDb().setIdsi0011FormatoDataDb(areaIdsv0001.getAreaComune().getFormatoDataDb().getFormatoDataDb());
        // COB_CODE: MOVE IDSV0001-LIVELLO-DEBUG
        //                              TO IDSI0011-LIVELLO-DEBUG
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloDebug().setIdsi0011LivelloDebugFormatted(areaIdsv0001.getAreaComune().getLivelloDebug().getIdsi0011LivelloDebugFormatted());
        // COB_CODE: MOVE 0             TO IDSI0011-ID-MOVI-ANNULLATO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011IdMoviAnnullato(0);
        // COB_CODE: SET IDSI0011-PTF-NEWLIFE          TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011IdentitaChiamante().setPtfNewlife();
        // COB_CODE: SET IDSV0012-STATUS-SHARED-MEM-KO TO TRUE
        ws.getIdsv00122().getStatusSharedMemory().setKo();
        // COB_CODE: MOVE 'IDSS0010'             TO IDSI0011-PGM
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011Pgm("IDSS0010");
        //     SET WS-ADDRESS-DIS TO ADDRESS OF IN-OUT-IDSS0010.
        // COB_CODE: CALL IDSI0011-PGM           USING IDSI0011-AREA
        //                                             IDSO0011-AREA.
        idss0010 = Idss0010.getInstance();
        idss0010.run(ws.getDispatcherVariables(), ws.getDispatcherVariables().getIdso0011Area());
        // COB_CODE: MOVE IDSO0011-SQLCODE-SIGNED
        //                                  TO IDSO0011-SQLCODE.
        ws.getDispatcherVariables().getIdso0011Area().setSqlcode(ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned());
    }

    /**Original name: VALORIZZA-OUTPUT-TIT<br>
	 * <pre>----------------------------------------------------------------*
	 *     COPY LETTURA TABELLE
	 * ----------------------------------------------------------------*
	 * ------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    VALORIZZA OUTPUT COPY LCCVTIT3
	 *    ULTIMO AGG. 05 DIC 2014
	 * ------------------------------------------------------------</pre>*/
    private void valorizzaOutputTit() {
        // COB_CODE: MOVE TIT-ID-TIT-CONT
        //             TO (SF)-ID-PTF(IX-TAB-TIT)
        ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().setIdPtf(ws.getTitCont().getTitIdTitCont());
        // COB_CODE: MOVE TIT-ID-TIT-CONT
        //             TO (SF)-ID-TIT-CONT(IX-TAB-TIT)
        ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().setWtitIdTitCont(ws.getTitCont().getTitIdTitCont());
        // COB_CODE: MOVE TIT-ID-OGG
        //             TO (SF)-ID-OGG(IX-TAB-TIT)
        ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().setWtitIdOgg(ws.getTitCont().getTitIdOgg());
        // COB_CODE: MOVE TIT-TP-OGG
        //             TO (SF)-TP-OGG(IX-TAB-TIT)
        ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().setWtitTpOgg(ws.getTitCont().getTitTpOgg());
        // COB_CODE: IF TIT-IB-RICH-NULL = HIGH-VALUES
        //                TO (SF)-IB-RICH-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-IB-RICH(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitIbRich(), TitCont.Len.TIT_IB_RICH)) {
            // COB_CODE: MOVE TIT-IB-RICH-NULL
            //             TO (SF)-IB-RICH-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().setWtitIbRich(ws.getTitCont().getTitIbRich());
        }
        else {
            // COB_CODE: MOVE TIT-IB-RICH
            //             TO (SF)-IB-RICH(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().setWtitIbRich(ws.getTitCont().getTitIbRich());
        }
        // COB_CODE: MOVE TIT-ID-MOVI-CRZ
        //             TO (SF)-ID-MOVI-CRZ(IX-TAB-TIT)
        ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().setWtitIdMoviCrz(ws.getTitCont().getTitIdMoviCrz());
        // COB_CODE: IF TIT-ID-MOVI-CHIU-NULL = HIGH-VALUES
        //                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-ID-MOVI-CHIU(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitIdMoviChiu().getTitIdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE TIT-ID-MOVI-CHIU-NULL
            //             TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitIdMoviChiu().setWtitIdMoviChiuNull(ws.getTitCont().getTitIdMoviChiu().getTitIdMoviChiuNull());
        }
        else {
            // COB_CODE: MOVE TIT-ID-MOVI-CHIU
            //             TO (SF)-ID-MOVI-CHIU(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitIdMoviChiu().setWtitIdMoviChiu(ws.getTitCont().getTitIdMoviChiu().getTitIdMoviChiu());
        }
        // COB_CODE: MOVE TIT-DT-INI-EFF
        //             TO (SF)-DT-INI-EFF(IX-TAB-TIT)
        ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().setWtitDtIniEff(ws.getTitCont().getTitDtIniEff());
        // COB_CODE: MOVE TIT-DT-END-EFF
        //             TO (SF)-DT-END-EFF(IX-TAB-TIT)
        ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().setWtitDtEndEff(ws.getTitCont().getTitDtEndEff());
        // COB_CODE: MOVE TIT-COD-COMP-ANIA
        //             TO (SF)-COD-COMP-ANIA(IX-TAB-TIT)
        ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().setWtitCodCompAnia(ws.getTitCont().getTitCodCompAnia());
        // COB_CODE: MOVE TIT-TP-TIT
        //             TO (SF)-TP-TIT(IX-TAB-TIT)
        ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().setWtitTpTit(ws.getTitCont().getTitTpTit());
        // COB_CODE: IF TIT-PROG-TIT-NULL = HIGH-VALUES
        //                TO (SF)-PROG-TIT-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-PROG-TIT(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitProgTit().getTitProgTitNullFormatted())) {
            // COB_CODE: MOVE TIT-PROG-TIT-NULL
            //             TO (SF)-PROG-TIT-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitProgTit().setWtitProgTitNull(ws.getTitCont().getTitProgTit().getTitProgTitNull());
        }
        else {
            // COB_CODE: MOVE TIT-PROG-TIT
            //             TO (SF)-PROG-TIT(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitProgTit().setWtitProgTit(ws.getTitCont().getTitProgTit().getTitProgTit());
        }
        // COB_CODE: MOVE TIT-TP-PRE-TIT
        //             TO (SF)-TP-PRE-TIT(IX-TAB-TIT)
        ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().setWtitTpPreTit(ws.getTitCont().getTitTpPreTit());
        // COB_CODE: MOVE TIT-TP-STAT-TIT
        //             TO (SF)-TP-STAT-TIT(IX-TAB-TIT)
        ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().setWtitTpStatTit(ws.getTitCont().getTitTpStatTit());
        // COB_CODE: IF TIT-DT-INI-COP-NULL = HIGH-VALUES
        //                TO (SF)-DT-INI-COP-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-DT-INI-COP(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitDtIniCop().getTitDtIniCopNullFormatted())) {
            // COB_CODE: MOVE TIT-DT-INI-COP-NULL
            //             TO (SF)-DT-INI-COP-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitDtIniCop().setWtitDtIniCopNull(ws.getTitCont().getTitDtIniCop().getTitDtIniCopNull());
        }
        else {
            // COB_CODE: MOVE TIT-DT-INI-COP
            //             TO (SF)-DT-INI-COP(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitDtIniCop().setWtitDtIniCop(ws.getTitCont().getTitDtIniCop().getTitDtIniCop());
        }
        // COB_CODE: IF TIT-DT-END-COP-NULL = HIGH-VALUES
        //                TO (SF)-DT-END-COP-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-DT-END-COP(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitDtEndCop().getTitDtEndCopNullFormatted())) {
            // COB_CODE: MOVE TIT-DT-END-COP-NULL
            //             TO (SF)-DT-END-COP-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitDtEndCop().setWtitDtEndCopNull(ws.getTitCont().getTitDtEndCop().getTitDtEndCopNull());
        }
        else {
            // COB_CODE: MOVE TIT-DT-END-COP
            //             TO (SF)-DT-END-COP(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitDtEndCop().setWtitDtEndCop(ws.getTitCont().getTitDtEndCop().getTitDtEndCop());
        }
        // COB_CODE: IF TIT-IMP-PAG-NULL = HIGH-VALUES
        //                TO (SF)-IMP-PAG-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-IMP-PAG(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitImpPag().getTitImpPagNullFormatted())) {
            // COB_CODE: MOVE TIT-IMP-PAG-NULL
            //             TO (SF)-IMP-PAG-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitImpPag().setWtitImpPagNull(ws.getTitCont().getTitImpPag().getTitImpPagNull());
        }
        else {
            // COB_CODE: MOVE TIT-IMP-PAG
            //             TO (SF)-IMP-PAG(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitImpPag().setWtitImpPag(Trunc.toDecimal(ws.getTitCont().getTitImpPag().getTitImpPag(), 15, 3));
        }
        // COB_CODE: IF TIT-FL-SOLL-NULL = HIGH-VALUES
        //                TO (SF)-FL-SOLL-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-FL-SOLL(IX-TAB-TIT)
        //           END-IF
        if (Conditions.eq(ws.getTitCont().getTitFlSoll(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE TIT-FL-SOLL-NULL
            //             TO (SF)-FL-SOLL-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().setWtitFlSoll(ws.getTitCont().getTitFlSoll());
        }
        else {
            // COB_CODE: MOVE TIT-FL-SOLL
            //             TO (SF)-FL-SOLL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().setWtitFlSoll(ws.getTitCont().getTitFlSoll());
        }
        // COB_CODE: IF TIT-FRAZ-NULL = HIGH-VALUES
        //                TO (SF)-FRAZ-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-FRAZ(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitFraz().getTitFrazNullFormatted())) {
            // COB_CODE: MOVE TIT-FRAZ-NULL
            //             TO (SF)-FRAZ-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitFraz().setWtitFrazNull(ws.getTitCont().getTitFraz().getTitFrazNull());
        }
        else {
            // COB_CODE: MOVE TIT-FRAZ
            //             TO (SF)-FRAZ(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitFraz().setWtitFraz(ws.getTitCont().getTitFraz().getTitFraz());
        }
        // COB_CODE: IF TIT-DT-APPLZ-MORA-NULL = HIGH-VALUES
        //                TO (SF)-DT-APPLZ-MORA-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-DT-APPLZ-MORA(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitDtApplzMora().getTitDtApplzMoraNullFormatted())) {
            // COB_CODE: MOVE TIT-DT-APPLZ-MORA-NULL
            //             TO (SF)-DT-APPLZ-MORA-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitDtApplzMora().setWtitDtApplzMoraNull(ws.getTitCont().getTitDtApplzMora().getTitDtApplzMoraNull());
        }
        else {
            // COB_CODE: MOVE TIT-DT-APPLZ-MORA
            //             TO (SF)-DT-APPLZ-MORA(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitDtApplzMora().setWtitDtApplzMora(ws.getTitCont().getTitDtApplzMora().getTitDtApplzMora());
        }
        // COB_CODE: IF TIT-FL-MORA-NULL = HIGH-VALUES
        //                TO (SF)-FL-MORA-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-FL-MORA(IX-TAB-TIT)
        //           END-IF
        if (Conditions.eq(ws.getTitCont().getTitFlMora(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE TIT-FL-MORA-NULL
            //             TO (SF)-FL-MORA-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().setWtitFlMora(ws.getTitCont().getTitFlMora());
        }
        else {
            // COB_CODE: MOVE TIT-FL-MORA
            //             TO (SF)-FL-MORA(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().setWtitFlMora(ws.getTitCont().getTitFlMora());
        }
        // COB_CODE: IF TIT-ID-RAPP-RETE-NULL = HIGH-VALUES
        //                TO (SF)-ID-RAPP-RETE-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-ID-RAPP-RETE(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitIdRappRete().getTitIdRappReteNullFormatted())) {
            // COB_CODE: MOVE TIT-ID-RAPP-RETE-NULL
            //             TO (SF)-ID-RAPP-RETE-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitIdRappRete().setWtitIdRappReteNull(ws.getTitCont().getTitIdRappRete().getTitIdRappReteNull());
        }
        else {
            // COB_CODE: MOVE TIT-ID-RAPP-RETE
            //             TO (SF)-ID-RAPP-RETE(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitIdRappRete().setWtitIdRappRete(ws.getTitCont().getTitIdRappRete().getTitIdRappRete());
        }
        // COB_CODE: IF TIT-ID-RAPP-ANA-NULL = HIGH-VALUES
        //                TO (SF)-ID-RAPP-ANA-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-ID-RAPP-ANA(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitIdRappAna().getTitIdRappAnaNullFormatted())) {
            // COB_CODE: MOVE TIT-ID-RAPP-ANA-NULL
            //             TO (SF)-ID-RAPP-ANA-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitIdRappAna().setWtitIdRappAnaNull(ws.getTitCont().getTitIdRappAna().getTitIdRappAnaNull());
        }
        else {
            // COB_CODE: MOVE TIT-ID-RAPP-ANA
            //             TO (SF)-ID-RAPP-ANA(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitIdRappAna().setWtitIdRappAna(ws.getTitCont().getTitIdRappAna().getTitIdRappAna());
        }
        // COB_CODE: IF TIT-COD-DVS-NULL = HIGH-VALUES
        //                TO (SF)-COD-DVS-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-COD-DVS(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitCodDvsFormatted())) {
            // COB_CODE: MOVE TIT-COD-DVS-NULL
            //             TO (SF)-COD-DVS-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().setWtitCodDvs(ws.getTitCont().getTitCodDvs());
        }
        else {
            // COB_CODE: MOVE TIT-COD-DVS
            //             TO (SF)-COD-DVS(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().setWtitCodDvs(ws.getTitCont().getTitCodDvs());
        }
        // COB_CODE: IF TIT-DT-EMIS-TIT-NULL = HIGH-VALUES
        //                TO (SF)-DT-EMIS-TIT-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-DT-EMIS-TIT(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitDtEmisTit().getTitDtEmisTitNullFormatted())) {
            // COB_CODE: MOVE TIT-DT-EMIS-TIT-NULL
            //             TO (SF)-DT-EMIS-TIT-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitDtEmisTit().setWtitDtEmisTitNull(ws.getTitCont().getTitDtEmisTit().getTitDtEmisTitNull());
        }
        else {
            // COB_CODE: MOVE TIT-DT-EMIS-TIT
            //             TO (SF)-DT-EMIS-TIT(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitDtEmisTit().setWtitDtEmisTit(ws.getTitCont().getTitDtEmisTit().getTitDtEmisTit());
        }
        // COB_CODE: IF TIT-DT-ESI-TIT-NULL = HIGH-VALUES
        //                TO (SF)-DT-ESI-TIT-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-DT-ESI-TIT(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitDtEsiTit().getTitDtEsiTitNullFormatted())) {
            // COB_CODE: MOVE TIT-DT-ESI-TIT-NULL
            //             TO (SF)-DT-ESI-TIT-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitDtEsiTit().setWtitDtEsiTitNull(ws.getTitCont().getTitDtEsiTit().getTitDtEsiTitNull());
        }
        else {
            // COB_CODE: MOVE TIT-DT-ESI-TIT
            //             TO (SF)-DT-ESI-TIT(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitDtEsiTit().setWtitDtEsiTit(ws.getTitCont().getTitDtEsiTit().getTitDtEsiTit());
        }
        // COB_CODE: IF TIT-TOT-PRE-NET-NULL = HIGH-VALUES
        //                TO (SF)-TOT-PRE-NET-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-PRE-NET(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotPreNet().getTitTotPreNetNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-PRE-NET-NULL
            //             TO (SF)-TOT-PRE-NET-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotPreNet().setWtitTotPreNetNull(ws.getTitCont().getTitTotPreNet().getTitTotPreNetNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-PRE-NET
            //             TO (SF)-TOT-PRE-NET(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotPreNet().setWtitTotPreNet(Trunc.toDecimal(ws.getTitCont().getTitTotPreNet().getTitTotPreNet(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-INTR-FRAZ-NULL = HIGH-VALUES
        //                TO (SF)-TOT-INTR-FRAZ-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-INTR-FRAZ(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotIntrFraz().getTitTotIntrFrazNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-INTR-FRAZ-NULL
            //             TO (SF)-TOT-INTR-FRAZ-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotIntrFraz().setWtitTotIntrFrazNull(ws.getTitCont().getTitTotIntrFraz().getTitTotIntrFrazNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-INTR-FRAZ
            //             TO (SF)-TOT-INTR-FRAZ(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotIntrFraz().setWtitTotIntrFraz(Trunc.toDecimal(ws.getTitCont().getTitTotIntrFraz().getTitTotIntrFraz(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-INTR-MORA-NULL = HIGH-VALUES
        //                TO (SF)-TOT-INTR-MORA-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-INTR-MORA(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotIntrMora().getTitTotIntrMoraNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-INTR-MORA-NULL
            //             TO (SF)-TOT-INTR-MORA-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotIntrMora().setWtitTotIntrMoraNull(ws.getTitCont().getTitTotIntrMora().getTitTotIntrMoraNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-INTR-MORA
            //             TO (SF)-TOT-INTR-MORA(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotIntrMora().setWtitTotIntrMora(Trunc.toDecimal(ws.getTitCont().getTitTotIntrMora().getTitTotIntrMora(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-INTR-PREST-NULL = HIGH-VALUES
        //                TO (SF)-TOT-INTR-PREST-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-INTR-PREST(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotIntrPrest().getTitTotIntrPrestNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-INTR-PREST-NULL
            //             TO (SF)-TOT-INTR-PREST-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotIntrPrest().setWtitTotIntrPrestNull(ws.getTitCont().getTitTotIntrPrest().getTitTotIntrPrestNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-INTR-PREST
            //             TO (SF)-TOT-INTR-PREST(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotIntrPrest().setWtitTotIntrPrest(Trunc.toDecimal(ws.getTitCont().getTitTotIntrPrest().getTitTotIntrPrest(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-INTR-RETDT-NULL = HIGH-VALUES
        //                TO (SF)-TOT-INTR-RETDT-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-INTR-RETDT(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotIntrRetdt().getTitTotIntrRetdtNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-INTR-RETDT-NULL
            //             TO (SF)-TOT-INTR-RETDT-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotIntrRetdt().setWtitTotIntrRetdtNull(ws.getTitCont().getTitTotIntrRetdt().getTitTotIntrRetdtNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-INTR-RETDT
            //             TO (SF)-TOT-INTR-RETDT(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotIntrRetdt().setWtitTotIntrRetdt(Trunc.toDecimal(ws.getTitCont().getTitTotIntrRetdt().getTitTotIntrRetdt(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-INTR-RIAT-NULL = HIGH-VALUES
        //                TO (SF)-TOT-INTR-RIAT-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-INTR-RIAT(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotIntrRiat().getTitTotIntrRiatNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-INTR-RIAT-NULL
            //             TO (SF)-TOT-INTR-RIAT-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotIntrRiat().setWtitTotIntrRiatNull(ws.getTitCont().getTitTotIntrRiat().getTitTotIntrRiatNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-INTR-RIAT
            //             TO (SF)-TOT-INTR-RIAT(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotIntrRiat().setWtitTotIntrRiat(Trunc.toDecimal(ws.getTitCont().getTitTotIntrRiat().getTitTotIntrRiat(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-DIR-NULL = HIGH-VALUES
        //                TO (SF)-TOT-DIR-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-DIR(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotDir().getTitTotDirNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-DIR-NULL
            //             TO (SF)-TOT-DIR-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotDir().setWtitTotDirNull(ws.getTitCont().getTitTotDir().getTitTotDirNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-DIR
            //             TO (SF)-TOT-DIR(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotDir().setWtitTotDir(Trunc.toDecimal(ws.getTitCont().getTitTotDir().getTitTotDir(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-SPE-MED-NULL = HIGH-VALUES
        //                TO (SF)-TOT-SPE-MED-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-SPE-MED(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotSpeMed().getTitTotSpeMedNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-SPE-MED-NULL
            //             TO (SF)-TOT-SPE-MED-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotSpeMed().setWtitTotSpeMedNull(ws.getTitCont().getTitTotSpeMed().getTitTotSpeMedNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-SPE-MED
            //             TO (SF)-TOT-SPE-MED(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotSpeMed().setWtitTotSpeMed(Trunc.toDecimal(ws.getTitCont().getTitTotSpeMed().getTitTotSpeMed(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-TAX-NULL = HIGH-VALUES
        //                TO (SF)-TOT-TAX-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-TAX(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotTax().getTitTotTaxNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-TAX-NULL
            //             TO (SF)-TOT-TAX-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotTax().setWtitTotTaxNull(ws.getTitCont().getTitTotTax().getTitTotTaxNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-TAX
            //             TO (SF)-TOT-TAX(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotTax().setWtitTotTax(Trunc.toDecimal(ws.getTitCont().getTitTotTax().getTitTotTax(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-SOPR-SAN-NULL = HIGH-VALUES
        //                TO (SF)-TOT-SOPR-SAN-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-SOPR-SAN(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotSoprSan().getTitTotSoprSanNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-SOPR-SAN-NULL
            //             TO (SF)-TOT-SOPR-SAN-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotSoprSan().setWtitTotSoprSanNull(ws.getTitCont().getTitTotSoprSan().getTitTotSoprSanNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-SOPR-SAN
            //             TO (SF)-TOT-SOPR-SAN(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotSoprSan().setWtitTotSoprSan(Trunc.toDecimal(ws.getTitCont().getTitTotSoprSan().getTitTotSoprSan(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-SOPR-TEC-NULL = HIGH-VALUES
        //                TO (SF)-TOT-SOPR-TEC-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-SOPR-TEC(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotSoprTec().getTitTotSoprTecNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-SOPR-TEC-NULL
            //             TO (SF)-TOT-SOPR-TEC-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotSoprTec().setWtitTotSoprTecNull(ws.getTitCont().getTitTotSoprTec().getTitTotSoprTecNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-SOPR-TEC
            //             TO (SF)-TOT-SOPR-TEC(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotSoprTec().setWtitTotSoprTec(Trunc.toDecimal(ws.getTitCont().getTitTotSoprTec().getTitTotSoprTec(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-SOPR-SPO-NULL = HIGH-VALUES
        //                TO (SF)-TOT-SOPR-SPO-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-SOPR-SPO(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotSoprSpo().getTitTotSoprSpoNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-SOPR-SPO-NULL
            //             TO (SF)-TOT-SOPR-SPO-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotSoprSpo().setWtitTotSoprSpoNull(ws.getTitCont().getTitTotSoprSpo().getTitTotSoprSpoNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-SOPR-SPO
            //             TO (SF)-TOT-SOPR-SPO(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotSoprSpo().setWtitTotSoprSpo(Trunc.toDecimal(ws.getTitCont().getTitTotSoprSpo().getTitTotSoprSpo(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-SOPR-PROF-NULL = HIGH-VALUES
        //                TO (SF)-TOT-SOPR-PROF-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-SOPR-PROF(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotSoprProf().getTitTotSoprProfNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-SOPR-PROF-NULL
            //             TO (SF)-TOT-SOPR-PROF-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotSoprProf().setWtitTotSoprProfNull(ws.getTitCont().getTitTotSoprProf().getTitTotSoprProfNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-SOPR-PROF
            //             TO (SF)-TOT-SOPR-PROF(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotSoprProf().setWtitTotSoprProf(Trunc.toDecimal(ws.getTitCont().getTitTotSoprProf().getTitTotSoprProf(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-SOPR-ALT-NULL = HIGH-VALUES
        //                TO (SF)-TOT-SOPR-ALT-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-SOPR-ALT(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotSoprAlt().getTitTotSoprAltNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-SOPR-ALT-NULL
            //             TO (SF)-TOT-SOPR-ALT-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotSoprAlt().setWtitTotSoprAltNull(ws.getTitCont().getTitTotSoprAlt().getTitTotSoprAltNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-SOPR-ALT
            //             TO (SF)-TOT-SOPR-ALT(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotSoprAlt().setWtitTotSoprAlt(Trunc.toDecimal(ws.getTitCont().getTitTotSoprAlt().getTitTotSoprAlt(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-PRE-TOT-NULL = HIGH-VALUES
        //                TO (SF)-TOT-PRE-TOT-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-PRE-TOT(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotPreTot().getTitTotPreTotNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-PRE-TOT-NULL
            //             TO (SF)-TOT-PRE-TOT-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotPreTot().setWtitTotPreTotNull(ws.getTitCont().getTitTotPreTot().getTitTotPreTotNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-PRE-TOT
            //             TO (SF)-TOT-PRE-TOT(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotPreTot().setWtitTotPreTot(Trunc.toDecimal(ws.getTitCont().getTitTotPreTot().getTitTotPreTot(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-PRE-PP-IAS-NULL = HIGH-VALUES
        //                TO (SF)-TOT-PRE-PP-IAS-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-PRE-PP-IAS(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotPrePpIas().getTitTotPrePpIasNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-PRE-PP-IAS-NULL
            //             TO (SF)-TOT-PRE-PP-IAS-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotPrePpIas().setWtitTotPrePpIasNull(ws.getTitCont().getTitTotPrePpIas().getTitTotPrePpIasNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-PRE-PP-IAS
            //             TO (SF)-TOT-PRE-PP-IAS(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotPrePpIas().setWtitTotPrePpIas(Trunc.toDecimal(ws.getTitCont().getTitTotPrePpIas().getTitTotPrePpIas(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-CAR-ACQ-NULL = HIGH-VALUES
        //                TO (SF)-TOT-CAR-ACQ-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-CAR-ACQ(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotCarAcq().getTitTotCarAcqNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-CAR-ACQ-NULL
            //             TO (SF)-TOT-CAR-ACQ-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotCarAcq().setWtitTotCarAcqNull(ws.getTitCont().getTitTotCarAcq().getTitTotCarAcqNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-CAR-ACQ
            //             TO (SF)-TOT-CAR-ACQ(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotCarAcq().setWtitTotCarAcq(Trunc.toDecimal(ws.getTitCont().getTitTotCarAcq().getTitTotCarAcq(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-CAR-GEST-NULL = HIGH-VALUES
        //                TO (SF)-TOT-CAR-GEST-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-CAR-GEST(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotCarGest().getTitTotCarGestNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-CAR-GEST-NULL
            //             TO (SF)-TOT-CAR-GEST-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotCarGest().setWtitTotCarGestNull(ws.getTitCont().getTitTotCarGest().getTitTotCarGestNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-CAR-GEST
            //             TO (SF)-TOT-CAR-GEST(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotCarGest().setWtitTotCarGest(Trunc.toDecimal(ws.getTitCont().getTitTotCarGest().getTitTotCarGest(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-CAR-INC-NULL = HIGH-VALUES
        //                TO (SF)-TOT-CAR-INC-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-CAR-INC(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotCarInc().getTitTotCarIncNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-CAR-INC-NULL
            //             TO (SF)-TOT-CAR-INC-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotCarInc().setWtitTotCarIncNull(ws.getTitCont().getTitTotCarInc().getTitTotCarIncNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-CAR-INC
            //             TO (SF)-TOT-CAR-INC(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotCarInc().setWtitTotCarInc(Trunc.toDecimal(ws.getTitCont().getTitTotCarInc().getTitTotCarInc(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-PRE-SOLO-RSH-NULL = HIGH-VALUES
        //                TO (SF)-TOT-PRE-SOLO-RSH-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-PRE-SOLO-RSH(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotPreSoloRsh().getTitTotPreSoloRshNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-PRE-SOLO-RSH-NULL
            //             TO (SF)-TOT-PRE-SOLO-RSH-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotPreSoloRsh().setWtitTotPreSoloRshNull(ws.getTitCont().getTitTotPreSoloRsh().getTitTotPreSoloRshNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-PRE-SOLO-RSH
            //             TO (SF)-TOT-PRE-SOLO-RSH(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotPreSoloRsh().setWtitTotPreSoloRsh(Trunc.toDecimal(ws.getTitCont().getTitTotPreSoloRsh().getTitTotPreSoloRsh(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-PROV-ACQ-1AA-NULL = HIGH-VALUES
        //                TO (SF)-TOT-PROV-ACQ-1AA-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-PROV-ACQ-1AA(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotProvAcq1aa().getTitTotProvAcq1aaNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-PROV-ACQ-1AA-NULL
            //             TO (SF)-TOT-PROV-ACQ-1AA-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotProvAcq1aa().setWtitTotProvAcq1aaNull(ws.getTitCont().getTitTotProvAcq1aa().getTitTotProvAcq1aaNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-PROV-ACQ-1AA
            //             TO (SF)-TOT-PROV-ACQ-1AA(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotProvAcq1aa().setWtitTotProvAcq1aa(Trunc.toDecimal(ws.getTitCont().getTitTotProvAcq1aa().getTitTotProvAcq1aa(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-PROV-ACQ-2AA-NULL = HIGH-VALUES
        //                TO (SF)-TOT-PROV-ACQ-2AA-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-PROV-ACQ-2AA(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotProvAcq2aa().getTitTotProvAcq2aaNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-PROV-ACQ-2AA-NULL
            //             TO (SF)-TOT-PROV-ACQ-2AA-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotProvAcq2aa().setWtitTotProvAcq2aaNull(ws.getTitCont().getTitTotProvAcq2aa().getTitTotProvAcq2aaNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-PROV-ACQ-2AA
            //             TO (SF)-TOT-PROV-ACQ-2AA(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotProvAcq2aa().setWtitTotProvAcq2aa(Trunc.toDecimal(ws.getTitCont().getTitTotProvAcq2aa().getTitTotProvAcq2aa(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-PROV-RICOR-NULL = HIGH-VALUES
        //                TO (SF)-TOT-PROV-RICOR-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-PROV-RICOR(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotProvRicor().getTitTotProvRicorNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-PROV-RICOR-NULL
            //             TO (SF)-TOT-PROV-RICOR-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotProvRicor().setWtitTotProvRicorNull(ws.getTitCont().getTitTotProvRicor().getTitTotProvRicorNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-PROV-RICOR
            //             TO (SF)-TOT-PROV-RICOR(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotProvRicor().setWtitTotProvRicor(Trunc.toDecimal(ws.getTitCont().getTitTotProvRicor().getTitTotProvRicor(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-PROV-INC-NULL = HIGH-VALUES
        //                TO (SF)-TOT-PROV-INC-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-PROV-INC(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotProvInc().getTitTotProvIncNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-PROV-INC-NULL
            //             TO (SF)-TOT-PROV-INC-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotProvInc().setWtitTotProvIncNull(ws.getTitCont().getTitTotProvInc().getTitTotProvIncNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-PROV-INC
            //             TO (SF)-TOT-PROV-INC(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotProvInc().setWtitTotProvInc(Trunc.toDecimal(ws.getTitCont().getTitTotProvInc().getTitTotProvInc(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-PROV-DA-REC-NULL = HIGH-VALUES
        //                TO (SF)-TOT-PROV-DA-REC-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-PROV-DA-REC(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotProvDaRec().getTitTotProvDaRecNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-PROV-DA-REC-NULL
            //             TO (SF)-TOT-PROV-DA-REC-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotProvDaRec().setWtitTotProvDaRecNull(ws.getTitCont().getTitTotProvDaRec().getTitTotProvDaRecNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-PROV-DA-REC
            //             TO (SF)-TOT-PROV-DA-REC(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotProvDaRec().setWtitTotProvDaRec(Trunc.toDecimal(ws.getTitCont().getTitTotProvDaRec().getTitTotProvDaRec(), 15, 3));
        }
        // COB_CODE: IF TIT-IMP-AZ-NULL = HIGH-VALUES
        //                TO (SF)-IMP-AZ-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-IMP-AZ(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitImpAz().getTitImpAzNullFormatted())) {
            // COB_CODE: MOVE TIT-IMP-AZ-NULL
            //             TO (SF)-IMP-AZ-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitImpAz().setWtitImpAzNull(ws.getTitCont().getTitImpAz().getTitImpAzNull());
        }
        else {
            // COB_CODE: MOVE TIT-IMP-AZ
            //             TO (SF)-IMP-AZ(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitImpAz().setWtitImpAz(Trunc.toDecimal(ws.getTitCont().getTitImpAz().getTitImpAz(), 15, 3));
        }
        // COB_CODE: IF TIT-IMP-ADER-NULL = HIGH-VALUES
        //                TO (SF)-IMP-ADER-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-IMP-ADER(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitImpAder().getTitImpAderNullFormatted())) {
            // COB_CODE: MOVE TIT-IMP-ADER-NULL
            //             TO (SF)-IMP-ADER-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitImpAder().setWtitImpAderNull(ws.getTitCont().getTitImpAder().getTitImpAderNull());
        }
        else {
            // COB_CODE: MOVE TIT-IMP-ADER
            //             TO (SF)-IMP-ADER(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitImpAder().setWtitImpAder(Trunc.toDecimal(ws.getTitCont().getTitImpAder().getTitImpAder(), 15, 3));
        }
        // COB_CODE: IF TIT-IMP-TFR-NULL = HIGH-VALUES
        //                TO (SF)-IMP-TFR-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-IMP-TFR(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitImpTfr().getTitImpTfrNullFormatted())) {
            // COB_CODE: MOVE TIT-IMP-TFR-NULL
            //             TO (SF)-IMP-TFR-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitImpTfr().setWtitImpTfrNull(ws.getTitCont().getTitImpTfr().getTitImpTfrNull());
        }
        else {
            // COB_CODE: MOVE TIT-IMP-TFR
            //             TO (SF)-IMP-TFR(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitImpTfr().setWtitImpTfr(Trunc.toDecimal(ws.getTitCont().getTitImpTfr().getTitImpTfr(), 15, 3));
        }
        // COB_CODE: IF TIT-IMP-VOLO-NULL = HIGH-VALUES
        //                TO (SF)-IMP-VOLO-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-IMP-VOLO(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitImpVolo().getTitImpVoloNullFormatted())) {
            // COB_CODE: MOVE TIT-IMP-VOLO-NULL
            //             TO (SF)-IMP-VOLO-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitImpVolo().setWtitImpVoloNull(ws.getTitCont().getTitImpVolo().getTitImpVoloNull());
        }
        else {
            // COB_CODE: MOVE TIT-IMP-VOLO
            //             TO (SF)-IMP-VOLO(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitImpVolo().setWtitImpVolo(Trunc.toDecimal(ws.getTitCont().getTitImpVolo().getTitImpVolo(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-MANFEE-ANTIC-NULL = HIGH-VALUES
        //                TO (SF)-TOT-MANFEE-ANTIC-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-MANFEE-ANTIC(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotManfeeAntic().getTitTotManfeeAnticNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-MANFEE-ANTIC-NULL
            //             TO (SF)-TOT-MANFEE-ANTIC-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotManfeeAntic().setWtitTotManfeeAnticNull(ws.getTitCont().getTitTotManfeeAntic().getTitTotManfeeAnticNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-MANFEE-ANTIC
            //             TO (SF)-TOT-MANFEE-ANTIC(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotManfeeAntic().setWtitTotManfeeAntic(Trunc.toDecimal(ws.getTitCont().getTitTotManfeeAntic().getTitTotManfeeAntic(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-MANFEE-RICOR-NULL = HIGH-VALUES
        //                TO (SF)-TOT-MANFEE-RICOR-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-MANFEE-RICOR(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotManfeeRicor().getTitTotManfeeRicorNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-MANFEE-RICOR-NULL
            //             TO (SF)-TOT-MANFEE-RICOR-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotManfeeRicor().setWtitTotManfeeRicorNull(ws.getTitCont().getTitTotManfeeRicor().getTitTotManfeeRicorNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-MANFEE-RICOR
            //             TO (SF)-TOT-MANFEE-RICOR(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotManfeeRicor().setWtitTotManfeeRicor(Trunc.toDecimal(ws.getTitCont().getTitTotManfeeRicor().getTitTotManfeeRicor(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-MANFEE-REC-NULL = HIGH-VALUES
        //                TO (SF)-TOT-MANFEE-REC-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-MANFEE-REC(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotManfeeRec().getTitTotManfeeRecNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-MANFEE-REC-NULL
            //             TO (SF)-TOT-MANFEE-REC-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotManfeeRec().setWtitTotManfeeRecNull(ws.getTitCont().getTitTotManfeeRec().getTitTotManfeeRecNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-MANFEE-REC
            //             TO (SF)-TOT-MANFEE-REC(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotManfeeRec().setWtitTotManfeeRec(Trunc.toDecimal(ws.getTitCont().getTitTotManfeeRec().getTitTotManfeeRec(), 15, 3));
        }
        // COB_CODE: IF TIT-TP-MEZ-PAG-ADD-NULL = HIGH-VALUES
        //                TO (SF)-TP-MEZ-PAG-ADD-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TP-MEZ-PAG-ADD(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTpMezPagAddFormatted())) {
            // COB_CODE: MOVE TIT-TP-MEZ-PAG-ADD-NULL
            //             TO (SF)-TP-MEZ-PAG-ADD-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().setWtitTpMezPagAdd(ws.getTitCont().getTitTpMezPagAdd());
        }
        else {
            // COB_CODE: MOVE TIT-TP-MEZ-PAG-ADD
            //             TO (SF)-TP-MEZ-PAG-ADD(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().setWtitTpMezPagAdd(ws.getTitCont().getTitTpMezPagAdd());
        }
        // COB_CODE: IF TIT-ESTR-CNT-CORR-ADD-NULL = HIGH-VALUES
        //                TO (SF)-ESTR-CNT-CORR-ADD-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-ESTR-CNT-CORR-ADD(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitEstrCntCorrAddFormatted())) {
            // COB_CODE: MOVE TIT-ESTR-CNT-CORR-ADD-NULL
            //             TO (SF)-ESTR-CNT-CORR-ADD-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().setWtitEstrCntCorrAdd(ws.getTitCont().getTitEstrCntCorrAdd());
        }
        else {
            // COB_CODE: MOVE TIT-ESTR-CNT-CORR-ADD
            //             TO (SF)-ESTR-CNT-CORR-ADD(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().setWtitEstrCntCorrAdd(ws.getTitCont().getTitEstrCntCorrAdd());
        }
        // COB_CODE: IF TIT-DT-VLT-NULL = HIGH-VALUES
        //                TO (SF)-DT-VLT-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-DT-VLT(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitDtVlt().getTitDtVltNullFormatted())) {
            // COB_CODE: MOVE TIT-DT-VLT-NULL
            //             TO (SF)-DT-VLT-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitDtVlt().setWtitDtVltNull(ws.getTitCont().getTitDtVlt().getTitDtVltNull());
        }
        else {
            // COB_CODE: MOVE TIT-DT-VLT
            //             TO (SF)-DT-VLT(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitDtVlt().setWtitDtVlt(ws.getTitCont().getTitDtVlt().getTitDtVlt());
        }
        // COB_CODE: IF TIT-FL-FORZ-DT-VLT-NULL = HIGH-VALUES
        //                TO (SF)-FL-FORZ-DT-VLT-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-FL-FORZ-DT-VLT(IX-TAB-TIT)
        //           END-IF
        if (Conditions.eq(ws.getTitCont().getTitFlForzDtVlt(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE TIT-FL-FORZ-DT-VLT-NULL
            //             TO (SF)-FL-FORZ-DT-VLT-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().setWtitFlForzDtVlt(ws.getTitCont().getTitFlForzDtVlt());
        }
        else {
            // COB_CODE: MOVE TIT-FL-FORZ-DT-VLT
            //             TO (SF)-FL-FORZ-DT-VLT(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().setWtitFlForzDtVlt(ws.getTitCont().getTitFlForzDtVlt());
        }
        // COB_CODE: IF TIT-DT-CAMBIO-VLT-NULL = HIGH-VALUES
        //                TO (SF)-DT-CAMBIO-VLT-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-DT-CAMBIO-VLT(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitDtCambioVlt().getTitDtCambioVltNullFormatted())) {
            // COB_CODE: MOVE TIT-DT-CAMBIO-VLT-NULL
            //             TO (SF)-DT-CAMBIO-VLT-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitDtCambioVlt().setWtitDtCambioVltNull(ws.getTitCont().getTitDtCambioVlt().getTitDtCambioVltNull());
        }
        else {
            // COB_CODE: MOVE TIT-DT-CAMBIO-VLT
            //             TO (SF)-DT-CAMBIO-VLT(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitDtCambioVlt().setWtitDtCambioVlt(ws.getTitCont().getTitDtCambioVlt().getTitDtCambioVlt());
        }
        // COB_CODE: IF TIT-TOT-SPE-AGE-NULL = HIGH-VALUES
        //                TO (SF)-TOT-SPE-AGE-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-SPE-AGE(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotSpeAge().getTitTotSpeAgeNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-SPE-AGE-NULL
            //             TO (SF)-TOT-SPE-AGE-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotSpeAge().setWtitTotSpeAgeNull(ws.getTitCont().getTitTotSpeAge().getTitTotSpeAgeNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-SPE-AGE
            //             TO (SF)-TOT-SPE-AGE(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotSpeAge().setWtitTotSpeAge(Trunc.toDecimal(ws.getTitCont().getTitTotSpeAge().getTitTotSpeAge(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-CAR-IAS-NULL = HIGH-VALUES
        //                TO (SF)-TOT-CAR-IAS-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-CAR-IAS(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotCarIas().getTitTotCarIasNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-CAR-IAS-NULL
            //             TO (SF)-TOT-CAR-IAS-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotCarIas().setWtitTotCarIasNull(ws.getTitCont().getTitTotCarIas().getTitTotCarIasNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-CAR-IAS
            //             TO (SF)-TOT-CAR-IAS(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotCarIas().setWtitTotCarIas(Trunc.toDecimal(ws.getTitCont().getTitTotCarIas().getTitTotCarIas(), 15, 3));
        }
        // COB_CODE: IF TIT-NUM-RAT-ACCORPATE-NULL = HIGH-VALUES
        //                TO (SF)-NUM-RAT-ACCORPATE-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-NUM-RAT-ACCORPATE(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitNumRatAccorpate().getTitNumRatAccorpateNullFormatted())) {
            // COB_CODE: MOVE TIT-NUM-RAT-ACCORPATE-NULL
            //             TO (SF)-NUM-RAT-ACCORPATE-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitNumRatAccorpate().setWtitNumRatAccorpateNull(ws.getTitCont().getTitNumRatAccorpate().getTitNumRatAccorpateNull());
        }
        else {
            // COB_CODE: MOVE TIT-NUM-RAT-ACCORPATE
            //             TO (SF)-NUM-RAT-ACCORPATE(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitNumRatAccorpate().setWtitNumRatAccorpate(ws.getTitCont().getTitNumRatAccorpate().getTitNumRatAccorpate());
        }
        // COB_CODE: MOVE TIT-DS-RIGA
        //             TO (SF)-DS-RIGA(IX-TAB-TIT)
        ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().setWtitDsRiga(ws.getTitCont().getTitDsRiga());
        // COB_CODE: MOVE TIT-DS-OPER-SQL
        //             TO (SF)-DS-OPER-SQL(IX-TAB-TIT)
        ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().setWtitDsOperSql(ws.getTitCont().getTitDsOperSql());
        // COB_CODE: MOVE TIT-DS-VER
        //             TO (SF)-DS-VER(IX-TAB-TIT)
        ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().setWtitDsVer(ws.getTitCont().getTitDsVer());
        // COB_CODE: MOVE TIT-DS-TS-INI-CPTZ
        //             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-TIT)
        ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().setWtitDsTsIniCptz(ws.getTitCont().getTitDsTsIniCptz());
        // COB_CODE: MOVE TIT-DS-TS-END-CPTZ
        //             TO (SF)-DS-TS-END-CPTZ(IX-TAB-TIT)
        ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().setWtitDsTsEndCptz(ws.getTitCont().getTitDsTsEndCptz());
        // COB_CODE: MOVE TIT-DS-UTENTE
        //             TO (SF)-DS-UTENTE(IX-TAB-TIT)
        ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().setWtitDsUtente(ws.getTitCont().getTitDsUtente());
        // COB_CODE: MOVE TIT-DS-STATO-ELAB
        //             TO (SF)-DS-STATO-ELAB(IX-TAB-TIT)
        ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().setWtitDsStatoElab(ws.getTitCont().getTitDsStatoElab());
        // COB_CODE: IF TIT-FL-TIT-DA-REINVST-NULL = HIGH-VALUES
        //                TO (SF)-FL-TIT-DA-REINVST-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-FL-TIT-DA-REINVST(IX-TAB-TIT)
        //           END-IF
        if (Conditions.eq(ws.getTitCont().getTitFlTitDaReinvst(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE TIT-FL-TIT-DA-REINVST-NULL
            //             TO (SF)-FL-TIT-DA-REINVST-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().setWtitFlTitDaReinvst(ws.getTitCont().getTitFlTitDaReinvst());
        }
        else {
            // COB_CODE: MOVE TIT-FL-TIT-DA-REINVST
            //             TO (SF)-FL-TIT-DA-REINVST(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().setWtitFlTitDaReinvst(ws.getTitCont().getTitFlTitDaReinvst());
        }
        // COB_CODE: IF TIT-DT-RICH-ADD-RID-NULL = HIGH-VALUES
        //                TO (SF)-DT-RICH-ADD-RID-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-DT-RICH-ADD-RID(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitDtRichAddRid().getTitDtRichAddRidNullFormatted())) {
            // COB_CODE: MOVE TIT-DT-RICH-ADD-RID-NULL
            //             TO (SF)-DT-RICH-ADD-RID-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitDtRichAddRid().setWtitDtRichAddRidNull(ws.getTitCont().getTitDtRichAddRid().getTitDtRichAddRidNull());
        }
        else {
            // COB_CODE: MOVE TIT-DT-RICH-ADD-RID
            //             TO (SF)-DT-RICH-ADD-RID(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitDtRichAddRid().setWtitDtRichAddRid(ws.getTitCont().getTitDtRichAddRid().getTitDtRichAddRid());
        }
        // COB_CODE: IF TIT-TP-ESI-RID-NULL = HIGH-VALUES
        //                TO (SF)-TP-ESI-RID-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TP-ESI-RID(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTpEsiRidFormatted())) {
            // COB_CODE: MOVE TIT-TP-ESI-RID-NULL
            //             TO (SF)-TP-ESI-RID-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().setWtitTpEsiRid(ws.getTitCont().getTitTpEsiRid());
        }
        else {
            // COB_CODE: MOVE TIT-TP-ESI-RID
            //             TO (SF)-TP-ESI-RID(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().setWtitTpEsiRid(ws.getTitCont().getTitTpEsiRid());
        }
        // COB_CODE: IF TIT-COD-IBAN-NULL = HIGH-VALUES
        //                TO (SF)-COD-IBAN-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-COD-IBAN(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitCodIban(), TitCont.Len.TIT_COD_IBAN)) {
            // COB_CODE: MOVE TIT-COD-IBAN-NULL
            //             TO (SF)-COD-IBAN-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().setWtitCodIban(ws.getTitCont().getTitCodIban());
        }
        else {
            // COB_CODE: MOVE TIT-COD-IBAN
            //             TO (SF)-COD-IBAN(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().setWtitCodIban(ws.getTitCont().getTitCodIban());
        }
        // COB_CODE: IF TIT-IMP-TRASFE-NULL = HIGH-VALUES
        //                TO (SF)-IMP-TRASFE-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-IMP-TRASFE(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitImpTrasfe().getTitImpTrasfeNullFormatted())) {
            // COB_CODE: MOVE TIT-IMP-TRASFE-NULL
            //             TO (SF)-IMP-TRASFE-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitImpTrasfe().setWtitImpTrasfeNull(ws.getTitCont().getTitImpTrasfe().getTitImpTrasfeNull());
        }
        else {
            // COB_CODE: MOVE TIT-IMP-TRASFE
            //             TO (SF)-IMP-TRASFE(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitImpTrasfe().setWtitImpTrasfe(Trunc.toDecimal(ws.getTitCont().getTitImpTrasfe().getTitImpTrasfe(), 15, 3));
        }
        // COB_CODE: IF TIT-IMP-TFR-STRC-NULL = HIGH-VALUES
        //                TO (SF)-IMP-TFR-STRC-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-IMP-TFR-STRC(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitImpTfrStrc().getTitImpTfrStrcNullFormatted())) {
            // COB_CODE: MOVE TIT-IMP-TFR-STRC-NULL
            //             TO (SF)-IMP-TFR-STRC-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitImpTfrStrc().setWtitImpTfrStrcNull(ws.getTitCont().getTitImpTfrStrc().getTitImpTfrStrcNull());
        }
        else {
            // COB_CODE: MOVE TIT-IMP-TFR-STRC
            //             TO (SF)-IMP-TFR-STRC(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitImpTfrStrc().setWtitImpTfrStrc(Trunc.toDecimal(ws.getTitCont().getTitImpTfrStrc().getTitImpTfrStrc(), 15, 3));
        }
        // COB_CODE: IF TIT-DT-CERT-FISC-NULL = HIGH-VALUES
        //                TO (SF)-DT-CERT-FISC-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-DT-CERT-FISC(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitDtCertFisc().getTitDtCertFiscNullFormatted())) {
            // COB_CODE: MOVE TIT-DT-CERT-FISC-NULL
            //             TO (SF)-DT-CERT-FISC-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitDtCertFisc().setWtitDtCertFiscNull(ws.getTitCont().getTitDtCertFisc().getTitDtCertFiscNull());
        }
        else {
            // COB_CODE: MOVE TIT-DT-CERT-FISC
            //             TO (SF)-DT-CERT-FISC(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitDtCertFisc().setWtitDtCertFisc(ws.getTitCont().getTitDtCertFisc().getTitDtCertFisc());
        }
        // COB_CODE: IF TIT-TP-CAUS-STOR-NULL = HIGH-VALUES
        //                TO (SF)-TP-CAUS-STOR-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TP-CAUS-STOR(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTpCausStor().getTitTpCausStorNullFormatted())) {
            // COB_CODE: MOVE TIT-TP-CAUS-STOR-NULL
            //             TO (SF)-TP-CAUS-STOR-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTpCausStor().setWtitTpCausStorNull(ws.getTitCont().getTitTpCausStor().getTitTpCausStorNull());
        }
        else {
            // COB_CODE: MOVE TIT-TP-CAUS-STOR
            //             TO (SF)-TP-CAUS-STOR(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTpCausStor().setWtitTpCausStor(ws.getTitCont().getTitTpCausStor().getTitTpCausStor());
        }
        // COB_CODE: IF TIT-TP-CAUS-DISP-STOR-NULL = HIGH-VALUES
        //                TO (SF)-TP-CAUS-DISP-STOR-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TP-CAUS-DISP-STOR(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTpCausDispStorFormatted())) {
            // COB_CODE: MOVE TIT-TP-CAUS-DISP-STOR-NULL
            //             TO (SF)-TP-CAUS-DISP-STOR-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().setWtitTpCausDispStor(ws.getTitCont().getTitTpCausDispStor());
        }
        else {
            // COB_CODE: MOVE TIT-TP-CAUS-DISP-STOR
            //             TO (SF)-TP-CAUS-DISP-STOR(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().setWtitTpCausDispStor(ws.getTitCont().getTitTpCausDispStor());
        }
        // COB_CODE: IF TIT-TP-TIT-MIGRAZ-NULL = HIGH-VALUES
        //                TO (SF)-TP-TIT-MIGRAZ-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TP-TIT-MIGRAZ(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTpTitMigrazFormatted())) {
            // COB_CODE: MOVE TIT-TP-TIT-MIGRAZ-NULL
            //             TO (SF)-TP-TIT-MIGRAZ-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().setWtitTpTitMigraz(ws.getTitCont().getTitTpTitMigraz());
        }
        else {
            // COB_CODE: MOVE TIT-TP-TIT-MIGRAZ
            //             TO (SF)-TP-TIT-MIGRAZ(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().setWtitTpTitMigraz(ws.getTitCont().getTitTpTitMigraz());
        }
        // COB_CODE: IF TIT-TOT-ACQ-EXP-NULL = HIGH-VALUES
        //                TO (SF)-TOT-ACQ-EXP-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-ACQ-EXP(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotAcqExp().getTitTotAcqExpNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-ACQ-EXP-NULL
            //             TO (SF)-TOT-ACQ-EXP-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotAcqExp().setWtitTotAcqExpNull(ws.getTitCont().getTitTotAcqExp().getTitTotAcqExpNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-ACQ-EXP
            //             TO (SF)-TOT-ACQ-EXP(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotAcqExp().setWtitTotAcqExp(Trunc.toDecimal(ws.getTitCont().getTitTotAcqExp().getTitTotAcqExp(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-REMUN-ASS-NULL = HIGH-VALUES
        //                TO (SF)-TOT-REMUN-ASS-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-REMUN-ASS(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotRemunAss().getTitTotRemunAssNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-REMUN-ASS-NULL
            //             TO (SF)-TOT-REMUN-ASS-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotRemunAss().setWtitTotRemunAssNull(ws.getTitCont().getTitTotRemunAss().getTitTotRemunAssNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-REMUN-ASS
            //             TO (SF)-TOT-REMUN-ASS(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotRemunAss().setWtitTotRemunAss(Trunc.toDecimal(ws.getTitCont().getTitTotRemunAss().getTitTotRemunAss(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-COMMIS-INTER-NULL = HIGH-VALUES
        //                TO (SF)-TOT-COMMIS-INTER-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-COMMIS-INTER(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotCommisInter().getTitTotCommisInterNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-COMMIS-INTER-NULL
            //             TO (SF)-TOT-COMMIS-INTER-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotCommisInter().setWtitTotCommisInterNull(ws.getTitCont().getTitTotCommisInter().getTitTotCommisInterNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-COMMIS-INTER
            //             TO (SF)-TOT-COMMIS-INTER(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotCommisInter().setWtitTotCommisInter(Trunc.toDecimal(ws.getTitCont().getTitTotCommisInter().getTitTotCommisInter(), 15, 3));
        }
        // COB_CODE: IF TIT-TP-CAUS-RIMB-NULL = HIGH-VALUES
        //                TO (SF)-TP-CAUS-RIMB-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TP-CAUS-RIMB(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTpCausRimbFormatted())) {
            // COB_CODE: MOVE TIT-TP-CAUS-RIMB-NULL
            //             TO (SF)-TP-CAUS-RIMB-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().setWtitTpCausRimb(ws.getTitCont().getTitTpCausRimb());
        }
        else {
            // COB_CODE: MOVE TIT-TP-CAUS-RIMB
            //             TO (SF)-TP-CAUS-RIMB(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().setWtitTpCausRimb(ws.getTitCont().getTitTpCausRimb());
        }
        // COB_CODE: IF TIT-TOT-CNBT-ANTIRAC-NULL = HIGH-VALUES
        //                TO (SF)-TOT-CNBT-ANTIRAC-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-CNBT-ANTIRAC(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotCnbtAntirac().getTitTotCnbtAntiracNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-CNBT-ANTIRAC-NULL
            //             TO (SF)-TOT-CNBT-ANTIRAC-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotCnbtAntirac().setWtitTotCnbtAntiracNull(ws.getTitCont().getTitTotCnbtAntirac().getTitTotCnbtAntiracNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-CNBT-ANTIRAC
            //             TO (SF)-TOT-CNBT-ANTIRAC(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().getWtitTotCnbtAntirac().setWtitTotCnbtAntirac(Trunc.toDecimal(ws.getTitCont().getTitTotCnbtAntirac().getTitTotCnbtAntirac(), 15, 3));
        }
        // COB_CODE: IF TIT-FL-INC-AUTOGEN-NULL = HIGH-VALUES
        //                TO (SF)-FL-INC-AUTOGEN-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-FL-INC-AUTOGEN(IX-TAB-TIT)
        //           END-IF.
        if (Conditions.eq(ws.getTitCont().getTitFlIncAutogen(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE TIT-FL-INC-AUTOGEN-NULL
            //             TO (SF)-FL-INC-AUTOGEN-NULL(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().setWtitFlIncAutogen(ws.getTitCont().getTitFlIncAutogen());
        }
        else {
            // COB_CODE: MOVE TIT-FL-INC-AUTOGEN
            //             TO (SF)-FL-INC-AUTOGEN(IX-TAB-TIT)
            ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().setWtitFlIncAutogen(ws.getTitCont().getTitFlIncAutogen());
        }
    }

    /**Original name: INIZIA-TOT-TIT<br>
	 * <pre>------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    INIZIALIZZAZIONE CAMPI NULL COPY LCCVTIT4
	 *    ULTIMO AGG. 05 DIC 2014
	 * ------------------------------------------------------------</pre>*/
    private void iniziaTotTit() {
        // COB_CODE: PERFORM INIZIA-ZEROES-TIT THRU INIZIA-ZEROES-TIT-EX
        iniziaZeroesTit();
        // COB_CODE: PERFORM INIZIA-SPACES-TIT THRU INIZIA-SPACES-TIT-EX
        //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LCCVTIT4:line=12, because the code is unreachable.
        // COB_CODE: PERFORM INIZIA-NULL-TIT THRU INIZIA-NULL-TIT-EX.
        //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LCCVTIT4:line=14, because the code is unreachable.
    }

    /**Original name: INIZIA-ZEROES-TIT<br>*/
    private void iniziaZeroesTit() {
        // COB_CODE: MOVE 0 TO (SF)-ID-TIT-CONT(IX-TAB-TIT)
        ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().setWtitIdTitCont(0);
        // COB_CODE: MOVE 0 TO (SF)-ID-OGG(IX-TAB-TIT)
        ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().setWtitIdOgg(0);
        // COB_CODE: MOVE 0 TO (SF)-ID-MOVI-CRZ(IX-TAB-TIT)
        ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().setWtitIdMoviCrz(0);
        // COB_CODE: MOVE 0 TO (SF)-DT-INI-EFF(IX-TAB-TIT)
        ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().setWtitDtIniEff(0);
        // COB_CODE: MOVE 0 TO (SF)-DT-END-EFF(IX-TAB-TIT)
        ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().setWtitDtEndEff(0);
        // COB_CODE: MOVE 0 TO (SF)-COD-COMP-ANIA(IX-TAB-TIT)
        ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().setWtitCodCompAnia(0);
        // COB_CODE: MOVE 0 TO (SF)-DS-RIGA(IX-TAB-TIT)
        ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().setWtitDsRiga(0);
        // COB_CODE: MOVE 0 TO (SF)-DS-VER(IX-TAB-TIT)
        ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().setWtitDsVer(0);
        // COB_CODE: MOVE 0 TO (SF)-DS-TS-INI-CPTZ(IX-TAB-TIT)
        ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().setWtitDsTsIniCptz(0);
        // COB_CODE: MOVE 0 TO (SF)-DS-TS-END-CPTZ(IX-TAB-TIT).
        ws.getWtitTabTitCont(ws.getIxIndici().getTit()).getLccvtit1().getDati().setWtitDsTsEndCptz(0);
    }

    public void initAreaOutput() {
        lccc0490.setRateoFormatted("000000000");
        lccc0490.setDtFineCopFormatted("00000000");
        lccc0490.getPresenzaRateo().setPresenzaRateo(Types.SPACE_CHAR);
        lccc0490.getTitoliEmessi().setTitoliEmessi(Types.SPACE_CHAR);
    }

    public void initIxIndici() {
        ws.getIxIndici().setGrz(((short)0));
        ws.getIxIndici().setRate(((short)0));
        ws.getIxIndici().setTit(((short)0));
        ws.getIxIndici().setDtc(((short)0));
    }

    public void initTitCont() {
        ws.getTitCont().setTitIdTitCont(0);
        ws.getTitCont().setTitIdOgg(0);
        ws.getTitCont().setTitTpOgg("");
        ws.getTitCont().setTitIbRich("");
        ws.getTitCont().setTitIdMoviCrz(0);
        ws.getTitCont().getTitIdMoviChiu().setTitIdMoviChiu(0);
        ws.getTitCont().setTitDtIniEff(0);
        ws.getTitCont().setTitDtEndEff(0);
        ws.getTitCont().setTitCodCompAnia(0);
        ws.getTitCont().setTitTpTit("");
        ws.getTitCont().getTitProgTit().setTitProgTit(0);
        ws.getTitCont().setTitTpPreTit("");
        ws.getTitCont().setTitTpStatTit("");
        ws.getTitCont().getTitDtIniCop().setTitDtIniCop(0);
        ws.getTitCont().getTitDtEndCop().setTitDtEndCop(0);
        ws.getTitCont().getTitImpPag().setTitImpPag(new AfDecimal(0, 15, 3));
        ws.getTitCont().setTitFlSoll(Types.SPACE_CHAR);
        ws.getTitCont().getTitFraz().setTitFraz(0);
        ws.getTitCont().getTitDtApplzMora().setTitDtApplzMora(0);
        ws.getTitCont().setTitFlMora(Types.SPACE_CHAR);
        ws.getTitCont().getTitIdRappRete().setTitIdRappRete(0);
        ws.getTitCont().getTitIdRappAna().setTitIdRappAna(0);
        ws.getTitCont().setTitCodDvs("");
        ws.getTitCont().getTitDtEmisTit().setTitDtEmisTit(0);
        ws.getTitCont().getTitDtEsiTit().setTitDtEsiTit(0);
        ws.getTitCont().getTitTotPreNet().setTitTotPreNet(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotIntrFraz().setTitTotIntrFraz(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotIntrMora().setTitTotIntrMora(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotIntrPrest().setTitTotIntrPrest(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotIntrRetdt().setTitTotIntrRetdt(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotIntrRiat().setTitTotIntrRiat(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotDir().setTitTotDir(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotSpeMed().setTitTotSpeMed(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotTax().setTitTotTax(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotSoprSan().setTitTotSoprSan(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotSoprTec().setTitTotSoprTec(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotSoprSpo().setTitTotSoprSpo(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotSoprProf().setTitTotSoprProf(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotSoprAlt().setTitTotSoprAlt(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotPreTot().setTitTotPreTot(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotPrePpIas().setTitTotPrePpIas(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotCarAcq().setTitTotCarAcq(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotCarGest().setTitTotCarGest(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotCarInc().setTitTotCarInc(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotPreSoloRsh().setTitTotPreSoloRsh(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotProvAcq1aa().setTitTotProvAcq1aa(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotProvAcq2aa().setTitTotProvAcq2aa(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotProvRicor().setTitTotProvRicor(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotProvInc().setTitTotProvInc(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotProvDaRec().setTitTotProvDaRec(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitImpAz().setTitImpAz(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitImpAder().setTitImpAder(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitImpTfr().setTitImpTfr(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitImpVolo().setTitImpVolo(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotManfeeAntic().setTitTotManfeeAntic(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotManfeeRicor().setTitTotManfeeRicor(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotManfeeRec().setTitTotManfeeRec(new AfDecimal(0, 15, 3));
        ws.getTitCont().setTitTpMezPagAdd("");
        ws.getTitCont().setTitEstrCntCorrAdd("");
        ws.getTitCont().getTitDtVlt().setTitDtVlt(0);
        ws.getTitCont().setTitFlForzDtVlt(Types.SPACE_CHAR);
        ws.getTitCont().getTitDtCambioVlt().setTitDtCambioVlt(0);
        ws.getTitCont().getTitTotSpeAge().setTitTotSpeAge(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotCarIas().setTitTotCarIas(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitNumRatAccorpate().setTitNumRatAccorpate(0);
        ws.getTitCont().setTitDsRiga(0);
        ws.getTitCont().setTitDsOperSql(Types.SPACE_CHAR);
        ws.getTitCont().setTitDsVer(0);
        ws.getTitCont().setTitDsTsIniCptz(0);
        ws.getTitCont().setTitDsTsEndCptz(0);
        ws.getTitCont().setTitDsUtente("");
        ws.getTitCont().setTitDsStatoElab(Types.SPACE_CHAR);
        ws.getTitCont().setTitFlTitDaReinvst(Types.SPACE_CHAR);
        ws.getTitCont().getTitDtRichAddRid().setTitDtRichAddRid(0);
        ws.getTitCont().setTitTpEsiRid("");
        ws.getTitCont().setTitCodIban("");
        ws.getTitCont().getTitImpTrasfe().setTitImpTrasfe(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitImpTfrStrc().setTitImpTfrStrc(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitDtCertFisc().setTitDtCertFisc(0);
        ws.getTitCont().getTitTpCausStor().setTitTpCausStor(0);
        ws.getTitCont().setTitTpCausDispStor("");
        ws.getTitCont().setTitTpTitMigraz("");
        ws.getTitCont().getTitTotAcqExp().setTitTotAcqExp(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotRemunAss().setTitTotRemunAss(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotCommisInter().setTitTotCommisInter(new AfDecimal(0, 15, 3));
        ws.getTitCont().setTitTpCausRimb("");
        ws.getTitCont().getTitTotCnbtAntirac().setTitTotCnbtAntirac(new AfDecimal(0, 15, 3));
        ws.getTitCont().setTitFlIncAutogen(Types.SPACE_CHAR);
    }

    public void initAreaLccc0062() {
        ws.getAreaLccc0062().setDtCompetenza(0);
        ws.getAreaLccc0062().setDtDecorrenza(0);
        ws.getAreaLccc0062().setDtUltQtzo(0);
        ws.getAreaLccc0062().setDtUltgzTrch(0);
        ws.getAreaLccc0062().setFrazionamentoFormatted("00000");
        ws.getAreaLccc0062().setTotNumRate(0);
        for (int idx0 = 1; idx0 <= WcomAreaPagina.TAB_RATE_MAXOCCURS; idx0++) {
            ws.getAreaLccc0062().getTabRate(idx0).setDtInf(0);
            ws.getAreaLccc0062().getTabRate(idx0).setDtSup(0);
        }
    }

    public void initIoA2kLccc0003() {
        ws.getIoA2kLccc0003().getInput().setA2kFunz("");
        ws.getIoA2kLccc0003().getInput().setA2kInfo("");
        ws.getIoA2kLccc0003().getInput().setA2kDeltaFormatted("000");
        ws.getIoA2kLccc0003().getInput().setA2kTdelta(Types.SPACE_CHAR);
        ws.getIoA2kLccc0003().getInput().setA2kFislav(Types.SPACE_CHAR);
        ws.getIoA2kLccc0003().getInput().setA2kInicon(Types.SPACE_CHAR);
        ws.getIoA2kLccc0003().getInput().getA2kIndata().setA2kIndata("");
        ws.getIoA2kLccc0003().getOutput().getA2kOugmaX().setA2kOugmaFormatted("00000000");
        ws.getIoA2kLccc0003().getOutput().getA2kOugbmba().setOugg02Formatted("00");
        ws.getIoA2kLccc0003().getOutput().getA2kOugbmba().setOus102(Types.SPACE_CHAR);
        ws.getIoA2kLccc0003().getOutput().getA2kOugbmba().setOumm02Formatted("00");
        ws.getIoA2kLccc0003().getOutput().getA2kOugbmba().setOus202(Types.SPACE_CHAR);
        ws.getIoA2kLccc0003().getOutput().getA2kOugbmba().setOuss02Formatted("00");
        ws.getIoA2kLccc0003().getOutput().getA2kOugbmba().setOuaa02Formatted("00");
        ws.getIoA2kLccc0003().getOutput().getA2kOuamgX().setA2kOuamgFormatted("00000000");
        ws.getIoA2kLccc0003().getOutput().setA2kOuamgp(0);
        ws.getIoA2kLccc0003().getOutput().setA2kOuamg0p(0);
        ws.getIoA2kLccc0003().getOutput().setA2kOuprog9(0);
        ws.getIoA2kLccc0003().getOutput().setA2kOuprogc(0);
        ws.getIoA2kLccc0003().getOutput().setA2kOuprog(0);
        ws.getIoA2kLccc0003().getOutput().setA2kOuproco(0);
        ws.getIoA2kLccc0003().getOutput().getA2kOujulX().setA2kOujulFormatted("0000000");
        ws.getIoA2kLccc0003().getOutput().getA2kOusta().setOugg04Formatted("00");
        ws.getIoA2kLccc0003().getOutput().getA2kOusta().setOumm04("");
        ws.getIoA2kLccc0003().getOutput().getA2kOusta().setOuss04Formatted("00");
        ws.getIoA2kLccc0003().getOutput().getA2kOusta().setOuaa04Formatted("00");
        ws.getIoA2kLccc0003().getOutput().getA2kOurid().setOugg05Formatted("00");
        ws.getIoA2kLccc0003().getOutput().getA2kOurid().setOumm05("");
        ws.getIoA2kLccc0003().getOutput().getA2kOurid().setOuaa05Formatted("00");
        ws.getIoA2kLccc0003().getOutput().setA2kOugg06("");
        ws.getIoA2kLccc0003().getOutput().setA2kOung06Formatted("0");
        ws.getIoA2kLccc0003().getOutput().setA2kOutg06(Types.SPACE_CHAR);
        ws.getIoA2kLccc0003().getOutput().setA2kOugg07Formatted("00");
        ws.getIoA2kLccc0003().getOutput().getA2kOugl07X().setA2kOufa07Formatted("000");
        ws.getIoA2kLccc0003().getOutput().setA2kOugg08Formatted("0");
        ws.getIoA2kLccc0003().getOutput().setA2kOugg09(Types.SPACE_CHAR);
        ws.getIoA2kLccc0003().getOutput().setA2kOugg10(Types.SPACE_CHAR);
        ws.getIoA2kLccc0003().setRcode("");
    }
}
