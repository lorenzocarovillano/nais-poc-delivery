package it.accenture.jnais;

import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.WmovDati;
import it.accenture.jnais.ws.AreaIdsv0001;
import it.accenture.jnais.ws.AreaIoLccs0023;
import it.accenture.jnais.ws.enums.Idso0011SqlcodeSigned;
import it.accenture.jnais.ws.enums.WsFgDefaultGravita;
import it.accenture.jnais.ws.enums.WsTpAnnullo;
import it.accenture.jnais.ws.Ieai9901Area;
import it.accenture.jnais.ws.Lccc0001;
import it.accenture.jnais.ws.Lccs0023Data;
import it.accenture.jnais.ws.occurs.Lccc0023TabMovFuturi;
import it.accenture.jnais.ws.redefines.Lccc0023IdMoviAnn;
import it.accenture.jnais.ws.redefines.Lccc0023IdOgg;
import it.accenture.jnais.ws.redefines.Lccc0023IdRich;
import it.accenture.jnais.ws.redefines.Lccc0023TpMovi;
import it.accenture.jnais.ws.redefines.WmovIdMoviAnn;
import it.accenture.jnais.ws.redefines.WmovIdOgg;
import it.accenture.jnais.ws.redefines.WmovIdRich;
import it.accenture.jnais.ws.redefines.WmovTpMovi;

/**Original name: LCCS0023<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA  VER 1.0                          **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2008.
 * DATE-COMPILED.
 * ----------------------------------------------------------------*
 *     PROGRAMMA ..... LCCS0023
 *     TIPOLOGIA...... AUTORIZZAZIONI
 *     PROCESSO....... XXXX
 *     FUNZIONE....... XXXX
 *     DESCRIZIONE.... VERIFICA PRESENZA MOVIMENTI FUTURI
 *     PAGINA WEB..... N/A
 * **------------------------------------------------------------***</pre>*/
public class Lccs0023 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lccs0023Data ws = new Lccs0023Data();
    //Original name: AREA-IDSV0001
    private AreaIdsv0001 areaIdsv0001;
    //Original name: WCOM-AREA-STATI
    private Lccc0001 lccc0001;
    //Original name: AREA-IO-LCCS0023
    private AreaIoLccs0023 areaIoLccs0023;

    //==== METHODS ====
    /**Original name: PROGRAM_LCCS0023_FIRST_SENTENCES<br>
	 * <pre>---------------------------------------------------------------*</pre>*/
    public long execute(AreaIdsv0001 areaIdsv0001, Lccc0001 lccc0001, AreaIoLccs0023 areaIoLccs0023) {
        this.areaIdsv0001 = areaIdsv0001;
        this.lccc0001 = lccc0001;
        this.areaIoLccs0023 = areaIoLccs0023;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: IF IDSV0001-ESITO-OK
        //                 THRU EX-S1000
        //           END-IF.
        if (this.areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: PERFORM S1000-ELABORAZIONE
            //              THRU EX-S1000
            s1000Elaborazione();
        }
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lccs0023 getInstance() {
        return ((Lccs0023)Programs.getInstance(Lccs0023.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *     OPERAZIONI INIZIALI                                         *
	 * ----------------------------------------------------------------*
	 * --> INIZIALIZZAZIONE AREA D'APPOGGIO PER MOVIMENTI E AREA DI PAG.</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE         IX-INDICI
        //                              WK-VARIABILI.
        initIxIndici();
        initWkVariabili();
        // COB_CODE: MOVE ZEROES     TO LCCC0023-ELE-MOV-FUTURI-MAX
        //                              WMOV-ELE-MOVI-MAX.
        areaIoLccs0023.setEleMovFuturiMax(((short)0));
        ws.setWmovEleMoviMax(((short)0));
        // COB_CODE: MOVE 'N'        TO LCCC0023-PRE-GRAV-BLOCCANTE
        //                              LCCC0023-PRE-GRAV-ANNULLABILE.
        areaIoLccs0023.setPreGravBloccanteFormatted("N");
        areaIoLccs0023.setPreGravAnnullabileFormatted("N");
        // COB_CODE:      PERFORM VARYING IX-TAB-MOV FROM 1 BY 1
        //                  UNTIL IX-TAB-MOV > WK-ELE-MOV-MAX
        //           *-->   INIZIALIZZAZIONE CAMPI NULL
        //                                 WMOV-DS-STATO-ELAB(IX-TAB-MOV)
        //                END-PERFORM.
        ws.setIxTabMov(((short)1));
        while (!(ws.getIxTabMov() > ws.getWkEleMovMax())) {
            //-->   INIZIALIZZAZIONE CAMPI NULL
            // COB_CODE: MOVE HIGH-VALUES TO WMOV-ID-OGG-NULL(IX-TAB-MOV)
            //                               LCCC0023-ID-OGG-NULL(IX-TAB-MOV)
            //                               WMOV-IB-OGG-NULL(IX-TAB-MOV)
            //                               LCCC0023-IB-OGG-NULL(IX-TAB-MOV)
            //                               WMOV-IB-MOVI-NULL(IX-TAB-MOV)
            //                               LCCC0023-IB-MOVI-NULL(IX-TAB-MOV)
            //                               WMOV-TP-OGG-NULL(IX-TAB-MOV)
            //                               LCCC0023-TP-OGG-NULL(IX-TAB-MOV)
            //                               WMOV-ID-RICH-NULL(IX-TAB-MOV)
            //                               LCCC0023-ID-RICH-NULL(IX-TAB-MOV)
            //                               WMOV-TP-MOVI-NULL(IX-TAB-MOV)
            //                               LCCC0023-TP-MOVI-NULL(IX-TAB-MOV)
            //                               WMOV-ID-MOVI-ANN-NULL(IX-TAB-MOV)
            //                               LCCC0023-ID-MOVI-ANN-NULL(IX-TAB-MOV)
            ws.getWmovTabMovi(ws.getIxTabMov()).getLccvmov1().getDati().getWmovIdOgg().setWmovIdOggNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WmovIdOgg.Len.WMOV_ID_OGG_NULL));
            areaIoLccs0023.getTabMovFuturi(ws.getIxTabMov()).getLccc0023IdOgg().setLccc0023IdOggNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Lccc0023IdOgg.Len.LCCC0023_ID_OGG_NULL));
            ws.getWmovTabMovi(ws.getIxTabMov()).getLccvmov1().getDati().setWmovIbOgg(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WmovDati.Len.WMOV_IB_OGG));
            areaIoLccs0023.getTabMovFuturi(ws.getIxTabMov()).setLccc0023IbOgg(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Lccc0023TabMovFuturi.Len.LCCC0023_IB_OGG));
            ws.getWmovTabMovi(ws.getIxTabMov()).getLccvmov1().getDati().setWmovIbMovi(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WmovDati.Len.WMOV_IB_MOVI));
            areaIoLccs0023.getTabMovFuturi(ws.getIxTabMov()).setLccc0023IbMovi(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Lccc0023TabMovFuturi.Len.LCCC0023_IB_MOVI));
            ws.getWmovTabMovi(ws.getIxTabMov()).getLccvmov1().getDati().setWmovTpOgg(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WmovDati.Len.WMOV_TP_OGG));
            areaIoLccs0023.getTabMovFuturi(ws.getIxTabMov()).setLccc0023TpOgg(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Lccc0023TabMovFuturi.Len.LCCC0023_TP_OGG));
            ws.getWmovTabMovi(ws.getIxTabMov()).getLccvmov1().getDati().getWmovIdRich().setWmovIdRichNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WmovIdRich.Len.WMOV_ID_RICH_NULL));
            areaIoLccs0023.getTabMovFuturi(ws.getIxTabMov()).getLccc0023IdRich().setLccc0023IdRichNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Lccc0023IdRich.Len.LCCC0023_ID_RICH_NULL));
            ws.getWmovTabMovi(ws.getIxTabMov()).getLccvmov1().getDati().getWmovTpMovi().setWmovTpMoviNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WmovTpMovi.Len.WMOV_TP_MOVI_NULL));
            areaIoLccs0023.getTabMovFuturi(ws.getIxTabMov()).getLccc0023TpMovi().setLccc0023TpMoviNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Lccc0023TpMovi.Len.LCCC0023_TP_MOVI_NULL));
            ws.getWmovTabMovi(ws.getIxTabMov()).getLccvmov1().getDati().getWmovIdMoviAnn().setWmovIdMoviAnnNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WmovIdMoviAnn.Len.WMOV_ID_MOVI_ANN_NULL));
            areaIoLccs0023.getTabMovFuturi(ws.getIxTabMov()).getLccc0023IdMoviAnn().setLccc0023IdMoviAnnNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Lccc0023IdMoviAnn.Len.LCCC0023_ID_MOVI_ANN_NULL));
            //-->   INIZIALIZZAZIONE CAMPI NUMERICI NOT NULL
            // COB_CODE: MOVE ZEROES TO WMOV-ID-MOVI(IX-TAB-MOV)
            //                          LCCC0023-ID-MOVI(IX-TAB-MOV)
            //                          WMOV-COD-COMP-ANIA(IX-TAB-MOV)
            //                          WMOV-DT-EFF(IX-TAB-MOV)
            //                          WMOV-DT-EFF(IX-TAB-MOV)
            //                          WMOV-DS-VER(IX-TAB-MOV)
            //                          WMOV-DS-TS-CPTZ(IX-TAB-MOV)
            ws.getWmovTabMovi(ws.getIxTabMov()).getLccvmov1().getDati().setWmovIdMovi(0);
            areaIoLccs0023.getTabMovFuturi(ws.getIxTabMov()).setLccc0023IdMovi(0);
            ws.getWmovTabMovi(ws.getIxTabMov()).getLccvmov1().getDati().setWmovCodCompAnia(0);
            ws.getWmovTabMovi(ws.getIxTabMov()).getLccvmov1().getDati().setWmovDtEff(0);
            ws.getWmovTabMovi(ws.getIxTabMov()).getLccvmov1().getDati().setWmovDtEff(0);
            ws.getWmovTabMovi(ws.getIxTabMov()).getLccvmov1().getDati().setWmovDsVer(0);
            ws.getWmovTabMovi(ws.getIxTabMov()).getLccvmov1().getDati().setWmovDsTsCptz(0);
            //-->   INIZIALIZZAZIONE CAMPI ALFANUMERICI
            // COB_CODE: MOVE SPACES TO WMOV-DS-OPER-SQL(IX-TAB-MOV)
            //                          WMOV-DS-UTENTE(IX-TAB-MOV)
            //                          WMOV-DS-STATO-ELAB(IX-TAB-MOV)
            ws.getWmovTabMovi(ws.getIxTabMov()).getLccvmov1().getDati().setWmovDsOperSql(Types.SPACE_CHAR);
            ws.getWmovTabMovi(ws.getIxTabMov()).getLccvmov1().getDati().setWmovDsUtente("");
            ws.getWmovTabMovi(ws.getIxTabMov()).getLccvmov1().getDati().setWmovDsStatoElab(Types.SPACE_CHAR);
            ws.setIxTabMov(Trunc.toShort(ws.getIxTabMov() + 1, 4));
        }
        // COB_CODE: PERFORM S0005-CTRL-DATI-INPUT
        //              THRU EX-S0005.
        s0005CtrlDatiInput();
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO TO WS-MOVIMENTO.
        ws.getWsMovimento().setWsMovimentoFormatted(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimentoFormatted());
        //
        // COB_CODE: IF ANN-VERSAM-AGGIUNTIVO
        //              SET ANNULLO-VERS-SI TO TRUE
        //           ELSE
        //              SET ANNULLO-VERS-NO TO TRUE
        //           END-IF.
        if (ws.getWsMovimento().isAnnVersamAggiuntivo()) {
            // COB_CODE: SET ANNULLO-VERS-SI TO TRUE
            ws.getWsFgAnnVag().setSi();
        }
        else {
            // COB_CODE: SET ANNULLO-VERS-NO TO TRUE
            ws.getWsFgAnnVag().setNo();
        }
    }

    /**Original name: S0005-CTRL-DATI-INPUT<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLI INPUT
	 * ----------------------------------------------------------------*
	 * --  il campo ID-OGGETTO deve essere un numerico</pre>*/
    private void s0005CtrlDatiInput() {
        // COB_CODE: IF LCCC0023-ID-OGG-PTF NOT NUMERIC
        //                 THRU EX-S0300
        //           END-IF
        if (!Functions.isNumber(areaIoLccs0023.getIdOggPtf())) {
            // COB_CODE: MOVE WK-PGM                  TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S0005-CTRL-DATI-INPUT' TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S0005-CTRL-DATI-INPUT");
            // COB_CODE: MOVE '005076'                TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005076");
            // COB_CODE: MOVE 'LCCC0023-ID-OGG-PTF'   TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("LCCC0023-ID-OGG-PTF");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
        //--  il campo ID-OGGETTO deve essere un numerico > zero
        // COB_CODE: IF IDSV0001-ESITO-OK
        //              END-IF
        //           END-IF
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: IF LCCC0023-ID-OGG-PTF NOT > ZERO
            //                 THRU EX-S0300
            //           END-IF
            if (areaIoLccs0023.getIdOggPtf() <= 0) {
                // COB_CODE: MOVE WK-PGM                TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE 'S0005-CTRL-DATI-INPUT'
                //                                      TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr("S0005-CTRL-DATI-INPUT");
                // COB_CODE: MOVE '005018'              TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("005018");
                // COB_CODE: MOVE 'LCCC0023-ID-OGG-PTF' TO IEAI9901-PARAMETRI-ERR
                ws.getIeai9901Area().setParametriErr("LCCC0023-ID-OGG-PTF");
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
            }
        }
        //--  il campo TP-OGGETTO deve essere diverso da spaces e deve
        //--  essere un valore compreso nei valori di dominio
        // COB_CODE: IF IDSV0001-ESITO-OK
        //              END-IF
        //           END-IF
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: IF LCCC0023-TP-OGG-PTF = SPACES
            //                                 OR HIGH-VALUE OR LOW-VALUE
            //                 THRU EX-S0300
            //           END-IF
            if (Characters.EQ_SPACE.test(areaIoLccs0023.getTpOggPtf()) || Characters.EQ_HIGH.test(areaIoLccs0023.getTpOggPtfFormatted()) || Characters.EQ_LOW.test(areaIoLccs0023.getTpOggPtfFormatted())) {
                // COB_CODE: MOVE WK-PGM                TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE 'S0005-CTRL-DATI-INPUT'
                //                                      TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr("S0005-CTRL-DATI-INPUT");
                // COB_CODE: MOVE '005007'              TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("005007");
                // COB_CODE: MOVE 'LCCC0023-TP-OGG-PTF' TO IEAI9901-PARAMETRI-ERR
                ws.getIeai9901Area().setParametriErr("LCCC0023-TP-OGG-PTF");
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
            }
        }
        // COB_CODE: IF IDSV0001-ESITO-OK
        //              END-IF
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: MOVE LCCC0023-TP-OGG-PTF      TO WS-TP-OGG
            ws.getWsTpOgg().setWsTpOgg(areaIoLccs0023.getTpOggPtf());
            // COB_CODE: IF  (NOT POLIZZA) AND (NOT ADESIONE)
            //           AND (NOT TRANCHE) AND (NOT PREVENTIVO)
            //                 THRU EX-S0300
            //           END-IF
            if (!ws.getWsTpOgg().isPolizza() && !ws.getWsTpOgg().isAdesione() && !ws.getWsTpOgg().isTranche() && !ws.getWsTpOgg().isPreventivo()) {
                // COB_CODE: MOVE WK-PGM                TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE 'S0005-CTRL-DATI-INPUT'
                //                                      TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr("S0005-CTRL-DATI-INPUT");
                // COB_CODE: MOVE '005018'              TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("005018");
                // COB_CODE: MOVE 'LCCC0023-TP-OGG-PTF' TO IEAI9901-PARAMETRI-ERR
                ws.getIeai9901Area().setParametriErr("LCCC0023-TP-OGG-PTF");
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
            }
        }
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*
	 * --> LETTURA MOVIMENTI FUTURI PER :CODICE_COMPAGNIA, TIPO_OGGETTO,
	 * --> ID_OGGETTO, STATO_ELABORAZIONE = 'concluso'
	 * --> e DATA EFFETTO > data effetto corrente;</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: PERFORM S1100-LETTURA-MOV-FUTURI
        //              THRU EX-S1100.
        s1100LetturaMovFuturi();
        // COB_CODE: INITIALIZE POLI.
        initPoli();
        // COB_CODE: EVALUATE LCCC0023-TP-OGG-PTF
        //               WHEN 'PO'
        //                       THRU EX-S1110
        //               WHEN 'AD'
        //                    END-IF
        //               WHEN 'TG'
        //                    END-IF
        //               WHEN OTHER
        //                    CONTINUE
        //           END-EVALUATE.
        switch (areaIoLccs0023.getTpOggPtf()) {

            case "PO":// COB_CODE: MOVE LCCC0023-ID-OGG-PTF    TO POL-ID-POLI
                ws.getPoli().setPolIdPoli(areaIoLccs0023.getIdOggPtf());
                // COB_CODE: PERFORM S1110-LEGGI-POLI
                //              THRU EX-S1110
                s1110LeggiPoli();
                break;

            case "AD":// COB_CODE: PERFORM S1111-LEGGI-ADES
                //              THRU EX-S1111
                s1111LeggiAdes();
                // COB_CODE: IF IDSV0001-ESITO-OK
                //                 THRU EX-S1110
                //           END-IF
                if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                    // COB_CODE: PERFORM S1110-LEGGI-POLI
                    //              THRU EX-S1110
                    s1110LeggiPoli();
                }
                break;

            case "TG":// COB_CODE: PERFORM S1112-LEGGI-TRCH
                //              THRU EX-S1112
                s1112LeggiTrch();
                // COB_CODE: IF IDSV0001-ESITO-OK
                //                 THRU EX-S1110
                //           END-IF
                if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                    // COB_CODE: PERFORM S1110-LEGGI-POLI
                    //              THRU EX-S1110
                    s1110LeggiPoli();
                }
                break;

            default:// COB_CODE: CONTINUE
            //continue
                break;
        }
        //    IF IDSV0001-ESITO-OK
        //       PERFORM S1110-LEGGI-POLI
        //          THRU EX-S1110
        //    END-IF
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *-->    SE WMOV-ELE-MOVI-MAX VALE ZERO SIGNIFICA CHE NON CI SONO
        //           *-->    MOVIMENTI FUTURI
        //                   END-IF
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //-->    SE WMOV-ELE-MOVI-MAX VALE ZERO SIGNIFICA CHE NON CI SONO
            //-->    MOVIMENTI FUTURI
            // COB_CODE:         IF WMOV-ELE-MOVI-MAX NOT = ZERO
            //           *-->       CONTROLLIAMO IL DEFAULT SE E' PRESENTE
            //                      END-IF
            //                   END-IF
            if (ws.getWmovEleMoviMax() != 0) {
                //-->       CONTROLLIAMO IL DEFAULT SE E' PRESENTE
                // COB_CODE: IF POL-FL-POLI-IFP = 'P'
                //                 THRU EX-S1210
                //           ELSE
                //                 THRU EX-S1200
                //           END-IF
                if (ws.getPoli().getPolFlPoliIfp() == 'P') {
                    // COB_CODE: PERFORM S1210-GESTIONE-MOV-FUTURI-FNZ
                    //              THRU EX-S1210
                    s1210GestioneMovFuturiFnz();
                }
                else {
                    // COB_CODE: PERFORM S1200-GESTIONE-MOV-FUTURI-DEF
                    //              THRU EX-S1200
                    s1200GestioneMovFuturiDef();
                }
            }
        }
        //--> RICERCA I MOVIMENTI FUTURI RECUPERATI NELLA TABELLA
        //--> AMMISSIBILITA' FUNZIONE FUNZIONE
        // COB_CODE:      PERFORM VARYING IX-TAB-MOV FROM 1 BY 1
        //                  UNTIL IX-TAB-MOV > WMOV-ELE-MOVI-MAX
        //                     OR IDSV0001-ESITO-KO
        //           *
        //                     END-IF
        //           *
        //                END-PERFORM.
        ws.setIxTabMov(((short)1));
        while (!(ws.getIxTabMov() > ws.getWmovEleMoviMax() || areaIdsv0001.getEsito().isIdsv0001EsitoKo())) {
            //
            // COB_CODE: IF ANNULLO-VERS-SI
            //              END-IF
            //           ELSE
            //                 THRU EX-S1300
            //           END-IF
            if (ws.getWsFgAnnVag().isSi()) {
                // COB_CODE: IF WMOV-DT-EFF(IX-TAB-MOV) >= WK-DT-EFF
                //              END-IF
                //           END-IF
                if (ws.getWmovTabMovi(ws.getIxTabMov()).getLccvmov1().getDati().getWmovDtEff() >= ws.getWkVariabili().getWkDtEff()) {
                    // COB_CODE: IF POL-FL-POLI-IFP = 'P'
                    //                 THRU EX-S1310
                    //           ELSE
                    //                 THRU EX-S1300
                    //           END-IF
                    if (ws.getPoli().getPolFlPoliIfp() == 'P') {
                        // COB_CODE: PERFORM S1310-GESTIONE-MOV-FNZ
                        //              THRU EX-S1310
                        s1310GestioneMovFnz();
                    }
                    else {
                        // COB_CODE: PERFORM S1300-GESTIONE-MOV-FUTURI
                        //              THRU EX-S1300
                        s1300GestioneMovFuturi();
                    }
                }
            }
            else {
                // COB_CODE: PERFORM S1300-GESTIONE-MOV-FUTURI
                //              THRU EX-S1300
                s1300GestioneMovFuturi();
            }
            //
            ws.setIxTabMov(Trunc.toShort(ws.getIxTabMov() + 1, 4));
        }
    }

    /**Original name: S1110-LEGGI-POLI<br>
	 * <pre>----------------------------------------------------------------*
	 *     LETTURA TABELLA MOVIMENTO
	 * ----------------------------------------------------------------*
	 * -- Inizializzazione area di lettura
	 *     INITIALIZE POLI.
	 * -- La data effetto/competenza viene sempre valorizzata</pre>*/
    private void s1110LeggiPoli() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE ZEROES                 TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                          IDSI0011-DATA-FINE-EFFETTO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        // COB_CODE: MOVE ZEROES                 TO IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        //-- Setto i valori di output per il controllo degli errori
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC  TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
        //-- Impostazione codice di ricerca
        //    MOVE MOV-ID-OGG             TO POL-ID-POLI.
        //-- Parametri per l'estrazione
        // COB_CODE: SET IDSI0011-TRATT-X-COMPETENZA
        //                                       TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattXCompetenza();
        // COB_CODE: SET IDSI0011-ID             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setIdsi0011Id();
        // COB_CODE: SET IDSI0011-SELECT         TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        //-- Passaggio dei campi
        // COB_CODE: MOVE 'POLI'                 TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("POLI");
        // COB_CODE: MOVE SPACES                 TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
        // COB_CODE: MOVE POLI                   TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getPoli().getPoliFormatted());
        //-- Chiamata al dispatcher
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX.
        callDispatcher();
        //-- Controllo il risultato
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //                   END-EVALUATE
        //                ELSE
        //           *--    GESTIRE ERRORE DISPATCHER
        //                      THRU EX-S0300
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            // COB_CODE:         EVALUATE TRUE
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //           *--         OPERAZIONE ESEGUITA CORRETTAMENTE
            //                          MOVE IDSO0011-BUFFER-DATI TO POLI
            //                       WHEN OTHER
            //           *--         NESSUNA OCCORRENZA TROVATA
            //                             THRU EX-S0300
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://--         OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: MOVE IDSO0011-BUFFER-DATI TO POLI
                    ws.getPoli().setPoliFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    break;

                default://--         NESSUNA OCCORRENZA TROVATA
                    // COB_CODE: MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'S1110-LEGGI-POLI'
                    //                              TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("S1110-LEGGI-POLI");
                    // COB_CODE: MOVE '005069'      TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005069");
                    // COB_CODE: MOVE PGM-IDBSPOL0  TO IEAI9901-PARAMETRI-ERR
                    ws.getIeai9901Area().setParametriErr(ws.getPgmIdbspol0());
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;
            }
        }
        else {
            //--    GESTIRE ERRORE DISPATCHER
            // COB_CODE: MOVE WK-PGM                  TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S1110-LEGGI-POLI'      TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S1110-LEGGI-POLI");
            // COB_CODE: MOVE '005016'                TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING PGM-IDBSPOL0         ';'
            //                  IDSO0011-RETURN-CODE ';'
            //                  IDSO0011-SQLCODE
            //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getPgmIdbspol0Formatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: S1111-LEGGI-ADES<br>
	 * <pre>----------------------------------------------------------------*
	 *     LETTURA TABELLA ADESIONE
	 * ----------------------------------------------------------------*
	 * -- Inizializzazione area di lettura</pre>*/
    private void s1111LeggiAdes() {
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE ADES.
        initAdes();
        //-- La data effetto/competenza viene sempre valorizzata
        // COB_CODE: MOVE ZEROES                 TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                          IDSI0011-DATA-FINE-EFFETTO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        // COB_CODE: MOVE ZEROES                 TO IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        //-- Setto i valori di output per il controllo degli errori
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC  TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
        //-- Impostazione codice di ricerca
        // COB_CODE: MOVE LCCC0023-ID-OGG-PTF    TO ADE-ID-ADES.
        ws.getAdes().setAdeIdAdes(areaIoLccs0023.getIdOggPtf());
        //-- Parametri per l'estrazione
        // COB_CODE: SET IDSI0011-TRATT-X-COMPETENZA
        //                                       TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattXCompetenza();
        // COB_CODE: SET IDSI0011-ID             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setIdsi0011Id();
        // COB_CODE: SET IDSI0011-SELECT         TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        //-- Passaggio dei campi
        // COB_CODE: MOVE 'ADES'                 TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("ADES");
        // COB_CODE: MOVE SPACES                 TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
        // COB_CODE: MOVE ADES                   TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getAdes().getAdesFormatted());
        //-- Chiamata al dispatcher
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX.
        callDispatcher();
        //-- Controllo il risultato
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //                   END-EVALUATE
        //                ELSE
        //           *--    GESTIRE ERRORE DISPATCHER
        //                      THRU EX-S0300
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            // COB_CODE:         EVALUATE TRUE
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //           *--         OPERAZIONE ESEGUITA CORRETTAMENTE
            //                          MOVE ADE-ID-POLI    TO POL-ID-POLI
            //                       WHEN OTHER
            //           *--         NESSUNA OCCORRENZA TROVATA
            //                             THRU EX-S0300
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://--         OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: MOVE IDSO0011-BUFFER-DATI TO ADES
                    ws.getAdes().setAdesFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    // COB_CODE: MOVE ADE-ID-POLI    TO POL-ID-POLI
                    ws.getPoli().setPolIdPoli(ws.getAdes().getAdeIdPoli());
                    break;

                default://--         NESSUNA OCCORRENZA TROVATA
                    // COB_CODE: MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'S1111-LEGGI-ADES'
                    //                              TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("S1111-LEGGI-ADES");
                    // COB_CODE: MOVE '005069'      TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005069");
                    // COB_CODE: MOVE PGM-IDBSADE0  TO IEAI9901-PARAMETRI-ERR
                    ws.getIeai9901Area().setParametriErr(ws.getPgmIdbsade0());
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;
            }
        }
        else {
            //--    GESTIRE ERRORE DISPATCHER
            // COB_CODE: MOVE WK-PGM                  TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S1111-LEGGI-ADES'      TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S1111-LEGGI-ADES");
            // COB_CODE: MOVE '005016'                TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING PGM-IDBSADE0         ';'
            //                  IDSO0011-RETURN-CODE ';'
            //                  IDSO0011-SQLCODE
            //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getPgmIdbsade0Formatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: S1112-LEGGI-TRCH<br>
	 * <pre>----------------------------------------------------------------*
	 *     LETTURA TABELLA TRANCHE
	 * ----------------------------------------------------------------*
	 * -- Inizializzazione area di lettura</pre>*/
    private void s1112LeggiTrch() {
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE TRCH-DI-GAR.
        initTrchDiGar();
        //-- La data effetto/competenza viene sempre valorizzata
        // COB_CODE: MOVE ZEROES                 TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                          IDSI0011-DATA-FINE-EFFETTO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        // COB_CODE: MOVE ZEROES                 TO IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        //-- Setto i valori di output per il controllo degli errori
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC  TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
        //-- Impostazione codice di ricerca
        // COB_CODE: MOVE LCCC0023-ID-OGG-PTF    TO TGA-ID-TRCH-DI-GAR.
        ws.getTrchDiGar().setTgaIdTrchDiGar(areaIoLccs0023.getIdOggPtf());
        //-- Parametri per l'estrazione
        // COB_CODE: SET IDSI0011-TRATT-X-COMPETENZA
        //                                       TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattXCompetenza();
        // COB_CODE: SET IDSI0011-ID             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setIdsi0011Id();
        // COB_CODE: SET IDSI0011-SELECT         TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        //-- Passaggio dei campi
        // COB_CODE: MOVE 'TRCH-DI-GAR'          TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("TRCH-DI-GAR");
        // COB_CODE: MOVE SPACES                 TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
        // COB_CODE: MOVE TRCH-DI-GAR            TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getTrchDiGar().getTrchDiGarFormatted());
        //-- Chiamata al dispatcher
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX.
        callDispatcher();
        //-- Controllo il risultato
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //                   END-EVALUATE
        //                ELSE
        //           *--    GESTIRE ERRORE DISPATCHER
        //                      THRU EX-S0300
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            // COB_CODE:         EVALUATE TRUE
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //           *--         OPERAZIONE ESEGUITA CORRETTAMENTE
            //                          MOVE TGA-ID-POLI   TO POL-ID-POLI
            //                       WHEN OTHER
            //           *--         NESSUNA OCCORRENZA TROVATA
            //                             THRU EX-S0300
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://--         OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: MOVE IDSO0011-BUFFER-DATI TO TRCH-DI-GAR
                    ws.getTrchDiGar().setTrchDiGarFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    // COB_CODE: MOVE TGA-ID-POLI   TO POL-ID-POLI
                    ws.getPoli().setPolIdPoli(ws.getTrchDiGar().getTgaIdPoli());
                    break;

                default://--         NESSUNA OCCORRENZA TROVATA
                    // COB_CODE: MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'S1112-LEGGI-TRCH'
                    //                              TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("S1112-LEGGI-TRCH");
                    // COB_CODE: MOVE '005069'      TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005069");
                    // COB_CODE: MOVE PGM-IDBSTGA0  TO IEAI9901-PARAMETRI-ERR
                    ws.getIeai9901Area().setParametriErr(ws.getPgmIdbstga0());
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;
            }
        }
        else {
            //--    GESTIRE ERRORE DISPATCHER
            // COB_CODE: MOVE WK-PGM                  TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S1112-LEGGI-TRCH'      TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S1112-LEGGI-TRCH");
            // COB_CODE: MOVE '005016'                TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING PGM-IDBSTGA0         ';'
            //                  IDSO0011-RETURN-CODE ';'
            //                  IDSO0011-SQLCODE
            //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getPgmIdbstga0Formatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: S1100-LETTURA-MOV-FUTURI<br>
	 * <pre>----------------------------------------------------------------*
	 *     LETTURA TABELLA MOVIMENTO
	 * ----------------------------------------------------------------*</pre>*/
    private void s1100LetturaMovFuturi() {
        // COB_CODE: INITIALIZE IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
        // COB_CODE: SET  IDSO0011-SUCCESSFUL-RC   TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET  IDSO0011-SUCCESSFUL-SQL  TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
        // COB_CODE: SET  IDSV0001-ESITO-OK        TO TRUE.
        areaIdsv0001.getEsito().setIdsv0001EsitoOk();
        // COB_CODE: SET  WCOM-OVERFLOW-NO         TO TRUE.
        ws.getLccc0007().getFlagOverflow().setNo();
        // COB_CODE: SET IDSI0011-TRATT-DEFAULT    TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setIdsi0011TrattDefault();
        // COB_CODE: SET IDSI0011-WHERE-CONDITION  TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setWhereCondition();
        // COB_CODE: SET IDSI0011-FETCH-FIRST      TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setFetchFirst();
        // COB_CODE: MOVE 'LDBS2430'               TO IDSI0011-CODICE-STR-DATO
        //                                            WK-TABELLA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("LDBS2430");
        ws.getWkVariabili().setWkTabella("LDBS2430");
        // COB_CODE: MOVE ZERO                     TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                            IDSI0011-DATA-FINE-EFFETTO
        //                                            IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        // COB_CODE: MOVE SPACES                   TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //                                         TO MOV-COD-COMP-ANIA
        ws.getMovi().setMovCodCompAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: MOVE IDSV0001-DATA-EFFETTO    TO MOV-DT-EFF
        ws.getMovi().setMovDtEff(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
        // COB_CODE: MOVE LCCC0023-ID-OGG-PTF      TO MOV-ID-OGG
        ws.getMovi().getMovIdOgg().setMovIdOgg(areaIoLccs0023.getIdOggPtf());
        // COB_CODE: MOVE LCCC0023-TP-OGG-PTF      TO MOV-TP-OGG
        ws.getMovi().setMovTpOgg(areaIoLccs0023.getTpOggPtf());
        // COB_CODE: MOVE MOVI                     TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getMovi().getMoviFormatted());
        // COB_CODE: PERFORM S1150-FETCH-DISPATCHER
        //              THRU EX-S1150.
        s1150FetchDispatcher();
    }

    /**Original name: S1140-PREP-LET-MOV-FUTURI<br>
	 * <pre>----------------------------------------------------------------*
	 *     LETTURA TABELLA MOVIMENTO
	 * ----------------------------------------------------------------*</pre>*/
    private void s1140PrepLetMovFuturi() {
        // COB_CODE: SET  IDSO0011-SUCCESSFUL-RC   TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET  IDSO0011-SUCCESSFUL-SQL  TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
        // COB_CODE: SET IDSI0011-TRATT-DEFAULT    TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setIdsi0011TrattDefault();
        // COB_CODE: SET IDSI0011-WHERE-CONDITION  TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setWhereCondition();
        // COB_CODE: SET IDSI0011-FETCH-NEXT       TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setFetchNext();
        // COB_CODE: MOVE 'LDBS2430'               TO IDSI0011-CODICE-STR-DATO
        //                                            WK-TABELLA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("LDBS2430");
        ws.getWkVariabili().setWkTabella("LDBS2430");
    }

    /**Original name: S1150-FETCH-DISPATCHER<br>
	 * <pre>----------------------------------------------------------------*
	 *    CALL DISPATCHER PER FETCH
	 * ----------------------------------------------------------------*</pre>*/
    private void s1150FetchDispatcher() {
        ConcatUtil concatUtil = null;
        // COB_CODE: PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC
        //                      OR NOT IDSO0011-SUCCESSFUL-SQL
        //                      OR NOT IDSV0001-ESITO-OK
        //                      OR WCOM-OVERFLOW-YES
        //               END-IF
        //           END-PERFORM.
        while (!(!ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc() || !ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().isSuccessfulSql() || !areaIdsv0001.getEsito().isIdsv0001EsitoOk() || ws.getLccc0007().getFlagOverflow().isYes())) {
            // COB_CODE: PERFORM CALL-DISPATCHER
            //              THRU CALL-DISPATCHER-EX
            callDispatcher();
            // COB_CODE:          IF IDSO0011-SUCCESSFUL-RC
            //                       END-EVALUATE
            //                    ELSE
            //           *-->        ERRORE DISPATCHER
            //                          THRU EX-S0300
            //                    END-IF
            if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
                // COB_CODE:             EVALUATE TRUE
                //                           WHEN IDSO0011-NOT-FOUND
                //           *-->            NON CI SONO MOVIMENTI FUTURI
                //                                END-IF
                //                           WHEN IDSO0011-SUCCESSFUL-SQL
                //           *-->            OPERAZIONE ESEGUITA CORRETTAMENTE
                //                                END-IF
                //                       END-EVALUATE
                switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                    case Idso0011SqlcodeSigned.NOT_FOUND://-->            NON CI SONO MOVIMENTI FUTURI
                        // COB_CODE: IF IDSI0011-FETCH-FIRST
                        //                TO LCCC0023-ELE-MOV-FUTURI-MAX
                        //           END-IF
                        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchFirst()) {
                            // COB_CODE: MOVE ZEROES
                            //             TO LCCC0023-ELE-MOV-FUTURI-MAX
                            areaIoLccs0023.setEleMovFuturiMax(((short)0));
                        }
                        break;

                    case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://-->            OPERAZIONE ESEGUITA CORRETTAMENTE
                        // COB_CODE: MOVE IDSO0011-BUFFER-DATI     TO MOVI
                        ws.getMovi().setMoviFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                        //--> i movimenti futuri che gia sono stati annullati devono
                        //--> essere scartati
                        // COB_CODE:                      IF MOV-ID-MOVI-ANN-NULL   = HIGH-VALUE
                        //           * Prima di salvare la movi verifico se e' un movimento di annullo
                        //                                   END-IF
                        //                                END-IF
                        if (Characters.EQ_HIGH.test(ws.getMovi().getMovIdMoviAnn().getMovIdMoviAnnNullFormatted())) {
                            // Prima di salvare la movi verifico se e' un movimento di annullo
                            // COB_CODE: SET NO-MOVI-ANN TO TRUE
                            ws.getWsFgMoviAnn().setNoMoviAnn();
                            // COB_CODE: PERFORM S1160-VERIF-MOVI-ANN
                            //              THRU EX-S1160
                            s1160VerifMoviAnn();
                            // La riga seguente h asteriscata per risolvere la sir BNL
                            // FCTVI00011717. Per ulteriori chiarimenti rivolgersi a
                            // Nicola Esposito - Espedito Gioia
                            //                    AND MOV-ID-MOVI-COLLG-NULL = HIGH-VALUE
                            // COB_CODE: IF NO-MOVI-ANN
                            //              END-IF
                            //           END-IF
                            if (ws.getWsFgMoviAnn().isNoMoviAnn()) {
                                // COB_CODE: ADD  1  TO WMOV-ELE-MOVI-MAX
                                ws.setWmovEleMoviMax(Trunc.toShort(1 + ws.getWmovEleMoviMax(), 4));
                                //-->  Se il contatore di lettura h maggiore della capienza
                                //-->  prevista per la TAB.MOVIMENTI FUTURI dell'area di
                                //-->  pagina allora siamo in overflow
                                // COB_CODE: IF WMOV-ELE-MOVI-MAX > WK-ELE-MOV-MAX
                                //             THRU S11860-CLOSE-CURSOR-EX
                                //           ELSE
                                //              END-IF
                                //           END-IF
                                if (ws.getWmovEleMoviMax() > ws.getWkEleMovMax()) {
                                    // COB_CODE: SET WCOM-OVERFLOW-YES   TO TRUE
                                    ws.getLccc0007().getFlagOverflow().setYes();
                                    // COB_CODE: COMPUTE WMOV-ELE-MOVI-MAX =
                                    //                   WMOV-ELE-MOVI-MAX - 1
                                    ws.setWmovEleMoviMax(Trunc.toShort(ws.getWmovEleMoviMax() - 1, 4));
                                    // COB_CODE: PERFORM S11860-CLOSE-CURSOR
                                    //             THRU S11860-CLOSE-CURSOR-EX
                                    s11860CloseCursor();
                                }
                                else {
                                    // COB_CODE: MOVE MOVI
                                    //             TO WMOV-DATI(WMOV-ELE-MOVI-MAX)
                                    ws.getWmovTabMovi(ws.getWmovEleMoviMax()).getLccvmov1().getDati().setDatiBytes(ws.getMovi().getMoviBytes());
                                    // COB_CODE: MOVE MOV-TP-MOVI
                                    //             TO WS-MOVIMENTO
                                    ws.getWsMovimento().setWsMovimento(TruncAbs.toInt(ws.getMovi().getMovTpMovi().getMovTpMovi(), 5));
                                    // COB_CODE: IF VERSAM-AGGIUNTIVO
                                    //           AND ANNULLO-VERS-SI
                                    //                TO WK-DT-EFF
                                    //           END-IF
                                    if (ws.getWsMovimento().isVersamAggiuntivo() && ws.getWsFgAnnVag().isSi()) {
                                        // COB_CODE: MOVE MOV-DT-EFF
                                        //             TO WK-DT-EFF
                                        ws.getWkVariabili().setWkDtEff(ws.getMovi().getMovDtEff());
                                    }
                                }
                            }
                        }
                        // COB_CODE: IF IDSV0001-ESITO-OK
                        //                 THRU EX-S1140
                        //           END-IF
                        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                            // COB_CODE: PERFORM S1140-PREP-LET-MOV-FUTURI
                            //              THRU EX-S1140
                            s1140PrepLetMovFuturi();
                        }
                        break;

                    default:break;
                }
            }
            else {
                //-->        ERRORE DISPATCHER
                // COB_CODE: MOVE WK-PGM
                //             TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE 'S1150-FETCH-DISPATCHER'
                //             TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr("S1150-FETCH-DISPATCHER");
                // COB_CODE: MOVE '005016'
                //             TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("005016");
                // COB_CODE: STRING WK-TABELLA            ';'
                //                  IDSO0011-RETURN-CODE  ';'
                //                  IDSO0011-SQLCODE
                //                  DELIMITED BY SIZE INTO
                //                  IEAI9901-PARAMETRI-ERR
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkVariabili().getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
            }
        }
    }

    /**Original name: S11860-CLOSE-CURSOR<br>*/
    private void s11860CloseCursor() {
        ConcatUtil concatUtil = null;
        // COB_CODE: SET  IDSO0011-SUCCESSFUL-RC   TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET  IDSO0011-SUCCESSFUL-SQL  TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
        // COB_CODE: SET IDSI0011-TRATT-DEFAULT    TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setIdsi0011TrattDefault();
        // COB_CODE: SET IDSI0011-WHERE-CONDITION  TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setWhereCondition();
        // COB_CODE: MOVE 'LDBS2430'               TO IDSI0011-CODICE-STR-DATO
        //                                            WK-TABELLA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("LDBS2430");
        ws.getWkVariabili().setWkTabella("LDBS2430");
        // COB_CODE: SET IDSI0011-CLOSE-CURSOR
        //             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setIdsv0003CloseCursor();
        //
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX
        callDispatcher();
        //
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //           *
        //                   END-EVALUATE
        //           *
        //                ELSE
        //           *
        //                      THRU EX-S0300
        //           *
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            //
            //
            // COB_CODE:         EVALUATE TRUE
            //           *
            //                       WHEN IDSO0011-NOT-FOUND
            //           *
            //                          CONTINUE
            //           *
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //           *
            //                          CONTINUE
            //           *
            //                       WHEN OTHER
            //           *
            //                               THRU EX-S0300
            //           *
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.NOT_FOUND://
                // COB_CODE: CONTINUE
                //continue
                //
                    break;

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://
                // COB_CODE: CONTINUE
                //continue
                //
                    break;

                default://
                    // COB_CODE: MOVE WK-PGM
                    //             TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'S11860-CLOSE-CURSOR'
                    //             TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("S11860-CLOSE-CURSOR");
                    // COB_CODE: MOVE '005016'
                    //             TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005016");
                    // COB_CODE: STRING WK-TABELLA   ';'
                    //                  IDSO0011-RETURN-CODE ';'
                    //                  IDSO0011-SQLCODE
                    //             DELIMITED BY SIZE
                    //             INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkVariabili().getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    //
                    break;
            }
            //
        }
        else {
            //
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S11860-CLOSE-CURSOR'
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S11860-CLOSE-CURSOR");
            // COB_CODE: MOVE '005056'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005056");
            // COB_CODE: STRING WK-TABELLA  ';'
            //                  IDSO0011-RETURN-CODE ';'
            //                  IDSO0011-SQLCODE
            //             DELIMITED BY SIZE
            //             INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkVariabili().getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
            //
        }
    }

    /**Original name: S1160-VERIF-MOVI-ANN<br>
	 * <pre>----------------------------------------------------------------*
	 *     VERIFICO ESISTENZA MOVIMENTO DI ANNULLO
	 * ----------------------------------------------------------------*</pre>*/
    private void s1160VerifMoviAnn() {
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC    TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
        // COB_CODE: SET IDSV0001-ESITO-OK         TO TRUE.
        areaIdsv0001.getEsito().setIdsv0001EsitoOk();
        // COB_CODE: SET IDSI0011-TRATT-SENZA-STOR TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattSenzaStor();
        // COB_CODE: SET IDSI0011-WHERE-CONDITION  TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setWhereCondition();
        // COB_CODE: SET IDSI0011-SELECT           TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        // COB_CODE: MOVE 'LDBS2440'               TO IDSI0011-CODICE-STR-DATO
        //                                            WK-TABELLA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("LDBS2440");
        ws.getWkVariabili().setWkTabella("LDBS2440");
        // COB_CODE: MOVE ZERO                     TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                            IDSI0011-DATA-FINE-EFFETTO
        //                                            IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        // COB_CODE: MOVE SPACES                   TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //                                         TO L05-COD-COMP-ANIA.
        ws.getAmmbFunzFunz().setCodCompAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: MOVE MOV-TP-MOVI              TO L05-TP-MOVI-ESEC
        //                                            L05-TP-MOVI-RIFTO.
        ws.getAmmbFunzFunz().setTpMoviEsec(ws.getMovi().getMovTpMovi().getMovTpMovi());
        ws.getAmmbFunzFunz().setTpMoviRifto(ws.getMovi().getMovTpMovi().getMovTpMovi());
        // COB_CODE: MOVE 'AN'                     TO LDBV2441-GRAV-FUNZ-FUNZ-1.
        ws.getLdbv2441().setFunz1("AN");
        // COB_CODE: MOVE LDBV2441                 TO IDSI0011-BUFFER-WHERE-COND.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferWhereCond(ws.getLdbv2441().getLdbv2441Formatted());
        // COB_CODE: MOVE AMMB-FUNZ-FUNZ           TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getAmmbFunzFunz().getAmmbFunzFunzFormatted());
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX.
        callDispatcher();
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //                   END-EVALUATE
        //                ELSE
        //           *->     GESTIRE ERRORE DISPATCHER
        //                      THRU EX-S0300
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            // COB_CODE:         EVALUATE TRUE
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //           *-->        OPERAZIONE ESEGUITA CORRETTAMENTE
            //                            SET SI-MOVI-ANN   TO TRUE
            //                       WHEN IDSO0011-NOT-FOUND
            //           *-->        CHIAVE NON TROVATA
            //                            SET NO-MOVI-ANN   TO TRUE
            //                       WHEN OTHER
            //           *-->        ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $ RC=$
            //           *-->        SQLCODE=$
            //                               THRU EX-S0300
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://-->        OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: SET SI-MOVI-ANN   TO TRUE
                    ws.getWsFgMoviAnn().setSiMoviAnn();
                    break;

                case Idso0011SqlcodeSigned.NOT_FOUND://-->        CHIAVE NON TROVATA
                    // COB_CODE: SET NO-MOVI-ANN   TO TRUE
                    ws.getWsFgMoviAnn().setNoMoviAnn();
                    break;

                default://-->        ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $ RC=$
                    //-->        SQLCODE=$
                    // COB_CODE: MOVE WK-PGM       TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'S1160-VERIF-MOVI-ANN'
                    //                             TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("S1160-VERIF-MOVI-ANN");
                    // COB_CODE: MOVE '005016'     TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005016");
                    // COB_CODE: STRING WK-TABELLA           ';'
                    //                  IDSO0011-RETURN-CODE ';'
                    //                  IDSO0011-SQLCODE
                    //                  DELIMITED BY SIZE INTO
                    //                  IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkVariabili().getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;
            }
        }
        else {
            //->     GESTIRE ERRORE DISPATCHER
            // COB_CODE: MOVE WK-PGM                TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S1160-VERIF-MOVI-ANN'
            //                                      TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S1160-VERIF-MOVI-ANN");
            // COB_CODE: MOVE '005016'              TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING WK-TABELLA           ';'
            //                  IDSO0011-RETURN-CODE ';'
            //                  IDSO0011-SQLCODE
            //                  DELIMITED BY SIZE INTO
            //                  IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkVariabili().getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: S1200-GESTIONE-MOV-FUTURI-DEF<br>
	 * <pre>----------------------------------------------------------------*
	 *     GESTIONE MOVIMENTI FUTURI DEFAULT
	 * ----------------------------------------------------------------*</pre>*/
    private void s1200GestioneMovFuturiDef() {
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE IDSI0011-BUFFER-DATI
        //                      LDBVG781.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
        initLdbvg781();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC    TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
        // COB_CODE: SET IDSV0001-ESITO-OK         TO TRUE.
        areaIdsv0001.getEsito().setIdsv0001EsitoOk();
        // COB_CODE: SET WCOM-OVERFLOW-NO          TO TRUE.
        ws.getLccc0007().getFlagOverflow().setNo();
        // COB_CODE: SET IDSI0011-TRATT-SENZA-STOR TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattSenzaStor();
        // COB_CODE: SET IDSI0011-WHERE-CONDITION  TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setWhereCondition();
        // COB_CODE: SET IDSI0011-SELECT           TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        // COB_CODE: MOVE 'LDBSG780'               TO IDSI0011-CODICE-STR-DATO
        //                                            WK-TABELLA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("LDBSG780");
        ws.getWkVariabili().setWkTabella("LDBSG780");
        // COB_CODE: MOVE ZERO                     TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                            IDSI0011-DATA-FINE-EFFETTO
        //                                            IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        // COB_CODE: MOVE SPACES                   TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //                                         TO L05-COD-COMP-ANIA.
        ws.getAmmbFunzFunz().setCodCompAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO  TO L05-TP-MOVI-ESEC.
        ws.getAmmbFunzFunz().setTpMoviEsec(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimento());
        // COB_CODE: MOVE ZEROES                   TO L05-TP-MOVI-RIFTO.
        ws.getAmmbFunzFunz().setTpMoviRifto(0);
        // COB_CODE: MOVE 'G'                      TO LDBVG781-FL-POLI-IFP
        ws.getLdbvg781().setFlPoliIfpFormatted("G");
        // COB_CODE: MOVE 'BL'                     TO LDBVG781-GRAV-FUNZ-FUNZ-1.
        ws.getLdbvg781().setGravFunzFunz1("BL");
        // COB_CODE: MOVE 'AM'                     TO LDBVG781-GRAV-FUNZ-FUNZ-2.
        ws.getLdbvg781().setGravFunzFunz2("AM");
        // COB_CODE: MOVE LDBVG781                 TO IDSI0011-BUFFER-WHERE-COND.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferWhereCond(ws.getLdbvg781().getLdbvg781Formatted());
        // COB_CODE: MOVE AMMB-FUNZ-FUNZ           TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getAmmbFunzFunz().getAmmbFunzFunzFormatted());
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX.
        callDispatcher();
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //                   END-EVALUATE
        //                ELSE
        //           *->     GESTIRE ERRORE DISPATCHER
        //                      THRU EX-S0300
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            // COB_CODE:         EVALUATE TRUE
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //           *-->        OPERAZIONE ESEGUITA CORRETTAMENTE
            //                            END-EVALUATE
            //                       WHEN IDSO0011-NOT-FOUND
            //           *-->        CHIAVE NON TROVATA
            //                            SET WS-DEF-AMMISSIBILE         TO TRUE
            //                       WHEN OTHER
            //           *-->        ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $ RC=$
            //           *-->        SQLCODE=$
            //                               THRU EX-S0300
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://-->        OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: MOVE IDSO0011-BUFFER-DATI
                    //             TO AMMB-FUNZ-FUNZ
                    ws.getAmmbFunzFunz().setAmmbFunzFunzFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    // COB_CODE: MOVE L05-GRAV-FUNZ-FUNZ        TO WS-TP-ANNULLO
                    ws.getWsTpAnnullo().setWsTpAnnullo(ws.getAmmbFunzFunz().getGravFunzFunz());
                    // COB_CODE: EVALUATE TRUE
                    //               WHEN TA-BLOCCANTE
                    //                  SET WS-DEF-BLOCCANTE    TO TRUE
                    //               WHEN TA-AMMISSIBILE
                    //                  SET WS-DEF-AMMISSIBILE  TO TRUE
                    //           END-EVALUATE
                    switch (ws.getWsTpAnnullo().getWsTpAnnullo()) {

                        case WsTpAnnullo.BLOCCANTE:// COB_CODE: SET WS-DEF-BLOCCANTE    TO TRUE
                            ws.getWsFgDefaultGravita().setBloccante();
                            break;

                        case WsTpAnnullo.AMMISSIBILE:// COB_CODE: SET WS-DEF-AMMISSIBILE  TO TRUE
                            ws.getWsFgDefaultGravita().setAmmissibile();
                            break;

                        default:break;
                    }
                    break;

                case Idso0011SqlcodeSigned.NOT_FOUND://-->        CHIAVE NON TROVATA
                    // COB_CODE: SET WS-DEF-AMMISSIBILE         TO TRUE
                    ws.getWsFgDefaultGravita().setAmmissibile();
                    break;

                default://-->        ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $ RC=$
                    //-->        SQLCODE=$
                    // COB_CODE: MOVE WK-PGM
                    //             TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'S1200-GESTIONE-MOV-FUTURI-DEF'
                    //             TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("S1200-GESTIONE-MOV-FUTURI-DEF");
                    // COB_CODE: MOVE '005016'
                    //             TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005016");
                    // COB_CODE: STRING WK-TABELLA           ';'
                    //                  IDSO0011-RETURN-CODE ';'
                    //                  IDSO0011-SQLCODE
                    //                  DELIMITED BY SIZE INTO
                    //                  IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkVariabili().getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;
            }
        }
        else {
            //->     GESTIRE ERRORE DISPATCHER
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S1200-GESTIONE-MOV-FUTURI-DEF'
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S1200-GESTIONE-MOV-FUTURI-DEF");
            // COB_CODE: MOVE '005016'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING WK-TABELLA           ';'
            //                  IDSO0011-RETURN-CODE ';'
            //                  IDSO0011-SQLCODE
            //                  DELIMITED BY SIZE INTO
            //                  IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkVariabili().getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: S1210-GESTIONE-MOV-FUTURI-FNZ<br>
	 * <pre>----------------------------------------------------------------*
	 *     GESTIONE MOVIMENTI FUTURI FNZ
	 * ----------------------------------------------------------------*</pre>*/
    private void s1210GestioneMovFuturiFnz() {
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC    TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
        // COB_CODE: SET IDSV0001-ESITO-OK         TO TRUE.
        areaIdsv0001.getEsito().setIdsv0001EsitoOk();
        // COB_CODE: SET WCOM-OVERFLOW-NO          TO TRUE.
        ws.getLccc0007().getFlagOverflow().setNo();
        // COB_CODE: SET IDSI0011-TRATT-SENZA-STOR TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattSenzaStor();
        // COB_CODE: SET IDSI0011-WHERE-CONDITION  TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setWhereCondition();
        // COB_CODE: SET IDSI0011-SELECT           TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        // COB_CODE: MOVE 'LDBSG780'               TO IDSI0011-CODICE-STR-DATO
        //                                            WK-TABELLA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("LDBSG780");
        ws.getWkVariabili().setWkTabella("LDBSG780");
        // COB_CODE: MOVE ZERO                     TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                            IDSI0011-DATA-FINE-EFFETTO
        //                                            IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        // COB_CODE: MOVE SPACES                   TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //                                         TO L05-COD-COMP-ANIA.
        ws.getAmmbFunzFunz().setCodCompAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO  TO L05-TP-MOVI-ESEC.
        ws.getAmmbFunzFunz().setTpMoviEsec(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimento());
        // COB_CODE: MOVE ZEROES                   TO L05-TP-MOVI-RIFTO.
        ws.getAmmbFunzFunz().setTpMoviRifto(0);
        // COB_CODE: MOVE 'P'                      TO L05-FL-POLI-IFP.
        ws.getAmmbFunzFunz().setFlPoliIfpFormatted("P");
        // COB_CODE: MOVE 'BL'                     TO LDBVG781-GRAV-FUNZ-FUNZ-1.
        ws.getLdbvg781().setGravFunzFunz1("BL");
        // COB_CODE: MOVE 'AM'                     TO LDBVG781-GRAV-FUNZ-FUNZ-2.
        ws.getLdbvg781().setGravFunzFunz2("AM");
        // COB_CODE: MOVE LDBVG781                 TO IDSI0011-BUFFER-WHERE-COND.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferWhereCond(ws.getLdbvg781().getLdbvg781Formatted());
        // COB_CODE: MOVE AMMB-FUNZ-FUNZ           TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getAmmbFunzFunz().getAmmbFunzFunzFormatted());
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX.
        callDispatcher();
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //                   END-EVALUATE
        //                ELSE
        //           *->     GESTIRE ERRORE DISPATCHER
        //                      THRU EX-S0300
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            // COB_CODE:         EVALUATE TRUE
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //           *-->        OPERAZIONE ESEGUITA CORRETTAMENTE
            //                            END-EVALUATE
            //                       WHEN IDSO0011-NOT-FOUND
            //           *-->        CHIAVE NON TROVATA
            //                               THRU EX-S1200
            //                       WHEN OTHER
            //           *-->        ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $ RC=$
            //           *-->        SQLCODE=$
            //                               THRU EX-S0300
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://-->        OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: MOVE IDSO0011-BUFFER-DATI
                    //             TO AMMB-FUNZ-FUNZ
                    ws.getAmmbFunzFunz().setAmmbFunzFunzFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    // COB_CODE: MOVE L05-GRAV-FUNZ-FUNZ        TO WS-TP-ANNULLO
                    ws.getWsTpAnnullo().setWsTpAnnullo(ws.getAmmbFunzFunz().getGravFunzFunz());
                    // COB_CODE: EVALUATE TRUE
                    //               WHEN TA-BLOCCANTE
                    //                  SET WS-DEF-BLOCCANTE    TO TRUE
                    //               WHEN TA-AMMISSIBILE
                    //                  SET WS-DEF-AMMISSIBILE  TO TRUE
                    //           END-EVALUATE
                    switch (ws.getWsTpAnnullo().getWsTpAnnullo()) {

                        case WsTpAnnullo.BLOCCANTE:// COB_CODE: SET WS-DEF-BLOCCANTE    TO TRUE
                            ws.getWsFgDefaultGravita().setBloccante();
                            break;

                        case WsTpAnnullo.AMMISSIBILE:// COB_CODE: SET WS-DEF-AMMISSIBILE  TO TRUE
                            ws.getWsFgDefaultGravita().setAmmissibile();
                            break;

                        default:break;
                    }
                    break;

                case Idso0011SqlcodeSigned.NOT_FOUND://-->        CHIAVE NON TROVATA
                    // COB_CODE: PERFORM S1200-GESTIONE-MOV-FUTURI-DEF
                    //              THRU EX-S1200
                    s1200GestioneMovFuturiDef();
                    break;

                default://-->        ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $ RC=$
                    //-->        SQLCODE=$
                    // COB_CODE: MOVE WK-PGM
                    //             TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'S1210-GESTIONE-MOV-FUTURI-FNZ'
                    //             TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("S1210-GESTIONE-MOV-FUTURI-FNZ");
                    // COB_CODE: MOVE '005016'
                    //             TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005016");
                    // COB_CODE: STRING WK-TABELLA           ';'
                    //                  IDSO0011-RETURN-CODE ';'
                    //                  IDSO0011-SQLCODE
                    //                  DELIMITED BY SIZE INTO
                    //                  IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkVariabili().getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;
            }
        }
        else {
            //->     GESTIRE ERRORE DISPATCHER
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S1210-GESTIONE-MOV-FUTURI-FNZ'
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S1210-GESTIONE-MOV-FUTURI-FNZ");
            // COB_CODE: MOVE '005016'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING WK-TABELLA           ';'
            //                  IDSO0011-RETURN-CODE ';'
            //                  IDSO0011-SQLCODE
            //                  DELIMITED BY SIZE INTO
            //                  IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkVariabili().getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: S1300-GESTIONE-MOV-FUTURI<br>
	 * <pre>----------------------------------------------------------------*
	 *     GESTIONE MOVIMENTI FUTURI
	 * ----------------------------------------------------------------*</pre>*/
    private void s1300GestioneMovFuturi() {
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC    TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
        // COB_CODE: SET IDSV0001-ESITO-OK         TO TRUE.
        areaIdsv0001.getEsito().setIdsv0001EsitoOk();
        // COB_CODE: SET WCOM-OVERFLOW-NO          TO TRUE.
        ws.getLccc0007().getFlagOverflow().setNo();
        // COB_CODE: SET IDSI0011-TRATT-SENZA-STOR TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattSenzaStor();
        // COB_CODE: SET IDSI0011-WHERE-CONDITION  TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setWhereCondition();
        // COB_CODE: SET IDSI0011-SELECT           TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        // COB_CODE: MOVE 'LDBSG780'               TO IDSI0011-CODICE-STR-DATO
        //                                            WK-TABELLA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("LDBSG780");
        ws.getWkVariabili().setWkTabella("LDBSG780");
        // COB_CODE: MOVE ZERO                     TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                            IDSI0011-DATA-FINE-EFFETTO
        //                                            IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        // COB_CODE: MOVE SPACES                   TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //                                         TO L05-COD-COMP-ANIA.
        ws.getAmmbFunzFunz().setCodCompAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO  TO L05-TP-MOVI-ESEC.
        ws.getAmmbFunzFunz().setTpMoviEsec(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimento());
        // COB_CODE: MOVE WMOV-TP-MOVI(IX-TAB-MOV) TO L05-TP-MOVI-RIFTO.
        ws.getAmmbFunzFunz().setTpMoviRifto(ws.getWmovTabMovi(ws.getIxTabMov()).getLccvmov1().getDati().getWmovTpMovi().getWmovTpMovi());
        // COB_CODE: MOVE 'G'                      TO L05-FL-POLI-IFP.
        ws.getAmmbFunzFunz().setFlPoliIfpFormatted("G");
        // COB_CODE: MOVE 'BL'                     TO LDBVG781-GRAV-FUNZ-FUNZ-1.
        ws.getLdbvg781().setGravFunzFunz1("BL");
        // COB_CODE: MOVE 'AM'                     TO LDBVG781-GRAV-FUNZ-FUNZ-2.
        ws.getLdbvg781().setGravFunzFunz2("AM");
        // COB_CODE: MOVE LDBVG781                 TO IDSI0011-BUFFER-WHERE-COND.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferWhereCond(ws.getLdbvg781().getLdbvg781Formatted());
        // COB_CODE: MOVE AMMB-FUNZ-FUNZ           TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getAmmbFunzFunz().getAmmbFunzFunzFormatted());
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX.
        callDispatcher();
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //                   END-EVALUATE
        //                ELSE
        //           *->     GESTIRE ERRORE DISPATCHER
        //                      THRU EX-S0300
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            // COB_CODE:         EVALUATE TRUE
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //           *-->        OPERAZIONE ESEGUITA CORRETTAMENTE
            //                               THRU EX-S1350
            //                       WHEN IDSO0011-NOT-FOUND
            //           *-->        CHIAVE NON TROVATA
            //                               THRU EX-S1350
            //                       WHEN OTHER
            //           *-->        ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $ RC=$
            //           *-->        SQLCODE=$
            //                               THRU EX-S0300
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://-->        OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: MOVE IDSO0011-BUFFER-DATI
                    //             TO AMMB-FUNZ-FUNZ
                    ws.getAmmbFunzFunz().setAmmbFunzFunzFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    // COB_CODE: MOVE L05-GRAV-FUNZ-FUNZ
                    //             TO WS-TP-GRAVITA-MOVIMENTO
                    ws.getWkVariabili().setWsTpGravitaMovimento(ws.getAmmbFunzFunz().getGravFunzFunz());
                    // COB_CODE: PERFORM S1350-VALORIZZA-PAG
                    //              THRU EX-S1350
                    s1350ValorizzaPag();
                    break;

                case Idso0011SqlcodeSigned.NOT_FOUND://-->        CHIAVE NON TROVATA
                    // COB_CODE: EVALUATE TRUE
                    //               WHEN WS-DEF-BLOCCANTE
                    //                  SET TA-BLOCCANTE    TO TRUE
                    //               WHEN WS-DEF-AMMISSIBILE
                    //                  SET TA-AMMISSIBILE  TO TRUE
                    //           END-EVALUATE
                    switch (ws.getWsFgDefaultGravita().getWsFgDefaultGravita()) {

                        case WsFgDefaultGravita.BLOCCANTE:// COB_CODE: SET TA-BLOCCANTE    TO TRUE
                            ws.getWsTpAnnullo().setBloccante();
                            break;

                        case WsFgDefaultGravita.AMMISSIBILE:// COB_CODE: SET TA-AMMISSIBILE  TO TRUE
                            ws.getWsTpAnnullo().setAmmissibile();
                            break;

                        default:break;
                    }
                    // COB_CODE: MOVE WS-TP-ANNULLO
                    //             TO WS-TP-GRAVITA-MOVIMENTO
                    ws.getWkVariabili().setWsTpGravitaMovimento(ws.getWsTpAnnullo().getWsTpAnnullo());
                    // COB_CODE: PERFORM S1350-VALORIZZA-PAG
                    //              THRU EX-S1350
                    s1350ValorizzaPag();
                    break;

                default://-->        ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $ RC=$
                    //-->        SQLCODE=$
                    // COB_CODE: MOVE WK-PGM
                    //             TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'S1300-GESTIONE-MOV-FUTURI'
                    //             TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("S1300-GESTIONE-MOV-FUTURI");
                    // COB_CODE: MOVE '005016'
                    //             TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005016");
                    // COB_CODE: STRING WK-TABELLA           ';'
                    //                  IDSO0011-RETURN-CODE ';'
                    //                  IDSO0011-SQLCODE
                    //                  DELIMITED BY SIZE INTO
                    //                  IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkVariabili().getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;
            }
        }
        else {
            //->     GESTIRE ERRORE DISPATCHER
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S1300-GESTIONE-MOV-FUTURI'
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S1300-GESTIONE-MOV-FUTURI");
            // COB_CODE: MOVE '005016'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING WK-TABELLA           ';'
            //                  IDSO0011-RETURN-CODE ';'
            //                  IDSO0011-SQLCODE
            //                  DELIMITED BY SIZE INTO
            //                  IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkVariabili().getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: S1310-GESTIONE-MOV-FNZ<br>
	 * <pre>----------------------------------------------------------------*
	 *     GESTIONE MOVIMENTI FUTURI
	 * ----------------------------------------------------------------*</pre>*/
    private void s1310GestioneMovFnz() {
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC    TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
        // COB_CODE: SET IDSV0001-ESITO-OK         TO TRUE.
        areaIdsv0001.getEsito().setIdsv0001EsitoOk();
        // COB_CODE: SET WCOM-OVERFLOW-NO          TO TRUE.
        ws.getLccc0007().getFlagOverflow().setNo();
        // COB_CODE: SET IDSI0011-TRATT-SENZA-STOR TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattSenzaStor();
        // COB_CODE: SET IDSI0011-WHERE-CONDITION  TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setWhereCondition();
        // COB_CODE: SET IDSI0011-SELECT           TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        // COB_CODE: MOVE 'LDBSG780'               TO IDSI0011-CODICE-STR-DATO
        //                                            WK-TABELLA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("LDBSG780");
        ws.getWkVariabili().setWkTabella("LDBSG780");
        // COB_CODE: MOVE ZERO                     TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                            IDSI0011-DATA-FINE-EFFETTO
        //                                            IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        // COB_CODE: MOVE SPACES                   TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //                                         TO L05-COD-COMP-ANIA.
        ws.getAmmbFunzFunz().setCodCompAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO  TO L05-TP-MOVI-ESEC.
        ws.getAmmbFunzFunz().setTpMoviEsec(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimento());
        // COB_CODE: MOVE WMOV-TP-MOVI(IX-TAB-MOV) TO L05-TP-MOVI-RIFTO.
        ws.getAmmbFunzFunz().setTpMoviRifto(ws.getWmovTabMovi(ws.getIxTabMov()).getLccvmov1().getDati().getWmovTpMovi().getWmovTpMovi());
        // COB_CODE: MOVE 'P'                      TO L05-FL-POLI-IFP.
        ws.getAmmbFunzFunz().setFlPoliIfpFormatted("P");
        // COB_CODE: MOVE 'BL'                     TO LDBVG781-GRAV-FUNZ-FUNZ-1.
        ws.getLdbvg781().setGravFunzFunz1("BL");
        // COB_CODE: MOVE 'AM'                     TO LDBVG781-GRAV-FUNZ-FUNZ-2.
        ws.getLdbvg781().setGravFunzFunz2("AM");
        // COB_CODE: MOVE LDBVG781                 TO IDSI0011-BUFFER-WHERE-COND.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferWhereCond(ws.getLdbvg781().getLdbvg781Formatted());
        // COB_CODE: MOVE AMMB-FUNZ-FUNZ           TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getAmmbFunzFunz().getAmmbFunzFunzFormatted());
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX.
        callDispatcher();
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //                   END-EVALUATE
        //                ELSE
        //           *->     GESTIRE ERRORE DISPATCHER
        //                      THRU EX-S0300
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            // COB_CODE:         EVALUATE TRUE
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //           *-->        OPERAZIONE ESEGUITA CORRETTAMENTE
            //                               THRU EX-S1350
            //                       WHEN IDSO0011-NOT-FOUND
            //           *-->        CHIAVE NON TROVATA
            //                               THRU EX-S1300
            //                       WHEN OTHER
            //           *-->        ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $ RC=$
            //           *-->        SQLCODE=$
            //                               THRU EX-S0300
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://-->        OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: MOVE IDSO0011-BUFFER-DATI
                    //             TO AMMB-FUNZ-FUNZ
                    ws.getAmmbFunzFunz().setAmmbFunzFunzFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    // COB_CODE: MOVE L05-GRAV-FUNZ-FUNZ
                    //             TO WS-TP-GRAVITA-MOVIMENTO
                    ws.getWkVariabili().setWsTpGravitaMovimento(ws.getAmmbFunzFunz().getGravFunzFunz());
                    // COB_CODE: PERFORM S1350-VALORIZZA-PAG
                    //              THRU EX-S1350
                    s1350ValorizzaPag();
                    break;

                case Idso0011SqlcodeSigned.NOT_FOUND://-->        CHIAVE NON TROVATA
                    // COB_CODE: PERFORM S1300-GESTIONE-MOV-FUTURI
                    //              THRU EX-S1300
                    s1300GestioneMovFuturi();
                    break;

                default://-->        ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $ RC=$
                    //-->        SQLCODE=$
                    // COB_CODE: MOVE WK-PGM
                    //             TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'S1310-GESTIONE-MOV-FNZ'
                    //             TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("S1310-GESTIONE-MOV-FNZ");
                    // COB_CODE: MOVE '005016'
                    //             TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005016");
                    // COB_CODE: STRING WK-TABELLA           ';'
                    //                  IDSO0011-RETURN-CODE ';'
                    //                  IDSO0011-SQLCODE
                    //                  DELIMITED BY SIZE INTO
                    //                  IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkVariabili().getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;
            }
        }
        else {
            //->     GESTIRE ERRORE DISPATCHER
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S1310-GESTIONE-MOV-FNZ'
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S1310-GESTIONE-MOV-FNZ");
            // COB_CODE: MOVE '005016'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING WK-TABELLA           ';'
            //                  IDSO0011-RETURN-CODE ';'
            //                  IDSO0011-SQLCODE
            //                  DELIMITED BY SIZE INTO
            //                  IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkVariabili().getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: S1350-VALORIZZA-PAG<br>
	 * <pre>----------------------------------------------------------------*
	 *     SALVATAGGIO DEL MOVIMENTO NELL'AREA DI INTERFACCIA
	 * ----------------------------------------------------------------*</pre>*/
    private void s1350ValorizzaPag() {
        // COB_CODE: ADD  1
        //             TO LCCC0023-ELE-MOV-FUTURI-MAX
        areaIoLccs0023.setEleMovFuturiMax(Trunc.toShort(1 + areaIoLccs0023.getEleMovFuturiMax(), 4));
        // COB_CODE: MOVE WMOV-ID-MOVI(IX-TAB-MOV)
        //             TO LCCC0023-ID-MOVI
        //               (LCCC0023-ELE-MOV-FUTURI-MAX)
        areaIoLccs0023.getTabMovFuturi(areaIoLccs0023.getEleMovFuturiMax()).setLccc0023IdMovi(ws.getWmovTabMovi(ws.getIxTabMov()).getLccvmov1().getDati().getWmovIdMovi());
        // COB_CODE: MOVE WMOV-ID-OGG(IX-TAB-MOV)
        //             TO LCCC0023-ID-OGG
        //               (LCCC0023-ELE-MOV-FUTURI-MAX)
        areaIoLccs0023.getTabMovFuturi(areaIoLccs0023.getEleMovFuturiMax()).getLccc0023IdOgg().setLccc0023IdOgg(ws.getWmovTabMovi(ws.getIxTabMov()).getLccvmov1().getDati().getWmovIdOgg().getWmovIdOgg());
        // COB_CODE: IF WMOV-IB-OGG-NULL(IX-TAB-MOV) = HIGH-VALUE
        //                  (LCCC0023-ELE-MOV-FUTURI-MAX)
        //           ELSE
        //                  (LCCC0023-ELE-MOV-FUTURI-MAX)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWmovTabMovi(ws.getIxTabMov()).getLccvmov1().getDati().getWmovIbOgg(), WmovDati.Len.WMOV_IB_OGG)) {
            // COB_CODE: MOVE WMOV-IB-OGG-NULL(IX-TAB-MOV)
            //             TO LCCC0023-IB-OGG-NULL
            //               (LCCC0023-ELE-MOV-FUTURI-MAX)
            areaIoLccs0023.getTabMovFuturi(areaIoLccs0023.getEleMovFuturiMax()).setLccc0023IbOgg(ws.getWmovTabMovi(ws.getIxTabMov()).getLccvmov1().getDati().getWmovIbOgg());
        }
        else {
            // COB_CODE: MOVE WMOV-IB-OGG-NULL(IX-TAB-MOV)
            //             TO LCCC0023-IB-OGG
            //               (LCCC0023-ELE-MOV-FUTURI-MAX)
            areaIoLccs0023.getTabMovFuturi(areaIoLccs0023.getEleMovFuturiMax()).setLccc0023IbOgg(ws.getWmovTabMovi(ws.getIxTabMov()).getLccvmov1().getDati().getWmovIbOgg());
        }
        // COB_CODE: IF WMOV-IB-MOVI-NULL(IX-TAB-MOV) = HIGH-VALUE
        //                  (LCCC0023-ELE-MOV-FUTURI-MAX)
        //           ELSE
        //                  (LCCC0023-ELE-MOV-FUTURI-MAX)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWmovTabMovi(ws.getIxTabMov()).getLccvmov1().getDati().getWmovIbMovi(), WmovDati.Len.WMOV_IB_MOVI)) {
            // COB_CODE: MOVE WMOV-IB-MOVI-NULL(IX-TAB-MOV)
            //             TO LCCC0023-IB-MOVI-NULL
            //               (LCCC0023-ELE-MOV-FUTURI-MAX)
            areaIoLccs0023.getTabMovFuturi(areaIoLccs0023.getEleMovFuturiMax()).setLccc0023IbMovi(ws.getWmovTabMovi(ws.getIxTabMov()).getLccvmov1().getDati().getWmovIbMovi());
        }
        else {
            // COB_CODE: MOVE WMOV-IB-MOVI(IX-TAB-MOV)
            //             TO LCCC0023-IB-MOVI
            //               (LCCC0023-ELE-MOV-FUTURI-MAX)
            areaIoLccs0023.getTabMovFuturi(areaIoLccs0023.getEleMovFuturiMax()).setLccc0023IbMovi(ws.getWmovTabMovi(ws.getIxTabMov()).getLccvmov1().getDati().getWmovIbMovi());
        }
        // COB_CODE: IF WMOV-TP-OGG-NULL(IX-TAB-MOV) = HIGH-VALUE
        //                  (LCCC0023-ELE-MOV-FUTURI-MAX)
        //           ELSE
        //                  (LCCC0023-ELE-MOV-FUTURI-MAX)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWmovTabMovi(ws.getIxTabMov()).getLccvmov1().getDati().getWmovTpOggFormatted())) {
            // COB_CODE: MOVE WMOV-TP-OGG-NULL(IX-TAB-MOV)
            //             TO LCCC0023-TP-OGG-NULL
            //               (LCCC0023-ELE-MOV-FUTURI-MAX)
            areaIoLccs0023.getTabMovFuturi(areaIoLccs0023.getEleMovFuturiMax()).setLccc0023TpOgg(ws.getWmovTabMovi(ws.getIxTabMov()).getLccvmov1().getDati().getWmovTpOgg());
        }
        else {
            // COB_CODE: MOVE WMOV-TP-OGG(IX-TAB-MOV)
            //             TO LCCC0023-TP-OGG
            //               (LCCC0023-ELE-MOV-FUTURI-MAX)
            areaIoLccs0023.getTabMovFuturi(areaIoLccs0023.getEleMovFuturiMax()).setLccc0023TpOgg(ws.getWmovTabMovi(ws.getIxTabMov()).getLccvmov1().getDati().getWmovTpOgg());
        }
        // COB_CODE: IF WMOV-ID-RICH-NULL(IX-TAB-MOV) = HIGH-VALUE
        //                  (LCCC0023-ELE-MOV-FUTURI-MAX)
        //           ELSE
        //                  (LCCC0023-ELE-MOV-FUTURI-MAX)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWmovTabMovi(ws.getIxTabMov()).getLccvmov1().getDati().getWmovIdRich().getWmovIdRichNullFormatted())) {
            // COB_CODE: MOVE WMOV-ID-RICH-NULL(IX-TAB-MOV)
            //             TO LCCC0023-ID-RICH-NULL
            //               (LCCC0023-ELE-MOV-FUTURI-MAX)
            areaIoLccs0023.getTabMovFuturi(areaIoLccs0023.getEleMovFuturiMax()).getLccc0023IdRich().setLccc0023IdRichNull(ws.getWmovTabMovi(ws.getIxTabMov()).getLccvmov1().getDati().getWmovIdRich().getWmovIdRichNull());
        }
        else {
            // COB_CODE: MOVE WMOV-ID-RICH(IX-TAB-MOV)
            //             TO LCCC0023-ID-RICH
            //               (LCCC0023-ELE-MOV-FUTURI-MAX)
            areaIoLccs0023.getTabMovFuturi(areaIoLccs0023.getEleMovFuturiMax()).getLccc0023IdRich().setLccc0023IdRich(ws.getWmovTabMovi(ws.getIxTabMov()).getLccvmov1().getDati().getWmovIdRich().getWmovIdRich());
        }
        // COB_CODE: IF WMOV-TP-MOVI-NULL(IX-TAB-MOV) = HIGH-VALUE
        //                  (LCCC0023-ELE-MOV-FUTURI-MAX)
        //           ELSE
        //                  (LCCC0023-ELE-MOV-FUTURI-MAX)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWmovTabMovi(ws.getIxTabMov()).getLccvmov1().getDati().getWmovTpMovi().getWmovTpMoviNullFormatted())) {
            // COB_CODE: MOVE WMOV-TP-MOVI-NULL(IX-TAB-MOV)
            //             TO LCCC0023-TP-MOVI-NULL
            //               (LCCC0023-ELE-MOV-FUTURI-MAX)
            areaIoLccs0023.getTabMovFuturi(areaIoLccs0023.getEleMovFuturiMax()).getLccc0023TpMovi().setLccc0023TpMoviNull(ws.getWmovTabMovi(ws.getIxTabMov()).getLccvmov1().getDati().getWmovTpMovi().getWmovTpMoviNull());
        }
        else {
            // COB_CODE: MOVE WMOV-TP-MOVI(IX-TAB-MOV)
            //             TO LCCC0023-TP-MOVI
            //               (LCCC0023-ELE-MOV-FUTURI-MAX)
            areaIoLccs0023.getTabMovFuturi(areaIoLccs0023.getEleMovFuturiMax()).getLccc0023TpMovi().setLccc0023TpMovi(ws.getWmovTabMovi(ws.getIxTabMov()).getLccvmov1().getDati().getWmovTpMovi().getWmovTpMovi());
        }
        // COB_CODE: MOVE WMOV-DT-EFF(IX-TAB-MOV)
        //             TO LCCC0023-DT-EFF
        //               (LCCC0023-ELE-MOV-FUTURI-MAX)
        areaIoLccs0023.getTabMovFuturi(areaIoLccs0023.getEleMovFuturiMax()).setLccc0023DtEff(ws.getWmovTabMovi(ws.getIxTabMov()).getLccvmov1().getDati().getWmovDtEff());
        // COB_CODE: IF WMOV-ID-MOVI-ANN-NULL(IX-TAB-MOV) = HIGH-VALUE
        //                  (LCCC0023-ELE-MOV-FUTURI-MAX)
        //           ELSE
        //                  (LCCC0023-ELE-MOV-FUTURI-MAX)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWmovTabMovi(ws.getIxTabMov()).getLccvmov1().getDati().getWmovIdMoviAnn().getWmovIdMoviAnnNullFormatted())) {
            // COB_CODE: MOVE WMOV-ID-MOVI-ANN-NULL(IX-TAB-MOV)
            //             TO LCCC0023-ID-MOVI-ANN-NULL
            //               (LCCC0023-ELE-MOV-FUTURI-MAX)
            areaIoLccs0023.getTabMovFuturi(areaIoLccs0023.getEleMovFuturiMax()).getLccc0023IdMoviAnn().setLccc0023IdMoviAnnNull(ws.getWmovTabMovi(ws.getIxTabMov()).getLccvmov1().getDati().getWmovIdMoviAnn().getWmovIdMoviAnnNull());
        }
        else {
            // COB_CODE: MOVE WMOV-ID-MOVI-ANN(IX-TAB-MOV)
            //             TO LCCC0023-ID-MOVI-ANN
            //               (LCCC0023-ELE-MOV-FUTURI-MAX)
            areaIoLccs0023.getTabMovFuturi(areaIoLccs0023.getEleMovFuturiMax()).getLccc0023IdMoviAnn().setLccc0023IdMoviAnn(ws.getWmovTabMovi(ws.getIxTabMov()).getLccvmov1().getDati().getWmovIdMoviAnn().getWmovIdMoviAnn());
        }
        // COB_CODE: MOVE WMOV-DS-TS-CPTZ(IX-TAB-MOV)
        //             TO LCCC0023-DS-TS-CPTZ
        //               (LCCC0023-ELE-MOV-FUTURI-MAX)
        areaIoLccs0023.getTabMovFuturi(areaIoLccs0023.getEleMovFuturiMax()).setLccc0023DsTsCptz(ws.getWmovTabMovi(ws.getIxTabMov()).getLccvmov1().getDati().getWmovDsTsCptz());
        // COB_CODE: MOVE WS-TP-GRAVITA-MOVIMENTO
        //             TO LCCC0023-TP-GRAVITA-MOVIMENTO
        //               (LCCC0023-ELE-MOV-FUTURI-MAX).
        areaIoLccs0023.getTabMovFuturi(areaIoLccs0023.getEleMovFuturiMax()).setLccc0023TpGravitaMovimento(ws.getWkVariabili().getWsTpGravitaMovimento());
        // COB_CODE: MOVE WS-TP-GRAVITA-MOVIMENTO
        //             TO WS-TP-ANNULLO
        ws.getWsTpAnnullo().setWsTpAnnullo(ws.getWkVariabili().getWsTpGravitaMovimento());
        // COB_CODE:      EVALUATE TRUE
        //                    WHEN TA-BLOCCANTE
        //                       MOVE 'S'    TO LCCC0023-PRE-GRAV-BLOCCANTE
        //                    WHEN TA-AMMISSIBILE
        //           *-->        SE C'E' ALMENO UN MOVIMENTO FUTURO BLOCCANTE, NON
        //           *-->        CONTROLLO L'ANNULLABILITA' IMPLICITA
        //                       END-IF
        //                END-EVALUATE.
        switch (ws.getWsTpAnnullo().getWsTpAnnullo()) {

            case WsTpAnnullo.BLOCCANTE:// COB_CODE: MOVE 'S'    TO LCCC0023-PRE-GRAV-BLOCCANTE
                areaIoLccs0023.setPreGravBloccanteFormatted("S");
                break;

            case WsTpAnnullo.AMMISSIBILE://-->        SE C'E' ALMENO UN MOVIMENTO FUTURO BLOCCANTE, NON
                //-->        CONTROLLO L'ANNULLABILITA' IMPLICITA
                // COB_CODE: IF LCCC0023-PRE-GRAV-BLOCCANTE = 'N'
                //              MOVE 'S' TO LCCC0023-PRE-GRAV-ANNULLABILE
                //           END-IF
                if (areaIoLccs0023.getPreGravBloccante() == 'N') {
                    // COB_CODE: MOVE 'S' TO LCCC0023-PRE-GRAV-ANNULLABILE
                    areaIoLccs0023.setPreGravAnnullabileFormatted("S");
                }
                break;

            default:break;
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *     OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    /**Original name: S0300-RICERCA-GRAVITA-ERRORE<br>
	 * <pre>MUOVO L'AREA DEL SERVIZIO NELL'AREA DATI DELL'AREA DI CONTESTO.
	 * ----->VALORIZZO I CAMPI DELLA IDSV0001</pre>*/
    private void s0300RicercaGravitaErrore() {
        Ieas9900 ieas9900 = null;
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR       TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE  'IEAS9900'              TO CALL-PGM
        ws.getIdsv0002().setCallPgm("IEAS9900");
        // COB_CODE: CALL CALL-PGM USING IDSV0001-AREA-CONTESTO
        //                               IEAI9901-AREA
        //                               IEAO9901-AREA
        //             ON EXCEPTION
        //                   THRU EX-S0310-ERRORE-FATALE
        //           END-CALL.
        try {
            ieas9900 = Ieas9900.getInstance();
            ieas9900.run(areaIdsv0001, ws.getIeai9901Area(), ws.getIeao9901Area());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
            areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
            // COB_CODE: PERFORM S0310-ERRORE-FATALE
            //              THRU EX-S0310-ERRORE-FATALE
            s0310ErroreFatale();
        }
        //SE LA CHIAMATA AL SERVIZIO HA ESITO POSITIVO (NESSUN ERRORE DI
        //SISTEMA, VALORIZZO L'AREA DI OUTPUT.
        //INCREMENTO L'INDICE DEGLI ERRORI
        // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
        areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
        //SE L'INDICE E' MINORE DI 10 ...
        // COB_CODE:      IF IDSV0001-MAX-ELE-ERRORI NOT GREATER 10
        //           *SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
        //                   END-IF
        //                ELSE
        //           *IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        //                       TO IDSV0001-DESC-ERRORE-ESTESA
        //                END-IF.
        if (areaIdsv0001.getMaxEleErrori() <= 10) {
            //SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
            // COB_CODE: IF IEAO9901-COD-ERRORE-990 = ZEROES
            //                      EX-S0300-IMPOSTA-ERRORE
            //           ELSE
            //                   IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
            //           END-IF
            if (Characters.EQ_ZERO.test(ws.getIeao9901Area().getCodErrore990Formatted())) {
                // COB_CODE: PERFORM S0300-IMPOSTA-ERRORE THRU
                //                   EX-S0300-IMPOSTA-ERRORE
                s0300ImpostaErrore();
            }
            else {
                // COB_CODE: MOVE 'IEAS9900'
                //             TO IDSV0001-COD-SERVIZIO-BE
                areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
                // COB_CODE: MOVE IEAO9901-LABEL-ERR-990
                //             TO IDSV0001-LABEL-ERR
                areaIdsv0001.getLogErrore().setLabelErr(ws.getIeao9901Area().getLabelErr990());
                // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
                //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
                // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
                areaIdsv0001.getEsito().setIdsv0001EsitoKo();
                // IMPOSTO IL CODICE ERRORE GESTITO ALL'INTERNO DEL PROGRAMMA
                // COB_CODE: MOVE IEAO9901-COD-ERRORE-990
                //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeao9901Area().getCodErrore990Formatted());
                // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
                //             TO IDSV0001-DESC-ERRORE-ESTESA
                //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
            }
        }
        else {
            //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
            // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
            //             TO IDSV0001-LABEL-ERR
            areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
            // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 'IEAS9900'
            //             TO IDSV0001-COD-SERVIZIO-BE
            areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
            // COB_CODE: MOVE 'OVERFLOW IMPAGINAZIONE ERRORI'
            //             TO IDSV0001-DESC-ERRORE-ESTESA
            areaIdsv0001.getLogErrore().setDescErroreEstesa("OVERFLOW IMPAGINAZIONE ERRORI");
        }
    }

    /**Original name: S0300-IMPOSTA-ERRORE<br>
	 * <pre>  se la gravit— di tutti gli errori dell'elaborazione
	 *   h minore di zero ( ad esempio -3) vuol dire che il return-code
	 *   dell'elaborazione complessiva deve essere abbassato da '08' a
	 *   '04'. Per fare cir utilizzo il flag IDSV0001-FORZ-RC-04
	 * IMPOSTO LA GRAVITA'</pre>*/
    private void s0300ImpostaErrore() {
        // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
        // COB_CODE: EVALUATE TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = -3
        //                       IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        //             WHEN  IEAO9901-LIV-GRAVITA = 0 OR 1 OR 2
        //                   SET IDSV0001-FORZ-RC-04-NO  TO TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = 3 OR 4
        //                   SET IDSV0001-ESITO-KO       TO TRUE
        //           END-EVALUATE
        if (ws.getIeao9901Area().getLivGravita() == -3) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-YES TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04Yes();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 2  TO
            //               IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
            areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)2));
        }
        else if (ws.getIeao9901Area().getLivGravita() == 0 || ws.getIeao9901Area().getLivGravita() == 1 || ws.getIeao9901Area().getLivGravita() == 2) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
        }
        else if (ws.getIeao9901Area().getLivGravita() == 3 || ws.getIeao9901Area().getLivGravita() == 4) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        }
        // IMPOSTO IL CODICE ERRORE
        // COB_CODE: MOVE IEAI9901-COD-ERRORE
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeai9901Area().getCodErroreFormatted());
        // SE IL LIVELLO DI GRAVITA' RESTITUITO DALLO IAS9900 E' MAGGIORE
        // DI 2 (ERRORE BLOCCANTE)...IMPOSTO L'ESITO E I DATI DI CONTESTO
        //RELATIVI ALL'ERRORE BLOCCANTE
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE
        //             TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR
        //             TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
        //             TO IDSV0001-DESC-ERRORE-ESTESA
        //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
        // COB_CODE: MOVE SPACES          TO IEAI9901-COD-SERVIZIO-BE
        //                                  IEAI9901-LABEL-ERR
        //                                   IEAI9901-PARAMETRI-ERR.
        ws.getIeai9901Area().setCodServizioBe("");
        ws.getIeai9901Area().setLabelErr("");
        ws.getIeai9901Area().setParametriErr("");
        // COB_CODE: MOVE ZEROES          TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErrore(0);
    }

    /**Original name: S0310-ERRORE-FATALE<br>
	 * <pre>IMPOSTO LA GRAVITA' ERRORE</pre>*/
    private void s0310ErroreFatale() {
        // COB_CODE: MOVE  3
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)3));
        // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
        areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        //IMPOSTO IL CODICE ERRORE (ERRORE FISSO)
        // COB_CODE: MOVE 900001
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErrore(900001);
        //IMPOSTO NOME DEL MODULO
        // COB_CODE: MOVE 'IEAS9900'               TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
        //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
        //              TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
        // COB_CODE: MOVE  'ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900'
        //              TO IDSV0001-DESC-ERRORE-ESTESA
        //                 IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
    }

    /**Original name: CALL-DISPATCHER<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES CALL DISPATCHER
	 * ----------------------------------------------------------------*
	 * *****************************************************************
	 *    CALL DISPATCHER
	 * *****************************************************************</pre>*/
    private void callDispatcher() {
        Idss0010 idss0010 = null;
        // COB_CODE: MOVE IDSV0001-MODALITA-ESECUTIVA
        //                              TO IDSI0011-MODALITA-ESECUTIVA
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011ModalitaEsecutiva().setIdsi0011ModalitaEsecutiva(areaIdsv0001.getAreaComune().getModalitaEsecutiva().getModalitaEsecutiva());
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //                              TO IDSI0011-CODICE-COMPAGNIA-ANIA
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceCompagniaAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: MOVE IDSV0001-COD-MAIN-BATCH
        //                              TO IDSI0011-COD-MAIN-BATCH
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodMainBatch(areaIdsv0001.getAreaComune().getCodMainBatch());
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO
        //                              TO IDSI0011-TIPO-MOVIMENTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011TipoMovimentoFormatted(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimentoFormatted());
        // COB_CODE: MOVE IDSV0001-SESSIONE
        //                              TO IDSI0011-SESSIONE
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011Sessione(areaIdsv0001.getAreaComune().getSessione());
        // COB_CODE: MOVE IDSV0001-USER-NAME
        //                              TO IDSI0011-USER-NAME
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011UserName(areaIdsv0001.getAreaComune().getUserName());
        // COB_CODE: IF IDSI0011-DATA-INIZIO-EFFETTO = 0
        //              END-IF
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataInizioEffetto() == 0) {
            // COB_CODE: IF IDSV0001-LETT-ULT-IMMAGINE-SI
            //              END-IF
            //           ELSE
            //                TO IDSI0011-DATA-INIZIO-EFFETTO
            //           END-IF
            if (areaIdsv0001.getAreaComune().getLettUltImmagine().isIdsv0001LettUltImmagineSi()) {
                // COB_CODE: IF IDSI0011-SELECT
                //           OR IDSI0011-FETCH-FIRST
                //           OR IDSI0011-FETCH-NEXT
                //                TO IDSI0011-DATA-COMPETENZA
                //           ELSE
                //                TO IDSI0011-DATA-INIZIO-EFFETTO
                //           END-IF
                if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isSelect() || ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchFirst() || ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchNext()) {
                    // COB_CODE: MOVE 99991230
                    //             TO IDSI0011-DATA-INIZIO-EFFETTO
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(99991230);
                    // COB_CODE: MOVE 999912304023595999
                    //             TO IDSI0011-DATA-COMPETENZA
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(999912304023595999L);
                }
                else {
                    // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
                    //             TO IDSI0011-DATA-INIZIO-EFFETTO
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
                }
            }
            else {
                // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
                //             TO IDSI0011-DATA-INIZIO-EFFETTO
                ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
            }
        }
        // COB_CODE: IF IDSI0011-DATA-FINE-EFFETTO = 0
        //              MOVE 99991231   TO IDSI0011-DATA-FINE-EFFETTO
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataFineEffetto() == 0) {
            // COB_CODE: MOVE 99991231   TO IDSI0011-DATA-FINE-EFFETTO
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(99991231);
        }
        // COB_CODE: IF IDSI0011-DATA-COMPETENZA = 0
        //                              TO IDSI0011-DATA-COMPETENZA
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataCompetenza() == 0) {
            // COB_CODE: MOVE IDSV0001-DATA-COMPETENZA
            //                           TO IDSI0011-DATA-COMPETENZA
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(areaIdsv0001.getAreaComune().getIdsv0001DataCompetenza());
        }
        // COB_CODE: MOVE IDSV0001-DATA-COMP-AGG-STOR
        //                              TO IDSI0011-DATA-COMP-AGG-STOR
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompAggStor(areaIdsv0001.getAreaComune().getIdsv0001DataCompAggStor());
        // COB_CODE: IF IDSI0011-TRATT-DEFAULT
        //                              TO IDSI0011-TRATTAMENTO-STORICITA
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().isTrattDefault()) {
            // COB_CODE: MOVE IDSV0001-TRATTAMENTO-STORICITA
            //                           TO IDSI0011-TRATTAMENTO-STORICITA
            ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattamentoStoricita(areaIdsv0001.getAreaComune().getTrattamentoStoricita().getTrattamentoStoricita());
        }
        // COB_CODE: MOVE IDSV0001-FORMATO-DATA-DB
        //                              TO IDSI0011-FORMATO-DATA-DB
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011FormatoDataDb().setIdsi0011FormatoDataDb(areaIdsv0001.getAreaComune().getFormatoDataDb().getFormatoDataDb());
        // COB_CODE: MOVE IDSV0001-LIVELLO-DEBUG
        //                              TO IDSI0011-LIVELLO-DEBUG
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloDebug().setIdsi0011LivelloDebugFormatted(areaIdsv0001.getAreaComune().getLivelloDebug().getIdsi0011LivelloDebugFormatted());
        // COB_CODE: MOVE 0             TO IDSI0011-ID-MOVI-ANNULLATO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011IdMoviAnnullato(0);
        // COB_CODE: SET IDSI0011-PTF-NEWLIFE          TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011IdentitaChiamante().setPtfNewlife();
        // COB_CODE: SET IDSV0012-STATUS-SHARED-MEM-KO TO TRUE
        ws.getIdsv00122().getStatusSharedMemory().setKo();
        // COB_CODE: MOVE 'IDSS0010'             TO IDSI0011-PGM
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011Pgm("IDSS0010");
        //     SET WS-ADDRESS-DIS TO ADDRESS OF IN-OUT-IDSS0010.
        // COB_CODE: CALL IDSI0011-PGM           USING IDSI0011-AREA
        //                                             IDSO0011-AREA.
        idss0010 = Idss0010.getInstance();
        idss0010.run(ws.getDispatcherVariables(), ws.getDispatcherVariables().getIdso0011Area());
        // COB_CODE: MOVE IDSO0011-SQLCODE-SIGNED
        //                                  TO IDSO0011-SQLCODE.
        ws.getDispatcherVariables().getIdso0011Area().setSqlcode(ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned());
    }

    public void initIxIndici() {
        ws.setIxTabMov(((short)0));
    }

    public void initWkVariabili() {
        ws.getWkVariabili().setWkTabella("");
        ws.getWkVariabili().setWsTpGravitaMovimento("");
        ws.getWkVariabili().setWkDtEff(0);
    }

    public void initPoli() {
        ws.getPoli().setPolIdPoli(0);
        ws.getPoli().setPolIdMoviCrz(0);
        ws.getPoli().getPolIdMoviChiu().setPolIdMoviChiu(0);
        ws.getPoli().setPolIbOgg("");
        ws.getPoli().setPolIbProp("");
        ws.getPoli().getPolDtProp().setPolDtProp(0);
        ws.getPoli().setPolDtIniEff(0);
        ws.getPoli().setPolDtEndEff(0);
        ws.getPoli().setPolCodCompAnia(0);
        ws.getPoli().setPolDtDecor(0);
        ws.getPoli().setPolDtEmis(0);
        ws.getPoli().setPolTpPoli("");
        ws.getPoli().getPolDurAa().setPolDurAa(0);
        ws.getPoli().getPolDurMm().setPolDurMm(0);
        ws.getPoli().getPolDtScad().setPolDtScad(0);
        ws.getPoli().setPolCodProd("");
        ws.getPoli().setPolDtIniVldtProd(0);
        ws.getPoli().setPolCodConv("");
        ws.getPoli().setPolCodRamo("");
        ws.getPoli().getPolDtIniVldtConv().setPolDtIniVldtConv(0);
        ws.getPoli().getPolDtApplzConv().setPolDtApplzConv(0);
        ws.getPoli().setPolTpFrmAssva("");
        ws.getPoli().setPolTpRgmFisc("");
        ws.getPoli().setPolFlEstas(Types.SPACE_CHAR);
        ws.getPoli().setPolFlRshComun(Types.SPACE_CHAR);
        ws.getPoli().setPolFlRshComunCond(Types.SPACE_CHAR);
        ws.getPoli().setPolTpLivGenzTit("");
        ws.getPoli().setPolFlCopFinanz(Types.SPACE_CHAR);
        ws.getPoli().setPolTpApplzDir("");
        ws.getPoli().getPolSpeMed().setPolSpeMed(new AfDecimal(0, 15, 3));
        ws.getPoli().getPolDirEmis().setPolDirEmis(new AfDecimal(0, 15, 3));
        ws.getPoli().getPolDir1oVers().setPolDir1oVers(new AfDecimal(0, 15, 3));
        ws.getPoli().getPolDirVersAgg().setPolDirVersAgg(new AfDecimal(0, 15, 3));
        ws.getPoli().setPolCodDvs("");
        ws.getPoli().setPolFlFntAz(Types.SPACE_CHAR);
        ws.getPoli().setPolFlFntAder(Types.SPACE_CHAR);
        ws.getPoli().setPolFlFntTfr(Types.SPACE_CHAR);
        ws.getPoli().setPolFlFntVolo(Types.SPACE_CHAR);
        ws.getPoli().setPolTpOpzAScad("");
        ws.getPoli().getPolAaDiffProrDflt().setPolAaDiffProrDflt(0);
        ws.getPoli().setPolFlVerProd("");
        ws.getPoli().getPolDurGg().setPolDurGg(0);
        ws.getPoli().getPolDirQuiet().setPolDirQuiet(new AfDecimal(0, 15, 3));
        ws.getPoli().setPolTpPtfEstno("");
        ws.getPoli().setPolFlCumPreCntr(Types.SPACE_CHAR);
        ws.getPoli().setPolFlAmmbMovi(Types.SPACE_CHAR);
        ws.getPoli().setPolConvGeco("");
        ws.getPoli().setPolDsRiga(0);
        ws.getPoli().setPolDsOperSql(Types.SPACE_CHAR);
        ws.getPoli().setPolDsVer(0);
        ws.getPoli().setPolDsTsIniCptz(0);
        ws.getPoli().setPolDsTsEndCptz(0);
        ws.getPoli().setPolDsUtente("");
        ws.getPoli().setPolDsStatoElab(Types.SPACE_CHAR);
        ws.getPoli().setPolFlScudoFisc(Types.SPACE_CHAR);
        ws.getPoli().setPolFlTrasfe(Types.SPACE_CHAR);
        ws.getPoli().setPolFlTfrStrc(Types.SPACE_CHAR);
        ws.getPoli().getPolDtPresc().setPolDtPresc(0);
        ws.getPoli().setPolCodConvAgg("");
        ws.getPoli().setPolSubcatProd("");
        ws.getPoli().setPolFlQuestAdegzAss(Types.SPACE_CHAR);
        ws.getPoli().setPolCodTpa("");
        ws.getPoli().getPolIdAccComm().setPolIdAccComm(0);
        ws.getPoli().setPolFlPoliCpiPr(Types.SPACE_CHAR);
        ws.getPoli().setPolFlPoliBundling(Types.SPACE_CHAR);
        ws.getPoli().setPolIndPoliPrinColl(Types.SPACE_CHAR);
        ws.getPoli().setPolFlVndBundle(Types.SPACE_CHAR);
        ws.getPoli().setPolIbBs("");
        ws.getPoli().setPolFlPoliIfp(Types.SPACE_CHAR);
    }

    public void initAdes() {
        ws.getAdes().setAdeIdAdes(0);
        ws.getAdes().setAdeIdPoli(0);
        ws.getAdes().setAdeIdMoviCrz(0);
        ws.getAdes().getAdeIdMoviChiu().setAdeIdMoviChiu(0);
        ws.getAdes().setAdeDtIniEff(0);
        ws.getAdes().setAdeDtEndEff(0);
        ws.getAdes().setAdeIbPrev("");
        ws.getAdes().setAdeIbOgg("");
        ws.getAdes().setAdeCodCompAnia(0);
        ws.getAdes().getAdeDtDecor().setAdeDtDecor(0);
        ws.getAdes().getAdeDtScad().setAdeDtScad(0);
        ws.getAdes().getAdeEtaAScad().setAdeEtaAScad(0);
        ws.getAdes().getAdeDurAa().setAdeDurAa(0);
        ws.getAdes().getAdeDurMm().setAdeDurMm(0);
        ws.getAdes().getAdeDurGg().setAdeDurGg(0);
        ws.getAdes().setAdeTpRgmFisc("");
        ws.getAdes().setAdeTpRiat("");
        ws.getAdes().setAdeTpModPagTit("");
        ws.getAdes().setAdeTpIas("");
        ws.getAdes().getAdeDtVarzTpIas().setAdeDtVarzTpIas(0);
        ws.getAdes().getAdePreNetInd().setAdePreNetInd(new AfDecimal(0, 15, 3));
        ws.getAdes().getAdePreLrdInd().setAdePreLrdInd(new AfDecimal(0, 15, 3));
        ws.getAdes().getAdeRatLrdInd().setAdeRatLrdInd(new AfDecimal(0, 15, 3));
        ws.getAdes().getAdePrstzIniInd().setAdePrstzIniInd(new AfDecimal(0, 15, 3));
        ws.getAdes().setAdeFlCoincAssto(Types.SPACE_CHAR);
        ws.getAdes().setAdeIbDflt("");
        ws.getAdes().setAdeModCalc("");
        ws.getAdes().setAdeTpFntCnbtva("");
        ws.getAdes().getAdeImpAz().setAdeImpAz(new AfDecimal(0, 15, 3));
        ws.getAdes().getAdeImpAder().setAdeImpAder(new AfDecimal(0, 15, 3));
        ws.getAdes().getAdeImpTfr().setAdeImpTfr(new AfDecimal(0, 15, 3));
        ws.getAdes().getAdeImpVolo().setAdeImpVolo(new AfDecimal(0, 15, 3));
        ws.getAdes().getAdePcAz().setAdePcAz(new AfDecimal(0, 6, 3));
        ws.getAdes().getAdePcAder().setAdePcAder(new AfDecimal(0, 6, 3));
        ws.getAdes().getAdePcTfr().setAdePcTfr(new AfDecimal(0, 6, 3));
        ws.getAdes().getAdePcVolo().setAdePcVolo(new AfDecimal(0, 6, 3));
        ws.getAdes().getAdeDtNovaRgmFisc().setAdeDtNovaRgmFisc(0);
        ws.getAdes().setAdeFlAttiv(Types.SPACE_CHAR);
        ws.getAdes().getAdeImpRecRitVis().setAdeImpRecRitVis(new AfDecimal(0, 15, 3));
        ws.getAdes().getAdeImpRecRitAcc().setAdeImpRecRitAcc(new AfDecimal(0, 15, 3));
        ws.getAdes().setAdeFlVarzStatTbgc(Types.SPACE_CHAR);
        ws.getAdes().setAdeFlProvzaMigraz(Types.SPACE_CHAR);
        ws.getAdes().getAdeImpbVisDaRec().setAdeImpbVisDaRec(new AfDecimal(0, 15, 3));
        ws.getAdes().getAdeDtDecorPrestBan().setAdeDtDecorPrestBan(0);
        ws.getAdes().getAdeDtEffVarzStatT().setAdeDtEffVarzStatT(0);
        ws.getAdes().setAdeDsRiga(0);
        ws.getAdes().setAdeDsOperSql(Types.SPACE_CHAR);
        ws.getAdes().setAdeDsVer(0);
        ws.getAdes().setAdeDsTsIniCptz(0);
        ws.getAdes().setAdeDsTsEndCptz(0);
        ws.getAdes().setAdeDsUtente("");
        ws.getAdes().setAdeDsStatoElab(Types.SPACE_CHAR);
        ws.getAdes().getAdeCumCnbtCap().setAdeCumCnbtCap(new AfDecimal(0, 15, 3));
        ws.getAdes().getAdeImpGarCnbt().setAdeImpGarCnbt(new AfDecimal(0, 15, 3));
        ws.getAdes().getAdeDtUltConsCnbt().setAdeDtUltConsCnbt(0);
        ws.getAdes().setAdeIdenIscFnd("");
        ws.getAdes().getAdeNumRatPian().setAdeNumRatPian(new AfDecimal(0, 12, 5));
        ws.getAdes().getAdeDtPresc().setAdeDtPresc(0);
        ws.getAdes().setAdeConcsPrest(Types.SPACE_CHAR);
    }

    public void initTrchDiGar() {
        ws.getTrchDiGar().setTgaIdTrchDiGar(0);
        ws.getTrchDiGar().setTgaIdGar(0);
        ws.getTrchDiGar().setTgaIdAdes(0);
        ws.getTrchDiGar().setTgaIdPoli(0);
        ws.getTrchDiGar().setTgaIdMoviCrz(0);
        ws.getTrchDiGar().getTgaIdMoviChiu().setTgaIdMoviChiu(0);
        ws.getTrchDiGar().setTgaDtIniEff(0);
        ws.getTrchDiGar().setTgaDtEndEff(0);
        ws.getTrchDiGar().setTgaCodCompAnia(0);
        ws.getTrchDiGar().setTgaDtDecor(0);
        ws.getTrchDiGar().getTgaDtScad().setTgaDtScad(0);
        ws.getTrchDiGar().setTgaIbOgg("");
        ws.getTrchDiGar().setTgaTpRgmFisc("");
        ws.getTrchDiGar().getTgaDtEmis().setTgaDtEmis(0);
        ws.getTrchDiGar().setTgaTpTrch("");
        ws.getTrchDiGar().getTgaDurAa().setTgaDurAa(0);
        ws.getTrchDiGar().getTgaDurMm().setTgaDurMm(0);
        ws.getTrchDiGar().getTgaDurGg().setTgaDurGg(0);
        ws.getTrchDiGar().getTgaPreCasoMor().setTgaPreCasoMor(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaPcIntrRiat().setTgaPcIntrRiat(new AfDecimal(0, 6, 3));
        ws.getTrchDiGar().getTgaImpBnsAntic().setTgaImpBnsAntic(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaPreIniNet().setTgaPreIniNet(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaPrePpIni().setTgaPrePpIni(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaPrePpUlt().setTgaPrePpUlt(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaPreTariIni().setTgaPreTariIni(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaPreTariUlt().setTgaPreTariUlt(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaPreInvrioIni().setTgaPreInvrioIni(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaPreInvrioUlt().setTgaPreInvrioUlt(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaPreRivto().setTgaPreRivto(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaImpSoprProf().setTgaImpSoprProf(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaImpSoprSan().setTgaImpSoprSan(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaImpSoprSpo().setTgaImpSoprSpo(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaImpSoprTec().setTgaImpSoprTec(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaImpAltSopr().setTgaImpAltSopr(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaPreStab().setTgaPreStab(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaDtEffStab().setTgaDtEffStab(0);
        ws.getTrchDiGar().getTgaTsRivalFis().setTgaTsRivalFis(new AfDecimal(0, 14, 9));
        ws.getTrchDiGar().getTgaTsRivalIndiciz().setTgaTsRivalIndiciz(new AfDecimal(0, 14, 9));
        ws.getTrchDiGar().getTgaOldTsTec().setTgaOldTsTec(new AfDecimal(0, 14, 9));
        ws.getTrchDiGar().getTgaRatLrd().setTgaRatLrd(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaPreLrd().setTgaPreLrd(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaPrstzIni().setTgaPrstzIni(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaPrstzUlt().setTgaPrstzUlt(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaCptInOpzRivto().setTgaCptInOpzRivto(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaPrstzIniStab().setTgaPrstzIniStab(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaCptRshMor().setTgaCptRshMor(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaPrstzRidIni().setTgaPrstzRidIni(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().setTgaFlCarCont(Types.SPACE_CHAR);
        ws.getTrchDiGar().getTgaBnsGiaLiqto().setTgaBnsGiaLiqto(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaImpBns().setTgaImpBns(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().setTgaCodDvs("");
        ws.getTrchDiGar().getTgaPrstzIniNewfis().setTgaPrstzIniNewfis(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaImpScon().setTgaImpScon(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaAlqScon().setTgaAlqScon(new AfDecimal(0, 6, 3));
        ws.getTrchDiGar().getTgaImpCarAcq().setTgaImpCarAcq(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaImpCarInc().setTgaImpCarInc(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaImpCarGest().setTgaImpCarGest(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaEtaAa1oAssto().setTgaEtaAa1oAssto(((short)0));
        ws.getTrchDiGar().getTgaEtaMm1oAssto().setTgaEtaMm1oAssto(((short)0));
        ws.getTrchDiGar().getTgaEtaAa2oAssto().setTgaEtaAa2oAssto(((short)0));
        ws.getTrchDiGar().getTgaEtaMm2oAssto().setTgaEtaMm2oAssto(((short)0));
        ws.getTrchDiGar().getTgaEtaAa3oAssto().setTgaEtaAa3oAssto(((short)0));
        ws.getTrchDiGar().getTgaEtaMm3oAssto().setTgaEtaMm3oAssto(((short)0));
        ws.getTrchDiGar().getTgaRendtoLrd().setTgaRendtoLrd(new AfDecimal(0, 14, 9));
        ws.getTrchDiGar().getTgaPcRetr().setTgaPcRetr(new AfDecimal(0, 6, 3));
        ws.getTrchDiGar().getTgaRendtoRetr().setTgaRendtoRetr(new AfDecimal(0, 14, 9));
        ws.getTrchDiGar().getTgaMinGarto().setTgaMinGarto(new AfDecimal(0, 14, 9));
        ws.getTrchDiGar().getTgaMinTrnut().setTgaMinTrnut(new AfDecimal(0, 14, 9));
        ws.getTrchDiGar().getTgaPreAttDiTrch().setTgaPreAttDiTrch(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaMatuEnd2000().setTgaMatuEnd2000(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaAbbTotIni().setTgaAbbTotIni(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaAbbTotUlt().setTgaAbbTotUlt(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaAbbAnnuUlt().setTgaAbbAnnuUlt(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaDurAbb().setTgaDurAbb(0);
        ws.getTrchDiGar().setTgaTpAdegAbb(Types.SPACE_CHAR);
        ws.getTrchDiGar().setTgaModCalc("");
        ws.getTrchDiGar().getTgaImpAz().setTgaImpAz(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaImpAder().setTgaImpAder(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaImpTfr().setTgaImpTfr(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaImpVolo().setTgaImpVolo(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaVisEnd2000().setTgaVisEnd2000(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaDtVldtProd().setTgaDtVldtProd(0);
        ws.getTrchDiGar().getTgaDtIniValTar().setTgaDtIniValTar(0);
        ws.getTrchDiGar().getTgaImpbVisEnd2000().setTgaImpbVisEnd2000(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaRenIniTsTec0().setTgaRenIniTsTec0(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaPcRipPre().setTgaPcRipPre(new AfDecimal(0, 6, 3));
        ws.getTrchDiGar().setTgaFlImportiForz(Types.SPACE_CHAR);
        ws.getTrchDiGar().getTgaPrstzIniNforz().setTgaPrstzIniNforz(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaVisEnd2000Nforz().setTgaVisEnd2000Nforz(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaIntrMora().setTgaIntrMora(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaManfeeAntic().setTgaManfeeAntic(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaManfeeRicor().setTgaManfeeRicor(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaPreUniRivto().setTgaPreUniRivto(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaProv1aaAcq().setTgaProv1aaAcq(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaProv2aaAcq().setTgaProv2aaAcq(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaProvRicor().setTgaProvRicor(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaProvInc().setTgaProvInc(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaAlqProvAcq().setTgaAlqProvAcq(new AfDecimal(0, 6, 3));
        ws.getTrchDiGar().getTgaAlqProvInc().setTgaAlqProvInc(new AfDecimal(0, 6, 3));
        ws.getTrchDiGar().getTgaAlqProvRicor().setTgaAlqProvRicor(new AfDecimal(0, 6, 3));
        ws.getTrchDiGar().getTgaImpbProvAcq().setTgaImpbProvAcq(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaImpbProvInc().setTgaImpbProvInc(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaImpbProvRicor().setTgaImpbProvRicor(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().setTgaFlProvForz(Types.SPACE_CHAR);
        ws.getTrchDiGar().getTgaPrstzAggIni().setTgaPrstzAggIni(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaIncrPre().setTgaIncrPre(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaIncrPrstz().setTgaIncrPrstz(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaDtUltAdegPrePr().setTgaDtUltAdegPrePr(0);
        ws.getTrchDiGar().getTgaPrstzAggUlt().setTgaPrstzAggUlt(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaTsRivalNet().setTgaTsRivalNet(new AfDecimal(0, 14, 9));
        ws.getTrchDiGar().getTgaPrePattuito().setTgaPrePattuito(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().setTgaTpRival("");
        ws.getTrchDiGar().getTgaRisMat().setTgaRisMat(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaCptMinScad().setTgaCptMinScad(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaCommisGest().setTgaCommisGest(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().setTgaTpManfeeAppl("");
        ws.getTrchDiGar().setTgaDsRiga(0);
        ws.getTrchDiGar().setTgaDsOperSql(Types.SPACE_CHAR);
        ws.getTrchDiGar().setTgaDsVer(0);
        ws.getTrchDiGar().setTgaDsTsIniCptz(0);
        ws.getTrchDiGar().setTgaDsTsEndCptz(0);
        ws.getTrchDiGar().setTgaDsUtente("");
        ws.getTrchDiGar().setTgaDsStatoElab(Types.SPACE_CHAR);
        ws.getTrchDiGar().getTgaPcCommisGest().setTgaPcCommisGest(new AfDecimal(0, 6, 3));
        ws.getTrchDiGar().getTgaNumGgRival().setTgaNumGgRival(0);
        ws.getTrchDiGar().getTgaImpTrasfe().setTgaImpTrasfe(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaImpTfrStrc().setTgaImpTfrStrc(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaAcqExp().setTgaAcqExp(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaRemunAss().setTgaRemunAss(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaCommisInter().setTgaCommisInter(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaAlqRemunAss().setTgaAlqRemunAss(new AfDecimal(0, 6, 3));
        ws.getTrchDiGar().getTgaAlqCommisInter().setTgaAlqCommisInter(new AfDecimal(0, 6, 3));
        ws.getTrchDiGar().getTgaImpbRemunAss().setTgaImpbRemunAss(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaImpbCommisInter().setTgaImpbCommisInter(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaCosRunAssva().setTgaCosRunAssva(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaCosRunAssvaIdc().setTgaCosRunAssvaIdc(new AfDecimal(0, 15, 3));
    }

    public void initLdbvg781() {
        ws.getLdbvg781().setGravFunzFunz1("");
        ws.getLdbvg781().setGravFunzFunz2("");
        ws.getLdbvg781().setGravFunzFunz3("");
        ws.getLdbvg781().setGravFunzFunz4("");
        ws.getLdbvg781().setGravFunzFunz5("");
        ws.getLdbvg781().setFlPoliIfp(Types.SPACE_CHAR);
    }
}
