package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.RichDisFndValAstDao;
import it.accenture.jnais.commons.data.to.IRichDisFndValAst;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ldbs5470Data;
import it.accenture.jnais.ws.redefines.RdfCommisGest;
import it.accenture.jnais.ws.redefines.RdfCosRunAssvaIdc;
import it.accenture.jnais.ws.redefines.RdfDtCambioVlt;
import it.accenture.jnais.ws.redefines.RdfDtDis;
import it.accenture.jnais.ws.redefines.RdfDtDisCalc;
import it.accenture.jnais.ws.redefines.RdfIdMoviChiu;
import it.accenture.jnais.ws.redefines.RdfImpMovto;
import it.accenture.jnais.ws.redefines.RdfNumQuo;
import it.accenture.jnais.ws.redefines.RdfNumQuoCdgFnz;
import it.accenture.jnais.ws.redefines.RdfNumQuoCdgtotFnz;
import it.accenture.jnais.ws.redefines.RdfPc;
import it.accenture.jnais.ws.RichDisFnd;

/**Original name: LDBS5470<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  25 NOV 2019.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO AD HOC PER ACCESSO RISORSE DB        *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Ldbs5470 extends Program implements IRichDisFndValAst {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private RichDisFndValAstDao richDisFndValAstDao = new RichDisFndValAstDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Ldbs5470Data ws = new Ldbs5470Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: RICH-DIS-FND
    private RichDisFnd richDisFnd;

    //==== METHODS ====
    /**Original name: PROGRAM_LDBS5470_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, RichDisFnd richDisFnd) {
        this.idsv0003 = idsv0003;
        this.richDisFnd = richDisFnd;
        // COB_CODE: MOVE IDSV0003-BUFFER-WHERE-COND TO LDBV5471.
        ws.setLdbv5471Formatted(this.idsv0003.getBufferWhereCondFormatted());
        // COB_CODE: PERFORM A000-INIZIO              THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                        a200ElaboraWcEff();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                        b200ElaboraWcCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //               SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                        c200ElaboraWcNst();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: MOVE LDBV5471 TO IDSV0003-BUFFER-WHERE-COND.
        this.idsv0003.setBufferWhereCond(ws.getLdbv5471Formatted());
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Ldbs5470 getInstance() {
        return ((Ldbs5470)Programs.getInstance(Ldbs5470.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'LDBS5470'              TO   IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("LDBS5470");
        // COB_CODE: MOVE 'RICH-DIS-FND' TO   IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("RICH-DIS-FND");
        // COB_CODE: MOVE '00'                      TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                    TO   IDSV0003-SQLCODE
        //                                               IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                    TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                               IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-WC-EFF<br>*/
    private void a200ElaboraWcEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-WC-EFF          THRU A210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-WC-EFF          THRU A210-EX
            a210SelectWcEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
            a260OpenCursorWcEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
            a270CloseCursorWcEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
            a280FetchFirstWcEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
            a290FetchNextWcEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B200-ELABORA-WC-CPZ<br>*/
    private void b200ElaboraWcCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
            b210SelectWcCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
            b260OpenCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
            b270CloseCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
            b280FetchFirstWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
            b290FetchNextWcCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: C200-ELABORA-WC-NST<br>*/
    private void c200ElaboraWcNst() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM C210-SELECT-WC-NST          THRU C210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM C210-SELECT-WC-NST          THRU C210-EX
            c210SelectWcNst();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
            c260OpenCursorWcNst();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
            c270CloseCursorWcNst();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
            c280FetchFirstWcNst();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
            c290FetchNextWcNst();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A205-DECLARE-CURSOR-WC-EFF<br>
	 * <pre>----
	 * ----  gestione WC Effetto
	 * ----</pre>*/
    private void a205DeclareCursorWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //              DECLARE C-EFF CURSOR FOR
        //              SELECT
        //                     B.ID_RICH_DIS_FND
        //                    ,B.ID_MOVI_FINRIO
        //                    ,B.ID_MOVI_CRZ
        //                    ,B.ID_MOVI_CHIU
        //                    ,B.DT_INI_EFF
        //                    ,B.DT_END_EFF
        //                    ,B.COD_COMP_ANIA
        //                    ,B.COD_FND
        //                    ,B.NUM_QUO
        //                    ,B.PC
        //                    ,B.IMP_MOVTO
        //                    ,B.DT_DIS
        //                    ,B.COD_TARI
        //                    ,B.TP_STAT
        //                    ,B.TP_MOD_DIS
        //                    ,B.COD_DIV
        //                    ,B.DT_CAMBIO_VLT
        //                    ,B.TP_FND
        //                    ,B.DS_RIGA
        //                    ,B.DS_OPER_SQL
        //                    ,B.DS_VER
        //                    ,B.DS_TS_INI_CPTZ
        //                    ,B.DS_TS_END_CPTZ
        //                    ,B.DS_UTENTE
        //                    ,B.DS_STATO_ELAB
        //                    ,B.DT_DIS_CALC
        //                    ,B.FL_CALC_DIS
        //                    ,B.COMMIS_GEST
        //                    ,B.NUM_QUO_CDG_FNZ
        //                    ,B.NUM_QUO_CDGTOT_FNZ
        //                    ,B.COS_RUN_ASSVA_IDC
        //                    ,B.FL_SWM_BP2S
        //              FROM VAL_AST A,
        //                   RICH_DIS_FND B
        //              WHERE      A.ID_RICH_DIS_FND = B.ID_RICH_DIS_FND
        //                    AND A.ID_TRCH_DI_GAR  = :LDBV5471-ID-TRCH-DI-GAR
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.ID_MOVI_CHIU IS NULL
        //                    AND B.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A210-SELECT-WC-EFF<br>*/
    private void a210SelectWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                     B.ID_RICH_DIS_FND
        //                    ,B.ID_MOVI_FINRIO
        //                    ,B.ID_MOVI_CRZ
        //                    ,B.ID_MOVI_CHIU
        //                    ,B.DT_INI_EFF
        //                    ,B.DT_END_EFF
        //                    ,B.COD_COMP_ANIA
        //                    ,B.COD_FND
        //                    ,B.NUM_QUO
        //                    ,B.PC
        //                    ,B.IMP_MOVTO
        //                    ,B.DT_DIS
        //                    ,B.COD_TARI
        //                    ,B.TP_STAT
        //                    ,B.TP_MOD_DIS
        //                    ,B.COD_DIV
        //                    ,B.DT_CAMBIO_VLT
        //                    ,B.TP_FND
        //                    ,B.DS_RIGA
        //                    ,B.DS_OPER_SQL
        //                    ,B.DS_VER
        //                    ,B.DS_TS_INI_CPTZ
        //                    ,B.DS_TS_END_CPTZ
        //                    ,B.DS_UTENTE
        //                    ,B.DS_STATO_ELAB
        //                    ,B.DT_DIS_CALC
        //                    ,B.FL_CALC_DIS
        //                    ,B.COMMIS_GEST
        //                    ,B.NUM_QUO_CDG_FNZ
        //                    ,B.NUM_QUO_CDGTOT_FNZ
        //                    ,B.COS_RUN_ASSVA_IDC
        //                    ,B.FL_SWM_BP2S
        //             INTO
        //                :RDF-ID-RICH-DIS-FND
        //               ,:RDF-ID-MOVI-FINRIO
        //               ,:RDF-ID-MOVI-CRZ
        //               ,:RDF-ID-MOVI-CHIU
        //                :IND-RDF-ID-MOVI-CHIU
        //               ,:RDF-DT-INI-EFF-DB
        //               ,:RDF-DT-END-EFF-DB
        //               ,:RDF-COD-COMP-ANIA
        //               ,:RDF-COD-FND
        //                :IND-RDF-COD-FND
        //               ,:RDF-NUM-QUO
        //                :IND-RDF-NUM-QUO
        //               ,:RDF-PC
        //                :IND-RDF-PC
        //               ,:RDF-IMP-MOVTO
        //                :IND-RDF-IMP-MOVTO
        //               ,:RDF-DT-DIS-DB
        //                :IND-RDF-DT-DIS
        //               ,:RDF-COD-TARI
        //                :IND-RDF-COD-TARI
        //               ,:RDF-TP-STAT
        //                :IND-RDF-TP-STAT
        //               ,:RDF-TP-MOD-DIS
        //                :IND-RDF-TP-MOD-DIS
        //               ,:RDF-COD-DIV
        //               ,:RDF-DT-CAMBIO-VLT-DB
        //                :IND-RDF-DT-CAMBIO-VLT
        //               ,:RDF-TP-FND
        //               ,:RDF-DS-RIGA
        //               ,:RDF-DS-OPER-SQL
        //               ,:RDF-DS-VER
        //               ,:RDF-DS-TS-INI-CPTZ
        //               ,:RDF-DS-TS-END-CPTZ
        //               ,:RDF-DS-UTENTE
        //               ,:RDF-DS-STATO-ELAB
        //               ,:RDF-DT-DIS-CALC-DB
        //                :IND-RDF-DT-DIS-CALC
        //               ,:RDF-FL-CALC-DIS
        //                :IND-RDF-FL-CALC-DIS
        //               ,:RDF-COMMIS-GEST
        //                :IND-RDF-COMMIS-GEST
        //               ,:RDF-NUM-QUO-CDG-FNZ
        //                :IND-RDF-NUM-QUO-CDG-FNZ
        //               ,:RDF-NUM-QUO-CDGTOT-FNZ
        //                :IND-RDF-NUM-QUO-CDGTOT-FNZ
        //               ,:RDF-COS-RUN-ASSVA-IDC
        //                :IND-RDF-COS-RUN-ASSVA-IDC
        //               ,:RDF-FL-SWM-BP2S
        //                :IND-RDF-FL-SWM-BP2S
        //             FROM VAL_AST A,
        //                  RICH_DIS_FND B
        //             WHERE      A.ID_RICH_DIS_FND = B.ID_RICH_DIS_FND
        //                    AND A.ID_TRCH_DI_GAR  = :LDBV5471-ID-TRCH-DI-GAR
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.ID_MOVI_CHIU IS NULL
        //                    AND B.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        richDisFndValAstDao.selectRec(ws.getLdbv5471IdTrchDiGar(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: A260-OPEN-CURSOR-WC-EFF<br>*/
    private void a260OpenCursorWcEff() {
        // COB_CODE: PERFORM A205-DECLARE-CURSOR-WC-EFF THRU A205-EX.
        a205DeclareCursorWcEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-EFF
        //           END-EXEC.
        richDisFndValAstDao.openCEff27(ws.getLdbv5471IdTrchDiGar(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A270-CLOSE-CURSOR-WC-EFF<br>*/
    private void a270CloseCursorWcEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-EFF
        //           END-EXEC.
        richDisFndValAstDao.closeCEff27();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A280-FETCH-FIRST-WC-EFF<br>*/
    private void a280FetchFirstWcEff() {
        // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF    THRU A260-EX.
        a260OpenCursorWcEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
            a290FetchNextWcEff();
        }
    }

    /**Original name: A290-FETCH-NEXT-WC-EFF<br>*/
    private void a290FetchNextWcEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-EFF
        //           INTO
        //                :RDF-ID-RICH-DIS-FND
        //               ,:RDF-ID-MOVI-FINRIO
        //               ,:RDF-ID-MOVI-CRZ
        //               ,:RDF-ID-MOVI-CHIU
        //                :IND-RDF-ID-MOVI-CHIU
        //               ,:RDF-DT-INI-EFF-DB
        //               ,:RDF-DT-END-EFF-DB
        //               ,:RDF-COD-COMP-ANIA
        //               ,:RDF-COD-FND
        //                :IND-RDF-COD-FND
        //               ,:RDF-NUM-QUO
        //                :IND-RDF-NUM-QUO
        //               ,:RDF-PC
        //                :IND-RDF-PC
        //               ,:RDF-IMP-MOVTO
        //                :IND-RDF-IMP-MOVTO
        //               ,:RDF-DT-DIS-DB
        //                :IND-RDF-DT-DIS
        //               ,:RDF-COD-TARI
        //                :IND-RDF-COD-TARI
        //               ,:RDF-TP-STAT
        //                :IND-RDF-TP-STAT
        //               ,:RDF-TP-MOD-DIS
        //                :IND-RDF-TP-MOD-DIS
        //               ,:RDF-COD-DIV
        //               ,:RDF-DT-CAMBIO-VLT-DB
        //                :IND-RDF-DT-CAMBIO-VLT
        //               ,:RDF-TP-FND
        //               ,:RDF-DS-RIGA
        //               ,:RDF-DS-OPER-SQL
        //               ,:RDF-DS-VER
        //               ,:RDF-DS-TS-INI-CPTZ
        //               ,:RDF-DS-TS-END-CPTZ
        //               ,:RDF-DS-UTENTE
        //               ,:RDF-DS-STATO-ELAB
        //               ,:RDF-DT-DIS-CALC-DB
        //                :IND-RDF-DT-DIS-CALC
        //               ,:RDF-FL-CALC-DIS
        //                :IND-RDF-FL-CALC-DIS
        //               ,:RDF-COMMIS-GEST
        //                :IND-RDF-COMMIS-GEST
        //               ,:RDF-NUM-QUO-CDG-FNZ
        //                :IND-RDF-NUM-QUO-CDG-FNZ
        //               ,:RDF-NUM-QUO-CDGTOT-FNZ
        //                :IND-RDF-NUM-QUO-CDGTOT-FNZ
        //               ,:RDF-COS-RUN-ASSVA-IDC
        //                :IND-RDF-COS-RUN-ASSVA-IDC
        //               ,:RDF-FL-SWM-BP2S
        //                :IND-RDF-FL-SWM-BP2S
        //           END-EXEC.
        richDisFndValAstDao.fetchCEff27(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF THRU A270-EX
            a270CloseCursorWcEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B205-DECLARE-CURSOR-WC-CPZ<br>
	 * <pre>----
	 * ----  gestione WC Competenza
	 * ----</pre>*/
    private void b205DeclareCursorWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //              DECLARE C-CPZ CURSOR FOR
        //              SELECT
        //                     B.ID_RICH_DIS_FND
        //                    ,B.ID_MOVI_FINRIO
        //                    ,B.ID_MOVI_CRZ
        //                    ,B.ID_MOVI_CHIU
        //                    ,B.DT_INI_EFF
        //                    ,B.DT_END_EFF
        //                    ,B.COD_COMP_ANIA
        //                    ,B.COD_FND
        //                    ,B.NUM_QUO
        //                    ,B.PC
        //                    ,B.IMP_MOVTO
        //                    ,B.DT_DIS
        //                    ,B.COD_TARI
        //                    ,B.TP_STAT
        //                    ,B.TP_MOD_DIS
        //                    ,B.COD_DIV
        //                    ,B.DT_CAMBIO_VLT
        //                    ,B.TP_FND
        //                    ,B.DS_RIGA
        //                    ,B.DS_OPER_SQL
        //                    ,B.DS_VER
        //                    ,B.DS_TS_INI_CPTZ
        //                    ,B.DS_TS_END_CPTZ
        //                    ,B.DS_UTENTE
        //                    ,B.DS_STATO_ELAB
        //                    ,B.DT_DIS_CALC
        //                    ,B.FL_CALC_DIS
        //                    ,B.COMMIS_GEST
        //                    ,B.NUM_QUO_CDG_FNZ
        //                    ,B.NUM_QUO_CDGTOT_FNZ
        //                    ,B.COS_RUN_ASSVA_IDC
        //                    ,B.FL_SWM_BP2S
        //              FROM VAL_AST A,
        //                   RICH_DIS_FND B
        //              WHERE      A.ID_RICH_DIS_FND = B.ID_RICH_DIS_FND
        //                        AND A.ID_TRCH_DI_GAR  = :LDBV5471-ID-TRCH-DI-GAR
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND A.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //                    AND B.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND B.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B210-SELECT-WC-CPZ<br>*/
    private void b210SelectWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                     B.ID_RICH_DIS_FND
        //                    ,B.ID_MOVI_FINRIO
        //                    ,B.ID_MOVI_CRZ
        //                    ,B.ID_MOVI_CHIU
        //                    ,B.DT_INI_EFF
        //                    ,B.DT_END_EFF
        //                    ,B.COD_COMP_ANIA
        //                    ,B.COD_FND
        //                    ,B.NUM_QUO
        //                    ,B.PC
        //                    ,B.IMP_MOVTO
        //                    ,B.DT_DIS
        //                    ,B.COD_TARI
        //                    ,B.TP_STAT
        //                    ,B.TP_MOD_DIS
        //                    ,B.COD_DIV
        //                    ,B.DT_CAMBIO_VLT
        //                    ,B.TP_FND
        //                    ,B.DS_RIGA
        //                    ,B.DS_OPER_SQL
        //                    ,B.DS_VER
        //                    ,B.DS_TS_INI_CPTZ
        //                    ,B.DS_TS_END_CPTZ
        //                    ,B.DS_UTENTE
        //                    ,B.DS_STATO_ELAB
        //                    ,B.DT_DIS_CALC
        //                    ,B.FL_CALC_DIS
        //                    ,B.COMMIS_GEST
        //                    ,B.NUM_QUO_CDG_FNZ
        //                    ,B.NUM_QUO_CDGTOT_FNZ
        //                    ,B.COS_RUN_ASSVA_IDC
        //                    ,B.FL_SWM_BP2S
        //             INTO
        //                :RDF-ID-RICH-DIS-FND
        //               ,:RDF-ID-MOVI-FINRIO
        //               ,:RDF-ID-MOVI-CRZ
        //               ,:RDF-ID-MOVI-CHIU
        //                :IND-RDF-ID-MOVI-CHIU
        //               ,:RDF-DT-INI-EFF-DB
        //               ,:RDF-DT-END-EFF-DB
        //               ,:RDF-COD-COMP-ANIA
        //               ,:RDF-COD-FND
        //                :IND-RDF-COD-FND
        //               ,:RDF-NUM-QUO
        //                :IND-RDF-NUM-QUO
        //               ,:RDF-PC
        //                :IND-RDF-PC
        //               ,:RDF-IMP-MOVTO
        //                :IND-RDF-IMP-MOVTO
        //               ,:RDF-DT-DIS-DB
        //                :IND-RDF-DT-DIS
        //               ,:RDF-COD-TARI
        //                :IND-RDF-COD-TARI
        //               ,:RDF-TP-STAT
        //                :IND-RDF-TP-STAT
        //               ,:RDF-TP-MOD-DIS
        //                :IND-RDF-TP-MOD-DIS
        //               ,:RDF-COD-DIV
        //               ,:RDF-DT-CAMBIO-VLT-DB
        //                :IND-RDF-DT-CAMBIO-VLT
        //               ,:RDF-TP-FND
        //               ,:RDF-DS-RIGA
        //               ,:RDF-DS-OPER-SQL
        //               ,:RDF-DS-VER
        //               ,:RDF-DS-TS-INI-CPTZ
        //               ,:RDF-DS-TS-END-CPTZ
        //               ,:RDF-DS-UTENTE
        //               ,:RDF-DS-STATO-ELAB
        //               ,:RDF-DT-DIS-CALC-DB
        //                :IND-RDF-DT-DIS-CALC
        //               ,:RDF-FL-CALC-DIS
        //                :IND-RDF-FL-CALC-DIS
        //               ,:RDF-COMMIS-GEST
        //                :IND-RDF-COMMIS-GEST
        //               ,:RDF-NUM-QUO-CDG-FNZ
        //                :IND-RDF-NUM-QUO-CDG-FNZ
        //               ,:RDF-NUM-QUO-CDGTOT-FNZ
        //                :IND-RDF-NUM-QUO-CDGTOT-FNZ
        //               ,:RDF-COS-RUN-ASSVA-IDC
        //                :IND-RDF-COS-RUN-ASSVA-IDC
        //               ,:RDF-FL-SWM-BP2S
        //                :IND-RDF-FL-SWM-BP2S
        //             FROM VAL_AST A,
        //                  RICH_DIS_FND B
        //             WHERE      A.ID_RICH_DIS_FND = B.ID_RICH_DIS_FND
        //                    AND A.ID_TRCH_DI_GAR  = :LDBV5471-ID-TRCH-DI-GAR
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND A.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //                    AND B.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND B.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        richDisFndValAstDao.selectRec1(ws.getLdbv5471IdTrchDiGar(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: B260-OPEN-CURSOR-WC-CPZ<br>*/
    private void b260OpenCursorWcCpz() {
        // COB_CODE: PERFORM B205-DECLARE-CURSOR-WC-CPZ THRU B205-EX.
        b205DeclareCursorWcCpz();
        // COB_CODE: EXEC SQL
        //                OPEN C-CPZ
        //           END-EXEC.
        richDisFndValAstDao.openCCpz27(ws.getLdbv5471IdTrchDiGar(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B270-CLOSE-CURSOR-WC-CPZ<br>*/
    private void b270CloseCursorWcCpz() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-CPZ
        //           END-EXEC.
        richDisFndValAstDao.closeCCpz27();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B280-FETCH-FIRST-WC-CPZ<br>*/
    private void b280FetchFirstWcCpz() {
        // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ    THRU B260-EX.
        b260OpenCursorWcCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
            b290FetchNextWcCpz();
        }
    }

    /**Original name: B290-FETCH-NEXT-WC-CPZ<br>*/
    private void b290FetchNextWcCpz() {
        // COB_CODE: EXEC SQL
        //                FETCH C-CPZ
        //           INTO
        //                :RDF-ID-RICH-DIS-FND
        //               ,:RDF-ID-MOVI-FINRIO
        //               ,:RDF-ID-MOVI-CRZ
        //               ,:RDF-ID-MOVI-CHIU
        //                :IND-RDF-ID-MOVI-CHIU
        //               ,:RDF-DT-INI-EFF-DB
        //               ,:RDF-DT-END-EFF-DB
        //               ,:RDF-COD-COMP-ANIA
        //               ,:RDF-COD-FND
        //                :IND-RDF-COD-FND
        //               ,:RDF-NUM-QUO
        //                :IND-RDF-NUM-QUO
        //               ,:RDF-PC
        //                :IND-RDF-PC
        //               ,:RDF-IMP-MOVTO
        //                :IND-RDF-IMP-MOVTO
        //               ,:RDF-DT-DIS-DB
        //                :IND-RDF-DT-DIS
        //               ,:RDF-COD-TARI
        //                :IND-RDF-COD-TARI
        //               ,:RDF-TP-STAT
        //                :IND-RDF-TP-STAT
        //               ,:RDF-TP-MOD-DIS
        //                :IND-RDF-TP-MOD-DIS
        //               ,:RDF-COD-DIV
        //               ,:RDF-DT-CAMBIO-VLT-DB
        //                :IND-RDF-DT-CAMBIO-VLT
        //               ,:RDF-TP-FND
        //               ,:RDF-DS-RIGA
        //               ,:RDF-DS-OPER-SQL
        //               ,:RDF-DS-VER
        //               ,:RDF-DS-TS-INI-CPTZ
        //               ,:RDF-DS-TS-END-CPTZ
        //               ,:RDF-DS-UTENTE
        //               ,:RDF-DS-STATO-ELAB
        //               ,:RDF-DT-DIS-CALC-DB
        //                :IND-RDF-DT-DIS-CALC
        //               ,:RDF-FL-CALC-DIS
        //                :IND-RDF-FL-CALC-DIS
        //               ,:RDF-COMMIS-GEST
        //                :IND-RDF-COMMIS-GEST
        //               ,:RDF-NUM-QUO-CDG-FNZ
        //                :IND-RDF-NUM-QUO-CDG-FNZ
        //               ,:RDF-NUM-QUO-CDGTOT-FNZ
        //                :IND-RDF-NUM-QUO-CDGTOT-FNZ
        //               ,:RDF-COS-RUN-ASSVA-IDC
        //                :IND-RDF-COS-RUN-ASSVA-IDC
        //               ,:RDF-FL-SWM-BP2S
        //                :IND-RDF-FL-SWM-BP2S
        //           END-EXEC.
        richDisFndValAstDao.fetchCCpz27(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ THRU B270-EX
            b270CloseCursorWcCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: C205-DECLARE-CURSOR-WC-NST<br>
	 * <pre>----
	 * ----  gestione WC Senza Storicitä
	 * ----</pre>*/
    private void c205DeclareCursorWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C210-SELECT-WC-NST<br>*/
    private void c210SelectWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C260-OPEN-CURSOR-WC-NST<br>*/
    private void c260OpenCursorWcNst() {
        // COB_CODE: PERFORM C205-DECLARE-CURSOR-WC-NST THRU C205-EX.
        c205DeclareCursorWcNst();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C270-CLOSE-CURSOR-WC-NST<br>*/
    private void c270CloseCursorWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C280-FETCH-FIRST-WC-NST<br>*/
    private void c280FetchFirstWcNst() {
        // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST    THRU C260-EX.
        c260OpenCursorWcNst();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
            c290FetchNextWcNst();
        }
    }

    /**Original name: C290-FETCH-NEXT-WC-NST<br>*/
    private void c290FetchNextWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>----
	 * ----  utilitä comuni a tutti i livelli operazione
	 * ----</pre>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-RDF-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO RDF-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndRichDisFnd().getCodStrDato2() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RDF-ID-MOVI-CHIU-NULL
            richDisFnd.getRdfIdMoviChiu().setRdfIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RdfIdMoviChiu.Len.RDF_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-RDF-COD-FND = -1
        //              MOVE HIGH-VALUES TO RDF-COD-FND-NULL
        //           END-IF
        if (ws.getIndRichDisFnd().getCodDato() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RDF-COD-FND-NULL
            richDisFnd.setRdfCodFnd(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RichDisFnd.Len.RDF_COD_FND));
        }
        // COB_CODE: IF IND-RDF-NUM-QUO = -1
        //              MOVE HIGH-VALUES TO RDF-NUM-QUO-NULL
        //           END-IF
        if (ws.getIndRichDisFnd().getFlagKey() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RDF-NUM-QUO-NULL
            richDisFnd.getRdfNumQuo().setRdfNumQuoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RdfNumQuo.Len.RDF_NUM_QUO_NULL));
        }
        // COB_CODE: IF IND-RDF-PC = -1
        //              MOVE HIGH-VALUES TO RDF-PC-NULL
        //           END-IF
        if (ws.getIndRichDisFnd().getFlagReturnCode() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RDF-PC-NULL
            richDisFnd.getRdfPc().setRdfPcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RdfPc.Len.RDF_PC_NULL));
        }
        // COB_CODE: IF IND-RDF-IMP-MOVTO = -1
        //              MOVE HIGH-VALUES TO RDF-IMP-MOVTO-NULL
        //           END-IF
        if (ws.getIndRichDisFnd().getFlagCallUsing() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RDF-IMP-MOVTO-NULL
            richDisFnd.getRdfImpMovto().setRdfImpMovtoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RdfImpMovto.Len.RDF_IMP_MOVTO_NULL));
        }
        // COB_CODE: IF IND-RDF-DT-DIS = -1
        //              MOVE HIGH-VALUES TO RDF-DT-DIS-NULL
        //           END-IF
        if (ws.getIndRichDisFnd().getFlagWhereCond() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RDF-DT-DIS-NULL
            richDisFnd.getRdfDtDis().setRdfDtDisNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RdfDtDis.Len.RDF_DT_DIS_NULL));
        }
        // COB_CODE: IF IND-RDF-COD-TARI = -1
        //              MOVE HIGH-VALUES TO RDF-COD-TARI-NULL
        //           END-IF
        if (ws.getIndRichDisFnd().getFlagRedefines() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RDF-COD-TARI-NULL
            richDisFnd.setRdfCodTari(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RichDisFnd.Len.RDF_COD_TARI));
        }
        // COB_CODE: IF IND-RDF-TP-STAT = -1
        //              MOVE HIGH-VALUES TO RDF-TP-STAT-NULL
        //           END-IF
        if (ws.getIndRichDisFnd().getTipoDato() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RDF-TP-STAT-NULL
            richDisFnd.setRdfTpStat(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RichDisFnd.Len.RDF_TP_STAT));
        }
        // COB_CODE: IF IND-RDF-TP-MOD-DIS = -1
        //              MOVE HIGH-VALUES TO RDF-TP-MOD-DIS-NULL
        //           END-IF
        if (ws.getIndRichDisFnd().getLunghezzaDato() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RDF-TP-MOD-DIS-NULL
            richDisFnd.setRdfTpModDis(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RichDisFnd.Len.RDF_TP_MOD_DIS));
        }
        // COB_CODE: IF IND-RDF-DT-CAMBIO-VLT = -1
        //              MOVE HIGH-VALUES TO RDF-DT-CAMBIO-VLT-NULL
        //           END-IF
        if (ws.getIndRichDisFnd().getPrecisioneDato() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RDF-DT-CAMBIO-VLT-NULL
            richDisFnd.getRdfDtCambioVlt().setRdfDtCambioVltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RdfDtCambioVlt.Len.RDF_DT_CAMBIO_VLT_NULL));
        }
        // COB_CODE: IF IND-RDF-DT-DIS-CALC = -1
        //              MOVE HIGH-VALUES TO RDF-DT-DIS-CALC-NULL
        //           END-IF
        if (ws.getIndRichDisFnd().getCodDominio() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RDF-DT-DIS-CALC-NULL
            richDisFnd.getRdfDtDisCalc().setRdfDtDisCalcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RdfDtDisCalc.Len.RDF_DT_DIS_CALC_NULL));
        }
        // COB_CODE: IF IND-RDF-FL-CALC-DIS = -1
        //              MOVE HIGH-VALUES TO RDF-FL-CALC-DIS-NULL
        //           END-IF
        if (ws.getIndRichDisFnd().getFormattazioneDato() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RDF-FL-CALC-DIS-NULL
            richDisFnd.setRdfFlCalcDis(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-RDF-COMMIS-GEST = -1
        //              MOVE HIGH-VALUES TO RDF-COMMIS-GEST-NULL
        //           END-IF
        if (ws.getIndRichDisFnd().getServizioConvers() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RDF-COMMIS-GEST-NULL
            richDisFnd.getRdfCommisGest().setRdfCommisGestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RdfCommisGest.Len.RDF_COMMIS_GEST_NULL));
        }
        // COB_CODE: IF IND-RDF-NUM-QUO-CDG-FNZ = -1
        //              MOVE HIGH-VALUES TO RDF-NUM-QUO-CDG-FNZ-NULL
        //           END-IF
        if (ws.getIndRichDisFnd().getAreaConvStandard() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RDF-NUM-QUO-CDG-FNZ-NULL
            richDisFnd.getRdfNumQuoCdgFnz().setRdfNumQuoCdgFnzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RdfNumQuoCdgFnz.Len.RDF_NUM_QUO_CDG_FNZ_NULL));
        }
        // COB_CODE: IF IND-RDF-NUM-QUO-CDGTOT-FNZ = -1
        //              MOVE HIGH-VALUES TO RDF-NUM-QUO-CDGTOT-FNZ-NULL
        //           END-IF
        if (ws.getIndRichDisFnd().getRicorrenza() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RDF-NUM-QUO-CDGTOT-FNZ-NULL
            richDisFnd.getRdfNumQuoCdgtotFnz().setRdfNumQuoCdgtotFnzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RdfNumQuoCdgtotFnz.Len.RDF_NUM_QUO_CDGTOT_FNZ_NULL));
        }
        // COB_CODE: IF IND-RDF-COS-RUN-ASSVA-IDC = -1
        //              MOVE HIGH-VALUES TO RDF-COS-RUN-ASSVA-IDC-NULL
        //           END-IF
        if (ws.getIndRichDisFnd().getObbligatorieta() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RDF-COS-RUN-ASSVA-IDC-NULL
            richDisFnd.getRdfCosRunAssvaIdc().setRdfCosRunAssvaIdcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RdfCosRunAssvaIdc.Len.RDF_COS_RUN_ASSVA_IDC_NULL));
        }
        // COB_CODE: IF IND-RDF-FL-SWM-BP2S = -1
        //              MOVE HIGH-VALUES TO RDF-FL-SWM-BP2S-NULL
        //           END-IF.
        if (ws.getIndRichDisFnd().getValoreDefault() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RDF-FL-SWM-BP2S-NULL
            richDisFnd.setRdfFlSwmBp2s(Types.HIGH_CHAR_VAL);
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE RDF-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getRichDisFndDb().getIniEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO RDF-DT-INI-EFF
        richDisFnd.setRdfDtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE RDF-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getRichDisFndDb().getEndEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO RDF-DT-END-EFF
        richDisFnd.setRdfDtEndEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-RDF-DT-DIS = 0
        //               MOVE WS-DATE-N      TO RDF-DT-DIS
        //           END-IF
        if (ws.getIndRichDisFnd().getFlagWhereCond() == 0) {
            // COB_CODE: MOVE RDF-DT-DIS-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getRichDisFndDb().getIniCopDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO RDF-DT-DIS
            richDisFnd.getRdfDtDis().setRdfDtDis(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-RDF-DT-CAMBIO-VLT = 0
        //               MOVE WS-DATE-N      TO RDF-DT-CAMBIO-VLT
        //           END-IF
        if (ws.getIndRichDisFnd().getPrecisioneDato() == 0) {
            // COB_CODE: MOVE RDF-DT-CAMBIO-VLT-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getRichDisFndDb().getEndCopDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO RDF-DT-CAMBIO-VLT
            richDisFnd.getRdfDtCambioVlt().setRdfDtCambioVlt(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-RDF-DT-DIS-CALC = 0
        //               MOVE WS-DATE-N      TO RDF-DT-DIS-CALC
        //           END-IF.
        if (ws.getIndRichDisFnd().getCodDominio() == 0) {
            // COB_CODE: MOVE RDF-DT-DIS-CALC-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getRichDisFndDb().getEsiTitDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO RDF-DT-DIS-CALC
            richDisFnd.getRdfDtDisCalc().setRdfDtDisCalc(ws.getIdsv0010().getWsDateN());
        }
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z970-CODICE-ADHOC-PRE<br>
	 * <pre>----
	 * ----  prevede statements AD HOC PRE Query
	 * ----</pre>*/
    private void z970CodiceAdhocPre() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z980-CODICE-ADHOC-POST<br>
	 * <pre>----
	 * ----  prevede statements AD HOC POST Query
	 * ----</pre>*/
    private void z980CodiceAdhocPost() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public int getCodCompAnia() {
        return richDisFnd.getRdfCodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.richDisFnd.setRdfCodCompAnia(codCompAnia);
    }

    @Override
    public String getCodDiv() {
        return richDisFnd.getRdfCodDiv();
    }

    @Override
    public void setCodDiv(String codDiv) {
        this.richDisFnd.setRdfCodDiv(codDiv);
    }

    @Override
    public String getCodFnd() {
        return richDisFnd.getRdfCodFnd();
    }

    @Override
    public void setCodFnd(String codFnd) {
        this.richDisFnd.setRdfCodFnd(codFnd);
    }

    @Override
    public String getCodFndObj() {
        if (ws.getIndRichDisFnd().getCodDato() >= 0) {
            return getCodFnd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodFndObj(String codFndObj) {
        if (codFndObj != null) {
            setCodFnd(codFndObj);
            ws.getIndRichDisFnd().setCodDato(((short)0));
        }
        else {
            ws.getIndRichDisFnd().setCodDato(((short)-1));
        }
    }

    @Override
    public String getCodTari() {
        return richDisFnd.getRdfCodTari();
    }

    @Override
    public void setCodTari(String codTari) {
        this.richDisFnd.setRdfCodTari(codTari);
    }

    @Override
    public String getCodTariObj() {
        if (ws.getIndRichDisFnd().getFlagRedefines() >= 0) {
            return getCodTari();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodTariObj(String codTariObj) {
        if (codTariObj != null) {
            setCodTari(codTariObj);
            ws.getIndRichDisFnd().setFlagRedefines(((short)0));
        }
        else {
            ws.getIndRichDisFnd().setFlagRedefines(((short)-1));
        }
    }

    @Override
    public AfDecimal getCommisGest() {
        return richDisFnd.getRdfCommisGest().getRdfCommisGest();
    }

    @Override
    public void setCommisGest(AfDecimal commisGest) {
        this.richDisFnd.getRdfCommisGest().setRdfCommisGest(commisGest.copy());
    }

    @Override
    public AfDecimal getCommisGestObj() {
        if (ws.getIndRichDisFnd().getServizioConvers() >= 0) {
            return getCommisGest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCommisGestObj(AfDecimal commisGestObj) {
        if (commisGestObj != null) {
            setCommisGest(new AfDecimal(commisGestObj, 18, 7));
            ws.getIndRichDisFnd().setServizioConvers(((short)0));
        }
        else {
            ws.getIndRichDisFnd().setServizioConvers(((short)-1));
        }
    }

    @Override
    public AfDecimal getCosRunAssvaIdc() {
        return richDisFnd.getRdfCosRunAssvaIdc().getRdfCosRunAssvaIdc();
    }

    @Override
    public void setCosRunAssvaIdc(AfDecimal cosRunAssvaIdc) {
        this.richDisFnd.getRdfCosRunAssvaIdc().setRdfCosRunAssvaIdc(cosRunAssvaIdc.copy());
    }

    @Override
    public AfDecimal getCosRunAssvaIdcObj() {
        if (ws.getIndRichDisFnd().getObbligatorieta() >= 0) {
            return getCosRunAssvaIdc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCosRunAssvaIdcObj(AfDecimal cosRunAssvaIdcObj) {
        if (cosRunAssvaIdcObj != null) {
            setCosRunAssvaIdc(new AfDecimal(cosRunAssvaIdcObj, 15, 3));
            ws.getIndRichDisFnd().setObbligatorieta(((short)0));
        }
        else {
            ws.getIndRichDisFnd().setObbligatorieta(((short)-1));
        }
    }

    @Override
    public char getDsOperSql() {
        return richDisFnd.getRdfDsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.richDisFnd.setRdfDsOperSql(dsOperSql);
    }

    @Override
    public long getDsRiga() {
        return richDisFnd.getRdfDsRiga();
    }

    @Override
    public void setDsRiga(long dsRiga) {
        this.richDisFnd.setRdfDsRiga(dsRiga);
    }

    @Override
    public char getDsStatoElab() {
        return richDisFnd.getRdfDsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.richDisFnd.setRdfDsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsEndCptz() {
        return richDisFnd.getRdfDsTsEndCptz();
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.richDisFnd.setRdfDsTsEndCptz(dsTsEndCptz);
    }

    @Override
    public long getDsTsIniCptz() {
        return richDisFnd.getRdfDsTsIniCptz();
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.richDisFnd.setRdfDsTsIniCptz(dsTsIniCptz);
    }

    @Override
    public String getDsUtente() {
        return richDisFnd.getRdfDsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.richDisFnd.setRdfDsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return richDisFnd.getRdfDsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.richDisFnd.setRdfDsVer(dsVer);
    }

    @Override
    public String getDtCambioVltDb() {
        return ws.getRichDisFndDb().getEndCopDb();
    }

    @Override
    public void setDtCambioVltDb(String dtCambioVltDb) {
        this.ws.getRichDisFndDb().setEndCopDb(dtCambioVltDb);
    }

    @Override
    public String getDtCambioVltDbObj() {
        if (ws.getIndRichDisFnd().getPrecisioneDato() >= 0) {
            return getDtCambioVltDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtCambioVltDbObj(String dtCambioVltDbObj) {
        if (dtCambioVltDbObj != null) {
            setDtCambioVltDb(dtCambioVltDbObj);
            ws.getIndRichDisFnd().setPrecisioneDato(((short)0));
        }
        else {
            ws.getIndRichDisFnd().setPrecisioneDato(((short)-1));
        }
    }

    @Override
    public String getDtDisCalcDb() {
        return ws.getRichDisFndDb().getEsiTitDb();
    }

    @Override
    public void setDtDisCalcDb(String dtDisCalcDb) {
        this.ws.getRichDisFndDb().setEsiTitDb(dtDisCalcDb);
    }

    @Override
    public String getDtDisCalcDbObj() {
        if (ws.getIndRichDisFnd().getCodDominio() >= 0) {
            return getDtDisCalcDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtDisCalcDbObj(String dtDisCalcDbObj) {
        if (dtDisCalcDbObj != null) {
            setDtDisCalcDb(dtDisCalcDbObj);
            ws.getIndRichDisFnd().setCodDominio(((short)0));
        }
        else {
            ws.getIndRichDisFnd().setCodDominio(((short)-1));
        }
    }

    @Override
    public String getDtDisDb() {
        return ws.getRichDisFndDb().getIniCopDb();
    }

    @Override
    public void setDtDisDb(String dtDisDb) {
        this.ws.getRichDisFndDb().setIniCopDb(dtDisDb);
    }

    @Override
    public String getDtDisDbObj() {
        if (ws.getIndRichDisFnd().getFlagWhereCond() >= 0) {
            return getDtDisDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtDisDbObj(String dtDisDbObj) {
        if (dtDisDbObj != null) {
            setDtDisDb(dtDisDbObj);
            ws.getIndRichDisFnd().setFlagWhereCond(((short)0));
        }
        else {
            ws.getIndRichDisFnd().setFlagWhereCond(((short)-1));
        }
    }

    @Override
    public String getDtEndEffDb() {
        return ws.getRichDisFndDb().getEndEffDb();
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        this.ws.getRichDisFndDb().setEndEffDb(dtEndEffDb);
    }

    @Override
    public String getDtIniEffDb() {
        return ws.getRichDisFndDb().getIniEffDb();
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        this.ws.getRichDisFndDb().setIniEffDb(dtIniEffDb);
    }

    @Override
    public char getFlCalcDis() {
        return richDisFnd.getRdfFlCalcDis();
    }

    @Override
    public void setFlCalcDis(char flCalcDis) {
        this.richDisFnd.setRdfFlCalcDis(flCalcDis);
    }

    @Override
    public Character getFlCalcDisObj() {
        if (ws.getIndRichDisFnd().getFormattazioneDato() >= 0) {
            return ((Character)getFlCalcDis());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlCalcDisObj(Character flCalcDisObj) {
        if (flCalcDisObj != null) {
            setFlCalcDis(((char)flCalcDisObj));
            ws.getIndRichDisFnd().setFormattazioneDato(((short)0));
        }
        else {
            ws.getIndRichDisFnd().setFormattazioneDato(((short)-1));
        }
    }

    @Override
    public char getFlSwmBp2s() {
        return richDisFnd.getRdfFlSwmBp2s();
    }

    @Override
    public void setFlSwmBp2s(char flSwmBp2s) {
        this.richDisFnd.setRdfFlSwmBp2s(flSwmBp2s);
    }

    @Override
    public Character getFlSwmBp2sObj() {
        if (ws.getIndRichDisFnd().getValoreDefault() >= 0) {
            return ((Character)getFlSwmBp2s());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlSwmBp2sObj(Character flSwmBp2sObj) {
        if (flSwmBp2sObj != null) {
            setFlSwmBp2s(((char)flSwmBp2sObj));
            ws.getIndRichDisFnd().setValoreDefault(((short)0));
        }
        else {
            ws.getIndRichDisFnd().setValoreDefault(((short)-1));
        }
    }

    @Override
    public int getIdMoviChiu() {
        return richDisFnd.getRdfIdMoviChiu().getRdfIdMoviChiu();
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        this.richDisFnd.getRdfIdMoviChiu().setRdfIdMoviChiu(idMoviChiu);
    }

    @Override
    public Integer getIdMoviChiuObj() {
        if (ws.getIndRichDisFnd().getCodStrDato2() >= 0) {
            return ((Integer)getIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        if (idMoviChiuObj != null) {
            setIdMoviChiu(((int)idMoviChiuObj));
            ws.getIndRichDisFnd().setCodStrDato2(((short)0));
        }
        else {
            ws.getIndRichDisFnd().setCodStrDato2(((short)-1));
        }
    }

    @Override
    public int getIdMoviCrz() {
        return richDisFnd.getRdfIdMoviCrz();
    }

    @Override
    public void setIdMoviCrz(int idMoviCrz) {
        this.richDisFnd.setRdfIdMoviCrz(idMoviCrz);
    }

    @Override
    public int getIdMoviFinrio() {
        return richDisFnd.getRdfIdMoviFinrio();
    }

    @Override
    public void setIdMoviFinrio(int idMoviFinrio) {
        this.richDisFnd.setRdfIdMoviFinrio(idMoviFinrio);
    }

    @Override
    public int getIdRichDisFnd() {
        return richDisFnd.getRdfIdRichDisFnd();
    }

    @Override
    public void setIdRichDisFnd(int idRichDisFnd) {
        this.richDisFnd.setRdfIdRichDisFnd(idRichDisFnd);
    }

    @Override
    public AfDecimal getImpMovto() {
        return richDisFnd.getRdfImpMovto().getRdfImpMovto();
    }

    @Override
    public void setImpMovto(AfDecimal impMovto) {
        this.richDisFnd.getRdfImpMovto().setRdfImpMovto(impMovto.copy());
    }

    @Override
    public AfDecimal getImpMovtoObj() {
        if (ws.getIndRichDisFnd().getFlagCallUsing() >= 0) {
            return getImpMovto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpMovtoObj(AfDecimal impMovtoObj) {
        if (impMovtoObj != null) {
            setImpMovto(new AfDecimal(impMovtoObj, 15, 3));
            ws.getIndRichDisFnd().setFlagCallUsing(((short)0));
        }
        else {
            ws.getIndRichDisFnd().setFlagCallUsing(((short)-1));
        }
    }

    @Override
    public AfDecimal getNumQuo() {
        return richDisFnd.getRdfNumQuo().getRdfNumQuo();
    }

    @Override
    public void setNumQuo(AfDecimal numQuo) {
        this.richDisFnd.getRdfNumQuo().setRdfNumQuo(numQuo.copy());
    }

    @Override
    public AfDecimal getNumQuoCdgFnz() {
        return richDisFnd.getRdfNumQuoCdgFnz().getRdfNumQuoCdgFnz();
    }

    @Override
    public void setNumQuoCdgFnz(AfDecimal numQuoCdgFnz) {
        this.richDisFnd.getRdfNumQuoCdgFnz().setRdfNumQuoCdgFnz(numQuoCdgFnz.copy());
    }

    @Override
    public AfDecimal getNumQuoCdgFnzObj() {
        if (ws.getIndRichDisFnd().getAreaConvStandard() >= 0) {
            return getNumQuoCdgFnz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setNumQuoCdgFnzObj(AfDecimal numQuoCdgFnzObj) {
        if (numQuoCdgFnzObj != null) {
            setNumQuoCdgFnz(new AfDecimal(numQuoCdgFnzObj, 12, 5));
            ws.getIndRichDisFnd().setAreaConvStandard(((short)0));
        }
        else {
            ws.getIndRichDisFnd().setAreaConvStandard(((short)-1));
        }
    }

    @Override
    public AfDecimal getNumQuoCdgtotFnz() {
        return richDisFnd.getRdfNumQuoCdgtotFnz().getRdfNumQuoCdgtotFnz();
    }

    @Override
    public void setNumQuoCdgtotFnz(AfDecimal numQuoCdgtotFnz) {
        this.richDisFnd.getRdfNumQuoCdgtotFnz().setRdfNumQuoCdgtotFnz(numQuoCdgtotFnz.copy());
    }

    @Override
    public AfDecimal getNumQuoCdgtotFnzObj() {
        if (ws.getIndRichDisFnd().getRicorrenza() >= 0) {
            return getNumQuoCdgtotFnz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setNumQuoCdgtotFnzObj(AfDecimal numQuoCdgtotFnzObj) {
        if (numQuoCdgtotFnzObj != null) {
            setNumQuoCdgtotFnz(new AfDecimal(numQuoCdgtotFnzObj, 12, 5));
            ws.getIndRichDisFnd().setRicorrenza(((short)0));
        }
        else {
            ws.getIndRichDisFnd().setRicorrenza(((short)-1));
        }
    }

    @Override
    public AfDecimal getNumQuoObj() {
        if (ws.getIndRichDisFnd().getFlagKey() >= 0) {
            return getNumQuo();
        }
        else {
            return null;
        }
    }

    @Override
    public void setNumQuoObj(AfDecimal numQuoObj) {
        if (numQuoObj != null) {
            setNumQuo(new AfDecimal(numQuoObj, 12, 5));
            ws.getIndRichDisFnd().setFlagKey(((short)0));
        }
        else {
            ws.getIndRichDisFnd().setFlagKey(((short)-1));
        }
    }

    @Override
    public AfDecimal getPc() {
        return richDisFnd.getRdfPc().getRdfPc();
    }

    @Override
    public void setPc(AfDecimal pc) {
        this.richDisFnd.getRdfPc().setRdfPc(pc.copy());
    }

    @Override
    public AfDecimal getPcObj() {
        if (ws.getIndRichDisFnd().getFlagReturnCode() >= 0) {
            return getPc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcObj(AfDecimal pcObj) {
        if (pcObj != null) {
            setPc(new AfDecimal(pcObj, 6, 3));
            ws.getIndRichDisFnd().setFlagReturnCode(((short)0));
        }
        else {
            ws.getIndRichDisFnd().setFlagReturnCode(((short)-1));
        }
    }

    @Override
    public char getTpFnd() {
        return richDisFnd.getRdfTpFnd();
    }

    @Override
    public void setTpFnd(char tpFnd) {
        this.richDisFnd.setRdfTpFnd(tpFnd);
    }

    @Override
    public String getTpModDis() {
        return richDisFnd.getRdfTpModDis();
    }

    @Override
    public void setTpModDis(String tpModDis) {
        this.richDisFnd.setRdfTpModDis(tpModDis);
    }

    @Override
    public String getTpModDisObj() {
        if (ws.getIndRichDisFnd().getLunghezzaDato() >= 0) {
            return getTpModDis();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpModDisObj(String tpModDisObj) {
        if (tpModDisObj != null) {
            setTpModDis(tpModDisObj);
            ws.getIndRichDisFnd().setLunghezzaDato(((short)0));
        }
        else {
            ws.getIndRichDisFnd().setLunghezzaDato(((short)-1));
        }
    }

    @Override
    public String getTpStat() {
        return richDisFnd.getRdfTpStat();
    }

    @Override
    public void setTpStat(String tpStat) {
        this.richDisFnd.setRdfTpStat(tpStat);
    }

    @Override
    public String getTpStatObj() {
        if (ws.getIndRichDisFnd().getTipoDato() >= 0) {
            return getTpStat();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpStatObj(String tpStatObj) {
        if (tpStatObj != null) {
            setTpStat(tpStatObj);
            ws.getIndRichDisFnd().setTipoDato(((short)0));
        }
        else {
            ws.getIndRichDisFnd().setTipoDato(((short)-1));
        }
    }
}
