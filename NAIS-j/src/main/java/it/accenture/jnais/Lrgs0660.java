package it.accenture.jnais;

import com.bphx.ctu.af.core.buffer.BasicBytesClass;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.pointer.IPointerManager;
import com.bphx.ctu.af.core.program.DynamicCall;
import com.bphx.ctu.af.core.program.GenericParam;
import com.bphx.ctu.af.core.program.StringParam;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.io.file.FileAccessStatus;
import com.bphx.ctu.af.io.file.FileBufferedDAO;
import com.bphx.ctu.af.io.file.FileRecordBuffer;
import com.bphx.ctu.af.io.file.OpenMode;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.lang.types.RoundingMode;
import com.bphx.ctu.af.lang.util.MathUtil;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.display.DisplayUtil;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.batch.DdCard;
import com.modernsystems.ctu.core.impl.BatchProgram;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.A2kInput;
import it.accenture.jnais.copy.A2kOugbmba;
import it.accenture.jnais.copy.A2kOurid;
import it.accenture.jnais.copy.A2kOusta;
import it.accenture.jnais.copy.A2kOutput;
import it.accenture.jnais.copy.Ispc0580DatiInput;
import it.accenture.jnais.copy.Ldbv6191;
import it.accenture.jnais.copy.WrinRecOutput;
import it.accenture.jnais.ws.A2kOuamgX;
import it.accenture.jnais.ws.A2kOugl07X;
import it.accenture.jnais.ws.A2kOugmaX;
import it.accenture.jnais.ws.A2kOujulX;
import it.accenture.jnais.ws.AreaIdsv0001;
import it.accenture.jnais.ws.AreaIoIsps0580;
import it.accenture.jnais.ws.AreaIoIsps0590;
import it.accenture.jnais.ws.enums.FileStatus1;
import it.accenture.jnais.ws.enums.Idso0011SqlcodeSigned;
import it.accenture.jnais.ws.enums.OperazioneFilesqs;
import it.accenture.jnais.ws.Iabv0006;
import it.accenture.jnais.ws.Ieai9901Area;
import it.accenture.jnais.ws.IxIndiciLrgs0660;
import it.accenture.jnais.ws.Lrgs0660Data;
import it.accenture.jnais.ws.occurs.EleCache10;
import it.accenture.jnais.ws.occurs.EleCache9;
import it.accenture.jnais.ws.occurs.Ispc0211TabErrori;
import it.accenture.jnais.ws.ptr.Idsv8888StrPerformanceDbg;
import it.accenture.jnais.ws.S0024AreaComune;
import it.accenture.jnais.ws.WkAppoDataJob;
import it.accenture.jnais.ws.WkAreaFile;
import it.accenture.jnais.ws.WkFinRic;
import it.accenture.jnais.ws.WkIniRic;
import it.accenture.jnais.ws.WsVariabiliLrgs0660;
import javax.inject.Inject;
import static com.bphx.ctu.af.lang.types.AfDecimal.abs;
import static java.lang.Math.abs;

/**Original name: LRGS0660<br>
 * <pre>AUTHOR.        ATS NAPOLI.
 * DATE-WRITTEN.  2010.
 * DATE-COMPILED.
 * ----------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                      *
 *  F A S E         : SERVIZIO BUSINESS RICHIAMATO DAL BATCH EXEC. *
 *                                                                 *
 * ----------------------------------------------------------------*
 *  Fase Diagnostica - Estratto Conto
 * ----------------------------------------------------------------*
 * --> FILEOUT 1
 * --> OK E/C 'RIVALUTABILI, INDEX E UNIT'
 * --> FILEOUT 2
 * --> KO E/C 'RIVALUTABILI, INDEX E UNIT'
 * --> FILEOUT 3
 * --> FILE DEGLI SCARTI
 * --> INPUT 4
 * --> IMPOSTA SOSTITUTIVA
 * --> INPUT 5
 * --> GRAVITA ERRORI
 * --> FILEOUT 6
 * --> KO E/C 'RIVALUTABILI, INDEX E UNIT'</pre>*/
public class Lrgs0660 extends BatchProgram {

    //==== PROPERTIES ====
    @Inject
    private IPointerManager pointerManager;
    public FileRecordBuffer filesqs1To = new FileRecordBuffer(Len.FILESQS1_REC);
    public FileBufferedDAO filesqs1DAO = new FileBufferedDAO(new FileAccessStatus(), "FILESQS1", Len.FILESQS1_REC);
    public FileRecordBuffer filesqs2To = new FileRecordBuffer(Len.FILESQS2_REC);
    public FileBufferedDAO filesqs2DAO = new FileBufferedDAO(new FileAccessStatus(), "FILESQS2", Len.FILESQS2_REC);
    public FileRecordBuffer filesqs3To = new FileRecordBuffer(Len.FILESQS3_REC);
    public FileBufferedDAO filesqs3DAO = new FileBufferedDAO(new FileAccessStatus(), "FILESQS3", Len.FILESQS3_REC);
    public FileRecordBuffer filesqs4To = new FileRecordBuffer(Len.FILESQS4_REC);
    public FileBufferedDAO filesqs4DAO = new FileBufferedDAO(new FileAccessStatus(), "FILESQS4", Len.FILESQS4_REC);
    public FileRecordBuffer filesqs5To = new FileRecordBuffer(Len.FILESQS5_REC);
    public FileBufferedDAO filesqs5DAO = new FileBufferedDAO(new FileAccessStatus(), "FILESQS5", Len.FILESQS5_REC);
    public FileRecordBuffer filesqs6To = new FileRecordBuffer(Len.FILESQS6_REC);
    public FileBufferedDAO filesqs6DAO = new FileBufferedDAO(new FileAccessStatus(), "FILESQS6", Len.FILESQS6_REC);
    //Original name: WORKING-STORAGE
    private Lrgs0660Data ws = new Lrgs0660Data();
    //Original name: AREA-IDSV0001
    private AreaIdsv0001 areaIdsv0001;
    //Original name: WCOM-IO-STATI
    private S0024AreaComune wcomIoStati;
    //Original name: IABV0006
    private Iabv0006 iabv0006;
    //Original name: WK-APPO-DATA-JOB
    private WkAppoDataJob wkAppoDataJob;

    //==== METHODS ====
    /**Original name: PROGRAM_LRGS0660_FIRST_SENTENCES<br>
	 * <pre>                         AREA-CONTATORI.
	 * ----------------------------------------------------------------*</pre>*/
    public long execute(AreaIdsv0001 areaIdsv0001, S0024AreaComune wcomIoStati, Iabv0006 iabv0006, WkAppoDataJob wkAppoDataJob) {
        this.areaIdsv0001 = areaIdsv0001;
        this.wcomIoStati = wcomIoStati;
        this.iabv0006 = iabv0006;
        this.wkAppoDataJob = wkAppoDataJob;
        // COB_CODE: IF IABV0006-ULTIMO-LANCIO
        //                     / IABV0006-CUSTOM-COUNT(REC-ELABORATI)
        //           ELSE
        //                 THRU EX-S9000
        //           END-IF.
        if (this.iabv0006.getTipoLancioBus().isIabv0006UltimoLancio()) {
            // COB_CODE: PERFORM CHIUSURA-FILE
            //              THRU CHIUSURA-FILE-EX
            chiusuraFile();
            // COB_CODE: COMPUTE  IABV0006-CUSTOM-COUNT(REC-PERC) =
            //                  (100 * (IABV0006-CUSTOM-COUNT(REC-ELABORATI)
            //                    - IABV0006-CUSTOM-COUNT(REC-SCARTATI)))
            //                  / IABV0006-CUSTOM-COUNT(REC-ELABORATI)
            this.iabv0006.getCustomCounters().getEleErrori(ws.getWkAreaContatori().getRecPerc()).setIabv0006CustomCount(Trunc.toInt(new AfDecimal(((((double)(100 * (this.iabv0006.getCustomCounters().getEleErrori(ws.getWkAreaContatori().getRecElaborati()).getIabv0006CustomCount() - this.iabv0006.getCustomCounters().getEleErrori(ws.getWkAreaContatori().getRecScartati()).getIabv0006CustomCount())))) / this.iabv0006.getCustomCounters().getEleErrori(ws.getWkAreaContatori().getRecElaborati()).getIabv0006CustomCount()), 13, 0), 9));
        }
        else {
            // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
            //              THRU EX-S0000
            s0000OperazioniIniziali();
            // COB_CODE: IF IDSV0001-ESITO-OK
            //                 THRU EX-S8000
            //           END-IF
            if (this.areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                // COB_CODE: PERFORM S8000-ELABORA
                //              THRU EX-S8000
                s8000Elabora();
            }
            // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
            //              THRU EX-S9000
            s9000OperazioniFinali();
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Lrgs0660 getInstance() {
        return ((Lrgs0660)Programs.getInstance(Lrgs0660.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *     OPERAZIONI INIZIALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: MOVE 'S0000-OPERAZIONI-INIZIALI'
        //             TO IDSV8888-AREA-DISPLAY.
        ws.getIdsv8888().getAreaDisplay().setAreaDisplay("S0000-OPERAZIONI-INIZIALI");
        // COB_CODE: PERFORM ESEGUI-DISPLAY
        //              THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        // COB_CODE: INITIALIZE WS-ERRORE
        //                      TRACCIATO-FILE.
        ws.getWsVariabili().setWsErrore("");
        initTracciatoFile();
        //
        // COB_CODE: PERFORM S0001-INIZIALIZZA
        //              THRU EX-S0001
        s0001Inizializza();
        //
        //--> COPY PER L'INIZIALIZZAZIONE AREA ERRORI
        //--> DELL'AREA CONTESTO.
        // CONTIENE UNA SEQUENZA DI ISTRUZIONI PER AZZERAR 'AREA DEGLI
        //ERRORI DELL'AREA CONTESTO.
        // COB_CODE: MOVE 'OK'   TO IDSV0001-ESITO
        areaIdsv0001.getEsito().setEsito("OK");
        // COB_CODE: MOVE SPACES TO IDSV0001-LOG-ERRORE
        areaIdsv0001.getLogErrore().initLogErroreSpaces();
        // COB_CODE: SET IDSV0001-FORZ-RC-04-NO        TO TRUE
        areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
        // COB_CODE: PERFORM VARYING IDSV0001-MAX-ELE-ERRORI FROM 1 BY 1
        //                     UNTIL IDSV0001-MAX-ELE-ERRORI > 10
        //               TO IDSV0001-TIPO-TRATT-FE(IDSV0001-MAX-ELE-ERRORI)
        //           END-PERFORM
        areaIdsv0001.setMaxEleErrori(((short)1));
        while (!(areaIdsv0001.getMaxEleErrori() > 10)) {
            // COB_CODE: MOVE SPACES
            //             TO IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
            areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore("");
            // COB_CODE: MOVE ZEROES
            //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
            areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErrore(0);
            // COB_CODE: MOVE ZEROES
            //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
            areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)0));
            // COB_CODE: MOVE SPACES
            //             TO IDSV0001-TIPO-TRATT-FE(IDSV0001-MAX-ELE-ERRORI)
            areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setTipoTrattFe(Types.SPACE_CHAR);
            areaIdsv0001.setMaxEleErrori(Trunc.toShort(areaIdsv0001.getMaxEleErrori() + 1, 4));
        }
        // COB_CODE: MOVE ZEROES TO IDSV0001-MAX-ELE-ERRORI.
        areaIdsv0001.setMaxEleErrori(((short)0));
        //
        // COB_CODE: SET PRIMO-RECORD-NO    TO TRUE
        ws.getAreaFlag().getFlagRecord0().setNo();
        //
        //--> APERTURA FILE DI OUTPUT
        // COB_CODE: IF IDSV0001-ESITO-OK
        //              END-IF
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: IF IABV0006-PRIMO-LANCIO
            //              END-IF
            //           END-IF
            if (iabv0006.getTipoLancioBus().isIabv0006PrimoLancio()) {
                // COB_CODE: PERFORM S8100-APERTURA-FLUSSI
                //              THRU EX-S8100
                s8100AperturaFlussi();
                //
                // COB_CODE: IF IDSV0001-ESITO-OK
                //                 THRU EX-S8120
                //           END-IF
                if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                    // COB_CODE: PERFORM S8120-SCRIVI-TST
                    //              THRU EX-S8120
                    s8120ScriviTst();
                }
                //
                // COB_CODE: IF IDSV0001-ESITO-OK
                //                 THRU EX-S8140
                //           END-IF
                if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                    // COB_CODE: PERFORM S8140-APERTURA-GRAV-ERR
                    //              THRU EX-S8140
                    s8140AperturaGravErr();
                }
            }
        }
    }

    /**Original name: S0001-INIZIALIZZA<br>
	 * <pre>----------------------------------------------------------------*
	 *    INIZIALIZZAZIONE VARIABILI
	 * ----------------------------------------------------------------*</pre>*/
    private void s0001Inizializza() {
        // COB_CODE: MOVE 'S0001-INIZIALIZZA'
        //             TO IDSV8888-AREA-DISPLAY.
        ws.getIdsv8888().getAreaDisplay().setAreaDisplay("S0001-INIZIALIZZA");
        // COB_CODE: INITIALIZE                       IX-INDICI
        //                                            WS-VARIABILI
        //                                            ELE-MAX-CACHE-3
        //                                            ELE-MAX-CACHE-10
        //                                            AREA-CACHE-8
        //                                            AREA-CACHE-9
        //                                            AREA-CACHE-10.
        initIxIndici();
        initWsVariabili();
        ws.setEleMaxCache3Formatted("000000000");
        ws.setEleMaxCache10Formatted("000000000");
        initAreaCache8();
        initAreaCache9();
        initAreaCache10();
        // COB_CODE: SET CNTRL-37-ERR-NO TO TRUE.
        ws.getAreaFlag().getFlagCntrl37().setNo();
        // COB_CODE: SET CNTRL-34-ERR-NO TO TRUE.
        ws.getAreaFlag().getFlagCntrl34().setNo();
        // COB_CODE: SET INTESTAZIONE-SI TO TRUE.
        ws.getAreaFlag().getFlagIntErr().setSi();
    }

    /**Original name: S8000-ELABORA<br>
	 * <pre>----------------------------------------------------------------*
	 *    Elaboriamo l'INPUT (OUTPUT del processo di Estratto Conto)   *
	 *    e lo riscriviamo dopo opportuni controlli                    *
	 * ----------------------------------------------------------------*</pre>*/
    private void s8000Elabora() {
        // COB_CODE: MOVE 'S8000-ELABORA'
        //             TO IDSV8888-AREA-DISPLAY.
        ws.getIdsv8888().getAreaDisplay().setAreaDisplay("S8000-ELABORA");
        // COB_CODE: PERFORM ESEGUI-DISPLAY
        //              THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: SET WRITE-SI TO TRUE
        ws.getAreaFlag().getFlagScrittura().setSi();
        // COB_CODE: SET PRIMO-RECORD-NO TO TRUE
        ws.getAreaFlag().getFlagRecord0().setNo();
        //--> FASE DI CREAZIONE DELLE CACHE
        // COB_CODE: IF IDSV0001-ESITO-OK
        //                   OR IDSV0001-ESITO-KO
        //           END-IF
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: PERFORM S9600-SCORRI-INPUT
            //              THRU EX-S9600
            //           VARYING IX-IND FROM 1 BY 1
            //             UNTIL IX-IND > WK-APPO-ELE-MAX
            //                OR IDSV0001-ESITO-KO
            ws.getIxIndici().setInd(((short)1));
            while (!(ws.getIxIndici().getInd() > wkAppoDataJob.getEleMax() || areaIdsv0001.getEsito().isIdsv0001EsitoKo())) {
                s9600ScorriInput();
                ws.getIxIndici().setInd(Trunc.toShort(ws.getIxIndici().getInd() + 1, 4));
            }
        }
        //--> FASE DEI CONTROLLI
        // COB_CODE: IF IDSV0001-ESITO-OK
        //           AND PRIMO-RECORD-NO
        //                 THRU EX-S9700
        //           END-IF
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk() && ws.getAreaFlag().getFlagRecord0().isNo()) {
            // COB_CODE: PERFORM S9700-CONTROLLI
            //              THRU EX-S9700
            s9700Controlli();
        }
        // COB_CODE:      IF WRITE-SI
        //                OR PRIMO-RECORD-SI
        //           *--> FASE DI SCRITTURA DELL'E/C CORRETTI
        //                        OR IDSV0001-ESITO-KO
        //                ELSE
        //           *--> FASE DI SCRITTURA SCARTI
        //                        OR IDSV0001-ESITO-KO
        //                END-IF.
        if (ws.getAreaFlag().getFlagScrittura().isSi() || ws.getAreaFlag().getFlagRecord0().isSi()) {
            //--> FASE DI SCRITTURA DELL'E/C CORRETTI
            // COB_CODE: PERFORM W100-SCRIVI-FILE
            //              THRU EX-W100
            //           VARYING IX-IND FROM 1 BY 1
            //             UNTIL IX-IND > WK-APPO-ELE-MAX
            //                OR IDSV0001-ESITO-KO
            ws.getIxIndici().setInd(((short)1));
            while (!(ws.getIxIndici().getInd() > wkAppoDataJob.getEleMax() || areaIdsv0001.getEsito().isIdsv0001EsitoKo())) {
                w100ScriviFile();
                ws.getIxIndici().setInd(Trunc.toShort(ws.getIxIndici().getInd() + 1, 4));
            }
        }
        else {
            //--> FASE DI SCRITTURA SCARTI
            // COB_CODE: PERFORM W200-SCRIVI-SCARTI
            //              THRU EX-W200
            w200ScriviScarti();
            //--> FASE DI SCRITTURA DELL'E/C KO
            // COB_CODE: PERFORM W999-SCRIVI-FILE-KO
            //              THRU EX-W999
            //           VARYING IX-IND FROM 1 BY 1
            //             UNTIL IX-IND > WK-APPO-ELE-MAX
            //                OR IDSV0001-ESITO-KO
            ws.getIxIndici().setInd(((short)1));
            while (!(ws.getIxIndici().getInd() > wkAppoDataJob.getEleMax() || areaIdsv0001.getEsito().isIdsv0001EsitoKo())) {
                w999ScriviFileKo();
                ws.getIxIndici().setInd(Trunc.toShort(ws.getIxIndici().getInd() + 1, 4));
            }
        }
    }

    /**Original name: S8100-APERTURA-FLUSSI<br>
	 * <pre>----------------------------------------------------------------*
	 *    APERTURA DEI FILE DI INPUT/OUTPUT                            *
	 * ----------------------------------------------------------------*</pre>*/
    private void s8100AperturaFlussi() {
        // COB_CODE: MOVE 'S8100-APERTURA-FLUSSI'                 TO WK-LABEL.
        ws.getWsVariabili().setWkLabel("S8100-APERTURA-FLUSSI");
        // COB_CODE: SET OPEN-FILESQS-OPER             TO TRUE
        ws.getIabvsqs1().getOperazioneFilesqs().setOpenFilesqsOper();
        // COB_CODE: SET OPEN-FILESQS1-SI              TO TRUE
        ws.getIabvsqs1().getFlagOpenFilesqs1().setOpenFilesqs1Si();
        // COB_CODE: SET OPEN-FILESQS2-SI              TO TRUE
        ws.getIabvsqs1().getFlagOpenFilesqs2().setOpenFilesqs2Si();
        // COB_CODE: SET OPEN-FILESQS3-SI              TO TRUE
        ws.getIabvsqs1().getFlagOpenFilesqs3().setOpenFilesqs3Si();
        // COB_CODE: SET OPEN-FILESQS4-NO              TO TRUE
        ws.getIabvsqs1().getFlagOpenFilesqs4().setOpenFilesqs4No();
        // COB_CODE: SET OPEN-FILESQS6-SI              TO TRUE
        ws.getIabvsqs1().getFlagOpenFilesqs6().setOpenFilesqs6Si();
        // COB_CODE: SET OUTPUT-FILESQS1               TO TRUE
        ws.getIabvsqs1().getOpenTypeFilesqs1().setOutputFilesqsAll();
        // COB_CODE: SET OUTPUT-FILESQS2               TO TRUE
        ws.getIabvsqs1().getOpenTypeFilesqs2().setOutputFilesqsAll();
        // COB_CODE: SET OUTPUT-FILESQS3               TO TRUE
        ws.getIabvsqs1().getOpenTypeFilesqs3().setOutputFilesqsAll();
        // COB_CODE: SET OUTPUT-FILESQS6               TO TRUE
        ws.getIabvsqs1().getOpenTypeFilesqs6().setOutputFilesqsAll();
        // COB_CODE: PERFORM CALL-FILESEQ              THRU CALL-FILESEQ-EX
        callFileseq();
        // COB_CODE: IF NOT FILE-STATUS-OK
        //                 THRU EX-S0300
        //           END-IF.
        if (!ws.getIabcsq99().getFileStatus().isFileStatusOk()) {
            // COB_CODE: MOVE WK-PGM                    TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL                  TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWsVariabili().getWkLabel());
            // COB_CODE: MOVE '005166'                  TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005166");
            // COB_CODE: MOVE SPACES                    TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("");
            // COB_CODE: MOVE SEQUENTIAL-DESC-ERRORE-ESTESA
            //                                          TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr(ws.getIabcsq99().getSequentialDescErroreEstesa());
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: S9600-SCORRI-INPUT<br>
	 * <pre>----------------------------------------------------------------*
	 *    Scorriamo l'INPUT (OUTPUT del processo di Estratto Conto)    *
	 *    e totalizziamo alcuni valori                                 *
	 * ----------------------------------------------------------------*</pre>*/
    private void s9600ScorriInput() {
        // COB_CODE: MOVE 'S9600-SCORRI-INPUT'                 TO WK-LABEL.
        ws.getWsVariabili().setWkLabel("S9600-SCORRI-INPUT");
        // COB_CODE: INITIALIZE WRIN-REC-FILEIN.
        initWrinRecFilein();
        // COB_CODE: MOVE WK-APPO-BLOB-DATA-REC(IX-IND)        TO WRIN-REC-FILEIN.
        ws.setWrinRecFileinFormatted(wkAppoDataJob.getBlobData(ws.getIxIndici().getInd()).getWkAppoBlobDataRecFormatted());
        // COB_CODE:      EVALUATE WRIN-TP-RECORD
        //                    WHEN '0000'
        //                          END-IF
        //                    WHEN '0001'
        //                          SET CNTRL-37-ERR-NO TO TRUE
        //                    WHEN '0002'
        //                            TO REC2-NOME-ASS
        //                    WHEN '0003'
        //           *--->    MEMORIZZAZIONE DEL RECORD 3
        //                        END-IF
        //                    WHEN '0005'
        //                         MOVE IX-IND              TO IX-REC-5
        //                    WHEN '0008'
        //           *--->    MEMORIZZAZIONE DELLE SOMME DEL RECORD 8
        //                         END-IF
        //           *--> FLAG PER CONTROLLO 37
        //           *             COMPUTE WK-CNTRL-37 =
        //           *                     (WRIN-NUM-QUO-PREC -
        //           *                      WRIN-NUM-QUO-DISINV-RISC-PARZ -
        //           *                      WRIN-NUM-QUO-DIS-RISC-PARZ-PRG -
        //           *                      WRIN-NUM-QUO-DISINV-IMPOS-SOST -
        //           *                      WRIN-NUM-QUO-DISINV-COMMGES -
        //           *                      WRIN-NUM-QUO-DISINV-PREL-COSTI -
        //           *                      WRIN-NUM-QUO-DISINV-SWITCH +
        //           *                      WRIN-NUM-QUO-INVST-SWITCH +
        //           *                      WRIN-NUM-QUO-INVST-VERS +
        //           *                      WRIN-NUM-QUO-INVST-REBATE  +
        //           *                      WRIN-NUM-QUO-INVST-PURO-RISC) -
        //           *                      WRIN-NUM-QUO-ATTUALE
        //           *
        //           *--> FLAG PER CONTROLLO 37
        //           *            IF WK-CNTRL-37 > 0,01
        //           *            OR WK-CNTRL-37 < -0,01
        //           *               SET CNTRL-37-ERR-SI TO TRUE
        //           *            ELSE
        //           *               CONTINUE
        //           *            END-IF
        //           *            END-IF
        //                    WHEN '0009'
        //           *--> PER IL CONTROLLO 38 VIENE LASCITO IL CUMULO DEL RISCATTO
        //           *--> DI PRIMA
        //                           THRU EX-C915
        //                    WHEN '0010'
        //           *--->    MEMORIZZAZIONE DEL RECORD 10
        //                             THRU EX-C200
        //                    WHEN OTHER
        //                         CONTINUE
        //                END-EVALUATE.
        switch (ws.getWrinRecOutput().getWrinTpRecord()) {

            case "0000":// COB_CODE: SET PRIMO-RECORD-SI TO TRUE
                ws.getAreaFlag().getFlagRecord0().setSi();
                // COB_CODE: INITIALIZE DATE-WORK
                initDateWork();
                // COB_CODE: MOVE WRIN-DATA-RICH-DA TO WK-DT-RICOR-DA-X
                ws.getDateWork().getWkDtRicorDaX().setWkDtRicorDaX(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinDataRichDa());
                // COB_CODE: MOVE WRIN-DATA-RICH-A  TO WK-DT-RICOR-A-X
                ws.getDateWork().getWkDtRicorAX().setWkDtRicorAX(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinDataRichA());
                // COB_CODE: MOVE WRIN-ANNO-RIF     TO WK-ANNO-RIF-X
                ws.getDateWork().getWkAnnoRifX().setWkAnnoRifX(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinAnnoRif());
                // COB_CODE: IF (WK-ANNO-RIF > 2014)
                //              SET CNTRL-34-NO TO TRUE
                //           ELSE
                //              SET CNTRL-34-SI TO TRUE
                //           END-IF
                if (ws.getDateWork().getWkAnnoRifX().getWkAnnoRif() > 2014) {
                    // COB_CODE: SET CNTRL-34-NO TO TRUE
                    ws.getAreaFlag().getFlagCntrlEsec34().setNo();
                }
                else {
                    // COB_CODE: SET CNTRL-34-SI TO TRUE
                    ws.getAreaFlag().getFlagCntrlEsec34().setSi();
                }
                // COB_CODE: MOVE WRIN-TP-INVST      TO TIPOLOGIA-EC
                ws.getTipologiaEc().setTipologiaEc(ws.getWrinRecOutput().getWrinTpInvst());
                // COB_CODE: IF TP-ESTR-RV
                //                 THRU S100-CALL-ACTUATOR-EX
                //           END-IF
                if (ws.getTipologiaEc().isRv()) {
                    // COB_CODE: PERFORM S100-CALL-ACTUATOR
                    //              THRU S100-CALL-ACTUATOR-EX
                    s100CallActuator();
                }
                // COB_CODE: IF IDSV0001-ESITO-OK
                //                 THRU EX-W999
                //           END-IF
                if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                    // COB_CODE: PERFORM W999-SCRIVI-FILE-KO
                    //              THRU EX-W999
                    w999ScriviFileKo();
                }
                break;

            case "0001":// COB_CODE: INITIALIZE              WK-AREA-FILE
                initWkAreaFile();
                // COB_CODE: MOVE 0                  TO WK-TASSO-TEC-RV
                ws.getWorkCalcoli().setWkTassoTecRv(Trunc.toDecimal(0, 14, 9));
                // COB_CODE: DISPLAY 'LRGS0660 - POLIZZA:' WRIN-IB-POLI-IND
                DisplayUtil.sysout.write("LRGS0660 - POLIZZA:", ws.getWrinRecOutput().getWrinIbPoliIndFormatted());
                // COB_CODE: DISPLAY 'LRGS0660 - ID-POLI:' WRIN-ID-POLI
                DisplayUtil.sysout.write("LRGS0660 - ID-POLI:", ws.getWrinRecOutput().getWrinTpRecordDati().getWrinIdPoliAsString());
                // COB_CODE: MOVE WRIN-ID-POLI       TO WK-ID-POLI
                ws.getWkAreaFile().setIdPoliFormatted(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinIdPoliFormatted());
                // COB_CODE: MOVE WRIN-ID-ADES       TO WK-ID-ADES
                ws.getWkAreaFile().setIdAdesFormatted(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinIdAdesFormatted());
                // COB_CODE: MOVE WRIN-IB-POLI-IND   TO IABV0006-IB-OGG-POLI
                //                                      OUT-IB-OGG
                //                                      WK-IB-OGG
                iabv0006.getReport().setIbOggPoli(ws.getWrinRecOutput().getWrinIbPoliInd());
                ws.getTracciatoFile().setOutIbOgg(ws.getWrinRecOutput().getWrinIbPoliInd());
                ws.getWkAreaFile().setIbOgg(ws.getWrinRecOutput().getWrinIbPoliInd());
                // COB_CODE: MOVE WRIN-DT-RIF-DA     TO WK-DT-INF
                ws.getWsVariabili().setWkDtInfFormatted(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinDtRifDaFormatted());
                // COB_CODE: MOVE WRIN-DT-RIF-A      TO WK-DT-SUP
                ws.getWsVariabili().setWkDtSupFormatted(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinDtRifAFormatted());
                // COB_CODE: MOVE WRIN-IB-ADES       TO IABV0006-IB-OGG-ADES
                iabv0006.getReport().setIbOggAdes(ws.getWrinRecOutput().getWrinIbAdes());
                // COB_CODE: MOVE WRIN-DT-DECOR-POLI
                //             TO WK-DT-DECOR
                ws.getWsVariabili().setWkDtDecor(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinDtDecorPoli());
                // COB_CODE: ADD 1     TO IABV0006-CUSTOM-COUNT(REC-ELABORATI)
                iabv0006.getCustomCounters().getEleErrori(ws.getWkAreaContatori().getRecElaborati()).setIabv0006CustomCount(Trunc.toInt(1 + iabv0006.getCustomCounters().getEleErrori(ws.getWkAreaContatori().getRecElaborati()).getIabv0006CustomCount(), 9));
                // COB_CODE: MOVE 0    TO IX-C37-TAB
                ws.getIxIndici().setC37Tab(((short)0));
                // COB_CODE: MOVE 0    TO IX-MAX-C37
                ws.setIxMaxC37(0);
                // COB_CODE: MOVE 0    TO IX-MAX-C35
                ws.setIxMaxC35(0);
                // COB_CODE: SET CNTRL-34-ERR-NO TO TRUE
                ws.getAreaFlag().getFlagCntrl34().setNo();
                // COB_CODE: SET CNTRL-37-ERR-NO TO TRUE
                ws.getAreaFlag().getFlagCntrl37().setNo();
                break;

            case "0002":// COB_CODE: MOVE WRIN-COGNOME-ASSICU
                //             TO REC2-COGNOME-ASS
                ws.getWsVariabili().setRec2CognomeAss(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinCognomeAssicu());
                // COB_CODE: MOVE WRIN-NOME-ASSICU
                //             TO REC2-NOME-ASS
                ws.getWsVariabili().setRec2NomeAss(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinNomeAssicu());
                break;

            case "0003"://--->    MEMORIZZAZIONE DEL RECORD 3
                // COB_CODE:                IF TP-ESTR-RV
                //           *                 IF WRIN-TP-GAR = 1
                //                             END-IF
                //                          END-IF
                if (ws.getTipologiaEc().isRv()) {
                    //                 IF WRIN-TP-GAR = 1
                    // COB_CODE: IF WRIN-TP-GAR = ( 1 OR 5 )
                    //           AND WRIN-TP-INVST-GAR = 4
                    //                TO WK-FRAZ
                    //           END-IF
                    if ((ws.getWrinRecOutput().getWrinTpRecordDati().getWrinTpGar() == 1 || ws.getWrinRecOutput().getWrinTpRecordDati().getWrinTpGar() == 5) && ws.getWrinRecOutput().getWrinTpRecordDati().getWrinTpInvstGar() == 4) {
                        // COB_CODE: MOVE WRIN-FRAZIONAMENTO
                        //             TO WK-FRAZ
                        ws.getWsVariabili().setWkFrazFormatted(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinFrazionamentoFormatted());
                    }
                }
                // COB_CODE: PERFORM C100-CACHE-REC-3
                //              THRU EX-C100
                c100CacheRec3();
                //
                //--> FLAG PER CONTROLLO 34
                // COB_CODE: IF  (WRIN-COD-GAR = 'TRVU1'
                //                OR WRIN-COD-GAR = 'TRVUD')
                //               END-PERFORM
                //           ELSE
                //              CONTINUE
                //           END-IF
                if (Conditions.eq(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinCodGar(), "TRVU1") || Conditions.eq(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinCodGar(), "TRVUD")) {
                    // COB_CODE: MOVE WK-ANNO-RIF
                    //             TO WK-DT-FIN-AA
                    ws.getDateWork().getWkFinRic().setAaFormatted(ws.getDateWork().getWkAnnoRifX().getWkAnnoRifFormatted());
                    // COB_CODE: MOVE 12 TO  WK-DT-INI-MM  WK-DT-FIN-MM
                    ws.getDateWork().getWkIniRic().setMm(((short)12));
                    ws.getDateWork().getWkFinRic().setMm(((short)12));
                    // COB_CODE: MOVE 31 TO  WK-DT-INI-GG  WK-DT-FIN-GG
                    ws.getDateWork().getWkIniRic().setGg(((short)31));
                    ws.getDateWork().getWkFinRic().setGg(((short)31));
                    // COB_CODE: COMPUTE WK-DT-INI-AA = WK-ANNO-RIF - 1
                    ws.getDateWork().getWkIniRic().setAa(Trunc.toShort(abs(ws.getDateWork().getWkAnnoRifX().getWkAnnoRif() - 1), 4));
                    // COB_CODE:                  PERFORM VARYING IX-CNTR-34 FROM 1 BY 1
                    //                                      UNTIL IX-CNTR-34 > 12
                    //                                        OR CNTRL-34-ERR-SI
                    //           *--> DT-fin-PER-INV >= dt_ini_elab
                    //           *--> && DT_INI_PER_INV <= DT_END_ELAB
                    //                            END-IF
                    //                            END-PERFORM
                    ws.getIxIndici().setCntr34(((short)1));
                    while (!(ws.getIxIndici().getCntr34() > 12 || ws.getAreaFlag().getFlagCntrl34().isSi())) {
                        //--> DT-fin-PER-INV >= dt_ini_elab
                        //--> && DT_INI_PER_INV <= DT_END_ELAB
                        // COB_CODE: IF  WRIN-DT-INI-PER-INV(IX-CNTR-34) NOT EQUAL
                        //               SPACES AND LOW-VALUES AND HIGH-VALUES
                        //             END-IF
                        //           ELSE
                        //             END-IF
                        //           END-IF
                        if (!Characters.EQ_SPACE.test(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinDtIniPerInv(ws.getIxIndici().getCntr34())) && !Characters.EQ_LOW.test(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinDtIniPerInvFormatted(ws.getIxIndici().getCntr34())) && !Characters.EQ_HIGH.test(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinDtIniPerInvFormatted(ws.getIxIndici().getCntr34()))) {
                            // COB_CODE: IF  WRIN-DT-FIN-PER-INV(IX-CNTR-34) >=
                            //               WK-INI-RIC
                            //           AND WRIN-DT-INI-PER-INV(IX-CNTR-34) <=
                            //               WK-FIN-RIC
                            //               CONTINUE
                            //           ELSE
                            //               SET CNTRL-34-ERR-SI TO TRUE
                            //           END-IF
                            if (Conditions.gte(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinDtFinPerInv(ws.getIxIndici().getCntr34()), ws.getDateWork().getWkIniRic().getWkIniRicBytes()) && Conditions.lte(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinDtIniPerInv(ws.getIxIndici().getCntr34()), ws.getDateWork().getWkFinRic().getWkFinRicBytes())) {
                            // COB_CODE: CONTINUE
                            //continue
                            }
                            else {
                                // COB_CODE: SET CNTRL-34-ERR-SI TO TRUE
                                ws.getAreaFlag().getFlagCntrl34().setSi();
                            }
                        }
                        else if (ws.getIxIndici().getCntr34() < 2) {
                            // COB_CODE: IF IX-CNTR-34 < 2
                            //              SET CNTRL-34-ERR-SI TO TRUE
                            //           END-IF
                            // COB_CODE: SET CNTRL-34-ERR-SI TO TRUE
                            ws.getAreaFlag().getFlagCntrl34().setSi();
                        }
                        ws.getIxIndici().setCntr34(Trunc.toShort(ws.getIxIndici().getCntr34() + 1, 4));
                    }
                }
                else {
                // COB_CODE: CONTINUE
                //continue
                }
                break;

            case "0005":// COB_CODE: MOVE IX-IND              TO IX-REC-5
                ws.getIxIndici().setRec5(ws.getIxIndici().getInd());
                break;

            case "0008"://--->    MEMORIZZAZIONE DELLE SOMME DEL RECORD 8
                // COB_CODE: PERFORM C300-CACHE-REC-8
                //              THRU EX-C300
                c300CacheRec8();
                // COB_CODE: ADD 1 TO IX-C35-TAB
                ws.getIxIndici().setC35Tab(Trunc.toShort(1 + ws.getIxIndici().getC35Tab(), 4));
                // COB_CODE: MOVE WRIN-ID-GAR-FND
                //             TO C35-ID-GAR(IX-C35-TAB)
                ws.getEleCache35(ws.getIxIndici().getC35Tab()).setIdGarFormatted(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinIdGarFndFormatted());
                // COB_CODE: MOVE WRIN-CNTRVAL-PREC
                //             TO C35-CNTRVAL-PREC(IX-C35-TAB)
                ws.getEleCache35(ws.getIxIndici().getC35Tab()).setCntrvalPrec(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinCntrvalPrec(), 15, 3));
                // COB_CODE: MOVE IX-C35-TAB TO IX-MAX-C35
                ws.setIxMaxC35(ws.getIxIndici().getC35Tab());
                // COB_CODE: SET TROVATO-37-NO TO TRUE
                ws.getAreaFlag().getErrore37Tr().setNo();
                //              MOVE IX-C37-TAB TO IX-MAX-C37
                // COB_CODE:                PERFORM VARYING IX-C37-TAB FROM 1 BY 1
                //                            UNTIL IX-C37-TAB > IX-MAX-C37
                //                               END-IF
                //           *
                //                             END-PERFORM
                ws.getIxIndici().setC37Tab(((short)1));
                while (!(ws.getIxIndici().getC37Tab() > ws.getIxMaxC37())) {
                    // COB_CODE: IF WRIN-COD-FND =
                    //               C37-COD-FND(IX-C37-TAB)
                    //             C37-NUM-QUO-INVST-COMP-NAV(IX-C37-TAB)
                    //            END-IF
                    if (Conditions.eq(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinCodFnd(), ws.getEleCache37(ws.getIxIndici().getC37Tab()).getCodFnd())) {
                        // COB_CODE: SET TROVATO-37-SI TO TRUE
                        ws.getAreaFlag().getErrore37Tr().setSi();
                        // COB_CODE: COMPUTE C37-NUM-QUO-PREC(IX-C37-TAB) =
                        //                   WRIN-NUM-QUO-PREC +
                        //               C37-NUM-QUO-PREC(IX-C37-TAB)
                        ws.getEleCache37(ws.getIxIndici().getC37Tab()).setNumQuoPrec(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinNumQuoPrec().add(ws.getEleCache37(ws.getIxIndici().getC37Tab()).getNumQuoPrec()), 12, 5));
                        // COB_CODE: COMPUTE C37-NUM-QUO-DISINV-RISC-PARZ(IX-C37-TAB)=
                        //                   WRIN-NUM-QUO-DISINV-RISC-PARZ +
                        //                 C37-NUM-QUO-DISINV-RISC-PARZ(IX-C37-TAB)
                        ws.getEleCache37(ws.getIxIndici().getC37Tab()).setNumQuoDisinvRiscParz(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinNumQuoDisinvRiscParz().add(ws.getEleCache37(ws.getIxIndici().getC37Tab()).getNumQuoDisinvRiscParz()), 12, 5));
                        // COB_CODE: COMPUTE C37-NUM-QUO-DIS-RISC-PARZ-PRG(IX-C37-TAB)=
                        //                   WRIN-NUM-QUO-DIS-RISC-PARZ-PRG +
                        //                 C37-NUM-QUO-DIS-RISC-PARZ-PRG(IX-C37-TAB)
                        ws.getEleCache37(ws.getIxIndici().getC37Tab()).setNumQuoDisRiscParzPrg(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinNumQuoDisRiscParzPrg().add(ws.getEleCache37(ws.getIxIndici().getC37Tab()).getNumQuoDisRiscParzPrg()), 12, 5));
                        // COB_CODE: COMPUTE C37-NUM-QUO-DISINV-IMPOS-SOST(IX-C37-TAB)=
                        //                     WRIN-NUM-QUO-DISINV-IMPOS-SOST +
                        //                   C37-NUM-QUO-DISINV-IMPOS-SOST(IX-C37-TAB)
                        ws.getEleCache37(ws.getIxIndici().getC37Tab()).setNumQuoDisinvImposSost(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinNumQuoDisinvImposSost().add(ws.getEleCache37(ws.getIxIndici().getC37Tab()).getNumQuoDisinvImposSost()), 12, 5));
                        // COB_CODE: COMPUTE C37-NUM-QUO-DISINV-COMMGES(IX-C37-TAB)=
                        //                    WRIN-NUM-QUO-DISINV-COMMGES +
                        //               C37-NUM-QUO-DISINV-COMMGES(IX-C37-TAB)
                        ws.getEleCache37(ws.getIxIndici().getC37Tab()).setNumQuoDisinvCommges(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinNumQuoDisinvCommges().add(ws.getEleCache37(ws.getIxIndici().getC37Tab()).getNumQuoDisinvCommges()), 12, 5));
                        // COB_CODE: COMPUTE C37-NUM-QUO-DISINV-PREL-COSTI(IX-C37-TAB)=
                        //                    WRIN-NUM-QUO-DISINV-PREL-COSTI +
                        //              C37-NUM-QUO-DISINV-PREL-COSTI(IX-C37-TAB)
                        ws.getEleCache37(ws.getIxIndici().getC37Tab()).setNumQuoDisinvPrelCosti(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinNumQuoDisinvPrelCosti().add(ws.getEleCache37(ws.getIxIndici().getC37Tab()).getNumQuoDisinvPrelCosti()), 12, 5));
                        // COB_CODE: COMPUTE C37-NUM-QUO-DISINV-SWITCH(IX-C37-TAB)=
                        //                  WRIN-NUM-QUO-DISINV-SWITCH +
                        //               C37-NUM-QUO-DISINV-SWITCH(IX-C37-TAB)
                        ws.getEleCache37(ws.getIxIndici().getC37Tab()).setNumQuoDisinvSwitch(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinNumQuoDisinvSwitch().add(ws.getEleCache37(ws.getIxIndici().getC37Tab()).getNumQuoDisinvSwitch()), 12, 5));
                        // COB_CODE: COMPUTE C37-NUM-QUO-INVST-SWITCH(IX-C37-TAB)=
                        //                WRIN-NUM-QUO-INVST-SWITCH +
                        //             C37-NUM-QUO-INVST-SWITCH(IX-C37-TAB)
                        ws.getEleCache37(ws.getIxIndici().getC37Tab()).setNumQuoInvstSwitch(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinNumQuoInvstSwitch().add(ws.getEleCache37(ws.getIxIndici().getC37Tab()).getNumQuoInvstSwitch()), 12, 5));
                        // COB_CODE: COMPUTE C37-NUM-QUO-INVST-VERS(IX-C37-TAB)=
                        //              WRIN-NUM-QUO-INVST-VERS +
                        //             C37-NUM-QUO-INVST-VERS(IX-C37-TAB)
                        ws.getEleCache37(ws.getIxIndici().getC37Tab()).setNumQuoInvstVers(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinNumQuoInvstVers().add(ws.getEleCache37(ws.getIxIndici().getC37Tab()).getNumQuoInvstVers()), 12, 5));
                        // COB_CODE: COMPUTE C37-NUM-QUO-INVST-REBATE(IX-C37-TAB)=
                        //                    WRIN-NUM-QUO-INVST-REBATE +
                        //                  C37-NUM-QUO-INVST-REBATE(IX-C37-TAB)
                        ws.getEleCache37(ws.getIxIndici().getC37Tab()).setNumQuoInvstRebate(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinNumQuoInvstRebate().add(ws.getEleCache37(ws.getIxIndici().getC37Tab()).getNumQuoInvstRebate()), 12, 5));
                        // COB_CODE: COMPUTE C37-NUM-QUO-INVST-PURO-RISC(IX-C37-TAB)=
                        //                    WRIN-NUM-QUO-INVST-PURO-RISC +
                        //                C37-NUM-QUO-INVST-PURO-RISC(IX-C37-TAB)
                        ws.getEleCache37(ws.getIxIndici().getC37Tab()).setNumQuoInvstPuroRisc(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinNumQuoInvstPuroRisc().add(ws.getEleCache37(ws.getIxIndici().getC37Tab()).getNumQuoInvstPuroRisc()), 12, 5));
                        // COB_CODE: COMPUTE C37-NUM-QUO-ATTUALE(IX-C37-TAB) =
                        //                     WRIN-NUM-QUO-ATTUALE +
                        //                   C37-NUM-QUO-ATTUALE(IX-C37-TAB)
                        ws.getEleCache37(ws.getIxIndici().getC37Tab()).setNumQuoAttuale(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinNumQuoAttuale().add(ws.getEleCache37(ws.getIxIndici().getC37Tab()).getNumQuoAttuale()), 12, 5));
                        // COB_CODE: COMPUTE C37-NUM-QUO-DISINV-COMP-NAV(IX-C37-TAB)=
                        //                    WRIN-NUM-QUO-DISINV-COMP-NAV +
                        //               C37-NUM-QUO-DISINV-COMP-NAV(IX-C37-TAB)
                        ws.getEleCache37(ws.getIxIndici().getC37Tab()).setNumQuoDisinvCompNav(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinNumQuoDisinvCompNav().add(ws.getEleCache37(ws.getIxIndici().getC37Tab()).getNumQuoDisinvCompNav()), 12, 5));
                        // COB_CODE: COMPUTE C37-NUM-QUO-INVST-COMP-NAV(IX-C37-TAB) =
                        //                    WRIN-NUM-QUO-INVST-COMP-NAV +
                        //                  C37-NUM-QUO-INVST-COMP-NAV(IX-C37-TAB)
                        ws.getEleCache37(ws.getIxIndici().getC37Tab()).setNumQuoInvstCompNav(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinNumQuoInvstCompNav().add(ws.getEleCache37(ws.getIxIndici().getC37Tab()).getNumQuoInvstCompNav()), 12, 5));
                    }
                    //
                    ws.getIxIndici().setC37Tab(Trunc.toShort(ws.getIxIndici().getC37Tab() + 1, 4));
                }
                // COB_CODE:  IF TROVATO-37-NO
                //               MOVE IX-C37-TAB TO IX-MAX-C37
                //           END-IF
                if (ws.getAreaFlag().getErrore37Tr().isNo()) {
                    // COB_CODE: MOVE WRIN-COD-FND
                    //             TO C37-COD-FND(IX-C37-TAB)
                    ws.getEleCache37(ws.getIxIndici().getC37Tab()).setCodFnd(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinCodFnd());
                    // COB_CODE: MOVE WRIN-NUM-QUO-PREC
                    //             TO C37-NUM-QUO-PREC(IX-C37-TAB)
                    ws.getEleCache37(ws.getIxIndici().getC37Tab()).setNumQuoPrec(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinNumQuoPrec(), 12, 5));
                    // COB_CODE: MOVE WRIN-NUM-QUO-DISINV-RISC-PARZ
                    //             TO C37-NUM-QUO-DISINV-RISC-PARZ(IX-C37-TAB)
                    ws.getEleCache37(ws.getIxIndici().getC37Tab()).setNumQuoDisinvRiscParz(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinNumQuoDisinvRiscParz(), 12, 5));
                    // COB_CODE: MOVE WRIN-NUM-QUO-DIS-RISC-PARZ-PRG
                    //             TO C37-NUM-QUO-DIS-RISC-PARZ-PRG(IX-C37-TAB)
                    ws.getEleCache37(ws.getIxIndici().getC37Tab()).setNumQuoDisRiscParzPrg(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinNumQuoDisRiscParzPrg(), 12, 5));
                    // COB_CODE: MOVE WRIN-NUM-QUO-DISINV-IMPOS-SOST
                    //             TO C37-NUM-QUO-DISINV-IMPOS-SOST(IX-C37-TAB)
                    ws.getEleCache37(ws.getIxIndici().getC37Tab()).setNumQuoDisinvImposSost(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinNumQuoDisinvImposSost(), 12, 5));
                    // COB_CODE: MOVE WRIN-NUM-QUO-DISINV-COMMGES
                    //             TO C37-NUM-QUO-DISINV-COMMGES(IX-C37-TAB)
                    ws.getEleCache37(ws.getIxIndici().getC37Tab()).setNumQuoDisinvCommges(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinNumQuoDisinvCommges(), 12, 5));
                    // COB_CODE: MOVE WRIN-NUM-QUO-DISINV-PREL-COSTI
                    //             TO C37-NUM-QUO-DISINV-PREL-COSTI(IX-C37-TAB)
                    ws.getEleCache37(ws.getIxIndici().getC37Tab()).setNumQuoDisinvPrelCosti(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinNumQuoDisinvPrelCosti(), 12, 5));
                    // COB_CODE: MOVE WRIN-NUM-QUO-DISINV-SWITCH
                    //             TO C37-NUM-QUO-DISINV-SWITCH(IX-C37-TAB)
                    ws.getEleCache37(ws.getIxIndici().getC37Tab()).setNumQuoDisinvSwitch(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinNumQuoDisinvSwitch(), 12, 5));
                    // COB_CODE: MOVE WRIN-NUM-QUO-INVST-SWITCH
                    //             TO C37-NUM-QUO-INVST-SWITCH(IX-C37-TAB)
                    ws.getEleCache37(ws.getIxIndici().getC37Tab()).setNumQuoInvstSwitch(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinNumQuoInvstSwitch(), 12, 5));
                    // COB_CODE: MOVE WRIN-NUM-QUO-INVST-VERS
                    //             TO C37-NUM-QUO-INVST-VERS(IX-C37-TAB)
                    ws.getEleCache37(ws.getIxIndici().getC37Tab()).setNumQuoInvstVers(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinNumQuoInvstVers(), 12, 5));
                    // COB_CODE: MOVE WRIN-NUM-QUO-INVST-REBATE
                    //             TO C37-NUM-QUO-INVST-REBATE(IX-C37-TAB)
                    ws.getEleCache37(ws.getIxIndici().getC37Tab()).setNumQuoInvstRebate(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinNumQuoInvstRebate(), 12, 5));
                    // COB_CODE: MOVE WRIN-NUM-QUO-INVST-PURO-RISC
                    //             TO C37-NUM-QUO-INVST-PURO-RISC(IX-C37-TAB)
                    ws.getEleCache37(ws.getIxIndici().getC37Tab()).setNumQuoInvstPuroRisc(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinNumQuoInvstPuroRisc(), 12, 5));
                    // COB_CODE: MOVE WRIN-NUM-QUO-ATTUALE
                    //             TO C37-NUM-QUO-ATTUALE(IX-C37-TAB)
                    ws.getEleCache37(ws.getIxIndici().getC37Tab()).setNumQuoAttuale(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinNumQuoAttuale(), 12, 5));
                    // COB_CODE: MOVE WRIN-NUM-QUO-DISINV-COMP-NAV
                    //             TO C37-NUM-QUO-DISINV-COMP-NAV(IX-C37-TAB)
                    ws.getEleCache37(ws.getIxIndici().getC37Tab()).setNumQuoDisinvCompNav(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinNumQuoDisinvCompNav(), 12, 5));
                    // COB_CODE: MOVE WRIN-NUM-QUO-INVST-COMP-NAV
                    //             TO C37-NUM-QUO-INVST-COMP-NAV(IX-C37-TAB)
                    ws.getEleCache37(ws.getIxIndici().getC37Tab()).setNumQuoInvstCompNav(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinNumQuoInvstCompNav(), 12, 5));
                    // COB_CODE: MOVE IX-C37-TAB TO IX-MAX-C37
                    ws.setIxMaxC37(ws.getIxIndici().getC37Tab());
                }
                //--> FLAG PER CONTROLLO 37
                //             COMPUTE WK-CNTRL-37 =
                //                     (WRIN-NUM-QUO-PREC -
                //                      WRIN-NUM-QUO-DISINV-RISC-PARZ -
                //                      WRIN-NUM-QUO-DIS-RISC-PARZ-PRG -
                //                      WRIN-NUM-QUO-DISINV-IMPOS-SOST -
                //                      WRIN-NUM-QUO-DISINV-COMMGES -
                //                      WRIN-NUM-QUO-DISINV-PREL-COSTI -
                //                      WRIN-NUM-QUO-DISINV-SWITCH +
                //                      WRIN-NUM-QUO-INVST-SWITCH +
                //                      WRIN-NUM-QUO-INVST-VERS +
                //                      WRIN-NUM-QUO-INVST-REBATE  +
                //                      WRIN-NUM-QUO-INVST-PURO-RISC) -
                //                      WRIN-NUM-QUO-ATTUALE
                //
                //--> FLAG PER CONTROLLO 37
                //            IF WK-CNTRL-37 > 0,01
                //            OR WK-CNTRL-37 < -0,01
                //               SET CNTRL-37-ERR-SI TO TRUE
                //            ELSE
                //               CONTINUE
                //            END-IF
                //            END-IF
                break;

            case "0009"://--> PER IL CONTROLLO 38 VIENE LASCITO IL CUMULO DEL RISCATTO
                //--> DI PRIMA
                // COB_CODE: COMPUTE REC9-IMP-LORDO-RISC-PARZ =
                //                   REC9-IMP-LORDO-RISC-PARZ +
                //                    WRIN-IMP-LORDO-RISC-PARZ
                ws.getWsVariabili().setRec9ImpLordoRiscParz(Trunc.toDecimal(ws.getWsVariabili().getRec9ImpLordoRiscParz().add(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinImpLordoRiscParz()), 15, 3));
                // COB_CODE: PERFORM C195-CACHE-REC-9
                //              THRU EX-C915
                c195CacheRec9();
                break;

            case "0010"://--->    MEMORIZZAZIONE DEL RECORD 10
                // COB_CODE: PERFORM C200-CACHE-REC-10
                //              THRU EX-C200
                c200CacheRec10();
                break;

            default:// COB_CODE: CONTINUE
            //continue
                break;
        }
    }

    /**Original name: C195-CACHE-REC-9<br>
	 * <pre>---------------------------------------------------------------*
	 * --> CACHE DEL RECORD 9 IN BASE AL TIPO MOVIMENTO
	 * ----------------------------------------------------------------*</pre>*/
    private void c195CacheRec9() {
        // COB_CODE: MOVE 'C195-CACHE-REC-9'
        //              TO WK-LABEL
        ws.getWsVariabili().setWkLabel("C195-CACHE-REC-9");
        // COB_CODE: SET TROVATO-MOVI-NO           TO TRUE
        ws.getAreaFlag().getFlagTrovatoMovi().setNo();
        // COB_CODE: PERFORM VARYING IX-REC-9 FROM 1 BY 1
        //             UNTIL IX-REC-9 > ELE-MAX-CACHE-9
        //                OR TROVATO-MOVI-SI
        //               END-IF
        //           END-PERFORM
        ws.getIxIndici().setRec9(((short)1));
        while (!(ws.getIxIndici().getRec9() > ws.getEleMaxCache9() || ws.getAreaFlag().getFlagTrovatoMovi().isSi())) {
            // COB_CODE: IF WRIN-RP-TP-MOVI = REC9-RP-TP-MOVI(IX-REC-9)
            //              SET TROVATO-MOVI-SI          TO TRUE
            //           END-IF
            if (ws.getWrinRecOutput().getWrinTpRecordDati().getWrinRpTpMovi() == ws.getEleCache9(ws.getIxIndici().getRec9()).getRpTpMovi()) {
                // COB_CODE: ADD WRIN-IMP-LORDO-RISC-PARZ
                //             TO REC9-IMP-LRD-RISC-PARZ(IX-REC-9)
                ws.getEleCache9(ws.getIxIndici().getRec9()).setImpLrdRiscParz(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinImpLordoRiscParz().add(ws.getEleCache9(ws.getIxIndici().getRec9()).getImpLrdRiscParz()), 15, 3));
                // COB_CODE: SET TROVATO-MOVI-SI          TO TRUE
                ws.getAreaFlag().getFlagTrovatoMovi().setSi();
            }
            ws.getIxIndici().setRec9(Trunc.toShort(ws.getIxIndici().getRec9() + 1, 4));
        }
        // COB_CODE: IF TROVATO-MOVI-NO
        //                 TO REC9-IMP-LRD-RISC-PARZ(IX-REC-9)
        //           END-IF.
        if (ws.getAreaFlag().getFlagTrovatoMovi().isNo()) {
            // COB_CODE: ADD 1 TO ELE-MAX-CACHE-9
            ws.setEleMaxCache9(Trunc.toInt(1 + ws.getEleMaxCache9(), 9));
            // COB_CODE: MOVE ELE-MAX-CACHE-9
            //              TO IX-REC-9
            ws.getIxIndici().setRec9(Trunc.toShort(ws.getEleMaxCache9(), 4));
            // COB_CODE: MOVE WRIN-DT-EFFETTO-LIQ
            //              TO REC9-DT-EFFETTO-LIQ(IX-REC-9)
            ws.getEleCache9(ws.getIxIndici().getRec9()).setDtEffettoLiq(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinDtEffettoLiq());
            // COB_CODE: MOVE WRIN-RP-TP-MOVI
            //              TO REC9-RP-TP-MOVI(IX-REC-9)
            ws.getEleCache9(ws.getIxIndici().getRec9()).setRpTpMoviFormatted(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinRpTpMoviFormatted());
            // COB_CODE: MOVE WRIN-IMP-LORDO-RISC-PARZ
            //              TO REC9-IMP-LRD-RISC-PARZ(IX-REC-9)
            ws.getEleCache9(ws.getIxIndici().getRec9()).setImpLrdRiscParz(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinImpLordoRiscParz(), 15, 3));
        }
    }

    /**Original name: C100-CACHE-REC-3<br>
	 * <pre>----------------------------------------------------------------*
	 *    CREAZIONE CACHE X IL RECORD 3                                *
	 * ----------------------------------------------------------------*</pre>*/
    private void c100CacheRec3() {
        // COB_CODE: MOVE 'C100-CACHE-REC-3'                   TO WK-LABEL.
        ws.getWsVariabili().setWkLabel("C100-CACHE-REC-3");
        // COB_CODE: ADD 1 TO IX-REC-3
        ws.getIxIndici().setRec3(Trunc.toShort(1 + ws.getIxIndici().getRec3(), 4));
        // COB_CODE: MOVE WRIN-ID-GAR
        //             TO REC3-ID-GAR(IX-REC-3)
        ws.getEleCache3(ws.getIxIndici().getRec3()).setIdGarFormatted(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinIdGarFormatted());
        // COB_CODE: MOVE WRIN-COD-GAR
        //             TO REC3-COD-GAR(IX-REC-3)
        ws.getEleCache3(ws.getIxIndici().getRec3()).setCodGar(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinCodGar());
        // COB_CODE: MOVE WRIN-TP-GAR
        //             TO REC3-TP-GAR(IX-REC-3)
        ws.getEleCache3(ws.getIxIndici().getRec3()).setTpGarFormatted(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinTpGarFormatted());
        // COB_CODE: MOVE WRIN-TP-INVST-GAR
        //             TO REC3-TP-INVST(IX-REC-3)
        ws.getEleCache3(ws.getIxIndici().getRec3()).setTpInvstFormatted(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinTpInvstGarFormatted());
        // COB_CODE: MOVE WRIN-CUM-PREM-INVST-AA-RIF
        //             TO REC3-CUM-PREM-INVST-AA-RIF(IX-REC-3)
        ws.getEleCache3(ws.getIxIndici().getRec3()).setCumPremInvstAaRif(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinCumPremInvstAaRif(), 15, 3));
        // COB_CODE: MOVE WRIN-CUM-PREM-VERS-AA-RIF
        //             TO REC3-CUM-PREM-VERS-AA-RIF(IX-REC-3)
        ws.getEleCache3(ws.getIxIndici().getRec3()).setCumPremVersAaRif(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinCumPremVersAaRif(), 15, 3));
        // COB_CODE: MOVE WRIN-PREST-MATUR-AA-RIF
        //             TO REC3-PREST-MATUR-AA-RIF(IX-REC-3)
        ws.getEleCache3(ws.getIxIndici().getRec3()).setPrestMaturAaRif(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinPrestMaturAaRif(), 15, 3));
        // COB_CODE: MOVE WRIN-CAP-LIQ-GA
        //             TO REC3-CAP-LIQ-GA(IX-REC-3)
        ws.getEleCache3(ws.getIxIndici().getRec3()).setCapLiqGa(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinCapLiqGa(), 15, 3));
        // COB_CODE: MOVE WRIN-RISC-TOT-GA
        //             TO REC3-RISC-TOT-GA(IX-REC-3)
        ws.getEleCache3(ws.getIxIndici().getRec3()).setRiscTotGa(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinRiscTotGa(), 15, 3));
        // COB_CODE: MOVE WRIN-CUM-PREM-VERS-EC-PREC
        //             TO REC3-CUM-PREM-VERS-EC-PREC(IX-REC-3)
        ws.getEleCache3(ws.getIxIndici().getRec3()).setCumPremVersEcPrec(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinCumPremVersEcPrec(), 15, 3));
        // COB_CODE: MOVE WRIN-PREST-MATUR-EC-PREC
        //             TO REC3-PREST-MATUR-EC-PREC(IX-REC-3)
        ws.getEleCache3(ws.getIxIndici().getRec3()).setPrestMaturEcPrec(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinPrestMaturEcPrec(), 15, 3));
        // COB_CODE: MOVE WRIN-IMP-LRD-RISC-PARZ-AP-GA
        //             TO REC3-IMP-LRD-RISC-PARZ-AP-GAR(IX-REC-3)
        ws.getEleCache3(ws.getIxIndici().getRec3()).setImpLrdRiscParzApGar(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinImpLrdRiscParzApGa(), 15, 3));
        // COB_CODE: MOVE WRIN-IMP-LRD-RISC-PARZ-AC-GA
        //             TO REC3-IMP-LRD-RISC-PARZ-AC-GAR(IX-REC-3)
        ws.getEleCache3(ws.getIxIndici().getRec3()).setImpLrdRiscParzAcGar(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinImpLrdRiscParzAcGa(), 15, 3));
        // COB_CODE: MOVE WRIN-TASSO-ANN-RIVAL-PREST-GAR
        //             TO REC3-TASSO-ANN-RIVAL-PREST-GAR(IX-REC-3)
        ws.getEleCache3(ws.getIxIndici().getRec3()).setTassoAnnRivalPrestGar(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinTassoAnnRivalPrestGar(), 14, 9));
        // COB_CODE: MOVE WRIN-TASSO-TECNICO
        //             TO REC3-TASSO-TECNICO(IX-REC-3)
        ws.getEleCache3(ws.getIxIndici().getRec3()).setTassoTecnico(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinTassoTecnico(), 14, 9));
        //    AND WRIN-TP-GAR =  1 AND WRIN-TP-INVST-GAR = 4
        // COB_CODE:      IF TP-ESTR-RV
        //           *    AND WRIN-TP-GAR =  1 AND WRIN-TP-INVST-GAR = 4
        //                AND WRIN-TP-GAR =  ( 1 OR 5 ) AND WRIN-TP-INVST-GAR = 4
        //                     TO WK-TASSO-TEC-RV
        //                END-IF
        if (ws.getTipologiaEc().isRv() && (ws.getWrinRecOutput().getWrinTpRecordDati().getWrinTpGar() == 1 || ws.getWrinRecOutput().getWrinTpRecordDati().getWrinTpGar() == 5) && ws.getWrinRecOutput().getWrinTpRecordDati().getWrinTpInvstGar() == 4) {
            // COB_CODE: MOVE WRIN-TASSO-TECNICO
            //             TO WK-TASSO-TEC-RV
            ws.getWorkCalcoli().setWkTassoTecRv(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinTassoTecnico(), 14, 9));
        }
        // COB_CODE: MOVE WRIN-REND-LOR-GEST-SEP-GAR
        //             TO REC3-REND-LOR-GEST-SEP-GAR(IX-REC-3)
        ws.getEleCache3(ws.getIxIndici().getRec3()).setRendLorGestSepGar(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinRendLorGestSepGar(), 14, 9));
        // COB_CODE: MOVE WRIN-ALIQ-RETROC-RICON-GAR
        //             TO REC3-ALIQ-RETROC-RICON-GAR(IX-REC-3)
        ws.getEleCache3(ws.getIxIndici().getRec3()).setAliqRetrocRiconGar(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinAliqRetrocRiconGar(), 6, 3));
        // COB_CODE: MOVE WRIN-TASSO-ANN-REND-RETROC-GAR
        //             TO REC3-TASSO-ANN-REND-RETROC-GAR(IX-REC-3)
        ws.getEleCache3(ws.getIxIndici().getRec3()).setTassoAnnRendRetrocGar(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinTassoAnnRendRetrocGar(), 14, 9));
        // COB_CODE: MOVE WRIN-PC-COMM-GEST-GAR
        //             TO REC3-PC-COMM-GEST-GAR(IX-REC-3)
        ws.getEleCache3(ws.getIxIndici().getRec3()).setPcCommGestGar(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinPcCommGestGar(), 6, 3));
        // COB_CODE: MOVE WRIN-REN-MIN-TRNUT
        //             TO REC3-REN-MIN-TRNUT(IX-REC-3)
        ws.getEleCache3(ws.getIxIndici().getRec3()).setRenMinTrnut(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinRenMinTrnut(), 14, 9));
        // COB_CODE: MOVE WRIN-REN-MIN-GARTO
        //             TO REC3-REN-MIN-GARTO(IX-REC-3).
        ws.getEleCache3(ws.getIxIndici().getRec3()).setRenMinGarto(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinRenMinGarto(), 14, 9));
        // COB_CODE: MOVE WRIN-FL-STORNATE-ANN
        //             TO REC3-FL-STORNATE-ANN(IX-REC-3).
        ws.getEleCache3(ws.getIxIndici().getRec3()).setFlStornateAnn(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinFlStornateAnn());
        // COB_CODE: MOVE IX-REC-3   TO ELE-MAX-CACHE-3.
        ws.setEleMaxCache3(ws.getIxIndici().getRec3());
    }

    /**Original name: C300-CACHE-REC-8<br>
	 * <pre>----------------------------------------------------------------*
	 *    CREAZIONE CACHE X LE SOMME DEL RECORD 8                      *
	 * ----------------------------------------------------------------*</pre>*/
    private void c300CacheRec8() {
        // COB_CODE: MOVE 'C300-CACHE-REC-8'                    TO WK-LABEL.
        ws.getWsVariabili().setWkLabel("C300-CACHE-REC-8");
        // COB_CODE: COMPUTE REC8-CNTRVAL-PREC =
        //                   REC8-CNTRVAL-PREC
        //                  + WRIN-CNTRVAL-PREC
        ws.getAreaCache8().setCntrvalPrec(Trunc.toDecimal(ws.getAreaCache8().getCntrvalPrec().add(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinCntrvalPrec()), 15, 3));
        // COB_CODE: COMPUTE REC8-CNTRVAL-ATTUALE =
        //                   REC8-CNTRVAL-ATTUALE +
        //                   WRIN-CNTRVAL-ATTUALE
        ws.getAreaCache8().setCntrvalAttuale(Trunc.toDecimal(ws.getAreaCache8().getCntrvalAttuale().add(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinCntrvalAttuale()), 15, 3));
        // COB_CODE: COMPUTE REC8-NUM-QUO-INVST-VERS =
        //                   REC8-NUM-QUO-INVST-VERS +
        //                   WRIN-NUM-QUO-INVST-VERS
        ws.getAreaCache8().setNumQuoInvstVers(Trunc.toDecimal(ws.getAreaCache8().getNumQuoInvstVers().add(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinNumQuoInvstVers()), 12, 5));
        // COB_CODE: COMPUTE REC8-NUM-QUO-DISINV-COMMGES =
        //                   REC8-NUM-QUO-DISINV-COMMGES +
        //                   WRIN-NUM-QUO-DISINV-COMMGES
        ws.getAreaCache8().setNumQuoDisinvCommges(Trunc.toDecimal(ws.getAreaCache8().getNumQuoDisinvCommges().add(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinNumQuoDisinvCommges()), 12, 5));
        // COB_CODE: COMPUTE REC8-NUM-QUO-DISINV-PREL-COSTI =
        //                   REC8-NUM-QUO-DISINV-PREL-COSTI +
        //                   WRIN-NUM-QUO-DISINV-PREL-COSTI
        ws.getAreaCache8().setNumQuoDisinvPrelCosti(Trunc.toDecimal(ws.getAreaCache8().getNumQuoDisinvPrelCosti().add(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinNumQuoDisinvPrelCosti()), 12, 5));
        // COB_CODE: COMPUTE REC8-NUM-QUO-INVST-REBATE =
        //                   REC8-NUM-QUO-INVST-REBATE +
        //                   WRIN-NUM-QUO-INVST-REBATE
        ws.getAreaCache8().setNumQuoInvstRebate(Trunc.toDecimal(ws.getAreaCache8().getNumQuoInvstRebate().add(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinNumQuoInvstRebate()), 12, 5));
        // COB_CODE: COMPUTE REC8-NUM-QUO-DISINV-SWITCH =
        //                   REC8-NUM-QUO-DISINV-SWITCH +
        //                   WRIN-NUM-QUO-DISINV-SWITCH
        ws.getAreaCache8().setNumQuoDisinvSwitch(Trunc.toDecimal(ws.getAreaCache8().getNumQuoDisinvSwitch().add(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinNumQuoDisinvSwitch()), 12, 5));
        // COB_CODE: COMPUTE REC8-NUM-QUO-INV-SWITCH =
        //                   REC8-NUM-QUO-INV-SWITCH +
        //                   WRIN-NUM-QUO-INVST-SWITCH
        ws.getAreaCache8().setNumQuoInvSwitch(Trunc.toDecimal(ws.getAreaCache8().getNumQuoInvSwitch().add(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinNumQuoInvstSwitch()), 12, 5));
        // COB_CODE: COMPUTE REC8-NUM-QUO-DISINV-COMP-NAV =
        //                   REC8-NUM-QUO-DISINV-COMP-NAV +
        //                   WRIN-NUM-QUO-DISINV-COMP-NAV
        ws.getAreaCache8().setNumQuoDisinvCompNav(Trunc.toDecimal(ws.getAreaCache8().getNumQuoDisinvCompNav().add(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinNumQuoDisinvCompNav()), 12, 5));
        // COB_CODE: COMPUTE REC8-NUM-QUO-INVST-COMP-NAV =
        //                   REC8-NUM-QUO-INVST-COMP-NAV +
        //                   WRIN-NUM-QUO-INVST-COMP-NAV
        ws.getAreaCache8().setNumQuoInvstCompNav(Trunc.toDecimal(ws.getAreaCache8().getNumQuoInvstCompNav().add(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinNumQuoInvstCompNav()), 12, 5));
        //--> SALVATAGGIO DELL'INDICE DEL RECORD 8 X I CONTRLLI
        // COB_CODE: MOVE IX-IND              TO IX-REC-8.
        ws.getIxIndici().setRec8(ws.getIxIndici().getInd());
    }

    /**Original name: C200-CACHE-REC-10<br>
	 * <pre>----------------------------------------------------------------*
	 *    CREAZIONE CACHE X IL RECORD 10                               *
	 * ----------------------------------------------------------------*</pre>*/
    private void c200CacheRec10() {
        // COB_CODE: MOVE 'C100-CACHE-REC-10'                   TO WK-LABEL.
        ws.getWsVariabili().setWkLabel("C100-CACHE-REC-10");
        // COB_CODE: ADD 1 TO IX-REC-10
        ws.getIxIndici().setRec10(Trunc.toShort(1 + ws.getIxIndici().getRec10(), 4));
        // COB_CODE: MOVE WRIN-ID-GARANZIA
        //             TO REC10-ID-GAR(IX-REC-10)
        ws.getEleCache10(ws.getIxIndici().getRec10()).setIdGarFormatted(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinIdGaranziaFormatted());
        // COB_CODE: MOVE WRIN-DESC-TP-MOVI
        //             TO REC10-DESC-TP-MOVI(IX-REC-10)
        ws.getEleCache10(ws.getIxIndici().getRec10()).setDescTpMovi(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinDescTpMovi());
        // COB_CODE: MOVE WRIN-IMP-OPER
        //             TO REC10-IMP-OPER(IX-REC-10)
        ws.getEleCache10(ws.getIxIndici().getRec10()).setImpOper(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinImpOper(), 15, 3));
        // COB_CODE: MOVE WRIN-IMP-VERS
        //             TO REC10-IMP-VERS(IX-REC-10)
        ws.getEleCache10(ws.getIxIndici().getRec10()).setImpVers(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinImpVers(), 15, 3));
        // COB_CODE: MOVE WRIN-NUMERO-QUO
        //             TO REC10-NUMERO-QUO(IX-REC-10)
        ws.getEleCache10(ws.getIxIndici().getRec10()).setNumeroQuo(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinNumeroQuo(), 12, 5));
        // COB_CODE: MOVE WRIN-DT-EFF-MOVI
        //             TO REC10-DT-EFF-MOVI(IX-REC-10)
        ws.getEleCache10(ws.getIxIndici().getRec10()).setDtEffMovi(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinDtEffMovi());
        // COB_CODE: MOVE WRIN-TP-MOVI
        //             TO REC10-TP-MOVI(IX-REC-10)
        ws.getEleCache10(ws.getIxIndici().getRec10()).setTpMoviFormatted(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinTpMoviFormatted());
        // COB_CODE: MOVE IX-REC-10   TO ELE-MAX-CACHE-10.
        ws.setEleMaxCache10(ws.getIxIndici().getRec10());
    }

    /**Original name: S9700-CONTROLLI<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLI                                                    *
	 * ----------------------------------------------------------------*</pre>*/
    private void s9700Controlli() {
        // COB_CODE: MOVE 'S9700-CONTROLLI'                   TO WK-LABEL.
        ws.getWsVariabili().setWkLabel("S9700-CONTROLLI");
        // COB_CODE: PERFORM S9710-CONTROLLI-COMUNI
        //              THRU EX-S9710.
        s9710ControlliComuni();
        // COB_CODE: SET CONTROLLO-OK TO TRUE
        ws.getAreaFlag().getFlagControllo().setOk();
        // COB_CODE: IF  CONTROLLO-OK
        //           AND IDSV0001-ESITO-OK
        //                 THRU EX-S9720
        //           END-IF.
        if (ws.getAreaFlag().getFlagControllo().isOk() && areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: PERFORM S9720-CONTROLLI-AD-HOC
            //              THRU EX-S9720
            s9720ControlliAdHoc();
        }
    }

    /**Original name: S9710-CONTROLLI-COMUNI<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLI COMUNI                                             *
	 * ----------------------------------------------------------------*</pre>*/
    private void s9710ControlliComuni() {
        // COB_CODE: MOVE 'S9710-CONTROLLI-COMUNI'          TO WK-LABEL.
        ws.getWsVariabili().setWkLabel("S9710-CONTROLLI-COMUNI");
        //--> CONTROLLI SUL RECORD 1
        // COB_CODE: MOVE WK-APPO-BLOB-DATA-REC(1)        TO WRIN-REC-FILEIN
        ws.setWrinRecFileinFormatted(wkAppoDataJob.getBlobData(1).getWkAppoBlobDataRecFormatted());
        // COB_CODE: SET CONTROLLO-OK TO TRUE
        ws.getAreaFlag().getFlagControllo().setOk();
        // COB_CODE: PERFORM C0001-CNTRL-1
        //              THRU EX-C0001
        c0001Cntrl1();
        // COB_CODE: IF CONTROLLO-OK
        //                 THRU EX-C0002
        //           END-IF
        if (ws.getAreaFlag().getFlagControllo().isOk()) {
            // COB_CODE: PERFORM C0002-CNTRL-2
            //              THRU EX-C0002
            c0002Cntrl2();
        }
        // COB_CODE: IF CONTROLLO-OK
        //                 THRU EX-C0030
        //           END-IF.
        if (ws.getAreaFlag().getFlagControllo().isOk()) {
            // COB_CODE: PERFORM C0030-CNTRL-3-12
            //              THRU EX-C0030
            c0030Cntrl312();
        }
        // COB_CODE: IF CONTROLLO-OK
        //                 THRU EX-C0003
        //           END-IF
        if (ws.getAreaFlag().getFlagControllo().isOk()) {
            // COB_CODE: PERFORM C0003-CNTRL-6-7-14
            //              THRU EX-C0003
            c0003Cntrl6714();
        }
        // COB_CODE: IF CONTROLLO-OK
        //                 THRU EX-C0004
        //           END-IF
        if (ws.getAreaFlag().getFlagControllo().isOk()) {
            // COB_CODE: PERFORM C0004-CNTRL-8-9-18
            //              THRU EX-C0004
            c0004Cntrl8918();
        }
        // COB_CODE: IF CONTROLLO-OK
        //                 THRU EX-C0005
        //           END-IF
        if (ws.getAreaFlag().getFlagControllo().isOk()) {
            // COB_CODE: PERFORM C0005-CNTRL-10-19
            //              THRU EX-C0005
            c0005Cntrl1019();
        }
        // COB_CODE: IF CONTROLLO-OK
        //                 THRU EX-C0006
        //           END-IF
        if (ws.getAreaFlag().getFlagControllo().isOk()) {
            // COB_CODE: PERFORM C0006-CNTRL-11-15
            //              THRU EX-C0006
            c0006Cntrl1115();
        }
        // COB_CODE: IF CONTROLLO-OK
        //                 THRU EX-C0007
        //           END-IF
        if (ws.getAreaFlag().getFlagControllo().isOk()) {
            // COB_CODE: PERFORM C0007-CNTRL-13-42
            //              THRU EX-C0007
            c0007Cntrl1342();
        }
        // COB_CODE: IF CONTROLLO-OK
        //                 THRU EX-C0008
        //           END-IF
        if (ws.getAreaFlag().getFlagControllo().isOk()) {
            // COB_CODE: PERFORM C0008-CNTRL-16
            //              THRU EX-C0008
            c0008Cntrl16();
        }
        // COB_CODE: IF CONTROLLO-OK
        //                 THRU EX-C0009
        //           END-IF
        if (ws.getAreaFlag().getFlagControllo().isOk()) {
            // COB_CODE: PERFORM C0009-CNTRL-17
            //              THRU EX-C0009
            c0009Cntrl17();
        }
        // COB_CODE: IF CONTROLLO-OK
        //                 THRU EX-C0010
        //           END-IF
        if (ws.getAreaFlag().getFlagControllo().isOk()) {
            // COB_CODE: PERFORM C0010-CNTRL-20
            //              THRU EX-C0010
            c0010Cntrl20();
        }
        // COB_CODE: IF CONTROLLO-OK
        //                 THRU EX-C0011
        //           END-IF
        if (ws.getAreaFlag().getFlagControllo().isOk()) {
            // COB_CODE: PERFORM C0011-CNTRL-21
            //              THRU EX-C0011
            c0011Cntrl21();
        }
        //-->
        // COB_CODE: IF CONTROLLO-OK
        //                 THRU EX-C0012
        //           END-IF
        if (ws.getAreaFlag().getFlagControllo().isOk()) {
            // COB_CODE: PERFORM C0012-CNTRL-23
            //              THRU EX-C0012
            c0012Cntrl23();
        }
        // COB_CODE: IF CONTROLLO-OK
        //                 THRU EX-C0013
        //           END-IF
        if (ws.getAreaFlag().getFlagControllo().isOk()) {
            // COB_CODE: PERFORM C0013-CNTRL-33
            //              THRU EX-C0013
            c0013Cntrl33();
        }
        // COB_CODE: IF CONTROLLO-OK
        //                 THRU EX-C0014
        //           END-IF
        if (ws.getAreaFlag().getFlagControllo().isOk()) {
            // COB_CODE: PERFORM C0014-CNTRL-38
            //              THRU EX-C0014
            c0014Cntrl38();
        }
        // COB_CODE: IF CONTROLLO-OK
        //                 THRU EX-C0015
        //           END-IF.
        if (ws.getAreaFlag().getFlagControllo().isOk()) {
            // COB_CODE: PERFORM C0015-CNTRL-40
            //              THRU EX-C0015
            c0015Cntrl40();
        }
    }

    /**Original name: S9720-CONTROLLI-AD-HOC<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLI AD HOC DIFFERENZIATI PER TP ESTR CONTO             *
	 * ----------------------------------------------------------------*</pre>*/
    private void s9720ControlliAdHoc() {
        // COB_CODE: MOVE 'S9720-CONTROLLI-AD-HOC'          TO WK-LABEL.
        ws.getWsVariabili().setWkLabel("S9720-CONTROLLI-AD-HOC");
        // COB_CODE: IF TP-ESTR-MU
        //           OR TP-ESTR-RV
        //                 THRU EX-S9722
        //           END-IF
        if (ws.getTipologiaEc().isMu() || ws.getTipologiaEc().isRv()) {
            // COB_CODE: PERFORM S9722-CONTROLLI-MU-RV
            //              THRU EX-S9722
            s9722ControlliMuRv();
        }
        // COB_CODE: IF TP-ESTR-MU
        //           OR TP-ESTR-UL
        //                 THRU EX-S9724
        //           END-IF
        if (ws.getTipologiaEc().isMu() || ws.getTipologiaEc().isUl()) {
            // COB_CODE: PERFORM S9724-CONTROLLI-MU-UL
            //              THRU EX-S9724
            s9724ControlliMuUl();
        }
        // COB_CODE: IF TP-ESTR-MU
        //                 THRU EX-S9726
        //           END-IF.
        if (ws.getTipologiaEc().isMu()) {
            // COB_CODE: PERFORM S9726-CONTROLLI-MU
            //              THRU EX-S9726
            s9726ControlliMu();
        }
    }

    /**Original name: S9722-CONTROLLI-MU-RV<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLI PER MULTIRAMO E RIVALUTABILI                       *
	 * ----------------------------------------------------------------*</pre>*/
    private void s9722ControlliMuRv() {
        // COB_CODE: MOVE 'S9722-CONTROLLI-MU-RV'          TO WK-LABEL.
        ws.getWsVariabili().setWkLabel("S9722-CONTROLLI-MU-RV");
        // COB_CODE: IF CONTROLLO-OK
        //                 THRU EX-C0020
        //           END-IF.
        if (ws.getAreaFlag().getFlagControllo().isOk()) {
            // COB_CODE: PERFORM C0020-CNTRL-4-5-39
            //              THRU EX-C0020
            c0020Cntrl4539();
        }
        // COB_CODE: IF CONTROLLO-OK
        //                 THRU EX-C0022
        //           END-IF.
        if (ws.getAreaFlag().getFlagControllo().isOk()) {
            // COB_CODE: PERFORM C0022-CNTRL-RIVALUTAZ
            //              THRU EX-C0022
            c0022CntrlRivalutaz();
        }
        // COB_CODE: IF CONTROLLO-OK
        //                 THRU EX-C0255
        //           END-IF.
        if (ws.getAreaFlag().getFlagControllo().isOk()) {
            // COB_CODE: PERFORM C0255-CNTRL-25
            //              THRU EX-C0255
            c0255Cntrl25();
        }
        // COB_CODE: IF CONTROLLO-OK
        //                 THRU EX-C0024
        //           END-IF.
        if (ws.getAreaFlag().getFlagControllo().isOk()) {
            // COB_CODE: PERFORM C0024-CNTRL-43
            //              THRU EX-C0024
            c0024Cntrl43();
        }
    }

    /**Original name: S9724-CONTROLLI-MU-UL<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLI PER MULTIRAMO E UNIT LINKED                        *
	 * ----------------------------------------------------------------*</pre>*/
    private void s9724ControlliMuUl() {
        // COB_CODE: MOVE 'S9724-CONTROLLI-MU-UL'          TO WK-LABEL.
        ws.getWsVariabili().setWkLabel("S9724-CONTROLLI-MU-UL");
        // COB_CODE: IF CONTROLLO-OK
        //                 THRU EX-C0032
        //           END-IF.
        if (ws.getAreaFlag().getFlagControllo().isOk()) {
            // COB_CODE: PERFORM C0032-CNTRL-24
            //              THRU EX-C0032
            c0032Cntrl24();
        }
        // COB_CODE: IF CONTROLLO-OK
        //                 THRU EX-C0034
        //           END-IF.
        if (ws.getAreaFlag().getFlagControllo().isOk()) {
            // COB_CODE: PERFORM C0034-CNTRL-35
            //              THRU EX-C0034
            c0034Cntrl35();
        }
        // COB_CODE: IF CONTROLLO-OK
        //                 THRU EX-C0036
        //           END-IF.
        if (ws.getAreaFlag().getFlagControllo().isOk()) {
            // COB_CODE: PERFORM C0036-CNTRL-36
            //              THRU EX-C0036
            c0036Cntrl36();
        }
        // COB_CODE: IF CONTROLLO-OK
        //                 THRU EX-C0038
        //           END-IF.
        if (ws.getAreaFlag().getFlagControllo().isOk()) {
            // COB_CODE: PERFORM C0038-CNTRL-37
            //              THRU EX-C0038
            c0038Cntrl37();
        }
        // COB_CODE: IF CONTROLLO-OK
        //                 THRU EX-C0040
        //           END-IF.
        if (ws.getAreaFlag().getFlagControllo().isOk()) {
            // COB_CODE: PERFORM C0040-CNTRL-44
            //              THRU EX-C0040
            c0040Cntrl44();
        }
        // COB_CODE: IF CONTROLLO-OK
        //                 THRU EX-C0042
        //           END-IF.
        if (ws.getAreaFlag().getFlagControllo().isOk()) {
            // COB_CODE: PERFORM C0042-CNTRL-45
            //              THRU EX-C0042
            c0042Cntrl45();
        }
        // COB_CODE: IF CONTROLLO-OK
        //                 THRU EX-C0044
        //           END-IF.
        if (ws.getAreaFlag().getFlagControllo().isOk()) {
            // COB_CODE: PERFORM C0044-CNTRL-46
            //              THRU EX-C0044
            c0044Cntrl46();
        }
        // COB_CODE: IF CONTROLLO-OK
        //                 THRU EX-C0046
        //           END-IF.
        if (ws.getAreaFlag().getFlagControllo().isOk()) {
            // COB_CODE: PERFORM C0046-CNTRL-47
            //              THRU EX-C0046
            c0046Cntrl47();
        }
        // COB_CODE: IF CONTROLLO-OK
        //                 THRU EX-C0048
        //           END-IF.
        if (ws.getAreaFlag().getFlagControllo().isOk()) {
            // COB_CODE: PERFORM C0048-CNTRL-48
            //              THRU EX-C0048
            c0048Cntrl48();
        }
        // COB_CODE: IF CONTROLLO-OK
        //                 THRU EX-C0050
        //           END-IF.
        if (ws.getAreaFlag().getFlagControllo().isOk()) {
            // COB_CODE: PERFORM C0050-CNTRL-49
            //              THRU EX-C0050
            c0050Cntrl49();
        }
        // COB_CODE: IF CONTROLLO-OK
        //                 THRU EX-C0052
        //           END-IF.
        if (ws.getAreaFlag().getFlagControllo().isOk()) {
            // COB_CODE: PERFORM C0052-CNTRL-50
            //              THRU EX-C0052
            c0052Cntrl50();
        }
        // COB_CODE: IF CONTROLLO-OK
        //                 THRU EX-C0054
        //           END-IF.
        if (ws.getAreaFlag().getFlagControllo().isOk()) {
            // COB_CODE: PERFORM C0054-CNTRL-51
            //              THRU EX-C0054
            c0054Cntrl51();
        }
    }

    /**Original name: S9726-CONTROLLI-MU<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLI PER MULTIRAMO                                      *
	 * ----------------------------------------------------------------*</pre>*/
    private void s9726ControlliMu() {
        // COB_CODE: MOVE 'S9726-CONTROLLI-MU'          TO WK-LABEL.
        ws.getWsVariabili().setWkLabel("S9726-CONTROLLI-MU");
        // COB_CODE: IF CONTROLLO-OK
        //                 THRU EX-C0060
        //           END-IF.
        if (ws.getAreaFlag().getFlagControllo().isOk()) {
            // COB_CODE: PERFORM C0060-CNTRL-34
            //              THRU EX-C0060
            c0060Cntrl34();
        }
    }

    /**Original name: W100-SCRIVI-FILE<br>
	 * <pre>----------------------------------------------------------------*
	 *     SCRITTURA FILE DI OUTPUT OK
	 * ----------------------------------------------------------------*</pre>*/
    private void w100ScriviFile() {
        // COB_CODE: MOVE 'W100-SCRIVI-FILE'
        //             TO IDSV8888-AREA-DISPLAY.
        ws.getIdsv8888().getAreaDisplay().setAreaDisplay("W100-SCRIVI-FILE");
        //
        // COB_CODE: INITIALIZE WRIN-REC-FILEIN.
        initWrinRecFilein();
        //
        // COB_CODE: MOVE WK-APPO-BLOB-DATA-REC(IX-IND)
        //             TO WRIN-REC-FILEIN.
        ws.setWrinRecFileinFormatted(wkAppoDataJob.getBlobData(ws.getIxIndici().getInd()).getWkAppoBlobDataRecFormatted());
        //
        // COB_CODE: INITIALIZE FILESQS1-REC
        //                      WOUT-REC-OUTPUT.
        filesqs1To.setVariable("");
        initWoutRecOutput();
        //
        // COB_CODE: MOVE WRIN-REC-OUTPUT              TO WOUT-REC-OUTPUT
        ws.getWoutRecOutput().setWoutRecOutputBytes(ws.getWrinRecOutput().getWrinRecOutputBytes());
        // COB_CODE: MOVE AREA-LRGC0031                TO FILESQS1-REC
        filesqs1To.setVariable(ws.getAreaLrgc0031Formatted());
        // COB_CODE: SET WRITE-FILESQS-OPER            TO TRUE
        ws.getIabvsqs1().getOperazioneFilesqs().setWriteFilesqsOper();
        // COB_CODE: SET WRITE-FILESQS1-SI             TO TRUE
        ws.getIabvsqs1().getFlagWriteFilesqs1().setSi();
        // COB_CODE: PERFORM CALL-FILESEQ              THRU CALL-FILESEQ-EX
        callFileseq();
        // COB_CODE: IF SEQUENTIAL-ESITO-KO
        //                 THRU EX-S0300
        //           END-IF.
        if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoKo()) {
            // COB_CODE: MOVE WK-PGM                    TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL                  TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWsVariabili().getWkLabel());
            // COB_CODE: MOVE '005166'                  TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005166");
            // COB_CODE: MOVE SPACES                    TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("");
            // COB_CODE: MOVE SEQUENTIAL-DESC-ERRORE-ESTESA
            //                                          TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr(ws.getIabcsq99().getSequentialDescErroreEstesa());
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: W999-SCRIVI-FILE-KO<br>
	 * <pre>----------------------------------------------------------------*
	 *     SCRITTURA FILE DI OUTPUT KO FORMATO E/C
	 * ----------------------------------------------------------------*</pre>*/
    private void w999ScriviFileKo() {
        // COB_CODE: MOVE 'W999-SCRIVI-FILE-KO'
        //             TO IDSV8888-AREA-DISPLAY.
        ws.getIdsv8888().getAreaDisplay().setAreaDisplay("W999-SCRIVI-FILE-KO");
        //
        // COB_CODE: INITIALIZE WRIN-REC-FILEIN.
        initWrinRecFilein();
        //
        // COB_CODE: MOVE WK-APPO-BLOB-DATA-REC(IX-IND)
        //             TO WRIN-REC-FILEIN.
        ws.setWrinRecFileinFormatted(wkAppoDataJob.getBlobData(ws.getIxIndici().getInd()).getWkAppoBlobDataRecFormatted());
        //
        // COB_CODE: INITIALIZE FILESQS6-REC.
        filesqs6To.setVariable("");
        // COB_CODE: INITIALIZE  WOUT-REC-OUTPUT.
        initWoutRecOutput();
        //
        // COB_CODE: MOVE WRIN-REC-OUTPUT              TO WOUT-REC-OUTPUT
        ws.getWoutRecOutput().setWoutRecOutputBytes(ws.getWrinRecOutput().getWrinRecOutputBytes());
        // COB_CODE: MOVE AREA-LRGC0031                TO FILESQS6-REC
        filesqs6To.setVariable(ws.getAreaLrgc0031Formatted());
        // COB_CODE: SET WRITE-FILESQS-OPER            TO TRUE
        ws.getIabvsqs1().getOperazioneFilesqs().setWriteFilesqsOper();
        // COB_CODE: SET WRITE-FILESQS6-SI             TO TRUE
        ws.getIabvsqs1().getFlagWriteFilesqs6().setWriteFilesqs6Si();
        // COB_CODE: PERFORM CALL-FILESEQ              THRU CALL-FILESEQ-EX
        callFileseq();
        // COB_CODE: IF SEQUENTIAL-ESITO-KO
        //                 THRU EX-S0300
        //           END-IF.
        if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoKo()) {
            // COB_CODE: MOVE WK-PGM                    TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL                  TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWsVariabili().getWkLabel());
            // COB_CODE: MOVE '005166'                  TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005166");
            // COB_CODE: MOVE SPACES                    TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("");
            // COB_CODE: MOVE SEQUENTIAL-DESC-ERRORE-ESTESA
            //                                          TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr(ws.getIabcsq99().getSequentialDescErroreEstesa());
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: W200-SCRIVI-SCARTI<br>
	 * <pre>----------------------------------------------------------------*
	 *     SCRITTURA FILE DI OUTPUT KO
	 * ----------------------------------------------------------------*</pre>*/
    private void w200ScriviScarti() {
        // COB_CODE: MOVE ' W200-SCRIVI-SCARTI'
        //             TO IDSV8888-AREA-DISPLAY.
        ws.getIdsv8888().getAreaDisplay().setAreaDisplay(" W200-SCRIVI-SCARTI");
        //
        //    COMPUTE WS-NUM-SCART = WS-NUM-SCART + 1
        //
        // COB_CODE: ADD 1     TO IABV0006-CUSTOM-COUNT(REC-SCARTATI)
        iabv0006.getCustomCounters().getEleErrori(ws.getWkAreaContatori().getRecScartati()).setIabv0006CustomCount(Trunc.toInt(1 + iabv0006.getCustomCounters().getEleErrori(ws.getWkAreaContatori().getRecScartati()).getIabv0006CustomCount(), 9));
        // COB_CODE: INITIALIZE FILESQS2-REC
        filesqs2To.setVariable("");
        // COB_CODE: MOVE WK-AREA-FILE                 TO FILESQS2-REC
        filesqs2To.setVariable(ws.getWkAreaFile().getWkAreaFileFormatted());
        // COB_CODE: SET WRITE-FILESQS-OPER            TO TRUE
        ws.getIabvsqs1().getOperazioneFilesqs().setWriteFilesqsOper();
        // COB_CODE: SET WRITE-FILESQS2-SI             TO TRUE
        ws.getIabvsqs1().getFlagWriteFilesqs2().setSi();
        // COB_CODE: PERFORM CALL-FILESEQ              THRU CALL-FILESEQ-EX
        callFileseq();
        // COB_CODE: IF SEQUENTIAL-ESITO-KO
        //                 THRU EX-S0300
        //           END-IF.
        if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoKo()) {
            // COB_CODE: MOVE WK-PGM                    TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL                  TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWsVariabili().getWkLabel());
            // COB_CODE: MOVE '005166'                  TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005166");
            // COB_CODE: MOVE SPACES                    TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("");
            // COB_CODE: MOVE SEQUENTIAL-DESC-ERRORE-ESTESA
            //                                          TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr(ws.getIabcsq99().getSequentialDescErroreEstesa());
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: E001-SCARTO<br>
	 * <pre>----------------------------------------------------------------*
	 *     SCRITTURA SCARTO
	 * ----------------------------------------------------------------*</pre>*/
    private void e001Scarto() {
        // COB_CODE: MOVE 'E001-SCARTO'
        //             TO IDSV8888-AREA-DISPLAY.
        ws.getIdsv8888().getAreaDisplay().setAreaDisplay("E001-SCARTO");
        // COB_CODE: MOVE SPACES TO FILESQS3-REC.
        filesqs3To.setVariable("");
        // COB_CODE: PERFORM VARYING IX-GRAV FROM 1 BY 1
        //             UNTIL IX-GRAV > ELE-MAX-ERRORI
        //                OR CONTROLLO(IX-GRAV) = WK-CONTROLLO
        //                OR CONTROLLO(IX-GRAV) > WK-CONTROLLO
        //           END-PERFORM
        ws.getIxIndici().setGrav(((short)1));
        while (!(ws.getIxIndici().getGrav() > ws.getEleMaxErrori() || ws.getEleAreaErr(ws.getIxIndici().getGrav()).getControllo() == ws.getWsVariabili().getWkControllo() || ws.getEleAreaErr(ws.getIxIndici().getGrav()).getControllo() > ws.getWsVariabili().getWkControllo())) {
            ws.getIxIndici().setGrav(Trunc.toShort(ws.getIxIndici().getGrav() + 1, 4));
        }
        // COB_CODE: IF CONTROLLO(IX-GRAV) = WK-CONTROLLO
        //              END-IF
        //           END-IF
        if (ws.getEleAreaErr(ws.getIxIndici().getGrav()).getControllo() == ws.getWsVariabili().getWkControllo()) {
            // COB_CODE: IF GRAVITA(IX-GRAV) = 3
            //              SET WRITE-NO  TO TRUE
            //           ELSE
            //              CONTINUE
            //           END-IF
            if (ws.getEleAreaErr(ws.getIxIndici().getGrav()).getGravita() == 3) {
                // COB_CODE: SET WRITE-NO  TO TRUE
                ws.getAreaFlag().getFlagScrittura().setNo();
            }
            else {
            // COB_CODE: CONTINUE
            //continue
            }
        }
        // COB_CODE: MOVE WS-ERRORE                    TO OUT-ERR
        ws.getTracciatoFile().setOutErr(ws.getWsVariabili().getWsErrore());
        // COB_CODE: MOVE IABV0006-IB-OGG-POLI         TO OUT-IB-OGG
        ws.getTracciatoFile().setOutIbOgg(iabv0006.getReport().getIbOggPoli());
        // COB_CODE: MOVE ';'                          TO FILLER-1
        //                                                FILLER-2
        //                                                FILLER-3
        //                                                FILLER-4
        //                                                FILLER-5
        //                                                FILLER-6
        //                                                FILLER-7
        ws.getTracciatoFile().setFiller1Formatted(";");
        ws.getTracciatoFile().setFiller2Formatted(";");
        ws.getTracciatoFile().setFiller3Formatted(";");
        ws.getTracciatoFile().setFiller4Formatted(";");
        ws.getTracciatoFile().setFiller5Formatted(";");
        ws.getTracciatoFile().setFiller6Formatted(";");
        ws.getTracciatoFile().setFiller7Formatted(";");
        // COB_CODE: MOVE TRACCIATO-FILE               TO FILESQS3-REC
        filesqs3To.setVariable(ws.getTracciatoFile().getTracciatoFileFormatted());
        // COB_CODE: SET WRITE-FILESQS-OPER            TO TRUE
        ws.getIabvsqs1().getOperazioneFilesqs().setWriteFilesqsOper();
        // COB_CODE: SET WRITE-FILESQS3-SI             TO TRUE
        ws.getIabvsqs1().getFlagWriteFilesqs3().setSi();
        // COB_CODE: PERFORM CALL-FILESEQ              THRU CALL-FILESEQ-EX
        callFileseq();
        // COB_CODE: IF SEQUENTIAL-ESITO-KO
        //                 THRU EX-S0300
        //           END-IF.
        if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoKo()) {
            // COB_CODE: MOVE WK-PGM                    TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL                  TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWsVariabili().getWkLabel());
            // COB_CODE: MOVE '005166'                  TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005166");
            // COB_CODE: MOVE SPACES                    TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("");
            // COB_CODE: MOVE SEQUENTIAL-DESC-ERRORE-ESTESA
            //                                          TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr(ws.getIabcsq99().getSequentialDescErroreEstesa());
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
        // COB_CODE: INITIALIZE WS-ERRORE
        //                      TRACCIATO-FILE.
        ws.getWsVariabili().setWsErrore("");
        initTracciatoFile();
    }

    /**Original name: S8120-SCRIVI-TST<br>
	 * <pre>----------------------------------------------------------------*
	 *     SCRITTURA INTESTAZIONE FILE DI SCARTO
	 * ----------------------------------------------------------------*</pre>*/
    private void s8120ScriviTst() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'S8120-SCRIVI-TST'
        //             TO IDSV8888-AREA-DISPLAY.
        ws.getIdsv8888().getAreaDisplay().setAreaDisplay("S8120-SCRIVI-TST");
        // COB_CODE: MOVE SPACES TO FILESQS3-REC.
        filesqs3To.setVariable("");
        // COB_CODE: STRING 'IB-OGGETTO;NUMERO CNTRL;DESC;CAMPO-1;CAMPO-2;CAMPO-3'
        //                  ';DATA-1;DATA-2'
        //           DELIMITED BY SIZE
        //           INTO  FILESQS3-REC
        concatUtil = ConcatUtil.buildString(Len.FILESQS3_REC, "IB-OGGETTO;NUMERO CNTRL;DESC;CAMPO-1;CAMPO-2;CAMPO-3", ";DATA-1;DATA-2");
        filesqs3To.setVariable(concatUtil.replaceInString(filesqs3To.getVariableFormatted()));
        // COB_CODE: SET WRITE-FILESQS-OPER            TO TRUE
        ws.getIabvsqs1().getOperazioneFilesqs().setWriteFilesqsOper();
        // COB_CODE: SET WRITE-FILESQS3-SI             TO TRUE
        ws.getIabvsqs1().getFlagWriteFilesqs3().setSi();
        // COB_CODE: PERFORM CALL-FILESEQ              THRU CALL-FILESEQ-EX
        callFileseq();
        // COB_CODE: IF SEQUENTIAL-ESITO-KO
        //                 THRU EX-S0300
        //           END-IF.
        if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoKo()) {
            // COB_CODE: MOVE WK-PGM                    TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL                  TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWsVariabili().getWkLabel());
            // COB_CODE: MOVE '005166'                  TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005166");
            // COB_CODE: MOVE SPACES                    TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("");
            // COB_CODE: MOVE SEQUENTIAL-DESC-ERRORE-ESTESA
            //                                          TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr(ws.getIabcsq99().getSequentialDescErroreEstesa());
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *     OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: MOVE 'S9000-OPERAZIONI-FINALI'
        //             TO IDSV8888-AREA-DISPLAY.
        ws.getIdsv8888().getAreaDisplay().setAreaDisplay("S9000-OPERAZIONI-FINALI");
        //
        // COB_CODE: PERFORM ESEGUI-DISPLAY
        //              THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
    }

    /**Original name: CHIUSURA-FILE<br>
	 * <pre>----------------------------------------------------------------*
	 *  CLOSE FILE SEQUENZIALE
	 * ----------------------------------------------------------------*</pre>*/
    private void chiusuraFile() {
        // COB_CODE: MOVE 'CHIUSURA-FILE'              TO WK-LABEL.
        ws.getWsVariabili().setWkLabel("CHIUSURA-FILE");
        // COB_CODE: MOVE WK-LABEL                     TO IDSV8888-AREA-DISPLAY.
        ws.getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getWsVariabili().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY            THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: SET CLOSE-FILESQS-OPER            TO TRUE
        ws.getIabvsqs1().getOperazioneFilesqs().setCloseFilesqsOper();
        // COB_CODE: SET CLOSE-FILESQS1-SI             TO TRUE
        ws.getIabvsqs1().getFlagCloseFilesqs1().setSi();
        // COB_CODE: SET CLOSE-FILESQS2-SI             TO TRUE
        ws.getIabvsqs1().getFlagCloseFilesqs2().setSi();
        // COB_CODE: SET CLOSE-FILESQS3-SI             TO TRUE
        ws.getIabvsqs1().getFlagCloseFilesqs3().setSi();
        // COB_CODE: SET CLOSE-FILESQS4-NO             TO TRUE
        ws.getIabvsqs1().getFlagCloseFilesqs4().setCloseFilesqs4No();
        // COB_CODE: SET CLOSE-FILESQS6-SI             TO TRUE
        ws.getIabvsqs1().getFlagCloseFilesqs6().setCloseFilesqs6Si();
        // COB_CODE: PERFORM CALL-FILESEQ              THRU CALL-FILESEQ-EX
        callFileseq();
        // COB_CODE: IF NOT FILE-STATUS-OK
        //                 THRU EX-S0300
        //           END-IF.
        if (!ws.getIabcsq99().getFileStatus().isFileStatusOk()) {
            // COB_CODE: MOVE WK-PGM                    TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL                  TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWsVariabili().getWkLabel());
            // COB_CODE: MOVE '005166'                  TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005166");
            // COB_CODE: MOVE SPACES                    TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("");
            // COB_CODE: MOVE SEQUENTIAL-DESC-ERRORE-ESTESA
            //                                          TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr(ws.getIabcsq99().getSequentialDescErroreEstesa());
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //               THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: S100-CALL-ACTUATOR<br>
	 * <pre>----------------------------------------------------------------*
	 *     SERVIZIO ACTUATOR PER RECUPERO TASSI MINIMI
	 * ----------------------------------------------------------------*</pre>*/
    private void s100CallActuator() {
        // COB_CODE: PERFORM S110-PREP-AREA-ISPS0580
        //              THRU S110-PREP-AREA-ISPS0580-EX.
        s110PrepAreaIsps0580();
        //
        // COB_CODE: PERFORM S120-CALL-ISPS0580
        //              THRU S120-CALL-ISPS0580-EX.
        s120CallIsps0580();
        //
        // COB_CODE: IF IDSV0001-ESITO-OK
        //                 THRU S140-CALL-ISPS0590-EX
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: PERFORM S130-PREP-AREA-ISPS0590
            //              THRU S130-PREP-AREA-ISPS0590-EX
            s130PrepAreaIsps0590();
            //
            // COB_CODE: PERFORM S140-CALL-ISPS0590
            //              THRU S140-CALL-ISPS0590-EX
            s140CallIsps0590();
        }
    }

    /**Original name: S110-PREP-AREA-ISPS0580<br>
	 * <pre>----------------------------------------------------------------*
	 *   VALORIZZAZIONE CAMPI DI INPUT DA PASSARE AL SERVIZIO          *
	 * ----------------------------------------------------------------*</pre>*/
    private void s110PrepAreaIsps0580() {
        // COB_CODE: INITIALIZE AREA-IO-ISPS0580.
        initAreaIoIsps0580();
        //
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //             TO ISPC0580-COD-COMPAGNIA-ANIA.
        ws.getAreaIoIsps0580().getDatiInput().setCodCompagniaAniaFormatted(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAniaFormatted());
        //
        // COB_CODE: MOVE WK-DT-RICOR-DA
        //             TO ISPC0580-DATA-INIZ-ELAB
        ws.getAreaIoIsps0580().getDatiInput().setDataInizElab(ws.getDateWork().getWkDtRicorDaX().getWkDtRicorDaFormatted());
        // COB_CODE: MOVE WK-DT-RICOR-A
        //             TO ISPC0580-DATA-FINE-ELAB
        ws.getAreaIoIsps0580().getDatiInput().setDataFineElab(ws.getDateWork().getWkDtRicorAX().getWkDtRicorAFormatted());
        // COB_CODE: MOVE WK-DT-RICOR-A
        //             TO ISPC0580-DATA-RIFERIMENTO
        ws.getAreaIoIsps0580().getDatiInput().setDataRiferimento(ws.getDateWork().getWkDtRicorAX().getWkDtRicorAFormatted());
        //
        // COB_CODE: MOVE 1
        //             TO ISPC0580-LIVELLO-UTENTE.
        ws.getAreaIoIsps0580().getDatiInput().setIspc0580LivelloUtente(((short)1));
        //
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO
        //             TO ISPC0580-FUNZIONALITA.
        ws.getAreaIoIsps0580().getDatiInput().setIspc0580FunzionalitaFormatted(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimentoFormatted());
        //
        // COB_CODE: MOVE IDSV0001-SESSIONE
        //             TO ISPC0580-SESSION-ID.
        ws.getAreaIoIsps0580().getDatiInput().setSessionId(areaIdsv0001.getAreaComune().getSessione());
    }

    /**Original name: S120-CALL-ISPS0580<br>
	 * <pre>----------------------------------------------------------------*
	 *     CHIAMATA AL SERVIZIO CONSULTAZIONE FONDI
	 * ----------------------------------------------------------------*</pre>*/
    private void s120CallIsps0580() {
        Isps0580 isps0580 = null;
        // COB_CODE: CALL ISPS0580 USING AREA-IDSV0001
        //                               WCOM-AREA-STATI
        //                               AREA-IO-ISPS0580
        //             ON EXCEPTION
        //                    THRU GESTIONE-ERR-SIST-EX
        //           END-CALL.
        try {
            isps0580 = Isps0580.getInstance();
            isps0580.run(areaIdsv0001, wcomIoStati, ws.getAreaIoIsps0580());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE 'ISPS0580'
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe("ISPS0580");
            // COB_CODE: MOVE 'SERVIZIO ACTUATOR DIAGNOSTICO'
            //             TO CALL-DESC
            ws.getIdsv0002().setCallDesc("SERVIZIO ACTUATOR DIAGNOSTICO");
            // COB_CODE: MOVE 'CALL-ISPS0580'
            //             TO WK-LABEL-ERR
            ws.getWsVariabili().setWkLabelErr("CALL-ISPS0580");
            // COB_CODE: PERFORM GESTIONE-ERR-SIST
            //              THRU GESTIONE-ERR-SIST-EX
            gestioneErrSist();
        }
    }

    /**Original name: S130-PREP-AREA-ISPS0590<br>
	 * <pre>----------------------------------------------------------------*
	 *   VALORIZZAZIONE CAMPI DI INPUT DA PASSARE AL SERVIZIO          *
	 * ----------------------------------------------------------------*</pre>*/
    private void s130PrepAreaIsps0590() {
        // COB_CODE: INITIALIZE AREA-IO-ISPS0590.
        initAreaIoIsps0590();
        //
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //             TO ISPC0590-COD-COMPAGNIA-ANIA.
        ws.getAreaIoIsps0590().getDatiInput().setCodCompagniaAniaFormatted(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAniaFormatted());
        //
        // COB_CODE: MOVE WK-DT-RICOR-DA
        //             TO ISPC0590-DATA-INIZ-ELAB
        ws.getAreaIoIsps0590().getDatiInput().setDataInizElab(ws.getDateWork().getWkDtRicorDaX().getWkDtRicorDaFormatted());
        // COB_CODE: MOVE WK-DT-RICOR-A
        //             TO ISPC0590-DATA-FINE-ELAB
        ws.getAreaIoIsps0590().getDatiInput().setDataFineElab(ws.getDateWork().getWkDtRicorAX().getWkDtRicorAFormatted());
        // COB_CODE: MOVE WK-DT-RICOR-A
        //             TO ISPC0590-DATA-RIFERIMENTO
        ws.getAreaIoIsps0590().getDatiInput().setDataRiferimento(ws.getDateWork().getWkDtRicorAX().getWkDtRicorAFormatted());
        //
        // COB_CODE: MOVE 1
        //             TO ISPC0590-LIVELLO-UTENTE.
        ws.getAreaIoIsps0590().getDatiInput().setIspc0580LivelloUtente(((short)1));
        //
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO
        //             TO ISPC0590-FUNZIONALITA.
        ws.getAreaIoIsps0590().getDatiInput().setIspc0580FunzionalitaFormatted(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimentoFormatted());
        //
        // COB_CODE: MOVE IDSV0001-SESSIONE
        //             TO ISPC0590-SESSION-ID.
        ws.getAreaIoIsps0590().getDatiInput().setSessionId(areaIdsv0001.getAreaComune().getSessione());
    }

    /**Original name: S140-CALL-ISPS0590<br>
	 * <pre>----------------------------------------------------------------*
	 *     CHIAMATA AL SERVIZIO TASSI MINIMI
	 * ----------------------------------------------------------------*</pre>*/
    private void s140CallIsps0590() {
        Isps0590 isps0590 = null;
        // COB_CODE: CALL ISPS0590 USING AREA-IDSV0001
        //                               WCOM-AREA-STATI
        //                               AREA-IO-ISPS0590
        //             ON EXCEPTION
        //                    THRU GESTIONE-ERR-SIST-EX
        //           END-CALL.
        try {
            isps0590 = Isps0590.getInstance();
            isps0590.run(areaIdsv0001, wcomIoStati, ws.getAreaIoIsps0590());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE 'ISPS0590'
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe("ISPS0590");
            // COB_CODE: MOVE 'SERVIZIO ACTUATOR DIAGNOSTICO'
            //             TO CALL-DESC
            ws.getIdsv0002().setCallDesc("SERVIZIO ACTUATOR DIAGNOSTICO");
            // COB_CODE: MOVE 'CALL-ISPS0590'
            //             TO WK-LABEL-ERR
            ws.getWsVariabili().setWkLabelErr("CALL-ISPS0590");
            // COB_CODE: PERFORM GESTIONE-ERR-SIST
            //              THRU GESTIONE-ERR-SIST-EX
            gestioneErrSist();
        }
    }

    /**Original name: S8140-APERTURA-GRAV-ERR<br>
	 * <pre>----------------------------------------------------------------*
	 *           GESTIONE FILE ESTERNO DELLE GRAVITA ERRORI            *
	 * ----------------------------------------------------------------*</pre>*/
    private void s8140AperturaGravErr() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 0                            TO ELE-MAX-ERRORI
        ws.setEleMaxErrori(0);
        //
        // COB_CODE: SET OPEN-FILESQS-OPER             TO TRUE
        ws.getIabvsqs1().getOperazioneFilesqs().setOpenFilesqsOper();
        // COB_CODE: SET OPEN-FILESQS1-NO              TO TRUE
        ws.getIabvsqs1().getFlagOpenFilesqs1().setOpenFilesqs1No();
        // COB_CODE: SET OPEN-FILESQS2-NO              TO TRUE
        ws.getIabvsqs1().getFlagOpenFilesqs2().setOpenFilesqs2No();
        // COB_CODE: SET OPEN-FILESQS3-NO              TO TRUE
        ws.getIabvsqs1().getFlagOpenFilesqs3().setOpenFilesqs3No();
        // COB_CODE: SET OPEN-FILESQS4-NO              TO TRUE
        ws.getIabvsqs1().getFlagOpenFilesqs4().setOpenFilesqs4No();
        // COB_CODE: SET OPEN-FILESQS5-SI              TO TRUE
        ws.getIabvsqs1().getFlagOpenFilesqs5().setOpenFilesqs5Si();
        // COB_CODE: SET OPEN-FILESQS6-NO              TO TRUE
        ws.getIabvsqs1().getFlagOpenFilesqs6().setOpenFilesqs6No();
        // COB_CODE: SET INPUT-FILESQS5                TO TRUE
        ws.getIabvsqs1().getOpenTypeFilesqs5().setInputFilesqs5();
        // COB_CODE: PERFORM CALL-FILESEQ              THRU CALL-FILESEQ-EX
        callFileseq();
        // COB_CODE: IF NOT FILE-STATUS-OK
        //                 THRU EX-S0300
        //           END-IF
        if (!ws.getIabcsq99().getFileStatus().isFileStatusOk()) {
            // COB_CODE: MOVE WK-PGM            TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL          TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWsVariabili().getWkLabel());
            // COB_CODE: MOVE '005166'          TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005166");
            // COB_CODE: MOVE SPACES            TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("");
            // COB_CODE: MOVE SEQUENTIAL-DESC-ERRORE-ESTESA
            //                                  TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr(ws.getIabcsq99().getSequentialDescErroreEstesa());
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
        // COB_CODE: MOVE 0 TO IX-GRAV
        ws.getIxIndici().setGrav(((short)0));
        // COB_CODE: PERFORM UNTIL IDSV0001-ESITO-KO
        //                      OR FILESQS5-EOF-SI
        //                      OR SEQUENTIAL-ESITO-KO
        //                   END-IF
        //           END-PERFORM
        while (!(areaIdsv0001.getEsito().isIdsv0001EsitoKo() || ws.getIabvsqs1().getEofFilesqs5().isFilesqs5EofSi() || ws.getIabcsq99().getSequentialEsito().isSequentialEsitoKo())) {
            // COB_CODE: SET READ-FILESQS-OPER      TO TRUE
            ws.getIabvsqs1().getOperazioneFilesqs().setReadFilesqsOper();
            // COB_CODE: SET READ-FILESQS5-SI       TO TRUE
            ws.getIabvsqs1().getFlagReadFilesqs5().setReadFilesqs5Si();
            // COB_CODE: PERFORM CALL-FILESEQ     THRU CALL-FILESEQ-EX
            callFileseq();
            // COB_CODE: IF SEQUENTIAL-ESITO-KO
            //                 THRU EX-S0300
            //           END-IF
            if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoKo()) {
                // COB_CODE: MOVE SPACES TO IEAI9901-PARAMETRI-ERR
                ws.getIeai9901Area().setParametriErr("");
                // COB_CODE: MOVE '005016'
                //               TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("005016");
                // COB_CODE: MOVE WK-PGM
                //               TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: STRING 'ERRORE LETTURA SEQ'
                //                  DELIMITED BY SIZE INTO
                //                  IEAI9901-PARAMETRI-ERR
                //           END-STRING
                ws.getIeai9901Area().setParametriErr("ERRORE LETTURA SEQ");
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
            }
            // COB_CODE: IF SEQUENTIAL-ESITO-OK
            //              END-IF
            //           END-IF
            if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoOk()) {
                // COB_CODE: IF INTESTAZIONE-SI
                //              SET INTESTAZIONE-NO TO TRUE
                //           ELSE
                //              END-IF
                //           END-IF
                if (ws.getAreaFlag().getFlagIntErr().isSi()) {
                    // COB_CODE: SET INTESTAZIONE-NO TO TRUE
                    ws.getAreaFlag().getFlagIntErr().setNo();
                }
                else if (ws.getIabvsqs1().getEofFilesqs5().isFilesqs5EofNo()) {
                    // COB_CODE: IF FILESQS5-EOF-NO
                    //                TO ELE-MAX-ERRORI
                    //           END-IF
                    // COB_CODE: MOVE FILESQS5-REC
                    //             TO TRACCIATO-FILE-GRAV
                    ws.getTracciatoFileGrav().setTracciatoFileGravFormatted(filesqs5To.getVariableFormatted());
                    // COB_CODE: ADD 1 TO IX-GRAV
                    ws.getIxIndici().setGrav(Trunc.toShort(1 + ws.getIxIndici().getGrav(), 4));
                    // COB_CODE: MOVE IN-NUM-CNTRL
                    //             TO CONTROLLO(IX-GRAV)
                    ws.getEleAreaErr(ws.getIxIndici().getGrav()).setControlloFormatted(ws.getTracciatoFileGrav().getNumCntrlFormatted());
                    // COB_CODE: MOVE IN-GRAV-CNTRL
                    //             TO GRAVITA(IX-GRAV)
                    ws.getEleAreaErr(ws.getIxIndici().getGrav()).setGravitaFormatted(ws.getTracciatoFileGrav().getGravCntrlFormatted());
                    // COB_CODE: MOVE IX-GRAV
                    //             TO ELE-MAX-ERRORI
                    ws.setEleMaxErrori(ws.getIxIndici().getGrav());
                }
            }
        }
        //
        // COB_CODE: IF IDSV0001-ESITO-OK
        //           AND FILESQS5-APERTO
        //              END-IF
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk() && ws.getIabvsqs1().getStatoFilesqs5().isAperto()) {
            // COB_CODE: SET CLOSE-FILESQS-OPER    TO TRUE
            ws.getIabvsqs1().getOperazioneFilesqs().setCloseFilesqsOper();
            // COB_CODE: SET CLOSE-FILESQS1-NO     TO TRUE
            ws.getIabvsqs1().getFlagCloseFilesqs1().setCloseFilesqs1No();
            // COB_CODE: SET CLOSE-FILESQS2-NO     TO TRUE
            ws.getIabvsqs1().getFlagCloseFilesqs2().setCloseFilesqs2No();
            // COB_CODE: SET CLOSE-FILESQS3-NO     TO TRUE
            ws.getIabvsqs1().getFlagCloseFilesqs3().setCloseFilesqs3No();
            // COB_CODE: SET CLOSE-FILESQS4-NO     TO TRUE
            ws.getIabvsqs1().getFlagCloseFilesqs4().setCloseFilesqs4No();
            // COB_CODE: SET CLOSE-FILESQS5-SI     TO TRUE
            ws.getIabvsqs1().getFlagCloseFilesqs5().setSi();
            // COB_CODE: SET CLOSE-FILESQS6-NO     TO TRUE
            ws.getIabvsqs1().getFlagCloseFilesqs6().setCloseFilesqs6No();
            // COB_CODE: PERFORM CALL-FILESEQ      THRU CALL-FILESEQ-EX
            callFileseq();
            // COB_CODE: IF NOT FILE-STATUS-OK
            //                 THRU EX-S0300
            //           END-IF
            if (!ws.getIabcsq99().getFileStatus().isFileStatusOk()) {
                // COB_CODE: MOVE WK-PGM            TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE WK-LABEL          TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr(ws.getWsVariabili().getWkLabel());
                // COB_CODE: MOVE '005166'          TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("005166");
                // COB_CODE: MOVE SPACES            TO IEAI9901-PARAMETRI-ERR
                ws.getIeai9901Area().setParametriErr("");
                // COB_CODE: MOVE SEQUENTIAL-DESC-ERRORE-ESTESA
                //                                  TO IEAI9901-PARAMETRI-ERR
                ws.getIeai9901Area().setParametriErr(ws.getIabcsq99().getSequentialDescErroreEstesa());
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
            }
        }
    }

    /**Original name: GESTIONE-ERR-SIST<br>
	 * <pre>----------------------------------------------------------------*
	 *            GESTIONE STANDARD DELL'ERRORE DI SISTEMA             *
	 * ----------------------------------------------------------------*</pre>*/
    private void gestioneErrSist() {
        // COB_CODE: MOVE WK-LABEL-ERR
        //             TO IEAI9901-LABEL-ERR
        ws.getIeai9901Area().setLabelErr(ws.getWsVariabili().getWkLabelErr());
        //
        // COB_CODE: PERFORM S0290-ERRORE-DI-SISTEMA
        //              THRU EX-S0290.
        s0290ErroreDiSistema();
    }

    /**Original name: CALL-FILESEQ<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES GESTIONE FILE SEQUENZIALI
	 * ----------------------------------------------------------------*
	 * *************************************************************
	 *  STATEMETS X GESTIONE CON 6 FILES SEQUENZIALI
	 *  N.B. - DA UTILIZZARE CON LA COPY IABVSQS1
	 * *************************************************************</pre>*/
    private void callFileseq() {
        // COB_CODE: PERFORM INIZIO-FILESEQ      THRU INIZIO-FILESEQ-EX.
        inizioFileseq();
        // COB_CODE: PERFORM ELABORA-FILESEQ    THRU ELABORA-FILESEQ-EX
        elaboraFileseq();
        // COB_CODE: IF SEQUENTIAL-ESITO-OK
        //              PERFORM FINE-FILESEQ       THRU FINE-FILESEQ-EX
        //           END-IF.
        if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoOk()) {
            // COB_CODE: PERFORM FINE-FILESEQ       THRU FINE-FILESEQ-EX
            fineFileseq();
        }
    }

    /**Original name: INIZIO-FILESEQ<br>
	 * <pre>*****************************************************************</pre>*/
    private void inizioFileseq() {
        // COB_CODE: SET SEQUENTIAL-ESITO-OK   TO TRUE.
        ws.getIabcsq99().getSequentialEsito().setSequentialEsitoOk();
    }

    /**Original name: CHECK-RETURN-CODE-FILESEQ<br>
	 * <pre>*****************************************************************</pre>*/
    private void checkReturnCodeFileseq() {
        // COB_CODE: EVALUATE TRUE
        //               WHEN FILE-STATUS-OK
        //                    CONTINUE
        //               WHEN FILE-STATUS-END-OF-FILE
        //                  END-IF
        //               WHEN OTHER
        //                                            THRU COMPONI-MSG-FILESEQ-EX
        //           END-EVALUATE.
        switch (ws.getIabcsq99().getFileStatus().getFileStatus()) {

            case FileStatus1.OK:// COB_CODE: CONTINUE
            //continue
                break;

            case FileStatus1.END_OF_FILE:// COB_CODE: IF NOT READ-FILESQS-OPER
                //                                     THRU COMPONI-MSG-FILESEQ-EX
                //           END-IF
                if (!ws.getIabvsqs1().getOperazioneFilesqs().isReadFilesqsOper()) {
                    // COB_CODE: SET SEQUENTIAL-ESITO-KO  TO TRUE
                    ws.getIabcsq99().getSequentialEsito().setSequentialEsitoKo();
                    // COB_CODE: PERFORM COMPONI-MSG-FILESEQ
                    //                                  THRU COMPONI-MSG-FILESEQ-EX
                    componiMsgFileseq();
                }
                break;

            default:// COB_CODE: SET SEQUENTIAL-ESITO-KO  TO TRUE
                ws.getIabcsq99().getSequentialEsito().setSequentialEsitoKo();
                // COB_CODE: PERFORM COMPONI-MSG-FILESEQ
                //                                     THRU COMPONI-MSG-FILESEQ-EX
                componiMsgFileseq();
                break;
        }
    }

    /**Original name: COMPONI-MSG-FILESEQ<br>
	 * <pre>*****************************************************************</pre>*/
    private void componiMsgFileseq() {
        // COB_CODE: MOVE FILE-STATUS           TO MSG-RC
        ws.getIabcsq99().getMsgErrFile().setRc(ws.getIabcsq99().getFileStatus().getFileStatus());
        // COB_CODE: MOVE MSG-ERR-FILE          TO SEQUENTIAL-DESC-ERRORE-ESTESA.
        ws.getIabcsq99().setSequentialDescErroreEstesa(ws.getIabcsq99().getMsgErrFile().getMsgErrFileFormatted());
    }

    /**Original name: ELABORA-FILESEQ<br>
	 * <pre>*****************************************************************</pre>*/
    private void elaboraFileseq() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN OPEN-FILESQS-OPER
        //                 PERFORM OPEN-FILESEQ        THRU OPEN-FILESEQ-EX
        //              WHEN CLOSE-FILESQS-OPER
        //                 PERFORM CLOSE-FILESEQ       THRU CLOSE-FILESEQ-EX
        //              WHEN READ-FILESQS-OPER
        //                   PERFORM READ-FILESEQ      THRU READ-FILESEQ-EX
        //              WHEN WRITE-FILESQS-OPER
        //                 PERFORM WRITE-FILESEQ       THRU WRITE-FILESEQ-EX
        //              WHEN REWRITE-FILESQS-OPER
        //                 PERFORM REWRITE-FILESEQ     THRU REWRITE-FILESEQ-EX
        //              WHEN OTHER
        //                                        TO SEQUENTIAL-DESC-ERRORE-ESTESA
        //           END-EVALUATE.
        switch (ws.getIabvsqs1().getOperazioneFilesqs().getOperazioneFilesqs()) {

            case OperazioneFilesqs.OPEN_FILESQS_OPER:// COB_CODE: PERFORM OPEN-FILESEQ        THRU OPEN-FILESEQ-EX
                openFileseq();
                break;

            case OperazioneFilesqs.CLOSE_FILESQS_OPER:// COB_CODE: PERFORM CLOSE-FILESEQ       THRU CLOSE-FILESEQ-EX
                closeFileseq();
                break;

            case OperazioneFilesqs.READ_FILESQS_OPER:// COB_CODE: PERFORM READ-FILESEQ      THRU READ-FILESEQ-EX
                readFileseq();
                break;

            case OperazioneFilesqs.WRITE_FILESQS_OPER:// COB_CODE: PERFORM WRITE-FILESEQ       THRU WRITE-FILESEQ-EX
                writeFileseq();
                break;

            case OperazioneFilesqs.REWRITE_FILESQS_OPER:// COB_CODE: PERFORM REWRITE-FILESEQ     THRU REWRITE-FILESEQ-EX
                rewriteFileseq();
                break;

            default:// COB_CODE: SET SEQUENTIAL-ESITO-KO  TO TRUE
                ws.getIabcsq99().getSequentialEsito().setSequentialEsitoKo();
                // COB_CODE: MOVE 'TIPO OPERAZIONE NON VALIDA'
                //                                  TO SEQUENTIAL-DESC-ERRORE-ESTESA
                ws.getIabcsq99().setSequentialDescErroreEstesa("TIPO OPERAZIONE NON VALIDA");
                break;
        }
    }

    /**Original name: OPEN-FILESEQ<br>
	 * <pre>*****************************************************************</pre>*/
    private void openFileseq() {
        // COB_CODE: SET MSG-OPEN                         TO TRUE.
        ws.getIabcsq99().getMsgErrFile().getOperazioni().setOpen();
        // COB_CODE: IF OPEN-FILESQS1-SI OR
        //              OPEN-FILESQS-ALL-SI
        //              PERFORM OPEN-FILESQS1             THRU OPEN-FILESQS1-EX
        //           END-IF
        if (ws.getIabvsqs1().getFlagOpenFilesqs1().isSi() || ws.getIabvsqs1().getFlagOpenFilesqsAll().isSi()) {
            // COB_CODE: PERFORM OPEN-FILESQS1             THRU OPEN-FILESQS1-EX
            openFilesqs1();
        }
        // COB_CODE: IF SEQUENTIAL-ESITO-OK
        //              END-IF
        //           END-IF.
        if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoOk()) {
            // COB_CODE: IF OPEN-FILESQS2-SI OR
            //              OPEN-FILESQS-ALL-SI
            //              PERFORM OPEN-FILESQS2          THRU OPEN-FILESQS2-EX
            //           END-IF
            if (ws.getIabvsqs1().getFlagOpenFilesqs2().isSi() || ws.getIabvsqs1().getFlagOpenFilesqsAll().isSi()) {
                // COB_CODE: PERFORM OPEN-FILESQS2          THRU OPEN-FILESQS2-EX
                openFilesqs2();
            }
            // COB_CODE: IF SEQUENTIAL-ESITO-OK
            //              END-IF
            //           END-IF
            if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoOk()) {
                // COB_CODE: IF OPEN-FILESQS3-SI OR
                //              OPEN-FILESQS-ALL-SI
                //              PERFORM OPEN-FILESQS3       THRU OPEN-FILESQS3-EX
                //           END-IF
                if (ws.getIabvsqs1().getFlagOpenFilesqs3().isSi() || ws.getIabvsqs1().getFlagOpenFilesqsAll().isSi()) {
                    // COB_CODE: PERFORM OPEN-FILESQS3       THRU OPEN-FILESQS3-EX
                    openFilesqs3();
                }
                // COB_CODE: IF SEQUENTIAL-ESITO-OK
                //              END-IF
                //           END-IF
                if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoOk()) {
                    // COB_CODE: IF OPEN-FILESQS4-SI OR
                    //              OPEN-FILESQS-ALL-SI
                    //              PERFORM OPEN-FILESQS4    THRU OPEN-FILESQS4-EX
                    //           END-IF
                    if (ws.getIabvsqs1().getFlagOpenFilesqs4().isSi() || ws.getIabvsqs1().getFlagOpenFilesqsAll().isSi()) {
                        // COB_CODE: PERFORM OPEN-FILESQS4    THRU OPEN-FILESQS4-EX
                        openFilesqs4();
                    }
                    // COB_CODE: IF SEQUENTIAL-ESITO-OK
                    //              END-IF
                    //           END-IF
                    if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoOk()) {
                        // COB_CODE: IF OPEN-FILESQS5-SI OR
                        //              OPEN-FILESQS-ALL-SI
                        //              PERFORM OPEN-FILESQS5 THRU OPEN-FILESQS5-EX
                        //           END-IF
                        if (ws.getIabvsqs1().getFlagOpenFilesqs5().isSi() || ws.getIabvsqs1().getFlagOpenFilesqsAll().isSi()) {
                            // COB_CODE: PERFORM OPEN-FILESQS5 THRU OPEN-FILESQS5-EX
                            openFilesqs5();
                        }
                        // COB_CODE: IF SEQUENTIAL-ESITO-OK
                        //              END-IF
                        //           END-IF
                        if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoOk()) {
                            // COB_CODE: IF OPEN-FILESQS6-SI OR
                            //              OPEN-FILESQS-ALL-SI
                            //                                 THRU OPEN-FILESQS6-EX
                            //           END-IF
                            if (ws.getIabvsqs1().getFlagOpenFilesqs6().isOpenFilesqs6Si() || ws.getIabvsqs1().getFlagOpenFilesqsAll().isSi()) {
                                // COB_CODE: PERFORM OPEN-FILESQS6
                                //                              THRU OPEN-FILESQS6-EX
                                openFilesqs6();
                            }
                        }
                    }
                }
            }
        }
    }

    /**Original name: OPEN-FILESQS1<br>
	 * <pre>*****************************************************************</pre>*/
    private void openFilesqs1() {
        // COB_CODE: MOVE NOME-FILESQS1            TO MSG-NOME-FILE.
        ws.getIabcsq99().getMsgErrFile().setNomeFile(ws.getIabcsq99().getNomeFilesqs1());
        // COB_CODE: EVALUATE TRUE
        //              WHEN INPUT-FILESQS1
        //              WHEN INPUT-FILESQS-ALL
        //                   OPEN INPUT FILESQS1
        //              WHEN OUTPUT-FILESQS1
        //              WHEN OUTPUT-FILESQS-ALL
        //                   OPEN OUTPUT FILESQS1
        //              WHEN I-O-FILESQS1
        //              WHEN I-O-FILESQS-ALL
        //                   OPEN I-O FILESQS1
        //              WHEN EXTEND-FILESQS1
        //              WHEN EXTEND-FILESQS-ALL
        //                   OPEN EXTEND FILESQS1
        //              WHEN OTHER
        //                                        TO SEQUENTIAL-DESC-ERRORE-ESTESA
        //           END-EVALUATE
        if (ws.getIabvsqs1().getOpenTypeFilesqs1().isInputFilesqs1() || ws.getIabvsqs1().getOpenTypeFilesqsAll().isInputFilesqs1()) {
            // COB_CODE: OPEN INPUT FILESQS1
            filesqs1DAO.open(OpenMode.READ, "Lrgs0660");
            ws.getIabcsq99().setFsFilesqs1(filesqs1DAO.getFileStatus().getStatusCodeFormatted());
        }
        else if (ws.getIabvsqs1().getOpenTypeFilesqs1().isOutputFilesqs1() || ws.getIabvsqs1().getOpenTypeFilesqsAll().isOutputFilesqs1()) {
            // COB_CODE: OPEN OUTPUT FILESQS1
            filesqs1DAO.open(OpenMode.WRITE, "Lrgs0660");
            ws.getIabcsq99().setFsFilesqs1(filesqs1DAO.getFileStatus().getStatusCodeFormatted());
        }
        else if (ws.getIabvsqs1().getOpenTypeFilesqs1().isiOFilesqs1() || ws.getIabvsqs1().getOpenTypeFilesqsAll().isiOFilesqs1()) {
            // COB_CODE: OPEN I-O FILESQS1
            filesqs1DAO.open(OpenMode.UPDATE, "Lrgs0660");
            ws.getIabcsq99().setFsFilesqs1(filesqs1DAO.getFileStatus().getStatusCodeFormatted());
        }
        else if (ws.getIabvsqs1().getOpenTypeFilesqs1().isExtendFilesqs1() || ws.getIabvsqs1().getOpenTypeFilesqsAll().isExtendFilesqs1()) {
            // COB_CODE: OPEN EXTEND FILESQS1
            filesqs1DAO.open(OpenMode.APPEND, "Lrgs0660");
            ws.getIabcsq99().setFsFilesqs1(filesqs1DAO.getFileStatus().getStatusCodeFormatted());
        }
        else {
            // COB_CODE: SET SEQUENTIAL-ESITO-KO  TO TRUE
            ws.getIabcsq99().getSequentialEsito().setSequentialEsitoKo();
            // COB_CODE: MOVE 'TIPO OPEN NON VALIDO'
            //                                TO SEQUENTIAL-DESC-ERRORE-ESTESA
            ws.getIabcsq99().setSequentialDescErroreEstesa("TIPO OPEN NON VALIDO");
        }
        // COB_CODE: MOVE FS-FILESQS1          TO FILE-STATUS.
        ws.getIabcsq99().getFileStatus().setFileStatus(ws.getIabcsq99().getFsFilesqs1());
        // COB_CODE: PERFORM CHECK-RETURN-CODE-FILESEQ
        //                                     THRU CHECK-RETURN-CODE-FILESEQ-EX.
        checkReturnCodeFileseq();
        // COB_CODE: IF SEQUENTIAL-ESITO-OK
        //              SET FILESQS1-EOF-NO    TO TRUE
        //           END-IF.
        if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoOk()) {
            // COB_CODE: SET FILESQS1-APERTO    TO TRUE
            ws.getIabvsqs1().getStatoFilesqs1().setAperto();
            // COB_CODE: SET FILESQS1-EOF-NO    TO TRUE
            ws.getIabvsqs1().getEofFilesqs1().setNo();
        }
    }

    /**Original name: OPEN-FILESQS2<br>
	 * <pre>*****************************************************************</pre>*/
    private void openFilesqs2() {
        // COB_CODE: MOVE NOME-FILESQS2        TO MSG-NOME-FILE.
        ws.getIabcsq99().getMsgErrFile().setNomeFile(ws.getIabcsq99().getNomeFilesqs2());
        // COB_CODE: EVALUATE TRUE
        //              WHEN INPUT-FILESQS2
        //              WHEN INPUT-FILESQS-ALL
        //                   OPEN INPUT FILESQS2
        //              WHEN OUTPUT-FILESQS2
        //              WHEN OUTPUT-FILESQS-ALL
        //                   OPEN OUTPUT FILESQS2
        //              WHEN I-O-FILESQS2
        //              WHEN I-O-FILESQS-ALL
        //                   OPEN I-O FILESQS2
        //              WHEN EXTEND-FILESQS2
        //              WHEN EXTEND-FILESQS-ALL
        //                   OPEN EXTEND FILESQS2
        //              WHEN OTHER
        //                                        TO SEQUENTIAL-DESC-ERRORE-ESTESA
        //           END-EVALUATE
        if (ws.getIabvsqs1().getOpenTypeFilesqs2().isInputFilesqs1() || ws.getIabvsqs1().getOpenTypeFilesqsAll().isInputFilesqs1()) {
            // COB_CODE: OPEN INPUT FILESQS2
            filesqs2DAO.open(OpenMode.READ, "Lrgs0660");
            ws.getIabcsq99().setFsFilesqs2(filesqs2DAO.getFileStatus().getStatusCodeFormatted());
        }
        else if (ws.getIabvsqs1().getOpenTypeFilesqs2().isOutputFilesqs1() || ws.getIabvsqs1().getOpenTypeFilesqsAll().isOutputFilesqs1()) {
            // COB_CODE: OPEN OUTPUT FILESQS2
            filesqs2DAO.open(OpenMode.WRITE, "Lrgs0660");
            ws.getIabcsq99().setFsFilesqs2(filesqs2DAO.getFileStatus().getStatusCodeFormatted());
        }
        else if (ws.getIabvsqs1().getOpenTypeFilesqs2().isiOFilesqs1() || ws.getIabvsqs1().getOpenTypeFilesqsAll().isiOFilesqs1()) {
            // COB_CODE: OPEN I-O FILESQS2
            filesqs2DAO.open(OpenMode.UPDATE, "Lrgs0660");
            ws.getIabcsq99().setFsFilesqs2(filesqs2DAO.getFileStatus().getStatusCodeFormatted());
        }
        else if (ws.getIabvsqs1().getOpenTypeFilesqs2().isExtendFilesqs1() || ws.getIabvsqs1().getOpenTypeFilesqsAll().isExtendFilesqs1()) {
            // COB_CODE: OPEN EXTEND FILESQS2
            filesqs2DAO.open(OpenMode.APPEND, "Lrgs0660");
            ws.getIabcsq99().setFsFilesqs2(filesqs2DAO.getFileStatus().getStatusCodeFormatted());
        }
        else {
            // COB_CODE: SET SEQUENTIAL-ESITO-KO  TO TRUE
            ws.getIabcsq99().getSequentialEsito().setSequentialEsitoKo();
            // COB_CODE: MOVE 'TIPO OPEN NON VALIDO'
            //                                TO SEQUENTIAL-DESC-ERRORE-ESTESA
            ws.getIabcsq99().setSequentialDescErroreEstesa("TIPO OPEN NON VALIDO");
        }
        // COB_CODE: MOVE FS-FILESQS2          TO FILE-STATUS.
        ws.getIabcsq99().getFileStatus().setFileStatus(ws.getIabcsq99().getFsFilesqs2());
        // COB_CODE: PERFORM CHECK-RETURN-CODE-FILESEQ
        //                                     THRU CHECK-RETURN-CODE-FILESEQ-EX.
        checkReturnCodeFileseq();
        // COB_CODE: IF SEQUENTIAL-ESITO-OK
        //              SET FILESQS2-EOF-NO    TO TRUE
        //           END-IF.
        if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoOk()) {
            // COB_CODE: SET FILESQS2-APERTO    TO TRUE
            ws.getIabvsqs1().getStatoFilesqs2().setAperto();
            // COB_CODE: SET FILESQS2-EOF-NO    TO TRUE
            ws.getIabvsqs1().getEofFilesqs2().setNo();
        }
    }

    /**Original name: OPEN-FILESQS3<br>
	 * <pre>*****************************************************************</pre>*/
    private void openFilesqs3() {
        // COB_CODE: MOVE NOME-FILESQS3          TO MSG-NOME-FILE.
        ws.getIabcsq99().getMsgErrFile().setNomeFile(ws.getIabcsq99().getNomeFilesqs3());
        // COB_CODE: EVALUATE TRUE
        //              WHEN INPUT-FILESQS3
        //              WHEN INPUT-FILESQS-ALL
        //                   OPEN INPUT FILESQS3
        //              WHEN OUTPUT-FILESQS3
        //              WHEN OUTPUT-FILESQS-ALL
        //                   OPEN OUTPUT FILESQS3
        //              WHEN I-O-FILESQS3
        //              WHEN I-O-FILESQS-ALL
        //                   OPEN I-O FILESQS3
        //              WHEN EXTEND-FILESQS3
        //              WHEN EXTEND-FILESQS-ALL
        //                   OPEN EXTEND FILESQS3
        //              WHEN OTHER
        //                                        TO SEQUENTIAL-DESC-ERRORE-ESTESA
        //           END-EVALUATE
        if (ws.getIabvsqs1().getOpenTypeFilesqs3().isInputFilesqs1() || ws.getIabvsqs1().getOpenTypeFilesqsAll().isInputFilesqs1()) {
            // COB_CODE: OPEN INPUT FILESQS3
            filesqs3DAO.open(OpenMode.READ, "Lrgs0660");
            ws.getIabcsq99().setFsFilesqs3(filesqs3DAO.getFileStatus().getStatusCodeFormatted());
        }
        else if (ws.getIabvsqs1().getOpenTypeFilesqs3().isOutputFilesqs1() || ws.getIabvsqs1().getOpenTypeFilesqsAll().isOutputFilesqs1()) {
            // COB_CODE: OPEN OUTPUT FILESQS3
            filesqs3DAO.open(OpenMode.WRITE, "Lrgs0660");
            ws.getIabcsq99().setFsFilesqs3(filesqs3DAO.getFileStatus().getStatusCodeFormatted());
        }
        else if (ws.getIabvsqs1().getOpenTypeFilesqs3().isiOFilesqs1() || ws.getIabvsqs1().getOpenTypeFilesqsAll().isiOFilesqs1()) {
            // COB_CODE: OPEN I-O FILESQS3
            filesqs3DAO.open(OpenMode.UPDATE, "Lrgs0660");
            ws.getIabcsq99().setFsFilesqs3(filesqs3DAO.getFileStatus().getStatusCodeFormatted());
        }
        else if (ws.getIabvsqs1().getOpenTypeFilesqs3().isExtendFilesqs1() || ws.getIabvsqs1().getOpenTypeFilesqsAll().isExtendFilesqs1()) {
            // COB_CODE: OPEN EXTEND FILESQS3
            filesqs3DAO.open(OpenMode.APPEND, "Lrgs0660");
            ws.getIabcsq99().setFsFilesqs3(filesqs3DAO.getFileStatus().getStatusCodeFormatted());
        }
        else {
            // COB_CODE: SET SEQUENTIAL-ESITO-KO  TO TRUE
            ws.getIabcsq99().getSequentialEsito().setSequentialEsitoKo();
            // COB_CODE: MOVE 'TIPO OPEN NON VALIDO'
            //                                TO SEQUENTIAL-DESC-ERRORE-ESTESA
            ws.getIabcsq99().setSequentialDescErroreEstesa("TIPO OPEN NON VALIDO");
        }
        // COB_CODE: MOVE FS-FILESQS3          TO FILE-STATUS.
        ws.getIabcsq99().getFileStatus().setFileStatus(ws.getIabcsq99().getFsFilesqs3());
        // COB_CODE: PERFORM CHECK-RETURN-CODE-FILESEQ
        //                                     THRU CHECK-RETURN-CODE-FILESEQ-EX.
        checkReturnCodeFileseq();
        // COB_CODE: IF SEQUENTIAL-ESITO-OK
        //              SET FILESQS3-EOF-NO    TO TRUE
        //           END-IF.
        if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoOk()) {
            // COB_CODE: SET FILESQS3-APERTO    TO TRUE
            ws.getIabvsqs1().getStatoFilesqs3().setAperto();
            // COB_CODE: SET FILESQS3-EOF-NO    TO TRUE
            ws.getIabvsqs1().getEofFilesqs3().setNo();
        }
    }

    /**Original name: OPEN-FILESQS4<br>
	 * <pre>*****************************************************************</pre>*/
    private void openFilesqs4() {
        // COB_CODE: MOVE NOME-FILESQS4          TO MSG-NOME-FILE.
        ws.getIabcsq99().getMsgErrFile().setNomeFile(ws.getIabcsq99().getNomeFilesqs4());
        // COB_CODE: EVALUATE TRUE
        //              WHEN INPUT-FILESQS4
        //              WHEN INPUT-FILESQS-ALL
        //                   OPEN INPUT FILESQS4
        //              WHEN OUTPUT-FILESQS4
        //              WHEN OUTPUT-FILESQS-ALL
        //                   OPEN OUTPUT FILESQS4
        //              WHEN I-O-FILESQS4
        //              WHEN I-O-FILESQS-ALL
        //                   OPEN I-O FILESQS4
        //              WHEN EXTEND-FILESQS4
        //              WHEN EXTEND-FILESQS-ALL
        //                   OPEN EXTEND FILESQS4
        //              WHEN OTHER
        //                                        TO SEQUENTIAL-DESC-ERRORE-ESTESA
        //           END-EVALUATE
        if (ws.getIabvsqs1().getOpenTypeFilesqs4().isInputFilesqs1() || ws.getIabvsqs1().getOpenTypeFilesqsAll().isInputFilesqs1()) {
            // COB_CODE: OPEN INPUT FILESQS4
            filesqs4DAO.open(OpenMode.READ, "Lrgs0660");
            ws.getIabcsq99().setFsFilesqs4(filesqs4DAO.getFileStatus().getStatusCodeFormatted());
        }
        else if (ws.getIabvsqs1().getOpenTypeFilesqs4().isOutputFilesqs1() || ws.getIabvsqs1().getOpenTypeFilesqsAll().isOutputFilesqs1()) {
            // COB_CODE: OPEN OUTPUT FILESQS4
            filesqs4DAO.open(OpenMode.WRITE, "Lrgs0660");
            ws.getIabcsq99().setFsFilesqs4(filesqs4DAO.getFileStatus().getStatusCodeFormatted());
        }
        else if (ws.getIabvsqs1().getOpenTypeFilesqs4().isiOFilesqs1() || ws.getIabvsqs1().getOpenTypeFilesqsAll().isiOFilesqs1()) {
            // COB_CODE: OPEN I-O FILESQS4
            filesqs4DAO.open(OpenMode.UPDATE, "Lrgs0660");
            ws.getIabcsq99().setFsFilesqs4(filesqs4DAO.getFileStatus().getStatusCodeFormatted());
        }
        else if (ws.getIabvsqs1().getOpenTypeFilesqs4().isExtendFilesqs1() || ws.getIabvsqs1().getOpenTypeFilesqsAll().isExtendFilesqs1()) {
            // COB_CODE: OPEN EXTEND FILESQS4
            filesqs4DAO.open(OpenMode.APPEND, "Lrgs0660");
            ws.getIabcsq99().setFsFilesqs4(filesqs4DAO.getFileStatus().getStatusCodeFormatted());
        }
        else {
            // COB_CODE: SET SEQUENTIAL-ESITO-KO  TO TRUE
            ws.getIabcsq99().getSequentialEsito().setSequentialEsitoKo();
            // COB_CODE: MOVE 'TIPO OPEN NON VALIDO'
            //                                TO SEQUENTIAL-DESC-ERRORE-ESTESA
            ws.getIabcsq99().setSequentialDescErroreEstesa("TIPO OPEN NON VALIDO");
        }
        // COB_CODE: MOVE FS-FILESQS4          TO FILE-STATUS.
        ws.getIabcsq99().getFileStatus().setFileStatus(ws.getIabcsq99().getFsFilesqs4());
        // COB_CODE: PERFORM CHECK-RETURN-CODE-FILESEQ
        //                                     THRU CHECK-RETURN-CODE-FILESEQ-EX.
        checkReturnCodeFileseq();
        // COB_CODE: IF SEQUENTIAL-ESITO-OK
        //              SET FILESQS4-EOF-NO    TO TRUE
        //           END-IF.
        if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoOk()) {
            // COB_CODE: SET FILESQS4-APERTO    TO TRUE
            ws.getIabvsqs1().getStatoFilesqs4().setAperto();
            // COB_CODE: SET FILESQS4-EOF-NO    TO TRUE
            ws.getIabvsqs1().getEofFilesqs4().setNo();
        }
    }

    /**Original name: OPEN-FILESQS5<br>
	 * <pre>*****************************************************************</pre>*/
    private void openFilesqs5() {
        // COB_CODE: MOVE NOME-FILESQS5          TO MSG-NOME-FILE.
        ws.getIabcsq99().getMsgErrFile().setNomeFile(ws.getIabcsq99().getNomeFilesqs5());
        // COB_CODE: EVALUATE TRUE
        //              WHEN INPUT-FILESQS5
        //              WHEN INPUT-FILESQS-ALL
        //                   OPEN INPUT FILESQS5
        //              WHEN OUTPUT-FILESQS5
        //              WHEN OUTPUT-FILESQS-ALL
        //                   OPEN OUTPUT FILESQS5
        //              WHEN I-O-FILESQS5
        //              WHEN I-O-FILESQS-ALL
        //                   OPEN I-O FILESQS5
        //              WHEN EXTEND-FILESQS5
        //              WHEN EXTEND-FILESQS-ALL
        //                   OPEN EXTEND FILESQS5
        //              WHEN OTHER
        //                                        TO SEQUENTIAL-DESC-ERRORE-ESTESA
        //           END-EVALUATE
        if (ws.getIabvsqs1().getOpenTypeFilesqs5().isInputFilesqs1() || ws.getIabvsqs1().getOpenTypeFilesqsAll().isInputFilesqs1()) {
            // COB_CODE: OPEN INPUT FILESQS5
            filesqs5DAO.open(OpenMode.READ, "Lrgs0660");
            ws.getIabcsq99().setFsFilesqs5(filesqs5DAO.getFileStatus().getStatusCodeFormatted());
        }
        else if (ws.getIabvsqs1().getOpenTypeFilesqs5().isOutputFilesqs1() || ws.getIabvsqs1().getOpenTypeFilesqsAll().isOutputFilesqs1()) {
            // COB_CODE: OPEN OUTPUT FILESQS5
            filesqs5DAO.open(OpenMode.WRITE, "Lrgs0660");
            ws.getIabcsq99().setFsFilesqs5(filesqs5DAO.getFileStatus().getStatusCodeFormatted());
        }
        else if (ws.getIabvsqs1().getOpenTypeFilesqs5().isiOFilesqs1() || ws.getIabvsqs1().getOpenTypeFilesqsAll().isiOFilesqs1()) {
            // COB_CODE: OPEN I-O FILESQS5
            filesqs5DAO.open(OpenMode.UPDATE, "Lrgs0660");
            ws.getIabcsq99().setFsFilesqs5(filesqs5DAO.getFileStatus().getStatusCodeFormatted());
        }
        else if (ws.getIabvsqs1().getOpenTypeFilesqs5().isExtendFilesqs1() || ws.getIabvsqs1().getOpenTypeFilesqsAll().isExtendFilesqs1()) {
            // COB_CODE: OPEN EXTEND FILESQS5
            filesqs5DAO.open(OpenMode.APPEND, "Lrgs0660");
            ws.getIabcsq99().setFsFilesqs5(filesqs5DAO.getFileStatus().getStatusCodeFormatted());
        }
        else {
            // COB_CODE: SET SEQUENTIAL-ESITO-KO  TO TRUE
            ws.getIabcsq99().getSequentialEsito().setSequentialEsitoKo();
            // COB_CODE: MOVE 'TIPO OPEN NON VALIDO'
            //                                TO SEQUENTIAL-DESC-ERRORE-ESTESA
            ws.getIabcsq99().setSequentialDescErroreEstesa("TIPO OPEN NON VALIDO");
        }
        // COB_CODE: MOVE FS-FILESQS5          TO FILE-STATUS.
        ws.getIabcsq99().getFileStatus().setFileStatus(ws.getIabcsq99().getFsFilesqs5());
        // COB_CODE: PERFORM CHECK-RETURN-CODE-FILESEQ
        //                                     THRU CHECK-RETURN-CODE-FILESEQ-EX.
        checkReturnCodeFileseq();
        // COB_CODE: IF SEQUENTIAL-ESITO-OK
        //              SET FILESQS5-EOF-NO    TO TRUE
        //           END-IF.
        if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoOk()) {
            // COB_CODE: SET FILESQS5-APERTO    TO TRUE
            ws.getIabvsqs1().getStatoFilesqs5().setAperto();
            // COB_CODE: SET FILESQS5-EOF-NO    TO TRUE
            ws.getIabvsqs1().getEofFilesqs5().setNo();
        }
    }

    /**Original name: OPEN-FILESQS6<br>
	 * <pre>*****************************************************************</pre>*/
    private void openFilesqs6() {
        // COB_CODE: MOVE NOME-FILESQS6          TO MSG-NOME-FILE.
        ws.getIabcsq99().getMsgErrFile().setNomeFile(ws.getIabcsq99().getNomeFilesqs6());
        // COB_CODE: EVALUATE TRUE
        //              WHEN INPUT-FILESQS6
        //              WHEN INPUT-FILESQS-ALL
        //                   OPEN INPUT FILESQS6
        //              WHEN OUTPUT-FILESQS6
        //              WHEN OUTPUT-FILESQS-ALL
        //                   OPEN OUTPUT FILESQS6
        //              WHEN I-O-FILESQS6
        //              WHEN I-O-FILESQS-ALL
        //                   OPEN I-O FILESQS6
        //              WHEN EXTEND-FILESQS6
        //              WHEN EXTEND-FILESQS-ALL
        //                   OPEN EXTEND FILESQS6
        //              WHEN OTHER
        //                                        TO SEQUENTIAL-DESC-ERRORE-ESTESA
        //           END-EVALUATE
        if (ws.getIabvsqs1().getOpenTypeFilesqs6().isInputFilesqs1() || ws.getIabvsqs1().getOpenTypeFilesqsAll().isInputFilesqs1()) {
            // COB_CODE: OPEN INPUT FILESQS6
            filesqs6DAO.open(OpenMode.READ, "Lrgs0660");
            ws.getIabcsq99().setFsFilesqs6(filesqs6DAO.getFileStatus().getStatusCodeFormatted());
        }
        else if (ws.getIabvsqs1().getOpenTypeFilesqs6().isOutputFilesqs1() || ws.getIabvsqs1().getOpenTypeFilesqsAll().isOutputFilesqs1()) {
            // COB_CODE: OPEN OUTPUT FILESQS6
            filesqs6DAO.open(OpenMode.WRITE, "Lrgs0660");
            ws.getIabcsq99().setFsFilesqs6(filesqs6DAO.getFileStatus().getStatusCodeFormatted());
        }
        else if (ws.getIabvsqs1().getOpenTypeFilesqs6().isiOFilesqs1() || ws.getIabvsqs1().getOpenTypeFilesqsAll().isiOFilesqs1()) {
            // COB_CODE: OPEN I-O FILESQS6
            filesqs6DAO.open(OpenMode.UPDATE, "Lrgs0660");
            ws.getIabcsq99().setFsFilesqs6(filesqs6DAO.getFileStatus().getStatusCodeFormatted());
        }
        else if (ws.getIabvsqs1().getOpenTypeFilesqs6().isExtendFilesqs1() || ws.getIabvsqs1().getOpenTypeFilesqsAll().isExtendFilesqs1()) {
            // COB_CODE: OPEN EXTEND FILESQS6
            filesqs6DAO.open(OpenMode.APPEND, "Lrgs0660");
            ws.getIabcsq99().setFsFilesqs6(filesqs6DAO.getFileStatus().getStatusCodeFormatted());
        }
        else {
            // COB_CODE: SET SEQUENTIAL-ESITO-KO  TO TRUE
            ws.getIabcsq99().getSequentialEsito().setSequentialEsitoKo();
            // COB_CODE: MOVE 'TIPO OPEN NON VALIDO'
            //                                TO SEQUENTIAL-DESC-ERRORE-ESTESA
            ws.getIabcsq99().setSequentialDescErroreEstesa("TIPO OPEN NON VALIDO");
        }
        // COB_CODE: MOVE FS-FILESQS6          TO FILE-STATUS.
        ws.getIabcsq99().getFileStatus().setFileStatus(ws.getIabcsq99().getFsFilesqs6());
        // COB_CODE: PERFORM CHECK-RETURN-CODE-FILESEQ
        //                                     THRU CHECK-RETURN-CODE-FILESEQ-EX.
        checkReturnCodeFileseq();
        // COB_CODE: IF SEQUENTIAL-ESITO-OK
        //              SET FILESQS6-EOF-NO    TO TRUE
        //           END-IF.
        if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoOk()) {
            // COB_CODE: SET FILESQS6-APERTO    TO TRUE
            ws.getIabvsqs1().getStatoFilesqs6().setFilesqs6Aperto();
            // COB_CODE: SET FILESQS6-EOF-NO    TO TRUE
            ws.getIabvsqs1().getEofFilesqs6().setFilesqs6EofNo();
        }
    }

    /**Original name: CLOSE-FILESEQ<br>
	 * <pre>*****************************************************************</pre>*/
    private void closeFileseq() {
        // COB_CODE: SET MSG-CLOSE                      TO TRUE.
        ws.getIabcsq99().getMsgErrFile().getOperazioni().setClose();
        // COB_CODE: IF CLOSE-FILESQS1-SI OR
        //              CLOSE-FILESQS-ALL-SI
        //              PERFORM CLOSE-FILESQS1          THRU CLOSE-FILESQS1-EX
        //           END-IF
        if (ws.getIabvsqs1().getFlagCloseFilesqs1().isSi() || ws.getIabvsqs1().getFlagCloseFilesqsAll().isSi()) {
            // COB_CODE: PERFORM CLOSE-FILESQS1          THRU CLOSE-FILESQS1-EX
            closeFilesqs1();
        }
        // COB_CODE: IF SEQUENTIAL-ESITO-OK
        //              END-IF
        //           END-IF.
        if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoOk()) {
            // COB_CODE: IF CLOSE-FILESQS2-SI OR
            //              CLOSE-FILESQS-ALL-SI
            //              PERFORM CLOSE-FILESQS2       THRU CLOSE-FILESQS2-EX
            //           END-IF
            if (ws.getIabvsqs1().getFlagCloseFilesqs2().isSi() || ws.getIabvsqs1().getFlagCloseFilesqsAll().isSi()) {
                // COB_CODE: PERFORM CLOSE-FILESQS2       THRU CLOSE-FILESQS2-EX
                closeFilesqs2();
            }
            // COB_CODE: IF SEQUENTIAL-ESITO-OK
            //              END-IF
            //           END-IF
            if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoOk()) {
                // COB_CODE: IF CLOSE-FILESQS3-SI OR
                //              CLOSE-FILESQS-ALL-SI
                //              PERFORM CLOSE-FILESQS3    THRU CLOSE-FILESQS3-EX
                //           END-IF
                if (ws.getIabvsqs1().getFlagCloseFilesqs3().isSi() || ws.getIabvsqs1().getFlagCloseFilesqsAll().isSi()) {
                    // COB_CODE: PERFORM CLOSE-FILESQS3    THRU CLOSE-FILESQS3-EX
                    closeFilesqs3();
                }
                // COB_CODE: IF SEQUENTIAL-ESITO-OK
                //              END-IF
                //           END-IF
                if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoOk()) {
                    // COB_CODE: IF CLOSE-FILESQS4-SI OR
                    //              CLOSE-FILESQS-ALL-SI
                    //              PERFORM CLOSE-FILESQS4 THRU CLOSE-FILESQS4-EX
                    //           END-IF
                    if (ws.getIabvsqs1().getFlagCloseFilesqs4().isSi() || ws.getIabvsqs1().getFlagCloseFilesqsAll().isSi()) {
                        // COB_CODE: PERFORM CLOSE-FILESQS4 THRU CLOSE-FILESQS4-EX
                        closeFilesqs4();
                    }
                    // COB_CODE: IF SEQUENTIAL-ESITO-OK
                    //              END-IF
                    //           END-IF
                    if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoOk()) {
                        // COB_CODE: IF CLOSE-FILESQS5-SI OR
                        //              CLOSE-FILESQS-ALL-SI
                        //                                  THRU CLOSE-FILESQS5-EX
                        //           END-IF
                        if (ws.getIabvsqs1().getFlagCloseFilesqs5().isSi() || ws.getIabvsqs1().getFlagCloseFilesqsAll().isSi()) {
                            // COB_CODE: PERFORM CLOSE-FILESQS5
                            //                               THRU CLOSE-FILESQS5-EX
                            closeFilesqs5();
                        }
                        // COB_CODE: IF SEQUENTIAL-ESITO-OK
                        //              END-IF
                        //           END-IF
                        if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoOk()) {
                            // COB_CODE: IF CLOSE-FILESQS6-SI OR
                            //              CLOSE-FILESQS-ALL-SI
                            //                               THRU CLOSE-FILESQS6-EX
                            //           END-IF
                            if (ws.getIabvsqs1().getFlagCloseFilesqs6().isCloseFilesqs6Si() || ws.getIabvsqs1().getFlagCloseFilesqsAll().isSi()) {
                                // COB_CODE: PERFORM CLOSE-FILESQS6
                                //                            THRU CLOSE-FILESQS6-EX
                                closeFilesqs6();
                            }
                        }
                    }
                }
            }
        }
    }

    /**Original name: CLOSE-FILESQS1<br>
	 * <pre>*****************************************************************</pre>*/
    private void closeFilesqs1() {
        // COB_CODE: IF FILESQS1-APERTO
        //              END-IF
        //           END-IF.
        if (ws.getIabvsqs1().getStatoFilesqs1().isAperto()) {
            // COB_CODE: MOVE NOME-FILESQS1        TO MSG-NOME-FILE
            ws.getIabcsq99().getMsgErrFile().setNomeFile(ws.getIabcsq99().getNomeFilesqs1());
            // COB_CODE: CLOSE FILESQS1
            filesqs1DAO.close();
            ws.getIabcsq99().setFsFilesqs1(filesqs1DAO.getFileStatus().getStatusCodeFormatted());
            // COB_CODE: MOVE FS-FILESQS1          TO FILE-STATUS
            ws.getIabcsq99().getFileStatus().setFileStatus(ws.getIabcsq99().getFsFilesqs1());
            // COB_CODE: PERFORM CHECK-RETURN-CODE-FILESEQ
            //                   THRU CHECK-RETURN-CODE-FILESEQ-EX
            checkReturnCodeFileseq();
            // COB_CODE: IF SEQUENTIAL-ESITO-OK
            //              SET FILESQS1-CHIUSO    TO TRUE
            //           END-IF
            if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoOk()) {
                // COB_CODE: SET FILESQS1-CHIUSO    TO TRUE
                ws.getIabvsqs1().getStatoFilesqs1().setChiuso();
            }
        }
    }

    /**Original name: CLOSE-FILESQS2<br>
	 * <pre>*****************************************************************</pre>*/
    private void closeFilesqs2() {
        // COB_CODE: IF FILESQS2-APERTO
        //              END-IF
        //           END-IF.
        if (ws.getIabvsqs1().getStatoFilesqs2().isAperto()) {
            // COB_CODE: MOVE NOME-FILESQS2       TO MSG-NOME-FILE
            ws.getIabcsq99().getMsgErrFile().setNomeFile(ws.getIabcsq99().getNomeFilesqs2());
            // COB_CODE: CLOSE FILESQS2
            filesqs2DAO.close();
            ws.getIabcsq99().setFsFilesqs2(filesqs2DAO.getFileStatus().getStatusCodeFormatted());
            // COB_CODE: MOVE FS-FILESQS2         TO FILE-STATUS
            ws.getIabcsq99().getFileStatus().setFileStatus(ws.getIabcsq99().getFsFilesqs2());
            // COB_CODE: PERFORM CHECK-RETURN-CODE-FILESEQ
            //                   THRU CHECK-RETURN-CODE-FILESEQ-EX
            checkReturnCodeFileseq();
            // COB_CODE: IF SEQUENTIAL-ESITO-OK
            //              SET FILESQS2-CHIUSO    TO TRUE
            //           END-IF
            if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoOk()) {
                // COB_CODE: SET FILESQS2-CHIUSO    TO TRUE
                ws.getIabvsqs1().getStatoFilesqs2().setChiuso();
            }
        }
    }

    /**Original name: CLOSE-FILESQS3<br>
	 * <pre>*****************************************************************</pre>*/
    private void closeFilesqs3() {
        // COB_CODE: IF FILESQS3-APERTO
        //              END-IF
        //           END-IF.
        if (ws.getIabvsqs1().getStatoFilesqs3().isAperto()) {
            // COB_CODE: MOVE NOME-FILESQS3        TO MSG-NOME-FILE
            ws.getIabcsq99().getMsgErrFile().setNomeFile(ws.getIabcsq99().getNomeFilesqs3());
            // COB_CODE: CLOSE FILESQS3
            filesqs3DAO.close();
            ws.getIabcsq99().setFsFilesqs3(filesqs3DAO.getFileStatus().getStatusCodeFormatted());
            // COB_CODE: MOVE FS-FILESQS3          TO FILE-STATUS
            ws.getIabcsq99().getFileStatus().setFileStatus(ws.getIabcsq99().getFsFilesqs3());
            // COB_CODE: PERFORM CHECK-RETURN-CODE-FILESEQ
            //                   THRU CHECK-RETURN-CODE-FILESEQ-EX
            checkReturnCodeFileseq();
            // COB_CODE: IF SEQUENTIAL-ESITO-OK
            //              SET FILESQS3-CHIUSO    TO TRUE
            //           END-IF
            if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoOk()) {
                // COB_CODE: SET FILESQS3-CHIUSO    TO TRUE
                ws.getIabvsqs1().getStatoFilesqs3().setChiuso();
            }
        }
    }

    /**Original name: CLOSE-FILESQS4<br>
	 * <pre>*****************************************************************</pre>*/
    private void closeFilesqs4() {
        // COB_CODE: IF FILESQS4-APERTO
        //              END-IF
        //           END-IF.
        if (ws.getIabvsqs1().getStatoFilesqs4().isAperto()) {
            // COB_CODE: MOVE NOME-FILESQS4        TO MSG-NOME-FILE
            ws.getIabcsq99().getMsgErrFile().setNomeFile(ws.getIabcsq99().getNomeFilesqs4());
            // COB_CODE: CLOSE FILESQS4
            filesqs4DAO.close();
            ws.getIabcsq99().setFsFilesqs4(filesqs4DAO.getFileStatus().getStatusCodeFormatted());
            // COB_CODE: MOVE FS-FILESQS4          TO FILE-STATUS
            ws.getIabcsq99().getFileStatus().setFileStatus(ws.getIabcsq99().getFsFilesqs4());
            // COB_CODE: PERFORM CHECK-RETURN-CODE-FILESEQ
            //                   THRU CHECK-RETURN-CODE-FILESEQ-EX
            checkReturnCodeFileseq();
            // COB_CODE: IF SEQUENTIAL-ESITO-OK
            //              SET FILESQS4-CHIUSO    TO TRUE
            //           END-IF
            if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoOk()) {
                // COB_CODE: SET FILESQS4-CHIUSO    TO TRUE
                ws.getIabvsqs1().getStatoFilesqs4().setChiuso();
            }
        }
    }

    /**Original name: CLOSE-FILESQS5<br>
	 * <pre>*****************************************************************</pre>*/
    private void closeFilesqs5() {
        // COB_CODE: IF FILESQS5-APERTO
        //              END-IF
        //           END-IF.
        if (ws.getIabvsqs1().getStatoFilesqs5().isAperto()) {
            // COB_CODE: MOVE NOME-FILESQS5        TO MSG-NOME-FILE
            ws.getIabcsq99().getMsgErrFile().setNomeFile(ws.getIabcsq99().getNomeFilesqs5());
            // COB_CODE: CLOSE FILESQS5
            filesqs5DAO.close();
            ws.getIabcsq99().setFsFilesqs5(filesqs5DAO.getFileStatus().getStatusCodeFormatted());
            // COB_CODE: MOVE FS-FILESQS5          TO FILE-STATUS
            ws.getIabcsq99().getFileStatus().setFileStatus(ws.getIabcsq99().getFsFilesqs5());
            // COB_CODE: PERFORM CHECK-RETURN-CODE-FILESEQ
            //                   THRU CHECK-RETURN-CODE-FILESEQ-EX
            checkReturnCodeFileseq();
            // COB_CODE: IF SEQUENTIAL-ESITO-OK
            //              SET FILESQS5-CHIUSO    TO TRUE
            //           END-IF
            if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoOk()) {
                // COB_CODE: SET FILESQS5-CHIUSO    TO TRUE
                ws.getIabvsqs1().getStatoFilesqs5().setChiuso();
            }
        }
    }

    /**Original name: CLOSE-FILESQS6<br>
	 * <pre>*****************************************************************</pre>*/
    private void closeFilesqs6() {
        // COB_CODE: IF FILESQS6-APERTO
        //              END-IF
        //           END-IF.
        if (ws.getIabvsqs1().getStatoFilesqs6().isFilesqs6Aperto()) {
            // COB_CODE: MOVE NOME-FILESQS6        TO MSG-NOME-FILE
            ws.getIabcsq99().getMsgErrFile().setNomeFile(ws.getIabcsq99().getNomeFilesqs6());
            // COB_CODE: CLOSE FILESQS6
            filesqs6DAO.close();
            ws.getIabcsq99().setFsFilesqs6(filesqs6DAO.getFileStatus().getStatusCodeFormatted());
            // COB_CODE: MOVE FS-FILESQS6          TO FILE-STATUS
            ws.getIabcsq99().getFileStatus().setFileStatus(ws.getIabcsq99().getFsFilesqs6());
            // COB_CODE: PERFORM CHECK-RETURN-CODE-FILESEQ
            //                   THRU CHECK-RETURN-CODE-FILESEQ-EX
            checkReturnCodeFileseq();
            // COB_CODE: IF SEQUENTIAL-ESITO-OK
            //              SET FILESQS6-CHIUSO    TO TRUE
            //           END-IF
            if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoOk()) {
                // COB_CODE: SET FILESQS6-CHIUSO    TO TRUE
                ws.getIabvsqs1().getStatoFilesqs6().setFilesqs6Chiuso();
            }
        }
    }

    /**Original name: READ-FILESEQ<br>
	 * <pre>*****************************************************************</pre>*/
    private void readFileseq() {
        // COB_CODE: SET MSG-READ                      TO TRUE.
        ws.getIabcsq99().getMsgErrFile().getOperazioni().setRead();
        // COB_CODE: IF READ-FILESQS1-SI OR
        //              READ-FILESQS-ALL-SI
        //              PERFORM READ-FILESQS1          THRU READ-FILESQS1-EX
        //           END-IF
        if (ws.getIabvsqs1().getFlagReadFilesqs1().isSi() || ws.getIabvsqs1().getFlagReadFilesqsAll().isSi()) {
            // COB_CODE: PERFORM READ-FILESQS1          THRU READ-FILESQS1-EX
            readFilesqs1();
        }
        // COB_CODE: IF SEQUENTIAL-ESITO-OK
        //              END-IF
        //           END-IF.
        if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoOk()) {
            // COB_CODE: IF READ-FILESQS2-SI OR
            //              READ-FILESQS-ALL-SI
            //              PERFORM READ-FILESQS2       THRU READ-FILESQS2-EX
            //           END-IF
            if (ws.getIabvsqs1().getFlagReadFilesqs2().isSi() || ws.getIabvsqs1().getFlagReadFilesqsAll().isSi()) {
                // COB_CODE: PERFORM READ-FILESQS2       THRU READ-FILESQS2-EX
                readFilesqs2();
            }
            // COB_CODE: IF SEQUENTIAL-ESITO-OK
            //              END-IF
            //           END-IF
            if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoOk()) {
                // COB_CODE: IF READ-FILESQS3-SI OR
                //              READ-FILESQS-ALL-SI
                //              PERFORM READ-FILESQS3    THRU READ-FILESQS3-EX
                //           END-IF
                if (ws.getIabvsqs1().getFlagReadFilesqs3().isSi() || ws.getIabvsqs1().getFlagReadFilesqsAll().isSi()) {
                    // COB_CODE: PERFORM READ-FILESQS3    THRU READ-FILESQS3-EX
                    readFilesqs3();
                }
                // COB_CODE: IF SEQUENTIAL-ESITO-OK
                //              END-IF
                //           END-IF
                if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoOk()) {
                    // COB_CODE: IF READ-FILESQS4-SI OR
                    //              READ-FILESQS-ALL-SI
                    //              PERFORM READ-FILESQS4 THRU READ-FILESQS4-EX
                    //           END-IF
                    if (ws.getIabvsqs1().getFlagReadFilesqs4().isSi() || ws.getIabvsqs1().getFlagReadFilesqsAll().isSi()) {
                        // COB_CODE: PERFORM READ-FILESQS4 THRU READ-FILESQS4-EX
                        readFilesqs4();
                    }
                    // COB_CODE: IF SEQUENTIAL-ESITO-OK
                    //              END-IF
                    //           END-IF
                    if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoOk()) {
                        // COB_CODE: IF READ-FILESQS5-SI OR
                        //              READ-FILESQS-ALL-SI
                        //                                 THRU READ-FILESQS5-EX
                        //           END-IF
                        if (ws.getIabvsqs1().getFlagReadFilesqs5().isSi() || ws.getIabvsqs1().getFlagReadFilesqsAll().isSi()) {
                            // COB_CODE: PERFORM READ-FILESQS5
                            //                              THRU READ-FILESQS5-EX
                            readFilesqs5();
                        }
                        // COB_CODE: IF SEQUENTIAL-ESITO-OK
                        //              END-IF
                        //           END-IF
                        if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoOk()) {
                            // COB_CODE: IF READ-FILESQS6-SI OR
                            //              READ-FILESQS-ALL-SI
                            //                              THRU READ-FILESQS6-EX
                            //           END-IF
                            if (ws.getIabvsqs1().getFlagReadFilesqs6().isReadFilesqs6Si() || ws.getIabvsqs1().getFlagReadFilesqsAll().isSi()) {
                                // COB_CODE: PERFORM READ-FILESQS6
                                //                           THRU READ-FILESQS6-EX
                                readFilesqs6();
                            }
                        }
                    }
                }
            }
        }
    }

    /**Original name: READ-FILESQS1<br>
	 * <pre>*****************************************************************</pre>*/
    private void readFilesqs1() {
        // COB_CODE: MOVE NOME-FILESQS1        TO MSG-NOME-FILE.
        ws.getIabcsq99().getMsgErrFile().setNomeFile(ws.getIabcsq99().getNomeFilesqs1());
        // COB_CODE: READ FILESQS1.
        filesqs1To = filesqs1DAO.read(filesqs1To);
        ws.getIabcsq99().setFsFilesqs1(filesqs1DAO.getFileStatus().getStatusCodeFormatted());
        // COB_CODE: MOVE FS-FILESQS1          TO FILE-STATUS.
        ws.getIabcsq99().getFileStatus().setFileStatus(ws.getIabcsq99().getFsFilesqs1());
        // COB_CODE: PERFORM CHECK-RETURN-CODE-FILESEQ
        //                                     THRU CHECK-RETURN-CODE-FILESEQ-EX.
        checkReturnCodeFileseq();
        // COB_CODE: IF SEQUENTIAL-ESITO-OK
        //              END-IF
        //           END-IF.
        if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoOk()) {
            // COB_CODE: IF FILE-STATUS-END-OF-FILE
            //              END-IF
            //           END-IF
            if (ws.getIabcsq99().getFileStatus().isFileStatusEndOfFile()) {
                // COB_CODE: SET FILESQS1-EOF-SI              TO TRUE
                ws.getIabvsqs1().getEofFilesqs1().setSi();
                // COB_CODE: SET CLOSE-FILESQS1-SI            TO TRUE
                ws.getIabvsqs1().getFlagCloseFilesqs1().setSi();
                // COB_CODE: PERFORM CLOSE-FILESEQ            THRU CLOSE-FILESEQ-EX
                closeFileseq();
                // COB_CODE: IF SEQUENTIAL-ESITO-OK
                //              SET FILE-STATUS-END-OF-FILE   TO TRUE
                //           END-IF
                if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoOk()) {
                    // COB_CODE: SET FILE-STATUS-END-OF-FILE   TO TRUE
                    ws.getIabcsq99().getFileStatus().setFileStatusEndOfFile();
                }
            }
        }
    }

    /**Original name: READ-FILESQS2<br>
	 * <pre>*****************************************************************</pre>*/
    private void readFilesqs2() {
        // COB_CODE: MOVE NOME-FILESQS2        TO MSG-NOME-FILE.
        ws.getIabcsq99().getMsgErrFile().setNomeFile(ws.getIabcsq99().getNomeFilesqs2());
        // COB_CODE: READ FILESQS2.
        filesqs2To = filesqs2DAO.read(filesqs2To);
        ws.getIabcsq99().setFsFilesqs2(filesqs2DAO.getFileStatus().getStatusCodeFormatted());
        // COB_CODE: MOVE FS-FILESQS2          TO FILE-STATUS.
        ws.getIabcsq99().getFileStatus().setFileStatus(ws.getIabcsq99().getFsFilesqs2());
        // COB_CODE: PERFORM CHECK-RETURN-CODE-FILESEQ
        //                                     THRU CHECK-RETURN-CODE-FILESEQ-EX.
        checkReturnCodeFileseq();
        // COB_CODE: IF SEQUENTIAL-ESITO-OK
        //              END-IF
        //           END-IF.
        if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoOk()) {
            // COB_CODE: IF FILE-STATUS-END-OF-FILE
            //              END-IF
            //           END-IF
            if (ws.getIabcsq99().getFileStatus().isFileStatusEndOfFile()) {
                // COB_CODE: SET FILESQS2-EOF-SI              TO TRUE
                ws.getIabvsqs1().getEofFilesqs2().setSi();
                // COB_CODE: SET CLOSE-FILESQS2-SI            TO TRUE
                ws.getIabvsqs1().getFlagCloseFilesqs2().setSi();
                // COB_CODE: PERFORM CLOSE-FILESEQ            THRU CLOSE-FILESEQ-EX
                closeFileseq();
                // COB_CODE: IF SEQUENTIAL-ESITO-OK
                //              SET FILE-STATUS-END-OF-FILE   TO TRUE
                //           END-IF
                if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoOk()) {
                    // COB_CODE: SET FILE-STATUS-END-OF-FILE   TO TRUE
                    ws.getIabcsq99().getFileStatus().setFileStatusEndOfFile();
                }
            }
        }
    }

    /**Original name: READ-FILESQS3<br>
	 * <pre>*****************************************************************</pre>*/
    private void readFilesqs3() {
        // COB_CODE: MOVE NOME-FILESQS3        TO MSG-NOME-FILE.
        ws.getIabcsq99().getMsgErrFile().setNomeFile(ws.getIabcsq99().getNomeFilesqs3());
        // COB_CODE: READ FILESQS3.
        filesqs3To = filesqs3DAO.read(filesqs3To);
        ws.getIabcsq99().setFsFilesqs3(filesqs3DAO.getFileStatus().getStatusCodeFormatted());
        // COB_CODE: MOVE FS-FILESQS3          TO FILE-STATUS.
        ws.getIabcsq99().getFileStatus().setFileStatus(ws.getIabcsq99().getFsFilesqs3());
        // COB_CODE: PERFORM CHECK-RETURN-CODE-FILESEQ
        //                                     THRU CHECK-RETURN-CODE-FILESEQ-EX.
        checkReturnCodeFileseq();
        // COB_CODE: IF SEQUENTIAL-ESITO-OK
        //              END-IF
        //           END-IF.
        if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoOk()) {
            // COB_CODE: IF FILE-STATUS-END-OF-FILE
            //              END-IF
            //           END-IF
            if (ws.getIabcsq99().getFileStatus().isFileStatusEndOfFile()) {
                // COB_CODE: SET FILESQS3-EOF-SI              TO TRUE
                ws.getIabvsqs1().getEofFilesqs3().setSi();
                // COB_CODE: SET CLOSE-FILESQS3-SI            TO TRUE
                ws.getIabvsqs1().getFlagCloseFilesqs3().setSi();
                // COB_CODE: PERFORM CLOSE-FILESEQ            THRU CLOSE-FILESEQ-EX
                closeFileseq();
                // COB_CODE: IF SEQUENTIAL-ESITO-OK
                //              SET FILE-STATUS-END-OF-FILE   TO TRUE
                //           END-IF
                if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoOk()) {
                    // COB_CODE: SET FILE-STATUS-END-OF-FILE   TO TRUE
                    ws.getIabcsq99().getFileStatus().setFileStatusEndOfFile();
                }
            }
        }
    }

    /**Original name: READ-FILESQS4<br>
	 * <pre>*****************************************************************</pre>*/
    private void readFilesqs4() {
        // COB_CODE: MOVE NOME-FILESQS4        TO MSG-NOME-FILE.
        ws.getIabcsq99().getMsgErrFile().setNomeFile(ws.getIabcsq99().getNomeFilesqs4());
        // COB_CODE: READ FILESQS4.
        filesqs4To = filesqs4DAO.read(filesqs4To);
        ws.getIabcsq99().setFsFilesqs4(filesqs4DAO.getFileStatus().getStatusCodeFormatted());
        // COB_CODE: MOVE FS-FILESQS4          TO FILE-STATUS.
        ws.getIabcsq99().getFileStatus().setFileStatus(ws.getIabcsq99().getFsFilesqs4());
        // COB_CODE: PERFORM CHECK-RETURN-CODE-FILESEQ
        //                                     THRU CHECK-RETURN-CODE-FILESEQ-EX.
        checkReturnCodeFileseq();
        // COB_CODE: IF SEQUENTIAL-ESITO-OK
        //              END-IF
        //           END-IF.
        if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoOk()) {
            // COB_CODE: IF FILE-STATUS-END-OF-FILE
            //              END-IF
            //           END-IF
            if (ws.getIabcsq99().getFileStatus().isFileStatusEndOfFile()) {
                // COB_CODE: SET FILESQS4-EOF-SI              TO TRUE
                ws.getIabvsqs1().getEofFilesqs4().setSi();
                // COB_CODE: SET CLOSE-FILESQS4-SI            TO TRUE
                ws.getIabvsqs1().getFlagCloseFilesqs4().setSi();
                // COB_CODE: PERFORM CLOSE-FILESEQ            THRU CLOSE-FILESEQ-EX
                closeFileseq();
                // COB_CODE: IF SEQUENTIAL-ESITO-OK
                //              SET FILE-STATUS-END-OF-FILE   TO TRUE
                //           END-IF
                if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoOk()) {
                    // COB_CODE: SET FILE-STATUS-END-OF-FILE   TO TRUE
                    ws.getIabcsq99().getFileStatus().setFileStatusEndOfFile();
                }
            }
        }
    }

    /**Original name: READ-FILESQS5<br>
	 * <pre>*****************************************************************</pre>*/
    private void readFilesqs5() {
        // COB_CODE: MOVE NOME-FILESQS5        TO MSG-NOME-FILE.
        ws.getIabcsq99().getMsgErrFile().setNomeFile(ws.getIabcsq99().getNomeFilesqs5());
        // COB_CODE: READ FILESQS5.
        filesqs5To = filesqs5DAO.read(filesqs5To);
        ws.getIabcsq99().setFsFilesqs5(filesqs5DAO.getFileStatus().getStatusCodeFormatted());
        // COB_CODE: MOVE FS-FILESQS5          TO FILE-STATUS.
        ws.getIabcsq99().getFileStatus().setFileStatus(ws.getIabcsq99().getFsFilesqs5());
        // COB_CODE: PERFORM CHECK-RETURN-CODE-FILESEQ
        //                                     THRU CHECK-RETURN-CODE-FILESEQ-EX.
        checkReturnCodeFileseq();
        // COB_CODE: IF SEQUENTIAL-ESITO-OK
        //              END-IF
        //           END-IF.
        if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoOk()) {
            // COB_CODE: IF FILE-STATUS-END-OF-FILE
            //              END-IF
            //           END-IF
            if (ws.getIabcsq99().getFileStatus().isFileStatusEndOfFile()) {
                // COB_CODE: SET FILESQS5-EOF-SI              TO TRUE
                ws.getIabvsqs1().getEofFilesqs5().setSi();
                // COB_CODE: SET CLOSE-FILESQS5-SI            TO TRUE
                ws.getIabvsqs1().getFlagCloseFilesqs5().setSi();
                // COB_CODE: PERFORM CLOSE-FILESEQ            THRU CLOSE-FILESEQ-EX
                closeFileseq();
                // COB_CODE: IF SEQUENTIAL-ESITO-OK
                //              SET FILE-STATUS-END-OF-FILE   TO TRUE
                //           END-IF
                if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoOk()) {
                    // COB_CODE: SET FILE-STATUS-END-OF-FILE   TO TRUE
                    ws.getIabcsq99().getFileStatus().setFileStatusEndOfFile();
                }
            }
        }
    }

    /**Original name: READ-FILESQS6<br>
	 * <pre>*****************************************************************</pre>*/
    private void readFilesqs6() {
        // COB_CODE: MOVE NOME-FILESQS6        TO MSG-NOME-FILE.
        ws.getIabcsq99().getMsgErrFile().setNomeFile(ws.getIabcsq99().getNomeFilesqs6());
        // COB_CODE: READ FILESQS6.
        filesqs6To = filesqs6DAO.read(filesqs6To);
        ws.getIabcsq99().setFsFilesqs6(filesqs6DAO.getFileStatus().getStatusCodeFormatted());
        // COB_CODE: MOVE FS-FILESQS6          TO FILE-STATUS.
        ws.getIabcsq99().getFileStatus().setFileStatus(ws.getIabcsq99().getFsFilesqs6());
        // COB_CODE: PERFORM CHECK-RETURN-CODE-FILESEQ
        //                                     THRU CHECK-RETURN-CODE-FILESEQ-EX.
        checkReturnCodeFileseq();
        // COB_CODE: IF SEQUENTIAL-ESITO-OK
        //              END-IF
        //           END-IF.
        if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoOk()) {
            // COB_CODE: IF FILE-STATUS-END-OF-FILE
            //              END-IF
            //           END-IF
            if (ws.getIabcsq99().getFileStatus().isFileStatusEndOfFile()) {
                // COB_CODE: SET FILESQS6-EOF-SI              TO TRUE
                ws.getIabvsqs1().getEofFilesqs6().setFilesqs6EofSi();
                // COB_CODE: SET CLOSE-FILESQS6-SI            TO TRUE
                ws.getIabvsqs1().getFlagCloseFilesqs6().setCloseFilesqs6Si();
                // COB_CODE: PERFORM CLOSE-FILESEQ            THRU CLOSE-FILESEQ-EX
                closeFileseq();
                // COB_CODE: IF SEQUENTIAL-ESITO-OK
                //              SET FILE-STATUS-END-OF-FILE   TO TRUE
                //           END-IF
                if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoOk()) {
                    // COB_CODE: SET FILE-STATUS-END-OF-FILE   TO TRUE
                    ws.getIabcsq99().getFileStatus().setFileStatusEndOfFile();
                }
            }
        }
    }

    /**Original name: WRITE-FILESEQ<br>
	 * <pre>*****************************************************************</pre>*/
    private void writeFileseq() {
        // COB_CODE: SET MSG-WRITE                      TO TRUE.
        ws.getIabcsq99().getMsgErrFile().getOperazioni().setMsgWrite();
        // COB_CODE: IF WRITE-FILESQS1-SI OR
        //              WRITE-FILESQS-ALL-SI
        //              PERFORM WRITE-FILESQS1          THRU WRITE-FILESQS1-EX
        //           END-IF
        if (ws.getIabvsqs1().getFlagWriteFilesqs1().isSi() || ws.getIabvsqs1().getFlagWriteFilesqsAll().isSi()) {
            // COB_CODE: PERFORM WRITE-FILESQS1          THRU WRITE-FILESQS1-EX
            writeFilesqs1();
        }
        // COB_CODE: IF SEQUENTIAL-ESITO-OK
        //              END-IF
        //           END-IF.
        if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoOk()) {
            // COB_CODE: IF WRITE-FILESQS2-SI OR
            //              WRITE-FILESQS-ALL-SI
            //              PERFORM WRITE-FILESQS2       THRU WRITE-FILESQS2-EX
            //           END-IF
            if (ws.getIabvsqs1().getFlagWriteFilesqs2().isSi() || ws.getIabvsqs1().getFlagWriteFilesqsAll().isSi()) {
                // COB_CODE: PERFORM WRITE-FILESQS2       THRU WRITE-FILESQS2-EX
                writeFilesqs2();
            }
            // COB_CODE: IF SEQUENTIAL-ESITO-OK
            //              END-IF
            //           END-IF
            if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoOk()) {
                // COB_CODE: IF WRITE-FILESQS3-SI OR
                //              WRITE-FILESQS-ALL-SI
                //              PERFORM WRITE-FILESQS3    THRU WRITE-FILESQS3-EX
                //           END-IF
                if (ws.getIabvsqs1().getFlagWriteFilesqs3().isSi() || ws.getIabvsqs1().getFlagWriteFilesqsAll().isSi()) {
                    // COB_CODE: PERFORM WRITE-FILESQS3    THRU WRITE-FILESQS3-EX
                    writeFilesqs3();
                }
                // COB_CODE: IF SEQUENTIAL-ESITO-OK
                //              END-IF
                //           END-IF
                if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoOk()) {
                    // COB_CODE: IF WRITE-FILESQS4-SI OR
                    //              WRITE-FILESQS-ALL-SI
                    //              PERFORM WRITE-FILESQS4 THRU WRITE-FILESQS4-EX
                    //           END-IF
                    if (ws.getIabvsqs1().getFlagWriteFilesqs4().isSi() || ws.getIabvsqs1().getFlagWriteFilesqsAll().isSi()) {
                        // COB_CODE: PERFORM WRITE-FILESQS4 THRU WRITE-FILESQS4-EX
                        writeFilesqs4();
                    }
                    // COB_CODE: IF SEQUENTIAL-ESITO-OK
                    //              END-IF
                    //           END-IF
                    if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoOk()) {
                        // COB_CODE: IF WRITE-FILESQS5-SI OR
                        //              WRITE-FILESQS-ALL-SI
                        //                                  THRU WRITE-FILESQS5-EX
                        //           END-IF
                        if (ws.getIabvsqs1().getFlagWriteFilesqs5().isSi() || ws.getIabvsqs1().getFlagWriteFilesqsAll().isSi()) {
                            // COB_CODE: PERFORM WRITE-FILESQS5
                            //                               THRU WRITE-FILESQS5-EX
                            writeFilesqs5();
                        }
                        // COB_CODE: IF SEQUENTIAL-ESITO-OK
                        //              END-IF
                        //           END-IF
                        if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoOk()) {
                            // COB_CODE: IF WRITE-FILESQS6-SI OR
                            //              WRITE-FILESQS-ALL-SI
                            //                               THRU WRITE-FILESQS6-EX
                            //           END-IF
                            if (ws.getIabvsqs1().getFlagWriteFilesqs6().isWriteFilesqs6Si() || ws.getIabvsqs1().getFlagWriteFilesqsAll().isSi()) {
                                // COB_CODE: PERFORM WRITE-FILESQS6
                                //                            THRU WRITE-FILESQS6-EX
                                writeFilesqs6();
                            }
                        }
                    }
                }
            }
        }
    }

    /**Original name: WRITE-FILESQS1<br>
	 * <pre>*****************************************************************</pre>*/
    private void writeFilesqs1() {
        // COB_CODE: MOVE NOME-FILESQS1        TO MSG-NOME-FILE.
        ws.getIabcsq99().getMsgErrFile().setNomeFile(ws.getIabcsq99().getNomeFilesqs1());
        // COB_CODE: WRITE FILESQS1-REC.
        filesqs1DAO.write(filesqs1To);
        ws.getIabcsq99().setFsFilesqs1(filesqs1DAO.getFileStatus().getStatusCodeFormatted());
        // COB_CODE: MOVE FS-FILESQS1          TO FILE-STATUS.
        ws.getIabcsq99().getFileStatus().setFileStatus(ws.getIabcsq99().getFsFilesqs1());
        // COB_CODE: PERFORM CHECK-RETURN-CODE-FILESEQ
        //                                     THRU CHECK-RETURN-CODE-FILESEQ-EX.
        checkReturnCodeFileseq();
    }

    /**Original name: WRITE-FILESQS2<br>
	 * <pre>*****************************************************************</pre>*/
    private void writeFilesqs2() {
        // COB_CODE: MOVE NOME-FILESQS2        TO MSG-NOME-FILE.
        ws.getIabcsq99().getMsgErrFile().setNomeFile(ws.getIabcsq99().getNomeFilesqs2());
        // COB_CODE: WRITE FILESQS2-REC.
        filesqs2DAO.write(filesqs2To);
        ws.getIabcsq99().setFsFilesqs2(filesqs2DAO.getFileStatus().getStatusCodeFormatted());
        // COB_CODE: MOVE FS-FILESQS2          TO FILE-STATUS.
        ws.getIabcsq99().getFileStatus().setFileStatus(ws.getIabcsq99().getFsFilesqs2());
        // COB_CODE: PERFORM CHECK-RETURN-CODE-FILESEQ
        //                                     THRU CHECK-RETURN-CODE-FILESEQ-EX.
        checkReturnCodeFileseq();
    }

    /**Original name: WRITE-FILESQS3<br>
	 * <pre>*****************************************************************</pre>*/
    private void writeFilesqs3() {
        // COB_CODE: MOVE NOME-FILESQS3        TO MSG-NOME-FILE.
        ws.getIabcsq99().getMsgErrFile().setNomeFile(ws.getIabcsq99().getNomeFilesqs3());
        // COB_CODE: WRITE FILESQS3-REC.
        filesqs3DAO.write(filesqs3To);
        ws.getIabcsq99().setFsFilesqs3(filesqs3DAO.getFileStatus().getStatusCodeFormatted());
        // COB_CODE: MOVE FS-FILESQS3          TO FILE-STATUS.
        ws.getIabcsq99().getFileStatus().setFileStatus(ws.getIabcsq99().getFsFilesqs3());
        // COB_CODE: PERFORM CHECK-RETURN-CODE-FILESEQ
        //                                     THRU CHECK-RETURN-CODE-FILESEQ-EX.
        checkReturnCodeFileseq();
    }

    /**Original name: WRITE-FILESQS4<br>
	 * <pre>*****************************************************************</pre>*/
    private void writeFilesqs4() {
        // COB_CODE: MOVE NOME-FILESQS4        TO MSG-NOME-FILE.
        ws.getIabcsq99().getMsgErrFile().setNomeFile(ws.getIabcsq99().getNomeFilesqs4());
        // COB_CODE: WRITE FILESQS4-REC.
        filesqs4DAO.write(filesqs4To);
        ws.getIabcsq99().setFsFilesqs4(filesqs4DAO.getFileStatus().getStatusCodeFormatted());
        // COB_CODE: MOVE FS-FILESQS4          TO FILE-STATUS.
        ws.getIabcsq99().getFileStatus().setFileStatus(ws.getIabcsq99().getFsFilesqs4());
        // COB_CODE: PERFORM CHECK-RETURN-CODE-FILESEQ
        //                                     THRU CHECK-RETURN-CODE-FILESEQ-EX.
        checkReturnCodeFileseq();
    }

    /**Original name: WRITE-FILESQS5<br>
	 * <pre>*****************************************************************</pre>*/
    private void writeFilesqs5() {
        // COB_CODE: MOVE NOME-FILESQS5        TO MSG-NOME-FILE.
        ws.getIabcsq99().getMsgErrFile().setNomeFile(ws.getIabcsq99().getNomeFilesqs5());
        // COB_CODE: WRITE FILESQS5-REC.
        filesqs5DAO.write(filesqs5To);
        ws.getIabcsq99().setFsFilesqs5(filesqs5DAO.getFileStatus().getStatusCodeFormatted());
        // COB_CODE: MOVE FS-FILESQS5          TO FILE-STATUS.
        ws.getIabcsq99().getFileStatus().setFileStatus(ws.getIabcsq99().getFsFilesqs5());
        // COB_CODE: PERFORM CHECK-RETURN-CODE-FILESEQ
        //                                     THRU CHECK-RETURN-CODE-FILESEQ-EX.
        checkReturnCodeFileseq();
    }

    /**Original name: WRITE-FILESQS6<br>
	 * <pre>*****************************************************************</pre>*/
    private void writeFilesqs6() {
        // COB_CODE: MOVE NOME-FILESQS6        TO MSG-NOME-FILE.
        ws.getIabcsq99().getMsgErrFile().setNomeFile(ws.getIabcsq99().getNomeFilesqs6());
        // COB_CODE: WRITE FILESQS6-REC.
        filesqs6DAO.write(filesqs6To);
        ws.getIabcsq99().setFsFilesqs6(filesqs6DAO.getFileStatus().getStatusCodeFormatted());
        // COB_CODE: MOVE FS-FILESQS6          TO FILE-STATUS.
        ws.getIabcsq99().getFileStatus().setFileStatus(ws.getIabcsq99().getFsFilesqs6());
        // COB_CODE: PERFORM CHECK-RETURN-CODE-FILESEQ
        //                                     THRU CHECK-RETURN-CODE-FILESEQ-EX.
        checkReturnCodeFileseq();
    }

    /**Original name: REWRITE-FILESEQ<br>
	 * <pre>*****************************************************************</pre>*/
    private void rewriteFileseq() {
        // COB_CODE: SET MSG-REWRITE                   TO TRUE.
        ws.getIabcsq99().getMsgErrFile().getOperazioni().setMsgRewrite();
        // COB_CODE: IF REWRITE-FILESQS1-SI OR
        //              REWRITE-FILESQS-ALL-SI
        //              PERFORM REWRITE-FILESQS1       THRU REWRITE-FILESQS1-EX
        //           END-IF
        if (ws.getIabvsqs1().getFlagRewriteFilesqs1().isSi() || ws.getIabvsqs1().getFlagRewriteFilesqsAll().isSi()) {
            // COB_CODE: PERFORM REWRITE-FILESQS1       THRU REWRITE-FILESQS1-EX
            rewriteFilesqs1();
        }
        // COB_CODE: IF SEQUENTIAL-ESITO-OK
        //              END-IF
        //           END-IF.
        if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoOk()) {
            // COB_CODE: IF REWRITE-FILESQS2-SI OR
            //              REWRITE-FILESQS-ALL-SI
            //              PERFORM REWRITE-FILESQS2    THRU REWRITE-FILESQS2-EX
            //           END-IF
            if (ws.getIabvsqs1().getFlagRewriteFilesqs2().isSi() || ws.getIabvsqs1().getFlagRewriteFilesqsAll().isSi()) {
                // COB_CODE: PERFORM REWRITE-FILESQS2    THRU REWRITE-FILESQS2-EX
                rewriteFilesqs2();
            }
            // COB_CODE: IF SEQUENTIAL-ESITO-OK
            //              END-IF
            //           END-IF
            if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoOk()) {
                // COB_CODE: IF REWRITE-FILESQS3-SI OR
                //              REWRITE-FILESQS-ALL-SI
                //              PERFORM REWRITE-FILESQS3 THRU REWRITE-FILESQS3-EX
                //           END-IF
                if (ws.getIabvsqs1().getFlagRewriteFilesqs3().isSi() || ws.getIabvsqs1().getFlagRewriteFilesqsAll().isSi()) {
                    // COB_CODE: PERFORM REWRITE-FILESQS3 THRU REWRITE-FILESQS3-EX
                    rewriteFilesqs3();
                }
                // COB_CODE: IF SEQUENTIAL-ESITO-OK
                //              END-IF
                //           END-IF
                if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoOk()) {
                    // COB_CODE: IF REWRITE-FILESQS4-SI OR
                    //              REWRITE-FILESQS-ALL-SI
                    //                                    THRU REWRITE-FILESQS4-EX
                    //           END-IF
                    if (ws.getIabvsqs1().getFlagRewriteFilesqs4().isSi() || ws.getIabvsqs1().getFlagRewriteFilesqsAll().isSi()) {
                        // COB_CODE: PERFORM REWRITE-FILESQS4
                        //                                 THRU REWRITE-FILESQS4-EX
                        rewriteFilesqs4();
                    }
                    // COB_CODE: IF SEQUENTIAL-ESITO-OK
                    //              END-IF
                    //           END-IF
                    if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoOk()) {
                        // COB_CODE: IF REWRITE-FILESQS5-SI OR
                        //              REWRITE-FILESQS-ALL-SI
                        //                                 THRU REWRITE-FILESQS5-EX
                        //           END-IF
                        if (ws.getIabvsqs1().getFlagRewriteFilesqs5().isSi() || ws.getIabvsqs1().getFlagRewriteFilesqsAll().isSi()) {
                            // COB_CODE: PERFORM REWRITE-FILESQS5
                            //                              THRU REWRITE-FILESQS5-EX
                            rewriteFilesqs5();
                        }
                        // COB_CODE: IF SEQUENTIAL-ESITO-OK
                        //              END-IF
                        //           END-IF
                        if (ws.getIabcsq99().getSequentialEsito().isSequentialEsitoOk()) {
                            // COB_CODE: IF REWRITE-FILESQS6-SI OR
                            //              REWRITE-FILESQS-ALL-SI
                            //                              THRU REWRITE-FILESQS6-EX
                            //           END-IF
                            if (ws.getIabvsqs1().getFlagRewriteFilesqs6().isRewriteFilesqs6Si() || ws.getIabvsqs1().getFlagRewriteFilesqsAll().isSi()) {
                                // COB_CODE: PERFORM REWRITE-FILESQS6
                                //                           THRU REWRITE-FILESQS6-EX
                                rewriteFilesqs6();
                            }
                        }
                    }
                }
            }
        }
    }

    /**Original name: REWRITE-FILESQS1<br>
	 * <pre>*****************************************************************</pre>*/
    private void rewriteFilesqs1() {
        // COB_CODE: MOVE NOME-FILESQS1        TO MSG-NOME-FILE.
        ws.getIabcsq99().getMsgErrFile().setNomeFile(ws.getIabcsq99().getNomeFilesqs1());
        // COB_CODE: REWRITE FILESQS1-REC.
        filesqs1DAO.rewrite(filesqs1To);
        ws.getIabcsq99().setFsFilesqs1(filesqs1DAO.getFileStatus().getStatusCodeFormatted());
        // COB_CODE: MOVE FS-FILESQS1          TO FILE-STATUS.
        ws.getIabcsq99().getFileStatus().setFileStatus(ws.getIabcsq99().getFsFilesqs1());
        // COB_CODE: PERFORM CHECK-RETURN-CODE-FILESEQ
        //                                     THRU CHECK-RETURN-CODE-FILESEQ-EX.
        checkReturnCodeFileseq();
    }

    /**Original name: REWRITE-FILESQS2<br>
	 * <pre>*****************************************************************</pre>*/
    private void rewriteFilesqs2() {
        // COB_CODE: MOVE NOME-FILESQS2        TO MSG-NOME-FILE.
        ws.getIabcsq99().getMsgErrFile().setNomeFile(ws.getIabcsq99().getNomeFilesqs2());
        // COB_CODE: REWRITE FILESQS2-REC.
        filesqs2DAO.rewrite(filesqs2To);
        ws.getIabcsq99().setFsFilesqs2(filesqs2DAO.getFileStatus().getStatusCodeFormatted());
        // COB_CODE: MOVE FS-FILESQS2          TO FILE-STATUS.
        ws.getIabcsq99().getFileStatus().setFileStatus(ws.getIabcsq99().getFsFilesqs2());
        // COB_CODE: PERFORM CHECK-RETURN-CODE-FILESEQ
        //                                     THRU CHECK-RETURN-CODE-FILESEQ-EX.
        checkReturnCodeFileseq();
    }

    /**Original name: REWRITE-FILESQS3<br>
	 * <pre>*****************************************************************</pre>*/
    private void rewriteFilesqs3() {
        // COB_CODE: MOVE NOME-FILESQS3        TO MSG-NOME-FILE.
        ws.getIabcsq99().getMsgErrFile().setNomeFile(ws.getIabcsq99().getNomeFilesqs3());
        // COB_CODE: REWRITE FILESQS3-REC.
        filesqs3DAO.rewrite(filesqs3To);
        ws.getIabcsq99().setFsFilesqs3(filesqs3DAO.getFileStatus().getStatusCodeFormatted());
        // COB_CODE: MOVE FS-FILESQS3          TO FILE-STATUS.
        ws.getIabcsq99().getFileStatus().setFileStatus(ws.getIabcsq99().getFsFilesqs3());
        // COB_CODE: PERFORM CHECK-RETURN-CODE-FILESEQ
        //                                     THRU CHECK-RETURN-CODE-FILESEQ-EX.
        checkReturnCodeFileseq();
    }

    /**Original name: REWRITE-FILESQS4<br>
	 * <pre>*****************************************************************</pre>*/
    private void rewriteFilesqs4() {
        // COB_CODE: MOVE NOME-FILESQS4        TO MSG-NOME-FILE.
        ws.getIabcsq99().getMsgErrFile().setNomeFile(ws.getIabcsq99().getNomeFilesqs4());
        // COB_CODE: REWRITE FILESQS4-REC.
        filesqs4DAO.rewrite(filesqs4To);
        ws.getIabcsq99().setFsFilesqs4(filesqs4DAO.getFileStatus().getStatusCodeFormatted());
        // COB_CODE: MOVE FS-FILESQS4          TO FILE-STATUS.
        ws.getIabcsq99().getFileStatus().setFileStatus(ws.getIabcsq99().getFsFilesqs4());
        // COB_CODE: PERFORM CHECK-RETURN-CODE-FILESEQ
        //                                     THRU CHECK-RETURN-CODE-FILESEQ-EX.
        checkReturnCodeFileseq();
    }

    /**Original name: REWRITE-FILESQS5<br>
	 * <pre>*****************************************************************</pre>*/
    private void rewriteFilesqs5() {
        // COB_CODE: MOVE NOME-FILESQS5        TO MSG-NOME-FILE.
        ws.getIabcsq99().getMsgErrFile().setNomeFile(ws.getIabcsq99().getNomeFilesqs5());
        // COB_CODE: REWRITE FILESQS5-REC.
        filesqs5DAO.rewrite(filesqs5To);
        ws.getIabcsq99().setFsFilesqs5(filesqs5DAO.getFileStatus().getStatusCodeFormatted());
        // COB_CODE: MOVE FS-FILESQS5          TO FILE-STATUS.
        ws.getIabcsq99().getFileStatus().setFileStatus(ws.getIabcsq99().getFsFilesqs5());
        // COB_CODE: PERFORM CHECK-RETURN-CODE-FILESEQ
        //                                     THRU CHECK-RETURN-CODE-FILESEQ-EX.
        checkReturnCodeFileseq();
    }

    /**Original name: REWRITE-FILESQS6<br>
	 * <pre>*****************************************************************</pre>*/
    private void rewriteFilesqs6() {
        // COB_CODE: MOVE NOME-FILESQS6        TO MSG-NOME-FILE.
        ws.getIabcsq99().getMsgErrFile().setNomeFile(ws.getIabcsq99().getNomeFilesqs6());
        // COB_CODE: REWRITE FILESQS6-REC.
        filesqs6DAO.rewrite(filesqs6To);
        ws.getIabcsq99().setFsFilesqs6(filesqs6DAO.getFileStatus().getStatusCodeFormatted());
        // COB_CODE: MOVE FS-FILESQS6          TO FILE-STATUS.
        ws.getIabcsq99().getFileStatus().setFileStatus(ws.getIabcsq99().getFsFilesqs6());
        // COB_CODE: PERFORM CHECK-RETURN-CODE-FILESEQ
        //                                     THRU CHECK-RETURN-CODE-FILESEQ-EX.
        checkReturnCodeFileseq();
    }

    /**Original name: FINE-FILESEQ<br>
	 * <pre>*****************************************************************</pre>*/
    private void fineFileseq() {
        // COB_CODE: SET READ-FILESQS1-NO       TO TRUE.
        ws.getIabvsqs1().getFlagReadFilesqs1().setNo();
        // COB_CODE: SET READ-FILESQS2-NO       TO TRUE.
        ws.getIabvsqs1().getFlagReadFilesqs2().setNo();
        // COB_CODE: SET READ-FILESQS3-NO       TO TRUE.
        ws.getIabvsqs1().getFlagReadFilesqs3().setNo();
        // COB_CODE: SET READ-FILESQS4-NO       TO TRUE.
        ws.getIabvsqs1().getFlagReadFilesqs4().setNo();
        // COB_CODE: SET READ-FILESQS5-NO       TO TRUE.
        ws.getIabvsqs1().getFlagReadFilesqs5().setNo();
        // COB_CODE: SET READ-FILESQS6-NO       TO TRUE.
        ws.getIabvsqs1().getFlagReadFilesqs6().setReadFilesqs6No();
        // COB_CODE: SET READ-FILESQS-ALL-NO    TO TRUE.
        ws.getIabvsqs1().getFlagReadFilesqsAll().setNo();
        // COB_CODE: SET WRITE-FILESQS1-NO      TO TRUE.
        ws.getIabvsqs1().getFlagWriteFilesqs1().setNo();
        // COB_CODE: SET WRITE-FILESQS2-NO      TO TRUE.
        ws.getIabvsqs1().getFlagWriteFilesqs2().setNo();
        // COB_CODE: SET WRITE-FILESQS3-NO      TO TRUE.
        ws.getIabvsqs1().getFlagWriteFilesqs3().setNo();
        // COB_CODE: SET WRITE-FILESQS4-NO      TO TRUE.
        ws.getIabvsqs1().getFlagWriteFilesqs4().setNo();
        // COB_CODE: SET WRITE-FILESQS5-NO      TO TRUE.
        ws.getIabvsqs1().getFlagWriteFilesqs5().setNo();
        // COB_CODE: SET WRITE-FILESQS6-NO      TO TRUE.
        ws.getIabvsqs1().getFlagWriteFilesqs6().setWriteFilesqs6No();
        // COB_CODE: SET WRITE-FILESQS-ALL-NO   TO TRUE.
        ws.getIabvsqs1().getFlagWriteFilesqsAll().setNo();
        // COB_CODE: SET REWRITE-FILESQS1-NO    TO TRUE.
        ws.getIabvsqs1().getFlagRewriteFilesqs1().setNo();
        // COB_CODE: SET REWRITE-FILESQS2-NO    TO TRUE.
        ws.getIabvsqs1().getFlagRewriteFilesqs2().setNo();
        // COB_CODE: SET REWRITE-FILESQS3-NO    TO TRUE.
        ws.getIabvsqs1().getFlagRewriteFilesqs3().setNo();
        // COB_CODE: SET REWRITE-FILESQS4-NO    TO TRUE.
        ws.getIabvsqs1().getFlagRewriteFilesqs4().setNo();
        // COB_CODE: SET REWRITE-FILESQS5-NO    TO TRUE.
        ws.getIabvsqs1().getFlagRewriteFilesqs5().setNo();
        // COB_CODE: SET REWRITE-FILESQS6-NO    TO TRUE.
        ws.getIabvsqs1().getFlagRewriteFilesqs6().setRewriteFilesqs6No();
        // COB_CODE: SET REWRITE-FILESQS-ALL-NO TO TRUE.
        ws.getIabvsqs1().getFlagRewriteFilesqsAll().setNo();
    }

    /**Original name: S0290-ERRORE-DI-SISTEMA<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES GESTIONE ERRORE
	 * ----------------------------------------------------------------*
	 * **-------------------------------------------------------------**
	 *     PROGRAMMA ..... IERP9902
	 *     TIPOLOGIA...... COPY
	 *     DESCRIZIONE.... ROUTINE GENERALIZZATA GESTIONE ERRORI CALL
	 * **-------------------------------------------------------------**</pre>*/
    private void s0290ErroreDiSistema() {
        // COB_CODE: MOVE '005006'           TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErroreFormatted("005006");
        // COB_CODE: MOVE CALL-DESC          TO IEAI9901-PARAMETRI-ERR.
        ws.getIeai9901Area().setParametriErr(ws.getIdsv0002().getCallDesc());
        // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
        //              THRU EX-S0300.
        s0300RicercaGravitaErrore();
    }

    /**Original name: S0300-RICERCA-GRAVITA-ERRORE<br>
	 * <pre>MUOVO L'AREA DEL SERVIZIO NELL'AREA DATI DELL'AREA DI CONTESTO.
	 * ----->VALORIZZO I CAMPI DELLA IDSV0001</pre>*/
    private void s0300RicercaGravitaErrore() {
        Ieas9900 ieas9900 = null;
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR       TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE  'IEAS9900'              TO CALL-PGM
        ws.getIdsv0002().setCallPgm("IEAS9900");
        // COB_CODE: CALL CALL-PGM USING IDSV0001-AREA-CONTESTO
        //                               IEAI9901-AREA
        //                               IEAO9901-AREA
        //             ON EXCEPTION
        //                   THRU EX-S0310-ERRORE-FATALE
        //           END-CALL.
        try {
            ieas9900 = Ieas9900.getInstance();
            ieas9900.run(areaIdsv0001, ws.getIeai9901Area(), ws.getIeao9901Area());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
            areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
            // COB_CODE: PERFORM S0310-ERRORE-FATALE
            //              THRU EX-S0310-ERRORE-FATALE
            s0310ErroreFatale();
        }
        //SE LA CHIAMATA AL SERVIZIO HA ESITO POSITIVO (NESSUN ERRORE DI
        //SISTEMA, VALORIZZO L'AREA DI OUTPUT.
        //INCREMENTO L'INDICE DEGLI ERRORI
        // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
        areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
        //SE L'INDICE E' MINORE DI 10 ...
        // COB_CODE:      IF IDSV0001-MAX-ELE-ERRORI NOT GREATER 10
        //           *SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
        //                   END-IF
        //                ELSE
        //           *IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        //                       TO IDSV0001-DESC-ERRORE-ESTESA
        //                END-IF.
        if (areaIdsv0001.getMaxEleErrori() <= 10) {
            //SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
            // COB_CODE: IF IEAO9901-COD-ERRORE-990 = ZEROES
            //                      EX-S0300-IMPOSTA-ERRORE
            //           ELSE
            //                   IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
            //           END-IF
            if (Characters.EQ_ZERO.test(ws.getIeao9901Area().getCodErrore990Formatted())) {
                // COB_CODE: PERFORM S0300-IMPOSTA-ERRORE THRU
                //                   EX-S0300-IMPOSTA-ERRORE
                s0300ImpostaErrore();
            }
            else {
                // COB_CODE: MOVE 'IEAS9900'
                //             TO IDSV0001-COD-SERVIZIO-BE
                areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
                // COB_CODE: MOVE IEAO9901-LABEL-ERR-990
                //             TO IDSV0001-LABEL-ERR
                areaIdsv0001.getLogErrore().setLabelErr(ws.getIeao9901Area().getLabelErr990());
                // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
                //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
                // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
                areaIdsv0001.getEsito().setIdsv0001EsitoKo();
                // IMPOSTO IL CODICE ERRORE GESTITO ALL'INTERNO DEL PROGRAMMA
                // COB_CODE: MOVE IEAO9901-COD-ERRORE-990
                //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeao9901Area().getCodErrore990Formatted());
                // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
                //             TO IDSV0001-DESC-ERRORE-ESTESA
                //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
            }
        }
        else {
            //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
            // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
            //             TO IDSV0001-LABEL-ERR
            areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
            // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 'IEAS9900'
            //             TO IDSV0001-COD-SERVIZIO-BE
            areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
            // COB_CODE: MOVE 'OVERFLOW IMPAGINAZIONE ERRORI'
            //             TO IDSV0001-DESC-ERRORE-ESTESA
            areaIdsv0001.getLogErrore().setDescErroreEstesa("OVERFLOW IMPAGINAZIONE ERRORI");
        }
    }

    /**Original name: S0300-IMPOSTA-ERRORE<br>
	 * <pre>  se la gravit— di tutti gli errori dell'elaborazione
	 *   h minore di zero ( ad esempio -3) vuol dire che il return-code
	 *   dell'elaborazione complessiva deve essere abbassato da '08' a
	 *   '04'. Per fare cir utilizzo il flag IDSV0001-FORZ-RC-04
	 * IMPOSTO LA GRAVITA'</pre>*/
    private void s0300ImpostaErrore() {
        // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
        // COB_CODE: EVALUATE TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = -3
        //                       IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        //             WHEN  IEAO9901-LIV-GRAVITA = 0 OR 1 OR 2
        //                   SET IDSV0001-FORZ-RC-04-NO  TO TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = 3 OR 4
        //                   SET IDSV0001-ESITO-KO       TO TRUE
        //           END-EVALUATE
        if (ws.getIeao9901Area().getLivGravita() == -3) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-YES TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04Yes();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 2  TO
            //               IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
            areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)2));
        }
        else if (ws.getIeao9901Area().getLivGravita() == 0 || ws.getIeao9901Area().getLivGravita() == 1 || ws.getIeao9901Area().getLivGravita() == 2) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
        }
        else if (ws.getIeao9901Area().getLivGravita() == 3 || ws.getIeao9901Area().getLivGravita() == 4) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        }
        // IMPOSTO IL CODICE ERRORE
        // COB_CODE: MOVE IEAI9901-COD-ERRORE
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeai9901Area().getCodErroreFormatted());
        // SE IL LIVELLO DI GRAVITA' RESTITUITO DALLO IAS9900 E' MAGGIORE
        // DI 2 (ERRORE BLOCCANTE)...IMPOSTO L'ESITO E I DATI DI CONTESTO
        //RELATIVI ALL'ERRORE BLOCCANTE
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE
        //             TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR
        //             TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
        //             TO IDSV0001-DESC-ERRORE-ESTESA
        //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
        // COB_CODE: MOVE SPACES          TO IEAI9901-COD-SERVIZIO-BE
        //                                  IEAI9901-LABEL-ERR
        //                                   IEAI9901-PARAMETRI-ERR.
        ws.getIeai9901Area().setCodServizioBe("");
        ws.getIeai9901Area().setLabelErr("");
        ws.getIeai9901Area().setParametriErr("");
        // COB_CODE: MOVE ZEROES          TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErrore(0);
    }

    /**Original name: S0310-ERRORE-FATALE<br>
	 * <pre>IMPOSTO LA GRAVITA' ERRORE</pre>*/
    private void s0310ErroreFatale() {
        // COB_CODE: MOVE  3
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)3));
        // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
        areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        //IMPOSTO IL CODICE ERRORE (ERRORE FISSO)
        // COB_CODE: MOVE 900001
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErrore(900001);
        //IMPOSTO NOME DEL MODULO
        // COB_CODE: MOVE 'IEAS9900'               TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
        //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
        //              TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
        // COB_CODE: MOVE  'ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900'
        //              TO IDSV0001-DESC-ERRORE-ESTESA
        //                 IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
    }

    /**Original name: ESEGUI-DISPLAY<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI GESTIONE DISPLAY
	 * ----------------------------------------------------------------*</pre>*/
    private void eseguiDisplay() {
        Idss8880 idss8880 = null;
        GenericParam idsv8888DisplayAddress = null;
        // COB_CODE: IF IDSV0001-ANY-TUNING-DBG AND
        //              IDSV8888-ANY-TUNING-DBG
        //              END-IF
        //           ELSE
        //              END-IF
        //           END-IF.
        if (areaIdsv0001.getAreaComune().getLivelloDebug().isAnyTuningDbg() && ws.getIdsv8888().getLivelloDebug().isAnyTuningDbg()) {
            // COB_CODE: IF IDSV0001-TOT-TUNING-DBG
            //              INITIALIZE IDSV8888-STR-PERFORMANCE-DBG
            //           ELSE
            //              END-IF
            //           END-IF
            if (areaIdsv0001.getAreaComune().getLivelloDebug().isTotTuningDbg()) {
                // COB_CODE: IF IDSV8888-STRESS-TEST-DBG
                //              CONTINUE
                //           ELSE
                //              END-IF
                //           END-IF
                if (ws.getIdsv8888().getLivelloDebug().isStressTestDbg()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else if (ws.getIdsv8888().getLivelloDebug().isComCobJavDbg()) {
                    // COB_CODE: IF IDSV8888-COM-COB-JAV-DBG
                    //              SET IDSV8888-COM-COB-JAV   TO TRUE
                    //           ELSE
                    //              END-IF
                    //           END-IF
                    // COB_CODE: SET IDSV8888-COM-COB-JAV   TO TRUE
                    ws.getIdsv8888().getStrPerformanceDbg().setComCobJav();
                }
                else if (ws.getIdsv8888().getLivelloDebug().isBusinessDbg()) {
                    // COB_CODE: IF IDSV8888-BUSINESS-DBG
                    //              SET IDSV8888-BUSINESS   TO TRUE
                    //           ELSE
                    //              END-IF
                    //           END-IF
                    // COB_CODE: SET IDSV8888-BUSINESS   TO TRUE
                    ws.getIdsv8888().getStrPerformanceDbg().setBusiness();
                }
                else if (ws.getIdsv8888().getLivelloDebug().isArchBatchDbg()) {
                    // COB_CODE: IF IDSV8888-ARCH-BATCH-DBG
                    //              SET IDSV8888-ARCH-BATCH TO TRUE
                    //           ELSE
                    //              MOVE 'ERRO'   TO IDSV8888-FASE
                    //           END-IF
                    // COB_CODE: SET IDSV8888-ARCH-BATCH TO TRUE
                    ws.getIdsv8888().getStrPerformanceDbg().setArchBatch();
                }
                else {
                    // COB_CODE: MOVE 'ERRO'   TO IDSV8888-FASE
                    ws.getIdsv8888().getStrPerformanceDbg().setFase("ERRO");
                }
                // COB_CODE: CALL IDSV8888-CALL-TIMESTAMP
                //                USING IDSV8888-TIMESTAMP
                DynamicCall.invoke(ws.getIdsv8888().getCallTimestamp(), new BasicBytesClass(ws.getIdsv8888().getStrPerformanceDbg().getArray(), Idsv8888StrPerformanceDbg.Pos.TIMESTAMP_FLD - 1));
                // COB_CODE: SET IDSV8888-DISPLAY-ADDRESS
                //                TO ADDRESS OF IDSV8888-STR-PERFORMANCE-DBG
                ws.getIdsv8888().setDisplayAddress(pointerManager.addressOf(ws.getIdsv8888().getStrPerformanceDbg()));
                // COB_CODE: CALL IDSV8888-CALL-DISPLAY
                //                USING IDSV8888-DISPLAY-ADDRESS
                idss8880 = Idss8880.getInstance();
                idsv8888DisplayAddress = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getIdsv8888().getDisplayAddress()));
                idss8880.run(idsv8888DisplayAddress);
                ws.getIdsv8888().setDisplayAddressFromBuffer(idsv8888DisplayAddress.getByteData());
                // COB_CODE: INITIALIZE IDSV8888-STR-PERFORMANCE-DBG
                initStrPerformanceDbg();
            }
            else if (ws.getIdsv8888().getLivelloDebug().getLivelloDebug() == areaIdsv0001.getAreaComune().getLivelloDebug().getIdsi0011LivelloDebug()) {
                // COB_CODE: IF IDSV8888-LIVELLO-DEBUG =
                //              IDSV0001-LIVELLO-DEBUG
                //              INITIALIZE IDSV8888-STR-PERFORMANCE-DBG
                //           END-IF
                // COB_CODE: IF IDSV8888-STRESS-TEST-DBG
                //              CONTINUE
                //           ELSE
                //              END-IF
                //           END-IF
                if (ws.getIdsv8888().getLivelloDebug().isStressTestDbg()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else if (ws.getIdsv8888().getLivelloDebug().isComCobJavDbg()) {
                    // COB_CODE: IF IDSV8888-COM-COB-JAV-DBG
                    //              SET IDSV8888-COM-COB-JAV   TO TRUE
                    //           ELSE
                    //              END-IF
                    //           END-IF
                    // COB_CODE: SET IDSV8888-COM-COB-JAV   TO TRUE
                    ws.getIdsv8888().getStrPerformanceDbg().setComCobJav();
                }
                else if (ws.getIdsv8888().getLivelloDebug().isBusinessDbg()) {
                    // COB_CODE: IF IDSV8888-BUSINESS-DBG
                    //              SET IDSV8888-BUSINESS   TO TRUE
                    //           ELSE
                    //              SET IDSV8888-ARCH-BATCH TO TRUE
                    //           END-IF
                    // COB_CODE: SET IDSV8888-BUSINESS   TO TRUE
                    ws.getIdsv8888().getStrPerformanceDbg().setBusiness();
                }
                else {
                    // COB_CODE: SET IDSV8888-ARCH-BATCH TO TRUE
                    ws.getIdsv8888().getStrPerformanceDbg().setArchBatch();
                }
                // COB_CODE: CALL IDSV8888-CALL-TIMESTAMP
                //                USING IDSV8888-TIMESTAMP
                DynamicCall.invoke(ws.getIdsv8888().getCallTimestamp(), new BasicBytesClass(ws.getIdsv8888().getStrPerformanceDbg().getArray(), Idsv8888StrPerformanceDbg.Pos.TIMESTAMP_FLD - 1));
                // COB_CODE: SET IDSV8888-DISPLAY-ADDRESS
                //               TO ADDRESS OF IDSV8888-STR-PERFORMANCE-DBG
                ws.getIdsv8888().setDisplayAddress(pointerManager.addressOf(ws.getIdsv8888().getStrPerformanceDbg()));
                // COB_CODE: CALL IDSV8888-CALL-DISPLAY
                //                USING IDSV8888-DISPLAY-ADDRESS
                idss8880 = Idss8880.getInstance();
                idsv8888DisplayAddress = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getIdsv8888().getDisplayAddress()));
                idss8880.run(idsv8888DisplayAddress);
                ws.getIdsv8888().setDisplayAddressFromBuffer(idsv8888DisplayAddress.getByteData());
                // COB_CODE: INITIALIZE IDSV8888-STR-PERFORMANCE-DBG
                initStrPerformanceDbg();
            }
        }
        else if (areaIdsv0001.getAreaComune().getLivelloDebug().isAnyApplDbg() && ws.getIdsv8888().getLivelloDebug().isAnyApplDbg()) {
            // COB_CODE: IF IDSV0001-ANY-APPL-DBG AND
            //              IDSV8888-ANY-APPL-DBG
            //               END-IF
            //           END-IF
            // COB_CODE: IF IDSV8888-LIVELLO-DEBUG <=
            //              IDSV0001-LIVELLO-DEBUG
            //              MOVE SPACES TO IDSV8888-AREA-DISPLAY
            //           END-IF
            if (ws.getIdsv8888().getLivelloDebug().getLivelloDebug() <= areaIdsv0001.getAreaComune().getLivelloDebug().getIdsi0011LivelloDebug()) {
                // COB_CODE: SET IDSV8888-DISPLAY-ADDRESS
                //               TO ADDRESS OF IDSV8888-AREA-DISPLAY
                ws.getIdsv8888().setDisplayAddress(pointerManager.addressOf(ws.getIdsv8888().getAreaDisplay()));
                // COB_CODE: CALL IDSV8888-CALL-DISPLAY
                //                USING IDSV8888-DISPLAY-ADDRESS
                idss8880 = Idss8880.getInstance();
                idsv8888DisplayAddress = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getIdsv8888().getDisplayAddress()));
                idss8880.run(idsv8888DisplayAddress);
                ws.getIdsv8888().setDisplayAddressFromBuffer(idsv8888DisplayAddress.getByteData());
                // COB_CODE: MOVE SPACES TO IDSV8888-AREA-DISPLAY
                ws.getIdsv8888().getAreaDisplay().setAreaDisplay("");
            }
        }
        // COB_CODE: SET IDSV8888-NO-DEBUG              TO TRUE.
        ws.getIdsv8888().getLivelloDebug().setNoDebug();
    }

    /**Original name: CALL-DISPATCHER<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES CALL DISPATCHER
	 * ----------------------------------------------------------------*
	 * *****************************************************************
	 *    CALL DISPATCHER
	 * *****************************************************************</pre>*/
    private void callDispatcher() {
        Idss0010 idss0010 = null;
        // COB_CODE: MOVE IDSV0001-MODALITA-ESECUTIVA
        //                              TO IDSI0011-MODALITA-ESECUTIVA
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011ModalitaEsecutiva().setIdsi0011ModalitaEsecutiva(areaIdsv0001.getAreaComune().getModalitaEsecutiva().getModalitaEsecutiva());
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //                              TO IDSI0011-CODICE-COMPAGNIA-ANIA
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceCompagniaAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: MOVE IDSV0001-COD-MAIN-BATCH
        //                              TO IDSI0011-COD-MAIN-BATCH
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodMainBatch(areaIdsv0001.getAreaComune().getCodMainBatch());
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO
        //                              TO IDSI0011-TIPO-MOVIMENTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011TipoMovimentoFormatted(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimentoFormatted());
        // COB_CODE: MOVE IDSV0001-SESSIONE
        //                              TO IDSI0011-SESSIONE
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011Sessione(areaIdsv0001.getAreaComune().getSessione());
        // COB_CODE: MOVE IDSV0001-USER-NAME
        //                              TO IDSI0011-USER-NAME
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011UserName(areaIdsv0001.getAreaComune().getUserName());
        // COB_CODE: IF IDSI0011-DATA-INIZIO-EFFETTO = 0
        //              END-IF
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataInizioEffetto() == 0) {
            // COB_CODE: IF IDSV0001-LETT-ULT-IMMAGINE-SI
            //              END-IF
            //           ELSE
            //                TO IDSI0011-DATA-INIZIO-EFFETTO
            //           END-IF
            if (areaIdsv0001.getAreaComune().getLettUltImmagine().isIdsv0001LettUltImmagineSi()) {
                // COB_CODE: IF IDSI0011-SELECT
                //           OR IDSI0011-FETCH-FIRST
                //           OR IDSI0011-FETCH-NEXT
                //                TO IDSI0011-DATA-COMPETENZA
                //           ELSE
                //                TO IDSI0011-DATA-INIZIO-EFFETTO
                //           END-IF
                if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isSelect() || ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchFirst() || ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchNext()) {
                    // COB_CODE: MOVE 99991230
                    //             TO IDSI0011-DATA-INIZIO-EFFETTO
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(99991230);
                    // COB_CODE: MOVE 999912304023595999
                    //             TO IDSI0011-DATA-COMPETENZA
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(999912304023595999L);
                }
                else {
                    // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
                    //             TO IDSI0011-DATA-INIZIO-EFFETTO
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
                }
            }
            else {
                // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
                //             TO IDSI0011-DATA-INIZIO-EFFETTO
                ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
            }
        }
        // COB_CODE: IF IDSI0011-DATA-FINE-EFFETTO = 0
        //              MOVE 99991231   TO IDSI0011-DATA-FINE-EFFETTO
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataFineEffetto() == 0) {
            // COB_CODE: MOVE 99991231   TO IDSI0011-DATA-FINE-EFFETTO
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(99991231);
        }
        // COB_CODE: IF IDSI0011-DATA-COMPETENZA = 0
        //                              TO IDSI0011-DATA-COMPETENZA
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataCompetenza() == 0) {
            // COB_CODE: MOVE IDSV0001-DATA-COMPETENZA
            //                           TO IDSI0011-DATA-COMPETENZA
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(areaIdsv0001.getAreaComune().getIdsv0001DataCompetenza());
        }
        // COB_CODE: MOVE IDSV0001-DATA-COMP-AGG-STOR
        //                              TO IDSI0011-DATA-COMP-AGG-STOR
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompAggStor(areaIdsv0001.getAreaComune().getIdsv0001DataCompAggStor());
        // COB_CODE: IF IDSI0011-TRATT-DEFAULT
        //                              TO IDSI0011-TRATTAMENTO-STORICITA
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().isTrattDefault()) {
            // COB_CODE: MOVE IDSV0001-TRATTAMENTO-STORICITA
            //                           TO IDSI0011-TRATTAMENTO-STORICITA
            ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattamentoStoricita(areaIdsv0001.getAreaComune().getTrattamentoStoricita().getTrattamentoStoricita());
        }
        // COB_CODE: MOVE IDSV0001-FORMATO-DATA-DB
        //                              TO IDSI0011-FORMATO-DATA-DB
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011FormatoDataDb().setIdsi0011FormatoDataDb(areaIdsv0001.getAreaComune().getFormatoDataDb().getFormatoDataDb());
        // COB_CODE: MOVE IDSV0001-LIVELLO-DEBUG
        //                              TO IDSI0011-LIVELLO-DEBUG
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloDebug().setIdsi0011LivelloDebugFormatted(areaIdsv0001.getAreaComune().getLivelloDebug().getIdsi0011LivelloDebugFormatted());
        // COB_CODE: MOVE 0             TO IDSI0011-ID-MOVI-ANNULLATO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011IdMoviAnnullato(0);
        // COB_CODE: SET IDSI0011-PTF-NEWLIFE          TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011IdentitaChiamante().setPtfNewlife();
        // COB_CODE: SET IDSV0012-STATUS-SHARED-MEM-KO TO TRUE
        ws.getIdsv00122().getStatusSharedMemory().setKo();
        // COB_CODE: MOVE 'IDSS0010'             TO IDSI0011-PGM
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011Pgm("IDSS0010");
        //     SET WS-ADDRESS-DIS TO ADDRESS OF IN-OUT-IDSS0010.
        // COB_CODE: CALL IDSI0011-PGM           USING IDSI0011-AREA
        //                                             IDSO0011-AREA.
        idss0010 = Idss0010.getInstance();
        idss0010.run(ws.getDispatcherVariables(), ws.getDispatcherVariables().getIdso0011Area());
        // COB_CODE: MOVE IDSO0011-SQLCODE-SIGNED
        //                                  TO IDSO0011-SQLCODE.
        ws.getDispatcherVariables().getIdso0011Area().setSqlcode(ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned());
    }

    /**Original name: C0001-CNTRL-1<br>
	 * <pre>----------------------------------------------------------------*
	 *     COPY PROCEDURE
	 * ----------------------------------------------------------------*
	 * --> CONTROLLI COMUNI A TUTTI GLI E/C
	 * ----------------------------------------------------------------*
	 *     ESTRATTO CONTO - FASE DIAGNOSTICO
	 *        - CONTROLLI COMUNI A TUTTI GLI E/C
	 * ----------------------------------------------------------------*
	 * ----------------------------------------------------------------*
	 *    CONTROLLO 1                                                  *
	 * ----------------------------------------------------------------*</pre>*/
    private void c0001Cntrl1() {
        // COB_CODE: MOVE 'C0001-CNTRL-1'          TO WK-LABEL.
        ws.getWsVariabili().setWkLabel("C0001-CNTRL-1");
        //
        // COB_CODE: IF ELE-MAX-CACHE-3 > 0
        //              CONTINUE
        //           ELSE
        //              PERFORM E001-SCARTO THRU EX-E001
        //           END-IF.
        if (ws.getEleMaxCache3() > 0) {
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE 1          TO WK-CONTROLLO
            ws.getWsVariabili().setWkControllo(1);
            // COB_CODE: SET CNTRL-1     TO TRUE
            ws.getLrgc0661().getMsgErrore().setCntrl1();
            // COB_CODE: MOVE MSG-ERRORE TO WS-ERRORE
            ws.getWsVariabili().setWsErrore(ws.getLrgc0661().getMsgErrore().getMsgErrore());
            // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
            e001Scarto();
        }
    }

    /**Original name: C0002-CNTRL-2<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLO 2                                                  *
	 * ----------------------------------------------------------------*</pre>*/
    private void c0002Cntrl2() {
        // COB_CODE: MOVE 'C0002-CNTRL-2'          TO WK-LABEL.
        ws.getWsVariabili().setWkLabel("C0002-CNTRL-2");
        //--> LA DT_RIF_A DEVE ESSERE COMPRESA NEL
        //--> PERIODO DI PRENOTAZIONE
        // COB_CODE:      IF  WRIN-DT-RIF-A      >= WK-DT-RICOR-DA
        //                AND WRIN-DT-RIF-A      <= WK-DT-RICOR-A
        //           *--> LA DT_RIF_DA DEVE ESSERE LA MAX TRA
        //           *--> (DT_DECOR E DT_RIF_A - 1 ANNO)
        //                    END-IF
        //                ELSE
        //                   PERFORM E001-SCARTO THRU EX-E001
        //                END-IF.
        if (Conditions.gte(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinDtRifA(), ws.getDateWork().getWkDtRicorDaX().getWkDtRicorDaFormatted()) && Conditions.lte(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinDtRifA(), ws.getDateWork().getWkDtRicorAX().getWkDtRicorAFormatted())) {
            //--> LA DT_RIF_DA DEVE ESSERE LA MAX TRA
            //--> (DT_DECOR E DT_RIF_A - 1 ANNO)
            // COB_CODE: MOVE WRIN-DT-RIF-A TO WK-DATA-IN
            ws.getWsVariabili().setWkDataInFormatted(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinDtRifAFormatted());
            //
            // COB_CODE: PERFORM A700-CALCOLO-DATA
            //              THRU A700-EX
            a700CalcoloData();
            //
            // COB_CODE: IF  WRIN-DT-RIF-DA = WK-DATA-OUT
            //           OR  WRIN-DT-RIF-DA = WRIN-DT-DECOR-POLI
            //               CONTINUE
            //           ELSE
            //               END-IF
            //           END-IF
            if (Conditions.eq(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinDtRifDa(), ws.getWsVariabili().getWkDataOutFormatted()) || Conditions.eq(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinDtRifDa(), ws.getWrinRecOutput().getWrinTpRecordDati().getWrinDtDecorPoli())) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (Conditions.eq(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinDtRifAFormatted().substring((1) - 1, 4), ws.getWrinRecOutput().getWrinTpRecordDati().getWrinDtRifDaFormatted().substring((1) - 1, 4)) && Conditions.eq(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinCodProdAct(), "PTCASH")) {
            // COB_CODE: IF WRIN-DT-RIF-A(1:4) =  WRIN-DT-RIF-DA(1:4)
            //           AND WRIN-COD-PROD-ACT = 'PTCASH'
            //               CONTINUE
            //           ELSE
            //               PERFORM E001-SCARTO THRU EX-E001
            //           END-IF
            // COB_CODE: CONTINUE
            //continue
            }
            else {
                // COB_CODE: MOVE 2                  TO WK-CONTROLLO
                ws.getWsVariabili().setWkControllo(2);
                // COB_CODE: MOVE WRIN-DT-RIF-DA     TO WS-DATA-1
                ws.getTracciatoFile().setWsData1(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinDtRifDa());
                // COB_CODE: MOVE WRIN-DT-RIF-A      TO WS-DATA-2
                ws.getTracciatoFile().setWsData2(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinDtRifA());
                // COB_CODE: MOVE WRIN-DT-DECOR-POLI TO WS-DATA-3
                ws.getTracciatoFile().setWsData3(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinDtDecorPoli());
                // COB_CODE: SET CNTRL-2         TO TRUE
                ws.getLrgc0661().getMsgErrore().setCntrl2();
                // COB_CODE: MOVE MSG-ERRORE     TO WS-ERRORE
                ws.getWsVariabili().setWsErrore(ws.getLrgc0661().getMsgErrore().getMsgErrore());
                // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
                e001Scarto();
            }
        }
        else {
            // COB_CODE: MOVE 2                  TO WK-CONTROLLO
            ws.getWsVariabili().setWkControllo(2);
            // COB_CODE: MOVE WRIN-DT-RIF-DA     TO WS-DATA-1
            ws.getTracciatoFile().setWsData1(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinDtRifDa());
            // COB_CODE: MOVE WRIN-DT-RIF-A      TO WS-DATA-2
            ws.getTracciatoFile().setWsData2(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinDtRifA());
            // COB_CODE: MOVE WRIN-DT-DECOR-POLI TO WS-DATA-3
            ws.getTracciatoFile().setWsData3(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinDtDecorPoli());
            // COB_CODE: MOVE 2          TO WK-CONTROLLO
            ws.getWsVariabili().setWkControllo(2);
            // COB_CODE: SET CNTRL-2         TO TRUE
            ws.getLrgc0661().getMsgErrore().setCntrl2();
            // COB_CODE: MOVE MSG-ERRORE     TO WS-ERRORE
            ws.getWsVariabili().setWsErrore(ws.getLrgc0661().getMsgErrore().getMsgErrore());
            // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
            e001Scarto();
        }
    }

    /**Original name: C0003-CNTRL-6-7-14<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLO 6 7 14                                             *
	 * ----------------------------------------------------------------*</pre>*/
    private void c0003Cntrl6714() {
        // COB_CODE: MOVE 'C0003-CNTRL-6-7-14'      TO WK-LABEL.
        ws.getWsVariabili().setWkLabel("C0003-CNTRL-6-7-14");
        // COB_CODE: INITIALIZE WORK-CALCOLI
        initWorkCalcoli();
        // COB_CODE: COMPUTE WS-DIFF = WRIN-TOT-CUM-PREMI-VERS
        //                           - WRIN-TOT-CUM-PRE-INVST-AA-RIF
        ws.getWorkCalcoli().setWsDiff(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinTotCumPremiVers().subtract(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinTotCumPreInvstAaRif()), 15, 3));
        // COB_CODE: IF WRIN-TOT-CUM-PREMI-VERS > 0
        //              COMPUTE WS-PERC = WS-DIV * 100
        //           END-IF
        if (ws.getWrinRecOutput().getWrinTpRecordDati().getWrinTotCumPremiVers().compareTo(0) > 0) {
            // COB_CODE: COMPUTE WS-DIV = WS-DIFF / WRIN-TOT-CUM-PREMI-VERS
            ws.getWorkCalcoli().setWsDiv(Trunc.toDecimal(new AfDecimal((ws.getWorkCalcoli().getWsDiff().divide(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinTotCumPremiVers())), 18, 3), 15, 3));
            // COB_CODE: COMPUTE WS-PERC = WS-DIV * 100
            ws.getWorkCalcoli().setWsPerc(Trunc.toDecimal(ws.getWorkCalcoli().getWsDiv().multiply(100), 15, 3));
        }
        // COB_CODE: IF TP-ESTR-MU
        //           OR TP-ESTR-UL
        //              END-IF
        //           ELSE
        //              END-IF
        //           END-IF
        if (ws.getTipologiaEc().isMu() || ws.getTipologiaEc().isUl()) {
            // COB_CODE: IF WS-PERC > 6
            //              PERFORM E001-SCARTO THRU EX-E001
            //           END-IF
            if (ws.getWorkCalcoli().getWsPerc().compareTo(6) > 0) {
                // COB_CODE: MOVE 6          TO WK-CONTROLLO
                ws.getWsVariabili().setWkControllo(6);
                // COB_CODE: MOVE WRIN-TOT-CUM-PREMI-VERS       TO WS-CAMPO-1
                ws.getTracciatoFile().setWsCampo1(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinTotCumPremiVers());
                // COB_CODE: MOVE WRIN-TOT-CUM-PRE-INVST-AA-RIF TO WS-CAMPO-2
                ws.getTracciatoFile().setWsCampo2(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinTotCumPreInvstAaRif());
                // COB_CODE: MOVE WS-PERC                       TO WS-CAMPO-3
                ws.getTracciatoFile().setWsCampo3(ws.getWorkCalcoli().getWsPerc());
                // COB_CODE: SET CNTRL-6     TO TRUE
                ws.getLrgc0661().getMsgErrore().setCntrl6();
                // COB_CODE: MOVE MSG-ERRORE TO WS-ERRORE
                ws.getWsVariabili().setWsErrore(ws.getLrgc0661().getMsgErrore().getMsgErrore());
                // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
                e001Scarto();
            }
        }
        else if (ws.getTipologiaEc().isRv()) {
            // COB_CODE: IF TP-ESTR-RV
            //              END-IF
            //           END-IF
            // COB_CODE: MOVE WK-DT-DECOR(1:4) TO WK-DT-DECOR-AA
            ws.getWsVariabili().setWkDtDecorAaFormatted(ws.getWsVariabili().getWkDtDecorFormatted().substring((1) - 1, 4));
            // COB_CODE: IF WK-DT-DECOR-AA = WK-ANNO-RIF
            //           AND ((WK-FRAZ = 12) OR (WK-FRAZ = 4))
            //              END-IF
            //           ELSE
            //              END-IF
            //           END-IF
            if (ws.getWsVariabili().getWkDtDecorAa() == ws.getDateWork().getWkAnnoRifX().getWkAnnoRif() && (ws.getWsVariabili().getWkFraz() == 12 || ws.getWsVariabili().getWkFraz() == 4)) {
                // COB_CODE: IF WS-PERC > 50
                //              PERFORM E001-SCARTO THRU EX-E001
                //           END-IF
                if (ws.getWorkCalcoli().getWsPerc().compareTo(50) > 0) {
                    // COB_CODE: MOVE 6          TO WK-CONTROLLO
                    ws.getWsVariabili().setWkControllo(6);
                    // COB_CODE: MOVE WRIN-TOT-CUM-PREMI-VERS       TO WS-CAMPO-1
                    ws.getTracciatoFile().setWsCampo1(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinTotCumPremiVers());
                    // COB_CODE: MOVE WRIN-TOT-CUM-PRE-INVST-AA-RIF TO WS-CAMPO-2
                    ws.getTracciatoFile().setWsCampo2(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinTotCumPreInvstAaRif());
                    // COB_CODE: MOVE WS-PERC                       TO WS-CAMPO-3
                    ws.getTracciatoFile().setWsCampo3(ws.getWorkCalcoli().getWsPerc());
                    // COB_CODE: MOVE WK-DT-DECOR                   TO WS-DATA-1
                    ws.getTracciatoFile().setWsData1(ws.getWsVariabili().getWkDtDecor());
                    // COB_CODE: SET CNTRL-6     TO TRUE
                    ws.getLrgc0661().getMsgErrore().setCntrl6();
                    // COB_CODE: MOVE MSG-ERRORE TO WS-ERRORE
                    ws.getWsVariabili().setWsErrore(ws.getLrgc0661().getMsgErrore().getMsgErrore());
                    // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
                    e001Scarto();
                }
            }
            else if (ws.getWorkCalcoli().getWsPerc().compareTo(25) > 0) {
                // COB_CODE: IF WS-PERC > 25
                //              PERFORM E001-SCARTO THRU EX-E001
                //           END-IF
                // COB_CODE: MOVE 6          TO WK-CONTROLLO
                ws.getWsVariabili().setWkControllo(6);
                // COB_CODE: MOVE WRIN-TOT-CUM-PREMI-VERS       TO WS-CAMPO-1
                ws.getTracciatoFile().setWsCampo1(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinTotCumPremiVers());
                // COB_CODE: MOVE WRIN-TOT-CUM-PRE-INVST-AA-RIF TO WS-CAMPO-2
                ws.getTracciatoFile().setWsCampo2(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinTotCumPreInvstAaRif());
                // COB_CODE: MOVE WS-PERC                       TO WS-CAMPO-3
                ws.getTracciatoFile().setWsCampo3(ws.getWorkCalcoli().getWsPerc());
                // COB_CODE: MOVE WK-DT-DECOR                   TO WS-DATA-1
                ws.getTracciatoFile().setWsData1(ws.getWsVariabili().getWkDtDecor());
                // COB_CODE: SET CNTRL-6     TO TRUE
                ws.getLrgc0661().getMsgErrore().setCntrl6();
                // COB_CODE: MOVE MSG-ERRORE TO WS-ERRORE
                ws.getWsVariabili().setWsErrore(ws.getLrgc0661().getMsgErrore().getMsgErrore());
                // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
                e001Scarto();
            }
        }
        // COB_CODE: IF WRIN-TOT-CUM-PREMI-VERS = 0
        //              END-IF
        //           END-IF
        if (ws.getWrinRecOutput().getWrinTpRecordDati().getWrinTotCumPremiVers().compareTo(0) == 0) {
            // COB_CODE: IF WRIN-TOT-CUM-PRE-INVST-AA-RIF = 0
            //              CONTINUE
            //           ELSE
            //              PERFORM E001-SCARTO THRU EX-E001
            //           END-IF
            if (ws.getWrinRecOutput().getWrinTpRecordDati().getWrinTotCumPreInvstAaRif().compareTo(0) == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else {
                // COB_CODE: MOVE 7          TO WK-CONTROLLO
                ws.getWsVariabili().setWkControllo(7);
                // COB_CODE: MOVE WRIN-TOT-CUM-PREMI-VERS       TO WS-CAMPO-1
                ws.getTracciatoFile().setWsCampo1(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinTotCumPremiVers());
                // COB_CODE: MOVE WRIN-TOT-CUM-PRE-INVST-AA-RIF TO WS-CAMPO-2
                ws.getTracciatoFile().setWsCampo2(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinTotCumPreInvstAaRif());
                // COB_CODE: SET CNTRL-7     TO TRUE
                ws.getLrgc0661().getMsgErrore().setCntrl7();
                // COB_CODE: MOVE MSG-ERRORE TO WS-ERRORE
                ws.getWsVariabili().setWsErrore(ws.getLrgc0661().getMsgErrore().getMsgErrore());
                // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
                e001Scarto();
            }
        }
        // COB_CODE: MOVE 0 TO WS-SOMMA
        ws.getWorkCalcoli().setWsSomma(Trunc.toDecimal(0, 15, 3));
        // COB_CODE: PERFORM VARYING IX-REC-3 FROM 1 BY 1
        //             UNTIL IX-REC-3 > ELE-MAX-CACHE-3
        //                   + REC3-CUM-PREM-VERS-AA-RIF(IX-REC-3)
        //           END-PERFORM.
        ws.getIxIndici().setRec3(((short)1));
        while (!(ws.getIxIndici().getRec3() > ws.getEleMaxCache3())) {
            // COB_CODE: COMPUTE WS-SOMMA = WS-SOMMA
            //               + REC3-CUM-PREM-VERS-AA-RIF(IX-REC-3)
            ws.getWorkCalcoli().setWsSomma(Trunc.toDecimal(ws.getWorkCalcoli().getWsSomma().add(ws.getEleCache3(ws.getIxIndici().getRec3()).getCumPremVersAaRif()), 15, 3));
            ws.getIxIndici().setRec3(Trunc.toShort(ws.getIxIndici().getRec3() + 1, 4));
        }
        // COB_CODE: IF  WRIN-TOT-CUM-PREMI-VERS
        //               = WS-SOMMA
        //              CONTINUE
        //           ELSE
        //              PERFORM E001-SCARTO THRU EX-E001
        //           END-IF.
        if (ws.getWrinRecOutput().getWrinTpRecordDati().getWrinTotCumPremiVers().compareTo(ws.getWorkCalcoli().getWsSomma()) == 0) {
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE 14                      TO WK-CONTROLLO
            ws.getWsVariabili().setWkControllo(14);
            // COB_CODE: MOVE WRIN-TOT-CUM-PREMI-VERS TO WS-CAMPO-1
            ws.getTracciatoFile().setWsCampo1(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinTotCumPremiVers());
            // COB_CODE: MOVE WS-SOMMA                TO WS-CAMPO-2
            ws.getTracciatoFile().setWsCampo2(ws.getWorkCalcoli().getWsSomma());
            // COB_CODE: SET CNTRL-14     TO TRUE
            ws.getLrgc0661().getMsgErrore().setCntrl14();
            // COB_CODE: MOVE MSG-ERRORE TO WS-ERRORE
            ws.getWsVariabili().setWsErrore(ws.getLrgc0661().getMsgErrore().getMsgErrore());
            // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
            e001Scarto();
        }
    }

    /**Original name: C0004-CNTRL-8-9-18<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLO 8 9 18                                             *
	 * ----------------------------------------------------------------*</pre>*/
    private void c0004Cntrl8918() {
        // COB_CODE: MOVE 'C0004-CNTRL-8-9-18'      TO WK-LABEL.
        ws.getWsVariabili().setWkLabel("C0004-CNTRL-8-9-18");
        // COB_CODE: IF WRIN-DT-DECOR-POLI < WRIN-DT-RIF-DA
        //              END-IF
        //           END-IF.
        if (Conditions.lt(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinDtDecorPoli(), ws.getWrinRecOutput().getWrinTpRecordDati().getWrinDtRifDa())) {
            // COB_CODE: IF WRIN-TOT-CUM-PREMI-VERS-AP > 0
            //              CONTINUE
            //           ELSE
            //              PERFORM E001-SCARTO THRU EX-E001
            //           END-IF
            if (ws.getWrinRecOutput().getWrinTpRecordDati().getWrinTotCumPremiVersAp().compareTo(0) > 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else {
                // COB_CODE: MOVE 8          TO WK-CONTROLLO
                ws.getWsVariabili().setWkControllo(8);
                // COB_CODE: MOVE WRIN-TOT-CUM-PREMI-VERS-AP
                //             TO WS-CAMPO-1
                ws.getTracciatoFile().setWsCampo1(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinTotCumPremiVersAp());
                // COB_CODE: MOVE WRIN-DT-DECOR-POLI TO WS-DATA-1
                ws.getTracciatoFile().setWsData1(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinDtDecorPoli());
                // COB_CODE: MOVE WRIN-DT-RIF-DA     TO WS-DATA-2
                ws.getTracciatoFile().setWsData2(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinDtRifDa());
                // COB_CODE: SET CNTRL-8     TO TRUE
                ws.getLrgc0661().getMsgErrore().setCntrl8();
                // COB_CODE: MOVE MSG-ERRORE TO WS-ERRORE
                ws.getWsVariabili().setWsErrore(ws.getLrgc0661().getMsgErrore().getMsgErrore());
                // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
                e001Scarto();
            }
        }
        // COB_CODE: IF WRIN-DT-RIF-DA = WRIN-DT-RIF-A
        //             END-IF
        //           END-IF.
        if (Conditions.eq(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinDtRifDa(), ws.getWrinRecOutput().getWrinTpRecordDati().getWrinDtRifA())) {
            // COB_CODE: IF  WRIN-TOT-CUM-PREMI-VERS-AP IS NUMERIC
            //               CONTINUE
            //           ELSE
            //               MOVE 0 TO WRIN-TOT-CUM-PREMI-VERS-AP
            //           END-IF
            if (Functions.isNumber(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinTotCumPremiVersAp())) {
            // COB_CODE: CONTINUE
            //continue
            }
            else {
                // COB_CODE: MOVE 0 TO WRIN-TOT-CUM-PREMI-VERS-AP
                ws.getWrinRecOutput().getWrinTpRecordDati().setWrinTotCumPremiVersAp(Trunc.toDecimal(0, 15, 3));
            }
            // COB_CODE:  IF  WRIN-TOT-CUM-PREMI-VERS-AP = 0
            //            AND WRIN-PRESTAZIONE-AP-POL = 0
            //            AND WRIN-IMP-LRD-RISC-PARZ-AP-POL = 0
            //            AND WRIN-IMP-LRD-RISC-PARZ-AC-POL = 0
            //                CONTINUE
            //            ELSE
            //                PERFORM E001-SCARTO THRU EX-E001
            //           END-IF
            if (ws.getWrinRecOutput().getWrinTpRecordDati().getWrinTotCumPremiVersAp().compareTo(0) == 0 && ws.getWrinRecOutput().getWrinTpRecordDati().getWrinPrestazioneApPol().compareTo(0) == 0 && ws.getWrinRecOutput().getWrinTpRecordDati().getWrinImpLrdRiscParzApPol().compareTo(0) == 0 && ws.getWrinRecOutput().getWrinTpRecordDati().getWrinImpLrdRiscParzAcPol().compareTo(0) == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else {
                // COB_CODE: MOVE 9          TO WK-CONTROLLO
                ws.getWsVariabili().setWkControllo(9);
                // COB_CODE: MOVE WRIN-TOT-CUM-PREMI-VERS-AP TO WS-CAMPO-1
                ws.getTracciatoFile().setWsCampo1(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinTotCumPremiVersAp());
                // COB_CODE: MOVE WRIN-PRESTAZIONE-AP-POL     TO WS-CAMPO-2
                ws.getTracciatoFile().setWsCampo2(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinPrestazioneApPol());
                // COB_CODE: SET CNTRL-9     TO TRUE
                ws.getLrgc0661().getMsgErrore().setCntrl9();
                // COB_CODE: MOVE MSG-ERRORE TO WS-ERRORE
                ws.getWsVariabili().setWsErrore(ws.getLrgc0661().getMsgErrore().getMsgErrore());
                // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
                e001Scarto();
            }
        }
        // COB_CODE: MOVE 0 TO WS-SOMMA
        ws.getWorkCalcoli().setWsSomma(Trunc.toDecimal(0, 15, 3));
        // COB_CODE: PERFORM VARYING IX-REC-3 FROM 1 BY 1
        //             UNTIL IX-REC-3 > ELE-MAX-CACHE-3
        //                   + REC3-CUM-PREM-VERS-EC-PREC(IX-REC-3)
        //           END-PERFORM.
        ws.getIxIndici().setRec3(((short)1));
        while (!(ws.getIxIndici().getRec3() > ws.getEleMaxCache3())) {
            // COB_CODE: COMPUTE WS-SOMMA = WS-SOMMA
            //               + REC3-CUM-PREM-VERS-EC-PREC(IX-REC-3)
            ws.getWorkCalcoli().setWsSomma(Trunc.toDecimal(ws.getWorkCalcoli().getWsSomma().add(ws.getEleCache3(ws.getIxIndici().getRec3()).getCumPremVersEcPrec()), 15, 3));
            ws.getIxIndici().setRec3(Trunc.toShort(ws.getIxIndici().getRec3() + 1, 4));
        }
        // COB_CODE: IF  WRIN-TOT-CUM-PREMI-VERS-AP
        //               = WS-SOMMA
        //              CONTINUE
        //           ELSE
        //              PERFORM E001-SCARTO THRU EX-E001
        //           END-IF.
        if (ws.getWrinRecOutput().getWrinTpRecordDati().getWrinTotCumPremiVersAp().compareTo(ws.getWorkCalcoli().getWsSomma()) == 0) {
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE 18         TO WK-CONTROLLO
            ws.getWsVariabili().setWkControllo(18);
            // COB_CODE: MOVE WRIN-TOT-CUM-PREMI-VERS-AP TO WS-CAMPO-1
            ws.getTracciatoFile().setWsCampo1(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinTotCumPremiVersAp());
            // COB_CODE: MOVE WS-SOMMA                   TO WS-CAMPO-2
            ws.getTracciatoFile().setWsCampo2(ws.getWorkCalcoli().getWsSomma());
            // COB_CODE: SET CNTRL-18     TO TRUE
            ws.getLrgc0661().getMsgErrore().setCntrl18();
            // COB_CODE: MOVE MSG-ERRORE TO WS-ERRORE
            ws.getWsVariabili().setWsErrore(ws.getLrgc0661().getMsgErrore().getMsgErrore());
            // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
            e001Scarto();
        }
    }

    /**Original name: C0005-CNTRL-10-19<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLO 10 19                                              *
	 * ----------------------------------------------------------------*</pre>*/
    private void c0005Cntrl1019() {
        // COB_CODE: MOVE 'C0005-CNTRL-10-19'      TO WK-LABEL.
        ws.getWsVariabili().setWkLabel("C0005-CNTRL-10-19");
        // COB_CODE: IF  WRIN-DT-DECOR-POLI < WRIN-DT-RIF-DA
        //               END-IF
        //           END-IF.
        if (Conditions.lt(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinDtDecorPoli(), ws.getWrinRecOutput().getWrinTpRecordDati().getWrinDtRifDa())) {
            // COB_CODE: IF WRIN-PRESTAZIONE-AP-POL > 0
            //              CONTINUE
            //           ELSE
            //              PERFORM E001-SCARTO THRU EX-E001
            //           END-IF
            if (ws.getWrinRecOutput().getWrinTpRecordDati().getWrinPrestazioneApPol().compareTo(0) > 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else {
                // COB_CODE: MOVE WRIN-DT-DECOR-POLI TO WS-DATA-1
                ws.getTracciatoFile().setWsData1(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinDtDecorPoli());
                // COB_CODE: MOVE WRIN-DT-RIF-DA     TO WS-DATA-2
                ws.getTracciatoFile().setWsData2(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinDtRifDa());
                // COB_CODE: MOVE WRIN-PRESTAZIONE-AP-POL TO WS-CAMPO-1
                ws.getTracciatoFile().setWsCampo1(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinPrestazioneApPol());
                // COB_CODE: MOVE 10         TO WK-CONTROLLO
                ws.getWsVariabili().setWkControllo(10);
                // COB_CODE: SET CNTRL-10     TO TRUE
                ws.getLrgc0661().getMsgErrore().setCntrl10();
                // COB_CODE: MOVE MSG-ERRORE TO WS-ERRORE
                ws.getWsVariabili().setWsErrore(ws.getLrgc0661().getMsgErrore().getMsgErrore());
                // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
                e001Scarto();
            }
        }
        // COB_CODE: MOVE 0 TO WS-SOMMA
        ws.getWorkCalcoli().setWsSomma(Trunc.toDecimal(0, 15, 3));
        // COB_CODE: PERFORM VARYING IX-REC-3 FROM 1 BY 1
        //             UNTIL IX-REC-3 > ELE-MAX-CACHE-3
        //               END-IF
        //           END-PERFORM.
        ws.getIxIndici().setRec3(((short)1));
        while (!(ws.getIxIndici().getRec3() > ws.getEleMaxCache3())) {
            // COB_CODE:          IF REC3-TP-GAR(IX-REC-3) = 1 OR 5
            //           *        IF REC3-TP-GAR(IX-REC-3) = 1
            //                              + REC3-PREST-MATUR-EC-PREC(IX-REC-3)
            //                    END-IF
            if (ws.getEleCache3(ws.getIxIndici().getRec3()).getTpGar() == 1 || ws.getEleCache3(ws.getIxIndici().getRec3()).getTpGar() == 5) {
                //        IF REC3-TP-GAR(IX-REC-3) = 1
                // COB_CODE: COMPUTE WS-SOMMA = WS-SOMMA
                //                  + REC3-PREST-MATUR-EC-PREC(IX-REC-3)
                ws.getWorkCalcoli().setWsSomma(Trunc.toDecimal(ws.getWorkCalcoli().getWsSomma().add(ws.getEleCache3(ws.getIxIndici().getRec3()).getPrestMaturEcPrec()), 15, 3));
            }
            ws.getIxIndici().setRec3(Trunc.toShort(ws.getIxIndici().getRec3() + 1, 4));
        }
        // COB_CODE: IF  WRIN-PRESTAZIONE-AP-POL
        //               = WS-SOMMA
        //              CONTINUE
        //           ELSE
        //              PERFORM E001-SCARTO THRU EX-E001
        //           END-IF.
        if (ws.getWrinRecOutput().getWrinTpRecordDati().getWrinPrestazioneApPol().compareTo(ws.getWorkCalcoli().getWsSomma()) == 0) {
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE 19         TO WK-CONTROLLO
            ws.getWsVariabili().setWkControllo(19);
            // COB_CODE: MOVE WRIN-PRESTAZIONE-AP-POL    TO WS-CAMPO-1
            ws.getTracciatoFile().setWsCampo1(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinPrestazioneApPol());
            // COB_CODE: MOVE WS-SOMMA                   TO WS-CAMPO-2
            ws.getTracciatoFile().setWsCampo2(ws.getWorkCalcoli().getWsSomma());
            // COB_CODE: SET CNTRL-19     TO TRUE
            ws.getLrgc0661().getMsgErrore().setCntrl19();
            // COB_CODE: MOVE MSG-ERRORE TO WS-ERRORE
            ws.getWsVariabili().setWsErrore(ws.getLrgc0661().getMsgErrore().getMsgErrore());
            // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
            e001Scarto();
        }
    }

    /**Original name: C0006-CNTRL-11-15<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLO 11 15                                              *
	 * ----------------------------------------------------------------*</pre>*/
    private void c0006Cntrl1115() {
        // COB_CODE: MOVE 'C0006-CNTRL-11-15'      TO WK-LABEL.
        ws.getWsVariabili().setWkLabel("C0006-CNTRL-11-15");
        // COB_CODE: IF  WRIN-PRESTAZIONE-AC-POL > 0
        //               CONTINUE
        //           ELSE
        //              PERFORM E001-SCARTO THRU EX-E001
        //           END-IF.
        if (ws.getWrinRecOutput().getWrinTpRecordDati().getWrinPrestazioneAcPol().compareTo(0) > 0) {
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE 11         TO WK-CONTROLLO
            ws.getWsVariabili().setWkControllo(11);
            // COB_CODE: MOVE WRIN-PRESTAZIONE-AC-POL    TO WS-CAMPO-1
            ws.getTracciatoFile().setWsCampo1(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinPrestazioneAcPol());
            // COB_CODE: MOVE ZERO                       TO WS-CAMPO-2
            ws.getTracciatoFile().setWsCampo2(new AfDecimal(0));
            // COB_CODE: SET CNTRL-11     TO TRUE
            ws.getLrgc0661().getMsgErrore().setCntrl11();
            // COB_CODE: MOVE MSG-ERRORE TO WS-ERRORE
            ws.getWsVariabili().setWsErrore(ws.getLrgc0661().getMsgErrore().getMsgErrore());
            // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
            e001Scarto();
        }
        // COB_CODE: MOVE 0 TO WS-SOMMA
        ws.getWorkCalcoli().setWsSomma(Trunc.toDecimal(0, 15, 3));
        // COB_CODE: PERFORM VARYING IX-REC-3 FROM 1 BY 1
        //             UNTIL IX-REC-3 > ELE-MAX-CACHE-3
        //               END-IF
        //           END-PERFORM.
        ws.getIxIndici().setRec3(((short)1));
        while (!(ws.getIxIndici().getRec3() > ws.getEleMaxCache3())) {
            // COB_CODE:          IF REC3-TP-GAR(IX-REC-3) = 1 OR 5
            //           *        IF REC3-TP-GAR(IX-REC-3) = 1
            //                           + REC3-PREST-MATUR-AA-RIF(IX-REC-3)
            //                    END-IF
            if (ws.getEleCache3(ws.getIxIndici().getRec3()).getTpGar() == 1 || ws.getEleCache3(ws.getIxIndici().getRec3()).getTpGar() == 5) {
                //        IF REC3-TP-GAR(IX-REC-3) = 1
                // COB_CODE: COMPUTE WS-SOMMA = WS-SOMMA
                //               + REC3-PREST-MATUR-AA-RIF(IX-REC-3)
                ws.getWorkCalcoli().setWsSomma(Trunc.toDecimal(ws.getWorkCalcoli().getWsSomma().add(ws.getEleCache3(ws.getIxIndici().getRec3()).getPrestMaturAaRif()), 15, 3));
            }
            ws.getIxIndici().setRec3(Trunc.toShort(ws.getIxIndici().getRec3() + 1, 4));
        }
        // COB_CODE: IF  WRIN-PRESTAZIONE-AC-POL
        //               = WS-SOMMA
        //              CONTINUE
        //           ELSE
        //              PERFORM E001-SCARTO THRU EX-E001
        //           END-IF.
        if (ws.getWrinRecOutput().getWrinTpRecordDati().getWrinPrestazioneAcPol().compareTo(ws.getWorkCalcoli().getWsSomma()) == 0) {
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE 15         TO WK-CONTROLLO
            ws.getWsVariabili().setWkControllo(15);
            // COB_CODE: MOVE WRIN-PRESTAZIONE-AC-POL    TO WS-CAMPO-1
            ws.getTracciatoFile().setWsCampo1(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinPrestazioneAcPol());
            // COB_CODE: MOVE WS-SOMMA                   TO WS-CAMPO-2
            ws.getTracciatoFile().setWsCampo2(ws.getWorkCalcoli().getWsSomma());
            // COB_CODE: SET CNTRL-15     TO TRUE
            ws.getLrgc0661().getMsgErrore().setCntrl15();
            // COB_CODE: MOVE MSG-ERRORE TO WS-ERRORE
            ws.getWsVariabili().setWsErrore(ws.getLrgc0661().getMsgErrore().getMsgErrore());
            // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
            e001Scarto();
        }
    }

    /**Original name: C0007-CNTRL-13-42<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLO 13 42                                              *
	 * ----------------------------------------------------------------*
	 *  --> CONTROLLO 13
	 *  --> FILTRARE LE POLIZZE TCM DAI CONTROLLI 13 E 42</pre>*/
    private void c0007Cntrl1342() {
        // COB_CODE: IF TP-ESTR-TCM
        //              CONTINUE
        //           ELSE
        //              END-IF
        //           END-IF.
        if (ws.getTipologiaEc().isTcm()) {
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE 'C0007-CNTRL-13-42'      TO WK-LABEL
            ws.getWsVariabili().setWkLabel("C0007-CNTRL-13-42");
            // COB_CODE: MOVE 0 TO WS-SOMMA
            ws.getWorkCalcoli().setWsSomma(Trunc.toDecimal(0, 15, 3));
            // COB_CODE:         PERFORM VARYING IX-REC-3 FROM 1 BY 1
            //                     UNTIL IX-REC-3 > ELE-MAX-CACHE-3
            //           *           IF REC3-TP-GAR(IX-REC-3) = 1
            //                       END-IF
            //                   END-PERFORM
            ws.getIxIndici().setRec3(((short)1));
            while (!(ws.getIxIndici().getRec3() > ws.getEleMaxCache3())) {
                //           IF REC3-TP-GAR(IX-REC-3) = 1
                // COB_CODE: IF REC3-TP-GAR(IX-REC-3) = 1 OR 5
                //                  + REC3-CUM-PREM-INVST-AA-RIF(IX-REC-3)
                //           END-IF
                if (ws.getEleCache3(ws.getIxIndici().getRec3()).getTpGar() == 1 || ws.getEleCache3(ws.getIxIndici().getRec3()).getTpGar() == 5) {
                    // COB_CODE: COMPUTE WS-SOMMA = WS-SOMMA
                    //               + REC3-CUM-PREM-INVST-AA-RIF(IX-REC-3)
                    ws.getWorkCalcoli().setWsSomma(Trunc.toDecimal(ws.getWorkCalcoli().getWsSomma().add(ws.getEleCache3(ws.getIxIndici().getRec3()).getCumPremInvstAaRif()), 15, 3));
                }
                ws.getIxIndici().setRec3(Trunc.toShort(ws.getIxIndici().getRec3() + 1, 4));
            }
            // COB_CODE: IF  WRIN-TOT-CUM-PRE-INVST-AA-RIF
            //               = WS-SOMMA
            //              CONTINUE
            //           ELSE
            //              PERFORM E001-SCARTO THRU EX-E001
            //           END-IF
            if (ws.getWrinRecOutput().getWrinTpRecordDati().getWrinTotCumPreInvstAaRif().compareTo(ws.getWorkCalcoli().getWsSomma()) == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else {
                // COB_CODE: MOVE 13         TO WK-CONTROLLO
                ws.getWsVariabili().setWkControllo(13);
                // COB_CODE: MOVE WRIN-TOT-CUM-PRE-INVST-AA-RIF TO WS-CAMPO-1
                ws.getTracciatoFile().setWsCampo1(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinTotCumPreInvstAaRif());
                // COB_CODE: MOVE WS-SOMMA                      TO WS-CAMPO-2
                ws.getTracciatoFile().setWsCampo2(ws.getWorkCalcoli().getWsSomma());
                // COB_CODE: SET CNTRL-13     TO TRUE
                ws.getLrgc0661().getMsgErrore().setCntrl13();
                // COB_CODE: MOVE MSG-ERRORE TO WS-ERRORE
                ws.getWsVariabili().setWsErrore(ws.getLrgc0661().getMsgErrore().getMsgErrore());
                // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
                e001Scarto();
            }
        }
        // --> CONTROLLO 42
        // COB_CODE: MOVE 0 TO WS-SOMMA-123
        //                     WS-DIFF-123
        ws.getWorkCalcoli().setWsSomma123(Trunc.toDecimal(0, 15, 3));
        ws.getWorkCalcoli().setWsDiff123(Trunc.toDecimal(0, 15, 3));
        // COB_CODE: PERFORM VARYING IX-REC-10 FROM 1 BY 1
        //             UNTIL IX-REC-10 > ELE-MAX-CACHE-10
        //               END-IF
        //           END-PERFORM.
        ws.getIxIndici().setRec10(((short)1));
        while (!(ws.getIxIndici().getRec10() > ws.getEleMaxCache10())) {
            // COB_CODE: IF REC10-DESC-TP-MOVI(IX-REC-10) =
            //              TP-VERS-PREMI
            //                   + REC10-IMP-OPER(IX-REC-10)
            //           END-IF
            if (Conditions.eq(ws.getEleCache10(ws.getIxIndici().getRec10()).getDescTpMovi(), ws.getLrgc0661().getWsDescTpMovi().getVersPremi())) {
                // COB_CODE: COMPUTE WS-SOMMA-123 = WS-SOMMA-123
                //               + REC10-IMP-OPER(IX-REC-10)
                ws.getWorkCalcoli().setWsSomma123(Trunc.toDecimal(ws.getWorkCalcoli().getWsSomma123().add(ws.getEleCache10(ws.getIxIndici().getRec10()).getImpOper()), 15, 3));
            }
            ws.getIxIndici().setRec10(Trunc.toShort(ws.getIxIndici().getRec10() + 1, 4));
        }
        // COB_CODE: COMPUTE WS-DIFF-123 = WRIN-TOT-CUM-PRE-INVST-AA-RIF -
        //                             WS-SOMMA-123
        ws.getWorkCalcoli().setWsDiff123(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinTotCumPreInvstAaRif().subtract(ws.getWorkCalcoli().getWsSomma123()), 15, 3));
        // COB_CODE: IF  WS-DIFF-123 < 0,05 AND WS-DIFF-123 > -0,05
        //              CONTINUE
        //           ELSE
        //              PERFORM E001-SCARTO THRU EX-E001
        //           END-IF.
        if (ws.getWorkCalcoli().getWsDiff123().compareTo("0.05") < 0 && ws.getWorkCalcoli().getWsDiff123().compareTo("-0.05") > 0) {
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE 42         TO WK-CONTROLLO
            ws.getWsVariabili().setWkControllo(42);
            // COB_CODE: MOVE WS-SOMMA-123               TO WS-CAMPO-1
            ws.getTracciatoFile().setWsCampo1(ws.getWorkCalcoli().getWsSomma123());
            // COB_CODE: MOVE WRIN-TOT-CUM-PRE-INVST-AA-RIF TO WS-CAMPO-2
            ws.getTracciatoFile().setWsCampo2(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinTotCumPreInvstAaRif());
            // COB_CODE: SET CNTRL-42     TO TRUE
            ws.getLrgc0661().getMsgErrore().setCntrl42();
            // COB_CODE: MOVE MSG-ERRORE TO WS-ERRORE
            ws.getWsVariabili().setWsErrore(ws.getLrgc0661().getMsgErrore().getMsgErrore());
            // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
            e001Scarto();
        }
    }

    /**Original name: C0008-CNTRL-16<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLO 16                                                 *
	 * ----------------------------------------------------------------*</pre>*/
    private void c0008Cntrl16() {
        // COB_CODE: MOVE 'C0008-CNTRL-16'      TO WK-LABEL.
        ws.getWsVariabili().setWkLabel("C0008-CNTRL-16");
        // COB_CODE: MOVE 0 TO WS-SOMMA
        ws.getWorkCalcoli().setWsSomma(Trunc.toDecimal(0, 15, 3));
        // COB_CODE:      PERFORM VARYING IX-REC-3 FROM 1 BY 1
        //                  UNTIL IX-REC-3 > ELE-MAX-CACHE-3
        //           *        IF REC3-TP-GAR(IX-REC-3) = 1
        //                    END-IF
        //                END-PERFORM.
        ws.getIxIndici().setRec3(((short)1));
        while (!(ws.getIxIndici().getRec3() > ws.getEleMaxCache3())) {
            //        IF REC3-TP-GAR(IX-REC-3) = 1
            // COB_CODE: IF REC3-TP-GAR(IX-REC-3) = 1 OR 5
            //                    + REC3-CAP-LIQ-GA(IX-REC-3)
            //           END-IF
            if (ws.getEleCache3(ws.getIxIndici().getRec3()).getTpGar() == 1 || ws.getEleCache3(ws.getIxIndici().getRec3()).getTpGar() == 5) {
                // COB_CODE: COMPUTE WS-SOMMA = WS-SOMMA
                //                 + REC3-CAP-LIQ-GA(IX-REC-3)
                ws.getWorkCalcoli().setWsSomma(Trunc.toDecimal(ws.getWorkCalcoli().getWsSomma().add(ws.getEleCache3(ws.getIxIndici().getRec3()).getCapLiqGa()), 15, 3));
            }
            ws.getIxIndici().setRec3(Trunc.toShort(ws.getIxIndici().getRec3() + 1, 4));
        }
        // COB_CODE: IF  WRIN-CAP-LIQ-DT-RIF-EST-CC
        //               = WS-SOMMA
        //              CONTINUE
        //           ELSE
        //              PERFORM E001-SCARTO THRU EX-E001
        //           END-IF.
        if (ws.getWrinRecOutput().getWrinTpRecordDati().getWrinCapLiqDtRifEstCc().compareTo(ws.getWorkCalcoli().getWsSomma()) == 0) {
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE 16         TO WK-CONTROLLO
            ws.getWsVariabili().setWkControllo(16);
            // COB_CODE: MOVE WRIN-CAP-LIQ-DT-RIF-EST-CC TO WS-CAMPO-1
            ws.getTracciatoFile().setWsCampo1(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinCapLiqDtRifEstCc());
            // COB_CODE: MOVE WS-SOMMA                   TO WS-CAMPO-2
            ws.getTracciatoFile().setWsCampo2(ws.getWorkCalcoli().getWsSomma());
            // COB_CODE: SET CNTRL-16     TO TRUE
            ws.getLrgc0661().getMsgErrore().setCntrl16();
            // COB_CODE: MOVE MSG-ERRORE TO WS-ERRORE
            ws.getWsVariabili().setWsErrore(ws.getLrgc0661().getMsgErrore().getMsgErrore());
            // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
            e001Scarto();
        }
    }

    /**Original name: C0009-CNTRL-17<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLO 17                                                 *
	 * ----------------------------------------------------------------*</pre>*/
    private void c0009Cntrl17() {
        // COB_CODE: MOVE 'C0009-CNTRL-17'      TO WK-LABEL.
        ws.getWsVariabili().setWkLabel("C0009-CNTRL-17");
        // COB_CODE: MOVE 0 TO WS-SOMMA
        ws.getWorkCalcoli().setWsSomma(Trunc.toDecimal(0, 15, 3));
        // COB_CODE: PERFORM VARYING IX-REC-3 FROM 1 BY 1
        //             UNTIL IX-REC-3 > ELE-MAX-CACHE-3
        //                   + REC3-RISC-TOT-GA(IX-REC-3)
        //           END-PERFORM.
        ws.getIxIndici().setRec3(((short)1));
        while (!(ws.getIxIndici().getRec3() > ws.getEleMaxCache3())) {
            // COB_CODE: COMPUTE WS-SOMMA = WS-SOMMA
            //               + REC3-RISC-TOT-GA(IX-REC-3)
            ws.getWorkCalcoli().setWsSomma(Trunc.toDecimal(ws.getWorkCalcoli().getWsSomma().add(ws.getEleCache3(ws.getIxIndici().getRec3()).getRiscTotGa()), 15, 3));
            ws.getIxIndici().setRec3(Trunc.toShort(ws.getIxIndici().getRec3() + 1, 4));
        }
        // COB_CODE: IF  WRIN-RISC-TOT-DT-RIF-EST-CC
        //               = WS-SOMMA
        //              CONTINUE
        //           ELSE
        //              PERFORM E001-SCARTO THRU EX-E001
        //           END-IF.
        if (ws.getWrinRecOutput().getWrinTpRecordDati().getWrinRiscTotDtRifEstCc().compareTo(ws.getWorkCalcoli().getWsSomma()) == 0) {
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE 17         TO WK-CONTROLLO
            ws.getWsVariabili().setWkControllo(17);
            // COB_CODE: MOVE WRIN-RISC-TOT-DT-RIF-EST-CC TO WS-CAMPO-1
            ws.getTracciatoFile().setWsCampo1(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinRiscTotDtRifEstCc());
            // COB_CODE: MOVE WS-SOMMA                   TO WS-CAMPO-2
            ws.getTracciatoFile().setWsCampo2(ws.getWorkCalcoli().getWsSomma());
            // COB_CODE: SET CNTRL-17     TO TRUE
            ws.getLrgc0661().getMsgErrore().setCntrl17();
            // COB_CODE: MOVE MSG-ERRORE TO WS-ERRORE
            ws.getWsVariabili().setWsErrore(ws.getLrgc0661().getMsgErrore().getMsgErrore());
            // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
            e001Scarto();
        }
    }

    /**Original name: C0010-CNTRL-20<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLO 20                                                 *
	 * ----------------------------------------------------------------*</pre>*/
    private void c0010Cntrl20() {
        // COB_CODE: MOVE 'C0010-CNTRL-20'      TO WK-LABEL.
        ws.getWsVariabili().setWkLabel("C0010-CNTRL-20");
        // COB_CODE: MOVE 0 TO WS-SOMMA
        ws.getWorkCalcoli().setWsSomma(Trunc.toDecimal(0, 15, 3));
        // COB_CODE: PERFORM VARYING IX-REC-3 FROM 1 BY 1
        //             UNTIL IX-REC-3 > ELE-MAX-CACHE-3
        //                   + REC3-IMP-LRD-RISC-PARZ-AP-GAR(IX-REC-3)
        //           END-PERFORM.
        ws.getIxIndici().setRec3(((short)1));
        while (!(ws.getIxIndici().getRec3() > ws.getEleMaxCache3())) {
            // COB_CODE: COMPUTE WS-SOMMA = WS-SOMMA
            //               + REC3-IMP-LRD-RISC-PARZ-AP-GAR(IX-REC-3)
            ws.getWorkCalcoli().setWsSomma(Trunc.toDecimal(ws.getWorkCalcoli().getWsSomma().add(ws.getEleCache3(ws.getIxIndici().getRec3()).getImpLrdRiscParzApGar()), 15, 3));
            ws.getIxIndici().setRec3(Trunc.toShort(ws.getIxIndici().getRec3() + 1, 4));
        }
        // COB_CODE: IF  WRIN-IMP-LRD-RISC-PARZ-AP-POL
        //               = WS-SOMMA
        //              CONTINUE
        //           ELSE
        //              PERFORM E001-SCARTO THRU EX-E001
        //           END-IF.
        if (ws.getWrinRecOutput().getWrinTpRecordDati().getWrinImpLrdRiscParzApPol().compareTo(ws.getWorkCalcoli().getWsSomma()) == 0) {
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE 20         TO WK-CONTROLLO
            ws.getWsVariabili().setWkControllo(20);
            // COB_CODE: MOVE WRIN-IMP-LRD-RISC-PARZ-AP-POL TO WS-CAMPO-1
            ws.getTracciatoFile().setWsCampo1(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinImpLrdRiscParzApPol());
            // COB_CODE: MOVE WS-SOMMA                      TO WS-CAMPO-2
            ws.getTracciatoFile().setWsCampo2(ws.getWorkCalcoli().getWsSomma());
            // COB_CODE: SET CNTRL-20     TO TRUE
            ws.getLrgc0661().getMsgErrore().setCntrl20();
            // COB_CODE: MOVE MSG-ERRORE TO WS-ERRORE
            ws.getWsVariabili().setWsErrore(ws.getLrgc0661().getMsgErrore().getMsgErrore());
            // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
            e001Scarto();
        }
    }

    /**Original name: C0011-CNTRL-21<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLO 21                                                 *
	 * ----------------------------------------------------------------*</pre>*/
    private void c0011Cntrl21() {
        // COB_CODE: MOVE 'C0011-CNTRL-21'      TO WK-LABEL.
        ws.getWsVariabili().setWkLabel("C0011-CNTRL-21");
        // COB_CODE: MOVE 0 TO WS-SOMMA
        ws.getWorkCalcoli().setWsSomma(Trunc.toDecimal(0, 15, 3));
        // COB_CODE: PERFORM VARYING IX-REC-3 FROM 1 BY 1
        //             UNTIL IX-REC-3 > ELE-MAX-CACHE-3
        //                   + REC3-IMP-LRD-RISC-PARZ-AC-GAR(IX-REC-3)
        //           END-PERFORM.
        ws.getIxIndici().setRec3(((short)1));
        while (!(ws.getIxIndici().getRec3() > ws.getEleMaxCache3())) {
            // COB_CODE: COMPUTE WS-SOMMA = WS-SOMMA
            //               + REC3-IMP-LRD-RISC-PARZ-AC-GAR(IX-REC-3)
            ws.getWorkCalcoli().setWsSomma(Trunc.toDecimal(ws.getWorkCalcoli().getWsSomma().add(ws.getEleCache3(ws.getIxIndici().getRec3()).getImpLrdRiscParzAcGar()), 15, 3));
            ws.getIxIndici().setRec3(Trunc.toShort(ws.getIxIndici().getRec3() + 1, 4));
        }
        // COB_CODE: IF  WRIN-IMP-LRD-RISC-PARZ-AC-POL
        //               = WS-SOMMA
        //              CONTINUE
        //           ELSE
        //              PERFORM E001-SCARTO THRU EX-E001
        //           END-IF.
        if (ws.getWrinRecOutput().getWrinTpRecordDati().getWrinImpLrdRiscParzAcPol().compareTo(ws.getWorkCalcoli().getWsSomma()) == 0) {
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE 21         TO WK-CONTROLLO
            ws.getWsVariabili().setWkControllo(21);
            // COB_CODE: MOVE WRIN-IMP-LRD-RISC-PARZ-AC-POL TO WS-CAMPO-1
            ws.getTracciatoFile().setWsCampo1(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinImpLrdRiscParzAcPol());
            // COB_CODE: MOVE WS-SOMMA                      TO WS-CAMPO-2
            ws.getTracciatoFile().setWsCampo2(ws.getWorkCalcoli().getWsSomma());
            // COB_CODE: SET CNTRL-21     TO TRUE
            ws.getLrgc0661().getMsgErrore().setCntrl21();
            // COB_CODE: MOVE MSG-ERRORE TO WS-ERRORE
            ws.getWsVariabili().setWsErrore(ws.getLrgc0661().getMsgErrore().getMsgErrore());
            // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
            e001Scarto();
        }
    }

    /**Original name: C0012-CNTRL-23<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLO 23                                                 *
	 * ----------------------------------------------------------------*</pre>*/
    private void c0012Cntrl23() {
        // COB_CODE: MOVE 'C0012-CNTRL-23'      TO WK-LABEL.
        ws.getWsVariabili().setWkLabel("C0012-CNTRL-23");
        // COB_CODE: IF REC2-COGNOME-ASS EQUAL SPACES
        //              OR HIGH-VALUES OR LOW-VALUES
        //              PERFORM E001-SCARTO THRU EX-E001
        //           ELSE
        //              END-IF
        //           END-IF.
        if (Characters.EQ_SPACE.test(ws.getWsVariabili().getRec2CognomeAss()) || Characters.EQ_HIGH.test(ws.getWsVariabili().getRec2CognomeAss(), WsVariabiliLrgs0660.Len.REC2_COGNOME_ASS) || Characters.EQ_LOW.test(ws.getWsVariabili().getRec2CognomeAss(), WsVariabiliLrgs0660.Len.REC2_COGNOME_ASS)) {
            // COB_CODE: SET CNTRL-23     TO TRUE
            ws.getLrgc0661().getMsgErrore().setCntrl23();
            // COB_CODE: MOVE MSG-ERRORE TO WS-ERRORE
            ws.getWsVariabili().setWsErrore(ws.getLrgc0661().getMsgErrore().getMsgErrore());
            // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
            e001Scarto();
        }
        else if (Characters.EQ_SPACE.test(ws.getWsVariabili().getRec2NomeAss()) || Characters.EQ_HIGH.test(ws.getWsVariabili().getRec2NomeAss(), WsVariabiliLrgs0660.Len.REC2_NOME_ASS) || Characters.EQ_LOW.test(ws.getWsVariabili().getRec2NomeAss(), WsVariabiliLrgs0660.Len.REC2_NOME_ASS)) {
            // COB_CODE: IF REC2-NOME-ASS EQUAL SPACES
            //           OR HIGH-VALUES OR LOW-VALUES
            //              PERFORM E001-SCARTO THRU EX-E001
            //           END-IF
            // COB_CODE: SET CNTRL-23     TO TRUE
            ws.getLrgc0661().getMsgErrore().setCntrl23();
            // COB_CODE: MOVE MSG-ERRORE TO WS-ERRORE
            ws.getWsVariabili().setWsErrore(ws.getLrgc0661().getMsgErrore().getMsgErrore());
            // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
            e001Scarto();
        }
    }

    /**Original name: C0013-CNTRL-33<br>
	 * <pre>-  DA RIPRISTINARE DOPO AVER FATTO I TEST           ------------*
	 * ----------------------------------------------------------------*
	 *    CONTROLLO 33                                                 *
	 * ----------------------------------------------------------------*</pre>*/
    private void c0013Cntrl33() {
        // COB_CODE: MOVE 'C0013-CNTRL-33'      TO WK-LABEL.
        ws.getWsVariabili().setWkLabel("C0013-CNTRL-33");
        // COB_CODE: INITIALIZE WORK-CALCOLI.
        initWorkCalcoli();
        // COB_CODE: SET ABBINATA-NO TO TRUE
        ws.getAreaFlag().getFlagAbbinata().setNo();
        // COB_CODE: PERFORM VARYING IX-REC-3 FROM 1 BY 1
        //             UNTIL IX-REC-3 > ELE-MAX-CACHE-3
        //               END-IF
        //           END-PERFORM
        ws.getIxIndici().setRec3(((short)1));
        while (!(ws.getIxIndici().getRec3() > ws.getEleMaxCache3())) {
            // COB_CODE: IF TP-ESTR-RV
            //              END-IF
            //           ELSE
            //              END-IF
            //           END-IF
            if (ws.getTipologiaEc().isRv()) {
                // COB_CODE: IF REC3-TP-GAR(IX-REC-3) = ( 1 OR 5 )
                //           AND REC3-TP-INVST(IX-REC-3) = 4
                //                             + WS-APPO-2
                //           END-IF
                if ((ws.getEleCache3(ws.getIxIndici().getRec3()).getTpGar() == 1 || ws.getEleCache3(ws.getIxIndici().getRec3()).getTpGar() == 5) && ws.getEleCache3(ws.getIxIndici().getRec3()).getTpInvst() == 4) {
                    // COB_CODE: COMPUTE WS-APPO-1 = REC3-PREST-MATUR-AA-RIF(IX-REC-3)
                    //                             + WS-APPO-1
                    ws.getWorkCalcoli().setWsAppo1(Trunc.toDecimal(ws.getEleCache3(ws.getIxIndici().getRec3()).getPrestMaturAaRif().add(ws.getWorkCalcoli().getWsAppo1()), 15, 3));
                    // COB_CODE: COMPUTE WS-APPO-2 = REC3-CAP-LIQ-GA(IX-REC-3)
                    //                             + WS-APPO-2
                    ws.getWorkCalcoli().setWsAppo2(Trunc.toDecimal(ws.getEleCache3(ws.getIxIndici().getRec3()).getCapLiqGa().add(ws.getWorkCalcoli().getWsAppo2()), 15, 3));
                }
            }
            else if (ws.getEleCache3(ws.getIxIndici().getRec3()).getTpGar() == 1 && ws.getEleCache3(ws.getIxIndici().getRec3()).getTpInvst() == 7) {
                // COB_CODE: IF REC3-TP-GAR(IX-REC-3) = 1
                //           AND REC3-TP-INVST(IX-REC-3) = 7
                //                             + WS-APPO-2
                //           END-IF
                // COB_CODE: COMPUTE WS-APPO-1 = REC3-PREST-MATUR-AA-RIF(IX-REC-3)
                //                             + WS-APPO-1
                ws.getWorkCalcoli().setWsAppo1(Trunc.toDecimal(ws.getEleCache3(ws.getIxIndici().getRec3()).getPrestMaturAaRif().add(ws.getWorkCalcoli().getWsAppo1()), 15, 3));
                // COB_CODE: COMPUTE WS-APPO-2 = REC3-CAP-LIQ-GA(IX-REC-3)
                //                             + WS-APPO-2
                ws.getWorkCalcoli().setWsAppo2(Trunc.toDecimal(ws.getEleCache3(ws.getIxIndici().getRec3()).getCapLiqGa().add(ws.getWorkCalcoli().getWsAppo2()), 15, 3));
            }
            ws.getIxIndici().setRec3(Trunc.toShort(ws.getIxIndici().getRec3() + 1, 4));
        }
        // COB_CODE: MOVE WS-APPO-1 TO WS-DIFF
        ws.getWorkCalcoli().setWsDiff(Trunc.toDecimal(ws.getWorkCalcoli().getWsAppo1(), 15, 3));
        // COB_CODE: SET GAR-RAMO3-NO            TO TRUE
        ws.getFlagGarRamo3().setNo();
        // COB_CODE: SET GAR-RAMO3-STOR-NO       TO TRUE
        ws.getFlagGarRamo3Stornata().setNo();
        // COB_CODE: PERFORM VARYING IX-REC-3 FROM 1 BY 1
        //             UNTIL IX-REC-3 > ELE-MAX-CACHE-3
        //               END-IF
        //           END-PERFORM
        ws.getIxIndici().setRec3(((short)1));
        while (!(ws.getIxIndici().getRec3() > ws.getEleMaxCache3())) {
            // COB_CODE: IF  REC3-TP-GAR(IX-REC-3) = 1
            //           AND REC3-TP-INVST(IX-REC-3) = 7 OR 8
            //               END-IF
            //           END-IF
            if (ws.getEleCache3(ws.getIxIndici().getRec3()).getTpGar() == 1 && ws.getEleCache3(ws.getIxIndici().getRec3()).getTpInvst() == 7 || ws.getEleCache3(ws.getIxIndici().getRec3()).getTpInvst() == 8) {
                // COB_CODE: SET GAR-RAMO3-SI       TO TRUE
                ws.getFlagGarRamo3().setSi();
                //    CONTROLLIAMO SE LA GARANZIA DI RAMO 3 SIA STORNATA
                // COB_CODE: IF REC3-FL-STORNATE-ANN(IX-REC-3) = '1'
                //              SET GAR-RAMO3-STOR-SI  TO TRUE
                //           END-IF
                if (ws.getEleCache3(ws.getIxIndici().getRec3()).getFlStornateAnn() == '1') {
                    // COB_CODE: SET GAR-RAMO3-STOR-SI  TO TRUE
                    ws.getFlagGarRamo3Stornata().setSi();
                }
            }
            // COB_CODE: IF REC3-TP-GAR(IX-REC-3) = 4
            //                                REC3-CAP-LIQ-GA(IX-REC-3)
            //           END-IF
            if (ws.getEleCache3(ws.getIxIndici().getRec3()).getTpGar() == 4) {
                // COB_CODE: SET ABBINATA-SI TO TRUE
                ws.getAreaFlag().getFlagAbbinata().setSi();
                // COB_CODE: COMPUTE WS-APPO-3 = WS-APPO-3     +
                //                             REC3-CAP-LIQ-GA(IX-REC-3)
                ws.getWorkCalcoli().setWsAppo3(Trunc.toDecimal(ws.getWorkCalcoli().getWsAppo3().add(ws.getEleCache3(ws.getIxIndici().getRec3()).getCapLiqGa()), 15, 3));
            }
            ws.getIxIndici().setRec3(Trunc.toShort(ws.getIxIndici().getRec3() + 1, 4));
        }
        // COB_CODE: IF (TP-ESTR-MU AND GAR-RAMO3-NO)
        //                          OR
        //              (TP-ESTR-MU AND (GAR-RAMO3-SI AND GAR-RAMO3-STOR-SI))
        //              SET ABBINATA-NO TO TRUE
        //           ELSE
        //              COMPUTE WS-DIFF = (WS-APPO-1 + WS-APPO-3) - WS-APPO-2
        //           END-IF
        if (ws.getTipologiaEc().isMu() && ws.getFlagGarRamo3().isNo() || ws.getTipologiaEc().isMu() && ws.getFlagGarRamo3().isSi() && ws.getFlagGarRamo3Stornata().isSi()) {
            // COB_CODE: SET ABBINATA-NO TO TRUE
            ws.getAreaFlag().getFlagAbbinata().setNo();
        }
        else {
            // COB_CODE: COMPUTE WS-DIFF = (WS-APPO-1 + WS-APPO-3) - WS-APPO-2
            ws.getWorkCalcoli().setWsDiff(Trunc.toDecimal(ws.getWorkCalcoli().getWsAppo1().add(ws.getWorkCalcoli().getWsAppo3()).subtract(ws.getWorkCalcoli().getWsAppo2()), 15, 3));
        }
        // COB_CODE: IF ABBINATA-SI
        //              END-IF
        //           END-IF.
        if (ws.getAreaFlag().getFlagAbbinata().isSi()) {
            // COB_CODE: IF WS-DIFF < -1 OR
            //              WS-DIFF > 1
            //              PERFORM E001-SCARTO THRU EX-E001
            //           END-IF
            if (ws.getWorkCalcoli().getWsDiff().compareTo(-1) < 0 || ws.getWorkCalcoli().getWsDiff().compareTo(1) > 0) {
                // COB_CODE: MOVE 33         TO WK-CONTROLLO
                ws.getWsVariabili().setWkControllo(33);
                // COB_CODE: MOVE WS-APPO-1  TO WS-CAMPO-1
                ws.getTracciatoFile().setWsCampo1(ws.getWorkCalcoli().getWsAppo1());
                // COB_CODE: MOVE WS-APPO-2  TO WS-CAMPO-2
                ws.getTracciatoFile().setWsCampo2(ws.getWorkCalcoli().getWsAppo2());
                // COB_CODE: MOVE WS-APPO-3  TO WS-CAMPO-3
                ws.getTracciatoFile().setWsCampo3(ws.getWorkCalcoli().getWsAppo3());
                // COB_CODE: SET CNTRL-33        TO TRUE
                ws.getLrgc0661().getMsgErrore().setCntrl33();
                // COB_CODE: MOVE MSG-ERRORE     TO WS-ERRORE
                ws.getWsVariabili().setWsErrore(ws.getLrgc0661().getMsgErrore().getMsgErrore());
                // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
                e001Scarto();
            }
        }
    }

    /**Original name: C0014-CNTRL-38<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLO 38                                                 *
	 * ----------------------------------------------------------------*</pre>*/
    private void c0014Cntrl38() {
        // COB_CODE: MOVE 'C0014-CNTRL-38'      TO WK-LABEL.
        ws.getWsVariabili().setWkLabel("C0014-CNTRL-38");
        // COB_CODE: MOVE 0 TO WS-SOMMA
        ws.getWorkCalcoli().setWsSomma(Trunc.toDecimal(0, 15, 3));
        // COB_CODE: PERFORM VARYING IX-REC-3 FROM 1 BY 1
        //             UNTIL IX-REC-3 > ELE-MAX-CACHE-3
        //                   + REC3-IMP-LRD-RISC-PARZ-AC-GAR(IX-REC-3)
        //           END-PERFORM.
        ws.getIxIndici().setRec3(((short)1));
        while (!(ws.getIxIndici().getRec3() > ws.getEleMaxCache3())) {
            // COB_CODE: COMPUTE WS-SOMMA = WS-SOMMA
            //               + REC3-IMP-LRD-RISC-PARZ-AC-GAR(IX-REC-3)
            ws.getWorkCalcoli().setWsSomma(Trunc.toDecimal(ws.getWorkCalcoli().getWsSomma().add(ws.getEleCache3(ws.getIxIndici().getRec3()).getImpLrdRiscParzAcGar()), 15, 3));
            ws.getIxIndici().setRec3(Trunc.toShort(ws.getIxIndici().getRec3() + 1, 4));
        }
        // COB_CODE: IF  REC9-IMP-LORDO-RISC-PARZ =
        //               WS-SOMMA
        //               END-IF
        //           ELSE
        //               PERFORM E001-SCARTO THRU EX-E001
        //           END-IF.
        if (ws.getWsVariabili().getRec9ImpLordoRiscParz().compareTo(ws.getWorkCalcoli().getWsSomma()) == 0) {
            // COB_CODE: IF REC9-IMP-LORDO-RISC-PARZ =
            //              WRIN-IMP-LRD-RISC-PARZ-AC-POL
            //              CONTINUE
            //           ELSE
            //              PERFORM E001-SCARTO THRU EX-E001
            //           END-IF
            if (ws.getWsVariabili().getRec9ImpLordoRiscParz().compareTo(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinImpLrdRiscParzAcPol()) == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else {
                // COB_CODE: MOVE REC9-IMP-LORDO-RISC-PARZ TO WS-CAMPO-1
                ws.getTracciatoFile().setWsCampo1(ws.getWsVariabili().getRec9ImpLordoRiscParz());
                // COB_CODE: MOVE WRIN-IMP-LRD-RISC-PARZ-AC-POL
                //             TO WS-CAMPO-2
                ws.getTracciatoFile().setWsCampo2(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinImpLrdRiscParzAcPol());
                // COB_CODE: MOVE WS-SOMMA
                //             TO WS-CAMPO-3
                ws.getTracciatoFile().setWsCampo3(ws.getWorkCalcoli().getWsSomma());
                // COB_CODE: MOVE 38         TO WK-CONTROLLO
                ws.getWsVariabili().setWkControllo(38);
                // COB_CODE: SET CNTRL-38     TO TRUE
                ws.getLrgc0661().getMsgErrore().setCntrl38();
                // COB_CODE: MOVE MSG-ERRORE TO WS-ERRORE
                ws.getWsVariabili().setWsErrore(ws.getLrgc0661().getMsgErrore().getMsgErrore());
                // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
                e001Scarto();
            }
        }
        else {
            // COB_CODE: MOVE REC9-IMP-LORDO-RISC-PARZ TO WS-CAMPO-1
            ws.getTracciatoFile().setWsCampo1(ws.getWsVariabili().getRec9ImpLordoRiscParz());
            // COB_CODE: MOVE WRIN-IMP-LRD-RISC-PARZ-AC-POL
            //             TO WS-CAMPO-2
            ws.getTracciatoFile().setWsCampo2(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinImpLrdRiscParzAcPol());
            // COB_CODE: MOVE WS-SOMMA
            //             TO WS-CAMPO-3
            ws.getTracciatoFile().setWsCampo3(ws.getWorkCalcoli().getWsSomma());
            // COB_CODE: MOVE 38         TO WK-CONTROLLO
            ws.getWsVariabili().setWkControllo(38);
            // COB_CODE: SET CNTRL-38     TO TRUE
            ws.getLrgc0661().getMsgErrore().setCntrl38();
            // COB_CODE: MOVE MSG-ERRORE TO WS-ERRORE
            ws.getWsVariabili().setWsErrore(ws.getLrgc0661().getMsgErrore().getMsgErrore());
            // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
            e001Scarto();
        }
    }

    /**Original name: C0015-CNTRL-40<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLO 40                                                 *
	 * ----------------------------------------------------------------*</pre>*/
    private void c0015Cntrl40() {
        // COB_CODE: MOVE 'C0015-CNTRL-40'      TO WK-LABEL.
        ws.getWsVariabili().setWkLabel("C0015-CNTRL-40");
        // COB_CODE: MOVE 0 TO WS-SOMMA
        //                     WS-DIFF
        ws.getWorkCalcoli().setWsSomma(Trunc.toDecimal(0, 15, 3));
        ws.getWorkCalcoli().setWsDiff(Trunc.toDecimal(0, 15, 3));
        // COB_CODE: SET REC-9-TP-MOVI-TROVATO-NO TO TRUE
        ws.getAreaFlag().getRec9TpMoviTrovato().setNo();
        // COB_CODE: SET ERRORE-NO TO TRUE
        ws.getAreaFlag().getFlagErroreSi().setNo();
        // COB_CODE: PERFORM VARYING IX-REC-9 FROM 1 BY 1
        //             UNTIL IX-REC-9 > ELE-MAX-CACHE-9
        //                OR ERRORE-SI
        //                OR REC-9-TP-MOVI-TROVATO-SI
        //              END-PERFORM
        //           END-PERFORM.
        ws.getIxIndici().setRec9(((short)1));
        while (!(ws.getIxIndici().getRec9() > ws.getEleMaxCache9() || ws.getAreaFlag().getFlagErroreSi().isSi() || ws.getAreaFlag().getRec9TpMoviTrovato().isSi())) {
            // COB_CODE: SET REC-9-TP-MOVI-TROVATO-NO TO TRUE
            ws.getAreaFlag().getRec9TpMoviTrovato().setNo();
            // COB_CODE: MOVE 0 TO WS-SOMMA
            //                     WS-DIFF
            ws.getWorkCalcoli().setWsSomma(Trunc.toDecimal(0, 15, 3));
            ws.getWorkCalcoli().setWsDiff(Trunc.toDecimal(0, 15, 3));
            // COB_CODE: PERFORM VARYING IX-REC-10 FROM 1 BY 1
            //             UNTIL IX-REC-10 > ELE-MAX-CACHE-10
            //                  OR ERRORE-SI
            //                END-IF
            //           END-PERFORM
            ws.getIxIndici().setRec10(((short)1));
            while (!(ws.getIxIndici().getRec10() > ws.getEleMaxCache10() || ws.getAreaFlag().getFlagErroreSi().isSi())) {
                // COB_CODE: IF REC10-TP-MOVI(IX-REC-10) =
                //              REC9-RP-TP-MOVI(IX-REC-9)
                //                  SET REC-9-TP-MOVI-TROVATO-SI TO TRUE
                //              END-IF
                if (ws.getEleCache10(ws.getIxIndici().getRec10()).getTpMovi() == ws.getEleCache9(ws.getIxIndici().getRec9()).getRpTpMovi()) {
                    // COB_CODE: COMPUTE WS-SOMMA =
                    //               WS-SOMMA +
                    //               REC10-IMP-OPER(IX-REC-10)
                    ws.getWorkCalcoli().setWsSomma(Trunc.toDecimal(ws.getWorkCalcoli().getWsSomma().add(ws.getEleCache10(ws.getIxIndici().getRec10()).getImpOper()), 15, 3));
                    // COB_CODE: MOVE IX-REC-9 TO IX-REC9-TP-MOVI
                    ws.getIxIndici().setRec9TpMovi(ws.getIxIndici().getRec9());
                    // COB_CODE: SET REC-9-TP-MOVI-TROVATO-SI TO TRUE
                    ws.getAreaFlag().getRec9TpMoviTrovato().setSi();
                }
                ws.getIxIndici().setRec10(Trunc.toShort(ws.getIxIndici().getRec10() + 1, 4));
            }
            ws.getIxIndici().setRec9(Trunc.toShort(ws.getIxIndici().getRec9() + 1, 4));
        }
        // COB_CODE: IF REC-9-TP-MOVI-TROVATO-SI
        //                END-IF
        //           END-IF.
        if (ws.getAreaFlag().getRec9TpMoviTrovato().isSi()) {
            // COB_CODE: COMPUTE WS-DIFF = REC9-IMP-LRD-RISC-PARZ(IX-REC9-TP-MOVI)-
            //                             WS-SOMMA
            ws.getWorkCalcoli().setWsDiff(Trunc.toDecimal(ws.getEleCache9(ws.getIxIndici().getRec9TpMovi()).getImpLrdRiscParz().subtract(ws.getWorkCalcoli().getWsSomma()), 15, 3));
            // COB_CODE: IF WS-DIFF < 0,05 AND WS-DIFF > -0,05
            //               CONTINUE
            //           ELSE
            //                SET ERRORE-SI TO TRUE
            //             END-IF
            if (ws.getWorkCalcoli().getWsDiff().compareTo("0.05") < 0 && ws.getWorkCalcoli().getWsDiff().compareTo("-0.05") > 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else {
                // COB_CODE: MOVE 40      TO WK-CONTROLLO
                ws.getWsVariabili().setWkControllo(40);
                // COB_CODE: MOVE REC9-IMP-LRD-RISC-PARZ(IX-REC9-TP-MOVI)
                //                                           TO WS-CAMPO-1
                ws.getTracciatoFile().setWsCampo1(ws.getEleCache9(ws.getIxIndici().getRec9TpMovi()).getImpLrdRiscParz());
                // COB_CODE: MOVE WS-SOMMA                TO WS-CAMPO-2
                ws.getTracciatoFile().setWsCampo2(ws.getWorkCalcoli().getWsSomma());
                // COB_CODE: SET CNTRL-40  TO TRUE
                ws.getLrgc0661().getMsgErrore().setCntrl40();
                // COB_CODE: MOVE MSG-ERRORE TO WS-ERRORE
                ws.getWsVariabili().setWsErrore(ws.getLrgc0661().getMsgErrore().getMsgErrore());
                // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
                e001Scarto();
                // COB_CODE: SET ERRORE-SI TO TRUE
                ws.getAreaFlag().getFlagErroreSi().setSi();
            }
        }
    }

    /**Original name: A700-CALCOLO-DATA<br>
	 * <pre>----------------------------------------------------------------*
	 *  CALCOLO LA DATA + DELTA GIORNI
	 * ----------------------------------------------------------------*</pre>*/
    private void a700CalcoloData() {
        // COB_CODE: MOVE 'A700-CALCOLO-DATA'          TO WK-LABEL.
        ws.getWsVariabili().setWkLabel("A700-CALCOLO-DATA");
        // SOTTRAE UN ANNO ALLA DATA RIF A
        // COB_CODE: INITIALIZE IO-A2K-LCCC0003.
        initIoA2kLccc0003();
        // COB_CODE: MOVE '03'                           TO A2K-FUNZ.
        ws.getIoA2kLccc0003().getInput().setA2kFunz("03");
        // COB_CODE: MOVE '03'                           TO A2K-INFO.
        ws.getIoA2kLccc0003().getInput().setA2kInfo("03");
        // COB_CODE: MOVE 1                              TO A2K-DELTA.
        ws.getIoA2kLccc0003().getInput().setA2kDelta(((short)1));
        // COB_CODE: MOVE 'A'                            TO A2K-TDELTA.
        ws.getIoA2kLccc0003().getInput().setA2kTdeltaFormatted("A");
        // COB_CODE: MOVE WK-DATA-IN                     TO A2K-INDATA.
        ws.getIoA2kLccc0003().getInput().getA2kIndata().setA2kIndata(ws.getWsVariabili().getWkDataInFormatted());
        // COB_CODE: MOVE '0'                            TO A2K-FISLAV
        //                                                  A2K-INICON.
        ws.getIoA2kLccc0003().getInput().setA2kFislavFormatted("0");
        ws.getIoA2kLccc0003().getInput().setA2kIniconFormatted("0");
        // COB_CODE: PERFORM A710-CALL-LCCS0003
        //              THRU A710-EX.
        a710CallLccs0003();
        // COB_CODE: IF IDSV0001-ESITO-OK
        //              MOVE A2K-OUAMG                   TO WK-DATA-OUT
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: MOVE A2K-OUAMG                   TO WK-DATA-OUT
            ws.getWsVariabili().setWkDataOutFormatted(ws.getIoA2kLccc0003().getOutput().getA2kOuamgX().getA2kOuamgFormatted());
        }
    }

    /**Original name: A710-CALL-LCCS0003<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE PER IL CALCOLO SULLE DATE
	 * ----------------------------------------------------------------*</pre>*/
    private void a710CallLccs0003() {
        Lccs0003 lccs0003 = null;
        StringParam wkRcode = null;
        // COB_CODE: MOVE 'A710-CALL-LCCS0003'     TO WK-LABEL.
        ws.getWsVariabili().setWkLabel("A710-CALL-LCCS0003");
        // COB_CODE: CALL PGM-LCCS0003 USING IO-A2K-LCCC0003 WK-RCODE
        //           ON EXCEPTION
        //                 THRU EX-S0290
        //           END-CALL.
        try {
            lccs0003 = Lccs0003.getInstance();
            wkRcode = new StringParam(ws.getWsVariabili().getWkRcode(), WsVariabiliLrgs0660.Len.WK_RCODE);
            lccs0003.run(ws.getIoA2kLccc0003(), wkRcode);
            ws.getWsVariabili().setWkRcode(wkRcode.getString());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-PGM                 TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'SERVIZIO OPERAZIONI SULLE DATE'
            //                                       TO CALL-DESC
            ws.getIdsv0002().setCallDesc("SERVIZIO OPERAZIONI SULLE DATE");
            // COB_CODE: MOVE WK-LABEL               TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWsVariabili().getWkLabel());
            // COB_CODE: PERFORM S0290-ERRORE-DI-SISTEMA
            //              THRU EX-S0290
            s0290ErroreDiSistema();
        }
        // COB_CODE: IF WK-RCODE NOT = '00'
        //                 THRU EX-S0300
        //           END-IF.
        if (!Conditions.eq(ws.getWsVariabili().getWkRcode(), "00")) {
            // COB_CODE: MOVE WK-PGM                 TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL               TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWsVariabili().getWkLabel());
            // COB_CODE: MOVE '005016'               TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: MOVE SPACES                 TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: C0020-CNTRL-4-5-39<br>
	 * <pre>--> CONTROLLI
	 * ----------------------------------------------------------------*
	 *     ESTRATTO CONTO - FASE DIAGNOSTICO
	 *        - CONTROLLI AD HOC MULTIRAMO & RIVALUTABILI
	 * ----------------------------------------------------------------*
	 * ----------------------------------------------------------------*
	 *    CONTROLLO 4 5 39                                             *
	 * ----------------------------------------------------------------*</pre>*/
    private void c0020Cntrl4539() {
        // COB_CODE: MOVE 'C0020-CNTRL-4-5-39'          TO WK-LABEL.
        ws.getWsVariabili().setWkLabel("C0020-CNTRL-4-5-39");
        // COB_CODE: IF WRIN-FLAG-PROD-CON-CEDOLE = 'SI'
        //                 THRU EX-C0021
        //           END-IF
        if (Conditions.eq(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinFlagProdConCedole(), "SI")) {
            // COB_CODE: PERFORM C0021-CNTRL-4
            //              THRU EX-C0021
            c0021Cntrl4();
        }
        // COB_CODE:      IF CONTROLLO-OK
        //           *--> CONTROLLO 5
        //                END-IF
        //                END-IF
        if (ws.getAreaFlag().getFlagControllo().isOk()) {
            //--> CONTROLLO 5
            // COB_CODE: IF WRIN-IMP-LOR-CED-LIQU > 0
            //              END-IF
            //           END-IF
            if (ws.getWrinRecOutput().getWrinTpRecordDati().getWrinImpLorCedLiqu().compareTo(0) > 0) {
                // COB_CODE: IF  (WRIN-CC-PAESE EQUAL HIGH-VALUES
                //                OR LOW-VALUES OR SPACES)
                //           OR  (WRIN-CC-CHECK-DIGIT EQUAL HIGH-VALUES
                //                OR LOW-VALUES OR SPACES)
                //           OR (WRIN-CC-CIN EQUAL HIGH-VALUES
                //               OR LOW-VALUES OR SPACES)
                //           OR (WRIN-CC-ABI EQUAL HIGH-VALUES
                //               OR LOW-VALUES OR SPACES)
                //           OR (WRIN-CC-CAB EQUAL HIGH-VALUES
                //               OR LOW-VALUES OR SPACES)
                //           OR (WRIN-CC-NUM-CONTO EQUAL HIGH-VALUES
                //               OR LOW-VALUES OR SPACES)
                //               PERFORM E001-SCARTO THRU EX-E001
                //           END-IF
                if (Characters.EQ_HIGH.test(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinCcPaeseFormatted()) || Characters.EQ_LOW.test(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinCcPaeseFormatted()) || Characters.EQ_SPACE.test(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinCcPaese()) || Characters.EQ_HIGH.test(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinCcCheckDigitFormatted()) || Characters.EQ_LOW.test(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinCcCheckDigitFormatted()) || Characters.EQ_SPACE.test(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinCcCheckDigit()) || Conditions.eq(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinCcCin(), Types.HIGH_CHAR_VAL) || Conditions.eq(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinCcCin(), Types.LOW_CHAR_VAL) || Conditions.eq(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinCcCin(), Types.SPACE_CHAR) || Characters.EQ_HIGH.test(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinCcAbiFormatted()) || Characters.EQ_LOW.test(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinCcAbiFormatted()) || Characters.EQ_SPACE.test(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinCcAbi()) || Characters.EQ_HIGH.test(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinCcCabFormatted()) || Characters.EQ_LOW.test(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinCcCabFormatted()) || Characters.EQ_SPACE.test(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinCcCab()) || Characters.EQ_HIGH.test(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinCcNumContoFormatted()) || Characters.EQ_LOW.test(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinCcNumContoFormatted()) || Characters.EQ_SPACE.test(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinCcNumConto())) {
                    // COB_CODE: MOVE 5 TO WK-CONTROLLO
                    ws.getWsVariabili().setWkControllo(5);
                    // COB_CODE: SET CNTRL-5     TO TRUE
                    ws.getLrgc0661().getMsgErrore().setCntrl5();
                    // COB_CODE: MOVE MSG-ERRORE TO WS-ERRORE
                    ws.getWsVariabili().setWsErrore(ws.getLrgc0661().getMsgErrore().getMsgErrore());
                    // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
                    e001Scarto();
                }
            }
        }
        // COB_CODE: IF CONTROLLO-OK
        //           END-IF
        //           END-IF.
        if (ws.getAreaFlag().getFlagControllo().isOk()) {
            // COB_CODE: MOVE 0 TO WS-SOMMA
            ws.getWorkCalcoli().setWsSomma(Trunc.toDecimal(0, 15, 3));
            // COB_CODE: PERFORM VARYING IX-REC-10 FROM 1 BY 1
            //             UNTIL IX-REC-10 > ELE-MAX-CACHE-10
            //               END-IF
            //           END-PERFORM
            ws.getIxIndici().setRec10(((short)1));
            while (!(ws.getIxIndici().getRec10() > ws.getEleMaxCache10())) {
                // COB_CODE: IF REC10-DESC-TP-MOVI(IX-REC-10) =
                //              TP-PAG-CEDOLA
                //                    + REC10-IMP-OPER(IX-REC-10)
                //           END-IF
                if (Conditions.eq(ws.getEleCache10(ws.getIxIndici().getRec10()).getDescTpMovi(), ws.getLrgc0661().getWsDescTpMovi().getPagCedola())) {
                    // COB_CODE: COMPUTE WS-SOMMA = WS-SOMMA
                    //                 + REC10-IMP-OPER(IX-REC-10)
                    ws.getWorkCalcoli().setWsSomma(Trunc.toDecimal(ws.getWorkCalcoli().getWsSomma().add(ws.getEleCache10(ws.getIxIndici().getRec10()).getImpOper()), 15, 3));
                }
                ws.getIxIndici().setRec10(Trunc.toShort(ws.getIxIndici().getRec10() + 1, 4));
            }
            // COB_CODE: COMPUTE WS-DIFF = WRIN-IMP-LOR-CED-LIQU - WS-SOMMA
            ws.getWorkCalcoli().setWsDiff(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinImpLorCedLiqu().subtract(ws.getWorkCalcoli().getWsSomma()), 15, 3));
            // COB_CODE: IF  WS-DIFF < 0,05 AND WS-DIFF > -0,05
            //              CONTINUE
            //           ELSE
            //              PERFORM E001-SCARTO THRU EX-E001
            //           END-IF
            if (ws.getWorkCalcoli().getWsDiff().compareTo("0.05") < 0 && ws.getWorkCalcoli().getWsDiff().compareTo("-0.05") > 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else {
                // COB_CODE: MOVE WS-SOMMA              TO WS-CAMPO-1
                ws.getTracciatoFile().setWsCampo1(ws.getWorkCalcoli().getWsSomma());
                // COB_CODE: MOVE WRIN-IMP-LOR-CED-LIQU TO WS-CAMPO-2
                ws.getTracciatoFile().setWsCampo2(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinImpLorCedLiqu());
                // COB_CODE: MOVE 39 TO WK-CONTROLLO
                ws.getWsVariabili().setWkControllo(39);
                // COB_CODE: SET CNTRL-39     TO TRUE
                ws.getLrgc0661().getMsgErrore().setCntrl39();
                // COB_CODE: MOVE MSG-ERRORE TO WS-ERRORE
                ws.getWsVariabili().setWsErrore(ws.getLrgc0661().getMsgErrore().getMsgErrore());
                // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
                e001Scarto();
            }
        }
    }

    /**Original name: C0021-CNTRL-4<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLO 4                                                  *
	 * ----------------------------------------------------------------*</pre>*/
    private void c0021Cntrl4() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'C0021-CNTRL-4'          TO WK-LABEL.
        ws.getWsVariabili().setWkLabel("C0021-CNTRL-4");
        // COB_CODE: INITIALIZE ESTR-CNT-DIAGN-CED
        //                      IDSI0011-BUFFER-DATI.
        initEstrCntDiagnCed();
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
        // COB_CODE: MOVE ZEROES        TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                 IDSI0011-DATA-FINE-EFFETTO
        //                                 IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        // COB_CODE: SET IDSI0011-WHERE-CONDITION   TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setWhereCondition();
        // COB_CODE: SET IDSI0011-SELECT            TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        // COB_CODE: SET IDSI0011-TRATT-SENZA-STOR  TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattSenzaStor();
        //
        // COB_CODE: MOVE WRIN-ID-POLI     TO LDBVF301-ID-POLI.
        ws.getLdbvf301().setIdPoli(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinIdPoli());
        //    MOVE 20150101         TO LDBVF301-DT-LIQ-CED-DA
        //    MOVE 20150131         TO LDBVF301-DT-LIQ-CED-A
        //
        // COB_CODE: MOVE WK-DT-INF        TO LDBVF301-DT-LIQ-CED-DA
        ws.getLdbvf301().setDtLiqCedDa(ws.getWsVariabili().getWkDtInf());
        //
        // COB_CODE: MOVE WK-DT-SUP        TO LDBVF301-DT-LIQ-CED-A
        ws.getLdbvf301().setDtLiqCedA(ws.getWsVariabili().getWkDtSup());
        //
        // COB_CODE: MOVE 'LDBSF300'       TO IDSI0011-CODICE-STR-DATO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("LDBSF300");
        //
        // COB_CODE: MOVE LDBVF301         TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getLdbvf301().getLdbvf301Formatted());
        // COB_CODE: MOVE LDBVF301         TO IDSI0011-BUFFER-WHERE-COND.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferWhereCond(ws.getLdbvf301().getLdbvf301Formatted());
        //
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX
        callDispatcher();
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //                   END-EVALUATE
        //                ELSE
        //           *-->    ERRORE DISPATCHER
        //                   PERFORM S0300-RICERCA-GRAVITA-ERRORE THRU EX-S0300
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            // COB_CODE:         EVALUATE TRUE
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //                             END-IF
            //           *
            //                       WHEN IDSO0011-NOT-FOUND
            //                            END-IF
            //           *
            //                       WHEN OTHER
            //                                      THRU EX-S0300
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL:// COB_CODE: MOVE IDSO0011-BUFFER-DATI
                    //             TO ESTR-CNT-DIAGN-CED
                    ws.getEstrCntDiagnCed().setEstrCntDiagnCedFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    // COB_CODE: IF WRIN-IMP-LOR-CED-LIQU =
                    //              P84-IMP-LRD-CEDOLE-LIQ
                    //              CONTINUE
                    //           ELSE
                    //              PERFORM E001-SCARTO THRU EX-E001
                    //           END-IF
                    if (ws.getWrinRecOutput().getWrinTpRecordDati().getWrinImpLorCedLiqu().compareTo(ws.getEstrCntDiagnCed().getP84ImpLrdCedoleLiq().getP84ImpLrdCedoleLiq()) == 0) {
                    // COB_CODE: CONTINUE
                    //continue
                    }
                    else {
                        // COB_CODE: MOVE WRIN-IMP-LOR-CED-LIQU
                        //             TO WS-CAMPO-1
                        ws.getTracciatoFile().setWsCampo1(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinImpLorCedLiqu());
                        // COB_CODE: MOVE P84-IMP-LRD-CEDOLE-LIQ
                        //             TO WS-CAMPO-2
                        ws.getTracciatoFile().setWsCampo2(ws.getEstrCntDiagnCed().getP84ImpLrdCedoleLiq().getP84ImpLrdCedoleLiq());
                        // COB_CODE: MOVE 4 TO WK-CONTROLLO
                        ws.getWsVariabili().setWkControllo(4);
                        // COB_CODE: SET CNTRL-4     TO TRUE
                        ws.getLrgc0661().getMsgErrore().setCntrl4();
                        // COB_CODE: MOVE MSG-ERRORE  TO WS-ERRORE
                        ws.getWsVariabili().setWsErrore(ws.getLrgc0661().getMsgErrore().getMsgErrore());
                        // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
                        e001Scarto();
                    }
                    //
                    break;

                case Idso0011SqlcodeSigned.NOT_FOUND:// COB_CODE: IF WRIN-IMP-LOR-CED-LIQU EQUAL ZERO
                    //              CONTINUE
                    //           ELSE
                    //              PERFORM E001-SCARTO THRU EX-E001
                    //           END-IF
                    if (ws.getWrinRecOutput().getWrinTpRecordDati().getWrinImpLorCedLiqu().compareTo(0) == 0) {
                    // COB_CODE: CONTINUE
                    //continue
                    }
                    else {
                        // COB_CODE: MOVE 203 TO WK-CONTROLLO
                        ws.getWsVariabili().setWkControllo(203);
                        // COB_CODE: STRING '203; POLIZZA NOT '
                        //                  'FOUND IN TAB CEDOLE'
                        //           DELIMITED BY SIZE
                        //           INTO WS-ERRORE
                        concatUtil = ConcatUtil.buildString(WsVariabiliLrgs0660.Len.WS_ERRORE, "203; POLIZZA NOT ", "FOUND IN TAB CEDOLE");
                        ws.getWsVariabili().setWsErrore(concatUtil.replaceInString(ws.getWsVariabili().getWsErroreFormatted()));
                        // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
                        e001Scarto();
                    }
                    //
                    break;

                default:// COB_CODE: MOVE WK-PGM         TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE WK-LABEL       TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr(ws.getWsVariabili().getWkLabel());
                    // COB_CODE: MOVE '005016'       TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005016");
                    // COB_CODE: STRING 'LDBSF310'       ';'
                    //                  IDSO0011-RETURN-CODE ';'
                    //                  IDSO0011-SQLCODE
                    //                  DELIMITED BY SIZE
                    //                  INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "LDBSF310", ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //                   THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;
            }
        }
        else {
            //-->    ERRORE DISPATCHER
            // COB_CODE: MOVE WK-PGM                TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL              TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWsVariabili().getWkLabel());
            // COB_CODE: MOVE '005016'              TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING 'LDBSF310'        ';'
            //                  IDSO0011-RETURN-CODE ';'
            //                  IDSO0011-SQLCODE
            //                  DELIMITED BY   SIZE INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "LDBSF310", ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: C0022-CNTRL-RIVALUTAZ<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLO RIVALUTAZ   - 26 27 28 29 30 31 32                 *
	 * ----------------------------------------------------------------*</pre>*/
    private void c0022CntrlRivalutaz() {
        // COB_CODE: MOVE 'C0022-CNTRL-RIVALUTAZ'          TO WK-LABEL.
        ws.getWsVariabili().setWkLabel("C0022-CNTRL-RIVALUTAZ");
        //
        // COB_CODE:      PERFORM VARYING IX-REC-3 FROM 1 BY 1
        //                  UNTIL IX-REC-3 > ELE-MAX-CACHE-3
        //           *        IF REC3-TP-GAR(IX-REC-3) = 1
        //                    END-IF
        //                END-PERFORM.
        ws.getIxIndici().setRec3(((short)1));
        while (!(ws.getIxIndici().getRec3() > ws.getEleMaxCache3())) {
            //        IF REC3-TP-GAR(IX-REC-3) = 1
            // COB_CODE: IF REC3-TP-GAR(IX-REC-3) = ( 1 OR 5 )
            //              AND REC3-TP-INVST(IX-REC-3) = 4
            //                 THRU EX-C023
            //           END-IF
            if ((ws.getEleCache3(ws.getIxIndici().getRec3()).getTpGar() == 1 || ws.getEleCache3(ws.getIxIndici().getRec3()).getTpGar() == 5) && ws.getEleCache3(ws.getIxIndici().getRec3()).getTpInvst() == 4) {
                // COB_CODE: PERFORM C023-CONTROLLI
                //              THRU EX-C023
                c023Controlli();
            }
            ws.getIxIndici().setRec3(Trunc.toShort(ws.getIxIndici().getRec3() + 1, 4));
        }
    }

    /**Original name: C023-CONTROLLI<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLO RIVALUTAZ   - 26 27 28 29 30 31 32                 *
	 * ----------------------------------------------------------------*</pre>*/
    private void c023Controlli() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'C023-CONTROLLI'          TO WK-LABEL.
        ws.getWsVariabili().setWkLabel("C023-CONTROLLI");
        // COB_CODE: INITIALIZE ESTR-CNT-DIAGN-RIV
        //                      IDSI0011-BUFFER-DATI.
        initEstrCntDiagnRiv();
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
        // COB_CODE: MOVE ZEROES        TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                 IDSI0011-DATA-FINE-EFFETTO
        //                                 IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        // COB_CODE: SET IDSI0011-WHERE-CONDITION   TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setWhereCondition();
        // COB_CODE: SET IDSI0011-SELECT            TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        // COB_CODE: SET IDSI0011-TRATT-SENZA-STOR  TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattSenzaStor();
        //
        // COB_CODE: MOVE WRIN-ID-POLI     TO LDBVF321-ID-POLI.
        ws.getLdbvf321().setIdPoli(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinIdPoli());
        //    MOVE 20150101         TO LDBVF321-DT-RIV-DA
        //    MOVE 20150131         TO LDBVF321-DT-RIV-A
        //
        // COB_CODE: MOVE WK-DT-INF        TO LDBVF321-DT-RIV-DA
        ws.getLdbvf321().setDtRivDa(ws.getWsVariabili().getWkDtInf());
        // COB_CODE: MOVE WK-DT-SUP        TO LDBVF321-DT-RIV-A
        ws.getLdbvf321().setDtRivA(ws.getWsVariabili().getWkDtSup());
        // COB_CODE: MOVE REC3-COD-GAR(IX-REC-3)
        //                                 TO LDBVF321-COD-TARI
        ws.getLdbvf321().setCodTari(ws.getEleCache3(ws.getIxIndici().getRec3()).getCodGar());
        //
        // COB_CODE: MOVE 'LDBSF320'       TO IDSI0011-CODICE-STR-DATO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("LDBSF320");
        //
        // COB_CODE: MOVE LDBVF321         TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getLdbvf321().getLdbvf321Formatted());
        // COB_CODE: MOVE LDBVF321         TO IDSI0011-BUFFER-WHERE-COND.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferWhereCond(ws.getLdbvf321().getLdbvf321Formatted());
        //
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX
        callDispatcher();
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //                   END-EVALUATE
        //                ELSE
        //           *-->    ERRORE DISPATCHER
        //                   PERFORM S0300-RICERCA-GRAVITA-ERRORE THRU EX-S0300
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            // COB_CODE:         EVALUATE TRUE
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //                                 THRU CONTROLLI-FLUSSO-RIVA-EX
            //           *
            //                       WHEN IDSO0011-NOT-FOUND
            //                               PERFORM E001-SCARTO THRU EX-E001
            //           *
            //                       WHEN OTHER
            //                                      THRU EX-S0300
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL:// COB_CODE: MOVE IDSO0011-BUFFER-DATI
                    //             TO ESTR-CNT-DIAGN-RIV
                    ws.getEstrCntDiagnRiv().setEstrCntDiagnRivFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    // COB_CODE: MOVE 1 TO IX-TAB-P85
                    ws.getIxIndici().setTabP85(((short)1));
                    // COB_CODE: PERFORM VALORIZZA-OUTPUT-P85
                    //              THRU VALORIZZA-OUTPUT-P85-EX
                    valorizzaOutputP85();
                    //
                    // COB_CODE: PERFORM CONTROLLI-FLUSSO-RIVA
                    //              THRU CONTROLLI-FLUSSO-RIVA-EX
                    controlliFlussoRiva();
                    //
                    break;

                case Idso0011SqlcodeSigned.NOT_FOUND:// COB_CODE: MOVE 203 TO WK-CONTROLLO
                    ws.getWsVariabili().setWkControllo(203);
                    // COB_CODE: STRING '203; POLIZZA NOT '
                    //                  'FOUND IN TAB RIVAL'
                    //           DELIMITED BY SIZE
                    //           INTO WS-ERRORE
                    concatUtil = ConcatUtil.buildString(WsVariabiliLrgs0660.Len.WS_ERRORE, "203; POLIZZA NOT ", "FOUND IN TAB RIVAL");
                    ws.getWsVariabili().setWsErrore(concatUtil.replaceInString(ws.getWsVariabili().getWsErroreFormatted()));
                    // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
                    e001Scarto();
                    //
                    break;

                default:// COB_CODE: MOVE WK-PGM         TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE WK-LABEL       TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr(ws.getWsVariabili().getWkLabel());
                    // COB_CODE: MOVE '005016'       TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005016");
                    // COB_CODE: STRING 'LDBSF320'       ';'
                    //                  IDSO0011-RETURN-CODE ';'
                    //                  IDSO0011-SQLCODE
                    //                  DELIMITED BY SIZE
                    //                  INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "LDBSF320", ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //                   THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;
            }
        }
        else {
            //-->    ERRORE DISPATCHER
            // COB_CODE: MOVE WK-PGM                TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL              TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWsVariabili().getWkLabel());
            // COB_CODE: MOVE '005016'              TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING 'LDBSF320'        ';'
            //                  IDSO0011-RETURN-CODE ';'
            //                  IDSO0011-SQLCODE
            //                  DELIMITED BY   SIZE INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "LDBSF320", ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: C0255-CNTRL-25<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLO 25                                                 *
	 * ----------------------------------------------------------------*</pre>*/
    private void c0255Cntrl25() {
        // COB_CODE: MOVE 'C0255-CNTRL-25'                 TO WK-LABEL.
        ws.getWsVariabili().setWkLabel("C0255-CNTRL-25");
        //
        // COB_CODE:      PERFORM VARYING IX-REC-3 FROM 1 BY 1
        //                  UNTIL IX-REC-3 > ELE-MAX-CACHE-3
        //           *       IF REC3-TP-GAR(IX-REC-3) = 1
        //                   END-IF
        //                END-PERFORM.
        ws.getIxIndici().setRec3(((short)1));
        while (!(ws.getIxIndici().getRec3() > ws.getEleMaxCache3())) {
            //       IF REC3-TP-GAR(IX-REC-3) = 1
            // COB_CODE: IF REC3-TP-GAR(IX-REC-3) =  ( 1 OR 5 )
            //           AND REC3-TP-INVST(IX-REC-3) = 4
            //           END-PERFORM
            //           END-IF
            if ((ws.getEleCache3(ws.getIxIndici().getRec3()).getTpGar() == 1 || ws.getEleCache3(ws.getIxIndici().getRec3()).getTpGar() == 5) && ws.getEleCache3(ws.getIxIndici().getRec3()).getTpInvst() == 4) {
                // COB_CODE: SET LOOP-CNTRL-25-NO TO TRUE
                ws.getAreaFlag().getFlagLoopCntrl25().setNo();
                // COB_CODE: PERFORM VARYING IX-IND FROM 1 BY 1
                //             UNTIL IDSV0001-ESITO-KO
                //                OR IX-IND > ISPC0590-NUM-MAX-PROD
                //                OR LOOP-CNTRL-25-SI
                //               END-IF
                //           END-PERFORM
                ws.getIxIndici().setInd(((short)1));
                while (!(areaIdsv0001.getEsito().isIdsv0001EsitoKo() || ws.getIxIndici().getInd() > ws.getAreaIoIsps0590().getNumMaxProd() || ws.getAreaFlag().getFlagLoopCntrl25().isSi())) {
                    // COB_CODE: IF REC3-COD-GAR(IX-REC-3) =
                    //              ISPC0590-COD-TARI(IX-IND)
                    //              END-IF
                    //           END-IF
                    if (Conditions.eq(ws.getEleCache3(ws.getIxIndici().getRec3()).getCodGar(), ws.getAreaIoIsps0590().getTabProd(ws.getIxIndici().getInd()).getCodTari())) {
                        // COB_CODE: IF (ISPC0590-TASSO-TECNICO(IX-IND) * 100) =
                        //              REC3-TASSO-TECNICO(IX-REC-3)
                        //              SET LOOP-CNTRL-25-SI TO TRUE
                        //           ELSE
                        //              PERFORM E001-SCARTO THRU EX-E001
                        //           END-IF
                        if (ws.getAreaIoIsps0590().getTabProd(ws.getIxIndici().getInd()).getTassoTecnico().multiply(100).compareTo(ws.getEleCache3(ws.getIxIndici().getRec3()).getTassoTecnico()) == 0) {
                            // COB_CODE: SET LOOP-CNTRL-25-SI TO TRUE
                            ws.getAreaFlag().getFlagLoopCntrl25().setSi();
                        }
                        else {
                            // COB_CODE: MOVE REC3-TASSO-TECNICO(IX-REC-3)
                            //             TO WS-CAMPO-1
                            ws.getTracciatoFile().setWsCampo1(ws.getEleCache3(ws.getIxIndici().getRec3()).getTassoTecnico());
                            // COB_CODE: MOVE ISPC0590-TASSO-TECNICO(IX-IND)
                            //             TO WS-CAMPO-2
                            ws.getTracciatoFile().setWsCampo2(ws.getAreaIoIsps0590().getTabProd(ws.getIxIndici().getInd()).getTassoTecnico());
                            // COB_CODE: MOVE 25 TO WK-CONTROLLO
                            ws.getWsVariabili().setWkControllo(25);
                            // COB_CODE: SET CNTRL-25     TO TRUE
                            ws.getLrgc0661().getMsgErrore().setCntrl25();
                            // COB_CODE: MOVE MSG-ERRORE TO WS-ERRORE
                            ws.getWsVariabili().setWsErrore(ws.getLrgc0661().getMsgErrore().getMsgErrore());
                            // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
                            e001Scarto();
                        }
                    }
                    ws.getIxIndici().setInd(Trunc.toShort(ws.getIxIndici().getInd() + 1, 4));
                }
            }
            ws.getIxIndici().setRec3(Trunc.toShort(ws.getIxIndici().getRec3() + 1, 4));
        }
    }

    /**Original name: CONTROLLI-FLUSSO-RIVA<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLI SUL FLUSSO RIVALUTAZIONE                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void controlliFlussoRiva() {
        // COB_CODE: MOVE 'CONTROLLI-FLUSSO-RIVA'          TO WK-LABEL.
        ws.getWsVariabili().setWkLabel("CONTROLLI-FLUSSO-RIVA");
        // COB_CODE: INITIALIZE AREA-RIVAL
        initAreaRival();
        //--> CONTROLLO 26
        // COB_CODE: IF WP85-RENDTO-LRD-NULL(IX-TAB-P85) = HIGH-VALUES
        //              MOVE 0 TO WP85-RENDTO-LRD(IX-TAB-P85)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWp84TabP85(ws.getIxIndici().getTabP85()).getLccvp851().getDati().getWp85RendtoLrd().getWp85RendtoLrdNullFormatted())) {
            // COB_CODE: MOVE 0 TO WP85-RENDTO-LRD(IX-TAB-P85)
            ws.getWp84TabP85(ws.getIxIndici().getTabP85()).getLccvp851().getDati().getWp85RendtoLrd().setWp85RendtoLrd(Trunc.toDecimal(0, 14, 9));
        }
        // COB_CODE: COMPUTE WREC-RENDTO-LRD ROUNDED =
        //                   REC3-REND-LOR-GEST-SEP-GAR(IX-REC-3)
        ws.getLoar0171().getWrecOut().setRendtoLrd(Trunc.toDecimal(MathUtil.convertRoundDecimal((ws.getEleCache3(ws.getIxIndici().getRec3()).getRendLorGestSepGar()), 2, RoundingMode.ROUND_UP, 31, 2), 5, 2));
        //    IF REC3-REND-LOR-GEST-SEP-GAR(IX-REC-3) =
        // COB_CODE: IF WREC-RENDTO-LRD =
        //              WP85-RENDTO-LRD(IX-TAB-P85)
        //              CONTINUE
        //           ELSE
        //              PERFORM E001-SCARTO THRU EX-E001
        //           END-IF.
        if (ws.getLoar0171().getWrecOut().getRendtoLrd().compareTo(ws.getWp84TabP85(ws.getIxIndici().getTabP85()).getLccvp851().getDati().getWp85RendtoLrd().getWp85RendtoLrd()) == 0) {
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE 26 TO WK-CONTROLLO
            ws.getWsVariabili().setWkControllo(26);
            // COB_CODE: MOVE REC3-REND-LOR-GEST-SEP-GAR(IX-REC-3)
            //             TO WS-CAMPO-1
            ws.getTracciatoFile().setWsCampo1(ws.getEleCache3(ws.getIxIndici().getRec3()).getRendLorGestSepGar());
            // COB_CODE: MOVE WP85-RENDTO-LRD(IX-TAB-P85)
            //             TO WS-CAMPO-2
            ws.getTracciatoFile().setWsCampo2(ws.getWp84TabP85(ws.getIxIndici().getTabP85()).getLccvp851().getDati().getWp85RendtoLrd().getWp85RendtoLrd());
            // COB_CODE: SET CNTRL-26     TO TRUE
            ws.getLrgc0661().getMsgErrore().setCntrl26();
            // COB_CODE: MOVE MSG-ERRORE  TO WS-ERRORE
            ws.getWsVariabili().setWsErrore(ws.getLrgc0661().getMsgErrore().getMsgErrore());
            // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
            e001Scarto();
        }
        //
        //--> CONTROLLO 27
        // COB_CODE: IF WP85-PC-RETR-NULL(IX-TAB-P85) = HIGH-VALUES
        //              MOVE 0 TO WP85-PC-RETR(IX-TAB-P85)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWp84TabP85(ws.getIxIndici().getTabP85()).getLccvp851().getDati().getWp85PcRetr().getWp85PcRetrNullFormatted())) {
            // COB_CODE: MOVE 0 TO WP85-PC-RETR(IX-TAB-P85)
            ws.getWp84TabP85(ws.getIxIndici().getTabP85()).getLccvp851().getDati().getWp85PcRetr().setWp85PcRetr(Trunc.toDecimal(0, 6, 3));
        }
        // COB_CODE: COMPUTE WREC-PC-RETR    ROUNDED =
        //                   REC3-ALIQ-RETROC-RICON-GAR(IX-REC-3)
        ws.getLoar0171().getWrecOut().setPcRetr(Trunc.toDecimal(MathUtil.convertRoundDecimal((ws.getEleCache3(ws.getIxIndici().getRec3()).getAliqRetrocRiconGar()), 3, RoundingMode.ROUND_UP, 31, 3), 6, 3));
        //    IF REC3-ALIQ-RETROC-RICON-GAR(IX-REC-3) =
        // COB_CODE: IF WREC-PC-RETR =
        //              WP85-PC-RETR(IX-TAB-P85)
        //              CONTINUE
        //           ELSE
        //              PERFORM E001-SCARTO THRU EX-E001
        //           END-IF.
        if (ws.getLoar0171().getWrecOut().getPcRetr().compareTo(ws.getWp84TabP85(ws.getIxIndici().getTabP85()).getLccvp851().getDati().getWp85PcRetr().getWp85PcRetr()) == 0) {
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE 27 TO WK-CONTROLLO
            ws.getWsVariabili().setWkControllo(27);
            // COB_CODE: MOVE REC3-ALIQ-RETROC-RICON-GAR(IX-REC-3)
            //             TO WS-CAMPO-1
            ws.getTracciatoFile().setWsCampo1(ws.getEleCache3(ws.getIxIndici().getRec3()).getAliqRetrocRiconGar());
            // COB_CODE: MOVE WP85-PC-RETR(IX-TAB-P85)
            //             TO WS-CAMPO-2
            ws.getTracciatoFile().setWsCampo2(ws.getWp84TabP85(ws.getIxIndici().getTabP85()).getLccvp851().getDati().getWp85PcRetr().getWp85PcRetr());
            // COB_CODE: SET CNTRL-27     TO TRUE
            ws.getLrgc0661().getMsgErrore().setCntrl27();
            // COB_CODE: MOVE MSG-ERRORE  TO WS-ERRORE
            ws.getWsVariabili().setWsErrore(ws.getLrgc0661().getMsgErrore().getMsgErrore());
            // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
            e001Scarto();
        }
        //
        //--> CONTROLLO 28 CONFRONTO CON UN CAMPO 9(3)V9(2)
        // COB_CODE: IF WP85-RENDTO-RETR-NULL(IX-TAB-P85) = HIGH-VALUES
        //              MOVE 0 TO WP85-RENDTO-RETR(IX-TAB-P85)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWp84TabP85(ws.getIxIndici().getTabP85()).getLccvp851().getDati().getWp85RendtoRetr().getWp85RendtoRetrNullFormatted())) {
            // COB_CODE: MOVE 0 TO WP85-RENDTO-RETR(IX-TAB-P85)
            ws.getWp84TabP85(ws.getIxIndici().getTabP85()).getLccvp851().getDati().getWp85RendtoRetr().setWp85RendtoRetr(Trunc.toDecimal(0, 14, 9));
        }
        // COB_CODE: COMPUTE WREC-RENDTO-RETR ROUNDED =
        //                   REC3-TASSO-ANN-REND-RETROC-GAR(IX-REC-3)
        ws.getLoar0171().getWrecOut().setRendtoRetr(Trunc.toDecimal(MathUtil.convertRoundDecimal((ws.getEleCache3(ws.getIxIndici().getRec3()).getTassoAnnRendRetrocGar()), 2, RoundingMode.ROUND_UP, 31, 2), 5, 2));
        // COB_CODE: MOVE REC3-TASSO-ANN-REND-RETROC-GAR(IX-REC-3)
        //             TO WS-CAMPO-APPOGGIO
        ws.getWorkCalcoli().setWsCampoAppoggio(Trunc.toDecimal(ws.getEleCache3(ws.getIxIndici().getRec3()).getTassoAnnRendRetrocGar(), 5, 2));
        //    IF WS-CAMPO-APPOGGIO =
        // COB_CODE: IF WREC-RENDTO-RETR  =
        //              WP85-RENDTO-RETR(IX-TAB-P85)
        //              CONTINUE
        //           ELSE
        //              PERFORM E001-SCARTO THRU EX-E001
        //           END-IF.
        if (ws.getLoar0171().getWrecOut().getRendtoRetr().compareTo(ws.getWp84TabP85(ws.getIxIndici().getTabP85()).getLccvp851().getDati().getWp85RendtoRetr().getWp85RendtoRetr()) == 0) {
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE 28 TO WK-CONTROLLO
            ws.getWsVariabili().setWkControllo(28);
            // COB_CODE: MOVE REC3-TASSO-ANN-REND-RETROC-GAR(IX-REC-3)
            //             TO WS-CAMPO-1
            ws.getTracciatoFile().setWsCampo1(ws.getEleCache3(ws.getIxIndici().getRec3()).getTassoAnnRendRetrocGar());
            // COB_CODE: MOVE WP85-RENDTO-RETR(IX-TAB-P85)
            //             TO WS-CAMPO-2
            ws.getTracciatoFile().setWsCampo2(ws.getWp84TabP85(ws.getIxIndici().getTabP85()).getLccvp851().getDati().getWp85RendtoRetr().getWp85RendtoRetr());
            // COB_CODE: SET CNTRL-28     TO TRUE
            ws.getLrgc0661().getMsgErrore().setCntrl28();
            // COB_CODE: MOVE MSG-ERRORE  TO WS-ERRORE
            ws.getWsVariabili().setWsErrore(ws.getLrgc0661().getMsgErrore().getMsgErrore());
            // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
            e001Scarto();
        }
        //
        //--> CONTROLLO 29
        // COB_CODE: IF WP85-COMMIS-GEST-NULL(IX-TAB-P85) = HIGH-VALUES
        //              MOVE 0 TO WP85-COMMIS-GEST(IX-TAB-P85)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWp84TabP85(ws.getIxIndici().getTabP85()).getLccvp851().getDati().getWp85CommisGest().getWp85CommisGestNullFormatted())) {
            // COB_CODE: MOVE 0 TO WP85-COMMIS-GEST(IX-TAB-P85)
            ws.getWp84TabP85(ws.getIxIndici().getTabP85()).getLccvp851().getDati().getWp85CommisGest().setWp85CommisGest(Trunc.toDecimal(0, 15, 3));
        }
        // COB_CODE: COMPUTE WREC-COMM-GEST  ROUNDED =
        //                   REC3-PC-COMM-GEST-GAR(IX-REC-3)
        ws.getLoar0171().getWrecOut().setCommGest(Trunc.toDecimal(MathUtil.convertRoundDecimal((ws.getEleCache3(ws.getIxIndici().getRec3()).getPcCommGestGar()), 3, RoundingMode.ROUND_UP, 31, 3), 15, 3));
        //    IF REC3-PC-COMM-GEST-GAR(IX-REC-3) =
        // COB_CODE: IF WREC-COMM-GEST =
        //              WP85-COMMIS-GEST(IX-TAB-P85)
        //              CONTINUE
        //           ELSE
        //              PERFORM E001-SCARTO THRU EX-E001
        //           END-IF.
        if (ws.getLoar0171().getWrecOut().getCommGest().compareTo(ws.getWp84TabP85(ws.getIxIndici().getTabP85()).getLccvp851().getDati().getWp85CommisGest().getWp85CommisGest()) == 0) {
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE 29 TO WK-CONTROLLO
            ws.getWsVariabili().setWkControllo(29);
            // COB_CODE: MOVE REC3-PC-COMM-GEST-GAR(IX-REC-3)
            //             TO WS-CAMPO-1
            ws.getTracciatoFile().setWsCampo1(ws.getEleCache3(ws.getIxIndici().getRec3()).getPcCommGestGar());
            // COB_CODE: MOVE WP85-COMMIS-GEST(IX-TAB-P85)
            //             TO WS-CAMPO-2
            ws.getTracciatoFile().setWsCampo2(ws.getWp84TabP85(ws.getIxIndici().getTabP85()).getLccvp851().getDati().getWp85CommisGest().getWp85CommisGest());
            // COB_CODE: SET CNTRL-29     TO TRUE
            ws.getLrgc0661().getMsgErrore().setCntrl29();
            // COB_CODE: MOVE MSG-ERRORE  TO WS-ERRORE
            ws.getWsVariabili().setWsErrore(ws.getLrgc0661().getMsgErrore().getMsgErrore());
            // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
            e001Scarto();
        }
        //
        //--> CONTROLLO 30
        // COB_CODE: SET CONTROLLO-30-SI TO TRUE
        ws.getAreaFlag().getFlagCntrl30().setSi();
        // COB_CODE: PERFORM VARYING IX-REC-10 FROM 1 BY 1
        //             UNTIL IX-REC-10 > ELE-MAX-CACHE-10
        //                OR CONTROLLO-30-NO
        //               END-IF
        //           END-PERFORM
        ws.getIxIndici().setRec10(((short)1));
        while (!(ws.getIxIndici().getRec10() > ws.getEleMaxCache10() || ws.getAreaFlag().getFlagCntrl30().isNo())) {
            // COB_CODE: IF REC10-DESC-TP-MOVI(IX-REC-10) =
            //              TP-IMP-SOST
            //              SET CONTROLLO-30-NO TO TRUE
            //           END-IF
            if (Conditions.eq(ws.getEleCache10(ws.getIxIndici().getRec10()).getDescTpMovi(), ws.getLrgc0661().getWsDescTpMovi().getImpSost())) {
                // COB_CODE: SET CONTROLLO-30-NO TO TRUE
                ws.getAreaFlag().getFlagCntrl30().setNo();
            }
            ws.getIxIndici().setRec10(Trunc.toShort(ws.getIxIndici().getRec10() + 1, 4));
        }
        // COB_CODE:      IF CONTROLLO-30-SI
        //           *       IF WP85-COD-TARI(IX-TAB-P85) = 'TRVU3'
        //           *       AND WK-ANNO-RIF = 2015
        //           *           MOVE 2,3
        //           *             TO WP85-TS-RIVAL-NET(IX-TAB-P85)
        //           *       END-IF
        //                   END-IF
        //                END-IF.
        if (ws.getAreaFlag().getFlagCntrl30().isSi()) {
            //       IF WP85-COD-TARI(IX-TAB-P85) = 'TRVU3'
            //       AND WK-ANNO-RIF = 2015
            //           MOVE 2,3
            //             TO WP85-TS-RIVAL-NET(IX-TAB-P85)
            //       END-IF
            // COB_CODE: IF WP85-TS-RIVAL-NET-NULL(IX-TAB-P85) = HIGH-VALUES
            //              MOVE 0 TO WP85-TS-RIVAL-NET(IX-TAB-P85)
            //           END-IF
            if (Characters.EQ_HIGH.test(ws.getWp84TabP85(ws.getIxIndici().getTabP85()).getLccvp851().getDati().getWp85TsRivalNet().getWp85TsRivalNetNullFormatted())) {
                // COB_CODE: MOVE 0 TO WP85-TS-RIVAL-NET(IX-TAB-P85)
                ws.getWp84TabP85(ws.getIxIndici().getTabP85()).getLccvp851().getDati().getWp85TsRivalNet().setWp85TsRivalNet(Trunc.toDecimal(0, 14, 9));
            }
            // COB_CODE: COMPUTE WREC-RENDTO-NET ROUNDED =
            //                   REC3-TASSO-ANN-RIVAL-PREST-GAR(IX-REC-3)
            ws.getLoar0171().getWrecOut().setRendtoNet(Trunc.toDecimal(MathUtil.convertRoundDecimal((ws.getEleCache3(ws.getIxIndici().getRec3()).getTassoAnnRivalPrestGar()), 2, RoundingMode.ROUND_UP, 31, 2), 5, 2));
            // COB_CODE: IF WREC-RENDTO-NET =
            //              WP85-TS-RIVAL-NET(IX-TAB-P85)
            //              CONTINUE
            //           ELSE
            //              PERFORM E001-SCARTO THRU EX-E001
            //           END-IF
            if (ws.getLoar0171().getWrecOut().getRendtoNet().compareTo(ws.getWp84TabP85(ws.getIxIndici().getTabP85()).getLccvp851().getDati().getWp85TsRivalNet().getWp85TsRivalNet()) == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else {
                // COB_CODE: MOVE 30 TO WK-CONTROLLO
                ws.getWsVariabili().setWkControllo(30);
                // COB_CODE: MOVE REC3-TASSO-ANN-RIVAL-PREST-GAR(IX-REC-3)
                //             TO WS-CAMPO-1
                ws.getTracciatoFile().setWsCampo1(ws.getEleCache3(ws.getIxIndici().getRec3()).getTassoAnnRivalPrestGar());
                // COB_CODE: MOVE WP85-TS-RIVAL-NET(IX-TAB-P85)
                //             TO WS-CAMPO-2
                ws.getTracciatoFile().setWsCampo2(ws.getWp84TabP85(ws.getIxIndici().getTabP85()).getLccvp851().getDati().getWp85TsRivalNet().getWp85TsRivalNet());
                // COB_CODE: SET CNTRL-30     TO TRUE
                ws.getLrgc0661().getMsgErrore().setCntrl30();
                // COB_CODE: MOVE MSG-ERRORE  TO WS-ERRORE
                ws.getWsVariabili().setWsErrore(ws.getLrgc0661().getMsgErrore().getMsgErrore());
                // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
                e001Scarto();
            }
        }
        //
        //--> CONTROLLO 31
        // COB_CODE: IF WP85-MIN-TRNUT-NULL(IX-TAB-P85) = HIGH-VALUES
        //              MOVE 0 TO WP85-MIN-TRNUT(IX-TAB-P85)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWp84TabP85(ws.getIxIndici().getTabP85()).getLccvp851().getDati().getWp85MinTrnut().getWp85MinTrnutNullFormatted())) {
            // COB_CODE: MOVE 0 TO WP85-MIN-TRNUT(IX-TAB-P85)
            ws.getWp84TabP85(ws.getIxIndici().getTabP85()).getLccvp851().getDati().getWp85MinTrnut().setWp85MinTrnut(Trunc.toDecimal(0, 14, 9));
        }
        // COB_CODE: COMPUTE WREC-MIN-TRNUT  ROUNDED =
        //                   REC3-REN-MIN-TRNUT(IX-REC-3)
        ws.getLoar0171().getWrecOut().setMinTrnut(Trunc.toDecimal(MathUtil.convertRoundDecimal((ws.getEleCache3(ws.getIxIndici().getRec3()).getRenMinTrnut()), 3, RoundingMode.ROUND_UP, 31, 3), 6, 3));
        //    IF REC3-REN-MIN-TRNUT(IX-REC-3) =
        // COB_CODE: IF WREC-MIN-TRNUT  =
        //              WP85-MIN-TRNUT(IX-TAB-P85)
        //              CONTINUE
        //           ELSE
        //              PERFORM E001-SCARTO THRU EX-E001
        //           END-IF.
        if (ws.getLoar0171().getWrecOut().getMinTrnut().compareTo(ws.getWp84TabP85(ws.getIxIndici().getTabP85()).getLccvp851().getDati().getWp85MinTrnut().getWp85MinTrnut()) == 0) {
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE 31 TO WK-CONTROLLO
            ws.getWsVariabili().setWkControllo(31);
            // COB_CODE: MOVE REC3-REN-MIN-TRNUT(IX-REC-3)
            //             TO WS-CAMPO-1
            ws.getTracciatoFile().setWsCampo1(ws.getEleCache3(ws.getIxIndici().getRec3()).getRenMinTrnut());
            // COB_CODE: MOVE WP85-MIN-TRNUT(IX-TAB-P85)
            //             TO WS-CAMPO-2
            ws.getTracciatoFile().setWsCampo2(ws.getWp84TabP85(ws.getIxIndici().getTabP85()).getLccvp851().getDati().getWp85MinTrnut().getWp85MinTrnut());
            // COB_CODE: SET CNTRL-31     TO TRUE
            ws.getLrgc0661().getMsgErrore().setCntrl31();
            // COB_CODE: MOVE MSG-ERRORE  TO WS-ERRORE
            ws.getWsVariabili().setWsErrore(ws.getLrgc0661().getMsgErrore().getMsgErrore());
            // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
            e001Scarto();
        }
        //
        //--> CONTROLLO 32
        //    IF WP85-COD-TARI(IX-TAB-P85) = 'TRVU3'
        //    AND WK-ANNO-RIF = 2015
        //       MOVE 0
        //         TO WP85-MIN-GARTO(IX-TAB-P85)
        //    END-IF
        // COB_CODE: IF WP85-MIN-GARTO-NULL(IX-TAB-P85) = HIGH-VALUES
        //              MOVE 0 TO WP85-MIN-GARTO(IX-TAB-P85)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWp84TabP85(ws.getIxIndici().getTabP85()).getLccvp851().getDati().getWp85MinGarto().getWp85MinGartoNullFormatted())) {
            // COB_CODE: MOVE 0 TO WP85-MIN-GARTO(IX-TAB-P85)
            ws.getWp84TabP85(ws.getIxIndici().getTabP85()).getLccvp851().getDati().getWp85MinGarto().setWp85MinGarto(Trunc.toDecimal(0, 14, 9));
        }
        // COB_CODE: COMPUTE WREC-MIN-GARTO  ROUNDED =
        //                   REC3-REN-MIN-GARTO(IX-REC-3)
        ws.getLoar0171().getWrecOut().setMinGarto(Trunc.toDecimal(MathUtil.convertRoundDecimal((ws.getEleCache3(ws.getIxIndici().getRec3()).getRenMinGarto()), 2, RoundingMode.ROUND_UP, 31, 2), 5, 2));
        //    IF REC3-REN-MIN-GARTO(IX-REC-3) =
        // COB_CODE: IF WREC-MIN-GARTO =
        //              WP85-MIN-GARTO(IX-TAB-P85)
        //              CONTINUE
        //           ELSE
        //              PERFORM E001-SCARTO THRU EX-E001
        //           END-IF.
        if (ws.getLoar0171().getWrecOut().getMinGarto().compareTo(ws.getWp84TabP85(ws.getIxIndici().getTabP85()).getLccvp851().getDati().getWp85MinGarto().getWp85MinGarto()) == 0) {
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE 32 TO WK-CONTROLLO
            ws.getWsVariabili().setWkControllo(32);
            // COB_CODE: MOVE REC3-REN-MIN-GARTO(IX-REC-3)
            //             TO WS-CAMPO-1
            ws.getTracciatoFile().setWsCampo1(ws.getEleCache3(ws.getIxIndici().getRec3()).getRenMinGarto());
            // COB_CODE: MOVE WP85-MIN-GARTO(IX-TAB-P85)
            //             TO WS-CAMPO-2
            ws.getTracciatoFile().setWsCampo2(ws.getWp84TabP85(ws.getIxIndici().getTabP85()).getLccvp851().getDati().getWp85MinGarto().getWp85MinGarto());
            // COB_CODE: SET CNTRL-32     TO TRUE
            ws.getLrgc0661().getMsgErrore().setCntrl32();
            // COB_CODE: MOVE MSG-ERRORE  TO WS-ERRORE
            ws.getWsVariabili().setWsErrore(ws.getLrgc0661().getMsgErrore().getMsgErrore());
            // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
            e001Scarto();
        }
    }

    /**Original name: CONTROLLO-43<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLO 43                                                 *
	 * ----------------------------------------------------------------*</pre>*/
    private void controllo43() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'CONTROLLO-43'                   TO WK-LABEL.
        ws.getWsVariabili().setWkLabel("CONTROLLO-43");
        // COB_CODE: SET TROVATO-NO           TO TRUE
        ws.getFlagTrovato().setNo();
        // COB_CODE: IF IDSV0001-ESITO-OK
        //                 END-IF
        //           END-IF
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: PERFORM LETTURA-IMPST-SOST
            //              THRU LETTURA-IMPST-SOST-EX
            letturaImpstSost();
            // COB_CODE:            IF IDSV0001-ESITO-OK
            //                      AND TROVATO-SI
            //           *--> CONTROLLO 43
            //                         END-IF
            //                      END-IF
            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk() && ws.getFlagTrovato().isSi()) {
                //--> CONTROLLO 43
                // COB_CODE: IF ISO-IMPST-SOST-NULL NOT = HIGH-VALUE
                //                                    AND LOW-VALUE
                //                                    AND SPACES
                //             MOVE ISO-IMPST-SOST TO WS-APPO-43-2
                //           ELSE
                //                PERFORM E001-SCARTO THRU EX-E001
                //           END-IF
                if (!Characters.EQ_HIGH.test(ws.getImpstSost().getIsoImpstSost().getIsoImpstSostNullFormatted()) && !Characters.EQ_LOW.test(ws.getImpstSost().getIsoImpstSost().getIsoImpstSostNullFormatted()) && !Characters.EQ_SPACE.test(ws.getImpstSost().getIsoImpstSost().getIsoImpstSostNull())) {
                    // COB_CODE: MOVE ISO-IMPST-SOST TO WS-APPO-43-2
                    ws.getWorkCalcoli().setWsAppo432(Trunc.toDecimal(ws.getImpstSost().getIsoImpstSost().getIsoImpstSost(), 15, 3));
                }
                else {
                    // COB_CODE: MOVE 43 TO WK-CONTROLLO
                    ws.getWsVariabili().setWkControllo(43);
                    // COB_CODE: STRING '203; POLIZZA NOT '
                    //                  'FOUND IN TAB IMPOSTA SOSTITUTIVA'
                    //           DELIMITED BY SIZE
                    //           INTO WS-ERRORE
                    concatUtil = ConcatUtil.buildString(WsVariabiliLrgs0660.Len.WS_ERRORE, "203; POLIZZA NOT ", "FOUND IN TAB IMPOSTA SOSTITUTIVA");
                    ws.getWsVariabili().setWsErrore(concatUtil.replaceInString(ws.getWsVariabili().getWsErroreFormatted()));
                    // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
                    e001Scarto();
                }
            }
        }
        //
        // COB_CODE: IF IDSV0001-ESITO-OK AND TROVATO-NO
        //              INTO WS-ERRORE
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk() && ws.getFlagTrovato().isNo()) {
            // COB_CODE: MOVE 201 TO WK-CONTROLLO
            ws.getWsVariabili().setWkControllo(201);
            // COB_CODE: STRING '201;POLIZZA NON TROVATA NELLA TAB IMPSOST'
            //           DELIMITED BY SIZE
            //           INTO WS-ERRORE
            ws.getWsVariabili().setWsErrore("201;POLIZZA NON TROVATA NELLA TAB IMPSOST");
        }
    }

    /**Original name: C0024-CNTRL-43<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLO 43                                                 *
	 * ----------------------------------------------------------------*</pre>*/
    private void c0024Cntrl43() {
        // COB_CODE: MOVE 'C0024-CNTRL-43'          TO WK-LABEL.
        ws.getWsVariabili().setWkLabel("C0024-CNTRL-43");
        // COB_CODE: INITIALIZE WS-APPO-43-1
        //                      WS-APPO-43-2
        //                      WS-SOMMA
        ws.getWorkCalcoli().setWsAppo431(new AfDecimal(0, 15, 3));
        ws.getWorkCalcoli().setWsAppo432(new AfDecimal(0, 15, 3));
        ws.getWorkCalcoli().setWsSomma(new AfDecimal(0, 15, 3));
        // COB_CODE: PERFORM VARYING IX-REC-10 FROM 1 BY 1
        //             UNTIL IX-REC-10 > ELE-MAX-CACHE-10
        //               END-IF
        //           END-PERFORM.
        ws.getIxIndici().setRec10(((short)1));
        while (!(ws.getIxIndici().getRec10() > ws.getEleMaxCache10())) {
            // COB_CODE: IF REC10-DESC-TP-MOVI(IX-REC-10) =
            //              TP-IMP-SOST
            //                TO WS-SOMMA
            //           END-IF
            if (Conditions.eq(ws.getEleCache10(ws.getIxIndici().getRec10()).getDescTpMovi(), ws.getLrgc0661().getWsDescTpMovi().getImpSost())) {
                // COB_CODE: MOVE 0
                //             TO TMP-IMP-OPER
                ws.getWsVariabili().setTmpImpOper(new AfDecimal(0));
                // COB_CODE: PERFORM CONTROLLO-43
                //              THRU CONTROLLO-43-EX
                controllo43();
                // COB_CODE: MOVE REC10-IMP-OPER(IX-REC-10)
                //             TO TMP-IMP-OPER
                ws.getWsVariabili().setTmpImpOper(ws.getEleCache10(ws.getIxIndici().getRec10()).getImpOper());
                // COB_CODE: MOVE TMP-IMP-OPER
                //             TO WS-APPO-43-1
                ws.getWorkCalcoli().setWsAppo431(Trunc.toDecimal(ws.getWsVariabili().getTmpImpOper(), 15, 3));
                // COB_CODE: ADD WS-APPO-43-1
                //            TO WS-SOMMA
                ws.getWorkCalcoli().setWsSomma(Trunc.toDecimal(abs(ws.getWorkCalcoli().getWsAppo431().add(ws.getWorkCalcoli().getWsSomma())), 15, 3));
            }
            ws.getIxIndici().setRec10(Trunc.toShort(ws.getIxIndici().getRec10() + 1, 4));
        }
        // COB_CODE: IF (WS-SOMMA - WS-APPO-43-2) <= 0,05
        //           AND (WS-SOMMA - WS-APPO-43-2) >= -0,05
        //               CONTINUE
        //           ELSE
        //               PERFORM E001-SCARTO THRU EX-E001
        //           END-IF.
        if (ws.getWorkCalcoli().getWsSomma().subtract(ws.getWorkCalcoli().getWsAppo432()).compareTo("0.05") <= 0 && ws.getWorkCalcoli().getWsSomma().subtract(ws.getWorkCalcoli().getWsAppo432()).compareTo("-0.05") >= 0) {
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE TMP-IMP-OPER
            //             TO WS-CAMPO-1
            ws.getTracciatoFile().setWsCampo1(Trunc.toDecimal(ws.getWsVariabili().getTmpImpOper(), 15, 3));
            // COB_CODE: MOVE ISO-IMPST-SOST
            //             TO WS-CAMPO-2
            ws.getTracciatoFile().setWsCampo2(ws.getImpstSost().getIsoImpstSost().getIsoImpstSost());
            // COB_CODE: MOVE 43 TO WK-CONTROLLO
            ws.getWsVariabili().setWkControllo(43);
            // COB_CODE: SET CNTRL-43    TO TRUE
            ws.getLrgc0661().getMsgErrore().setCntrl43();
            // COB_CODE: MOVE MSG-ERRORE  TO WS-ERRORE
            ws.getWsVariabili().setWsErrore(ws.getLrgc0661().getMsgErrore().getMsgErrore());
            // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
            e001Scarto();
        }
    }

    /**Original name: LETTURA-IMPST-SOST<br>
	 * <pre>*****************************************************************
	 * --> LETTURA TABELLA IMPOSTA SOSTITUTIVA
	 * *****************************************************************</pre>*/
    private void letturaImpstSost() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'LETTURA-IMPST-SOST'       TO WK-LABEL.
        ws.getWsVariabili().setWkLabel("LETTURA-IMPST-SOST");
        // COB_CODE: INITIALIZE LDBV6191
        //                      IMPST-SOST
        //                      IDSI0011-BUFFER-DATI
        //                      IDSI0011-BUFFER-WHERE-COND.
        initLdbv6191();
        initImpstSost();
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferWhereCond("");
        // COB_CODE: SET IDSI0011-SELECT           TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC    TO TRUE
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL   TO TRUE
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
        // COB_CODE: SET IDSI0011-TRATT-X-COMPETENZA
        //                                         TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattXCompetenza();
        // COB_CODE: SET IDSI0011-WHERE-CONDITION  TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setWhereCondition();
        // COB_CODE: SET TROVATO-NO           TO TRUE
        ws.getFlagTrovato().setNo();
        // COB_CODE: MOVE WK-DT-INF                 TO LDBV6191-DT-INF
        ws.getLdbv6191().setInfFormatted(ws.getWsVariabili().getWkDtInfFormatted());
        // COB_CODE: MOVE WK-DT-SUP                 TO LDBV6191-DT-SUP
        ws.getLdbv6191().setSupFormatted(ws.getWsVariabili().getWkDtSupFormatted());
        // COB_CODE: MOVE WK-ID-ADES           TO ISO-ID-OGG
        ws.getImpstSost().getIsoIdOgg().setIsoIdOgg(ws.getWkAreaFile().getIdAdes());
        // COB_CODE: MOVE ZERO
        //              TO IDSI0011-DATA-INIZIO-EFFETTO
        //                 IDSI0011-DATA-FINE-EFFETTO
        //                 IDSI0011-DATA-COMPETENZA
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        // COB_CODE: MOVE 'AD'
        //              TO ISO-TP-OGG
        ws.getImpstSost().setIsoTpOgg("AD");
        // COB_CODE: MOVE 'LDBS6190'              TO IDSI0011-CODICE-STR-DATO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("LDBS6190");
        // COB_CODE: MOVE IMPST-SOST              TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getImpstSost().getImpstSostFormatted());
        // COB_CODE: MOVE LDBV6191                TO IDSI0011-BUFFER-WHERE-COND.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferWhereCond(ws.getLdbv6191().getLdbv6191Formatted());
        // COB_CODE: SET IDSV8888-BUSINESS-DBG           TO TRUE
        ws.getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
        // COB_CODE: MOVE 'LDBS6190'                     TO IDSV8888-NOME-PGM.
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("LDBS6190");
        // COB_CODE: SET  IDSV8888-INIZIO                TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setInizio();
        // COB_CODE: MOVE SPACES                         TO IDSV8888-DESC-PGM
        //skipped translation for moving SPACES to IDSV8888-DESC-PGM; considered in STRING statement translation below
        // COB_CODE: STRING 'Impst_sost'
        //                   DELIMITED BY SIZE
        //                   INTO IDSV8888-DESC-PGM
        //           END-STRING
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("Impst_sost");
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX
        callDispatcher();
        // COB_CODE: SET IDSV8888-BUSINESS-DBG           TO TRUE
        ws.getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
        // COB_CODE: MOVE 'LDBS6190'                     TO IDSV8888-NOME-PGM.
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("LDBS6190");
        // COB_CODE: SET  IDSV8888-FINE                  TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setFine();
        // COB_CODE: MOVE SPACES                         TO IDSV8888-DESC-PGM
        //skipped translation for moving SPACES to IDSV8888-DESC-PGM; considered in STRING statement translation below
        // COB_CODE: STRING 'Impst_sost'
        //                   DELIMITED BY SIZE
        //                   INTO IDSV8888-DESC-PGM
        //           END-STRING
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("Impst_sost");
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //                   END-EVALUATE
        //                ELSE
        //           *-->    ERRORE DISPATCHER
        //                      THRU EX-S0300
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            // COB_CODE:         EVALUATE TRUE
            //                   WHEN IDSO0011-SUCCESSFUL-SQL
            //           * --       OPERAZIONE ESEGUITA CON SUCCESSO
            //                      SET  TROVATO-SI              TO TRUE
            //                   WHEN IDSO0011-NOT-FOUND
            //                        CONTINUE
            //                   WHEN OTHER
            //           * --    ERRORE DB
            //                             THRU EX-S0300
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL:// --       OPERAZIONE ESEGUITA CON SUCCESSO
                    // COB_CODE: MOVE IDSO0011-BUFFER-DATI     TO IMPST-SOST
                    ws.getImpstSost().setImpstSostFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    // COB_CODE: SET  TROVATO-SI              TO TRUE
                    ws.getFlagTrovato().setSi();
                    break;

                case Idso0011SqlcodeSigned.NOT_FOUND:// COB_CODE: CONTINUE
                //continue
                    break;

                default:// --    ERRORE DB
                    // COB_CODE: MOVE WK-PGM
                    //             TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE WK-LABEL-ERR
                    //             TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr(ws.getWsVariabili().getWkLabelErr());
                    // COB_CODE: MOVE '005016'
                    //             TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005016");
                    // COB_CODE: STRING 'LDBS6190-ERRORE DB;'
                    //                IDSO0011-RETURN-CODE ';'
                    //                IDSO0011-SQLCODE
                    //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "LDBS6190-ERRORE DB;", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;
            }
        }
        else {
            //-->    ERRORE DISPATCHER
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL-ERR
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWsVariabili().getWkLabelErr());
            // COB_CODE: MOVE '005016'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING 'LDBS6190-ERRORE DISPATCHER;'
            //                  IDSO0011-RETURN-CODE ';'
            //                  IDSO0011-SQLCODE
            //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "LDBS6190-ERRORE DISPATCHER;", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: C0030-CNTRL-3-12<br>
	 * <pre>----------------------------------------------------------------*
	 *     ESTRATTO CONTO - FASE DIAGNOSTICO
	 *        - CONTROLLI AD HOC MULTIRAMO & UNIT LINKED
	 * ----------------------------------------------------------------*
	 * ----------------------------------------------------------------*
	 *    CONTROLLO 3 12                                               *
	 * ----------------------------------------------------------------*</pre>*/
    private void c0030Cntrl312() {
        // COB_CODE: MOVE 'C0030-CNTRL-3-12'          TO WK-LABEL.
        ws.getWsVariabili().setWkLabel("C0030-CNTRL-3-12");
        // COB_CODE: SET CONTR-3-RV-SI  TO TRUE
        ws.getAreaFlag().getFlagCntrl3Rv().setSi();
        // COB_CODE: SET CONTR-12-RV-SI TO TRUE
        ws.getAreaFlag().getFlagCntrl12Rv().setSi();
        // COB_CODE: IF TP-ESTR-RV
        //              END-PERFORM
        //           END-IF
        if (ws.getTipologiaEc().isRv()) {
            // COB_CODE: SET POLI-580-NO TO TRUE
            ws.getAreaFlag().getFlag580().setNo();
            // COB_CODE: PERFORM RECUPERA-PROD
            //              THRU RECUPERA-PROD-EX
            recuperaProd();
            // COB_CODE: PERFORM VARYING IX-IND FROM 1 BY 1
            //             UNTIL IDSV0001-ESITO-KO
            //                OR IX-IND > ISPC0580-NUM-MAX-PROD
            //                OR CONTR-3-RV-NO
            //                END-IF
            //           END-PERFORM
            ws.getIxIndici().setInd(((short)1));
            while (!(areaIdsv0001.getEsito().isIdsv0001EsitoKo() || ws.getIxIndici().getInd() > ws.getAreaIoIsps0580().getNumMaxProd() || ws.getAreaFlag().getFlagCntrl3Rv().isNo())) {
                // COB_CODE: IF POL-COD-PROD = ISPC0580-TIPO-PRD(IX-IND)
                //              MOVE IX-IND TO IX-580
                //           END-IF
                if (Conditions.eq(ws.getPoli().getPolCodProd(), ws.getAreaIoIsps0580().getTabProd(ws.getIxIndici().getInd()).getTipoPrd())) {
                    // COB_CODE: SET POLI-580-SI TO TRUE
                    ws.getAreaFlag().getFlag580().setSi();
                    // COB_CODE: IF ISPC0580-SOTTORAMO(IX-IND) = 'Ramo V'
                    //           OR (ISPC0580-FORMA(IX-IND) = 'Capitale Differito'
                    //              AND ISPC0580-TIPOPREMIO(IX-IND) = 'Annuo')
                    //           OR (ISPC0580-FORMA(IX-IND) = 'Capitale Differito'
                    //              AND ISPC0580-TIPOPREMIO(IX-IND) = 'Unico')
                    //           OR (ISPC0580-FORMA(IX-IND) = 'Capitale Differito'
                    //               AND ISPC0580-TIPOPREMIO(IX-IND) =
                    //               'Unico Ricorrente'
                    //               AND WK-TASSO-TEC-RV > 0 )
                    //               SET CONTR-3-RV-NO TO TRUE
                    //           END-IF
                    if (Conditions.eq(ws.getAreaIoIsps0580().getTabProd(ws.getIxIndici().getInd()).getSottoramo(), "Ramo V") || Conditions.eq(ws.getAreaIoIsps0580().getTabProd(ws.getIxIndici().getInd()).getForma(), "Capitale Differito") && Conditions.eq(ws.getAreaIoIsps0580().getTabProd(ws.getIxIndici().getInd()).getTipopremio(), "Annuo") || Conditions.eq(ws.getAreaIoIsps0580().getTabProd(ws.getIxIndici().getInd()).getForma(), "Capitale Differito") && Conditions.eq(ws.getAreaIoIsps0580().getTabProd(ws.getIxIndici().getInd()).getTipopremio(), "Unico") || Conditions.eq(ws.getAreaIoIsps0580().getTabProd(ws.getIxIndici().getInd()).getForma(), "Capitale Differito") && Conditions.eq(ws.getAreaIoIsps0580().getTabProd(ws.getIxIndici().getInd()).getTipopremio(), "Unico Ricorrente") && ws.getWorkCalcoli().getWkTassoTecRv().compareTo(0) > 0) {
                        // COB_CODE: SET CONTR-3-RV-NO TO TRUE
                        ws.getAreaFlag().getFlagCntrl3Rv().setNo();
                    }
                    // COB_CODE: MOVE IX-IND TO IX-580
                    ws.getIxIndici().setIx580(ws.getIxIndici().getInd());
                }
                ws.getIxIndici().setInd(Trunc.toShort(ws.getIxIndici().getInd() + 1, 4));
            }
        }
        // COB_CODE: IF TP-ESTR-RV
        //              END-IF
        //           END-IF
        if (ws.getTipologiaEc().isRv()) {
            // COB_CODE: IF POLI-580-SI
            //              END-IF
            //           ELSE
            //              END-IF
            //           END-IF
            if (ws.getAreaFlag().getFlag580().isSi()) {
                // COB_CODE: IF ISPC0580-FORMAPREST(IX-580) = 'Rendita'
                //              SET CONTR-12-RV-NO TO TRUE
                //           END-IF
                if (Conditions.eq(ws.getAreaIoIsps0580().getTabProd(ws.getIxIndici().getIx580()).getFormaprest(), "Rendita")) {
                    // COB_CODE: SET CONTR-12-RV-NO TO TRUE
                    ws.getAreaFlag().getFlagCntrl12Rv().setNo();
                }
            }
            else if (Conditions.eq(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinFlagProdConCedole(), "SI")) {
                // COB_CODE: IF WRIN-FLAG-PROD-CON-CEDOLE = 'SI'
                //              SET CONTR-12-RV-NO TO TRUE
                //           END-IF
                // COB_CODE: SET CONTR-12-RV-NO TO TRUE
                ws.getAreaFlag().getFlagCntrl12Rv().setNo();
            }
        }
        //
        // COB_CODE:      IF CONTR-3-RV-SI
        //           * --> filtrare le polizze TCM dal controllo 3
        //                   END-IF
        //                END-IF
        if (ws.getAreaFlag().getFlagCntrl3Rv().isSi()) {
            // --> filtrare le polizze TCM dal controllo 3
            // COB_CODE: IF TP-ESTR-TCM
            //              CONTINUE
            //           ELSE
            //             END-IF
            //           END-IF
            if (ws.getTipologiaEc().isTcm()) {
            // COB_CODE: CONTINUE
            //continue
            }
            else {
                // COB_CODE: MOVE 0 TO WS-DIFF-2
                ws.getWorkCalcoli().setWsDiff2(Trunc.toDecimal(0, 14, 2));
                // COB_CODE: COMPUTE WS-DIFF-2 = WRIN-CAP-LIQ-DT-RIF-EST-CC
                //                            -  WRIN-PRESTAZIONE-AC-POL
                ws.getWorkCalcoli().setWsDiff2(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinCapLiqDtRifEstCc().subtract(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinPrestazioneAcPol()), 14, 2));
                // --> ripristinata la tolleranza a -0,01 per il controllo 3
                //         IF WS-DIFF-2 < -1
                // COB_CODE:  IF WS-DIFF-2 < -0,01
                //               PERFORM E001-SCARTO THRU EX-E001
                //           END-IF
                if (ws.getWorkCalcoli().getWsDiff2().compareTo("-0.01") < 0) {
                    // COB_CODE: MOVE 3 TO WK-CONTROLLO
                    ws.getWsVariabili().setWkControllo(3);
                    // COB_CODE: MOVE WRIN-CAP-LIQ-DT-RIF-EST-CC TO WS-CAMPO-1
                    ws.getTracciatoFile().setWsCampo1(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinCapLiqDtRifEstCc());
                    // COB_CODE: MOVE WRIN-PRESTAZIONE-AC-POL    TO WS-CAMPO-2
                    ws.getTracciatoFile().setWsCampo2(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinPrestazioneAcPol());
                    // COB_CODE: SET CNTRL-3      TO TRUE
                    ws.getLrgc0661().getMsgErrore().setCntrl3();
                    // COB_CODE: MOVE MSG-ERRORE TO WS-ERRORE
                    ws.getWsVariabili().setWsErrore(ws.getLrgc0661().getMsgErrore().getMsgErrore());
                    // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
                    e001Scarto();
                }
            }
        }
        // COB_CODE: IF CONTR-12-RV-SI
        //             END-IF
        //           END-IF.
        if (ws.getAreaFlag().getFlagCntrl12Rv().isSi()) {
            // COB_CODE: MOVE 0 TO WS-DIFF-2
            ws.getWorkCalcoli().setWsDiff2(Trunc.toDecimal(0, 14, 2));
            // COB_CODE: COMPUTE WS-DIFF-2 = WRIN-PRESTAZIONE-AC-POL
            //                           - WRIN-RISC-TOT-DT-RIF-EST-CC
            ws.getWorkCalcoli().setWsDiff2(Trunc.toDecimal(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinPrestazioneAcPol().subtract(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinRiscTotDtRifEstCc()), 14, 2));
            // --> ripristinata la tolleranza a -0,01 per il controllo 12
            //      IF WS-DIFF-2 < -10
            // COB_CODE: IF WS-DIFF-2 < -0,01
            //              PERFORM E001-SCARTO THRU EX-E001
            //           END-IF
            if (ws.getWorkCalcoli().getWsDiff2().compareTo("-0.01") < 0) {
                // COB_CODE: MOVE 12 TO WK-CONTROLLO
                ws.getWsVariabili().setWkControllo(12);
                // COB_CODE: MOVE WRIN-PRESTAZIONE-AC-POL     TO WS-CAMPO-1
                ws.getTracciatoFile().setWsCampo1(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinPrestazioneAcPol());
                // COB_CODE: MOVE WRIN-RISC-TOT-DT-RIF-EST-CC TO WS-CAMPO-2
                ws.getTracciatoFile().setWsCampo2(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinRiscTotDtRifEstCc());
                // COB_CODE: SET CNTRL-12      TO TRUE
                ws.getLrgc0661().getMsgErrore().setCntrl12();
                // COB_CODE: MOVE MSG-ERRORE TO WS-ERRORE
                ws.getWsVariabili().setWsErrore(ws.getLrgc0661().getMsgErrore().getMsgErrore());
                // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
                e001Scarto();
            }
        }
    }

    /**Original name: C0032-CNTRL-24<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLO 24                                                 *
	 * ----------------------------------------------------------------*</pre>*/
    private void c0032Cntrl24() {
        // COB_CODE: MOVE 'C0032-CNTRL-24'          TO WK-LABEL.
        ws.getWsVariabili().setWkLabel("C0032-CNTRL-24");
        // COB_CODE: MOVE 0 TO WS-SOMMA WS-SOMMA-2
        ws.getWorkCalcoli().setWsSomma(Trunc.toDecimal(0, 15, 3));
        ws.getWorkCalcoli().setWsSomma2(Trunc.toDecimal(0, 15, 3));
        // COB_CODE: MOVE 0 TO WS-DIFF-2
        ws.getWorkCalcoli().setWsDiff2(Trunc.toDecimal(0, 14, 2));
        // COB_CODE:      PERFORM VARYING IX-REC-3 FROM 1 BY 1
        //                  UNTIL IX-REC-3 > ELE-MAX-CACHE-3
        //           *        IF REC3-TP-GAR(IX-REC-3) = 1
        //                    END-IF
        //                END-PERFORM.
        ws.getIxIndici().setRec3(((short)1));
        while (!(ws.getIxIndici().getRec3() > ws.getEleMaxCache3())) {
            //        IF REC3-TP-GAR(IX-REC-3) = 1
            // COB_CODE: IF REC3-TP-GAR(IX-REC-3) = 1 OR 5
            //                    + REC3-PREST-MATUR-AA-RIF(IX-REC-3)
            //           END-IF
            if (ws.getEleCache3(ws.getIxIndici().getRec3()).getTpGar() == 1 || ws.getEleCache3(ws.getIxIndici().getRec3()).getTpGar() == 5) {
                // COB_CODE: COMPUTE WS-SOMMA = WS-SOMMA
                //                 + REC3-CAP-LIQ-GA(IX-REC-3)
                ws.getWorkCalcoli().setWsSomma(Trunc.toDecimal(ws.getWorkCalcoli().getWsSomma().add(ws.getEleCache3(ws.getIxIndici().getRec3()).getCapLiqGa()), 15, 3));
                // COB_CODE: COMPUTE WS-SOMMA-2 = WS-SOMMA-2
                //                 + REC3-PREST-MATUR-AA-RIF(IX-REC-3)
                ws.getWorkCalcoli().setWsSomma2(Trunc.toDecimal(ws.getWorkCalcoli().getWsSomma2().add(ws.getEleCache3(ws.getIxIndici().getRec3()).getPrestMaturAaRif()), 15, 3));
            }
            ws.getIxIndici().setRec3(Trunc.toShort(ws.getIxIndici().getRec3() + 1, 4));
        }
        // COB_CODE: COMPUTE WS-DIFF-2  = WS-SOMMA - WS-SOMMA-2
        ws.getWorkCalcoli().setWsDiff2(Trunc.toDecimal(ws.getWorkCalcoli().getWsSomma().subtract(ws.getWorkCalcoli().getWsSomma2()), 14, 2));
        // --> ripristinata la tolleranza a -0,01 per il controllo 24
        //    IF WS-DIFF-2 < -1
        // COB_CODE: IF WS-DIFF-2 < -0,01
        //              PERFORM E001-SCARTO THRU EX-E001
        //           END-IF.
        if (ws.getWorkCalcoli().getWsDiff2().compareTo("-0.01") < 0) {
            // COB_CODE: MOVE WS-SOMMA   TO WS-CAMPO-1
            ws.getTracciatoFile().setWsCampo1(ws.getWorkCalcoli().getWsSomma());
            // COB_CODE: MOVE WS-SOMMA-2 TO WS-CAMPO-2
            ws.getTracciatoFile().setWsCampo2(ws.getWorkCalcoli().getWsSomma2());
            // COB_CODE: MOVE 24 TO WK-CONTROLLO
            ws.getWsVariabili().setWkControllo(24);
            // COB_CODE: SET CNTRL-24      TO TRUE
            ws.getLrgc0661().getMsgErrore().setCntrl24();
            // COB_CODE: MOVE MSG-ERRORE TO WS-ERRORE
            ws.getWsVariabili().setWsErrore(ws.getLrgc0661().getMsgErrore().getMsgErrore());
            // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
            e001Scarto();
        }
    }

    /**Original name: C0034-CNTRL-35<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLO 35                                                 *
	 * ----------------------------------------------------------------*</pre>*/
    private void c0034Cntrl35() {
        // COB_CODE: MOVE ' C0034-CNTRL-35'          TO WK-LABEL.
        ws.getWsVariabili().setWkLabel(" C0034-CNTRL-35");
        // COB_CODE: MOVE 0 TO WS-SOMMA.
        ws.getWorkCalcoli().setWsSomma(Trunc.toDecimal(0, 15, 3));
        // COB_CODE: MOVE 0 TO WS-CNTRVAL-PREC.
        ws.getWorkCalcoli().setWsCntrvalPrec(Trunc.toDecimal(0, 15, 3));
        // COB_CODE: SET 35-ERRORE-NO TO TRUE
        ws.getAreaFlag().getErrore35().setNo();
        // COB_CODE:      PERFORM VARYING IX-REC-3 FROM 1 BY 1
        //                  UNTIL IX-REC-3 > ELE-MAX-CACHE-3
        //                     OR 35-ERRORE-SI
        //           *
        //                    END-IF
        //           *
        //                END-PERFORM.
        ws.getIxIndici().setRec3(((short)1));
        while (!(ws.getIxIndici().getRec3() > ws.getEleMaxCache3() || ws.getAreaFlag().getErrore35().isSi())) {
            //
            // COB_CODE: MOVE 0 TO WS-SOMMA
            ws.getWorkCalcoli().setWsSomma(Trunc.toDecimal(0, 15, 3));
            // COB_CODE: MOVE 0 TO WS-CNTRVAL-PREC
            ws.getWorkCalcoli().setWsCntrvalPrec(Trunc.toDecimal(0, 15, 3));
            //
            // COB_CODE:           IF REC3-TP-INVST(IX-REC-3) = 7
            //           *
            //                              + REC3-PREST-MATUR-EC-PREC(IX-REC-3)
            //                     END-IF
            if (ws.getEleCache3(ws.getIxIndici().getRec3()).getTpInvst() == 7) {
                //
                // COB_CODE: PERFORM VARYING IX-C35-TAB FROM 1 BY 1
                //              UNTIL IX-C35-TAB > IX-MAX-C35
                //              END-IF
                //           END-PERFORM
                ws.getIxIndici().setC35Tab(((short)1));
                while (!(ws.getIxIndici().getC35Tab() > ws.getIxMaxC35())) {
                    // COB_CODE: IF REC3-ID-GAR(IX-REC-3) = C35-ID-GAR(IX-C35-TAB)
                    //                      WS-CNTRVAL-PREC
                    //           END-IF
                    if (ws.getEleCache3(ws.getIxIndici().getRec3()).getIdGar() == ws.getEleCache35(ws.getIxIndici().getC35Tab()).getIdGar()) {
                        // COB_CODE: COMPUTE WS-CNTRVAL-PREC =
                        //                   C35-CNTRVAL-PREC(IX-C35-TAB) +
                        //                   WS-CNTRVAL-PREC
                        ws.getWorkCalcoli().setWsCntrvalPrec(Trunc.toDecimal(ws.getEleCache35(ws.getIxIndici().getC35Tab()).getCntrvalPrec().add(ws.getWorkCalcoli().getWsCntrvalPrec()), 15, 3));
                    }
                    ws.getIxIndici().setC35Tab(Trunc.toShort(ws.getIxIndici().getC35Tab() + 1, 4));
                }
                //
                // COB_CODE: COMPUTE WS-SOMMA = WS-SOMMA
                //                 + REC3-PREST-MATUR-EC-PREC(IX-REC-3)
                ws.getWorkCalcoli().setWsSomma(Trunc.toDecimal(ws.getWorkCalcoli().getWsSomma().add(ws.getEleCache3(ws.getIxIndici().getRec3()).getPrestMaturEcPrec()), 15, 3));
            }
            // --> ripristinata la tolleranza a -0,01/+0,01 per il controllo 35
            //        IF (WS-SOMMA - WS-CNTRVAL-PREC) > +10
            //        OR (WS-SOMMA - WS-CNTRVAL-PREC) < -10
            // COB_CODE: IF (WS-SOMMA - WS-CNTRVAL-PREC) > +0,01
            //           OR (WS-SOMMA - WS-CNTRVAL-PREC) < -0,01
            //               PERFORM E001-SCARTO THRU EX-E001
            //           END-IF
            if (ws.getWorkCalcoli().getWsSomma().subtract(ws.getWorkCalcoli().getWsCntrvalPrec()).compareTo(new AfDecimal("0.01", 3, 2)) > 0 || ws.getWorkCalcoli().getWsSomma().subtract(ws.getWorkCalcoli().getWsCntrvalPrec()).compareTo("-0.01") < 0) {
                // COB_CODE: SET 35-ERRORE-SI TO TRUE
                ws.getAreaFlag().getErrore35().setSi();
                // COB_CODE: MOVE 35 TO WK-CONTROLLO
                ws.getWsVariabili().setWkControllo(35);
                // COB_CODE: MOVE WS-SOMMA          TO WS-CAMPO-1
                ws.getTracciatoFile().setWsCampo1(ws.getWorkCalcoli().getWsSomma());
                // COB_CODE: MOVE WS-CNTRVAL-PREC   TO WS-CAMPO-2
                ws.getTracciatoFile().setWsCampo2(ws.getWorkCalcoli().getWsCntrvalPrec());
                // COB_CODE: SET CNTRL-35      TO TRUE
                ws.getLrgc0661().getMsgErrore().setCntrl35();
                // COB_CODE: MOVE MSG-ERRORE TO WS-ERRORE
                ws.getWsVariabili().setWsErrore(ws.getLrgc0661().getMsgErrore().getMsgErrore());
                // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
                e001Scarto();
            }
            //
            ws.getIxIndici().setRec3(Trunc.toShort(ws.getIxIndici().getRec3() + 1, 4));
        }
    }

    /**Original name: C0036-CNTRL-36<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLO 36                                                 *
	 * ----------------------------------------------------------------*</pre>*/
    private void c0036Cntrl36() {
        // COB_CODE: MOVE ' C0036-CNTRL-36'          TO WK-LABEL.
        ws.getWsVariabili().setWkLabel(" C0036-CNTRL-36");
        // COB_CODE: MOVE 0 TO WS-SOMMA.
        ws.getWorkCalcoli().setWsSomma(Trunc.toDecimal(0, 15, 3));
        // COB_CODE: PERFORM VARYING IX-REC-3 FROM 1 BY 1
        //             UNTIL IX-REC-3 > ELE-MAX-CACHE-3
        //               END-IF
        //           END-PERFORM.
        ws.getIxIndici().setRec3(((short)1));
        while (!(ws.getIxIndici().getRec3() > ws.getEleMaxCache3())) {
            // COB_CODE: IF REC3-TP-INVST(IX-REC-3) = 7
            //                    + REC3-PREST-MATUR-AA-RIF(IX-REC-3)
            //           END-IF
            if (ws.getEleCache3(ws.getIxIndici().getRec3()).getTpInvst() == 7) {
                // COB_CODE: COMPUTE WS-SOMMA = WS-SOMMA
                //                 + REC3-PREST-MATUR-AA-RIF(IX-REC-3)
                ws.getWorkCalcoli().setWsSomma(Trunc.toDecimal(ws.getWorkCalcoli().getWsSomma().add(ws.getEleCache3(ws.getIxIndici().getRec3()).getPrestMaturAaRif()), 15, 3));
            }
            ws.getIxIndici().setRec3(Trunc.toShort(ws.getIxIndici().getRec3() + 1, 4));
        }
        // COB_CODE: IF (WS-SOMMA - REC8-CNTRVAL-ATTUALE) > 0,01
        //           OR (WS-SOMMA - REC8-CNTRVAL-ATTUALE) < -0,01
        //              PERFORM E001-SCARTO THRU EX-E001
        //           END-IF.
        if (ws.getWorkCalcoli().getWsSomma().subtract(ws.getAreaCache8().getCntrvalAttuale()).compareTo("0.01") > 0 || ws.getWorkCalcoli().getWsSomma().subtract(ws.getAreaCache8().getCntrvalAttuale()).compareTo("-0.01") < 0) {
            // COB_CODE: MOVE 36 TO WK-CONTROLLO
            ws.getWsVariabili().setWkControllo(36);
            // COB_CODE: MOVE WS-SOMMA             TO WS-CAMPO-1
            ws.getTracciatoFile().setWsCampo1(ws.getWorkCalcoli().getWsSomma());
            // COB_CODE: MOVE REC8-CNTRVAL-ATTUALE TO WS-CAMPO-2
            ws.getTracciatoFile().setWsCampo2(ws.getAreaCache8().getCntrvalAttuale());
            // COB_CODE: SET CNTRL-36      TO TRUE
            ws.getLrgc0661().getMsgErrore().setCntrl36();
            // COB_CODE: MOVE MSG-ERRORE TO WS-ERRORE
            ws.getWsVariabili().setWsErrore(ws.getLrgc0661().getMsgErrore().getMsgErrore());
            // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
            e001Scarto();
        }
    }

    /**Original name: C0038-CNTRL-37<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLO 37                                                 *
	 * ----------------------------------------------------------------*</pre>*/
    private void c0038Cntrl37() {
        // COB_CODE: MOVE 'C0038-CNTRL-37'          TO WK-LABEL.
        ws.getWsVariabili().setWkLabel("C0038-CNTRL-37");
        //
        // COB_CODE: SET CNTRL-37-ERR-NO TO TRUE
        ws.getAreaFlag().getFlagCntrl37().setNo();
        // COB_CODE: PERFORM VARYING IX-C37-TAB FROM 1 BY 1
        //                   UNTIL IX-C37-TAB > IX-MAX-C37
        //                      OR CNTRL-37-ERR-SI
        //              END-IF
        //           END-PERFORM.
        ws.getIxIndici().setC37Tab(((short)1));
        while (!(ws.getIxIndici().getC37Tab() > ws.getIxMaxC37() || ws.getAreaFlag().getFlagCntrl37().isSi())) {
            // COB_CODE:  COMPUTE WK-CNTRL-37 =
            //              (C37-NUM-QUO-PREC(IX-C37-TAB) -
            //           C37-NUM-QUO-DISINV-RISC-PARZ(IX-C37-TAB) -
            //           C37-NUM-QUO-DIS-RISC-PARZ-PRG(IX-C37-TAB) -
            //           C37-NUM-QUO-DISINV-IMPOS-SOST(IX-C37-TAB) -
            //           C37-NUM-QUO-DISINV-COMMGES(IX-C37-TAB) -
            //           C37-NUM-QUO-DISINV-PREL-COSTI(IX-C37-TAB) -
            //           C37-NUM-QUO-DISINV-SWITCH(IX-C37-TAB) -
            //           C37-NUM-QUO-DISINV-COMP-NAV(IX-C37-TAB) +
            //           C37-NUM-QUO-INVST-SWITCH(IX-C37-TAB) +
            //           C37-NUM-QUO-INVST-VERS(IX-C37-TAB) +
            //           C37-NUM-QUO-INVST-REBATE(IX-C37-TAB)  +
            //           C37-NUM-QUO-INVST-PURO-RISC(IX-C37-TAB) +
            //           C37-NUM-QUO-INVST-COMP-NAV(IX-C37-TAB)) -
            //           C37-NUM-QUO-ATTUALE(IX-C37-TAB)
            ws.getWsVariabili().setWkCntrl37(Trunc.toDecimal(ws.getEleCache37(ws.getIxIndici().getC37Tab()).getNumQuoPrec().subtract(ws.getEleCache37(ws.getIxIndici().getC37Tab()).getNumQuoDisinvRiscParz()).subtract(ws.getEleCache37(ws.getIxIndici().getC37Tab()).getNumQuoDisRiscParzPrg()).subtract(ws.getEleCache37(ws.getIxIndici().getC37Tab()).getNumQuoDisinvImposSost()).subtract(ws.getEleCache37(ws.getIxIndici().getC37Tab()).getNumQuoDisinvCommges()).subtract(ws.getEleCache37(ws.getIxIndici().getC37Tab()).getNumQuoDisinvPrelCosti()).subtract(ws.getEleCache37(ws.getIxIndici().getC37Tab()).getNumQuoDisinvSwitch()).subtract(ws.getEleCache37(ws.getIxIndici().getC37Tab()).getNumQuoDisinvCompNav()).add(ws.getEleCache37(ws.getIxIndici().getC37Tab()).getNumQuoInvstSwitch()).add(ws.getEleCache37(ws.getIxIndici().getC37Tab()).getNumQuoInvstVers()).add(ws.getEleCache37(ws.getIxIndici().getC37Tab()).getNumQuoInvstRebate()).add(ws.getEleCache37(ws.getIxIndici().getC37Tab()).getNumQuoInvstPuroRisc()).add(ws.getEleCache37(ws.getIxIndici().getC37Tab()).getNumQuoInvstCompNav()).subtract(ws.getEleCache37(ws.getIxIndici().getC37Tab()).getNumQuoAttuale()), 15, 3));
            // COB_CODE: IF WK-CNTRL-37 > 0,01
            //           OR WK-CNTRL-37 < -0,01
            //              SET CNTRL-37-ERR-SI TO TRUE
            //           END-IF
            if (ws.getWsVariabili().getWkCntrl37().compareTo("0.01") > 0 || ws.getWsVariabili().getWkCntrl37().compareTo("-0.01") < 0) {
                // COB_CODE: MOVE WK-CNTRL-37
                //              TO WS-CAMPO-1
                ws.getTracciatoFile().setWsCampo1(ws.getWsVariabili().getWkCntrl37());
                // COB_CODE: MOVE C37-NUM-QUO-PREC(IX-C37-TAB)
                //             TO WS-CAMPO-2
                ws.getTracciatoFile().setWsCampo2(ws.getEleCache37(ws.getIxIndici().getC37Tab()).getNumQuoPrec());
                // COB_CODE: MOVE C37-NUM-QUO-ATTUALE(IX-C37-TAB)
                //             TO WS-CAMPO-3
                ws.getTracciatoFile().setWsCampo3(ws.getEleCache37(ws.getIxIndici().getC37Tab()).getNumQuoAttuale());
                // COB_CODE: MOVE 37 TO WK-CONTROLLO
                ws.getWsVariabili().setWkControllo(37);
                // COB_CODE: SET CNTRL-37        TO TRUE
                ws.getLrgc0661().getMsgErrore().setCntrl37();
                // COB_CODE: MOVE MSG-ERRORE     TO WS-ERRORE
                ws.getWsVariabili().setWsErrore(ws.getLrgc0661().getMsgErrore().getMsgErrore());
                // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
                e001Scarto();
                // COB_CODE: SET CNTRL-37-ERR-SI TO TRUE
                ws.getAreaFlag().getFlagCntrl37().setSi();
            }
            ws.getIxIndici().setC37Tab(Trunc.toShort(ws.getIxIndici().getC37Tab() + 1, 4));
        }
    }

    /**Original name: C0040-CNTRL-44<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLO 44                                                 *
	 * ----------------------------------------------------------------*</pre>*/
    private void c0040Cntrl44() {
        // COB_CODE: MOVE 'C0040-CNTRL-44'          TO WK-LABEL.
        ws.getWsVariabili().setWkLabel("C0040-CNTRL-44");
        // COB_CODE: MOVE 0 TO WS-SOMMA-3
        ws.getWorkCalcoli().setWsSomma3(Trunc.toDecimal(0, 12, 5));
        // COB_CODE: MOVE 0 TO WS-DIFF-3
        ws.getWorkCalcoli().setWsDiff3(Trunc.toDecimal(0, 12, 5));
        // COB_CODE: PERFORM VARYING IX-REC-10 FROM 1 BY 1
        //             UNTIL IX-REC-10 > ELE-MAX-CACHE-10
        //               END-IF
        //           END-PERFORM.
        ws.getIxIndici().setRec10(((short)1));
        while (!(ws.getIxIndici().getRec10() > ws.getEleMaxCache10())) {
            // COB_CODE: IF REC10-DESC-TP-MOVI(IX-REC-10) =
            //              TP-COMM-GEST
            //                    + REC10-NUMERO-QUO(IX-REC-10)
            //           END-IF
            if (Conditions.eq(ws.getEleCache10(ws.getIxIndici().getRec10()).getDescTpMovi(), ws.getLrgc0661().getWsDescTpMovi().getCommGest())) {
                // COB_CODE: COMPUTE WS-SOMMA-3 = WS-SOMMA-3
                //                 + REC10-NUMERO-QUO(IX-REC-10)
                ws.getWorkCalcoli().setWsSomma3(Trunc.toDecimal(ws.getWorkCalcoli().getWsSomma3().add(ws.getEleCache10(ws.getIxIndici().getRec10()).getNumeroQuo()), 12, 5));
            }
            ws.getIxIndici().setRec10(Trunc.toShort(ws.getIxIndici().getRec10() + 1, 4));
        }
        // COB_CODE: COMPUTE WS-DIFF-3 = REC8-NUM-QUO-DISINV-COMMGES
        //                           - WS-SOMMA-3
        ws.getWorkCalcoli().setWsDiff3(Trunc.toDecimal(ws.getAreaCache8().getNumQuoDisinvCommges().subtract(ws.getWorkCalcoli().getWsSomma3()), 12, 5));
        // COB_CODE: IF  WS-DIFF-3 > -0,05 AND WS-DIFF-3 < 0,05
        //              CONTINUE
        //           ELSE
        //              PERFORM E001-SCARTO THRU EX-E001
        //           END-IF.
        if (ws.getWorkCalcoli().getWsDiff3().compareTo("-0.05") > 0 && ws.getWorkCalcoli().getWsDiff3().compareTo("0.05") < 0) {
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE WS-SOMMA-3                  TO WS-CAMPO-1
            ws.getTracciatoFile().setWsCampo1(ws.getWorkCalcoli().getWsSomma3());
            // COB_CODE: MOVE REC8-NUM-QUO-DISINV-COMMGES TO WS-CAMPO-2
            ws.getTracciatoFile().setWsCampo2(ws.getAreaCache8().getNumQuoDisinvCommges());
            // COB_CODE: MOVE 44 TO WK-CONTROLLO
            ws.getWsVariabili().setWkControllo(44);
            // COB_CODE: SET CNTRL-44     TO TRUE
            ws.getLrgc0661().getMsgErrore().setCntrl44();
            // COB_CODE: MOVE MSG-ERRORE TO WS-ERRORE
            ws.getWsVariabili().setWsErrore(ws.getLrgc0661().getMsgErrore().getMsgErrore());
            // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
            e001Scarto();
        }
    }

    /**Original name: C0042-CNTRL-45<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLO 45                                                 *
	 * ----------------------------------------------------------------*</pre>*/
    private void c0042Cntrl45() {
        // COB_CODE: MOVE 'C0042-CNTRL-45'          TO WK-LABEL.
        ws.getWsVariabili().setWkLabel("C0042-CNTRL-45");
        // COB_CODE: MOVE 0 TO WS-SOMMA-3
        ws.getWorkCalcoli().setWsSomma3(Trunc.toDecimal(0, 12, 5));
        // COB_CODE: PERFORM VARYING IX-REC-10 FROM 1 BY 1
        //             UNTIL IX-REC-10 > ELE-MAX-CACHE-10
        //               END-IF
        //           END-PERFORM.
        ws.getIxIndici().setRec10(((short)1));
        while (!(ws.getIxIndici().getRec10() > ws.getEleMaxCache10())) {
            // COB_CODE: IF REC10-DESC-TP-MOVI(IX-REC-10) =
            //              TP-CASO-MORTE
            //                    + REC10-NUMERO-QUO(IX-REC-10)
            //           END-IF
            if (Conditions.eq(ws.getEleCache10(ws.getIxIndici().getRec10()).getDescTpMovi(), ws.getLrgc0661().getWsDescTpMovi().getCasoMorte())) {
                // COB_CODE: COMPUTE WS-SOMMA-3 = WS-SOMMA-3
                //                 + REC10-NUMERO-QUO(IX-REC-10)
                ws.getWorkCalcoli().setWsSomma3(Trunc.toDecimal(ws.getWorkCalcoli().getWsSomma3().add(ws.getEleCache10(ws.getIxIndici().getRec10()).getNumeroQuo()), 12, 5));
            }
            ws.getIxIndici().setRec10(Trunc.toShort(ws.getIxIndici().getRec10() + 1, 4));
        }
        // COB_CODE: COMPUTE WS-DIFF-3 = REC8-NUM-QUO-DISINV-PREL-COSTI
        //                           - WS-SOMMA-3
        ws.getWorkCalcoli().setWsDiff3(Trunc.toDecimal(ws.getAreaCache8().getNumQuoDisinvPrelCosti().subtract(ws.getWorkCalcoli().getWsSomma3()), 12, 5));
        // COB_CODE: IF  WS-DIFF-3 > -0,05 AND WS-DIFF-3 < 0,05
        //              CONTINUE
        //           ELSE
        //              PERFORM E001-SCARTO THRU EX-E001
        //           END-IF.
        if (ws.getWorkCalcoli().getWsDiff3().compareTo("-0.05") > 0 && ws.getWorkCalcoli().getWsDiff3().compareTo("0.05") < 0) {
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE WS-SOMMA-3                     TO WS-CAMPO-1
            ws.getTracciatoFile().setWsCampo1(ws.getWorkCalcoli().getWsSomma3());
            // COB_CODE: MOVE REC8-NUM-QUO-DISINV-PREL-COSTI TO WS-CAMPO-2
            ws.getTracciatoFile().setWsCampo2(ws.getAreaCache8().getNumQuoDisinvPrelCosti());
            // COB_CODE: MOVE 45 TO WK-CONTROLLO
            ws.getWsVariabili().setWkControllo(45);
            // COB_CODE: SET CNTRL-45     TO TRUE
            ws.getLrgc0661().getMsgErrore().setCntrl45();
            // COB_CODE: MOVE MSG-ERRORE TO WS-ERRORE
            ws.getWsVariabili().setWsErrore(ws.getLrgc0661().getMsgErrore().getMsgErrore());
            // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
            e001Scarto();
        }
    }

    /**Original name: C0044-CNTRL-46<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLO 46                                                 *
	 * ----------------------------------------------------------------*</pre>*/
    private void c0044Cntrl46() {
        // COB_CODE: MOVE 'C0044-CNTRL-46'          TO WK-LABEL.
        ws.getWsVariabili().setWkLabel("C0044-CNTRL-46");
        // COB_CODE: MOVE 0 TO WS-SOMMA-3
        ws.getWorkCalcoli().setWsSomma3(Trunc.toDecimal(0, 12, 5));
        // COB_CODE: PERFORM VARYING IX-REC-10 FROM 1 BY 1
        //             UNTIL IX-REC-10 > ELE-MAX-CACHE-10
        //               END-IF
        //           END-PERFORM.
        ws.getIxIndici().setRec10(((short)1));
        while (!(ws.getIxIndici().getRec10() > ws.getEleMaxCache10())) {
            // COB_CODE: IF REC10-DESC-TP-MOVI(IX-REC-10) =
            //              TP-REBATE
            //                    + REC10-NUMERO-QUO(IX-REC-10)
            //           END-IF
            if (Conditions.eq(ws.getEleCache10(ws.getIxIndici().getRec10()).getDescTpMovi(), ws.getLrgc0661().getWsDescTpMovi().getRebate())) {
                // COB_CODE: COMPUTE WS-SOMMA-3 = WS-SOMMA-3
                //                 + REC10-NUMERO-QUO(IX-REC-10)
                ws.getWorkCalcoli().setWsSomma3(Trunc.toDecimal(ws.getWorkCalcoli().getWsSomma3().add(ws.getEleCache10(ws.getIxIndici().getRec10()).getNumeroQuo()), 12, 5));
            }
            ws.getIxIndici().setRec10(Trunc.toShort(ws.getIxIndici().getRec10() + 1, 4));
        }
        // COB_CODE: IF  REC8-NUM-QUO-INVST-REBATE
        //               = WS-SOMMA-3
        //              CONTINUE
        //           ELSE
        //              PERFORM E001-SCARTO THRU EX-E001
        //           END-IF.
        if (ws.getAreaCache8().getNumQuoInvstRebate().compareTo(ws.getWorkCalcoli().getWsSomma3()) == 0) {
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE WS-SOMMA-3                     TO WS-CAMPO-1
            ws.getTracciatoFile().setWsCampo1(ws.getWorkCalcoli().getWsSomma3());
            // COB_CODE: MOVE REC8-NUM-QUO-INVST-REBATE      TO WS-CAMPO-2
            ws.getTracciatoFile().setWsCampo2(ws.getAreaCache8().getNumQuoInvstRebate());
            // COB_CODE: MOVE 46 TO WK-CONTROLLO
            ws.getWsVariabili().setWkControllo(46);
            // COB_CODE: SET CNTRL-46     TO TRUE
            ws.getLrgc0661().getMsgErrore().setCntrl46();
            // COB_CODE: MOVE MSG-ERRORE TO WS-ERRORE
            ws.getWsVariabili().setWsErrore(ws.getLrgc0661().getMsgErrore().getMsgErrore());
            // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
            e001Scarto();
        }
    }

    /**Original name: C0046-CNTRL-47<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLO 47                                                 *
	 * ----------------------------------------------------------------*</pre>*/
    private void c0046Cntrl47() {
        // COB_CODE: MOVE 'C0046-CNTRL-47'          TO WK-LABEL.
        ws.getWsVariabili().setWkLabel("C0046-CNTRL-47");
        // COB_CODE: MOVE 0 TO WS-SOMMA-3
        ws.getWorkCalcoli().setWsSomma3(Trunc.toDecimal(0, 12, 5));
        // COB_CODE: PERFORM VARYING IX-REC-10 FROM 1 BY 1
        //             UNTIL IX-REC-10 > ELE-MAX-CACHE-10
        //               END-IF
        //           END-PERFORM.
        ws.getIxIndici().setRec10(((short)1));
        while (!(ws.getIxIndici().getRec10() > ws.getEleMaxCache10())) {
            // COB_CODE: IF REC10-DESC-TP-MOVI(IX-REC-10) =
            //              TP-VERS-PREMI
            //                    + REC10-NUMERO-QUO(IX-REC-10)
            //           END-IF
            if (Conditions.eq(ws.getEleCache10(ws.getIxIndici().getRec10()).getDescTpMovi(), ws.getLrgc0661().getWsDescTpMovi().getVersPremi())) {
                // COB_CODE: COMPUTE WS-SOMMA-3 = WS-SOMMA-3
                //                 + REC10-NUMERO-QUO(IX-REC-10)
                ws.getWorkCalcoli().setWsSomma3(Trunc.toDecimal(ws.getWorkCalcoli().getWsSomma3().add(ws.getEleCache10(ws.getIxIndici().getRec10()).getNumeroQuo()), 12, 5));
            }
            ws.getIxIndici().setRec10(Trunc.toShort(ws.getIxIndici().getRec10() + 1, 4));
        }
        // COB_CODE: IF  REC8-NUM-QUO-INVST-VERS
        //               = WS-SOMMA-3
        //              CONTINUE
        //           ELSE
        //              PERFORM E001-SCARTO THRU EX-E001
        //           END-IF.
        if (ws.getAreaCache8().getNumQuoInvstVers().compareTo(ws.getWorkCalcoli().getWsSomma3()) == 0) {
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE WS-SOMMA-3                     TO WS-CAMPO-1
            ws.getTracciatoFile().setWsCampo1(ws.getWorkCalcoli().getWsSomma3());
            // COB_CODE: MOVE REC8-NUM-QUO-INVST-VERS        TO WS-CAMPO-2
            ws.getTracciatoFile().setWsCampo2(ws.getAreaCache8().getNumQuoInvstVers());
            // COB_CODE: MOVE 47 TO WK-CONTROLLO
            ws.getWsVariabili().setWkControllo(47);
            // COB_CODE: SET CNTRL-47     TO TRUE
            ws.getLrgc0661().getMsgErrore().setCntrl47();
            // COB_CODE: MOVE MSG-ERRORE TO WS-ERRORE
            ws.getWsVariabili().setWsErrore(ws.getLrgc0661().getMsgErrore().getMsgErrore());
            // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
            e001Scarto();
        }
    }

    /**Original name: C0048-CNTRL-48<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLO 48                                                 *
	 * ----------------------------------------------------------------*</pre>*/
    private void c0048Cntrl48() {
        // COB_CODE: MOVE 'C0048-CNTRL-48'          TO WK-LABEL.
        ws.getWsVariabili().setWkLabel("C0048-CNTRL-48");
        // COB_CODE: MOVE 0 TO WS-SOMMA-3
        ws.getWorkCalcoli().setWsSomma3(Trunc.toDecimal(0, 12, 5));
        // COB_CODE: PERFORM VARYING IX-REC-10 FROM 1 BY 1
        //             UNTIL IX-REC-10 > ELE-MAX-CACHE-10
        //               END-IF
        //           END-PERFORM.
        ws.getIxIndici().setRec10(((short)1));
        while (!(ws.getIxIndici().getRec10() > ws.getEleMaxCache10())) {
            // COB_CODE: IF REC10-DESC-TP-MOVI(IX-REC-10) =
            //              TP-SW-OUT-1 OR TP-SW-OUT-2 OR
            //              TP-SW-OUT-3 OR TP-SW-OUT-4 OR
            //              TP-SW-OUT-5 OR TP-SW-OUT-6 OR
            //              TP-SW-OUT-7 OR TP-SW-OUT-8 OR
            //              TP-OUT-SW-PASSO-PASSO OR
            //              TP-OUT-SW-STOP-LOSS   OR
            //              TP-OUT-SW-LINEA OR
            //              TP-OUT-SW-BATCH-PARZ-MASS OR
            //              TP-OUT-SW-RIBIL           OR
            //              TP-OUT-SW-GAP-EVENT OR
            //              TP-OUT-SW-SUP-SOGLIA OR
            //              TP-OUT-SW-MASSIVO-FNZ
            //                  + REC10-NUMERO-QUO(IX-REC-10)
            //           END-IF
            if (Conditions.eq(ws.getEleCache10(ws.getIxIndici().getRec10()).getDescTpMovi(), ws.getLrgc0661().getWsDescTpMovi().getSwOut1()) || Conditions.eq(ws.getEleCache10(ws.getIxIndici().getRec10()).getDescTpMovi(), ws.getLrgc0661().getWsDescTpMovi().getSwOut2()) || Conditions.eq(ws.getEleCache10(ws.getIxIndici().getRec10()).getDescTpMovi(), ws.getLrgc0661().getWsDescTpMovi().getSwOut3()) || Conditions.eq(ws.getEleCache10(ws.getIxIndici().getRec10()).getDescTpMovi(), ws.getLrgc0661().getWsDescTpMovi().getSwOut4()) || Conditions.eq(ws.getEleCache10(ws.getIxIndici().getRec10()).getDescTpMovi(), ws.getLrgc0661().getWsDescTpMovi().getSwOut5()) || Conditions.eq(ws.getEleCache10(ws.getIxIndici().getRec10()).getDescTpMovi(), ws.getLrgc0661().getWsDescTpMovi().getSwOut6()) || Conditions.eq(ws.getEleCache10(ws.getIxIndici().getRec10()).getDescTpMovi(), ws.getLrgc0661().getWsDescTpMovi().getSwOut7()) || Conditions.eq(ws.getEleCache10(ws.getIxIndici().getRec10()).getDescTpMovi(), ws.getLrgc0661().getWsDescTpMovi().getSwOut8()) || Conditions.eq(ws.getEleCache10(ws.getIxIndici().getRec10()).getDescTpMovi(), ws.getLrgc0661().getWsDescTpMovi().getOutSwPassoPasso()) || Conditions.eq(ws.getEleCache10(ws.getIxIndici().getRec10()).getDescTpMovi(), ws.getLrgc0661().getWsDescTpMovi().getOutSwStopLoss()) || Conditions.eq(ws.getEleCache10(ws.getIxIndici().getRec10()).getDescTpMovi(), ws.getLrgc0661().getWsDescTpMovi().getOutSwLinea()) || Conditions.eq(ws.getEleCache10(ws.getIxIndici().getRec10()).getDescTpMovi(), ws.getLrgc0661().getWsDescTpMovi().getOutSwBatchParzMass()) || Conditions.eq(ws.getEleCache10(ws.getIxIndici().getRec10()).getDescTpMovi(), ws.getLrgc0661().getWsDescTpMovi().getOutSwRibil()) || Conditions.eq(ws.getEleCache10(ws.getIxIndici().getRec10()).getDescTpMovi(), ws.getLrgc0661().getWsDescTpMovi().getOutSwGapEvent()) || Conditions.eq(ws.getEleCache10(ws.getIxIndici().getRec10()).getDescTpMovi(), ws.getLrgc0661().getWsDescTpMovi().getOutSwSupSoglia()) || Conditions.eq(ws.getEleCache10(ws.getIxIndici().getRec10()).getDescTpMovi(), ws.getLrgc0661().getWsDescTpMovi().getOutSwMassivoFnz())) {
                // COB_CODE: COMPUTE WS-SOMMA-3 = WS-SOMMA-3
                //               + REC10-NUMERO-QUO(IX-REC-10)
                ws.getWorkCalcoli().setWsSomma3(Trunc.toDecimal(ws.getWorkCalcoli().getWsSomma3().add(ws.getEleCache10(ws.getIxIndici().getRec10()).getNumeroQuo()), 12, 5));
            }
            ws.getIxIndici().setRec10(Trunc.toShort(ws.getIxIndici().getRec10() + 1, 4));
        }
        // COB_CODE: IF  REC8-NUM-QUO-DISINV-SWITCH
        //               = WS-SOMMA-3
        //              CONTINUE
        //           ELSE
        //              PERFORM E001-SCARTO THRU EX-E001
        //           END-IF.
        if (ws.getAreaCache8().getNumQuoDisinvSwitch().compareTo(ws.getWorkCalcoli().getWsSomma3()) == 0) {
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE WS-SOMMA-3                     TO WS-CAMPO-1
            ws.getTracciatoFile().setWsCampo1(ws.getWorkCalcoli().getWsSomma3());
            // COB_CODE: MOVE REC8-NUM-QUO-DISINV-SWITCH     TO WS-CAMPO-2
            ws.getTracciatoFile().setWsCampo2(ws.getAreaCache8().getNumQuoDisinvSwitch());
            // COB_CODE: MOVE 48 TO WK-CONTROLLO
            ws.getWsVariabili().setWkControllo(48);
            // COB_CODE: SET CNTRL-48     TO TRUE
            ws.getLrgc0661().getMsgErrore().setCntrl48();
            // COB_CODE: MOVE MSG-ERRORE TO WS-ERRORE
            ws.getWsVariabili().setWsErrore(ws.getLrgc0661().getMsgErrore().getMsgErrore());
            // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
            e001Scarto();
        }
    }

    /**Original name: C0050-CNTRL-49<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLO 49                                                 *
	 * ----------------------------------------------------------------*</pre>*/
    private void c0050Cntrl49() {
        // COB_CODE: MOVE 'C0050-CNTRL-49'          TO WK-LABEL.
        ws.getWsVariabili().setWkLabel("C0050-CNTRL-49");
        // COB_CODE: MOVE 0 TO WS-SOMMA-3
        ws.getWorkCalcoli().setWsSomma3(Trunc.toDecimal(0, 12, 5));
        // COB_CODE: PERFORM VARYING IX-REC-10 FROM 1 BY 1
        //             UNTIL IX-REC-10 > ELE-MAX-CACHE-10
        //               END-IF
        //           END-PERFORM.
        ws.getIxIndici().setRec10(((short)1));
        while (!(ws.getIxIndici().getRec10() > ws.getEleMaxCache10())) {
            // COB_CODE: IF REC10-DESC-TP-MOVI(IX-REC-10) =
            //              TP-SW-IN-1 OR TP-SW-IN-2 OR
            //              TP-SW-IN-3 OR TP-SW-IN-4 OR
            //              TP-SW-IN-5 OR TP-SW-IN-6 OR
            //              TP-SW-IN-7 OR TP-SW-IN-8 OR
            //              TP-IN-SW-PASSO-PASSO     OR
            //              TP-IN-SW-STOP-LOSS       OR
            //              TP-IN-SW-LINEA OR
            //              TP-IN-SW-BATCH-PARZ-MASS OR
            //              TP-IN-SW-RIBIL  OR
            //              TP-IN-SW-GAP-EVENT OR
            //              TP-IN-SW-SUP-SOGLIA OR
            //              TP-IN-SW-MASSIVO-FNZ
            //                  + REC10-NUMERO-QUO(IX-REC-10)
            //           END-IF
            if (Conditions.eq(ws.getEleCache10(ws.getIxIndici().getRec10()).getDescTpMovi(), ws.getLrgc0661().getWsDescTpMovi().getSwIn1()) || Conditions.eq(ws.getEleCache10(ws.getIxIndici().getRec10()).getDescTpMovi(), ws.getLrgc0661().getWsDescTpMovi().getSwIn2()) || Conditions.eq(ws.getEleCache10(ws.getIxIndici().getRec10()).getDescTpMovi(), ws.getLrgc0661().getWsDescTpMovi().getSwIn3()) || Conditions.eq(ws.getEleCache10(ws.getIxIndici().getRec10()).getDescTpMovi(), ws.getLrgc0661().getWsDescTpMovi().getSwIn4()) || Conditions.eq(ws.getEleCache10(ws.getIxIndici().getRec10()).getDescTpMovi(), ws.getLrgc0661().getWsDescTpMovi().getSwIn5()) || Conditions.eq(ws.getEleCache10(ws.getIxIndici().getRec10()).getDescTpMovi(), ws.getLrgc0661().getWsDescTpMovi().getSwIn6()) || Conditions.eq(ws.getEleCache10(ws.getIxIndici().getRec10()).getDescTpMovi(), ws.getLrgc0661().getWsDescTpMovi().getSwIn7()) || Conditions.eq(ws.getEleCache10(ws.getIxIndici().getRec10()).getDescTpMovi(), ws.getLrgc0661().getWsDescTpMovi().getSwIn8()) || Conditions.eq(ws.getEleCache10(ws.getIxIndici().getRec10()).getDescTpMovi(), ws.getLrgc0661().getWsDescTpMovi().getInSwPassoPasso()) || Conditions.eq(ws.getEleCache10(ws.getIxIndici().getRec10()).getDescTpMovi(), ws.getLrgc0661().getWsDescTpMovi().getInSwStopLoss()) || Conditions.eq(ws.getEleCache10(ws.getIxIndici().getRec10()).getDescTpMovi(), ws.getLrgc0661().getWsDescTpMovi().getInSwLinea()) || Conditions.eq(ws.getEleCache10(ws.getIxIndici().getRec10()).getDescTpMovi(), ws.getLrgc0661().getWsDescTpMovi().getInSwBatchParzMass()) || Conditions.eq(ws.getEleCache10(ws.getIxIndici().getRec10()).getDescTpMovi(), ws.getLrgc0661().getWsDescTpMovi().getInSwRibil()) || Conditions.eq(ws.getEleCache10(ws.getIxIndici().getRec10()).getDescTpMovi(), ws.getLrgc0661().getWsDescTpMovi().getInSwGapEvent()) || Conditions.eq(ws.getEleCache10(ws.getIxIndici().getRec10()).getDescTpMovi(), ws.getLrgc0661().getWsDescTpMovi().getInSwSupSoglia()) || Conditions.eq(ws.getEleCache10(ws.getIxIndici().getRec10()).getDescTpMovi(), ws.getLrgc0661().getWsDescTpMovi().getInSwMassivoFnz())) {
                // COB_CODE: COMPUTE WS-SOMMA-3 = WS-SOMMA-3
                //               + REC10-NUMERO-QUO(IX-REC-10)
                ws.getWorkCalcoli().setWsSomma3(Trunc.toDecimal(ws.getWorkCalcoli().getWsSomma3().add(ws.getEleCache10(ws.getIxIndici().getRec10()).getNumeroQuo()), 12, 5));
            }
            ws.getIxIndici().setRec10(Trunc.toShort(ws.getIxIndici().getRec10() + 1, 4));
        }
        // COB_CODE: IF  REC8-NUM-QUO-INV-SWITCH
        //               = WS-SOMMA-3
        //              CONTINUE
        //           ELSE
        //              PERFORM E001-SCARTO THRU EX-E001
        //           END-IF.
        if (ws.getAreaCache8().getNumQuoInvSwitch().compareTo(ws.getWorkCalcoli().getWsSomma3()) == 0) {
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE WS-SOMMA-3                     TO WS-CAMPO-1
            ws.getTracciatoFile().setWsCampo1(ws.getWorkCalcoli().getWsSomma3());
            // COB_CODE: MOVE REC8-NUM-QUO-INV-SWITCH        TO WS-CAMPO-2
            ws.getTracciatoFile().setWsCampo2(ws.getAreaCache8().getNumQuoInvSwitch());
            // COB_CODE: MOVE 49 TO WK-CONTROLLO
            ws.getWsVariabili().setWkControllo(49);
            // COB_CODE: SET CNTRL-49     TO TRUE
            ws.getLrgc0661().getMsgErrore().setCntrl49();
            // COB_CODE: MOVE MSG-ERRORE TO WS-ERRORE
            ws.getWsVariabili().setWsErrore(ws.getLrgc0661().getMsgErrore().getMsgErrore());
            // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
            e001Scarto();
        }
    }

    /**Original name: C0052-CNTRL-50<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLO 50                                                 *
	 * ----------------------------------------------------------------*</pre>*/
    private void c0052Cntrl50() {
        // COB_CODE: MOVE 'C0052-CNTRL-50'          TO WK-LABEL.
        ws.getWsVariabili().setWkLabel("C0052-CNTRL-50");
        // COB_CODE: MOVE 0 TO WS-SOMMA-3
        ws.getWorkCalcoli().setWsSomma3(Trunc.toDecimal(0, 12, 5));
        // COB_CODE: MOVE 0 TO WS-DIFF-3
        ws.getWorkCalcoli().setWsDiff3(Trunc.toDecimal(0, 12, 5));
        // COB_CODE: PERFORM VARYING IX-REC-10 FROM 1 BY 1
        //             UNTIL IX-REC-10 > ELE-MAX-CACHE-10
        //               END-IF
        //           END-PERFORM.
        ws.getIxIndici().setRec10(((short)1));
        while (!(ws.getIxIndici().getRec10() > ws.getEleMaxCache10())) {
            // COB_CODE: IF REC10-DESC-TP-MOVI(IX-REC-10) =
            //              TP-COMP-NAV-NEG
            //                    + REC10-NUMERO-QUO(IX-REC-10)
            //           END-IF
            if (Conditions.eq(ws.getEleCache10(ws.getIxIndici().getRec10()).getDescTpMovi(), ws.getLrgc0661().getWsDescTpMovi().getCompNavNeg())) {
                // COB_CODE: COMPUTE WS-SOMMA-3 = WS-SOMMA-3
                //                 + REC10-NUMERO-QUO(IX-REC-10)
                ws.getWorkCalcoli().setWsSomma3(Trunc.toDecimal(ws.getWorkCalcoli().getWsSomma3().add(ws.getEleCache10(ws.getIxIndici().getRec10()).getNumeroQuo()), 12, 5));
            }
            ws.getIxIndici().setRec10(Trunc.toShort(ws.getIxIndici().getRec10() + 1, 4));
        }
        // COB_CODE: COMPUTE WS-DIFF-3 = REC8-NUM-QUO-DISINV-COMP-NAV
        //                           - WS-SOMMA-3
        ws.getWorkCalcoli().setWsDiff3(Trunc.toDecimal(ws.getAreaCache8().getNumQuoDisinvCompNav().subtract(ws.getWorkCalcoli().getWsSomma3()), 12, 5));
        // COB_CODE: IF  WS-DIFF-3 > -0,05 AND WS-DIFF-3 < 0,05
        //              CONTINUE
        //           ELSE
        //              PERFORM E001-SCARTO THRU EX-E001
        //           END-IF.
        if (ws.getWorkCalcoli().getWsDiff3().compareTo("-0.05") > 0 && ws.getWorkCalcoli().getWsDiff3().compareTo("0.05") < 0) {
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE WS-SOMMA-3                   TO WS-CAMPO-1
            ws.getTracciatoFile().setWsCampo1(ws.getWorkCalcoli().getWsSomma3());
            // COB_CODE: MOVE REC8-NUM-QUO-DISINV-COMP-NAV TO WS-CAMPO-2
            ws.getTracciatoFile().setWsCampo2(ws.getAreaCache8().getNumQuoDisinvCompNav());
            // COB_CODE: MOVE 50 TO WK-CONTROLLO
            ws.getWsVariabili().setWkControllo(50);
            // COB_CODE: SET CNTRL-50     TO TRUE
            ws.getLrgc0661().getMsgErrore().setCntrl50();
            // COB_CODE: MOVE MSG-ERRORE TO WS-ERRORE
            ws.getWsVariabili().setWsErrore(ws.getLrgc0661().getMsgErrore().getMsgErrore());
            // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
            e001Scarto();
        }
    }

    /**Original name: C0054-CNTRL-51<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLO 51                                                 *
	 * ----------------------------------------------------------------*</pre>*/
    private void c0054Cntrl51() {
        // COB_CODE: MOVE 'C0054-CNTRL-51'          TO WK-LABEL.
        ws.getWsVariabili().setWkLabel("C0054-CNTRL-51");
        // COB_CODE: MOVE 0 TO WS-SOMMA-3
        ws.getWorkCalcoli().setWsSomma3(Trunc.toDecimal(0, 12, 5));
        // COB_CODE: PERFORM VARYING IX-REC-10 FROM 1 BY 1
        //             UNTIL IX-REC-10 > ELE-MAX-CACHE-10
        //               END-IF
        //           END-PERFORM.
        ws.getIxIndici().setRec10(((short)1));
        while (!(ws.getIxIndici().getRec10() > ws.getEleMaxCache10())) {
            // COB_CODE: IF REC10-DESC-TP-MOVI(IX-REC-10) =
            //              TP-COMP-NAV-POS
            //                    + REC10-NUMERO-QUO(IX-REC-10)
            //           END-IF
            if (Conditions.eq(ws.getEleCache10(ws.getIxIndici().getRec10()).getDescTpMovi(), ws.getLrgc0661().getWsDescTpMovi().getCompNavPos())) {
                // COB_CODE: COMPUTE WS-SOMMA-3 = WS-SOMMA-3
                //                 + REC10-NUMERO-QUO(IX-REC-10)
                ws.getWorkCalcoli().setWsSomma3(Trunc.toDecimal(ws.getWorkCalcoli().getWsSomma3().add(ws.getEleCache10(ws.getIxIndici().getRec10()).getNumeroQuo()), 12, 5));
            }
            ws.getIxIndici().setRec10(Trunc.toShort(ws.getIxIndici().getRec10() + 1, 4));
        }
        // COB_CODE: IF  REC8-NUM-QUO-INVST-COMP-NAV
        //               = WS-SOMMA-3
        //              CONTINUE
        //           ELSE
        //              PERFORM E001-SCARTO THRU EX-E001
        //           END-IF.
        if (ws.getAreaCache8().getNumQuoInvstCompNav().compareTo(ws.getWorkCalcoli().getWsSomma3()) == 0) {
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE WS-SOMMA-3                     TO WS-CAMPO-1
            ws.getTracciatoFile().setWsCampo1(ws.getWorkCalcoli().getWsSomma3());
            // COB_CODE: MOVE REC8-NUM-QUO-INVST-COMP-NAV    TO WS-CAMPO-2
            ws.getTracciatoFile().setWsCampo2(ws.getAreaCache8().getNumQuoInvstCompNav());
            // COB_CODE: MOVE 51 TO WK-CONTROLLO
            ws.getWsVariabili().setWkControllo(51);
            // COB_CODE: SET CNTRL-51     TO TRUE
            ws.getLrgc0661().getMsgErrore().setCntrl51();
            // COB_CODE: MOVE MSG-ERRORE TO WS-ERRORE
            ws.getWsVariabili().setWsErrore(ws.getLrgc0661().getMsgErrore().getMsgErrore());
            // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
            e001Scarto();
        }
    }

    /**Original name: RECUPERA-PROD<br>
	 * <pre>----------------------------------------------------------------*
	 *    LETTURA POLIZZA PER RECUPERARE IL CODICE PRODOTTO            *
	 * ----------------------------------------------------------------*</pre>*/
    private void recuperaProd() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'RECUPERA-PROD'          TO WK-LABEL.
        ws.getWsVariabili().setWkLabel("RECUPERA-PROD");
        //
        // COB_CODE: INITIALIZE POLI
        //                      IDSI0011-BUFFER-DATI.
        initPoli();
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
        // COB_CODE: MOVE ZEROES        TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                 IDSI0011-DATA-FINE-EFFETTO
        //                                 IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        // COB_CODE: SET IDSI0011-ID                TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setIdsi0011Id();
        // COB_CODE: SET IDSI0011-SELECT            TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        // COB_CODE: SET IDSI0011-TRATT-DEFAULT     TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setIdsi0011TrattDefault();
        //
        // COB_CODE: MOVE WRIN-ID-POLI               TO POL-ID-POLI.
        ws.getPoli().setPolIdPoli(ws.getWrinRecOutput().getWrinTpRecordDati().getWrinIdPoli());
        //
        // COB_CODE: MOVE 'POLI'                    TO IDSI0011-CODICE-STR-DATO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("POLI");
        //
        // COB_CODE: MOVE POLI                      TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getPoli().getPoliFormatted());
        //
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX
        callDispatcher();
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //                   END-EVALUATE
        //                ELSE
        //           *-->    ERRORE DISPATCHER
        //                   PERFORM S0300-RICERCA-GRAVITA-ERRORE THRU EX-S0300
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            // COB_CODE:         EVALUATE TRUE
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //                            MOVE IDSO0011-BUFFER-DATI TO POLI
            //           *
            //                       WHEN OTHER
            //                                      THRU EX-S0300
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL:// COB_CODE: MOVE IDSO0011-BUFFER-DATI TO POLI
                    ws.getPoli().setPoliFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    //
                    break;

                default:// COB_CODE: MOVE WK-PGM         TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE WK-LABEL       TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr(ws.getWsVariabili().getWkLabel());
                    // COB_CODE: MOVE '005016'       TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005016");
                    // COB_CODE: STRING 'POLI'       ';'
                    //                  IDSO0011-RETURN-CODE ';'
                    //                  IDSO0011-SQLCODE
                    //                  DELIMITED BY SIZE
                    //                  INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "POLI", ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //                   THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;
            }
        }
        else {
            //-->    ERRORE DISPATCHER
            // COB_CODE: MOVE WK-PGM                TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL              TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWsVariabili().getWkLabel());
            // COB_CODE: MOVE '005016'              TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING 'POLI       '        ';'
            //                  IDSO0011-RETURN-CODE ';'
            //                  IDSO0011-SQLCODE
            //                  DELIMITED BY   SIZE INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "POLI       ", ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: C0060-CNTRL-34<br>
	 * <pre>----------------------------------------------------------------*
	 *     ESTRATTO CONTO - FASE DIAGNOSTICO
	 *        - CONTROLLI AD HOC MULTIRAMO
	 * ----------------------------------------------------------------*
	 * ----------------------------------------------------------------*
	 *    CONTROLLO 34                                                 *
	 * ----------------------------------------------------------------*</pre>*/
    private void c0060Cntrl34() {
        // COB_CODE: MOVE 'C0060-CNTRL-34'          TO WK-LABEL.
        ws.getWsVariabili().setWkLabel("C0060-CNTRL-34");
        // COB_CODE: IF CNTRL-34-ERR-NO
        //               CONTINUE
        //           ELSE
        //               END-IF
        //           END-IF.
        if (ws.getAreaFlag().getFlagCntrl34().isNo()) {
        // COB_CODE: CONTINUE
        //continue
        }
        else if (ws.getAreaFlag().getFlagCntrlEsec34().isSi()) {
            // COB_CODE: IF  CNTRL-34-SI
            //           PERFORM E001-SCARTO THRU EX-E001
            //           END-IF
            // COB_CODE: MOVE 34 TO WK-CONTROLLO
            ws.getWsVariabili().setWkControllo(34);
            // COB_CODE: SET CNTRL-34        TO TRUE
            ws.getLrgc0661().getMsgErrore().setCntrl34();
            // COB_CODE: MOVE MSG-ERRORE     TO WS-ERRORE
            ws.getWsVariabili().setWsErrore(ws.getLrgc0661().getMsgErrore().getMsgErrore());
            // COB_CODE: PERFORM E001-SCARTO THRU EX-E001
            e001Scarto();
        }
    }

    /**Original name: VALORIZZA-OUTPUT-P85<br>
	 * <pre>----------------------------------------------------------------*
	 *     COPY TAB ESTR CNT DIAGN
	 * ----------------------------------------------------------------*
	 * ------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    VALORIZZA OUTPUT COPY LCCVP853
	 *    ULTIMO AGG. 26 SET 2014
	 * ------------------------------------------------------------</pre>*/
    private void valorizzaOutputP85() {
        // COB_CODE: MOVE P85-COD-COMP-ANIA
        //             TO (SF)-COD-COMP-ANIA(IX-TAB-P85)
        ws.getWp84TabP85(ws.getIxIndici().getTabP85()).getLccvp851().getDati().setWp85CodCompAnia(ws.getEstrCntDiagnRiv().getP85CodCompAnia());
        // COB_CODE: MOVE P85-ID-POLI
        //             TO (SF)-ID-POLI(IX-TAB-P85)
        ws.getWp84TabP85(ws.getIxIndici().getTabP85()).getLccvp851().getDati().setWp85IdPoli(ws.getEstrCntDiagnRiv().getP85IdPoli());
        // COB_CODE: MOVE P85-DT-RIVAL
        //             TO (SF)-DT-RIVAL(IX-TAB-P85)
        ws.getWp84TabP85(ws.getIxIndici().getTabP85()).getLccvp851().getDati().setWp85DtRival(ws.getEstrCntDiagnRiv().getP85DtRival());
        // COB_CODE: MOVE P85-COD-TARI
        //             TO (SF)-COD-TARI(IX-TAB-P85)
        ws.getWp84TabP85(ws.getIxIndici().getTabP85()).getLccvp851().getDati().setWp85CodTari(ws.getEstrCntDiagnRiv().getP85CodTari());
        // COB_CODE: IF P85-RENDTO-LRD-NULL = HIGH-VALUES
        //                TO (SF)-RENDTO-LRD-NULL(IX-TAB-P85)
        //           ELSE
        //                TO (SF)-RENDTO-LRD(IX-TAB-P85)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getEstrCntDiagnRiv().getP85RendtoLrd().getP85RendtoLrdNullFormatted())) {
            // COB_CODE: MOVE P85-RENDTO-LRD-NULL
            //             TO (SF)-RENDTO-LRD-NULL(IX-TAB-P85)
            ws.getWp84TabP85(ws.getIxIndici().getTabP85()).getLccvp851().getDati().getWp85RendtoLrd().setWp85RendtoLrdNull(ws.getEstrCntDiagnRiv().getP85RendtoLrd().getP85RendtoLrdNull());
        }
        else {
            // COB_CODE: MOVE P85-RENDTO-LRD
            //             TO (SF)-RENDTO-LRD(IX-TAB-P85)
            ws.getWp84TabP85(ws.getIxIndici().getTabP85()).getLccvp851().getDati().getWp85RendtoLrd().setWp85RendtoLrd(Trunc.toDecimal(ws.getEstrCntDiagnRiv().getP85RendtoLrd().getP85RendtoLrd(), 14, 9));
        }
        // COB_CODE: IF P85-PC-RETR-NULL = HIGH-VALUES
        //                TO (SF)-PC-RETR-NULL(IX-TAB-P85)
        //           ELSE
        //                TO (SF)-PC-RETR(IX-TAB-P85)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getEstrCntDiagnRiv().getP85PcRetr().getP85PcRetrNullFormatted())) {
            // COB_CODE: MOVE P85-PC-RETR-NULL
            //             TO (SF)-PC-RETR-NULL(IX-TAB-P85)
            ws.getWp84TabP85(ws.getIxIndici().getTabP85()).getLccvp851().getDati().getWp85PcRetr().setWp85PcRetrNull(ws.getEstrCntDiagnRiv().getP85PcRetr().getP85PcRetrNull());
        }
        else {
            // COB_CODE: MOVE P85-PC-RETR
            //             TO (SF)-PC-RETR(IX-TAB-P85)
            ws.getWp84TabP85(ws.getIxIndici().getTabP85()).getLccvp851().getDati().getWp85PcRetr().setWp85PcRetr(Trunc.toDecimal(ws.getEstrCntDiagnRiv().getP85PcRetr().getP85PcRetr(), 6, 3));
        }
        // COB_CODE: IF P85-RENDTO-RETR-NULL = HIGH-VALUES
        //                TO (SF)-RENDTO-RETR-NULL(IX-TAB-P85)
        //           ELSE
        //                TO (SF)-RENDTO-RETR(IX-TAB-P85)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getEstrCntDiagnRiv().getP85RendtoRetr().getP85RendtoRetrNullFormatted())) {
            // COB_CODE: MOVE P85-RENDTO-RETR-NULL
            //             TO (SF)-RENDTO-RETR-NULL(IX-TAB-P85)
            ws.getWp84TabP85(ws.getIxIndici().getTabP85()).getLccvp851().getDati().getWp85RendtoRetr().setWp85RendtoRetrNull(ws.getEstrCntDiagnRiv().getP85RendtoRetr().getP85RendtoRetrNull());
        }
        else {
            // COB_CODE: MOVE P85-RENDTO-RETR
            //             TO (SF)-RENDTO-RETR(IX-TAB-P85)
            ws.getWp84TabP85(ws.getIxIndici().getTabP85()).getLccvp851().getDati().getWp85RendtoRetr().setWp85RendtoRetr(Trunc.toDecimal(ws.getEstrCntDiagnRiv().getP85RendtoRetr().getP85RendtoRetr(), 14, 9));
        }
        // COB_CODE: IF P85-COMMIS-GEST-NULL = HIGH-VALUES
        //                TO (SF)-COMMIS-GEST-NULL(IX-TAB-P85)
        //           ELSE
        //                TO (SF)-COMMIS-GEST(IX-TAB-P85)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getEstrCntDiagnRiv().getP85CommisGest().getP85CommisGestNullFormatted())) {
            // COB_CODE: MOVE P85-COMMIS-GEST-NULL
            //             TO (SF)-COMMIS-GEST-NULL(IX-TAB-P85)
            ws.getWp84TabP85(ws.getIxIndici().getTabP85()).getLccvp851().getDati().getWp85CommisGest().setWp85CommisGestNull(ws.getEstrCntDiagnRiv().getP85CommisGest().getP85CommisGestNull());
        }
        else {
            // COB_CODE: MOVE P85-COMMIS-GEST
            //             TO (SF)-COMMIS-GEST(IX-TAB-P85)
            ws.getWp84TabP85(ws.getIxIndici().getTabP85()).getLccvp851().getDati().getWp85CommisGest().setWp85CommisGest(Trunc.toDecimal(ws.getEstrCntDiagnRiv().getP85CommisGest().getP85CommisGest(), 15, 3));
        }
        // COB_CODE: IF P85-TS-RIVAL-NET-NULL = HIGH-VALUES
        //                TO (SF)-TS-RIVAL-NET-NULL(IX-TAB-P85)
        //           ELSE
        //                TO (SF)-TS-RIVAL-NET(IX-TAB-P85)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getEstrCntDiagnRiv().getP85TsRivalNet().getP85TsRivalNetNullFormatted())) {
            // COB_CODE: MOVE P85-TS-RIVAL-NET-NULL
            //             TO (SF)-TS-RIVAL-NET-NULL(IX-TAB-P85)
            ws.getWp84TabP85(ws.getIxIndici().getTabP85()).getLccvp851().getDati().getWp85TsRivalNet().setWp85TsRivalNetNull(ws.getEstrCntDiagnRiv().getP85TsRivalNet().getP85TsRivalNetNull());
        }
        else {
            // COB_CODE: MOVE P85-TS-RIVAL-NET
            //             TO (SF)-TS-RIVAL-NET(IX-TAB-P85)
            ws.getWp84TabP85(ws.getIxIndici().getTabP85()).getLccvp851().getDati().getWp85TsRivalNet().setWp85TsRivalNet(Trunc.toDecimal(ws.getEstrCntDiagnRiv().getP85TsRivalNet().getP85TsRivalNet(), 14, 9));
        }
        // COB_CODE: IF P85-MIN-GARTO-NULL = HIGH-VALUES
        //                TO (SF)-MIN-GARTO-NULL(IX-TAB-P85)
        //           ELSE
        //                TO (SF)-MIN-GARTO(IX-TAB-P85)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getEstrCntDiagnRiv().getP85MinGarto().getP85MinGartoNullFormatted())) {
            // COB_CODE: MOVE P85-MIN-GARTO-NULL
            //             TO (SF)-MIN-GARTO-NULL(IX-TAB-P85)
            ws.getWp84TabP85(ws.getIxIndici().getTabP85()).getLccvp851().getDati().getWp85MinGarto().setWp85MinGartoNull(ws.getEstrCntDiagnRiv().getP85MinGarto().getP85MinGartoNull());
        }
        else {
            // COB_CODE: MOVE P85-MIN-GARTO
            //             TO (SF)-MIN-GARTO(IX-TAB-P85)
            ws.getWp84TabP85(ws.getIxIndici().getTabP85()).getLccvp851().getDati().getWp85MinGarto().setWp85MinGarto(Trunc.toDecimal(ws.getEstrCntDiagnRiv().getP85MinGarto().getP85MinGarto(), 14, 9));
        }
        // COB_CODE: IF P85-MIN-TRNUT-NULL = HIGH-VALUES
        //                TO (SF)-MIN-TRNUT-NULL(IX-TAB-P85)
        //           ELSE
        //                TO (SF)-MIN-TRNUT(IX-TAB-P85)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getEstrCntDiagnRiv().getP85MinTrnut().getP85MinTrnutNullFormatted())) {
            // COB_CODE: MOVE P85-MIN-TRNUT-NULL
            //             TO (SF)-MIN-TRNUT-NULL(IX-TAB-P85)
            ws.getWp84TabP85(ws.getIxIndici().getTabP85()).getLccvp851().getDati().getWp85MinTrnut().setWp85MinTrnutNull(ws.getEstrCntDiagnRiv().getP85MinTrnut().getP85MinTrnutNull());
        }
        else {
            // COB_CODE: MOVE P85-MIN-TRNUT
            //             TO (SF)-MIN-TRNUT(IX-TAB-P85)
            ws.getWp84TabP85(ws.getIxIndici().getTabP85()).getLccvp851().getDati().getWp85MinTrnut().setWp85MinTrnut(Trunc.toDecimal(ws.getEstrCntDiagnRiv().getP85MinTrnut().getP85MinTrnut(), 14, 9));
        }
        // COB_CODE: MOVE P85-IB-POLI
        //             TO (SF)-IB-POLI(IX-TAB-P85)
        ws.getWp84TabP85(ws.getIxIndici().getTabP85()).getLccvp851().getDati().setWp85IbPoli(ws.getEstrCntDiagnRiv().getP85IbPoli());
        // COB_CODE: MOVE P85-DS-OPER-SQL
        //             TO (SF)-DS-OPER-SQL(IX-TAB-P85)
        ws.getWp84TabP85(ws.getIxIndici().getTabP85()).getLccvp851().getDati().setWp85DsOperSql(ws.getEstrCntDiagnRiv().getP85DsOperSql());
        // COB_CODE: MOVE P85-DS-VER
        //             TO (SF)-DS-VER(IX-TAB-P85)
        ws.getWp84TabP85(ws.getIxIndici().getTabP85()).getLccvp851().getDati().setWp85DsVer(ws.getEstrCntDiagnRiv().getP85DsVer());
        // COB_CODE: MOVE P85-DS-TS-CPTZ
        //             TO (SF)-DS-TS-CPTZ(IX-TAB-P85)
        ws.getWp84TabP85(ws.getIxIndici().getTabP85()).getLccvp851().getDati().setWp85DsTsCptz(ws.getEstrCntDiagnRiv().getP85DsTsCptz());
        // COB_CODE: MOVE P85-DS-UTENTE
        //             TO (SF)-DS-UTENTE(IX-TAB-P85)
        ws.getWp84TabP85(ws.getIxIndici().getTabP85()).getLccvp851().getDati().setWp85DsUtente(ws.getEstrCntDiagnRiv().getP85DsUtente());
        // COB_CODE: MOVE P85-DS-STATO-ELAB
        //             TO (SF)-DS-STATO-ELAB(IX-TAB-P85).
        ws.getWp84TabP85(ws.getIxIndici().getTabP85()).getLccvp851().getDati().setWp85DsStatoElab(ws.getEstrCntDiagnRiv().getP85DsStatoElab());
    }

    @Override
    public DdCard[] getDefaultDdCards() {
        return new DdCard[] {new DdCard("FILESQS5", 21), new DdCard("FILESQS6", 2500), new DdCard("FILESQS1", 2500), new DdCard("FILESQS2", 49), new DdCard("FILESQS3", 282), new DdCard("FILESQS4", 500)};
    }

    public void initTracciatoFile() {
        ws.getTracciatoFile().setOutIbOgg("");
        ws.getTracciatoFile().setFiller1(Types.SPACE_CHAR);
        ws.getTracciatoFile().setOutErr("");
        ws.getTracciatoFile().setFiller2(Types.SPACE_CHAR);
        ws.getTracciatoFile().setWsCampo1(new AfDecimal(0));
        ws.getTracciatoFile().setFiller3(Types.SPACE_CHAR);
        ws.getTracciatoFile().setWsCampo2(new AfDecimal(0));
        ws.getTracciatoFile().setFiller4(Types.SPACE_CHAR);
        ws.getTracciatoFile().setWsCampo3(new AfDecimal(0));
        ws.getTracciatoFile().setFiller7(Types.SPACE_CHAR);
        ws.getTracciatoFile().setWsData1("");
        ws.getTracciatoFile().setFiller5(Types.SPACE_CHAR);
        ws.getTracciatoFile().setWsData2("");
        ws.getTracciatoFile().setFiller6(Types.SPACE_CHAR);
        ws.getTracciatoFile().setWsData3("");
    }

    public void initIxIndici() {
        ws.getIxIndici().setInd(((short)0));
        ws.getIxIndici().setRec5Formatted("0000");
        ws.getIxIndici().setRec8Formatted("0000");
        ws.getIxIndici().setRec3(((short)0));
        ws.getIxIndici().setRec10(((short)0));
        ws.getIxIndici().setRec9(((short)0));
        ws.getIxIndici().setIndP84Formatted("0000");
        ws.getIxIndici().setTabP85(((short)0));
        ws.getIxIndici().setCntr34(((short)0));
        ws.getIxIndici().setIx580(((short)0));
        ws.getIxIndici().setGrav(((short)0));
        ws.getIxIndici().setC37Tab(((short)0));
        ws.getIxIndici().setC35Tab(((short)0));
        ws.getIxIndici().setRec9TpMovi(((short)0));
    }

    public void initWsVariabili() {
        ws.getWsVariabili().setRec2CognomeAss("");
        ws.getWsVariabili().setRec2NomeAss("");
        ws.getWsVariabili().setWkTabella("");
        ws.getWsVariabili().setWkLabel("");
        ws.getWsVariabili().setWsErrore("");
        ws.getWsVariabili().setWkLabelErr("");
        ws.getWsVariabili().setTmpImpOper(new AfDecimal(0));
        ws.getWsVariabili().setWkCntrl37(new AfDecimal(0, 15, 3));
        ws.getWsVariabili().setWkRcode("");
        ws.getWsVariabili().setWkDataInFormatted("00000000");
        ws.getWsVariabili().setWkDataOutFormatted("00000000");
        ws.getWsVariabili().setWkControlloFormatted("00000000");
        ws.getWsVariabili().setWkFrazFormatted("00000");
        ws.getWsVariabili().setWkDtDecor("");
        ws.getWsVariabili().setWkDtDecorAaFormatted("0000");
        ws.getWsVariabili().setWkDtInfFormatted("00000000");
        ws.getWsVariabili().setWkDtSupFormatted("00000000");
        ws.getWsVariabili().setRec9ImpLordoRiscParz(new AfDecimal(0, 15, 3));
    }

    public void initAreaCache8() {
        ws.getAreaCache8().setNumQuoDisinvCommges(new AfDecimal(0, 12, 5));
        ws.getAreaCache8().setCntrvalPrec(new AfDecimal(0, 15, 3));
        ws.getAreaCache8().setCntrvalAttuale(new AfDecimal(0, 15, 3));
        ws.getAreaCache8().setNumQuoInvSwitch(new AfDecimal(0, 12, 5));
        ws.getAreaCache8().setNumQuoInvstVers(new AfDecimal(0, 12, 5));
        ws.getAreaCache8().setNumQuoInvstRebate(new AfDecimal(0, 12, 5));
        ws.getAreaCache8().setNumQuoDisinvSwitch(new AfDecimal(0, 12, 5));
        ws.getAreaCache8().setNumQuoDisinvPrelCosti(new AfDecimal(0, 12, 5));
        ws.getAreaCache8().setNumQuoDisinvCompNav(new AfDecimal(0, 12, 5));
        ws.getAreaCache8().setNumQuoInvstCompNav(new AfDecimal(0, 12, 5));
    }

    public void initAreaCache9() {
        ws.setEleMaxCache9Formatted("000000000");
        for (int idx0 = 1; idx0 <= Lrgs0660Data.ELE_CACHE9_MAXOCCURS; idx0++) {
            ws.getEleCache9(idx0).setImpLrdRiscParz(new AfDecimal(0, 15, 3));
            ws.getEleCache9(idx0).setRpTpMoviFormatted("00000");
            ws.getEleCache9(idx0).setDtEffettoLiq("");
        }
    }

    public void initAreaCache10() {
        ws.setEleMaxCache10Formatted("000000000");
        for (int idx0 = 1; idx0 <= Lrgs0660Data.ELE_CACHE10_MAXOCCURS; idx0++) {
            ws.getEleCache10(idx0).setIdGarFormatted("000000000");
            ws.getEleCache10(idx0).setDescTpMovi("");
            ws.getEleCache10(idx0).setTpMoviFormatted("00000");
            ws.getEleCache10(idx0).setImpOper(new AfDecimal(0, 15, 3));
            ws.getEleCache10(idx0).setImpVers(new AfDecimal(0, 15, 3));
            ws.getEleCache10(idx0).setNumeroQuo(new AfDecimal(0, 12, 5));
            ws.getEleCache10(idx0).setDtEffMovi("");
        }
    }

    public void initWrinRecFilein() {
        ws.getWrinRecOutput().setWrinMacrofunzionalita("");
        ws.getWrinRecOutput().setWrinFunzionalita("");
        ws.getWrinRecOutput().setWrinCodCompAniaFormatted("00000");
        ws.getWrinRecOutput().setWrinIbPoliInd("");
        ws.getWrinRecOutput().setWrinIbPoliCol("");
        ws.getWrinRecOutput().setWrinIbAdes("");
        ws.getWrinRecOutput().setWrinIbGar("");
        ws.getWrinRecOutput().setWrinIbTrchDiGar("");
        ws.getWrinRecOutput().setWrinTpInvst("");
        ws.getWrinRecOutput().setWrinTpRecord("");
        ws.getWrinRecOutput().getWrinTpRecordDati().setWrinTpRecordDati("");
    }

    public void initDateWork() {
        ws.getDateWork().getWkDtRicorDaX().setWkDtRicorDaX("");
        ws.getDateWork().getWkDtRicorAX().setWkDtRicorAX("");
        ws.getDateWork().getWkAnnoRifX().setWkAnnoRifX("");
        ws.getDateWork().getWkIniRic().setAaFormatted("0000");
        ws.getDateWork().getWkIniRic().setMmFormatted("00");
        ws.getDateWork().getWkIniRic().setGgFormatted("00");
        ws.getDateWork().getWkFinRic().setAaFormatted("0000");
        ws.getDateWork().getWkFinRic().setMmFormatted("00");
        ws.getDateWork().getWkFinRic().setGgFormatted("00");
    }

    public void initWkAreaFile() {
        ws.getWkAreaFile().setIbOgg("");
        ws.getWkAreaFile().setIdPoliFormatted("000000000");
        ws.getWkAreaFile().setIdAdesFormatted("000000000");
    }

    public void initWoutRecOutput() {
        ws.getWoutRecOutput().setWrinMacrofunzionalita("");
        ws.getWoutRecOutput().setWrinFunzionalita("");
        ws.getWoutRecOutput().setWrinCodCompAniaFormatted("00000");
        ws.getWoutRecOutput().setWrinIbPoliInd("");
        ws.getWoutRecOutput().setWrinIbPoliCol("");
        ws.getWoutRecOutput().setWrinIbAdes("");
        ws.getWoutRecOutput().setWrinIbGar("");
        ws.getWoutRecOutput().setWrinIbTrchDiGar("");
        ws.getWoutRecOutput().setWrinTpInvst("");
        ws.getWoutRecOutput().setWrinTpRecord("");
        ws.getWoutRecOutput().getWrinTpRecordDati().setWrinTpRecordDati("");
    }

    public void initAreaIoIsps0580() {
        ws.getAreaIoIsps0580().getDatiInput().setCodCompagniaAniaFormatted("00000");
        ws.getAreaIoIsps0580().getDatiInput().setDataInizElab("");
        ws.getAreaIoIsps0580().getDatiInput().setDataFineElab("");
        ws.getAreaIoIsps0580().getDatiInput().setDataRiferimento("");
        ws.getAreaIoIsps0580().getDatiInput().setIspc0580LivelloUtenteFormatted("00");
        ws.getAreaIoIsps0580().getDatiInput().setIspc0580FunzionalitaFormatted("00000");
        ws.getAreaIoIsps0580().getDatiInput().setSessionId("");
        ws.getAreaIoIsps0580().setNumMaxProdFormatted("000");
        for (int idx0 = 1; idx0 <= AreaIoIsps0580.TAB_PROD_MAXOCCURS; idx0++) {
            ws.getAreaIoIsps0580().getTabProd(idx0).setTipoPrd("");
            ws.getAreaIoIsps0580().getTabProd(idx0).setTipoGar("");
            ws.getAreaIoIsps0580().getTabProd(idx0).setSottoramo("");
            ws.getAreaIoIsps0580().getTabProd(idx0).setFormaprest("");
            ws.getAreaIoIsps0580().getTabProd(idx0).setForma("");
            ws.getAreaIoIsps0580().getTabProd(idx0).setTipopremio("");
        }
        ws.getAreaIoIsps0580().setEsitoFormatted("00");
        ws.getAreaIoIsps0580().setErrNumEleFormatted("000");
        for (int idx0 = 1; idx0 <= AreaIoIsps0580.TAB_ERRORI_MAXOCCURS; idx0++) {
            ws.getAreaIoIsps0580().getTabErrori(idx0).setCodErr("");
            ws.getAreaIoIsps0580().getTabErrori(idx0).setDescrizioneErr("");
            ws.getAreaIoIsps0580().getTabErrori(idx0).setIspc0211GravitaErrFormatted("00");
            ws.getAreaIoIsps0580().getTabErrori(idx0).setIspc0211LivelloDerogaFormatted("00");
        }
    }

    public void initAreaIoIsps0590() {
        ws.getAreaIoIsps0590().getDatiInput().setCodCompagniaAniaFormatted("00000");
        ws.getAreaIoIsps0590().getDatiInput().setDataInizElab("");
        ws.getAreaIoIsps0590().getDatiInput().setDataFineElab("");
        ws.getAreaIoIsps0590().getDatiInput().setDataRiferimento("");
        ws.getAreaIoIsps0590().getDatiInput().setIspc0580LivelloUtenteFormatted("00");
        ws.getAreaIoIsps0590().getDatiInput().setIspc0580FunzionalitaFormatted("00000");
        ws.getAreaIoIsps0590().getDatiInput().setSessionId("");
        ws.getAreaIoIsps0590().setNumMaxProdFormatted("000");
        for (int idx0 = 1; idx0 <= AreaIoIsps0590.TAB_PROD_MAXOCCURS; idx0++) {
            ws.getAreaIoIsps0590().getTabProd(idx0).setCodTari("");
            ws.getAreaIoIsps0590().getTabProd(idx0).setTassoTecnico(new AfDecimal(0, 14, 9));
        }
        ws.getAreaIoIsps0590().setEsitoFormatted("00");
        ws.getAreaIoIsps0590().setErrNumEleFormatted("000");
        for (int idx0 = 1; idx0 <= AreaIoIsps0590.TAB_ERRORI_MAXOCCURS; idx0++) {
            ws.getAreaIoIsps0590().getTabErrori(idx0).setCodErr("");
            ws.getAreaIoIsps0590().getTabErrori(idx0).setDescrizioneErr("");
            ws.getAreaIoIsps0590().getTabErrori(idx0).setIspc0211GravitaErrFormatted("00");
            ws.getAreaIoIsps0590().getTabErrori(idx0).setIspc0211LivelloDerogaFormatted("00");
        }
    }

    public void initStrPerformanceDbg() {
        ws.getIdsv8888().getStrPerformanceDbg().setFase("");
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("");
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("");
        ws.getIdsv8888().getStrPerformanceDbg().setTimestampFld("");
        ws.getIdsv8888().getStrPerformanceDbg().setStato("");
        ws.getIdsv8888().getStrPerformanceDbg().setModalitaEsecutiva(Types.SPACE_CHAR);
        ws.getIdsv8888().getStrPerformanceDbg().setUserName("");
    }

    public void initWorkCalcoli() {
        ws.getWorkCalcoli().setWsDiff(new AfDecimal(0, 15, 3));
        ws.getWorkCalcoli().setWsDiff3(new AfDecimal(0, 12, 5));
        ws.getWorkCalcoli().setWsDiff2(new AfDecimal(0, 14, 2));
        ws.getWorkCalcoli().setWsDiv(new AfDecimal(0, 15, 3));
        ws.getWorkCalcoli().setWsPerc(new AfDecimal(0, 15, 3));
        ws.getWorkCalcoli().setWsSomma(new AfDecimal(0, 15, 3));
        ws.getWorkCalcoli().setWsSomma2(new AfDecimal(0, 15, 3));
        ws.getWorkCalcoli().setWsSomma3(new AfDecimal(0, 12, 5));
        ws.getWorkCalcoli().setWsSomma4(new AfDecimal(0, 12, 5));
        ws.getWorkCalcoli().setWsSomma123(new AfDecimal(0, 15, 3));
        ws.getWorkCalcoli().setWsDiff123(new AfDecimal(0, 15, 3));
        ws.getWorkCalcoli().setWsCntrvalPrec(new AfDecimal(0, 15, 3));
        ws.getWorkCalcoli().setWsAppo431(new AfDecimal(0, 15, 3));
        ws.getWorkCalcoli().setWsAppo432(new AfDecimal(0, 15, 3));
        ws.getWorkCalcoli().setWsAppo1(new AfDecimal(0, 15, 3));
        ws.getWorkCalcoli().setWsAppo2(new AfDecimal(0, 15, 3));
        ws.getWorkCalcoli().setWsAppo3(new AfDecimal(0, 15, 3));
        ws.getWorkCalcoli().setWsCampoAppoggio(new AfDecimal(0, 5, 2));
        ws.getWorkCalcoli().setWkTassoTecRv(new AfDecimal(0, 14, 9));
    }

    public void initIoA2kLccc0003() {
        ws.getIoA2kLccc0003().getInput().setA2kFunz("");
        ws.getIoA2kLccc0003().getInput().setA2kInfo("");
        ws.getIoA2kLccc0003().getInput().setA2kDeltaFormatted("000");
        ws.getIoA2kLccc0003().getInput().setA2kTdelta(Types.SPACE_CHAR);
        ws.getIoA2kLccc0003().getInput().setA2kFislav(Types.SPACE_CHAR);
        ws.getIoA2kLccc0003().getInput().setA2kInicon(Types.SPACE_CHAR);
        ws.getIoA2kLccc0003().getInput().getA2kIndata().setA2kIndata("");
        ws.getIoA2kLccc0003().getOutput().getA2kOugmaX().setA2kOugmaFormatted("00000000");
        ws.getIoA2kLccc0003().getOutput().getA2kOugbmba().setOugg02Formatted("00");
        ws.getIoA2kLccc0003().getOutput().getA2kOugbmba().setOus102(Types.SPACE_CHAR);
        ws.getIoA2kLccc0003().getOutput().getA2kOugbmba().setOumm02Formatted("00");
        ws.getIoA2kLccc0003().getOutput().getA2kOugbmba().setOus202(Types.SPACE_CHAR);
        ws.getIoA2kLccc0003().getOutput().getA2kOugbmba().setOuss02Formatted("00");
        ws.getIoA2kLccc0003().getOutput().getA2kOugbmba().setOuaa02Formatted("00");
        ws.getIoA2kLccc0003().getOutput().getA2kOuamgX().setA2kOuamgFormatted("00000000");
        ws.getIoA2kLccc0003().getOutput().setA2kOuamgp(0);
        ws.getIoA2kLccc0003().getOutput().setA2kOuamg0p(0);
        ws.getIoA2kLccc0003().getOutput().setA2kOuprog9(0);
        ws.getIoA2kLccc0003().getOutput().setA2kOuprogc(0);
        ws.getIoA2kLccc0003().getOutput().setA2kOuprog(0);
        ws.getIoA2kLccc0003().getOutput().setA2kOuproco(0);
        ws.getIoA2kLccc0003().getOutput().getA2kOujulX().setA2kOujulFormatted("0000000");
        ws.getIoA2kLccc0003().getOutput().getA2kOusta().setOugg04Formatted("00");
        ws.getIoA2kLccc0003().getOutput().getA2kOusta().setOumm04("");
        ws.getIoA2kLccc0003().getOutput().getA2kOusta().setOuss04Formatted("00");
        ws.getIoA2kLccc0003().getOutput().getA2kOusta().setOuaa04Formatted("00");
        ws.getIoA2kLccc0003().getOutput().getA2kOurid().setOugg05Formatted("00");
        ws.getIoA2kLccc0003().getOutput().getA2kOurid().setOumm05("");
        ws.getIoA2kLccc0003().getOutput().getA2kOurid().setOuaa05Formatted("00");
        ws.getIoA2kLccc0003().getOutput().setA2kOugg06("");
        ws.getIoA2kLccc0003().getOutput().setA2kOung06Formatted("0");
        ws.getIoA2kLccc0003().getOutput().setA2kOutg06(Types.SPACE_CHAR);
        ws.getIoA2kLccc0003().getOutput().setA2kOugg07Formatted("00");
        ws.getIoA2kLccc0003().getOutput().getA2kOugl07X().setA2kOufa07Formatted("000");
        ws.getIoA2kLccc0003().getOutput().setA2kOugg08Formatted("0");
        ws.getIoA2kLccc0003().getOutput().setA2kOugg09(Types.SPACE_CHAR);
        ws.getIoA2kLccc0003().getOutput().setA2kOugg10(Types.SPACE_CHAR);
        ws.getIoA2kLccc0003().setRcode("");
    }

    public void initEstrCntDiagnCed() {
        ws.getEstrCntDiagnCed().setP84CodCompAnia(0);
        ws.getEstrCntDiagnCed().setP84IdPoli(0);
        ws.getEstrCntDiagnCed().setP84DtLiqCed(0);
        ws.getEstrCntDiagnCed().setP84CodTari("");
        ws.getEstrCntDiagnCed().getP84ImpLrdCedoleLiq().setP84ImpLrdCedoleLiq(new AfDecimal(0, 15, 3));
        ws.getEstrCntDiagnCed().setP84IbPoli("");
        ws.getEstrCntDiagnCed().setP84DsOperSql(Types.SPACE_CHAR);
        ws.getEstrCntDiagnCed().setP84DsVer(0);
        ws.getEstrCntDiagnCed().setP84DsTsCptz(0);
        ws.getEstrCntDiagnCed().setP84DsUtente("");
        ws.getEstrCntDiagnCed().setP84DsStatoElab(Types.SPACE_CHAR);
    }

    public void initEstrCntDiagnRiv() {
        ws.getEstrCntDiagnRiv().setP85CodCompAnia(0);
        ws.getEstrCntDiagnRiv().setP85IdPoli(0);
        ws.getEstrCntDiagnRiv().setP85DtRival(0);
        ws.getEstrCntDiagnRiv().setP85CodTari("");
        ws.getEstrCntDiagnRiv().getP85RendtoLrd().setP85RendtoLrd(new AfDecimal(0, 14, 9));
        ws.getEstrCntDiagnRiv().getP85PcRetr().setP85PcRetr(new AfDecimal(0, 6, 3));
        ws.getEstrCntDiagnRiv().getP85RendtoRetr().setP85RendtoRetr(new AfDecimal(0, 14, 9));
        ws.getEstrCntDiagnRiv().getP85CommisGest().setP85CommisGest(new AfDecimal(0, 15, 3));
        ws.getEstrCntDiagnRiv().getP85TsRivalNet().setP85TsRivalNet(new AfDecimal(0, 14, 9));
        ws.getEstrCntDiagnRiv().getP85MinGarto().setP85MinGarto(new AfDecimal(0, 14, 9));
        ws.getEstrCntDiagnRiv().getP85MinTrnut().setP85MinTrnut(new AfDecimal(0, 14, 9));
        ws.getEstrCntDiagnRiv().setP85IbPoli("");
        ws.getEstrCntDiagnRiv().setP85DsOperSql(Types.SPACE_CHAR);
        ws.getEstrCntDiagnRiv().setP85DsVer(0);
        ws.getEstrCntDiagnRiv().setP85DsTsCptz(0);
        ws.getEstrCntDiagnRiv().setP85DsUtente("");
        ws.getEstrCntDiagnRiv().setP85DsStatoElab(Types.SPACE_CHAR);
    }

    public void initAreaRival() {
        ws.getLoar0171().setWrecTipoRec("");
        ws.getLoar0171().getWrecOut().setWrecOut("");
    }

    public void initLdbv6191() {
        ws.getLdbv6191().setInfFormatted("00000000");
        ws.getLdbv6191().setInfDb("");
        ws.getLdbv6191().setSupFormatted("00000000");
        ws.getLdbv6191().setSupDb("");
    }

    public void initImpstSost() {
        ws.getImpstSost().setIsoIdImpstSost(0);
        ws.getImpstSost().getIsoIdOgg().setIsoIdOgg(0);
        ws.getImpstSost().setIsoTpOgg("");
        ws.getImpstSost().setIsoIdMoviCrz(0);
        ws.getImpstSost().getIsoIdMoviChiu().setIsoIdMoviChiu(0);
        ws.getImpstSost().setIsoDtIniEff(0);
        ws.getImpstSost().setIsoDtEndEff(0);
        ws.getImpstSost().getIsoDtIniPer().setIsoDtIniPer(0);
        ws.getImpstSost().getIsoDtEndPer().setIsoDtEndPer(0);
        ws.getImpstSost().setIsoCodCompAnia(0);
        ws.getImpstSost().getIsoImpstSost().setIsoImpstSost(new AfDecimal(0, 15, 3));
        ws.getImpstSost().getIsoImpbIs().setIsoImpbIs(new AfDecimal(0, 15, 3));
        ws.getImpstSost().getIsoAlqIs().setIsoAlqIs(new AfDecimal(0, 6, 3));
        ws.getImpstSost().setIsoCodTrb("");
        ws.getImpstSost().getIsoPrstzLrdAnteIs().setIsoPrstzLrdAnteIs(new AfDecimal(0, 15, 3));
        ws.getImpstSost().getIsoRisMatNetPrec().setIsoRisMatNetPrec(new AfDecimal(0, 15, 3));
        ws.getImpstSost().getIsoRisMatAnteTax().setIsoRisMatAnteTax(new AfDecimal(0, 15, 3));
        ws.getImpstSost().getIsoRisMatPostTax().setIsoRisMatPostTax(new AfDecimal(0, 15, 3));
        ws.getImpstSost().getIsoPrstzNet().setIsoPrstzNet(new AfDecimal(0, 15, 3));
        ws.getImpstSost().getIsoPrstzPrec().setIsoPrstzPrec(new AfDecimal(0, 15, 3));
        ws.getImpstSost().getIsoCumPreVers().setIsoCumPreVers(new AfDecimal(0, 15, 3));
        ws.getImpstSost().setIsoTpCalcImpst("");
        ws.getImpstSost().setIsoImpGiaTassato(new AfDecimal(0, 15, 3));
        ws.getImpstSost().setIsoDsRiga(0);
        ws.getImpstSost().setIsoDsOperSql(Types.SPACE_CHAR);
        ws.getImpstSost().setIsoDsVer(0);
        ws.getImpstSost().setIsoDsTsIniCptz(0);
        ws.getImpstSost().setIsoDsTsEndCptz(0);
        ws.getImpstSost().setIsoDsUtente("");
        ws.getImpstSost().setIsoDsStatoElab(Types.SPACE_CHAR);
    }

    public void initPoli() {
        ws.getPoli().setPolIdPoli(0);
        ws.getPoli().setPolIdMoviCrz(0);
        ws.getPoli().getPolIdMoviChiu().setPolIdMoviChiu(0);
        ws.getPoli().setPolIbOgg("");
        ws.getPoli().setPolIbProp("");
        ws.getPoli().getPolDtProp().setPolDtProp(0);
        ws.getPoli().setPolDtIniEff(0);
        ws.getPoli().setPolDtEndEff(0);
        ws.getPoli().setPolCodCompAnia(0);
        ws.getPoli().setPolDtDecor(0);
        ws.getPoli().setPolDtEmis(0);
        ws.getPoli().setPolTpPoli("");
        ws.getPoli().getPolDurAa().setPolDurAa(0);
        ws.getPoli().getPolDurMm().setPolDurMm(0);
        ws.getPoli().getPolDtScad().setPolDtScad(0);
        ws.getPoli().setPolCodProd("");
        ws.getPoli().setPolDtIniVldtProd(0);
        ws.getPoli().setPolCodConv("");
        ws.getPoli().setPolCodRamo("");
        ws.getPoli().getPolDtIniVldtConv().setPolDtIniVldtConv(0);
        ws.getPoli().getPolDtApplzConv().setPolDtApplzConv(0);
        ws.getPoli().setPolTpFrmAssva("");
        ws.getPoli().setPolTpRgmFisc("");
        ws.getPoli().setPolFlEstas(Types.SPACE_CHAR);
        ws.getPoli().setPolFlRshComun(Types.SPACE_CHAR);
        ws.getPoli().setPolFlRshComunCond(Types.SPACE_CHAR);
        ws.getPoli().setPolTpLivGenzTit("");
        ws.getPoli().setPolFlCopFinanz(Types.SPACE_CHAR);
        ws.getPoli().setPolTpApplzDir("");
        ws.getPoli().getPolSpeMed().setPolSpeMed(new AfDecimal(0, 15, 3));
        ws.getPoli().getPolDirEmis().setPolDirEmis(new AfDecimal(0, 15, 3));
        ws.getPoli().getPolDir1oVers().setPolDir1oVers(new AfDecimal(0, 15, 3));
        ws.getPoli().getPolDirVersAgg().setPolDirVersAgg(new AfDecimal(0, 15, 3));
        ws.getPoli().setPolCodDvs("");
        ws.getPoli().setPolFlFntAz(Types.SPACE_CHAR);
        ws.getPoli().setPolFlFntAder(Types.SPACE_CHAR);
        ws.getPoli().setPolFlFntTfr(Types.SPACE_CHAR);
        ws.getPoli().setPolFlFntVolo(Types.SPACE_CHAR);
        ws.getPoli().setPolTpOpzAScad("");
        ws.getPoli().getPolAaDiffProrDflt().setPolAaDiffProrDflt(0);
        ws.getPoli().setPolFlVerProd("");
        ws.getPoli().getPolDurGg().setPolDurGg(0);
        ws.getPoli().getPolDirQuiet().setPolDirQuiet(new AfDecimal(0, 15, 3));
        ws.getPoli().setPolTpPtfEstno("");
        ws.getPoli().setPolFlCumPreCntr(Types.SPACE_CHAR);
        ws.getPoli().setPolFlAmmbMovi(Types.SPACE_CHAR);
        ws.getPoli().setPolConvGeco("");
        ws.getPoli().setPolDsRiga(0);
        ws.getPoli().setPolDsOperSql(Types.SPACE_CHAR);
        ws.getPoli().setPolDsVer(0);
        ws.getPoli().setPolDsTsIniCptz(0);
        ws.getPoli().setPolDsTsEndCptz(0);
        ws.getPoli().setPolDsUtente("");
        ws.getPoli().setPolDsStatoElab(Types.SPACE_CHAR);
        ws.getPoli().setPolFlScudoFisc(Types.SPACE_CHAR);
        ws.getPoli().setPolFlTrasfe(Types.SPACE_CHAR);
        ws.getPoli().setPolFlTfrStrc(Types.SPACE_CHAR);
        ws.getPoli().getPolDtPresc().setPolDtPresc(0);
        ws.getPoli().setPolCodConvAgg("");
        ws.getPoli().setPolSubcatProd("");
        ws.getPoli().setPolFlQuestAdegzAss(Types.SPACE_CHAR);
        ws.getPoli().setPolCodTpa("");
        ws.getPoli().getPolIdAccComm().setPolIdAccComm(0);
        ws.getPoli().setPolFlPoliCpiPr(Types.SPACE_CHAR);
        ws.getPoli().setPolFlPoliBundling(Types.SPACE_CHAR);
        ws.getPoli().setPolIndPoliPrinColl(Types.SPACE_CHAR);
        ws.getPoli().setPolFlVndBundle(Types.SPACE_CHAR);
        ws.getPoli().setPolIbBs("");
        ws.getPoli().setPolFlPoliIfp(Types.SPACE_CHAR);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FILESQS1_REC = 2500;
        public static final int FILESQS2_REC = 49;
        public static final int FILESQS3_REC = 282;
        public static final int FILESQS4_REC = 500;
        public static final int FILESQS5_REC = 21;
        public static final int FILESQS6_REC = 2500;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
