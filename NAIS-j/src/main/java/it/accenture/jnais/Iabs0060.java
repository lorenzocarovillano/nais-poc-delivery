package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.BtcJobScheduleDao;
import it.accenture.jnais.commons.data.to.IBtcJobSchedule;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.BtcJobSchedule;
import it.accenture.jnais.ws.enums.FlagAccesso;
import it.accenture.jnais.ws.enums.Iabv0002TipoOrderBy;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Iabs0060Data;
import it.accenture.jnais.ws.Iabv0002;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.redefines.BjsDtEnd;
import it.accenture.jnais.ws.redefines.BjsDtStart;
import it.accenture.jnais.ws.redefines.BjsExecutionsCount;
import it.accenture.jnais.ws.redefines.BjsTpMovi;

/**Original name: IABS0060<br>
 * <pre>AUTHOR.        ATS NAPOLI.
 * DATE-WRITTEN.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : SERVIZIO ESTRAZIONE OCCORRENZE              *
 *                    DA TABELLE GUIDA RICHIAMATO DAL BATCH EXEC. *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Iabs0060 extends Program implements IBtcJobSchedule {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private BtcJobScheduleDao btcJobScheduleDao = new BtcJobScheduleDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Iabs0060Data ws = new Iabs0060Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: IABV0002
    private Iabv0002 iabv0002;
    //Original name: BTC-JOB-SCHEDULE
    private BtcJobSchedule btcJobSchedule;

    //==== METHODS ====
    /**Original name: PROGRAM_IABS0060_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, Iabv0002 iabv0002, BtcJobSchedule btcJobSchedule) {
        this.idsv0003 = idsv0003;
        this.iabv0002 = iabv0002;
        this.btcJobSchedule = btcJobSchedule;
        // COB_CODE: PERFORM A000-INIZIO                 THRU A000-EX.
        a000Inizio();
        // COB_CODE: PERFORM A060-CNTL-INPUT             THRU A060-EX.
        a060CntlInput();
        // COB_CODE: PERFORM A300-ELABORA                THRU A300-EX.
        a300Elabora();
        // COB_CODE: PERFORM A400-FINE                   THRU A400-EX.
        a400Fine();
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Iabs0060 getInstance() {
        return ((Iabs0060)Programs.getInstance(Iabs0060.class));
    }

    /**Original name: A000-INIZIO<br>
	 * <pre>*****************************************************************</pre>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'IABS0060'               TO IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("IABS0060");
        // COB_CODE: MOVE 'BTC_JOB_SCHEDULE'       TO IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("BTC_JOB_SCHEDULE");
        // COB_CODE: MOVE '00'                     TO IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                   TO IDSV0003-SQLCODE
        //                                            IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                   TO IDSV0003-DESCRIZ-ERR-DB2
        //                                            IDSV0003-KEY-TABELLA
        //                                            FLAG-ACCESSO.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        ws.getFlagAccesso().setFlagAccesso(Types.SPACE_CHAR);
        // COB_CODE: SET ACCESSO-X-RANGE-NO        TO TRUE.
        ws.getFlagAccessoXRange().setNo();
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
        // COB_CODE: MOVE IDSV0003-BUFFER-WHERE-COND TO FLAG-ACCESSO.
        ws.getFlagAccesso().setFlagAccessoFormatted(idsv0003.getBufferWhereCondFormatted());
    }

    /**Original name: A060-CNTL-INPUT<br>
	 * <pre>*****************************************************************</pre>*/
    private void a060CntlInput() {
        // COB_CODE: IF NOT ACCESSO-IB-OGG
        //              END-IF
        //           END-IF.
        if (!ws.getFlagAccesso().isIbOgg()) {
            // COB_CODE: SET ACCESSO-STD              TO TRUE
            ws.getFlagAccesso().setStd();
            // COB_CODE: IF BJS-COD-MACROFUNCT NOT = SPACES     AND
            //              BJS-COD-MACROFUNCT NOT = LOW-VALUE  AND
            //              BJS-COD-MACROFUNCT NOT = HIGH-VALUE
            //              SET ACCESSO-MCRFNCT          TO TRUE
            //           END-IF
            if (!Characters.EQ_SPACE.test(btcJobSchedule.getBjsCodMacrofunct()) && !Characters.EQ_LOW.test(btcJobSchedule.getBjsCodMacrofunctFormatted()) && !Characters.EQ_HIGH.test(btcJobSchedule.getBjsCodMacrofunctFormatted())) {
                // COB_CODE: SET ACCESSO-MCRFNCT          TO TRUE
                ws.getFlagAccesso().setMcrfnct();
            }
            // COB_CODE: IF BJS-TP-MOVI-NULL IS NUMERIC   AND
            //              BJS-TP-MOVI      NOT = ZERO
            //              END-IF
            //           END-IF
            if (Functions.isNumber(btcJobSchedule.getBjsTpMovi().getBjsTpMoviNullFormatted()) && btcJobSchedule.getBjsTpMovi().getBjsTpMovi() != 0) {
                // COB_CODE: IF ACCESSO-MCRFNCT
                //              SET ACCESSO-MCRFNCT-TPMV  TO TRUE
                //           ELSE
                //              SET ACCESSO-TPMV          TO TRUE
                //           END-IF
                if (ws.getFlagAccesso().isMcrfnct()) {
                    // COB_CODE: SET ACCESSO-MCRFNCT-TPMV  TO TRUE
                    ws.getFlagAccesso().setMcrfnctTpmv();
                }
                else {
                    // COB_CODE: SET ACCESSO-TPMV          TO TRUE
                    ws.getFlagAccesso().setTpmv();
                }
            }
            // COB_CODE: SET ACCESSO-X-RANGE-NO          TO TRUE
            ws.getFlagAccessoXRange().setNo();
            // COB_CODE: IF IABV0009-ID-OGG-DA IS NUMERIC AND
            //              IABV0009-ID-OGG-DA NOT = ZEROES
            //              END-IF
            //           END-IF
            if (Functions.isNumber(iabv0002.getIabv0009GestGuideService().getIdOggDa()) && iabv0002.getIabv0009GestGuideService().getIdOggDa() != 0) {
                // COB_CODE: IF IABV0009-ID-OGG-A  IS NUMERIC AND
                //              IABV0009-ID-OGG-A  NOT = ZEROES
                //              SET ACCESSO-STD           TO TRUE
                //           END-IF
                if (Functions.isNumber(iabv0002.getIabv0009GestGuideService().getIdOggA()) && iabv0002.getIabv0009GestGuideService().getIdOggA() != 0) {
                    // COB_CODE: SET ACCESSO-X-RANGE-SI    TO TRUE
                    ws.getFlagAccessoXRange().setSi();
                    // COB_CODE: SET ACCESSO-STD           TO TRUE
                    ws.getFlagAccesso().setStd();
                }
            }
        }
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>
	 * <pre>*****************************************************************</pre>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-SELECT         OR
                //              IDSV0003-FETCH-FIRST    OR
                //              IDSV0003-FETCH-NEXT     OR
                //              IDSV0003-UPDATE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isUpdate()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A300-ELABORA<br>
	 * <pre>*****************************************************************</pre>*/
    private void a300Elabora() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A310-SELECT                 THRU A310-EX
        //              WHEN IDSV0003-INSERT
        //                 PERFORM A318-INSERT                 THRU A318-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A360-OPEN-CURSOR            THRU A360-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A370-CLOSE-CURSOR           THRU A370-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A380-FETCH-FIRST            THRU A380-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A390-FETCH-NEXT             THRU A390-EX
        //              WHEN IDSV0003-UPDATE
        //                 PERFORM A320-UPDATE                 THRU A320-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A310-SELECT                 THRU A310-EX
            a310Select();
        }
        else if (idsv0003.getOperazione().isInsert()) {
            // COB_CODE: PERFORM A318-INSERT                 THRU A318-EX
            a318Insert();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A360-OPEN-CURSOR            THRU A360-EX
            a360OpenCursor();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A370-CLOSE-CURSOR           THRU A370-EX
            a370CloseCursor();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A380-FETCH-FIRST            THRU A380-EX
            a380FetchFirst();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT             THRU A390-EX
            a390FetchNext();
        }
        else if (idsv0003.getOperazione().isUpdate()) {
            // COB_CODE: PERFORM A320-UPDATE                 THRU A320-EX
            a320Update();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A400-FINE<br>
	 * <pre>*****************************************************************</pre>*/
    private void a400Fine() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A310-SELECT<br>
	 * <pre>*****************************************************************</pre>*/
    private void a310Select() {
        // COB_CODE: IF ACCESSO-X-RANGE-NO
        //              END-EVALUATE
        //           ELSE
        //              END-EVALUATE
        //           END-IF.
        if (ws.getFlagAccessoXRange().isNo()) {
            // COB_CODE: EVALUATE TRUE
            //              WHEN ACCESSO-STD
            //                 PERFORM A311-SELECT-STD          THRU A311-EX
            //              WHEN ACCESSO-MCRFNCT
            //                 PERFORM A312-SELECT-MCRFNCT      THRU A312-EX
            //              WHEN ACCESSO-TPMV
            //                 PERFORM A313-SELECT-TPMV         THRU A313-EX
            //              WHEN ACCESSO-MCRFNCT-TPMV
            //                 PERFORM A314-SELECT-MCRFNCT-TPMV THRU A314-EX
            //              WHEN ACCESSO-IB-OGG
            //                 PERFORM A315-SELECT-IB-OGG       THRU A315-EX
            //           END-EVALUATE
            switch (ws.getFlagAccesso().getFlagAccesso()) {

                case FlagAccesso.STD:// COB_CODE: PERFORM A311-SELECT-STD          THRU A311-EX
                    a311SelectStd();
                    break;

                case FlagAccesso.MCRFNCT:// COB_CODE: PERFORM A312-SELECT-MCRFNCT      THRU A312-EX
                    a312SelectMcrfnct();
                    break;

                case FlagAccesso.TPMV:// COB_CODE: PERFORM A313-SELECT-TPMV         THRU A313-EX
                    a313SelectTpmv();
                    break;

                case FlagAccesso.MCRFNCT_TPMV:// COB_CODE: PERFORM A314-SELECT-MCRFNCT-TPMV THRU A314-EX
                    a314SelectMcrfnctTpmv();
                    break;

                case FlagAccesso.IB_OGG:// COB_CODE: PERFORM A315-SELECT-IB-OGG       THRU A315-EX
                    a315SelectIbOgg();
                    break;

                default:break;
            }
        }
        else {
            // COB_CODE: EVALUATE TRUE
            //              WHEN ACCESSO-STD
            //                 PERFORM R311-SELECT-STD-X-RNG     THRU R311-EX
            //              WHEN OTHER
            //                                     TO IDSV0003-DESCRIZ-ERR-DB2
            //           END-EVALUATE
            switch (ws.getFlagAccesso().getFlagAccesso()) {

                case FlagAccesso.STD:// COB_CODE: PERFORM R311-SELECT-STD-X-RNG     THRU R311-EX
                    r311SelectStdXRng();
                    break;

                default:// COB_CODE: SET IDSV0003-GENERIC-ERROR        TO TRUE
                    idsv0003.getReturnCode().setGenericError();
                    // COB_CODE: MOVE 'TIPO DI ACCESSO NON VALIDO X MODALITA'' RANGE'
                    //                               TO IDSV0003-DESCRIZ-ERR-DB2
                    idsv0003.getCampiEsito().setDescrizErrDb2("TIPO DI ACCESSO NON VALIDO X MODALITA' RANGE");
                    break;
            }
        }
    }

    /**Original name: A311-SELECT-STD<br>
	 * <pre>*****************************************************************</pre>*/
    private void a311SelectStd() {
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_BATCH
        //                ,ID_JOB
        //                ,COD_ELAB_STATE
        //                ,FLAG_WARNINGS
        //                ,DT_START
        //                ,DT_END
        //                ,USER_START
        //                ,FLAG_ALWAYS_EXE
        //                ,EXECUTIONS_COUNT
        //                ,DATA_CONTENT_TYPE
        //                ,COD_MACROFUNCT
        //                ,TP_MOVI
        //                ,IB_OGG
        //                ,DATA
        //           INTO
        //                :BJS-ID-BATCH
        //               ,:BJS-ID-JOB
        //               ,:BJS-COD-ELAB-STATE
        //               ,:BJS-FLAG-WARNINGS
        //                :IND-BJS-FLAG-WARNINGS
        //               ,:BJS-DT-START-DB
        //                :IND-BJS-DT-START
        //               ,:BJS-DT-END-DB
        //                :IND-BJS-DT-END
        //               ,:BJS-USER-START
        //                :IND-BJS-USER-START
        //               ,:BJS-FLAG-ALWAYS-EXE
        //                :IND-BJS-FLAG-ALWAYS-EXE
        //               ,:BJS-EXECUTIONS-COUNT
        //                :IND-BJS-EXECUTIONS-COUNT
        //               ,:BJS-DATA-CONTENT-TYPE
        //                :IND-BJS-DATA-CONTENT-TYPE
        //               ,:BJS-COD-MACROFUNCT
        //                :IND-BJS-COD-MACROFUNCT
        //               ,:BJS-TP-MOVI
        //                :IND-BJS-TP-MOVI
        //               ,:BJS-IB-OGG
        //                :IND-BJS-IB-OGG
        //               ,:BJS-DATA-VCHAR
        //                :IND-BJS-DATA
        //              FROM BTC_JOB_SCHEDULE
        //              WHERE     ID_BATCH                 = :BJS-ID-BATCH
        //                    AND
        //                    (
        //                          COD_ELAB_STATE IN (
        //                                         :IABV0002-STATE-01,
        //                                         :IABV0002-STATE-02,
        //                                         :IABV0002-STATE-03,
        //                                         :IABV0002-STATE-04,
        //                                         :IABV0002-STATE-05,
        //                                         :IABV0002-STATE-06,
        //                                         :IABV0002-STATE-07,
        //                                         :IABV0002-STATE-08,
        //                                         :IABV0002-STATE-09,
        //                                         :IABV0002-STATE-10
        //                                            )
        //                        )
        //              FETCH FIRST ROW ONLY
        //           END-EXEC.
        btcJobScheduleDao.selectRec(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A312-SELECT-MCRFNCT<br>
	 * <pre>*****************************************************************</pre>*/
    private void a312SelectMcrfnct() {
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_BATCH
        //                ,ID_JOB
        //                ,COD_ELAB_STATE
        //                ,FLAG_WARNINGS
        //                ,DT_START
        //                ,DT_END
        //                ,USER_START
        //                ,FLAG_ALWAYS_EXE
        //                ,EXECUTIONS_COUNT
        //                ,DATA_CONTENT_TYPE
        //                ,COD_MACROFUNCT
        //                ,TP_MOVI
        //                ,IB_OGG
        //                ,DATA
        //           INTO
        //                :BJS-ID-BATCH
        //               ,:BJS-ID-JOB
        //               ,:BJS-COD-ELAB-STATE
        //               ,:BJS-FLAG-WARNINGS
        //                :IND-BJS-FLAG-WARNINGS
        //               ,:BJS-DT-START-DB
        //                :IND-BJS-DT-START
        //               ,:BJS-DT-END-DB
        //                :IND-BJS-DT-END
        //               ,:BJS-USER-START
        //                :IND-BJS-USER-START
        //               ,:BJS-FLAG-ALWAYS-EXE
        //                :IND-BJS-FLAG-ALWAYS-EXE
        //               ,:BJS-EXECUTIONS-COUNT
        //                :IND-BJS-EXECUTIONS-COUNT
        //               ,:BJS-DATA-CONTENT-TYPE
        //                :IND-BJS-DATA-CONTENT-TYPE
        //               ,:BJS-COD-MACROFUNCT
        //                :IND-BJS-COD-MACROFUNCT
        //               ,:BJS-TP-MOVI
        //                :IND-BJS-TP-MOVI
        //               ,:BJS-IB-OGG
        //                :IND-BJS-IB-OGG
        //               ,:BJS-DATA-VCHAR
        //                :IND-BJS-DATA
        //              FROM BTC_JOB_SCHEDULE
        //              WHERE     ID_BATCH                 = :BJS-ID-BATCH
        //                    AND COD_MACROFUNCT           = :BJS-COD-MACROFUNCT
        //                    AND
        //                    (
        //                          COD_ELAB_STATE IN (
        //                                         :IABV0002-STATE-01,
        //                                         :IABV0002-STATE-02,
        //                                         :IABV0002-STATE-03,
        //                                         :IABV0002-STATE-04,
        //                                         :IABV0002-STATE-05,
        //                                         :IABV0002-STATE-06,
        //                                         :IABV0002-STATE-07,
        //                                         :IABV0002-STATE-08,
        //                                         :IABV0002-STATE-09,
        //                                         :IABV0002-STATE-10
        //                                            )
        //                        )
        //              FETCH FIRST ROW ONLY
        //           END-EXEC.
        btcJobScheduleDao.selectRec1(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A313-SELECT-TPMV<br>
	 * <pre>*****************************************************************</pre>*/
    private void a313SelectTpmv() {
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_BATCH
        //                ,ID_JOB
        //                ,COD_ELAB_STATE
        //                ,FLAG_WARNINGS
        //                ,DT_START
        //                ,DT_END
        //                ,USER_START
        //                ,FLAG_ALWAYS_EXE
        //                ,EXECUTIONS_COUNT
        //                ,DATA_CONTENT_TYPE
        //                ,COD_MACROFUNCT
        //                ,TP_MOVI
        //                ,IB_OGG
        //                ,DATA
        //           INTO
        //                :BJS-ID-BATCH
        //               ,:BJS-ID-JOB
        //               ,:BJS-COD-ELAB-STATE
        //               ,:BJS-FLAG-WARNINGS
        //                :IND-BJS-FLAG-WARNINGS
        //               ,:BJS-DT-START-DB
        //                :IND-BJS-DT-START
        //               ,:BJS-DT-END-DB
        //                :IND-BJS-DT-END
        //               ,:BJS-USER-START
        //                :IND-BJS-USER-START
        //               ,:BJS-FLAG-ALWAYS-EXE
        //                :IND-BJS-FLAG-ALWAYS-EXE
        //               ,:BJS-EXECUTIONS-COUNT
        //                :IND-BJS-EXECUTIONS-COUNT
        //               ,:BJS-DATA-CONTENT-TYPE
        //                :IND-BJS-DATA-CONTENT-TYPE
        //               ,:BJS-COD-MACROFUNCT
        //                :IND-BJS-COD-MACROFUNCT
        //               ,:BJS-TP-MOVI
        //                :IND-BJS-TP-MOVI
        //               ,:BJS-IB-OGG
        //                :IND-BJS-IB-OGG
        //               ,:BJS-DATA-VCHAR
        //                :IND-BJS-DATA
        //              FROM BTC_JOB_SCHEDULE
        //              WHERE     ID_BATCH                 = :BJS-ID-BATCH
        //                    AND TP_MOVI                  = :BJS-TP-MOVI
        //                    AND
        //                    (
        //                          COD_ELAB_STATE IN (
        //                                         :IABV0002-STATE-01,
        //                                         :IABV0002-STATE-02,
        //                                         :IABV0002-STATE-03,
        //                                         :IABV0002-STATE-04,
        //                                         :IABV0002-STATE-05,
        //                                         :IABV0002-STATE-06,
        //                                         :IABV0002-STATE-07,
        //                                         :IABV0002-STATE-08,
        //                                         :IABV0002-STATE-09,
        //                                         :IABV0002-STATE-10
        //                                            )
        //                        )
        //              FETCH FIRST ROW ONLY
        //           END-EXEC.
        btcJobScheduleDao.selectRec2(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A314-SELECT-MCRFNCT-TPMV<br>
	 * <pre>*****************************************************************</pre>*/
    private void a314SelectMcrfnctTpmv() {
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_BATCH
        //                ,ID_JOB
        //                ,COD_ELAB_STATE
        //                ,FLAG_WARNINGS
        //                ,DT_START
        //                ,DT_END
        //                ,USER_START
        //                ,FLAG_ALWAYS_EXE
        //                ,EXECUTIONS_COUNT
        //                ,DATA_CONTENT_TYPE
        //                ,COD_MACROFUNCT
        //                ,TP_MOVI
        //                ,IB_OGG
        //                ,DATA
        //           INTO
        //                :BJS-ID-BATCH
        //               ,:BJS-ID-JOB
        //               ,:BJS-COD-ELAB-STATE
        //               ,:BJS-FLAG-WARNINGS
        //                :IND-BJS-FLAG-WARNINGS
        //               ,:BJS-DT-START-DB
        //                :IND-BJS-DT-START
        //               ,:BJS-DT-END-DB
        //                :IND-BJS-DT-END
        //               ,:BJS-USER-START
        //                :IND-BJS-USER-START
        //               ,:BJS-FLAG-ALWAYS-EXE
        //                :IND-BJS-FLAG-ALWAYS-EXE
        //               ,:BJS-EXECUTIONS-COUNT
        //                :IND-BJS-EXECUTIONS-COUNT
        //               ,:BJS-DATA-CONTENT-TYPE
        //                :IND-BJS-DATA-CONTENT-TYPE
        //               ,:BJS-COD-MACROFUNCT
        //                :IND-BJS-COD-MACROFUNCT
        //               ,:BJS-TP-MOVI
        //                :IND-BJS-TP-MOVI
        //               ,:BJS-IB-OGG
        //                :IND-BJS-IB-OGG
        //               ,:BJS-DATA-VCHAR
        //                :IND-BJS-DATA
        //              FROM BTC_JOB_SCHEDULE
        //              WHERE     ID_BATCH                 = :BJS-ID-BATCH
        //                    AND COD_MACROFUNCT           = :BJS-COD-MACROFUNCT
        //                    AND TP_MOVI                  = :BJS-TP-MOVI
        //                    AND
        //                    (
        //                          COD_ELAB_STATE IN (
        //                                         :IABV0002-STATE-01,
        //                                         :IABV0002-STATE-02,
        //                                         :IABV0002-STATE-03,
        //                                         :IABV0002-STATE-04,
        //                                         :IABV0002-STATE-05,
        //                                         :IABV0002-STATE-06,
        //                                         :IABV0002-STATE-07,
        //                                         :IABV0002-STATE-08,
        //                                         :IABV0002-STATE-09,
        //                                         :IABV0002-STATE-10
        //                                            )
        //                        )
        //              FETCH FIRST ROW ONLY
        //           END-EXEC.
        btcJobScheduleDao.selectRec3(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A315-SELECT-IB-OGG<br>
	 * <pre>*****************************************************************</pre>*/
    private void a315SelectIbOgg() {
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_BATCH
        //                ,ID_JOB
        //                ,COD_ELAB_STATE
        //                ,FLAG_WARNINGS
        //                ,DT_START
        //                ,DT_END
        //                ,USER_START
        //                ,FLAG_ALWAYS_EXE
        //                ,EXECUTIONS_COUNT
        //                ,DATA_CONTENT_TYPE
        //                ,COD_MACROFUNCT
        //                ,TP_MOVI
        //                ,IB_OGG
        //                ,DATA
        //           INTO
        //                :BJS-ID-BATCH
        //               ,:BJS-ID-JOB
        //               ,:BJS-COD-ELAB-STATE
        //               ,:BJS-FLAG-WARNINGS
        //                :IND-BJS-FLAG-WARNINGS
        //               ,:BJS-DT-START-DB
        //                :IND-BJS-DT-START
        //               ,:BJS-DT-END-DB
        //                :IND-BJS-DT-END
        //               ,:BJS-USER-START
        //                :IND-BJS-USER-START
        //               ,:BJS-FLAG-ALWAYS-EXE
        //                :IND-BJS-FLAG-ALWAYS-EXE
        //               ,:BJS-EXECUTIONS-COUNT
        //                :IND-BJS-EXECUTIONS-COUNT
        //               ,:BJS-DATA-CONTENT-TYPE
        //                :IND-BJS-DATA-CONTENT-TYPE
        //               ,:BJS-COD-MACROFUNCT
        //                :IND-BJS-COD-MACROFUNCT
        //               ,:BJS-TP-MOVI
        //                :IND-BJS-TP-MOVI
        //               ,:BJS-IB-OGG
        //                :IND-BJS-IB-OGG
        //               ,:BJS-DATA-VCHAR
        //                :IND-BJS-DATA
        //              FROM BTC_JOB_SCHEDULE
        //              WHERE ID_BATCH                 = :BJS-ID-BATCH AND
        //                    IB_OGG                   = :BJS-IB-OGG   AND
        //                    (
        //                          COD_ELAB_STATE IN (
        //                                         :IABV0002-STATE-01,
        //                                         :IABV0002-STATE-02,
        //                                         :IABV0002-STATE-03,
        //                                         :IABV0002-STATE-04,
        //                                         :IABV0002-STATE-05,
        //                                         :IABV0002-STATE-06,
        //                                         :IABV0002-STATE-07,
        //                                         :IABV0002-STATE-08,
        //                                         :IABV0002-STATE-09,
        //                                         :IABV0002-STATE-10
        //                                            )
        //                        )
        //           END-EXEC.
        btcJobScheduleDao.selectRec4(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A318-INSERT<br>
	 * <pre>*****************************************************************</pre>*/
    private void a318Insert() {
        // COB_CODE: MOVE BJS-IB-OGG(41:9) TO WK-ID-JOB-X
        ws.getWkIdJobX().setWkIdJobX(btcJobSchedule.getBjsIbOggFormatted().substring((41) - 1, 49));
        // COB_CODE: MOVE SPACES           TO BJS-IB-OGG(41:9)
        btcJobSchedule.setBjsIbOggSubstring(LiteralGenerator.create(Types.SPACE_CHAR, 9), 41, 9);
        // COB_CODE: IF  WK-ID-JOB-N IS NUMERIC
        //           AND WK-ID-JOB-N > ZERO
        //              MOVE WK-ID-JOB-N        TO BJS-ID-JOB
        //           ELSE
        //              PERFORM Z400-SEQ                       THRU Z400-EX
        //           END-IF.
        if (Functions.isNumber(ws.getWkIdJobX().getWkIdJobNFormatted()) && Characters.GT_ZERO.test(ws.getWkIdJobX().getWkIdJobNFormatted())) {
            // COB_CODE: MOVE WK-ID-JOB-N        TO BJS-ID-JOB
            btcJobSchedule.setBjsIdJob(ws.getWkIdJobX().getWkIdJobN());
        }
        else {
            // COB_CODE: PERFORM Z400-SEQ                       THRU Z400-EX
            z400Seq();
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES THRU Z150-EX
            z150ValorizzaDataServices();
            // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX
            z200SetIndicatoriNull();
            // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX
            z900ConvertiNToX();
            // COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX
            z960LengthVchar();
            // COB_CODE: EXEC SQL
            //              INSERT
            //              INTO BTC_JOB_SCHEDULE
            //                  (
            //             ID_BATCH
            //             ,ID_JOB
            //             ,COD_ELAB_STATE
            //             ,FLAG_WARNINGS
            //             ,DT_START
            //             ,DT_END
            //             ,USER_START
            //             ,FLAG_ALWAYS_EXE
            //             ,EXECUTIONS_COUNT
            //             ,DATA_CONTENT_TYPE
            //             ,COD_MACROFUNCT
            //             ,TP_MOVI
            //             ,IB_OGG
            //             ,DATA
            //                  )
            //              VALUES
            //                  (
            //             :BJS-ID-BATCH
            //            ,:BJS-ID-JOB
            //            ,:BJS-COD-ELAB-STATE
            //            ,:BJS-FLAG-WARNINGS
            //             :IND-BJS-FLAG-WARNINGS
            //            ,:BJS-DT-START-DB
            //             :IND-BJS-DT-START
            //            ,:BJS-DT-END-DB
            //             :IND-BJS-DT-END
            //            ,:BJS-USER-START
            //             :IND-BJS-USER-START
            //            ,:BJS-FLAG-ALWAYS-EXE
            //             :IND-BJS-FLAG-ALWAYS-EXE
            //            ,:BJS-EXECUTIONS-COUNT
            //             :IND-BJS-EXECUTIONS-COUNT
            //            ,:BJS-DATA-CONTENT-TYPE
            //             :IND-BJS-DATA-CONTENT-TYPE
            //            ,:BJS-COD-MACROFUNCT
            //             :IND-BJS-COD-MACROFUNCT
            //            ,:BJS-TP-MOVI
            //             :IND-BJS-TP-MOVI
            //            ,:BJS-IB-OGG
            //             :IND-BJS-IB-OGG
            //            ,:BJS-DATA-VCHAR
            //             :IND-BJS-DATA
            //                  )
            //           END-EXEC
            btcJobScheduleDao.insertRec(this);
            // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
            a100CheckReturnCode();
        }
    }

    /**Original name: A320-UPDATE<br>
	 * <pre>*****************************************************************</pre>*/
    private void a320Update() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-PRIMARY-KEY
        //                   PERFORM A330-UPDATE-PK           THRU A330-EX
        //              WHEN IDSV0003-FIRST-ACTION
        //                   PERFORM A340-UPDATE-FIRST-ACTION THRU A340-EX
        //              WHEN IDSV0003-WHERE-CONDITION
        //                   PERFORM A350-UPDATE-WHERE-COND   THRU A350-EX
        //              WHEN OTHER
        //                   SET IDSV0003-INVALID-LEVEL-OPER  TO TRUE
        //           END-EVALUATE.
        switch (idsv0003.getLivelloOperazione().getLivelloOperazione()) {

            case Idsv0003LivelloOperazione.PRIMARY_KEY:// COB_CODE: PERFORM A330-UPDATE-PK           THRU A330-EX
                a330UpdatePk();
                break;

            case Idsv0003LivelloOperazione.FIRST_ACTION:// COB_CODE: PERFORM A340-UPDATE-FIRST-ACTION THRU A340-EX
                a340UpdateFirstAction();
                break;

            case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM A350-UPDATE-WHERE-COND   THRU A350-EX
                a350UpdateWhereCond();
                break;

            default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER  TO TRUE
                idsv0003.getReturnCode().setInvalidLevelOper();
                break;
        }
    }

    /**Original name: A330-UPDATE-PK<br>
	 * <pre>*****************************************************************</pre>*/
    private void a330UpdatePk() {
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX
        z900ConvertiNToX();
        // COB_CODE: EXEC SQL
        //                UPDATE BTC_JOB_SCHEDULE SET
        //                   COD_ELAB_STATE         = :IABV0002-STATE-CURRENT
        //                  ,EXECUTIONS_COUNT       = :BJS-EXECUTIONS-COUNT
        //                                            :IND-BJS-EXECUTIONS-COUNT
        //                  ,FLAG_WARNINGS          = :BJS-FLAG-WARNINGS
        //                                            :IND-BJS-FLAG-WARNINGS
        //                  ,DT_START               = :BJS-DT-START-DB
        //                                            :IND-BJS-DT-START
        //                  ,DT_END                 = :BJS-DT-END-DB
        //                                            :IND-BJS-DT-END
        //                  ,USER_START             = :BJS-USER-START
        //                                            :IND-BJS-USER-START
        //                WHERE   ID_BATCH = :BJS-ID-BATCH
        //                  AND   ID_JOB   = :BJS-ID-JOB
        //           END-EXEC.
        btcJobScheduleDao.updateRec(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              MOVE IABV0002-STATE-CURRENT TO BJS-COD-ELAB-STATE
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: MOVE IABV0002-STATE-CURRENT TO BJS-COD-ELAB-STATE
            btcJobSchedule.setBjsCodElabState(iabv0002.getIabv0002StateCurrent());
        }
    }

    /**Original name: A340-UPDATE-FIRST-ACTION<br>
	 * <pre>*****************************************************************</pre>*/
    private void a340UpdateFirstAction() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN ACCESSO-STD
        //                 PERFORM A341-UPDATE-F-ACT-STD          THRU A341-EX
        //              WHEN ACCESSO-MCRFNCT
        //                 PERFORM A342-UPDATE-F-ACT-MCRFNCT      THRU A342-EX
        //              WHEN ACCESSO-TPMV
        //                 PERFORM A343-UPDATE-F-ACT-TPMV         THRU A343-EX
        //              WHEN ACCESSO-MCRFNCT-TPMV
        //                 PERFORM A344-UPDATE-F-ACT-MCRFNCT-TPMV THRU A344-EX
        //           END-EVALUATE.
        switch (ws.getFlagAccesso().getFlagAccesso()) {

            case FlagAccesso.STD:// COB_CODE: PERFORM A341-UPDATE-F-ACT-STD          THRU A341-EX
                a341UpdateFActStd();
                break;

            case FlagAccesso.MCRFNCT:// COB_CODE: PERFORM A342-UPDATE-F-ACT-MCRFNCT      THRU A342-EX
                a342UpdateFActMcrfnct();
                break;

            case FlagAccesso.TPMV:// COB_CODE: PERFORM A343-UPDATE-F-ACT-TPMV         THRU A343-EX
                a343UpdateFActTpmv();
                break;

            case FlagAccesso.MCRFNCT_TPMV:// COB_CODE: PERFORM A344-UPDATE-F-ACT-MCRFNCT-TPMV THRU A344-EX
                a344UpdateFActMcrfnctTpmv();
                break;

            default:break;
        }
    }

    /**Original name: A341-UPDATE-F-ACT-STD<br>
	 * <pre>*****************************************************************</pre>*/
    private void a341UpdateFActStd() {
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX
        z900ConvertiNToX();
        // COB_CODE: EXEC SQL
        //                UPDATE BTC_JOB_SCHEDULE SET
        //                  COD_ELAB_STATE       = :IABV0002-STATE-CURRENT
        //              WHERE     ID_BATCH       = :BJS-ID-BATCH
        //                    AND (
        //                         COD_ELAB_STATE IN (
        //                                         :IABV0002-STATE-01,
        //                                         :IABV0002-STATE-02,
        //                                         :IABV0002-STATE-03,
        //                                         :IABV0002-STATE-04,
        //                                         :IABV0002-STATE-05,
        //                                         :IABV0002-STATE-06,
        //                                         :IABV0002-STATE-07,
        //                                         :IABV0002-STATE-08,
        //                                         :IABV0002-STATE-09,
        //                                         :IABV0002-STATE-10
        //                                            )
        //                        )
        //           END-EXEC.
        btcJobScheduleDao.updateRec1(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              MOVE IABV0002-STATE-CURRENT TO BJS-COD-ELAB-STATE
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: MOVE IABV0002-STATE-CURRENT TO BJS-COD-ELAB-STATE
            btcJobSchedule.setBjsCodElabState(iabv0002.getIabv0002StateCurrent());
        }
    }

    /**Original name: A342-UPDATE-F-ACT-MCRFNCT<br>
	 * <pre>*****************************************************************</pre>*/
    private void a342UpdateFActMcrfnct() {
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX
        z900ConvertiNToX();
        // COB_CODE: EXEC SQL
        //                UPDATE BTC_JOB_SCHEDULE SET
        //                  COD_ELAB_STATE       = :IABV0002-STATE-CURRENT
        //              WHERE     ID_BATCH       = :BJS-ID-BATCH
        //                    AND COD_MACROFUNCT = :BJS-COD-MACROFUNCT
        //                    AND (
        //                         COD_ELAB_STATE IN (
        //                                         :IABV0002-STATE-01,
        //                                         :IABV0002-STATE-02,
        //                                         :IABV0002-STATE-03,
        //                                         :IABV0002-STATE-04,
        //                                         :IABV0002-STATE-05,
        //                                         :IABV0002-STATE-06,
        //                                         :IABV0002-STATE-07,
        //                                         :IABV0002-STATE-08,
        //                                         :IABV0002-STATE-09,
        //                                         :IABV0002-STATE-10
        //                                            )
        //                        )
        //           END-EXEC.
        btcJobScheduleDao.updateRec2(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              MOVE IABV0002-STATE-CURRENT TO BJS-COD-ELAB-STATE
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: MOVE IABV0002-STATE-CURRENT TO BJS-COD-ELAB-STATE
            btcJobSchedule.setBjsCodElabState(iabv0002.getIabv0002StateCurrent());
        }
    }

    /**Original name: A343-UPDATE-F-ACT-TPMV<br>
	 * <pre>*****************************************************************</pre>*/
    private void a343UpdateFActTpmv() {
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX
        z900ConvertiNToX();
        // COB_CODE: EXEC SQL
        //                UPDATE BTC_JOB_SCHEDULE SET
        //                  COD_ELAB_STATE       = :IABV0002-STATE-CURRENT
        //              WHERE     ID_BATCH       = :BJS-ID-BATCH
        //                    AND TP_MOVI        = :BJS-TP-MOVI
        //                    AND (
        //                         COD_ELAB_STATE IN (
        //                                         :IABV0002-STATE-01,
        //                                         :IABV0002-STATE-02,
        //                                         :IABV0002-STATE-03,
        //                                         :IABV0002-STATE-04,
        //                                         :IABV0002-STATE-05,
        //                                         :IABV0002-STATE-06,
        //                                         :IABV0002-STATE-07,
        //                                         :IABV0002-STATE-08,
        //                                         :IABV0002-STATE-09,
        //                                         :IABV0002-STATE-10
        //                                            )
        //                        )
        //           END-EXEC.
        btcJobScheduleDao.updateRec3(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              MOVE IABV0002-STATE-CURRENT TO BJS-COD-ELAB-STATE
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: MOVE IABV0002-STATE-CURRENT TO BJS-COD-ELAB-STATE
            btcJobSchedule.setBjsCodElabState(iabv0002.getIabv0002StateCurrent());
        }
    }

    /**Original name: A344-UPDATE-F-ACT-MCRFNCT-TPMV<br>
	 * <pre>*****************************************************************</pre>*/
    private void a344UpdateFActMcrfnctTpmv() {
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX
        z900ConvertiNToX();
        // COB_CODE: EXEC SQL
        //                UPDATE BTC_JOB_SCHEDULE SET
        //                  COD_ELAB_STATE       = :IABV0002-STATE-CURRENT
        //              WHERE     ID_BATCH       = :BJS-ID-BATCH
        //                    AND COD_MACROFUNCT = :BJS-COD-MACROFUNCT
        //                    AND TP_MOVI        = :BJS-TP-MOVI
        //                    AND (
        //                         COD_ELAB_STATE IN (
        //                                         :IABV0002-STATE-01,
        //                                         :IABV0002-STATE-02,
        //                                         :IABV0002-STATE-03,
        //                                         :IABV0002-STATE-04,
        //                                         :IABV0002-STATE-05,
        //                                         :IABV0002-STATE-06,
        //                                         :IABV0002-STATE-07,
        //                                         :IABV0002-STATE-08,
        //                                         :IABV0002-STATE-09,
        //                                         :IABV0002-STATE-10
        //                                            )
        //                        )
        //           END-EXEC.
        btcJobScheduleDao.updateRec4(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              MOVE IABV0002-STATE-CURRENT TO BJS-COD-ELAB-STATE
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: MOVE IABV0002-STATE-CURRENT TO BJS-COD-ELAB-STATE
            btcJobSchedule.setBjsCodElabState(iabv0002.getIabv0002StateCurrent());
        }
    }

    /**Original name: A350-UPDATE-WHERE-COND<br>
	 * <pre>*****************************************************************</pre>*/
    private void a350UpdateWhereCond() {
        // COB_CODE: IF ACCESSO-X-RANGE-NO
        //              END-EVALUATE
        //           ELSE
        //              END-EVALUATE
        //           END-IF.
        if (ws.getFlagAccessoXRange().isNo()) {
            // COB_CODE: EVALUATE TRUE
            //              WHEN ACCESSO-STD
            //                 PERFORM A351-UPD-WH-COND-STD          THRU A351-EX
            //              WHEN ACCESSO-MCRFNCT
            //                 PERFORM A352-UPD-WH-COND-MCRFNCT      THRU A352-EX
            //              WHEN ACCESSO-TPMV
            //                 PERFORM A353-UPD-WH-COND-TPMV         THRU A353-EX
            //              WHEN ACCESSO-MCRFNCT-TPMV
            //                 PERFORM A354-UPD-WH-COND-MCRFNCT-TPMV THRU A354-EX
            //           END-EVALUATE
            switch (ws.getFlagAccesso().getFlagAccesso()) {

                case FlagAccesso.STD:// COB_CODE: PERFORM A351-UPD-WH-COND-STD          THRU A351-EX
                    a351UpdWhCondStd();
                    break;

                case FlagAccesso.MCRFNCT:// COB_CODE: PERFORM A352-UPD-WH-COND-MCRFNCT      THRU A352-EX
                    a352UpdWhCondMcrfnct();
                    break;

                case FlagAccesso.TPMV:// COB_CODE: PERFORM A353-UPD-WH-COND-TPMV         THRU A353-EX
                    a353UpdWhCondTpmv();
                    break;

                case FlagAccesso.MCRFNCT_TPMV:// COB_CODE: PERFORM A354-UPD-WH-COND-MCRFNCT-TPMV THRU A354-EX
                    a354UpdWhCondMcrfnctTpmv();
                    break;

                default:break;
            }
        }
        else {
            // COB_CODE: EVALUATE TRUE
            //              WHEN ACCESSO-STD
            //                 PERFORM U351-UPD-WH-COND-STD-X-RNG    THRU U351-EX
            //              WHEN OTHER
            //                                     TO IDSV0003-DESCRIZ-ERR-DB2
            //           END-EVALUATE
            switch (ws.getFlagAccesso().getFlagAccesso()) {

                case FlagAccesso.STD:// COB_CODE: PERFORM U351-UPD-WH-COND-STD-X-RNG    THRU U351-EX
                    u351UpdWhCondStdXRng();
                    break;

                default:// COB_CODE: SET IDSV0003-GENERIC-ERROR        TO TRUE
                    idsv0003.getReturnCode().setGenericError();
                    // COB_CODE: MOVE 'TIPO DI ACCESSO NON VALIDO X MODALITA'' RANGE'
                    //                               TO IDSV0003-DESCRIZ-ERR-DB2
                    idsv0003.getCampiEsito().setDescrizErrDb2("TIPO DI ACCESSO NON VALIDO X MODALITA' RANGE");
                    break;
            }
        }
    }

    /**Original name: A351-UPD-WH-COND-STD<br>
	 * <pre>*****************************************************************</pre>*/
    private void a351UpdWhCondStd() {
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX
        z900ConvertiNToX();
        // COB_CODE: EXEC SQL
        //                UPDATE BTC_JOB_SCHEDULE SET
        //                  COD_ELAB_STATE       = :IABV0002-STATE-CURRENT
        //              WHERE     ID_BATCH       = :BJS-ID-BATCH
        //                    AND COD_ELAB_STATE IN (
        //                                         :IABV0002-STATE-01,
        //                                         :IABV0002-STATE-02,
        //                                         :IABV0002-STATE-03,
        //                                         :IABV0002-STATE-04,
        //                                         :IABV0002-STATE-05,
        //                                         :IABV0002-STATE-06,
        //                                         :IABV0002-STATE-07,
        //                                         :IABV0002-STATE-08,
        //                                         :IABV0002-STATE-09,
        //                                         :IABV0002-STATE-10
        //                                          )
        //           END-EXEC.
        btcJobScheduleDao.updateRec5(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              MOVE IABV0002-STATE-CURRENT TO BJS-COD-ELAB-STATE
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: MOVE IABV0002-STATE-CURRENT TO BJS-COD-ELAB-STATE
            btcJobSchedule.setBjsCodElabState(iabv0002.getIabv0002StateCurrent());
        }
    }

    /**Original name: A352-UPD-WH-COND-MCRFNCT<br>
	 * <pre>*****************************************************************</pre>*/
    private void a352UpdWhCondMcrfnct() {
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX
        z900ConvertiNToX();
        // COB_CODE: EXEC SQL
        //                UPDATE BTC_JOB_SCHEDULE SET
        //                  COD_ELAB_STATE       = :IABV0002-STATE-CURRENT
        //              WHERE     ID_BATCH       = :BJS-ID-BATCH
        //                    AND COD_MACROFUNCT = :BJS-COD-MACROFUNCT
        //                    AND COD_ELAB_STATE IN (
        //                                         :IABV0002-STATE-01,
        //                                         :IABV0002-STATE-02,
        //                                         :IABV0002-STATE-03,
        //                                         :IABV0002-STATE-04,
        //                                         :IABV0002-STATE-05,
        //                                         :IABV0002-STATE-06,
        //                                         :IABV0002-STATE-07,
        //                                         :IABV0002-STATE-08,
        //                                         :IABV0002-STATE-09,
        //                                         :IABV0002-STATE-10
        //                                          )
        //           END-EXEC.
        btcJobScheduleDao.updateRec6(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              MOVE IABV0002-STATE-CURRENT TO BJS-COD-ELAB-STATE
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: MOVE IABV0002-STATE-CURRENT TO BJS-COD-ELAB-STATE
            btcJobSchedule.setBjsCodElabState(iabv0002.getIabv0002StateCurrent());
        }
    }

    /**Original name: A353-UPD-WH-COND-TPMV<br>
	 * <pre>*****************************************************************</pre>*/
    private void a353UpdWhCondTpmv() {
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX
        z900ConvertiNToX();
        // COB_CODE: EXEC SQL
        //                UPDATE BTC_JOB_SCHEDULE SET
        //                  COD_ELAB_STATE       = :IABV0002-STATE-CURRENT
        //              WHERE     ID_BATCH       = :BJS-ID-BATCH
        //                    AND TP_MOVI        = :BJS-TP-MOVI
        //                    AND COD_ELAB_STATE IN (
        //                                         :IABV0002-STATE-01,
        //                                         :IABV0002-STATE-02,
        //                                         :IABV0002-STATE-03,
        //                                         :IABV0002-STATE-04,
        //                                         :IABV0002-STATE-05,
        //                                         :IABV0002-STATE-06,
        //                                         :IABV0002-STATE-07,
        //                                         :IABV0002-STATE-08,
        //                                         :IABV0002-STATE-09,
        //                                         :IABV0002-STATE-10
        //                                          )
        //           END-EXEC.
        btcJobScheduleDao.updateRec7(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              MOVE IABV0002-STATE-CURRENT TO BJS-COD-ELAB-STATE
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: MOVE IABV0002-STATE-CURRENT TO BJS-COD-ELAB-STATE
            btcJobSchedule.setBjsCodElabState(iabv0002.getIabv0002StateCurrent());
        }
    }

    /**Original name: A354-UPD-WH-COND-MCRFNCT-TPMV<br>
	 * <pre>*****************************************************************</pre>*/
    private void a354UpdWhCondMcrfnctTpmv() {
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX
        z900ConvertiNToX();
        // COB_CODE: EXEC SQL
        //                UPDATE BTC_JOB_SCHEDULE SET
        //                  COD_ELAB_STATE       = :IABV0002-STATE-CURRENT
        //              WHERE     ID_BATCH       = :BJS-ID-BATCH
        //                    AND COD_MACROFUNCT = :BJS-COD-MACROFUNCT
        //                    AND TP_MOVI        = :BJS-TP-MOVI
        //                    AND COD_ELAB_STATE IN (
        //                                         :IABV0002-STATE-01,
        //                                         :IABV0002-STATE-02,
        //                                         :IABV0002-STATE-03,
        //                                         :IABV0002-STATE-04,
        //                                         :IABV0002-STATE-05,
        //                                         :IABV0002-STATE-06,
        //                                         :IABV0002-STATE-07,
        //                                         :IABV0002-STATE-08,
        //                                         :IABV0002-STATE-09,
        //                                         :IABV0002-STATE-10
        //                                          )
        //           END-EXEC.
        btcJobScheduleDao.updateRec8(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              MOVE IABV0002-STATE-CURRENT TO BJS-COD-ELAB-STATE
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: MOVE IABV0002-STATE-CURRENT TO BJS-COD-ELAB-STATE
            btcJobSchedule.setBjsCodElabState(iabv0002.getIabv0002StateCurrent());
        }
    }

    /**Original name: A360-OPEN-CURSOR<br>
	 * <pre>*****************************************************************</pre>*/
    private void a360OpenCursor() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IABV0002-ORDER-BY-ID-JOB
        //                   PERFORM D100-DECLARE-CURSOR-IDJ THRU D100-EX
        //              WHEN IABV0002-ORDER-BY-IB-OGG
        //                   PERFORM D600-DECLARE-CURSOR-IBO THRU D600-EX
        //              WHEN OTHER
        //                   SET IDSV0003-INVALID-OPER       TO TRUE
        //           END-EVALUATE.
        switch (iabv0002.getIabv0002TipoOrderBy().getIabv0002TipoOrderBy()) {

            case Iabv0002TipoOrderBy.ID_JOB:// COB_CODE: PERFORM D100-DECLARE-CURSOR-IDJ THRU D100-EX
                d100DeclareCursorIdj();
                break;

            case Iabv0002TipoOrderBy.IB_OGG:// COB_CODE: PERFORM D600-DECLARE-CURSOR-IBO THRU D600-EX
                d600DeclareCursorIbo();
                break;

            default:// COB_CODE: SET IDSV0003-INVALID-OPER       TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
                break;
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
            a100CheckReturnCode();
        }
    }

    /**Original name: A370-CLOSE-CURSOR<br>
	 * <pre>*****************************************************************</pre>*/
    private void a370CloseCursor() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IABV0002-ORDER-BY-ID-JOB
        //                   PERFORM C100-CLOSE-IDJ THRU C100-EX
        //              WHEN IABV0002-ORDER-BY-IB-OGG
        //                   PERFORM C600-CLOSE-IBO THRU C600-EX
        //              WHEN OTHER
        //                   SET IDSV0003-INVALID-OPER       TO TRUE
        //           END-EVALUATE.
        switch (iabv0002.getIabv0002TipoOrderBy().getIabv0002TipoOrderBy()) {

            case Iabv0002TipoOrderBy.ID_JOB:// COB_CODE: PERFORM C100-CLOSE-IDJ THRU C100-EX
                c100CloseIdj();
                break;

            case Iabv0002TipoOrderBy.IB_OGG:// COB_CODE: PERFORM C600-CLOSE-IBO THRU C600-EX
                c600CloseIbo();
                break;

            default:// COB_CODE: SET IDSV0003-INVALID-OPER       TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
                break;
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
            a100CheckReturnCode();
        }
    }

    /**Original name: A380-FETCH-FIRST<br>
	 * <pre>*****************************************************************</pre>*/
    private void a380FetchFirst() {
        // COB_CODE: PERFORM A360-OPEN-CURSOR    THRU A360-EX.
        a360OpenCursor();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A390-FETCH-NEXT THRU A390-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT THRU A390-EX
            a390FetchNext();
        }
    }

    /**Original name: A390-FETCH-NEXT<br>
	 * <pre>*****************************************************************</pre>*/
    private void a390FetchNext() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IABV0002-ORDER-BY-ID-JOB
        //                   PERFORM F100-FET-NEXT-IDJ     THRU F100-EX
        //              WHEN IABV0002-ORDER-BY-IB-OGG
        //                   PERFORM F600-FET-NEXT-IBO     THRU F600-EX
        //              WHEN OTHER
        //                   SET IDSV0003-INVALID-OPER     TO TRUE
        //           END-EVALUATE.
        switch (iabv0002.getIabv0002TipoOrderBy().getIabv0002TipoOrderBy()) {

            case Iabv0002TipoOrderBy.ID_JOB:// COB_CODE: PERFORM F100-FET-NEXT-IDJ     THRU F100-EX
                f100FetNextIdj();
                break;

            case Iabv0002TipoOrderBy.IB_OGG:// COB_CODE: PERFORM F600-FET-NEXT-IBO     THRU F600-EX
                f600FetNextIbo();
                break;

            default:// COB_CODE: SET IDSV0003-INVALID-OPER     TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
                break;
        }
    }

    /**Original name: D101-DCL-CUR-IDJ-STD<br>
	 * <pre>*****************************************************************</pre>*/
    private void d101DclCurIdjStd() {
    // COB_CODE: EXEC SQL
    //              DECLARE CUR-BJS-IDJ-STD CURSOR WITH HOLD FOR
    //              SELECT
    //                ID_BATCH
    //                ,ID_JOB
    //                ,COD_ELAB_STATE
    //                ,FLAG_WARNINGS
    //                ,DT_START
    //                ,DT_END
    //                ,USER_START
    //                ,FLAG_ALWAYS_EXE
    //                ,EXECUTIONS_COUNT
    //                ,DATA_CONTENT_TYPE
    //                ,COD_MACROFUNCT
    //                ,TP_MOVI
    //                ,IB_OGG
    //                ,DATA
    //              FROM BTC_JOB_SCHEDULE
    //              WHERE     ID_BATCH              =  :BJS-ID-BATCH
    //                    AND
    //                    (
    //                          COD_ELAB_STATE IN (
    //                                         :IABV0002-STATE-01,
    //                                         :IABV0002-STATE-02,
    //                                         :IABV0002-STATE-03,
    //                                         :IABV0002-STATE-04,
    //                                         :IABV0002-STATE-05,
    //                                         :IABV0002-STATE-06,
    //                                         :IABV0002-STATE-07,
    //                                         :IABV0002-STATE-08,
    //                                         :IABV0002-STATE-09,
    //                                         :IABV0002-STATE-10
    //                                            )
    //                        )              ORDER BY ID_JOB
    //           END-EXEC.
    // DECLARE CURSOR doesn't need a translation;
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: D102-DCL-CUR-IDJ-MCRFNCT<br>
	 * <pre>*****************************************************************</pre>*/
    private void d102DclCurIdjMcrfnct() {
    // COB_CODE: EXEC SQL
    //              DECLARE CUR-BJS-IDJ-MF CURSOR WITH HOLD FOR
    //              SELECT
    //                ID_BATCH
    //                ,ID_JOB
    //                ,COD_ELAB_STATE
    //                ,FLAG_WARNINGS
    //                ,DT_START
    //                ,DT_END
    //                ,USER_START
    //                ,FLAG_ALWAYS_EXE
    //                ,EXECUTIONS_COUNT
    //                ,DATA_CONTENT_TYPE
    //                ,COD_MACROFUNCT
    //                ,TP_MOVI
    //                ,IB_OGG
    //                ,DATA
    //              FROM BTC_JOB_SCHEDULE
    //              WHERE     ID_BATCH              =  :BJS-ID-BATCH
    //                    AND COD_MACROFUNCT        =  :BJS-COD-MACROFUNCT
    //                    AND
    //                    (
    //                          COD_ELAB_STATE IN (
    //                                         :IABV0002-STATE-01,
    //                                         :IABV0002-STATE-02,
    //                                         :IABV0002-STATE-03,
    //                                         :IABV0002-STATE-04,
    //                                         :IABV0002-STATE-05,
    //                                         :IABV0002-STATE-06,
    //                                         :IABV0002-STATE-07,
    //                                         :IABV0002-STATE-08,
    //                                         :IABV0002-STATE-09,
    //                                         :IABV0002-STATE-10
    //                                            )
    //                        )              ORDER BY ID_JOB
    //           END-EXEC.
    // DECLARE CURSOR doesn't need a translation;
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: D103-DCL-CUR-IDJ-TPMV<br>
	 * <pre>*****************************************************************</pre>*/
    private void d103DclCurIdjTpmv() {
    // COB_CODE: EXEC SQL
    //              DECLARE CUR-BJS-IDJ-TM CURSOR WITH HOLD FOR
    //              SELECT
    //                ID_BATCH
    //                ,ID_JOB
    //                ,COD_ELAB_STATE
    //                ,FLAG_WARNINGS
    //                ,DT_START
    //                ,DT_END
    //                ,USER_START
    //                ,FLAG_ALWAYS_EXE
    //                ,EXECUTIONS_COUNT
    //                ,DATA_CONTENT_TYPE
    //                ,COD_MACROFUNCT
    //                ,TP_MOVI
    //                ,IB_OGG
    //                ,DATA
    //              FROM BTC_JOB_SCHEDULE
    //              WHERE     ID_BATCH              =  :BJS-ID-BATCH
    //                    AND TP_MOVI               =  :BJS-TP-MOVI
    //                    AND
    //                    (
    //                          COD_ELAB_STATE IN (
    //                                         :IABV0002-STATE-01,
    //                                         :IABV0002-STATE-02,
    //                                         :IABV0002-STATE-03,
    //                                         :IABV0002-STATE-04,
    //                                         :IABV0002-STATE-05,
    //                                         :IABV0002-STATE-06,
    //                                         :IABV0002-STATE-07,
    //                                         :IABV0002-STATE-08,
    //                                         :IABV0002-STATE-09,
    //                                         :IABV0002-STATE-10
    //                                            )
    //                        )              ORDER BY ID_JOB
    //           END-EXEC.
    // DECLARE CURSOR doesn't need a translation;
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: D104-DCL-CUR-IDJ-MCRFNCT-TPMV<br>
	 * <pre>*****************************************************************</pre>*/
    private void d104DclCurIdjMcrfnctTpmv() {
    // COB_CODE: EXEC SQL
    //              DECLARE CUR-BJS-IDJ-MF-TM CURSOR WITH HOLD FOR
    //              SELECT
    //                ID_BATCH
    //                ,ID_JOB
    //                ,COD_ELAB_STATE
    //                ,FLAG_WARNINGS
    //                ,DT_START
    //                ,DT_END
    //                ,USER_START
    //                ,FLAG_ALWAYS_EXE
    //                ,EXECUTIONS_COUNT
    //                ,DATA_CONTENT_TYPE
    //                ,COD_MACROFUNCT
    //                ,TP_MOVI
    //                ,IB_OGG
    //                ,DATA
    //              FROM BTC_JOB_SCHEDULE
    //              WHERE     ID_BATCH              =  :BJS-ID-BATCH
    //                    AND COD_MACROFUNCT        =  :BJS-COD-MACROFUNCT
    //                    AND TP_MOVI               =  :BJS-TP-MOVI
    //                    AND
    //                    (
    //                          COD_ELAB_STATE IN (
    //                                         :IABV0002-STATE-01,
    //                                         :IABV0002-STATE-02,
    //                                         :IABV0002-STATE-03,
    //                                         :IABV0002-STATE-04,
    //                                         :IABV0002-STATE-05,
    //                                         :IABV0002-STATE-06,
    //                                         :IABV0002-STATE-07,
    //                                         :IABV0002-STATE-08,
    //                                         :IABV0002-STATE-09,
    //                                         :IABV0002-STATE-10
    //                                            )
    //                        )              ORDER BY ID_JOB
    //           END-EXEC.
    // DECLARE CURSOR doesn't need a translation;
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: D601-DCL-CUR-IBO-STD<br>
	 * <pre>*****************************************************************</pre>*/
    private void d601DclCurIboStd() {
    // COB_CODE: EXEC SQL
    //              DECLARE CUR-BJS-IBO-STD CURSOR WITH HOLD FOR
    //              SELECT
    //                ID_BATCH
    //                ,ID_JOB
    //                ,COD_ELAB_STATE
    //                ,FLAG_WARNINGS
    //                ,DT_START
    //                ,DT_END
    //                ,USER_START
    //                ,FLAG_ALWAYS_EXE
    //                ,EXECUTIONS_COUNT
    //                ,DATA_CONTENT_TYPE
    //                ,COD_MACROFUNCT
    //                ,TP_MOVI
    //                ,IB_OGG
    //                ,DATA
    //              FROM BTC_JOB_SCHEDULE
    //              WHERE     ID_BATCH               = :BJS-ID-BATCH
    //                   AND
    //                    (
    //                          COD_ELAB_STATE IN (
    //                                         :IABV0002-STATE-01,
    //                                         :IABV0002-STATE-02,
    //                                         :IABV0002-STATE-03,
    //                                         :IABV0002-STATE-04,
    //                                         :IABV0002-STATE-05,
    //                                         :IABV0002-STATE-06,
    //                                         :IABV0002-STATE-07,
    //                                         :IABV0002-STATE-08,
    //                                         :IABV0002-STATE-09,
    //                                         :IABV0002-STATE-10
    //                                            )
    //                        )
    //              ORDER BY IB_OGG
    //           END-EXEC.
    // DECLARE CURSOR doesn't need a translation;
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: D602-DCL-CUR-IBO-MCRFNCT<br>
	 * <pre>*****************************************************************</pre>*/
    private void d602DclCurIboMcrfnct() {
    // COB_CODE: EXEC SQL
    //              DECLARE CUR-BJS-IBO-MF CURSOR WITH HOLD FOR
    //              SELECT
    //                ID_BATCH
    //                ,ID_JOB
    //                ,COD_ELAB_STATE
    //                ,FLAG_WARNINGS
    //                ,DT_START
    //                ,DT_END
    //                ,USER_START
    //                ,FLAG_ALWAYS_EXE
    //                ,EXECUTIONS_COUNT
    //                ,DATA_CONTENT_TYPE
    //                ,COD_MACROFUNCT
    //                ,TP_MOVI
    //                ,IB_OGG
    //                ,DATA
    //              FROM BTC_JOB_SCHEDULE
    //              WHERE     ID_BATCH               = :BJS-ID-BATCH
    //                   AND COD_MACROFUNCT          = :BJS-COD-MACROFUNCT
    //                   AND
    //                    (
    //                          COD_ELAB_STATE IN (
    //                                         :IABV0002-STATE-01,
    //                                         :IABV0002-STATE-02,
    //                                         :IABV0002-STATE-03,
    //                                         :IABV0002-STATE-04,
    //                                         :IABV0002-STATE-05,
    //                                         :IABV0002-STATE-06,
    //                                         :IABV0002-STATE-07,
    //                                         :IABV0002-STATE-08,
    //                                         :IABV0002-STATE-09,
    //                                         :IABV0002-STATE-10
    //                                            )
    //                        )
    //              ORDER BY IB_OGG
    //           END-EXEC.
    // DECLARE CURSOR doesn't need a translation;
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: D603-DCL-CUR-IBO-TPMV<br>
	 * <pre>*****************************************************************</pre>*/
    private void d603DclCurIboTpmv() {
    // COB_CODE: EXEC SQL
    //              DECLARE CUR-BJS-IBO-TM CURSOR WITH HOLD FOR
    //              SELECT
    //                ID_BATCH
    //                ,ID_JOB
    //                ,COD_ELAB_STATE
    //                ,FLAG_WARNINGS
    //                ,DT_START
    //                ,DT_END
    //                ,USER_START
    //                ,FLAG_ALWAYS_EXE
    //                ,EXECUTIONS_COUNT
    //                ,DATA_CONTENT_TYPE
    //                ,COD_MACROFUNCT
    //                ,TP_MOVI
    //                ,IB_OGG
    //                ,DATA
    //              FROM BTC_JOB_SCHEDULE
    //              WHERE     ID_BATCH               = :BJS-ID-BATCH
    //                   AND TP_MOVI                 = :BJS-TP-MOVI
    //                   AND
    //                    (
    //                          COD_ELAB_STATE IN (
    //                                         :IABV0002-STATE-01,
    //                                         :IABV0002-STATE-02,
    //                                         :IABV0002-STATE-03,
    //                                         :IABV0002-STATE-04,
    //                                         :IABV0002-STATE-05,
    //                                         :IABV0002-STATE-06,
    //                                         :IABV0002-STATE-07,
    //                                         :IABV0002-STATE-08,
    //                                         :IABV0002-STATE-09,
    //                                         :IABV0002-STATE-10
    //                                            )
    //                        )
    //              ORDER BY IB_OGG
    //           END-EXEC.
    // DECLARE CURSOR doesn't need a translation;
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: D604-DCL-CUR-IBO-MCRFNCT-TPMV<br>
	 * <pre>*****************************************************************</pre>*/
    private void d604DclCurIboMcrfnctTpmv() {
    // COB_CODE: EXEC SQL
    //              DECLARE CUR-BJS-IBO-MF-TM CURSOR WITH HOLD FOR
    //              SELECT
    //                ID_BATCH
    //                ,ID_JOB
    //                ,COD_ELAB_STATE
    //                ,FLAG_WARNINGS
    //                ,DT_START
    //                ,DT_END
    //                ,USER_START
    //                ,FLAG_ALWAYS_EXE
    //                ,EXECUTIONS_COUNT
    //                ,DATA_CONTENT_TYPE
    //                ,COD_MACROFUNCT
    //                ,TP_MOVI
    //                ,IB_OGG
    //                ,DATA
    //              FROM BTC_JOB_SCHEDULE
    //              WHERE     ID_BATCH               = :BJS-ID-BATCH
    //                   AND COD_MACROFUNCT          = :BJS-COD-MACROFUNCT
    //                   AND TP_MOVI                 = :BJS-TP-MOVI
    //                   AND
    //                    (
    //                          COD_ELAB_STATE IN (
    //                                         :IABV0002-STATE-01,
    //                                         :IABV0002-STATE-02,
    //                                         :IABV0002-STATE-03,
    //                                         :IABV0002-STATE-04,
    //                                         :IABV0002-STATE-05,
    //                                         :IABV0002-STATE-06,
    //                                         :IABV0002-STATE-07,
    //                                         :IABV0002-STATE-08,
    //                                         :IABV0002-STATE-09,
    //                                         :IABV0002-STATE-10
    //                                            )
    //                        )
    //              ORDER BY IB_OGG
    //           END-EXEC.
    // DECLARE CURSOR doesn't need a translation;
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: F100-FET-NEXT-IDJ<br>
	 * <pre>*****************************************************************</pre>*/
    private void f100FetNextIdj() {
        // COB_CODE: IF ACCESSO-X-RANGE-NO
        //              END-EVALUATE
        //           ELSE
        //              END-EVALUATE
        //           END-IF.
        if (ws.getFlagAccessoXRange().isNo()) {
            // COB_CODE: EVALUATE TRUE
            //              WHEN ACCESSO-STD
            //                 PERFORM F101-FET-NEXT-IDJ-STD          THRU F101-EX
            //              WHEN ACCESSO-MCRFNCT
            //                 PERFORM F102-FET-NEXT-IDJ-MCRFNCT      THRU F102-EX
            //              WHEN ACCESSO-TPMV
            //                 PERFORM F103-FET-NEXT-IDJ-TPMV         THRU F103-EX
            //              WHEN ACCESSO-MCRFNCT-TPMV
            //                 PERFORM F104-FET-NEXT-IDJ-MCRFNCT-TPMV THRU F104-EX
            //           END-EVALUATE
            switch (ws.getFlagAccesso().getFlagAccesso()) {

                case FlagAccesso.STD:// COB_CODE: PERFORM F101-FET-NEXT-IDJ-STD          THRU F101-EX
                    f101FetNextIdjStd();
                    break;

                case FlagAccesso.MCRFNCT:// COB_CODE: PERFORM F102-FET-NEXT-IDJ-MCRFNCT      THRU F102-EX
                    f102FetNextIdjMcrfnct();
                    break;

                case FlagAccesso.TPMV:// COB_CODE: PERFORM F103-FET-NEXT-IDJ-TPMV         THRU F103-EX
                    f103FetNextIdjTpmv();
                    break;

                case FlagAccesso.MCRFNCT_TPMV:// COB_CODE: PERFORM F104-FET-NEXT-IDJ-MCRFNCT-TPMV THRU F104-EX
                    f104FetNextIdjMcrfnctTpmv();
                    break;

                default:break;
            }
        }
        else {
            // COB_CODE: EVALUATE TRUE
            //              WHEN ACCESSO-STD
            //                 PERFORM T101-FET-NEXT-IDJ-STD-X-RNG    THRU T101-EX
            //              WHEN OTHER
            //                                     TO IDSV0003-DESCRIZ-ERR-DB2
            //           END-EVALUATE
            switch (ws.getFlagAccesso().getFlagAccesso()) {

                case FlagAccesso.STD:// COB_CODE: PERFORM T101-FET-NEXT-IDJ-STD-X-RNG    THRU T101-EX
                    t101FetNextIdjStdXRng();
                    break;

                default:// COB_CODE: SET IDSV0003-GENERIC-ERROR        TO TRUE
                    idsv0003.getReturnCode().setGenericError();
                    // COB_CODE: MOVE 'TIPO DI ACCESSO NON VALIDO X MODALITA'' RANGE'
                    //                               TO IDSV0003-DESCRIZ-ERR-DB2
                    idsv0003.getCampiEsito().setDescrizErrDb2("TIPO DI ACCESSO NON VALIDO X MODALITA' RANGE");
                    break;
            }
        }
    }

    /**Original name: F101-FET-NEXT-IDJ-STD<br>
	 * <pre>*****************************************************************</pre>*/
    private void f101FetNextIdjStd() {
        // COB_CODE: EXEC SQL
        //                FETCH CUR-BJS-IDJ-STD
        //           INTO
        //                :BJS-ID-BATCH
        //               ,:BJS-ID-JOB
        //               ,:BJS-COD-ELAB-STATE
        //               ,:BJS-FLAG-WARNINGS
        //                :IND-BJS-FLAG-WARNINGS
        //               ,:BJS-DT-START-DB
        //                :IND-BJS-DT-START
        //               ,:BJS-DT-END-DB
        //                :IND-BJS-DT-END
        //               ,:BJS-USER-START
        //                :IND-BJS-USER-START
        //               ,:BJS-FLAG-ALWAYS-EXE
        //                :IND-BJS-FLAG-ALWAYS-EXE
        //               ,:BJS-EXECUTIONS-COUNT
        //                :IND-BJS-EXECUTIONS-COUNT
        //               ,:BJS-DATA-CONTENT-TYPE
        //                :IND-BJS-DATA-CONTENT-TYPE
        //               ,:BJS-COD-MACROFUNCT
        //                :IND-BJS-COD-MACROFUNCT
        //               ,:BJS-TP-MOVI
        //                :IND-BJS-TP-MOVI
        //               ,:BJS-IB-OGG
        //                :IND-BJS-IB-OGG
        //               ,:BJS-DATA-VCHAR
        //                :IND-BJS-DATA
        //           END-EXEC.
        btcJobScheduleDao.fetchCurBjsIdjStd(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A370-CLOSE-CURSOR THRU A370-EX
            a370CloseCursor();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: F102-FET-NEXT-IDJ-MCRFNCT<br>
	 * <pre>*************************************************************</pre>*/
    private void f102FetNextIdjMcrfnct() {
        // COB_CODE: EXEC SQL
        //                FETCH CUR-BJS-IDJ-MF
        //           INTO
        //                :BJS-ID-BATCH
        //               ,:BJS-ID-JOB
        //               ,:BJS-COD-ELAB-STATE
        //               ,:BJS-FLAG-WARNINGS
        //                :IND-BJS-FLAG-WARNINGS
        //               ,:BJS-DT-START-DB
        //                :IND-BJS-DT-START
        //               ,:BJS-DT-END-DB
        //                :IND-BJS-DT-END
        //               ,:BJS-USER-START
        //                :IND-BJS-USER-START
        //               ,:BJS-FLAG-ALWAYS-EXE
        //                :IND-BJS-FLAG-ALWAYS-EXE
        //               ,:BJS-EXECUTIONS-COUNT
        //                :IND-BJS-EXECUTIONS-COUNT
        //               ,:BJS-DATA-CONTENT-TYPE
        //                :IND-BJS-DATA-CONTENT-TYPE
        //               ,:BJS-COD-MACROFUNCT
        //                :IND-BJS-COD-MACROFUNCT
        //               ,:BJS-TP-MOVI
        //                :IND-BJS-TP-MOVI
        //               ,:BJS-IB-OGG
        //                :IND-BJS-IB-OGG
        //               ,:BJS-DATA-VCHAR
        //                :IND-BJS-DATA
        //           END-EXEC.
        btcJobScheduleDao.fetchCurBjsIdjMf(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A370-CLOSE-CURSOR THRU A370-EX
            a370CloseCursor();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: F103-FET-NEXT-IDJ-TPMV<br>
	 * <pre>*****************************************************************</pre>*/
    private void f103FetNextIdjTpmv() {
        // COB_CODE: EXEC SQL
        //                FETCH CUR-BJS-IDJ-TM
        //           INTO
        //                :BJS-ID-BATCH
        //               ,:BJS-ID-JOB
        //               ,:BJS-COD-ELAB-STATE
        //               ,:BJS-FLAG-WARNINGS
        //                :IND-BJS-FLAG-WARNINGS
        //               ,:BJS-DT-START-DB
        //                :IND-BJS-DT-START
        //               ,:BJS-DT-END-DB
        //                :IND-BJS-DT-END
        //               ,:BJS-USER-START
        //                :IND-BJS-USER-START
        //               ,:BJS-FLAG-ALWAYS-EXE
        //                :IND-BJS-FLAG-ALWAYS-EXE
        //               ,:BJS-EXECUTIONS-COUNT
        //                :IND-BJS-EXECUTIONS-COUNT
        //               ,:BJS-DATA-CONTENT-TYPE
        //                :IND-BJS-DATA-CONTENT-TYPE
        //               ,:BJS-COD-MACROFUNCT
        //                :IND-BJS-COD-MACROFUNCT
        //               ,:BJS-TP-MOVI
        //                :IND-BJS-TP-MOVI
        //               ,:BJS-IB-OGG
        //                :IND-BJS-IB-OGG
        //               ,:BJS-DATA-VCHAR
        //                :IND-BJS-DATA
        //           END-EXEC.
        btcJobScheduleDao.fetchCurBjsIdjTm(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A370-CLOSE-CURSOR THRU A370-EX
            a370CloseCursor();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: F104-FET-NEXT-IDJ-MCRFNCT-TPMV<br>
	 * <pre>*****************************************************************</pre>*/
    private void f104FetNextIdjMcrfnctTpmv() {
        // COB_CODE: EXEC SQL
        //                FETCH CUR-BJS-IDJ-MF-TM
        //           INTO
        //                :BJS-ID-BATCH
        //               ,:BJS-ID-JOB
        //               ,:BJS-COD-ELAB-STATE
        //               ,:BJS-FLAG-WARNINGS
        //                :IND-BJS-FLAG-WARNINGS
        //               ,:BJS-DT-START-DB
        //                :IND-BJS-DT-START
        //               ,:BJS-DT-END-DB
        //                :IND-BJS-DT-END
        //               ,:BJS-USER-START
        //                :IND-BJS-USER-START
        //               ,:BJS-FLAG-ALWAYS-EXE
        //                :IND-BJS-FLAG-ALWAYS-EXE
        //               ,:BJS-EXECUTIONS-COUNT
        //                :IND-BJS-EXECUTIONS-COUNT
        //               ,:BJS-DATA-CONTENT-TYPE
        //                :IND-BJS-DATA-CONTENT-TYPE
        //               ,:BJS-COD-MACROFUNCT
        //                :IND-BJS-COD-MACROFUNCT
        //               ,:BJS-TP-MOVI
        //                :IND-BJS-TP-MOVI
        //               ,:BJS-IB-OGG
        //                :IND-BJS-IB-OGG
        //               ,:BJS-DATA-VCHAR
        //                :IND-BJS-DATA
        //           END-EXEC.
        btcJobScheduleDao.fetchCurBjsIdjMfTm(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A370-CLOSE-CURSOR THRU A370-EX
            a370CloseCursor();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: F600-FET-NEXT-IBO<br>
	 * <pre>*****************************************************************</pre>*/
    private void f600FetNextIbo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN ACCESSO-STD
        //                 PERFORM F601-FET-NEXT-IBO-STD          THRU F601-EX
        //              WHEN ACCESSO-MCRFNCT
        //                 PERFORM F602-FET-NEXT-IBO-MCRFNCT      THRU F602-EX
        //              WHEN ACCESSO-TPMV
        //                 PERFORM F603-FET-NEXT-IBO-TPMV         THRU F603-EX
        //              WHEN ACCESSO-MCRFNCT-TPMV
        //                 PERFORM F604-FET-NEXT-IBO-MCRFNCT-TPMV THRU F604-EX
        //           END-EVALUATE.
        switch (ws.getFlagAccesso().getFlagAccesso()) {

            case FlagAccesso.STD:// COB_CODE: PERFORM F601-FET-NEXT-IBO-STD          THRU F601-EX
                f601FetNextIboStd();
                break;

            case FlagAccesso.MCRFNCT:// COB_CODE: PERFORM F602-FET-NEXT-IBO-MCRFNCT      THRU F602-EX
                f602FetNextIboMcrfnct();
                break;

            case FlagAccesso.TPMV:// COB_CODE: PERFORM F603-FET-NEXT-IBO-TPMV         THRU F603-EX
                f603FetNextIboTpmv();
                break;

            case FlagAccesso.MCRFNCT_TPMV:// COB_CODE: PERFORM F604-FET-NEXT-IBO-MCRFNCT-TPMV THRU F604-EX
                f604FetNextIboMcrfnctTpmv();
                break;

            default:break;
        }
    }

    /**Original name: F601-FET-NEXT-IBO-STD<br>
	 * <pre>*****************************************************************</pre>*/
    private void f601FetNextIboStd() {
        // COB_CODE: EXEC SQL
        //                FETCH CUR-BJS-IBO-STD
        //           INTO
        //                :BJS-ID-BATCH
        //               ,:BJS-ID-JOB
        //               ,:BJS-COD-ELAB-STATE
        //               ,:BJS-FLAG-WARNINGS
        //                :IND-BJS-FLAG-WARNINGS
        //               ,:BJS-DT-START-DB
        //                :IND-BJS-DT-START
        //               ,:BJS-DT-END-DB
        //                :IND-BJS-DT-END
        //               ,:BJS-USER-START
        //                :IND-BJS-USER-START
        //               ,:BJS-FLAG-ALWAYS-EXE
        //                :IND-BJS-FLAG-ALWAYS-EXE
        //               ,:BJS-EXECUTIONS-COUNT
        //                :IND-BJS-EXECUTIONS-COUNT
        //               ,:BJS-DATA-CONTENT-TYPE
        //                :IND-BJS-DATA-CONTENT-TYPE
        //               ,:BJS-COD-MACROFUNCT
        //                :IND-BJS-COD-MACROFUNCT
        //               ,:BJS-TP-MOVI
        //                :IND-BJS-TP-MOVI
        //               ,:BJS-IB-OGG
        //                :IND-BJS-IB-OGG
        //               ,:BJS-DATA-VCHAR
        //                :IND-BJS-DATA
        //           END-EXEC.
        btcJobScheduleDao.fetchCurBjsIboStd(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A370-CLOSE-CURSOR THRU A370-EX
            a370CloseCursor();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: F602-FET-NEXT-IBO-MCRFNCT<br>
	 * <pre>*****************************************************************</pre>*/
    private void f602FetNextIboMcrfnct() {
        // COB_CODE: EXEC SQL
        //                FETCH CUR-BJS-IBO-MF
        //           INTO
        //                :BJS-ID-BATCH
        //               ,:BJS-ID-JOB
        //               ,:BJS-COD-ELAB-STATE
        //               ,:BJS-FLAG-WARNINGS
        //                :IND-BJS-FLAG-WARNINGS
        //               ,:BJS-DT-START-DB
        //                :IND-BJS-DT-START
        //               ,:BJS-DT-END-DB
        //                :IND-BJS-DT-END
        //               ,:BJS-USER-START
        //                :IND-BJS-USER-START
        //               ,:BJS-FLAG-ALWAYS-EXE
        //                :IND-BJS-FLAG-ALWAYS-EXE
        //               ,:BJS-EXECUTIONS-COUNT
        //                :IND-BJS-EXECUTIONS-COUNT
        //               ,:BJS-DATA-CONTENT-TYPE
        //                :IND-BJS-DATA-CONTENT-TYPE
        //               ,:BJS-COD-MACROFUNCT
        //                :IND-BJS-COD-MACROFUNCT
        //               ,:BJS-TP-MOVI
        //                :IND-BJS-TP-MOVI
        //               ,:BJS-IB-OGG
        //                :IND-BJS-IB-OGG
        //               ,:BJS-DATA-VCHAR
        //                :IND-BJS-DATA
        //           END-EXEC.
        btcJobScheduleDao.fetchCurBjsIboMf(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A370-CLOSE-CURSOR THRU A370-EX
            a370CloseCursor();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: F603-FET-NEXT-IBO-TPMV<br>
	 * <pre>*****************************************************************</pre>*/
    private void f603FetNextIboTpmv() {
        // COB_CODE: EXEC SQL
        //                FETCH CUR-BJS-IBO-TM
        //           INTO
        //                :BJS-ID-BATCH
        //               ,:BJS-ID-JOB
        //               ,:BJS-COD-ELAB-STATE
        //               ,:BJS-FLAG-WARNINGS
        //                :IND-BJS-FLAG-WARNINGS
        //               ,:BJS-DT-START-DB
        //                :IND-BJS-DT-START
        //               ,:BJS-DT-END-DB
        //                :IND-BJS-DT-END
        //               ,:BJS-USER-START
        //                :IND-BJS-USER-START
        //               ,:BJS-FLAG-ALWAYS-EXE
        //                :IND-BJS-FLAG-ALWAYS-EXE
        //               ,:BJS-EXECUTIONS-COUNT
        //                :IND-BJS-EXECUTIONS-COUNT
        //               ,:BJS-DATA-CONTENT-TYPE
        //                :IND-BJS-DATA-CONTENT-TYPE
        //               ,:BJS-COD-MACROFUNCT
        //                :IND-BJS-COD-MACROFUNCT
        //               ,:BJS-TP-MOVI
        //                :IND-BJS-TP-MOVI
        //               ,:BJS-IB-OGG
        //                :IND-BJS-IB-OGG
        //               ,:BJS-DATA-VCHAR
        //                :IND-BJS-DATA
        //           END-EXEC.
        btcJobScheduleDao.fetchCurBjsIboTm(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A370-CLOSE-CURSOR THRU A370-EX
            a370CloseCursor();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: F604-FET-NEXT-IBO-MCRFNCT-TPMV<br>
	 * <pre>*****************************************************************</pre>*/
    private void f604FetNextIboMcrfnctTpmv() {
        // COB_CODE: EXEC SQL
        //                FETCH CUR-BJS-IBO-MF-TM
        //           INTO
        //                :BJS-ID-BATCH
        //               ,:BJS-ID-JOB
        //               ,:BJS-COD-ELAB-STATE
        //               ,:BJS-FLAG-WARNINGS
        //                :IND-BJS-FLAG-WARNINGS
        //               ,:BJS-DT-START-DB
        //                :IND-BJS-DT-START
        //               ,:BJS-DT-END-DB
        //                :IND-BJS-DT-END
        //               ,:BJS-USER-START
        //                :IND-BJS-USER-START
        //               ,:BJS-FLAG-ALWAYS-EXE
        //                :IND-BJS-FLAG-ALWAYS-EXE
        //               ,:BJS-EXECUTIONS-COUNT
        //                :IND-BJS-EXECUTIONS-COUNT
        //               ,:BJS-DATA-CONTENT-TYPE
        //                :IND-BJS-DATA-CONTENT-TYPE
        //               ,:BJS-COD-MACROFUNCT
        //                :IND-BJS-COD-MACROFUNCT
        //               ,:BJS-TP-MOVI
        //                :IND-BJS-TP-MOVI
        //               ,:BJS-IB-OGG
        //                :IND-BJS-IB-OGG
        //               ,:BJS-DATA-VCHAR
        //                :IND-BJS-DATA
        //           END-EXEC.
        btcJobScheduleDao.fetchCurBjsIboMfTm(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A370-CLOSE-CURSOR THRU A370-EX
            a370CloseCursor();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: R311-SELECT-STD-X-RNG<br>
	 * <pre>*****************************************************************</pre>*/
    private void r311SelectStdXRng() {
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_BATCH
        //                ,ID_JOB
        //                ,COD_ELAB_STATE
        //                ,FLAG_WARNINGS
        //                ,DT_START
        //                ,DT_END
        //                ,USER_START
        //                ,FLAG_ALWAYS_EXE
        //                ,EXECUTIONS_COUNT
        //                ,DATA_CONTENT_TYPE
        //                ,COD_MACROFUNCT
        //                ,TP_MOVI
        //                ,IB_OGG
        //                ,DATA
        //           INTO
        //                :BJS-ID-BATCH
        //               ,:BJS-ID-JOB
        //               ,:BJS-COD-ELAB-STATE
        //               ,:BJS-FLAG-WARNINGS
        //                :IND-BJS-FLAG-WARNINGS
        //               ,:BJS-DT-START-DB
        //                :IND-BJS-DT-START
        //               ,:BJS-DT-END-DB
        //                :IND-BJS-DT-END
        //               ,:BJS-USER-START
        //                :IND-BJS-USER-START
        //               ,:BJS-FLAG-ALWAYS-EXE
        //                :IND-BJS-FLAG-ALWAYS-EXE
        //               ,:BJS-EXECUTIONS-COUNT
        //                :IND-BJS-EXECUTIONS-COUNT
        //               ,:BJS-DATA-CONTENT-TYPE
        //                :IND-BJS-DATA-CONTENT-TYPE
        //               ,:BJS-COD-MACROFUNCT
        //                :IND-BJS-COD-MACROFUNCT
        //               ,:BJS-TP-MOVI
        //                :IND-BJS-TP-MOVI
        //               ,:BJS-IB-OGG
        //                :IND-BJS-IB-OGG
        //               ,:BJS-DATA-VCHAR
        //                :IND-BJS-DATA
        //              FROM BTC_JOB_SCHEDULE
        //              WHERE     ID_BATCH        = :BJS-ID-BATCH
        //                    AND
        //                        ID_JOB BETWEEN :IABV0009-ID-OGG-DA AND
        //                                       :IABV0009-ID-OGG-A
        //                    AND
        //                    (
        //                          COD_ELAB_STATE IN (
        //                                         :IABV0002-STATE-01,
        //                                         :IABV0002-STATE-02,
        //                                         :IABV0002-STATE-03,
        //                                         :IABV0002-STATE-04,
        //                                         :IABV0002-STATE-05,
        //                                         :IABV0002-STATE-06,
        //                                         :IABV0002-STATE-07,
        //                                         :IABV0002-STATE-08,
        //                                         :IABV0002-STATE-09,
        //                                         :IABV0002-STATE-10
        //                                            )
        //                        )
        //              FETCH FIRST ROW ONLY
        //           END-EXEC.
        btcJobScheduleDao.selectRec5(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: S101-DCL-IDJ-STD-X-RNG<br>
	 * <pre>*****************************************************************</pre>*/
    private void s101DclIdjStdXRng() {
    // COB_CODE: EXEC SQL
    //              DECLARE CUR-IDJ-STD-X-RNG CURSOR WITH HOLD FOR
    //              SELECT
    //                ID_BATCH
    //                ,ID_JOB
    //                ,COD_ELAB_STATE
    //                ,FLAG_WARNINGS
    //                ,DT_START
    //                ,DT_END
    //                ,USER_START
    //                ,FLAG_ALWAYS_EXE
    //                ,EXECUTIONS_COUNT
    //                ,DATA_CONTENT_TYPE
    //                ,COD_MACROFUNCT
    //                ,TP_MOVI
    //                ,IB_OGG
    //                ,DATA
    //              FROM BTC_JOB_SCHEDULE
    //              WHERE     ID_BATCH              =  :BJS-ID-BATCH
    //                    AND
    //                        ID_JOB BETWEEN :IABV0009-ID-OGG-DA AND
    //                                       :IABV0009-ID-OGG-A
    //                    AND
    //                    (
    //                          COD_ELAB_STATE IN (
    //                                         :IABV0002-STATE-01,
    //                                         :IABV0002-STATE-02,
    //                                         :IABV0002-STATE-03,
    //                                         :IABV0002-STATE-04,
    //                                         :IABV0002-STATE-05,
    //                                         :IABV0002-STATE-06,
    //                                         :IABV0002-STATE-07,
    //                                         :IABV0002-STATE-08,
    //                                         :IABV0002-STATE-09,
    //                                         :IABV0002-STATE-10
    //                                            )
    //                        )              ORDER BY ID_JOB
    //           END-EXEC.
    // DECLARE CURSOR doesn't need a translation;
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: T101-FET-NEXT-IDJ-STD-X-RNG<br>
	 * <pre>*****************************************************************</pre>*/
    private void t101FetNextIdjStdXRng() {
        // COB_CODE: EXEC SQL
        //                FETCH CUR-IDJ-STD-X-RNG
        //           INTO
        //                :BJS-ID-BATCH
        //               ,:BJS-ID-JOB
        //               ,:BJS-COD-ELAB-STATE
        //               ,:BJS-FLAG-WARNINGS
        //                :IND-BJS-FLAG-WARNINGS
        //               ,:BJS-DT-START-DB
        //                :IND-BJS-DT-START
        //               ,:BJS-DT-END-DB
        //                :IND-BJS-DT-END
        //               ,:BJS-USER-START
        //                :IND-BJS-USER-START
        //               ,:BJS-FLAG-ALWAYS-EXE
        //                :IND-BJS-FLAG-ALWAYS-EXE
        //               ,:BJS-EXECUTIONS-COUNT
        //                :IND-BJS-EXECUTIONS-COUNT
        //               ,:BJS-DATA-CONTENT-TYPE
        //                :IND-BJS-DATA-CONTENT-TYPE
        //               ,:BJS-COD-MACROFUNCT
        //                :IND-BJS-COD-MACROFUNCT
        //               ,:BJS-TP-MOVI
        //                :IND-BJS-TP-MOVI
        //               ,:BJS-IB-OGG
        //                :IND-BJS-IB-OGG
        //               ,:BJS-DATA-VCHAR
        //                :IND-BJS-DATA
        //           END-EXEC.
        btcJobScheduleDao.fetchCurIdjStdXRng(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A370-CLOSE-CURSOR THRU A370-EX
            a370CloseCursor();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: U351-UPD-WH-COND-STD-X-RNG<br>
	 * <pre>*****************************************************************</pre>*/
    private void u351UpdWhCondStdXRng() {
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX
        z900ConvertiNToX();
        // COB_CODE: EXEC SQL
        //                UPDATE BTC_JOB_SCHEDULE SET
        //                  COD_ELAB_STATE       = :IABV0002-STATE-CURRENT
        //              WHERE     ID_BATCH       = :BJS-ID-BATCH
        //                    AND
        //                      ID_JOB   BETWEEN :IABV0009-ID-OGG-DA AND
        //                                       :IABV0009-ID-OGG-A
        //                    AND
        //                     COD_ELAB_STATE IN (
        //                                         :IABV0002-STATE-01,
        //                                         :IABV0002-STATE-02,
        //                                         :IABV0002-STATE-03,
        //                                         :IABV0002-STATE-04,
        //                                         :IABV0002-STATE-05,
        //                                         :IABV0002-STATE-06,
        //                                         :IABV0002-STATE-07,
        //                                         :IABV0002-STATE-08,
        //                                         :IABV0002-STATE-09,
        //                                         :IABV0002-STATE-10
        //                                          )
        //           END-EXEC.
        btcJobScheduleDao.updateRec9(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              MOVE IABV0002-STATE-CURRENT TO BJS-COD-ELAB-STATE
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: MOVE IABV0002-STATE-CURRENT TO BJS-COD-ELAB-STATE
            btcJobSchedule.setBjsCodElabState(iabv0002.getIabv0002StateCurrent());
        }
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>*****************************************************************</pre>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-BJS-FLAG-WARNINGS = -1
        //              MOVE HIGH-VALUES TO BJS-FLAG-WARNINGS-NULL
        //           END-IF
        if (ws.getIndBtcJobSchedule().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BJS-FLAG-WARNINGS-NULL
            btcJobSchedule.setBjsFlagWarnings(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-BJS-DT-START = -1
        //              MOVE HIGH-VALUES TO BJS-DT-START-NULL
        //           END-IF
        if (ws.getIndBtcJobSchedule().getCodParam() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BJS-DT-START-NULL
            btcJobSchedule.getBjsDtStart().setBjsDtStartNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BjsDtStart.Len.BJS_DT_START_NULL));
        }
        // COB_CODE: IF IND-BJS-DT-END = -1
        //              MOVE HIGH-VALUES TO BJS-DT-END-NULL
        //           END-IF
        if (ws.getIndBtcJobSchedule().getTpParam() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BJS-DT-END-NULL
            btcJobSchedule.getBjsDtEnd().setBjsDtEndNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BjsDtEnd.Len.BJS_DT_END_NULL));
        }
        // COB_CODE: IF IND-BJS-USER-START = -1
        //              MOVE HIGH-VALUES TO BJS-USER-START-NULL
        //           END-IF
        if (ws.getIndBtcJobSchedule().getTpD() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BJS-USER-START-NULL
            btcJobSchedule.setBjsUserStart(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BtcJobSchedule.Len.BJS_USER_START));
        }
        // COB_CODE: IF IND-BJS-FLAG-ALWAYS-EXE = -1
        //              MOVE HIGH-VALUES TO BJS-FLAG-ALWAYS-EXE-NULL
        //           END-IF
        if (ws.getIndBtcJobSchedule().getValImp() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BJS-FLAG-ALWAYS-EXE-NULL
            btcJobSchedule.setBjsFlagAlwaysExe(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-BJS-EXECUTIONS-COUNT = -1
        //              MOVE HIGH-VALUES TO BJS-EXECUTIONS-COUNT-NULL
        //           END-IF
        if (ws.getIndBtcJobSchedule().getValDt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BJS-EXECUTIONS-COUNT-NULL
            btcJobSchedule.getBjsExecutionsCount().setBjsExecutionsCountNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BjsExecutionsCount.Len.BJS_EXECUTIONS_COUNT_NULL));
        }
        // COB_CODE: IF IND-BJS-DATA-CONTENT-TYPE = -1
        //              MOVE HIGH-VALUES TO BJS-DATA-CONTENT-TYPE-NULL
        //           END-IF
        if (ws.getIndBtcJobSchedule().getValTs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BJS-DATA-CONTENT-TYPE-NULL
            btcJobSchedule.setBjsDataContentType(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BtcJobSchedule.Len.BJS_DATA_CONTENT_TYPE));
        }
        // COB_CODE: IF IND-BJS-COD-MACROFUNCT = -1
        //              MOVE HIGH-VALUES TO BJS-COD-MACROFUNCT-NULL
        //           END-IF
        if (ws.getIndBtcJobSchedule().getValTxt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BJS-COD-MACROFUNCT-NULL
            btcJobSchedule.setBjsCodMacrofunct(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BtcJobSchedule.Len.BJS_COD_MACROFUNCT));
        }
        // COB_CODE: IF IND-BJS-TP-MOVI = -1
        //              MOVE HIGH-VALUES TO BJS-TP-MOVI-NULL
        //           END-IF
        if (ws.getIndBtcJobSchedule().getValFl() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BJS-TP-MOVI-NULL
            btcJobSchedule.getBjsTpMovi().setBjsTpMoviNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BjsTpMovi.Len.BJS_TP_MOVI_NULL));
        }
        // COB_CODE: IF IND-BJS-IB-OGG = -1
        //              MOVE HIGH-VALUES TO BJS-IB-OGG-NULL
        //           END-IF
        if (ws.getIndBtcJobSchedule().getValNum() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BJS-IB-OGG-NULL
            btcJobSchedule.setBjsIbOgg(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BtcJobSchedule.Len.BJS_IB_OGG));
        }
        // COB_CODE: IF IND-BJS-DATA = -1
        //              MOVE HIGH-VALUES TO BJS-DATA
        //           END-IF.
        if (ws.getIndBtcJobSchedule().getValPc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BJS-DATA
            btcJobSchedule.setBjsData(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BtcJobSchedule.Len.BJS_DATA));
        }
    }

    /**Original name: Z150-VALORIZZA-DATA-SERVICES<br>
	 * <pre>*****************************************************************</pre>*/
    private void z150ValorizzaDataServices() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z200-SET-INDICATORI-NULL<br>
	 * <pre>*****************************************************************</pre>*/
    private void z200SetIndicatoriNull() {
        // COB_CODE: IF BJS-FLAG-WARNINGS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BJS-FLAG-WARNINGS
        //           ELSE
        //              MOVE 0 TO IND-BJS-FLAG-WARNINGS
        //           END-IF
        if (Conditions.eq(btcJobSchedule.getBjsFlagWarnings(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-BJS-FLAG-WARNINGS
            ws.getIndBtcJobSchedule().setIdMoviChiu(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BJS-FLAG-WARNINGS
            ws.getIndBtcJobSchedule().setIdMoviChiu(((short)0));
        }
        // COB_CODE: IF BJS-DT-START-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BJS-DT-START
        //           ELSE
        //              MOVE 0 TO IND-BJS-DT-START
        //           END-IF
        if (Characters.EQ_HIGH.test(btcJobSchedule.getBjsDtStart().getBjsDtStartNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BJS-DT-START
            ws.getIndBtcJobSchedule().setCodParam(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BJS-DT-START
            ws.getIndBtcJobSchedule().setCodParam(((short)0));
        }
        // COB_CODE: IF BJS-DT-END-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BJS-DT-END
        //           ELSE
        //              MOVE 0 TO IND-BJS-DT-END
        //           END-IF
        if (Characters.EQ_HIGH.test(btcJobSchedule.getBjsDtEnd().getBjsDtEndNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BJS-DT-END
            ws.getIndBtcJobSchedule().setTpParam(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BJS-DT-END
            ws.getIndBtcJobSchedule().setTpParam(((short)0));
        }
        // COB_CODE: IF BJS-USER-START-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BJS-USER-START
        //           ELSE
        //              MOVE 0 TO IND-BJS-USER-START
        //           END-IF
        if (Characters.EQ_HIGH.test(btcJobSchedule.getBjsUserStart(), BtcJobSchedule.Len.BJS_USER_START)) {
            // COB_CODE: MOVE -1 TO IND-BJS-USER-START
            ws.getIndBtcJobSchedule().setTpD(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BJS-USER-START
            ws.getIndBtcJobSchedule().setTpD(((short)0));
        }
        // COB_CODE: IF BJS-FLAG-ALWAYS-EXE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BJS-FLAG-ALWAYS-EXE
        //           ELSE
        //              MOVE 0 TO IND-BJS-FLAG-ALWAYS-EXE
        //           END-IF
        if (Conditions.eq(btcJobSchedule.getBjsFlagAlwaysExe(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-BJS-FLAG-ALWAYS-EXE
            ws.getIndBtcJobSchedule().setValImp(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BJS-FLAG-ALWAYS-EXE
            ws.getIndBtcJobSchedule().setValImp(((short)0));
        }
        // COB_CODE: IF BJS-EXECUTIONS-COUNT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BJS-EXECUTIONS-COUNT
        //           ELSE
        //              MOVE 0 TO IND-BJS-EXECUTIONS-COUNT
        //           END-IF
        if (Characters.EQ_HIGH.test(btcJobSchedule.getBjsExecutionsCount().getBjsExecutionsCountNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BJS-EXECUTIONS-COUNT
            ws.getIndBtcJobSchedule().setValDt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BJS-EXECUTIONS-COUNT
            ws.getIndBtcJobSchedule().setValDt(((short)0));
        }
        // COB_CODE: IF BJS-DATA-CONTENT-TYPE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BJS-DATA-CONTENT-TYPE
        //           ELSE
        //              MOVE 0 TO IND-BJS-DATA-CONTENT-TYPE
        //           END-IF
        if (Characters.EQ_HIGH.test(btcJobSchedule.getBjsDataContentType(), BtcJobSchedule.Len.BJS_DATA_CONTENT_TYPE)) {
            // COB_CODE: MOVE -1 TO IND-BJS-DATA-CONTENT-TYPE
            ws.getIndBtcJobSchedule().setValTs(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BJS-DATA-CONTENT-TYPE
            ws.getIndBtcJobSchedule().setValTs(((short)0));
        }
        // COB_CODE: IF BJS-COD-MACROFUNCT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BJS-COD-MACROFUNCT
        //           ELSE
        //              MOVE 0 TO IND-BJS-COD-MACROFUNCT
        //           END-IF
        if (Characters.EQ_HIGH.test(btcJobSchedule.getBjsCodMacrofunctFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BJS-COD-MACROFUNCT
            ws.getIndBtcJobSchedule().setValTxt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BJS-COD-MACROFUNCT
            ws.getIndBtcJobSchedule().setValTxt(((short)0));
        }
        // COB_CODE: IF BJS-TP-MOVI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BJS-TP-MOVI
        //           ELSE
        //              MOVE 0 TO IND-BJS-TP-MOVI
        //           END-IF
        if (Characters.EQ_HIGH.test(btcJobSchedule.getBjsTpMovi().getBjsTpMoviNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BJS-TP-MOVI
            ws.getIndBtcJobSchedule().setValFl(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BJS-TP-MOVI
            ws.getIndBtcJobSchedule().setValFl(((short)0));
        }
        // COB_CODE: IF BJS-IB-OGG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BJS-IB-OGG
        //           ELSE
        //              MOVE 0 TO IND-BJS-IB-OGG
        //           END-IF
        if (Characters.EQ_HIGH.test(btcJobSchedule.getBjsIbOgg(), BtcJobSchedule.Len.BJS_IB_OGG)) {
            // COB_CODE: MOVE -1 TO IND-BJS-IB-OGG
            ws.getIndBtcJobSchedule().setValNum(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BJS-IB-OGG
            ws.getIndBtcJobSchedule().setValNum(((short)0));
        }
        // COB_CODE: IF BJS-DATA = HIGH-VALUES
        //              MOVE -1 TO IND-BJS-DATA
        //           ELSE
        //              MOVE 0 TO IND-BJS-DATA
        //           END-IF.
        if (Characters.EQ_HIGH.test(btcJobSchedule.getBjsData(), BtcJobSchedule.Len.BJS_DATA)) {
            // COB_CODE: MOVE -1 TO IND-BJS-DATA
            ws.getIndBtcJobSchedule().setValPc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BJS-DATA
            ws.getIndBtcJobSchedule().setValPc(((short)0));
        }
    }

    /**Original name: Z400-SEQ<br>*/
    private void z400Seq() {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_ID_JOB
        //              INTO :BJS-ID-JOB
        //           END-EXEC.
        //TODO: Implement: valuesStmt;
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: Z900-CONVERTI-N-TO-X<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da 9(8) comp-3 a date
	 * ----</pre>*/
    private void z900ConvertiNToX() {
        // COB_CODE: IF IND-BJS-DT-START = 0
        //               MOVE WS-TIMESTAMP-X      TO BJS-DT-START-DB
        //           END-IF
        if (ws.getIndBtcJobSchedule().getCodParam() == 0) {
            // COB_CODE: MOVE BJS-DT-START TO WS-TIMESTAMP-N
            ws.getIdsv0010().setWsTimestampN(TruncAbs.toLong(btcJobSchedule.getBjsDtStart().getBjsDtStart(), 18));
            // COB_CODE: PERFORM Z701-TS-N-TO-X   THRU Z701-EX
            z701TsNToX();
            // COB_CODE: MOVE WS-TIMESTAMP-X      TO BJS-DT-START-DB
            ws.getIdbvbjs3().setBjsDtStartDb(ws.getIdsv0010().getWsTimestampX());
        }
        // COB_CODE: IF IND-BJS-DT-END = 0
        //               MOVE WS-TIMESTAMP-X      TO BJS-DT-END-DB
        //           END-IF.
        if (ws.getIndBtcJobSchedule().getTpParam() == 0) {
            // COB_CODE: MOVE BJS-DT-END TO WS-TIMESTAMP-N
            ws.getIdsv0010().setWsTimestampN(TruncAbs.toLong(btcJobSchedule.getBjsDtEnd().getBjsDtEnd(), 18));
            // COB_CODE: PERFORM Z701-TS-N-TO-X   THRU Z701-EX
            z701TsNToX();
            // COB_CODE: MOVE WS-TIMESTAMP-X      TO BJS-DT-END-DB
            ws.getIdbvbjs3().setBjsDtEndDb(ws.getIdsv0010().getWsTimestampX());
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: IF IND-BJS-DT-START = 0
        //               MOVE WS-TIMESTAMP-N      TO BJS-DT-START
        //           END-IF
        if (ws.getIndBtcJobSchedule().getCodParam() == 0) {
            // COB_CODE: MOVE BJS-DT-START-DB TO WS-TIMESTAMP-X
            ws.getIdsv0010().setWsTimestampX(ws.getIdbvbjs3().getBjsDtStartDb());
            // COB_CODE: PERFORM Z801-TS-X-TO-N     THRU Z801-EX
            z801TsXToN();
            // COB_CODE: MOVE WS-TIMESTAMP-N      TO BJS-DT-START
            btcJobSchedule.getBjsDtStart().setBjsDtStart(ws.getIdsv0010().getWsTimestampN());
        }
        // COB_CODE: IF IND-BJS-DT-END = 0
        //               MOVE WS-TIMESTAMP-N      TO BJS-DT-END
        //           END-IF.
        if (ws.getIndBtcJobSchedule().getTpParam() == 0) {
            // COB_CODE: MOVE BJS-DT-END-DB TO WS-TIMESTAMP-X
            ws.getIdsv0010().setWsTimestampX(ws.getIdbvbjs3().getBjsDtEndDb());
            // COB_CODE: PERFORM Z801-TS-X-TO-N     THRU Z801-EX
            z801TsXToN();
            // COB_CODE: MOVE WS-TIMESTAMP-N      TO BJS-DT-END
            btcJobSchedule.getBjsDtEnd().setBjsDtEnd(ws.getIdsv0010().getWsTimestampN());
        }
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
        // COB_CODE: IF BJS-DATA-LEN NOT > ZEROES
        //                          TO BJS-DATA-LEN
        //           END-IF.
        if (btcJobSchedule.getBjsDataLen() <= 0) {
            // COB_CODE: MOVE LENGTH OF BJS-DATA
            //                       TO BJS-DATA-LEN
            btcJobSchedule.setBjsDataLen(((short)BtcJobSchedule.Len.BJS_DATA));
        }
    }

    /**Original name: C100-CLOSE-IDJ<br>
	 * <pre>*****************************************************************</pre>*/
    private void c100CloseIdj() {
        // COB_CODE: IF ACCESSO-X-RANGE-NO
        //              END-EVALUATE
        //           ELSE
        //              END-EVALUATE
        //           END-IF.
        if (ws.getFlagAccessoXRange().isNo()) {
            // COB_CODE: EVALUATE TRUE
            //              WHEN ACCESSO-STD
            //                 END-EXEC
            //              WHEN ACCESSO-MCRFNCT
            //                 END-EXEC
            //              WHEN ACCESSO-TPMV
            //                 END-EXEC
            //              WHEN ACCESSO-MCRFNCT-TPMV
            //                 END-EXEC
            //           END-EVALUATE
            switch (ws.getFlagAccesso().getFlagAccesso()) {

                case FlagAccesso.STD:// COB_CODE: EXEC SQL
                    //                CLOSE CUR-BJS-IDJ-STD
                    //           END-EXEC
                    btcJobScheduleDao.closeCurBjsIdjStd();
                    break;

                case FlagAccesso.MCRFNCT:// COB_CODE: EXEC SQL
                    //                CLOSE CUR-BJS-IDJ-MF
                    //           END-EXEC
                    btcJobScheduleDao.closeCurBjsIdjMf();
                    break;

                case FlagAccesso.TPMV:// COB_CODE: EXEC SQL
                    //                CLOSE CUR-BJS-IDJ-TM
                    //           END-EXEC
                    btcJobScheduleDao.closeCurBjsIdjTm();
                    break;

                case FlagAccesso.MCRFNCT_TPMV:// COB_CODE: EXEC SQL
                    //                CLOSE CUR-BJS-IDJ-MF-TM
                    //           END-EXEC
                    btcJobScheduleDao.closeCurBjsIdjMfTm();
                    break;

                default:break;
            }
        }
        else {
            // COB_CODE: EVALUATE TRUE
            //              WHEN ACCESSO-STD
            //                 END-EXEC
            //              WHEN OTHER
            //                                     TO IDSV0003-DESCRIZ-ERR-DB2
            //           END-EVALUATE
            switch (ws.getFlagAccesso().getFlagAccesso()) {

                case FlagAccesso.STD:// COB_CODE: EXEC SQL
                    //                CLOSE CUR-IDJ-STD-X-RNG
                    //           END-EXEC
                    btcJobScheduleDao.closeCurIdjStdXRng();
                    break;

                default:// COB_CODE: SET IDSV0003-GENERIC-ERROR        TO TRUE
                    idsv0003.getReturnCode().setGenericError();
                    // COB_CODE: MOVE 'TIPO DI ACCESSO NON VALIDO X MODALITA'' RANGE'
                    //                               TO IDSV0003-DESCRIZ-ERR-DB2
                    idsv0003.getCampiEsito().setDescrizErrDb2("TIPO DI ACCESSO NON VALIDO X MODALITA' RANGE");
                    break;
            }
        }
    }

    /**Original name: C600-CLOSE-IBO<br>
	 * <pre>*****************************************************************</pre>*/
    private void c600CloseIbo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN ACCESSO-STD
        //                 END-EXEC
        //              WHEN ACCESSO-MCRFNCT
        //                 END-EXEC
        //              WHEN ACCESSO-TPMV
        //                 END-EXEC
        //              WHEN ACCESSO-MCRFNCT-TPMV
        //                 END-EXEC
        //           END-EVALUATE.
        switch (ws.getFlagAccesso().getFlagAccesso()) {

            case FlagAccesso.STD:// COB_CODE: EXEC SQL
                //                CLOSE CUR-BJS-IBO-STD
                //           END-EXEC
                btcJobScheduleDao.closeCurBjsIboStd();
                break;

            case FlagAccesso.MCRFNCT:// COB_CODE: EXEC SQL
                //                CLOSE CUR-BJS-IBO-MF
                //           END-EXEC
                btcJobScheduleDao.closeCurBjsIboMf();
                break;

            case FlagAccesso.TPMV:// COB_CODE: EXEC SQL
                //                CLOSE CUR-BJS-IBO-TM
                //           END-EXEC
                btcJobScheduleDao.closeCurBjsIboTm();
                break;

            case FlagAccesso.MCRFNCT_TPMV:// COB_CODE: EXEC SQL
                //                CLOSE CUR-BJS-IBO-MF-TM
                //           END-EXEC
                btcJobScheduleDao.closeCurBjsIboMfTm();
                break;

            default:break;
        }
    }

    /**Original name: D100-DECLARE-CURSOR-IDJ<br>
	 * <pre>*****************************************************************</pre>*/
    private void d100DeclareCursorIdj() {
        // COB_CODE: IF ACCESSO-X-RANGE-NO
        //              END-EVALUATE
        //           ELSE
        //              END-EVALUATE
        //           END-IF.
        if (ws.getFlagAccessoXRange().isNo()) {
            // COB_CODE: EVALUATE TRUE
            //              WHEN ACCESSO-STD
            //                 END-EXEC
            //              WHEN ACCESSO-MCRFNCT
            //                 END-EXEC
            //              WHEN ACCESSO-TPMV
            //                 END-EXEC
            //              WHEN ACCESSO-MCRFNCT-TPMV
            //                 END-EXEC
            //           END-EVALUATE
            switch (ws.getFlagAccesso().getFlagAccesso()) {

                case FlagAccesso.STD:// COB_CODE: PERFORM D101-DCL-CUR-IDJ-STD     THRU D101-EX
                    d101DclCurIdjStd();
                    // COB_CODE: EXEC SQL
                    //                OPEN CUR-BJS-IDJ-STD
                    //           END-EXEC
                    btcJobScheduleDao.openCurBjsIdjStd(this);
                    break;

                case FlagAccesso.MCRFNCT:// COB_CODE: PERFORM D102-DCL-CUR-IDJ-MCRFNCT THRU D102-EX
                    d102DclCurIdjMcrfnct();
                    // COB_CODE: EXEC SQL
                    //                OPEN CUR-BJS-IDJ-MF
                    //           END-EXEC
                    btcJobScheduleDao.openCurBjsIdjMf(this);
                    break;

                case FlagAccesso.TPMV:// COB_CODE: PERFORM D103-DCL-CUR-IDJ-TPMV    THRU D103-EX
                    d103DclCurIdjTpmv();
                    // COB_CODE: EXEC SQL
                    //                OPEN CUR-BJS-IDJ-TM
                    //           END-EXEC
                    btcJobScheduleDao.openCurBjsIdjTm(this);
                    break;

                case FlagAccesso.MCRFNCT_TPMV:// COB_CODE: PERFORM D104-DCL-CUR-IDJ-MCRFNCT-TPMV
                    //                                            THRU D104-EX
                    d104DclCurIdjMcrfnctTpmv();
                    // COB_CODE: EXEC SQL
                    //                OPEN CUR-BJS-IDJ-MF-TM
                    //           END-EXEC
                    btcJobScheduleDao.openCurBjsIdjMfTm(this);
                    break;

                default:break;
            }
        }
        else {
            // COB_CODE: EVALUATE TRUE
            //              WHEN ACCESSO-STD
            //                 END-EXEC
            //              WHEN OTHER
            //                                     TO IDSV0003-DESCRIZ-ERR-DB2
            //           END-EVALUATE
            switch (ws.getFlagAccesso().getFlagAccesso()) {

                case FlagAccesso.STD:// COB_CODE: PERFORM S101-DCL-IDJ-STD-X-RNG   THRU S101-EX
                    s101DclIdjStdXRng();
                    // COB_CODE: EXEC SQL
                    //                OPEN CUR-IDJ-STD-X-RNG
                    //           END-EXEC
                    btcJobScheduleDao.openCurIdjStdXRng(this);
                    break;

                default:// COB_CODE: SET IDSV0003-GENERIC-ERROR        TO TRUE
                    idsv0003.getReturnCode().setGenericError();
                    // COB_CODE: MOVE 'TIPO DI ACCESSO NON VALIDO X MODALITA'' RANGE'
                    //                               TO IDSV0003-DESCRIZ-ERR-DB2
                    idsv0003.getCampiEsito().setDescrizErrDb2("TIPO DI ACCESSO NON VALIDO X MODALITA' RANGE");
                    break;
            }
        }
    }

    /**Original name: D600-DECLARE-CURSOR-IBO<br>
	 * <pre>*****************************************************************</pre>*/
    private void d600DeclareCursorIbo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN ACCESSO-STD
        //                 END-EXEC
        //              WHEN ACCESSO-MCRFNCT
        //                 END-EXEC
        //              WHEN ACCESSO-TPMV
        //                 END-EXEC
        //              WHEN ACCESSO-MCRFNCT-TPMV
        //                 END-EXEC
        //           END-EVALUATE.
        switch (ws.getFlagAccesso().getFlagAccesso()) {

            case FlagAccesso.STD:// COB_CODE: PERFORM D601-DCL-CUR-IBO-STD          THRU D601-EX
                d601DclCurIboStd();
                // COB_CODE: EXEC SQL
                //                OPEN CUR-BJS-IBO-STD
                //           END-EXEC
                btcJobScheduleDao.openCurBjsIboStd(this);
                break;

            case FlagAccesso.MCRFNCT:// COB_CODE: PERFORM D602-DCL-CUR-IBO-MCRFNCT      THRU D602-EX
                d602DclCurIboMcrfnct();
                // COB_CODE: EXEC SQL
                //                OPEN CUR-BJS-IBO-MF
                //           END-EXEC
                btcJobScheduleDao.openCurBjsIboMf(this);
                break;

            case FlagAccesso.TPMV:// COB_CODE: PERFORM D603-DCL-CUR-IBO-TPMV         THRU D603-EX
                d603DclCurIboTpmv();
                // COB_CODE: EXEC SQL
                //                OPEN CUR-BJS-IBO-TM
                //           END-EXEC
                btcJobScheduleDao.openCurBjsIboTm(this);
                break;

            case FlagAccesso.MCRFNCT_TPMV:// COB_CODE: PERFORM D604-DCL-CUR-IBO-MCRFNCT-TPMV THRU D604-EX
                d604DclCurIboMcrfnctTpmv();
                // COB_CODE: EXEC SQL
                //                OPEN CUR-BJS-IBO-MF-TM
                //           END-EXEC
                btcJobScheduleDao.openCurBjsIboMfTm(this);
                break;

            default:break;
        }
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IDSP0003:line=25, because the code is unreachable.
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IDSP0003:line=33, because the code is unreachable.
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z701-TS-N-TO-X<br>*/
    private void z701TsNToX() {
        // COB_CODE: MOVE WS-STR-TIMESTAMP-N(1:4)
        //                TO WS-TIMESTAMP-X(1:4)
        ws.getIdsv0010().setWsTimestampX(Functions.setSubstring(ws.getIdsv0010().getWsTimestampX(), ws.getIdsv0010().getWsStrTimestampNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-TIMESTAMP-N(5:2)
        //                TO WS-TIMESTAMP-X(6:2)
        ws.getIdsv0010().setWsTimestampX(Functions.setSubstring(ws.getIdsv0010().getWsTimestampX(), ws.getIdsv0010().getWsStrTimestampNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-TIMESTAMP-N(7:2)
        //                TO WS-TIMESTAMP-X(9:2)
        ws.getIdsv0010().setWsTimestampX(Functions.setSubstring(ws.getIdsv0010().getWsTimestampX(), ws.getIdsv0010().getWsStrTimestampNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE WS-STR-TIMESTAMP-N(9:2)
        //                TO WS-TIMESTAMP-X(12:2)
        ws.getIdsv0010().setWsTimestampX(Functions.setSubstring(ws.getIdsv0010().getWsTimestampX(), ws.getIdsv0010().getWsStrTimestampNFormatted().substring((9) - 1, 10), 12, 2));
        // COB_CODE: MOVE WS-STR-TIMESTAMP-N(11:2)
        //                TO WS-TIMESTAMP-X(15:2)
        ws.getIdsv0010().setWsTimestampX(Functions.setSubstring(ws.getIdsv0010().getWsTimestampX(), ws.getIdsv0010().getWsStrTimestampNFormatted().substring((11) - 1, 12), 15, 2));
        // COB_CODE: MOVE WS-STR-TIMESTAMP-N(13:2)
        //                TO WS-TIMESTAMP-X(18:2)
        ws.getIdsv0010().setWsTimestampX(Functions.setSubstring(ws.getIdsv0010().getWsTimestampX(), ws.getIdsv0010().getWsStrTimestampNFormatted().substring((13) - 1, 14), 18, 2));
        // COB_CODE: MOVE WS-STR-TIMESTAMP-N(15:4)
        //                TO WS-TIMESTAMP-X(21:4)
        ws.getIdsv0010().setWsTimestampX(Functions.setSubstring(ws.getIdsv0010().getWsTimestampX(), ws.getIdsv0010().getWsStrTimestampNFormatted().substring((15) - 1, 18), 21, 4));
        // COB_CODE: MOVE '00'
        //                TO WS-TIMESTAMP-X(25:2)
        ws.getIdsv0010().setWsTimestampX(Functions.setSubstring(ws.getIdsv0010().getWsTimestampX(), "00", 25, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-TIMESTAMP-X(5:1)
        //                   WS-TIMESTAMP-X(8:1)
        ws.getIdsv0010().setWsTimestampX(Functions.setSubstring(ws.getIdsv0010().getWsTimestampX(), "-", 5, 1));
        ws.getIdsv0010().setWsTimestampX(Functions.setSubstring(ws.getIdsv0010().getWsTimestampX(), "-", 8, 1));
        // COB_CODE: MOVE SPACE
        //                TO WS-TIMESTAMP-X(11:1)
        ws.getIdsv0010().setWsTimestampX(Functions.setSubstring(ws.getIdsv0010().getWsTimestampX(), LiteralGenerator.create(Types.SPACE_CHAR, 1), 11, 1));
        // COB_CODE: MOVE ':'
        //                TO WS-TIMESTAMP-X(14:1)
        //                   WS-TIMESTAMP-X(17:1)
        ws.getIdsv0010().setWsTimestampX(Functions.setSubstring(ws.getIdsv0010().getWsTimestampX(), ":", 14, 1));
        ws.getIdsv0010().setWsTimestampX(Functions.setSubstring(ws.getIdsv0010().getWsTimestampX(), ":", 17, 1));
        // COB_CODE: MOVE '.'
        //                TO WS-TIMESTAMP-X(20:1).
        ws.getIdsv0010().setWsTimestampX(Functions.setSubstring(ws.getIdsv0010().getWsTimestampX(), ".", 20, 1));
    }

    /**Original name: Z801-TS-X-TO-N<br>*/
    private void z801TsXToN() {
        // COB_CODE: MOVE WS-TIMESTAMP-X(1:4)
        //                TO WS-STR-TIMESTAMP-N(1:4)
        ws.getIdsv0010().setWsStrTimestampNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrTimestampNFormatted(), ws.getIdsv0010().getWsTimestampXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-TIMESTAMP-X(6:2)
        //                TO WS-STR-TIMESTAMP-N(5:2)
        ws.getIdsv0010().setWsStrTimestampNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrTimestampNFormatted(), ws.getIdsv0010().getWsTimestampXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-TIMESTAMP-X(9:2)
        //                TO WS-STR-TIMESTAMP-N(7:2)
        ws.getIdsv0010().setWsStrTimestampNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrTimestampNFormatted(), ws.getIdsv0010().getWsTimestampXFormatted().substring((9) - 1, 10), 7, 2));
        // COB_CODE: MOVE WS-TIMESTAMP-X(12:2)
        //                TO WS-STR-TIMESTAMP-N(9:2)
        ws.getIdsv0010().setWsStrTimestampNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrTimestampNFormatted(), ws.getIdsv0010().getWsTimestampXFormatted().substring((12) - 1, 13), 9, 2));
        // COB_CODE: MOVE WS-TIMESTAMP-X(15:2)
        //                TO WS-STR-TIMESTAMP-N(11:2)
        ws.getIdsv0010().setWsStrTimestampNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrTimestampNFormatted(), ws.getIdsv0010().getWsTimestampXFormatted().substring((15) - 1, 16), 11, 2));
        // COB_CODE: MOVE WS-TIMESTAMP-X(18:2)
        //                TO WS-STR-TIMESTAMP-N(13:2)
        ws.getIdsv0010().setWsStrTimestampNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrTimestampNFormatted(), ws.getIdsv0010().getWsTimestampXFormatted().substring((18) - 1, 19), 13, 2));
        // COB_CODE: MOVE WS-TIMESTAMP-X(21:4)
        //                TO WS-STR-TIMESTAMP-N(15:4).
        ws.getIdsv0010().setWsStrTimestampNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrTimestampNFormatted(), ws.getIdsv0010().getWsTimestampXFormatted().substring((21) - 1, 24), 15, 4));
    }

    @Override
    public String getBjsCodMacrofunct() {
        return btcJobSchedule.getBjsCodMacrofunct();
    }

    @Override
    public void setBjsCodMacrofunct(String bjsCodMacrofunct) {
        this.btcJobSchedule.setBjsCodMacrofunct(bjsCodMacrofunct);
    }

    @Override
    public String getBjsCodMacrofunctObj() {
        if (ws.getIndBtcJobSchedule().getValTxt() >= 0) {
            return getBjsCodMacrofunct();
        }
        else {
            return null;
        }
    }

    @Override
    public void setBjsCodMacrofunctObj(String bjsCodMacrofunctObj) {
        if (bjsCodMacrofunctObj != null) {
            setBjsCodMacrofunct(bjsCodMacrofunctObj);
            ws.getIndBtcJobSchedule().setValTxt(((short)0));
        }
        else {
            ws.getIndBtcJobSchedule().setValTxt(((short)-1));
        }
    }

    @Override
    public int getBjsIdBatch() {
        return btcJobSchedule.getBjsIdBatch();
    }

    @Override
    public void setBjsIdBatch(int bjsIdBatch) {
        this.btcJobSchedule.setBjsIdBatch(bjsIdBatch);
    }

    @Override
    public int getBjsTpMovi() {
        return btcJobSchedule.getBjsTpMovi().getBjsTpMovi();
    }

    @Override
    public void setBjsTpMovi(int bjsTpMovi) {
        this.btcJobSchedule.getBjsTpMovi().setBjsTpMovi(bjsTpMovi);
    }

    @Override
    public Integer getBjsTpMoviObj() {
        if (ws.getIndBtcJobSchedule().getValFl() >= 0) {
            return ((Integer)getBjsTpMovi());
        }
        else {
            return null;
        }
    }

    @Override
    public void setBjsTpMoviObj(Integer bjsTpMoviObj) {
        if (bjsTpMoviObj != null) {
            setBjsTpMovi(((int)bjsTpMoviObj));
            ws.getIndBtcJobSchedule().setValFl(((short)0));
        }
        else {
            ws.getIndBtcJobSchedule().setValFl(((short)-1));
        }
    }

    @Override
    public char getCodElabState() {
        return btcJobSchedule.getBjsCodElabState();
    }

    @Override
    public void setCodElabState(char codElabState) {
        this.btcJobSchedule.setBjsCodElabState(codElabState);
    }

    @Override
    public String getDataContentType() {
        return btcJobSchedule.getBjsDataContentType();
    }

    @Override
    public void setDataContentType(String dataContentType) {
        this.btcJobSchedule.setBjsDataContentType(dataContentType);
    }

    @Override
    public String getDataContentTypeObj() {
        if (ws.getIndBtcJobSchedule().getValTs() >= 0) {
            return getDataContentType();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDataContentTypeObj(String dataContentTypeObj) {
        if (dataContentTypeObj != null) {
            setDataContentType(dataContentTypeObj);
            ws.getIndBtcJobSchedule().setValTs(((short)0));
        }
        else {
            ws.getIndBtcJobSchedule().setValTs(((short)-1));
        }
    }

    @Override
    public String getDataVchar() {
        return btcJobSchedule.getBjsDataVcharFormatted();
    }

    @Override
    public void setDataVchar(String dataVchar) {
        this.btcJobSchedule.setBjsDataVcharFormatted(dataVchar);
    }

    @Override
    public String getDataVcharObj() {
        if (ws.getIndBtcJobSchedule().getValPc() >= 0) {
            return getDataVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDataVcharObj(String dataVcharObj) {
        if (dataVcharObj != null) {
            setDataVchar(dataVcharObj);
            ws.getIndBtcJobSchedule().setValPc(((short)0));
        }
        else {
            ws.getIndBtcJobSchedule().setValPc(((short)-1));
        }
    }

    @Override
    public String getDtEndDb() {
        return ws.getIdbvbjs3().getBjsDtEndDb();
    }

    @Override
    public void setDtEndDb(String dtEndDb) {
        this.ws.getIdbvbjs3().setBjsDtEndDb(dtEndDb);
    }

    @Override
    public String getDtEndDbObj() {
        if (ws.getIndBtcJobSchedule().getTpParam() >= 0) {
            return getDtEndDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtEndDbObj(String dtEndDbObj) {
        if (dtEndDbObj != null) {
            setDtEndDb(dtEndDbObj);
            ws.getIndBtcJobSchedule().setTpParam(((short)0));
        }
        else {
            ws.getIndBtcJobSchedule().setTpParam(((short)-1));
        }
    }

    @Override
    public String getDtStartDb() {
        return ws.getIdbvbjs3().getBjsDtStartDb();
    }

    @Override
    public void setDtStartDb(String dtStartDb) {
        this.ws.getIdbvbjs3().setBjsDtStartDb(dtStartDb);
    }

    @Override
    public String getDtStartDbObj() {
        if (ws.getIndBtcJobSchedule().getCodParam() >= 0) {
            return getDtStartDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtStartDbObj(String dtStartDbObj) {
        if (dtStartDbObj != null) {
            setDtStartDb(dtStartDbObj);
            ws.getIndBtcJobSchedule().setCodParam(((short)0));
        }
        else {
            ws.getIndBtcJobSchedule().setCodParam(((short)-1));
        }
    }

    @Override
    public int getExecutionsCount() {
        return btcJobSchedule.getBjsExecutionsCount().getBjsExecutionsCount();
    }

    @Override
    public void setExecutionsCount(int executionsCount) {
        this.btcJobSchedule.getBjsExecutionsCount().setBjsExecutionsCount(executionsCount);
    }

    @Override
    public Integer getExecutionsCountObj() {
        if (ws.getIndBtcJobSchedule().getValDt() >= 0) {
            return ((Integer)getExecutionsCount());
        }
        else {
            return null;
        }
    }

    @Override
    public void setExecutionsCountObj(Integer executionsCountObj) {
        if (executionsCountObj != null) {
            setExecutionsCount(((int)executionsCountObj));
            ws.getIndBtcJobSchedule().setValDt(((short)0));
        }
        else {
            ws.getIndBtcJobSchedule().setValDt(((short)-1));
        }
    }

    @Override
    public char getFlagAlwaysExe() {
        return btcJobSchedule.getBjsFlagAlwaysExe();
    }

    @Override
    public void setFlagAlwaysExe(char flagAlwaysExe) {
        this.btcJobSchedule.setBjsFlagAlwaysExe(flagAlwaysExe);
    }

    @Override
    public Character getFlagAlwaysExeObj() {
        if (ws.getIndBtcJobSchedule().getValImp() >= 0) {
            return ((Character)getFlagAlwaysExe());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlagAlwaysExeObj(Character flagAlwaysExeObj) {
        if (flagAlwaysExeObj != null) {
            setFlagAlwaysExe(((char)flagAlwaysExeObj));
            ws.getIndBtcJobSchedule().setValImp(((short)0));
        }
        else {
            ws.getIndBtcJobSchedule().setValImp(((short)-1));
        }
    }

    @Override
    public char getFlagWarnings() {
        return btcJobSchedule.getBjsFlagWarnings();
    }

    @Override
    public void setFlagWarnings(char flagWarnings) {
        this.btcJobSchedule.setBjsFlagWarnings(flagWarnings);
    }

    @Override
    public Character getFlagWarningsObj() {
        if (ws.getIndBtcJobSchedule().getIdMoviChiu() >= 0) {
            return ((Character)getFlagWarnings());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlagWarningsObj(Character flagWarningsObj) {
        if (flagWarningsObj != null) {
            setFlagWarnings(((char)flagWarningsObj));
            ws.getIndBtcJobSchedule().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndBtcJobSchedule().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public char getIabv0002State01() {
        return iabv0002.getIabv0002StateGlobal().getState01();
    }

    @Override
    public void setIabv0002State01(char iabv0002State01) {
        this.iabv0002.getIabv0002StateGlobal().setState01(iabv0002State01);
    }

    @Override
    public char getIabv0002State02() {
        return iabv0002.getIabv0002StateGlobal().getState02();
    }

    @Override
    public void setIabv0002State02(char iabv0002State02) {
        this.iabv0002.getIabv0002StateGlobal().setState02(iabv0002State02);
    }

    @Override
    public char getIabv0002State03() {
        return iabv0002.getIabv0002StateGlobal().getState03();
    }

    @Override
    public void setIabv0002State03(char iabv0002State03) {
        this.iabv0002.getIabv0002StateGlobal().setState03(iabv0002State03);
    }

    @Override
    public char getIabv0002State04() {
        return iabv0002.getIabv0002StateGlobal().getState04();
    }

    @Override
    public void setIabv0002State04(char iabv0002State04) {
        this.iabv0002.getIabv0002StateGlobal().setState04(iabv0002State04);
    }

    @Override
    public char getIabv0002State05() {
        return iabv0002.getIabv0002StateGlobal().getState05();
    }

    @Override
    public void setIabv0002State05(char iabv0002State05) {
        this.iabv0002.getIabv0002StateGlobal().setState05(iabv0002State05);
    }

    @Override
    public char getIabv0002State06() {
        return iabv0002.getIabv0002StateGlobal().getState06();
    }

    @Override
    public void setIabv0002State06(char iabv0002State06) {
        this.iabv0002.getIabv0002StateGlobal().setState06(iabv0002State06);
    }

    @Override
    public char getIabv0002State07() {
        return iabv0002.getIabv0002StateGlobal().getState07();
    }

    @Override
    public void setIabv0002State07(char iabv0002State07) {
        this.iabv0002.getIabv0002StateGlobal().setState07(iabv0002State07);
    }

    @Override
    public char getIabv0002State08() {
        return iabv0002.getIabv0002StateGlobal().getState08();
    }

    @Override
    public void setIabv0002State08(char iabv0002State08) {
        this.iabv0002.getIabv0002StateGlobal().setState08(iabv0002State08);
    }

    @Override
    public char getIabv0002State09() {
        return iabv0002.getIabv0002StateGlobal().getState09();
    }

    @Override
    public void setIabv0002State09(char iabv0002State09) {
        this.iabv0002.getIabv0002StateGlobal().setState09(iabv0002State09);
    }

    @Override
    public char getIabv0002State10() {
        return iabv0002.getIabv0002StateGlobal().getState10();
    }

    @Override
    public void setIabv0002State10(char iabv0002State10) {
        this.iabv0002.getIabv0002StateGlobal().setState10(iabv0002State10);
    }

    @Override
    public char getIabv0002StateCurrent() {
        return iabv0002.getIabv0002StateCurrent();
    }

    @Override
    public void setIabv0002StateCurrent(char iabv0002StateCurrent) {
        this.iabv0002.setIabv0002StateCurrent(iabv0002StateCurrent);
    }

    @Override
    public int getIabv0009IdOggA() {
        return iabv0002.getIabv0009GestGuideService().getIdOggA();
    }

    @Override
    public void setIabv0009IdOggA(int iabv0009IdOggA) {
        this.iabv0002.getIabv0009GestGuideService().setIdOggA(iabv0009IdOggA);
    }

    @Override
    public int getIabv0009IdOggDa() {
        return iabv0002.getIabv0009GestGuideService().getIdOggDa();
    }

    @Override
    public void setIabv0009IdOggDa(int iabv0009IdOggDa) {
        this.iabv0002.getIabv0009GestGuideService().setIdOggDa(iabv0009IdOggDa);
    }

    @Override
    public String getIbOgg() {
        return btcJobSchedule.getBjsIbOgg();
    }

    @Override
    public void setIbOgg(String ibOgg) {
        this.btcJobSchedule.setBjsIbOgg(ibOgg);
    }

    @Override
    public String getIbOggObj() {
        if (ws.getIndBtcJobSchedule().getValNum() >= 0) {
            return getIbOgg();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIbOggObj(String ibOggObj) {
        if (ibOggObj != null) {
            setIbOgg(ibOggObj);
            ws.getIndBtcJobSchedule().setValNum(((short)0));
        }
        else {
            ws.getIndBtcJobSchedule().setValNum(((short)-1));
        }
    }

    @Override
    public int getIdJob() {
        return btcJobSchedule.getBjsIdJob();
    }

    @Override
    public void setIdJob(int idJob) {
        this.btcJobSchedule.setBjsIdJob(idJob);
    }

    @Override
    public String getUserStart() {
        return btcJobSchedule.getBjsUserStart();
    }

    @Override
    public void setUserStart(String userStart) {
        this.btcJobSchedule.setBjsUserStart(userStart);
    }

    @Override
    public String getUserStartObj() {
        if (ws.getIndBtcJobSchedule().getTpD() >= 0) {
            return getUserStart();
        }
        else {
            return null;
        }
    }

    @Override
    public void setUserStartObj(String userStartObj) {
        if (userStartObj != null) {
            setUserStart(userStartObj);
            ws.getIndBtcJobSchedule().setTpD(((short)0));
        }
        else {
            ws.getIndBtcJobSchedule().setTpD(((short)-1));
        }
    }
}
