package it.accenture.jnais;

import com.bphx.ctu.af.core.buffer.BasicBytesClass;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.pointer.IPointerManager;
import com.bphx.ctu.af.core.program.DynamicCall;
import com.bphx.ctu.af.core.program.GenericParam;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.lang.util.MathUtil;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.MathFunctions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Ivvc0211CampiEsito;
import it.accenture.jnais.copy.Ivvc0211RestoDati;
import it.accenture.jnais.copy.Ivvc0216;
import it.accenture.jnais.copy.Ivvc0217;
import it.accenture.jnais.copy.Ivvc0222;
import it.accenture.jnais.copy.Ivvc0223;
import it.accenture.jnais.copy.Ivvc0224;
import it.accenture.jnais.copy.Lccv0021AreaOutput;
import it.accenture.jnais.ws.AreaBusinessIvvs0211;
import it.accenture.jnais.ws.AreaInput;
import it.accenture.jnais.ws.ComodoStatCaus;
import it.accenture.jnais.ws.enums.FlagGestione;
import it.accenture.jnais.ws.enums.FlagTipoGaranzia;
import it.accenture.jnais.ws.enums.FlagTipologiaScheda;
import it.accenture.jnais.ws.enums.FlagTipoTranche;
import it.accenture.jnais.ws.enums.FlagVersione;
import it.accenture.jnais.ws.enums.Idsv0003Sqlcode;
import it.accenture.jnais.ws.enums.WsTpTrch;
import it.accenture.jnais.ws.InputIvvs0211;
import it.accenture.jnais.ws.Ivvc0215;
import it.accenture.jnais.ws.Ivvs0211Data;
import it.accenture.jnais.ws.Lccv0021;
import it.accenture.jnais.ws.MatrValVarLdbs1400;
import it.accenture.jnais.ws.occurs.ComodoStatBusEle;
import it.accenture.jnais.ws.occurs.ComodoTpCausEle;
import it.accenture.jnais.ws.occurs.Ivvc0222TabTran;
import it.accenture.jnais.ws.occurs.Ivvc0223TabLivelliGar;
import it.accenture.jnais.ws.occurs.Ivvv0212CtrlAutOper;
import it.accenture.jnais.ws.occurs.WopzOpzioni;
import it.accenture.jnais.ws.ptr.Idsv8888StrPerformanceDbg;
import it.accenture.jnais.ws.redefines.C216TabValP;
import it.accenture.jnais.ws.redefines.C216TabValT;
import it.accenture.jnais.ws.redefines.Ldbv1351StatBusTab;
import it.accenture.jnais.ws.redefines.Ldbv1351TpCausTab;
import it.accenture.jnais.ws.redefines.UnzipTab;
import it.accenture.jnais.ws.redefines.WcntTabVar;
import it.accenture.jnais.ws.redefines.WdeqTab;
import it.accenture.jnais.ws.redefines.Wl19Tabella;
import it.accenture.jnais.ws.redefines.WocoTab;
import it.accenture.jnais.ws.redefines.WpmoTab;
import it.accenture.jnais.ws.redefines.WpogTab;
import it.accenture.jnais.ws.redefines.WrdfTabella;
import it.accenture.jnais.ws.redefines.WrifTabella;
import it.accenture.jnais.ws.redefines.WtgaTab;
import it.accenture.jnais.ws.redefines.WtliTab;
import it.accenture.jnais.ws.UnzipStructure;
import it.accenture.jnais.ws.VarFunzDiCalcR;
import javax.inject.Inject;
import static com.bphx.ctu.af.lang.AfSystem.rTrim;

/**Original name: IVVS0211<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2007.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *     PROGRAMMA...... IVVS0211
 *     TIPOLOGIA...... TRASVERSALE
 *     PROCESSO....... XXX
 *     FUNZIONE....... VALORIZZATORE VARIABILI
 *     DESCRIZIONE.... MAIN GESTORE VARIABILI
 * **------------------------------------------------------------***</pre>*/
public class Ivvs0211 extends Program {

    //==== PROPERTIES ====
    @Inject
    private IPointerManager pointerManager;
    //Original name: WORKING-STORAGE
    private Ivvs0211Data ws = new Ivvs0211Data();
    //Original name: INPUT-IVVS0211
    private InputIvvs0211 inputIvvs0211;

    //==== METHODS ====
    /**Original name: PROGRAM_IVVS0211_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(InputIvvs0211 inputIvvs0211) {
        this.inputIvvs0211 = inputIvvs0211;
        // COB_CODE: PERFORM A000-OPERAZIONI-INIZIALI THRU A000-EX.
        a000OperazioniIniziali();
        // COB_CODE: IF IVVC0211-SUCCESSFUL-RC
        //              PERFORM A100-ELABORAZIONE     THRU A100-EX
        //           END-IF.
        if (this.inputIvvs0211.getIvvc0211RestoDati().getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A100-ELABORAZIONE     THRU A100-EX
            a100Elaborazione();
        }
        // COB_CODE: PERFORM A900-OPERAZIONI-FINALI   THRU A900-EX.
        a900OperazioniFinali();
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Ivvs0211 getInstance() {
        return ((Ivvs0211)Programs.getInstance(Ivvs0211.class));
    }

    /**Original name: A000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *     OPERAZIONI INIZIALI
	 * ----------------------------------------------------------------*</pre>*/
    private void a000OperazioniIniziali() {
        // COB_CODE: PERFORM A001-INITIALIZE         THRU A001-EX.
        a001Initialize();
        // COB_CODE: PERFORM A002-SET                THRU A002-EX.
        a002Set();
        // COB_CODE: PERFORM A003-ZERO               THRU A003-EX.
        a003Zero();
        // COB_CODE: PERFORM A004-SPACE              THRU A004-EX.
        a004Space();
        // COB_CODE: PERFORM A005-CTRL-DATI-INPUT    THRU A005-EX.
        a005CtrlDatiInput();
        // COB_CODE: IF IVVC0211-SUCCESSFUL-RC
        //              PERFORM I000-VALORIZZA-IDSV0003 THRU I000-EX
        //           END-IF.
        if (inputIvvs0211.getIvvc0211RestoDati().getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM I000-VALORIZZA-IDSV0003 THRU I000-EX
            i000ValorizzaIdsv0003();
        }
    }

    /**Original name: A001-INITIALIZE<br>
	 * <pre>----------------------------------------------------------------*
	 *     INITIALIZE
	 * ----------------------------------------------------------------*</pre>*/
    private void a001Initialize() {
        // COB_CODE: SET  IDSV8888-BUSINESS-DBG          TO TRUE
        ws.getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
        // COB_CODE: MOVE 'IVVS0211'                 TO IDSV8888-NOME-PGM.
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("IVVS0211");
        // COB_CODE: SET  IDSV8888-INIZIO            TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setInizio();
        // COB_CODE: MOVE SPACES                     TO IDSV8888-DESC-PGM
        //skipped translation for moving SPACES to IDSV8888-DESC-PGM; considered in STRING statement translation below
        // COB_CODE: STRING 'Iniz. area di working'
        //                   DELIMITED BY SIZE
        //                   INTO IDSV8888-DESC-PGM
        //           END-STRING
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("Iniz. area di working");
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //                                    AREA-BUSINESS
        //                                    AREA-IVVC0216
        // COB_CODE:      INITIALIZE                      IX-INDICI
        //           *                                    AREA-BUSINESS
        //                                                WADE-AREA-ADESIONE
        //                                                WBEP-AREA-BENEF
        //                                                WBEL-AREA-BENEF-LIQ
        //                                                WDCO-AREA-DT-COLLETTIVA
        //                                                WDFA-AREA-DT-FISC-ADES
        //                                                WDEQ-AREA-DETT-QUEST
        //                                                WDTC-AREA-DETT-TIT-CONT
        //                                                WGRZ-AREA-GARANZIA
        //                                                WGRL-AREA-GARANZIA-LIQ
        //                                                WISO-AREA-IMPOSTA-SOST
        //                                                WLQU-AREA-LIQUIDAZIONE
        //                                                WMOV-AREA-MOVIMENTO
        //                                                WMFZ-AREA-MOVI-FINRIO
        //                                                WPOG-AREA-PARAM-OGG
        //                                                WPMO-AREA-PARAM-MOV
        //                                                WPLI-AREA-PERC-LIQ
        //                                                WPOL-AREA-POLIZZA
        //                                                WPRE-AREA-PRESTITI
        //                                                WPVT-AREA-PROV
        //                                                WQUE-AREA-QUEST
        //                                                WRIC-AREA-RICH
        //                                                WRDF-AREA-RICH-DISINV-FND
        //                                                WRIF-AREA-RICH-INV-FND
        //                                                WRDF-ELE-RIC-INV-MAX
        //                                                WRIF-ELE-RIC-INV-MAX
        //                                                WRAN-AREA-RAPP-ANAG
        //                                                WE15-AREA-EST-RAPP-ANAG
        //                                                WRRE-AREA-RAPP-RETE
        //                                                WSPG-AREA-SOPRAP-GAR
        //                                                WSDI-AREA-STRA-INV
        //                                                WTIT-AREA-TIT-CONT
        //                                                WTCL-AREA-TIT-LIQ
        //                                                WTGA-AREA-TRANCHE
        //                                                WDAD-AREA-DEFAULT-ADES
        //                                                WOCO-AREA-OGG-COLL
        //                                                WDFL-AREA-DFL
        //                                                WRST-AREA-RST
        //                                                WL19-AREA-QUOTE
        //                                                WL19-ELE-FND-MAX
        //                                                WL30-AREA-REINVST-POLI
        //                                                WL23-AREA-VINC-PEG
        //                                                WOPZ-AREA-OPZIONI
        //                                                WGOP-AREA-GARANZIA-OPZ
        //                                                WTOP-AREA-TRANCHE-OPZ
        //                                                WTLI-AREA-TRCH-LIQ
        //                                                WCNT-AREA-DATI-CONTEST
        //                                                WCLT-AREA-CLAU-TXT
        //                                                WDEQ-ELE-DETT-QUEST-MAX
        //                                                WPOG-ELE-PARAM-OGG-MAX
        //                                                WPMO-ELE-PARAM-MOV-MAX
        //                                                WTGA-ELE-TRAN-MAX
        //                                                WTLI-ELE-TRCH-LIQ-MAX
        //                                                WOCO-ELE-OGG-COLL-MAX
        //                                                WP58-AREA-IMPOSTA-BOLLO
        //                                                WP61-AREA-D-CRIST
        //                                                WP67-AREA-EST-POLI-CPI-PR
        //                                                WP01-AREA-RICH-EST
        //                                                DISTINCT-STR
        //                                                GAR
        //                                                TRCH-DI-GAR
        //                                                MATR-VAL-VAR
        //                                                VAR-FUNZ-DI-CALC
        //           *                                    AREA-IVVC0216
        //                                                AREA-WARNING-IVVC0216
        //                                                LCCV0021
        //                                                IVVC0222-AREA-FND-X-TRANCHE
        //                                                AREA-IVVC0223
        //                                                WCDG-AREA-COMMIS-GEST-VV.
        initIxIndici();
        initWadeAreaAdesione();
        initWbepAreaBenef();
        initWbelAreaBenefLiq();
        initWdcoAreaDtCollettiva();
        initWdfaAreaDtFiscAdes();
        initWdeqAreaDettQuest();
        initWdtcAreaDettTitCont();
        initWgrzAreaGaranzia();
        initWgrlAreaGaranziaLiq();
        initWisoAreaImpostaSost();
        initWlquAreaLiquidazione();
        initWmovAreaMovimento();
        initWmfzAreaMoviFinrio();
        initWpogAreaParamOgg();
        initWpmoAreaParamMov();
        initWpliAreaPercLiq();
        initWpolAreaPolizza();
        initWpreAreaPrestiti();
        initWpvtAreaProv();
        initWqueAreaQuest();
        initWricAreaRich();
        initWrdfAreaRichDisinvFnd();
        initWrifAreaRichInvFnd();
        ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().setWrdfEleRicInvMax(((short)0));
        ws.getAreaBusiness().getWrifAreaRichInvFnd().setWrifEleRicInvMax(((short)0));
        initWranAreaRappAnag();
        initWe15AreaEstRappAnag();
        initWrreAreaRappRete();
        initWspgAreaSoprapGar();
        initWsdiAreaStraInv();
        initWtitAreaTitCont();
        initWtclAreaTitLiq();
        initWtgaAreaTranche();
        initWdadAreaDefaultAdes();
        initWocoAreaOggColl();
        initWdflAreaDfl();
        initWrstAreaRst();
        initWl19AreaQuote();
        ws.getAreaBusiness().getWl19AreaQuote().setWl19EleFndMax(((short)0));
        initWl30AreaReinvstPoli();
        initWl23AreaVincPeg();
        initWopzAreaOpzioni();
        initWgopAreaGaranziaOpz();
        initWtopAreaTrancheOpz();
        initWtliAreaTrchLiq();
        initWcntAreaDatiContest();
        initWcltAreaClauTxt();
        ws.getAreaBusiness().getWdeqAreaDettQuest().setWdeqEleDettQuestMax(((short)0));
        ws.getAreaBusiness().getWpogAreaParamOgg().setWpogEleParamOggMax(((short)0));
        ws.getAreaBusiness().getWpmoAreaParamMov().setWpmoEleParamMovMax(((short)0));
        ws.getAreaBusiness().getWtgaAreaTranche().setWtgaEleTranMax(((short)0));
        ws.getAreaBusiness().getWtliAreaTrchLiq().setWtliEleTrchLiqMax(((short)0));
        ws.getAreaBusiness().getWocoAreaOggColl().setWocoEleOggCollMax(((short)0));
        initWp58AreaImpostaBollo();
        initWp61AreaDCrist();
        initWp67AreaEstPoliCpiPr();
        initWp01AreaRichEst();
        initDistinctStr();
        initGar();
        initTrchDiGar();
        initMatrValVar();
        initVarFunzDiCalc();
        initIvvc0215();
        initLccv0021();
        initIvvc0222AreaFndXTranche();
        initAreaIvvc0223();
        initAreaCommisGestVv();
        //
        //    Inizializzazione  STR-DIST-DT-VLDT-PROD-TGA
        //
        // COB_CODE: INITIALIZE  DIST-TAB-DT-VLDT-PROD(1).
        ws.getStrDistDtVldtProdTga().setDistTabDtVldtProd(1, 0);
        // COB_CODE: MOVE STR-DIST-DT-VLDT-PROD-TGA
        //             TO STR-RES-DIST-DT-VLDT-PROD-TGA.
        ws.getStrDistDtVldtProdTga().setStrResDistDtVldtProdTga(ws.getStrDistDtVldtProdTga().getStrDistDtVldtProdTgaFormatted());
        //--  INIZIALIZZAZIONE AREA DETTAGLIO QUESTIONARIO
        // COB_CODE: INITIALIZE             WDEQ-TAB-DETT-QUEST(1).
        initTabDettQuest();
        // COB_CODE: MOVE  WDEQ-TAB         TO WDEQ-RESTO-TAB.
        ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setRestoTab(ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().getWdeqTabFormatted());
        //--  INIZIALIZZAZIONE AREA PARAMETRO OGGETTO
        // COB_CODE: INITIALIZE             WPOG-TAB-PARAM-OGG(1).
        initTabParamOgg();
        // COB_CODE: MOVE  WPOG-TAB         TO WPOG-RESTO-TAB.
        ws.getAreaBusiness().getWpogAreaParamOgg().getWpogTab().setRestoTab(ws.getAreaBusiness().getWpogAreaParamOgg().getWpogTab().getWpogTabFormatted());
        //--  INIZIALIZZAZIONE AREA PARAMETRO MOVIMENTO
        // COB_CODE: INITIALIZE             WPMO-TAB-PARAM-MOV(1).
        initTabParamMov();
        // COB_CODE: MOVE  WPMO-TAB         TO WPMO-RESTO-TAB.
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setRestoTab(ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().getWpmoTabFormatted());
        //--  INIZIALIZZAZIONE AREA TRANCHE DI GARANZIA
        // COB_CODE: INITIALIZE             WTGA-TAB-TRAN(1).
        initTabTran();
        // COB_CODE: MOVE  WTGA-TAB         TO WTGA-RESTO-TAB.
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setRestoTab(ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().getWtgaTabFormatted());
        //-- INIZIALIZZAZIONE AREA TRANCHE GARANZIA DI LIQUIDAZIONE
        // COB_CODE: INITIALIZE             WTLI-TAB-TRCH-LIQ(1).
        initTabTrchLiq();
        // COB_CODE: MOVE  WTLI-TAB         TO WTLI-RESTO-TAB.
        ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setRestoTab(ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().getWtliTabFormatted());
        //-- INIZIALIZZAZIONE AREA OGGETTO COLLEGATO
        // COB_CODE: INITIALIZE             WOCO-TAB-OGG-COLL(1).
        initTabOggColl();
        // COB_CODE: MOVE  WOCO-TAB         TO WOCO-RESTO-TAB.
        ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setRestoTab(ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().getWocoTabFormatted());
        //--  AREA RICHIESTA DISINVESTIMENTO FONDO
        // COB_CODE: INITIALIZE             WRDF-TAB-RIC-DISINV(1).
        initTabRicDisinv();
        // COB_CODE: MOVE  WRDF-TABELLA     TO WRDF-RESTO-TABELLA.
        ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setRestoTabella(ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().getWrdfTabellaFormatted());
        //--  AREA RICHIESTA INVESTIMENTO FONDO
        // COB_CODE: INITIALIZE             WRIF-TAB-RIC-INV(1).
        initTabRicInv();
        // COB_CODE: MOVE  WRIF-TABELLA     TO WRIF-RESTO-TABELLA.
        ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setRestoTabella(ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().getWrifTabellaFormatted());
        //-- INIZIALIZZAZIONE AREA QUOTAZ FONDI UNIT
        // COB_CODE: INITIALIZE             WL19-TAB-FND(1).
        initTabFnd();
        // COB_CODE: MOVE  WL19-TABELLA     TO WL19-RESTO-TABELLA.
        ws.getAreaBusiness().getWl19AreaQuote().getWl19Tabella().setRestoTabella(ws.getAreaBusiness().getWl19AreaQuote().getWl19Tabella().getWl19TabellaFormatted());
        //--  INIZIALIZZAZIONE AREA WORK SCHEDA VARIABILI
        // COB_CODE: INITIALIZE             IVVC0216-ELE-LIVELLO-MAX-P
        //                                  IVVC0216-DEE
        //                                  IVVC0216-ID-POL-P            (1)
        //                                  IVVC0216-COD-TIPO-OPZIONE-P  (1)
        //                                  IVVC0216-TP-LIVELLO-P        (1)
        //                                  IVVC0216-COD-LIVELLO-P       (1)
        //                                  IVVC0216-ID-LIVELLO-P        (1)
        //                                  IVVC0216-DT-INIZ-PROD-P      (1)
        //                                  IVVC0216-COD-RGM-FISC-P      (1)
        //                                  IVVC0216-NOME-SERVIZIO-P     (1)
        //                                  IVVC0216-ELE-VARIABILI-MAX-P (1)
        //                                  IVVC0216-AREA-VARIABILI-P    (1)
        //                                  IVVC0216-VAR-AUT-OPER.
        ws.getAreaWorkIvvc0216().setC216EleLivelloMaxP(((short)0));
        ws.getAreaWorkIvvc0216().setC216Dee("");
        ws.getAreaWorkIvvc0216().getC216TabValP().setIvvc0216IdPolPFormatted(1, "000000000");
        ws.getAreaWorkIvvc0216().getC216TabValP().setIvvc0216CodTipoOpzioneP(1, "");
        ws.getAreaWorkIvvc0216().getC216TabValP().setIvvc0216TpLivelloP(1, Types.SPACE_CHAR);
        ws.getAreaWorkIvvc0216().getC216TabValP().setIvvc0216CodLivelloP(1, "");
        ws.getAreaWorkIvvc0216().getC216TabValP().setIvvc0216IdLivelloPFormatted(1, "000000000");
        ws.getAreaWorkIvvc0216().getC216TabValP().setIvvc0216DtInizProdP(1, "");
        ws.getAreaWorkIvvc0216().getC216TabValP().setIvvc0216CodRgmFiscP(1, "");
        ws.getAreaWorkIvvc0216().getC216TabValP().setIvvc0216NomeServizioP(1, "");
        ws.getAreaWorkIvvc0216().getC216TabValP().setEleVariabiliMaxP(1, ((short)0));
        initIvvc0216AreaVariabiliP();
        initIvvc0216VarAutOper();
        // COB_CODE: MOVE IVVC0216-TAB-VAL-P TO IVVC0216-RESTO-TAB-VAL-P.
        ws.getAreaWorkIvvc0216().getC216TabValP().setIvvc0216RestoTabValP(ws.getAreaWorkIvvc0216().getC216TabValP().getIvvc0216TabValPFormatted());
        // COB_CODE: INITIALIZE             IVVC0216-ELE-LIVELLO-MAX-T
        //                                  IVVC0216-ID-GAR-T            (1)
        //                                  IVVC0216-COD-TIPO-OPZIONE-T  (1)
        //                                  IVVC0216-TP-LIVELLO-T        (1)
        //                                  IVVC0216-COD-LIVELLO-T       (1)
        //                                  IVVC0216-ID-LIVELLO-T        (1)
        //                                  IVVC0216-DT-DECOR-TRCH-T     (1)
        //                                  IVVC0216-DT-INIZ-TARI-T      (1)
        //                                  IVVC0216-COD-RGM-FISC-T      (1)
        //                                  IVVC0216-NOME-SERVIZIO-T     (1)
        //                                  IVVC0216-ELE-VARIABILI-MAX-T (1)
        //                                  IVVC0216-AREA-VARIABILI-T    (1)
        ws.getAreaWorkIvvc0216().setC216EleLivelloMaxT(((short)0));
        ws.getAreaWorkIvvc0216().getC216TabValT().setIvvc0216IdGarTFormatted(1, "000000000");
        ws.getAreaWorkIvvc0216().getC216TabValT().setIvvc0216CodTipoOpzioneT(1, "");
        ws.getAreaWorkIvvc0216().getC216TabValT().setIvvc0216TpLivelloT(1, Types.SPACE_CHAR);
        ws.getAreaWorkIvvc0216().getC216TabValT().setIvvc0216CodLivelloT(1, "");
        ws.getAreaWorkIvvc0216().getC216TabValT().setIvvc0216IdLivelloTFormatted(1, "000000000");
        ws.getAreaWorkIvvc0216().getC216TabValT().setIvvc0216DtDecorTrchT(1, "");
        ws.getAreaWorkIvvc0216().getC216TabValT().setIvvc0216DtInizTariT(1, "");
        ws.getAreaWorkIvvc0216().getC216TabValT().setIvvc0216CodRgmFiscT(1, "");
        ws.getAreaWorkIvvc0216().getC216TabValT().setIvvc0216NomeServizioT(1, "");
        ws.getAreaWorkIvvc0216().getC216TabValT().setEleVariabiliMaxT(1, ((short)0));
        initIvvc0216AreaVariabiliT();
        // COB_CODE: MOVE IVVC0216-TAB-VAL-T TO IVVC0216-RESTO-TAB-VAL-T.
        ws.getAreaWorkIvvc0216().getC216TabValT().setIvvc0216RestoTabValT(ws.getAreaWorkIvvc0216().getC216TabValT().getIvvc0216TabValTFormatted());
        //--  INIZIALIZZAZIONE AREA VARIABILI
        // COB_CODE: INITIALIZE             C216-ELE-LIVELLO-MAX-P
        //                                  C216-DEE
        //                                  C216-ID-POL-P            (1)
        //                                  C216-COD-TIPO-OPZIONE-P  (1)
        //                                  C216-TP-LIVELLO-P        (1)
        //                                  C216-COD-LIVELLO-P       (1)
        //                                  C216-ID-LIVELLO-P        (1)
        //                                  C216-DT-INIZ-PROD-P      (1)
        //                                  C216-COD-RGM-FISC-P      (1)
        //                                  C216-NOME-SERVIZIO-P     (1)
        //                                  C216-ELE-VARIABILI-MAX-P (1)
        //                                  C216-AREA-VARIABILI-P    (1)
        //                                  C216-VAR-AUT-OPER.
        ws.getIvvc0216().setC216EleLivelloMaxP(((short)0));
        ws.getIvvc0216().setC216Dee("");
        ws.getIvvc0216().getC216TabValP().setIvvc0216IdPolPFormatted(1, "000000000");
        ws.getIvvc0216().getC216TabValP().setIvvc0216CodTipoOpzioneP(1, "");
        ws.getIvvc0216().getC216TabValP().setIvvc0216TpLivelloP(1, Types.SPACE_CHAR);
        ws.getIvvc0216().getC216TabValP().setIvvc0216CodLivelloP(1, "");
        ws.getIvvc0216().getC216TabValP().setIvvc0216IdLivelloPFormatted(1, "000000000");
        ws.getIvvc0216().getC216TabValP().setIvvc0216DtInizProdP(1, "");
        ws.getIvvc0216().getC216TabValP().setIvvc0216CodRgmFiscP(1, "");
        ws.getIvvc0216().getC216TabValP().setIvvc0216NomeServizioP(1, "");
        ws.getIvvc0216().getC216TabValP().setEleVariabiliMaxP(1, ((short)0));
        initAreaVariabiliP();
        initC216VarAutOper();
        // COB_CODE: MOVE C216-TAB-VAL-P TO C216-RESTO-TAB-VAL-P.
        ws.getIvvc0216().getC216TabValP().setIvvc0216RestoTabValP(ws.getIvvc0216().getC216TabValP().getIvvc0216TabValPFormatted());
        // COB_CODE: INITIALIZE             C216-ELE-LIVELLO-MAX-T
        //                                  C216-ID-GAR-T            (1)
        //                                  C216-COD-TIPO-OPZIONE-T  (1)
        //                                  C216-TP-LIVELLO-T        (1)
        //                                  C216-COD-LIVELLO-T       (1)
        //                                  C216-ID-LIVELLO-T        (1)
        //                                  C216-DT-DECOR-TRCH-T     (1)
        //                                  C216-DT-INIZ-TARI-T      (1)
        //                                  C216-COD-RGM-FISC-T      (1)
        //                                  C216-NOME-SERVIZIO-T     (1)
        //                                  C216-ELE-VARIABILI-MAX-T (1)
        //                                  C216-AREA-VARIABILI-T    (1)
        ws.getIvvc0216().setC216EleLivelloMaxT(((short)0));
        ws.getIvvc0216().getC216TabValT().setIvvc0216IdGarTFormatted(1, "000000000");
        ws.getIvvc0216().getC216TabValT().setIvvc0216CodTipoOpzioneT(1, "");
        ws.getIvvc0216().getC216TabValT().setIvvc0216TpLivelloT(1, Types.SPACE_CHAR);
        ws.getIvvc0216().getC216TabValT().setIvvc0216CodLivelloT(1, "");
        ws.getIvvc0216().getC216TabValT().setIvvc0216IdLivelloTFormatted(1, "000000000");
        ws.getIvvc0216().getC216TabValT().setIvvc0216DtDecorTrchT(1, "");
        ws.getIvvc0216().getC216TabValT().setIvvc0216DtInizTariT(1, "");
        ws.getIvvc0216().getC216TabValT().setIvvc0216CodRgmFiscT(1, "");
        ws.getIvvc0216().getC216TabValT().setIvvc0216NomeServizioT(1, "");
        ws.getIvvc0216().getC216TabValT().setEleVariabiliMaxT(1, ((short)0));
        initAreaVariabiliT();
        // COB_CODE: MOVE C216-TAB-VAL-T TO C216-RESTO-TAB-VAL-T.
        ws.getIvvc0216().getC216TabValT().setIvvc0216RestoTabValT(ws.getIvvc0216().getC216TabValT().getIvvc0216TabValTFormatted());
        // COB_CODE: SET  IDSV8888-BUSINESS-DBG          TO TRUE
        ws.getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
        // COB_CODE: MOVE 'IVVS0211'                 TO IDSV8888-NOME-PGM.
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("IVVS0211");
        // COB_CODE: SET  IDSV8888-FINE              TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setFine();
        // COB_CODE: MOVE SPACES                     TO IDSV8888-DESC-PGM
        //skipped translation for moving SPACES to IDSV8888-DESC-PGM; considered in STRING statement translation below
        // COB_CODE: STRING 'Iniz. area di working'
        //                   DELIMITED BY SIZE
        //                   INTO IDSV8888-DESC-PGM
        //           END-STRING
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("Iniz. area di working");
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
    }

    /**Original name: A002-SET<br>
	 * <pre>----------------------------------------------------------------*
	 *     SETTAGGIO FLAGS
	 * ----------------------------------------------------------------*</pre>*/
    private void a002Set() {
        // COB_CODE: SET IVVC0211-SUCCESSFUL-RC      TO TRUE.
        inputIvvs0211.getIvvc0211RestoDati().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET TIPO-STANDARD               TO TRUE.
        ws.getFlagTipologiaScheda().setStandard();
        // COB_CODE: SET SKEDE-TOTALI                TO TRUE.
        ws.getFlagSkede().setTotali();
        // COB_CODE: SET GARANZIA-INIT               TO TRUE.
        ws.getFlagTipoGaranzia().setInit();
        // COB_CODE: SET TRANCHE-INIT                TO TRUE.
        ws.getFlagTipoTranche().setInit();
        // COB_CODE: SET FINE-ELAB-NO                TO TRUE.
        ws.getFlagFineElab().setNo();
        // COB_CODE: SET STAT-CAUS-TROVATI-NO        TO TRUE.
        ws.getFlagStatCaus().setNo();
        // COB_CODE: SET WK-VAR-FUNZ-TROVATA-NO      TO TRUE.
        ws.getWkVarFunzTrovata().setNo();
    }

    /**Original name: A003-ZERO<br>
	 * <pre>----------------------------------------------------------------*
	 *     SETTAGGIO ZERO
	 * ----------------------------------------------------------------*</pre>*/
    private void a003Zero() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A004-SPACE<br>
	 * <pre>----------------------------------------------------------------*
	 *     SETTAGGIO SPACES
	 * ----------------------------------------------------------------*</pre>*/
    private void a004Space() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A005-CTRL-DATI-INPUT<br>
	 * <pre>----------------------------------------------------------------*
	 *     CONTROLLI DATI INPUT
	 * ----------------------------------------------------------------*
	 * --> CONTROLLO DELLA FUNZIONALITA'</pre>*/
    private void a005CtrlDatiInput() {
        ConcatUtil concatUtil = null;
        // COB_CODE: IF  IVVC0211-CODICE-INIZIATIVA > SPACES
        //           AND IVVC0211-CODICE-INIZIATIVA NOT = HIGH-VALUE
        //           AND IVVC0211-CODICE-TRATTATO   > SPACES
        //           AND IVVC0211-CODICE-TRATTATO   NOT = HIGH-VALUE
        //               END-STRING
        //           END-IF.
        if (Characters.GT_SPACE.test(inputIvvs0211.getIvvc0211RestoDati().getCodiceIniziativa()) && !Characters.EQ_HIGH.test(inputIvvs0211.getIvvc0211RestoDati().getCodiceIniziativaFormatted()) && Characters.GT_SPACE.test(inputIvvs0211.getIvvc0211RestoDati().getCodiceTrattato()) && !Characters.EQ_HIGH.test(inputIvvs0211.getIvvc0211RestoDati().getCodiceTrattatoFormatted())) {
            // COB_CODE: SET IVVC0211-FUNZ-NON-DEFINITA TO TRUE
            inputIvvs0211.getIvvc0211RestoDati().getReturnCode().setFunzNonDefinita();
            // COB_CODE: MOVE WK-PGM          TO IVVC0211-COD-SERVIZIO-BE
            inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: STRING 'FUNZIONALITA'' NON DEFINITA'
            //               DELIMITED BY SIZE INTO
            //               IVVC0211-DESCRIZ-ERR
            //           END-STRING
            inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setDescrizErr("FUNZIONALITA' NON DEFINITA");
        }
        //--> CONTROLLO DELLO STEP ELABORAZIONE IN CASO DI ACCESSO X RIASS
        // COB_CODE: IF  IVVC0211-CODICE-TRATTATO   > SPACES
        //           AND IVVC0211-CODICE-TRATTATO   NOT = HIGH-VALUE
        //           AND NOT IVVC0211-IN-CONV
        //               END-STRING
        //           END-IF.
        if (Characters.GT_SPACE.test(inputIvvs0211.getIvvc0211RestoDati().getCodiceTrattato()) && !Characters.EQ_HIGH.test(inputIvvs0211.getIvvc0211RestoDati().getCodiceTrattatoFormatted()) && !inputIvvs0211.getIvvc0211DatiInput().getStepElab().isInConv()) {
            // COB_CODE: SET IVVC0211-STEP-ELAB-NOT-VALID TO TRUE
            inputIvvs0211.getIvvc0211RestoDati().getReturnCode().setStepElabNotValid();
            // COB_CODE: MOVE WK-PGM          TO IVVC0211-COD-SERVIZIO-BE
            inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: STRING 'STEP ELABORAZIONE NON VALIDO'
            //               DELIMITED BY SIZE INTO
            //               IVVC0211-DESCRIZ-ERR
            //           END-STRING
            inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setDescrizErr("STEP ELABORAZIONE NON VALIDO");
        }
        //--> CONTROLLO DELLO STEP ELABORAZIONE IN CASO DI ACCESSO X
        //    INIZIATIVA COMMERCIALE (DA DEFINIRE)
        // COB_CODE: IF  IVVC0211-CODICE-INIZIATIVA   > SPACES
        //            AND IVVC0211-CODICE-INIZIATIVA   NOT = HIGH-VALUE
        //            AND IVVC0211-STEP-ELAB NOT = '3' AND '4' AND '5' AND '6'
        //                                     AND '7'
        //                END-STRING
        //            END-IF.
        if (Characters.GT_SPACE.test(inputIvvs0211.getIvvc0211RestoDati().getCodiceIniziativa()) && !Characters.EQ_HIGH.test(inputIvvs0211.getIvvc0211RestoDati().getCodiceIniziativaFormatted()) && inputIvvs0211.getIvvc0211DatiInput().getStepElab().getStepElab() != '3' && inputIvvs0211.getIvvc0211DatiInput().getStepElab().getStepElab() != '4' && inputIvvs0211.getIvvc0211DatiInput().getStepElab().getStepElab() != '5' && inputIvvs0211.getIvvc0211DatiInput().getStepElab().getStepElab() != '6' && inputIvvs0211.getIvvc0211DatiInput().getStepElab().getStepElab() != '7') {
            // COB_CODE: SET IVVC0211-STEP-ELAB-NOT-VALID TO TRUE
            inputIvvs0211.getIvvc0211RestoDati().getReturnCode().setStepElabNotValid();
            // COB_CODE: MOVE WK-PGM          TO IVVC0211-COD-SERVIZIO-BE
            inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: STRING 'STEP ELABORAZIONE NON VALIDO'
            //               DELIMITED BY SIZE INTO
            //               IVVC0211-DESCRIZ-ERR
            //           END-STRING
            inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setDescrizErr("STEP ELABORAZIONE NON VALIDO");
        }
        //--> CONTROLLO DELLO STEP ELABORAZIONE IN CASO DI ACCESSO X
        //    PRODOTTO
        // COB_CODE: IF (IVVC0211-CODICE-INIZIATIVA > SPACES
        //           AND IVVC0211-CODICE-INIZIATIVA NOT = HIGH-VALUE)
        //           OR (IVVC0211-CODICE-TRATTATO   > SPACES
        //           AND IVVC0211-CODICE-TRATTATO   NOT = HIGH-VALUE)
        //               NEXT SENTENCE
        //           ELSE
        //              END-IF
        //           END-IF.
        if (Characters.GT_SPACE.test(inputIvvs0211.getIvvc0211RestoDati().getCodiceIniziativa()) && !Characters.EQ_HIGH.test(inputIvvs0211.getIvvc0211RestoDati().getCodiceIniziativaFormatted()) || Characters.GT_SPACE.test(inputIvvs0211.getIvvc0211RestoDati().getCodiceTrattato()) && !Characters.EQ_HIGH.test(inputIvvs0211.getIvvc0211RestoDati().getCodiceTrattatoFormatted())) {
        // COB_CODE: NEXT SENTENCE
        //next sentence
        }
        else if (!inputIvvs0211.getIvvc0211DatiInput().getStepElab().isPreConv() && !inputIvvs0211.getIvvc0211DatiInput().getStepElab().isPostConv() && !inputIvvs0211.getIvvc0211DatiInput().getStepElab().isInConv()) {
            // COB_CODE:         IF  NOT IVVC0211-PRE-CONV
            //                   AND NOT IVVC0211-POST-CONV
            //                   AND NOT IVVC0211-IN-CONV
            //           *--> DATI INPUT ERRATI
            //                       END-STRING
            //                   END-IF
            //--> DATI INPUT ERRATI
            // COB_CODE: SET IVVC0211-STEP-ELAB-NOT-VALID TO TRUE
            inputIvvs0211.getIvvc0211RestoDati().getReturnCode().setStepElabNotValid();
            // COB_CODE: MOVE WK-PGM          TO IVVC0211-COD-SERVIZIO-BE
            inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: STRING 'STEP ELABORAZIONE NON VALIDO'
            //               DELIMITED BY SIZE INTO
            //               IVVC0211-DESCRIZ-ERR
            //           END-STRING
            inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setDescrizErr("STEP ELABORAZIONE NON VALIDO");
        }
        // COB_CODE:      IF IVVC0211-SUCCESSFUL-RC
        //           *--> CONTROLLO DEL FLAG AREA
        //                   END-IF
        //                END-IF.
        if (inputIvvs0211.getIvvc0211RestoDati().getReturnCode().isSuccessfulRc()) {
            //--> CONTROLLO DEL FLAG AREA
            // COB_CODE: IF  NOT IVVC0211-AREA-VE
            //           AND NOT IVVC0211-AREA-PV
            //               END-STRING
            //           END-IF
            if (!inputIvvs0211.getIvvc0211DatiInput().getFlgArea().isVe() && !inputIvvs0211.getIvvc0211DatiInput().getFlgArea().isPv()) {
                // COB_CODE: SET IVVC0211-FLAG-AREA-NOT-VALID  TO TRUE
                inputIvvs0211.getIvvc0211RestoDati().getReturnCode().setFlagAreaNotValid();
                // COB_CODE: MOVE WK-PGM       TO IVVC0211-COD-SERVIZIO-BE
                inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: STRING 'FLAG-AREA NON VALIDO'
                //                  DELIMITED BY SIZE INTO
                //                  IVVC0211-DESCRIZ-ERR
                //           END-STRING
                inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setDescrizErr("FLAG-AREA NON VALIDO");
            }
            // COB_CODE:         IF IVVC0211-SUCCESSFUL-RC
            //           *--> CONTROLLO TIPO MOVIMENTO
            //                      END-IF
            //                   END-IF
            if (inputIvvs0211.getIvvc0211RestoDati().getReturnCode().isSuccessfulRc()) {
                //--> CONTROLLO TIPO MOVIMENTO
                // COB_CODE: IF  IVVC0211-TIPO-MOVIMENTO NOT NUMERIC
                //            OR IVVC0211-TIPO-MOVIMENTO = ZEROES
                //               END-STRING
                //           END-IF
                if (!Functions.isNumber(inputIvvs0211.getIvvc0211DatiInput().getTipoMovimentoFormatted()) || Characters.EQ_ZERO.test(inputIvvs0211.getIvvc0211DatiInput().getTipoMovimentoFormatted())) {
                    // COB_CODE: SET IVVC0211-TIPO-MOVIM-NOT-VALID  TO TRUE
                    inputIvvs0211.getIvvc0211RestoDati().getReturnCode().setTipoMovimNotValid();
                    // COB_CODE: MOVE WK-PGM    TO IVVC0211-COD-SERVIZIO-BE
                    inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: STRING 'TIPO MOVIMENTO NON VALIDO'
                    //                  DELIMITED BY SIZE INTO
                    //                  IVVC0211-DESCRIZ-ERR
                    //           END-STRING
                    inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setDescrizErr("TIPO MOVIMENTO NON VALIDO");
                }
                // COB_CODE: IF IVVC0211-SUCCESSFUL-RC
                //              PERFORM V000-VERIFICA-OPZ-POL-GAR THRU V000-EX
                //           END-IF
                if (inputIvvs0211.getIvvc0211RestoDati().getReturnCode().isSuccessfulRc()) {
                    // COB_CODE: PERFORM V000-VERIFICA-OPZ-POL-GAR THRU V000-EX
                    v000VerificaOpzPolGar();
                }
            }
        }
    }

    /**Original name: A010-VALORIZZA-DCLGEN<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 *     RISPETTIVE AREE DELLA COPY IVVC0214
	 * ----------------------------------------------------------------*</pre>*/
    private void a010ValorizzaDclgen() {
        // COB_CODE: IF IVVC0211-TAB-ALIAS(IND-STR) =
        //              IVVC0218-ALIAS-ADES
        //                TO WADE-AREA-ADESIONE
        //           END-IF.
        if (Conditions.eq(inputIvvs0211.getIvvc0211TabInfo1().getTabAlias(ws.getIxIndici().getStr()), ws.getIvvc0218().getAliasAdes())) {
            // COB_CODE: MOVE IVVC0211-BUFFER-DATI
            //               (IVVC0211-POSIZ-INI(IND-STR) :
            //                IVVC0211-LUNGHEZZA(IND-STR))
            //             TO WADE-AREA-ADESIONE
            ws.getAreaBusiness().setWadeAreaAdesioneFormatted(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1));
        }
        // COB_CODE: IF IVVC0211-TAB-ALIAS(IND-STR) =
        //              IVVC0218-ALIAS-BENEF
        //                TO WBEP-AREA-BENEF
        //           END-IF.
        if (Conditions.eq(inputIvvs0211.getIvvc0211TabInfo1().getTabAlias(ws.getIxIndici().getStr()), ws.getIvvc0218().getAliasBenef())) {
            // COB_CODE: MOVE IVVC0211-BUFFER-DATI
            //               (IVVC0211-POSIZ-INI(IND-STR) :
            //                IVVC0211-LUNGHEZZA(IND-STR))
            //             TO WBEP-AREA-BENEF
            ws.getAreaBusiness().setWbepAreaBenefFormatted(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1));
        }
        // COB_CODE: IF IVVC0211-TAB-ALIAS(IND-STR) =
        //              IVVC0218-ALIAS-BENEF-LIQ
        //                TO WBEL-AREA-BENEF-LIQ
        //           END-IF.
        if (Conditions.eq(inputIvvs0211.getIvvc0211TabInfo1().getTabAlias(ws.getIxIndici().getStr()), ws.getIvvc0218().getAliasBenefLiq())) {
            // COB_CODE: MOVE IVVC0211-BUFFER-DATI
            //               (IVVC0211-POSIZ-INI(IND-STR) :
            //                IVVC0211-LUNGHEZZA(IND-STR))
            //             TO WBEL-AREA-BENEF-LIQ
            ws.getAreaBusiness().setWbelAreaBenefLiqFormatted(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1));
        }
        // COB_CODE: IF IVVC0211-TAB-ALIAS(IND-STR) =
        //              IVVC0218-ALIAS-DT-COLL
        //                TO WDCO-AREA-DT-COLLETTIVA
        //           END-IF.
        if (Conditions.eq(inputIvvs0211.getIvvc0211TabInfo1().getTabAlias(ws.getIxIndici().getStr()), ws.getIvvc0218().getAliasDtColl())) {
            // COB_CODE: MOVE IVVC0211-BUFFER-DATI
            //               (IVVC0211-POSIZ-INI(IND-STR) :
            //                IVVC0211-LUNGHEZZA(IND-STR))
            //             TO WDCO-AREA-DT-COLLETTIVA
            ws.getAreaBusiness().setWdcoAreaDtCollettivaFormatted(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1));
        }
        // COB_CODE: IF IVVC0211-TAB-ALIAS(IND-STR) =
        //              IVVC0218-ALIAS-DT-FISC-ADES
        //                TO WDFA-AREA-DT-FISC-ADES
        //           END-IF.
        if (Conditions.eq(inputIvvs0211.getIvvc0211TabInfo1().getTabAlias(ws.getIxIndici().getStr()), ws.getIvvc0218().getAliasDtFiscAdes())) {
            // COB_CODE: MOVE IVVC0211-BUFFER-DATI
            //               (IVVC0211-POSIZ-INI(IND-STR) :
            //                IVVC0211-LUNGHEZZA(IND-STR))
            //             TO WDFA-AREA-DT-FISC-ADES
            ws.getAreaBusiness().setWdfaAreaDtFiscAdesFormatted(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1));
        }
        // COB_CODE: IF IVVC0211-TAB-ALIAS(IND-STR) =
        //              IVVC0218-ALIAS-DETT-QUEST
        //                TO WDEQ-AREA-DETT-QUEST
        //           END-IF.
        if (Conditions.eq(inputIvvs0211.getIvvc0211TabInfo1().getTabAlias(ws.getIxIndici().getStr()), ws.getIvvc0218().getAliasDettQuest())) {
            // COB_CODE: MOVE IVVC0211-BUFFER-DATI
            //               (IVVC0211-POSIZ-INI(IND-STR) :
            //                IVVC0211-LUNGHEZZA(IND-STR))
            //             TO WDEQ-AREA-DETT-QUEST
            ws.getAreaBusiness().getWdeqAreaDettQuest().setWdeqAreaDettQuestFormatted(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1));
        }
        // COB_CODE: IF IVVC0211-TAB-ALIAS(IND-STR) =
        //              IVVC0218-ALIAS-DETT-TIT-CONT
        //                TO WDTC-AREA-DETT-TIT-CONT
        //           END-IF.
        if (Conditions.eq(inputIvvs0211.getIvvc0211TabInfo1().getTabAlias(ws.getIxIndici().getStr()), ws.getIvvc0218().getAliasDettTitCont())) {
            // COB_CODE: MOVE IVVC0211-BUFFER-DATI
            //               (IVVC0211-POSIZ-INI(IND-STR) :
            //                IVVC0211-LUNGHEZZA(IND-STR))
            //             TO WDTC-AREA-DETT-TIT-CONT
            ws.getAreaBusiness().setWdtcAreaDettTitContFormatted(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1));
        }
        // COB_CODE: IF IVVC0211-TAB-ALIAS(IND-STR) =
        //              IVVC0218-ALIAS-GARANZIA
        //                TO WGRZ-AREA-GARANZIA
        //           END-IF.
        if (Conditions.eq(inputIvvs0211.getIvvc0211TabInfo1().getTabAlias(ws.getIxIndici().getStr()), ws.getIvvc0218().getAliasGaranzia())) {
            // COB_CODE: MOVE IVVC0211-BUFFER-DATI
            //               (IVVC0211-POSIZ-INI(IND-STR) :
            //                IVVC0211-LUNGHEZZA(IND-STR))
            //             TO WGRZ-AREA-GARANZIA
            ws.getAreaBusiness().setWgrzAreaGaranziaFormatted(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1));
        }
        // COB_CODE: IF IVVC0211-TAB-ALIAS(IND-STR) =
        //              IVVC0218-ALIAS-GARANZIA-OPZ
        //                TO WGOP-AREA-GARANZIA-OPZ
        //           END-IF.
        if (Conditions.eq(inputIvvs0211.getIvvc0211TabInfo1().getTabAlias(ws.getIxIndici().getStr()), ws.getIvvc0218().getAliasGaranziaOpz())) {
            // COB_CODE: MOVE IVVC0211-BUFFER-DATI
            //               (IVVC0211-POSIZ-INI(IND-STR) :
            //                IVVC0211-LUNGHEZZA(IND-STR))
            //             TO WGOP-AREA-GARANZIA-OPZ
            ws.getAreaBusiness().setWgopAreaGaranziaOpzFormatted(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1));
        }
        // COB_CODE: IF IVVC0211-TAB-ALIAS(IND-STR) =
        //              IVVC0218-ALIAS-GAR-LIQ
        //                TO WGRL-AREA-GARANZIA-LIQ
        //           END-IF.
        if (Conditions.eq(inputIvvs0211.getIvvc0211TabInfo1().getTabAlias(ws.getIxIndici().getStr()), ws.getIvvc0218().getAliasGarLiq())) {
            // COB_CODE: MOVE IVVC0211-BUFFER-DATI
            //               (IVVC0211-POSIZ-INI(IND-STR) :
            //                IVVC0211-LUNGHEZZA(IND-STR))
            //             TO WGRL-AREA-GARANZIA-LIQ
            ws.getAreaBusiness().setWgrlAreaGaranziaLiqFormatted(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1));
        }
        // COB_CODE: IF IVVC0211-TAB-ALIAS(IND-STR) =
        //              IVVC0218-ALIAS-IMPOSTA-SOST
        //                TO WISO-AREA-IMPOSTA-SOST
        //           END-IF.
        if (Conditions.eq(inputIvvs0211.getIvvc0211TabInfo1().getTabAlias(ws.getIxIndici().getStr()), ws.getIvvc0218().getAliasImpostaSost())) {
            // COB_CODE: MOVE IVVC0211-BUFFER-DATI
            //               (IVVC0211-POSIZ-INI(IND-STR) :
            //                IVVC0211-LUNGHEZZA(IND-STR))
            //             TO WISO-AREA-IMPOSTA-SOST
            ws.getAreaBusiness().setWisoAreaImpostaSostFormatted(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1));
        }
        // COB_CODE: IF IVVC0211-TAB-ALIAS(IND-STR) =
        //              IVVC0218-ALIAS-LIQUIDAZ
        //                TO WLQU-AREA-LIQUIDAZIONE
        //           END-IF.
        if (Conditions.eq(inputIvvs0211.getIvvc0211TabInfo1().getTabAlias(ws.getIxIndici().getStr()), ws.getIvvc0218().getAliasLiquidaz())) {
            // COB_CODE: MOVE IVVC0211-BUFFER-DATI
            //               (IVVC0211-POSIZ-INI(IND-STR) :
            //                IVVC0211-LUNGHEZZA(IND-STR))
            //             TO WLQU-AREA-LIQUIDAZIONE
            ws.getAreaBusiness().setWlquAreaLiquidazioneFormatted(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1));
        }
        // COB_CODE: IF IVVC0211-TAB-ALIAS(IND-STR) =
        //              IVVC0218-ALIAS-MOVIMENTO
        //                TO WMOV-AREA-MOVIMENTO
        //           END-IF.
        if (Conditions.eq(inputIvvs0211.getIvvc0211TabInfo1().getTabAlias(ws.getIxIndici().getStr()), ws.getIvvc0218().getAliasMovimento())) {
            // COB_CODE: MOVE IVVC0211-BUFFER-DATI
            //               (IVVC0211-POSIZ-INI(IND-STR) :
            //                IVVC0211-LUNGHEZZA(IND-STR))
            //             TO WMOV-AREA-MOVIMENTO
            ws.getAreaBusiness().setWmovAreaMovimentoFormatted(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1));
        }
        // COB_CODE: IF IVVC0211-TAB-ALIAS(IND-STR) =
        //              IVVC0218-ALIAS-MOVI-FINRIO
        //                TO WMFZ-AREA-MOVI-FINRIO
        //           END-IF.
        if (Conditions.eq(inputIvvs0211.getIvvc0211TabInfo1().getTabAlias(ws.getIxIndici().getStr()), ws.getIvvc0218().getAliasMoviFinrio())) {
            // COB_CODE: MOVE IVVC0211-BUFFER-DATI
            //               (IVVC0211-POSIZ-INI(IND-STR) :
            //                IVVC0211-LUNGHEZZA(IND-STR))
            //             TO WMFZ-AREA-MOVI-FINRIO
            ws.getAreaBusiness().setWmfzAreaMoviFinrioFormatted(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1));
        }
        // COB_CODE: IF IVVC0211-TAB-ALIAS(IND-STR) =
        //              IVVC0218-ALIAS-PARAM-OGG
        //                TO WPOG-AREA-PARAM-OGG
        //           END-IF.
        if (Conditions.eq(inputIvvs0211.getIvvc0211TabInfo1().getTabAlias(ws.getIxIndici().getStr()), ws.getIvvc0218().getAliasParamOgg())) {
            // COB_CODE: MOVE IVVC0211-BUFFER-DATI
            //               (IVVC0211-POSIZ-INI(IND-STR) :
            //                IVVC0211-LUNGHEZZA(IND-STR))
            //             TO WPOG-AREA-PARAM-OGG
            ws.getAreaBusiness().getWpogAreaParamOgg().setWpogAreaParamOggFormatted(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1));
        }
        // COB_CODE: IF IVVC0211-TAB-ALIAS(IND-STR) =
        //              IVVC0218-ALIAS-PARAM-MOV
        //                TO WPMO-AREA-PARAM-MOV
        //           END-IF.
        if (Conditions.eq(inputIvvs0211.getIvvc0211TabInfo1().getTabAlias(ws.getIxIndici().getStr()), ws.getIvvc0218().getAliasParamMov())) {
            // COB_CODE: MOVE IVVC0211-BUFFER-DATI
            //               (IVVC0211-POSIZ-INI(IND-STR) :
            //                IVVC0211-LUNGHEZZA(IND-STR))
            //             TO WPMO-AREA-PARAM-MOV
            ws.getAreaBusiness().getWpmoAreaParamMov().setWpmoAreaParamMovFormatted(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1));
        }
        // COB_CODE: IF IVVC0211-TAB-ALIAS(IND-STR) =
        //              IVVC0218-ALIAS-PERC-LIQ
        //                TO WPLI-AREA-PERC-LIQ
        //           END-IF.
        if (Conditions.eq(inputIvvs0211.getIvvc0211TabInfo1().getTabAlias(ws.getIxIndici().getStr()), ws.getIvvc0218().getAliasPercLiq())) {
            // COB_CODE: MOVE IVVC0211-BUFFER-DATI
            //               (IVVC0211-POSIZ-INI(IND-STR) :
            //                IVVC0211-LUNGHEZZA(IND-STR))
            //             TO WPLI-AREA-PERC-LIQ
            ws.getAreaBusiness().setWpliAreaPercLiqFormatted(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1));
        }
        // COB_CODE: IF IVVC0211-TAB-ALIAS(IND-STR) =
        //              IVVC0218-ALIAS-POLI
        //                TO WPOL-AREA-POLIZZA
        //           END-IF.
        if (Conditions.eq(inputIvvs0211.getIvvc0211TabInfo1().getTabAlias(ws.getIxIndici().getStr()), ws.getIvvc0218().getAliasPoli())) {
            // COB_CODE: MOVE IVVC0211-BUFFER-DATI
            //               (IVVC0211-POSIZ-INI(IND-STR) :
            //                IVVC0211-LUNGHEZZA(IND-STR))
            //             TO WPOL-AREA-POLIZZA
            ws.getAreaBusiness().setWpolAreaPolizzaFormatted(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1));
        }
        // COB_CODE: IF IVVC0211-TAB-ALIAS(IND-STR) =
        //              IVVC0218-ALIAS-PRESTITI
        //                TO WPRE-AREA-PRESTITI
        //           END-IF.
        if (Conditions.eq(inputIvvs0211.getIvvc0211TabInfo1().getTabAlias(ws.getIxIndici().getStr()), ws.getIvvc0218().getAliasPrestiti())) {
            // COB_CODE: MOVE IVVC0211-BUFFER-DATI
            //               (IVVC0211-POSIZ-INI(IND-STR) :
            //                IVVC0211-LUNGHEZZA(IND-STR))
            //             TO WPRE-AREA-PRESTITI
            ws.getAreaBusiness().setWpreAreaPrestitiFormatted(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1));
        }
        // COB_CODE: IF IVVC0211-TAB-ALIAS(IND-STR) =
        //              IVVC0218-ALIAS-PROVV-TRAN
        //                TO WPVT-AREA-PROV
        //           END-IF.
        if (Conditions.eq(inputIvvs0211.getIvvc0211TabInfo1().getTabAlias(ws.getIxIndici().getStr()), ws.getIvvc0218().getAliasProvvTran())) {
            // COB_CODE: MOVE IVVC0211-BUFFER-DATI
            //               (IVVC0211-POSIZ-INI(IND-STR) :
            //                IVVC0211-LUNGHEZZA(IND-STR))
            //             TO WPVT-AREA-PROV
            ws.getAreaBusiness().setWpvtAreaProvFormatted(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1));
        }
        // COB_CODE: IF IVVC0211-TAB-ALIAS(IND-STR) =
        //              IVVC0218-ALIAS-QUEST
        //                TO WQUE-AREA-QUEST
        //           END-IF.
        if (Conditions.eq(inputIvvs0211.getIvvc0211TabInfo1().getTabAlias(ws.getIxIndici().getStr()), ws.getIvvc0218().getAliasQuest())) {
            // COB_CODE: MOVE IVVC0211-BUFFER-DATI
            //               (IVVC0211-POSIZ-INI(IND-STR) :
            //                IVVC0211-LUNGHEZZA(IND-STR))
            //             TO WQUE-AREA-QUEST
            ws.getAreaBusiness().setWqueAreaQuestFormatted(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1));
        }
        // COB_CODE: IF IVVC0211-TAB-ALIAS(IND-STR) =
        //              IVVC0218-ALIAS-RICH
        //                TO WRIC-AREA-RICH
        //           END-IF.
        if (Conditions.eq(inputIvvs0211.getIvvc0211TabInfo1().getTabAlias(ws.getIxIndici().getStr()), ws.getIvvc0218().getAliasRich())) {
            // COB_CODE: MOVE IVVC0211-BUFFER-DATI
            //               (IVVC0211-POSIZ-INI(IND-STR) :
            //                IVVC0211-LUNGHEZZA(IND-STR))
            //             TO WRIC-AREA-RICH
            ws.getAreaBusiness().setWricAreaRichFormatted(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1));
        }
        // COB_CODE: IF IVVC0211-TAB-ALIAS(IND-STR) =
        //              IVVC0218-ALIAS-RICH-DISINV-FND
        //                TO WRDF-AREA-RICH-DISINV-FND
        //           END-IF.
        if (Conditions.eq(inputIvvs0211.getIvvc0211TabInfo1().getTabAlias(ws.getIxIndici().getStr()), ws.getIvvc0218().getAliasRichDisinvFnd())) {
            // COB_CODE: MOVE IVVC0211-BUFFER-DATI
            //               (IVVC0211-POSIZ-INI(IND-STR) :
            //                IVVC0211-LUNGHEZZA(IND-STR))
            //             TO WRDF-AREA-RICH-DISINV-FND
            ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().setWrdfAreaRichDisinvFndFormatted(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1));
        }
        // COB_CODE: IF IVVC0211-TAB-ALIAS(IND-STR) =
        //              IVVC0218-ALIAS-RICH-INV-FND
        //                TO WRIF-AREA-RICH-INV-FND
        //           END-IF.
        if (Conditions.eq(inputIvvs0211.getIvvc0211TabInfo1().getTabAlias(ws.getIxIndici().getStr()), ws.getIvvc0218().getAliasRichInvFnd())) {
            // COB_CODE: MOVE IVVC0211-BUFFER-DATI
            //               (IVVC0211-POSIZ-INI(IND-STR) :
            //                IVVC0211-LUNGHEZZA(IND-STR))
            //             TO WRIF-AREA-RICH-INV-FND
            ws.getAreaBusiness().getWrifAreaRichInvFnd().setWrifAreaRichInvFndFormatted(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1));
        }
        // COB_CODE: IF IVVC0211-TAB-ALIAS(IND-STR) =
        //              IVVC0218-ALIAS-RAPP-ANAG
        //                TO WRAN-AREA-RAPP-ANAG
        //           END-IF.
        if (Conditions.eq(inputIvvs0211.getIvvc0211TabInfo1().getTabAlias(ws.getIxIndici().getStr()), ws.getIvvc0218().getAliasRappAnag())) {
            // COB_CODE: MOVE IVVC0211-BUFFER-DATI
            //               (IVVC0211-POSIZ-INI(IND-STR) :
            //                IVVC0211-LUNGHEZZA(IND-STR))
            //             TO WRAN-AREA-RAPP-ANAG
            ws.getAreaBusiness().setWranAreaRappAnagFormatted(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1));
        }
        // COB_CODE: IF IVVC0211-TAB-ALIAS(IND-STR) =
        //              IVVC0218-ALIAS-EST-RAPP-ANAG
        //                TO WE15-AREA-EST-RAPP-ANAG
        //           END-IF.
        if (Conditions.eq(inputIvvs0211.getIvvc0211TabInfo1().getTabAlias(ws.getIxIndici().getStr()), ws.getIvvc0218().getAliasEstRappAnag())) {
            // COB_CODE: MOVE IVVC0211-BUFFER-DATI
            //               (IVVC0211-POSIZ-INI(IND-STR) :
            //                IVVC0211-LUNGHEZZA(IND-STR))
            //             TO WE15-AREA-EST-RAPP-ANAG
            ws.getAreaBusiness().setWe15AreaEstRappAnagFormatted(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1));
        }
        // COB_CODE: IF IVVC0211-TAB-ALIAS(IND-STR) =
        //              IVVC0218-ALIAS-RAPP-RETE
        //                TO WRRE-AREA-RAPP-RETE
        //           END-IF.
        if (Conditions.eq(inputIvvs0211.getIvvc0211TabInfo1().getTabAlias(ws.getIxIndici().getStr()), ws.getIvvc0218().getAliasRappRete())) {
            // COB_CODE: MOVE IVVC0211-BUFFER-DATI
            //               (IVVC0211-POSIZ-INI(IND-STR) :
            //                IVVC0211-LUNGHEZZA(IND-STR))
            //             TO WRRE-AREA-RAPP-RETE
            ws.getAreaBusiness().setWrreAreaRappReteFormatted(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1));
        }
        // COB_CODE: IF IVVC0211-TAB-ALIAS(IND-STR) =
        //              IVVC0218-ALIAS-SOPRAP-GAR
        //                TO WSPG-AREA-SOPRAP-GAR
        //           END-IF.
        if (Conditions.eq(inputIvvs0211.getIvvc0211TabInfo1().getTabAlias(ws.getIxIndici().getStr()), ws.getIvvc0218().getAliasSoprapGar())) {
            // COB_CODE: MOVE IVVC0211-BUFFER-DATI
            //               (IVVC0211-POSIZ-INI(IND-STR) :
            //                IVVC0211-LUNGHEZZA(IND-STR))
            //             TO WSPG-AREA-SOPRAP-GAR
            ws.getAreaBusiness().setWspgAreaSoprapGarFormatted(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1));
        }
        // COB_CODE: IF IVVC0211-TAB-ALIAS(IND-STR) =
        //              IVVC0218-ALIAS-STRA-INV
        //                TO WSDI-AREA-STRA-INV
        //           END-IF.
        if (Conditions.eq(inputIvvs0211.getIvvc0211TabInfo1().getTabAlias(ws.getIxIndici().getStr()), ws.getIvvc0218().getAliasStraInv())) {
            // COB_CODE: MOVE IVVC0211-BUFFER-DATI
            //               (IVVC0211-POSIZ-INI(IND-STR) :
            //                IVVC0211-LUNGHEZZA(IND-STR))
            //             TO WSDI-AREA-STRA-INV
            ws.getAreaBusiness().setWsdiAreaStraInvFormatted(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1));
        }
        // COB_CODE: IF IVVC0211-TAB-ALIAS(IND-STR) =
        //              IVVC0218-ALIAS-TIT-CONT
        //                TO WTIT-AREA-TIT-CONT
        //           END-IF.
        if (Conditions.eq(inputIvvs0211.getIvvc0211TabInfo1().getTabAlias(ws.getIxIndici().getStr()), ws.getIvvc0218().getAliasTitCont())) {
            // COB_CODE: MOVE IVVC0211-BUFFER-DATI
            //               (IVVC0211-POSIZ-INI(IND-STR) :
            //                IVVC0211-LUNGHEZZA(IND-STR))
            //             TO WTIT-AREA-TIT-CONT
            ws.getAreaBusiness().setWtitAreaTitContFormatted(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1));
        }
        // COB_CODE: IF IVVC0211-TAB-ALIAS(IND-STR) =
        //              IVVC0218-ALIAS-TIT-LIQ
        //                TO WTCL-AREA-TIT-LIQ
        //           END-IF.
        if (Conditions.eq(inputIvvs0211.getIvvc0211TabInfo1().getTabAlias(ws.getIxIndici().getStr()), ws.getIvvc0218().getAliasTitLiq())) {
            // COB_CODE: MOVE IVVC0211-BUFFER-DATI
            //               (IVVC0211-POSIZ-INI(IND-STR) :
            //                IVVC0211-LUNGHEZZA(IND-STR))
            //             TO WTCL-AREA-TIT-LIQ
            ws.getAreaBusiness().setWtclAreaTitLiqFormatted(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1));
        }
        // COB_CODE: IF IVVC0211-TAB-ALIAS(IND-STR) =
        //              IVVC0218-ALIAS-TRCH-GAR
        //                TO WTGA-AREA-TRANCHE
        //           END-IF.
        if (Conditions.eq(inputIvvs0211.getIvvc0211TabInfo1().getTabAlias(ws.getIxIndici().getStr()), ws.getIvvc0218().getAliasTrchGar())) {
            // COB_CODE: MOVE IVVC0211-BUFFER-DATI
            //               (IVVC0211-POSIZ-INI(IND-STR) :
            //                IVVC0211-LUNGHEZZA(IND-STR))
            //             TO WTGA-AREA-TRANCHE
            ws.getAreaBusiness().getWtgaAreaTranche().setWtgaAreaTrancheFormatted(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1));
        }
        // COB_CODE: IF IVVC0211-TAB-ALIAS(IND-STR) =
        //              IVVC0218-ALIAS-TRCH-GAR-OPZ
        //                TO WTOP-AREA-TRANCHE-OPZ
        //           END-IF.
        if (Conditions.eq(inputIvvs0211.getIvvc0211TabInfo1().getTabAlias(ws.getIxIndici().getStr()), ws.getIvvc0218().getAliasTrchGarOpz())) {
            // COB_CODE: MOVE IVVC0211-BUFFER-DATI
            //               (IVVC0211-POSIZ-INI(IND-STR) :
            //                IVVC0211-LUNGHEZZA(IND-STR))
            //             TO WTOP-AREA-TRANCHE-OPZ
            ws.getAreaBusiness().setWtopAreaTrancheOpzFormatted(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1));
        }
        // COB_CODE: IF IVVC0211-TAB-ALIAS(IND-STR) =
        //              IVVC0218-ALIAS-TRCH-LIQ
        //                TO WTLI-AREA-TRCH-LIQ
        //           END-IF.
        if (Conditions.eq(inputIvvs0211.getIvvc0211TabInfo1().getTabAlias(ws.getIxIndici().getStr()), ws.getIvvc0218().getAliasTrchLiq())) {
            // COB_CODE: MOVE IVVC0211-BUFFER-DATI
            //               (IVVC0211-POSIZ-INI(IND-STR) :
            //                IVVC0211-LUNGHEZZA(IND-STR))
            //             TO WTLI-AREA-TRCH-LIQ
            ws.getAreaBusiness().getWtliAreaTrchLiq().setWtliAreaTrchLiqFormatted(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1));
        }
        // COB_CODE: IF IVVC0211-TAB-ALIAS(IND-STR) =
        //              IVVC0218-ALIAS-DFLT-ADES
        //                TO WDAD-AREA-DEFAULT-ADES
        //           END-IF.
        if (Conditions.eq(inputIvvs0211.getIvvc0211TabInfo1().getTabAlias(ws.getIxIndici().getStr()), ws.getIvvc0218().getAliasDfltAdes())) {
            // COB_CODE: MOVE IVVC0211-BUFFER-DATI
            //               (IVVC0211-POSIZ-INI(IND-STR) :
            //                IVVC0211-LUNGHEZZA(IND-STR))
            //             TO WDAD-AREA-DEFAULT-ADES
            ws.getAreaBusiness().setWdadAreaDefaultAdesFormatted(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1));
        }
        // COB_CODE: IF IVVC0211-TAB-ALIAS(IND-STR) =
        //              IVVC0218-ALIAS-OGG-COLL
        //                TO WOCO-AREA-OGG-COLL
        //           END-IF.
        if (Conditions.eq(inputIvvs0211.getIvvc0211TabInfo1().getTabAlias(ws.getIxIndici().getStr()), ws.getIvvc0218().getAliasOggColl())) {
            // COB_CODE: MOVE IVVC0211-BUFFER-DATI
            //               (IVVC0211-POSIZ-INI(IND-STR) :
            //                IVVC0211-LUNGHEZZA(IND-STR))
            //             TO WOCO-AREA-OGG-COLL
            ws.getAreaBusiness().getWocoAreaOggColl().setWocoAreaOggCollFormatted(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1));
        }
        // COB_CODE: IF IVVC0211-TAB-ALIAS(IND-STR) =
        //              IVVC0218-ALIAS-DT-FORZ-LIQ
        //                TO WDFL-AREA-DFL
        //           END-IF.
        if (Conditions.eq(inputIvvs0211.getIvvc0211TabInfo1().getTabAlias(ws.getIxIndici().getStr()), ws.getIvvc0218().getAliasDtForzLiq())) {
            // COB_CODE: MOVE IVVC0211-BUFFER-DATI
            //               (IVVC0211-POSIZ-INI(IND-STR) :
            //                IVVC0211-LUNGHEZZA(IND-STR))
            //             TO WDFL-AREA-DFL
            ws.getAreaBusiness().setWdflAreaDflFormatted(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1));
        }
        // COB_CODE: IF IVVC0211-TAB-ALIAS(IND-STR) =
        //              IVVC0218-ALIAS-OPZIONI
        //                TO WOPZ-AREA-OPZIONI
        //           END-IF.
        if (Conditions.eq(inputIvvs0211.getIvvc0211TabInfo1().getTabAlias(ws.getIxIndici().getStr()), ws.getIvvc0218().getAliasOpzioni())) {
            // COB_CODE: MOVE IVVC0211-BUFFER-DATI
            //               (IVVC0211-POSIZ-INI(IND-STR) :
            //                IVVC0211-LUNGHEZZA(IND-STR))
            //             TO WOPZ-AREA-OPZIONI
            ws.getAreaBusiness().setWopzAreaOpzioniFormatted(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1));
        }
        // COB_CODE: IF IVVC0211-TAB-ALIAS(IND-STR) =
        //              IVVC0218-ALIAS-DATI-CONTEST
        //                TO WCNT-AREA-DATI-CONTEST
        //           END-IF.
        if (Conditions.eq(inputIvvs0211.getIvvc0211TabInfo1().getTabAlias(ws.getIxIndici().getStr()), ws.getIvvc0218().getAliasDatiContest())) {
            // COB_CODE: MOVE IVVC0211-BUFFER-DATI
            //               (IVVC0211-POSIZ-INI(IND-STR) :
            //                IVVC0211-LUNGHEZZA(IND-STR))
            //             TO WCNT-AREA-DATI-CONTEST
            ws.getAreaBusiness().setWcntAreaDatiContestFormatted(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1));
        }
        // COB_CODE: IF IVVC0211-TAB-ALIAS(IND-STR) =
        //              IVVC0218-ALIAS-RIS-DI-TRANCHE
        //                TO WRST-AREA-RST
        //           END-IF.
        if (Conditions.eq(inputIvvs0211.getIvvc0211TabInfo1().getTabAlias(ws.getIxIndici().getStr()), ws.getIvvc0218().getAliasRisDiTranche())) {
            // COB_CODE: MOVE IVVC0211-BUFFER-DATI
            //               (IVVC0211-POSIZ-INI(IND-STR) :
            //                IVVC0211-LUNGHEZZA(IND-STR))
            //             TO WRST-AREA-RST
            ws.getAreaBusiness().setWrstAreaRstFormatted(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1));
        }
        // COB_CODE: IF IVVC0211-TAB-ALIAS(IND-STR) =
        //              IVVC0218-ALIAS-QOTAZ-FON
        //                TO WL19-AREA-QUOTE
        //           END-IF.
        if (Conditions.eq(inputIvvs0211.getIvvc0211TabInfo1().getTabAlias(ws.getIxIndici().getStr()), ws.getIvvc0218().getAliasQotazFon())) {
            // COB_CODE: MOVE IVVC0211-BUFFER-DATI
            //               (IVVC0211-POSIZ-INI(IND-STR) :
            //                IVVC0211-LUNGHEZZA(IND-STR))
            //             TO WL19-AREA-QUOTE
            ws.getAreaBusiness().getWl19AreaQuote().setWl19AreaQuoteFormatted(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1));
        }
        // COB_CODE: IF IVVC0211-TAB-ALIAS(IND-STR) =
        //              IVVC0218-ALIAS-REINVST-POLI
        //                TO WL30-AREA-REINVST-POLI
        //           END-IF.
        if (Conditions.eq(inputIvvs0211.getIvvc0211TabInfo1().getTabAlias(ws.getIxIndici().getStr()), ws.getIvvc0218().getAliasReinvstPoli())) {
            // COB_CODE: MOVE IVVC0211-BUFFER-DATI
            //               (IVVC0211-POSIZ-INI(IND-STR) :
            //                IVVC0211-LUNGHEZZA(IND-STR))
            //             TO WL30-AREA-REINVST-POLI
            ws.getAreaBusiness().setWl30AreaReinvstPoliFormatted(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1));
        }
        // COB_CODE: IF IVVC0211-TAB-ALIAS(IND-STR) =
        //              IVVC0218-ALIAS-VINC-PEGN
        //                TO WL23-AREA-VINC-PEG
        //           END-IF.
        if (Conditions.eq(inputIvvs0211.getIvvc0211TabInfo1().getTabAlias(ws.getIxIndici().getStr()), ws.getIvvc0218().getAliasVincPegn())) {
            // COB_CODE: MOVE IVVC0211-BUFFER-DATI
            //               (IVVC0211-POSIZ-INI(IND-STR) :
            //                IVVC0211-LUNGHEZZA(IND-STR))
            //             TO WL23-AREA-VINC-PEG
            ws.getAreaBusiness().setWl23AreaVincPegFormatted(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1));
        }
        // COB_CODE: IF IVVC0211-TAB-ALIAS(IND-STR) =
        //              IVVC0218-ALIAS-CLAU-TEST
        //                TO WCLT-AREA-CLAU-TXT
        //           END-IF.
        if (Conditions.eq(inputIvvs0211.getIvvc0211TabInfo1().getTabAlias(ws.getIxIndici().getStr()), ws.getIvvc0218().getAliasClauTest())) {
            // COB_CODE: MOVE IVVC0211-BUFFER-DATI
            //               (IVVC0211-POSIZ-INI(IND-STR) :
            //                IVVC0211-LUNGHEZZA(IND-STR))
            //             TO WCLT-AREA-CLAU-TXT
            ws.getAreaBusiness().setWcltAreaClauTxtFormatted(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1));
        }
        // COB_CODE: IF IVVC0211-TAB-ALIAS(IND-STR) =
        //              IVVC0218-ALIAS-IMPOSTA-BOLLO
        //                TO WP58-AREA-IMPOSTA-BOLLO
        //           END-IF.
        if (Conditions.eq(inputIvvs0211.getIvvc0211TabInfo1().getTabAlias(ws.getIxIndici().getStr()), ws.getIvvc0218().getAliasImpostaBollo())) {
            // COB_CODE: MOVE IVVC0211-BUFFER-DATI
            //               (IVVC0211-POSIZ-INI(IND-STR) :
            //                IVVC0211-LUNGHEZZA(IND-STR))
            //             TO WP58-AREA-IMPOSTA-BOLLO
            ws.getAreaBusiness().setWp58AreaImpostaBolloFormatted(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1));
        }
        // COB_CODE: IF IVVC0211-TAB-ALIAS(IND-STR) =
        //              IVVC0218-ALIAS-DATI-CRIST
        //               TO WP61-AREA-D-CRIST
        //           END-IF.
        if (Conditions.eq(inputIvvs0211.getIvvc0211TabInfo1().getTabAlias(ws.getIxIndici().getStr()), ws.getIvvc0218().getAliasDatiCrist())) {
            // COB_CODE: MOVE IVVC0211-BUFFER-DATI
            //               (IVVC0211-POSIZ-INI(IND-STR) :
            //                IVVC0211-LUNGHEZZA(IND-STR))
            //            TO WP61-AREA-D-CRIST
            ws.getAreaBusiness().setWp61AreaDCristFormatted(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1));
        }
        // COB_CODE: IF IVVC0211-TAB-ALIAS(IND-STR) =
        //              IVVC0218-ALIAS-EST-POLI-CPI-PR
        //               TO WP67-AREA-EST-POLI-CPI-PR
        //           END-IF.
        if (Conditions.eq(inputIvvs0211.getIvvc0211TabInfo1().getTabAlias(ws.getIxIndici().getStr()), ws.getIvvc0218().getAliasEstPoliCpiPr())) {
            // COB_CODE: MOVE IVVC0211-BUFFER-DATI
            //               (IVVC0211-POSIZ-INI(IND-STR) :
            //                IVVC0211-LUNGHEZZA(IND-STR))
            //            TO WP67-AREA-EST-POLI-CPI-PR
            ws.getAreaBusiness().setWp67AreaEstPoliCpiPrFormatted(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1));
        }
        // COB_CODE: IF IVVC0211-TAB-ALIAS(IND-STR) =
        //              IVVC0218-ALIAS-RICH-EST
        //               TO WP01-AREA-RICH-EST
        //           END-IF.
        if (Conditions.eq(inputIvvs0211.getIvvc0211TabInfo1().getTabAlias(ws.getIxIndici().getStr()), ws.getIvvc0218().getAliasRichEst())) {
            // COB_CODE: MOVE IVVC0211-BUFFER-DATI
            //               (IVVC0211-POSIZ-INI(IND-STR) :
            //                IVVC0211-LUNGHEZZA(IND-STR))
            //            TO WP01-AREA-RICH-EST
            ws.getAreaBusiness().setWp01AreaRichEstFormatted(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1));
        }
        // COB_CODE: IF IVVC0211-TAB-ALIAS(IND-STR) =
        //              IVVC0218-ALIAS-AREA-FND-X-TRCH
        //                TO IVVC0222-AREA-FND-X-TRANCHE
        //           END-IF.
        if (Conditions.eq(inputIvvs0211.getIvvc0211TabInfo1().getTabAlias(ws.getIxIndici().getStr()), ws.getIvvc0218().getAliasAreaFndXTrch())) {
            // COB_CODE: MOVE IVVC0211-BUFFER-DATI
            //               (IVVC0211-POSIZ-INI(IND-STR) :
            //                IVVC0211-LUNGHEZZA(IND-STR))
            //             TO IVVC0222-AREA-FND-X-TRANCHE
            ws.getAreaBusiness().setIvvc0222AreaFndXTrancheFormatted(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1));
        }
        // COB_CODE: IF IVVC0211-TAB-ALIAS(IND-STR) =
        //              IVVC0218-ALIAS-AREA-VAR-X-GAR
        //                TO AREA-IVVC0223
        //           END-IF.
        if (Conditions.eq(inputIvvs0211.getIvvc0211TabInfo1().getTabAlias(ws.getIxIndici().getStr()), ws.getIvvc0218().getAliasAreaVarXGar())) {
            // COB_CODE: MOVE IVVC0211-BUFFER-DATI
            //               (IVVC0211-POSIZ-INI(IND-STR) :
            //                IVVC0211-LUNGHEZZA(IND-STR))
            //             TO AREA-IVVC0223
            ws.getAreaBusiness().setAreaIvvc0223Formatted(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1));
        }
        // COB_CODE: IF IVVC0211-TAB-ALIAS(IND-STR) =
        //              IVVC0218-ALIAS-ATT-SERV-VAL
        //                TO WP88-AREA-SERV-VAL
        //           END-IF.
        if (Conditions.eq(inputIvvs0211.getIvvc0211TabInfo1().getTabAlias(ws.getIxIndici().getStr()), ws.getIvvc0218().getAliasAttServVal())) {
            // COB_CODE: MOVE IVVC0211-BUFFER-DATI
            //               (IVVC0211-POSIZ-INI(IND-STR) :
            //                IVVC0211-LUNGHEZZA(IND-STR))
            //             TO WP88-AREA-SERV-VAL
            ws.getAreaBusiness().setWp88AreaServValFormatted(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1));
        }
        // COB_CODE: IF IVVC0211-TAB-ALIAS(IND-STR) =
        //              IVVC0218-ALIAS-D-ATT-SERV-VAL
        //                TO WP89-AREA-DSERV-VAL
        //           END-IF.
        if (Conditions.eq(inputIvvs0211.getIvvc0211TabInfo1().getTabAlias(ws.getIxIndici().getStr()), ws.getIvvc0218().getAliasDAttServVal())) {
            // COB_CODE: MOVE IVVC0211-BUFFER-DATI
            //               (IVVC0211-POSIZ-INI(IND-STR) :
            //                IVVC0211-LUNGHEZZA(IND-STR))
            //             TO WP89-AREA-DSERV-VAL
            ws.getAreaBusiness().setWp89AreaDservValFormatted(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1));
        }
        // COB_CODE: IF IVVC0211-TAB-ALIAS(IND-STR) =
        //              IVVC0218-ALIAS-QUEST-ADEG-VER
        //                TO WP56-AREA-QUEST-ADEG-VER
        //           END-IF.
        if (Conditions.eq(inputIvvs0211.getIvvc0211TabInfo1().getTabAlias(ws.getIxIndici().getStr()), ws.getIvvc0218().getAliasQuestAdegVer())) {
            // COB_CODE: MOVE IVVC0211-BUFFER-DATI
            //               (IVVC0211-POSIZ-INI(IND-STR) :
            //                IVVC0211-LUNGHEZZA(IND-STR))
            //             TO WP56-AREA-QUEST-ADEG-VER
            ws.getAreaBusiness().setWp56AreaQuestAdegVerFormatted(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1));
        }
        // COB_CODE: IF WPOL-FL-POLI-IFP NOT = 'P'
        //              END-IF
        //           END-IF.
        if (ws.getAreaBusiness().getLccvpol1().getDati().getWpolFlPoliIfp() != 'P') {
            // COB_CODE: IF IVVC0211-TAB-ALIAS(IND-STR) =
            //              IVVC0218-ALIAS-AREA-FND-X-CDG
            //                TO WCDG-AREA-COMMIS-GEST-VV
            //           END-IF
            if (Conditions.eq(inputIvvs0211.getIvvc0211TabInfo1().getTabAlias(ws.getIxIndici().getStr()), ws.getIvvc0218().getAliasAreaFndXCdg())) {
                // COB_CODE: MOVE IVVC0211-BUFFER-DATI
                //               (IVVC0211-POSIZ-INI(IND-STR) :
                //                IVVC0211-LUNGHEZZA(IND-STR))
                //             TO WCDG-AREA-COMMIS-GEST-VV
                ws.getAreaBusiness().getIvvc0224().setAreaCommisGestVvFormatted(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1));
            }
        }
    }

    /**Original name: A100-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*
	 * --> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 * --> RISPETTIVE AREE DELLA COPY IVVC0214</pre>*/
    private void a100Elaborazione() {
        // COB_CODE: PERFORM A010-VALORIZZA-DCLGEN THRU A010-EX
        //                   VARYING IND-STR FROM 1 BY 1
        //                     UNTIL IND-STR > IVVC0211-ELE-INFO-MAX OR
        //                           IVVC0211-TAB-ALIAS(IND-STR) =
        //                      SPACES OR LOW-VALUE OR HIGH-VALUE
        ws.getIxIndici().setStr(((short)1));
        while (!(ws.getIxIndici().getStr() > inputIvvs0211.getIvvc0211DatiInput().getEleInfoMax() || Characters.EQ_SPACE.test(inputIvvs0211.getIvvc0211TabInfo1().getTabAlias(ws.getIxIndici().getStr())) || Characters.EQ_LOW.test(inputIvvs0211.getIvvc0211TabInfo1().getTabAliasFormatted(ws.getIxIndici().getStr())) || Characters.EQ_HIGH.test(inputIvvs0211.getIvvc0211TabInfo1().getTabAliasFormatted(ws.getIxIndici().getStr())))) {
            a010ValorizzaDclgen();
            ws.getIxIndici().setStr(Trunc.toShort(ws.getIxIndici().getStr() + 1, 4));
        }
        //--> LA TRASCODIFICA DEL MOVIMENTO DA PTF A ACT DEVE ESSERE
        //--> EFFETTUATA SOLO SE CI TROVIAMO IN CONVERSAZIONE
        // COB_CODE: IF IVVC0211-IN-CONV
        //              PERFORM T000-TP-MOVI-PTF-TO-ACT  THRU T000-EX
        //           END-IF.
        if (inputIvvs0211.getIvvc0211DatiInput().getStepElab().isInConv()) {
            // COB_CODE: PERFORM T000-TP-MOVI-PTF-TO-ACT  THRU T000-EX
            t000TpMoviPtfToAct();
        }
        // COB_CODE: IF IVVC0211-SUCCESSFUL-RC
        //              END-IF
        //           END-IF
        if (inputIvvs0211.getIvvc0211RestoDati().getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM E000-ESTRAI-STAT-CAUS    THRU E000-EX
            e000EstraiStatCaus();
            // COB_CODE: IF IVVC0211-SUCCESSFUL-RC
            //              PERFORM L000-GESTIONE-LIVELLI THRU L000-EX
            //           END-IF
            if (inputIvvs0211.getIvvc0211RestoDati().getReturnCode().isSuccessfulRc()) {
                // COB_CODE: PERFORM L000-GESTIONE-LIVELLI THRU L000-EX
                l000GestioneLivelli();
            }
        }
        // COB_CODE: IF IVVC0211-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (inputIvvs0211.getIvvc0211RestoDati().getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IVVC0211-FLAG-GAR-OPZIONE-SI
            //              PERFORM G000-GESTIONE-OPZIONI     THRU G000-EX
            //           END-IF
            if (inputIvvs0211.getIvvc0211DatiInput().getFlagGarOpzione().isSi()) {
                // COB_CODE: SET TIPO-OPZIONI                  TO TRUE
                ws.getFlagTipologiaScheda().setOpzioni();
                // COB_CODE: PERFORM G000-GESTIONE-OPZIONI     THRU G000-EX
                g000GestioneOpzioni();
            }
            // COB_CODE: IF IVVC0211-SUCCESSFUL-RC
            //              END-IF
            //           END-IF
            if (inputIvvs0211.getIvvc0211RestoDati().getReturnCode().isSuccessfulRc()) {
                // COB_CODE: IF IVVC0211-POST-CONV
                //              PERFORM P000-ESTRAI-AUT-OPER   THRU P000-EX
                //           END-IF
                if (inputIvvs0211.getIvvc0211DatiInput().getStepElab().isPostConv()) {
                    // COB_CODE: PERFORM P000-ESTRAI-AUT-OPER   THRU P000-EX
                    p000EstraiAutOper();
                }
                // COB_CODE: IF IVVC0211-SUCCESSFUL-RC
                //              PERFORM P100-ELABORA-VARIABILI THRU P100-EX
                //           END-IF
                if (inputIvvs0211.getIvvc0211RestoDati().getReturnCode().isSuccessfulRc()) {
                    // COB_CODE: PERFORM P100-ELABORA-VARIABILI THRU P100-EX
                    p100ElaboraVariabili();
                }
            }
        }
    }

    /**Original name: C000-GESTIONE-DT-VLDT-PROD<br>
	 * <pre>----------------------------------------------------------------*
	 *     GESTIONE DATA INIZIO VALIDITA' TARIFFA DI TRANCHE
	 * ----------------------------------------------------------------*</pre>*/
    private void c000GestioneDtVldtProd() {
        // COB_CODE: PERFORM C050-CNTL-DT-VLDT-PROD THRU C050-EX.
        c050CntlDtVldtProd();
    }

    /**Original name: C050-CNTL-DT-VLDT-PROD<br>
	 * <pre>----------------------------------------------------------------*
	 *     VERIFICA LA VALIDITA' DEL CAMPO WTGA-DT-VLDT-PROD(IND-TGA)
	 * ----------------------------------------------------------------*</pre>*/
    private void c050CntlDtVldtProd() {
        ConcatUtil concatUtil = null;
        // COB_CODE: IF WTGA-DT-VLDT-PROD (IND-TGA) IS NUMERIC AND
        //              WTGA-DT-VLDT-PROD (IND-TGA) NOT ZERO
        //              END-IF
        //           ELSE
        //              END-STRING
        //           END-IF.
        if (Functions.isNumber(ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().getDtVldtProd(ws.getIxIndici().getTga())) && ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().getDtVldtProd(ws.getIxIndici().getTga()) != 0) {
            // COB_CODE: IF GESTIONE-PRODOTTO
            //              PERFORM C060-DISTINCT-DT-VLDT-PROD-TGA THRU C060-EX
            //           ELSE
            //              SET DT-VLDT-PROD-VALIDA-SI           TO TRUE
            //           END-IF
            if (ws.getFlagGestione().isProdotto()) {
                // COB_CODE: PERFORM C060-DISTINCT-DT-VLDT-PROD-TGA THRU C060-EX
                c060DistinctDtVldtProdTga();
            }
            else {
                // COB_CODE: SET DT-VLDT-PROD-VALIDA-SI           TO TRUE
                ws.getFlagDtVldtProd().setSi();
            }
            // COB_CODE: IF DT-VLDT-PROD-VALIDA-SI
            //              PERFORM M000-GESTIONE-VAR-FUN-CALC     THRU M000-EX
            //           END-IF
            if (ws.getFlagDtVldtProd().isSi()) {
                // COB_CODE: PERFORM M000-GESTIONE-VAR-FUN-CALC     THRU M000-EX
                m000GestioneVarFunCalc();
            }
        }
        else {
            // COB_CODE: SET IVVC0211-DT-VLDT-PROD-NOT-V        TO TRUE
            inputIvvs0211.getIvvc0211RestoDati().getReturnCode().setDtVldtProdNotV();
            // COB_CODE: MOVE WK-PGM          TO IVVC0211-COD-SERVIZIO-BE
            inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: STRING 'DATA VALIDITA'' PRODOTTO DI TRANCHE NON VALIDA'
            //               DELIMITED BY SIZE INTO
            //               IVVC0211-DESCRIZ-ERR
            //           END-STRING
            inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setDescrizErr("DATA VALIDITA' PRODOTTO DI TRANCHE NON VALIDA");
        }
    }

    /**Original name: C060-DISTINCT-DT-VLDT-PROD-TGA<br>
	 * <pre>----------------------------------------------------------------*
	 *     ESEGUE DISTINCT SULLA DATA VALIDITA' PRODOTTO DELLA TGA
	 * ----------------------------------------------------------------*</pre>*/
    private void c060DistinctDtVldtProdTga() {
        boolean endOfTable = false;
        // COB_CODE: SET DIST-SEARCH-IND                   TO 1
        ws.setDistSearchInd(1);
        // COB_CODE: SEARCH  DIST-TAB-INFO
        //                   AT END
        //                   SET DT-VLDT-PROD-VALIDA-SI    TO TRUE
        //                   ADD 1                         TO IND-TAB-DIST
        //                   MOVE WTGA-DT-VLDT-PROD (IND-TGA)
        //                        TO DIST-TAB-DT-VLDT-PROD (IND-TAB-DIST)
        //              WHEN DIST-TAB-DT-VLDT-PROD(DIST-SEARCH-IND) =
        //                       WTGA-DT-VLDT-PROD (IND-TGA)
        //                   SET DT-VLDT-PROD-VALIDA-NO    TO TRUE
        //                        TO DIST-TAB-DT-VLDT-PROD (IND-TAB-DIST)
        //              WHEN DIST-TAB-DT-VLDT-PROD(DIST-SEARCH-IND) =
        //                       WTGA-DT-VLDT-PROD (IND-TGA)
        //                   SET DT-VLDT-PROD-VALIDA-NO    TO TRUE
        //           END-SEARCH.
        endOfTable = true;
        while (ws.getDistSearchInd() <= 300) {
            if (ws.getStrDistDtVldtProdTga().getDistTabDtVldtProd(ws.getDistSearchInd()) == ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().getDtVldtProd(ws.getIxIndici().getTga())) {
                endOfTable = false;
                // COB_CODE: SET DT-VLDT-PROD-VALIDA-NO    TO TRUE
                ws.getFlagDtVldtProd().setNo();
                break;
            }
            ws.setDistSearchInd(Trunc.toInt(ws.getDistSearchInd() + 1, 9));
        }
        //AT END GROUP;
        if (endOfTable) {
            // COB_CODE: SET DT-VLDT-PROD-VALIDA-SI    TO TRUE
            ws.getFlagDtVldtProd().setSi();
            // COB_CODE: ADD 1                         TO IND-TAB-DIST
            ws.getIxIndici().setTabDist(Trunc.toShort(1 + ws.getIxIndici().getTabDist(), 4));
            // COB_CODE: MOVE WTGA-DT-VLDT-PROD (IND-TGA)
            //                TO DIST-TAB-DT-VLDT-PROD (IND-TAB-DIST)
            ws.getStrDistDtVldtProdTga().setDistTabDtVldtProd(ws.getIxIndici().getTabDist(), ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().getDtVldtProd(ws.getIxIndici().getTga()));
        }
    }

    /**Original name: E000-ESTRAI-STAT-CAUS<br>
	 * <pre>----------------------------------------------------------------*
	 *     ESTRAZIONE STATO E CAUSALE
	 * ----------------------------------------------------------------*
	 * --> VALORIZZAZIONE DCLGEN</pre>*/
    private void e000EstraiStatCaus() {
        Ldbs1400 ldbs1400 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE IVVC0211-COD-COMPAGNIA-ANIA TO MVV-COD-COMPAGNIA-ANIA
        ws.getMatrValVar().setMvvCodCompagniaAnia(inputIvvs0211.getIvvc0211DatiInput().getCodCompagniaAnia());
        // COB_CODE: MOVE IVVC0211-TIPO-MOVIMENTO     TO MVV-TIPO-MOVIMENTO
        ws.getMatrValVar().getMvvTipoMovimento().setMvvTipoMovimento(inputIvvs0211.getIvvc0211DatiInput().getTipoMovimento());
        // COB_CODE: MOVE 'A'                         TO MVV-STEP-VALORIZZATORE
        ws.getMatrValVar().setMvvStepValorizzatoreFormatted("A");
        // COB_CODE: MOVE IVVC0211-STEP-ELAB          TO MVV-STEP-CONVERSAZIONE
        ws.getMatrValVar().setMvvStepConversazione(inputIvvs0211.getIvvc0211DatiInput().getStepElab().getStepElab());
        //--> LIVELLO OPERAZIONE
        // COB_CODE: SET  IDSV0003-PRIMARY-KEY    TO TRUE
        ws.getIdsv0003().getLivelloOperazione().setPrimaryKey();
        //--> TIPO OPERAZIONE
        // COB_CODE: SET  IDSV0003-SELECT         TO TRUE
        ws.getIdsv0003().getOperazione().setSelect();
        //--> TRATTAMENTO-STORICITA
        // COB_CODE: SET IDSV0003-TRATT-SENZA-STOR TO TRUE
        ws.getIdsv0003().getTrattamentoStoricita().setTrattSenzaStor();
        //--> INIZIALIZZA CODICE DI RITORNO
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC    TO TRUE
        ws.getIdsv0003().getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE
        ws.getIdsv0003().getSqlcode().setSuccessfulSql();
        // COB_CODE: CALL PGM-LDBS1400           USING IDSV0003
        //                                             MATR-VAL-VAR
        ldbs1400 = Ldbs1400.getInstance();
        ldbs1400.run(ws.getIdsv0003(), ws.getMatrValVar());
        // COB_CODE:      IF IDSV0003-SUCCESSFUL-RC
        //                    END-EVALUATE
        //                ELSE
        //           *-->     GESTIRE ERRORE DISPATCHER
        //                    END-STRING
        //                END-IF.
        if (ws.getIdsv0003().getReturnCode().isSuccessfulRc()) {
            // COB_CODE:          EVALUATE TRUE
            //                        WHEN IDSV0003-SUCCESSFUL-SQL
            //           *-->         OPERAZIONE ESEGUITA CORRETTAMENTE
            //                            END-IF
            //                        WHEN IDSV0003-NOT-FOUND
            //           *--->        CAMPO $ NON TROVATO
            //                           SET STAT-CAUS-TROVATI-NO   TO TRUE
            //                        WHEN IDSV0003-MORE-THAN-ONE-ROW
            //           *--->        ERRORE DI ACCESSO AL DB
            //                           END-STRING
            //                        WHEN OTHER
            //           *--->        ERRORE DI ACCESSO AL DB
            //                          END-STRING
            //                    END-EVALUATE
            switch (ws.getIdsv0003().getSqlcode().getSqlcode()) {

                case Idsv0003Sqlcode.SUCCESSFUL_SQL://-->         OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: SET STAT-CAUS-TROVATI-SI  TO TRUE
                    ws.getFlagStatCaus().setSi();
                    // COB_CODE: PERFORM E500-VALORIZZA-STAT-CAUS     THRU E500-EX
                    e500ValorizzaStatCaus();
                    // COB_CODE: IF IVVC0211-SUCCESSFUL-RC
                    //              PERFORM S000-VALORIZZA-TIPO-SKEDE THRU S000-EX
                    //           END-IF
                    if (inputIvvs0211.getIvvc0211RestoDati().getReturnCode().isSuccessfulRc()) {
                        // COB_CODE: PERFORM S000-VALORIZZA-TIPO-SKEDE THRU S000-EX
                        s000ValorizzaTipoSkede();
                    }
                    break;

                case Idsv0003Sqlcode.NOT_FOUND://--->        CAMPO $ NON TROVATO
                    // COB_CODE: SET STAT-CAUS-TROVATI-NO   TO TRUE
                    ws.getFlagStatCaus().setNo();
                    break;

                case Idsv0003Sqlcode.MORE_THAN_ONE_ROW://--->        ERRORE DI ACCESSO AL DB
                    // COB_CODE: SET FINE-ELAB-SI           TO TRUE
                    ws.getFlagFineElab().setSi();
                    // COB_CODE: SET IVVC0211-SQL-ERROR     TO TRUE
                    inputIvvs0211.getIvvc0211RestoDati().getReturnCode().setSqlError();
                    // COB_CODE: MOVE PGM-LDBS1400
                    //                        TO IVVC0211-COD-SERVIZIO-BE
                    inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setCodServizioBe(ws.getPgmLdbs1400());
                    // COB_CODE: MOVE IDSV0003-SQLCODE TO WS-SQLCODE
                    ws.setWsSqlcode(ws.getIdsv0003().getSqlcode().getSqlcode());
                    // COB_CODE: STRING 'TROVATE PIU'' OCCORRENZE SU '
                    //                  'MATR_VAL_VAR PER STATO E CAUSALE'
                    //                  ' - '
                    //                  'SQLCODE : '
                    //                  WS-SQLCODE
                    //                  DELIMITED BY SIZE INTO
                    //                  IVVC0211-DESCRIZ-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ivvc0211CampiEsito.Len.DESCRIZ_ERR, "TROVATE PIU' OCCORRENZE SU ", "MATR_VAL_VAR PER STATO E CAUSALE", " - ", "SQLCODE : ", ws.getWsSqlcodeAsString());
                    inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setDescrizErr(concatUtil.replaceInString(inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().getDescrizErrFormatted()));
                    break;

                default://--->        ERRORE DI ACCESSO AL DB
                    // COB_CODE: SET FINE-ELAB-SI           TO TRUE
                    ws.getFlagFineElab().setSi();
                    // COB_CODE: SET IVVC0211-SQL-ERROR     TO TRUE
                    inputIvvs0211.getIvvc0211RestoDati().getReturnCode().setSqlError();
                    // COB_CODE: MOVE PGM-LDBS1400 TO IVVC0211-COD-SERVIZIO-BE
                    inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setCodServizioBe(ws.getPgmLdbs1400());
                    // COB_CODE: MOVE IDSV0003-SQLCODE TO WS-SQLCODE
                    ws.setWsSqlcode(ws.getIdsv0003().getSqlcode().getSqlcode());
                    // COB_CODE: STRING 'MATR_VAL_VAR'
                    //                ' - '
                    //                'RC : '
                    //               IDSV0003-RETURN-CODE
                    //                ' - '
                    //                'SQLCODE : '
                    //               WS-SQLCODE
                    //               DELIMITED BY SIZE INTO
                    //               IVVC0211-DESCRIZ-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ivvc0211CampiEsito.Len.DESCRIZ_ERR, new String[] {"MATR_VAL_VAR", " - ", "RC : ", ws.getIdsv0003().getReturnCode().getReturnCodeFormatted(), " - ", "SQLCODE : ", ws.getWsSqlcodeAsString()});
                    inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setDescrizErr(concatUtil.replaceInString(inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().getDescrizErrFormatted()));
                    break;
            }
        }
        else {
            //-->     GESTIRE ERRORE DISPATCHER
            // COB_CODE: SET IVVC0211-SQL-ERROR  TO TRUE
            inputIvvs0211.getIvvc0211RestoDati().getReturnCode().setSqlError();
            // COB_CODE: MOVE PGM-LDBS1400       TO IVVC0211-COD-SERVIZIO-BE
            inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setCodServizioBe(ws.getPgmLdbs1400());
            // COB_CODE: STRING PGM-LDBS1400
            //                  ' - '
            //                  'RC : '
            //                  IDSV0003-RETURN-CODE
            //                  DELIMITED BY SIZE INTO
            //                  IVVC0211-DESCRIZ-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ivvc0211CampiEsito.Len.DESCRIZ_ERR, ws.getPgmLdbs1400Formatted(), " - ", "RC : ", ws.getIdsv0003().getReturnCode().getReturnCodeFormatted());
            inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setDescrizErr(concatUtil.replaceInString(inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().getDescrizErrFormatted()));
        }
    }

    /**Original name: E999-CNTL-STAT-CAUS<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZA L'AREA VARIABILI STATO , CAUSALE
	 * ----------------------------------------------------------------*</pre>*/
    private void e999CntlStatCaus() {
        ConcatUtil concatUtil = null;
        // COB_CODE: IF STAT-CAUS-TROVATI-NO
        //              END-IF
        //           END-IF.
        if (ws.getFlagStatCaus().isNo()) {
            // COB_CODE: SET FINE-ELAB-SI              TO TRUE
            ws.getFlagFineElab().setSi();
            // COB_CODE: IF NOT IVVC0211-PRE-CONV            AND
            //              NOT IVVC0211-POST-CONV           AND
            //              NOT IVVC0211-PRE-CALC-INIZIATIVA AND
            //              NOT IVVC0211-POST-CALC-INIZIATIVA
            //                      TO IVVC0211-NOME-TABELLA
            //           END-IF
            if (!inputIvvs0211.getIvvc0211DatiInput().getStepElab().isPreConv() && !inputIvvs0211.getIvvc0211DatiInput().getStepElab().isPostConv() && !inputIvvs0211.getIvvc0211DatiInput().getStepElab().isPreCalcIniziativa() && !inputIvvs0211.getIvvc0211DatiInput().getStepElab().isPostCalcIniziativa()) {
                // COB_CODE: SET IVVC0211-SQL-ERROR     TO TRUE
                inputIvvs0211.getIvvc0211RestoDati().getReturnCode().setSqlError();
                // COB_CODE: MOVE PGM-LDBS1400
                //                     TO IVVC0211-COD-SERVIZIO-BE
                inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setCodServizioBe(ws.getPgmLdbs1400());
                // COB_CODE: STRING 'NON ESISTONO STATI E CAUSALI '
                //                  'PER ACCEDERE SUL DATABASE'
                //                DELIMITED BY SIZE INTO
                //                IVVC0211-DESCRIZ-ERR
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Ivvc0211CampiEsito.Len.DESCRIZ_ERR, "NON ESISTONO STATI E CAUSALI ", "PER ACCEDERE SUL DATABASE");
                inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setDescrizErr(concatUtil.replaceInString(inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().getDescrizErrFormatted()));
                // COB_CODE: MOVE 'MATR_VAL_VAR'
                //                   TO IVVC0211-NOME-TABELLA
                inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setNomeTabella("MATR_VAL_VAR");
            }
        }
    }

    /**Original name: E500-VALORIZZA-STAT-CAUS<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZA L'AREA VARIABILI STATO , CAUSALE
	 * ----------------------------------------------------------------*</pre>*/
    private void e500ValorizzaStatCaus() {
        // COB_CODE: INITIALIZE                     COMODO-STAT-CAUS.
        initComodoStatCaus();
        // COB_CODE: MOVE MVV-OPER-LOG-STATO-BUS TO COMODO-OPER-LOG-STAT-BUS.
        ws.getComodoStatCaus().setOperLogStatBus(ws.getMatrValVar().getMvvOperLogStatoBus());
        // COB_CODE: MOVE MVV-OPER-LOG-CAUSALE   TO COMODO-OPER-LOG-CAUS.
        ws.getComodoStatCaus().setOperLogCaus(ws.getMatrValVar().getMvvOperLogCausale());
        // COB_CODE: PERFORM E501-PREPARA-UNZIP-STAT     THRU E501-EX
        e501PreparaUnzipStat();
        // COB_CODE: PERFORM U999-UNZIP-STRING           THRU U999-EX
        u999UnzipString();
        // COB_CODE: IF IVVC0211-SUCCESSFUL-RC
        //              PERFORM E502-CARICA-STAT-UNZIPPED   THRU E502-EX
        //           END-IF.
        if (inputIvvs0211.getIvvc0211RestoDati().getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM E502-CARICA-STAT-UNZIPPED   THRU E502-EX
            e502CaricaStatUnzipped();
        }
        // COB_CODE: IF IVVC0211-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (inputIvvs0211.getIvvc0211RestoDati().getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM E503-PREPARA-UNZIP-CAUS     THRU E503-EX
            e503PreparaUnzipCaus();
            // COB_CODE: PERFORM U999-UNZIP-STRING           THRU U999-EX
            u999UnzipString();
            // COB_CODE: IF IVVC0211-SUCCESSFUL-RC
            //              PERFORM E504-CARICA-CAUS-UNZIPPED   THRU E504-EX
            //           END-IF
            if (inputIvvs0211.getIvvc0211RestoDati().getReturnCode().isSuccessfulRc()) {
                // COB_CODE: PERFORM E504-CARICA-CAUS-UNZIPPED   THRU E504-EX
                e504CaricaCausUnzipped();
            }
        }
    }

    /**Original name: E501-PREPARA-UNZIP-STAT<br>
	 * <pre>----------------------------------------------------------------*
	 *    PREPARA STRINGA DA UNZIPPARE
	 * ----------------------------------------------------------------*
	 *      INITIALIZE UNZIP-STRUCTURE</pre>*/
    private void e501PreparaUnzipStat() {
        // COB_CODE: INITIALIZE              UNZIP-LENGTH-STR-MAX
        //                                   UNZIP-LENGTH-FIELD
        //                                   UNZIP-STRING-ZIPPED
        //                                   UNZIP-IDENTIFICATORE
        //                                   UNZIP-ELE-VARIABILI-MAX
        //                                   UNZIP-COD-VARIABILE(1).
        ws.getUnzipStructure().setUnzipLengthStrMaxFormatted("0000");
        ws.getUnzipStructure().setUnzipLengthFieldFormatted("00");
        ws.getUnzipStructure().setUnzipStringZipped("");
        ws.getUnzipStructure().setUnzipIdentificatore(Types.SPACE_CHAR);
        ws.getUnzipStructure().setUnzipEleVariabiliMax(((short)0));
        ws.getUnzipStructure().getUnzipTab().setCodVariabile(1, "");
        // COB_CODE: MOVE UNZIP-TAB       TO  UNZIP-RESTO-TAB.
        ws.getUnzipStructure().getUnzipTab().setRestoTab(ws.getUnzipStructure().getUnzipTab().getUnzipTabFormatted());
        // COB_CODE: MOVE LENGTH OF MVV-ARRAY-STATO-BUS TO UNZIP-LENGTH-STR-MAX.
        ws.getUnzipStructure().setUnzipLengthStrMax(((short)MatrValVarLdbs1400.Len.MVV_ARRAY_STATO_BUS));
        // COB_CODE: MOVE LENGTH OF COMODO-TP-STAT-BUS(1)
        //                                              TO UNZIP-LENGTH-FIELD
        ws.getUnzipStructure().setUnzipLengthField(Trunc.toShort(ComodoStatBusEle.Len.COMODO_TP_STAT_BUS, 2));
        // COB_CODE: MOVE MVV-ARRAY-STATO-BUS           TO UNZIP-STRING-ZIPPED.
        ws.getUnzipStructure().setUnzipStringZipped(ws.getMatrValVar().getMvvArrayStatoBus());
        // COB_CODE: MOVE ','                           TO UNZIP-IDENTIFICATORE.
        ws.getUnzipStructure().setUnzipIdentificatoreFormatted(",");
    }

    /**Original name: E502-CARICA-STAT-UNZIPPED<br>
	 * <pre>----------------------------------------------------------------*
	 *  CARICA CAMPI UNZIPPED
	 * ----------------------------------------------------------------*</pre>*/
    private void e502CaricaStatUnzipped() {
        ConcatUtil concatUtil = null;
        // COB_CODE: SET UNZIP-TROVATO-NO    TO TRUE
        ws.getFlagUnzipTrovato().setNo();
        // COB_CODE: PERFORM VARYING IND-UNZIP FROM 1 BY 1
        //                   UNTIL IND-UNZIP  >  LIMITE-VARIABILI-UNZIPPED OR
        //                         IND-UNZIP  >  UNZIP-ELE-VARIABILI-MAX   OR
        //                         UNZIP-COD-VARIABILE(IND-UNZIP) =
        //                         SPACES OR LOW-VALUE OR HIGH-VALUE
        //                   SET UNZIP-TROVATO-SI    TO TRUE
        //           END-PERFORM.
        ws.getIxIndici().setUnzip(((short)1));
        while (!(ws.getIxIndici().getUnzip() > ws.getLimiteVariabiliUnzipped() || ws.getIxIndici().getUnzip() > ws.getUnzipStructure().getUnzipEleVariabiliMax() || Characters.EQ_SPACE.test(ws.getUnzipStructure().getUnzipTab().getCodVariabile(ws.getIxIndici().getUnzip())) || Characters.EQ_LOW.test(ws.getUnzipStructure().getUnzipTab().getCodVariabile(ws.getIxIndici().getUnzip()), UnzipTab.Len.COD_VARIABILE) || Characters.EQ_HIGH.test(ws.getUnzipStructure().getUnzipTab().getCodVariabile(ws.getIxIndici().getUnzip()), UnzipTab.Len.COD_VARIABILE))) {
            // COB_CODE: MOVE UNZIP-COD-VARIABILE(IND-UNZIP)
            //                                   (1:UNZIP-LENGTH-FIELD)
            //             TO COMODO-TP-STAT-BUS(IND-UNZIP)
            ws.getComodoStatCaus().getStatBusEle(ws.getIxIndici().getUnzip()).setComodoTpStatBus(ws.getUnzipStructure().getUnzipTab().getCodVariabileFormatted(ws.getIxIndici().getUnzip()).substring((1) - 1, ws.getUnzipStructure().getUnzipLengthField()));
            // COB_CODE: SET UNZIP-TROVATO-SI    TO TRUE
            ws.getFlagUnzipTrovato().setSi();
            ws.getIxIndici().setUnzip(Trunc.toShort(ws.getIxIndici().getUnzip() + 1, 4));
        }
        //--->  STATO NON TROVATO
        // COB_CODE: IF UNZIP-TROVATO-NO
        //              MOVE 'MATR_VAL_VAR'        TO IVVC0211-NOME-TABELLA
        //           END-IF.
        if (ws.getFlagUnzipTrovato().isNo()) {
            // COB_CODE: SET FINE-ELAB-SI           TO TRUE
            ws.getFlagFineElab().setSi();
            // COB_CODE: SET IVVC0211-GENERIC-ERROR TO TRUE
            inputIvvs0211.getIvvc0211RestoDati().getReturnCode().setGenericError();
            // COB_CODE: MOVE WK-PGM                TO IVVC0211-COD-SERVIZIO-BE
            inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: STRING 'STATO NON TROVATO SU MATR-VAL-VAR'
            //                  ' - '
            //                  'COMPAGNIA : '
            //                  WK-CODICE-COMPAGNIA
            //                  ' - '
            //                  'TIPO MOVIM. : '
            //                  WK-TIPO-MOVIMENTO
            //                  ' - '
            //                  'STEP VALOR. : '
            //                  MVV-STEP-VALORIZZATORE
            //                  ' - '
            //                  'STEP CONVER. : '
            //                  MVV-STEP-CONVERSAZIONE
            //                  DELIMITED BY SIZE INTO
            //                  IVVC0211-DESCRIZ-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ivvc0211CampiEsito.Len.DESCRIZ_ERR, new String[] {"STATO NON TROVATO SU MATR-VAL-VAR", " - ", "COMPAGNIA : ", ws.getWkCodiceCompagniaAsString(), " - ", "TIPO MOVIM. : ", ws.getWkTipoMovimentoAsString(), " - ", "STEP VALOR. : ", String.valueOf(ws.getMatrValVar().getMvvStepValorizzatore()), " - ", "STEP CONVER. : ", String.valueOf(ws.getMatrValVar().getMvvStepConversazione())});
            inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setDescrizErr(concatUtil.replaceInString(inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().getDescrizErrFormatted()));
            // COB_CODE: MOVE 'MATR_VAL_VAR'        TO IVVC0211-NOME-TABELLA
            inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setNomeTabella("MATR_VAL_VAR");
        }
    }

    /**Original name: E503-PREPARA-UNZIP-CAUS<br>
	 * <pre>----------------------------------------------------------------*
	 *    PREPARA STRINGA DA UNZIPPARE
	 * ----------------------------------------------------------------*
	 *      INITIALIZE UNZIP-STRUCTURE</pre>*/
    private void e503PreparaUnzipCaus() {
        // COB_CODE: INITIALIZE              UNZIP-LENGTH-STR-MAX
        //                                   UNZIP-LENGTH-FIELD
        //                                   UNZIP-STRING-ZIPPED
        //                                   UNZIP-IDENTIFICATORE
        //                                   UNZIP-ELE-VARIABILI-MAX
        //                                   UNZIP-COD-VARIABILE(1).
        ws.getUnzipStructure().setUnzipLengthStrMaxFormatted("0000");
        ws.getUnzipStructure().setUnzipLengthFieldFormatted("00");
        ws.getUnzipStructure().setUnzipStringZipped("");
        ws.getUnzipStructure().setUnzipIdentificatore(Types.SPACE_CHAR);
        ws.getUnzipStructure().setUnzipEleVariabiliMax(((short)0));
        ws.getUnzipStructure().getUnzipTab().setCodVariabile(1, "");
        // COB_CODE: MOVE UNZIP-TAB       TO  UNZIP-RESTO-TAB.
        ws.getUnzipStructure().getUnzipTab().setRestoTab(ws.getUnzipStructure().getUnzipTab().getUnzipTabFormatted());
        // COB_CODE: MOVE LENGTH OF MVV-ARRAY-CAUSALE   TO UNZIP-LENGTH-STR-MAX.
        ws.getUnzipStructure().setUnzipLengthStrMax(((short)MatrValVarLdbs1400.Len.MVV_ARRAY_CAUSALE));
        // COB_CODE: MOVE LENGTH OF COMODO-TP-CAUS(1)
        //                                              TO UNZIP-LENGTH-FIELD
        ws.getUnzipStructure().setUnzipLengthField(Trunc.toShort(ComodoTpCausEle.Len.COMODO_TP_CAUS, 2));
        // COB_CODE: MOVE MVV-ARRAY-CAUSALE             TO UNZIP-STRING-ZIPPED.
        ws.getUnzipStructure().setUnzipStringZipped(ws.getMatrValVar().getMvvArrayCausale());
        // COB_CODE: MOVE ','                           TO UNZIP-IDENTIFICATORE.
        ws.getUnzipStructure().setUnzipIdentificatoreFormatted(",");
    }

    /**Original name: E504-CARICA-CAUS-UNZIPPED<br>
	 * <pre>----------------------------------------------------------------*
	 *  CARICA CAMPI UNZIPPED
	 * ----------------------------------------------------------------*</pre>*/
    private void e504CaricaCausUnzipped() {
        ConcatUtil concatUtil = null;
        // COB_CODE: SET UNZIP-TROVATO-NO    TO TRUE
        ws.getFlagUnzipTrovato().setNo();
        // COB_CODE: PERFORM VARYING IND-UNZIP FROM 1 BY 1
        //                   UNTIL IND-UNZIP  >  LIMITE-VARIABILI-UNZIPPED OR
        //                         IND-UNZIP  >  UNZIP-ELE-VARIABILI-MAX   OR
        //                         UNZIP-COD-VARIABILE(IND-UNZIP) =
        //                         SPACES OR LOW-VALUE OR HIGH-VALUE
        //                   SET UNZIP-TROVATO-SI    TO TRUE
        //           END-PERFORM.
        ws.getIxIndici().setUnzip(((short)1));
        while (!(ws.getIxIndici().getUnzip() > ws.getLimiteVariabiliUnzipped() || ws.getIxIndici().getUnzip() > ws.getUnzipStructure().getUnzipEleVariabiliMax() || Characters.EQ_SPACE.test(ws.getUnzipStructure().getUnzipTab().getCodVariabile(ws.getIxIndici().getUnzip())) || Characters.EQ_LOW.test(ws.getUnzipStructure().getUnzipTab().getCodVariabile(ws.getIxIndici().getUnzip()), UnzipTab.Len.COD_VARIABILE) || Characters.EQ_HIGH.test(ws.getUnzipStructure().getUnzipTab().getCodVariabile(ws.getIxIndici().getUnzip()), UnzipTab.Len.COD_VARIABILE))) {
            // COB_CODE: MOVE UNZIP-COD-VARIABILE(IND-UNZIP)
            //                                   (1:UNZIP-LENGTH-FIELD)
            //             TO COMODO-TP-CAUS(IND-UNZIP)
            ws.getComodoStatCaus().getTpCausEle(ws.getIxIndici().getUnzip()).setComodoTpCaus(ws.getUnzipStructure().getUnzipTab().getCodVariabileFormatted(ws.getIxIndici().getUnzip()).substring((1) - 1, ws.getUnzipStructure().getUnzipLengthField()));
            // COB_CODE: SET UNZIP-TROVATO-SI    TO TRUE
            ws.getFlagUnzipTrovato().setSi();
            ws.getIxIndici().setUnzip(Trunc.toShort(ws.getIxIndici().getUnzip() + 1, 4));
        }
        //--->  STATO NON TROVATO
        // COB_CODE: IF UNZIP-TROVATO-NO
        //              MOVE 'MATR_VAL_VAR'        TO IVVC0211-NOME-TABELLA
        //           END-IF.
        if (ws.getFlagUnzipTrovato().isNo()) {
            // COB_CODE: SET FINE-ELAB-SI           TO TRUE
            ws.getFlagFineElab().setSi();
            // COB_CODE: SET IVVC0211-GENERIC-ERROR TO TRUE
            inputIvvs0211.getIvvc0211RestoDati().getReturnCode().setGenericError();
            // COB_CODE: MOVE WK-PGM                TO IVVC0211-COD-SERVIZIO-BE
            inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: STRING 'CAUSALE NON TROVATO SU MATR-VAL-VAR'
            //                  ' - '
            //                  'COMPAGNIA : '
            //                  WK-CODICE-COMPAGNIA
            //                  ' - '
            //                  'TIPO MOVIM. : '
            //                  WK-TIPO-MOVIMENTO
            //                  ' - '
            //                  'STEP VALOR. : '
            //                  MVV-STEP-VALORIZZATORE
            //                  ' - '
            //                  'STEP CONVER. : '
            //                  MVV-STEP-CONVERSAZIONE
            //                  DELIMITED BY SIZE INTO
            //                  IVVC0211-DESCRIZ-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ivvc0211CampiEsito.Len.DESCRIZ_ERR, new String[] {"CAUSALE NON TROVATO SU MATR-VAL-VAR", " - ", "COMPAGNIA : ", ws.getWkCodiceCompagniaAsString(), " - ", "TIPO MOVIM. : ", ws.getWkTipoMovimentoAsString(), " - ", "STEP VALOR. : ", String.valueOf(ws.getMatrValVar().getMvvStepValorizzatore()), " - ", "STEP CONVER. : ", String.valueOf(ws.getMatrValVar().getMvvStepConversazione())});
            inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setDescrizErr(concatUtil.replaceInString(inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().getDescrizErrFormatted()));
            // COB_CODE: MOVE 'MATR_VAL_VAR'        TO IVVC0211-NOME-TABELLA
            inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setNomeTabella("MATR_VAL_VAR");
        }
    }

    /**Original name: G000-GESTIONE-OPZIONI<br>
	 * <pre>----------------------------------------------------------------*
	 *     GESTIONE OPZIONI
	 * ----------------------------------------------------------------*</pre>*/
    private void g000GestioneOpzioni() {
        // COB_CODE: MOVE 1                       TO IND-TIPO-OPZ
        ws.getIxIndici().setTipoOpz(((short)1));
        // COB_CODE: PERFORM VARYING IND-TIPO-OPZ FROM 1 BY 1
        //                   UNTIL IND-TIPO-OPZ > LIMITE-TIPO-OPZ      OR
        //                         IND-TIPO-OPZ > WOPZ-ELE-MAX-OPZIONI OR
        //                         WOPZ-COD-TIPO-OPZIONE(IND-TIPO-OPZ) =
        //                         SPACES OR LOW-VALUE OR HIGH-VALUE   OR
        //                         NOT IVVC0211-SUCCESSFUL-RC
        //              END-IF
        //           END-PERFORM.
        ws.getIxIndici().setTipoOpz(((short)1));
        while (!(ws.getIxIndici().getTipoOpz() > ws.getLimiteTipoOpz() || ws.getIxIndici().getTipoOpz() > ws.getAreaBusiness().getIvvc0217().getEleMaxOpzioni() || Characters.EQ_SPACE.test(ws.getAreaBusiness().getIvvc0217().getOpzioni(ws.getIxIndici().getTipoOpz()).getCodTipoOpzione()) || Characters.EQ_LOW.test(ws.getAreaBusiness().getIvvc0217().getOpzioni(ws.getIxIndici().getTipoOpz()).getWopzCodTipoOpzioneFormatted()) || Characters.EQ_HIGH.test(ws.getAreaBusiness().getIvvc0217().getOpzioni(ws.getIxIndici().getTipoOpz()).getWopzCodTipoOpzioneFormatted()) || !inputIvvs0211.getIvvc0211RestoDati().getReturnCode().isSuccessfulRc())) {
            // COB_CODE: PERFORM G100-GESTIONE-OPZIONI-PR       THRU G100-EX
            g100GestioneOpzioniPr();
            // COB_CODE: IF IVVC0211-SUCCESSFUL-RC
            //              PERFORM G200-GESTIONE-OPZIONI-GA    THRU G200-EX
            //           END-IF
            if (inputIvvs0211.getIvvc0211RestoDati().getReturnCode().isSuccessfulRc()) {
                // COB_CODE: PERFORM G200-GESTIONE-OPZIONI-GA    THRU G200-EX
                g200GestioneOpzioniGa();
            }
            ws.getIxIndici().setTipoOpz(Trunc.toShort(ws.getIxIndici().getTipoOpz() + 1, 4));
        }
    }

    /**Original name: G100-GESTIONE-OPZIONI-PR<br>
	 * <pre>----------------------------------------------------------------*
	 *     GESTIONE OPZIONI
	 * ----------------------------------------------------------------*</pre>*/
    private void g100GestioneOpzioniPr() {
        // COB_CODE: SET GESTIONE-PRODOTTO         TO TRUE
        ws.getFlagGestione().setProdotto();
        // COB_CODE: PERFORM M000-GESTIONE-VAR-FUN-CALC  THRU M000-EX.
        m000GestioneVarFunCalc();
    }

    /**Original name: G200-GESTIONE-OPZIONI-GA<br>
	 * <pre>----------------------------------------------------------------*
	 *     GESTIONE OPZIONI
	 * ----------------------------------------------------------------*</pre>*/
    private void g200GestioneOpzioniGa() {
        // COB_CODE: SET GESTIONE-GARANZIE         TO TRUE
        ws.getFlagGestione().setGaranzie();
        // COB_CODE: MOVE 1                        TO IND-OPZ
        ws.getIxIndici().setOpz(((short)1));
        // COB_CODE: PERFORM VARYING IND-OPZ FROM 1 BY 1
        //             UNTIL IND-OPZ > WOPZ-NUM-GAR-OPZ-MAX-ELE(IND-TIPO-OPZ) OR
        //                   IND-OPZ > LIMITE-OPZ                             OR
        //                   WOPZ-GAR-OPZ-CODICE(IND-TIPO-OPZ, IND-OPZ) =
        //                     SPACES OR HIGH-VALUE OR LOW-VALUE              OR
        //                     FINE-ELAB-SI
        //                     PERFORM G500-ELABORA-OPZIONE  THRU G500-EX
        //           END-PERFORM.
        ws.getIxIndici().setOpz(((short)1));
        while (!(ws.getIxIndici().getOpz() > ws.getAreaBusiness().getIvvc0217().getOpzioni(ws.getIxIndici().getTipoOpz()).getWopzNumGarOpzMaxEle() || ws.getIxIndici().getOpz() > ws.getLimiteOpz() || Characters.EQ_SPACE.test(ws.getAreaBusiness().getIvvc0217().getOpzioni(ws.getIxIndici().getTipoOpz()).getGarOpzione(ws.getIxIndici().getOpz()).getCodice()) || Characters.EQ_HIGH.test(ws.getAreaBusiness().getIvvc0217().getOpzioni(ws.getIxIndici().getTipoOpz()).getGarOpzione(ws.getIxIndici().getOpz()).getWopzGarOpzCodiceFormatted()) || Characters.EQ_LOW.test(ws.getAreaBusiness().getIvvc0217().getOpzioni(ws.getIxIndici().getTipoOpz()).getGarOpzione(ws.getIxIndici().getOpz()).getWopzGarOpzCodiceFormatted()) || ws.getFlagFineElab().isSi())) {
            // COB_CODE: PERFORM G500-ELABORA-OPZIONE  THRU G500-EX
            g500ElaboraOpzione();
            ws.getIxIndici().setOpz(Trunc.toShort(ws.getIxIndici().getOpz() + 1, 4));
        }
    }

    /**Original name: G500-ELABORA-OPZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     GESTIONE OPZIONI
	 * ----------------------------------------------------------------*</pre>*/
    private void g500ElaboraOpzione() {
        ConcatUtil concatUtil = null;
        // COB_CODE: IF WOPZ-GAR-OPZ-VERSIONE(IND-TIPO-OPZ, IND-OPZ) NOT =
        //              LOW-VALUE OR HIGH-VALUE
        //              PERFORM M000-GESTIONE-VAR-FUN-CALC  THRU M000-EX
        //           ELSE
        //              END-STRING
        //           END-IF.
        if (!Conditions.eq(ws.getAreaBusiness().getIvvc0217().getOpzioni(ws.getIxIndici().getTipoOpz()).getGarOpzione(ws.getIxIndici().getOpz()).getVersione(), Types.LOW_CHAR_VAL) || !Conditions.eq(ws.getAreaBusiness().getIvvc0217().getOpzioni(ws.getIxIndici().getTipoOpz()).getGarOpzione(ws.getIxIndici().getOpz()).getVersione(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE WOPZ-GAR-OPZ-VERSIONE(IND-TIPO-OPZ, IND-OPZ)
            //                                               TO FLAG-VERSIONE
            ws.getFlagVersione().setFlagVersione(ws.getAreaBusiness().getIvvc0217().getOpzioni(ws.getIxIndici().getTipoOpz()).getGarOpzione(ws.getIxIndici().getOpz()).getVersione());
            // COB_CODE: PERFORM M000-GESTIONE-VAR-FUN-CALC  THRU M000-EX
            m000GestioneVarFunCalc();
        }
        else {
            // COB_CODE: SET IVVC0211-VERS-OPZ-NOT-VALID     TO TRUE
            inputIvvs0211.getIvvc0211RestoDati().getReturnCode().setVersOpzNotValid();
            // COB_CODE: MOVE WK-PGM            TO IVVC0211-COD-SERVIZIO-BE
            inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: STRING 'FLAG VERS. OPZ. NON VALIDO'
            //                 DELIMITED BY SIZE INTO
            //                 IVVC0211-DESCRIZ-ERR
            //           END-STRING
            inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setDescrizErr("FLAG VERS. OPZ. NON VALIDO");
        }
    }

    /**Original name: I000-VALORIZZA-IDSV0003<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZA CAMPI DI IDSV0003
	 * ----------------------------------------------------------------*</pre>*/
    private void i000ValorizzaIdsv0003() {
        // COB_CODE: MOVE IVVC0211-MODALITA-ESECUTIVA
        //                                 TO IDSV0003-MODALITA-ESECUTIVA
        ws.getIdsv0003().getModalitaEsecutiva().setModalitaEsecutiva(inputIvvs0211.getIvvc0211DatiInput().getModalitaEsecutiva().getModalitaEsecutiva());
        // COB_CODE: MOVE IVVC0211-COD-COMPAGNIA-ANIA
        //                                 TO IDSV0003-CODICE-COMPAGNIA-ANIA
        //                                    WK-CODICE-COMPAGNIA
        ws.getIdsv0003().setCodiceCompagniaAnia(inputIvvs0211.getIvvc0211DatiInput().getCodCompagniaAnia());
        ws.setWkCodiceCompagniaFormatted(inputIvvs0211.getIvvc0211DatiInput().getCodCompagniaAniaFormatted());
        // COB_CODE: MOVE IVVC0211-TIPO-MOVIMENTO
        //                                 TO IDSV0003-TIPO-MOVIMENTO
        //                                    WK-TIPO-MOVIMENTO
        ws.getIdsv0003().setTipoMovimentoFormatted(inputIvvs0211.getIvvc0211DatiInput().getTipoMovimentoFormatted());
        ws.setWkTipoMovimentoFormatted(inputIvvs0211.getIvvc0211DatiInput().getTipoMovimentoFormatted());
        // COB_CODE: IF IVVC0211-DATA-ULT-TIT-INC GREATER ZERO
        //                                 TO IDSV0003-DATA-INIZIO-EFFETTO
        //           ELSE
        //                                 TO IDSV0003-DATA-INIZIO-EFFETTO
        //           END-IF
        if (inputIvvs0211.getIvvc0211DatiInput().getDataUltTitInc() > 0) {
            // COB_CODE: MOVE IVVC0211-DATA-ULT-TIT-INC
            //                              TO IDSV0003-DATA-INIZIO-EFFETTO
            ws.getIdsv0003().setDataInizioEffetto(inputIvvs0211.getIvvc0211DatiInput().getDataUltTitInc());
        }
        else {
            // COB_CODE: MOVE IVVC0211-DATA-EFFETTO
            //                              TO IDSV0003-DATA-INIZIO-EFFETTO
            ws.getIdsv0003().setDataInizioEffetto(inputIvvs0211.getIvvc0211DatiInput().getDataEffetto());
        }
        // COB_CODE: MOVE IVVC0211-DATA-COMPETENZA
        //                                 TO IDSV0003-DATA-COMPETENZA
        ws.getIdsv0003().setDataCompetenza(inputIvvs0211.getIvvc0211DatiInput().getDataCompetenza());
        // COB_CODE: MOVE IVVC0211-DATA-COMP-AGG-STOR
        //                                 TO IDSV0003-DATA-COMP-AGG-STOR
        ws.getIdsv0003().setDataCompAggStor(inputIvvs0211.getIvvc0211DatiInput().getDataCompAggStor());
        // COB_CODE: MOVE IVVC0211-TRATTAMENTO-STORICITA
        //                                 TO IDSV0003-TRATTAMENTO-STORICITA
        ws.getIdsv0003().getTrattamentoStoricita().setTrattamentoStoricita(inputIvvs0211.getIvvc0211DatiInput().getTrattamentoStoricita());
        // COB_CODE: MOVE IVVC0211-FORMATO-DATA-DB
        //                                 TO IDSV0003-FORMATO-DATA-DB.
        ws.getIdsv0003().getFormatoDataDb().setFormatoDataDb(inputIvvs0211.getIvvc0211DatiInput().getFormatoDataDb().getFormatoDataDb());
    }

    /**Original name: L000-GESTIONE-LIVELLI<br>
	 * <pre>----------------------------------------------------------------*
	 *     GESTIONE CREAZIONE LIVELLI
	 * ----------------------------------------------------------------*</pre>*/
    private void l000GestioneLivelli() {
        // COB_CODE: SET FLAG-TRATTATO-VALIDO-SI  TO TRUE
        ws.getFlagTrattatoValido().setSi();
        // COB_CODE: SET RIASS-NO                         TO TRUE
        ws.getFlagRiass().setNo();
        // COB_CODE: IF  IVVC0211-CODICE-TRATTATO > SPACES
        //           AND IVVC0211-CODICE-TRATTATO NOT = HIGH-VALUE
        //               SET RIASS-SI                     TO TRUE
        //           END-IF.
        if (Characters.GT_SPACE.test(inputIvvs0211.getIvvc0211RestoDati().getCodiceTrattato()) && !Characters.EQ_HIGH.test(inputIvvs0211.getIvvc0211RestoDati().getCodiceTrattatoFormatted())) {
            // COB_CODE: SET RIASS-SI                     TO TRUE
            ws.getFlagRiass().setSi();
        }
        // COB_CODE: SET  WK-GLOVARLIST-VUOTA  TO TRUE.
        ws.getWkGlovarlist().setVuota();
        // COB_CODE: MOVE WPOL-FL-VER-PROD   TO FLAG-VERSIONE
        ws.getFlagVersione().setFlagVersioneFormatted(ws.getAreaBusiness().getLccvpol1().getDati().getWpolFlVerProdFormatted());
        // COB_CODE: PERFORM L200-CREAZIONE-PRODOTTO THRU L200-EX
        l200CreazioneProdotto();
        // COB_CODE: IF IVVC0211-SUCCESSFUL-RC
        //              PERFORM L100-CREAZIONE-GARANZIA THRU L100-EX
        //           END-IF.
        if (inputIvvs0211.getIvvc0211RestoDati().getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM L100-CREAZIONE-GARANZIA THRU L100-EX
            l100CreazioneGaranzia();
        }
        // COB_CODE: IF  RIASS-SI
        //           AND FLAG-TRATTATO-VALIDO-NO
        //               SET IVVC0211-TRATTATO-NOT-VALID TO TRUE
        //           END-IF.
        if (ws.getFlagRiass().isSi() && ws.getFlagTrattatoValido().isNo()) {
            // COB_CODE: SET IVVC0211-TRATTATO-NOT-VALID TO TRUE
            inputIvvs0211.getIvvc0211RestoDati().getReturnCode().setTrattatoNotValid();
        }
    }

    /**Original name: L100-CREAZIONE-GARANZIA<br>
	 * <pre>----------------------------------------------------------------*
	 *     GESTIONE CREAZIONE LIVELLO GARANZIA
	 * ----------------------------------------------------------------*</pre>*/
    private void l100CreazioneGaranzia() {
        // COB_CODE: IF SKEDE-GARANZIE OR SKEDE-TOTALI
        //              END-IF
        //           END-IF.
        if (ws.getFlagSkede().isGaranzie() || ws.getFlagSkede().isTotali()) {
            // COB_CODE: SET GARANZIA-INIT                     TO TRUE
            ws.getFlagTipoGaranzia().setInit();
            // COB_CODE: PERFORM L300-GEST-GARANZIE-CONT       THRU L300-EX
            l300GestGaranzieCont();
            // COB_CODE: IF NOT GARANZIA-CONTESTO AND FINE-ELAB-NO
            //              END-IF
            //           END-IF
            if (!ws.getFlagTipoGaranzia().isContesto() && ws.getFlagFineElab().isNo()) {
                // COB_CODE: PERFORM N000-CERCA-ADESIONE         THRU N000-EX
                n000CercaAdesione();
                // COB_CODE: IF IVVC0211-SUCCESSFUL-RC
                //              PERFORM L440-LETTURA-GARANZIA-DB THRU L440-EX
                //           END-IF
                if (inputIvvs0211.getIvvc0211RestoDati().getReturnCode().isSuccessfulRc()) {
                    // COB_CODE: PERFORM L440-LETTURA-GARANZIA-DB THRU L440-EX
                    l440LetturaGaranziaDb();
                }
            }
        }
    }

    /**Original name: L200-CREAZIONE-PRODOTTO<br>
	 * <pre>----------------------------------------------------------------*
	 *     GESTIONE CREAZIONE LIVELL PRODOTTO
	 * ----------------------------------------------------------------*</pre>*/
    private void l200CreazioneProdotto() {
        // COB_CODE: IF SKEDE-PRODOTTI OR SKEDE-TOTALI
        //              END-IF
        //           END-IF.
        if (ws.getFlagSkede().isProdotti() || ws.getFlagSkede().isTotali()) {
            // COB_CODE: SET GESTIONE-PRODOTTO                 TO TRUE
            ws.getFlagGestione().setProdotto();
            // COB_CODE: IF EMISSIONE OR IVVC0211-AREA-VE
            //              PERFORM M000-GESTIONE-VAR-FUN-CALC THRU M000-EX
            //           ELSE
            //              PERFORM L330-GESTIONE-PRODOTTO     THRU L330-EX
            //           END-IF
            if (ws.getFlagVersione().isEmissione() || inputIvvs0211.getIvvc0211DatiInput().getFlgArea().isVe()) {
                // COB_CODE: PERFORM M000-GESTIONE-VAR-FUN-CALC THRU M000-EX
                m000GestioneVarFunCalc();
            }
            else {
                // COB_CODE: PERFORM L330-GESTIONE-PRODOTTO     THRU L330-EX
                l330GestioneProdotto();
            }
        }
    }

    /**Original name: L300-GEST-GARANZIE-CONT<br>
	 * <pre>----------------------------------------------------------------*
	 *     GESTIONE GARANZIE DI CONTESTO
	 * ----------------------------------------------------------------*</pre>*/
    private void l300GestGaranzieCont() {
        // COB_CODE: SET GESTIONE-GARANZIE          TO TRUE
        ws.getFlagGestione().setGaranzie();
        // COB_CODE: MOVE 1                         TO IND-GRZ
        ws.getIxIndici().setGrz(((short)1));
        // COB_CODE: PERFORM VARYING IND-GRZ FROM 1 BY 1
        //             UNTIL IND-GRZ > WGRZ-ELE-GARANZIA-MAX  OR
        //                   IND-GRZ > WK-GRZ-MAX-B             OR
        //                   FINE-ELAB-SI
        //                   END-IF
        //           END-PERFORM.
        ws.getIxIndici().setGrz(((short)1));
        while (!(ws.getIxIndici().getGrz() > ws.getAreaBusiness().getWgrzEleGaranziaMax() || ws.getIxIndici().getGrz() > ws.getLccvgrzz().getMaxB() || ws.getFlagFineElab().isSi())) {
            // COB_CODE: IF  RIASS-SI
            //               END-IF
            //           END-IF
            if (ws.getFlagRiass().isSi()) {
                // COB_CODE: IF  WGRZ-COD-TRAT-RIASS(IND-GRZ)     > SPACES
                //           AND WGRZ-COD-TRAT-RIASS(IND-GRZ) NOT = HIGH-VALUE
                //               END-IF
                //           ELSE
                //               SET FINE-ELAB-SI             TO TRUE
                //           END-IF
                if (Characters.GT_SPACE.test(ws.getAreaBusiness().getWgrzTabGar(ws.getIxIndici().getGrz()).getLccvgrz1().getDati().getWgrzCodTratRiass()) && !Characters.EQ_HIGH.test(ws.getAreaBusiness().getWgrzTabGar(ws.getIxIndici().getGrz()).getLccvgrz1().getDati().getWgrzCodTratRiassFormatted())) {
                    // COB_CODE: IF WGRZ-COD-TRAT-RIASS(IND-GRZ) NOT =
                    //              IVVC0211-CODICE-TRATTATO
                    //              SET FINE-ELAB-SI             TO TRUE
                    //           END-IF
                    if (!Conditions.eq(ws.getAreaBusiness().getWgrzTabGar(ws.getIxIndici().getGrz()).getLccvgrz1().getDati().getWgrzCodTratRiass(), inputIvvs0211.getIvvc0211RestoDati().getCodiceTrattato())) {
                        // COB_CODE: SET FLAG-TRATTATO-VALIDO-NO  TO TRUE
                        ws.getFlagTrattatoValido().setNo();
                        // COB_CODE: SET FINE-ELAB-SI             TO TRUE
                        ws.getFlagFineElab().setSi();
                    }
                }
                else {
                    // COB_CODE: SET FLAG-TRATTATO-VALIDO-NO  TO TRUE
                    ws.getFlagTrattatoValido().setNo();
                    // COB_CODE: SET FINE-ELAB-SI             TO TRUE
                    ws.getFlagFineElab().setSi();
                }
            }
            // COB_CODE: IF FINE-ELAB-NO
            //              PERFORM L310-ELABORA-GARANZIA  THRU L310-EX
            //           END-IF
            if (ws.getFlagFineElab().isNo()) {
                // COB_CODE: SET GARANZIA-CONTESTO          TO TRUE
                ws.getFlagTipoGaranzia().setContesto();
                // COB_CODE: PERFORM L310-ELABORA-GARANZIA  THRU L310-EX
                l310ElaboraGaranzia();
            }
            ws.getIxIndici().setGrz(Trunc.toShort(ws.getIxIndici().getGrz() + 1, 4));
        }
    }

    /**Original name: L310-ELABORA-GARANZIA<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORA GARANZIE
	 * ----------------------------------------------------------------*</pre>*/
    private void l310ElaboraGaranzia() {
        // COB_CODE: PERFORM L400-CERCA-TRANCHE         THRU L400-EX.
        l400CercaTranche();
    }

    /**Original name: L330-GESTIONE-PRODOTTO<br>
	 * <pre>----------------------------------------------------------------*
	 *     GESTIONE PRODOTTO
	 * ----------------------------------------------------------------*</pre>*/
    private void l330GestioneProdotto() {
        // COB_CODE: SET GESTIONE-PRODOTTO          TO TRUE
        ws.getFlagGestione().setProdotto();
        //    IF WADE-ID-ADES IS NUMERIC
        //       IF WADE-ID-ADES NOT = ZEROES
        // COB_CODE: PERFORM L400-CERCA-TRANCHE     THRU L400-EX.
        l400CercaTranche();
    }

    /**Original name: L350-CARICA-LIVELLI<br>
	 * <pre>----------------------------------------------------------------*
	 *     CARICAMENTO DEI LIVELLI PRODOTTI NELLE OCCORRENZE
	 *     DELL'AREA VARIABILI
	 * ----------------------------------------------------------------*</pre>*/
    private void l350CaricaLivelli() {
        // COB_CODE: IF GESTIONE-PRODOTTO
        //              PERFORM L360-CARICA-LIVELLI-PROD THRU L360-EX
        //           ELSE
        //              PERFORM L370-CARICA-LIVELLI-GAR  THRU L370-EX
        //           END-IF.
        if (ws.getFlagGestione().isProdotto()) {
            // COB_CODE: PERFORM L360-CARICA-LIVELLI-PROD THRU L360-EX
            l360CaricaLivelliProd();
        }
        else {
            // COB_CODE: PERFORM L370-CARICA-LIVELLI-GAR  THRU L370-EX
            l370CaricaLivelliGar();
        }
    }

    /**Original name: L355-CARICA-LIVELLI-RIASS<br>
	 * <pre>----------------------------------------------------------------*
	 *     CARICAMENTO DEI LIVELLI PRODOTTI NELLE OCCORRENZE
	 *     DELL'AREA VARIABILI
	 * ----------------------------------------------------------------*</pre>*/
    private void l355CaricaLivelliRiass() {
        // COB_CODE: IF GESTIONE-PRODOTTO
        //              CONTINUE
        //           ELSE
        //              PERFORM L370-CARICA-LIVELLI-GAR  THRU L370-EX
        //           END-IF.
        if (ws.getFlagGestione().isProdotto()) {
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: SUBTRACT   1          FROM IND-LIVELLO
            ws.getIxIndici().setLivello(Trunc.toShort(ws.getIxIndici().getLivello() - 1, 4));
            // COB_CODE: PERFORM L370-CARICA-LIVELLI-GAR  THRU L370-EX
            l370CaricaLivelliGar();
        }
    }

    /**Original name: L360-CARICA-LIVELLI-PROD<br>
	 * <pre>----------------------------------------------------------------*
	 *     CARICAMENTO DEI LIVELLI PRODOTTI NELLE OCCORRENZE
	 *     DELL'AREA VARIABILI
	 * ----------------------------------------------------------------*
	 *     ADD 1                        TO IND-LIVELLO.</pre>*/
    private void l360CaricaLivelliProd() {
        // COB_CODE: MOVE WPOL-ID-POLI
        //             TO IVVC0216-ID-POL-P         (IND-LIVELLO).
        ws.getAreaWorkIvvc0216().getC216TabValP().setIvvc0216IdPolP(ws.getIxIndici().getLivello(), TruncAbs.toInt(ws.getAreaBusiness().getLccvpol1().getDati().getWpolIdPoli(), 9));
        // COB_CODE: IF TIPO-OPZIONI
        //                   TO IVVC0216-COD-TIPO-OPZIONE-P (IND-LIVELLO)
        //           END-IF
        if (ws.getFlagTipologiaScheda().isOpzioni()) {
            // COB_CODE: MOVE WOPZ-COD-TIPO-OPZIONE     (IND-TIPO-OPZ)
            //                TO IVVC0216-COD-TIPO-OPZIONE-P (IND-LIVELLO)
            ws.getAreaWorkIvvc0216().getC216TabValP().setIvvc0216CodTipoOpzioneP(ws.getIxIndici().getLivello(), ws.getAreaBusiness().getIvvc0217().getOpzioni(ws.getIxIndici().getTipoOpz()).getCodTipoOpzione());
        }
        // COB_CODE: PERFORM L390-VALORIZZA-TP-LIVELLO THRU L390-EX.
        l390ValorizzaTpLivello();
        // COB_CODE: MOVE WPOL-COD-PROD
        //             TO IVVC0216-COD-LIVELLO-P    (IND-LIVELLO)
        ws.getAreaWorkIvvc0216().getC216TabValP().setIvvc0216CodLivelloP(ws.getIxIndici().getLivello(), ws.getAreaBusiness().getLccvpol1().getDati().getWpolCodProd());
        // COB_CODE: MOVE WPOL-DT-INI-VLDT-PROD
        //             TO IVVC0216-DT-INIZ-PROD-P   (IND-LIVELLO)
        ws.getAreaWorkIvvc0216().getC216TabValP().setIvvc0216DtInizProdP(ws.getIxIndici().getLivello(), ws.getAreaBusiness().getLccvpol1().getDati().getWpolDtIniVldtProdFormatted());
        // COB_CODE: MOVE WPOL-TP-RGM-FISC
        //             TO IVVC0216-COD-RGM-FISC-P   (IND-LIVELLO)
        ws.getAreaWorkIvvc0216().getC216TabValP().setIvvc0216CodRgmFiscP(ws.getIxIndici().getLivello(), ws.getAreaBusiness().getLccvpol1().getDati().getWpolTpRgmFisc());
        // COB_CODE: MOVE ZEROES
        //             TO IVVC0216-ID-LIVELLO-P     (IND-LIVELLO).
        ws.getAreaWorkIvvc0216().getC216TabValP().setIvvc0216IdLivelloP(ws.getIxIndici().getLivello(), 0);
    }

    /**Original name: L370-CARICA-LIVELLI-GAR<br>
	 * <pre>----------------------------------------------------------------*
	 *     CARICAMENTO DEI LIVELLI GARANZIA NELLE OCCORRENZE
	 *     DELL'AREA VARIABILI
	 * ----------------------------------------------------------------*</pre>*/
    private void l370CaricaLivelliGar() {
        // COB_CODE:      IF TIPO-OPZIONI
        //                   PERFORM L380-CARICA-LIVELLI-OPZ-GAR THRU L380-EX
        //                ELSE
        //           *       ADD 1                               TO IND-LIVELLO
        //                   END-EVALUATE
        //                END-IF.
        if (ws.getFlagTipologiaScheda().isOpzioni()) {
            // COB_CODE: PERFORM L380-CARICA-LIVELLI-OPZ-GAR THRU L380-EX
            l380CaricaLivelliOpzGar();
        }
        else {
            //       ADD 1                               TO IND-LIVELLO
            // COB_CODE: MOVE ZEROES
            //                 TO IVVC0216-ID-LIVELLO-T     (IND-LIVELLO)
            ws.getAreaWorkIvvc0216().getC216TabValT().setIvvc0216IdLivelloT(ws.getIxIndici().getLivello(), 0);
            // COB_CODE: EVALUATE TRUE
            //              WHEN GARANZIA-CONTESTO
            //                    TO IVVC0216-COD-LIVELLO-T    (IND-LIVELLO)
            //              WHEN GARANZIA-DB
            //                    TO IVVC0216-COD-LIVELLO-T    (IND-LIVELLO)
            //           END-EVALUATE
            switch (ws.getFlagTipoGaranzia().getFlagTipoGaranzia()) {

                case FlagTipoGaranzia.CONTESTO:// COB_CODE: MOVE WGRZ-ID-GAR               (IND-GRZ)
                    //             TO IVVC0216-ID-GAR-T         (IND-LIVELLO)
                    ws.getAreaWorkIvvc0216().getC216TabValT().setIvvc0216IdGarT(ws.getIxIndici().getLivello(), TruncAbs.toInt(ws.getAreaBusiness().getWgrzTabGar(ws.getIxIndici().getGrz()).getLccvgrz1().getDati().getWgrzIdGar(), 9));
                    // COB_CODE: MOVE WGRZ-COD-TARI             (IND-GRZ)
                    //             TO IVVC0216-COD-LIVELLO-T    (IND-LIVELLO)
                    ws.getAreaWorkIvvc0216().getC216TabValT().setIvvc0216CodLivelloT(ws.getIxIndici().getLivello(), ws.getAreaBusiness().getWgrzTabGar(ws.getIxIndici().getGrz()).getLccvgrz1().getDati().getWgrzCodTari());
                    break;

                case FlagTipoGaranzia.DB:// COB_CODE: MOVE GRZ-ID-GAR
                    //             TO IVVC0216-ID-GAR-T         (IND-LIVELLO)
                    ws.getAreaWorkIvvc0216().getC216TabValT().setIvvc0216IdGarT(ws.getIxIndici().getLivello(), TruncAbs.toInt(ws.getGar().getGrzIdGar(), 9));
                    // COB_CODE: MOVE GRZ-COD-TARI
                    //             TO IVVC0216-COD-LIVELLO-T    (IND-LIVELLO)
                    ws.getAreaWorkIvvc0216().getC216TabValT().setIvvc0216CodLivelloT(ws.getIxIndici().getLivello(), ws.getGar().getGrzCodTari());
                    break;

                default:break;
            }
            // COB_CODE: EVALUATE TRUE
            //              WHEN TRANCHE-CONTESTO
            //                  END-IF
            //              WHEN TRANCHE-DB
            //                  END-IF
            //           END-EVALUATE
            switch (ws.getFlagTipoTranche().getFlagTipoTranche()) {

                case FlagTipoTranche.CONTESTO:// COB_CODE: MOVE WTGA-ID-TRCH-DI-GAR       (IND-TGA)
                    //             TO IVVC0216-ID-LIVELLO-T     (IND-LIVELLO)
                    ws.getAreaWorkIvvc0216().getC216TabValT().setIvvc0216IdLivelloT(ws.getIxIndici().getLivello(), TruncAbs.toInt(ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().getIdTrchDiGar(ws.getIxIndici().getTga()), 9));
                    // COB_CODE: MOVE WTGA-DT-INI-VAL-TAR       (IND-TGA)
                    //             TO IVVC0216-DT-INIZ-TARI-T   (IND-LIVELLO)
                    ws.getAreaWorkIvvc0216().getC216TabValT().setIvvc0216DtInizTariT(ws.getIxIndici().getLivello(), ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().getDtIniValTarFormatted(ws.getIxIndici().getTga()));
                    // COB_CODE: MOVE WTGA-TP-RGM-FISC          (IND-TGA)
                    //             TO IVVC0216-COD-RGM-FISC-T   (IND-LIVELLO)
                    ws.getAreaWorkIvvc0216().getC216TabValT().setIvvc0216CodRgmFiscT(ws.getIxIndici().getLivello(), ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().getTpRgmFisc(ws.getIxIndici().getTga()));
                    // COB_CODE: MOVE WTGA-DT-DECOR             (IND-TGA)
                    //             TO IVVC0216-DT-DECOR-TRCH-T
                    //                                          (IND-LIVELLO)
                    ws.getAreaWorkIvvc0216().getC216TabValT().setIvvc0216DtDecorTrchT(ws.getIxIndici().getLivello(), ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().getDtDecorFormatted(ws.getIxIndici().getTga()));
                    // VERIFICHIAMO SE IL CAMPO SIA VALORIZZATO IN QUANTO IN FASE DI
                    // CREAZIONE NUOVA TRANCHE ESEMPIO EMISSIONE NON ABBIAMO ANCORA
                    // L'INFORMAZIONE
                    // COB_CODE: IF WTGA-TP-TRCH(IND-TGA) NOT =
                    //              HIGH-VALUE AND LOW-VALUE AND SPACES
                    //              END-EVALUATE
                    //           END-IF
                    if (!Characters.EQ_HIGH.test(ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().getTpTrchFormatted(ws.getIxIndici().getTga())) && !Characters.EQ_LOW.test(ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().getTpTrchFormatted(ws.getIxIndici().getTga())) && !Characters.EQ_SPACE.test(ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().getTpTrch(ws.getIxIndici().getTga()))) {
                        // COB_CODE: COMPUTE IVVC0216-TIPO-TRCH(IND-LIVELLO) =
                        //                   FUNCTION NUMVAL(WTGA-TP-TRCH(IND-TGA))
                        ws.getAreaWorkIvvc0216().getC216TabValT().setTipoTrch(ws.getIxIndici().getLivello(), MathUtil.convertRoundUpShort(MathFunctions.toNumber(ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().getTpTrchFormatted(ws.getIxIndici().getTga()))));
                        // COB_CODE: MOVE WTGA-TP-TRCH(IND-TGA)
                        //             TO WS-TP-TRCH
                        ws.getWsTpTrch().setWsTpTrch(ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().getTpTrch(ws.getIxIndici().getTga()));
                        // COB_CODE: EVALUATE TRUE
                        //               WHEN TP-TRCH-NEG-PRCOS
                        //               WHEN TP-TRCH-NEG-RIS-PAR
                        //               WHEN TP-TRCH-NEG-RIS-PRO
                        //               WHEN TP-TRCH-NEG-IMPST-SOST
                        //               WHEN TP-TRCH-NEG-DA-DIS
                        //               WHEN TP-TRCH-NEG-INV-DA-SWIT
                        //                     TO TRUE
                        //               WHEN OTHER
                        //                     TO TRUE
                        //           END-EVALUATE
                        switch (ws.getWsTpTrch().getWsTpTrch()) {

                            case WsTpTrch.NEG_PRCOS:
                            case WsTpTrch.NEG_RIS_PAR:
                            case WsTpTrch.NEG_RIS_PRO:
                            case WsTpTrch.NEG_IMPST_SOST:
                            case WsTpTrch.NEG_DA_DIS:
                            case WsTpTrch.NEG_INV_DA_SWIT:// COB_CODE: SET IVVC0216-FLG-ITN-NEG(IND-LIVELLO)
                                //            TO TRUE
                                ws.getAreaWorkIvvc0216().getC216TabValT().setIvvc0216FlgItnNeg(ws.getIxIndici().getLivello());
                                break;

                            default:// COB_CODE: SET IVVC0216-FLG-ITN-POS(IND-LIVELLO)
                                //            TO TRUE
                                ws.getAreaWorkIvvc0216().getC216TabValT().setIvvc0216FlgItnPos(ws.getIxIndici().getLivello());
                                break;
                        }
                    }
                    break;

                case FlagTipoTranche.DB:// COB_CODE: MOVE TGA-ID-TRCH-DI-GAR
                    //             TO IVVC0216-ID-LIVELLO-T     (IND-LIVELLO)
                    ws.getAreaWorkIvvc0216().getC216TabValT().setIvvc0216IdLivelloT(ws.getIxIndici().getLivello(), TruncAbs.toInt(ws.getTrchDiGar().getTgaIdTrchDiGar(), 9));
                    // COB_CODE: MOVE TGA-DT-INI-VAL-TAR TO IVVC0216-DT-INIZ-TARI-T
                    //                                          (IND-LIVELLO)
                    ws.getAreaWorkIvvc0216().getC216TabValT().setIvvc0216DtInizTariT(ws.getIxIndici().getLivello(), ws.getTrchDiGar().getTgaDtIniValTar().getTgaDtIniValTarFormatted());
                    // COB_CODE: MOVE TGA-TP-RGM-FISC    TO IVVC0216-COD-RGM-FISC-T
                    //                                          (IND-LIVELLO)
                    ws.getAreaWorkIvvc0216().getC216TabValT().setIvvc0216CodRgmFiscT(ws.getIxIndici().getLivello(), ws.getTrchDiGar().getTgaTpRgmFisc());
                    // COB_CODE: MOVE TGA-DT-DECOR       TO IVVC0216-DT-DECOR-TRCH-T
                    //                                          (IND-LIVELLO)
                    ws.getAreaWorkIvvc0216().getC216TabValT().setIvvc0216DtDecorTrchT(ws.getIxIndici().getLivello(), ws.getTrchDiGar().getTgaDtDecorFormatted());
                    // VERIFICHIAMO SE IL CAMPO SIA VALORIZZATO IN QUANTO IN FASE DI
                    // CREAZIONE NUOVA TRANCHE ESEMPIO EMISSIONE NON ABBIAMO ANCORA
                    // L'INFORMAZIONE
                    // COB_CODE: IF TGA-TP-TRCH NOT =
                    //              HIGH-VALUE AND LOW-VALUE AND SPACES
                    //              END-EVALUATE
                    //           END-IF
                    if (!Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaTpTrchFormatted()) && !Characters.EQ_LOW.test(ws.getTrchDiGar().getTgaTpTrchFormatted()) && !Characters.EQ_SPACE.test(ws.getTrchDiGar().getTgaTpTrch())) {
                        // COB_CODE: COMPUTE IVVC0216-TIPO-TRCH(IND-LIVELLO) =
                        //                   FUNCTION NUMVAL(TGA-TP-TRCH)
                        ws.getAreaWorkIvvc0216().getC216TabValT().setTipoTrch(ws.getIxIndici().getLivello(), MathUtil.convertRoundUpShort(MathFunctions.toNumber(ws.getTrchDiGar().getTgaTpTrchFormatted())));
                        // COB_CODE: MOVE TGA-TP-TRCH
                        //             TO WS-TP-TRCH
                        ws.getWsTpTrch().setWsTpTrch(ws.getTrchDiGar().getTgaTpTrch());
                        // COB_CODE: EVALUATE TRUE
                        //               WHEN TP-TRCH-NEG-PRCOS
                        //               WHEN TP-TRCH-NEG-RIS-PAR
                        //               WHEN TP-TRCH-NEG-RIS-PRO
                        //               WHEN TP-TRCH-NEG-IMPST-SOST
                        //               WHEN TP-TRCH-NEG-DA-DIS
                        //               WHEN TP-TRCH-NEG-INV-DA-SWIT
                        //                     TO TRUE
                        //               WHEN OTHER
                        //                     TO TRUE
                        //           END-EVALUATE
                        switch (ws.getWsTpTrch().getWsTpTrch()) {

                            case WsTpTrch.NEG_PRCOS:
                            case WsTpTrch.NEG_RIS_PAR:
                            case WsTpTrch.NEG_RIS_PRO:
                            case WsTpTrch.NEG_IMPST_SOST:
                            case WsTpTrch.NEG_DA_DIS:
                            case WsTpTrch.NEG_INV_DA_SWIT:// COB_CODE: SET IVVC0216-FLG-ITN-NEG(IND-LIVELLO)
                                //            TO TRUE
                                ws.getAreaWorkIvvc0216().getC216TabValT().setIvvc0216FlgItnNeg(ws.getIxIndici().getLivello());
                                break;

                            default:// COB_CODE: SET IVVC0216-FLG-ITN-POS(IND-LIVELLO)
                                //            TO TRUE
                                ws.getAreaWorkIvvc0216().getC216TabValT().setIvvc0216FlgItnPos(ws.getIxIndici().getLivello());
                                break;
                        }
                    }
                    break;

                default:break;
            }
        }
        // COB_CODE: PERFORM L390-VALORIZZA-TP-LIVELLO THRU L390-EX.
        l390ValorizzaTpLivello();
    }

    /**Original name: L380-CARICA-LIVELLI-OPZ-GAR<br>
	 * <pre>----------------------------------------------------------------*
	 *     CARICAMENTO DEI LIVELLI PRODOTTI NELLE OCCORRENZE
	 *     DELL'AREA VARIABILI
	 * ----------------------------------------------------------------*
	 *     ADD 1                        TO IND-LIVELLO</pre>*/
    private void l380CaricaLivelliOpzGar() {
        // COB_CODE: MOVE WOPZ-COD-TIPO-OPZIONE     (IND-TIPO-OPZ)
        //             TO IVVC0216-COD-TIPO-OPZIONE-T (IND-LIVELLO)
        ws.getAreaWorkIvvc0216().getC216TabValT().setIvvc0216CodTipoOpzioneT(ws.getIxIndici().getLivello(), ws.getAreaBusiness().getIvvc0217().getOpzioni(ws.getIxIndici().getTipoOpz()).getCodTipoOpzione());
        // COB_CODE: MOVE WOPZ-GAR-OPZ-CODICE       (IND-TIPO-OPZ, IND-OPZ)
        //             TO IVVC0216-COD-LIVELLO-T      (IND-LIVELLO)
        ws.getAreaWorkIvvc0216().getC216TabValT().setIvvc0216CodLivelloT(ws.getIxIndici().getLivello(), ws.getAreaBusiness().getIvvc0217().getOpzioni(ws.getIxIndici().getTipoOpz()).getGarOpzione(ws.getIxIndici().getOpz()).getCodice());
        // COB_CODE: MOVE 1                         TO IND-GOP
        ws.getIxIndici().setGop(((short)1));
        // COB_CODE: PERFORM VARYING IND-GOP FROM 1 BY 1
        //                   UNTIL IND-GOP > WK-GRZ-MAX-B             OR
        //                         IND-GOP > WTOP-ELE-TRAN-OPZ-MAX  OR
        //                         WGOP-ID-GAR(IND-GOP) NOT NUMERIC OR
        //                         WGOP-ID-GAR(IND-GOP) = ZEROES
        //                         END-IF
        //           END-PERFORM.
        ws.getIxIndici().setGop(((short)1));
        while (!(ws.getIxIndici().getGop() > ws.getLccvgrzz().getMaxB() || ws.getIxIndici().getGop() > ws.getAreaBusiness().getWtopEleTranOpzMax() || !Functions.isNumber(ws.getAreaBusiness().getWgopTabGar(ws.getIxIndici().getGop()).getLccvgrz1().getDati().getWgrzIdGar()) || ws.getAreaBusiness().getWgopTabGar(ws.getIxIndici().getGop()).getLccvgrz1().getDati().getWgrzIdGar() == 0)) {
            // COB_CODE: IF WOPZ-GAR-OPZ-CODICE(IND-TIPO-OPZ, IND-OPZ)
            //              =
            //              WGOP-COD-TARI(IND-GOP)
            //              PERFORM L381-RICERCA-TOP-X-GOP THRU L381-EX
            //           END-IF
            if (Conditions.eq(ws.getAreaBusiness().getIvvc0217().getOpzioni(ws.getIxIndici().getTipoOpz()).getGarOpzione(ws.getIxIndici().getOpz()).getCodice(), ws.getAreaBusiness().getWgopTabGar(ws.getIxIndici().getGop()).getLccvgrz1().getDati().getWgrzCodTari())) {
                // COB_CODE: MOVE WGOP-ID-GAR(IND-GOP)
                //                TO IVVC0216-ID-GAR-T    (IND-LIVELLO)
                ws.getAreaWorkIvvc0216().getC216TabValT().setIvvc0216IdGarT(ws.getIxIndici().getLivello(), TruncAbs.toInt(ws.getAreaBusiness().getWgopTabGar(ws.getIxIndici().getGop()).getLccvgrz1().getDati().getWgrzIdGar(), 9));
                // COB_CODE: PERFORM L381-RICERCA-TOP-X-GOP THRU L381-EX
                l381RicercaTopXGop();
            }
            ws.getIxIndici().setGop(Trunc.toShort(ws.getIxIndici().getGop() + 1, 4));
        }
    }

    /**Original name: L381-RICERCA-TOP-X-GOP<br>
	 * <pre>----------------------------------------------------------------*
	 *     CARICAMENTO DEI LIVELLI PRODOTTI NELLE OCCORRENZE
	 *     DELL'AREA VARIABILI
	 * ----------------------------------------------------------------*</pre>*/
    private void l381RicercaTopXGop() {
        // COB_CODE: MOVE 1                         TO IND-TOP
        ws.getIxIndici().setTop(((short)1));
        // COB_CODE: PERFORM VARYING IND-TOP FROM 1 BY 1
        //                   UNTIL IND-TOP > WK-TGA-MAX-B                     OR
        //                         IND-TOP > WTOP-ELE-TRAN-OPZ-MAX          OR
        //                         WTOP-ID-TRCH-DI-GAR(IND-TOP) NOT NUMERIC OR
        //                         WTOP-ID-TRCH-DI-GAR(IND-TOP) = ZEROES
        //                        END-IF
        //           END-PERFORM.
        ws.getIxIndici().setTop(((short)1));
        while (!(ws.getIxIndici().getTop() > ws.getWkTgaMax().getB() || ws.getIxIndici().getTop() > ws.getAreaBusiness().getWtopEleTranOpzMax() || !Functions.isNumber(ws.getAreaBusiness().getWtopTabTran(ws.getIxIndici().getTop()).getLccvtga1().getDati().getWtgaIdTrchDiGar()) || ws.getAreaBusiness().getWtopTabTran(ws.getIxIndici().getTop()).getLccvtga1().getDati().getWtgaIdTrchDiGar() == 0)) {
            // COB_CODE: IF WGOP-ID-GAR(IND-GOP)
            //              =
            //              WTOP-ID-GAR(IND-TOP)
            //                   TO IVVC0216-DT-DECOR-TRCH-T(IND-LIVELLO)
            //           END-IF
            if (ws.getAreaBusiness().getWgopTabGar(ws.getIxIndici().getGop()).getLccvgrz1().getDati().getWgrzIdGar() == ws.getAreaBusiness().getWtopTabTran(ws.getIxIndici().getTop()).getLccvtga1().getDati().getWtgaIdGar()) {
                // COB_CODE: MOVE WTOP-ID-TRCH-DI-GAR      (IND-TOP)
                //                TO IVVC0216-ID-LIVELLO-T  (IND-LIVELLO)
                ws.getAreaWorkIvvc0216().getC216TabValT().setIvvc0216IdLivelloT(ws.getIxIndici().getLivello(), TruncAbs.toInt(ws.getAreaBusiness().getWtopTabTran(ws.getIxIndici().getTop()).getLccvtga1().getDati().getWtgaIdTrchDiGar(), 9));
                // COB_CODE: MOVE WTOP-DT-INI-VAL-TAR      (IND-TOP)
                //                TO IVVC0216-DT-INIZ-TARI-T(IND-LIVELLO)
                ws.getAreaWorkIvvc0216().getC216TabValT().setIvvc0216DtInizTariT(ws.getIxIndici().getLivello(), ws.getAreaBusiness().getWtopTabTran(ws.getIxIndici().getTop()).getLccvtga1().getDati().getWtgaDtIniValTar().getWtopDtIniValTarFormatted());
                // COB_CODE: MOVE WTOP-TP-RGM-FISC         (IND-TOP)
                //                TO IVVC0216-COD-RGM-FISC-T(IND-LIVELLO)
                ws.getAreaWorkIvvc0216().getC216TabValT().setIvvc0216CodRgmFiscT(ws.getIxIndici().getLivello(), ws.getAreaBusiness().getWtopTabTran(ws.getIxIndici().getTop()).getLccvtga1().getDati().getWtgaTpRgmFisc());
                // COB_CODE: MOVE WTOP-DT-DECOR            (IND-TOP)
                //                TO IVVC0216-DT-DECOR-TRCH-T(IND-LIVELLO)
                ws.getAreaWorkIvvc0216().getC216TabValT().setIvvc0216DtDecorTrchT(ws.getIxIndici().getLivello(), ws.getAreaBusiness().getWtopTabTran(ws.getIxIndici().getTop()).getLccvtga1().getDati().getWtgaDtDecorFormatted());
            }
            ws.getIxIndici().setTop(Trunc.toShort(ws.getIxIndici().getTop() + 1, 4));
        }
    }

    /**Original name: L390-VALORIZZA-TP-LIVELLO<br>
	 * <pre>----------------------------------------------------------------*
	 *     CARICAMENTO DEI LIVELLI PRODOTTI NELLE OCCORRENZE
	 *     DELL'AREA VARIABILI
	 * ----------------------------------------------------------------*</pre>*/
    private void l390ValorizzaTpLivello() {
        // COB_CODE: IF GESTIONE-PRODOTTO
        //                TO IVVC0216-TP-LIVELLO-P     (IND-LIVELLO)
        //           ELSE
        //                TO IVVC0216-TP-LIVELLO-T     (IND-LIVELLO)
        //           END-IF.
        if (ws.getFlagGestione().isProdotto()) {
            // COB_CODE: MOVE WS-LIV-PRODOTTO
            //             TO IVVC0216-TP-LIVELLO-P     (IND-LIVELLO)
            ws.getAreaWorkIvvc0216().getC216TabValP().setIvvc0216TpLivelloP(ws.getIxIndici().getLivello(), ws.getWsLivProdotto());
        }
        else {
            // COB_CODE: MOVE WS-LIV-GARANZIA
            //             TO IVVC0216-TP-LIVELLO-T     (IND-LIVELLO)
            ws.getAreaWorkIvvc0216().getC216TabValT().setIvvc0216TpLivelloT(ws.getIxIndici().getLivello(), ws.getWsLivGaranzia());
        }
    }

    /**Original name: L400-CERCA-TRANCHE<br>
	 * <pre>----------------------------------------------------------------*
	 *     CERCA TRANCHE DI GARANZIA
	 * ----------------------------------------------------------------*</pre>*/
    private void l400CercaTranche() {
        // COB_CODE: SET TRANCHE-INIT                   TO TRUE
        ws.getFlagTipoTranche().setInit();
        // COB_CODE: PERFORM L451-LETTURA-TRANCHE-CONT  THRU L451-EX
        l451LetturaTrancheCont();
        // COB_CODE: IF NOT TRANCHE-CONTESTO AND FINE-ELAB-NO
        //              END-IF
        //           END-IF.
        if (!ws.getFlagTipoTranche().isContesto() && ws.getFlagFineElab().isNo()) {
            // COB_CODE: IF GESTIONE-GARANZIE AND WGRZ-ST-ADD(IND-GRZ)
            //              PERFORM M000-GESTIONE-VAR-FUN-CALC THRU M000-EX
            //           ELSE
            //              END-IF
            //           END-IF
            if (ws.getFlagGestione().isGaranzie() && ws.getAreaBusiness().getWgrzTabGar(ws.getIxIndici().getGrz()).getLccvgrz1().getStatus().isAdd()) {
                // COB_CODE: PERFORM M000-GESTIONE-VAR-FUN-CALC THRU M000-EX
                m000GestioneVarFunCalc();
            }
            else {
                // COB_CODE: PERFORM N000-CERCA-ADESIONE THRU N000-EX
                n000CercaAdesione();
                // COB_CODE: IF IVVC0211-SUCCESSFUL-RC
                //              END-IF
                //           END-IF
                if (inputIvvs0211.getIvvc0211RestoDati().getReturnCode().isSuccessfulRc()) {
                    //                 OR IVVC0211-TIPO-MOVIMENTO = 2334
                    //                 OR IVVC0211-TIPO-MOVIMENTO = 2335
                    // COB_CODE:               IF WPOL-TP-FRM-ASSVA = 'CO'
                    //                         AND (IVVC0211-TIPO-MOVIMENTO = 2031
                    //                             OR IVVC0211-TIPO-MOVIMENTO = 2001
                    //           *                 OR IVVC0211-TIPO-MOVIMENTO = 2334
                    //           *                 OR IVVC0211-TIPO-MOVIMENTO = 2335
                    //                             OR IVVC0211-TIPO-MOVIMENTO = 2339
                    //                             OR IVVC0211-TIPO-MOVIMENTO = 2340
                    //                             OR IVVC0211-TIPO-MOVIMENTO = 2006 )
                    //                             CONTINUE
                    //                         ELSE
                    //                         PERFORM L450-LETTURA-TRANCHE-DB THRU L450-EX
                    //                         END-IF
                    if (Conditions.eq(ws.getAreaBusiness().getLccvpol1().getDati().getWpolTpFrmAssva(), "CO") && (inputIvvs0211.getIvvc0211DatiInput().getTipoMovimento() == 2031 || inputIvvs0211.getIvvc0211DatiInput().getTipoMovimento() == 2001 || inputIvvs0211.getIvvc0211DatiInput().getTipoMovimento() == 2339 || inputIvvs0211.getIvvc0211DatiInput().getTipoMovimento() == 2340 || inputIvvs0211.getIvvc0211DatiInput().getTipoMovimento() == 2006)) {
                    // COB_CODE: CONTINUE
                    //continue
                    }
                    else {
                        // COB_CODE: PERFORM L450-LETTURA-TRANCHE-DB THRU L450-EX
                        l450LetturaTrancheDb();
                    }
                }
            }
        }
    }

    /**Original name: L440-LETTURA-GARANZIA-DB<br>
	 * <pre>----------------------------------------------------------------*
	 *     LETTURA GARANZIA DA DB
	 * ----------------------------------------------------------------*</pre>*/
    private void l440LetturaGaranziaDb() {
        Ldbs1350 ldbs1350 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: PERFORM E999-CNTL-STAT-CAUS THRU E999-EX
        e999CntlStatCaus();
        // COB_CODE:      IF FINE-ELAB-NO
        //           *--> valorizzazione
        //                   END-PERFORM
        //                END-IF.
        if (ws.getFlagFineElab().isNo()) {
            //--> valorizzazione
            // COB_CODE: PERFORM L441-VAL-WHERE-GRZ  THRU L441-EX
            l441ValWhereGrz();
            // COB_CODE: MOVE LDBV1351               TO IDSV0003-BUFFER-WHERE-COND
            ws.getIdsv0003().setBufferWhereCond(ws.getLdbv1351().getLdbv1351Formatted());
            //--> SETTAGGI
            //       MOVE IVVC0211-TRATTAMENTO-STORICITA
            //                          TO IDSV0003-TRATTAMENTO-STORICITA
            // COB_CODE: SET FINE-GARANZIE-NO        TO TRUE
            ws.getFlagEsitoGaranzie().setNo();
            // COB_CODE:         PERFORM UNTIL FINE-GARANZIE-SI OR
            //                                 FINE-ELAB-SI
            //           *--> PER LA JIRA8 E STATA SPOSTATO IL SEL DEL TRATTAMENTO ALL
            //           *--> INTERNO DELLA PERFORM VARYING PER EVITARE SETTAGGGI
            //           *--> SPORCHI
            //           *--> SETTAGGI
            //                       END-IF
            //                   END-PERFORM
            while (!(ws.getFlagEsitoGaranzie().isSi() || ws.getFlagFineElab().isSi())) {
                //--> PER LA JIRA8 E STATA SPOSTATO IL SEL DEL TRATTAMENTO ALL
                //--> INTERNO DELLA PERFORM VARYING PER EVITARE SETTAGGGI
                //--> SPORCHI
                //--> SETTAGGI
                // COB_CODE: MOVE IVVC0211-TRATTAMENTO-STORICITA
                //                              TO IDSV0003-TRATTAMENTO-STORICITA
                ws.getIdsv0003().getTrattamentoStoricita().setTrattamentoStoricita(inputIvvs0211.getIvvc0211DatiInput().getTrattamentoStoricita());
                //--> INIZIALIZZA CODICE DI RITORNO
                // COB_CODE: SET IDSV0003-SUCCESSFUL-RC     TO TRUE
                ws.getIdsv0003().getReturnCode().setIdsv0003SuccessfulRc();
                // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL    TO TRUE
                ws.getIdsv0003().getSqlcode().setSuccessfulSql();
                //
                // COB_CODE: CALL PGM-LDBS1350 USING      IDSV0003
                //                                        GAR
                ldbs1350 = Ldbs1350.getInstance();
                ldbs1350.run(ws.getIdsv0003(), ws.getGar());
                // COB_CODE:          IF IDSV0003-SUCCESSFUL-RC
                //                                END-STRING
                //                       ELSE
                //           *-->           GESTIRE ERRORE DISPATCHER
                //                          END-STRING
                //                       END-IF
                if (ws.getIdsv0003().getReturnCode().isSuccessfulRc()) {
                    // COB_CODE:           EVALUATE TRUE
                    //                         WHEN IDSV0003-SUCCESSFUL-SQL
                    //           *-->          OPERAZIONE ESEGUITA CORRETTAMENTE
                    //                              END-IF
                    //                         WHEN IDSV0003-NOT-FOUND
                    //           *--->         CAMPO $ NON TROVATO
                    //                               END-IF
                    //                            WHEN OTHER
                    //           *--->            ERRORE DI ACCESSO AL DB
                    //                                END-STRING
                    switch (ws.getIdsv0003().getSqlcode().getSqlcode()) {

                        case Idsv0003Sqlcode.SUCCESSFUL_SQL://-->          OPERAZIONE ESEGUITA CORRETTAMENTE
                            // COB_CODE: IF  RIASS-SI
                            //               END-IF
                            //           END-IF
                            if (ws.getFlagRiass().isSi()) {
                                // COB_CODE: IF  GRZ-COD-TRAT-RIASS     > SPACES
                                //           AND GRZ-COD-TRAT-RIASS NOT = HIGH-VALUE
                                //               END-IF
                                //           ELSE
                                //               SET FINE-ELAB-SI            TO TRUE
                                //           END-IF
                                if (Characters.GT_SPACE.test(ws.getGar().getGrzCodTratRiass()) && !Characters.EQ_HIGH.test(ws.getGar().getGrzCodTratRiassFormatted())) {
                                    // COB_CODE: IF  GRZ-COD-TRAT-RIASS NOT =
                                    //               IVVC0211-CODICE-TRATTATO
                                    //               SET FINE-ELAB-SI            TO TRUE
                                    //           END-IF
                                    if (!Conditions.eq(ws.getGar().getGrzCodTratRiass(), inputIvvs0211.getIvvc0211RestoDati().getCodiceTrattato())) {
                                        // COB_CODE: SET FLAG-TRATTATO-VALIDO-NO TO TRUE
                                        ws.getFlagTrattatoValido().setNo();
                                        // COB_CODE: SET FINE-ELAB-SI            TO TRUE
                                        ws.getFlagFineElab().setSi();
                                    }
                                }
                                else {
                                    // COB_CODE: SET FLAG-TRATTATO-VALIDO-NO TO TRUE
                                    ws.getFlagTrattatoValido().setNo();
                                    // COB_CODE: SET FINE-ELAB-SI            TO TRUE
                                    ws.getFlagFineElab().setSi();
                                }
                            }
                            // COB_CODE: IF FINE-ELAB-NO
                            //              SET IDSV0003-FETCH-NEXT   TO TRUE
                            //           END-IF
                            if (ws.getFlagFineElab().isNo()) {
                                // COB_CODE: SET GARANZIA-DB                TO TRUE
                                ws.getFlagTipoGaranzia().setDb();
                                // COB_CODE: PERFORM L310-ELABORA-GARANZIA  THRU L310-EX
                                l310ElaboraGaranzia();
                                // COB_CODE: SET IDSV0003-FETCH-NEXT   TO TRUE
                                ws.getIdsv0003().getOperazione().setFetchNext();
                            }
                            break;

                        case Idsv0003Sqlcode.NOT_FOUND://--->         CAMPO $ NON TROVATO
                            // COB_CODE: SET FINE-GARANZIE-SI       TO TRUE
                            ws.getFlagEsitoGaranzie().setSi();
                            // COB_CODE: IF IDSV0003-FETCH-FIRST
                            //               MOVE 'GAR-STB' TO IVVC0211-NOME-TABELLA
                            //            END-IF
                            if (ws.getIdsv0003().getOperazione().isFetchFirst()) {
                                // COB_CODE: SET FINE-ELAB-SI       TO TRUE
                                ws.getFlagFineElab().setSi();
                                // COB_CODE: SET IVVC0211-SQL-ERROR TO TRUE
                                inputIvvs0211.getIvvc0211RestoDati().getReturnCode().setSqlError();
                                // COB_CODE: MOVE PGM-LDBS1350
                                //                    TO IVVC0211-COD-SERVIZIO-BE
                                inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setCodServizioBe(ws.getPgmLdbs1350());
                                // COB_CODE: STRING 'GARANZIE NON TROVATE'
                                //                  ' - '
                                //                  'POLIZZA : '
                                //                  WK-ID-POLI
                                //                  ' - '
                                //                  'GARANZIA : '
                                //                  WK-ID-GAR
                                //                  DELIMITED BY SIZE INTO
                                //                  IVVC0211-DESCRIZ-ERR
                                //           END-STRING
                                concatUtil = ConcatUtil.buildString(Ivvc0211CampiEsito.Len.DESCRIZ_ERR, new String[] {"GARANZIE NON TROVATE", " - ", "POLIZZA : ", ws.getWkIdPoliAsString(), " - ", "GARANZIA : ", ws.getWkIdGarAsString()});
                                inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setDescrizErr(concatUtil.replaceInString(inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().getDescrizErrFormatted()));
                                // COB_CODE: MOVE 'GAR-STB' TO IVVC0211-NOME-TABELLA
                                inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setNomeTabella("GAR-STB");
                            }
                            break;

                        default://--->            ERRORE DI ACCESSO AL DB
                            // COB_CODE: SET FINE-ELAB-SI       TO TRUE
                            ws.getFlagFineElab().setSi();
                            // COB_CODE: SET IVVC0211-SQL-ERROR TO TRUE
                            inputIvvs0211.getIvvc0211RestoDati().getReturnCode().setSqlError();
                            // COB_CODE: MOVE PGM-LDBS1350
                            //                TO IVVC0211-COD-SERVIZIO-BE
                            inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setCodServizioBe(ws.getPgmLdbs1350());
                            // COB_CODE: MOVE IDSV0003-SQLCODE TO WS-SQLCODE
                            ws.setWsSqlcode(ws.getIdsv0003().getSqlcode().getSqlcode());
                            // COB_CODE: STRING PGM-LDBS1350
                            //                ' - '
                            //                'POLIZZA : '
                            //                WK-ID-POLI
                            //                ' - '
                            //                'GARANZIA : '
                            //                WK-ID-GAR
                            //                'RC : '
                            //               IDSV0003-RETURN-CODE
                            //                ' - '
                            //                'SQLCODE : '
                            //               WS-SQLCODE
                            //               DELIMITED BY SIZE INTO
                            //               IVVC0211-DESCRIZ-ERR
                            //           END-STRING
                            concatUtil = ConcatUtil.buildString(Ivvc0211CampiEsito.Len.DESCRIZ_ERR, new String[] {ws.getPgmLdbs1350Formatted(), " - ", "POLIZZA : ", ws.getWkIdPoliAsString(), " - ", "GARANZIA : ", ws.getWkIdGarAsString(), "RC : ", ws.getIdsv0003().getReturnCode().getReturnCodeFormatted(), " - ", "SQLCODE : ", ws.getWsSqlcodeAsString()});
                            inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setDescrizErr(concatUtil.replaceInString(inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().getDescrizErrFormatted()));
                            break;
                    }
                }
                else {
                    //-->           GESTIRE ERRORE DISPATCHER
                    // COB_CODE: SET FINE-ELAB-SI        TO TRUE
                    ws.getFlagFineElab().setSi();
                    // COB_CODE: SET IVVC0211-SQL-ERROR  TO TRUE
                    inputIvvs0211.getIvvc0211RestoDati().getReturnCode().setSqlError();
                    // COB_CODE: MOVE PGM-LDBS1350
                    //                TO IVVC0211-COD-SERVIZIO-BE
                    inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setCodServizioBe(ws.getPgmLdbs1350());
                    // COB_CODE: STRING PGM-LDBS1350
                    //                  ' - '
                    //                  'RC : '
                    //                  IDSV0003-RETURN-CODE
                    //                  DELIMITED BY SIZE INTO
                    //                  IVVC0211-DESCRIZ-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ivvc0211CampiEsito.Len.DESCRIZ_ERR, ws.getPgmLdbs1350Formatted(), " - ", "RC : ", ws.getIdsv0003().getReturnCode().getReturnCodeFormatted());
                    inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setDescrizErr(concatUtil.replaceInString(inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().getDescrizErrFormatted()));
                }
            }
        }
    }

    /**Original name: L441-VAL-WHERE-GRZ<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZA CAMPI WHERE CONDITION GARANZIA
	 * ----------------------------------------------------------------*</pre>*/
    private void l441ValWhereGrz() {
        // COB_CODE: PERFORM L800-VALORIZZA-STAT-CAUS-GRZ THRU L800-EX
        l800ValorizzaStatCausGrz();
        // COB_CODE: IF IVVC0211-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (inputIvvs0211.getIvvc0211RestoDati().getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE WADE-ID-ADES                TO GRZ-ID-ADES
            ws.getGar().getGrzIdAdes().setGrzIdAdes(ws.getAreaBusiness().getLccvade1().getDati().getWadeIdAdes());
            // COB_CODE: IF GESTIONE-PRODOTTO OR GESTIONE-GARANZIE
            //              SET IDSV0003-FETCH-FIRST      TO TRUE
            //           END-IF
            if (ws.getFlagGestione().isProdotto() || ws.getFlagGestione().isGaranzie()) {
                // COB_CODE: MOVE WPOL-ID-POLI             TO GRZ-ID-POLI
                //                                             WK-ID-POLI
                ws.getGar().setGrzIdPoli(ws.getAreaBusiness().getLccvpol1().getDati().getWpolIdPoli());
                ws.setWkIdPoli(TruncAbs.toInt(ws.getAreaBusiness().getLccvpol1().getDati().getWpolIdPoli(), 9));
                // COB_CODE: SET IDSV0003-FETCH-FIRST      TO TRUE
                ws.getIdsv0003().getOperazione().setFetchFirst();
            }
        }
    }

    /**Original name: L450-LETTURA-TRANCHE-DB<br>
	 * <pre>----------------------------------------------------------------*
	 *     LETTURA TRANCHE DI GARANZIA DA DB
	 * ----------------------------------------------------------------*</pre>*/
    private void l450LetturaTrancheDb() {
        Ldbs1360 ldbs1360 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: PERFORM E999-CNTL-STAT-CAUS THRU E999-EX
        e999CntlStatCaus();
        // COB_CODE: IF FINE-ELAB-NO
        //              END-PERFORM
        //           END-IF.
        if (ws.getFlagFineElab().isNo()) {
            // COB_CODE: SET IDSV0003-FETCH-FIRST                TO TRUE
            ws.getIdsv0003().getOperazione().setFetchFirst();
            // COB_CODE: SET FINE-TRANCHE-NO            TO TRUE
            ws.getFlagEsitoTranche().setNo();
            //----------------------------------------------------------------*
            // COB_CODE:         PERFORM UNTIL FINE-TRANCHE-SI OR
            //                                 FINE-ELAB-SI
            //           *-->    INIZIALIZZA CODICE DI RITORNO
            //                       END-IF
            //                   END-PERFORM
            while (!(ws.getFlagEsitoTranche().isSi() || ws.getFlagFineElab().isSi())) {
                //-->    INIZIALIZZA CODICE DI RITORNO
                // COB_CODE: SET IDSV0003-SUCCESSFUL-RC           TO TRUE
                ws.getIdsv0003().getReturnCode().setIdsv0003SuccessfulRc();
                // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL          TO TRUE
                ws.getIdsv0003().getSqlcode().setSuccessfulSql();
                // COB_CODE: MOVE IVVC0211-TRATTAMENTO-STORICITA
                //                            TO IDSV0003-TRATTAMENTO-STORICITA
                ws.getIdsv0003().getTrattamentoStoricita().setTrattamentoStoricita(inputIvvs0211.getIvvc0211DatiInput().getTrattamentoStoricita());
                //-->    valorizzazione stati causali
                // COB_CODE: PERFORM L900-VALORIZZA-STAT-CAUS-TGA THRU L900-EX
                l900ValorizzaStatCausTga();
                // COB_CODE: PERFORM L460-VALORIZZA-DCL-TGA       THRU L460-EX
                l460ValorizzaDclTga();
                // COB_CODE: MOVE LDBV1361       TO IDSV0003-BUFFER-WHERE-COND
                ws.getIdsv0003().setBufferWhereCond(ws.getLdbv1361().getLdbv1351Formatted());
                //
                // COB_CODE: CALL PGM-LDBS1360 USING      IDSV0003
                //                                        TRCH-DI-GAR
                ldbs1360 = Ldbs1360.getInstance();
                ldbs1360.run(ws.getIdsv0003(), ws.getTrchDiGar());
                // COB_CODE:             IF IDSV0003-SUCCESSFUL-RC
                //                                END-STRING
                //                       ELSE
                //           *-->           GESTIRE ERRORE DISPATCHER
                //                          END-STRING
                //                       END-IF
                if (ws.getIdsv0003().getReturnCode().isSuccessfulRc()) {
                    // COB_CODE:              EVALUATE TRUE
                    //                            WHEN IDSV0003-SUCCESSFUL-SQL
                    //           *-->             OPERAZIONE ESEGUITA CORRETTAMENTE
                    //                              SET IDSV0003-FETCH-NEXT      TO TRUE
                    //                            WHEN IDSV0003-NOT-FOUND
                    //           *--->            CAMPO $ NON TROVATO
                    //                               END-IF
                    //                            WHEN OTHER
                    //           *--->            ERRORE DI ACCESSO AL DB
                    //                                END-STRING
                    switch (ws.getIdsv0003().getSqlcode().getSqlcode()) {

                        case Idsv0003Sqlcode.SUCCESSFUL_SQL://-->             OPERAZIONE ESEGUITA CORRETTAMENTE
                            // COB_CODE: SET TRANCHE-DB               TO TRUE
                            ws.getFlagTipoTranche().setDb();
                            // COB_CODE: PERFORM M000-GESTIONE-VAR-FUN-CALC
                            //                                        THRU M000-EX
                            m000GestioneVarFunCalc();
                            // COB_CODE: SET IDSV0003-FETCH-NEXT      TO TRUE
                            ws.getIdsv0003().getOperazione().setFetchNext();
                            break;

                        case Idsv0003Sqlcode.NOT_FOUND://--->            CAMPO $ NON TROVATO
                            // COB_CODE: SET FINE-TRANCHE-SI          TO TRUE
                            ws.getFlagEsitoTranche().setSi();
                            // COB_CODE: IF IDSV0003-FETCH-FIRST
                            //                           TO IVVC0211-NOME-TABELLA
                            //            END-IF
                            if (ws.getIdsv0003().getOperazione().isFetchFirst()) {
                                // COB_CODE: SET FINE-ELAB-SI         TO TRUE
                                ws.getFlagFineElab().setSi();
                                // COB_CODE: SET IVVC0211-SQL-ERROR   TO TRUE
                                inputIvvs0211.getIvvc0211RestoDati().getReturnCode().setSqlError();
                                // COB_CODE: MOVE PGM-LDBS1360
                                //                       TO IVVC0211-COD-SERVIZIO-BE
                                inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setCodServizioBe(ws.getPgmLdbs1360());
                                // COB_CODE: IF GESTIONE-GARANZIE
                                //              END-STRING
                                //           ELSE
                                //              END-STRING
                                //           END-IF
                                if (ws.getFlagGestione().isGaranzie()) {
                                    // COB_CODE: STRING 'TRANCHE NON TROVATA'
                                    //                  ' PER GESTIONE GARANZIE'
                                    //                  ' - '
                                    //                  'POLIZZA : '
                                    //                  WK-ID-POLI
                                    //                  ' - '
                                    //                  'GARANZIA : '
                                    //                  WK-ID-GAR
                                    //                  DELIMITED BY SIZE INTO
                                    //                  IVVC0211-DESCRIZ-ERR
                                    //           END-STRING
                                    concatUtil = ConcatUtil.buildString(Ivvc0211CampiEsito.Len.DESCRIZ_ERR, new String[] {"TRANCHE NON TROVATA", " PER GESTIONE GARANZIE", " - ", "POLIZZA : ", ws.getWkIdPoliAsString(), " - ", "GARANZIA : ", ws.getWkIdGarAsString()});
                                    inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setDescrizErr(concatUtil.replaceInString(inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().getDescrizErrFormatted()));
                                }
                                else {
                                    // COB_CODE: STRING 'TRANCHE NON TROVATA'
                                    //                  ' PER GESTIONE PRODOTTO'
                                    //                  ' - '
                                    //                  'POLIZZA : '
                                    //                  WK-ID-POLI
                                    //                  DELIMITED BY SIZE INTO
                                    //                  IVVC0211-DESCRIZ-ERR
                                    //           END-STRING
                                    concatUtil = ConcatUtil.buildString(Ivvc0211CampiEsito.Len.DESCRIZ_ERR, "TRANCHE NON TROVATA", " PER GESTIONE PRODOTTO", " - ", "POLIZZA : ", ws.getWkIdPoliAsString());
                                    inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setDescrizErr(concatUtil.replaceInString(inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().getDescrizErrFormatted()));
                                }
                                // COB_CODE: MOVE 'TRCH_DI_GAR'
                                //                       TO IVVC0211-NOME-TABELLA
                                inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setNomeTabella("TRCH_DI_GAR");
                            }
                            break;

                        default://--->            ERRORE DI ACCESSO AL DB
                            // COB_CODE: SET FINE-ELAB-SI              TO TRUE
                            ws.getFlagFineElab().setSi();
                            // COB_CODE: SET IVVC0211-SQL-ERROR        TO TRUE
                            inputIvvs0211.getIvvc0211RestoDati().getReturnCode().setSqlError();
                            // COB_CODE: MOVE PGM-LDBS1360
                            //                        TO IVVC0211-COD-SERVIZIO-BE
                            inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setCodServizioBe(ws.getPgmLdbs1360());
                            // COB_CODE: MOVE IDSV0003-SQLCODE TO WS-SQLCODE
                            ws.setWsSqlcode(ws.getIdsv0003().getSqlcode().getSqlcode());
                            // COB_CODE: STRING PGM-LDBS1360
                            //                ' - '
                            //                'POLIZZA : '
                            //                WK-ID-POLI
                            //                ' - '
                            //                'GARANZIA : '
                            //                WK-ID-GAR
                            //                ' - '
                            //                'RC : '
                            //               IDSV0003-RETURN-CODE
                            //                ' - '
                            //                'SQLCODE : '
                            //               WS-SQLCODE
                            //               DELIMITED BY SIZE INTO
                            //               IVVC0211-DESCRIZ-ERR
                            //           END-STRING
                            concatUtil = ConcatUtil.buildString(Ivvc0211CampiEsito.Len.DESCRIZ_ERR, new String[] {ws.getPgmLdbs1360Formatted(), " - ", "POLIZZA : ", ws.getWkIdPoliAsString(), " - ", "GARANZIA : ", ws.getWkIdGarAsString(), " - ", "RC : ", ws.getIdsv0003().getReturnCode().getReturnCodeFormatted(), " - ", "SQLCODE : ", ws.getWsSqlcodeAsString()});
                            inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setDescrizErr(concatUtil.replaceInString(inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().getDescrizErrFormatted()));
                            break;
                    }
                }
                else {
                    //-->           GESTIRE ERRORE DISPATCHER
                    // COB_CODE: SET FINE-ELAB-SI        TO TRUE
                    ws.getFlagFineElab().setSi();
                    // COB_CODE: SET IVVC0211-SQL-ERROR  TO TRUE
                    inputIvvs0211.getIvvc0211RestoDati().getReturnCode().setSqlError();
                    // COB_CODE: MOVE PGM-LDBS1350       TO IVVC0211-COD-SERVIZIO-BE
                    inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setCodServizioBe(ws.getPgmLdbs1350());
                    // COB_CODE: STRING PGM-LDBS1360
                    //                  ' - '
                    //                  'RC : '
                    //                  IDSV0003-RETURN-CODE
                    //                  DELIMITED BY SIZE INTO
                    //                  IVVC0211-DESCRIZ-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ivvc0211CampiEsito.Len.DESCRIZ_ERR, ws.getPgmLdbs1360Formatted(), " - ", "RC : ", ws.getIdsv0003().getReturnCode().getReturnCodeFormatted());
                    inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setDescrizErr(concatUtil.replaceInString(inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().getDescrizErrFormatted()));
                }
            }
        }
    }

    /**Original name: L451-LETTURA-TRANCHE-CONT<br>
	 * <pre>----------------------------------------------------------------*
	 *     LETTURA TRANCHE DI GARANZIA A CONTESTO
	 * ----------------------------------------------------------------*
	 *     Inizializzazione  STR-DIST-DT-VLDT-PROD-TGA</pre>*/
    private void l451LetturaTrancheCont() {
        // COB_CODE: INITIALIZE  DIST-TAB-DT-VLDT-PROD(1).
        ws.getStrDistDtVldtProdTga().setDistTabDtVldtProd(1, 0);
        // COB_CODE: MOVE STR-DIST-DT-VLDT-PROD-TGA
        //             TO STR-RES-DIST-DT-VLDT-PROD-TGA.
        ws.getStrDistDtVldtProdTga().setStrResDistDtVldtProdTga(ws.getStrDistDtVldtProdTga().getStrDistDtVldtProdTgaFormatted());
        // COB_CODE: PERFORM VARYING IND-TGA FROM 1 BY 1
        //                   UNTIL IND-TGA >  WTGA-ELE-TRAN-MAX             OR
        //                         IND-TGA >  WK-TGA-MAX-C                    OR
        //                         NOT IVVC0211-SUCCESSFUL-RC
        //                         END-IF
        //           END-PERFORM.
        ws.getIxIndici().setTga(((short)1));
        while (!(ws.getIxIndici().getTga() > ws.getAreaBusiness().getWtgaAreaTranche().getWtgaEleTranMax() || ws.getIxIndici().getTga() > ws.getWkTgaMax().getC() || !inputIvvs0211.getIvvc0211RestoDati().getReturnCode().isSuccessfulRc())) {
            // COB_CODE: PERFORM L452-CNTL-TGA-X-GRZ THRU L452-EX
            l452CntlTgaXGrz();
            // COB_CODE: IF TRATTA-TRANCHE-SI
            //                                         THRU C000-EX
            //           END-IF
            if (ws.getFlagTrattaTranche().isSi()) {
                // COB_CODE: SET TRANCHE-CONTESTO       TO TRUE
                ws.getFlagTipoTranche().setContesto();
                // COB_CODE: PERFORM C000-GESTIONE-DT-VLDT-PROD
                //                                      THRU C000-EX
                c000GestioneDtVldtProd();
            }
            ws.getIxIndici().setTga(Trunc.toShort(ws.getIxIndici().getTga() + 1, 4));
        }
    }

    /**Original name: L452-CNTL-TGA-X-GRZ<br>
	 * <pre>----------------------------------------------------------------*
	 *     CONTROLLA TGA X GRZ
	 * ----------------------------------------------------------------*</pre>*/
    private void l452CntlTgaXGrz() {
        // COB_CODE: SET TRATTA-TRANCHE-SI          TO TRUE
        ws.getFlagTrattaTranche().setSi();
        // COB_CODE: IF GESTIONE-GARANZIE
        //              END-IF
        //           END-IF.
        if (ws.getFlagGestione().isGaranzie()) {
            // COB_CODE: IF GARANZIA-CONTESTO
            //              END-IF
            //           ELSE
            //              END-IF
            //           END-IF
            if (ws.getFlagTipoGaranzia().isContesto()) {
                // COB_CODE: IF WTGA-ID-GAR(IND-TGA) NOT = WGRZ-ID-GAR(IND-GRZ)
                //              SET TRATTA-TRANCHE-NO TO TRUE
                //           END-IF
                if (ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().getIdGar(ws.getIxIndici().getTga()) != ws.getAreaBusiness().getWgrzTabGar(ws.getIxIndici().getGrz()).getLccvgrz1().getDati().getWgrzIdGar()) {
                    // COB_CODE: SET TRATTA-TRANCHE-NO TO TRUE
                    ws.getFlagTrattaTranche().setNo();
                }
            }
            else if (ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().getIdGar(ws.getIxIndici().getTga()) != ws.getGar().getGrzIdGar()) {
                // COB_CODE: IF WTGA-ID-GAR(IND-TGA) NOT = GRZ-ID-GAR
                //              SET TRATTA-TRANCHE-NO TO TRUE
                //           END-IF
                // COB_CODE: SET TRATTA-TRANCHE-NO TO TRUE
                ws.getFlagTrattaTranche().setNo();
            }
        }
    }

    /**Original name: L460-VALORIZZA-DCL-TGA<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZAZIONE DCLGEN TGA
	 * ----------------------------------------------------------------*</pre>*/
    private void l460ValorizzaDclTga() {
        // COB_CODE: INITIALIZE TRCH-DI-GAR
        initTrchDiGar();
        // COB_CODE: MOVE WPOL-ID-POLI               TO TGA-ID-POLI
        //                                               WK-ID-POLI
        ws.getTrchDiGar().setTgaIdPoli(ws.getAreaBusiness().getLccvpol1().getDati().getWpolIdPoli());
        ws.setWkIdPoli(TruncAbs.toInt(ws.getAreaBusiness().getLccvpol1().getDati().getWpolIdPoli(), 9));
        // COB_CODE: MOVE WADE-ID-ADES               TO TGA-ID-ADES
        ws.getTrchDiGar().setTgaIdAdes(ws.getAreaBusiness().getLccvade1().getDati().getWadeIdAdes());
        // COB_CODE: EVALUATE TRUE
        //              WHEN GESTIONE-PRODOTTO
        //                   SET LDBV1361-PRODOTTO           TO TRUE
        //              WHEN GESTIONE-GARANZIE
        //                   END-IF
        //           END-EVALUATE.
        switch (ws.getFlagGestione().getFlagGestione()) {

            case FlagGestione.PRODOTTO:// COB_CODE: SET LDBV1361-PRODOTTO           TO TRUE
                ws.getLdbv1361().getLdbv1351LivelloOperazioni().setLdbv1361Prodotto();
                break;

            case FlagGestione.GARANZIE:// COB_CODE: SET LDBV1361-GARANZIE           TO TRUE
                ws.getLdbv1361().getLdbv1351LivelloOperazioni().setLdbv1361Garanzie();
                // COB_CODE: IF GARANZIA-CONTESTO
                //                                               WK-ID-GAR
                //           ELSE
                //                                               WK-ID-GAR
                //           END-IF
                if (ws.getFlagTipoGaranzia().isContesto()) {
                    // COB_CODE: MOVE WGRZ-ID-GAR(IND-GRZ)    TO TGA-ID-GAR
                    //                                            WK-ID-GAR
                    ws.getTrchDiGar().setTgaIdGar(ws.getAreaBusiness().getWgrzTabGar(ws.getIxIndici().getGrz()).getLccvgrz1().getDati().getWgrzIdGar());
                    ws.setWkIdGar(TruncAbs.toInt(ws.getAreaBusiness().getWgrzTabGar(ws.getIxIndici().getGrz()).getLccvgrz1().getDati().getWgrzIdGar(), 9));
                }
                else {
                    // COB_CODE: MOVE  GRZ-ID-GAR             TO TGA-ID-GAR
                    //                                            WK-ID-GAR
                    ws.getTrchDiGar().setTgaIdGar(ws.getGar().getGrzIdGar());
                    ws.setWkIdGar(TruncAbs.toInt(ws.getGar().getGrzIdGar(), 9));
                }
                break;

            default:break;
        }
    }

    /**Original name: L800-VALORIZZA-STAT-CAUS-GRZ<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZA STATI E CAUSALI DA COMODO X GRZ
	 * ----------------------------------------------------------------*</pre>*/
    private void l800ValorizzaStatCausGrz() {
        // COB_CODE: MOVE COMODO-OPER-LOG-STAT-BUS TO LDBV1351-OPER-LOG-STAT-BUS
        ws.getLdbv1351().setLdbv1351OperLogStatBus(ws.getComodoStatCaus().getOperLogStatBus());
        // COB_CODE: MOVE COMODO-STAT-BUS-TAB      TO LDBV1351-STAT-BUS-TAB.
        ws.getLdbv1351().getLdbv1351StatBusTab().setLdbv1351StatBusTabBytes(ws.getComodoStatCaus().getStatBusTabBytes());
        // COB_CODE: MOVE COMODO-OPER-LOG-CAUS     TO LDBV1351-OPER-LOG-CAUS
        ws.getLdbv1351().setLdbv1351OperLogCaus(ws.getComodoStatCaus().getOperLogCaus());
        // COB_CODE: MOVE COMODO-TP-CAUS-TAB       TO LDBV1351-TP-CAUS-TAB.
        ws.getLdbv1351().getLdbv1351TpCausTab().setLdbv1351TpCausTabBytes(ws.getComodoStatCaus().getTpCausTabBytes());
    }

    /**Original name: L900-VALORIZZA-STAT-CAUS-TGA<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZA STATI E CAUSALI DA COMODO X TGA
	 * ----------------------------------------------------------------*</pre>*/
    private void l900ValorizzaStatCausTga() {
        // COB_CODE: INITIALIZE LDBV1361.
        initLdbv1361();
        // COB_CODE: MOVE COMODO-OPER-LOG-STAT-BUS TO LDBV1361-OPER-LOG-STAT-BUS
        ws.getLdbv1361().setLdbv1351OperLogStatBus(ws.getComodoStatCaus().getOperLogStatBus());
        // COB_CODE: MOVE COMODO-STAT-BUS-TAB      TO LDBV1361-STAT-BUS-TAB.
        ws.getLdbv1361().getLdbv1351StatBusTab().setLdbv1351StatBusTabBytes(ws.getComodoStatCaus().getStatBusTabBytes());
        // COB_CODE: MOVE COMODO-OPER-LOG-CAUS     TO LDBV1361-OPER-LOG-CAUS
        ws.getLdbv1361().setLdbv1351OperLogCaus(ws.getComodoStatCaus().getOperLogCaus());
        // COB_CODE: MOVE COMODO-TP-CAUS-TAB       TO LDBV1361-TP-CAUS-TAB.
        ws.getLdbv1361().getLdbv1351TpCausTab().setLdbv1351TpCausTabBytes(ws.getComodoStatCaus().getTpCausTabBytes());
        // COB_CODE: IF IVVC0211-DATA-ULT-TIT-INC GREATER ZERO
        //              MOVE COMODO-STAT-CAUS-LIQ  TO LDBV1361-TP-CAUS-TAB
        //           END-IF.
        if (inputIvvs0211.getIvvc0211DatiInput().getDataUltTitInc() > 0) {
            // COB_CODE: MOVE COMODO-STAT-CAUS-LIQ  TO LDBV1361-TP-CAUS-TAB
            ws.getLdbv1361().getLdbv1351TpCausTab().setLdbv1351TpCausTabBytes(ws.getComodoStatCausLiq().getComodoStatCausLiqBytes());
        }
    }

    /**Original name: M000-GESTIONE-VAR-FUN-CALC<br>
	 * <pre>----------------------------------------------------------------*
	 *     GESTIONE VARIABILI FUNZIONI DI CALCOLO
	 * ----------------------------------------------------------------*
	 * --> VALORIZZAZIONE DCLGEN</pre>*/
    private void m000GestioneVarFunCalc() {
        // COB_CODE: PERFORM M100-VAL-DCLGEN-VAR-FUNZ THRU M100-EX.
        m100ValDclgenVarFunz();
        // COB_CODE:      IF IVVC0211-SUCCESSFUL-RC
        //           *     accesso x trattato RIASS
        //                   END-IF
        //                END-IF.
        if (inputIvvs0211.getIvvc0211RestoDati().getReturnCode().isSuccessfulRc()) {
            //     accesso x trattato RIASS
            // COB_CODE: IF RIASS-SI
            //               END-IF
            //           ELSE
            //               PERFORM M020-ACCESSO-STANDARD     THRU M020-EX
            //           END-IF
            if (ws.getFlagRiass().isSi()) {
                // COB_CODE: MOVE ZEROES                     TO IND-UNZIP
                ws.getIxIndici().setUnzip(((short)0));
                // COB_CODE: MOVE WPOL-COD-PROD              TO VFC-CODPROD
                //                                           WK-CODICE-PRODOTTO-VFC
                ws.getVarFunzDiCalc().setAc5Codprod(ws.getAreaBusiness().getLccvpol1().getDati().getWpolCodProd());
                ws.setWkCodiceProdottoVfc(ws.getAreaBusiness().getLccvpol1().getDati().getWpolCodProd());
                // COB_CODE: PERFORM M020-ACCESSO-STANDARD     THRU M020-EX
                m020AccessoStandard();
                // COB_CODE: IF IVVC0211-SUCCESSFUL-RC
                //              PERFORM M010-ACCESSO-X-RIASS      THRU M010-EX
                //           END-IF
                if (inputIvvs0211.getIvvc0211RestoDati().getReturnCode().isSuccessfulRc()) {
                    // COB_CODE: MOVE IVVC0211-CODICE-TRATTATO TO AC5-CODPROD
                    //                                        WK-CODICE-PRODOTTO-VFC
                    ws.getVarFunzDiCalcR().setAc5Codprod(inputIvvs0211.getIvvc0211RestoDati().getCodiceTrattato());
                    ws.setWkCodiceProdottoVfc(inputIvvs0211.getIvvc0211RestoDati().getCodiceTrattato());
                    // COB_CODE: PERFORM M010-ACCESSO-X-RIASS      THRU M010-EX
                    m010AccessoXRiass();
                }
            }
            else {
                // COB_CODE: PERFORM M020-ACCESSO-STANDARD     THRU M020-EX
                m020AccessoStandard();
            }
        }
    }

    /**Original name: M010-ACCESSO-X-RIASS<br>
	 * <pre>----------------------------------------------------------------*
	 *     ACCESSO ALLA TABELLA VAR_FUNZ_DI_CALC_R
	 * ----------------------------------------------------------------*
	 * --> LIVELLO OPERAZIONE</pre>*/
    private void m010AccessoXRiass() {
        Ldbs8800 ldbs8800 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: SET  IDSV0003-WHERE-CONDITION TO TRUE.
        ws.getIdsv0003().getLivelloOperazione().setWhereCondition();
        //--> TIPO OPERAZIONE
        // COB_CODE: SET  IDSV0003-SELECT         TO TRUE
        ws.getIdsv0003().getOperazione().setSelect();
        //--> TRATTAMENTO-STORICITA
        // COB_CODE: SET IDSV0003-TRATT-SENZA-STOR TO TRUE
        ws.getIdsv0003().getTrattamentoStoricita().setTrattSenzaStor();
        //--> INIZIALIZZA CODICE DI RITORNO
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC    TO TRUE
        ws.getIdsv0003().getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE
        ws.getIdsv0003().getSqlcode().setSuccessfulSql();
        // COB_CODE: CALL PGM-LDBS8800           USING IDSV0003
        //                                             VAR-FUNZ-DI-CALC-R
        ldbs8800 = Ldbs8800.getInstance();
        ldbs8800.run(ws.getIdsv0003(), ws.getVarFunzDiCalcR());
        // COB_CODE:         IF IDSV0003-SUCCESSFUL-RC
        //                       END-EVALUATE
        //                   ELSE
        //           *-->     GESTIRE ERRORE DISPATCHER
        //                       END-STRING
        //                   END-IF.
        if (ws.getIdsv0003().getReturnCode().isSuccessfulRc()) {
            // COB_CODE:             EVALUATE TRUE
            //                           WHEN IDSV0003-SUCCESSFUL-SQL
            //           *-->         OPERAZIONE ESEGUITA CORRETTAMENTE
            //                                  THRU M510-EX
            //                           WHEN IDSV0003-NOT-FOUND
            //           *--->        CAMPO $ NON TROVATO
            //                              END-IF
            //                           WHEN OTHER
            //           *--->        ERRORE DI ACCESSO AL DB
            //                             END-STRING
            //                       END-EVALUATE
            switch (ws.getIdsv0003().getSqlcode().getSqlcode()) {

                case Idsv0003Sqlcode.SUCCESSFUL_SQL://-->         OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: PERFORM M510-VALORIZZA-VARIABILI-RIASS
                    //              THRU M510-EX
                    m510ValorizzaVariabiliRiass();
                    break;

                case Idsv0003Sqlcode.NOT_FOUND://--->        CAMPO $ NON TROVATO
                    // COB_CODE: IF NOT TIPO-OPZIONI
                    //              END-IF
                    //           END-IF
                    if (!ws.getFlagTipologiaScheda().isOpzioni()) {
                        // COB_CODE: IF IVVC0211-IN-CONV
                        //              END-IF
                        //           END-IF
                        if (inputIvvs0211.getIvvc0211DatiInput().getStepElab().isInConv()) {
                            // COB_CODE: IF SKEDE-TOTALI AND GESTIONE-GARANZIE
                            //              CONTINUE
                            //           ELSE
                            //                           TO IVVC0211-NOME-TABELLA
                            //           END-IF
                            if (ws.getFlagSkede().isTotali() && ws.getFlagGestione().isGaranzie()) {
                            // COB_CODE: CONTINUE
                            //continue
                            }
                            else {
                                // COB_CODE: SET FINE-ELAB-SI           TO TRUE
                                ws.getFlagFineElab().setSi();
                                // COB_CODE: SET IVVC0211-GENERIC-ERROR TO TRUE
                                inputIvvs0211.getIvvc0211RestoDati().getReturnCode().setGenericError();
                                // COB_CODE: MOVE PGM-LDBS8800
                                //                  TO IVVC0211-COD-SERVIZIO-BE
                                inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setCodServizioBe(ws.getPgmLdbs8800());
                                // COB_CODE: STRING 'OCCORRENZA NON TROVATA SU AC5'
                                //                  ' - '
                                //                  'COMP : '
                                //                  WK-CODICE-COMPAGNIA
                                //                  ' - '
                                //                  'PROD : '
                                //                  WK-CODICE-PRODOTTO-VFC
                                //                  ' - '
                                //                  'TARI : '
                                //                  WK-CODICE-TARIFFA-AC5
                                //                  ' -  '
                                //                  'DT INI : '
                                //                  WK-DATA-INIZIO-VFC
                                //                  ' -  '
                                //                  'FUNZ : '
                                //                  WK-NOME-FUNZIONE-VFC
                                //                  ' -  '
                                //                  'STEP ELAB. : '
                                //                  WK-STEP-ELAB-VFC
                                //                  DELIMITED BY SIZE INTO
                                //                  IVVC0211-DESCRIZ-ERR
                                //           END-STRING
                                concatUtil = ConcatUtil.buildString(Ivvc0211CampiEsito.Len.DESCRIZ_ERR, new String[] {"OCCORRENZA NON TROVATA SU AC5", " - ", "COMP : ", ws.getWkCodiceCompagniaAsString(), " - ", "PROD : ", ws.getWkCodiceProdottoVfcFormatted(), " - ", "TARI : ", ws.getWkCodiceTariffaAc5Formatted(), " -  ", "DT INI : ", ws.getWkDataInizioVfcAsString(), " -  ", "FUNZ : ", ws.getWkNomeFunzioneVfcFormatted(), " -  ", "STEP ELAB. : ", ws.getWkStepElabVfcFormatted()});
                                inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setDescrizErr(concatUtil.replaceInString(inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().getDescrizErrFormatted()));
                                // COB_CODE: MOVE 'VAR_FUNZ_DI_CALC_R'
                                //                        TO IVVC0211-NOME-TABELLA
                                inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setNomeTabella("VAR_FUNZ_DI_CALC_R");
                            }
                        }
                    }
                    break;

                default://--->        ERRORE DI ACCESSO AL DB
                    // COB_CODE: SET FINE-ELAB-SI           TO TRUE
                    ws.getFlagFineElab().setSi();
                    // COB_CODE: SET IVVC0211-SQL-ERROR     TO TRUE
                    inputIvvs0211.getIvvc0211RestoDati().getReturnCode().setSqlError();
                    // COB_CODE: MOVE PGM-LDBS8800
                    //                        TO IVVC0211-COD-SERVIZIO-BE
                    inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setCodServizioBe(ws.getPgmLdbs8800());
                    // COB_CODE: MOVE IDSV0003-SQLCODE TO WS-SQLCODE
                    ws.setWsSqlcode(ws.getIdsv0003().getSqlcode().getSqlcode());
                    // COB_CODE: STRING PGM-LDBS8800
                    //                    ' - '
                    //                    'COMP : '
                    //                    WK-CODICE-COMPAGNIA
                    //                    ' - '
                    //                    'PROD : '
                    //                    WK-CODICE-PRODOTTO-VFC
                    //                    ' -  '
                    //                    'TARI : '
                    //                    WK-CODICE-TARIFFA-AC5
                    //                    ' -  '
                    //                    'DT INI : '
                    //                    WK-DATA-INIZIO-VFC
                    //                    ' -  '
                    //                    'FUNZ : '
                    //                    WK-NOME-FUNZIONE-VFC
                    //                    ' -  '
                    //                    'STEP ELAB. : '
                    //                    WK-STEP-ELAB-VFC
                    //                    ' - '
                    //                    'RC : '
                    //                    IDSV0003-RETURN-CODE
                    //                    ' - '
                    //                    'SQLCODE : '
                    //                    WS-SQLCODE
                    //                    DELIMITED BY SIZE INTO
                    //                    IVVC0211-DESCRIZ-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ivvc0211CampiEsito.Len.DESCRIZ_ERR, new String[] {ws.getPgmLdbs8800Formatted(), " - ", "COMP : ", ws.getWkCodiceCompagniaAsString(), " - ", "PROD : ", ws.getWkCodiceProdottoVfcFormatted(), " -  ", "TARI : ", ws.getWkCodiceTariffaAc5Formatted(), " -  ", "DT INI : ", ws.getWkDataInizioVfcAsString(), " -  ", "FUNZ : ", ws.getWkNomeFunzioneVfcFormatted(), " -  ", "STEP ELAB. : ", ws.getWkStepElabVfcFormatted(), " - ", "RC : ", ws.getIdsv0003().getReturnCode().getReturnCodeFormatted(), " - ", "SQLCODE : ", ws.getWsSqlcodeAsString()});
                    inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setDescrizErr(concatUtil.replaceInString(inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().getDescrizErrFormatted()));
                    break;
            }
        }
        else {
            //-->     GESTIRE ERRORE DISPATCHER
            // COB_CODE: SET IVVC0211-SQL-ERROR  TO TRUE
            inputIvvs0211.getIvvc0211RestoDati().getReturnCode().setSqlError();
            // COB_CODE: MOVE PGM-LDBS8800       TO IVVC0211-COD-SERVIZIO-BE
            inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setCodServizioBe(ws.getPgmLdbs8800());
            // COB_CODE: STRING PGM-LDBS8800
            //                  ' - '
            //                  'RC : '
            //                  IDSV0003-RETURN-CODE
            //                  DELIMITED BY SIZE INTO
            //                  IVVC0211-DESCRIZ-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ivvc0211CampiEsito.Len.DESCRIZ_ERR, ws.getPgmLdbs8800Formatted(), " - ", "RC : ", ws.getIdsv0003().getReturnCode().getReturnCodeFormatted());
            inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setDescrizErr(concatUtil.replaceInString(inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().getDescrizErrFormatted()));
        }
    }

    /**Original name: M020-ACCESSO-STANDARD<br>
	 * <pre>----------------------------------------------------------------*
	 *     ACCESSO ALLA TABELLA VAR_FUNZ_DI_CALC
	 * ----------------------------------------------------------------*
	 * --> LIVELLO OPERAZIONE</pre>*/
    private void m020AccessoStandard() {
        Ldbs0270 ldbs0270 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: SET  IDSV0003-PRIMARY-KEY    TO TRUE
        ws.getIdsv0003().getLivelloOperazione().setPrimaryKey();
        //--> TIPO OPERAZIONE
        // COB_CODE: SET  IDSV0003-SELECT         TO TRUE
        ws.getIdsv0003().getOperazione().setSelect();
        //--> TRATTAMENTO-STORICITA
        // COB_CODE: SET IDSV0003-TRATT-SENZA-STOR TO TRUE
        ws.getIdsv0003().getTrattamentoStoricita().setTrattSenzaStor();
        //--> INIZIALIZZA CODICE DI RITORNO
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC    TO TRUE
        ws.getIdsv0003().getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE
        ws.getIdsv0003().getSqlcode().setSuccessfulSql();
        // COB_CODE: CALL PGM-LDBS0270           USING IDSV0003
        //                                             VAR-FUNZ-DI-CALC
        ldbs0270 = Ldbs0270.getInstance();
        ldbs0270.run(ws.getIdsv0003(), ws.getVarFunzDiCalc());
        // COB_CODE:         IF IDSV0003-SUCCESSFUL-RC
        //                       END-EVALUATE
        //                   ELSE
        //           *-->     GESTIRE ERRORE DISPATCHER
        //                       END-STRING
        //                   END-IF.
        if (ws.getIdsv0003().getReturnCode().isSuccessfulRc()) {
            // COB_CODE:             EVALUATE TRUE
            //                           WHEN IDSV0003-SUCCESSFUL-SQL
            //           *-->         OPERAZIONE ESEGUITA CORRETTAMENTE
            //                               PERFORM M500-VALORIZZA-VARIABILI THRU M500-EX
            //                           WHEN IDSV0003-NOT-FOUND
            //           *--->        CAMPO $ NON TROVATO
            //                              END-IF
            //                           WHEN OTHER
            //           *--->        ERRORE DI ACCESSO AL DB
            //                             END-STRING
            //                       END-EVALUATE
            switch (ws.getIdsv0003().getSqlcode().getSqlcode()) {

                case Idsv0003Sqlcode.SUCCESSFUL_SQL://-->         OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: SET WK-VAR-FUNZ-TROVATA-SI TO TRUE
                    ws.getWkVarFunzTrovata().setSi();
                    // COB_CODE: IF GESTIONE-PRODOTTO
                    //                TO IND-LIVELLO
                    //           ELSE
                    //                TO IND-LIVELLO
                    //           END-IF
                    if (ws.getFlagGestione().isProdotto()) {
                        // COB_CODE: ADD 1 TO IVVC0216-ELE-LIVELLO-MAX-P
                        ws.getAreaWorkIvvc0216().setC216EleLivelloMaxP(Trunc.toShort(1 + ws.getAreaWorkIvvc0216().getC216EleLivelloMaxP(), 4));
                        // COB_CODE: MOVE IVVC0216-ELE-LIVELLO-MAX-P
                        //             TO IND-LIVELLO
                        ws.getIxIndici().setLivello(ws.getAreaWorkIvvc0216().getC216EleLivelloMaxP());
                    }
                    else {
                        // COB_CODE: ADD 1 TO IVVC0216-ELE-LIVELLO-MAX-T
                        ws.getAreaWorkIvvc0216().setC216EleLivelloMaxT(Trunc.toShort(1 + ws.getAreaWorkIvvc0216().getC216EleLivelloMaxT(), 4));
                        // COB_CODE: MOVE IVVC0216-ELE-LIVELLO-MAX-T
                        //             TO IND-LIVELLO
                        ws.getIxIndici().setLivello(ws.getAreaWorkIvvc0216().getC216EleLivelloMaxT());
                    }
                    // COB_CODE: PERFORM M500-VALORIZZA-VARIABILI THRU M500-EX
                    m500ValorizzaVariabili();
                    break;

                case Idsv0003Sqlcode.NOT_FOUND://--->        CAMPO $ NON TROVATO
                    // COB_CODE: IF NOT TIPO-OPZIONI
                    //              END-IF
                    //           END-IF
                    if (!ws.getFlagTipologiaScheda().isOpzioni()) {
                        // COB_CODE: IF IVVC0211-IN-CONV
                        //              END-IF
                        //           END-IF
                        if (inputIvvs0211.getIvvc0211DatiInput().getStepElab().isInConv()) {
                            // COB_CODE: IF SKEDE-TOTALI AND GESTIONE-GARANZIE
                            //              CONTINUE
                            //           ELSE
                            //                           TO IVVC0211-NOME-TABELLA
                            //           END-IF
                            if (ws.getFlagSkede().isTotali() && ws.getFlagGestione().isGaranzie()) {
                            // COB_CODE: CONTINUE
                            //continue
                            }
                            else {
                                // COB_CODE: SET FINE-ELAB-SI           TO TRUE
                                ws.getFlagFineElab().setSi();
                                // COB_CODE: SET IVVC0211-GENERIC-ERROR TO TRUE
                                inputIvvs0211.getIvvc0211RestoDati().getReturnCode().setGenericError();
                                // COB_CODE: MOVE PGM-LDBS0270
                                //                  TO IVVC0211-COD-SERVIZIO-BE
                                inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setCodServizioBe(ws.getPgmLdbs0270());
                                // COB_CODE: STRING 'OCCORRENZA NON TROVATA SU VFC'
                                //                  ' - '
                                //                  'COMP : '
                                //                  WK-CODICE-COMPAGNIA
                                //                  ' - '
                                //                  'PROD : '
                                //                  WK-CODICE-PRODOTTO-VFC
                                //                  ' - '
                                //                  'TARI : '
                                //                  WK-CODICE-TARIFFA-VFC
                                //                  ' -  '
                                //                  'DT INI : '
                                //                  WK-DATA-INIZIO-VFC
                                //                  ' -  '
                                //                  'FUNZ : '
                                //                  WK-NOME-FUNZIONE-VFC
                                //                  ' -  '
                                //                  'STEP ELAB. : '
                                //                  WK-STEP-ELAB-VFC
                                //                  DELIMITED BY SIZE INTO
                                //                  IVVC0211-DESCRIZ-ERR
                                //           END-STRING
                                concatUtil = ConcatUtil.buildString(Ivvc0211CampiEsito.Len.DESCRIZ_ERR, new String[] {"OCCORRENZA NON TROVATA SU VFC", " - ", "COMP : ", ws.getWkCodiceCompagniaAsString(), " - ", "PROD : ", ws.getWkCodiceProdottoVfcFormatted(), " - ", "TARI : ", ws.getWkCodiceTariffaVfcFormatted(), " -  ", "DT INI : ", ws.getWkDataInizioVfcAsString(), " -  ", "FUNZ : ", ws.getWkNomeFunzioneVfcFormatted(), " -  ", "STEP ELAB. : ", ws.getWkStepElabVfcFormatted()});
                                inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setDescrizErr(concatUtil.replaceInString(inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().getDescrizErrFormatted()));
                                // COB_CODE: MOVE 'VAR_FUNZ_DI_CALC'
                                //                        TO IVVC0211-NOME-TABELLA
                                inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setNomeTabella("VAR_FUNZ_DI_CALC");
                            }
                        }
                    }
                    break;

                default://--->        ERRORE DI ACCESSO AL DB
                    // COB_CODE: SET FINE-ELAB-SI           TO TRUE
                    ws.getFlagFineElab().setSi();
                    // COB_CODE: SET IVVC0211-SQL-ERROR     TO TRUE
                    inputIvvs0211.getIvvc0211RestoDati().getReturnCode().setSqlError();
                    // COB_CODE: MOVE PGM-LDBS0270
                    //                        TO IVVC0211-COD-SERVIZIO-BE
                    inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setCodServizioBe(ws.getPgmLdbs0270());
                    // COB_CODE: MOVE IDSV0003-SQLCODE TO WS-SQLCODE
                    ws.setWsSqlcode(ws.getIdsv0003().getSqlcode().getSqlcode());
                    // COB_CODE: STRING PGM-LDBS0270
                    //                    ' - '
                    //                    'COMP : '
                    //                    WK-CODICE-COMPAGNIA
                    //                    ' - '
                    //                    'PROD : '
                    //                    WK-CODICE-PRODOTTO-VFC
                    //                    ' -  '
                    //                    'TARI : '
                    //                    WK-CODICE-TARIFFA-VFC
                    //                    ' -  '
                    //                    'DT INI : '
                    //                    WK-DATA-INIZIO-VFC
                    //                    ' -  '
                    //                    'FUNZ : '
                    //                    WK-NOME-FUNZIONE-VFC
                    //                    ' -  '
                    //                    'STEP ELAB. : '
                    //                    WK-STEP-ELAB-VFC
                    //                    ' - '
                    //                    'RC : '
                    //                    IDSV0003-RETURN-CODE
                    //                    ' - '
                    //                    'SQLCODE : '
                    //                    WS-SQLCODE
                    //                    DELIMITED BY SIZE INTO
                    //                    IVVC0211-DESCRIZ-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ivvc0211CampiEsito.Len.DESCRIZ_ERR, new String[] {ws.getPgmLdbs0270Formatted(), " - ", "COMP : ", ws.getWkCodiceCompagniaAsString(), " - ", "PROD : ", ws.getWkCodiceProdottoVfcFormatted(), " -  ", "TARI : ", ws.getWkCodiceTariffaVfcFormatted(), " -  ", "DT INI : ", ws.getWkDataInizioVfcAsString(), " -  ", "FUNZ : ", ws.getWkNomeFunzioneVfcFormatted(), " -  ", "STEP ELAB. : ", ws.getWkStepElabVfcFormatted(), " - ", "RC : ", ws.getIdsv0003().getReturnCode().getReturnCodeFormatted(), " - ", "SQLCODE : ", ws.getWsSqlcodeAsString()});
                    inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setDescrizErr(concatUtil.replaceInString(inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().getDescrizErrFormatted()));
                    break;
            }
        }
        else {
            //-->     GESTIRE ERRORE DISPATCHER
            // COB_CODE: SET IVVC0211-SQL-ERROR  TO TRUE
            inputIvvs0211.getIvvc0211RestoDati().getReturnCode().setSqlError();
            // COB_CODE: MOVE PGM-LDBS0270       TO IVVC0211-COD-SERVIZIO-BE
            inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setCodServizioBe(ws.getPgmLdbs0270());
            // COB_CODE: STRING PGM-LDBS0270
            //                  ' - '
            //                  'RC : '
            //                  IDSV0003-RETURN-CODE
            //                  DELIMITED BY SIZE INTO
            //                  IVVC0211-DESCRIZ-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ivvc0211CampiEsito.Len.DESCRIZ_ERR, ws.getPgmLdbs0270Formatted(), " - ", "RC : ", ws.getIdsv0003().getReturnCode().getReturnCodeFormatted());
            inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setDescrizErr(concatUtil.replaceInString(inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().getDescrizErrFormatted()));
        }
    }

    /**Original name: M100-VAL-DCLGEN-VAR-FUNZ<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZA DCLGEN TABELLA VARIABILI FUNZIONI DI CALCOLO
	 * ----------------------------------------------------------------*</pre>*/
    private void m100ValDclgenVarFunz() {
        // COB_CODE: INITIALIZE                        VAR-FUNZ-DI-CALC
        //                                             VAR-FUNZ-DI-CALC-R.
        initVarFunzDiCalc();
        initVarFunzDiCalcR();
        // COB_CODE: MOVE IVVC0211-COD-COMPAGNIA-ANIA   TO VFC-IDCOMP
        //                                                 AC5-IDCOMP.
        ws.getVarFunzDiCalc().setAc5Idcomp(((short)(inputIvvs0211.getIvvc0211DatiInput().getCodCompagniaAnia())));
        ws.getVarFunzDiCalcR().setAc5Idcomp(((short)(inputIvvs0211.getIvvc0211DatiInput().getCodCompagniaAnia())));
        // COB_CODE: MOVE SPACES                        TO VFC-CODTARI
        //                                                 AC5-CODTARI
        //                                                 WK-CODICE-TARIFFA-AC5
        //                                                 WK-CODICE-TARIFFA-VFC.
        ws.getVarFunzDiCalc().setAc5Codtari("");
        ws.getVarFunzDiCalcR().setAc5Codtari("");
        ws.setWkCodiceTariffaAc5("");
        ws.setWkCodiceTariffaVfc("");
        // COB_CODE: MOVE IVVC0211-STEP-ELAB            TO VFC-STEP-ELAB
        //                                                 AC5-STEP-ELAB
        //                                                 WK-STEP-ELAB-VFC
        ws.getVarFunzDiCalc().setAc5StepElab(String.valueOf(inputIvvs0211.getIvvc0211DatiInput().getStepElab().getStepElab()));
        ws.getVarFunzDiCalcR().setAc5StepElab(String.valueOf(inputIvvs0211.getIvvc0211DatiInput().getStepElab().getStepElab()));
        ws.setWkStepElabVfc(String.valueOf(inputIvvs0211.getIvvc0211DatiInput().getStepElab().getStepElab()));
        // COB_CODE: EVALUATE TRUE
        //              WHEN TIPO-STANDARD
        //                 PERFORM M120-CNTL-VAL-DCLGEN-STD     THRU M120-EX
        //              WHEN TIPO-OPZIONI
        //                 PERFORM M130-CNTL-VAL-DCLGEN-OPZ      THRU M130-EX
        //           END-EVALUATE.
        switch (ws.getFlagTipologiaScheda().getFlagTipologiaScheda()) {

            case FlagTipologiaScheda.STANDARD:// COB_CODE: IF LCCV0021-TP-MOV-ACT = SPACES OR LOW-VALUE
                //                                    OR HIGH-VALUE
                //                                    OR ZEROES
                //                                            WK-NOME-FUNZIONE-VFC
                //           ELSE
                //                                            WK-NOME-FUNZIONE-VFC
                //           END-IF
                if (Characters.EQ_SPACE.test(ws.getLccv0021().getAreaOutput().getLccv0021TpMovActFormatted()) || Characters.EQ_LOW.test(ws.getLccv0021().getAreaOutput().getLccv0021TpMovActFormatted()) || Characters.EQ_HIGH.test(ws.getLccv0021().getAreaOutput().getLccv0021TpMovActFormatted()) || Characters.EQ_ZERO.test(ws.getLccv0021().getAreaOutput().getLccv0021TpMovActFormatted())) {
                    // COB_CODE: MOVE IVVC0211-TIPO-MOVIMENTO(2:4) TO VFC-NOMEFUNZ
                    //                                                AC5-NOMEFUNZ
                    //                                         WK-NOME-FUNZIONE-VFC
                    ws.getVarFunzDiCalc().setAc5Nomefunz(inputIvvs0211.getIvvc0211DatiInput().getTipoMovimentoFormatted().substring((2) - 1, 5));
                    ws.getVarFunzDiCalcR().setAc5Nomefunz(inputIvvs0211.getIvvc0211DatiInput().getTipoMovimentoFormatted().substring((2) - 1, 5));
                    ws.setWkNomeFunzioneVfc(inputIvvs0211.getIvvc0211DatiInput().getTipoMovimentoFormatted().substring((2) - 1, 5));
                }
                else {
                    // COB_CODE: MOVE LCCV0021-TP-MOV-ACT(2:4) TO VFC-NOMEFUNZ
                    //                                                AC5-NOMEFUNZ
                    //                                         WK-NOME-FUNZIONE-VFC
                    ws.getVarFunzDiCalc().setAc5Nomefunz(ws.getLccv0021().getAreaOutput().getLccv0021TpMovActFormatted().substring((2) - 1, 5));
                    ws.getVarFunzDiCalcR().setAc5Nomefunz(ws.getLccv0021().getAreaOutput().getLccv0021TpMovActFormatted().substring((2) - 1, 5));
                    ws.setWkNomeFunzioneVfc(ws.getLccv0021().getAreaOutput().getLccv0021TpMovActFormatted().substring((2) - 1, 5));
                }
                // COB_CODE: PERFORM M120-CNTL-VAL-DCLGEN-STD     THRU M120-EX
                m120CntlValDclgenStd();
                break;

            case FlagTipologiaScheda.OPZIONI:// COB_CODE: MOVE WOPZ-COD-TIPO-OPZIONE(IND-TIPO-OPZ)
                //                                           TO VFC-NOMEFUNZ
                //                                              AC5-NOMEFUNZ
                //                                              WK-NOME-FUNZIONE-VFC
                ws.getVarFunzDiCalc().setAc5Nomefunz(ws.getAreaBusiness().getIvvc0217().getOpzioni(ws.getIxIndici().getTipoOpz()).getCodTipoOpzione());
                ws.getVarFunzDiCalcR().setAc5Nomefunz(ws.getAreaBusiness().getIvvc0217().getOpzioni(ws.getIxIndici().getTipoOpz()).getCodTipoOpzione());
                ws.setWkNomeFunzioneVfc(ws.getAreaBusiness().getIvvc0217().getOpzioni(ws.getIxIndici().getTipoOpz()).getCodTipoOpzione());
                // COB_CODE: PERFORM M130-CNTL-VAL-DCLGEN-OPZ      THRU M130-EX
                m130CntlValDclgenOpz();
                break;

            default:break;
        }
        //
        // COB_CODE:      IF  IVVC0211-CODICE-INIZIATIVA > SPACES AND
        //                    IVVC0211-CODICE-INIZIATIVA NOT = HIGH-VALUE
        //           *   valorizzazione dati accesso alla tab. VAR_FUNZ_DI_CALC
        //           *   in caso di iniziativa commerciale
        //                                                       WK-CODICE-PRODOTTO-VFC
        //                ELSE
        //           *   Accesso alla tab. VAR_FUNZ_DI_CALC_R x codice trattato
        //           *   in caso di esplosione e ricalcolo RIASS
        //                    END-IF
        //                END-IF.
        if (Characters.GT_SPACE.test(inputIvvs0211.getIvvc0211RestoDati().getCodiceIniziativa()) && !Characters.EQ_HIGH.test(inputIvvs0211.getIvvc0211RestoDati().getCodiceIniziativaFormatted())) {
            //   valorizzazione dati accesso alla tab. VAR_FUNZ_DI_CALC
            //   in caso di iniziativa commerciale
            // COB_CODE: MOVE IVVC0211-CODICE-INIZIATIVA TO VFC-CODPROD
            //                                              WK-CODICE-PRODOTTO-VFC
            ws.getVarFunzDiCalc().setAc5Codprod(inputIvvs0211.getIvvc0211RestoDati().getCodiceIniziativa());
            ws.setWkCodiceProdottoVfc(inputIvvs0211.getIvvc0211RestoDati().getCodiceIniziativa());
        }
        else if (Characters.GT_SPACE.test(inputIvvs0211.getIvvc0211RestoDati().getCodiceTrattato()) && !Characters.EQ_HIGH.test(inputIvvs0211.getIvvc0211RestoDati().getCodiceTrattatoFormatted())) {
        //   Accesso alla tab. VAR_FUNZ_DI_CALC_R x codice trattato
        //   in caso di esplosione e ricalcolo RIASS
        // COB_CODE: IF  IVVC0211-CODICE-TRATTATO > SPACES
        //           AND IVVC0211-CODICE-TRATTATO NOT = HIGH-VALUE
        //               CONTINUE
        //           ELSE
        //                                              WK-CODICE-PRODOTTO-VFC
        //           END-IF
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE WPOL-COD-PROD            TO VFC-CODPROD
            //                                          WK-CODICE-PRODOTTO-VFC
            ws.getVarFunzDiCalc().setAc5Codprod(ws.getAreaBusiness().getLccvpol1().getDati().getWpolCodProd());
            ws.setWkCodiceProdottoVfc(ws.getAreaBusiness().getLccvpol1().getDati().getWpolCodProd());
        }
    }

    /**Original name: M101-VAL-DCLGEN-VAR-FUNZ-PROD<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZA DCLGEN TABELLA VARIABILI FUNZIONI DI CALCOLO
	 *     --- GESTIONE PRODOTTO ---
	 * ----------------------------------------------------------------*</pre>*/
    private void m101ValDclgenVarFunzProd() {
        // COB_CODE:      IF  IVVC0211-CODICE-INIZIATIVA > SPACES AND
        //                    IVVC0211-CODICE-INIZIATIVA NOT = HIGH-VALUE
        //           *   valorizzazione dati accesso alla tab. VAR_FUNZ_DI_CALC
        //           *   in caso di iniziativa commerciale
        //                                                     WK-CODICE-TARIFFA-VFC
        //                ELSE
        //           *   Accesso alla tab. VAR_FUNZ_DI_CALC_R x codice trattato
        //           *   in caso di esplosione e ricalcolo RIASS
        //                    END-IF
        //                END-IF.
        if (Characters.GT_SPACE.test(inputIvvs0211.getIvvc0211RestoDati().getCodiceIniziativa()) && !Characters.EQ_HIGH.test(inputIvvs0211.getIvvc0211RestoDati().getCodiceIniziativaFormatted())) {
            //   valorizzazione dati accesso alla tab. VAR_FUNZ_DI_CALC
            //   in caso di iniziativa commerciale
            // COB_CODE: MOVE 'P'                      TO VFC-CODTARI
            //                                            WK-CODICE-TARIFFA-VFC
            ws.getVarFunzDiCalc().setAc5Codtari("P");
            ws.setWkCodiceTariffaVfc("P");
        }
        else if (Characters.GT_SPACE.test(inputIvvs0211.getIvvc0211RestoDati().getCodiceTrattato()) && !Characters.EQ_HIGH.test(inputIvvs0211.getIvvc0211RestoDati().getCodiceTrattatoFormatted())) {
            //   Accesso alla tab. VAR_FUNZ_DI_CALC_R x codice trattato
            //   in caso di esplosione e ricalcolo RIASS
            // COB_CODE: IF  IVVC0211-CODICE-TRATTATO > SPACES
            //           AND IVVC0211-CODICE-TRATTATO NOT = HIGH-VALUE
            //                                       WK-CODICE-TARIFFA-AC5
            //           END-IF
            // COB_CODE: MOVE 'P'             TO AC5-CODTARI
            //                                   WK-CODICE-TARIFFA-AC5
            ws.getVarFunzDiCalcR().setAc5Codtari("P");
            ws.setWkCodiceTariffaAc5("P");
        }
        // COB_CODE: PERFORM M150-VERIFICA-VERS-PROD-GP  THRU M150-EX.
        m150VerificaVersProdGp();
    }

    /**Original name: M102-VAL-DCLGEN-VAR-FUNZ-GAR<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZA DCLGEN TABELLA VARIABILI FUNZIONI DI CALCOLO
	 *     --- GESTIONE GARANZIE ---
	 * ----------------------------------------------------------------*</pre>*/
    private void m102ValDclgenVarFunzGar() {
        // COB_CODE:      IF  IVVC0211-CODICE-INIZIATIVA > SPACES AND
        //                    IVVC0211-CODICE-INIZIATIVA NOT = HIGH-VALUE
        //           *   valorizzazione dati accesso alla tab. VAR_FUNZ_DI_CALC
        //           *   in caso di iniziativa commerciale
        //                                                     WK-CODICE-TARIFFA-VFC
        //                ELSE
        //           *   Accesso alla tab. VAR_FUNZ_DI_CALC_R x codice trattato
        //           *   in caso di esplosione e ricalcolo RIASS
        //                    END-IF
        //                END-IF.
        if (Characters.GT_SPACE.test(inputIvvs0211.getIvvc0211RestoDati().getCodiceIniziativa()) && !Characters.EQ_HIGH.test(inputIvvs0211.getIvvc0211RestoDati().getCodiceIniziativaFormatted())) {
            //   valorizzazione dati accesso alla tab. VAR_FUNZ_DI_CALC
            //   in caso di iniziativa commerciale
            // COB_CODE: MOVE 'G'                      TO VFC-CODTARI
            //                                            WK-CODICE-TARIFFA-VFC
            ws.getVarFunzDiCalc().setAc5Codtari("G");
            ws.setWkCodiceTariffaVfc("G");
        }
        else if (Characters.GT_SPACE.test(inputIvvs0211.getIvvc0211RestoDati().getCodiceTrattato()) && !Characters.EQ_HIGH.test(inputIvvs0211.getIvvc0211RestoDati().getCodiceTrattatoFormatted())) {
            //   Accesso alla tab. VAR_FUNZ_DI_CALC_R x codice trattato
            //   in caso di esplosione e ricalcolo RIASS
            // COB_CODE:          IF  IVVC0211-CODICE-TRATTATO > SPACES
            //                    AND IVVC0211-CODICE-TRATTATO NOT = HIGH-VALUE
            //                                                     WK-CODICE-TARIFFA-VFC
            //           * MOD20110729
            //                    ELSE
            //                        END-EVALUATE
            //                    END-IF
            // COB_CODE: MOVE 'G'             TO AC5-CODTARI
            //                                   WK-CODICE-TARIFFA-AC5
            ws.getVarFunzDiCalcR().setAc5Codtari("G");
            ws.setWkCodiceTariffaAc5("G");
            // MOD20110729
            // COB_CODE: MOVE WGRZ-COD-TARI(IND-GRZ) TO VFC-CODTARI
            //                                        WK-CODICE-TARIFFA-VFC
            ws.getVarFunzDiCalc().setAc5Codtari(ws.getAreaBusiness().getWgrzTabGar(ws.getIxIndici().getGrz()).getLccvgrz1().getDati().getWgrzCodTari());
            ws.setWkCodiceTariffaVfc(ws.getAreaBusiness().getWgrzTabGar(ws.getIxIndici().getGrz()).getLccvgrz1().getDati().getWgrzCodTari());
            // MOD20110729
        }
        else {
            // COB_CODE: EVALUATE TRUE
            //              WHEN GARANZIA-CONTESTO
            //                                           WK-CODICE-TARIFFA-VFC
            //                                           WK-CODICE-TARIFFA-VFC
            //              WHEN GARANZIA-DB
            //                                           WK-CODICE-TARIFFA-VFC
            //                                           WK-CODICE-TARIFFA-VFC
            //           END-EVALUATE
            switch (ws.getFlagTipoGaranzia().getFlagTipoGaranzia()) {

                case FlagTipoGaranzia.CONTESTO:// COB_CODE: MOVE WGRZ-COD-TARI(IND-GRZ) TO VFC-CODTARI
                    //                                    WK-CODICE-TARIFFA-VFC
                    ws.getVarFunzDiCalc().setAc5Codtari(ws.getAreaBusiness().getWgrzTabGar(ws.getIxIndici().getGrz()).getLccvgrz1().getDati().getWgrzCodTari());
                    ws.setWkCodiceTariffaVfc(ws.getAreaBusiness().getWgrzTabGar(ws.getIxIndici().getGrz()).getLccvgrz1().getDati().getWgrzCodTari());
                    break;

                case FlagTipoGaranzia.DB:// COB_CODE: MOVE GRZ-COD-TARI           TO VFC-CODTARI
                    //                                    WK-CODICE-TARIFFA-VFC
                    ws.getVarFunzDiCalc().setAc5Codtari(ws.getGar().getGrzCodTari());
                    ws.setWkCodiceTariffaVfc(ws.getGar().getGrzCodTari());
                    break;

                default:break;
            }
        }
        // COB_CODE: IF WGRZ-ST-ADD(IND-GRZ)
        //              PERFORM M160-VERIFICA-VERS-PROD-GG THRU M160-EX
        //           ELSE
        //              PERFORM M152-VAL-DT-VLDT-PROD   THRU M152-EX
        //           END-IF.
        if (ws.getAreaBusiness().getWgrzTabGar(ws.getIxIndici().getGrz()).getLccvgrz1().getStatus().isAdd()) {
            // COB_CODE: PERFORM M160-VERIFICA-VERS-PROD-GG THRU M160-EX
            m160VerificaVersProdGg();
        }
        else {
            // COB_CODE: PERFORM M152-VAL-DT-VLDT-PROD   THRU M152-EX
            m152ValDtVldtProd();
        }
    }

    /**Original name: M103-VAL-DCLGEN-VAR-FUNZ-OPZ<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZA DCLGEN TABELLA VARIABILI FUNZIONI DI CALCOLO
	 *     --- GESTIONE OPZIONI ---
	 * ----------------------------------------------------------------*</pre>*/
    private void m103ValDclgenVarFunzOpz() {
        // COB_CODE: MOVE WOPZ-GAR-OPZ-CODICE(IND-TIPO-OPZ, IND-OPZ)
        //                                             TO VFC-CODTARI
        //                                                AC5-CODTARI
        //                                                WK-CODICE-TARIFFA-AC5
        //                                                WK-CODICE-TARIFFA-VFC.
        ws.getVarFunzDiCalc().setAc5Codtari(ws.getAreaBusiness().getIvvc0217().getOpzioni(ws.getIxIndici().getTipoOpz()).getGarOpzione(ws.getIxIndici().getOpz()).getCodice());
        ws.getVarFunzDiCalcR().setAc5Codtari(ws.getAreaBusiness().getIvvc0217().getOpzioni(ws.getIxIndici().getTipoOpz()).getGarOpzione(ws.getIxIndici().getOpz()).getCodice());
        ws.setWkCodiceTariffaAc5(ws.getAreaBusiness().getIvvc0217().getOpzioni(ws.getIxIndici().getTipoOpz()).getGarOpzione(ws.getIxIndici().getOpz()).getCodice());
        ws.setWkCodiceTariffaVfc(ws.getAreaBusiness().getIvvc0217().getOpzioni(ws.getIxIndici().getTipoOpz()).getGarOpzione(ws.getIxIndici().getOpz()).getCodice());
        // COB_CODE: PERFORM M170-VERIFICA-VERS-PROD-GO THRU M170-EX.
        m170VerificaVersProdGo();
    }

    /**Original name: M120-CNTL-VAL-DCLGEN-STD<br>
	 * <pre>----------------------------------------------------------------*
	 *     CONTROLLO VALORIZZAZIONE DCLGEN STANDARD
	 * ----------------------------------------------------------------*</pre>*/
    private void m120CntlValDclgenStd() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN GESTIONE-PRODOTTO
        //                 PERFORM M101-VAL-DCLGEN-VAR-FUNZ-PROD THRU M101-EX
        //              WHEN GESTIONE-GARANZIE
        //                 PERFORM M102-VAL-DCLGEN-VAR-FUNZ-GAR  THRU M102-EX
        //           END-EVALUATE.
        switch (ws.getFlagGestione().getFlagGestione()) {

            case FlagGestione.PRODOTTO:// COB_CODE: PERFORM M101-VAL-DCLGEN-VAR-FUNZ-PROD THRU M101-EX
                m101ValDclgenVarFunzProd();
                break;

            case FlagGestione.GARANZIE:// COB_CODE: PERFORM M102-VAL-DCLGEN-VAR-FUNZ-GAR  THRU M102-EX
                m102ValDclgenVarFunzGar();
                break;

            default:break;
        }
    }

    /**Original name: M130-CNTL-VAL-DCLGEN-OPZ<br>
	 * <pre>----------------------------------------------------------------*
	 *     CONTROLLO VALORIZZAZIONE DCLGEN TIPO OPZIONI
	 * ----------------------------------------------------------------*</pre>*/
    private void m130CntlValDclgenOpz() {
        ConcatUtil concatUtil = null;
        // COB_CODE:      IF  IVVC0211-CODICE-INIZIATIVA > SPACES AND
        //                    IVVC0211-CODICE-INIZIATIVA NOT = HIGH-VALUE
        //           *   segnalazione di errore nel caso di iniziativa commerciale
        //                    END-STRING
        //                ELSE
        //           *   Accesso alla tab. VAR_FUNZ_DI_CALC_R x codice trattato
        //           *   in caso di esplosione e ricalcolo RIASS
        //                    END-IF
        //                END-IF.
        if (Characters.GT_SPACE.test(inputIvvs0211.getIvvc0211RestoDati().getCodiceIniziativa()) && !Characters.EQ_HIGH.test(inputIvvs0211.getIvvc0211RestoDati().getCodiceIniziativaFormatted())) {
            //   segnalazione di errore nel caso di iniziativa commerciale
            // COB_CODE: SET IVVC0211-TIPO-SCHEDA-NOT-VALID TO TRUE
            inputIvvs0211.getIvvc0211RestoDati().getReturnCode().setTipoSchedaNotValid();
            // COB_CODE: MOVE WK-PGM  TO IVVC0211-COD-SERVIZIO-BE
            inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: STRING 'TIPO SCHEDA NON GESTITA '
            //                  ' - '
            //                  'COMP : '
            //                  WK-CODICE-COMPAGNIA
            //                  ' - '
            //                  'TIPO SCHEDA : '
            //                  FLAG-TIPOLOGIA-SCHEDA
            //                  DELIMITED BY SIZE INTO
            //                  IVVC0211-DESCRIZ-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ivvc0211CampiEsito.Len.DESCRIZ_ERR, new String[] {"TIPO SCHEDA NON GESTITA ", " - ", "COMP : ", ws.getWkCodiceCompagniaAsString(), " - ", "TIPO SCHEDA : ", ws.getFlagTipologiaScheda().getFlagTipologiaSchedaFormatted()});
            inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setDescrizErr(concatUtil.replaceInString(inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().getDescrizErrFormatted()));
        }
        else if (Characters.GT_SPACE.test(inputIvvs0211.getIvvc0211RestoDati().getCodiceTrattato()) && !Characters.EQ_HIGH.test(inputIvvs0211.getIvvc0211RestoDati().getCodiceTrattatoFormatted())) {
            //   Accesso alla tab. VAR_FUNZ_DI_CALC_R x codice trattato
            //   in caso di esplosione e ricalcolo RIASS
            // COB_CODE: IF  IVVC0211-CODICE-TRATTATO > SPACES
            //           AND IVVC0211-CODICE-TRATTATO NOT = HIGH-VALUE
            //               END-IF
            //           END-IF
            // COB_CODE: IF GESTIONE-PRODOTTO
            //                                      WK-CODICE-TARIFFA-AC5
            //           ELSE
            //                                      WK-CODICE-TARIFFA-AC5
            //           END-IF
            if (ws.getFlagGestione().isProdotto()) {
                // COB_CODE: MOVE 'P'             TO AC5-CODTARI
                //                                   WK-CODICE-TARIFFA-AC5
                ws.getVarFunzDiCalcR().setAc5Codtari("P");
                ws.setWkCodiceTariffaAc5("P");
            }
            else {
                // COB_CODE: MOVE 'G'             TO AC5-CODTARI
                //                                   WK-CODICE-TARIFFA-AC5
                ws.getVarFunzDiCalcR().setAc5Codtari("G");
                ws.setWkCodiceTariffaAc5("G");
            }
        }
        // COB_CODE: IF IVVC0211-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (inputIvvs0211.getIvvc0211RestoDati().getReturnCode().isSuccessfulRc()) {
            // COB_CODE: EVALUATE TRUE
            //              WHEN GESTIONE-PRODOTTO
            //                 END-IF
            //              WHEN GESTIONE-GARANZIE
            //                 PERFORM M103-VAL-DCLGEN-VAR-FUNZ-OPZ  THRU M103-EX
            //           END-EVALUATE
            switch (ws.getFlagGestione().getFlagGestione()) {

                case FlagGestione.PRODOTTO:// COB_CODE: MOVE WPOL-FL-VER-PROD             TO FLAG-VERSIONE
                    ws.getFlagVersione().setFlagVersioneFormatted(ws.getAreaBusiness().getLccvpol1().getDati().getWpolFlVerProdFormatted());
                    // COB_CODE: IF EMISSIONE
                    //                                             WK-DATA-INIZIO-VFC
                    //           ELSE
                    //                                             WK-DATA-INIZIO-VFC
                    //           END-IF
                    if (ws.getFlagVersione().isEmissione()) {
                        // COB_CODE: MOVE WPOL-DT-INI-VLDT-PROD     TO VFC-DINIZ
                        //                                             AC5-DINIZ
                        //                                          WK-DATA-INIZIO-VFC
                        ws.getVarFunzDiCalc().setAc5Diniz(ws.getAreaBusiness().getLccvpol1().getDati().getWpolDtIniVldtProd());
                        ws.getVarFunzDiCalcR().setAc5Diniz(ws.getAreaBusiness().getLccvpol1().getDati().getWpolDtIniVldtProd());
                        ws.setWkDataInizioVfc(TruncAbs.toInt(ws.getAreaBusiness().getLccvpol1().getDati().getWpolDtIniVldtProd(), 9));
                    }
                    else {
                        // COB_CODE: MOVE IVVC0211-DATA-ULT-VERS-PROD TO VFC-DINIZ
                        //                                               AC5-DINIZ
                        //                                          WK-DATA-INIZIO-VFC
                        ws.getVarFunzDiCalc().setAc5Diniz(inputIvvs0211.getIvvc0211DatiInput().getDataUltVersProd());
                        ws.getVarFunzDiCalcR().setAc5Diniz(inputIvvs0211.getIvvc0211DatiInput().getDataUltVersProd());
                        ws.setWkDataInizioVfc(TruncAbs.toInt(inputIvvs0211.getIvvc0211DatiInput().getDataUltVersProd(), 9));
                    }
                    break;

                case FlagGestione.GARANZIE:// COB_CODE: PERFORM M103-VAL-DCLGEN-VAR-FUNZ-OPZ  THRU M103-EX
                    m103ValDclgenVarFunzOpz();
                    break;

                default:break;
            }
        }
    }

    /**Original name: M150-VERIFICA-VERS-PROD-GP<br>
	 * <pre>----------------------------------------------------------------*
	 *     VERIFICA IL FLAG VERSIONE DI PRODOTTO -
	 *     --- GESTIONE PRODOTTO ---
	 * ----------------------------------------------------------------*</pre>*/
    private void m150VerificaVersProdGp() {
        ConcatUtil concatUtil = null;
        // COB_CODE:      IF  (IVVC0211-CODICE-INIZIATIVA > SPACES AND
        //                     IVVC0211-CODICE-INIZIATIVA NOT = HIGH-VALUE )
        //           *    OR RIASS-SI
        //                                                  WK-DATA-INIZIO-VFC
        //                ELSE
        //                    END-EVALUATE
        //                END-IF.
        if (Characters.GT_SPACE.test(inputIvvs0211.getIvvc0211RestoDati().getCodiceIniziativa()) && !Characters.EQ_HIGH.test(inputIvvs0211.getIvvc0211RestoDati().getCodiceIniziativaFormatted())) {
            //    OR RIASS-SI
            // COB_CODE: MOVE IVVC0211-DATA-EFFETTO TO VFC-DINIZ
            //                                         AC5-DINIZ
            //                                         WK-DATA-INIZIO-VFC
            ws.getVarFunzDiCalc().setAc5Diniz(inputIvvs0211.getIvvc0211DatiInput().getDataEffetto());
            ws.getVarFunzDiCalcR().setAc5Diniz(inputIvvs0211.getIvvc0211DatiInput().getDataEffetto());
            ws.setWkDataInizioVfc(TruncAbs.toInt(inputIvvs0211.getIvvc0211DatiInput().getDataEffetto(), 9));
        }
        else {
            // COB_CODE: EVALUATE TRUE
            //              WHEN EMISSIONE
            //                   END-IF
            //              WHEN ULTIMA
            //                   PERFORM M151-CNTL-VEN-PVEN       THRU M151-EX
            //              WHEN OTHER
            //                   END-STRING
            //           END-EVALUATE
            switch (ws.getFlagVersione().getFlagVersione()) {

                case FlagVersione.EMISSIONE:// COB_CODE: MOVE WPOL-DT-INI-VLDT-PROD TO VFC-DINIZ
                    //                                         AC5-DINIZ
                    //                                         WK-DATA-INIZIO-VFC
                    ws.getVarFunzDiCalc().setAc5Diniz(ws.getAreaBusiness().getLccvpol1().getDati().getWpolDtIniVldtProd());
                    ws.getVarFunzDiCalcR().setAc5Diniz(ws.getAreaBusiness().getLccvpol1().getDati().getWpolDtIniVldtProd());
                    ws.setWkDataInizioVfc(TruncAbs.toInt(ws.getAreaBusiness().getLccvpol1().getDati().getWpolDtIniVldtProd(), 9));
                    // COB_CODE: IF RIASS-SI
                    //              MOVE WGRZ-DT-DECOR(1)   TO AC5-DINIZ
                    //           END-IF
                    if (ws.getFlagRiass().isSi()) {
                        // COB_CODE: MOVE WGRZ-DT-DECOR(1)   TO AC5-DINIZ
                        ws.getVarFunzDiCalcR().setAc5Diniz(ws.getAreaBusiness().getWgrzTabGar(1).getLccvgrz1().getDati().getWgrzDtDecor().getWgrzDtDecor());
                    }
                    break;

                case FlagVersione.ULTIMA:// COB_CODE: PERFORM M151-CNTL-VEN-PVEN       THRU M151-EX
                    m151CntlVenPven();
                    break;

                default:// COB_CODE: SET IVVC0211-VERS-PROD-NOT-VALID  TO TRUE
                    inputIvvs0211.getIvvc0211RestoDati().getReturnCode().setVersProdNotValid();
                    // COB_CODE: MOVE WK-PGM           TO IVVC0211-COD-SERVIZIO-BE
                    inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: STRING 'FLAG VERS. PROD.  NON VALIDO'
                    //                 DELIMITED BY SIZE INTO
                    //                 IVVC0211-DESCRIZ-ERR
                    //           END-STRING
                    inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setDescrizErr("FLAG VERS. PROD.  NON VALIDO");
                    break;
            }
        }
    }

    /**Original name: M151-CNTL-VEN-PVEN<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZA DCLGEN TABELLA VARIABILI FUNZIONI DI CALCOLO
	 * ----------------------------------------------------------------*</pre>*/
    private void m151CntlVenPven() {
        // COB_CODE: IF IVVC0211-AREA-VE
        //                                                  WK-DATA-INIZIO-VFC
        //           ELSE
        //              PERFORM M152-VAL-DT-VLDT-PROD THRU M152-EX
        //           END-IF.
        if (inputIvvs0211.getIvvc0211DatiInput().getFlgArea().isVe()) {
            // COB_CODE: MOVE IVVC0211-DATA-ULT-VERS-PROD TO VFC-DINIZ
            //                                               AC5-DINIZ
            //                                               WK-DATA-INIZIO-VFC
            ws.getVarFunzDiCalc().setAc5Diniz(inputIvvs0211.getIvvc0211DatiInput().getDataUltVersProd());
            ws.getVarFunzDiCalcR().setAc5Diniz(inputIvvs0211.getIvvc0211DatiInput().getDataUltVersProd());
            ws.setWkDataInizioVfc(TruncAbs.toInt(inputIvvs0211.getIvvc0211DatiInput().getDataUltVersProd(), 9));
        }
        else {
            // COB_CODE: PERFORM M152-VAL-DT-VLDT-PROD THRU M152-EX
            m152ValDtVldtProd();
        }
    }

    /**Original name: M152-VAL-DT-VLDT-PROD<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZA DCLGEN TABELLA VARIABILI FUNZIONI DI CALCOLO
	 * ----------------------------------------------------------------*</pre>*/
    private void m152ValDtVldtProd() {
        ConcatUtil concatUtil = null;
        // COB_CODE:      IF  (IVVC0211-CODICE-INIZIATIVA > SPACES AND
        //                     IVVC0211-CODICE-INIZIATIVA NOT = HIGH-VALUE )
        //           *    OR RIASS-SI
        //                                                  WK-DATA-INIZIO-VFC
        //                ELSE
        //                    END-IF
        //                END-IF.
        if (Characters.GT_SPACE.test(inputIvvs0211.getIvvc0211RestoDati().getCodiceIniziativa()) && !Characters.EQ_HIGH.test(inputIvvs0211.getIvvc0211RestoDati().getCodiceIniziativaFormatted())) {
            //    OR RIASS-SI
            // COB_CODE: MOVE IVVC0211-DATA-EFFETTO TO VFC-DINIZ
            //                                         AC5-DINIZ
            //                                         WK-DATA-INIZIO-VFC
            ws.getVarFunzDiCalc().setAc5Diniz(inputIvvs0211.getIvvc0211DatiInput().getDataEffetto());
            ws.getVarFunzDiCalcR().setAc5Diniz(inputIvvs0211.getIvvc0211DatiInput().getDataEffetto());
            ws.setWkDataInizioVfc(TruncAbs.toInt(inputIvvs0211.getIvvc0211DatiInput().getDataEffetto(), 9));
        }
        else if (ws.getFlagTipoTranche().isContesto()) {
            // COB_CODE: IF TRANCHE-CONTESTO
            //               END-IF
            //           ELSE
            //               END-IF
            //           END-IF
            // COB_CODE: IF WTGA-DT-VLDT-PROD-NULL(IND-TGA) NOT = HIGH-VALUE
            //              END-IF
            //           ELSE
            //              MOVE 'TRCH_DI_GAR'       TO IVVC0211-NOME-TABELLA
            //           END-IF
            if (!Characters.EQ_HIGH.test(ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().getDtVldtProdNullFormatted(ws.getIxIndici().getTga()))) {
                // COB_CODE: MOVE WTGA-DT-VLDT-PROD(IND-TGA) TO VFC-DINIZ
                //                                           AC5-DINIZ
                //                                           WK-DATA-INIZIO-VFC
                ws.getVarFunzDiCalc().setAc5Diniz(ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().getDtVldtProd(ws.getIxIndici().getTga()));
                ws.getVarFunzDiCalcR().setAc5Diniz(ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().getDtVldtProd(ws.getIxIndici().getTga()));
                ws.setWkDataInizioVfc(TruncAbs.toInt(ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().getDtVldtProd(ws.getIxIndici().getTga()), 9));
                // COB_CODE: IF RIASS-SI
                //              MOVE WGRZ-DT-DECOR(1)       TO AC5-DINIZ
                //           END-IF
                if (ws.getFlagRiass().isSi()) {
                    // COB_CODE: MOVE WGRZ-DT-DECOR(1)       TO AC5-DINIZ
                    ws.getVarFunzDiCalcR().setAc5Diniz(ws.getAreaBusiness().getWgrzTabGar(1).getLccvgrz1().getDati().getWgrzDtDecor().getWgrzDtDecor());
                }
            }
            else {
                // COB_CODE: SET IVVC0211-VERS-PROD-NOT-VALID  TO TRUE
                inputIvvs0211.getIvvc0211RestoDati().getReturnCode().setVersProdNotValid();
                // COB_CODE: MOVE WK-PGM            TO IVVC0211-COD-SERVIZIO-BE
                inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: STRING 'DATA VALID. PROD. NON VALIDA'
                //                  DELIMITED BY SIZE INTO
                //                  IVVC0211-DESCRIZ-ERR
                //           END-STRING
                inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setDescrizErr("DATA VALID. PROD. NON VALIDA");
                // COB_CODE: MOVE 'TRCH_DI_GAR'       TO IVVC0211-NOME-TABELLA
                inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setNomeTabella("TRCH_DI_GAR");
            }
        }
        else if (!Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaDtVldtProd().getTgaDtVldtProdNullFormatted())) {
            // COB_CODE: IF TGA-DT-VLDT-PROD-NULL NOT = HIGH-VALUE
            //              END-IF
            //           ELSE
            //              MOVE 'TRCH_DI_GAR'       TO IVVC0211-NOME-TABELLA
            //           END-IF
            // COB_CODE: MOVE TGA-DT-VLDT-PROD       TO VFC-DINIZ
            //                                          AC5-DINIZ
            //                                          WK-DATA-INIZIO-VFC
            ws.getVarFunzDiCalc().setAc5Diniz(ws.getTrchDiGar().getTgaDtVldtProd().getTgaDtVldtProd());
            ws.getVarFunzDiCalcR().setAc5Diniz(ws.getTrchDiGar().getTgaDtVldtProd().getTgaDtVldtProd());
            ws.setWkDataInizioVfc(TruncAbs.toInt(ws.getTrchDiGar().getTgaDtVldtProd().getTgaDtVldtProd(), 9));
            // COB_CODE: IF RIASS-SI
            //              MOVE WGRZ-DT-DECOR(1)       TO AC5-DINIZ
            //           END-IF
            if (ws.getFlagRiass().isSi()) {
                // COB_CODE: MOVE WGRZ-DT-DECOR(1)       TO AC5-DINIZ
                ws.getVarFunzDiCalcR().setAc5Diniz(ws.getAreaBusiness().getWgrzTabGar(1).getLccvgrz1().getDati().getWgrzDtDecor().getWgrzDtDecor());
            }
        }
        else {
            // COB_CODE: SET IVVC0211-VERS-PROD-NOT-VALID  TO TRUE
            inputIvvs0211.getIvvc0211RestoDati().getReturnCode().setVersProdNotValid();
            // COB_CODE: MOVE WK-PGM            TO IVVC0211-COD-SERVIZIO-BE
            inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: STRING 'DATA VALID. PROD. NON VALIDA'
            //                  DELIMITED BY SIZE INTO
            //                  IVVC0211-DESCRIZ-ERR
            //           END-STRING
            inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setDescrizErr("DATA VALID. PROD. NON VALIDA");
            // COB_CODE: MOVE 'TRCH_DI_GAR'       TO IVVC0211-NOME-TABELLA
            inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setNomeTabella("TRCH_DI_GAR");
        }
    }

    /**Original name: M160-VERIFICA-VERS-PROD-GG<br>
	 * <pre>----------------------------------------------------------------*
	 *     VERIFICA IL FLAG VERSIONE DI PRODOTTO
	 *     --- GESTIONE GARANZIE ---
	 * ----------------------------------------------------------------*</pre>*/
    private void m160VerificaVersProdGg() {
        ConcatUtil concatUtil = null;
        // COB_CODE: EVALUATE TRUE
        //              WHEN EMISSIONE
        //                   END-IF
        //              WHEN ULTIMA
        //                   END-IF
        //              WHEN OTHER
        //                   END-STRING
        //           END-EVALUATE.
        switch (ws.getFlagVersione().getFlagVersione()) {

            case FlagVersione.EMISSIONE:// COB_CODE: MOVE WPOL-DT-INI-VLDT-PROD   TO VFC-DINIZ
                //                                           AC5-DINIZ
                //                                           WK-DATA-INIZIO-VFC
                ws.getVarFunzDiCalc().setAc5Diniz(ws.getAreaBusiness().getLccvpol1().getDati().getWpolDtIniVldtProd());
                ws.getVarFunzDiCalcR().setAc5Diniz(ws.getAreaBusiness().getLccvpol1().getDati().getWpolDtIniVldtProd());
                ws.setWkDataInizioVfc(TruncAbs.toInt(ws.getAreaBusiness().getLccvpol1().getDati().getWpolDtIniVldtProd(), 9));
                // COB_CODE: IF RIASS-SI
                //              MOVE WGRZ-DT-DECOR(IND-GRZ) TO AC5-DINIZ
                //           END-IF
                if (ws.getFlagRiass().isSi()) {
                    // COB_CODE: MOVE WGRZ-DT-DECOR(IND-GRZ) TO AC5-DINIZ
                    ws.getVarFunzDiCalcR().setAc5Diniz(ws.getAreaBusiness().getWgrzTabGar(ws.getIxIndici().getGrz()).getLccvgrz1().getDati().getWgrzDtDecor().getWgrzDtDecor());
                }
                break;

            case FlagVersione.ULTIMA:// COB_CODE: MOVE IVVC0211-DATA-ULT-VERS-PROD
                //                                        TO VFC-DINIZ
                //                                           AC5-DINIZ
                //                                           WK-DATA-INIZIO-VFC
                ws.getVarFunzDiCalc().setAc5Diniz(inputIvvs0211.getIvvc0211DatiInput().getDataUltVersProd());
                ws.getVarFunzDiCalcR().setAc5Diniz(inputIvvs0211.getIvvc0211DatiInput().getDataUltVersProd());
                ws.setWkDataInizioVfc(TruncAbs.toInt(inputIvvs0211.getIvvc0211DatiInput().getDataUltVersProd(), 9));
                // COB_CODE: IF RIASS-SI
                //              MOVE WGRZ-DT-DECOR(IND-GRZ) TO AC5-DINIZ
                //           END-IF
                if (ws.getFlagRiass().isSi()) {
                    // COB_CODE: MOVE WGRZ-DT-DECOR(IND-GRZ) TO AC5-DINIZ
                    ws.getVarFunzDiCalcR().setAc5Diniz(ws.getAreaBusiness().getWgrzTabGar(ws.getIxIndici().getGrz()).getLccvgrz1().getDati().getWgrzDtDecor().getWgrzDtDecor());
                }
                break;

            default:// COB_CODE: SET IVVC0211-VERS-PROD-NOT-VALID  TO TRUE
                inputIvvs0211.getIvvc0211RestoDati().getReturnCode().setVersProdNotValid();
                // COB_CODE: MOVE WK-PGM            TO IVVC0211-COD-SERVIZIO-BE
                inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: STRING 'FLAG VERS. PROD.  NON VALIDO'
                //                 DELIMITED BY SIZE INTO
                //                 IVVC0211-DESCRIZ-ERR
                //           END-STRING
                inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setDescrizErr("FLAG VERS. PROD.  NON VALIDO");
                break;
        }
    }

    /**Original name: M170-VERIFICA-VERS-PROD-GO<br>
	 * <pre>----------------------------------------------------------------*
	 *     VERIFICA IL FLAG VERSIONE DI GARANZIE
	 *     --- GESTIONE OPZIONI ---
	 * ----------------------------------------------------------------*</pre>*/
    private void m170VerificaVersProdGo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN EMISSIONE
        //                                               WK-DATA-INIZIO-VFC
        //                                               WK-DATA-INIZIO-VFC
        //                                               WK-DATA-INIZIO-VFC
        //              WHEN ULTIMA
        //                                                WK-DATA-INIZIO-VFC
        //                                                WK-DATA-INIZIO-VFC
        //                                                WK-DATA-INIZIO-VFC
        //              WHEN SPAZIO
        //                                                WK-DATA-INIZIO-VFC
        //                                                WK-DATA-INIZIO-VFC
        //                                                WK-DATA-INIZIO-VFC
        //           END-EVALUATE.
        switch (ws.getFlagVersione().getFlagVersione()) {

            case FlagVersione.EMISSIONE:// COB_CODE: MOVE WPOL-DT-DECOR       TO VFC-DINIZ
                //                                       AC5-DINIZ
                //                                       WK-DATA-INIZIO-VFC
                ws.getVarFunzDiCalc().setAc5Diniz(ws.getAreaBusiness().getLccvpol1().getDati().getWpolDtDecor());
                ws.getVarFunzDiCalcR().setAc5Diniz(ws.getAreaBusiness().getLccvpol1().getDati().getWpolDtDecor());
                ws.setWkDataInizioVfc(TruncAbs.toInt(ws.getAreaBusiness().getLccvpol1().getDati().getWpolDtDecor(), 9));
                break;

            case FlagVersione.ULTIMA:// COB_CODE: MOVE WOPZ-GAR-OPZ-DECORRENZA(IND-TIPO-OPZ, IND-OPZ)
                //                                     TO VFC-DINIZ
                //                                        AC5-DINIZ
                //                                        WK-DATA-INIZIO-VFC
                ws.getVarFunzDiCalc().setVfcDinizFormatted(ws.getAreaBusiness().getIvvc0217().getOpzioni(ws.getIxIndici().getTipoOpz()).getGarOpzione(ws.getIxIndici().getOpz()).getWopzGarOpzDecorrenzaFormatted());
                ws.getVarFunzDiCalcR().setVfcDinizFormatted(ws.getAreaBusiness().getIvvc0217().getOpzioni(ws.getIxIndici().getTipoOpz()).getGarOpzione(ws.getIxIndici().getOpz()).getWopzGarOpzDecorrenzaFormatted());
                ws.setWkDataInizioVfcFormatted(ws.getAreaBusiness().getIvvc0217().getOpzioni(ws.getIxIndici().getTipoOpz()).getGarOpzione(ws.getIxIndici().getOpz()).getWopzGarOpzDecorrenzaFormatted());
                break;

            case FlagVersione.SPAZIO:// COB_CODE: MOVE WOPZ-GAR-OPZ-DECORRENZA(IND-TIPO-OPZ, IND-OPZ)
                //                                     TO VFC-DINIZ
                //                                        AC5-DINIZ
                //                                        WK-DATA-INIZIO-VFC
                ws.getVarFunzDiCalc().setVfcDinizFormatted(ws.getAreaBusiness().getIvvc0217().getOpzioni(ws.getIxIndici().getTipoOpz()).getGarOpzione(ws.getIxIndici().getOpz()).getWopzGarOpzDecorrenzaFormatted());
                ws.getVarFunzDiCalcR().setVfcDinizFormatted(ws.getAreaBusiness().getIvvc0217().getOpzioni(ws.getIxIndici().getTipoOpz()).getGarOpzione(ws.getIxIndici().getOpz()).getWopzGarOpzDecorrenzaFormatted());
                ws.setWkDataInizioVfcFormatted(ws.getAreaBusiness().getIvvc0217().getOpzioni(ws.getIxIndici().getTipoOpz()).getGarOpzione(ws.getIxIndici().getOpz()).getWopzGarOpzDecorrenzaFormatted());
                break;

            default:break;
        }
    }

    /**Original name: M500-VALORIZZA-VARIABILI<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZA L'AREA VARIABILI CON L'OUTPUT RESTITUITO DALLA
	 *     LETTURA SULLA TABELLA VARIABILI FUNZIONI DI CALCOLO
	 * ----------------------------------------------------------------*</pre>*/
    private void m500ValorizzaVariabili() {
        // COB_CODE: IF VFC-ISPRECALC = PRESENZA-VARIABILI
        //              END-IF
        //           ELSE
        //              END-IF
        //           END-IF.
        if (ws.getVarFunzDiCalc().getAc5Isprecalc().getAc5Isprecalc() == ws.getPresenzaVariabili()) {
            // COB_CODE: PERFORM L350-CARICA-LIVELLI       THRU L350-EX
            l350CaricaLivelli();
            // COB_CODE:         IF VFC-GLOVARLIST  = HIGH-VALUES  OR
            //                                        LOW-VALUE    OR
            //                                        SPACES
            //           *--->    VARIABILI NON PRESENTI
            //                     CONTINUE
            //           *           SET IVVC0211-GLOVARLIST-VUOTA  TO  TRUE
            //                   ELSE
            //                      PERFORM M600-SCOMPATTA-GLOVAR       THRU M600-EX
            //                   END-IF
            if (Characters.EQ_HIGH.test(ws.getVarFunzDiCalc().getAc5Glovarlist(), VarFunzDiCalcR.Len.AC5_GLOVARLIST) || Characters.EQ_LOW.test(ws.getVarFunzDiCalc().getAc5Glovarlist(), VarFunzDiCalcR.Len.AC5_GLOVARLIST) || Characters.EQ_SPACE.test(ws.getVarFunzDiCalc().getAc5Glovarlist())) {
            //--->    VARIABILI NON PRESENTI
            // COB_CODE: CONTINUE
            //continue
            //           SET IVVC0211-GLOVARLIST-VUOTA  TO  TRUE
            }
            else {
                // COB_CODE: SET WK-GLOVARLIST-PIENA  TO  TRUE
                ws.getWkGlovarlist().setPiena();
                // COB_CODE: PERFORM M600-SCOMPATTA-GLOVAR       THRU M600-EX
                m600ScompattaGlovar();
            }
        }
        else if (ws.getVarFunzDiCalc().getAc5Isprecalc().getAc5Isprecalc() == ws.getPresenzaServizio()) {
            // COB_CODE: IF VFC-ISPRECALC = PRESENZA-SERVIZIO
            //              END-IF
            //           END-IF
            // COB_CODE: ADD 1                    TO IND-LIVELLO
            ws.getIxIndici().setLivello(Trunc.toShort(1 + ws.getIxIndici().getLivello(), 4));
            // COB_CODE: IF TIPO-OPZIONI
            //              END-IF
            //           END-IF
            if (ws.getFlagTipologiaScheda().isOpzioni()) {
                // COB_CODE: IF GESTIONE-PRODOTTO
                //                   TO IVVC0216-COD-TIPO-OPZIONE-P(IND-LIVELLO)
                //           ELSE
                //                   TO IVVC0216-COD-TIPO-OPZIONE-T(IND-LIVELLO)
                //           END-IF
                if (ws.getFlagGestione().isProdotto()) {
                    // COB_CODE: MOVE WOPZ-COD-TIPO-OPZIONE     (IND-TIPO-OPZ)
                    //                TO IVVC0216-COD-TIPO-OPZIONE-P(IND-LIVELLO)
                    ws.getAreaWorkIvvc0216().getC216TabValP().setIvvc0216CodTipoOpzioneP(ws.getIxIndici().getLivello(), ws.getAreaBusiness().getIvvc0217().getOpzioni(ws.getIxIndici().getTipoOpz()).getCodTipoOpzione());
                }
                else {
                    // COB_CODE: MOVE WOPZ-COD-TIPO-OPZIONE     (IND-TIPO-OPZ)
                    //                TO IVVC0216-COD-TIPO-OPZIONE-T(IND-LIVELLO)
                    ws.getAreaWorkIvvc0216().getC216TabValT().setIvvc0216CodTipoOpzioneT(ws.getIxIndici().getLivello(), ws.getAreaBusiness().getIvvc0217().getOpzioni(ws.getIxIndici().getTipoOpz()).getCodTipoOpzione());
                }
            }
            // COB_CODE: IF GESTIONE-PRODOTTO
            //                   TO IVVC0216-NOME-SERVIZIO-P(IND-LIVELLO)
            //           ELSE
            //                   TO IVVC0216-NOME-SERVIZIO-T(IND-LIVELLO)
            //           END-IF
            if (ws.getFlagGestione().isProdotto()) {
                // COB_CODE: MOVE VFC-NOMEPROGR
                //                TO IVVC0216-NOME-SERVIZIO-P(IND-LIVELLO)
                ws.getAreaWorkIvvc0216().getC216TabValP().setIvvc0216NomeServizioP(ws.getIxIndici().getLivello(), ws.getVarFunzDiCalc().getAc5Nomeprogr());
            }
            else {
                // COB_CODE: MOVE VFC-NOMEPROGR
                //                TO IVVC0216-NOME-SERVIZIO-T(IND-LIVELLO)
                ws.getAreaWorkIvvc0216().getC216TabValT().setIvvc0216NomeServizioT(ws.getIxIndici().getLivello(), ws.getVarFunzDiCalc().getAc5Nomeprogr());
            }
        }
    }

    /**Original name: M501-PREPARA-UNZIP-GLOVAR<br>
	 * <pre>----------------------------------------------------------------*
	 *    PREPARA STRINGA DA UNZIPPARE
	 * ----------------------------------------------------------------*</pre>*/
    private void m501PreparaUnzipGlovar() {
        // COB_CODE: MOVE LENGTH OF VFC-GLOVARLIST      TO UNZIP-LENGTH-STR-MAX.
        ws.getUnzipStructure().setUnzipLengthStrMax(((short)VarFunzDiCalcR.Len.AC5_GLOVARLIST));
        // COB_CODE: IF GESTIONE-PRODOTTO
        //                                              TO UNZIP-LENGTH-FIELD
        //           ELSE
        //                                              TO UNZIP-LENGTH-FIELD
        //           END-IF
        if (ws.getFlagGestione().isProdotto()) {
            // COB_CODE: MOVE LENGTH OF IVVC0216-COD-VARIABILE-P(1, 1)
            //                                           TO UNZIP-LENGTH-FIELD
            ws.getUnzipStructure().setUnzipLengthField(Trunc.toShort(C216TabValP.Len.COD_VARIABILE_P, 2));
        }
        else {
            // COB_CODE: MOVE LENGTH OF IVVC0216-COD-VARIABILE-T(1, 1)
            //                                           TO UNZIP-LENGTH-FIELD
            ws.getUnzipStructure().setUnzipLengthField(Trunc.toShort(C216TabValT.Len.COD_VARIABILE_T, 2));
        }
        // COB_CODE: MOVE VFC-GLOVARLIST                TO UNZIP-STRING-ZIPPED.
        ws.getUnzipStructure().setUnzipStringZipped(ws.getVarFunzDiCalc().getAc5Glovarlist());
        // COB_CODE: MOVE ','                           TO UNZIP-IDENTIFICATORE.
        ws.getUnzipStructure().setUnzipIdentificatoreFormatted(",");
    }

    /**Original name: M502-CARICA-GLOVAR-UNZIPPED<br>
	 * <pre>----------------------------------------------------------------*
	 *  CARICA CAMPI UNZIPPED
	 * ----------------------------------------------------------------*</pre>*/
    private void m502CaricaGlovarUnzipped() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE UNZIP-ELE-VARIABILI-MAX TO VARIABILI-RIASS
        ws.setVariabiliRiass(TruncAbs.toShort(ws.getUnzipStructure().getUnzipEleVariabiliMax(), 3));
        // COB_CODE: IF RIASS-SI
        //              CONTINUE
        //           ELSE
        //              END-IF
        //           END-IF.
        if (ws.getFlagRiass().isSi()) {
        // COB_CODE: CONTINUE
        //continue
        }
        else if (ws.getFlagGestione().isProdotto()) {
            // COB_CODE: IF GESTIONE-PRODOTTO
            //              END-PERFORM
            //           ELSE
            //              END-PERFORM
            //           END-IF
            // COB_CODE: PERFORM VARYING IND-UNZIP FROM 1 BY 1
            //                   UNTIL IND-UNZIP  >
            //                         LIMITE-VARIABILI-UNZIPPED OR
            //                         IND-UNZIP  >
            //                         UNZIP-ELE-VARIABILI-MAX   OR
            //                         UNZIP-COD-VARIABILE(IND-UNZIP) =
            //                         SPACES OR LOW-VALUE OR HIGH-VALUE
            //                       (IND-LIVELLO, IND-UNZIP)
            //           END-PERFORM
            ws.getIxIndici().setUnzip(((short)1));
            while (!(ws.getIxIndici().getUnzip() > ws.getLimiteVariabiliUnzipped() || ws.getIxIndici().getUnzip() > ws.getUnzipStructure().getUnzipEleVariabiliMax() || Characters.EQ_SPACE.test(ws.getUnzipStructure().getUnzipTab().getCodVariabile(ws.getIxIndici().getUnzip())) || Characters.EQ_LOW.test(ws.getUnzipStructure().getUnzipTab().getCodVariabile(ws.getIxIndici().getUnzip()), UnzipTab.Len.COD_VARIABILE) || Characters.EQ_HIGH.test(ws.getUnzipStructure().getUnzipTab().getCodVariabile(ws.getIxIndici().getUnzip()), UnzipTab.Len.COD_VARIABILE))) {
                // COB_CODE: MOVE UNZIP-COD-VARIABILE(IND-UNZIP)
                //             TO IVVC0216-COD-VARIABILE-P
                //                (IND-LIVELLO, IND-UNZIP)
                ws.getAreaWorkIvvc0216().getC216TabValP().setCodVariabileP(ws.getIxIndici().getLivello(), ws.getIxIndici().getUnzip(), ws.getUnzipStructure().getUnzipTab().getCodVariabile(ws.getIxIndici().getUnzip()));
                ws.getIxIndici().setUnzip(Trunc.toShort(ws.getIxIndici().getUnzip() + 1, 4));
            }
        }
        else {
            // COB_CODE: PERFORM VARYING IND-UNZIP FROM 1 BY 1
            //                   UNTIL IND-UNZIP  >
            //                         LIMITE-VARIABILI-UNZIPPED OR
            //                         IND-UNZIP  >
            //                         UNZIP-ELE-VARIABILI-MAX   OR
            //                         UNZIP-COD-VARIABILE(IND-UNZIP) =
            //                         SPACES OR LOW-VALUE OR HIGH-VALUE
            //                       (IND-LIVELLO, IND-UNZIP)
            //           END-PERFORM
            ws.getIxIndici().setUnzip(((short)1));
            while (!(ws.getIxIndici().getUnzip() > ws.getLimiteVariabiliUnzipped() || ws.getIxIndici().getUnzip() > ws.getUnzipStructure().getUnzipEleVariabiliMax() || Characters.EQ_SPACE.test(ws.getUnzipStructure().getUnzipTab().getCodVariabile(ws.getIxIndici().getUnzip())) || Characters.EQ_LOW.test(ws.getUnzipStructure().getUnzipTab().getCodVariabile(ws.getIxIndici().getUnzip()), UnzipTab.Len.COD_VARIABILE) || Characters.EQ_HIGH.test(ws.getUnzipStructure().getUnzipTab().getCodVariabile(ws.getIxIndici().getUnzip()), UnzipTab.Len.COD_VARIABILE))) {
                // COB_CODE: MOVE UNZIP-COD-VARIABILE(IND-UNZIP)
                //             TO IVVC0216-COD-VARIABILE-T
                //                (IND-LIVELLO, IND-UNZIP)
                ws.getAreaWorkIvvc0216().getC216TabValT().setCodVariabileT(ws.getIxIndici().getLivello(), ws.getIxIndici().getUnzip(), ws.getUnzipStructure().getUnzipTab().getCodVariabile(ws.getIxIndici().getUnzip()));
                ws.getIxIndici().setUnzip(Trunc.toShort(ws.getIxIndici().getUnzip() + 1, 4));
            }
        }
        //--->  STATO NON TROVATO
        // COB_CODE: IF UNZIP-TROVATO-NO
        //                                           TO IVVC0211-NOME-TABELLA
        //           ELSE
        //              END-IF
        //           END-IF.
        if (ws.getFlagUnzipTrovato().isNo()) {
            // COB_CODE: SET FINE-ELAB-SI           TO TRUE
            ws.getFlagFineElab().setSi();
            // COB_CODE: SET IVVC0211-GENERIC-ERROR TO TRUE
            inputIvvs0211.getIvvc0211RestoDati().getReturnCode().setGenericError();
            // COB_CODE: MOVE WK-PGM                TO IVVC0211-COD-SERVIZIO-BE
            inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: STRING 'VARIABILI NON TROVATE SU '
            //                  'VAR_FUNZ_DI_CALC'
            //                  ' - '
            //                  'COMPAGNIA : '
            //                  ' -  '
            //                  WK-CODICE-COMPAGNIA
            //                  ' - '
            //                  'COD. PRODOTTO : '
            //                  WK-CODICE-PRODOTTO-VFC
            //                  ' -  '
            //                  'COD. TARIFFA : '
            //                  WK-CODICE-TARIFFA-VFC
            //                  ' -  '
            //                  'DATA INIZIO : '
            //                  WK-DATA-INIZIO-VFC
            //                  ' -  '
            //                  'NOMEFUNZIONE : '
            //                  WK-NOME-FUNZIONE-VFC
            //                  ' -  '
            //                  'STEP ELAB. : '
            //                  WK-STEP-ELAB-VFC
            //                  DELIMITED BY SIZE INTO
            //                  IVVC0211-DESCRIZ-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ivvc0211CampiEsito.Len.DESCRIZ_ERR, new String[] {"VARIABILI NON TROVATE SU ", "VAR_FUNZ_DI_CALC", " - ", "COMPAGNIA : ", " -  ", ws.getWkCodiceCompagniaAsString(), " - ", "COD. PRODOTTO : ", ws.getWkCodiceProdottoVfcFormatted(), " -  ", "COD. TARIFFA : ", ws.getWkCodiceTariffaVfcFormatted(), " -  ", "DATA INIZIO : ", ws.getWkDataInizioVfcAsString(), " -  ", "NOMEFUNZIONE : ", ws.getWkNomeFunzioneVfcFormatted(), " -  ", "STEP ELAB. : ", ws.getWkStepElabVfcFormatted()});
            inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setDescrizErr(concatUtil.replaceInString(inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().getDescrizErrFormatted()));
            // COB_CODE: MOVE 'VAR_FUNZ_DI_CALC'
            //                                        TO IVVC0211-NOME-TABELLA
            inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setNomeTabella("VAR_FUNZ_DI_CALC");
        }
        else if (ws.getFlagGestione().isProdotto()) {
            // COB_CODE: IF GESTIONE-PRODOTTO
            //                TO IVVC0216-ELE-VARIABILI-MAX-P(IND-LIVELLO)
            //           ELSE
            //                TO IVVC0216-ELE-VARIABILI-MAX-T(IND-LIVELLO)
            //           END-IF
            // COB_CODE: MOVE UNZIP-ELE-VARIABILI-MAX
            //             TO IVVC0216-ELE-VARIABILI-MAX-P(IND-LIVELLO)
            ws.getAreaWorkIvvc0216().getC216TabValP().setEleVariabiliMaxP(ws.getIxIndici().getLivello(), ws.getUnzipStructure().getUnzipEleVariabiliMax());
        }
        else {
            // COB_CODE: MOVE UNZIP-ELE-VARIABILI-MAX
            //             TO IVVC0216-ELE-VARIABILI-MAX-T(IND-LIVELLO)
            ws.getAreaWorkIvvc0216().getC216TabValT().setEleVariabiliMaxT(ws.getIxIndici().getLivello(), ws.getUnzipStructure().getUnzipEleVariabiliMax());
        }
    }

    /**Original name: M503-PREPARA-UNZIP-GLOVAR-RS<br>
	 * <pre>----------------------------------------------------------------*
	 *    PREPARA STRINGA DA UNZIPPARE RIASS
	 * ----------------------------------------------------------------*</pre>*/
    private void m503PreparaUnzipGlovarRs() {
        // COB_CODE: MOVE LENGTH OF AC5-GLOVARLIST      TO UNZIP-LENGTH-STR-MAX.
        ws.getUnzipStructure().setUnzipLengthStrMax(((short)VarFunzDiCalcR.Len.AC5_GLOVARLIST));
        // COB_CODE: IF GESTIONE-PRODOTTO
        //                                              TO UNZIP-LENGTH-FIELD
        //           ELSE
        //                                              TO UNZIP-LENGTH-FIELD
        //           END-IF
        if (ws.getFlagGestione().isProdotto()) {
            // COB_CODE: MOVE LENGTH OF IVVC0216-COD-VARIABILE-P(1, 1)
            //                                           TO UNZIP-LENGTH-FIELD
            ws.getUnzipStructure().setUnzipLengthField(Trunc.toShort(C216TabValP.Len.COD_VARIABILE_P, 2));
        }
        else {
            // COB_CODE: MOVE LENGTH OF IVVC0216-COD-VARIABILE-T(1, 1)
            //                                           TO UNZIP-LENGTH-FIELD
            ws.getUnzipStructure().setUnzipLengthField(Trunc.toShort(C216TabValT.Len.COD_VARIABILE_T, 2));
        }
        // COB_CODE: MOVE AC5-GLOVARLIST                TO UNZIP-STRING-ZIPPED.
        ws.getUnzipStructure().setUnzipStringZipped(ws.getVarFunzDiCalcR().getAc5Glovarlist());
        // COB_CODE: MOVE ','                           TO UNZIP-IDENTIFICATORE.
        ws.getUnzipStructure().setUnzipIdentificatoreFormatted(",");
    }

    /**Original name: M504-CARICA-GLOVAR-UNZIPPED-RS<br>
	 * <pre>----------------------------------------------------------------*
	 *  CARICA CAMPI UNZIPPED RIASS
	 * ----------------------------------------------------------------*</pre>*/
    private void m504CaricaGlovarUnzippedRs() {
        ConcatUtil concatUtil = null;
        // COB_CODE: IF RIASS-SI
        //                 THRU GESTIONE-RIASS-EX
        //           ELSE
        //              END-IF
        //           END-IF.
        if (ws.getFlagRiass().isSi()) {
            // COB_CODE: PERFORM GESTIONE-RIASS
            //              THRU GESTIONE-RIASS-EX
            gestioneRiass();
        }
        else if (ws.getFlagGestione().isProdotto()) {
            // COB_CODE: IF GESTIONE-PRODOTTO
            //              END-PERFORM
            //           ELSE
            //              END-PERFORM
            //           END-IF
            // COB_CODE: PERFORM VARYING IND-UNZIP
            //                   FROM  1 BY 1
            //                   UNTIL IND-UNZIP  >
            //                         LIMITE-VARIABILI-UNZIPPED OR
            //                         IND-UNZIP  >
            //                         UNZIP-ELE-VARIABILI-MAX   OR
            //                         UNZIP-COD-VARIABILE(IND-UNZIP) =
            //                         SPACES OR LOW-VALUE OR HIGH-VALUE
            //                        (IND-LIVELLO, IND-UNZIP)
            //           END-PERFORM
            ws.getIxIndici().setUnzip(((short)1));
            while (!(ws.getIxIndici().getUnzip() > ws.getLimiteVariabiliUnzipped() || ws.getIxIndici().getUnzip() > ws.getUnzipStructure().getUnzipEleVariabiliMax() || Characters.EQ_SPACE.test(ws.getUnzipStructure().getUnzipTab().getCodVariabile(ws.getIxIndici().getUnzip())) || Characters.EQ_LOW.test(ws.getUnzipStructure().getUnzipTab().getCodVariabile(ws.getIxIndici().getUnzip()), UnzipTab.Len.COD_VARIABILE) || Characters.EQ_HIGH.test(ws.getUnzipStructure().getUnzipTab().getCodVariabile(ws.getIxIndici().getUnzip()), UnzipTab.Len.COD_VARIABILE))) {
                // COB_CODE: MOVE UNZIP-COD-VARIABILE(IND-UNZIP)
                //             TO IVVC0216-COD-VARIABILE-P
                //                (IND-LIVELLO, IND-UNZIP)
                ws.getAreaWorkIvvc0216().getC216TabValP().setCodVariabileP(ws.getIxIndici().getLivello(), ws.getIxIndici().getUnzip(), ws.getUnzipStructure().getUnzipTab().getCodVariabile(ws.getIxIndici().getUnzip()));
                ws.getIxIndici().setUnzip(Trunc.toShort(ws.getIxIndici().getUnzip() + 1, 4));
            }
        }
        else {
            // COB_CODE: PERFORM VARYING IND-UNZIP
            //                   FROM  1 BY 1
            //                   UNTIL IND-UNZIP  >
            //                         LIMITE-VARIABILI-UNZIPPED OR
            //                         IND-UNZIP  >
            //                         UNZIP-ELE-VARIABILI-MAX   OR
            //                         UNZIP-COD-VARIABILE(IND-UNZIP) =
            //                         SPACES OR LOW-VALUE OR HIGH-VALUE
            //                        (IND-LIVELLO, IND-UNZIP)
            //           END-PERFORM
            ws.getIxIndici().setUnzip(((short)1));
            while (!(ws.getIxIndici().getUnzip() > ws.getLimiteVariabiliUnzipped() || ws.getIxIndici().getUnzip() > ws.getUnzipStructure().getUnzipEleVariabiliMax() || Characters.EQ_SPACE.test(ws.getUnzipStructure().getUnzipTab().getCodVariabile(ws.getIxIndici().getUnzip())) || Characters.EQ_LOW.test(ws.getUnzipStructure().getUnzipTab().getCodVariabile(ws.getIxIndici().getUnzip()), UnzipTab.Len.COD_VARIABILE) || Characters.EQ_HIGH.test(ws.getUnzipStructure().getUnzipTab().getCodVariabile(ws.getIxIndici().getUnzip()), UnzipTab.Len.COD_VARIABILE))) {
                // COB_CODE: MOVE UNZIP-COD-VARIABILE(IND-UNZIP)
                //             TO IVVC0216-COD-VARIABILE-T
                //                (IND-LIVELLO, IND-UNZIP)
                ws.getAreaWorkIvvc0216().getC216TabValT().setCodVariabileT(ws.getIxIndici().getLivello(), ws.getIxIndici().getUnzip(), ws.getUnzipStructure().getUnzipTab().getCodVariabile(ws.getIxIndici().getUnzip()));
                ws.getIxIndici().setUnzip(Trunc.toShort(ws.getIxIndici().getUnzip() + 1, 4));
            }
        }
        //--->  STATO NON TROVATO
        // COB_CODE: IF UNZIP-TROVATO-NO
        //                                           TO IVVC0211-NOME-TABELLA
        //           ELSE
        //              END-IF
        //           END-IF.
        if (ws.getFlagUnzipTrovato().isNo()) {
            // COB_CODE: SET FINE-ELAB-SI           TO TRUE
            ws.getFlagFineElab().setSi();
            // COB_CODE: SET IVVC0211-GENERIC-ERROR TO TRUE
            inputIvvs0211.getIvvc0211RestoDati().getReturnCode().setGenericError();
            // COB_CODE: MOVE WK-PGM                TO IVVC0211-COD-SERVIZIO-BE
            inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: STRING 'VARIABILI NON TROVATE SU '
            //                  'VAR_FUNZ_DI_CALC_RS'
            //                  ' - '
            //                  'COMPAGNIA : '
            //                  ' -  '
            //                  WK-CODICE-COMPAGNIA
            //                  ' - '
            //                  'COD. PRODOTTO : '
            //                  WK-CODICE-PRODOTTO-VFC
            //                  ' -  '
            //                  'COD. TARIFFA : '
            //                  WK-CODICE-TARIFFA-AC5
            //                  ' -  '
            //                  'DATA INIZIO : '
            //                  WK-DATA-INIZIO-VFC
            //                  ' -  '
            //                  'NOMEFUNZIONE : '
            //                  WK-NOME-FUNZIONE-VFC
            //                  ' -  '
            //                  'STEP ELAB. : '
            //                  WK-STEP-ELAB-VFC
            //                  DELIMITED BY SIZE INTO
            //                  IVVC0211-DESCRIZ-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ivvc0211CampiEsito.Len.DESCRIZ_ERR, new String[] {"VARIABILI NON TROVATE SU ", "VAR_FUNZ_DI_CALC_RS", " - ", "COMPAGNIA : ", " -  ", ws.getWkCodiceCompagniaAsString(), " - ", "COD. PRODOTTO : ", ws.getWkCodiceProdottoVfcFormatted(), " -  ", "COD. TARIFFA : ", ws.getWkCodiceTariffaAc5Formatted(), " -  ", "DATA INIZIO : ", ws.getWkDataInizioVfcAsString(), " -  ", "NOMEFUNZIONE : ", ws.getWkNomeFunzioneVfcFormatted(), " -  ", "STEP ELAB. : ", ws.getWkStepElabVfcFormatted()});
            inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setDescrizErr(concatUtil.replaceInString(inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().getDescrizErrFormatted()));
            // COB_CODE: MOVE 'VAR_FUNZ_DI_CALC_R'
            //                                        TO IVVC0211-NOME-TABELLA
            inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setNomeTabella("VAR_FUNZ_DI_CALC_R");
        }
        else if (ws.getFlagGestione().isProdotto()) {
            // COB_CODE: IF GESTIONE-PRODOTTO
            //                TO IVVC0216-ELE-VARIABILI-MAX-P(IND-LIVELLO)
            //           ELSE
            //                TO IVVC0216-ELE-VARIABILI-MAX-T(IND-LIVELLO)
            //           END-IF
            // COB_CODE: MOVE UNZIP-ELE-VARIABILI-MAX
            //             TO IVVC0216-ELE-VARIABILI-MAX-P(IND-LIVELLO)
            ws.getAreaWorkIvvc0216().getC216TabValP().setEleVariabiliMaxP(ws.getIxIndici().getLivello(), ws.getUnzipStructure().getUnzipEleVariabiliMax());
        }
        else {
            // COB_CODE: MOVE UNZIP-ELE-VARIABILI-MAX
            //             TO IVVC0216-ELE-VARIABILI-MAX-T(IND-LIVELLO)
            ws.getAreaWorkIvvc0216().getC216TabValT().setEleVariabiliMaxT(ws.getIxIndici().getLivello(), ws.getUnzipStructure().getUnzipEleVariabiliMax());
        }
    }

    /**Original name: M510-VALORIZZA-VARIABILI-RIASS<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZA L'AREA VARIABILI CON L'OUTPUT RESTITUITO DALLA
	 *     LETTURA SULLA TABELLA VARIABILI FUNZIONI DI CALCOLO RIASS
	 * ----------------------------------------------------------------*</pre>*/
    private void m510ValorizzaVariabiliRiass() {
        // COB_CODE: IF AC5-ISPRECALC = PRESENZA-VARIABILI
        //              END-IF
        //           ELSE
        //              END-IF
        //           END-IF.
        if (ws.getVarFunzDiCalcR().getAc5Isprecalc().getAc5Isprecalc() == ws.getPresenzaVariabili()) {
            // COB_CODE: PERFORM L355-CARICA-LIVELLI-RIASS       THRU L355-EX
            l355CaricaLivelliRiass();
            // COB_CODE:         IF AC5-GLOVARLIST  = HIGH-VALUES  OR
            //                                        LOW-VALUE    OR
            //                                        SPACES
            //           *--->    VARIABILI NON PRESENTI
            //                      CONTINUE
            //                   ELSE
            //                      PERFORM M601-SCOMPATTA-GLOVAR-RIASS    THRU M601-EX
            //                   END-IF
            if (Characters.EQ_HIGH.test(ws.getVarFunzDiCalcR().getAc5Glovarlist(), VarFunzDiCalcR.Len.AC5_GLOVARLIST) || Characters.EQ_LOW.test(ws.getVarFunzDiCalcR().getAc5Glovarlist(), VarFunzDiCalcR.Len.AC5_GLOVARLIST) || Characters.EQ_SPACE.test(ws.getVarFunzDiCalcR().getAc5Glovarlist())) {
            //--->    VARIABILI NON PRESENTI
            // COB_CODE: CONTINUE
            //continue
            }
            else {
                // COB_CODE: PERFORM M601-SCOMPATTA-GLOVAR-RIASS    THRU M601-EX
                m601ScompattaGlovarRiass();
            }
        }
        else if (ws.getVarFunzDiCalcR().getAc5Isprecalc().getAc5Isprecalc() == ws.getPresenzaServizio()) {
            // COB_CODE: IF AC5-ISPRECALC = PRESENZA-SERVIZIO
            //              END-IF
            //           END-IF
            // COB_CODE: ADD 1                    TO IND-LIVELLO
            ws.getIxIndici().setLivello(Trunc.toShort(1 + ws.getIxIndici().getLivello(), 4));
            // COB_CODE: IF TIPO-OPZIONI
            //              END-IF
            //           END-IF
            if (ws.getFlagTipologiaScheda().isOpzioni()) {
                // COB_CODE: IF GESTIONE-PRODOTTO
                //                   TO IVVC0216-COD-TIPO-OPZIONE-P (IND-LIVELLO)
                //           ELSE
                //                   TO IVVC0216-COD-TIPO-OPZIONE-T (IND-LIVELLO)
                //           END-IF
                if (ws.getFlagGestione().isProdotto()) {
                    // COB_CODE: MOVE WOPZ-COD-TIPO-OPZIONE     (IND-TIPO-OPZ)
                    //                TO IVVC0216-COD-TIPO-OPZIONE-P (IND-LIVELLO)
                    ws.getAreaWorkIvvc0216().getC216TabValP().setIvvc0216CodTipoOpzioneP(ws.getIxIndici().getLivello(), ws.getAreaBusiness().getIvvc0217().getOpzioni(ws.getIxIndici().getTipoOpz()).getCodTipoOpzione());
                }
                else {
                    // COB_CODE: MOVE WOPZ-COD-TIPO-OPZIONE     (IND-TIPO-OPZ)
                    //                TO IVVC0216-COD-TIPO-OPZIONE-T (IND-LIVELLO)
                    ws.getAreaWorkIvvc0216().getC216TabValT().setIvvc0216CodTipoOpzioneT(ws.getIxIndici().getLivello(), ws.getAreaBusiness().getIvvc0217().getOpzioni(ws.getIxIndici().getTipoOpz()).getCodTipoOpzione());
                }
            }
            // COB_CODE: IF GESTIONE-PRODOTTO
            //                   TO IVVC0216-NOME-SERVIZIO-P(IND-LIVELLO)
            //           ELSE
            //                   TO IVVC0216-NOME-SERVIZIO-T(IND-LIVELLO)
            //           END-IF
            if (ws.getFlagGestione().isProdotto()) {
                // COB_CODE: MOVE AC5-NOMEPROGR
                //                TO IVVC0216-NOME-SERVIZIO-P(IND-LIVELLO)
                ws.getAreaWorkIvvc0216().getC216TabValP().setIvvc0216NomeServizioP(ws.getIxIndici().getLivello(), ws.getVarFunzDiCalcR().getAc5Nomeprogr());
            }
            else {
                // COB_CODE: MOVE AC5-NOMEPROGR
                //                TO IVVC0216-NOME-SERVIZIO-T(IND-LIVELLO)
                ws.getAreaWorkIvvc0216().getC216TabValT().setIvvc0216NomeServizioT(ws.getIxIndici().getLivello(), ws.getVarFunzDiCalcR().getAc5Nomeprogr());
            }
        }
    }

    /**Original name: M600-SCOMPATTA-GLOVAR<br>
	 * <pre>----------------------------------------------------------------*
	 *    SCOMPATTA GLOVAR
	 * ----------------------------------------------------------------*</pre>*/
    private void m600ScompattaGlovar() {
        // COB_CODE: PERFORM M501-PREPARA-UNZIP-GLOVAR   THRU M501-EX
        m501PreparaUnzipGlovar();
        // COB_CODE: PERFORM U999-UNZIP-STRING           THRU U999-EX
        u999UnzipString();
        // COB_CODE: IF IVVC0211-SUCCESSFUL-RC
        //              PERFORM M502-CARICA-GLOVAR-UNZIPPED THRU M502-EX
        //           END-IF.
        if (inputIvvs0211.getIvvc0211RestoDati().getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM M502-CARICA-GLOVAR-UNZIPPED THRU M502-EX
            m502CaricaGlovarUnzipped();
        }
    }

    /**Original name: M601-SCOMPATTA-GLOVAR-RIASS<br>
	 * <pre>----------------------------------------------------------------*
	 *    SCOMPATTA GLOVAR RIASS
	 * ----------------------------------------------------------------*</pre>*/
    private void m601ScompattaGlovarRiass() {
        // COB_CODE: PERFORM M503-PREPARA-UNZIP-GLOVAR-RS   THRU M503-EX
        m503PreparaUnzipGlovarRs();
        // COB_CODE: PERFORM U999-UNZIP-STRING              THRU U999-EX
        u999UnzipString();
        // COB_CODE: IF IVVC0211-SUCCESSFUL-RC
        //              PERFORM M504-CARICA-GLOVAR-UNZIPPED-RS THRU M504-EX
        //           END-IF.
        if (inputIvvs0211.getIvvc0211RestoDati().getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM M504-CARICA-GLOVAR-UNZIPPED-RS THRU M504-EX
            m504CaricaGlovarUnzippedRs();
        }
    }

    /**Original name: N000-CERCA-ADESIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *    CERCA ADESIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void n000CercaAdesione() {
        ConcatUtil concatUtil = null;
        // COB_CODE: IF GESTIONE-GARANZIE
        //             END-IF
        //           END-IF.
        if (ws.getFlagGestione().isGaranzie()) {
            // COB_CODE: IF  WPOL-TP-FRM-ASSVA = 'CO'
            //           AND (IVVC0211-TIPO-MOVIMENTO = 2031
            //             OR IVVC0211-TIPO-MOVIMENTO = 2001
            //             OR IVVC0211-TIPO-MOVIMENTO = 2339
            //             OR IVVC0211-TIPO-MOVIMENTO = 2340
            //             OR IVVC0211-TIPO-MOVIMENTO = 2006 )
            //              CONTINUE
            //           ELSE
            //            END-IF
            //           END-IF
            if (Conditions.eq(ws.getAreaBusiness().getLccvpol1().getDati().getWpolTpFrmAssva(), "CO") && (inputIvvs0211.getIvvc0211DatiInput().getTipoMovimento() == 2031 || inputIvvs0211.getIvvc0211DatiInput().getTipoMovimento() == 2001 || inputIvvs0211.getIvvc0211DatiInput().getTipoMovimento() == 2339 || inputIvvs0211.getIvvc0211DatiInput().getTipoMovimento() == 2340 || inputIvvs0211.getIvvc0211DatiInput().getTipoMovimento() == 2006)) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (!Functions.isNumber(ws.getAreaBusiness().getLccvade1().getDati().getWadeIdAdes()) || ws.getAreaBusiness().getLccvade1().getDati().getWadeIdAdes() == 0) {
                // COB_CODE: IF WADE-ID-ADES NOT NUMERIC OR
                //              WADE-ID-ADES = ZEROES
                //               END-STRING
                //           END-IF
                // COB_CODE: SET IVVC0211-AREA-NOT-FOUND TO TRUE
                inputIvvs0211.getIvvc0211RestoDati().getReturnCode().setAreaNotFound();
                // COB_CODE: MOVE WK-PGM             TO IVVC0211-COD-SERVIZIO-BE
                inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: STRING 'ADESIONI NON TROVATE'
                //                  DELIMITED BY SIZE INTO
                //                  IVVC0211-DESCRIZ-ERR
                //           END-STRING
                inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setDescrizErr("ADESIONI NON TROVATE");
            }
        }
    }

    /**Original name: S000-VALORIZZA-TIPO-SKEDE<br>
	 * <pre>----------------------------------------------------------------*
	 *    VALORIZZA TIPO FLAG-SKEDE
	 * ----------------------------------------------------------------*</pre>*/
    private void s000ValorizzaTipoSkede() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE MVV-TIPO-OGGETTO             TO FLAG-SKEDE
        ws.getFlagSkede().setFlagSkede(ws.getMatrValVar().getMvvTipoOggetto());
        // COB_CODE: IF IVVC0211-IN-CONV
        //              END-IF
        //           END-IF.
        if (inputIvvs0211.getIvvc0211DatiInput().getStepElab().isInConv()) {
            // COB_CODE: IF NOT SKEDE-PRODOTTI AND
            //              NOT SKEDE-TOTALI
            //               MOVE 'MATR_VAL_VAR'      TO IVVC0211-NOME-TABELLA
            //           END-IF
            if (!ws.getFlagSkede().isProdotti() && !ws.getFlagSkede().isTotali()) {
                // COB_CODE: SET FINE-ELAB-SI           TO TRUE
                ws.getFlagFineElab().setSi();
                // COB_CODE: SET IVVC0211-GENERIC-ERROR TO TRUE
                inputIvvs0211.getIvvc0211RestoDati().getReturnCode().setGenericError();
                // COB_CODE: MOVE WK-PGM                TO IVVC0211-COD-SERVIZIO-BE
                inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: STRING 'TIPO_OGGETTO NON VALIDO'
                //                  ' - '
                //                  'COMPAGNIA : '
                //                  WK-CODICE-COMPAGNIA
                //                  ' - '
                //                  'TIPO MOVIM. : '
                //                  WK-TIPO-MOVIMENTO
                //                  ' - '
                //                  'STEP VALOR. : '
                //                  MVV-STEP-VALORIZZATORE
                //                  ' - '
                //                  'STEP CONVER. : '
                //                  MVV-STEP-CONVERSAZIONE
                //                  DELIMITED BY SIZE INTO
                //                  IVVC0211-DESCRIZ-ERR
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Ivvc0211CampiEsito.Len.DESCRIZ_ERR, new String[] {"TIPO_OGGETTO NON VALIDO", " - ", "COMPAGNIA : ", ws.getWkCodiceCompagniaAsString(), " - ", "TIPO MOVIM. : ", ws.getWkTipoMovimentoAsString(), " - ", "STEP VALOR. : ", String.valueOf(ws.getMatrValVar().getMvvStepValorizzatore()), " - ", "STEP CONVER. : ", String.valueOf(ws.getMatrValVar().getMvvStepConversazione())});
                inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setDescrizErr(concatUtil.replaceInString(inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().getDescrizErrFormatted()));
                // COB_CODE: MOVE 'MATR_VAL_VAR'      TO IVVC0211-NOME-TABELLA
                inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setNomeTabella("MATR_VAL_VAR");
            }
        }
    }

    /**Original name: U999-UNZIP-STRING<br>
	 * <pre>----------------------------------------------------------------*
	 *    ESTRAZIONE NOME VARIABILE DALLA STRINGA GLOBALVARLIST
	 * ----------------------------------------------------------------*
	 * --> IND-START-VAR  = INDICA IL PRIMO CHAR DI UNA VARIABILE(FISSO)
	 * --> IND-END-VAR    = INDICA L'ULTIMO CHAR DI UNA VARIABILE(VAR)
	 * --> IND-UNZIP      = E' IL CONTATORE DI VARIABILI TROVATE DA USARE
	 *                      PER LA OCCURS 100 DI OUTPUT(AREA VARIABILI)
	 * --> IND-CHAR       = INDICA LA LUNGHEZZA DI UNA VARIABILE
	 * --> WK-GLOVAR-MAX</pre>*/
    private void u999UnzipString() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 1                             TO IND-START-VAR.
        ws.getIxIndici().setStartVar(((short)1));
        // COB_CODE: MOVE ZEROES                        TO IND-CHAR
        ws.getIxIndici().setCharFld(((short)0));
        // COB_CODE: IF RIASS-SI
        //              CONTINUE
        //           ELSE
        //              MOVE ZEROES                     TO IND-UNZIP
        //           END-IF.
        if (ws.getFlagRiass().isSi()) {
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE ZEROES                     TO IND-UNZIP
            ws.getIxIndici().setUnzip(((short)0));
        }
        // COB_CODE: SET NO-FINE-VARLIST                TO TRUE.
        ws.getWkVarlist().setNoFineVarlist();
        // COB_CODE: PERFORM VARYING IND-END-VAR FROM 1 BY 1
        //                     UNTIL IND-END-VAR > UNZIP-LENGTH-STR-MAX
        //                        OR SI-FINE-VARLIST
        //             END-IF
        //           END-PERFORM.
        ws.getIxIndici().setEndVar(((short)1));
        while (!(ws.getIxIndici().getEndVar() > ws.getUnzipStructure().getUnzipLengthStrMax() || ws.getWkVarlist().isSiFineVarlist())) {
            // COB_CODE:        IF UNZIP-STRING-ZIPPED(IND-END-VAR:1) =
            //                                                  UNZIP-IDENTIFICATORE OR
            //                                                  SPACES               OR
            //                                                  LOW-VALUE            OR
            //                                                  HIGH-VALUE
            //           *-->   E' STATA TROVATA UNA VARIABILE E VIENE CARICATA IN
            //           *-->   OUTPUT
            //                     END-IF
            //                  ELSE
            //                     END-IF
            //                  END-IF
            if (Conditions.eq(ws.getUnzipStructure().getUnzipStringZippedFormatted().substring((ws.getIxIndici().getEndVar()) - 1, ws.getIxIndici().getEndVar()), rTrim(String.valueOf(ws.getUnzipStructure().getUnzipIdentificatore()))) || Conditions.eq(ws.getUnzipStructure().getUnzipStringZippedFormatted().substring((ws.getIxIndici().getEndVar()) - 1, ws.getIxIndici().getEndVar()), "") || Conditions.eq(ws.getUnzipStructure().getUnzipStringZippedFormatted().substring((ws.getIxIndici().getEndVar()) - 1, ws.getIxIndici().getEndVar()), LiteralGenerator.create(Types.LOW_CHAR_VAL, 1)) || Conditions.eq(ws.getUnzipStructure().getUnzipStringZippedFormatted().substring((ws.getIxIndici().getEndVar()) - 1, ws.getIxIndici().getEndVar()), LiteralGenerator.create(Types.HIGH_CHAR_VAL, 1))) {
                //-->   E' STATA TROVATA UNA VARIABILE E VIENE CARICATA IN
                //-->   OUTPUT
                // COB_CODE: IF IND-CHAR > 0
                //              END-IF
                //           END-IF
                if (ws.getIxIndici().getCharFld() > 0) {
                    // COB_CODE: ADD  1                         TO IND-UNZIP
                    ws.getIxIndici().setUnzip(Trunc.toShort(1 + ws.getIxIndici().getUnzip(), 4));
                    // COB_CODE: MOVE UNZIP-STRING-ZIPPED(IND-START-VAR:IND-CHAR)
                    //             TO UNZIP-COD-VARIABILE(IND-UNZIP)
                    //                                   (1:UNZIP-LENGTH-FIELD)
                    ws.getUnzipStructure().getUnzipTab().setCodVariabile(ws.getIxIndici().getUnzip(), Functions.setSubstring(ws.getUnzipStructure().getUnzipTab().getCodVariabile(ws.getIxIndici().getUnzip()), ws.getUnzipStructure().getUnzipStringZippedFormatted().substring((ws.getIxIndici().getStartVar()) - 1, ws.getIxIndici().getStartVar() + ws.getIxIndici().getCharFld() - 1), 1, ws.getUnzipStructure().getUnzipLengthField()));
                    // COB_CODE: COMPUTE IND-START-VAR = IND-END-VAR + 1
                    ws.getIxIndici().setStartVar(Trunc.toShort(ws.getIxIndici().getEndVar() + 1, 4));
                    // COB_CODE: MOVE ZEROES                          TO IND-CHAR
                    ws.getIxIndici().setCharFld(((short)0));
                    // COB_CODE: IF UNZIP-STRING-ZIPPED(IND-END-VAR:1) = SPACES   OR
                    //                                                LOW-VALUE   OR
                    //                                                HIGH-VALUE
                    //              SET SI-FINE-VARLIST               TO TRUE
                    //           END-IF
                    if (Conditions.eq(ws.getUnzipStructure().getUnzipStringZippedFormatted().substring((ws.getIxIndici().getEndVar()) - 1, ws.getIxIndici().getEndVar()), "") || Conditions.eq(ws.getUnzipStructure().getUnzipStringZippedFormatted().substring((ws.getIxIndici().getEndVar()) - 1, ws.getIxIndici().getEndVar()), LiteralGenerator.create(Types.LOW_CHAR_VAL, 1)) || Conditions.eq(ws.getUnzipStructure().getUnzipStringZippedFormatted().substring((ws.getIxIndici().getEndVar()) - 1, ws.getIxIndici().getEndVar()), LiteralGenerator.create(Types.HIGH_CHAR_VAL, 1))) {
                        // COB_CODE: SET SI-FINE-VARLIST               TO TRUE
                        ws.getWkVarlist().setSiFineVarlist();
                    }
                }
            }
            else {
                // COB_CODE: ADD  1                     TO IND-CHAR
                ws.getIxIndici().setCharFld(Trunc.toShort(1 + ws.getIxIndici().getCharFld(), 4));
                // COB_CODE: IF IND-END-VAR = UNZIP-LENGTH-STR-MAX
                //                                      (1:UNZIP-LENGTH-FIELD)
                //           END-IF
                if (ws.getIxIndici().getEndVar() == ws.getUnzipStructure().getUnzipLengthStrMax()) {
                    // COB_CODE: ADD  1                         TO IND-UNZIP
                    ws.getIxIndici().setUnzip(Trunc.toShort(1 + ws.getIxIndici().getUnzip(), 4));
                    // COB_CODE: MOVE UNZIP-STRING-ZIPPED(IND-START-VAR:IND-CHAR)
                    //             TO UNZIP-COD-VARIABILE(IND-UNZIP)
                    //                                   (1:UNZIP-LENGTH-FIELD)
                    ws.getUnzipStructure().getUnzipTab().setCodVariabile(ws.getIxIndici().getUnzip(), Functions.setSubstring(ws.getUnzipStructure().getUnzipTab().getCodVariabile(ws.getIxIndici().getUnzip()), ws.getUnzipStructure().getUnzipStringZippedFormatted().substring((ws.getIxIndici().getStartVar()) - 1, ws.getIxIndici().getStartVar() + ws.getIxIndici().getCharFld() - 1), 1, ws.getUnzipStructure().getUnzipLengthField()));
                }
            }
            ws.getIxIndici().setEndVar(Trunc.toShort(ws.getIxIndici().getEndVar() + 1, 4));
        }
        // COB_CODE: IF GESTIONE-PRODOTTO
        //              END-IF
        //           ELSE
        //              END-IF
        //           END-IF.
        if (ws.getFlagGestione().isProdotto()) {
            // COB_CODE: IF IND-UNZIP > IVVC0216-NUM-MAX-VARIABILI-P
            //              MOVE 'IVVC0216'             TO IVVC0211-NOME-TABELLA
            //           ELSE
            //              MOVE IND-UNZIP              TO UNZIP-ELE-VARIABILI-MAX
            //           END-IF
            if (ws.getIxIndici().getUnzip() > ws.getAreaWorkIvvc0216().getC216NumMaxVariabiliP()) {
                // COB_CODE: SET FINE-ELAB-SI           TO TRUE
                ws.getFlagFineElab().setSi();
                // COB_CODE: SET IVVC0211-GENERIC-ERROR TO TRUE
                inputIvvs0211.getIvvc0211RestoDati().getReturnCode().setGenericError();
                // COB_CODE: MOVE WK-PGM                TO IVVC0211-COD-SERVIZIO-BE
                inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: STRING 'OVERFLOW TABELLA VARIABILI '
                //                   'IVVC0216-AREA-VARIABILI'
                //                   ' - '
                //                   'COMPAGNIA : '
                //                   WK-CODICE-COMPAGNIA
                //                   ' - '
                //                   'TIPO MOVIM. : '
                //                   WK-TIPO-MOVIMENTO
                //                   ' - '
                //                   'STEP VALOR. : '
                //                   MVV-STEP-VALORIZZATORE
                //                   ' - '
                //                   'STEP CONVER. : '
                //                   MVV-STEP-CONVERSAZIONE
                //                   DELIMITED BY SIZE INTO
                //                   IVVC0211-DESCRIZ-ERR
                //            END-STRING
                concatUtil = ConcatUtil.buildString(Ivvc0211CampiEsito.Len.DESCRIZ_ERR, new String[] {"OVERFLOW TABELLA VARIABILI ", "IVVC0216-AREA-VARIABILI", " - ", "COMPAGNIA : ", ws.getWkCodiceCompagniaAsString(), " - ", "TIPO MOVIM. : ", ws.getWkTipoMovimentoAsString(), " - ", "STEP VALOR. : ", String.valueOf(ws.getMatrValVar().getMvvStepValorizzatore()), " - ", "STEP CONVER. : ", String.valueOf(ws.getMatrValVar().getMvvStepConversazione())});
                inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setDescrizErr(concatUtil.replaceInString(inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().getDescrizErrFormatted()));
                // COB_CODE: MOVE 'IVVC0216'             TO IVVC0211-NOME-TABELLA
                inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setNomeTabella("IVVC0216");
            }
            else {
                // COB_CODE: MOVE IND-UNZIP              TO UNZIP-ELE-VARIABILI-MAX
                ws.getUnzipStructure().setUnzipEleVariabiliMax(ws.getIxIndici().getUnzip());
            }
        }
        else if (ws.getIxIndici().getUnzip() > ws.getAreaWorkIvvc0216().getC216NumMaxVariabiliT()) {
            // COB_CODE: IF IND-UNZIP > IVVC0216-NUM-MAX-VARIABILI-T
            //              MOVE 'IVVC0216'             TO IVVC0211-NOME-TABELLA
            //           ELSE
            //              MOVE IND-UNZIP              TO UNZIP-ELE-VARIABILI-MAX
            //           END-IF
            // COB_CODE: SET FINE-ELAB-SI           TO TRUE
            ws.getFlagFineElab().setSi();
            // COB_CODE: SET IVVC0211-GENERIC-ERROR TO TRUE
            inputIvvs0211.getIvvc0211RestoDati().getReturnCode().setGenericError();
            // COB_CODE: MOVE WK-PGM                TO IVVC0211-COD-SERVIZIO-BE
            inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: STRING 'OVERFLOW TABELLA VARIABILI '
            //                   'IVVC0216-AREA-VARIABILI'
            //                   ' - '
            //                   'COMPAGNIA : '
            //                   WK-CODICE-COMPAGNIA
            //                   ' - '
            //                   'TIPO MOVIM. : '
            //                   WK-TIPO-MOVIMENTO
            //                   ' - '
            //                   'STEP VALOR. : '
            //                   MVV-STEP-VALORIZZATORE
            //                   ' - '
            //                   'STEP CONVER. : '
            //                   MVV-STEP-CONVERSAZIONE
            //                   DELIMITED BY SIZE INTO
            //                   IVVC0211-DESCRIZ-ERR
            //            END-STRING
            concatUtil = ConcatUtil.buildString(Ivvc0211CampiEsito.Len.DESCRIZ_ERR, new String[] {"OVERFLOW TABELLA VARIABILI ", "IVVC0216-AREA-VARIABILI", " - ", "COMPAGNIA : ", ws.getWkCodiceCompagniaAsString(), " - ", "TIPO MOVIM. : ", ws.getWkTipoMovimentoAsString(), " - ", "STEP VALOR. : ", String.valueOf(ws.getMatrValVar().getMvvStepValorizzatore()), " - ", "STEP CONVER. : ", String.valueOf(ws.getMatrValVar().getMvvStepConversazione())});
            inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setDescrizErr(concatUtil.replaceInString(inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().getDescrizErrFormatted()));
            // COB_CODE: MOVE 'IVVC0216'             TO IVVC0211-NOME-TABELLA
            inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setNomeTabella("IVVC0216");
        }
        else {
            // COB_CODE: MOVE IND-UNZIP              TO UNZIP-ELE-VARIABILI-MAX
            ws.getUnzipStructure().setUnzipEleVariabiliMax(ws.getIxIndici().getUnzip());
        }
    }

    /**Original name: P000-ESTRAI-AUT-OPER<br>
	 * <pre>----------------------------------------------------------------*
	 *     ESTRAI AUTONOMIA OPERATIVA
	 * ----------------------------------------------------------------*</pre>*/
    private void p000EstraiAutOper() {
        Ivvs0212 ivvs0212 = null;
        // COB_CODE: SET  IDSV8888-BUSINESS-DBG          TO TRUE
        ws.getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
        // COB_CODE: MOVE 'IVVS0212'                 TO IDSV8888-NOME-PGM.
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("IVVS0212");
        // COB_CODE: SET  IDSV8888-INIZIO            TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setInizio();
        // COB_CODE: MOVE SPACES                     TO IDSV8888-DESC-PGM
        //skipped translation for moving SPACES to IDSV8888-DESC-PGM; considered in STRING statement translation below
        // COB_CODE: STRING 'Valorizzatore variabili'
        //                   DELIMITED BY SIZE
        //                   INTO IDSV8888-DESC-PGM
        //           END-STRING
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("Valorizzatore variabili");
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: CALL PGM-IVVS0212         USING IDSV0003
        //                                           AREA-IVVV0212.
        ivvs0212 = Ivvs0212.getInstance();
        ivvs0212.run(ws.getIdsv0003(), ws.getAreaIvvv0212());
        // COB_CODE: MOVE IDSV0003-RETURN-CODE TO IVVC0211-RETURN-CODE
        inputIvvs0211.getIvvc0211RestoDati().getReturnCode().setReturnCode(ws.getIdsv0003().getReturnCode().getReturnCode());
        // COB_CODE: MOVE IDSV0003-CAMPI-ESITO TO IVVC0211-CAMPI-ESITO.
        inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setCampiEsitoBytes(ws.getIdsv0003().getCampiEsito().getCampiEsitoBytes());
        // COB_CODE: SET  IDSV8888-BUSINESS-DBG          TO TRUE
        ws.getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
        // COB_CODE: MOVE 'IVVS0212'                 TO IDSV8888-NOME-PGM.
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("IVVS0212");
        // COB_CODE: SET  IDSV8888-FINE              TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setFine();
        // COB_CODE: MOVE SPACES                     TO IDSV8888-DESC-PGM
        //skipped translation for moving SPACES to IDSV8888-DESC-PGM; considered in STRING statement translation below
        // COB_CODE: STRING 'Valorizzatore variabili'
        //                   DELIMITED BY SIZE
        //                   INTO IDSV8888-DESC-PGM
        //           END-STRING
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("Valorizzatore variabili");
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
    }

    /**Original name: P100-ELABORA-VARIABILI<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE VARIABILI
	 * ----------------------------------------------------------------*</pre>*/
    private void p100ElaboraVariabili() {
        Ivvs0216 ivvs0216 = null;
        // COB_CODE: MOVE IVVC0211-TRATTAMENTO-STORICITA
        //             TO IDSV0003-TRATTAMENTO-STORICITA.
        ws.getIdsv0003().getTrattamentoStoricita().setTrattamentoStoricita(inputIvvs0211.getIvvc0211DatiInput().getTrattamentoStoricita());
        //    MOVE IVVC0216-FLG-AREA
        //      TO IVVC0211-FLG-AREA
        // COB_CODE: MOVE IVVC0211-FLG-AREA
        //             TO IVVC0216-FLG-AREA
        ws.getIvvc0216FlgArea().setIvvc0216FlgArea(inputIvvs0211.getIvvc0211DatiInput().getFlgArea().getFlgArea());
        // COB_CODE: MOVE IVVC0211-AREA-ADDRESSES TO IDSV0101-AREA-ADDRESSES
        ws.getAreaIdsv0101().setAreaAddressesBytes(inputIvvs0211.getIvvc0211RestoDati().getAreaAddressesBytes());
        //
        // COB_CODE: MOVE IVVC0211-TIPO-MOVI-ORIG TO WK-MOVI-ORIG
        ws.getAreaBusiness().setWkMoviOrigFormatted(inputIvvs0211.getIvvc0211DatiInput().getTipoMoviOrigFormatted());
        //
        // COB_CODE: IF IVVC0216-ELE-LIVELLO-MAX-P > 0
        //           OR IVVC0216-ELE-LIVELLO-MAX-T > 0
        //              MOVE 'IVVS0211'           TO IVVC0211-COD-SERVIZIO-BE
        //           END-IF.
        if (ws.getAreaWorkIvvc0216().getC216EleLivelloMaxP() > 0 || ws.getAreaWorkIvvc0216().getC216EleLivelloMaxT() > 0) {
            // COB_CODE: SET  IDSV8888-BUSINESS-DBG      TO TRUE
            ws.getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
            // COB_CODE: MOVE 'IVVS0216'                 TO IDSV8888-NOME-PGM
            ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("IVVS0216");
            // COB_CODE: SET  IDSV8888-INIZIO            TO TRUE
            ws.getIdsv8888().getStrPerformanceDbg().setInizio();
            // COB_CODE: MOVE SPACES                     TO IDSV8888-DESC-PGM
            //skipped translation for moving SPACES to IDSV8888-DESC-PGM; considered in STRING statement translation below
            // COB_CODE: STRING 'Valorizzatore variabili'
            //                   DELIMITED BY SIZE
            //                   INTO IDSV8888-DESC-PGM
            //           END-STRING
            ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("Valorizzatore variabili");
            // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX
            eseguiDisplay();
            // COB_CODE: CALL PGM-IVVS0216         USING IDSV0003
            //                                           IVVC0216-FLG-AREA
            //                                           AREA-WORK-IVVC0216
            //                                           AREA-WARNING-IVVC0216
            //                                           AREA-BUSINESS
            //                                           AREA-IDSV0101
            ivvs0216 = Ivvs0216.getInstance();
            ivvs0216.run(new Object[] {ws.getIdsv0003(), ws.getIvvc0216FlgArea(), ws.getAreaWorkIvvc0216(), ws.getIvvc0215(), ws.getAreaBusiness(), ws.getAreaIdsv0101()});
            // COB_CODE: SET  IDSV8888-BUSINESS-DBG      TO TRUE
            ws.getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
            // COB_CODE: MOVE 'IVVS0216'                 TO IDSV8888-NOME-PGM
            ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("IVVS0216");
            // COB_CODE: SET  IDSV8888-FINE              TO TRUE
            ws.getIdsv8888().getStrPerformanceDbg().setFine();
            // COB_CODE: MOVE SPACES                     TO IDSV8888-DESC-PGM
            //skipped translation for moving SPACES to IDSV8888-DESC-PGM; considered in STRING statement translation below
            // COB_CODE: STRING 'Valorizzatore variabili'
            //                   DELIMITED BY SIZE
            //                   INTO IDSV8888-DESC-PGM
            //           END-STRING
            ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("Valorizzatore variabili");
            // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX
            eseguiDisplay();
            // COB_CODE: MOVE IDSV0003-RETURN-CODE TO IVVC0211-RETURN-CODE
            inputIvvs0211.getIvvc0211RestoDati().getReturnCode().setReturnCode(ws.getIdsv0003().getReturnCode().getReturnCode());
            // COB_CODE: MOVE IDSV0003-CAMPI-ESITO TO IVVC0211-CAMPI-ESITO
            inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setCampiEsitoBytes(ws.getIdsv0003().getCampiEsito().getCampiEsitoBytes());
            // COB_CODE: MOVE IVVC0216-AREA-VAR-KO TO IVVC0211-AREA-VAR-KO
            inputIvvs0211.getIvvc0211RestoDati().setAreaVarKoBytes(ws.getIvvc0215().getAreaVarKoBytes());
            // COB_CODE: MOVE 'IVVS0211'           TO IVVC0211-COD-SERVIZIO-BE
            inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setCodServizioBe("IVVS0211");
        }
    }

    /**Original name: A900-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *     OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void a900OperazioniFinali() {
        // COB_CODE: IF IVVC0211-SUCCESSFUL-RC
        //              END-IF
        //           ELSE
        //              END-IF
        //           END-IF.
        if (inputIvvs0211.getIvvc0211RestoDati().getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SPACES               TO IVVC0211-BUFFER-DATI
            inputIvvs0211.getIvvc0211RestoDati().setBufferDati("");
            // COB_CODE: PERFORM A950-TRAVASO-AREA-IVVC0216
            //              THRU A950-EX
            a950TravasoAreaIvvc0216();
            // COB_CODE: MOVE AREA-IVVC0216        TO IVVC0211-BUFFER-DATI
            inputIvvs0211.getIvvc0211RestoDati().setBufferDati(ws.getAreaIvvc0216Formatted());
            // COB_CODE: IF  WK-GLOVARLIST-VUOTA
            //               SET IVVC0211-GLOVARLIST-VUOTA  TO  TRUE
            //           END-IF
            if (ws.getWkGlovarlist().isVuota()) {
                // COB_CODE: SET IVVC0211-GLOVARLIST-VUOTA  TO  TRUE
                inputIvvs0211.getIvvc0211RestoDati().getReturnCode().setGlovarlistVuota();
            }
            // COB_CODE: IF IVVC0211-PRE-CONV
            //           OR IVVC0211-POST-CONV
            //              END-IF
            //           END-IF
            if (inputIvvs0211.getIvvc0211DatiInput().getStepElab().isPreConv() || inputIvvs0211.getIvvc0211DatiInput().getStepElab().isPostConv()) {
                // COB_CODE: IF (WK-VAR-FUNZ-TROVATA-NO AND
                //               STAT-CAUS-TROVATI-NO   )
                //           OR (WK-VAR-FUNZ-TROVATA-NO AND
                //               STAT-CAUS-TROVATI-SI   )
                //               SET IVVC0211-SKIP-CALL-ACTUATOR TO TRUE
                //           END-IF
                if (ws.getWkVarFunzTrovata().isNo() && ws.getFlagStatCaus().isNo() || ws.getWkVarFunzTrovata().isNo() && ws.getFlagStatCaus().isSi()) {
                    // COB_CODE: SET IVVC0211-SKIP-CALL-ACTUATOR TO TRUE
                    inputIvvs0211.getIvvc0211RestoDati().getReturnCode().setSkipCallActuator();
                }
            }
        }
        else {
            // COB_CODE: IF IVVC0211-CALCOLO-NON-COMPLETO
            //              MOVE AREA-IVVC0216     TO IVVC0211-BUFFER-DATI
            //           END-IF
            if (inputIvvs0211.getIvvc0211RestoDati().getReturnCode().isCalcoloNonCompleto()) {
                // COB_CODE: SET IVVC0211-SUCCESSFUL-RC TO TRUE
                inputIvvs0211.getIvvc0211RestoDati().getReturnCode().setSuccessfulRc();
                // COB_CODE: INITIALIZE             IVVC0211-AREA-VAR-KO
                initAreaVarKo();
                // COB_CODE: MOVE SPACES            TO IVVC0211-BUFFER-DATI
                inputIvvs0211.getIvvc0211RestoDati().setBufferDati("");
                // COB_CODE: PERFORM A950-TRAVASO-AREA-IVVC0216
                //              THRU A950-EX
                a950TravasoAreaIvvc0216();
                // COB_CODE: MOVE AREA-IVVC0216     TO IVVC0211-BUFFER-DATI
                inputIvvs0211.getIvvc0211RestoDati().setBufferDati(ws.getAreaIvvc0216Formatted());
            }
            // COB_CODE: IF IVVC0211-CACHE-PIENA
            //              MOVE AREA-IVVC0216     TO IVVC0211-BUFFER-DATI
            //           END-IF
            if (inputIvvs0211.getIvvc0211RestoDati().getReturnCode().isCachePiena()) {
                // COB_CODE: INITIALIZE             IVVC0211-AREA-VAR-KO
                initAreaVarKo();
                // COB_CODE: MOVE SPACES            TO IVVC0211-BUFFER-DATI
                inputIvvs0211.getIvvc0211RestoDati().setBufferDati("");
                // COB_CODE: PERFORM A950-TRAVASO-AREA-IVVC0216
                //              THRU A950-EX
                a950TravasoAreaIvvc0216();
                // COB_CODE: MOVE AREA-IVVC0216     TO IVVC0211-BUFFER-DATI
                inputIvvs0211.getIvvc0211RestoDati().setBufferDati(ws.getAreaIvvc0216Formatted());
            }
        }
    }

    /**Original name: A950-TRAVASO-AREA-IVVC0216<br>
	 * <pre>----------------------------------------------------------------*
	 *     TRAVASO AREA DI WORK IVVC0216 PREDISPOSTA CON 100 VARIABILI
	 *     NELL'AREA CON 60 VARIABILI RESTITUITA AL PGM CHIAMANTE
	 * ----------------------------------------------------------------*</pre>*/
    private void a950TravasoAreaIvvc0216() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE IVVC0216-ELE-LIVELLO-MAX-P  TO C216-ELE-LIVELLO-MAX-P.
        ws.getIvvc0216().setC216EleLivelloMaxP(ws.getAreaWorkIvvc0216().getC216EleLivelloMaxP());
        // COB_CODE: MOVE IVVC0216-ELE-LIVELLO-MAX-T  TO C216-ELE-LIVELLO-MAX-T.
        ws.getIvvc0216().setC216EleLivelloMaxT(ws.getAreaWorkIvvc0216().getC216EleLivelloMaxT());
        // COB_CODE: MOVE IVVC0216-DEE                TO C216-DEE
        ws.getIvvc0216().setC216Dee(ws.getAreaWorkIvvc0216().getC216Dee());
        // COB_CODE:      PERFORM VARYING IND-RIGA FROM 1 BY 1
        //                        UNTIL IND-RIGA > IVVC0216-ELE-LIVELLO-MAX-P
        //                           OR IND-RIGA > IVVC0216-NUM-MAX-LIVELLI-P
        //                           OR NOT IVVC0211-SUCCESSFUL-RC
        //                             TO C216-NOME-SERVIZIO-P(IND-RIGA)
        //           *            END-IF
        //                END-PERFORM.
        ws.getIxIndici().setRiga(((short)1));
        while (!(ws.getIxIndici().getRiga() > ws.getAreaWorkIvvc0216().getC216EleLivelloMaxP() || ws.getIxIndici().getRiga() > ws.getAreaWorkIvvc0216().getIvvc0216NumMaxLivelliP() || !inputIvvs0211.getIvvc0211RestoDati().getReturnCode().isSuccessfulRc())) {
            // COB_CODE: MOVE IVVC0216-ID-POL-P(IND-RIGA)
            //             TO C216-ID-POL-P(IND-RIGA)
            ws.getIvvc0216().getC216TabValP().setIvvc0216IdPolPFormatted(ws.getIxIndici().getRiga(), ws.getAreaWorkIvvc0216().getC216TabValP().getIdPolPFormatted(ws.getIxIndici().getRiga()));
            // COB_CODE: MOVE IVVC0216-COD-TIPO-OPZIONE-P(IND-RIGA)
            //             TO C216-COD-TIPO-OPZIONE-P(IND-RIGA)
            ws.getIvvc0216().getC216TabValP().setIvvc0216CodTipoOpzioneP(ws.getIxIndici().getRiga(), ws.getAreaWorkIvvc0216().getC216TabValP().getCodTipoOpzioneP(ws.getIxIndici().getRiga()));
            // COB_CODE: MOVE IVVC0216-TP-LIVELLO-P(IND-RIGA)
            //             TO C216-TP-LIVELLO-P(IND-RIGA)
            ws.getIvvc0216().getC216TabValP().setIvvc0216TpLivelloP(ws.getIxIndici().getRiga(), ws.getAreaWorkIvvc0216().getC216TabValP().getTpLivelloP(ws.getIxIndici().getRiga()));
            // COB_CODE: MOVE IVVC0216-COD-LIVELLO-P(IND-RIGA)
            //             TO C216-COD-LIVELLO-P(IND-RIGA)
            ws.getIvvc0216().getC216TabValP().setIvvc0216CodLivelloP(ws.getIxIndici().getRiga(), ws.getAreaWorkIvvc0216().getC216TabValP().getCodLivelloP(ws.getIxIndici().getRiga()));
            //  INFORMAZIONI DI TRANCHE
            // COB_CODE: MOVE IVVC0216-ID-LIVELLO-P(IND-RIGA)
            //             TO C216-ID-LIVELLO-P(IND-RIGA)
            ws.getIvvc0216().getC216TabValP().setIvvc0216IdLivelloPFormatted(ws.getIxIndici().getRiga(), ws.getAreaWorkIvvc0216().getC216TabValP().getIdLivelloPFormatted(ws.getIxIndici().getRiga()));
            // COB_CODE: MOVE IVVC0216-DT-INIZ-PROD-P(IND-RIGA)
            //             TO C216-DT-INIZ-PROD-P(IND-RIGA)
            ws.getIvvc0216().getC216TabValP().setIvvc0216DtInizProdP(ws.getIxIndici().getRiga(), ws.getAreaWorkIvvc0216().getC216TabValP().getDtInizProdP(ws.getIxIndici().getRiga()));
            // COB_CODE: MOVE IVVC0216-COD-RGM-FISC-P(IND-RIGA)
            //             TO C216-COD-RGM-FISC-P(IND-RIGA)
            ws.getIvvc0216().getC216TabValP().setIvvc0216CodRgmFiscP(ws.getIxIndici().getRiga(), ws.getAreaWorkIvvc0216().getC216TabValP().getCodRgmFiscP(ws.getIxIndici().getRiga()));
            // AREA VARIABILI PRODOTTO
            // COB_CODE: MOVE IVVC0216-ELE-VARIABILI-MAX-P(IND-RIGA)
            //             TO C216-ELE-VARIABILI-MAX-P(IND-RIGA)
            //                WK-ELE-VARIABILI-MAX
            ws.getIvvc0216().getC216TabValP().setEleVariabiliMaxP(ws.getIxIndici().getRiga(), ws.getAreaWorkIvvc0216().getC216TabValP().getEleVariabiliMaxP(ws.getIxIndici().getRiga()));
            ws.setWkEleVariabiliMax(TruncAbs.toShort(ws.getAreaWorkIvvc0216().getC216TabValP().getEleVariabiliMaxP(ws.getIxIndici().getRiga()), 3));
            //   CONTROLLO DI OVERFLOW IN CASO DI VENDITA O
            //   POST-VENDITA SUL NUMERO DI VARIABILI RESTITUITE
            // COB_CODE:              IF  (IVVC0216-ELE-VARIABILI-MAX-P(IND-RIGA) >
            //                             IVVC0216-NUM-MAX-VARIABILI-P )
            //                           END-STRING
            //                        ELSE
            //           *               IF IVVC0211-FL-AREA-VAR-EXTRA-SI
            //           *
            //           *                  PERFORM A951-GESTIONE-VARIABILI-EXTRA
            //           *                     THRU A951-EX
            //           *
            //           *               ELSE
            //                              END-PERFORM
            //                           END-IF
            if (ws.getAreaWorkIvvc0216().getC216TabValP().getEleVariabiliMaxP(ws.getIxIndici().getRiga()) > ws.getAreaWorkIvvc0216().getC216NumMaxVariabiliP()) {
                // COB_CODE: SET IVVC0211-GENERIC-ERROR TO TRUE
                inputIvvs0211.getIvvc0211RestoDati().getReturnCode().setGenericError();
                // COB_CODE: MOVE 'IVVC0216'
                //             TO IDSV0003-NOME-TABELLA
                ws.getIdsv0003().getCampiEsito().setNomeTabella("IVVC0216");
                // COB_CODE: MOVE WK-PGM            TO IVVC0211-COD-SERVIZIO-BE
                inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: STRING 'OVERFLOW TABELLA IVVC0216-AREA-VARIABILI'
                //                  ' IN FASE DI ' IVVC0216-FLG-AREA
                //                  ' - IVVC0216-ELE-VARIABILI-MAX :'
                //                   WK-ELE-VARIABILI-MAX
                //           DELIMITED BY SIZE INTO IVVC0211-DESCRIZ-ERR
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Ivvc0211CampiEsito.Len.DESCRIZ_ERR, "OVERFLOW TABELLA IVVC0216-AREA-VARIABILI", " IN FASE DI ", ws.getIvvc0216FlgArea().getIvvc0216FlgAreaFormatted(), " - IVVC0216-ELE-VARIABILI-MAX :", ws.getWkEleVariabiliMaxAsString());
                inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setDescrizErr(concatUtil.replaceInString(inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().getDescrizErrFormatted()));
            }
            else {
                //               IF IVVC0211-FL-AREA-VAR-EXTRA-SI
                //
                //                  PERFORM A951-GESTIONE-VARIABILI-EXTRA
                //                     THRU A951-EX
                //
                //               ELSE
                // COB_CODE: PERFORM VARYING IND-COL FROM 1 BY 1
                //             UNTIL IND-COL >
                //                   IVVC0216-ELE-VARIABILI-MAX-P(IND-RIGA)
                //                 OR IND-COL > IVVC0216-NUM-MAX-VARIABILI-P
                //               TO C216-TAB-VARIABILI-P(IND-RIGA, IND-COL)
                //            END-PERFORM
                ws.getIxIndici().setCol(((short)1));
                while (!(ws.getIxIndici().getCol() > ws.getAreaWorkIvvc0216().getC216TabValP().getEleVariabiliMaxP(ws.getIxIndici().getRiga()) || ws.getIxIndici().getCol() > ws.getAreaWorkIvvc0216().getC216NumMaxVariabiliP())) {
                    // COB_CODE: MOVE IVVC0216-TAB-VARIABILI-P
                    //                (IND-RIGA, IND-COL)
                    //             TO C216-TAB-VARIABILI-P(IND-RIGA, IND-COL)
                    ws.getIvvc0216().getC216TabValP().setTabVariabiliPBytes(ws.getIxIndici().getRiga(), ws.getIxIndici().getCol(), ws.getAreaWorkIvvc0216().getC216TabValP().getIvvc0216TabVariabiliPBytes(ws.getIxIndici().getRiga(), ws.getIxIndici().getCol()));
                    ws.getIxIndici().setCol(Trunc.toShort(ws.getIxIndici().getCol() + 1, 4));
                }
            }
            // COB_CODE: MOVE IVVC0216-NOME-SERVIZIO-P(IND-RIGA)
            //             TO C216-NOME-SERVIZIO-P(IND-RIGA)
            ws.getIvvc0216().getC216TabValP().setIvvc0216NomeServizioP(ws.getIxIndici().getRiga(), ws.getAreaWorkIvvc0216().getC216TabValP().getNomeServizioP(ws.getIxIndici().getRiga()));
            //            END-IF
            ws.getIxIndici().setRiga(Trunc.toShort(ws.getIxIndici().getRiga() + 1, 4));
        }
        // COB_CODE: PERFORM VARYING IND-RIGA FROM 1 BY 1
        //                   UNTIL IND-RIGA > IVVC0216-ELE-LIVELLO-MAX-T
        //                      OR IND-RIGA > IVVC0216-NUM-MAX-LIVELLI-T
        //                      OR NOT IVVC0211-SUCCESSFUL-RC
        //                     TO C216-NOME-SERVIZIO-T(IND-RIGA)
        //           END-PERFORM.
        ws.getIxIndici().setRiga(((short)1));
        while (!(ws.getIxIndici().getRiga() > ws.getAreaWorkIvvc0216().getC216EleLivelloMaxT() || ws.getIxIndici().getRiga() > ws.getAreaWorkIvvc0216().getIvvc0216NumMaxLivelliT() || !inputIvvs0211.getIvvc0211RestoDati().getReturnCode().isSuccessfulRc())) {
            // COB_CODE: MOVE IVVC0216-ID-GAR-T(IND-RIGA)
            //             TO C216-ID-GAR-T(IND-RIGA)
            ws.getIvvc0216().getC216TabValT().setIvvc0216IdGarTFormatted(ws.getIxIndici().getRiga(), ws.getAreaWorkIvvc0216().getC216TabValT().getIdGarTFormatted(ws.getIxIndici().getRiga()));
            // COB_CODE: MOVE IVVC0216-COD-TIPO-OPZIONE-T(IND-RIGA)
            //             TO C216-COD-TIPO-OPZIONE-T(IND-RIGA)
            ws.getIvvc0216().getC216TabValT().setIvvc0216CodTipoOpzioneT(ws.getIxIndici().getRiga(), ws.getAreaWorkIvvc0216().getC216TabValT().getCodTipoOpzioneT(ws.getIxIndici().getRiga()));
            // COB_CODE: MOVE IVVC0216-TP-LIVELLO-T(IND-RIGA)
            //             TO C216-TP-LIVELLO-T(IND-RIGA)
            ws.getIvvc0216().getC216TabValT().setIvvc0216TpLivelloT(ws.getIxIndici().getRiga(), ws.getAreaWorkIvvc0216().getC216TabValT().getTpLivelloT(ws.getIxIndici().getRiga()));
            // COB_CODE: MOVE IVVC0216-COD-LIVELLO-T(IND-RIGA)
            //             TO C216-COD-LIVELLO-T(IND-RIGA)
            ws.getIvvc0216().getC216TabValT().setIvvc0216CodLivelloT(ws.getIxIndici().getRiga(), ws.getAreaWorkIvvc0216().getC216TabValT().getCodLivelloT(ws.getIxIndici().getRiga()));
            //  INFORMAZIONI DI TRANCHE
            // COB_CODE: MOVE IVVC0216-ID-LIVELLO-T(IND-RIGA)
            //             TO C216-ID-LIVELLO-T(IND-RIGA)
            ws.getIvvc0216().getC216TabValT().setIvvc0216IdLivelloTFormatted(ws.getIxIndici().getRiga(), ws.getAreaWorkIvvc0216().getC216TabValT().getIdLivelloTFormatted(ws.getIxIndici().getRiga()));
            // COB_CODE: MOVE IVVC0216-DT-INIZ-TARI-T(IND-RIGA)
            //             TO C216-DT-INIZ-TARI-T(IND-RIGA)
            ws.getIvvc0216().getC216TabValT().setIvvc0216DtInizTariT(ws.getIxIndici().getRiga(), ws.getAreaWorkIvvc0216().getC216TabValT().getDtInizTariT(ws.getIxIndici().getRiga()));
            // COB_CODE: MOVE IVVC0216-DT-DECOR-TRCH-T(IND-RIGA)
            //             TO C216-DT-DECOR-TRCH-T(IND-RIGA)
            ws.getIvvc0216().getC216TabValT().setIvvc0216DtDecorTrchT(ws.getIxIndici().getRiga(), ws.getAreaWorkIvvc0216().getC216TabValT().getDtDecorTrchT(ws.getIxIndici().getRiga()));
            // COB_CODE: MOVE IVVC0216-COD-RGM-FISC-T(IND-RIGA)
            //             TO C216-COD-RGM-FISC-T(IND-RIGA)
            ws.getIvvc0216().getC216TabValT().setIvvc0216CodRgmFiscT(ws.getIxIndici().getRiga(), ws.getAreaWorkIvvc0216().getC216TabValT().getCodRgmFiscT(ws.getIxIndici().getRiga()));
            // COB_CODE: MOVE IVVC0216-TIPO-TRCH(IND-RIGA)
            //             TO C216-TIPO-TRCH(IND-RIGA)
            ws.getIvvc0216().getC216TabValT().setTipoTrchFormatted(ws.getIxIndici().getRiga(), ws.getAreaWorkIvvc0216().getC216TabValT().getIvvc0216TipoTrchFormatted(ws.getIxIndici().getRiga()));
            // COB_CODE: MOVE IVVC0216-FLG-ITN(IND-RIGA)
            //             TO C216-FLG-ITN(IND-RIGA)
            ws.getIvvc0216().getC216TabValT().setIvvc0216FlgItnFormatted(ws.getIxIndici().getRiga(), ws.getAreaWorkIvvc0216().getC216TabValT().getIvvc0216FlgItnFormatted(ws.getIxIndici().getRiga()));
            // AREA VARIABILI PRODOTTO
            // COB_CODE: MOVE IVVC0216-ELE-VARIABILI-MAX-T(IND-RIGA)
            //             TO C216-ELE-VARIABILI-MAX-T(IND-RIGA)
            //                WK-ELE-VARIABILI-MAX
            ws.getIvvc0216().getC216TabValT().setEleVariabiliMaxT(ws.getIxIndici().getRiga(), ws.getAreaWorkIvvc0216().getC216TabValT().getEleVariabiliMaxT(ws.getIxIndici().getRiga()));
            ws.setWkEleVariabiliMax(TruncAbs.toShort(ws.getAreaWorkIvvc0216().getC216TabValT().getEleVariabiliMaxT(ws.getIxIndici().getRiga()), 3));
            //   CONTROLLO DI OVERFLOW IN CASO DI VENDITA O
            //   POST-VENDITA SUL NUMERO DI VARIABILI RESTITUITE
            // COB_CODE: IF  (IVVC0216-ELE-VARIABILI-MAX-T(IND-RIGA) >
            //                IVVC0216-NUM-MAX-VARIABILI-T )
            //              END-STRING
            //           ELSE
            //              END-PERFORM
            //           END-IF
            if (ws.getAreaWorkIvvc0216().getC216TabValT().getEleVariabiliMaxT(ws.getIxIndici().getRiga()) > ws.getAreaWorkIvvc0216().getC216NumMaxVariabiliT()) {
                // COB_CODE: SET IVVC0211-GENERIC-ERROR TO TRUE
                inputIvvs0211.getIvvc0211RestoDati().getReturnCode().setGenericError();
                // COB_CODE: MOVE 'IVVC0216'
                //             TO IDSV0003-NOME-TABELLA
                ws.getIdsv0003().getCampiEsito().setNomeTabella("IVVC0216");
                // COB_CODE: MOVE WK-PGM            TO IVVC0211-COD-SERVIZIO-BE
                inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: STRING 'OVERFLOW TABELLA IVVC0216-AREA-VARIABILI'
                //                  ' IN FASE DI ' IVVC0216-FLG-AREA
                //                  ' - IVVC0216-ELE-VARIABILI-MAX :'
                //                   WK-ELE-VARIABILI-MAX
                //           DELIMITED BY SIZE INTO IVVC0211-DESCRIZ-ERR
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Ivvc0211CampiEsito.Len.DESCRIZ_ERR, "OVERFLOW TABELLA IVVC0216-AREA-VARIABILI", " IN FASE DI ", ws.getIvvc0216FlgArea().getIvvc0216FlgAreaFormatted(), " - IVVC0216-ELE-VARIABILI-MAX :", ws.getWkEleVariabiliMaxAsString());
                inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setDescrizErr(concatUtil.replaceInString(inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().getDescrizErrFormatted()));
            }
            else {
                // COB_CODE: PERFORM VARYING IND-COL FROM 1 BY 1
                //             UNTIL IND-COL >
                //                   IVVC0216-ELE-VARIABILI-MAX-T(IND-RIGA)
                //                 OR IND-COL > IVVC0216-NUM-MAX-VARIABILI-T
                //               TO C216-TAB-VARIABILI-T(IND-RIGA, IND-COL)
                //            END-PERFORM
                ws.getIxIndici().setCol(((short)1));
                while (!(ws.getIxIndici().getCol() > ws.getAreaWorkIvvc0216().getC216TabValT().getEleVariabiliMaxT(ws.getIxIndici().getRiga()) || ws.getIxIndici().getCol() > ws.getAreaWorkIvvc0216().getC216NumMaxVariabiliT())) {
                    // COB_CODE: MOVE IVVC0216-TAB-VARIABILI-T
                    //                (IND-RIGA, IND-COL)
                    //             TO C216-TAB-VARIABILI-T(IND-RIGA, IND-COL)
                    ws.getIvvc0216().getC216TabValT().setTabVariabiliTBytes(ws.getIxIndici().getRiga(), ws.getIxIndici().getCol(), ws.getAreaWorkIvvc0216().getC216TabValT().getIvvc0216TabVariabiliTBytes(ws.getIxIndici().getRiga(), ws.getIxIndici().getCol()));
                    ws.getIxIndici().setCol(Trunc.toShort(ws.getIxIndici().getCol() + 1, 4));
                }
            }
            // COB_CODE: MOVE IVVC0216-NOME-SERVIZIO-T(IND-RIGA)
            //             TO C216-NOME-SERVIZIO-T(IND-RIGA)
            ws.getIvvc0216().getC216TabValT().setIvvc0216NomeServizioT(ws.getIxIndici().getRiga(), ws.getAreaWorkIvvc0216().getC216TabValT().getNomeServizioT(ws.getIxIndici().getRiga()));
            ws.getIxIndici().setRiga(Trunc.toShort(ws.getIxIndici().getRiga() + 1, 4));
        }
        //AREA VARIABILI AUTONOMIA OPERATIVA
        // COB_CODE: MOVE IVVC0216-VAR-AUT-OPER  TO C216-VAR-AUT-OPER.
        ws.getIvvc0216().setC216VarAutOperBytes(ws.getAreaWorkIvvc0216().getIvvc0216VarAutOperBytes());
    }

    /**Original name: T000-TP-MOVI-PTF-TO-ACT<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZAZIONE STRUTTURA DATI VARIABILI EXTRA
	 * ----------------------------------------------------------------*
	 * A951-GESTIONE-VARIABILI-EXTRA.
	 *     PERFORM VARYING IND-COL FROM 1 BY 1
	 *       UNTIL IND-COL >
	 *            IVVC0216-ELE-VARIABILI-MAX(IND-RIGA)
	 *          OR IND-COL > 86
	 *          INITIALIZE C216-AREA-VAR-EXTRA(IND-RIGA, IND-COL)
	 *          MOVE IVVC0216-COD-VARIABILE(IND-RIGA, IND-COL)
	 *            TO C216-COD-VAR-EXTRA(IND-RIGA, IND-COL)
	 *          MOVE IVVC0216-TP-DATO(IND-RIGA, IND-COL)
	 *            TO C216-TP-DATO-EXTRA(IND-RIGA, IND-COL)
	 *          EVALUATE IVVC0216-TP-DATO(IND-RIGA, IND-COL)
	 *              WHEN 'A'
	 *              WHEN 'N'
	 *              WHEN 'I'
	 *                   MOVE IVVC0216-VAL-IMP(IND-RIGA, IND-COL)
	 *                     TO C216-VAL-IMP-EXTRA(IND-RIGA, IND-COL)
	 *              WHEN 'P'
	 *              WHEN 'M'
	 *                   MOVE IVVC0216-VAL-PERC(IND-RIGA, IND-COL)
	 *                     TO C216-VAL-PERC-EXTRA(IND-RIGA, IND-COL)
	 *              WHEN 'D'
	 *              WHEN 'S'
	 *              WHEN 'X'
	 *                   MOVE IVVC0216-VAL-STR(IND-RIGA, IND-COL)
	 *                     TO C216-VAL-STR-EXTRA(IND-RIGA, IND-COL)
	 *          END-EVALUATE
	 *     END-PERFORM.
	 * A951-EX.
	 *     EXIT.
	 * ----------------------------------------------------------------*
	 *     LETTURA SULLA TABELLA MATRICE MOVIMENTO
	 * ----------------------------------------------------------------*</pre>*/
    private void t000TpMoviPtfToAct() {
        Lccs0020 lccs0020 = null;
        // COB_CODE: MOVE IVVC0211-COD-COMPAGNIA-ANIA
        //                                        TO IDSV0001-COD-COMPAGNIA-ANIA
        ws.getAreaIdsv0001().getAreaComune().setIdsv0001CodCompagniaAniaFormatted(inputIvvs0211.getIvvc0211DatiInput().getCodCompagniaAniaFormatted());
        // COB_CODE: MOVE IVVC0211-TIPO-MOVIMENTO TO LCCV0021-TP-MOV-PTF
        ws.getLccv0021().setTpMovPtfFormatted(inputIvvs0211.getIvvc0211DatiInput().getTipoMovimentoFormatted());
        // COB_CODE: MOVE IVVC0211-TRATTAMENTO-STORICITA
        //                                   TO IDSV0001-TRATTAMENTO-STORICITA
        ws.getAreaIdsv0001().getAreaComune().getTrattamentoStoricita().setTrattamentoStoricita(inputIvvs0211.getIvvc0211DatiInput().getTrattamentoStoricita());
        // COB_CODE: MOVE IVVC0211-FORMATO-DATA-DB
        //                                   TO IDSV0001-FORMATO-DATA-DB
        ws.getAreaIdsv0001().getAreaComune().getFormatoDataDb().setFormatoDataDb(inputIvvs0211.getIvvc0211DatiInput().getFormatoDataDb().getFormatoDataDb());
        // COB_CODE: CALL PGM-LCCS0020            USING AREA-IDSV0001 LCCV0021.
        lccs0020 = Lccs0020.getInstance();
        lccs0020.run(ws.getAreaIdsv0001(), ws.getLccv0021());
        // COB_CODE: MOVE LCCV0021-RETURN-CODE    TO IVVC0211-RETURN-CODE
        inputIvvs0211.getIvvc0211RestoDati().getReturnCode().setReturnCode(ws.getLccv0021().getAreaOutput().getReturnCode().getReturnCode());
        // COB_CODE: MOVE LCCV0021-CAMPI-ESITO    TO IVVC0211-CAMPI-ESITO.
        inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setCampiEsitoBytes(ws.getLccv0021().getAreaOutput().getLccv0021CampiEsitoBytes());
    }

    /**Original name: V000-VERIFICA-OPZ-POL-GAR<br>
	 * <pre>----------------------------------------------------------------*
	 *     VERIFICA GARANZIE OPZIONE
	 * ----------------------------------------------------------------*
	 * --> CONTROLLO FLAG-GARANZIE-OPZIONI</pre>*/
    private void v000VerificaOpzPolGar() {
        // COB_CODE: IF IVVC0211-FLAG-GAR-OPZIONE-SI
        //              PERFORM V100-VERIFICA-GAR-OPZIONE THRU V100-EX
        //           END-IF
        if (inputIvvs0211.getIvvc0211DatiInput().getFlagGarOpzione().isSi()) {
            // COB_CODE: PERFORM V100-VERIFICA-GAR-OPZIONE THRU V100-EX
            v100VerificaGarOpzione();
        }
        // COB_CODE:      IF IVVC0211-SUCCESSFUL-RC
        //           *--> CONTROLLO POLIZZA
        //                   PERFORM V200-VERIFICA-POLIZZE     THRU V200-EX
        //                END-IF.
        if (inputIvvs0211.getIvvc0211RestoDati().getReturnCode().isSuccessfulRc()) {
            //--> CONTROLLO POLIZZA
            // COB_CODE: PERFORM V200-VERIFICA-POLIZZE     THRU V200-EX
            v200VerificaPolizze();
        }
    }

    /**Original name: V100-VERIFICA-GAR-OPZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     VERIFICA GARANZIE OPZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void v100VerificaGarOpzione() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE   IVVC0218-ALIAS-OPZIONI   TO WS-ALIAS
        ws.setWsAlias(ws.getIvvc0218().getAliasOpzioni());
        // COB_CODE: PERFORM V999-TROVA-AREA         THRU V999-EX
        v999TrovaArea();
        // COB_CODE: IF AREA-TROVATA-NO
        //               END-STRING
        //           ELSE
        //               END-IF
        //           END-IF.
        if (ws.getFlagArea().isNo()) {
            // COB_CODE: SET IVVC0211-AREA-NOT-FOUND TO TRUE
            inputIvvs0211.getIvvc0211RestoDati().getReturnCode().setAreaNotFound();
            // COB_CODE: MOVE WK-PGM             TO IVVC0211-COD-SERVIZIO-BE
            inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: STRING 'OPZIONI NON TROVATE'
            //                  DELIMITED BY SIZE INTO
            //                  IVVC0211-DESCRIZ-ERR
            //           END-STRING
            inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setDescrizErr("OPZIONI NON TROVATE");
        }
        else {
            // COB_CODE: SET IND-STR                  TO IVVC0211-SEARCH-IND
            ws.getIxIndici().setStr(Trunc.toShort(ws.getIvvc0211SearchInd(), 4));
            // COB_CODE: PERFORM V888-VERIFICA-BUFFER-DATI THRU V888-EX
            v888VerificaBufferDati();
            // COB_CODE: IF BUFFER-DATI-VALIDO-NO
            //              END-STRING
            //           END-IF
            if (ws.getFlagBufferDati().isNo()) {
                // COB_CODE: SET IVVC0211-BUFFER-NOT-VALID TO TRUE
                inputIvvs0211.getIvvc0211RestoDati().getReturnCode().setBufferNotValid();
                // COB_CODE: MOVE WK-PGM               TO IVVC0211-COD-SERVIZIO-BE
                inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: STRING 'BUFFER DI OPZIONE NON VALIDO'
                //                  DELIMITED BY SIZE INTO
                //                  IVVC0211-DESCRIZ-ERR
                //           END-STRING
                inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setDescrizErr("BUFFER DI OPZIONE NON VALIDO");
            }
        }
    }

    /**Original name: V200-VERIFICA-POLIZZE<br>
	 * <pre>----------------------------------------------------------------*
	 *     VERIFICA POLIZZE
	 * ----------------------------------------------------------------*</pre>*/
    private void v200VerificaPolizze() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE   IVVC0218-ALIAS-POLI      TO WS-ALIAS
        ws.setWsAlias(ws.getIvvc0218().getAliasPoli());
        // COB_CODE: PERFORM V999-TROVA-AREA         THRU V999-EX
        v999TrovaArea();
        // COB_CODE: IF AREA-TROVATA-NO
        //               END-STRING
        //           ELSE
        //               END-IF
        //           END-IF.
        if (ws.getFlagArea().isNo()) {
            // COB_CODE: SET IVVC0211-AREA-NOT-FOUND TO TRUE
            inputIvvs0211.getIvvc0211RestoDati().getReturnCode().setAreaNotFound();
            // COB_CODE: MOVE WK-PGM             TO IVVC0211-COD-SERVIZIO-BE
            inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: STRING 'POLIZZE NON TROVATE'
            //                  DELIMITED BY SIZE INTO
            //                  IVVC0211-DESCRIZ-ERR
            //           END-STRING
            inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setDescrizErr("POLIZZE NON TROVATE");
        }
        else {
            // COB_CODE: SET IND-STR            TO IVVC0211-SEARCH-IND
            ws.getIxIndici().setStr(Trunc.toShort(ws.getIvvc0211SearchInd(), 4));
            // COB_CODE: PERFORM V888-VERIFICA-BUFFER-DATI THRU V888-EX
            v888VerificaBufferDati();
            // COB_CODE: IF BUFFER-DATI-VALIDO-NO
            //              END-STRING
            //           END-IF
            if (ws.getFlagBufferDati().isNo()) {
                // COB_CODE: SET IVVC0211-BUFFER-NOT-VALID TO TRUE
                inputIvvs0211.getIvvc0211RestoDati().getReturnCode().setBufferNotValid();
                // COB_CODE: MOVE WK-PGM               TO IVVC0211-COD-SERVIZIO-BE
                inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: STRING 'BUFFER DI POLIZZA NON VALIDO'
                //                  DELIMITED BY SIZE INTO
                //                  IVVC0211-DESCRIZ-ERR
                //           END-STRING
                inputIvvs0211.getIvvc0211RestoDati().getCampiEsito().setDescrizErr("BUFFER DI POLIZZA NON VALIDO");
            }
        }
    }

    /**Original name: V888-VERIFICA-BUFFER-DATI<br>
	 * <pre>----------------------------------------------------------------*
	 *     VERIFICA GARANZIE OPZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void v888VerificaBufferDati() {
        // COB_CODE: SET BUFFER-DATI-VALIDO-SI        TO TRUE
        ws.getFlagBufferDati().setSi();
        // COB_CODE: IF IVVC0211-BUFFER-DATI  (IVVC0211-POSIZ-INI(IND-STR)
        //                                                 :
        //                                     IVVC0211-LUNGHEZZA(IND-STR)) =
        //                             SPACES OR LOW-VALUE OR HIGH-VALUE
        //              SET BUFFER-DATI-VALIDO-NO     TO TRUE
        //           END-IF.
        if (Conditions.eq(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1), "") || Conditions.eq(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1), LiteralGenerator.create(Types.LOW_CHAR_VAL, inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()))) || Conditions.eq(inputIvvs0211.getIvvc0211RestoDati().getBufferDatiFormatted().substring((inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr())) - 1, inputIvvs0211.getIvvc0211TabInfo1().getPosizIni(ws.getIxIndici().getStr()) + inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr()) - 1), LiteralGenerator.create(Types.HIGH_CHAR_VAL, inputIvvs0211.getIvvc0211TabInfo1().getLunghezza(ws.getIxIndici().getStr())))) {
            // COB_CODE: SET BUFFER-DATI-VALIDO-NO     TO TRUE
            ws.getFlagBufferDati().setNo();
        }
    }

    /**Original name: V999-TROVA-AREA<br>
	 * <pre>----------------------------------------------------------------*
	 *     VERIFICA AREE
	 * ----------------------------------------------------------------*</pre>*/
    private void v999TrovaArea() {
        boolean endOfTable = false;
        // COB_CODE: SET IVVC0211-SEARCH-IND        TO 1
        ws.setIvvc0211SearchInd(1);
        // COB_CODE: SEARCH  IVVC0211-TAB-INFO
        //                   AT END
        //                   SET AREA-TROVATA-NO    TO TRUE
        //              WHEN IVVC0211-TAB-ALIAS(IVVC0211-SEARCH-IND) = WS-ALIAS
        //                   SET AREA-TROVATA-SI    TO TRUE
        //                   SET AREA-TROVATA-NO    TO TRUE
        //              WHEN IVVC0211-TAB-ALIAS(IVVC0211-SEARCH-IND) = WS-ALIAS
        //                   SET AREA-TROVATA-SI    TO TRUE
        //           END-SEARCH.
        endOfTable = true;
        while (ws.getIvvc0211SearchInd() <= 100) {
            if (Conditions.eq(inputIvvs0211.getIvvc0211TabInfo1().getTabAlias(ws.getIvvc0211SearchInd()), ws.getWsAlias())) {
                endOfTable = false;
                // COB_CODE: SET AREA-TROVATA-SI    TO TRUE
                ws.getFlagArea().setSi();
                break;
            }
            ws.setIvvc0211SearchInd(Trunc.toInt(ws.getIvvc0211SearchInd() + 1, 9));
        }
        //AT END GROUP;
        if (endOfTable) {
            // COB_CODE: SET AREA-TROVATA-NO    TO TRUE
            ws.getFlagArea().setNo();
        }
    }

    /**Original name: GESTIONE-RIASS<br>
	 * <pre>----------------------------------------------------------------*
	 *  GESTIONE RIASS
	 * ----------------------------------------------------------------*</pre>*/
    private void gestioneRiass() {
        // COB_CODE: MOVE   ZERO       TO ELE-MAX-APP-COD-VAR.
        ws.setEleMaxAppCodVar(((short)0));
        // COB_CODE: IF GESTIONE-PRODOTTO
        //              END-PERFORM
        //           ELSE
        //              END-PERFORM
        //           END-IF
        if (ws.getFlagGestione().isProdotto()) {
            // COB_CODE: PERFORM VARYING IND-UNZIP FROM 1 BY 1
            //                  UNTIL IND-UNZIP  >  LIMITE-VARIABILI-UNZIPPED OR
            //                         IND-UNZIP  >  UNZIP-ELE-VARIABILI-MAX
            //                  END-IF
            //           END-PERFORM
            ws.getIxIndici().setUnzip(((short)1));
            while (!(ws.getIxIndici().getUnzip() > ws.getLimiteVariabiliUnzipped() || ws.getIxIndici().getUnzip() > ws.getUnzipStructure().getUnzipEleVariabiliMax())) {
                // COB_CODE: SET TROVATO-APP-NO TO TRUE
                ws.getFlagApp().setNo();
                // COB_CODE: PERFORM VARYING IND-COD-VAR FROM 1 BY 1
                //             UNTIL IND-COD-VAR > ELE-MAX-APP-COD-VAR
                //                OR TROVATO-APP-SI
                //              END-IF
                //           END-PERFORM
                ws.getIxIndici().setCodVar(((short)1));
                while (!(ws.getIxIndici().getCodVar() > ws.getEleMaxAppCodVar() || ws.getFlagApp().isSi())) {
                    // COB_CODE: IF UNZIP-COD-VARIABILE(IND-UNZIP) =
                    //              IVVC0216-COD-VARIABILE-P
                    //              (IND-LIVELLO, IND-COD-VAR)
                    //              SET TROVATO-APP-SI TO TRUE
                    //           END-IF
                    if (Conditions.eq(ws.getUnzipStructure().getUnzipTab().getCodVariabile(ws.getIxIndici().getUnzip()), ws.getAreaWorkIvvc0216().getC216TabValP().getCodVariabileP(ws.getIxIndici().getLivello(), ws.getIxIndici().getCodVar()))) {
                        // COB_CODE: SET TROVATO-APP-SI TO TRUE
                        ws.getFlagApp().setSi();
                    }
                    ws.getIxIndici().setCodVar(Trunc.toShort(ws.getIxIndici().getCodVar() + 1, 4));
                }
                // COB_CODE: IF TROVATO-APP-NO
                //              ADD 1 TO ELE-MAX-APP-COD-VAR
                //           END-IF
                if (ws.getFlagApp().isNo()) {
                    // COB_CODE: MOVE UNZIP-COD-VARIABILE(IND-UNZIP)
                    //           TO IVVC0216-COD-VARIABILE-P
                    //              (IND-LIVELLO, IND-COD-VAR)
                    ws.getAreaWorkIvvc0216().getC216TabValP().setCodVariabileP(ws.getIxIndici().getLivello(), ws.getIxIndici().getCodVar(), ws.getUnzipStructure().getUnzipTab().getCodVariabile(ws.getIxIndici().getUnzip()));
                    // COB_CODE: ADD 1 TO ELE-MAX-APP-COD-VAR
                    ws.setEleMaxAppCodVar(Trunc.toShort(1 + ws.getEleMaxAppCodVar(), 3));
                }
                ws.getIxIndici().setUnzip(Trunc.toShort(ws.getIxIndici().getUnzip() + 1, 4));
            }
        }
        else {
            // COB_CODE: PERFORM VARYING IND-UNZIP FROM 1 BY 1
            //                  UNTIL IND-UNZIP  >  LIMITE-VARIABILI-UNZIPPED OR
            //                         IND-UNZIP  >  UNZIP-ELE-VARIABILI-MAX
            //                  END-IF
            //           END-PERFORM
            ws.getIxIndici().setUnzip(((short)1));
            while (!(ws.getIxIndici().getUnzip() > ws.getLimiteVariabiliUnzipped() || ws.getIxIndici().getUnzip() > ws.getUnzipStructure().getUnzipEleVariabiliMax())) {
                // COB_CODE: SET TROVATO-APP-NO TO TRUE
                ws.getFlagApp().setNo();
                // COB_CODE: PERFORM VARYING IND-COD-VAR FROM 1 BY 1
                //             UNTIL IND-COD-VAR > ELE-MAX-APP-COD-VAR
                //                OR TROVATO-APP-SI
                //              END-IF
                //           END-PERFORM
                ws.getIxIndici().setCodVar(((short)1));
                while (!(ws.getIxIndici().getCodVar() > ws.getEleMaxAppCodVar() || ws.getFlagApp().isSi())) {
                    // COB_CODE: IF UNZIP-COD-VARIABILE(IND-UNZIP) =
                    //              IVVC0216-COD-VARIABILE-T
                    //              (IND-LIVELLO, IND-COD-VAR)
                    //              SET TROVATO-APP-SI TO TRUE
                    //           END-IF
                    if (Conditions.eq(ws.getUnzipStructure().getUnzipTab().getCodVariabile(ws.getIxIndici().getUnzip()), ws.getAreaWorkIvvc0216().getC216TabValT().getCodVariabileT(ws.getIxIndici().getLivello(), ws.getIxIndici().getCodVar()))) {
                        // COB_CODE: SET TROVATO-APP-SI TO TRUE
                        ws.getFlagApp().setSi();
                    }
                    ws.getIxIndici().setCodVar(Trunc.toShort(ws.getIxIndici().getCodVar() + 1, 4));
                }
                // COB_CODE: IF TROVATO-APP-NO
                //              ADD 1 TO ELE-MAX-APP-COD-VAR
                //           END-IF
                if (ws.getFlagApp().isNo()) {
                    // COB_CODE: MOVE UNZIP-COD-VARIABILE(IND-UNZIP)
                    //           TO IVVC0216-COD-VARIABILE-T
                    //              (IND-LIVELLO, IND-COD-VAR)
                    ws.getAreaWorkIvvc0216().getC216TabValT().setCodVariabileT(ws.getIxIndici().getLivello(), ws.getIxIndici().getCodVar(), ws.getUnzipStructure().getUnzipTab().getCodVariabile(ws.getIxIndici().getUnzip()));
                    // COB_CODE: ADD 1 TO ELE-MAX-APP-COD-VAR
                    ws.setEleMaxAppCodVar(Trunc.toShort(1 + ws.getEleMaxAppCodVar(), 3));
                }
                ws.getIxIndici().setUnzip(Trunc.toShort(ws.getIxIndici().getUnzip() + 1, 4));
            }
        }
        // COB_CODE: MOVE ELE-MAX-APP-COD-VAR TO UNZIP-ELE-VARIABILI-MAX.
        ws.getUnzipStructure().setUnzipEleVariabiliMax(ws.getEleMaxAppCodVar());
    }

    /**Original name: ESEGUI-DISPLAY<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI GESTIONE DISPLAY
	 * ----------------------------------------------------------------*</pre>*/
    private void eseguiDisplay() {
        Idss8880 idss8880 = null;
        GenericParam idsv8888DisplayAddress = null;
        // COB_CODE: IF IVVC0211-ANY-TUNING-DBG AND
        //              IDSV8888-ANY-TUNING-DBG
        //              END-IF
        //           ELSE
        //              END-IF
        //           END-IF.
        if (ws.getIvvc0211LivelloDebug().isAnyTuningDbg() && ws.getIdsv8888().getLivelloDebug().isAnyTuningDbg()) {
            // COB_CODE: IF IVVC0211-TOT-TUNING-DBG
            //              INITIALIZE IDSV8888-STR-PERFORMANCE-DBG
            //           ELSE
            //              END-IF
            //           END-IF
            if (ws.getIvvc0211LivelloDebug().isTotTuningDbg()) {
                // COB_CODE: IF IDSV8888-STRESS-TEST-DBG
                //              CONTINUE
                //           ELSE
                //              END-IF
                //           END-IF
                if (ws.getIdsv8888().getLivelloDebug().isStressTestDbg()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else if (ws.getIdsv8888().getLivelloDebug().isComCobJavDbg()) {
                    // COB_CODE: IF IDSV8888-COM-COB-JAV-DBG
                    //              SET IDSV8888-COM-COB-JAV   TO TRUE
                    //           ELSE
                    //              END-IF
                    //           END-IF
                    // COB_CODE: SET IDSV8888-COM-COB-JAV   TO TRUE
                    ws.getIdsv8888().getStrPerformanceDbg().setComCobJav();
                }
                else if (ws.getIdsv8888().getLivelloDebug().isBusinessDbg()) {
                    // COB_CODE: IF IDSV8888-BUSINESS-DBG
                    //              SET IDSV8888-BUSINESS   TO TRUE
                    //           ELSE
                    //              END-IF
                    //           END-IF
                    // COB_CODE: SET IDSV8888-BUSINESS   TO TRUE
                    ws.getIdsv8888().getStrPerformanceDbg().setBusiness();
                }
                else if (ws.getIdsv8888().getLivelloDebug().isArchBatchDbg()) {
                    // COB_CODE: IF IDSV8888-ARCH-BATCH-DBG
                    //              SET IDSV8888-ARCH-BATCH TO TRUE
                    //           ELSE
                    //              MOVE 'ERRO'   TO IDSV8888-FASE
                    //           END-IF
                    // COB_CODE: SET IDSV8888-ARCH-BATCH TO TRUE
                    ws.getIdsv8888().getStrPerformanceDbg().setArchBatch();
                }
                else {
                    // COB_CODE: MOVE 'ERRO'   TO IDSV8888-FASE
                    ws.getIdsv8888().getStrPerformanceDbg().setFase("ERRO");
                }
                // COB_CODE: CALL IDSV8888-CALL-TIMESTAMP
                //                USING IDSV8888-TIMESTAMP
                DynamicCall.invoke(ws.getIdsv8888().getCallTimestamp(), new BasicBytesClass(ws.getIdsv8888().getStrPerformanceDbg().getArray(), Idsv8888StrPerformanceDbg.Pos.TIMESTAMP_FLD - 1));
                // COB_CODE: SET IDSV8888-DISPLAY-ADDRESS
                //                TO ADDRESS OF IDSV8888-STR-PERFORMANCE-DBG
                ws.getIdsv8888().setDisplayAddress(pointerManager.addressOf(ws.getIdsv8888().getStrPerformanceDbg()));
                // COB_CODE: CALL IDSV8888-CALL-DISPLAY
                //                USING IDSV8888-DISPLAY-ADDRESS
                idss8880 = Idss8880.getInstance();
                idsv8888DisplayAddress = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getIdsv8888().getDisplayAddress()));
                idss8880.run(idsv8888DisplayAddress);
                ws.getIdsv8888().setDisplayAddressFromBuffer(idsv8888DisplayAddress.getByteData());
                // COB_CODE: INITIALIZE IDSV8888-STR-PERFORMANCE-DBG
                initStrPerformanceDbg();
            }
            else if (ws.getIdsv8888().getLivelloDebug().getLivelloDebug() == ws.getIvvc0211LivelloDebug().getLivelloDebug()) {
                // COB_CODE: IF IDSV8888-LIVELLO-DEBUG =
                //              IVVC0211-LIVELLO-DEBUG
                //              INITIALIZE IDSV8888-STR-PERFORMANCE-DBG
                //           END-IF
                // COB_CODE: IF IDSV8888-STRESS-TEST-DBG
                //              CONTINUE
                //           ELSE
                //              END-IF
                //           END-IF
                if (ws.getIdsv8888().getLivelloDebug().isStressTestDbg()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else if (ws.getIdsv8888().getLivelloDebug().isComCobJavDbg()) {
                    // COB_CODE: IF IDSV8888-COM-COB-JAV-DBG
                    //              SET IDSV8888-COM-COB-JAV   TO TRUE
                    //           ELSE
                    //              END-IF
                    //           END-IF
                    // COB_CODE: SET IDSV8888-COM-COB-JAV   TO TRUE
                    ws.getIdsv8888().getStrPerformanceDbg().setComCobJav();
                }
                else if (ws.getIdsv8888().getLivelloDebug().isBusinessDbg()) {
                    // COB_CODE: IF IDSV8888-BUSINESS-DBG
                    //              SET IDSV8888-BUSINESS   TO TRUE
                    //           ELSE
                    //              SET IDSV8888-ARCH-BATCH TO TRUE
                    //           END-IF
                    // COB_CODE: SET IDSV8888-BUSINESS   TO TRUE
                    ws.getIdsv8888().getStrPerformanceDbg().setBusiness();
                }
                else {
                    // COB_CODE: SET IDSV8888-ARCH-BATCH TO TRUE
                    ws.getIdsv8888().getStrPerformanceDbg().setArchBatch();
                }
                // COB_CODE: CALL IDSV8888-CALL-TIMESTAMP
                //                USING IDSV8888-TIMESTAMP
                DynamicCall.invoke(ws.getIdsv8888().getCallTimestamp(), new BasicBytesClass(ws.getIdsv8888().getStrPerformanceDbg().getArray(), Idsv8888StrPerformanceDbg.Pos.TIMESTAMP_FLD - 1));
                // COB_CODE: SET IDSV8888-DISPLAY-ADDRESS
                //               TO ADDRESS OF IDSV8888-STR-PERFORMANCE-DBG
                ws.getIdsv8888().setDisplayAddress(pointerManager.addressOf(ws.getIdsv8888().getStrPerformanceDbg()));
                // COB_CODE: CALL IDSV8888-CALL-DISPLAY
                //                USING IDSV8888-DISPLAY-ADDRESS
                idss8880 = Idss8880.getInstance();
                idsv8888DisplayAddress = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getIdsv8888().getDisplayAddress()));
                idss8880.run(idsv8888DisplayAddress);
                ws.getIdsv8888().setDisplayAddressFromBuffer(idsv8888DisplayAddress.getByteData());
                // COB_CODE: INITIALIZE IDSV8888-STR-PERFORMANCE-DBG
                initStrPerformanceDbg();
            }
        }
        else if (ws.getIvvc0211LivelloDebug().isAnyApplDbg() && ws.getIdsv8888().getLivelloDebug().isAnyApplDbg()) {
            // COB_CODE: IF IVVC0211-ANY-APPL-DBG AND
            //              IDSV8888-ANY-APPL-DBG
            //               END-IF
            //           END-IF
            // COB_CODE: IF IDSV8888-LIVELLO-DEBUG <=
            //              IVVC0211-LIVELLO-DEBUG
            //              MOVE SPACES TO IDSV8888-AREA-DISPLAY
            //           END-IF
            if (ws.getIdsv8888().getLivelloDebug().getLivelloDebug() <= ws.getIvvc0211LivelloDebug().getLivelloDebug()) {
                // COB_CODE: SET IDSV8888-DISPLAY-ADDRESS
                //               TO ADDRESS OF IDSV8888-AREA-DISPLAY
                ws.getIdsv8888().setDisplayAddress(pointerManager.addressOf(ws.getIdsv8888().getAreaDisplay()));
                // COB_CODE: CALL IDSV8888-CALL-DISPLAY
                //                USING IDSV8888-DISPLAY-ADDRESS
                idss8880 = Idss8880.getInstance();
                idsv8888DisplayAddress = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getIdsv8888().getDisplayAddress()));
                idss8880.run(idsv8888DisplayAddress);
                ws.getIdsv8888().setDisplayAddressFromBuffer(idsv8888DisplayAddress.getByteData());
                // COB_CODE: MOVE SPACES TO IDSV8888-AREA-DISPLAY
                ws.getIdsv8888().getAreaDisplay().setAreaDisplay("");
            }
        }
        // COB_CODE: SET IDSV8888-NO-DEBUG              TO TRUE.
        ws.getIdsv8888().getLivelloDebug().setNoDebug();
    }

    public void initIxIndici() {
        ws.getIxIndici().setLivello(((short)0));
        ws.getIxIndici().setStr(((short)0));
        ws.getIxIndici().setUnzip(((short)0));
        ws.getIxIndici().setStartVar(((short)0));
        ws.getIxIndici().setEndVar(((short)0));
        ws.getIxIndici().setDistinct(((short)0));
        ws.getIxIndici().setTga(((short)0));
        ws.getIxIndici().setGrz(((short)0));
        ws.getIxIndici().setGop(((short)0));
        ws.getIxIndici().setTop(((short)0));
        ws.getIxIndici().setCodVar(((short)0));
        ws.getIxIndici().setTipoOpz(((short)0));
        ws.getIxIndici().setOpz(((short)0));
        ws.getIxIndici().setCharFld(((short)0));
        ws.getIxIndici().setTabDist(((short)0));
        ws.getIxIndici().setRiga(((short)0));
        ws.getIxIndici().setCol(((short)0));
    }

    public void initWadeAreaAdesione() {
        ws.getAreaBusiness().setWadeEleAdesMax(((short)0));
        ws.getAreaBusiness().getLccvade1().getStatus().setStatus(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvade1().setIdPtf(0);
        ws.getAreaBusiness().getLccvade1().getDati().setWadeIdAdes(0);
        ws.getAreaBusiness().getLccvade1().getDati().setWadeIdPoli(0);
        ws.getAreaBusiness().getLccvade1().getDati().setWadeIdMoviCrz(0);
        ws.getAreaBusiness().getLccvade1().getDati().getWadeIdMoviChiu().setWadeIdMoviChiu(0);
        ws.getAreaBusiness().getLccvade1().getDati().setWadeDtIniEff(0);
        ws.getAreaBusiness().getLccvade1().getDati().setWadeDtEndEff(0);
        ws.getAreaBusiness().getLccvade1().getDati().setWadeIbPrev("");
        ws.getAreaBusiness().getLccvade1().getDati().setWadeIbOgg("");
        ws.getAreaBusiness().getLccvade1().getDati().setWadeCodCompAnia(0);
        ws.getAreaBusiness().getLccvade1().getDati().getWadeDtDecor().setWadeDtDecor(0);
        ws.getAreaBusiness().getLccvade1().getDati().getWadeDtScad().setWadeDtScad(0);
        ws.getAreaBusiness().getLccvade1().getDati().getWadeEtaAScad().setWadeEtaAScad(0);
        ws.getAreaBusiness().getLccvade1().getDati().getWadeDurAa().setWadeDurAa(0);
        ws.getAreaBusiness().getLccvade1().getDati().getWadeDurMm().setWadeDurMm(0);
        ws.getAreaBusiness().getLccvade1().getDati().getWadeDurGg().setWadeDurGg(0);
        ws.getAreaBusiness().getLccvade1().getDati().setWadeTpRgmFisc("");
        ws.getAreaBusiness().getLccvade1().getDati().setWadeTpRiat("");
        ws.getAreaBusiness().getLccvade1().getDati().setWadeTpModPagTit("");
        ws.getAreaBusiness().getLccvade1().getDati().setWadeTpIas("");
        ws.getAreaBusiness().getLccvade1().getDati().getWadeDtVarzTpIas().setWadeDtVarzTpIas(0);
        ws.getAreaBusiness().getLccvade1().getDati().getWadePreNetInd().setWadePreNetInd(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvade1().getDati().getWadePreLrdInd().setWadePreLrdInd(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvade1().getDati().getWadeRatLrdInd().setWadeRatLrdInd(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvade1().getDati().getWadePrstzIniInd().setWadePrstzIniInd(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvade1().getDati().setWadeFlCoincAssto(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvade1().getDati().setWadeIbDflt("");
        ws.getAreaBusiness().getLccvade1().getDati().setWadeModCalc("");
        ws.getAreaBusiness().getLccvade1().getDati().setWadeTpFntCnbtva("");
        ws.getAreaBusiness().getLccvade1().getDati().getWadeImpAz().setWadeImpAz(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvade1().getDati().getWadeImpAder().setWadeImpAder(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvade1().getDati().getWadeImpTfr().setWadeImpTfr(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvade1().getDati().getWadeImpVolo().setWadeImpVolo(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvade1().getDati().getWadePcAz().setWadePcAz(new AfDecimal(0, 6, 3));
        ws.getAreaBusiness().getLccvade1().getDati().getWadePcAder().setWadePcAder(new AfDecimal(0, 6, 3));
        ws.getAreaBusiness().getLccvade1().getDati().getWadePcTfr().setWadePcTfr(new AfDecimal(0, 6, 3));
        ws.getAreaBusiness().getLccvade1().getDati().getWadePcVolo().setWadePcVolo(new AfDecimal(0, 6, 3));
        ws.getAreaBusiness().getLccvade1().getDati().getWadeDtNovaRgmFisc().setWadeDtNovaRgmFisc(0);
        ws.getAreaBusiness().getLccvade1().getDati().setWadeFlAttiv(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvade1().getDati().getWadeImpRecRitVis().setWadeImpRecRitVis(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvade1().getDati().getWadeImpRecRitAcc().setWadeImpRecRitAcc(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvade1().getDati().setWadeFlVarzStatTbgc(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvade1().getDati().setWadeFlProvzaMigraz(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvade1().getDati().getWadeImpbVisDaRec().setWadeImpbVisDaRec(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvade1().getDati().getWadeDtDecorPrestBan().setWadeDtDecorPrestBan(0);
        ws.getAreaBusiness().getLccvade1().getDati().getWadeDtEffVarzStatT().setWadeDtEffVarzStatT(0);
        ws.getAreaBusiness().getLccvade1().getDati().setWadeDsRiga(0);
        ws.getAreaBusiness().getLccvade1().getDati().setWadeDsOperSql(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvade1().getDati().setWadeDsVer(0);
        ws.getAreaBusiness().getLccvade1().getDati().setWadeDsTsIniCptz(0);
        ws.getAreaBusiness().getLccvade1().getDati().setWadeDsTsEndCptz(0);
        ws.getAreaBusiness().getLccvade1().getDati().setWadeDsUtente("");
        ws.getAreaBusiness().getLccvade1().getDati().setWadeDsStatoElab(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvade1().getDati().getWadeCumCnbtCap().setWadeCumCnbtCap(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvade1().getDati().getWadeImpGarCnbt().setWadeImpGarCnbt(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvade1().getDati().getWadeDtUltConsCnbt().setWadeDtUltConsCnbt(0);
        ws.getAreaBusiness().getLccvade1().getDati().setWadeIdenIscFnd("");
        ws.getAreaBusiness().getLccvade1().getDati().getWadeNumRatPian().setWadeNumRatPian(new AfDecimal(0, 12, 5));
        ws.getAreaBusiness().getLccvade1().getDati().getWadeDtPresc().setWadeDtPresc(0);
        ws.getAreaBusiness().getLccvade1().getDati().setWadeConcsPrest(Types.SPACE_CHAR);
    }

    public void initWbepAreaBenef() {
        ws.getAreaBusiness().setWbepEleBenefMax(((short)0));
        for (int idx0 = 1; idx0 <= AreaBusinessIvvs0211.WBEP_TAB_BENEFICIARI_MAXOCCURS; idx0++) {
            ws.getAreaBusiness().getWbepTabBeneficiari(idx0).getLccvbep1().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWbepTabBeneficiari(idx0).getLccvbep1().setIdPtf(0);
            ws.getAreaBusiness().getWbepTabBeneficiari(idx0).getLccvbep1().getDati().setWbepIdBnfic(0);
            ws.getAreaBusiness().getWbepTabBeneficiari(idx0).getLccvbep1().getDati().setWbepIdRappAna(0);
            ws.getAreaBusiness().getWbepTabBeneficiari(idx0).getLccvbep1().getDati().getWbepIdBnficr().setWbepIdBnficr(0);
            ws.getAreaBusiness().getWbepTabBeneficiari(idx0).getLccvbep1().getDati().setWbepIdMoviCrz(0);
            ws.getAreaBusiness().getWbepTabBeneficiari(idx0).getLccvbep1().getDati().getWbepIdMoviChiu().setWbepIdMoviChiu(0);
            ws.getAreaBusiness().getWbepTabBeneficiari(idx0).getLccvbep1().getDati().setWbepDtIniEff(0);
            ws.getAreaBusiness().getWbepTabBeneficiari(idx0).getLccvbep1().getDati().setWbepDtEndEff(0);
            ws.getAreaBusiness().getWbepTabBeneficiari(idx0).getLccvbep1().getDati().setWbepCodCompAnia(0);
            ws.getAreaBusiness().getWbepTabBeneficiari(idx0).getLccvbep1().getDati().getWbepCodBnfic().setWbepCodBnfic(((short)0));
            ws.getAreaBusiness().getWbepTabBeneficiari(idx0).getLccvbep1().getDati().setWbepTpIndBnficr("");
            ws.getAreaBusiness().getWbepTabBeneficiari(idx0).getLccvbep1().getDati().setWbepCodBnficr("");
            ws.getAreaBusiness().getWbepTabBeneficiari(idx0).getLccvbep1().getDati().setWbepDescBnficr("");
            ws.getAreaBusiness().getWbepTabBeneficiari(idx0).getLccvbep1().getDati().getWbepPcDelBnficr().setWbepPcDelBnficr(new AfDecimal(0, 6, 3));
            ws.getAreaBusiness().getWbepTabBeneficiari(idx0).getLccvbep1().getDati().setWbepFlEse(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWbepTabBeneficiari(idx0).getLccvbep1().getDati().setWbepFlIrrev(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWbepTabBeneficiari(idx0).getLccvbep1().getDati().setWbepFlDflt(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWbepTabBeneficiari(idx0).getLccvbep1().getDati().setWbepEsrcnAttvtImprs(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWbepTabBeneficiari(idx0).getLccvbep1().getDati().setWbepFlBnficrColl(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWbepTabBeneficiari(idx0).getLccvbep1().getDati().setWbepDsRiga(0);
            ws.getAreaBusiness().getWbepTabBeneficiari(idx0).getLccvbep1().getDati().setWbepDsOperSql(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWbepTabBeneficiari(idx0).getLccvbep1().getDati().setWbepDsVer(0);
            ws.getAreaBusiness().getWbepTabBeneficiari(idx0).getLccvbep1().getDati().setWbepDsTsIniCptz(0);
            ws.getAreaBusiness().getWbepTabBeneficiari(idx0).getLccvbep1().getDati().setWbepDsTsEndCptz(0);
            ws.getAreaBusiness().getWbepTabBeneficiari(idx0).getLccvbep1().getDati().setWbepDsUtente("");
            ws.getAreaBusiness().getWbepTabBeneficiari(idx0).getLccvbep1().getDati().setWbepDsStatoElab(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWbepTabBeneficiari(idx0).getLccvbep1().getDati().setWbepTpNormalBnfic("");
        }
    }

    public void initWbelAreaBenefLiq() {
        ws.getAreaBusiness().setWbelEleBenefLiqMax(((short)0));
        for (int idx0 = 1; idx0 <= AreaBusinessIvvs0211.WBEL_TAB_BENE_LIQ_MAXOCCURS; idx0++) {
            ws.getAreaBusiness().getWbelTabBeneLiq(idx0).getLccvbel1().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWbelTabBeneLiq(idx0).getLccvbel1().setIdPtf(0);
            ws.getAreaBusiness().getWbelTabBeneLiq(idx0).getLccvbel1().getDati().setWbelIdBnficrLiq(0);
            ws.getAreaBusiness().getWbelTabBeneLiq(idx0).getLccvbel1().getDati().setWbelIdLiq(0);
            ws.getAreaBusiness().getWbelTabBeneLiq(idx0).getLccvbel1().getDati().setWbelIdMoviCrz(0);
            ws.getAreaBusiness().getWbelTabBeneLiq(idx0).getLccvbel1().getDati().getWbelIdMoviChiu().setWbelIdMoviChiu(0);
            ws.getAreaBusiness().getWbelTabBeneLiq(idx0).getLccvbel1().getDati().setWbelCodCompAnia(0);
            ws.getAreaBusiness().getWbelTabBeneLiq(idx0).getLccvbel1().getDati().setWbelDtIniEff(0);
            ws.getAreaBusiness().getWbelTabBeneLiq(idx0).getLccvbel1().getDati().setWbelDtEndEff(0);
            ws.getAreaBusiness().getWbelTabBeneLiq(idx0).getLccvbel1().getDati().getWbelIdRappAna().setWbelIdRappAna(0);
            ws.getAreaBusiness().getWbelTabBeneLiq(idx0).getLccvbel1().getDati().setWbelCodBnficr("");
            ws.getAreaBusiness().getWbelTabBeneLiq(idx0).getLccvbel1().getDati().setWbelDescBnficr("");
            ws.getAreaBusiness().getWbelTabBeneLiq(idx0).getLccvbel1().getDati().getWbelPcLiq().setWbelPcLiq(new AfDecimal(0, 6, 3));
            ws.getAreaBusiness().getWbelTabBeneLiq(idx0).getLccvbel1().getDati().setWbelEsrcnAttvtImprs(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWbelTabBeneLiq(idx0).getLccvbel1().getDati().setWbelTpIndBnficr("");
            ws.getAreaBusiness().getWbelTabBeneLiq(idx0).getLccvbel1().getDati().setWbelFlEse(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWbelTabBeneLiq(idx0).getLccvbel1().getDati().setWbelFlIrrev(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWbelTabBeneLiq(idx0).getLccvbel1().getDati().getWbelImpLrdLiqto().setWbelImpLrdLiqto(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWbelTabBeneLiq(idx0).getLccvbel1().getDati().getWbelImpstIpt().setWbelImpstIpt(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWbelTabBeneLiq(idx0).getLccvbel1().getDati().getWbelImpNetLiqto().setWbelImpNetLiqto(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWbelTabBeneLiq(idx0).getLccvbel1().getDati().getWbelRitAccIpt().setWbelRitAccIpt(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWbelTabBeneLiq(idx0).getLccvbel1().getDati().getWbelRitVisIpt().setWbelRitVisIpt(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWbelTabBeneLiq(idx0).getLccvbel1().getDati().getWbelRitTfrIpt().setWbelRitTfrIpt(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWbelTabBeneLiq(idx0).getLccvbel1().getDati().getWbelImpstIrpefIpt().setWbelImpstIrpefIpt(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWbelTabBeneLiq(idx0).getLccvbel1().getDati().getWbelImpstSostIpt().setWbelImpstSostIpt(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWbelTabBeneLiq(idx0).getLccvbel1().getDati().getWbelImpstPrvrIpt().setWbelImpstPrvrIpt(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWbelTabBeneLiq(idx0).getLccvbel1().getDati().getWbelImpst252Ipt().setWbelImpst252Ipt(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWbelTabBeneLiq(idx0).getLccvbel1().getDati().getWbelIdAssto().setWbelIdAssto(0);
            ws.getAreaBusiness().getWbelTabBeneLiq(idx0).getLccvbel1().getDati().getWbelTaxSep().setWbelTaxSep(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWbelTabBeneLiq(idx0).getLccvbel1().getDati().getWbelDtRiserveSomP().setWbelDtRiserveSomP(0);
            ws.getAreaBusiness().getWbelTabBeneLiq(idx0).getLccvbel1().getDati().getWbelDtVlt().setWbelDtVlt(0);
            ws.getAreaBusiness().getWbelTabBeneLiq(idx0).getLccvbel1().getDati().setWbelTpStatLiqBnficr("");
            ws.getAreaBusiness().getWbelTabBeneLiq(idx0).getLccvbel1().getDati().setWbelDsRiga(0);
            ws.getAreaBusiness().getWbelTabBeneLiq(idx0).getLccvbel1().getDati().setWbelDsOperSql(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWbelTabBeneLiq(idx0).getLccvbel1().getDati().setWbelDsVer(0);
            ws.getAreaBusiness().getWbelTabBeneLiq(idx0).getLccvbel1().getDati().setWbelDsTsIniCptz(0);
            ws.getAreaBusiness().getWbelTabBeneLiq(idx0).getLccvbel1().getDati().setWbelDsTsEndCptz(0);
            ws.getAreaBusiness().getWbelTabBeneLiq(idx0).getLccvbel1().getDati().setWbelDsUtente("");
            ws.getAreaBusiness().getWbelTabBeneLiq(idx0).getLccvbel1().getDati().setWbelDsStatoElab(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWbelTabBeneLiq(idx0).getLccvbel1().getDati().setWbelRichCalcCnbtInp(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWbelTabBeneLiq(idx0).getLccvbel1().getDati().getWbelImpIntrRitPag().setWbelImpIntrRitPag(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWbelTabBeneLiq(idx0).getLccvbel1().getDati().getWbelDtUltDocto().setWbelDtUltDocto(0);
            ws.getAreaBusiness().getWbelTabBeneLiq(idx0).getLccvbel1().getDati().getWbelDtDormienza().setWbelDtDormienza(0);
            ws.getAreaBusiness().getWbelTabBeneLiq(idx0).getLccvbel1().getDati().getWbelImpstBolloTotV().setWbelImpstBolloTotV(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWbelTabBeneLiq(idx0).getLccvbel1().getDati().getWbelImpstVis1382011().setWbelImpstVis1382011(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWbelTabBeneLiq(idx0).getLccvbel1().getDati().getWbelImpstSost1382011().setWbelImpstSost1382011(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWbelTabBeneLiq(idx0).getLccvbel1().getDati().getWbelImpstVis662014().setWbelImpstVis662014(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWbelTabBeneLiq(idx0).getLccvbel1().getDati().getWbelImpstSost662014().setWbelImpstSost662014(new AfDecimal(0, 15, 3));
        }
    }

    public void initWdcoAreaDtCollettiva() {
        ws.getAreaBusiness().setWdcoEleCollMax(((short)0));
        ws.getAreaBusiness().getLccvdco1().getStatus().setStatus(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvdco1().setIdPtf(0);
        ws.getAreaBusiness().getLccvdco1().getDati().setWdcoIdDColl(0);
        ws.getAreaBusiness().getLccvdco1().getDati().setWdcoIdPoli(0);
        ws.getAreaBusiness().getLccvdco1().getDati().setWdcoIdMoviCrz(0);
        ws.getAreaBusiness().getLccvdco1().getDati().getWdcoIdMoviChiu().setWdcoIdMoviChiu(0);
        ws.getAreaBusiness().getLccvdco1().getDati().setWdcoDtIniEff(0);
        ws.getAreaBusiness().getLccvdco1().getDati().setWdcoDtEndEff(0);
        ws.getAreaBusiness().getLccvdco1().getDati().setWdcoCodCompAnia(0);
        ws.getAreaBusiness().getLccvdco1().getDati().getWdcoImpArrotPre().setWdcoImpArrotPre(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdco1().getDati().getWdcoPcScon().setWdcoPcScon(new AfDecimal(0, 6, 3));
        ws.getAreaBusiness().getLccvdco1().getDati().setWdcoFlAdesSing(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvdco1().getDati().setWdcoTpImp("");
        ws.getAreaBusiness().getLccvdco1().getDati().setWdcoFlRiclPreDaCpt(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvdco1().getDati().setWdcoTpAdes("");
        ws.getAreaBusiness().getLccvdco1().getDati().getWdcoDtUltRinnTac().setWdcoDtUltRinnTac(0);
        ws.getAreaBusiness().getLccvdco1().getDati().getWdcoImpScon().setWdcoImpScon(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdco1().getDati().getWdcoFrazDflt().setWdcoFrazDflt(0);
        ws.getAreaBusiness().getLccvdco1().getDati().getWdcoEtaScadMascDflt().setWdcoEtaScadMascDflt(0);
        ws.getAreaBusiness().getLccvdco1().getDati().getWdcoEtaScadFemmDflt().setWdcoEtaScadFemmDflt(0);
        ws.getAreaBusiness().getLccvdco1().getDati().setWdcoTpDfltDur("");
        ws.getAreaBusiness().getLccvdco1().getDati().getWdcoDurAaAdesDflt().setWdcoDurAaAdesDflt(0);
        ws.getAreaBusiness().getLccvdco1().getDati().getWdcoDurMmAdesDflt().setWdcoDurMmAdesDflt(0);
        ws.getAreaBusiness().getLccvdco1().getDati().getWdcoDurGgAdesDflt().setWdcoDurGgAdesDflt(0);
        ws.getAreaBusiness().getLccvdco1().getDati().getWdcoDtScadAdesDflt().setWdcoDtScadAdesDflt(0);
        ws.getAreaBusiness().getLccvdco1().getDati().setWdcoCodFndDflt("");
        ws.getAreaBusiness().getLccvdco1().getDati().setWdcoTpDur("");
        ws.getAreaBusiness().getLccvdco1().getDati().setWdcoTpCalcDur("");
        ws.getAreaBusiness().getLccvdco1().getDati().setWdcoFlNoAderenti(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvdco1().getDati().setWdcoFlDistintaCnbtva(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvdco1().getDati().setWdcoFlCnbtAutes(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvdco1().getDati().setWdcoFlQtzPostEmis(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvdco1().getDati().setWdcoFlComnzFndIs(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvdco1().getDati().setWdcoDsRiga(0);
        ws.getAreaBusiness().getLccvdco1().getDati().setWdcoDsOperSql(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvdco1().getDati().setWdcoDsVer(0);
        ws.getAreaBusiness().getLccvdco1().getDati().setWdcoDsTsIniCptz(0);
        ws.getAreaBusiness().getLccvdco1().getDati().setWdcoDsTsEndCptz(0);
        ws.getAreaBusiness().getLccvdco1().getDati().setWdcoDsUtente("");
        ws.getAreaBusiness().getLccvdco1().getDati().setWdcoDsStatoElab(Types.SPACE_CHAR);
    }

    public void initWdfaAreaDtFiscAdes() {
        ws.getAreaBusiness().setWdfaEleFiscAdesMax(((short)0));
        ws.getAreaBusiness().getLccvdfa1().getStatus().setStatus(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvdfa1().setIdPtf(0);
        ws.getAreaBusiness().getLccvdfa1().getDati().setWdfaIdDFiscAdes(0);
        ws.getAreaBusiness().getLccvdfa1().getDati().setWdfaIdAdes(0);
        ws.getAreaBusiness().getLccvdfa1().getDati().setWdfaIdMoviCrz(0);
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaIdMoviChiu().setWdfaIdMoviChiu(0);
        ws.getAreaBusiness().getLccvdfa1().getDati().setWdfaDtIniEff(0);
        ws.getAreaBusiness().getLccvdfa1().getDati().setWdfaDtEndEff(0);
        ws.getAreaBusiness().getLccvdfa1().getDati().setWdfaCodCompAnia(0);
        ws.getAreaBusiness().getLccvdfa1().getDati().setWdfaTpIscFnd("");
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaDtAccnsRappFnd().setWdfaDtAccnsRappFnd(0);
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaImpCnbtAzK1().setWdfaImpCnbtAzK1(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaImpCnbtIscK1().setWdfaImpCnbtIscK1(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaImpCnbtTfrK1().setWdfaImpCnbtTfrK1(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaImpCnbtVolK1().setWdfaImpCnbtVolK1(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaImpCnbtAzK2().setWdfaImpCnbtAzK2(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaImpCnbtIscK2().setWdfaImpCnbtIscK2(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaImpCnbtTfrK2().setWdfaImpCnbtTfrK2(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaImpCnbtVolK2().setWdfaImpCnbtVolK2(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaMatuK1().setWdfaMatuK1(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaMatuResK1().setWdfaMatuResK1(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaMatuK2().setWdfaMatuK2(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaImpbVis().setWdfaImpbVis(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaImpstVis().setWdfaImpstVis(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaImpbIsK2().setWdfaImpbIsK2(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaImpstSostK2().setWdfaImpstSostK2(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaRidzTfr().setWdfaRidzTfr(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaPcTfr().setWdfaPcTfr(new AfDecimal(0, 6, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaAlqTfr().setWdfaAlqTfr(new AfDecimal(0, 6, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaTotAntic().setWdfaTotAntic(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaImpbTfrAntic().setWdfaImpbTfrAntic(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaImpstTfrAntic().setWdfaImpstTfrAntic(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaRidzTfrSuAntic().setWdfaRidzTfrSuAntic(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaImpbVisAntic().setWdfaImpbVisAntic(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaImpstVisAntic().setWdfaImpstVisAntic(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaImpbIsK2Antic().setWdfaImpbIsK2Antic(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaImpstSostK2Anti().setWdfaImpstSostK2Anti(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaUltCommisTrasfe().setWdfaUltCommisTrasfe(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().setWdfaCodDvs("");
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaAlqPrvr().setWdfaAlqPrvr(new AfDecimal(0, 6, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaPcEseImpstTfr().setWdfaPcEseImpstTfr(new AfDecimal(0, 6, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaImpEseImpstTfr().setWdfaImpEseImpstTfr(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaAnzCnbtvaCarass().setWdfaAnzCnbtvaCarass(((short)0));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaAnzCnbtvaCarazi().setWdfaAnzCnbtvaCarazi(((short)0));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaAnzSrvz().setWdfaAnzSrvz(((short)0));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaImpCnbtNdedK1().setWdfaImpCnbtNdedK1(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaImpCnbtNdedK2().setWdfaImpCnbtNdedK2(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaImpCnbtNdedK3().setWdfaImpCnbtNdedK3(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaImpCnbtAzK3().setWdfaImpCnbtAzK3(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaImpCnbtIscK3().setWdfaImpCnbtIscK3(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaImpCnbtTfrK3().setWdfaImpCnbtTfrK3(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaImpCnbtVolK3().setWdfaImpCnbtVolK3(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaMatuK3().setWdfaMatuK3(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaImpb252Antic().setWdfaImpb252Antic(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaImpst252Antic().setWdfaImpst252Antic(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaDt1aCnbz().setWdfaDt1aCnbz(0);
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaCommisDiTrasfe().setWdfaCommisDiTrasfe(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaAaCnbzK1().setWdfaAaCnbzK1(((short)0));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaAaCnbzK2().setWdfaAaCnbzK2(((short)0));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaAaCnbzK3().setWdfaAaCnbzK3(((short)0));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaMmCnbzK1().setWdfaMmCnbzK1(((short)0));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaMmCnbzK2().setWdfaMmCnbzK2(((short)0));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaMmCnbzK3().setWdfaMmCnbzK3(((short)0));
        ws.getAreaBusiness().getLccvdfa1().getDati().setWdfaFlApplzNewfis(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaRedtTassAbbatK3().setWdfaRedtTassAbbatK3(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaCnbtEcc4x100K1().setWdfaCnbtEcc4x100K1(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().setWdfaDsRiga(0);
        ws.getAreaBusiness().getLccvdfa1().getDati().setWdfaDsOperSql(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvdfa1().getDati().setWdfaDsVer(0);
        ws.getAreaBusiness().getLccvdfa1().getDati().setWdfaDsTsIniCptz(0);
        ws.getAreaBusiness().getLccvdfa1().getDati().setWdfaDsTsEndCptz(0);
        ws.getAreaBusiness().getLccvdfa1().getDati().setWdfaDsUtente("");
        ws.getAreaBusiness().getLccvdfa1().getDati().setWdfaDsStatoElab(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaCreditoIs().setWdfaCreditoIs(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaRedtTassAbbatK2().setWdfaRedtTassAbbatK2(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaImpbIsK3().setWdfaImpbIsK3(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaImpstSostK3().setWdfaImpstSostK3(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaImpb252K3().setWdfaImpb252K3(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaImpst252K3().setWdfaImpst252K3(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaImpbIsK3Antic().setWdfaImpbIsK3Antic(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaImpstSostK3Anti().setWdfaImpstSostK3Anti(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaImpbIrpefK1Anti().setWdfaImpbIrpefK1Anti(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaImpstIrpefK1Ant().setWdfaImpstIrpefK1Ant(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaImpbIrpefK2Anti().setWdfaImpbIrpefK2Anti(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaImpstIrpefK2Ant().setWdfaImpstIrpefK2Ant(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaDtCessazione().setWdfaDtCessazione(0);
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaTotImpst().setWdfaTotImpst(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaOnerTrasfe().setWdfaOnerTrasfe(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfa1().getDati().getWdfaImpNetTrasferito().setWdfaImpNetTrasferito(new AfDecimal(0, 15, 3));
    }

    public void initWdeqAreaDettQuest() {
        ws.getAreaBusiness().getWdeqAreaDettQuest().setWdeqEleDettQuestMax(((short)0));
        for (int idx0 = 1; idx0 <= WdeqTab.TAB_DETT_QUEST_MAXOCCURS; idx0++) {
            ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setStatus(idx0, Types.SPACE_CHAR);
            ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setIdPtf(idx0, 0);
            ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setIdDettQuest(idx0, 0);
            ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setIdQuest(idx0, 0);
            ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setIdMoviCrz(idx0, 0);
            ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setIdMoviChiu(idx0, 0);
            ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setDtIniEff(idx0, 0);
            ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setDtEndEff(idx0, 0);
            ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setCodCompAnia(idx0, 0);
            ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setCodQuest(idx0, "");
            ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setCodDom(idx0, "");
            ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setCodDomCollg(idx0, "");
            ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setRispNum(idx0, 0);
            ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setRispFl(idx0, Types.SPACE_CHAR);
            ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setRispTxt(idx0, "");
            ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setRispTs(idx0, new AfDecimal(0, 14, 9));
            ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setRispImp(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setRispPc(idx0, new AfDecimal(0, 6, 3));
            ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setRispDt(idx0, 0);
            ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setRispKey(idx0, "");
            ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setTpRisp(idx0, "");
            ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setIdCompQuest(idx0, 0);
            ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setValRisp(idx0, "");
            ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setDsRiga(idx0, 0);
            ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setDsOperSql(idx0, Types.SPACE_CHAR);
            ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setDsVer(idx0, 0);
            ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setDsTsIniCptz(idx0, 0);
            ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setDsTsEndCptz(idx0, 0);
            ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setDsUtente(idx0, "");
            ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setDsStatoElab(idx0, Types.SPACE_CHAR);
        }
    }

    public void initWdtcAreaDettTitCont() {
        ws.getAreaBusiness().setWdtcEleDettTitMax(((short)0));
        for (int idx0 = 1; idx0 <= AreaBusinessIvvs0211.WDTC_TAB_DTC_MAXOCCURS; idx0++) {
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().setIdPtf(0);
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().setWdtcIdDettTitCont(0);
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().setWdtcIdTitCont(0);
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().setWdtcIdOgg(0);
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().setWdtcTpOgg("");
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().setWdtcIdMoviCrz(0);
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().getWdtcIdMoviChiu().setWdtcIdMoviChiu(0);
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().setWdtcDtIniEff(0);
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().setWdtcDtEndEff(0);
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().setWdtcCodCompAnia(0);
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().getWdtcDtIniCop().setWdtcDtIniCop(0);
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().getWdtcDtEndCop().setWdtcDtEndCop(0);
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().getWdtcPreNet().setWdtcPreNet(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().getWdtcIntrFraz().setWdtcIntrFraz(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().getWdtcIntrMora().setWdtcIntrMora(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().getWdtcIntrRetdt().setWdtcIntrRetdt(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().getWdtcIntrRiat().setWdtcIntrRiat(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().getWdtcDir().setWdtcDir(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().getWdtcSpeMed().setWdtcSpeMed(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().getWdtcTax().setWdtcTax(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().getWdtcSoprSan().setWdtcSoprSan(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().getWdtcSoprSpo().setWdtcSoprSpo(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().getWdtcSoprTec().setWdtcSoprTec(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().getWdtcSoprProf().setWdtcSoprProf(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().getWdtcSoprAlt().setWdtcSoprAlt(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().getWdtcPreTot().setWdtcPreTot(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().getWdtcPrePpIas().setWdtcPrePpIas(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().getWdtcPreSoloRsh().setWdtcPreSoloRsh(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().getWdtcCarAcq().setWdtcCarAcq(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().getWdtcCarGest().setWdtcCarGest(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().getWdtcCarInc().setWdtcCarInc(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().getWdtcProvAcq1aa().setWdtcProvAcq1aa(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().getWdtcProvAcq2aa().setWdtcProvAcq2aa(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().getWdtcProvRicor().setWdtcProvRicor(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().getWdtcProvInc().setWdtcProvInc(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().getWdtcProvDaRec().setWdtcProvDaRec(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().setWdtcCodDvs("");
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().getWdtcFrqMovi().setWdtcFrqMovi(0);
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().setWdtcTpRgmFisc("");
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().setWdtcCodTari("");
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().setWdtcTpStatTit("");
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().getWdtcImpAz().setWdtcImpAz(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().getWdtcImpAder().setWdtcImpAder(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().getWdtcImpTfr().setWdtcImpTfr(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().getWdtcImpVolo().setWdtcImpVolo(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().getWdtcManfeeAntic().setWdtcManfeeAntic(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().getWdtcManfeeRicor().setWdtcManfeeRicor(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().getWdtcManfeeRec().setWdtcManfeeRec(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().getWdtcDtEsiTit().setWdtcDtEsiTit(0);
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().getWdtcSpeAge().setWdtcSpeAge(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().getWdtcCarIas().setWdtcCarIas(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().getWdtcTotIntrPrest().setWdtcTotIntrPrest(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().setWdtcDsRiga(0);
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().setWdtcDsOperSql(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().setWdtcDsVer(0);
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().setWdtcDsTsIniCptz(0);
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().setWdtcDsTsEndCptz(0);
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().setWdtcDsUtente("");
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().setWdtcDsStatoElab(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().getWdtcImpTrasfe().setWdtcImpTrasfe(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().getWdtcImpTfrStrc().setWdtcImpTfrStrc(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().getWdtcNumGgRitardoPag().setWdtcNumGgRitardoPag(0);
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().getWdtcNumGgRival().setWdtcNumGgRival(0);
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().getWdtcAcqExp().setWdtcAcqExp(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().getWdtcRemunAss().setWdtcRemunAss(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().getWdtcCommisInter().setWdtcCommisInter(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWdtcTabDtc(idx0).getLccvdtc1().getDati().getWdtcCnbtAntirac().setWdtcCnbtAntirac(new AfDecimal(0, 15, 3));
        }
    }

    public void initWgrzAreaGaranzia() {
        ws.getAreaBusiness().setWgrzEleGaranziaMax(((short)0));
        for (int idx0 = 1; idx0 <= AreaBusinessIvvs0211.WGRZ_TAB_GAR_MAXOCCURS; idx0++) {
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().setIdPtf(0);
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzIdGar(0);
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzIdAdes().setWgrzIdAdes(0);
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzIdPoli(0);
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzIdMoviCrz(0);
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzIdMoviChiu().setWgrzIdMoviChiu(0);
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDtIniEff(0);
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDtEndEff(0);
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzCodCompAnia(0);
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzIbOgg("");
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDtDecor().setWgrzDtDecor(0);
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDtScad().setWgrzDtScad(0);
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzCodSez("");
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzCodTari("");
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzRamoBila("");
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDtIniValTar().setWgrzDtIniValTar(0);
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzId1oAssto().setWgrzId1oAssto(0);
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzId2oAssto().setWgrzId2oAssto(0);
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzId3oAssto().setWgrzId3oAssto(0);
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzTpGar().setWgrzTpGar(((short)0));
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpRsh("");
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzTpInvst().setWgrzTpInvst(((short)0));
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzModPagGarcol("");
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpPerPre("");
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaAa1oAssto().setWgrzEtaAa1oAssto(((short)0));
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaMm1oAssto().setWgrzEtaMm1oAssto(((short)0));
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaAa2oAssto().setWgrzEtaAa2oAssto(((short)0));
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaMm2oAssto().setWgrzEtaMm2oAssto(((short)0));
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaAa3oAssto().setWgrzEtaAa3oAssto(((short)0));
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaMm3oAssto().setWgrzEtaMm3oAssto(((short)0));
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpEmisPur(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaAScad().setWgrzEtaAScad(0);
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpCalcPrePrstz("");
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpPre(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpDur("");
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDurAa().setWgrzDurAa(0);
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDurMm().setWgrzDurMm(0);
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDurGg().setWgrzDurGg(0);
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzNumAaPagPre().setWgrzNumAaPagPre(0);
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzAaPagPreUni().setWgrzAaPagPreUni(0);
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzMmPagPreUni().setWgrzMmPagPreUni(0);
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzFrazIniErogRen().setWgrzFrazIniErogRen(0);
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzMm1oRat().setWgrzMm1oRat(((short)0));
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzPc1oRat().setWgrzPc1oRat(new AfDecimal(0, 6, 3));
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpPrstzAssta("");
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDtEndCarz().setWgrzDtEndCarz(0);
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzPcRipPre().setWgrzPcRipPre(new AfDecimal(0, 6, 3));
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzCodFnd("");
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzAaRenCer("");
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzPcRevrsb().setWgrzPcRevrsb(new AfDecimal(0, 6, 3));
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpPcRip("");
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzPcOpz().setWgrzPcOpz(new AfDecimal(0, 6, 3));
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpIas("");
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpStab("");
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpAdegPre(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDtVarzTpIas().setWgrzDtVarzTpIas(0);
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzFrazDecrCpt().setWgrzFrazDecrCpt(0);
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzCodTratRiass("");
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpDtEmisRiass("");
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpCessRiass("");
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDsRiga(0);
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDsOperSql(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDsVer(0);
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDsTsIniCptz(0);
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDsTsEndCptz(0);
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDsUtente("");
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDsStatoElab(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzAaStab().setWgrzAaStab(0);
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzTsStabLimitata().setWgrzTsStabLimitata(new AfDecimal(0, 14, 9));
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDtPresc().setWgrzDtPresc(0);
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzRshInvst(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpRamoBila("");
        }
    }

    public void initWgrlAreaGaranziaLiq() {
        ws.getAreaBusiness().setWgrlEleGarLiqMax(((short)0));
        for (int idx0 = 1; idx0 <= AreaBusinessIvvs0211.WGRL_TAB_GAR_LIQ_MAXOCCURS; idx0++) {
            ws.getAreaBusiness().getWgrlTabGarLiq(idx0).getLccvgrl1().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWgrlTabGarLiq(idx0).getLccvgrl1().setIdPtf(0);
            ws.getAreaBusiness().getWgrlTabGarLiq(idx0).getLccvgrl1().getDati().setWgrlIdGarLiq(0);
            ws.getAreaBusiness().getWgrlTabGarLiq(idx0).getLccvgrl1().getDati().setWgrlIdLiq(0);
            ws.getAreaBusiness().getWgrlTabGarLiq(idx0).getLccvgrl1().getDati().setWgrlIdGar(0);
            ws.getAreaBusiness().getWgrlTabGarLiq(idx0).getLccvgrl1().getDati().setWgrlIdMoviCrz(0);
            ws.getAreaBusiness().getWgrlTabGarLiq(idx0).getLccvgrl1().getDati().getWgrlIdMoviChiu().setWgrlIdMoviChiu(0);
            ws.getAreaBusiness().getWgrlTabGarLiq(idx0).getLccvgrl1().getDati().setWgrlDtIniEff(0);
            ws.getAreaBusiness().getWgrlTabGarLiq(idx0).getLccvgrl1().getDati().setWgrlDtEndEff(0);
            ws.getAreaBusiness().getWgrlTabGarLiq(idx0).getLccvgrl1().getDati().setWgrlCodCompAnia(0);
            ws.getAreaBusiness().getWgrlTabGarLiq(idx0).getLccvgrl1().getDati().getWgrlDtSin1oAssto().setWgrlDtSin1oAssto(0);
            ws.getAreaBusiness().getWgrlTabGarLiq(idx0).getLccvgrl1().getDati().setWgrlCauSin1oAssto("");
            ws.getAreaBusiness().getWgrlTabGarLiq(idx0).getLccvgrl1().getDati().setWgrlTpSin1oAssto("");
            ws.getAreaBusiness().getWgrlTabGarLiq(idx0).getLccvgrl1().getDati().getWgrlDtSin2oAssto().setWgrlDtSin2oAssto(0);
            ws.getAreaBusiness().getWgrlTabGarLiq(idx0).getLccvgrl1().getDati().setWgrlCauSin2oAssto("");
            ws.getAreaBusiness().getWgrlTabGarLiq(idx0).getLccvgrl1().getDati().setWgrlTpSin2oAssto("");
            ws.getAreaBusiness().getWgrlTabGarLiq(idx0).getLccvgrl1().getDati().getWgrlDtSin3oAssto().setWgrlDtSin3oAssto(0);
            ws.getAreaBusiness().getWgrlTabGarLiq(idx0).getLccvgrl1().getDati().setWgrlCauSin3oAssto("");
            ws.getAreaBusiness().getWgrlTabGarLiq(idx0).getLccvgrl1().getDati().setWgrlTpSin3oAssto("");
            ws.getAreaBusiness().getWgrlTabGarLiq(idx0).getLccvgrl1().getDati().setWgrlDsRiga(0);
            ws.getAreaBusiness().getWgrlTabGarLiq(idx0).getLccvgrl1().getDati().setWgrlDsOperSql(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWgrlTabGarLiq(idx0).getLccvgrl1().getDati().setWgrlDsVer(0);
            ws.getAreaBusiness().getWgrlTabGarLiq(idx0).getLccvgrl1().getDati().setWgrlDsTsIniCptz(0);
            ws.getAreaBusiness().getWgrlTabGarLiq(idx0).getLccvgrl1().getDati().setWgrlDsTsEndCptz(0);
            ws.getAreaBusiness().getWgrlTabGarLiq(idx0).getLccvgrl1().getDati().setWgrlDsUtente("");
            ws.getAreaBusiness().getWgrlTabGarLiq(idx0).getLccvgrl1().getDati().setWgrlDsStatoElab(Types.SPACE_CHAR);
        }
    }

    public void initWisoAreaImpostaSost() {
        ws.getAreaBusiness().setWisoEleImpSostMax(((short)0));
        ws.getAreaBusiness().getLccviso1().getStatus().setStatus(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccviso1().setIdPtf(0);
        ws.getAreaBusiness().getLccviso1().getDati().setWisoIdImpstSost(0);
        ws.getAreaBusiness().getLccviso1().getDati().getWisoIdOgg().setWisoIdOgg(0);
        ws.getAreaBusiness().getLccviso1().getDati().setWisoTpOgg("");
        ws.getAreaBusiness().getLccviso1().getDati().setWisoIdMoviCrz(0);
        ws.getAreaBusiness().getLccviso1().getDati().getWisoIdMoviChiu().setWisoIdMoviChiu(0);
        ws.getAreaBusiness().getLccviso1().getDati().setWisoDtIniEff(0);
        ws.getAreaBusiness().getLccviso1().getDati().setWisoDtEndEff(0);
        ws.getAreaBusiness().getLccviso1().getDati().getWisoDtIniPer().setWisoDtIniPer(0);
        ws.getAreaBusiness().getLccviso1().getDati().getWisoDtEndPer().setWisoDtEndPer(0);
        ws.getAreaBusiness().getLccviso1().getDati().setWisoCodCompAnia(0);
        ws.getAreaBusiness().getLccviso1().getDati().getWisoImpstSost().setWisoImpstSost(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccviso1().getDati().getWisoImpbIs().setWisoImpbIs(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccviso1().getDati().getWisoAlqIs().setWisoAlqIs(new AfDecimal(0, 6, 3));
        ws.getAreaBusiness().getLccviso1().getDati().setWisoCodTrb("");
        ws.getAreaBusiness().getLccviso1().getDati().getWisoPrstzLrdAnteIs().setWisoPrstzLrdAnteIs(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccviso1().getDati().getWisoRisMatNetPrec().setWisoRisMatNetPrec(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccviso1().getDati().getWisoRisMatAnteTax().setWisoRisMatAnteTax(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccviso1().getDati().getWisoRisMatPostTax().setWisoRisMatPostTax(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccviso1().getDati().getWisoPrstzNet().setWisoPrstzNet(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccviso1().getDati().getWisoPrstzPrec().setWisoPrstzPrec(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccviso1().getDati().getWisoCumPreVers().setWisoCumPreVers(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccviso1().getDati().setWisoTpCalcImpst("");
        ws.getAreaBusiness().getLccviso1().getDati().setWisoImpGiaTassato(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccviso1().getDati().setWisoDsRiga(0);
        ws.getAreaBusiness().getLccviso1().getDati().setWisoDsOperSql(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccviso1().getDati().setWisoDsVer(0);
        ws.getAreaBusiness().getLccviso1().getDati().setWisoDsTsIniCptz(0);
        ws.getAreaBusiness().getLccviso1().getDati().setWisoDsTsEndCptz(0);
        ws.getAreaBusiness().getLccviso1().getDati().setWisoDsUtente("");
        ws.getAreaBusiness().getLccviso1().getDati().setWisoDsStatoElab(Types.SPACE_CHAR);
    }

    public void initWlquAreaLiquidazione() {
        ws.getAreaBusiness().setWlquEleLiqMax(((short)0));
        for (int idx0 = 1; idx0 <= AreaBusinessIvvs0211.WLQU_TAB_LIQ_MAXOCCURS; idx0++) {
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().setWlquIdPtf(0);
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().setWlquIdLiq(0);
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().setWlquIdOgg(0);
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().setWlquTpOgg("");
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().setWlquIdMoviCrz(0);
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089IdMoviChiu().setWlquIdMoviChiu(0);
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().setWlquDtIniEff(0);
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().setWlquDtEndEff(0);
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().setWlquCodCompAnia(0);
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().setWlquIbOgg("");
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().setWlquTpLiq("");
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().setWlquDescCauEveSin("");
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().setWlquCodCauSin("");
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().setWlquCodSinCatstrf("");
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089DtMor().setWlquDtMor(0);
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089DtDen().setWlquDtDen(0);
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089DtPervDen().setWlquDtPervDen(0);
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089DtRich().setWlquDtRich(0);
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().setWlquTpSin("");
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().setWlquTpRisc("");
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089TpMetRisc().setWlquTpMetRisc(((short)0));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089DtLiq().setWlquDtLiq(0);
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().setWlquCodDvs("");
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089TotImpLrdLiqto().setWlquTotImpLrdLiqto(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089TotImpPrest().setWlquTotImpPrest(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089TotImpIntrPrest().setWlquTotImpIntrPrest(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089TotImpUti().setWlquTotImpUti(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089TotImpRitTfr().setWlquTotImpRitTfr(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089TotImpRitAcc().setWlquTotImpRitAcc(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089TotImpRitVis().setWlquTotImpRitVis(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089TotImpbTfr().setWlquTotImpbTfr(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089TotImpbAcc().setWlquTotImpbAcc(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089TotImpbVis().setWlquTotImpbVis(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089TotImpRimb().setWlquTotImpRimb(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpbImpstPrvr().setWlquImpbImpstPrvr(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpstPrvr().setWlquImpstPrvr(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpbImpst252().setWlquImpbImpst252(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089Impst252().setWlquImpst252(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089TotImpIs().setWlquTotImpIs(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpDirLiq().setWlquImpDirLiq(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089TotImpNetLiqto().setWlquTotImpNetLiqto(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089MontEnd2000().setWlquMontEnd2000(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089MontEnd2006().setWlquMontEnd2006(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089PcRen().setWlquPcRen(new AfDecimal(0, 6, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpPnl().setWlquImpPnl(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpbIrpef().setWlquImpbIrpef(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpstIrpef().setWlquImpstIrpef(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089DtVlt().setWlquDtVlt(0);
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089DtEndIstr().setWlquDtEndIstr(0);
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().setWlquTpRimb("");
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089SpeRcs().setWlquSpeRcs(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().setWlquIbLiq("");
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089TotIasOnerPrvnt().setWlquTotIasOnerPrvnt(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089TotIasMggSin().setWlquTotIasMggSin(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089TotIasRstDpst().setWlquTotIasRstDpst(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpOnerLiq().setWlquImpOnerLiq(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089ComponTaxRimb().setWlquComponTaxRimb(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().setWlquTpMezPag("");
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpExcontr().setWlquImpExcontr(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpIntrRitPag().setWlquImpIntrRitPag(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089BnsNonGoduto().setWlquBnsNonGoduto(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089CnbtInpstfm().setWlquCnbtInpstfm(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpstDaRimb().setWlquImpstDaRimb(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpbIs().setWlquImpbIs(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089TaxSep().setWlquTaxSep(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpbTaxSep().setWlquImpbTaxSep(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpbIntrSuPrest().setWlquImpbIntrSuPrest(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089AddizComun().setWlquAddizComun(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpbAddizComun().setWlquImpbAddizComun(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089AddizRegion().setWlquAddizRegion(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpbAddizRegion().setWlquImpbAddizRegion(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089MontDal2007().setWlquMontDal2007(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpbCnbtInpstfm().setWlquImpbCnbtInpstfm(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpLrdDaRimb().setWlquImpLrdDaRimb(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpDirDaRimb().setWlquImpDirDaRimb(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089RisMat().setWlquRisMat(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089RisSpe().setWlquRisSpe(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().setWlquDsRiga(0);
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().setWlquDsOperSql(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().setWlquDsVer(0);
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().setWlquDsTsIniCptz(0);
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().setWlquDsTsEndCptz(0);
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().setWlquDsUtente("");
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().setWlquDsStatoElab(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089TotIasPnl().setWlquTotIasPnl(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().setWlquFlEveGarto(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpRenK1().setWlquImpRenK1(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpRenK2().setWlquImpRenK2(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpRenK3().setWlquImpRenK3(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089PcRenK1().setWlquPcRenK1(new AfDecimal(0, 6, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089PcRenK2().setWlquPcRenK2(new AfDecimal(0, 6, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089PcRenK3().setWlquPcRenK3(new AfDecimal(0, 6, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().setWlquTpCausAntic("");
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpLrdLiqtoRilt().setWlquImpLrdLiqtoRilt(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpstApplRilt().setWlquImpstApplRilt(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089PcRiscParz().setWlquPcRiscParz(new AfDecimal(0, 12, 5));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpstBolloTotV().setWlquImpstBolloTotV(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpstBolloDettC().setWlquImpstBolloDettC(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpstBolloTotSw().setWlquImpstBolloTotSw(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpstBolloTotAa().setWlquImpstBolloTotAa(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpbVis1382011().setWlquImpbVis1382011(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpstVis1382011().setWlquImpstVis1382011(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpbIs1382011().setWlquImpbIs1382011(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpstSost1382011().setWlquImpstSost1382011(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089PcAbbTitStat().setWlquPcAbbTitStat(new AfDecimal(0, 6, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpbBolloDettC().setWlquImpbBolloDettC(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().setWlquFlPreComp(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpbVis662014().setWlquImpbVis662014(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpstVis662014().setWlquImpstVis662014(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpbIs662014().setWlquImpbIs662014(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpstSost662014().setWlquImpstSost662014(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089PcAbbTs662014().setWlquPcAbbTs662014(new AfDecimal(0, 6, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpLrdCalcCp().setWlquImpLrdCalcCp(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWlquTabLiq(idx0).getLccvlqu1().getDati().getS089CosTunnelUscita().setWlquCosTunnelUscita(new AfDecimal(0, 15, 3));
        }
    }

    public void initWmovAreaMovimento() {
        ws.getAreaBusiness().setWmovEleMoviMax(((short)0));
        ws.getAreaBusiness().getLccvmov1().getStatus().setStatus(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvmov1().setIdPtf(0);
        ws.getAreaBusiness().getLccvmov1().getDati().setWmovIdMovi(0);
        ws.getAreaBusiness().getLccvmov1().getDati().setWmovCodCompAnia(0);
        ws.getAreaBusiness().getLccvmov1().getDati().getWmovIdOgg().setWmovIdOgg(0);
        ws.getAreaBusiness().getLccvmov1().getDati().setWmovIbOgg("");
        ws.getAreaBusiness().getLccvmov1().getDati().setWmovIbMovi("");
        ws.getAreaBusiness().getLccvmov1().getDati().setWmovTpOgg("");
        ws.getAreaBusiness().getLccvmov1().getDati().getWmovIdRich().setWmovIdRich(0);
        ws.getAreaBusiness().getLccvmov1().getDati().getWmovTpMovi().setWmovTpMovi(0);
        ws.getAreaBusiness().getLccvmov1().getDati().setWmovDtEff(0);
        ws.getAreaBusiness().getLccvmov1().getDati().getWmovIdMoviAnn().setWmovIdMoviAnn(0);
        ws.getAreaBusiness().getLccvmov1().getDati().getWmovIdMoviCollg().setWmovIdMoviCollg(0);
        ws.getAreaBusiness().getLccvmov1().getDati().setWmovDsOperSql(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvmov1().getDati().setWmovDsVer(0);
        ws.getAreaBusiness().getLccvmov1().getDati().setWmovDsTsCptz(0);
        ws.getAreaBusiness().getLccvmov1().getDati().setWmovDsUtente("");
        ws.getAreaBusiness().getLccvmov1().getDati().setWmovDsStatoElab(Types.SPACE_CHAR);
    }

    public void initWmfzAreaMoviFinrio() {
        ws.getAreaBusiness().setWmfzEleMoviFinrioMax(((short)0));
        for (int idx0 = 1; idx0 <= AreaBusinessIvvs0211.WMFZ_TAB_MOVI_FINRIO_MAXOCCURS; idx0++) {
            ws.getAreaBusiness().getWmfzTabMoviFinrio(idx0).getLccvmfz1().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWmfzTabMoviFinrio(idx0).getLccvmfz1().setIdPtf(0);
            ws.getAreaBusiness().getWmfzTabMoviFinrio(idx0).getLccvmfz1().getDati().setWmfzIdMoviFinrio(0);
            ws.getAreaBusiness().getWmfzTabMoviFinrio(idx0).getLccvmfz1().getDati().getWmfzIdAdes().setWmfzIdAdes(0);
            ws.getAreaBusiness().getWmfzTabMoviFinrio(idx0).getLccvmfz1().getDati().getWmfzIdLiq().setWmfzIdLiq(0);
            ws.getAreaBusiness().getWmfzTabMoviFinrio(idx0).getLccvmfz1().getDati().getWmfzIdTitCont().setWmfzIdTitCont(0);
            ws.getAreaBusiness().getWmfzTabMoviFinrio(idx0).getLccvmfz1().getDati().setWmfzIdMoviCrz(0);
            ws.getAreaBusiness().getWmfzTabMoviFinrio(idx0).getLccvmfz1().getDati().getWmfzIdMoviChiu().setWmfzIdMoviChiu(0);
            ws.getAreaBusiness().getWmfzTabMoviFinrio(idx0).getLccvmfz1().getDati().setWmfzDtIniEff(0);
            ws.getAreaBusiness().getWmfzTabMoviFinrio(idx0).getLccvmfz1().getDati().setWmfzDtEndEff(0);
            ws.getAreaBusiness().getWmfzTabMoviFinrio(idx0).getLccvmfz1().getDati().setWmfzCodCompAnia(0);
            ws.getAreaBusiness().getWmfzTabMoviFinrio(idx0).getLccvmfz1().getDati().setWmfzTpMoviFinrio("");
            ws.getAreaBusiness().getWmfzTabMoviFinrio(idx0).getLccvmfz1().getDati().getWmfzDtEffMoviFinrio().setWmfzDtEffMoviFinrio(0);
            ws.getAreaBusiness().getWmfzTabMoviFinrio(idx0).getLccvmfz1().getDati().getWmfzDtElab().setWmfzDtElab(0);
            ws.getAreaBusiness().getWmfzTabMoviFinrio(idx0).getLccvmfz1().getDati().getWmfzDtRichMovi().setWmfzDtRichMovi(0);
            ws.getAreaBusiness().getWmfzTabMoviFinrio(idx0).getLccvmfz1().getDati().getWmfzCosOprz().setWmfzCosOprz(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWmfzTabMoviFinrio(idx0).getLccvmfz1().getDati().setWmfzStatMovi("");
            ws.getAreaBusiness().getWmfzTabMoviFinrio(idx0).getLccvmfz1().getDati().setWmfzDsRiga(0);
            ws.getAreaBusiness().getWmfzTabMoviFinrio(idx0).getLccvmfz1().getDati().setWmfzDsOperSql(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWmfzTabMoviFinrio(idx0).getLccvmfz1().getDati().setWmfzDsVer(0);
            ws.getAreaBusiness().getWmfzTabMoviFinrio(idx0).getLccvmfz1().getDati().setWmfzDsTsIniCptz(0);
            ws.getAreaBusiness().getWmfzTabMoviFinrio(idx0).getLccvmfz1().getDati().setWmfzDsTsEndCptz(0);
            ws.getAreaBusiness().getWmfzTabMoviFinrio(idx0).getLccvmfz1().getDati().setWmfzDsUtente("");
            ws.getAreaBusiness().getWmfzTabMoviFinrio(idx0).getLccvmfz1().getDati().setWmfzDsStatoElab(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWmfzTabMoviFinrio(idx0).getLccvmfz1().getDati().setWmfzTpCausRettifica("");
        }
    }

    public void initWpogAreaParamOgg() {
        ws.getAreaBusiness().getWpogAreaParamOgg().setWpogEleParamOggMax(((short)0));
        for (int idx0 = 1; idx0 <= WpogTab.TAB_PARAM_OGG_MAXOCCURS; idx0++) {
            ws.getAreaBusiness().getWpogAreaParamOgg().getWpogTab().setStatus(idx0, Types.SPACE_CHAR);
            ws.getAreaBusiness().getWpogAreaParamOgg().getWpogTab().setIdPtf(idx0, 0);
            ws.getAreaBusiness().getWpogAreaParamOgg().getWpogTab().setIdParamOgg(idx0, 0);
            ws.getAreaBusiness().getWpogAreaParamOgg().getWpogTab().setIdOgg(idx0, 0);
            ws.getAreaBusiness().getWpogAreaParamOgg().getWpogTab().setTpOgg(idx0, "");
            ws.getAreaBusiness().getWpogAreaParamOgg().getWpogTab().setIdMoviCrz(idx0, 0);
            ws.getAreaBusiness().getWpogAreaParamOgg().getWpogTab().setIdMoviChiu(idx0, 0);
            ws.getAreaBusiness().getWpogAreaParamOgg().getWpogTab().setDtIniEff(idx0, 0);
            ws.getAreaBusiness().getWpogAreaParamOgg().getWpogTab().setDtEndEff(idx0, 0);
            ws.getAreaBusiness().getWpogAreaParamOgg().getWpogTab().setCodCompAnia(idx0, 0);
            ws.getAreaBusiness().getWpogAreaParamOgg().getWpogTab().setCodParam(idx0, "");
            ws.getAreaBusiness().getWpogAreaParamOgg().getWpogTab().setTpParam(idx0, Types.SPACE_CHAR);
            ws.getAreaBusiness().getWpogAreaParamOgg().getWpogTab().setTpD(idx0, "");
            ws.getAreaBusiness().getWpogAreaParamOgg().getWpogTab().setValImp(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWpogAreaParamOgg().getWpogTab().setValDt(idx0, 0);
            ws.getAreaBusiness().getWpogAreaParamOgg().getWpogTab().setValTs(idx0, new AfDecimal(0, 14, 9));
            ws.getAreaBusiness().getWpogAreaParamOgg().getWpogTab().setValTxt(idx0, "");
            ws.getAreaBusiness().getWpogAreaParamOgg().getWpogTab().setValFl(idx0, Types.SPACE_CHAR);
            ws.getAreaBusiness().getWpogAreaParamOgg().getWpogTab().setValNum(idx0, 0);
            ws.getAreaBusiness().getWpogAreaParamOgg().getWpogTab().setValPc(idx0, new AfDecimal(0, 14, 9));
            ws.getAreaBusiness().getWpogAreaParamOgg().getWpogTab().setDsRiga(idx0, 0);
            ws.getAreaBusiness().getWpogAreaParamOgg().getWpogTab().setDsOperSql(idx0, Types.SPACE_CHAR);
            ws.getAreaBusiness().getWpogAreaParamOgg().getWpogTab().setDsVer(idx0, 0);
            ws.getAreaBusiness().getWpogAreaParamOgg().getWpogTab().setDsTsIniCptz(idx0, 0);
            ws.getAreaBusiness().getWpogAreaParamOgg().getWpogTab().setDsTsEndCptz(idx0, 0);
            ws.getAreaBusiness().getWpogAreaParamOgg().getWpogTab().setDsUtente(idx0, "");
            ws.getAreaBusiness().getWpogAreaParamOgg().getWpogTab().setDsStatoElab(idx0, Types.SPACE_CHAR);
        }
    }

    public void initWpmoAreaParamMov() {
        ws.getAreaBusiness().getWpmoAreaParamMov().setWpmoEleParamMovMax(((short)0));
        for (int idx0 = 1; idx0 <= WpmoTab.TAB_PARAM_MOV_MAXOCCURS; idx0++) {
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setStatus(idx0, Types.SPACE_CHAR);
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setIdPtf(idx0, 0);
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setIdParamMovi(idx0, 0);
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setIdOgg(idx0, 0);
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setTpOgg(idx0, "");
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setIdMoviCrz(idx0, 0);
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setIdMoviChiu(idx0, 0);
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setDtIniEff(idx0, 0);
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setDtEndEff(idx0, 0);
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setCodCompAnia(idx0, 0);
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setTpMovi(idx0, 0);
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setFrqMovi(idx0, 0);
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setDurAa(idx0, 0);
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setDurMm(idx0, 0);
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setDurGg(idx0, 0);
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setDtRicorPrec(idx0, 0);
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setDtRicorSucc(idx0, 0);
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setPcIntrFraz(idx0, new AfDecimal(0, 6, 3));
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setImpBnsDaScoTot(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setImpBnsDaSco(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setPcAnticBns(idx0, new AfDecimal(0, 6, 3));
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setTpRinnColl(idx0, "");
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setTpRivalPre(idx0, "");
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setTpRivalPrstz(idx0, "");
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setFlEvidRival(idx0, Types.SPACE_CHAR);
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setUltPcPerd(idx0, new AfDecimal(0, 6, 3));
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setTotAaGiaPror(idx0, 0);
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setTpOpz(idx0, "");
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setAaRenCer(idx0, 0);
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setPcRevrsb(idx0, new AfDecimal(0, 6, 3));
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setImpRiscParzPrgt(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setImpLrdDiRat(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setIbOgg(idx0, "");
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setCosOner(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setSpePc(idx0, new AfDecimal(0, 6, 3));
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setFlAttivGar(idx0, Types.SPACE_CHAR);
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setCambioVerProd(idx0, Types.SPACE_CHAR);
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setMmDiff(idx0, ((short)0));
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setImpRatManfee(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setDtUltErogManfee(idx0, 0);
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setTpOggRival(idx0, "");
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setSomAsstaGarac(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setPcApplzOpz(idx0, new AfDecimal(0, 6, 3));
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setIdAdes(idx0, 0);
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setIdPoli(idx0, 0);
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setTpFrmAssva(idx0, "");
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setDsRiga(idx0, 0);
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setDsOperSql(idx0, Types.SPACE_CHAR);
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setDsVer(idx0, 0);
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setDsTsIniCptz(idx0, 0);
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setDsTsEndCptz(idx0, 0);
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setDsUtente(idx0, "");
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setDsStatoElab(idx0, Types.SPACE_CHAR);
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setTpEstrCnt(idx0, "");
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setCodRamo(idx0, "");
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setGenDaSin(idx0, Types.SPACE_CHAR);
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setCodTari(idx0, "");
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setNumRatPagPre(idx0, 0);
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setPcServVal(idx0, new AfDecimal(0, 6, 3));
            ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setEtaAaSoglBnficr(idx0, ((short)0));
        }
    }

    public void initWpliAreaPercLiq() {
        ws.getAreaBusiness().setWpliElePercLiqMax(((short)0));
        for (int idx0 = 1; idx0 <= AreaBusinessIvvs0211.WPLI_TAB_PERC_LIQ_MAXOCCURS; idx0++) {
            ws.getAreaBusiness().getWpliTabPercLiq(idx0).getLccvpli1().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWpliTabPercLiq(idx0).getLccvpli1().setIdPtf(0);
            ws.getAreaBusiness().getWpliTabPercLiq(idx0).getLccvpli1().getDati().setWpliIdPercLiq(0);
            ws.getAreaBusiness().getWpliTabPercLiq(idx0).getLccvpli1().getDati().setWpliIdBnficrLiq(0);
            ws.getAreaBusiness().getWpliTabPercLiq(idx0).getLccvpli1().getDati().setWpliIdMoviCrz(0);
            ws.getAreaBusiness().getWpliTabPercLiq(idx0).getLccvpli1().getDati().getWpliIdMoviChiu().setWpliIdMoviChiu(0);
            ws.getAreaBusiness().getWpliTabPercLiq(idx0).getLccvpli1().getDati().getWpliIdRappAna().setWpliIdRappAna(0);
            ws.getAreaBusiness().getWpliTabPercLiq(idx0).getLccvpli1().getDati().setWpliDtIniEff(0);
            ws.getAreaBusiness().getWpliTabPercLiq(idx0).getLccvpli1().getDati().setWpliDtEndEff(0);
            ws.getAreaBusiness().getWpliTabPercLiq(idx0).getLccvpli1().getDati().setWpliCodCompAnia(0);
            ws.getAreaBusiness().getWpliTabPercLiq(idx0).getLccvpli1().getDati().getWpliPcLiq().setWpliPcLiq(new AfDecimal(0, 6, 3));
            ws.getAreaBusiness().getWpliTabPercLiq(idx0).getLccvpli1().getDati().getWpliImpLiq().setWpliImpLiq(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWpliTabPercLiq(idx0).getLccvpli1().getDati().setWpliTpMezPag("");
            ws.getAreaBusiness().getWpliTabPercLiq(idx0).getLccvpli1().getDati().setWpliIterPagAvv(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWpliTabPercLiq(idx0).getLccvpli1().getDati().setWpliDsRiga(0);
            ws.getAreaBusiness().getWpliTabPercLiq(idx0).getLccvpli1().getDati().setWpliDsOperSql(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWpliTabPercLiq(idx0).getLccvpli1().getDati().setWpliDsVer(0);
            ws.getAreaBusiness().getWpliTabPercLiq(idx0).getLccvpli1().getDati().setWpliDsTsIniCptz(0);
            ws.getAreaBusiness().getWpliTabPercLiq(idx0).getLccvpli1().getDati().setWpliDsTsEndCptz(0);
            ws.getAreaBusiness().getWpliTabPercLiq(idx0).getLccvpli1().getDati().setWpliDsUtente("");
            ws.getAreaBusiness().getWpliTabPercLiq(idx0).getLccvpli1().getDati().setWpliDsStatoElab(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWpliTabPercLiq(idx0).getLccvpli1().getDati().getWpliDtVlt().setWpliDtVlt(0);
            ws.getAreaBusiness().getWpliTabPercLiq(idx0).getLccvpli1().getDati().setWpliIntCntCorrAccr("");
            ws.getAreaBusiness().getWpliTabPercLiq(idx0).getLccvpli1().getDati().setWpliCodIbanRitCon("");
        }
    }

    public void initWpolAreaPolizza() {
        ws.getAreaBusiness().setWpolElePoliMax(((short)0));
        ws.getAreaBusiness().getLccvpol1().getStatus().setStatus(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvpol1().setIdPtf(0);
        ws.getAreaBusiness().getLccvpol1().getDati().setWpolIdPoli(0);
        ws.getAreaBusiness().getLccvpol1().getDati().setWpolIdMoviCrz(0);
        ws.getAreaBusiness().getLccvpol1().getDati().getWpolIdMoviChiu().setWpolIdMoviChiu(0);
        ws.getAreaBusiness().getLccvpol1().getDati().setWpolIbOgg("");
        ws.getAreaBusiness().getLccvpol1().getDati().setWpolIbProp("");
        ws.getAreaBusiness().getLccvpol1().getDati().getWpolDtProp().setWpolDtProp(0);
        ws.getAreaBusiness().getLccvpol1().getDati().setWpolDtIniEff(0);
        ws.getAreaBusiness().getLccvpol1().getDati().setWpolDtEndEff(0);
        ws.getAreaBusiness().getLccvpol1().getDati().setWpolCodCompAnia(0);
        ws.getAreaBusiness().getLccvpol1().getDati().setWpolDtDecor(0);
        ws.getAreaBusiness().getLccvpol1().getDati().setWpolDtEmis(0);
        ws.getAreaBusiness().getLccvpol1().getDati().setWpolTpPoli("");
        ws.getAreaBusiness().getLccvpol1().getDati().getWpolDurAa().setWpolDurAa(0);
        ws.getAreaBusiness().getLccvpol1().getDati().getWpolDurMm().setWpolDurMm(0);
        ws.getAreaBusiness().getLccvpol1().getDati().getWpolDtScad().setWpolDtScad(0);
        ws.getAreaBusiness().getLccvpol1().getDati().setWpolCodProd("");
        ws.getAreaBusiness().getLccvpol1().getDati().setWpolDtIniVldtProd(0);
        ws.getAreaBusiness().getLccvpol1().getDati().setWpolCodConv("");
        ws.getAreaBusiness().getLccvpol1().getDati().setWpolCodRamo("");
        ws.getAreaBusiness().getLccvpol1().getDati().getWpolDtIniVldtConv().setWpolDtIniVldtConv(0);
        ws.getAreaBusiness().getLccvpol1().getDati().getWpolDtApplzConv().setWpolDtApplzConv(0);
        ws.getAreaBusiness().getLccvpol1().getDati().setWpolTpFrmAssva("");
        ws.getAreaBusiness().getLccvpol1().getDati().setWpolTpRgmFisc("");
        ws.getAreaBusiness().getLccvpol1().getDati().setWpolFlEstas(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvpol1().getDati().setWpolFlRshComun(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvpol1().getDati().setWpolFlRshComunCond(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvpol1().getDati().setWpolTpLivGenzTit("");
        ws.getAreaBusiness().getLccvpol1().getDati().setWpolFlCopFinanz(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvpol1().getDati().setWpolTpApplzDir("");
        ws.getAreaBusiness().getLccvpol1().getDati().getWpolSpeMed().setWpolSpeMed(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvpol1().getDati().getWpolDirEmis().setWpolDirEmis(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvpol1().getDati().getWpolDir1oVers().setWpolDir1oVers(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvpol1().getDati().getWpolDirVersAgg().setWpolDirVersAgg(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvpol1().getDati().setWpolCodDvs("");
        ws.getAreaBusiness().getLccvpol1().getDati().setWpolFlFntAz(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvpol1().getDati().setWpolFlFntAder(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvpol1().getDati().setWpolFlFntTfr(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvpol1().getDati().setWpolFlFntVolo(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvpol1().getDati().setWpolTpOpzAScad("");
        ws.getAreaBusiness().getLccvpol1().getDati().getWpolAaDiffProrDflt().setWpolAaDiffProrDflt(0);
        ws.getAreaBusiness().getLccvpol1().getDati().setWpolFlVerProd("");
        ws.getAreaBusiness().getLccvpol1().getDati().getWpolDurGg().setWpolDurGg(0);
        ws.getAreaBusiness().getLccvpol1().getDati().getWpolDirQuiet().setWpolDirQuiet(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvpol1().getDati().setWpolTpPtfEstno("");
        ws.getAreaBusiness().getLccvpol1().getDati().setWpolFlCumPreCntr(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvpol1().getDati().setWpolFlAmmbMovi(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvpol1().getDati().setWpolConvGeco("");
        ws.getAreaBusiness().getLccvpol1().getDati().setWpolDsRiga(0);
        ws.getAreaBusiness().getLccvpol1().getDati().setWpolDsOperSql(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvpol1().getDati().setWpolDsVer(0);
        ws.getAreaBusiness().getLccvpol1().getDati().setWpolDsTsIniCptz(0);
        ws.getAreaBusiness().getLccvpol1().getDati().setWpolDsTsEndCptz(0);
        ws.getAreaBusiness().getLccvpol1().getDati().setWpolDsUtente("");
        ws.getAreaBusiness().getLccvpol1().getDati().setWpolDsStatoElab(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvpol1().getDati().setWpolFlScudoFisc(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvpol1().getDati().setWpolFlTrasfe(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvpol1().getDati().setWpolFlTfrStrc(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvpol1().getDati().getWpolDtPresc().setWpolDtPresc(0);
        ws.getAreaBusiness().getLccvpol1().getDati().setWpolCodConvAgg("");
        ws.getAreaBusiness().getLccvpol1().getDati().setWpolSubcatProd("");
        ws.getAreaBusiness().getLccvpol1().getDati().setWpolFlQuestAdegzAss(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvpol1().getDati().setWpolCodTpa("");
        ws.getAreaBusiness().getLccvpol1().getDati().getWpolIdAccComm().setWpolIdAccComm(0);
        ws.getAreaBusiness().getLccvpol1().getDati().setWpolFlPoliCpiPr(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvpol1().getDati().setWpolFlPoliBundling(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvpol1().getDati().setWpolIndPoliPrinColl(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvpol1().getDati().setWpolFlVndBundle(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvpol1().getDati().setWpolIbBs("");
        ws.getAreaBusiness().getLccvpol1().getDati().setWpolFlPoliIfp(Types.SPACE_CHAR);
    }

    public void initWpreAreaPrestiti() {
        ws.getAreaBusiness().setWpreElePrestitiMax(((short)0));
        for (int idx0 = 1; idx0 <= AreaBusinessIvvs0211.WPRE_TAB_PRESTITI_MAXOCCURS; idx0++) {
            ws.getAreaBusiness().getWpreTabPrestiti(idx0).getLccvpre1().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWpreTabPrestiti(idx0).getLccvpre1().setIdPtf(0);
            ws.getAreaBusiness().getWpreTabPrestiti(idx0).getLccvpre1().getDati().setWpreIdPrest(0);
            ws.getAreaBusiness().getWpreTabPrestiti(idx0).getLccvpre1().getDati().setWpreIdOgg(0);
            ws.getAreaBusiness().getWpreTabPrestiti(idx0).getLccvpre1().getDati().setWpreTpOgg("");
            ws.getAreaBusiness().getWpreTabPrestiti(idx0).getLccvpre1().getDati().setWpreIdMoviCrz(0);
            ws.getAreaBusiness().getWpreTabPrestiti(idx0).getLccvpre1().getDati().getWpreIdMoviChiu().setWpreIdMoviChiu(0);
            ws.getAreaBusiness().getWpreTabPrestiti(idx0).getLccvpre1().getDati().setWpreDtIniEff(0);
            ws.getAreaBusiness().getWpreTabPrestiti(idx0).getLccvpre1().getDati().setWpreDtEndEff(0);
            ws.getAreaBusiness().getWpreTabPrestiti(idx0).getLccvpre1().getDati().setWpreCodCompAnia(0);
            ws.getAreaBusiness().getWpreTabPrestiti(idx0).getLccvpre1().getDati().getWpreDtConcsPrest().setWpreDtConcsPrest(0);
            ws.getAreaBusiness().getWpreTabPrestiti(idx0).getLccvpre1().getDati().getWpreDtDecorPrest().setWpreDtDecorPrest(0);
            ws.getAreaBusiness().getWpreTabPrestiti(idx0).getLccvpre1().getDati().getWpreImpPrest().setWpreImpPrest(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWpreTabPrestiti(idx0).getLccvpre1().getDati().getWpreIntrPrest().setWpreIntrPrest(new AfDecimal(0, 6, 3));
            ws.getAreaBusiness().getWpreTabPrestiti(idx0).getLccvpre1().getDati().setWpreTpPrest("");
            ws.getAreaBusiness().getWpreTabPrestiti(idx0).getLccvpre1().getDati().getWpreFrazPagIntr().setWpreFrazPagIntr(0);
            ws.getAreaBusiness().getWpreTabPrestiti(idx0).getLccvpre1().getDati().getWpreDtRimb().setWpreDtRimb(0);
            ws.getAreaBusiness().getWpreTabPrestiti(idx0).getLccvpre1().getDati().getWpreImpRimb().setWpreImpRimb(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWpreTabPrestiti(idx0).getLccvpre1().getDati().setWpreCodDvs("");
            ws.getAreaBusiness().getWpreTabPrestiti(idx0).getLccvpre1().getDati().setWpreDtRichPrest(0);
            ws.getAreaBusiness().getWpreTabPrestiti(idx0).getLccvpre1().getDati().setWpreModIntrPrest("");
            ws.getAreaBusiness().getWpreTabPrestiti(idx0).getLccvpre1().getDati().getWpreSpePrest().setWpreSpePrest(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWpreTabPrestiti(idx0).getLccvpre1().getDati().getWpreImpPrestLiqto().setWpreImpPrestLiqto(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWpreTabPrestiti(idx0).getLccvpre1().getDati().getWpreSdoIntr().setWpreSdoIntr(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWpreTabPrestiti(idx0).getLccvpre1().getDati().getWpreRimbEff().setWpreRimbEff(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWpreTabPrestiti(idx0).getLccvpre1().getDati().getWprePrestResEff().setWprePrestResEff(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWpreTabPrestiti(idx0).getLccvpre1().getDati().setWpreDsRiga(0);
            ws.getAreaBusiness().getWpreTabPrestiti(idx0).getLccvpre1().getDati().setWpreDsOperSql(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWpreTabPrestiti(idx0).getLccvpre1().getDati().setWpreDsVer(0);
            ws.getAreaBusiness().getWpreTabPrestiti(idx0).getLccvpre1().getDati().setWpreDsTsIniCptz(0);
            ws.getAreaBusiness().getWpreTabPrestiti(idx0).getLccvpre1().getDati().setWpreDsTsEndCptz(0);
            ws.getAreaBusiness().getWpreTabPrestiti(idx0).getLccvpre1().getDati().setWpreDsUtente("");
            ws.getAreaBusiness().getWpreTabPrestiti(idx0).getLccvpre1().getDati().setWpreDsStatoElab(Types.SPACE_CHAR);
        }
    }

    public void initWpvtAreaProv() {
        ws.getAreaBusiness().setWpvtEleProvMax(((short)0));
        for (int idx0 = 1; idx0 <= AreaBusinessIvvs0211.WPVT_TAB_PROV_TR_MAXOCCURS; idx0++) {
            ws.getAreaBusiness().getWpvtTabProvTr(idx0).getLccvpvt1().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWpvtTabProvTr(idx0).getLccvpvt1().setIdPtf(0);
            ws.getAreaBusiness().getWpvtTabProvTr(idx0).getLccvpvt1().getDati().setWpvtIdProvDiGar(0);
            ws.getAreaBusiness().getWpvtTabProvTr(idx0).getLccvpvt1().getDati().getWpvtIdGar().setWpvtIdGar(0);
            ws.getAreaBusiness().getWpvtTabProvTr(idx0).getLccvpvt1().getDati().setWpvtIdMoviCrz(0);
            ws.getAreaBusiness().getWpvtTabProvTr(idx0).getLccvpvt1().getDati().getWpvtIdMoviChiu().setWpvtIdMoviChiu(0);
            ws.getAreaBusiness().getWpvtTabProvTr(idx0).getLccvpvt1().getDati().setWpvtDtIniEff(0);
            ws.getAreaBusiness().getWpvtTabProvTr(idx0).getLccvpvt1().getDati().setWpvtDtEndEff(0);
            ws.getAreaBusiness().getWpvtTabProvTr(idx0).getLccvpvt1().getDati().setWpvtCodCompAnia(0);
            ws.getAreaBusiness().getWpvtTabProvTr(idx0).getLccvpvt1().getDati().setWpvtTpProv("");
            ws.getAreaBusiness().getWpvtTabProvTr(idx0).getLccvpvt1().getDati().getWpvtProv1aaAcq().setWpvtProv1aaAcq(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWpvtTabProvTr(idx0).getLccvpvt1().getDati().getWpvtProv2aaAcq().setWpvtProv2aaAcq(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWpvtTabProvTr(idx0).getLccvpvt1().getDati().getWpvtProvRicor().setWpvtProvRicor(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWpvtTabProvTr(idx0).getLccvpvt1().getDati().getWpvtProvInc().setWpvtProvInc(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWpvtTabProvTr(idx0).getLccvpvt1().getDati().getWpvtAlqProvAcq().setWpvtAlqProvAcq(new AfDecimal(0, 6, 3));
            ws.getAreaBusiness().getWpvtTabProvTr(idx0).getLccvpvt1().getDati().getWpvtAlqProvInc().setWpvtAlqProvInc(new AfDecimal(0, 6, 3));
            ws.getAreaBusiness().getWpvtTabProvTr(idx0).getLccvpvt1().getDati().getWpvtAlqProvRicor().setWpvtAlqProvRicor(new AfDecimal(0, 6, 3));
            ws.getAreaBusiness().getWpvtTabProvTr(idx0).getLccvpvt1().getDati().setWpvtFlStorProvAcq(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWpvtTabProvTr(idx0).getLccvpvt1().getDati().setWpvtFlRecProvStorn(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWpvtTabProvTr(idx0).getLccvpvt1().getDati().setWpvtFlProvForz(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWpvtTabProvTr(idx0).getLccvpvt1().getDati().setWpvtTpCalcProv(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWpvtTabProvTr(idx0).getLccvpvt1().getDati().setWpvtDsRiga(0);
            ws.getAreaBusiness().getWpvtTabProvTr(idx0).getLccvpvt1().getDati().setWpvtDsOperSql(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWpvtTabProvTr(idx0).getLccvpvt1().getDati().setWpvtDsVer(0);
            ws.getAreaBusiness().getWpvtTabProvTr(idx0).getLccvpvt1().getDati().setWpvtDsTsIniCptz(0);
            ws.getAreaBusiness().getWpvtTabProvTr(idx0).getLccvpvt1().getDati().setWpvtDsTsEndCptz(0);
            ws.getAreaBusiness().getWpvtTabProvTr(idx0).getLccvpvt1().getDati().setWpvtDsUtente("");
            ws.getAreaBusiness().getWpvtTabProvTr(idx0).getLccvpvt1().getDati().setWpvtDsStatoElab(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWpvtTabProvTr(idx0).getLccvpvt1().getDati().getWpvtRemunAss().setWpvtRemunAss(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWpvtTabProvTr(idx0).getLccvpvt1().getDati().getWpvtCommisInter().setWpvtCommisInter(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWpvtTabProvTr(idx0).getLccvpvt1().getDati().getWpvtAlqRemunAss().setWpvtAlqRemunAss(new AfDecimal(0, 6, 3));
            ws.getAreaBusiness().getWpvtTabProvTr(idx0).getLccvpvt1().getDati().getWpvtAlqCommisInter().setWpvtAlqCommisInter(new AfDecimal(0, 6, 3));
        }
    }

    public void initWqueAreaQuest() {
        ws.getAreaBusiness().setWqueEleQuestMax(((short)0));
        for (int idx0 = 1; idx0 <= AreaBusinessIvvs0211.WQUE_TAB_QUEST_MAXOCCURS; idx0++) {
            ws.getAreaBusiness().getWqueTabQuest(idx0).getLccvque1().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWqueTabQuest(idx0).getLccvque1().setIdPtf(0);
            ws.getAreaBusiness().getWqueTabQuest(idx0).getLccvque1().getDati().setWqueIdQuest(0);
            ws.getAreaBusiness().getWqueTabQuest(idx0).getLccvque1().getDati().setWqueIdRappAna(0);
            ws.getAreaBusiness().getWqueTabQuest(idx0).getLccvque1().getDati().setWqueIdMoviCrz(0);
            ws.getAreaBusiness().getWqueTabQuest(idx0).getLccvque1().getDati().getWqueIdMoviChiu().setWqueIdMoviChiu(0);
            ws.getAreaBusiness().getWqueTabQuest(idx0).getLccvque1().getDati().setWqueDtIniEff(0);
            ws.getAreaBusiness().getWqueTabQuest(idx0).getLccvque1().getDati().setWqueDtEndEff(0);
            ws.getAreaBusiness().getWqueTabQuest(idx0).getLccvque1().getDati().setWqueCodCompAnia(0);
            ws.getAreaBusiness().getWqueTabQuest(idx0).getLccvque1().getDati().setWqueCodQuest("");
            ws.getAreaBusiness().getWqueTabQuest(idx0).getLccvque1().getDati().setWqueTpQuest("");
            ws.getAreaBusiness().getWqueTabQuest(idx0).getLccvque1().getDati().setWqueFlVstMed(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWqueTabQuest(idx0).getLccvque1().getDati().setWqueFlStatBuonSal(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWqueTabQuest(idx0).getLccvque1().getDati().setWqueDsRiga(0);
            ws.getAreaBusiness().getWqueTabQuest(idx0).getLccvque1().getDati().setWqueDsOperSql(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWqueTabQuest(idx0).getLccvque1().getDati().setWqueDsVer(0);
            ws.getAreaBusiness().getWqueTabQuest(idx0).getLccvque1().getDati().setWqueDsTsIniCptz(0);
            ws.getAreaBusiness().getWqueTabQuest(idx0).getLccvque1().getDati().setWqueDsTsEndCptz(0);
            ws.getAreaBusiness().getWqueTabQuest(idx0).getLccvque1().getDati().setWqueDsUtente("");
            ws.getAreaBusiness().getWqueTabQuest(idx0).getLccvque1().getDati().setWqueDsStatoElab(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWqueTabQuest(idx0).getLccvque1().getDati().setWqueTpAdegz("");
        }
    }

    public void initWricAreaRich() {
        ws.getAreaBusiness().setWricEleRichMax(((short)0));
        ws.getAreaBusiness().getLccvric1().getStatus().setStatus(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvric1().setIdPtf(0);
        ws.getAreaBusiness().getLccvric1().getDati().setWricIdRich(0);
        ws.getAreaBusiness().getLccvric1().getDati().setWricCodCompAnia(0);
        ws.getAreaBusiness().getLccvric1().getDati().setWricTpRich("");
        ws.getAreaBusiness().getLccvric1().getDati().setWricCodMacrofunct("");
        ws.getAreaBusiness().getLccvric1().getDati().setWricTpMovi(0);
        ws.getAreaBusiness().getLccvric1().getDati().setWricIbRich("");
        ws.getAreaBusiness().getLccvric1().getDati().setWricDtEff(0);
        ws.getAreaBusiness().getLccvric1().getDati().setWricDtRgstrzRich(0);
        ws.getAreaBusiness().getLccvric1().getDati().setWricDtPervRich(0);
        ws.getAreaBusiness().getLccvric1().getDati().setWricDtEsecRich(0);
        ws.getAreaBusiness().getLccvric1().getDati().getWricTsEffEsecRich().setWricTsEffEsecRich(0);
        ws.getAreaBusiness().getLccvric1().getDati().getWricIdOgg().setWricIdOgg(0);
        ws.getAreaBusiness().getLccvric1().getDati().setWricTpOgg("");
        ws.getAreaBusiness().getLccvric1().getDati().setWricIbPoli("");
        ws.getAreaBusiness().getLccvric1().getDati().setWricIbAdes("");
        ws.getAreaBusiness().getLccvric1().getDati().setWricIbGar("");
        ws.getAreaBusiness().getLccvric1().getDati().setWricIbTrchDiGar("");
        ws.getAreaBusiness().getLccvric1().getDati().getWricIdBatch().setWricIdBatch(0);
        ws.getAreaBusiness().getLccvric1().getDati().getWricIdJob().setWricIdJob(0);
        ws.getAreaBusiness().getLccvric1().getDati().setWricFlSimulazione(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvric1().getDati().setWricKeyOrdinamento("");
        ws.getAreaBusiness().getLccvric1().getDati().getWricIdRichCollg().setWricIdRichCollg(0);
        ws.getAreaBusiness().getLccvric1().getDati().setWricTpRamoBila("");
        ws.getAreaBusiness().getLccvric1().getDati().setWricTpFrmAssva("");
        ws.getAreaBusiness().getLccvric1().getDati().setWricTpCalcRis("");
        ws.getAreaBusiness().getLccvric1().getDati().setWricDsOperSql(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvric1().getDati().setWricDsVer(0);
        ws.getAreaBusiness().getLccvric1().getDati().setWricDsTsCptz(0);
        ws.getAreaBusiness().getLccvric1().getDati().setWricDsUtente("");
        ws.getAreaBusiness().getLccvric1().getDati().setWricDsStatoElab(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvric1().getDati().setWricRamoBila("");
    }

    public void initWrdfAreaRichDisinvFnd() {
        ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().setWrdfEleRicInvMax(((short)0));
        for (int idx0 = 1; idx0 <= WrdfTabella.TAB_RIC_DISINV_MAXOCCURS; idx0++) {
            ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setStatus(idx0, Types.SPACE_CHAR);
            ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setIdPtf(idx0, 0);
            ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setIdRichDisFnd(idx0, 0);
            ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setIdMoviFinrio(idx0, 0);
            ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setIdMoviCrz(idx0, 0);
            ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setIdMoviChiu(idx0, 0);
            ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setDtIniEff(idx0, 0);
            ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setDtEndEff(idx0, 0);
            ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setCodCompAnia(idx0, 0);
            ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setCodFnd(idx0, "");
            ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setNumQuo(idx0, new AfDecimal(0, 12, 5));
            ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setPc(idx0, new AfDecimal(0, 6, 3));
            ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setImpMovto(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setDtDis(idx0, 0);
            ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setCodTari(idx0, "");
            ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setTpStat(idx0, "");
            ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setTpModDis(idx0, "");
            ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setCodDiv(idx0, "");
            ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setDtCambioVlt(idx0, 0);
            ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setTpFnd(idx0, Types.SPACE_CHAR);
            ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setDsRiga(idx0, 0);
            ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setDsOperSql(idx0, Types.SPACE_CHAR);
            ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setDsVer(idx0, 0);
            ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setDsTsIniCptz(idx0, 0);
            ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setDsTsEndCptz(idx0, 0);
            ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setDsUtente(idx0, "");
            ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setDsStatoElab(idx0, Types.SPACE_CHAR);
            ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setDtDisCalc(idx0, 0);
            ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setFlCalcDis(idx0, Types.SPACE_CHAR);
            ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setCommisGest(idx0, new AfDecimal(0, 18, 7));
            ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setNumQuoCdgFnz(idx0, new AfDecimal(0, 12, 5));
            ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setNumQuoCdgtotFnz(idx0, new AfDecimal(0, 12, 5));
            ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setCosRunAssvaIdc(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setFlSwmBp2s(idx0, Types.SPACE_CHAR);
        }
    }

    public void initWrifAreaRichInvFnd() {
        ws.getAreaBusiness().getWrifAreaRichInvFnd().setWrifEleRicInvMax(((short)0));
        for (int idx0 = 1; idx0 <= WrifTabella.TAB_RIC_INV_MAXOCCURS; idx0++) {
            ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setStatus(idx0, Types.SPACE_CHAR);
            ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setIdPtf(idx0, 0);
            ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setIdRichInvstFnd(idx0, 0);
            ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setIdMoviFinrio(idx0, 0);
            ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setIdMoviCrz(idx0, 0);
            ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setIdMoviChiu(idx0, 0);
            ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setDtIniEff(idx0, 0);
            ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setDtEndEff(idx0, 0);
            ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setCodCompAnia(idx0, 0);
            ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setCodFnd(idx0, "");
            ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setNumQuo(idx0, new AfDecimal(0, 12, 5));
            ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setPc(idx0, new AfDecimal(0, 6, 3));
            ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setImpMovto(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setDtInvst(idx0, 0);
            ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setCodTari(idx0, "");
            ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setTpStat(idx0, "");
            ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setTpModInvst(idx0, "");
            ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setCodDiv(idx0, "");
            ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setDtCambioVlt(idx0, 0);
            ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setTpFnd(idx0, Types.SPACE_CHAR);
            ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setDsRiga(idx0, 0);
            ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setDsOperSql(idx0, Types.SPACE_CHAR);
            ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setDsVer(idx0, 0);
            ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setDsTsIniCptz(idx0, 0);
            ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setDsTsEndCptz(idx0, 0);
            ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setDsUtente(idx0, "");
            ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setDsStatoElab(idx0, Types.SPACE_CHAR);
            ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setDtInvstCalc(idx0, 0);
            ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setFlCalcInvto(idx0, Types.SPACE_CHAR);
            ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setImpGapEvent(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setFlSwmBp2s(idx0, Types.SPACE_CHAR);
        }
    }

    public void initWranAreaRappAnag() {
        ws.getAreaBusiness().setWranEleRappAnagMax(((short)0));
        for (int idx0 = 1; idx0 <= AreaBusinessIvvs0211.WRAN_TAB_RAPP_ANAG_MAXOCCURS; idx0++) {
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().setIdPtf(0);
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().setWranIdRappAna(0);
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().getWranIdRappAnaCollg().setWranIdRappAnaCollg(0);
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().setWranIdOgg(0);
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().setWranTpOgg("");
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().setWranIdMoviCrz(0);
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().getWranIdMoviChiu().setWranIdMoviChiu(0);
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().setWranDtIniEff(0);
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().setWranDtEndEff(0);
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().setWranCodCompAnia(0);
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().setWranCodSogg("");
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().setWranTpRappAna("");
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().setWranTpPers(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().setWranSex(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().getWranDtNasc().setWranDtNasc(0);
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().setWranFlEstas(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().setWranIndir1("");
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().setWranIndir2("");
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().setWranIndir3("");
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().setWranTpUtlzIndir1("");
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().setWranTpUtlzIndir2("");
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().setWranTpUtlzIndir3("");
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().setWranEstrCntCorrAccr("");
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().setWranEstrCntCorrAdd("");
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().setWranEstrDocto("");
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().getWranPcNelRapp().setWranPcNelRapp(new AfDecimal(0, 6, 3));
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().setWranTpMezPagAdd("");
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().setWranTpMezPagAccr("");
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().setWranCodMatr("");
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().setWranTpAdegz("");
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().setWranFlTstRsh(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().setWranCodAz("");
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().setWranIndPrinc("");
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().getWranDtDeliberaCda().setWranDtDeliberaCda(0);
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().setWranDlgAlRisc(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().setWranLegaleRapprPrinc(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().setWranTpLegaleRappr("");
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().setWranTpIndPrinc("");
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().setWranTpStatRid("");
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().setWranNomeIntRid("");
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().setWranCognIntRid("");
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().setWranCognIntTratt("");
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().setWranNomeIntTratt("");
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().setWranCfIntRid("");
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().setWranFlCoincDipCntr(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().setWranFlCoincIntCntr(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().getWranDtDeces().setWranDtDeces(0);
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().setWranFlFumatore(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().setWranDsRiga(0);
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().setWranDsOperSql(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().setWranDsVer(0);
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().setWranDsTsIniCptz(0);
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().setWranDsTsEndCptz(0);
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().setWranDsUtente("");
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().setWranDsStatoElab(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().setWranFlLavDip(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().setWranTpVarzPagat("");
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().setWranCodRid("");
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().setWranTpCausRid("");
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().setWranIndMassaCorp("");
            ws.getAreaBusiness().getWranTabRappAnag(idx0).getLccvran1().getDati().setWranCatRshProf("");
        }
    }

    public void initWe15AreaEstRappAnag() {
        ws.getAreaBusiness().setWe15EleEstRappAnagMax(((short)0));
        for (int idx0 = 1; idx0 <= AreaBusinessIvvs0211.WE15_TAB_E15_MAXOCCURS; idx0++) {
            ws.getAreaBusiness().getWe15TabE15(idx0).getLccve151().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWe15TabE15(idx0).getLccve151().setIdPtf(0);
            ws.getAreaBusiness().getWe15TabE15(idx0).getLccve151().getDati().setWe15IdEstRappAna(0);
            ws.getAreaBusiness().getWe15TabE15(idx0).getLccve151().getDati().setWe15IdRappAna(0);
            ws.getAreaBusiness().getWe15TabE15(idx0).getLccve151().getDati().getWe15IdRappAnaCollg().setWe15IdRappAnaCollg(0);
            ws.getAreaBusiness().getWe15TabE15(idx0).getLccve151().getDati().setWe15IdOgg(0);
            ws.getAreaBusiness().getWe15TabE15(idx0).getLccve151().getDati().setWe15TpOgg("");
            ws.getAreaBusiness().getWe15TabE15(idx0).getLccve151().getDati().setWe15IdMoviCrz(0);
            ws.getAreaBusiness().getWe15TabE15(idx0).getLccve151().getDati().getWe15IdMoviChiu().setWe15IdMoviChiu(0);
            ws.getAreaBusiness().getWe15TabE15(idx0).getLccve151().getDati().setWe15DtIniEff(0);
            ws.getAreaBusiness().getWe15TabE15(idx0).getLccve151().getDati().setWe15DtEndEff(0);
            ws.getAreaBusiness().getWe15TabE15(idx0).getLccve151().getDati().setWe15CodCompAnia(0);
            ws.getAreaBusiness().getWe15TabE15(idx0).getLccve151().getDati().setWe15CodSogg("");
            ws.getAreaBusiness().getWe15TabE15(idx0).getLccve151().getDati().setWe15TpRappAna("");
            ws.getAreaBusiness().getWe15TabE15(idx0).getLccve151().getDati().setWe15DsRiga(0);
            ws.getAreaBusiness().getWe15TabE15(idx0).getLccve151().getDati().setWe15DsOperSql(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWe15TabE15(idx0).getLccve151().getDati().setWe15DsVer(0);
            ws.getAreaBusiness().getWe15TabE15(idx0).getLccve151().getDati().setWe15DsTsIniCptz(0);
            ws.getAreaBusiness().getWe15TabE15(idx0).getLccve151().getDati().setWe15DsTsEndCptz(0);
            ws.getAreaBusiness().getWe15TabE15(idx0).getLccve151().getDati().setWe15DsUtente("");
            ws.getAreaBusiness().getWe15TabE15(idx0).getLccve151().getDati().setWe15DsStatoElab(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWe15TabE15(idx0).getLccve151().getDati().getWe15IdSegmentazCli().setWe15IdSegmentazCli(0);
            ws.getAreaBusiness().getWe15TabE15(idx0).getLccve151().getDati().setWe15FlCoincTitEff(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWe15TabE15(idx0).getLccve151().getDati().setWe15FlPersEspPol(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWe15TabE15(idx0).getLccve151().getDati().setWe15DescPersEspPol("");
            ws.getAreaBusiness().getWe15TabE15(idx0).getLccve151().getDati().setWe15TpLegCntr("");
            ws.getAreaBusiness().getWe15TabE15(idx0).getLccve151().getDati().setWe15DescLegCntr("");
            ws.getAreaBusiness().getWe15TabE15(idx0).getLccve151().getDati().setWe15TpLegPercBnficr("");
            ws.getAreaBusiness().getWe15TabE15(idx0).getLccve151().getDati().setWe15DLegPercBnficr("");
            ws.getAreaBusiness().getWe15TabE15(idx0).getLccve151().getDati().setWe15TpCntCorr("");
            ws.getAreaBusiness().getWe15TabE15(idx0).getLccve151().getDati().setWe15TpCoincPicPac("");
        }
    }

    public void initWrreAreaRappRete() {
        ws.getAreaBusiness().setWrreEleRappReteMax(((short)0));
        for (int idx0 = 1; idx0 <= AreaBusinessIvvs0211.WRRE_TAB_RAPP_RETE_MAXOCCURS; idx0++) {
            ws.getAreaBusiness().getWrreTabRappRete(idx0).getLccvrre1().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWrreTabRappRete(idx0).getLccvrre1().setIdPtf(0);
            ws.getAreaBusiness().getWrreTabRappRete(idx0).getLccvrre1().getDati().setWrreIdRappRete(0);
            ws.getAreaBusiness().getWrreTabRappRete(idx0).getLccvrre1().getDati().getWrreIdOgg().setWrreIdOgg(0);
            ws.getAreaBusiness().getWrreTabRappRete(idx0).getLccvrre1().getDati().setWrreTpOgg("");
            ws.getAreaBusiness().getWrreTabRappRete(idx0).getLccvrre1().getDati().setWrreIdMoviCrz(0);
            ws.getAreaBusiness().getWrreTabRappRete(idx0).getLccvrre1().getDati().getWrreIdMoviChiu().setWrreIdMoviChiu(0);
            ws.getAreaBusiness().getWrreTabRappRete(idx0).getLccvrre1().getDati().setWrreDtIniEff(0);
            ws.getAreaBusiness().getWrreTabRappRete(idx0).getLccvrre1().getDati().setWrreDtEndEff(0);
            ws.getAreaBusiness().getWrreTabRappRete(idx0).getLccvrre1().getDati().setWrreCodCompAnia(0);
            ws.getAreaBusiness().getWrreTabRappRete(idx0).getLccvrre1().getDati().setWrreTpRete("");
            ws.getAreaBusiness().getWrreTabRappRete(idx0).getLccvrre1().getDati().getWrreTpAcqsCntrt().setWrreTpAcqsCntrt(0);
            ws.getAreaBusiness().getWrreTabRappRete(idx0).getLccvrre1().getDati().getWrreCodAcqsCntrt().setWrreCodAcqsCntrt(0);
            ws.getAreaBusiness().getWrreTabRappRete(idx0).getLccvrre1().getDati().getWrreCodPntReteIni().setWrreCodPntReteIni(0);
            ws.getAreaBusiness().getWrreTabRappRete(idx0).getLccvrre1().getDati().getWrreCodPntReteEnd().setWrreCodPntReteEnd(0);
            ws.getAreaBusiness().getWrreTabRappRete(idx0).getLccvrre1().getDati().setWrreFlPntRete1rio(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWrreTabRappRete(idx0).getLccvrre1().getDati().getWrreCodCan().setWrreCodCan(0);
            ws.getAreaBusiness().getWrreTabRappRete(idx0).getLccvrre1().getDati().setWrreDsRiga(0);
            ws.getAreaBusiness().getWrreTabRappRete(idx0).getLccvrre1().getDati().setWrreDsOperSql(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWrreTabRappRete(idx0).getLccvrre1().getDati().setWrreDsVer(0);
            ws.getAreaBusiness().getWrreTabRappRete(idx0).getLccvrre1().getDati().setWrreDsTsIniCptz(0);
            ws.getAreaBusiness().getWrreTabRappRete(idx0).getLccvrre1().getDati().setWrreDsTsEndCptz(0);
            ws.getAreaBusiness().getWrreTabRappRete(idx0).getLccvrre1().getDati().setWrreDsUtente("");
            ws.getAreaBusiness().getWrreTabRappRete(idx0).getLccvrre1().getDati().setWrreDsStatoElab(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWrreTabRappRete(idx0).getLccvrre1().getDati().getWrreCodPntReteIniC().setWrreCodPntReteIniC(0);
            ws.getAreaBusiness().getWrreTabRappRete(idx0).getLccvrre1().getDati().setWrreMatrOprt("");
        }
    }

    public void initWspgAreaSoprapGar() {
        ws.getAreaBusiness().setWspgEleSoprapGarMax(((short)0));
        for (int idx0 = 1; idx0 <= AreaBusinessIvvs0211.WSPG_TAB_SPG_MAXOCCURS; idx0++) {
            ws.getAreaBusiness().getWspgTabSpg(idx0).getLccvspg1().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWspgTabSpg(idx0).getLccvspg1().setIdPtf(0);
            ws.getAreaBusiness().getWspgTabSpg(idx0).getLccvspg1().getDati().setWspgIdSoprDiGar(0);
            ws.getAreaBusiness().getWspgTabSpg(idx0).getLccvspg1().getDati().getWspgIdGar().setWspgIdGar(0);
            ws.getAreaBusiness().getWspgTabSpg(idx0).getLccvspg1().getDati().setWspgIdMoviCrz(0);
            ws.getAreaBusiness().getWspgTabSpg(idx0).getLccvspg1().getDati().getWspgIdMoviChiu().setWspgIdMoviChiu(0);
            ws.getAreaBusiness().getWspgTabSpg(idx0).getLccvspg1().getDati().setWspgDtIniEff(0);
            ws.getAreaBusiness().getWspgTabSpg(idx0).getLccvspg1().getDati().setWspgDtEndEff(0);
            ws.getAreaBusiness().getWspgTabSpg(idx0).getLccvspg1().getDati().setWspgCodCompAnia(0);
            ws.getAreaBusiness().getWspgTabSpg(idx0).getLccvspg1().getDati().setWspgCodSopr("");
            ws.getAreaBusiness().getWspgTabSpg(idx0).getLccvspg1().getDati().setWspgTpD("");
            ws.getAreaBusiness().getWspgTabSpg(idx0).getLccvspg1().getDati().getWspgValPc().setWspgValPc(new AfDecimal(0, 14, 9));
            ws.getAreaBusiness().getWspgTabSpg(idx0).getLccvspg1().getDati().getWspgValImp().setWspgValImp(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWspgTabSpg(idx0).getLccvspg1().getDati().getWspgPcSopram().setWspgPcSopram(new AfDecimal(0, 14, 9));
            ws.getAreaBusiness().getWspgTabSpg(idx0).getLccvspg1().getDati().setWspgFlEsclSopr(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWspgTabSpg(idx0).getLccvspg1().getDati().setWspgDescEscl("");
            ws.getAreaBusiness().getWspgTabSpg(idx0).getLccvspg1().getDati().setWspgDsRiga(0);
            ws.getAreaBusiness().getWspgTabSpg(idx0).getLccvspg1().getDati().setWspgDsOperSql(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWspgTabSpg(idx0).getLccvspg1().getDati().setWspgDsVer(0);
            ws.getAreaBusiness().getWspgTabSpg(idx0).getLccvspg1().getDati().setWspgDsTsIniCptz(0);
            ws.getAreaBusiness().getWspgTabSpg(idx0).getLccvspg1().getDati().setWspgDsTsEndCptz(0);
            ws.getAreaBusiness().getWspgTabSpg(idx0).getLccvspg1().getDati().setWspgDsUtente("");
            ws.getAreaBusiness().getWspgTabSpg(idx0).getLccvspg1().getDati().setWspgDsStatoElab(Types.SPACE_CHAR);
        }
    }

    public void initWsdiAreaStraInv() {
        ws.getAreaBusiness().setWsdiEleStraInvMax(((short)0));
        for (int idx0 = 1; idx0 <= AreaBusinessIvvs0211.WSDI_TAB_STRA_INV_MAXOCCURS; idx0++) {
            ws.getAreaBusiness().getWsdiTabStraInv(idx0).getLccvsdi1().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWsdiTabStraInv(idx0).getLccvsdi1().setIdPtf(0);
            ws.getAreaBusiness().getWsdiTabStraInv(idx0).getLccvsdi1().getDati().setWsdiIdStraDiInvst(0);
            ws.getAreaBusiness().getWsdiTabStraInv(idx0).getLccvsdi1().getDati().setWsdiIdOgg(0);
            ws.getAreaBusiness().getWsdiTabStraInv(idx0).getLccvsdi1().getDati().setWsdiTpOgg("");
            ws.getAreaBusiness().getWsdiTabStraInv(idx0).getLccvsdi1().getDati().setWsdiIdMoviCrz(0);
            ws.getAreaBusiness().getWsdiTabStraInv(idx0).getLccvsdi1().getDati().getWsdiIdMoviChiu().setWsdiIdMoviChiu(0);
            ws.getAreaBusiness().getWsdiTabStraInv(idx0).getLccvsdi1().getDati().setWsdiDtIniEff(0);
            ws.getAreaBusiness().getWsdiTabStraInv(idx0).getLccvsdi1().getDati().setWsdiDtEndEff(0);
            ws.getAreaBusiness().getWsdiTabStraInv(idx0).getLccvsdi1().getDati().setWsdiCodCompAnia(0);
            ws.getAreaBusiness().getWsdiTabStraInv(idx0).getLccvsdi1().getDati().setWsdiCodStra("");
            ws.getAreaBusiness().getWsdiTabStraInv(idx0).getLccvsdi1().getDati().getWsdiModGest().setWsdiModGest(((short)0));
            ws.getAreaBusiness().getWsdiTabStraInv(idx0).getLccvsdi1().getDati().setWsdiVarRifto("");
            ws.getAreaBusiness().getWsdiTabStraInv(idx0).getLccvsdi1().getDati().setWsdiValVarRifto("");
            ws.getAreaBusiness().getWsdiTabStraInv(idx0).getLccvsdi1().getDati().getWsdiDtFis().setWsdiDtFis(((short)0));
            ws.getAreaBusiness().getWsdiTabStraInv(idx0).getLccvsdi1().getDati().getWsdiFrqValut().setWsdiFrqValut(0);
            ws.getAreaBusiness().getWsdiTabStraInv(idx0).getLccvsdi1().getDati().setWsdiFlIniEndPer(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWsdiTabStraInv(idx0).getLccvsdi1().getDati().setWsdiDsRiga(0);
            ws.getAreaBusiness().getWsdiTabStraInv(idx0).getLccvsdi1().getDati().setWsdiDsOperSql(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWsdiTabStraInv(idx0).getLccvsdi1().getDati().setWsdiDsVer(0);
            ws.getAreaBusiness().getWsdiTabStraInv(idx0).getLccvsdi1().getDati().setWsdiDsTsIniCptz(0);
            ws.getAreaBusiness().getWsdiTabStraInv(idx0).getLccvsdi1().getDati().setWsdiDsTsEndCptz(0);
            ws.getAreaBusiness().getWsdiTabStraInv(idx0).getLccvsdi1().getDati().setWsdiDsUtente("");
            ws.getAreaBusiness().getWsdiTabStraInv(idx0).getLccvsdi1().getDati().setWsdiDsStatoElab(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWsdiTabStraInv(idx0).getLccvsdi1().getDati().setWsdiTpStra("");
            ws.getAreaBusiness().getWsdiTabStraInv(idx0).getLccvsdi1().getDati().setWsdiTpProvzaStra("");
            ws.getAreaBusiness().getWsdiTabStraInv(idx0).getLccvsdi1().getDati().getWsdiPcProtezione().setWsdiPcProtezione(new AfDecimal(0, 6, 3));
        }
    }

    public void initWtitAreaTitCont() {
        ws.getAreaBusiness().setWtitEleTitContMax(((short)0));
        for (int idx0 = 1; idx0 <= AreaBusinessIvvs0211.WTIT_TAB_TIT_CONT_MAXOCCURS; idx0++) {
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().setIdPtf(0);
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().setWtitIdTitCont(0);
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().setWtitIdOgg(0);
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().setWtitTpOgg("");
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().setWtitIbRich("");
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().setWtitIdMoviCrz(0);
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitIdMoviChiu().setWtitIdMoviChiu(0);
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().setWtitDtIniEff(0);
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().setWtitDtEndEff(0);
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().setWtitCodCompAnia(0);
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().setWtitTpTit("");
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitProgTit().setWtitProgTit(0);
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().setWtitTpPreTit("");
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().setWtitTpStatTit("");
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitDtIniCop().setWtitDtIniCop(0);
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitDtEndCop().setWtitDtEndCop(0);
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitImpPag().setWtitImpPag(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().setWtitFlSoll(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitFraz().setWtitFraz(0);
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitDtApplzMora().setWtitDtApplzMora(0);
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().setWtitFlMora(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitIdRappRete().setWtitIdRappRete(0);
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitIdRappAna().setWtitIdRappAna(0);
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().setWtitCodDvs("");
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitDtEmisTit().setWtitDtEmisTit(0);
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitDtEsiTit().setWtitDtEsiTit(0);
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotPreNet().setWtitTotPreNet(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotIntrFraz().setWtitTotIntrFraz(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotIntrMora().setWtitTotIntrMora(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotIntrPrest().setWtitTotIntrPrest(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotIntrRetdt().setWtitTotIntrRetdt(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotIntrRiat().setWtitTotIntrRiat(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotDir().setWtitTotDir(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotSpeMed().setWtitTotSpeMed(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotTax().setWtitTotTax(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotSoprSan().setWtitTotSoprSan(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotSoprTec().setWtitTotSoprTec(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotSoprSpo().setWtitTotSoprSpo(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotSoprProf().setWtitTotSoprProf(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotSoprAlt().setWtitTotSoprAlt(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotPreTot().setWtitTotPreTot(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotPrePpIas().setWtitTotPrePpIas(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotCarAcq().setWtitTotCarAcq(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotCarGest().setWtitTotCarGest(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotCarInc().setWtitTotCarInc(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotPreSoloRsh().setWtitTotPreSoloRsh(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotProvAcq1aa().setWtitTotProvAcq1aa(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotProvAcq2aa().setWtitTotProvAcq2aa(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotProvRicor().setWtitTotProvRicor(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotProvInc().setWtitTotProvInc(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotProvDaRec().setWtitTotProvDaRec(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitImpAz().setWtitImpAz(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitImpAder().setWtitImpAder(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitImpTfr().setWtitImpTfr(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitImpVolo().setWtitImpVolo(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotManfeeAntic().setWtitTotManfeeAntic(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotManfeeRicor().setWtitTotManfeeRicor(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotManfeeRec().setWtitTotManfeeRec(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().setWtitTpMezPagAdd("");
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().setWtitEstrCntCorrAdd("");
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitDtVlt().setWtitDtVlt(0);
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().setWtitFlForzDtVlt(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitDtCambioVlt().setWtitDtCambioVlt(0);
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotSpeAge().setWtitTotSpeAge(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotCarIas().setWtitTotCarIas(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitNumRatAccorpate().setWtitNumRatAccorpate(0);
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().setWtitDsRiga(0);
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().setWtitDsOperSql(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().setWtitDsVer(0);
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().setWtitDsTsIniCptz(0);
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().setWtitDsTsEndCptz(0);
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().setWtitDsUtente("");
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().setWtitDsStatoElab(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().setWtitFlTitDaReinvst(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitDtRichAddRid().setWtitDtRichAddRid(0);
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().setWtitTpEsiRid("");
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().setWtitCodIban("");
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitImpTrasfe().setWtitImpTrasfe(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitImpTfrStrc().setWtitImpTfrStrc(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitDtCertFisc().setWtitDtCertFisc(0);
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTpCausStor().setWtitTpCausStor(0);
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().setWtitTpCausDispStor("");
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().setWtitTpTitMigraz("");
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotAcqExp().setWtitTotAcqExp(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotRemunAss().setWtitTotRemunAss(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotCommisInter().setWtitTotCommisInter(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().setWtitTpCausRimb("");
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotCnbtAntirac().setWtitTotCnbtAntirac(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtitTabTitCont(idx0).getLccvtit1().getDati().setWtitFlIncAutogen(Types.SPACE_CHAR);
        }
    }

    public void initWtclAreaTitLiq() {
        ws.getAreaBusiness().setWtclEleTitLiqMax(((short)0));
        ws.getAreaBusiness().getLccvtcl1().getStatus().setStatus(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvtcl1().setIdPtf(0);
        ws.getAreaBusiness().getLccvtcl1().getDati().setIdTcontLiq(0);
        ws.getAreaBusiness().getLccvtcl1().getDati().setIdPercLiq(0);
        ws.getAreaBusiness().getLccvtcl1().getDati().setIdBnficrLiq(0);
        ws.getAreaBusiness().getLccvtcl1().getDati().setIdMoviCrz(0);
        ws.getAreaBusiness().getLccvtcl1().getDati().setIdMoviChiu(0);
        ws.getAreaBusiness().getLccvtcl1().getDati().setDtIniEff(0);
        ws.getAreaBusiness().getLccvtcl1().getDati().setDtEndEff(0);
        ws.getAreaBusiness().getLccvtcl1().getDati().setCodCompAnia(0);
        ws.getAreaBusiness().getLccvtcl1().getDati().setDtVlt(0);
        ws.getAreaBusiness().getLccvtcl1().getDati().setImpLrdLiqto(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvtcl1().getDati().setImpPrest(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvtcl1().getDati().setImpIntrPrest(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvtcl1().getDati().setImpRat(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvtcl1().getDati().setImpUti(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvtcl1().getDati().setImpRitTfr(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvtcl1().getDati().setImpRitAcc(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvtcl1().getDati().setImpRitVis(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvtcl1().getDati().setImpbTfr(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvtcl1().getDati().setImpbAcc(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvtcl1().getDati().setImpbVis(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvtcl1().getDati().setImpRimb(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvtcl1().getDati().setImpCortvo(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvtcl1().getDati().setImpbImpstPrvr(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvtcl1().getDati().setImpstPrvr(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvtcl1().getDati().setImpbImpst252(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvtcl1().getDati().setImpst252(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvtcl1().getDati().setImpIs(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvtcl1().getDati().setImpDirLiq(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvtcl1().getDati().setImpNetLiqto(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvtcl1().getDati().setImpEfflq(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvtcl1().getDati().setCodDvs("");
        ws.getAreaBusiness().getLccvtcl1().getDati().setTpMezPagAccr("");
        ws.getAreaBusiness().getLccvtcl1().getDati().setEstrCntCorrAccr("");
        ws.getAreaBusiness().getLccvtcl1().getDati().setDsRiga(0);
        ws.getAreaBusiness().getLccvtcl1().getDati().setDsOperSql(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvtcl1().getDati().setDsVer(0);
        ws.getAreaBusiness().getLccvtcl1().getDati().setDsTsIniCptz(0);
        ws.getAreaBusiness().getLccvtcl1().getDati().setDsTsEndCptz(0);
        ws.getAreaBusiness().getLccvtcl1().getDati().setDsUtente("");
        ws.getAreaBusiness().getLccvtcl1().getDati().setDsStatoElab(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvtcl1().getDati().setTpStatTit("");
        ws.getAreaBusiness().getLccvtcl1().getDati().setImpbVis1382011(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvtcl1().getDati().setImpstVis1382011(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvtcl1().getDati().setImpstSost1382011(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvtcl1().getDati().setImpIntrRitPag(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvtcl1().getDati().setImpbIs(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvtcl1().getDati().setImpbIs1382011(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvtcl1().getDati().setImpbVis662014(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvtcl1().getDati().setImpstVis662014(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvtcl1().getDati().setImpbIs662014(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvtcl1().getDati().setImpstSost662014(new AfDecimal(0, 15, 3));
    }

    public void initWtgaAreaTranche() {
        ws.getAreaBusiness().getWtgaAreaTranche().setWtgaEleTranMax(((short)0));
        for (int idx0 = 1; idx0 <= WtgaTab.TAB_TRAN_MAXOCCURS; idx0++) {
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setStatus(idx0, Types.SPACE_CHAR);
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setIdPtf(idx0, 0);
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setIdTrchDiGar(idx0, 0);
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setIdGar(idx0, 0);
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setIdAdes(idx0, 0);
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setIdPoli(idx0, 0);
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setIdMoviCrz(idx0, 0);
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setIdMoviChiu(idx0, 0);
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setDtIniEff(idx0, 0);
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setDtEndEff(idx0, 0);
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setCodCompAnia(idx0, 0);
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setDtDecor(idx0, 0);
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setDtScad(idx0, 0);
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setIbOgg(idx0, "");
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setTpRgmFisc(idx0, "");
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setDtEmis(idx0, 0);
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setTpTrch(idx0, "");
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setDurAa(idx0, 0);
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setDurMm(idx0, 0);
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setDurGg(idx0, 0);
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setPreCasoMor(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setPcIntrRiat(idx0, new AfDecimal(0, 6, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setImpBnsAntic(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setPreIniNet(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setPrePpIni(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setPrePpUlt(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setPreTariIni(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setPreTariUlt(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setPreInvrioIni(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setPreInvrioUlt(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setPreRivto(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setImpSoprProf(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setImpSoprSan(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setImpSoprSpo(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setImpSoprTec(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setImpAltSopr(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setPreStab(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setDtEffStab(idx0, 0);
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setTsRivalFis(idx0, new AfDecimal(0, 14, 9));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setTsRivalIndiciz(idx0, new AfDecimal(0, 14, 9));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setOldTsTec(idx0, new AfDecimal(0, 14, 9));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setRatLrd(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setPreLrd(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setPrstzIni(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setPrstzUlt(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setCptInOpzRivto(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setPrstzIniStab(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setCptRshMor(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setPrstzRidIni(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setFlCarCont(idx0, Types.SPACE_CHAR);
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setBnsGiaLiqto(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setImpBns(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setCodDvs(idx0, "");
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setPrstzIniNewfis(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setImpScon(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setAlqScon(idx0, new AfDecimal(0, 6, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setImpCarAcq(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setImpCarInc(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setImpCarGest(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setEtaAa1oAssto(idx0, ((short)0));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setEtaMm1oAssto(idx0, ((short)0));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setEtaAa2oAssto(idx0, ((short)0));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setEtaMm2oAssto(idx0, ((short)0));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setEtaAa3oAssto(idx0, ((short)0));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setEtaMm3oAssto(idx0, ((short)0));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setRendtoLrd(idx0, new AfDecimal(0, 14, 9));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setPcRetr(idx0, new AfDecimal(0, 6, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setRendtoRetr(idx0, new AfDecimal(0, 14, 9));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setMinGarto(idx0, new AfDecimal(0, 14, 9));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setMinTrnut(idx0, new AfDecimal(0, 14, 9));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setPreAttDiTrch(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setMatuEnd2000(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setAbbTotIni(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setAbbTotUlt(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setAbbAnnuUlt(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setDurAbb(idx0, 0);
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setTpAdegAbb(idx0, Types.SPACE_CHAR);
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setModCalc(idx0, "");
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setImpAz(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setImpAder(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setImpTfr(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setImpVolo(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setVisEnd2000(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setDtVldtProd(idx0, 0);
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setDtIniValTar(idx0, 0);
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setImpbVisEnd2000(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setRenIniTsTec0(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setPcRipPre(idx0, new AfDecimal(0, 6, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setFlImportiForz(idx0, Types.SPACE_CHAR);
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setPrstzIniNforz(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setVisEnd2000Nforz(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setIntrMora(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setManfeeAntic(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setManfeeRicor(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setPreUniRivto(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setProv1aaAcq(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setProv2aaAcq(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setProvRicor(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setProvInc(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setAlqProvAcq(idx0, new AfDecimal(0, 6, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setAlqProvInc(idx0, new AfDecimal(0, 6, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setAlqProvRicor(idx0, new AfDecimal(0, 6, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setImpbProvAcq(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setImpbProvInc(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setImpbProvRicor(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setFlProvForz(idx0, Types.SPACE_CHAR);
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setPrstzAggIni(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setIncrPre(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setIncrPrstz(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setDtUltAdegPrePr(idx0, 0);
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setPrstzAggUlt(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setTsRivalNet(idx0, new AfDecimal(0, 14, 9));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setPrePattuito(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setTpRival(idx0, "");
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setRisMat(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setCptMinScad(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setCommisGest(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setTpManfeeAppl(idx0, "");
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setDsRiga(idx0, 0);
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setDsOperSql(idx0, Types.SPACE_CHAR);
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setDsVer(idx0, 0);
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setDsTsIniCptz(idx0, 0);
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setDsTsEndCptz(idx0, 0);
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setDsUtente(idx0, "");
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setDsStatoElab(idx0, Types.SPACE_CHAR);
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setPcCommisGest(idx0, new AfDecimal(0, 6, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setNumGgRival(idx0, 0);
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setImpTrasfe(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setImpTfrStrc(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setAcqExp(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setRemunAss(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setCommisInter(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setAlqRemunAss(idx0, new AfDecimal(0, 6, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setAlqCommisInter(idx0, new AfDecimal(0, 6, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setImpbRemunAss(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setImpbCommisInter(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setCosRunAssva(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setCosRunAssvaIdc(idx0, new AfDecimal(0, 15, 3));
        }
    }

    public void initWdadAreaDefaultAdes() {
        ws.getAreaBusiness().setWdadEleDfltAdesMax(((short)0));
        ws.getAreaBusiness().getLccvdad1().getStatus().setStatus(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvdad1().setIdPtf(0);
        ws.getAreaBusiness().getLccvdad1().getDati().setWdadIdDfltAdes(0);
        ws.getAreaBusiness().getLccvdad1().getDati().setWdadIdPoli(0);
        ws.getAreaBusiness().getLccvdad1().getDati().setWdadIbPoli("");
        ws.getAreaBusiness().getLccvdad1().getDati().setWdadIbDflt("");
        ws.getAreaBusiness().getLccvdad1().getDati().setWdadCodGar1("");
        ws.getAreaBusiness().getLccvdad1().getDati().setWdadCodGar2("");
        ws.getAreaBusiness().getLccvdad1().getDati().setWdadCodGar3("");
        ws.getAreaBusiness().getLccvdad1().getDati().setWdadCodGar4("");
        ws.getAreaBusiness().getLccvdad1().getDati().setWdadCodGar5("");
        ws.getAreaBusiness().getLccvdad1().getDati().setWdadCodGar6("");
        ws.getAreaBusiness().getLccvdad1().getDati().getWdadDtDecorDflt().setWdadDtDecorDflt(0);
        ws.getAreaBusiness().getLccvdad1().getDati().getWdadEtaScadMascDflt().setWdadEtaScadMascDflt(0);
        ws.getAreaBusiness().getLccvdad1().getDati().getWdadEtaScadFemmDflt().setWdadEtaScadFemmDflt(0);
        ws.getAreaBusiness().getLccvdad1().getDati().getWdadDurAaAdesDflt().setWdadDurAaAdesDflt(0);
        ws.getAreaBusiness().getLccvdad1().getDati().getWdadDurMmAdesDflt().setWdadDurMmAdesDflt(0);
        ws.getAreaBusiness().getLccvdad1().getDati().getWdadDurGgAdesDflt().setWdadDurGgAdesDflt(0);
        ws.getAreaBusiness().getLccvdad1().getDati().getWdadDtScadAdesDflt().setWdadDtScadAdesDflt(0);
        ws.getAreaBusiness().getLccvdad1().getDati().getWdadFrazDflt().setWdadFrazDflt(0);
        ws.getAreaBusiness().getLccvdad1().getDati().getWdadPcProvIncDflt().setWdadPcProvIncDflt(new AfDecimal(0, 6, 3));
        ws.getAreaBusiness().getLccvdad1().getDati().getWdadImpProvIncDflt().setWdadImpProvIncDflt(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdad1().getDati().getWdadImpAz().setWdadImpAz(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdad1().getDati().getWdadImpAder().setWdadImpAder(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdad1().getDati().getWdadImpTfr().setWdadImpTfr(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdad1().getDati().getWdadImpVolo().setWdadImpVolo(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdad1().getDati().getWdadPcAz().setWdadPcAz(new AfDecimal(0, 6, 3));
        ws.getAreaBusiness().getLccvdad1().getDati().getWdadPcAder().setWdadPcAder(new AfDecimal(0, 6, 3));
        ws.getAreaBusiness().getLccvdad1().getDati().getWdadPcTfr().setWdadPcTfr(new AfDecimal(0, 6, 3));
        ws.getAreaBusiness().getLccvdad1().getDati().getWdadPcVolo().setWdadPcVolo(new AfDecimal(0, 6, 3));
        ws.getAreaBusiness().getLccvdad1().getDati().setWdadTpFntCnbtva("");
        ws.getAreaBusiness().getLccvdad1().getDati().getWdadImpPreDflt().setWdadImpPreDflt(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdad1().getDati().setWdadCodCompAnia(0);
        ws.getAreaBusiness().getLccvdad1().getDati().setWdadTpPre(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvdad1().getDati().setWdadDsOperSql(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvdad1().getDati().setWdadDsVer(0);
        ws.getAreaBusiness().getLccvdad1().getDati().setWdadDsTsCptz(0);
        ws.getAreaBusiness().getLccvdad1().getDati().setWdadDsUtente("");
        ws.getAreaBusiness().getLccvdad1().getDati().setWdadDsStatoElab(Types.SPACE_CHAR);
    }

    public void initWocoAreaOggColl() {
        ws.getAreaBusiness().getWocoAreaOggColl().setWocoEleOggCollMax(((short)0));
        for (int idx0 = 1; idx0 <= WocoTab.TAB_OGG_COLL_MAXOCCURS; idx0++) {
            ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setStatus(idx0, Types.SPACE_CHAR);
            ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setIdPtf(idx0, 0);
            ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setIdOggCollg(idx0, 0);
            ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setIdOggCoinv(idx0, 0);
            ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setTpOggCoinv(idx0, "");
            ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setIdOggDer(idx0, 0);
            ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setTpOggDer(idx0, "");
            ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setIbRiftoEstno(idx0, "");
            ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setIdMoviCrz(idx0, 0);
            ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setIdMoviChiu(idx0, 0);
            ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setDtIniEff(idx0, 0);
            ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setDtEndEff(idx0, 0);
            ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setCodCompAnia(idx0, 0);
            ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setCodProd(idx0, "");
            ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setDtScad(idx0, 0);
            ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setTpCollgm(idx0, "");
            ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setTpTrasf(idx0, "");
            ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setRecProv(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setDtDecor(idx0, 0);
            ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setDtUltPrePag(idx0, 0);
            ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setTotPre(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setRisMat(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setRisZil(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setImpTrasf(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setImpReinvst(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setPcReinvstRilievi(idx0, new AfDecimal(0, 6, 3));
            ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setIb2oRiftoEstno(idx0, "");
            ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setIndLiqAggMan(idx0, Types.SPACE_CHAR);
            ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setDsRiga(idx0, 0);
            ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setDsOperSql(idx0, Types.SPACE_CHAR);
            ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setDsVer(idx0, 0);
            ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setDsTsIniCptz(idx0, 0);
            ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setDsTsEndCptz(idx0, 0);
            ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setDsUtente(idx0, "");
            ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setDsStatoElab(idx0, Types.SPACE_CHAR);
            ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setImpCollg(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setPrePerTrasf(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setCarAcq(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setPre1aAnnualita(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setImpTrasferito(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setTpModAbbinamento(idx0, "");
            ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setPcPreTrasferito(idx0, new AfDecimal(0, 6, 3));
        }
    }

    public void initWdflAreaDfl() {
        ws.getAreaBusiness().setWdflEleDflMax(((short)0));
        ws.getAreaBusiness().getLccvdfl1().getStatus().setStatus(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvdfl1().setIdPtf(0);
        ws.getAreaBusiness().getLccvdfl1().getDati().setWdflIdDForzLiq(0);
        ws.getAreaBusiness().getLccvdfl1().getDati().setWdflIdLiq(0);
        ws.getAreaBusiness().getLccvdfl1().getDati().setWdflIdMoviCrz(0);
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflIdMoviChiu().setWdflIdMoviChiu(0);
        ws.getAreaBusiness().getLccvdfl1().getDati().setWdflCodCompAnia(0);
        ws.getAreaBusiness().getLccvdfl1().getDati().setWdflDtIniEff(0);
        ws.getAreaBusiness().getLccvdfl1().getDati().setWdflDtEndEff(0);
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpLrdCalc().setWdflImpLrdCalc(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpLrdDfz().setWdflImpLrdDfz(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpLrdEfflq().setWdflImpLrdEfflq(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpNetCalc().setWdflImpNetCalc(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpNetDfz().setWdflImpNetDfz(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpNetEfflq().setWdflImpNetEfflq(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpstPrvrCalc().setWdflImpstPrvrCalc(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpstPrvrDfz().setWdflImpstPrvrDfz(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpstPrvrEfflq().setWdflImpstPrvrEfflq(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpstVisCalc().setWdflImpstVisCalc(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpstVisDfz().setWdflImpstVisDfz(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpstVisEfflq().setWdflImpstVisEfflq(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflRitAccCalc().setWdflRitAccCalc(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflRitAccDfz().setWdflRitAccDfz(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflRitAccEfflq().setWdflRitAccEfflq(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflRitIrpefCalc().setWdflRitIrpefCalc(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflRitIrpefDfz().setWdflRitIrpefDfz(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflRitIrpefEfflq().setWdflRitIrpefEfflq(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpstSostCalc().setWdflImpstSostCalc(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpstSostDfz().setWdflImpstSostDfz(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpstSostEfflq().setWdflImpstSostEfflq(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflTaxSepCalc().setWdflTaxSepCalc(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflTaxSepDfz().setWdflTaxSepDfz(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflTaxSepEfflq().setWdflTaxSepEfflq(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflIntrPrestCalc().setWdflIntrPrestCalc(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflIntrPrestDfz().setWdflIntrPrestDfz(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflIntrPrestEfflq().setWdflIntrPrestEfflq(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflAccpreSostCalc().setWdflAccpreSostCalc(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflAccpreSostDfz().setWdflAccpreSostDfz(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflAccpreSostEfflq().setWdflAccpreSostEfflq(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflAccpreVisCalc().setWdflAccpreVisCalc(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflAccpreVisDfz().setWdflAccpreVisDfz(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflAccpreVisEfflq().setWdflAccpreVisEfflq(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflAccpreAccCalc().setWdflAccpreAccCalc(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflAccpreAccDfz().setWdflAccpreAccDfz(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflAccpreAccEfflq().setWdflAccpreAccEfflq(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflResPrstzCalc().setWdflResPrstzCalc(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflResPrstzDfz().setWdflResPrstzDfz(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflResPrstzEfflq().setWdflResPrstzEfflq(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflResPreAttCalc().setWdflResPreAttCalc(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflResPreAttDfz().setWdflResPreAttDfz(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflResPreAttEfflq().setWdflResPreAttEfflq(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpExcontrEff().setWdflImpExcontrEff(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpbVisCalc().setWdflImpbVisCalc(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpbVisEfflq().setWdflImpbVisEfflq(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpbVisDfz().setWdflImpbVisDfz(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpbRitAccCalc().setWdflImpbRitAccCalc(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpbRitAccEfflq().setWdflImpbRitAccEfflq(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpbRitAccDfz().setWdflImpbRitAccDfz(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpbTfrCalc().setWdflImpbTfrCalc(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpbTfrEfflq().setWdflImpbTfrEfflq(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpbTfrDfz().setWdflImpbTfrDfz(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpbIsCalc().setWdflImpbIsCalc(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpbIsEfflq().setWdflImpbIsEfflq(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpbIsDfz().setWdflImpbIsDfz(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpbTaxSepCalc().setWdflImpbTaxSepCalc(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpbTaxSepEfflq().setWdflImpbTaxSepEfflq(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpbTaxSepDfz().setWdflImpbTaxSepDfz(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflIintPrestCalc().setWdflIintPrestCalc(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflIintPrestEfflq().setWdflIintPrestEfflq(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflIintPrestDfz().setWdflIintPrestDfz(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflMontEnd2000Calc().setWdflMontEnd2000Calc(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflMontEnd2000Efflq().setWdflMontEnd2000Efflq(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflMontEnd2000Dfz().setWdflMontEnd2000Dfz(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflMontEnd2006Calc().setWdflMontEnd2006Calc(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflMontEnd2006Efflq().setWdflMontEnd2006Efflq(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflMontEnd2006Dfz().setWdflMontEnd2006Dfz(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflMontDal2007Calc().setWdflMontDal2007Calc(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflMontDal2007Efflq().setWdflMontDal2007Efflq(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflMontDal2007Dfz().setWdflMontDal2007Dfz(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflIimpstPrvrCalc().setWdflIimpstPrvrCalc(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflIimpstPrvrEfflq().setWdflIimpstPrvrEfflq(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflIimpstPrvrDfz().setWdflIimpstPrvrDfz(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflIimpst252Calc().setWdflIimpst252Calc(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflIimpst252Efflq().setWdflIimpst252Efflq(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflIimpst252Dfz().setWdflIimpst252Dfz(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpst252Calc().setWdflImpst252Calc(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpst252Efflq().setWdflImpst252Efflq(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflRitTfrCalc().setWdflRitTfrCalc(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflRitTfrEfflq().setWdflRitTfrEfflq(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflRitTfrDfz().setWdflRitTfrDfz(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflCnbtInpstfmCalc().setWdflCnbtInpstfmCalc(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflCnbtInpstfmEfflq().setWdflCnbtInpstfmEfflq(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflCnbtInpstfmDfz().setWdflCnbtInpstfmDfz(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflIcnbInpstfmCalc().setWdflIcnbInpstfmCalc(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflIcnbInpstfmEfflq().setWdflIcnbInpstfmEfflq(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflIcnbInpstfmDfz().setWdflIcnbInpstfmDfz(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflCndeEnd2000Calc().setWdflCndeEnd2000Calc(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflCndeEnd2000Efflq().setWdflCndeEnd2000Efflq(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflCndeEnd2000Dfz().setWdflCndeEnd2000Dfz(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflCndeEnd2006Calc().setWdflCndeEnd2006Calc(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflCndeEnd2006Efflq().setWdflCndeEnd2006Efflq(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflCndeEnd2006Dfz().setWdflCndeEnd2006Dfz(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflCndeDal2007Calc().setWdflCndeDal2007Calc(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflCndeDal2007Efflq().setWdflCndeDal2007Efflq(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflCndeDal2007Dfz().setWdflCndeDal2007Dfz(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflAaCnbzEnd2000Ef().setWdflAaCnbzEnd2000Ef(0);
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflAaCnbzEnd2006Ef().setWdflAaCnbzEnd2006Ef(0);
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflAaCnbzDal2007Ef().setWdflAaCnbzDal2007Ef(0);
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflMmCnbzEnd2000Ef().setWdflMmCnbzEnd2000Ef(0);
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflMmCnbzEnd2006Ef().setWdflMmCnbzEnd2006Ef(0);
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflMmCnbzDal2007Ef().setWdflMmCnbzDal2007Ef(0);
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpstDaRimbEff().setWdflImpstDaRimbEff(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflAlqTaxSepCalc().setWdflAlqTaxSepCalc(new AfDecimal(0, 6, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflAlqTaxSepEfflq().setWdflAlqTaxSepEfflq(new AfDecimal(0, 6, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflAlqTaxSepDfz().setWdflAlqTaxSepDfz(new AfDecimal(0, 6, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflAlqCnbtInpstfmC().setWdflAlqCnbtInpstfmC(new AfDecimal(0, 6, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflAlqCnbtInpstfmE().setWdflAlqCnbtInpstfmE(new AfDecimal(0, 6, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflAlqCnbtInpstfmD().setWdflAlqCnbtInpstfmD(new AfDecimal(0, 6, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().setWdflDsRiga(0);
        ws.getAreaBusiness().getLccvdfl1().getDati().setWdflDsOperSql(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvdfl1().getDati().setWdflDsVer(0);
        ws.getAreaBusiness().getLccvdfl1().getDati().setWdflDsTsIniCptz(0);
        ws.getAreaBusiness().getLccvdfl1().getDati().setWdflDsTsEndCptz(0);
        ws.getAreaBusiness().getLccvdfl1().getDati().setWdflDsUtente("");
        ws.getAreaBusiness().getLccvdfl1().getDati().setWdflDsStatoElab(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpbVis1382011c().setWdflImpbVis1382011c(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpbVis1382011d().setWdflImpbVis1382011d(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpbVis1382011l().setWdflImpbVis1382011l(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpstVis1382011c().setWdflImpstVis1382011c(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpstVis1382011d().setWdflImpstVis1382011d(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpstVis1382011l().setWdflImpstVis1382011l(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpbIs1382011c().setWdflImpbIs1382011c(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpbIs1382011d().setWdflImpbIs1382011d(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpbIs1382011l().setWdflImpbIs1382011l(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflIs1382011c().setWdflIs1382011c(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflIs1382011d().setWdflIs1382011d(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflIs1382011l().setWdflIs1382011l(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpIntrRitPagC().setWdflImpIntrRitPagC(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpIntrRitPagD().setWdflImpIntrRitPagD(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpIntrRitPagL().setWdflImpIntrRitPagL(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpbBolloDettC().setWdflImpbBolloDettC(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpbBolloDettD().setWdflImpbBolloDettD(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpbBolloDettL().setWdflImpbBolloDettL(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpstBolloDettC().setWdflImpstBolloDettC(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpstBolloDettD().setWdflImpstBolloDettD(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpstBolloDettL().setWdflImpstBolloDettL(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpstBolloTotVc().setWdflImpstBolloTotVc(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpstBolloTotVd().setWdflImpstBolloTotVd(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpstBolloTotVl().setWdflImpstBolloTotVl(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpbVis662014c().setWdflImpbVis662014c(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpbVis662014d().setWdflImpbVis662014d(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpbVis662014l().setWdflImpbVis662014l(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpstVis662014c().setWdflImpstVis662014c(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpstVis662014d().setWdflImpstVis662014d(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpstVis662014l().setWdflImpstVis662014l(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpbIs662014c().setWdflImpbIs662014c(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpbIs662014d().setWdflImpbIs662014d(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflImpbIs662014l().setWdflImpbIs662014l(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflIs662014c().setWdflIs662014c(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflIs662014d().setWdflIs662014d(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvdfl1().getDati().getWdflIs662014l().setWdflIs662014l(new AfDecimal(0, 15, 3));
    }

    public void initWrstAreaRst() {
        ws.getAreaBusiness().setWrstEleRstMax(((short)0));
        for (int idx0 = 1; idx0 <= AreaBusinessIvvs0211.WRST_TAB_RST_MAXOCCURS; idx0++) {
            ws.getAreaBusiness().getWrstTabRst(idx0).getLccvrst1().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWrstTabRst(idx0).getLccvrst1().setIdPtf(0);
            ws.getAreaBusiness().getWrstTabRst(idx0).getLccvrst1().getDati().setWrstIdRisDiTrch(0);
            ws.getAreaBusiness().getWrstTabRst(idx0).getLccvrst1().getDati().setWrstIdMoviCrz(0);
            ws.getAreaBusiness().getWrstTabRst(idx0).getLccvrst1().getDati().getWrstIdMoviChiu().setWrstIdMoviChiu(0);
            ws.getAreaBusiness().getWrstTabRst(idx0).getLccvrst1().getDati().setWrstDtIniEff(0);
            ws.getAreaBusiness().getWrstTabRst(idx0).getLccvrst1().getDati().setWrstDtEndEff(0);
            ws.getAreaBusiness().getWrstTabRst(idx0).getLccvrst1().getDati().setWrstCodCompAnia(0);
            ws.getAreaBusiness().getWrstTabRst(idx0).getLccvrst1().getDati().setWrstTpCalcRis("");
            ws.getAreaBusiness().getWrstTabRst(idx0).getLccvrst1().getDati().getWrstUltRm().setWrstUltRm(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWrstTabRst(idx0).getLccvrst1().getDati().getWrstDtCalc().setWrstDtCalc(0);
            ws.getAreaBusiness().getWrstTabRst(idx0).getLccvrst1().getDati().getWrstDtElab().setWrstDtElab(0);
            ws.getAreaBusiness().getWrstTabRst(idx0).getLccvrst1().getDati().getWrstRisBila().setWrstRisBila(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWrstTabRst(idx0).getLccvrst1().getDati().getWrstRisMat().setWrstRisMat(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWrstTabRst(idx0).getLccvrst1().getDati().getWrstIncrXRival().setWrstIncrXRival(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWrstTabRst(idx0).getLccvrst1().getDati().getWrstRptoPre().setWrstRptoPre(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWrstTabRst(idx0).getLccvrst1().getDati().getWrstFrazPrePp().setWrstFrazPrePp(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWrstTabRst(idx0).getLccvrst1().getDati().getWrstRisTot().setWrstRisTot(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWrstTabRst(idx0).getLccvrst1().getDati().getWrstRisSpe().setWrstRisSpe(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWrstTabRst(idx0).getLccvrst1().getDati().getWrstRisAbb().setWrstRisAbb(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWrstTabRst(idx0).getLccvrst1().getDati().getWrstRisBnsfdt().setWrstRisBnsfdt(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWrstTabRst(idx0).getLccvrst1().getDati().getWrstRisSopr().setWrstRisSopr(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWrstTabRst(idx0).getLccvrst1().getDati().getWrstRisIntegBasTec().setWrstRisIntegBasTec(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWrstTabRst(idx0).getLccvrst1().getDati().getWrstRisIntegDecrTs().setWrstRisIntegDecrTs(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWrstTabRst(idx0).getLccvrst1().getDati().getWrstRisGarCasoMor().setWrstRisGarCasoMor(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWrstTabRst(idx0).getLccvrst1().getDati().getWrstRisZil().setWrstRisZil(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWrstTabRst(idx0).getLccvrst1().getDati().getWrstRisFaivl().setWrstRisFaivl(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWrstTabRst(idx0).getLccvrst1().getDati().getWrstRisCosAmmtz().setWrstRisCosAmmtz(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWrstTabRst(idx0).getLccvrst1().getDati().getWrstRisSpeFaivl().setWrstRisSpeFaivl(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWrstTabRst(idx0).getLccvrst1().getDati().getWrstRisPrestFaivl().setWrstRisPrestFaivl(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWrstTabRst(idx0).getLccvrst1().getDati().getWrstRisComponAssva().setWrstRisComponAssva(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWrstTabRst(idx0).getLccvrst1().getDati().getWrstUltCoeffRis().setWrstUltCoeffRis(new AfDecimal(0, 14, 9));
            ws.getAreaBusiness().getWrstTabRst(idx0).getLccvrst1().getDati().getWrstUltCoeffAggRis().setWrstUltCoeffAggRis(new AfDecimal(0, 14, 9));
            ws.getAreaBusiness().getWrstTabRst(idx0).getLccvrst1().getDati().getWrstRisAcq().setWrstRisAcq(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWrstTabRst(idx0).getLccvrst1().getDati().getWrstRisUti().setWrstRisUti(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWrstTabRst(idx0).getLccvrst1().getDati().getWrstRisMatEff().setWrstRisMatEff(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWrstTabRst(idx0).getLccvrst1().getDati().getWrstRisRistorniCap().setWrstRisRistorniCap(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWrstTabRst(idx0).getLccvrst1().getDati().getWrstRisTrmBns().setWrstRisTrmBns(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWrstTabRst(idx0).getLccvrst1().getDati().getWrstRisBnsric().setWrstRisBnsric(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWrstTabRst(idx0).getLccvrst1().getDati().setWrstDsRiga(0);
            ws.getAreaBusiness().getWrstTabRst(idx0).getLccvrst1().getDati().setWrstDsOperSql(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWrstTabRst(idx0).getLccvrst1().getDati().setWrstDsVer(0);
            ws.getAreaBusiness().getWrstTabRst(idx0).getLccvrst1().getDati().setWrstDsTsIniCptz(0);
            ws.getAreaBusiness().getWrstTabRst(idx0).getLccvrst1().getDati().setWrstDsTsEndCptz(0);
            ws.getAreaBusiness().getWrstTabRst(idx0).getLccvrst1().getDati().setWrstDsUtente("");
            ws.getAreaBusiness().getWrstTabRst(idx0).getLccvrst1().getDati().setWrstDsStatoElab(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWrstTabRst(idx0).getLccvrst1().getDati().setWrstIdTrchDiGar(0);
            ws.getAreaBusiness().getWrstTabRst(idx0).getLccvrst1().getDati().setWrstCodFnd("");
            ws.getAreaBusiness().getWrstTabRst(idx0).getLccvrst1().getDati().setWrstIdPoli(0);
            ws.getAreaBusiness().getWrstTabRst(idx0).getLccvrst1().getDati().setWrstIdAdes(0);
            ws.getAreaBusiness().getWrstTabRst(idx0).getLccvrst1().getDati().setWrstIdGar(0);
            ws.getAreaBusiness().getWrstTabRst(idx0).getLccvrst1().getDati().getWrstRisMinGarto().setWrstRisMinGarto(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWrstTabRst(idx0).getLccvrst1().getDati().getWrstRisRshDflt().setWrstRisRshDflt(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWrstTabRst(idx0).getLccvrst1().getDati().getWrstRisMoviNonInves().setWrstRisMoviNonInves(new AfDecimal(0, 15, 3));
        }
    }

    public void initWl19AreaQuote() {
        ws.getAreaBusiness().getWl19AreaQuote().setWl19EleFndMax(((short)0));
        for (int idx0 = 1; idx0 <= Wl19Tabella.TAB_FND_MAXOCCURS; idx0++) {
            ws.getAreaBusiness().getWl19AreaQuote().getWl19Tabella().setStatus(idx0, Types.SPACE_CHAR);
            ws.getAreaBusiness().getWl19AreaQuote().getWl19Tabella().setCodCompAnia(idx0, 0);
            ws.getAreaBusiness().getWl19AreaQuote().getWl19Tabella().setCodFnd(idx0, "");
            ws.getAreaBusiness().getWl19AreaQuote().getWl19Tabella().setDtQtz(idx0, 0);
            ws.getAreaBusiness().getWl19AreaQuote().getWl19Tabella().setValQuo(idx0, new AfDecimal(0, 12, 5));
            ws.getAreaBusiness().getWl19AreaQuote().getWl19Tabella().setValQuoManfee(idx0, new AfDecimal(0, 12, 5));
            ws.getAreaBusiness().getWl19AreaQuote().getWl19Tabella().setDsOperSql(idx0, Types.SPACE_CHAR);
            ws.getAreaBusiness().getWl19AreaQuote().getWl19Tabella().setDsVer(idx0, 0);
            ws.getAreaBusiness().getWl19AreaQuote().getWl19Tabella().setDsTsCptz(idx0, 0);
            ws.getAreaBusiness().getWl19AreaQuote().getWl19Tabella().setDsUtente(idx0, "");
            ws.getAreaBusiness().getWl19AreaQuote().getWl19Tabella().setDsStatoElab(idx0, Types.SPACE_CHAR);
            ws.getAreaBusiness().getWl19AreaQuote().getWl19Tabella().setTpFnd(idx0, Types.SPACE_CHAR);
            ws.getAreaBusiness().getWl19AreaQuote().getWl19Tabella().setDtRilevazioneNav(idx0, 0);
            ws.getAreaBusiness().getWl19AreaQuote().getWl19Tabella().setValQuoAcq(idx0, new AfDecimal(0, 12, 5));
            ws.getAreaBusiness().getWl19AreaQuote().getWl19Tabella().setFlNoNav(idx0, Types.SPACE_CHAR);
        }
    }

    public void initWl30AreaReinvstPoli() {
        ws.getAreaBusiness().setWl30EleReinvstPoliMax(((short)0));
        for (int idx0 = 1; idx0 <= AreaBusinessIvvs0211.WL30_TAB_REINVST_POLI_MAXOCCURS; idx0++) {
            ws.getAreaBusiness().getWl30TabReinvstPoli(idx0).getLccvl301().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWl30TabReinvstPoli(idx0).getLccvl301().setIdPtf(0);
            ws.getAreaBusiness().getWl30TabReinvstPoli(idx0).getLccvl301().getDati().setWl30IdReinvstPoliLq(0);
            ws.getAreaBusiness().getWl30TabReinvstPoli(idx0).getLccvl301().getDati().setWl30IdMoviCrz(0);
            ws.getAreaBusiness().getWl30TabReinvstPoli(idx0).getLccvl301().getDati().getWl30IdMoviChiu().setWl30IdMoviChiu(0);
            ws.getAreaBusiness().getWl30TabReinvstPoli(idx0).getLccvl301().getDati().setWl30DtIniEff(0);
            ws.getAreaBusiness().getWl30TabReinvstPoli(idx0).getLccvl301().getDati().setWl30DtEndEff(0);
            ws.getAreaBusiness().getWl30TabReinvstPoli(idx0).getLccvl301().getDati().setWl30CodCompAnia(0);
            ws.getAreaBusiness().getWl30TabReinvstPoli(idx0).getLccvl301().getDati().setWl30CodRamo("");
            ws.getAreaBusiness().getWl30TabReinvstPoli(idx0).getLccvl301().getDati().setWl30IbPoli("");
            ws.getAreaBusiness().getWl30TabReinvstPoli(idx0).getLccvl301().getDati().setWl30PrKeySistEstno("");
            ws.getAreaBusiness().getWl30TabReinvstPoli(idx0).getLccvl301().getDati().setWl30SecKeySistEstno("");
            ws.getAreaBusiness().getWl30TabReinvstPoli(idx0).getLccvl301().getDati().setWl30CodCan(0);
            ws.getAreaBusiness().getWl30TabReinvstPoli(idx0).getLccvl301().getDati().setWl30CodAge(0);
            ws.getAreaBusiness().getWl30TabReinvstPoli(idx0).getLccvl301().getDati().getWl30CodSubAge().setWl30CodSubAge(0);
            ws.getAreaBusiness().getWl30TabReinvstPoli(idx0).getLccvl301().getDati().setWl30ImpRes(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWl30TabReinvstPoli(idx0).getLccvl301().getDati().setWl30TpLiq("");
            ws.getAreaBusiness().getWl30TabReinvstPoli(idx0).getLccvl301().getDati().setWl30TpSistEstno("");
            ws.getAreaBusiness().getWl30TabReinvstPoli(idx0).getLccvl301().getDati().setWl30CodFiscPartIva("");
            ws.getAreaBusiness().getWl30TabReinvstPoli(idx0).getLccvl301().getDati().setWl30CodMoviLiq(0);
            ws.getAreaBusiness().getWl30TabReinvstPoli(idx0).getLccvl301().getDati().getWl30TpInvstLiq().setWl30TpInvstLiq(((short)0));
            ws.getAreaBusiness().getWl30TabReinvstPoli(idx0).getLccvl301().getDati().setWl30ImpTotLiq(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWl30TabReinvstPoli(idx0).getLccvl301().getDati().setWl30DsRiga(0);
            ws.getAreaBusiness().getWl30TabReinvstPoli(idx0).getLccvl301().getDati().setWl30DsOperSql(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWl30TabReinvstPoli(idx0).getLccvl301().getDati().setWl30DsVer(0);
            ws.getAreaBusiness().getWl30TabReinvstPoli(idx0).getLccvl301().getDati().setWl30DsTsIniCptz(0);
            ws.getAreaBusiness().getWl30TabReinvstPoli(idx0).getLccvl301().getDati().setWl30DsTsEndCptz(0);
            ws.getAreaBusiness().getWl30TabReinvstPoli(idx0).getLccvl301().getDati().setWl30DsUtente("");
            ws.getAreaBusiness().getWl30TabReinvstPoli(idx0).getLccvl301().getDati().setWl30DsStatoElab(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWl30TabReinvstPoli(idx0).getLccvl301().getDati().getWl30DtDecorPoliLq().setWl30DtDecorPoliLq(0);
        }
    }

    public void initWl23AreaVincPeg() {
        ws.getAreaBusiness().setWl23EleVincPegMax(((short)0));
        for (int idx0 = 1; idx0 <= AreaBusinessIvvs0211.WL23_TAB_VINC_PEG_MAXOCCURS; idx0++) {
            ws.getAreaBusiness().getWl23TabVincPeg(idx0).getLccvl231().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWl23TabVincPeg(idx0).getLccvl231().setIdPtf(0);
            ws.getAreaBusiness().getWl23TabVincPeg(idx0).getLccvl231().getDati().setWl23IdVincPeg(0);
            ws.getAreaBusiness().getWl23TabVincPeg(idx0).getLccvl231().getDati().setWl23IdMoviCrz(0);
            ws.getAreaBusiness().getWl23TabVincPeg(idx0).getLccvl231().getDati().getWl23IdMoviChiu().setWl23IdMoviChiu(0);
            ws.getAreaBusiness().getWl23TabVincPeg(idx0).getLccvl231().getDati().setWl23DtIniEff(0);
            ws.getAreaBusiness().getWl23TabVincPeg(idx0).getLccvl231().getDati().setWl23DtEndEff(0);
            ws.getAreaBusiness().getWl23TabVincPeg(idx0).getLccvl231().getDati().setWl23CodCompAnia(0);
            ws.getAreaBusiness().getWl23TabVincPeg(idx0).getLccvl231().getDati().setWl23IdRappAna(0);
            ws.getAreaBusiness().getWl23TabVincPeg(idx0).getLccvl231().getDati().setWl23TpVinc("");
            ws.getAreaBusiness().getWl23TabVincPeg(idx0).getLccvl231().getDati().setWl23FlDelegaAlRisc(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWl23TabVincPeg(idx0).getLccvl231().getDati().setWl23Desc("");
            ws.getAreaBusiness().getWl23TabVincPeg(idx0).getLccvl231().getDati().getWl23DtAttivVinpg().setWl23DtAttivVinpg(0);
            ws.getAreaBusiness().getWl23TabVincPeg(idx0).getLccvl231().getDati().getWl23CptVinctoPign().setWl23CptVinctoPign(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWl23TabVincPeg(idx0).getLccvl231().getDati().getWl23DtChiuVinpg().setWl23DtChiuVinpg(0);
            ws.getAreaBusiness().getWl23TabVincPeg(idx0).getLccvl231().getDati().getWl23ValRiscIniVinpg().setWl23ValRiscIniVinpg(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWl23TabVincPeg(idx0).getLccvl231().getDati().getWl23ValRiscEndVinpg().setWl23ValRiscEndVinpg(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWl23TabVincPeg(idx0).getLccvl231().getDati().getWl23SomPreVinpg().setWl23SomPreVinpg(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWl23TabVincPeg(idx0).getLccvl231().getDati().setWl23FlVinpgIntPrstz(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWl23TabVincPeg(idx0).getLccvl231().getDati().setWl23DescAggVinc("");
            ws.getAreaBusiness().getWl23TabVincPeg(idx0).getLccvl231().getDati().setWl23DsRiga(0);
            ws.getAreaBusiness().getWl23TabVincPeg(idx0).getLccvl231().getDati().setWl23DsOperSql(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWl23TabVincPeg(idx0).getLccvl231().getDati().setWl23DsVer(0);
            ws.getAreaBusiness().getWl23TabVincPeg(idx0).getLccvl231().getDati().setWl23DsTsIniCptz(0);
            ws.getAreaBusiness().getWl23TabVincPeg(idx0).getLccvl231().getDati().setWl23DsTsEndCptz(0);
            ws.getAreaBusiness().getWl23TabVincPeg(idx0).getLccvl231().getDati().setWl23DsUtente("");
            ws.getAreaBusiness().getWl23TabVincPeg(idx0).getLccvl231().getDati().setWl23DsStatoElab(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWl23TabVincPeg(idx0).getLccvl231().getDati().setWl23TpAutSeq("");
            ws.getAreaBusiness().getWl23TabVincPeg(idx0).getLccvl231().getDati().setWl23CodUffSeq("");
            ws.getAreaBusiness().getWl23TabVincPeg(idx0).getLccvl231().getDati().getWl23NumProvvSeq().setWl23NumProvvSeq(0);
            ws.getAreaBusiness().getWl23TabVincPeg(idx0).getLccvl231().getDati().setWl23TpProvvSeq("");
            ws.getAreaBusiness().getWl23TabVincPeg(idx0).getLccvl231().getDati().getWl23DtProvvSeq().setWl23DtProvvSeq(0);
            ws.getAreaBusiness().getWl23TabVincPeg(idx0).getLccvl231().getDati().getWl23DtNotificaBlocco().setWl23DtNotificaBlocco(0);
            ws.getAreaBusiness().getWl23TabVincPeg(idx0).getLccvl231().getDati().setWl23NotaProvv("");
        }
    }

    public void initWopzAreaOpzioni() {
        ws.getAreaBusiness().getIvvc0217().setEleMaxOpzioniFormatted("00");
        for (int idx0 = 1; idx0 <= Ivvc0217.OPZIONI_MAXOCCURS; idx0++) {
            ws.getAreaBusiness().getIvvc0217().getOpzioni(idx0).setCodTipoOpzione("");
            ws.getAreaBusiness().getIvvc0217().getOpzioni(idx0).setDescTipoOpzione("");
            ws.getAreaBusiness().getIvvc0217().getOpzioni(idx0).setWopzNumGarPolMaxEleFormatted("000");
            for (int idx1 = 1; idx1 <= WopzOpzioni.GAR_POLIZZA_MAXOCCURS; idx1++) {
                ws.getAreaBusiness().getIvvc0217().getOpzioni(idx0).getGarPolizza(idx1).setWopzGarCodice("");
            }
            ws.getAreaBusiness().getIvvc0217().getOpzioni(idx0).setWopzNumGarOpzMaxEleFormatted("000");
            for (int idx1 = 1; idx1 <= WopzOpzioni.GAR_OPZIONE_MAXOCCURS; idx1++) {
                ws.getAreaBusiness().getIvvc0217().getOpzioni(idx0).getGarOpzione(idx1).setCodice("");
                ws.getAreaBusiness().getIvvc0217().getOpzioni(idx0).getGarOpzione(idx1).setDecorrenza("");
                ws.getAreaBusiness().getIvvc0217().getOpzioni(idx0).getGarOpzione(idx1).setVersione(Types.SPACE_CHAR);
            }
        }
    }

    public void initWgopAreaGaranziaOpz() {
        ws.getAreaBusiness().setWgopEleGaranziaOpzMax(((short)0));
        for (int idx0 = 1; idx0 <= AreaBusinessIvvs0211.WGOP_TAB_GAR_MAXOCCURS; idx0++) {
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().setIdPtf(0);
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().setWgrzIdGar(0);
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().getWgrzIdAdes().setWgrzIdAdes(0);
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().setWgrzIdPoli(0);
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().setWgrzIdMoviCrz(0);
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().getWgrzIdMoviChiu().setWgrzIdMoviChiu(0);
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().setWgrzDtIniEff(0);
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().setWgrzDtEndEff(0);
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().setWgrzCodCompAnia(0);
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().setWgrzIbOgg("");
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().getWgrzDtDecor().setWgrzDtDecor(0);
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().getWgrzDtScad().setWgrzDtScad(0);
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().setWgrzCodSez("");
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().setWgrzCodTari("");
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().setWgrzRamoBila("");
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().getWgrzDtIniValTar().setWgrzDtIniValTar(0);
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().getWgrzId1oAssto().setWgrzId1oAssto(0);
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().getWgrzId2oAssto().setWgrzId2oAssto(0);
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().getWgrzId3oAssto().setWgrzId3oAssto(0);
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().getWgrzTpGar().setWgrzTpGar(((short)0));
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().setWgrzTpRsh("");
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().getWgrzTpInvst().setWgrzTpInvst(((short)0));
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().setWgrzModPagGarcol("");
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().setWgrzTpPerPre("");
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaAa1oAssto().setWgrzEtaAa1oAssto(((short)0));
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaMm1oAssto().setWgrzEtaMm1oAssto(((short)0));
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaAa2oAssto().setWgrzEtaAa2oAssto(((short)0));
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaMm2oAssto().setWgrzEtaMm2oAssto(((short)0));
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaAa3oAssto().setWgrzEtaAa3oAssto(((short)0));
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaMm3oAssto().setWgrzEtaMm3oAssto(((short)0));
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().setWgrzTpEmisPur(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaAScad().setWgrzEtaAScad(0);
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().setWgrzTpCalcPrePrstz("");
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().setWgrzTpPre(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().setWgrzTpDur("");
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().getWgrzDurAa().setWgrzDurAa(0);
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().getWgrzDurMm().setWgrzDurMm(0);
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().getWgrzDurGg().setWgrzDurGg(0);
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().getWgrzNumAaPagPre().setWgrzNumAaPagPre(0);
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().getWgrzAaPagPreUni().setWgrzAaPagPreUni(0);
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().getWgrzMmPagPreUni().setWgrzMmPagPreUni(0);
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().getWgrzFrazIniErogRen().setWgrzFrazIniErogRen(0);
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().getWgrzMm1oRat().setWgrzMm1oRat(((short)0));
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().getWgrzPc1oRat().setWgrzPc1oRat(new AfDecimal(0, 6, 3));
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().setWgrzTpPrstzAssta("");
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().getWgrzDtEndCarz().setWgrzDtEndCarz(0);
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().getWgrzPcRipPre().setWgrzPcRipPre(new AfDecimal(0, 6, 3));
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().setWgrzCodFnd("");
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().setWgrzAaRenCer("");
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().getWgrzPcRevrsb().setWgrzPcRevrsb(new AfDecimal(0, 6, 3));
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().setWgrzTpPcRip("");
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().getWgrzPcOpz().setWgrzPcOpz(new AfDecimal(0, 6, 3));
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().setWgrzTpIas("");
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().setWgrzTpStab("");
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().setWgrzTpAdegPre(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().getWgrzDtVarzTpIas().setWgrzDtVarzTpIas(0);
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().getWgrzFrazDecrCpt().setWgrzFrazDecrCpt(0);
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().setWgrzCodTratRiass("");
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().setWgrzTpDtEmisRiass("");
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().setWgrzTpCessRiass("");
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().setWgrzDsRiga(0);
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().setWgrzDsOperSql(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().setWgrzDsVer(0);
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().setWgrzDsTsIniCptz(0);
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().setWgrzDsTsEndCptz(0);
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().setWgrzDsUtente("");
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().setWgrzDsStatoElab(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().getWgrzAaStab().setWgrzAaStab(0);
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().getWgrzTsStabLimitata().setWgrzTsStabLimitata(new AfDecimal(0, 14, 9));
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().getWgrzDtPresc().setWgrzDtPresc(0);
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().setWgrzRshInvst(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWgopTabGar(idx0).getLccvgrz1().getDati().setWgrzTpRamoBila("");
        }
    }

    public void initWtopAreaTrancheOpz() {
        ws.getAreaBusiness().setWtopEleTranOpzMax(((short)0));
        for (int idx0 = 1; idx0 <= AreaBusinessIvvs0211.WTOP_TAB_TRAN_MAXOCCURS; idx0++) {
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().setIdPtf(0);
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().setWtgaIdTrchDiGar(0);
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().setWtgaIdGar(0);
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().setWtgaIdAdes(0);
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().setWtgaIdPoli(0);
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().setWtgaIdMoviCrz(0);
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaIdMoviChiu().setWtgaIdMoviChiu(0);
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().setWtgaDtIniEff(0);
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().setWtgaDtEndEff(0);
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().setWtgaCodCompAnia(0);
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().setWtgaDtDecor(0);
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaDtScad().setWtgaDtScad(0);
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().setWtgaIbOgg("");
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().setWtgaTpRgmFisc("");
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaDtEmis().setWtgaDtEmis(0);
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().setWtgaTpTrch("");
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaDurAa().setWtgaDurAa(0);
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaDurMm().setWtgaDurMm(0);
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaDurGg().setWtgaDurGg(0);
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaPreCasoMor().setWtgaPreCasoMor(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaPcIntrRiat().setWtgaPcIntrRiat(new AfDecimal(0, 6, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaImpBnsAntic().setWtgaImpBnsAntic(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaPreIniNet().setWtgaPreIniNet(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaPrePpIni().setWtgaPrePpIni(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaPrePpUlt().setWtgaPrePpUlt(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaPreTariIni().setWtgaPreTariIni(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaPreTariUlt().setWtgaPreTariUlt(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaPreInvrioIni().setWtgaPreInvrioIni(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaPreInvrioUlt().setWtgaPreInvrioUlt(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaPreRivto().setWtgaPreRivto(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaImpSoprProf().setWtgaImpSoprProf(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaImpSoprSan().setWtgaImpSoprSan(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaImpSoprSpo().setWtgaImpSoprSpo(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaImpSoprTec().setWtgaImpSoprTec(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaImpAltSopr().setWtgaImpAltSopr(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaPreStab().setWtgaPreStab(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaDtEffStab().setWtgaDtEffStab(0);
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaTsRivalFis().setWtgaTsRivalFis(new AfDecimal(0, 14, 9));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaTsRivalIndiciz().setWtgaTsRivalIndiciz(new AfDecimal(0, 14, 9));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaOldTsTec().setWtgaOldTsTec(new AfDecimal(0, 14, 9));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaRatLrd().setWtgaRatLrd(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaPreLrd().setWtgaPreLrd(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzIni().setWtgaPrstzIni(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzUlt().setWtgaPrstzUlt(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaCptInOpzRivto().setWtgaCptInOpzRivto(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzIniStab().setWtgaPrstzIniStab(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaCptRshMor().setWtgaCptRshMor(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzRidIni().setWtgaPrstzRidIni(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().setWtgaFlCarCont(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaBnsGiaLiqto().setWtgaBnsGiaLiqto(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaImpBns().setWtgaImpBns(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().setWtgaCodDvs("");
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzIniNewfis().setWtgaPrstzIniNewfis(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaImpScon().setWtgaImpScon(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaAlqScon().setWtgaAlqScon(new AfDecimal(0, 6, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaImpCarAcq().setWtgaImpCarAcq(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaImpCarInc().setWtgaImpCarInc(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaImpCarGest().setWtgaImpCarGest(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaEtaAa1oAssto().setWtgaEtaAa1oAssto(((short)0));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaEtaMm1oAssto().setWtgaEtaMm1oAssto(((short)0));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaEtaAa2oAssto().setWtgaEtaAa2oAssto(((short)0));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaEtaMm2oAssto().setWtgaEtaMm2oAssto(((short)0));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaEtaAa3oAssto().setWtgaEtaAa3oAssto(((short)0));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaEtaMm3oAssto().setWtgaEtaMm3oAssto(((short)0));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaRendtoLrd().setWtgaRendtoLrd(new AfDecimal(0, 14, 9));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaPcRetr().setWtgaPcRetr(new AfDecimal(0, 6, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaRendtoRetr().setWtgaRendtoRetr(new AfDecimal(0, 14, 9));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaMinGarto().setWtgaMinGarto(new AfDecimal(0, 14, 9));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaMinTrnut().setWtgaMinTrnut(new AfDecimal(0, 14, 9));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaPreAttDiTrch().setWtgaPreAttDiTrch(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaMatuEnd2000().setWtgaMatuEnd2000(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaAbbTotIni().setWtgaAbbTotIni(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaAbbTotUlt().setWtgaAbbTotUlt(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaAbbAnnuUlt().setWtgaAbbAnnuUlt(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaDurAbb().setWtgaDurAbb(0);
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().setWtgaTpAdegAbb(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().setWtgaModCalc("");
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaImpAz().setWtgaImpAz(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaImpAder().setWtgaImpAder(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaImpTfr().setWtgaImpTfr(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaImpVolo().setWtgaImpVolo(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaVisEnd2000().setWtgaVisEnd2000(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaDtVldtProd().setWtgaDtVldtProd(0);
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaDtIniValTar().setWtgaDtIniValTar(0);
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaImpbVisEnd2000().setWtgaImpbVisEnd2000(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaRenIniTsTec0().setWtgaRenIniTsTec0(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaPcRipPre().setWtgaPcRipPre(new AfDecimal(0, 6, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().setWtgaFlImportiForz(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzIniNforz().setWtgaPrstzIniNforz(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaVisEnd2000Nforz().setWtgaVisEnd2000Nforz(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaIntrMora().setWtgaIntrMora(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaManfeeAntic().setWtgaManfeeAntic(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaManfeeRicor().setWtgaManfeeRicor(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaPreUniRivto().setWtgaPreUniRivto(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaProv1aaAcq().setWtgaProv1aaAcq(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaProv2aaAcq().setWtgaProv2aaAcq(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaProvRicor().setWtgaProvRicor(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaProvInc().setWtgaProvInc(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaAlqProvAcq().setWtgaAlqProvAcq(new AfDecimal(0, 6, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaAlqProvInc().setWtgaAlqProvInc(new AfDecimal(0, 6, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaAlqProvRicor().setWtgaAlqProvRicor(new AfDecimal(0, 6, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaImpbProvAcq().setWtgaImpbProvAcq(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaImpbProvInc().setWtgaImpbProvInc(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaImpbProvRicor().setWtgaImpbProvRicor(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().setWtgaFlProvForz(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzAggIni().setWtgaPrstzAggIni(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaIncrPre().setWtgaIncrPre(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaIncrPrstz().setWtgaIncrPrstz(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaDtUltAdegPrePr().setWtgaDtUltAdegPrePr(0);
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzAggUlt().setWtgaPrstzAggUlt(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaTsRivalNet().setWtgaTsRivalNet(new AfDecimal(0, 14, 9));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaPrePattuito().setWtgaPrePattuito(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().setWtgaTpRival("");
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaRisMat().setWtgaRisMat(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaCptMinScad().setWtgaCptMinScad(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaCommisGest().setWtgaCommisGest(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().setWtgaTpManfeeAppl("");
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().setWtgaDsRiga(0);
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().setWtgaDsOperSql(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().setWtgaDsVer(0);
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().setWtgaDsTsIniCptz(0);
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().setWtgaDsTsEndCptz(0);
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().setWtgaDsUtente("");
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().setWtgaDsStatoElab(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaPcCommisGest().setWtgaPcCommisGest(new AfDecimal(0, 6, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaNumGgRival().setWtgaNumGgRival(0);
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaImpTrasfe().setWtgaImpTrasfe(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaImpTfrStrc().setWtgaImpTfrStrc(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaAcqExp().setWtgaAcqExp(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaRemunAss().setWtgaRemunAss(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaCommisInter().setWtgaCommisInter(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaAlqRemunAss().setWtgaAlqRemunAss(new AfDecimal(0, 6, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaAlqCommisInter().setWtgaAlqCommisInter(new AfDecimal(0, 6, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaImpbRemunAss().setWtgaImpbRemunAss(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaImpbCommisInter().setWtgaImpbCommisInter(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaCosRunAssva().setWtgaCosRunAssva(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtopTabTran(idx0).getLccvtga1().getDati().getWtgaCosRunAssvaIdc().setWtgaCosRunAssvaIdc(new AfDecimal(0, 15, 3));
        }
    }

    public void initWtliAreaTrchLiq() {
        ws.getAreaBusiness().getWtliAreaTrchLiq().setWtliEleTrchLiqMax(((short)0));
        for (int idx0 = 1; idx0 <= WtliTab.TAB_TRCH_LIQ_MAXOCCURS; idx0++) {
            ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setStatus(idx0, Types.SPACE_CHAR);
            ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setIdPtf(idx0, 0);
            ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setIdTrchLiq(idx0, 0);
            ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setIdGarLiq(idx0, 0);
            ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setIdLiq(idx0, 0);
            ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setIdTrchDiGar(idx0, 0);
            ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setIdMoviCrz(idx0, 0);
            ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setIdMoviChiu(idx0, 0);
            ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setDtIniEff(idx0, 0);
            ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setDtEndEff(idx0, 0);
            ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setCodCompAnia(idx0, 0);
            ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setImpLrdCalc(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setImpLrdDfz(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setImpLrdEfflq(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setImpNetCalc(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setImpNetDfz(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setImpNetEfflq(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setImpUti(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setCodTari(idx0, "");
            ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setCodDvs(idx0, "");
            ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setIasOnerPrvntFin(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setIasMggSin(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setIasRstDpst(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setImpRimb(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setComponTaxRimb(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setRisMat(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setRisSpe(idx0, new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setDsRiga(idx0, 0);
            ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setDsOperSql(idx0, Types.SPACE_CHAR);
            ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setDsVer(idx0, 0);
            ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setDsTsIniCptz(idx0, 0);
            ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setDsTsEndCptz(idx0, 0);
            ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setDsUtente(idx0, "");
            ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setDsStatoElab(idx0, Types.SPACE_CHAR);
            ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setIasPnl(idx0, new AfDecimal(0, 15, 3));
        }
    }

    public void initWcntAreaDatiContest() {
        ws.getAreaBusiness().getIvvc0212().setWcntEleVarContMax(((short)0));
        for (int idx0 = 1; idx0 <= WcntTabVar.TAB_VAR_CONT_MAXOCCURS; idx0++) {
            ws.getAreaBusiness().getIvvc0212().getWcntTabVar().setWcntCodVarCont(idx0, "");
            ws.getAreaBusiness().getIvvc0212().getWcntTabVar().setWcntTpDatoCont(idx0, Types.SPACE_CHAR);
            ws.getAreaBusiness().getIvvc0212().getWcntTabVar().setWcntValImpCont(idx0, new AfDecimal(0, 18, 7));
            ws.getAreaBusiness().getIvvc0212().getWcntTabVar().setWcntValPercCont(idx0, new AfDecimal(0, 14, 9));
            ws.getAreaBusiness().getIvvc0212().getWcntTabVar().setWcntValStrCont(idx0, "");
            ws.getAreaBusiness().getIvvc0212().getWcntTabVar().setWcntTpLivello(idx0, Types.SPACE_CHAR);
            ws.getAreaBusiness().getIvvc0212().getWcntTabVar().setWcntCodLivello(idx0, "");
            ws.getAreaBusiness().getIvvc0212().getWcntTabVar().setWcntIdLivelloFormatted(idx0, "000000000");
        }
    }

    public void initWcltAreaClauTxt() {
        ws.getAreaBusiness().setWcltEleClauTxtMax(((short)0));
        for (int idx0 = 1; idx0 <= AreaBusinessIvvs0211.WCLT_TAB_CLAU_TXT_MAXOCCURS; idx0++) {
            ws.getAreaBusiness().getWcltTabClauTxt(idx0).getLccvclt1().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWcltTabClauTxt(idx0).getLccvclt1().setIdPtf(0);
            ws.getAreaBusiness().getWcltTabClauTxt(idx0).getLccvclt1().getDati().setIdClauTxt(0);
            ws.getAreaBusiness().getWcltTabClauTxt(idx0).getLccvclt1().getDati().setIdOgg(0);
            ws.getAreaBusiness().getWcltTabClauTxt(idx0).getLccvclt1().getDati().setTpOgg("");
            ws.getAreaBusiness().getWcltTabClauTxt(idx0).getLccvclt1().getDati().setIdMoviCrz(0);
            ws.getAreaBusiness().getWcltTabClauTxt(idx0).getLccvclt1().getDati().setIdMoviChiu(0);
            ws.getAreaBusiness().getWcltTabClauTxt(idx0).getLccvclt1().getDati().setDtIniEff(0);
            ws.getAreaBusiness().getWcltTabClauTxt(idx0).getLccvclt1().getDati().setDtEndEff(0);
            ws.getAreaBusiness().getWcltTabClauTxt(idx0).getLccvclt1().getDati().setCodCompAnia(0);
            ws.getAreaBusiness().getWcltTabClauTxt(idx0).getLccvclt1().getDati().setTpClau("");
            ws.getAreaBusiness().getWcltTabClauTxt(idx0).getLccvclt1().getDati().setCodClau("");
            ws.getAreaBusiness().getWcltTabClauTxt(idx0).getLccvclt1().getDati().setDescBreve("");
            ws.getAreaBusiness().getWcltTabClauTxt(idx0).getLccvclt1().getDati().setDescLng("");
            ws.getAreaBusiness().getWcltTabClauTxt(idx0).getLccvclt1().getDati().setDsRiga(0);
            ws.getAreaBusiness().getWcltTabClauTxt(idx0).getLccvclt1().getDati().setDsOperSql(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWcltTabClauTxt(idx0).getLccvclt1().getDati().setDsVer(0);
            ws.getAreaBusiness().getWcltTabClauTxt(idx0).getLccvclt1().getDati().setDsTsIniCptz(0);
            ws.getAreaBusiness().getWcltTabClauTxt(idx0).getLccvclt1().getDati().setDsTsEndCptz(0);
            ws.getAreaBusiness().getWcltTabClauTxt(idx0).getLccvclt1().getDati().setDsUtente("");
            ws.getAreaBusiness().getWcltTabClauTxt(idx0).getLccvclt1().getDati().setDsStatoElab(Types.SPACE_CHAR);
        }
    }

    public void initWp58AreaImpostaBollo() {
        ws.getAreaBusiness().setWp58EleMaxImpstBol(((short)0));
        for (int idx0 = 1; idx0 <= AreaBusinessIvvs0211.WP58_TAB_IMPST_BOL_MAXOCCURS; idx0++) {
            ws.getAreaBusiness().getWp58TabImpstBol(idx0).getLccvp581().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWp58TabImpstBol(idx0).getLccvp581().setIdPtf(0);
            ws.getAreaBusiness().getWp58TabImpstBol(idx0).getLccvp581().getDati().setWp58IdImpstBollo(0);
            ws.getAreaBusiness().getWp58TabImpstBol(idx0).getLccvp581().getDati().setWp58CodCompAnia(0);
            ws.getAreaBusiness().getWp58TabImpstBol(idx0).getLccvp581().getDati().setWp58IdPoli(0);
            ws.getAreaBusiness().getWp58TabImpstBol(idx0).getLccvp581().getDati().setWp58IbPoli("");
            ws.getAreaBusiness().getWp58TabImpstBol(idx0).getLccvp581().getDati().setWp58CodFisc("");
            ws.getAreaBusiness().getWp58TabImpstBol(idx0).getLccvp581().getDati().setWp58CodPartIva("");
            ws.getAreaBusiness().getWp58TabImpstBol(idx0).getLccvp581().getDati().setWp58IdRappAna(0);
            ws.getAreaBusiness().getWp58TabImpstBol(idx0).getLccvp581().getDati().setWp58IdMoviCrz(0);
            ws.getAreaBusiness().getWp58TabImpstBol(idx0).getLccvp581().getDati().getWp58IdMoviChiu().setWp58IdMoviChiu(0);
            ws.getAreaBusiness().getWp58TabImpstBol(idx0).getLccvp581().getDati().setWp58DtIniEff(0);
            ws.getAreaBusiness().getWp58TabImpstBol(idx0).getLccvp581().getDati().setWp58DtEndEff(0);
            ws.getAreaBusiness().getWp58TabImpstBol(idx0).getLccvp581().getDati().setWp58DtIniCalc(0);
            ws.getAreaBusiness().getWp58TabImpstBol(idx0).getLccvp581().getDati().setWp58DtEndCalc(0);
            ws.getAreaBusiness().getWp58TabImpstBol(idx0).getLccvp581().getDati().setWp58TpCausBollo("");
            ws.getAreaBusiness().getWp58TabImpstBol(idx0).getLccvp581().getDati().setWp58ImpstBolloDettC(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWp58TabImpstBol(idx0).getLccvp581().getDati().getWp58ImpstBolloDettV().setWp58ImpstBolloDettV(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWp58TabImpstBol(idx0).getLccvp581().getDati().getWp58ImpstBolloTotV().setWp58ImpstBolloTotV(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWp58TabImpstBol(idx0).getLccvp581().getDati().setWp58ImpstBolloTotR(new AfDecimal(0, 15, 3));
            ws.getAreaBusiness().getWp58TabImpstBol(idx0).getLccvp581().getDati().setWp58DsRiga(0);
            ws.getAreaBusiness().getWp58TabImpstBol(idx0).getLccvp581().getDati().setWp58DsOperSql(Types.SPACE_CHAR);
            ws.getAreaBusiness().getWp58TabImpstBol(idx0).getLccvp581().getDati().setWp58DsVer(0);
            ws.getAreaBusiness().getWp58TabImpstBol(idx0).getLccvp581().getDati().setWp58DsTsIniCptz(0);
            ws.getAreaBusiness().getWp58TabImpstBol(idx0).getLccvp581().getDati().setWp58DsTsEndCptz(0);
            ws.getAreaBusiness().getWp58TabImpstBol(idx0).getLccvp581().getDati().setWp58DsUtente("");
            ws.getAreaBusiness().getWp58TabImpstBol(idx0).getLccvp581().getDati().setWp58DsStatoElab(Types.SPACE_CHAR);
        }
    }

    public void initWp61AreaDCrist() {
        ws.getAreaBusiness().setWp61EleMaxDCrist(((short)0));
        ws.getAreaBusiness().getLccvp611().getStatus().setStatus(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvp611().setIdPtf(0);
        ws.getAreaBusiness().getLccvp611().getDati().setWp61IdDCrist(0);
        ws.getAreaBusiness().getLccvp611().getDati().setWp61IdPoli(0);
        ws.getAreaBusiness().getLccvp611().getDati().setWp61CodCompAnia(0);
        ws.getAreaBusiness().getLccvp611().getDati().setWp61IdMoviCrz(0);
        ws.getAreaBusiness().getLccvp611().getDati().getWp61IdMoviChiu().setWp61IdMoviChiu(0);
        ws.getAreaBusiness().getLccvp611().getDati().setWp61DtIniEff(0);
        ws.getAreaBusiness().getLccvp611().getDati().setWp61DtEndEff(0);
        ws.getAreaBusiness().getLccvp611().getDati().setWp61CodProd("");
        ws.getAreaBusiness().getLccvp611().getDati().setWp61DtDecor(0);
        ws.getAreaBusiness().getLccvp611().getDati().setWp61DsRiga(0);
        ws.getAreaBusiness().getLccvp611().getDati().setWp61DsOperSql(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvp611().getDati().setWp61DsVer(0);
        ws.getAreaBusiness().getLccvp611().getDati().setWp61DsTsIniCptz(0);
        ws.getAreaBusiness().getLccvp611().getDati().setWp61DsTsEndCptz(0);
        ws.getAreaBusiness().getLccvp611().getDati().setWp61DsUtente("");
        ws.getAreaBusiness().getLccvp611().getDati().setWp61DsStatoElab(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvp611().getDati().getWp61RisMat31122011().setWp61RisMat31122011(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvp611().getDati().getWp61PreV31122011().setWp61PreV31122011(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvp611().getDati().getWp61PreRshV31122011().setWp61PreRshV31122011(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvp611().getDati().getWp61CptRivto31122011().setWp61CptRivto31122011(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvp611().getDati().getWp61ImpbVis31122011().setWp61ImpbVis31122011(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvp611().getDati().getWp61ImpbIs31122011().setWp61ImpbIs31122011(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvp611().getDati().getWp61ImpbVisRpP2011().setWp61ImpbVisRpP2011(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvp611().getDati().getWp61ImpbIsRpP2011().setWp61ImpbIsRpP2011(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvp611().getDati().getWp61PreV30062014().setWp61PreV30062014(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvp611().getDati().getWp61PreRshV30062014().setWp61PreRshV30062014(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvp611().getDati().getWp61CptIni30062014().setWp61CptIni30062014(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvp611().getDati().getWp61ImpbVis30062014().setWp61ImpbVis30062014(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvp611().getDati().getWp61ImpbIs30062014().setWp61ImpbIs30062014(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvp611().getDati().getWp61ImpbVisRpP62014().setWp61ImpbVisRpP62014(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvp611().getDati().getWp61ImpbIsRpP62014().setWp61ImpbIsRpP62014(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvp611().getDati().getWp61RisMat30062014().setWp61RisMat30062014(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvp611().getDati().getWp61IdAdes().setWp61IdAdes(0);
        ws.getAreaBusiness().getLccvp611().getDati().getWp61MontLrdEnd2000().setWp61MontLrdEnd2000(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvp611().getDati().getWp61PreLrdEnd2000().setWp61PreLrdEnd2000(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvp611().getDati().getWp61RendtoLrdEnd2000().setWp61RendtoLrdEnd2000(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvp611().getDati().getWp61MontLrdEnd2006().setWp61MontLrdEnd2006(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvp611().getDati().getWp61PreLrdEnd2006().setWp61PreLrdEnd2006(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvp611().getDati().getWp61RendtoLrdEnd2006().setWp61RendtoLrdEnd2006(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvp611().getDati().getWp61MontLrdDal2007().setWp61MontLrdDal2007(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvp611().getDati().getWp61PreLrdDal2007().setWp61PreLrdDal2007(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvp611().getDati().getWp61RendtoLrdDal2007().setWp61RendtoLrdDal2007(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvp611().getDati().getWp61IdTrchDiGar().setWp61IdTrchDiGar(0);
    }

    public void initWp67AreaEstPoliCpiPr() {
        ws.getAreaBusiness().setWp67EleMaxEstPoliCpiPr(((short)0));
        ws.getAreaBusiness().getLccvp671().getStatus().setStatus(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvp671().setIdPtf(0);
        ws.getAreaBusiness().getLccvp671().getDati().setWp67IdEstPoliCpiPr(0);
        ws.getAreaBusiness().getLccvp671().getDati().setWp67IdMoviCrz(0);
        ws.getAreaBusiness().getLccvp671().getDati().getWp67IdMoviChiu().setWp67IdMoviChiu(0);
        ws.getAreaBusiness().getLccvp671().getDati().setWp67DtIniEff(0);
        ws.getAreaBusiness().getLccvp671().getDati().setWp67DtEndEff(0);
        ws.getAreaBusiness().getLccvp671().getDati().setWp67CodCompAnia(0);
        ws.getAreaBusiness().getLccvp671().getDati().setWp67IbOgg("");
        ws.getAreaBusiness().getLccvp671().getDati().setWp67DsRiga(0);
        ws.getAreaBusiness().getLccvp671().getDati().setWp67DsOperSql(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvp671().getDati().setWp67DsVer(0);
        ws.getAreaBusiness().getLccvp671().getDati().setWp67DsTsIniCptz(0);
        ws.getAreaBusiness().getLccvp671().getDati().setWp67DsTsEndCptz(0);
        ws.getAreaBusiness().getLccvp671().getDati().setWp67DsUtente("");
        ws.getAreaBusiness().getLccvp671().getDati().setWp67DsStatoElab(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvp671().getDati().setWp67CodProdEstno("");
        ws.getAreaBusiness().getLccvp671().getDati().getWp67CptFin().setWp67CptFin(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvp671().getDati().getWp67NumTstFin().setWp67NumTstFin(0);
        ws.getAreaBusiness().getLccvp671().getDati().getWp67TsFinanz().setWp67TsFinanz(new AfDecimal(0, 14, 9));
        ws.getAreaBusiness().getLccvp671().getDati().getWp67DurMmFinanz().setWp67DurMmFinanz(0);
        ws.getAreaBusiness().getLccvp671().getDati().getWp67DtEndFinanz().setWp67DtEndFinanz(0);
        ws.getAreaBusiness().getLccvp671().getDati().getWp67Amm1aRat().setWp67Amm1aRat(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvp671().getDati().getWp67ValRiscBene().setWp67ValRiscBene(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvp671().getDati().getWp67AmmRatEnd().setWp67AmmRatEnd(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvp671().getDati().getWp67TsCreRatFinanz().setWp67TsCreRatFinanz(new AfDecimal(0, 14, 9));
        ws.getAreaBusiness().getLccvp671().getDati().getWp67ImpFinRevolving().setWp67ImpFinRevolving(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvp671().getDati().getWp67ImpUtilCRev().setWp67ImpUtilCRev(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvp671().getDati().getWp67ImpRatRevolving().setWp67ImpRatRevolving(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvp671().getDati().getWp67Dt1oUtlzCRev().setWp67Dt1oUtlzCRev(0);
        ws.getAreaBusiness().getLccvp671().getDati().getWp67ImpAssto().setWp67ImpAssto(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvp671().getDati().getWp67PreVers().setWp67PreVers(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvp671().getDati().getWp67DtScadCop().setWp67DtScadCop(0);
        ws.getAreaBusiness().getLccvp671().getDati().getWp67GgDelMmScadRat().setWp67GgDelMmScadRat(((short)0));
        ws.getAreaBusiness().getLccvp671().getDati().setWp67FlPreFin(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvp671().getDati().getWp67DtScad1aRat().setWp67DtScad1aRat(0);
        ws.getAreaBusiness().getLccvp671().getDati().getWp67DtErogFinanz().setWp67DtErogFinanz(0);
        ws.getAreaBusiness().getLccvp671().getDati().getWp67DtStipulaFinanz().setWp67DtStipulaFinanz(0);
        ws.getAreaBusiness().getLccvp671().getDati().getWp67MmPreamm().setWp67MmPreamm(0);
        ws.getAreaBusiness().getLccvp671().getDati().getWp67ImpDebRes().setWp67ImpDebRes(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvp671().getDati().getWp67ImpRatFinanz().setWp67ImpRatFinanz(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvp671().getDati().getWp67ImpCanoneAntic().setWp67ImpCanoneAntic(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvp671().getDati().getWp67PerRatFinanz().setWp67PerRatFinanz(0);
        ws.getAreaBusiness().getLccvp671().getDati().setWp67TpModPagRat("");
        ws.getAreaBusiness().getLccvp671().getDati().setWp67TpFinanzEr("");
        ws.getAreaBusiness().getLccvp671().getDati().setWp67CatFinanzEr("");
        ws.getAreaBusiness().getLccvp671().getDati().getWp67ValRiscEndLeas().setWp67ValRiscEndLeas(new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getLccvp671().getDati().getWp67DtEstFinanz().setWp67DtEstFinanz(0);
        ws.getAreaBusiness().getLccvp671().getDati().getWp67DtManCop().setWp67DtManCop(0);
        ws.getAreaBusiness().getLccvp671().getDati().setWp67NumFinanz("");
        ws.getAreaBusiness().getLccvp671().getDati().setWp67TpModAcqs("");
    }

    public void initWp01AreaRichEst() {
        ws.getAreaBusiness().setWp01EleMaxRichEst(((short)0));
        ws.getAreaBusiness().getLccvp011().getStatus().setStatus(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvp011().setIdPtf(0);
        ws.getAreaBusiness().getLccvp011().getDati().setWp01IdRichEst(0);
        ws.getAreaBusiness().getLccvp011().getDati().getWp01IdRichEstCollg().setWp01IdRichEstCollg(0);
        ws.getAreaBusiness().getLccvp011().getDati().getWp01IdLiq().setWp01IdLiq(0);
        ws.getAreaBusiness().getLccvp011().getDati().setWp01CodCompAnia(0);
        ws.getAreaBusiness().getLccvp011().getDati().setWp01IbRichEst("");
        ws.getAreaBusiness().getLccvp011().getDati().setWp01TpMovi(0);
        ws.getAreaBusiness().getLccvp011().getDati().setWp01DtFormRich(0);
        ws.getAreaBusiness().getLccvp011().getDati().setWp01DtInvioRich(0);
        ws.getAreaBusiness().getLccvp011().getDati().setWp01DtPervRich(0);
        ws.getAreaBusiness().getLccvp011().getDati().setWp01DtRgstrzRich(0);
        ws.getAreaBusiness().getLccvp011().getDati().getWp01DtEff().setWp01DtEff(0);
        ws.getAreaBusiness().getLccvp011().getDati().getWp01DtSin().setWp01DtSin(0);
        ws.getAreaBusiness().getLccvp011().getDati().setWp01TpOgg("");
        ws.getAreaBusiness().getLccvp011().getDati().setWp01IdOgg(0);
        ws.getAreaBusiness().getLccvp011().getDati().setWp01IbOgg("");
        ws.getAreaBusiness().getLccvp011().getDati().setWp01FlModExec(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvp011().getDati().getWp01IdRichiedente().setWp01IdRichiedente(0);
        ws.getAreaBusiness().getLccvp011().getDati().setWp01CodProd("");
        ws.getAreaBusiness().getLccvp011().getDati().setWp01DsOperSql(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvp011().getDati().setWp01DsVer(0);
        ws.getAreaBusiness().getLccvp011().getDati().setWp01DsTsCptz(0);
        ws.getAreaBusiness().getLccvp011().getDati().setWp01DsUtente("");
        ws.getAreaBusiness().getLccvp011().getDati().setWp01DsStatoElab(Types.SPACE_CHAR);
        ws.getAreaBusiness().getLccvp011().getDati().setWp01CodCan(0);
        ws.getAreaBusiness().getLccvp011().getDati().setWp01IbOggOrig("");
        ws.getAreaBusiness().getLccvp011().getDati().getWp01DtEstFinanz().setWp01DtEstFinanz(0);
        ws.getAreaBusiness().getLccvp011().getDati().getWp01DtManCop().setWp01DtManCop(0);
        ws.getAreaBusiness().getLccvp011().getDati().setWp01FlGestProtezione(Types.SPACE_CHAR);
    }

    public void initDistinctStr() {
        for (int idx0 = 1; idx0 <= Ivvs0211Data.DISTINCT_TAB_INFO_MAXOCCURS; idx0++) {
            ws.getDistinctTabInfo(idx0).setDistinctDtIniValTar(0);
        }
    }

    public void initGar() {
        ws.getGar().setGrzIdGar(0);
        ws.getGar().getGrzIdAdes().setGrzIdAdes(0);
        ws.getGar().setGrzIdPoli(0);
        ws.getGar().setGrzIdMoviCrz(0);
        ws.getGar().getGrzIdMoviChiu().setGrzIdMoviChiu(0);
        ws.getGar().setGrzDtIniEff(0);
        ws.getGar().setGrzDtEndEff(0);
        ws.getGar().setGrzCodCompAnia(0);
        ws.getGar().setGrzIbOgg("");
        ws.getGar().getGrzDtDecor().setGrzDtDecor(0);
        ws.getGar().getGrzDtScad().setGrzDtScad(0);
        ws.getGar().setGrzCodSez("");
        ws.getGar().setGrzCodTari("");
        ws.getGar().setGrzRamoBila("");
        ws.getGar().getGrzDtIniValTar().setGrzDtIniValTar(0);
        ws.getGar().getGrzId1oAssto().setGrzId1oAssto(0);
        ws.getGar().getGrzId2oAssto().setGrzId2oAssto(0);
        ws.getGar().getGrzId3oAssto().setGrzId3oAssto(0);
        ws.getGar().getGrzTpGar().setGrzTpGar(((short)0));
        ws.getGar().setGrzTpRsh("");
        ws.getGar().getGrzTpInvst().setGrzTpInvst(((short)0));
        ws.getGar().setGrzModPagGarcol("");
        ws.getGar().setGrzTpPerPre("");
        ws.getGar().getGrzEtaAa1oAssto().setGrzEtaAa1oAssto(((short)0));
        ws.getGar().getGrzEtaMm1oAssto().setGrzEtaMm1oAssto(((short)0));
        ws.getGar().getGrzEtaAa2oAssto().setGrzEtaAa2oAssto(((short)0));
        ws.getGar().getGrzEtaMm2oAssto().setGrzEtaMm2oAssto(((short)0));
        ws.getGar().getGrzEtaAa3oAssto().setGrzEtaAa3oAssto(((short)0));
        ws.getGar().getGrzEtaMm3oAssto().setGrzEtaMm3oAssto(((short)0));
        ws.getGar().setGrzTpEmisPur(Types.SPACE_CHAR);
        ws.getGar().getGrzEtaAScad().setGrzEtaAScad(0);
        ws.getGar().setGrzTpCalcPrePrstz("");
        ws.getGar().setGrzTpPre(Types.SPACE_CHAR);
        ws.getGar().setGrzTpDur("");
        ws.getGar().getGrzDurAa().setGrzDurAa(0);
        ws.getGar().getGrzDurMm().setGrzDurMm(0);
        ws.getGar().getGrzDurGg().setGrzDurGg(0);
        ws.getGar().getGrzNumAaPagPre().setGrzNumAaPagPre(0);
        ws.getGar().getGrzAaPagPreUni().setGrzAaPagPreUni(0);
        ws.getGar().getGrzMmPagPreUni().setGrzMmPagPreUni(0);
        ws.getGar().getGrzFrazIniErogRen().setGrzFrazIniErogRen(0);
        ws.getGar().getGrzMm1oRat().setGrzMm1oRat(((short)0));
        ws.getGar().getGrzPc1oRat().setGrzPc1oRat(new AfDecimal(0, 6, 3));
        ws.getGar().setGrzTpPrstzAssta("");
        ws.getGar().getGrzDtEndCarz().setGrzDtEndCarz(0);
        ws.getGar().getGrzPcRipPre().setGrzPcRipPre(new AfDecimal(0, 6, 3));
        ws.getGar().setGrzCodFnd("");
        ws.getGar().setGrzAaRenCer("");
        ws.getGar().getGrzPcRevrsb().setGrzPcRevrsb(new AfDecimal(0, 6, 3));
        ws.getGar().setGrzTpPcRip("");
        ws.getGar().getGrzPcOpz().setGrzPcOpz(new AfDecimal(0, 6, 3));
        ws.getGar().setGrzTpIas("");
        ws.getGar().setGrzTpStab("");
        ws.getGar().setGrzTpAdegPre(Types.SPACE_CHAR);
        ws.getGar().getGrzDtVarzTpIas().setGrzDtVarzTpIas(0);
        ws.getGar().getGrzFrazDecrCpt().setGrzFrazDecrCpt(0);
        ws.getGar().setGrzCodTratRiass("");
        ws.getGar().setGrzTpDtEmisRiass("");
        ws.getGar().setGrzTpCessRiass("");
        ws.getGar().setGrzDsRiga(0);
        ws.getGar().setGrzDsOperSql(Types.SPACE_CHAR);
        ws.getGar().setGrzDsVer(0);
        ws.getGar().setGrzDsTsIniCptz(0);
        ws.getGar().setGrzDsTsEndCptz(0);
        ws.getGar().setGrzDsUtente("");
        ws.getGar().setGrzDsStatoElab(Types.SPACE_CHAR);
        ws.getGar().getGrzAaStab().setGrzAaStab(0);
        ws.getGar().getGrzTsStabLimitata().setGrzTsStabLimitata(new AfDecimal(0, 14, 9));
        ws.getGar().getGrzDtPresc().setGrzDtPresc(0);
        ws.getGar().setGrzRshInvst(Types.SPACE_CHAR);
        ws.getGar().setGrzTpRamoBila("");
    }

    public void initTrchDiGar() {
        ws.getTrchDiGar().setTgaIdTrchDiGar(0);
        ws.getTrchDiGar().setTgaIdGar(0);
        ws.getTrchDiGar().setTgaIdAdes(0);
        ws.getTrchDiGar().setTgaIdPoli(0);
        ws.getTrchDiGar().setTgaIdMoviCrz(0);
        ws.getTrchDiGar().getTgaIdMoviChiu().setTgaIdMoviChiu(0);
        ws.getTrchDiGar().setTgaDtIniEff(0);
        ws.getTrchDiGar().setTgaDtEndEff(0);
        ws.getTrchDiGar().setTgaCodCompAnia(0);
        ws.getTrchDiGar().setTgaDtDecor(0);
        ws.getTrchDiGar().getTgaDtScad().setTgaDtScad(0);
        ws.getTrchDiGar().setTgaIbOgg("");
        ws.getTrchDiGar().setTgaTpRgmFisc("");
        ws.getTrchDiGar().getTgaDtEmis().setTgaDtEmis(0);
        ws.getTrchDiGar().setTgaTpTrch("");
        ws.getTrchDiGar().getTgaDurAa().setTgaDurAa(0);
        ws.getTrchDiGar().getTgaDurMm().setTgaDurMm(0);
        ws.getTrchDiGar().getTgaDurGg().setTgaDurGg(0);
        ws.getTrchDiGar().getTgaPreCasoMor().setTgaPreCasoMor(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaPcIntrRiat().setTgaPcIntrRiat(new AfDecimal(0, 6, 3));
        ws.getTrchDiGar().getTgaImpBnsAntic().setTgaImpBnsAntic(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaPreIniNet().setTgaPreIniNet(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaPrePpIni().setTgaPrePpIni(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaPrePpUlt().setTgaPrePpUlt(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaPreTariIni().setTgaPreTariIni(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaPreTariUlt().setTgaPreTariUlt(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaPreInvrioIni().setTgaPreInvrioIni(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaPreInvrioUlt().setTgaPreInvrioUlt(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaPreRivto().setTgaPreRivto(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaImpSoprProf().setTgaImpSoprProf(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaImpSoprSan().setTgaImpSoprSan(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaImpSoprSpo().setTgaImpSoprSpo(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaImpSoprTec().setTgaImpSoprTec(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaImpAltSopr().setTgaImpAltSopr(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaPreStab().setTgaPreStab(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaDtEffStab().setTgaDtEffStab(0);
        ws.getTrchDiGar().getTgaTsRivalFis().setTgaTsRivalFis(new AfDecimal(0, 14, 9));
        ws.getTrchDiGar().getTgaTsRivalIndiciz().setTgaTsRivalIndiciz(new AfDecimal(0, 14, 9));
        ws.getTrchDiGar().getTgaOldTsTec().setTgaOldTsTec(new AfDecimal(0, 14, 9));
        ws.getTrchDiGar().getTgaRatLrd().setTgaRatLrd(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaPreLrd().setTgaPreLrd(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaPrstzIni().setTgaPrstzIni(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaPrstzUlt().setTgaPrstzUlt(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaCptInOpzRivto().setTgaCptInOpzRivto(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaPrstzIniStab().setTgaPrstzIniStab(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaCptRshMor().setTgaCptRshMor(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaPrstzRidIni().setTgaPrstzRidIni(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().setTgaFlCarCont(Types.SPACE_CHAR);
        ws.getTrchDiGar().getTgaBnsGiaLiqto().setTgaBnsGiaLiqto(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaImpBns().setTgaImpBns(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().setTgaCodDvs("");
        ws.getTrchDiGar().getTgaPrstzIniNewfis().setTgaPrstzIniNewfis(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaImpScon().setTgaImpScon(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaAlqScon().setTgaAlqScon(new AfDecimal(0, 6, 3));
        ws.getTrchDiGar().getTgaImpCarAcq().setTgaImpCarAcq(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaImpCarInc().setTgaImpCarInc(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaImpCarGest().setTgaImpCarGest(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaEtaAa1oAssto().setTgaEtaAa1oAssto(((short)0));
        ws.getTrchDiGar().getTgaEtaMm1oAssto().setTgaEtaMm1oAssto(((short)0));
        ws.getTrchDiGar().getTgaEtaAa2oAssto().setTgaEtaAa2oAssto(((short)0));
        ws.getTrchDiGar().getTgaEtaMm2oAssto().setTgaEtaMm2oAssto(((short)0));
        ws.getTrchDiGar().getTgaEtaAa3oAssto().setTgaEtaAa3oAssto(((short)0));
        ws.getTrchDiGar().getTgaEtaMm3oAssto().setTgaEtaMm3oAssto(((short)0));
        ws.getTrchDiGar().getTgaRendtoLrd().setTgaRendtoLrd(new AfDecimal(0, 14, 9));
        ws.getTrchDiGar().getTgaPcRetr().setTgaPcRetr(new AfDecimal(0, 6, 3));
        ws.getTrchDiGar().getTgaRendtoRetr().setTgaRendtoRetr(new AfDecimal(0, 14, 9));
        ws.getTrchDiGar().getTgaMinGarto().setTgaMinGarto(new AfDecimal(0, 14, 9));
        ws.getTrchDiGar().getTgaMinTrnut().setTgaMinTrnut(new AfDecimal(0, 14, 9));
        ws.getTrchDiGar().getTgaPreAttDiTrch().setTgaPreAttDiTrch(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaMatuEnd2000().setTgaMatuEnd2000(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaAbbTotIni().setTgaAbbTotIni(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaAbbTotUlt().setTgaAbbTotUlt(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaAbbAnnuUlt().setTgaAbbAnnuUlt(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaDurAbb().setTgaDurAbb(0);
        ws.getTrchDiGar().setTgaTpAdegAbb(Types.SPACE_CHAR);
        ws.getTrchDiGar().setTgaModCalc("");
        ws.getTrchDiGar().getTgaImpAz().setTgaImpAz(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaImpAder().setTgaImpAder(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaImpTfr().setTgaImpTfr(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaImpVolo().setTgaImpVolo(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaVisEnd2000().setTgaVisEnd2000(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaDtVldtProd().setTgaDtVldtProd(0);
        ws.getTrchDiGar().getTgaDtIniValTar().setTgaDtIniValTar(0);
        ws.getTrchDiGar().getTgaImpbVisEnd2000().setTgaImpbVisEnd2000(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaRenIniTsTec0().setTgaRenIniTsTec0(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaPcRipPre().setTgaPcRipPre(new AfDecimal(0, 6, 3));
        ws.getTrchDiGar().setTgaFlImportiForz(Types.SPACE_CHAR);
        ws.getTrchDiGar().getTgaPrstzIniNforz().setTgaPrstzIniNforz(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaVisEnd2000Nforz().setTgaVisEnd2000Nforz(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaIntrMora().setTgaIntrMora(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaManfeeAntic().setTgaManfeeAntic(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaManfeeRicor().setTgaManfeeRicor(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaPreUniRivto().setTgaPreUniRivto(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaProv1aaAcq().setTgaProv1aaAcq(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaProv2aaAcq().setTgaProv2aaAcq(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaProvRicor().setTgaProvRicor(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaProvInc().setTgaProvInc(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaAlqProvAcq().setTgaAlqProvAcq(new AfDecimal(0, 6, 3));
        ws.getTrchDiGar().getTgaAlqProvInc().setTgaAlqProvInc(new AfDecimal(0, 6, 3));
        ws.getTrchDiGar().getTgaAlqProvRicor().setTgaAlqProvRicor(new AfDecimal(0, 6, 3));
        ws.getTrchDiGar().getTgaImpbProvAcq().setTgaImpbProvAcq(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaImpbProvInc().setTgaImpbProvInc(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaImpbProvRicor().setTgaImpbProvRicor(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().setTgaFlProvForz(Types.SPACE_CHAR);
        ws.getTrchDiGar().getTgaPrstzAggIni().setTgaPrstzAggIni(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaIncrPre().setTgaIncrPre(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaIncrPrstz().setTgaIncrPrstz(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaDtUltAdegPrePr().setTgaDtUltAdegPrePr(0);
        ws.getTrchDiGar().getTgaPrstzAggUlt().setTgaPrstzAggUlt(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaTsRivalNet().setTgaTsRivalNet(new AfDecimal(0, 14, 9));
        ws.getTrchDiGar().getTgaPrePattuito().setTgaPrePattuito(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().setTgaTpRival("");
        ws.getTrchDiGar().getTgaRisMat().setTgaRisMat(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaCptMinScad().setTgaCptMinScad(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaCommisGest().setTgaCommisGest(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().setTgaTpManfeeAppl("");
        ws.getTrchDiGar().setTgaDsRiga(0);
        ws.getTrchDiGar().setTgaDsOperSql(Types.SPACE_CHAR);
        ws.getTrchDiGar().setTgaDsVer(0);
        ws.getTrchDiGar().setTgaDsTsIniCptz(0);
        ws.getTrchDiGar().setTgaDsTsEndCptz(0);
        ws.getTrchDiGar().setTgaDsUtente("");
        ws.getTrchDiGar().setTgaDsStatoElab(Types.SPACE_CHAR);
        ws.getTrchDiGar().getTgaPcCommisGest().setTgaPcCommisGest(new AfDecimal(0, 6, 3));
        ws.getTrchDiGar().getTgaNumGgRival().setTgaNumGgRival(0);
        ws.getTrchDiGar().getTgaImpTrasfe().setTgaImpTrasfe(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaImpTfrStrc().setTgaImpTfrStrc(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaAcqExp().setTgaAcqExp(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaRemunAss().setTgaRemunAss(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaCommisInter().setTgaCommisInter(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaAlqRemunAss().setTgaAlqRemunAss(new AfDecimal(0, 6, 3));
        ws.getTrchDiGar().getTgaAlqCommisInter().setTgaAlqCommisInter(new AfDecimal(0, 6, 3));
        ws.getTrchDiGar().getTgaImpbRemunAss().setTgaImpbRemunAss(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaImpbCommisInter().setTgaImpbCommisInter(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaCosRunAssva().setTgaCosRunAssva(new AfDecimal(0, 15, 3));
        ws.getTrchDiGar().getTgaCosRunAssvaIdc().setTgaCosRunAssvaIdc(new AfDecimal(0, 15, 3));
    }

    public void initMatrValVar() {
        ws.getMatrValVar().setMvvIdMatrValVar(0);
        ws.getMatrValVar().getMvvIdpMatrValVar().setMvvIdpMatrValVar(0);
        ws.getMatrValVar().setMvvCodCompagniaAnia(0);
        ws.getMatrValVar().getMvvTipoMovimento().setMvvTipoMovimento(0);
        ws.getMatrValVar().setMvvCodDatoExt("");
        ws.getMatrValVar().setMvvObbligatorieta(Types.SPACE_CHAR);
        ws.getMatrValVar().setMvvValoreDefault("");
        ws.getMatrValVar().setMvvCodStrDatoPtf("");
        ws.getMatrValVar().setMvvCodDatoPtf("");
        ws.getMatrValVar().setMvvCodParametro("");
        ws.getMatrValVar().setMvvOperazione("");
        ws.getMatrValVar().setMvvLivelloOperazione("");
        ws.getMatrValVar().setMvvTipoOggetto("");
        ws.getMatrValVar().setMvvWhereConditionLen(((short)0));
        ws.getMatrValVar().setMvvWhereCondition("");
        ws.getMatrValVar().setMvvServizioLettura("");
        ws.getMatrValVar().setMvvModuloCalcolo("");
        ws.getMatrValVar().setMvvStepValorizzatore(Types.SPACE_CHAR);
        ws.getMatrValVar().setMvvStepConversazione(Types.SPACE_CHAR);
        ws.getMatrValVar().setMvvOperLogStatoBus("");
        ws.getMatrValVar().setMvvArrayStatoBus("");
        ws.getMatrValVar().setMvvOperLogCausale("");
        ws.getMatrValVar().setMvvArrayCausale("");
        ws.getMatrValVar().setMvvCodDatoInterno("");
    }

    public void initVarFunzDiCalc() {
        ws.getVarFunzDiCalc().setAc5Idcomp(((short)0));
        ws.getVarFunzDiCalc().setAc5CodprodLen(((short)0));
        ws.getVarFunzDiCalc().setAc5Codprod("");
        ws.getVarFunzDiCalc().setAc5CodtariLen(((short)0));
        ws.getVarFunzDiCalc().setAc5Codtari("");
        ws.getVarFunzDiCalc().setAc5Diniz(0);
        ws.getVarFunzDiCalc().setAc5NomefunzLen(((short)0));
        ws.getVarFunzDiCalc().setAc5Nomefunz("");
        ws.getVarFunzDiCalc().setAc5Dend(0);
        ws.getVarFunzDiCalc().getAc5Isprecalc().setAc5Isprecalc(((short)0));
        ws.getVarFunzDiCalc().setAc5NomeprogrLen(((short)0));
        ws.getVarFunzDiCalc().setAc5Nomeprogr("");
        ws.getVarFunzDiCalc().setAc5ModcalcLen(((short)0));
        ws.getVarFunzDiCalc().setAc5Modcalc("");
        ws.getVarFunzDiCalc().setAc5GlovarlistLen(((short)0));
        ws.getVarFunzDiCalc().setAc5Glovarlist("");
        ws.getVarFunzDiCalc().setAc5StepElabLen(((short)0));
        ws.getVarFunzDiCalc().setAc5StepElab("");
    }

    public void initIvvc0215() {
        ws.getIvvc0215().setEleMaxNotFound(((short)0));
        for (int idx0 = 1; idx0 <= Ivvc0215.TAB_VAR_NOT_FOUND_MAXOCCURS; idx0++) {
            ws.getIvvc0215().getTabVarNotFound(idx0).setFound("");
            ws.getIvvc0215().getTabVarNotFound(idx0).setFoundSp(Types.SPACE_CHAR);
        }
        ws.getIvvc0215().setEleMaxCalcKo(((short)0));
        for (int idx0 = 1; idx0 <= Ivvc0215.TAB_CALCOLO_KO_MAXOCCURS; idx0++) {
            ws.getIvvc0215().getTabCalcoloKo(idx0).setCalcoloKo("");
            ws.getIvvc0215().getTabCalcoloKo(idx0).setCalcSp(Types.SPACE_CHAR);
        }
        ws.getIvvc0215().getErroreValorizza().setErroreValorizza("");
    }

    public void initLccv0021() {
        ws.getLccv0021().setPgm("");
        ws.getLccv0021().setTpMovPtfFormatted("00000");
        ws.getLccv0021().getAreaOutput().setTpMovActFormatted("00000");
        ws.getLccv0021().getAreaOutput().setCodProcessoWf("");
        ws.getLccv0021().getAreaOutput().getReturnCode().setReturnCode("");
        ws.getLccv0021().getAreaOutput().getSqlcode().setSqlcodeSigned(0);
        ws.getLccv0021().getAreaOutput().setDescrizErr("");
        ws.getLccv0021().getAreaOutput().setCodServizioBe("");
        ws.getLccv0021().getAreaOutput().setNomeTabella("");
        ws.getLccv0021().getAreaOutput().setKeyTabella("");
    }

    public void initIvvc0222AreaFndXTranche() {
        ws.getAreaBusiness().setIvvc0222EleTrchMax(((short)0));
        ws.getAreaBusiness().getIvvc0222TabTranObj().fill(new Ivvc0222TabTran().initIvvc0222TabTran());
    }

    public void initAreaIvvc0223() {
        ws.getAreaBusiness().getIvvc0223().setEleMaxAreaGar(((short)0));
        for (int idx0 = 1; idx0 <= Ivvc0223.TAB_LIVELLI_GAR_MAXOCCURS; idx0++) {
            ws.getAreaBusiness().getIvvc0223().getTabLivelliGar(idx0).setIdGaranziaFormatted("000000000");
            ws.getAreaBusiness().getIvvc0223().getTabLivelliGar(idx0).setCodTari("");
            ws.getAreaBusiness().getIvvc0223().getTabLivelliGar(idx0).setEleMaxVarGar(((short)0));
            for (int idx1 = 1; idx1 <= Ivvc0223TabLivelliGar.TAB_VAR_GAR_MAXOCCURS; idx1++) {
                ws.getAreaBusiness().getIvvc0223().getTabLivelliGar(idx0).getTabVarGar(idx1).setCodVariabile("");
                ws.getAreaBusiness().getIvvc0223().getTabLivelliGar(idx0).getTabVarGar(idx1).setTpDato(Types.SPACE_CHAR);
                ws.getAreaBusiness().getIvvc0223().getTabLivelliGar(idx0).getTabVarGar(idx1).setValImp(new AfDecimal(0, 18, 7));
                ws.getAreaBusiness().getIvvc0223().getTabLivelliGar(idx0).getTabVarGar(idx1).setValPerc(new AfDecimal(0, 14, 9));
                ws.getAreaBusiness().getIvvc0223().getTabLivelliGar(idx0).getTabVarGar(idx1).setValStr("");
            }
        }
    }

    public void initAreaCommisGestVv() {
        ws.getAreaBusiness().getIvvc0224().setEleMaxCdgVvFormatted("0000");
        for (int idx0 = 1; idx0 <= Ivvc0224.FND_X_CDG_VV_MAXOCCURS; idx0++) {
            ws.getAreaBusiness().getIvvc0224().getFndXCdgVv(idx0).setCodFndVv("");
            ws.getAreaBusiness().getIvvc0224().getFndXCdgVv(idx0).setCdgRestVv(new AfDecimal(0, 18, 7));
        }
    }

    public void initTabDettQuest() {
        ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setStatus(1, Types.SPACE_CHAR);
        ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setIdPtf(1, 0);
        ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setIdDettQuest(1, 0);
        ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setIdQuest(1, 0);
        ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setIdMoviCrz(1, 0);
        ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setIdMoviChiu(1, 0);
        ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setDtIniEff(1, 0);
        ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setDtEndEff(1, 0);
        ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setCodCompAnia(1, 0);
        ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setCodQuest(1, "");
        ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setCodDom(1, "");
        ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setCodDomCollg(1, "");
        ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setRispNum(1, 0);
        ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setRispFl(1, Types.SPACE_CHAR);
        ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setRispTxt(1, "");
        ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setRispTs(1, new AfDecimal(0, 14, 9));
        ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setRispImp(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setRispPc(1, new AfDecimal(0, 6, 3));
        ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setRispDt(1, 0);
        ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setRispKey(1, "");
        ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setTpRisp(1, "");
        ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setIdCompQuest(1, 0);
        ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setValRisp(1, "");
        ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setDsRiga(1, 0);
        ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setDsOperSql(1, Types.SPACE_CHAR);
        ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setDsVer(1, 0);
        ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setDsTsIniCptz(1, 0);
        ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setDsTsEndCptz(1, 0);
        ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setDsUtente(1, "");
        ws.getAreaBusiness().getWdeqAreaDettQuest().getWdeqTab().setDsStatoElab(1, Types.SPACE_CHAR);
    }

    public void initTabParamOgg() {
        ws.getAreaBusiness().getWpogAreaParamOgg().getWpogTab().setStatus(1, Types.SPACE_CHAR);
        ws.getAreaBusiness().getWpogAreaParamOgg().getWpogTab().setIdPtf(1, 0);
        ws.getAreaBusiness().getWpogAreaParamOgg().getWpogTab().setIdParamOgg(1, 0);
        ws.getAreaBusiness().getWpogAreaParamOgg().getWpogTab().setIdOgg(1, 0);
        ws.getAreaBusiness().getWpogAreaParamOgg().getWpogTab().setTpOgg(1, "");
        ws.getAreaBusiness().getWpogAreaParamOgg().getWpogTab().setIdMoviCrz(1, 0);
        ws.getAreaBusiness().getWpogAreaParamOgg().getWpogTab().setIdMoviChiu(1, 0);
        ws.getAreaBusiness().getWpogAreaParamOgg().getWpogTab().setDtIniEff(1, 0);
        ws.getAreaBusiness().getWpogAreaParamOgg().getWpogTab().setDtEndEff(1, 0);
        ws.getAreaBusiness().getWpogAreaParamOgg().getWpogTab().setCodCompAnia(1, 0);
        ws.getAreaBusiness().getWpogAreaParamOgg().getWpogTab().setCodParam(1, "");
        ws.getAreaBusiness().getWpogAreaParamOgg().getWpogTab().setTpParam(1, Types.SPACE_CHAR);
        ws.getAreaBusiness().getWpogAreaParamOgg().getWpogTab().setTpD(1, "");
        ws.getAreaBusiness().getWpogAreaParamOgg().getWpogTab().setValImp(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWpogAreaParamOgg().getWpogTab().setValDt(1, 0);
        ws.getAreaBusiness().getWpogAreaParamOgg().getWpogTab().setValTs(1, new AfDecimal(0, 14, 9));
        ws.getAreaBusiness().getWpogAreaParamOgg().getWpogTab().setValTxt(1, "");
        ws.getAreaBusiness().getWpogAreaParamOgg().getWpogTab().setValFl(1, Types.SPACE_CHAR);
        ws.getAreaBusiness().getWpogAreaParamOgg().getWpogTab().setValNum(1, 0);
        ws.getAreaBusiness().getWpogAreaParamOgg().getWpogTab().setValPc(1, new AfDecimal(0, 14, 9));
        ws.getAreaBusiness().getWpogAreaParamOgg().getWpogTab().setDsRiga(1, 0);
        ws.getAreaBusiness().getWpogAreaParamOgg().getWpogTab().setDsOperSql(1, Types.SPACE_CHAR);
        ws.getAreaBusiness().getWpogAreaParamOgg().getWpogTab().setDsVer(1, 0);
        ws.getAreaBusiness().getWpogAreaParamOgg().getWpogTab().setDsTsIniCptz(1, 0);
        ws.getAreaBusiness().getWpogAreaParamOgg().getWpogTab().setDsTsEndCptz(1, 0);
        ws.getAreaBusiness().getWpogAreaParamOgg().getWpogTab().setDsUtente(1, "");
        ws.getAreaBusiness().getWpogAreaParamOgg().getWpogTab().setDsStatoElab(1, Types.SPACE_CHAR);
    }

    public void initTabParamMov() {
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setStatus(1, Types.SPACE_CHAR);
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setIdPtf(1, 0);
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setIdParamMovi(1, 0);
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setIdOgg(1, 0);
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setTpOgg(1, "");
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setIdMoviCrz(1, 0);
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setIdMoviChiu(1, 0);
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setDtIniEff(1, 0);
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setDtEndEff(1, 0);
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setCodCompAnia(1, 0);
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setTpMovi(1, 0);
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setFrqMovi(1, 0);
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setDurAa(1, 0);
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setDurMm(1, 0);
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setDurGg(1, 0);
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setDtRicorPrec(1, 0);
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setDtRicorSucc(1, 0);
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setPcIntrFraz(1, new AfDecimal(0, 6, 3));
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setImpBnsDaScoTot(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setImpBnsDaSco(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setPcAnticBns(1, new AfDecimal(0, 6, 3));
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setTpRinnColl(1, "");
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setTpRivalPre(1, "");
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setTpRivalPrstz(1, "");
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setFlEvidRival(1, Types.SPACE_CHAR);
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setUltPcPerd(1, new AfDecimal(0, 6, 3));
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setTotAaGiaPror(1, 0);
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setTpOpz(1, "");
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setAaRenCer(1, 0);
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setPcRevrsb(1, new AfDecimal(0, 6, 3));
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setImpRiscParzPrgt(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setImpLrdDiRat(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setIbOgg(1, "");
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setCosOner(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setSpePc(1, new AfDecimal(0, 6, 3));
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setFlAttivGar(1, Types.SPACE_CHAR);
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setCambioVerProd(1, Types.SPACE_CHAR);
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setMmDiff(1, ((short)0));
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setImpRatManfee(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setDtUltErogManfee(1, 0);
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setTpOggRival(1, "");
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setSomAsstaGarac(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setPcApplzOpz(1, new AfDecimal(0, 6, 3));
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setIdAdes(1, 0);
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setIdPoli(1, 0);
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setTpFrmAssva(1, "");
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setDsRiga(1, 0);
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setDsOperSql(1, Types.SPACE_CHAR);
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setDsVer(1, 0);
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setDsTsIniCptz(1, 0);
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setDsTsEndCptz(1, 0);
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setDsUtente(1, "");
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setDsStatoElab(1, Types.SPACE_CHAR);
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setTpEstrCnt(1, "");
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setCodRamo(1, "");
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setGenDaSin(1, Types.SPACE_CHAR);
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setCodTari(1, "");
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setNumRatPagPre(1, 0);
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setPcServVal(1, new AfDecimal(0, 6, 3));
        ws.getAreaBusiness().getWpmoAreaParamMov().getWpmoTab().setEtaAaSoglBnficr(1, ((short)0));
    }

    public void initTabTran() {
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setStatus(1, Types.SPACE_CHAR);
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setIdPtf(1, 0);
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setIdTrchDiGar(1, 0);
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setIdGar(1, 0);
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setIdAdes(1, 0);
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setIdPoli(1, 0);
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setIdMoviCrz(1, 0);
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setIdMoviChiu(1, 0);
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setDtIniEff(1, 0);
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setDtEndEff(1, 0);
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setCodCompAnia(1, 0);
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setDtDecor(1, 0);
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setDtScad(1, 0);
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setIbOgg(1, "");
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setTpRgmFisc(1, "");
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setDtEmis(1, 0);
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setTpTrch(1, "");
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setDurAa(1, 0);
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setDurMm(1, 0);
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setDurGg(1, 0);
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setPreCasoMor(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setPcIntrRiat(1, new AfDecimal(0, 6, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setImpBnsAntic(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setPreIniNet(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setPrePpIni(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setPrePpUlt(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setPreTariIni(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setPreTariUlt(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setPreInvrioIni(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setPreInvrioUlt(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setPreRivto(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setImpSoprProf(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setImpSoprSan(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setImpSoprSpo(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setImpSoprTec(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setImpAltSopr(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setPreStab(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setDtEffStab(1, 0);
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setTsRivalFis(1, new AfDecimal(0, 14, 9));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setTsRivalIndiciz(1, new AfDecimal(0, 14, 9));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setOldTsTec(1, new AfDecimal(0, 14, 9));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setRatLrd(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setPreLrd(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setPrstzIni(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setPrstzUlt(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setCptInOpzRivto(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setPrstzIniStab(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setCptRshMor(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setPrstzRidIni(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setFlCarCont(1, Types.SPACE_CHAR);
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setBnsGiaLiqto(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setImpBns(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setCodDvs(1, "");
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setPrstzIniNewfis(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setImpScon(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setAlqScon(1, new AfDecimal(0, 6, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setImpCarAcq(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setImpCarInc(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setImpCarGest(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setEtaAa1oAssto(1, ((short)0));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setEtaMm1oAssto(1, ((short)0));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setEtaAa2oAssto(1, ((short)0));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setEtaMm2oAssto(1, ((short)0));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setEtaAa3oAssto(1, ((short)0));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setEtaMm3oAssto(1, ((short)0));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setRendtoLrd(1, new AfDecimal(0, 14, 9));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setPcRetr(1, new AfDecimal(0, 6, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setRendtoRetr(1, new AfDecimal(0, 14, 9));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setMinGarto(1, new AfDecimal(0, 14, 9));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setMinTrnut(1, new AfDecimal(0, 14, 9));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setPreAttDiTrch(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setMatuEnd2000(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setAbbTotIni(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setAbbTotUlt(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setAbbAnnuUlt(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setDurAbb(1, 0);
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setTpAdegAbb(1, Types.SPACE_CHAR);
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setModCalc(1, "");
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setImpAz(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setImpAder(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setImpTfr(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setImpVolo(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setVisEnd2000(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setDtVldtProd(1, 0);
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setDtIniValTar(1, 0);
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setImpbVisEnd2000(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setRenIniTsTec0(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setPcRipPre(1, new AfDecimal(0, 6, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setFlImportiForz(1, Types.SPACE_CHAR);
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setPrstzIniNforz(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setVisEnd2000Nforz(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setIntrMora(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setManfeeAntic(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setManfeeRicor(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setPreUniRivto(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setProv1aaAcq(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setProv2aaAcq(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setProvRicor(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setProvInc(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setAlqProvAcq(1, new AfDecimal(0, 6, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setAlqProvInc(1, new AfDecimal(0, 6, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setAlqProvRicor(1, new AfDecimal(0, 6, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setImpbProvAcq(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setImpbProvInc(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setImpbProvRicor(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setFlProvForz(1, Types.SPACE_CHAR);
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setPrstzAggIni(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setIncrPre(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setIncrPrstz(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setDtUltAdegPrePr(1, 0);
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setPrstzAggUlt(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setTsRivalNet(1, new AfDecimal(0, 14, 9));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setPrePattuito(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setTpRival(1, "");
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setRisMat(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setCptMinScad(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setCommisGest(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setTpManfeeAppl(1, "");
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setDsRiga(1, 0);
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setDsOperSql(1, Types.SPACE_CHAR);
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setDsVer(1, 0);
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setDsTsIniCptz(1, 0);
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setDsTsEndCptz(1, 0);
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setDsUtente(1, "");
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setDsStatoElab(1, Types.SPACE_CHAR);
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setPcCommisGest(1, new AfDecimal(0, 6, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setNumGgRival(1, 0);
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setImpTrasfe(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setImpTfrStrc(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setAcqExp(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setRemunAss(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setCommisInter(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setAlqRemunAss(1, new AfDecimal(0, 6, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setAlqCommisInter(1, new AfDecimal(0, 6, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setImpbRemunAss(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setImpbCommisInter(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setCosRunAssva(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtgaAreaTranche().getWtgaTab().setCosRunAssvaIdc(1, new AfDecimal(0, 15, 3));
    }

    public void initTabTrchLiq() {
        ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setStatus(1, Types.SPACE_CHAR);
        ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setIdPtf(1, 0);
        ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setIdTrchLiq(1, 0);
        ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setIdGarLiq(1, 0);
        ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setIdLiq(1, 0);
        ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setIdTrchDiGar(1, 0);
        ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setIdMoviCrz(1, 0);
        ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setIdMoviChiu(1, 0);
        ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setDtIniEff(1, 0);
        ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setDtEndEff(1, 0);
        ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setCodCompAnia(1, 0);
        ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setImpLrdCalc(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setImpLrdDfz(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setImpLrdEfflq(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setImpNetCalc(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setImpNetDfz(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setImpNetEfflq(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setImpUti(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setCodTari(1, "");
        ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setCodDvs(1, "");
        ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setIasOnerPrvntFin(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setIasMggSin(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setIasRstDpst(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setImpRimb(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setComponTaxRimb(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setRisMat(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setRisSpe(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setDsRiga(1, 0);
        ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setDsOperSql(1, Types.SPACE_CHAR);
        ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setDsVer(1, 0);
        ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setDsTsIniCptz(1, 0);
        ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setDsTsEndCptz(1, 0);
        ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setDsUtente(1, "");
        ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setDsStatoElab(1, Types.SPACE_CHAR);
        ws.getAreaBusiness().getWtliAreaTrchLiq().getWtliTab().setIasPnl(1, new AfDecimal(0, 15, 3));
    }

    public void initTabOggColl() {
        ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setStatus(1, Types.SPACE_CHAR);
        ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setIdPtf(1, 0);
        ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setIdOggCollg(1, 0);
        ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setIdOggCoinv(1, 0);
        ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setTpOggCoinv(1, "");
        ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setIdOggDer(1, 0);
        ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setTpOggDer(1, "");
        ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setIbRiftoEstno(1, "");
        ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setIdMoviCrz(1, 0);
        ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setIdMoviChiu(1, 0);
        ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setDtIniEff(1, 0);
        ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setDtEndEff(1, 0);
        ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setCodCompAnia(1, 0);
        ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setCodProd(1, "");
        ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setDtScad(1, 0);
        ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setTpCollgm(1, "");
        ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setTpTrasf(1, "");
        ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setRecProv(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setDtDecor(1, 0);
        ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setDtUltPrePag(1, 0);
        ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setTotPre(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setRisMat(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setRisZil(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setImpTrasf(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setImpReinvst(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setPcReinvstRilievi(1, new AfDecimal(0, 6, 3));
        ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setIb2oRiftoEstno(1, "");
        ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setIndLiqAggMan(1, Types.SPACE_CHAR);
        ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setDsRiga(1, 0);
        ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setDsOperSql(1, Types.SPACE_CHAR);
        ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setDsVer(1, 0);
        ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setDsTsIniCptz(1, 0);
        ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setDsTsEndCptz(1, 0);
        ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setDsUtente(1, "");
        ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setDsStatoElab(1, Types.SPACE_CHAR);
        ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setImpCollg(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setPrePerTrasf(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setCarAcq(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setPre1aAnnualita(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setImpTrasferito(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setTpModAbbinamento(1, "");
        ws.getAreaBusiness().getWocoAreaOggColl().getWocoTab().setPcPreTrasferito(1, new AfDecimal(0, 6, 3));
    }

    public void initTabRicDisinv() {
        ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setStatus(1, Types.SPACE_CHAR);
        ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setIdPtf(1, 0);
        ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setIdRichDisFnd(1, 0);
        ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setIdMoviFinrio(1, 0);
        ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setIdMoviCrz(1, 0);
        ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setIdMoviChiu(1, 0);
        ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setDtIniEff(1, 0);
        ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setDtEndEff(1, 0);
        ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setCodCompAnia(1, 0);
        ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setCodFnd(1, "");
        ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setNumQuo(1, new AfDecimal(0, 12, 5));
        ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setPc(1, new AfDecimal(0, 6, 3));
        ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setImpMovto(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setDtDis(1, 0);
        ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setCodTari(1, "");
        ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setTpStat(1, "");
        ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setTpModDis(1, "");
        ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setCodDiv(1, "");
        ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setDtCambioVlt(1, 0);
        ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setTpFnd(1, Types.SPACE_CHAR);
        ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setDsRiga(1, 0);
        ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setDsOperSql(1, Types.SPACE_CHAR);
        ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setDsVer(1, 0);
        ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setDsTsIniCptz(1, 0);
        ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setDsTsEndCptz(1, 0);
        ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setDsUtente(1, "");
        ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setDsStatoElab(1, Types.SPACE_CHAR);
        ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setDtDisCalc(1, 0);
        ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setFlCalcDis(1, Types.SPACE_CHAR);
        ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setCommisGest(1, new AfDecimal(0, 18, 7));
        ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setNumQuoCdgFnz(1, new AfDecimal(0, 12, 5));
        ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setNumQuoCdgtotFnz(1, new AfDecimal(0, 12, 5));
        ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setCosRunAssvaIdc(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWrdfAreaRichDisinvFnd().getWrdfTabella().setFlSwmBp2s(1, Types.SPACE_CHAR);
    }

    public void initTabRicInv() {
        ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setStatus(1, Types.SPACE_CHAR);
        ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setIdPtf(1, 0);
        ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setIdRichInvstFnd(1, 0);
        ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setIdMoviFinrio(1, 0);
        ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setIdMoviCrz(1, 0);
        ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setIdMoviChiu(1, 0);
        ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setDtIniEff(1, 0);
        ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setDtEndEff(1, 0);
        ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setCodCompAnia(1, 0);
        ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setCodFnd(1, "");
        ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setNumQuo(1, new AfDecimal(0, 12, 5));
        ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setPc(1, new AfDecimal(0, 6, 3));
        ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setImpMovto(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setDtInvst(1, 0);
        ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setCodTari(1, "");
        ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setTpStat(1, "");
        ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setTpModInvst(1, "");
        ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setCodDiv(1, "");
        ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setDtCambioVlt(1, 0);
        ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setTpFnd(1, Types.SPACE_CHAR);
        ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setDsRiga(1, 0);
        ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setDsOperSql(1, Types.SPACE_CHAR);
        ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setDsVer(1, 0);
        ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setDsTsIniCptz(1, 0);
        ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setDsTsEndCptz(1, 0);
        ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setDsUtente(1, "");
        ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setDsStatoElab(1, Types.SPACE_CHAR);
        ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setDtInvstCalc(1, 0);
        ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setFlCalcInvto(1, Types.SPACE_CHAR);
        ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setImpGapEvent(1, new AfDecimal(0, 15, 3));
        ws.getAreaBusiness().getWrifAreaRichInvFnd().getWrifTabella().setFlSwmBp2s(1, Types.SPACE_CHAR);
    }

    public void initTabFnd() {
        ws.getAreaBusiness().getWl19AreaQuote().getWl19Tabella().setStatus(1, Types.SPACE_CHAR);
        ws.getAreaBusiness().getWl19AreaQuote().getWl19Tabella().setCodCompAnia(1, 0);
        ws.getAreaBusiness().getWl19AreaQuote().getWl19Tabella().setCodFnd(1, "");
        ws.getAreaBusiness().getWl19AreaQuote().getWl19Tabella().setDtQtz(1, 0);
        ws.getAreaBusiness().getWl19AreaQuote().getWl19Tabella().setValQuo(1, new AfDecimal(0, 12, 5));
        ws.getAreaBusiness().getWl19AreaQuote().getWl19Tabella().setValQuoManfee(1, new AfDecimal(0, 12, 5));
        ws.getAreaBusiness().getWl19AreaQuote().getWl19Tabella().setDsOperSql(1, Types.SPACE_CHAR);
        ws.getAreaBusiness().getWl19AreaQuote().getWl19Tabella().setDsVer(1, 0);
        ws.getAreaBusiness().getWl19AreaQuote().getWl19Tabella().setDsTsCptz(1, 0);
        ws.getAreaBusiness().getWl19AreaQuote().getWl19Tabella().setDsUtente(1, "");
        ws.getAreaBusiness().getWl19AreaQuote().getWl19Tabella().setDsStatoElab(1, Types.SPACE_CHAR);
        ws.getAreaBusiness().getWl19AreaQuote().getWl19Tabella().setTpFnd(1, Types.SPACE_CHAR);
        ws.getAreaBusiness().getWl19AreaQuote().getWl19Tabella().setDtRilevazioneNav(1, 0);
        ws.getAreaBusiness().getWl19AreaQuote().getWl19Tabella().setValQuoAcq(1, new AfDecimal(0, 12, 5));
        ws.getAreaBusiness().getWl19AreaQuote().getWl19Tabella().setFlNoNav(1, Types.SPACE_CHAR);
    }

    public void initIvvc0216AreaVariabiliP() {
        ws.getAreaWorkIvvc0216().getC216TabValP().setEleVariabiliMaxP(1, ((short)0));
        for (int idx0 = 1; idx0 <= C216TabValP.TAB_VARIABILI_P_MAXOCCURS; idx0++) {
            ws.getAreaWorkIvvc0216().getC216TabValP().setCodVariabileP(1, idx0, "");
            ws.getAreaWorkIvvc0216().getC216TabValP().setTpDatoP(1, idx0, Types.SPACE_CHAR);
            ws.getAreaWorkIvvc0216().getC216TabValP().setIvvc0216ValGenericoP(1, idx0, "");
        }
    }

    public void initIvvc0216VarAutOper() {
        ws.getAreaWorkIvvc0216().setC216EleCtrlAutOperMax(((short)0));
        for (int idx0 = 1; idx0 <= AreaInput.C216_CTRL_AUT_OPER_MAXOCCURS; idx0++) {
            ws.getAreaWorkIvvc0216().getC216CtrlAutOper(idx0).setCodErrore(0);
            ws.getAreaWorkIvvc0216().getC216CtrlAutOper(idx0).setCodLivAut(0);
            ws.getAreaWorkIvvc0216().getC216CtrlAutOper(idx0).setTpMotDeroga("");
            ws.getAreaWorkIvvc0216().getC216CtrlAutOper(idx0).setModVerifica("");
            ws.getAreaWorkIvvc0216().getC216CtrlAutOper(idx0).setCodiceCondizione("");
            ws.getAreaWorkIvvc0216().getC216CtrlAutOper(idx0).setProgressCondition(((short)0));
            ws.getAreaWorkIvvc0216().getC216CtrlAutOper(idx0).setRisultatoCondizione(Types.SPACE_CHAR);
            ws.getAreaWorkIvvc0216().getC216CtrlAutOper(idx0).setEleParamMax(((short)0));
            for (int idx1 = 1; idx1 <= Ivvv0212CtrlAutOper.TAB_PARAM_MAXOCCURS; idx1++) {
                ws.getAreaWorkIvvc0216().getC216CtrlAutOper(idx0).getTabParam(idx1).setCodParam("");
                ws.getAreaWorkIvvc0216().getC216CtrlAutOper(idx0).getTabParam(idx1).setValParam("");
            }
        }
    }

    public void initIvvc0216AreaVariabiliT() {
        ws.getAreaWorkIvvc0216().getC216TabValT().setEleVariabiliMaxT(1, ((short)0));
        for (int idx0 = 1; idx0 <= C216TabValT.TAB_VARIABILI_T_MAXOCCURS; idx0++) {
            ws.getAreaWorkIvvc0216().getC216TabValT().setCodVariabileT(1, idx0, "");
            ws.getAreaWorkIvvc0216().getC216TabValT().setTpDatoT(1, idx0, Types.SPACE_CHAR);
            ws.getAreaWorkIvvc0216().getC216TabValT().setIvvc0216ValGenericoT(1, idx0, "");
        }
    }

    public void initAreaVariabiliP() {
        ws.getIvvc0216().getC216TabValP().setEleVariabiliMaxP(1, ((short)0));
        for (int idx0 = 1; idx0 <= C216TabValP.TAB_VARIABILI_P_MAXOCCURS; idx0++) {
            ws.getIvvc0216().getC216TabValP().setCodVariabileP(1, idx0, "");
            ws.getIvvc0216().getC216TabValP().setTpDatoP(1, idx0, Types.SPACE_CHAR);
            ws.getIvvc0216().getC216TabValP().setIvvc0216ValGenericoP(1, idx0, "");
        }
    }

    public void initC216VarAutOper() {
        ws.getIvvc0216().setC216EleCtrlAutOperMax(((short)0));
        for (int idx0 = 1; idx0 <= Ivvc0216.C216_CTRL_AUT_OPER_MAXOCCURS; idx0++) {
            ws.getIvvc0216().getC216CtrlAutOper(idx0).setCodErrore(0);
            ws.getIvvc0216().getC216CtrlAutOper(idx0).setCodLivAut(0);
            ws.getIvvc0216().getC216CtrlAutOper(idx0).setTpMotDeroga("");
            ws.getIvvc0216().getC216CtrlAutOper(idx0).setModVerifica("");
            ws.getIvvc0216().getC216CtrlAutOper(idx0).setCodiceCondizione("");
            ws.getIvvc0216().getC216CtrlAutOper(idx0).setProgressCondition(((short)0));
            ws.getIvvc0216().getC216CtrlAutOper(idx0).setRisultatoCondizione(Types.SPACE_CHAR);
            ws.getIvvc0216().getC216CtrlAutOper(idx0).setEleParamMax(((short)0));
            for (int idx1 = 1; idx1 <= Ivvv0212CtrlAutOper.TAB_PARAM_MAXOCCURS; idx1++) {
                ws.getIvvc0216().getC216CtrlAutOper(idx0).getTabParam(idx1).setCodParam("");
                ws.getIvvc0216().getC216CtrlAutOper(idx0).getTabParam(idx1).setValParam("");
            }
        }
    }

    public void initAreaVariabiliT() {
        ws.getIvvc0216().getC216TabValT().setEleVariabiliMaxT(1, ((short)0));
        for (int idx0 = 1; idx0 <= C216TabValT.TAB_VARIABILI_T_MAXOCCURS; idx0++) {
            ws.getIvvc0216().getC216TabValT().setCodVariabileT(1, idx0, "");
            ws.getIvvc0216().getC216TabValT().setTpDatoT(1, idx0, Types.SPACE_CHAR);
            ws.getIvvc0216().getC216TabValT().setIvvc0216ValGenericoT(1, idx0, "");
        }
    }

    public void initComodoStatCaus() {
        ws.getComodoStatCaus().setOperLogStatBus("");
        for (int idx0 = 1; idx0 <= ComodoStatCaus.STAT_BUS_ELE_MAXOCCURS; idx0++) {
            ws.getComodoStatCaus().getStatBusEle(idx0).setComodoTpStatBus("");
        }
        ws.getComodoStatCaus().setOperLogCaus("");
        for (int idx0 = 1; idx0 <= ComodoStatCaus.TP_CAUS_ELE_MAXOCCURS; idx0++) {
            ws.getComodoStatCaus().getTpCausEle(idx0).setComodoTpCaus("");
        }
    }

    public void initLdbv1361() {
        ws.getLdbv1361().getLdbv1351LivelloOperazioni().setLdbv1351LivelloOperazioni(Types.SPACE_CHAR);
        ws.getLdbv1361().setLdbv1351OperLogStatBus("");
        for (int idx0 = 1; idx0 <= Ldbv1351StatBusTab.STAT_BUS_ELE_MAXOCCURS; idx0++) {
            ws.getLdbv1361().getLdbv1351StatBusTab().setLdbv1361TpStatBus(idx0, "");
        }
        ws.getLdbv1361().setLdbv1351OperLogCaus("");
        for (int idx0 = 1; idx0 <= Ldbv1351TpCausTab.ELE_MAXOCCURS; idx0++) {
            ws.getLdbv1361().getLdbv1351TpCausTab().setLdbv1361TpCaus(idx0, "");
        }
    }

    public void initVarFunzDiCalcR() {
        ws.getVarFunzDiCalcR().setAc5Idcomp(((short)0));
        ws.getVarFunzDiCalcR().setAc5CodprodLen(((short)0));
        ws.getVarFunzDiCalcR().setAc5Codprod("");
        ws.getVarFunzDiCalcR().setAc5CodtariLen(((short)0));
        ws.getVarFunzDiCalcR().setAc5Codtari("");
        ws.getVarFunzDiCalcR().setAc5Diniz(0);
        ws.getVarFunzDiCalcR().setAc5NomefunzLen(((short)0));
        ws.getVarFunzDiCalcR().setAc5Nomefunz("");
        ws.getVarFunzDiCalcR().setAc5Dend(0);
        ws.getVarFunzDiCalcR().getAc5Isprecalc().setAc5Isprecalc(((short)0));
        ws.getVarFunzDiCalcR().setAc5NomeprogrLen(((short)0));
        ws.getVarFunzDiCalcR().setAc5Nomeprogr("");
        ws.getVarFunzDiCalcR().setAc5ModcalcLen(((short)0));
        ws.getVarFunzDiCalcR().setAc5Modcalc("");
        ws.getVarFunzDiCalcR().setAc5GlovarlistLen(((short)0));
        ws.getVarFunzDiCalcR().setAc5Glovarlist("");
        ws.getVarFunzDiCalcR().setAc5StepElabLen(((short)0));
        ws.getVarFunzDiCalcR().setAc5StepElab("");
    }

    public void initAreaVarKo() {
        inputIvvs0211.getIvvc0211RestoDati().setEleMaxNotFound(((short)0));
        for (int idx0 = 1; idx0 <= Ivvc0211RestoDati.TAB_VAR_NOT_FOUND_MAXOCCURS; idx0++) {
            inputIvvs0211.getIvvc0211RestoDati().getTabVarNotFound(idx0).setFound("");
            inputIvvs0211.getIvvc0211RestoDati().getTabVarNotFound(idx0).setFoundSp(Types.SPACE_CHAR);
        }
        inputIvvs0211.getIvvc0211RestoDati().setEleMaxCalcKo(((short)0));
        for (int idx0 = 1; idx0 <= Ivvc0211RestoDati.TAB_CALCOLO_KO_MAXOCCURS; idx0++) {
            inputIvvs0211.getIvvc0211RestoDati().getTabCalcoloKo(idx0).setCalcoloKo("");
            inputIvvs0211.getIvvc0211RestoDati().getTabCalcoloKo(idx0).setNotFoundSp(Types.SPACE_CHAR);
        }
    }

    public void initStrPerformanceDbg() {
        ws.getIdsv8888().getStrPerformanceDbg().setFase("");
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("");
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("");
        ws.getIdsv8888().getStrPerformanceDbg().setTimestampFld("");
        ws.getIdsv8888().getStrPerformanceDbg().setStato("");
        ws.getIdsv8888().getStrPerformanceDbg().setModalitaEsecutiva(Types.SPACE_CHAR);
        ws.getIdsv8888().getStrPerformanceDbg().setUserName("");
    }
}
