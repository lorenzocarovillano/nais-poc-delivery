package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.program.GenericParam;
import com.bphx.ctu.af.core.program.StringParam;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.jdbc.SqlFunctions;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.ParamCompDao;
import it.accenture.jnais.copy.Idsv0001LogErrore;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.AreaIdsv0001;
import it.accenture.jnais.ws.enums.Idsv0003Sqlcode;
import it.accenture.jnais.ws.Idss0150Data;
import it.accenture.jnais.ws.redefines.PcoDtCont;
import static java.lang.Math.abs;

/**Original name: IDSS0150<br>
 * <pre>*****************************************************************
 * *                                                        ********
 * *               ROUTINE DI CALCOLO COMPETENZE            ********
 * *                                                        ********
 * *****************************************************************
 * AUTHOR.
 * DATE-WRITTEN.  MARZO 2008.
 * DATE-COMPILED.
 * REMARKS.
 * 00000000000000000000000000000000000000000000000000000000000000000
 *     PROGRAAMMA .... IDSS0150
 *     FUNZIONE ...... ROUTINE DI CALCOLO COMPETENZE
 * 00000000000000000000000000000000000000000000000000000000000000000</pre>*/
public class Idss0150 extends Program {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private ParamCompDao paramCompDao = new ParamCompDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Idss0150Data ws = new Idss0150Data();
    //Original name: AREA-IDSV0001
    private AreaIdsv0001 areaIdsv0001;

    //==== METHODS ====
    /**Original name: PROGRAM_IDSS0150_FIRST_SENTENCES<br>*/
    public long execute(AreaIdsv0001 areaIdsv0001) {
        this.areaIdsv0001 = areaIdsv0001;
        // COB_CODE: PERFORM A000-FASE-INIZIALE             THRU A000-EX
        a000FaseIniziale();
        // COB_CODE:      IF IDSV0003-SUCCESSFUL-RC
        //           *--- ESTRAE IL TIMESTAMP CORRENTE IN FORMATO DB --> X(26)
        //                   END-IF
        //                END-IF
        if (ws.getIdsv0003().getReturnCode().isSuccessfulRc()) {
            //--- ESTRAE IL TIMESTAMP CORRENTE IN FORMATO DB --> X(26)
            // COB_CODE: PERFORM A010-ESTRAI-TIMESTAMP-DB    THRU A010-EX
            a010EstraiTimestampDb();
            // COB_CODE:         IF IDSV0003-SUCCESSFUL-RC
            //           *--- ESTRAE LA DATA CORRENTE IN FORMATO DB --> X(10)
            //                      END-IF
            //                   END-IF
            if (ws.getIdsv0003().getReturnCode().isSuccessfulRc()) {
                //--- ESTRAE LA DATA CORRENTE IN FORMATO DB --> X(10)
                // COB_CODE: PERFORM A011-ESTRAI-DATE-DB      THRU A011-EX
                a011EstraiDateDb();
                // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
                //              END-IF
                //           END-IF
                if (ws.getIdsv0003().getReturnCode().isSuccessfulRc()) {
                    // COB_CODE: PERFORM A050-CALCOLA-CPTZ     THRU A050-EX
                    a050CalcolaCptz();
                    // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
                    //              SET IDSV0001-NO-TEMP-TABLE TO TRUE
                    //           END-IF
                    if (ws.getIdsv0003().getReturnCode().isSuccessfulRc()) {
                        // COB_CODE: IF IDSV0001-ID-TMPRY-DATA-SI
                        //                 THRU E000-ESTRAI-ID-TMPRY-DATA-EX
                        //           ELSE
                        //              END-IF
                        //           END-IF
                        if (this.areaIdsv0001.getAreaComune().getFlagIdTmpryData().isIdsv0001IdTmpryDataSi()) {
                            // COB_CODE: PERFORM E000-ESTRAI-ID-TMPRY-DATA
                            //              THRU E000-ESTRAI-ID-TMPRY-DATA-EX
                            e000EstraiIdTmpryData();
                        }
                        else if (!Functions.isNumber(this.areaIdsv0001.getAreaComune().getIdTemporaryData())) {
                            // COB_CODE: IF IDSV0001-ID-TEMPORARY-DATA NOT NUMERIC
                            //              MOVE ZEROES TO IDSV0001-ID-TEMPORARY-DATA
                            //           END-IF
                            // COB_CODE: MOVE ZEROES TO IDSV0001-ID-TEMPORARY-DATA
                            this.areaIdsv0001.getAreaComune().setIdTemporaryData(0);
                        }
                        // COB_CODE: SET IDSV0001-NO-TEMP-TABLE TO TRUE
                        this.areaIdsv0001.getAreaComune().getTemporaryTable().setIdsv0001NoTempTable();
                    }
                }
            }
        }
        // COB_CODE: PERFORM Z000-FASE-FINALE               THRU Z000-EX
        z000FaseFinale();
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Idss0150 getInstance() {
        return ((Idss0150)Programs.getInstance(Idss0150.class));
    }

    /**Original name: A000-FASE-INIZIALE<br>
	 * <pre>******************************************************</pre>*/
    private void a000FaseIniziale() {
        // COB_CODE: SET IDSV0001-ESITO-OK       TO TRUE
        areaIdsv0001.getEsito().setIdsv0001EsitoOk();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC  TO TRUE
        ws.getIdsv0003().getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL TO TRUE
        ws.getIdsv0003().getSqlcode().setSuccessfulSql();
        // COB_CODE: INITIALIZE WS-TS-SYSTEM-DB
        //                      WS-TS-SYSTEM
        //                      WS-DATE-SYSTEM-DB
        //                      WS-DIFFERENZA
        //                      WS-RISULTATO.
        ws.getIdsv0010().setWsTsSystemDb("");
        ws.getIdsv0010().setWsTsSystem(0);
        ws.getIdsv0010().setWsDateSystemDb("");
        ws.getIdsv0010().setWsDifferenza(0);
        ws.getIdsv0010().setWsRisultatoFormatted("00");
        // COB_CODE: PERFORM A001-CONTROLLA-INPUT THRU A001-EX.
        a001ControllaInput();
    }

    /**Original name: A001-CONTROLLA-INPUT<br>
	 * <pre>******************************************************</pre>*/
    private void a001ControllaInput() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'A001-CONTROLLA-INPUT' TO WK-LABEL.
        ws.setWkLabel("A001-CONTROLLA-INPUT");
        // COB_CODE: IF IDSV0001-COD-COMPAGNIA-ANIA NOT NUMERIC OR
        //              IDSV0001-COD-COMPAGNIA-ANIA = ZEROES
        //             END-STRING
        //           END-IF.
        if (!Functions.isNumber(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAniaFormatted()) || Characters.EQ_ZERO.test(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAniaFormatted())) {
            // COB_CODE: SET IDSV0003-COD-COMP-NOT-VALID TO TRUE
            ws.getIdsv0003().getReturnCode().setCodCompNotValid();
            // COB_CODE: MOVE WK-PGM       TO IDSV0001-COD-SERVIZIO-BE
            areaIdsv0001.getLogErrore().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL     TO IDSV0001-LABEL-ERR
            areaIdsv0001.getLogErrore().setLabelErr(ws.getWkLabel());
            // COB_CODE: STRING 'CODICE COMPAGNIA NON VALIDO'
            //                  ' : '
            //                  IDSV0001-COD-COMPAGNIA-ANIA
            //                  DELIMITED BY SIZE INTO
            //                  IDSV0001-DESC-ERRORE-ESTESA
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0001LogErrore.Len.DESC_ERRORE_ESTESA, "CODICE COMPAGNIA NON VALIDO", " : ", areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAniaAsString());
            areaIdsv0001.getLogErrore().setDescErroreEstesa(concatUtil.replaceInString(areaIdsv0001.getLogErrore().getDescErroreEstesaFormatted()));
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (ws.getIdsv0003().getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF NOT IDSV0001-DB-ISO AND
            //              NOT IDSV0001-DB-EUR
            //              END-STRING
            //           END-IF
            if (!areaIdsv0001.getAreaComune().getFormatoDataDb().isIdsv0001DbIso() && !areaIdsv0001.getAreaComune().getFormatoDataDb().isIdsv0001DbEur()) {
                // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED TO TRUE
                ws.getIdsv0003().getReturnCode().setFieldNotValued();
                // COB_CODE: MOVE WK-PGM       TO IDSV0001-COD-SERVIZIO-BE
                areaIdsv0001.getLogErrore().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE WK-LABEL     TO IDSV0001-LABEL-ERR
                areaIdsv0001.getLogErrore().setLabelErr(ws.getWkLabel());
                // COB_CODE: STRING 'FORMATO DB NON VALIDO'
                //                 DELIMITED BY SIZE INTO
                //                 IDSV0001-DESC-ERRORE-ESTESA
                //           END-STRING
                areaIdsv0001.getLogErrore().setDescErroreEstesa("FORMATO DB NON VALIDO");
            }
        }
    }

    /**Original name: A010-ESTRAI-TIMESTAMP-DB<br>
	 * <pre>******************************************************</pre>*/
    private void a010EstraiTimestampDb() {
        ConcatUtil concatUtil = null;
        // COB_CODE: EXEC SQL
        //                 VALUES CURRENT TIMESTAMP
        //                   INTO :WS-TS-SYSTEM-DB
        //           END-EXEC.
        ws.getIdsv0010().setWsTsSystemDb(SqlFunctions.currentTimestamp());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              MOVE WS-TIMESTAMP-N   TO WS-TS-SYSTEM
        //           ELSE
        //              END-STRING
        //           END-IF.
        if (ws.getIdsv0003().getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE WS-TS-SYSTEM-DB TO WS-TIMESTAMP-X
            ws.getIdsv0010().setWsTimestampX(ws.getIdsv0010().getWsTsSystemDb());
            // COB_CODE: PERFORM Z801-TS-X-TO-N   THRU Z801-EX
            z801TsXToN();
            // COB_CODE: MOVE WS-TIMESTAMP-N   TO WS-TS-SYSTEM
            ws.getIdsv0010().setWsTsSystemFormatted(ws.getIdsv0010().getWsTimestampNFormatted());
        }
        else {
            // COB_CODE: MOVE IDSV0003-SQLCODE TO WK-SQLCODE-ED
            ws.setWkSqlcodeEd(ws.getIdsv0003().getSqlcode().getSqlcode());
            // COB_CODE: STRING WK-PGM ' - ESTRAZIONE TIMESTAMP'
            //                        ' - '
            //                        'SQLCODE : '
            //                        WK-SQLCODE-ED
            //                        DELIMITED BY SIZE INTO
            //                        IDSV0001-DESC-ERRORE-ESTESA
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0001LogErrore.Len.DESC_ERRORE_ESTESA, ws.getWkPgmFormatted(), " - ESTRAZIONE TIMESTAMP", " - ", "SQLCODE : ", ws.getWkSqlcodeEdAsString());
            areaIdsv0001.getLogErrore().setDescErroreEstesa(concatUtil.replaceInString(areaIdsv0001.getLogErrore().getDescErroreEstesaFormatted()));
        }
    }

    /**Original name: A011-ESTRAI-DATE-DB<br>
	 * <pre>******************************************************</pre>*/
    private void a011EstraiDateDb() {
        ConcatUtil concatUtil = null;
        // COB_CODE: EXEC SQL
        //                 VALUES CURRENT DATE
        //                   INTO :WS-DATE-SYSTEM-DB
        //           END-EXEC.
        ws.getIdsv0010().setWsDateSystemDb(SqlFunctions.currentDate());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF NOT IDSV0003-SUCCESSFUL-RC
        //              END-STRING
        //           END-IF.
        if (!ws.getIdsv0003().getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE IDSV0003-SQLCODE TO WK-SQLCODE-ED
            ws.setWkSqlcodeEd(ws.getIdsv0003().getSqlcode().getSqlcode());
            // COB_CODE: STRING WK-PGM ' - ESTRAZIONE DATA DI SISTEMA'
            //                        ' - '
            //                        'SQLCODE : '
            //                        WK-SQLCODE-ED
            //                        DELIMITED BY SIZE INTO
            //                        IDSV0001-DESC-ERRORE-ESTESA
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0001LogErrore.Len.DESC_ERRORE_ESTESA, ws.getWkPgmFormatted(), " - ESTRAZIONE DATA DI SISTEMA", " - ", "SQLCODE : ", ws.getWkSqlcodeEdAsString());
            areaIdsv0001.getLogErrore().setDescErroreEstesa(concatUtil.replaceInString(areaIdsv0001.getLogErrore().getDescErroreEstesaFormatted()));
        }
    }

    /**Original name: A050-CALCOLA-CPTZ<br>
	 * <pre>**************************************************************</pre>*/
    private void a050CalcolaCptz() {
        ConcatUtil concatUtil = null;
        // COB_CODE: PERFORM A053-ESTRAI-DT-COMPETENZA THRU A053-EX
        a053EstraiDtCompetenza();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (ws.getIdsv0003().getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF PCO-DT-CONT-NULL = HIGH-VALUE
            //              END-IF
            //           ELSE
            //              PERFORM A070-GESTISCI-DIFF THRU A070-EX
            //           END-IF
            if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoDtCont().getPcoDtContNullFormatted())) {
                // COB_CODE: IF  IDSV0001-BATCH-INFR
                //           AND IDSV0001-KEY-BUSINESS3 NOT = SPACES
                //               END-IF
                //           ELSE
                //               PERFORM Z800-DT-X-TO-N  THRU Z800-EX
                //           END-IF
                if (areaIdsv0001.getAreaComune().getModalitaEsecutiva().isIdsv0001BatchInfr() && !Characters.EQ_SPACE.test(areaIdsv0001.getAreaComune().getKeyBusiness3())) {
                    // COB_CODE: IF IDSV0001-KEY-BUSINESS3(1:8) IS NUMERIC
                    //              END-IF
                    //           ELSE
                    //              END-STRING
                    //           END-IF
                    if (Functions.isNumber(areaIdsv0001.getAreaComune().getIdsv0001KeyBusiness3Formatted().substring((1) - 1, 8))) {
                        // COB_CODE: MOVE IDSV0001-KEY-BUSINESS3(1:8)
                        //             TO WK-DATA-AMG
                        ws.setWkDataAmg(areaIdsv0001.getAreaComune().getIdsv0001KeyBusiness3Formatted().substring((1) - 1, 8));
                        // COB_CODE: MOVE 4          TO WK-PARAM
                        ws.setWkParam(((short)4));
                        // COB_CODE: PERFORM A060-CTRL-DATA
                        //              THRU A060-EX
                        a060CtrlData();
                        // COB_CODE:                   IF WK-PARAM = 0  AND
                        //                                IDSV0003-SUCCESSFUL-RC
                        //           *
                        //                                PERFORM A070-GESTISCI-DIFF THRU A070-EX
                        //                             ELSE
                        //                                END-IF
                        //           *
                        //                             END-IF
                        if (ws.getWkParam() == 0 && ws.getIdsv0003().getReturnCode().isSuccessfulRc()) {
                            //
                            // COB_CODE: MOVE WK-DATA-AMG           TO WS-DATE-N
                            ws.getIdsv0010().setWsDateNFormatted(ws.getWkDataAmgFormatted());
                            // COB_CODE: PERFORM A070-GESTISCI-DIFF THRU A070-EX
                            a070GestisciDiff();
                        }
                        else {
                            // COB_CODE: IF  WK-PARAM  NOT = 0
                            //               END-STRING
                            //           END-IF
                            if (ws.getWkParam() != 0) {
                                // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED TO TRUE
                                ws.getIdsv0003().getReturnCode().setFieldNotValued();
                                // COB_CODE: MOVE WK-PGM      TO
                                //                            IDSV0001-COD-SERVIZIO-BE
                                areaIdsv0001.getLogErrore().setCodServizioBe(ws.getWkPgm());
                                // COB_CODE: MOVE WK-LABEL    TO IDSV0001-LABEL-ERR
                                areaIdsv0001.getLogErrore().setLabelErr(ws.getWkLabel());
                                // COB_CODE: MOVE 'LCCS0004'  TO IDSV0001-NOME-TABELLA
                                areaIdsv0001.getLogErrore().setNomeTabella("LCCS0004");
                                // COB_CODE: STRING 'DATA CONTABILE NON VALIDA IN '
                                //                  ' SK-PARAM: '
                                //                  WK-DATA-AMG
                                //                  DELIMITED BY SIZE INTO
                                //                  IDSV0001-DESC-ERRORE-ESTESA
                                //           END-STRING
                                concatUtil = ConcatUtil.buildString(Idsv0001LogErrore.Len.DESC_ERRORE_ESTESA, "DATA CONTABILE NON VALIDA IN ", " SK-PARAM: ", ws.getWkDataAmgFormatted());
                                areaIdsv0001.getLogErrore().setDescErroreEstesa(concatUtil.replaceInString(areaIdsv0001.getLogErrore().getDescErroreEstesaFormatted()));
                            }
                            //
                        }
                    }
                    else {
                        // COB_CODE: MOVE IDSV0001-KEY-BUSINESS3(1:8)
                        //             TO WK-DATA-AMG
                        ws.setWkDataAmg(areaIdsv0001.getAreaComune().getIdsv0001KeyBusiness3Formatted().substring((1) - 1, 8));
                        // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED TO TRUE
                        ws.getIdsv0003().getReturnCode().setFieldNotValued();
                        // COB_CODE: MOVE WK-PGM      TO IDSV0001-COD-SERVIZIO-BE
                        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getWkPgm());
                        // COB_CODE: MOVE WK-LABEL    TO IDSV0001-LABEL-ERR
                        areaIdsv0001.getLogErrore().setLabelErr(ws.getWkLabel());
                        // COB_CODE: MOVE 'CTRL_FORMALE'  TO IDSV0001-NOME-TABELLA
                        areaIdsv0001.getLogErrore().setNomeTabella("CTRL_FORMALE");
                        // COB_CODE: STRING 'DATA CONTABILE NON VALIDA IN SK-PARAM'
                        //                  ' : '
                        //                  WK-DATA-AMG
                        //                  DELIMITED BY SIZE INTO
                        //                  IDSV0001-DESC-ERRORE-ESTESA
                        //           END-STRING
                        concatUtil = ConcatUtil.buildString(Idsv0001LogErrore.Len.DESC_ERRORE_ESTESA, "DATA CONTABILE NON VALIDA IN SK-PARAM", " : ", ws.getWkDataAmgFormatted());
                        areaIdsv0001.getLogErrore().setDescErroreEstesa(concatUtil.replaceInString(areaIdsv0001.getLogErrore().getDescErroreEstesaFormatted()));
                    }
                }
                else {
                    // COB_CODE: MOVE ZEROES      TO WS-DIFFERENZA
                    ws.getIdsv0010().setWsDifferenza(0);
                    // COB_CODE: MOVE WS-DATE-SYSTEM-DB  TO WS-DATE-X
                    ws.getIdsv0010().setWsDateX(ws.getIdsv0010().getWsDateSystemDb());
                    // COB_CODE: PERFORM Z800-DT-X-TO-N  THRU Z800-EX
                    z800DtXToN();
                }
            }
            else {
                // COB_CODE: MOVE PCO-DT-CONT           TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(ws.getParamComp().getPcoDtCont().getPcoDtCont(), 8));
                // COB_CODE: PERFORM A070-GESTISCI-DIFF THRU A070-EX
                a070GestisciDiff();
            }
            // COB_CODE:         IF IDSV0003-SUCCESSFUL-RC
            //                                                  IDSV0001-DATA-COMPETENZA
            //           *--
            //           *-- Gestione Temporanea Fine
            //           *---------------------------------------------------------------
            //           *---
            //           *--- COMPONE IL TIMESTAMP PER LE SELEZIONI STORICHE
            //                   END-IF
            if (ws.getIdsv0003().getReturnCode().isSuccessfulRc()) {
                // COB_CODE: MOVE WS-TS-SYSTEM(1:8) TO WS-TS-DISPLAY(1:8)
                ws.getIdsv0010().setWsTsDisplaySubstring(ws.getIdsv0010().getWsTsSystemFormatted().substring((1) - 1, 8), 1, 8);
                // COB_CODE: MOVE QUARANTA          TO WS-TS-DISPLAY(9:2)
                ws.getIdsv0010().setWsTsDisplaySubstring(ws.getIdsv0010().getQuarantaFormatted(), 9, 2);
                // COB_CODE: MOVE WS-TS-SYSTEM(9:8) TO WS-TS-DISPLAY(11:8)
                ws.getIdsv0010().setWsTsDisplaySubstring(ws.getIdsv0010().getWsTsSystemFormatted().substring((9) - 1, 16), 11, 8);
                // COB_CODE: MOVE WS-TS-DISPLAY     TO IDSV0001-DATA-COMPETENZA
                areaIdsv0001.getAreaComune().setIdsv0001DataCompetenzaFormatted(ws.getIdsv0010().getWsTsDisplayFormatted());
                //--
                // COB_CODE: MOVE WS-DATE-N         TO WS-TS-DISPLAY(1:8)
                ws.getIdsv0010().setWsTsDisplaySubstring(ws.getIdsv0010().getWsDateNFormatted(), 1, 8);
                //---
                //--- COMPONE IL TIMESTAMP PER L'AGGIORNAMENTO STORICO
                //---
                // COB_CODE: IF WS-DIFFERENZA NOT = 0
                //              MOVE WS-STR-RISULTATO TO WS-TS-DISPLAY(9:2)
                //           END-IF
                if (ws.getIdsv0010().getWsDifferenza() != 0) {
                    // COB_CODE: COMPUTE WS-RISULTATO = QUARANTA + WS-DIFFERENZA
                    ws.getIdsv0010().setWsRisultato(Trunc.toShort(abs(ws.getIdsv0010().getQuaranta() + ws.getIdsv0010().getWsDifferenza()), 2));
                    // COB_CODE: MOVE WS-STR-RISULTATO TO WS-TS-DISPLAY(9:2)
                    ws.getIdsv0010().setWsTsDisplaySubstring(ws.getIdsv0010().getWsStrRisultatoFormatted(), 9, 2);
                }
                //---------------------------------------------------------------
                //-- Gestione Temporanea Inizio
                //-- da ELIMINARE in caso di Gestione delle Competenze Distinte
                //--
                // COB_CODE:            MOVE WS-TS-DISPLAY       TO IDSV0001-DATA-COMP-AGG-STOR
                //           *---------------------------------------------------------------
                //           *-- Gestione Temporanea Inizio
                //           *-- da ELIMINARE in caso di Gestione delle Competenze Distinte
                //           *--
                //                                                  IDSV0001-DATA-COMPETENZA
                areaIdsv0001.getAreaComune().setIdsv0001DataCompAggStorFormatted(ws.getIdsv0010().getWsTsDisplayFormatted());
                areaIdsv0001.getAreaComune().setIdsv0001DataCompetenzaFormatted(ws.getIdsv0010().getWsTsDisplayFormatted());
                //--
                //-- Gestione Temporanea Fine
                //---------------------------------------------------------------
                //---
                //--- COMPONE IL TIMESTAMP PER LE SELEZIONI STORICHE
            }
        }
    }

    /**Original name: A053-ESTRAI-DT-COMPETENZA<br>
	 * <pre>----------------------------------------------------------------*
	 *  ESTRAZIONE DATA COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a053EstraiDtCompetenza() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'A053-ESTRAI-DT-COMPETENZA'    TO WK-LABEL.
        ws.setWkLabel("A053-ESTRAI-DT-COMPETENZA");
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA TO PCO-COD-COMP-ANIA
        ws.getParamComp().setPcoCodCompAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: MOVE IDSV0001-FORMATO-DATA-DB    TO IDSV0003-FORMATO-DATA-DB
        ws.getIdsv0003().getFormatoDataDb().setFormatoDataDb(areaIdsv0001.getAreaComune().getFormatoDataDb().getFormatoDataDb());
        // COB_CODE: EXEC SQL
        //             SELECT
        //                  DT_CONT
        //                 ,FL_LIV_DEBUG
        //               INTO
        //                :PCO-DT-CONT-DB
        //                :IND-PCO-DT-CONT
        //               ,:PCO-FL-LIV-DEBUG
        //             FROM PARAM_COMP
        //             WHERE     COD_COMP_ANIA = :PCO-COD-COMP-ANIA
        //           END-EXEC.
        paramCompDao.selectByPcoCodCompAnia1(ws.getParamComp().getPcoCodCompAnia(), ws);
        // COB_CODE: MOVE SQLCODE       TO IDSV0003-SQLCODE
        ws.getIdsv0003().getSqlcode().setSqlcode(sqlca.getSqlcode());
        // COB_CODE:      EVALUATE TRUE
        //                    WHEN IDSV0003-SUCCESSFUL-SQL
        //           *-->     OPERAZIONE ESEGUITA CORRETTAMENTE
        //                         PERFORM D000-TRATTA-DEBUG     THRU D000-EX
        //                    WHEN IDSV0003-NOT-FOUND
        //           *--->    CAMPO $ NON TROVATO
        //                         MOVE 'PARAM_COMP' TO IDSV0001-NOME-TABELLA
        //                    WHEN OTHER
        //           *--->    ERRORE DI ACCESSO AL DB
        //                         MOVE 'PARAM_COMP' TO IDSV0001-NOME-TABELLA
        //                END-EVALUATE.
        switch (ws.getIdsv0003().getSqlcode().getSqlcode()) {

            case Idsv0003Sqlcode.SUCCESSFUL_SQL://-->     OPERAZIONE ESEGUITA CORRETTAMENTE
                // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
                z100SetColonneNull();
                // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
                z950ConvertiXToN();
                // COB_CODE: PERFORM D000-TRATTA-DEBUG     THRU D000-EX
                d000TrattaDebug();
                break;

            case Idsv0003Sqlcode.NOT_FOUND://--->    CAMPO $ NON TROVATO
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                ws.getIdsv0003().getReturnCode().setSqlError();
                // COB_CODE: MOVE WK-PGM       TO IDSV0001-COD-SERVIZIO-BE
                areaIdsv0001.getLogErrore().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE WK-LABEL     TO IDSV0001-LABEL-ERR
                areaIdsv0001.getLogErrore().setLabelErr(ws.getWkLabel());
                // COB_CODE: MOVE 'PARAM_COMP - OCCORRENZA NON TROVATA'
                //                  TO IDSV0001-DESC-ERRORE-ESTESA
                areaIdsv0001.getLogErrore().setDescErroreEstesa("PARAM_COMP - OCCORRENZA NON TROVATA");
                // COB_CODE: MOVE 'PARAM_COMP' TO IDSV0001-NOME-TABELLA
                areaIdsv0001.getLogErrore().setNomeTabella("PARAM_COMP");
                break;

            default://--->    ERRORE DI ACCESSO AL DB
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                ws.getIdsv0003().getReturnCode().setSqlError();
                // COB_CODE: MOVE WK-PGM       TO IDSV0001-COD-SERVIZIO-BE
                areaIdsv0001.getLogErrore().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE WK-LABEL     TO IDSV0001-LABEL-ERR
                areaIdsv0001.getLogErrore().setLabelErr(ws.getWkLabel());
                // COB_CODE: MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                ws.setWkSqlcode(ws.getIdsv0003().getSqlcode().getSqlcode());
                // COB_CODE: STRING 'PARAM_COMP'
                //                  ' - '
                //                  'SQLCODE : '
                //                  WK-SQLCODE
                //                  DELIMITED BY SIZE INTO
                //                  IDSV0001-DESC-ERRORE-ESTESA
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Idsv0001LogErrore.Len.DESC_ERRORE_ESTESA, "PARAM_COMP", " - ", "SQLCODE : ", ws.getWkSqlcodeAsString());
                areaIdsv0001.getLogErrore().setDescErroreEstesa(concatUtil.replaceInString(areaIdsv0001.getLogErrore().getDescErroreEstesaFormatted()));
                // COB_CODE: MOVE 'PARAM_COMP' TO IDSV0001-NOME-TABELLA
                areaIdsv0001.getLogErrore().setNomeTabella("PARAM_COMP");
                break;
        }
    }

    /**Original name: A060-CTRL-DATA<br>
	 * <pre>----------------------------------------------------------------*
	 *  CONTROLLO FORMALE DELLA DATA
	 * ----------------------------------------------------------------*</pre>*/
    private void a060CtrlData() {
        Lccs0004 lccs0004 = null;
        GenericParam wkParam = null;
        StringParam wkDataAmg = null;
        // COB_CODE: MOVE 'A060-CTRL-DATA'
        //             TO WK-LABEL.
        ws.setWkLabel("A060-CTRL-DATA");
        //
        //
        // COB_CODE:      CALL LCCS0004           USING WK-PARAM
        //                                              WK-DATA-AMG
        //           *
        //                ON EXCEPTION
        //           *
        //                     TO  IDSV0001-DESC-ERRORE-ESTESA
        //           *
        //                END-CALL.
        try {
            lccs0004 = Lccs0004.getInstance();
            wkParam = new GenericParam(MarshalByteExt.strToBuffer(ws.getWkParamFormatted(), Idss0150Data.Len.WK_PARAM));
            wkDataAmg = new StringParam(ws.getWkDataAmg(), Idss0150Data.Len.WK_DATA_AMG);
            lccs0004.run(wkParam, wkDataAmg);
            ws.setWkParamFromBuffer(wkParam.getByteData());
            ws.setWkDataAmg(wkDataAmg.getString());
        }
        catch (ProgramExecutionException __ex) {
            //
            // COB_CODE: SET IDSV0003-GENERIC-ERROR TO TRUE
            ws.getIdsv0003().getReturnCode().setGenericError();
            // COB_CODE: MOVE WK-PGM        TO IDSV0001-COD-SERVIZIO-BE
            areaIdsv0001.getLogErrore().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL      TO IDSV0001-LABEL-ERR
            areaIdsv0001.getLogErrore().setLabelErr(ws.getWkLabel());
            // COB_CODE: MOVE 'LCCS0004'    TO IDSV0001-NOME-TABELLA
            areaIdsv0001.getLogErrore().setNomeTabella("LCCS0004");
            // COB_CODE: MOVE 'ERRORE CHIAMATA MODULO LCCS0004'
            //             TO  IDSV0001-DESC-ERRORE-ESTESA
            areaIdsv0001.getLogErrore().setDescErroreEstesa("ERRORE CHIAMATA MODULO LCCS0004");
            //
        }
    }

    /**Original name: A070-GESTISCI-DIFF<br>
	 * <pre>----------------------------------------------------------------*
	 *  GSTIONE DIFFERENZA TRA DATE
	 * ----------------------------------------------------------------*</pre>*/
    private void a070GestisciDiff() {
        ConcatUtil concatUtil = null;
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX.
        //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IDSS0150.cbl:line=450, because the code is unreachable.
        // COB_CODE: PERFORM Y000-CALCOLA-DIFFERENZA THRU Y000-EX.
        y000CalcolaDifferenza();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (ws.getIdsv0003().getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF WS-DIFFERENZA > LIMITE-POSITIVO OR
            //              WS-DIFFERENZA < LIMITE-NEGATIVO
            //              END-STRING
            //           END-IF
            if (ws.getIdsv0010().getWsDifferenza() > ws.getLimitePositivo() || ws.getIdsv0010().getWsDifferenza() < ws.getLimiteNegativo()) {
                // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED TO TRUE
                ws.getIdsv0003().getReturnCode().setFieldNotValued();
                // COB_CODE: MOVE WK-PGM        TO IDSV0001-COD-SERVIZIO-BE
                areaIdsv0001.getLogErrore().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE WK-LABEL      TO IDSV0001-LABEL-ERR
                areaIdsv0001.getLogErrore().setLabelErr(ws.getWkLabel());
                // COB_CODE: MOVE 'PARAM_COMP'  TO IDSV0001-NOME-TABELLA
                areaIdsv0001.getLogErrore().setNomeTabella("PARAM_COMP");
                // COB_CODE: MOVE WS-DIFFERENZA TO DIFFERENZA-DISPLAY
                ws.setDifferenzaDisplay(ws.getIdsv0010().getWsDifferenza());
                // COB_CODE: STRING 'DIFFERENZA CONTABILE NON VALIDA'
                //                  ' : '
                //                  DIFFERENZA-DISPLAY
                //                  DELIMITED BY SIZE INTO
                //                  IDSV0001-DESC-ERRORE-ESTESA
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Idsv0001LogErrore.Len.DESC_ERRORE_ESTESA, "DIFFERENZA CONTABILE NON VALIDA", " : ", ws.getDifferenzaDisplayAsString());
                areaIdsv0001.getLogErrore().setDescErroreEstesa(concatUtil.replaceInString(areaIdsv0001.getLogErrore().getDescErroreEstesaFormatted()));
            }
        }
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>
	 * <pre>*********************************************************</pre>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (ws.getIdsv0003().getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            ws.getIdsv0003().getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            ws.getIdsv0003().getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (ws.getIdsv0003().getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (ws.getIdsv0003().getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (ws.getIdsv0003().getOperazione().isAggiornamentoStorico() || ws.getIdsv0003().getOperazione().isSelect() || ws.getIdsv0003().getOperazione().isFetchFirst() || ws.getIdsv0003().getOperazione().isFetchNext() || ws.getIdsv0003().getOperazione().isFetchFirstMultiple() || ws.getIdsv0003().getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    ws.getIdsv0003().getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                ws.getIdsv0003().getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: D000-TRATTA-DEBUG<br>
	 * <pre>*********************************************************</pre>*/
    private void d000TrattaDebug() {
        // COB_CODE: MOVE PCO-FL-LIV-DEBUG   TO IDSV0001-LIVELLO-DEBUG.
        areaIdsv0001.getAreaComune().getLivelloDebug().setIdsv0001LivelloDebug(TruncAbs.toShort(ws.getParamComp().getPcoFlLivDebug(), 1));
    }

    /**Original name: E000-ESTRAI-ID-TMPRY-DATA<br>
	 * <pre>*********************************************************</pre>*/
    private void e000EstraiIdTmpryData() {
        ConcatUtil concatUtil = null;
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_TEMPORARY_DATA
        //                INTO :IDSV0001-ID-TEMPORARY-DATA
        //           END-EXEC.
        //TODO: Implement: valuesStmt;
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF NOT IDSV0003-SUCCESSFUL-RC
        //              END-STRING
        //           END-IF.
        if (!ws.getIdsv0003().getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE IDSV0003-SQLCODE TO WK-SQLCODE-ED
            ws.setWkSqlcodeEd(ws.getIdsv0003().getSqlcode().getSqlcode());
            // COB_CODE: STRING WK-PGM ' - CALCOLA ID TEMPORARY DATA'
            //                        ' - '
            //                        'SQLCODE : '
            //                        WK-SQLCODE-ED
            //                        DELIMITED BY SIZE INTO
            //                        IDSV0001-DESC-ERRORE-ESTESA
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0001LogErrore.Len.DESC_ERRORE_ESTESA, ws.getWkPgmFormatted(), " - CALCOLA ID TEMPORARY DATA", " - ", "SQLCODE : ", ws.getWkSqlcodeEdAsString());
            areaIdsv0001.getLogErrore().setDescErroreEstesa(concatUtil.replaceInString(areaIdsv0001.getLogErrore().getDescErroreEstesaFormatted()));
        }
    }

    /**Original name: Y000-CALCOLA-DIFFERENZA<br>
	 * <pre>*********************************************************
	 * ---
	 * --- CALCOLA DIFFERENZA TRA DATA DI SISTEMA E
	 * --- DATA COMPETENZA DI CONTESTO
	 * ---</pre>*/
    private void y000CalcolaDifferenza() {
        ConcatUtil concatUtil = null;
        // COB_CODE: EXEC SQL
        //                VALUES
        //                       DAYS(:WS-DATE-SYSTEM-DB) -
        //                       DAYS(:WS-DATE-X)
        //                INTO   :WS-DIFFERENZA
        //           END-EXEC
        //TODO: Implement: valuesStmt;
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF NOT IDSV0003-SUCCESSFUL-RC
        //              END-STRING
        //           END-IF.
        if (!ws.getIdsv0003().getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE IDSV0003-SQLCODE TO WK-SQLCODE-ED
            ws.setWkSqlcodeEd(ws.getIdsv0003().getSqlcode().getSqlcode());
            // COB_CODE: STRING WK-PGM ' - CALCOLA DIFFERENZA TRA DATA DI SISTEMA'
            //                        ' E DATA COMPETENZA DI CONTESTO - '
            //                        'SQLCODE : '
            //                        WK-SQLCODE-ED
            //                        DELIMITED BY SIZE INTO
            //                        IDSV0001-DESC-ERRORE-ESTESA
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0001LogErrore.Len.DESC_ERRORE_ESTESA, ws.getWkPgmFormatted(), " - CALCOLA DIFFERENZA TRA DATA DI SISTEMA", " E DATA COMPETENZA DI CONTESTO - ", "SQLCODE : ", ws.getWkSqlcodeEdAsString());
            areaIdsv0001.getLogErrore().setDescErroreEstesa(concatUtil.replaceInString(areaIdsv0001.getLogErrore().getDescErroreEstesaFormatted()));
        }
    }

    /**Original name: Z000-FASE-FINALE<br>
	 * <pre>*********************************************************</pre>*/
    private void z000FaseFinale() {
        // COB_CODE: IF NOT IDSV0003-SUCCESSFUL-RC
        //                                            (IDSV0001-MAX-ELE-ERRORI)
        //           END-IF.
        if (!ws.getIdsv0003().getReturnCode().isSuccessfulRc()) {
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
            areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
            // COB_CODE: MOVE ERRORE-FATALE          TO IDSV0001-LIV-GRAVITA-BE
            //                                         (IDSV0001-MAX-ELE-ERRORI)
            areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBeFormatted(ws.getWkLivelloGravita().getErroreFataleFormatted());
            // COB_CODE: MOVE IDSV0001-DESC-ERRORE-ESTESA
            //                                       TO IDSV0001-DESC-ERRORE
            //                                         (IDSV0001-MAX-ELE-ERRORI)
            areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(areaIdsv0001.getLogErrore().getDescErroreEstesa());
        }
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>*********************************************************</pre>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        ws.getIdsv0003().getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-PCO-DT-CONT = -1
        //              MOVE HIGH-VALUES TO PCO-DT-CONT-NULL
        //           END-IF.
        if (ws.getIndParamComp().getDtCont() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCO-DT-CONT-NULL
            ws.getParamComp().getPcoDtCont().setPcoDtContNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcoDtCont.Len.PCO_DT_CONT_NULL));
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: IF IND-PCO-DT-CONT = 0
        //               MOVE WS-DATE-N      TO PCO-DT-CONT
        //           END-IF.
        if (ws.getIndParamComp().getDtCont() == 0) {
            // COB_CODE: MOVE PCO-DT-CONT-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamCompDb().getDtContDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCO-DT-CONT
            ws.getParamComp().getPcoDtCont().setPcoDtCont(ws.getIdsv0010().getWsDateN());
        }
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (ws.getIdsv0003().getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    /**Original name: Z801-TS-X-TO-N<br>*/
    private void z801TsXToN() {
        // COB_CODE: MOVE WS-TIMESTAMP-X(1:4)
        //                TO WS-STR-TIMESTAMP-N(1:4)
        ws.getIdsv0010().setWsStrTimestampNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrTimestampNFormatted(), ws.getIdsv0010().getWsTimestampXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-TIMESTAMP-X(6:2)
        //                TO WS-STR-TIMESTAMP-N(5:2)
        ws.getIdsv0010().setWsStrTimestampNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrTimestampNFormatted(), ws.getIdsv0010().getWsTimestampXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-TIMESTAMP-X(9:2)
        //                TO WS-STR-TIMESTAMP-N(7:2)
        ws.getIdsv0010().setWsStrTimestampNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrTimestampNFormatted(), ws.getIdsv0010().getWsTimestampXFormatted().substring((9) - 1, 10), 7, 2));
        // COB_CODE: MOVE WS-TIMESTAMP-X(12:2)
        //                TO WS-STR-TIMESTAMP-N(9:2)
        ws.getIdsv0010().setWsStrTimestampNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrTimestampNFormatted(), ws.getIdsv0010().getWsTimestampXFormatted().substring((12) - 1, 13), 9, 2));
        // COB_CODE: MOVE WS-TIMESTAMP-X(15:2)
        //                TO WS-STR-TIMESTAMP-N(11:2)
        ws.getIdsv0010().setWsStrTimestampNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrTimestampNFormatted(), ws.getIdsv0010().getWsTimestampXFormatted().substring((15) - 1, 16), 11, 2));
        // COB_CODE: MOVE WS-TIMESTAMP-X(18:2)
        //                TO WS-STR-TIMESTAMP-N(13:2)
        ws.getIdsv0010().setWsStrTimestampNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrTimestampNFormatted(), ws.getIdsv0010().getWsTimestampXFormatted().substring((18) - 1, 19), 13, 2));
        // COB_CODE: MOVE WS-TIMESTAMP-X(21:4)
        //                TO WS-STR-TIMESTAMP-N(15:4).
        ws.getIdsv0010().setWsStrTimestampNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrTimestampNFormatted(), ws.getIdsv0010().getWsTimestampXFormatted().substring((21) - 1, 24), 15, 4));
    }
}
