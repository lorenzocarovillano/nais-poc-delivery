package it.accenture.jnais;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.inspect.InspectMatcher;
import com.bphx.ctu.af.util.inspect.InspectPattern;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Idsv0001LogErrore;
import it.accenture.jnais.ws.AreaCallIwfs0050;
import it.accenture.jnais.ws.Iwfs0050Data;
import static java.lang.Math.abs;

/**Original name: IWFS0050<br>
 * <pre>*****************************************************************
 * *                                                        ********
 * *                                                        ********
 * *              CONVERSIONE STRINGA NUMERICA              ********
 * *****************************************************************
 * AUTHOR.
 * DATE-WRITTEN.  SETTEMBRE 2007.
 * DATE-COMPILED.
 * REMARKS.
 * °°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
 *     PROGRAAMMA .... IWFS0050
 *     FUNZIONE ......
 * °°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°</pre>*/
public class Iwfs0050 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Iwfs0050Data ws = new Iwfs0050Data();
    //Original name: AREA-CALL
    private AreaCallIwfs0050 areaCall;

    //==== METHODS ====
    /**Original name: PROGRAM_IWFS0050_FIRST_SENTENCES<br>*/
    public long execute(AreaCallIwfs0050 areaCall) {
        this.areaCall = areaCall;
        // COB_CODE: PERFORM A000-OPERAZ-INIZ                  THRU A000-EX
        a000OperazIniz();
        // COB_CODE: IF IWFO0051-ESITO-OK
        //              PERFORM B000-ELABORA                   THRU B000-EX
        //           END-IF.
        if (this.areaCall.getIwfo0051().getEsito().isOk()) {
            // COB_CODE: PERFORM B000-ELABORA                   THRU B000-EX
            b000Elabora();
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Iwfs0050 getInstance() {
        return ((Iwfs0050)Programs.getInstance(Iwfs0050.class));
    }

    /**Original name: A000-OPERAZ-INIZ<br>
	 * <pre>----------------------------------------------------------------*
	 *  OPERAZIONI INIZIALI                                            *
	 * ----------------------------------------------------------------*</pre>*/
    private void a000OperazIniz() {
        // COB_CODE: PERFORM A050-INITIALIZE        THRU A050-EX.
        a050Initialize();
        // COB_CODE: PERFORM A060-CTRL-INPUT        THRU A060-EX.
        a060CtrlInput();
    }

    /**Original name: A050-INITIALIZE<br>
	 * <pre>----------------------------------------------------------------*
	 *  INIZIALIZZA AREE DI WORKING                                    *
	 * ----------------------------------------------------------------*</pre>*/
    private void a050Initialize() {
        // COB_CODE: SET IWFO0051-ESITO-OK       TO TRUE
        areaCall.getIwfo0051().getEsito().setOk();
        // COB_CODE: SET INTERI                  TO TRUE
        ws.setInteri();
        // COB_CODE: SET DECIMALI                TO TRUE
        ws.setDecimali();
        // COB_CODE: MOVE 0                      TO IWFO0051-CAMPO-OUTPUT-DEFI
        //                                          WS-CAMPO-OUTPUT
        //                                          POSIZIONE-VIRGOLA
        //                                          WK-CONT-NEG.
        areaCall.getIwfo0051().setCampoOutputDefi(Trunc.toDecimal(0, 18, 5));
        ws.getWsCampoOutput().setWsCampoOutput(Trunc.toDecimal(0, 18, 5));
        ws.setPosizioneVirgola(((short)0));
        ws.setWkContNeg(((short)0));
        // COB_CODE: MOVE SPACES                 TO IWFO0051-LOG-ERRORE.
        areaCall.getIwfo0051().getLogErrore().initLogErroreSpaces();
    }

    /**Original name: A060-CTRL-INPUT<br>
	 * <pre>----------------------------------------------------------------*
	 *  CONTROLLI INPUT
	 * ----------------------------------------------------------------*</pre>*/
    private void a060CtrlInput() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'A060-CTRL-INPUT'    TO WK-LABEL.
        ws.setWkLabel("A060-CTRL-INPUT");
        // COB_CODE: IF IWFI0051-ARRAY-STRINGA-INPUT =
        //                  SPACES OR HIGH-VALUES OR LOW-VALUE
        //                                       TO IWFO0051-DESC-ERRORE-ESTESA
        //           END-IF.
        if (Characters.EQ_SPACE.test(areaCall.getIwfi0051ArrayStringaInputBytes()) || Characters.EQ_HIGH.test(areaCall.getIwfi0051ArrayStringaInputBytes()) || Characters.EQ_LOW.test(areaCall.getIwfi0051ArrayStringaInputBytes())) {
            // COB_CODE: SET IWFO0051-ESITO-KO    TO TRUE
            areaCall.getIwfo0051().getEsito().setKo();
            // COB_CODE: MOVE WK-PGM              TO IWFO0051-COD-SERVIZIO-BE
            areaCall.getIwfo0051().getLogErrore().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL            TO IWFO0051-LABEL-ERR
            areaCall.getIwfo0051().getLogErrore().setLabelErr(ws.getWkLabel());
            // COB_CODE: MOVE 'STRINGA DA CONVERTIRE NON VALIDA'
            //                                    TO IWFO0051-DESC-ERRORE-ESTESA
            areaCall.getIwfo0051().getLogErrore().setDescErroreEstesa("STRINGA DA CONVERTIRE NON VALIDA");
        }
        // COB_CODE: MOVE IWFI0051-ARRAY-STRINGA-INPUT
        //                                  TO WS-ARRAY-STRINGA-INPUT
        ws.setWsArrayStringaInput(areaCall.getIwfi0051ArrayStringaInputFormatted());
        // COB_CODE: INSPECT WS-ARRAY-STRINGA-INPUT
        //                                  REPLACING ALL SPACE      BY '0'.
        ws.setWsArrayStringaInput(ws.getWsArrayStringaInputFormatted().replace(Types.SPACE_STRING, "0"));
        // COB_CODE: INSPECT WS-ARRAY-STRINGA-INPUT
        //                                  REPLACING ALL LOW-VALUE  BY '0'.
        ws.setWsArrayStringaInput(ws.getWsArrayStringaInputFormatted().replace(String.valueOf(Types.LOW_CHAR_VAL), "0"));
        // COB_CODE: INSPECT WS-ARRAY-STRINGA-INPUT
        //                                  REPLACING ALL HIGH-VALUE BY '0'.
        ws.setWsArrayStringaInput(ws.getWsArrayStringaInputFormatted().replace(String.valueOf(Types.HIGH_CHAR_VAL), "0"));
        // COB_CODE: INSPECT WS-ARRAY-STRINGA-INPUT
        //                                  REPLACING ALL ','        BY '0'.
        ws.setWsArrayStringaInput(ws.getWsArrayStringaInputFormatted().replace(",", "0"));
        // COB_CODE: IF WS-ARRAY-STRINGA-INPUT NOT NUMERIC
        //              END-STRING
        //           END-IF.
        if (!Functions.isNumber(ws.getWsArrayStringaInputFormatted())) {
            // COB_CODE: SET IWFO0051-ESITO-KO    TO TRUE
            areaCall.getIwfo0051().getEsito().setKo();
            // COB_CODE: MOVE WK-PGM              TO IWFO0051-COD-SERVIZIO-BE
            areaCall.getIwfo0051().getLogErrore().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL            TO IWFO0051-LABEL-ERR
            areaCall.getIwfo0051().getLogErrore().setLabelErr(ws.getWkLabel());
            // COB_CODE: STRING 'STRINGA DA CONVERTIRE NON VALIDA '
            //                  ' : '
            //                  IWFI0051-ARRAY-STRINGA-INPUT
            //                  DELIMITED BY SIZE INTO
            //                  IWFO0051-DESC-ERRORE-ESTESA
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0001LogErrore.Len.DESC_ERRORE_ESTESA, "STRINGA DA CONVERTIRE NON VALIDA ", " : ", areaCall.getIwfi0051ArrayStringaInputFormatted());
            areaCall.getIwfo0051().getLogErrore().setDescErroreEstesa(concatUtil.replaceInString(areaCall.getIwfo0051().getLogErrore().getDescErroreEstesaFormatted()));
        }
    }

    /**Original name: B000-ELABORA<br>
	 * <pre>----------------------------------------------------------------*
	 *  ELABORAZIONE                                                   *
	 * ----------------------------------------------------------------*</pre>*/
    private void b000Elabora() {
        // COB_CODE: PERFORM B050-RICERCA-VIRGOLA THRU B050-EX.
        b050RicercaVirgola();
        // COB_CODE: PERFORM B100-TRATTA-STRINGA  THRU B100-EX.
        b100TrattaStringa();
        // COB_CODE: INSPECT WS-CAMPO-OUTPUT REPLACING ALL ' ' BY '0'.
        ws.getWsCampoOutput().setWsCampoOutputFormatted(ws.getWsCampoOutput().getWsCampoOutputFormatted().replace(" ", "0"));
        // COB_CODE: PERFORM B101-CONTROLLA-SEGNO THRU B101-EX.
        b101ControllaSegno();
    }

    /**Original name: B050-RICERCA-VIRGOLA<br>
	 * <pre>----------------------------------------------------------------*
	 *  RICERCA VIRGOLA
	 * ----------------------------------------------------------------*</pre>*/
    private void b050RicercaVirgola() {
        // COB_CODE: PERFORM VARYING IND-STRINGA   FROM 1 BY 1
        //                   UNTIL   IND-STRINGA       > WK-LIMITE-STRINGA
        //                   OR      POSIZIONE-VIRGOLA > 0
        //                   END-IF
        //           END-PERFORM.
        ws.setIndStringa(((short)1));
        while (!(ws.getIndStringa() > ws.getWkLimiteStringa() || ws.getPosizioneVirgola() > 0)) {
            // COB_CODE: IF IWFI0051-CAMPO-INPUT(IND-STRINGA) = VIRGOLA
            //              MOVE IND-STRINGA    TO POSIZIONE-VIRGOLA
            //           END-IF
            if (areaCall.getIwfi0051CampoInput(ws.getIndStringa()) == ws.getVirgola()) {
                // COB_CODE: MOVE IND-STRINGA    TO POSIZIONE-VIRGOLA
                ws.setPosizioneVirgola(ws.getIndStringa());
            }
            ws.setIndStringa(Trunc.toShort(ws.getIndStringa() + 1, 2));
        }
    }

    /**Original name: B100-TRATTA-STRINGA<br>
	 * <pre>----------------------------------------------------------------*
	 *  TRATTA STRINGA
	 * ----------------------------------------------------------------*</pre>*/
    private void b100TrattaStringa() {
        // COB_CODE: IF POSIZIONE-VIRGOLA = 0
        //              MOVE WK-LIMITE-STRINGA       TO WK-START-RICERCA
        //           ELSE
        //              PERFORM B300-TRATTA-DECIMALI THRU B300-EX
        //           END-IF.
        if (ws.getPosizioneVirgola() == 0) {
            // COB_CODE: MOVE WK-LIMITE-STRINGA       TO WK-START-RICERCA
            ws.setWkStartRicercaFormatted(ws.getWkLimiteStringaFormatted());
        }
        else {
            // COB_CODE: MOVE POSIZIONE-VIRGOLA       TO WK-START-RICERCA
            ws.setWkStartRicercaFormatted(ws.getPosizioneVirgolaFormatted());
            // COB_CODE: PERFORM B300-TRATTA-DECIMALI THRU B300-EX
            b300TrattaDecimali();
        }
        // COB_CODE: PERFORM B200-TRATTA-INTERI      THRU B200-EX.
        b200TrattaInteri();
    }

    /**Original name: B101-CONTROLLA-SEGNO<br>
	 * <pre>----------------------------------------------------------------*
	 *  TRATTA STRINGA
	 * ----------------------------------------------------------------*</pre>*/
    private void b101ControllaSegno() {
        InspectPattern iPattern = null;
        InspectMatcher iMatcher = null;
        // COB_CODE: INSPECT IWFI0051-ARRAY-STRINGA-INPUT
        //                    TALLYING WK-CONT-NEG FOR ALL '-'.
        iPattern = new InspectPattern("-").all();
        iMatcher = new InspectMatcher(areaCall.getIwfi0051ArrayStringaInputFormatted(), iPattern);
        ws.setWkContNeg(Trunc.toShort(ws.getWkContNeg() + iMatcher.count(), 4));
        // COB_CODE: IF WK-CONT-NEG > 0
        //              COMPUTE IWFO0051-CAMPO-OUTPUT-DEFI = WS-CAMPO-OUTPUT * -1
        //           ELSE
        //              MOVE WS-CAMPO-OUTPUT      TO IWFO0051-CAMPO-OUTPUT-DEFI
        //           END-IF.
        if (ws.getWkContNeg() > 0) {
            // COB_CODE: COMPUTE IWFO0051-CAMPO-OUTPUT-DEFI = WS-CAMPO-OUTPUT * -1
            areaCall.getIwfo0051().setCampoOutputDefi(Trunc.toDecimal(ws.getWsCampoOutput().getWsCampoOutput().multiply(-1), 18, 5));
        }
        else {
            // COB_CODE: MOVE WS-CAMPO-OUTPUT      TO IWFO0051-CAMPO-OUTPUT-DEFI
            areaCall.getIwfo0051().setCampoOutputDefi(Trunc.toDecimal(ws.getWsCampoOutput().getWsCampoOutput(), 18, 5));
        }
    }

    /**Original name: B200-TRATTA-INTERI<br>
	 * <pre>----------------------------------------------------------------*
	 *  TRATTA INTERI
	 * ----------------------------------------------------------------*</pre>*/
    private void b200TrattaInteri() {
        // COB_CODE: SUBTRACT 1                 FROM WK-START-RICERCA
        ws.setWkStartRicerca(Trunc.toShort(abs(ws.getWkStartRicerca() - 1), 2));
        // COB_CODE: PERFORM VARYING IND-STRINGA FROM WK-START-RICERCA BY -1
        //                   UNTIL   IND-STRINGA = 0
        //                   END-IF
        //           END-PERFORM.
        ws.setIndStringa(ws.getWkStartRicerca());
        while (!(ws.getIndStringa() == 0)) {
            // COB_CODE: IF IWFI0051-CAMPO-INPUT(IND-STRINGA)
            //                                     IS NUMERIC
            //              SUBTRACT 1             FROM IND-INTERI
            //           END-IF
            if (Functions.isNumber(areaCall.getIwfi0051CampoInput(ws.getIndStringa()))) {
                // COB_CODE: MOVE IWFI0051-CAMPO-INPUT(IND-STRINGA)
                //                TO ELE-STRINGA-OUTPUT(IND-INTERI)
                ws.getWsCampoOutput().setEleStringaOutput(ws.getIndInteri(), areaCall.getIwfi0051CampoInput(ws.getIndStringa()));
                // COB_CODE: SUBTRACT 1             FROM IND-INTERI
                ws.setIndInteri(Trunc.toShort(abs(ws.getIndInteri() - 1), 2));
            }
            ws.setIndStringa(Trunc.toShort(ws.getIndStringa() + (-1), 2));
        }
    }

    /**Original name: B300-TRATTA-DECIMALI<br>
	 * <pre>----------------------------------------------------------------*
	 *  TRATTA DECIMALI
	 * ----------------------------------------------------------------*</pre>*/
    private void b300TrattaDecimali() {
        // COB_CODE: ADD  1                      TO   WK-START-RICERCA
        ws.setWkStartRicerca(Trunc.toShort(1 + ws.getWkStartRicerca(), 2));
        // COB_CODE: PERFORM VARYING IND-STRINGA FROM WK-START-RICERCA BY 1
        //                   UNTIL   IND-STRINGA > WK-LIMITE-STRINGA
        //                   END-IF
        //            END-PERFORM.
        ws.setIndStringa(ws.getWkStartRicerca());
        while (!(ws.getIndStringa() > ws.getWkLimiteStringa())) {
            // COB_CODE: IF IWFI0051-CAMPO-INPUT(IND-STRINGA)
            //                                     IS NUMERIC
            //              ADD 1                  TO IND-DECIMALI
            //           END-IF
            if (Functions.isNumber(areaCall.getIwfi0051CampoInput(ws.getIndStringa()))) {
                // COB_CODE: MOVE IWFI0051-CAMPO-INPUT(IND-STRINGA)
                //                TO ELE-STRINGA-OUTPUT(IND-DECIMALI)
                ws.getWsCampoOutput().setEleStringaOutput(ws.getIndDecimali(), areaCall.getIwfi0051CampoInput(ws.getIndStringa()));
                // COB_CODE: ADD 1                  TO IND-DECIMALI
                ws.setIndDecimali(Trunc.toShort(1 + ws.getIndDecimali(), 2));
            }
            ws.setIndStringa(Trunc.toShort(ws.getIndStringa() + 1, 2));
        }
    }
}
