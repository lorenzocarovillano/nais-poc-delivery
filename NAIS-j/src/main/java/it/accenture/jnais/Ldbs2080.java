package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.QuotzFndUnitDao;
import it.accenture.jnais.commons.data.to.IQuotzFndUnit;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ldbs2080Data;
import it.accenture.jnais.ws.QuotzFndUnit;
import it.accenture.jnais.ws.redefines.L19DtRilevazioneNav;
import it.accenture.jnais.ws.redefines.L19ValQuo;
import it.accenture.jnais.ws.redefines.L19ValQuoAcq;
import it.accenture.jnais.ws.redefines.L19ValQuoManfee;

/**Original name: LDBS2080<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  27 APR 2010.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO AD HOC PER ACCESSO RISORSE DB        *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Ldbs2080 extends Program implements IQuotzFndUnit {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private QuotzFndUnitDao quotzFndUnitDao = new QuotzFndUnitDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Ldbs2080Data ws = new Ldbs2080Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: QUOTZ-FND-UNIT
    private QuotzFndUnit quotzFndUnit;

    //==== METHODS ====
    /**Original name: PROGRAM_LDBS2080_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, QuotzFndUnit quotzFndUnit) {
        this.idsv0003 = idsv0003;
        this.quotzFndUnit = quotzFndUnit;
        // COB_CODE: PERFORM A000-INIZIO              THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                        a200ElaboraWcEff();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                        b200ElaboraWcCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //               SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                        c200ElaboraWcNst();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Ldbs2080 getInstance() {
        return ((Ldbs2080)Programs.getInstance(Ldbs2080.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'LDBS2080'              TO   IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("LDBS2080");
        // COB_CODE: MOVE 'QUOTZ-FND-UNIT' TO   IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("QUOTZ-FND-UNIT");
        // COB_CODE: MOVE '00'                      TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                    TO   IDSV0003-SQLCODE
        //                                               IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                    TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                               IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-WC-EFF<br>*/
    private void a200ElaboraWcEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-WC-EFF          THRU A210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-WC-EFF          THRU A210-EX
            a210SelectWcEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
            a260OpenCursorWcEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
            a270CloseCursorWcEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
            a280FetchFirstWcEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
            a290FetchNextWcEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B200-ELABORA-WC-CPZ<br>*/
    private void b200ElaboraWcCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
            b210SelectWcCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
            b260OpenCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
            b270CloseCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
            b280FetchFirstWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
            b290FetchNextWcCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: C200-ELABORA-WC-NST<br>*/
    private void c200ElaboraWcNst() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM C210-SELECT-WC-NST          THRU C210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM C210-SELECT-WC-NST          THRU C210-EX
            c210SelectWcNst();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
            c260OpenCursorWcNst();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
            c270CloseCursorWcNst();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
            c280FetchFirstWcNst();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
            c290FetchNextWcNst();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A205-DECLARE-CURSOR-WC-EFF<br>
	 * <pre>----
	 * ----  gestione WC Effetto
	 * ----</pre>*/
    private void a205DeclareCursorWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A210-SELECT-WC-EFF<br>*/
    private void a210SelectWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A260-OPEN-CURSOR-WC-EFF<br>*/
    private void a260OpenCursorWcEff() {
        // COB_CODE: PERFORM A205-DECLARE-CURSOR-WC-EFF THRU A205-EX.
        a205DeclareCursorWcEff();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A270-CLOSE-CURSOR-WC-EFF<br>*/
    private void a270CloseCursorWcEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A280-FETCH-FIRST-WC-EFF<br>*/
    private void a280FetchFirstWcEff() {
        // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF    THRU A260-EX.
        a260OpenCursorWcEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
            a290FetchNextWcEff();
        }
    }

    /**Original name: A290-FETCH-NEXT-WC-EFF<br>*/
    private void a290FetchNextWcEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B205-DECLARE-CURSOR-WC-CPZ<br>
	 * <pre>----
	 * ----  gestione WC Competenza
	 * ----</pre>*/
    private void b205DeclareCursorWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B210-SELECT-WC-CPZ<br>*/
    private void b210SelectWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B260-OPEN-CURSOR-WC-CPZ<br>*/
    private void b260OpenCursorWcCpz() {
        // COB_CODE: PERFORM B205-DECLARE-CURSOR-WC-CPZ THRU B205-EX.
        b205DeclareCursorWcCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B270-CLOSE-CURSOR-WC-CPZ<br>*/
    private void b270CloseCursorWcCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B280-FETCH-FIRST-WC-CPZ<br>*/
    private void b280FetchFirstWcCpz() {
        // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ    THRU B260-EX.
        b260OpenCursorWcCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
            b290FetchNextWcCpz();
        }
    }

    /**Original name: B290-FETCH-NEXT-WC-CPZ<br>*/
    private void b290FetchNextWcCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C205-DECLARE-CURSOR-WC-NST<br>
	 * <pre>----
	 * ----  gestione WC Senza Storicità
	 * ----</pre>*/
    private void c205DeclareCursorWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //              DECLARE C-NST CURSOR FOR
        //              SELECT
        //                     COD_COMP_ANIA
        //                    ,COD_FND
        //                    ,DT_QTZ
        //                    ,VAL_QUO
        //                    ,VAL_QUO_MANFEE
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,TP_FND
        //                    ,DT_RILEVAZIONE_NAV
        //                    ,VAL_QUO_ACQ
        //                    ,FL_NO_NAV
        //              FROM QUOTZ_FND_UNIT
        //              WHERE  COD_FND   = :L19-COD-FND
        //                        AND DT_QTZ   <= :L19-DT-QTZ-DB
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //              ORDER BY DT_QTZ DESC
        //              FETCH FIRST ROW ONLY
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: C210-SELECT-WC-NST<br>*/
    private void c210SelectWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                     COD_COMP_ANIA
        //                    ,COD_FND
        //                    ,DT_QTZ
        //                    ,VAL_QUO
        //                    ,VAL_QUO_MANFEE
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,TP_FND
        //                    ,DT_RILEVAZIONE_NAV
        //                    ,VAL_QUO_ACQ
        //                    ,FL_NO_NAV
        //             INTO
        //                :L19-COD-COMP-ANIA
        //               ,:L19-COD-FND
        //               ,:L19-DT-QTZ-DB
        //               ,:L19-VAL-QUO
        //                :IND-L19-VAL-QUO
        //               ,:L19-VAL-QUO-MANFEE
        //                :IND-L19-VAL-QUO-MANFEE
        //               ,:L19-DS-OPER-SQL
        //               ,:L19-DS-VER
        //               ,:L19-DS-TS-CPTZ
        //               ,:L19-DS-UTENTE
        //               ,:L19-DS-STATO-ELAB
        //               ,:L19-TP-FND
        //               ,:L19-DT-RILEVAZIONE-NAV-DB
        //                :IND-L19-DT-RILEVAZIONE-NAV
        //               ,:L19-VAL-QUO-ACQ
        //                :IND-L19-VAL-QUO-ACQ
        //               ,:L19-FL-NO-NAV
        //                :IND-L19-FL-NO-NAV
        //             FROM QUOTZ_FND_UNIT
        //             WHERE  COD_FND   = :L19-COD-FND
        //                    AND DT_QTZ   <= :L19-DT-QTZ-DB
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //              ORDER BY DT_QTZ DESC
        //              FETCH FIRST ROW ONLY
        //           END-EXEC.
        quotzFndUnitDao.selectRec1(quotzFndUnit.getL19CodFnd(), ws.getIdbvl193().getL19DtQtzDb(), idsv0003.getCodiceCompagniaAnia(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: C260-OPEN-CURSOR-WC-NST<br>*/
    private void c260OpenCursorWcNst() {
        // COB_CODE: PERFORM C205-DECLARE-CURSOR-WC-NST THRU C205-EX.
        c205DeclareCursorWcNst();
        // COB_CODE: EXEC SQL
        //                OPEN C-NST
        //           END-EXEC.
        quotzFndUnitDao.openCNst2(quotzFndUnit.getL19CodFnd(), ws.getIdbvl193().getL19DtQtzDb(), idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: C270-CLOSE-CURSOR-WC-NST<br>*/
    private void c270CloseCursorWcNst() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-NST
        //           END-EXEC.
        quotzFndUnitDao.closeCNst2();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: C280-FETCH-FIRST-WC-NST<br>*/
    private void c280FetchFirstWcNst() {
        // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST    THRU C260-EX.
        c260OpenCursorWcNst();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
            c290FetchNextWcNst();
        }
    }

    /**Original name: C290-FETCH-NEXT-WC-NST<br>*/
    private void c290FetchNextWcNst() {
        // COB_CODE: EXEC SQL
        //                FETCH C-NST
        //           INTO
        //                :L19-COD-COMP-ANIA
        //               ,:L19-COD-FND
        //               ,:L19-DT-QTZ-DB
        //               ,:L19-VAL-QUO
        //                :IND-L19-VAL-QUO
        //               ,:L19-VAL-QUO-MANFEE
        //                :IND-L19-VAL-QUO-MANFEE
        //               ,:L19-DS-OPER-SQL
        //               ,:L19-DS-VER
        //               ,:L19-DS-TS-CPTZ
        //               ,:L19-DS-UTENTE
        //               ,:L19-DS-STATO-ELAB
        //               ,:L19-TP-FND
        //               ,:L19-DT-RILEVAZIONE-NAV-DB
        //                :IND-L19-DT-RILEVAZIONE-NAV
        //               ,:L19-VAL-QUO-ACQ
        //                :IND-L19-VAL-QUO-ACQ
        //               ,:L19-FL-NO-NAV
        //                :IND-L19-FL-NO-NAV
        //           END-EXEC.
        quotzFndUnitDao.fetchCNst2(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM C270-CLOSE-CURSOR-WC-NST THRU C270-EX
            c270CloseCursorWcNst();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>----
	 * ----  utilità comuni a tutti i livelli operazione
	 * ----</pre>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-L19-VAL-QUO = -1
        //              MOVE HIGH-VALUES TO L19-VAL-QUO-NULL
        //           END-IF
        if (ws.getIndQuotzFndUnit().getValQuo() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO L19-VAL-QUO-NULL
            quotzFndUnit.getL19ValQuo().setL19ValQuoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, L19ValQuo.Len.L19_VAL_QUO_NULL));
        }
        // COB_CODE: IF IND-L19-VAL-QUO-MANFEE = -1
        //              MOVE HIGH-VALUES TO L19-VAL-QUO-MANFEE-NULL
        //           END-IF
        if (ws.getIndQuotzFndUnit().getValQuoManfee() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO L19-VAL-QUO-MANFEE-NULL
            quotzFndUnit.getL19ValQuoManfee().setL19ValQuoManfeeNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, L19ValQuoManfee.Len.L19_VAL_QUO_MANFEE_NULL));
        }
        // COB_CODE: IF IND-L19-DT-RILEVAZIONE-NAV = -1
        //              MOVE HIGH-VALUES TO L19-DT-RILEVAZIONE-NAV-NULL
        //           END-IF
        if (ws.getIndQuotzFndUnit().getDtRilevazioneNav() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO L19-DT-RILEVAZIONE-NAV-NULL
            quotzFndUnit.getL19DtRilevazioneNav().setL19DtRilevazioneNavNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, L19DtRilevazioneNav.Len.L19_DT_RILEVAZIONE_NAV_NULL));
        }
        // COB_CODE: IF IND-L19-VAL-QUO-ACQ = -1
        //              MOVE HIGH-VALUES TO L19-VAL-QUO-ACQ-NULL
        //           END-IF.
        if (ws.getIndQuotzFndUnit().getValQuoAcq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO L19-VAL-QUO-ACQ-NULL
            quotzFndUnit.getL19ValQuoAcq().setL19ValQuoAcqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, L19ValQuoAcq.Len.L19_VAL_QUO_ACQ_NULL));
        }
        // COB_CODE: IF IND-L19-FL-NO-NAV = -1
        //              MOVE HIGH-VALUES TO L19-FL-NO-NAV-NULL
        //           END-IF.
        if (ws.getIndQuotzFndUnit().getFlNoNav() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO L19-FL-NO-NAV-NULL
            quotzFndUnit.setL19FlNoNav(Types.HIGH_CHAR_VAL);
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE L19-DT-QTZ-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getIdbvl193().getL19DtQtzDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO L19-DT-QTZ
        quotzFndUnit.setL19DtQtz(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-L19-DT-RILEVAZIONE-NAV = 0
        //               MOVE WS-DATE-N      TO L19-DT-RILEVAZIONE-NAV
        //           END-IF.
        if (ws.getIndQuotzFndUnit().getDtRilevazioneNav() == 0) {
            // COB_CODE: MOVE L19-DT-RILEVAZIONE-NAV-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getIdbvl193().getL19DtRilevazioneNavDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO L19-DT-RILEVAZIONE-NAV
            quotzFndUnit.getL19DtRilevazioneNav().setL19DtRilevazioneNav(ws.getIdsv0010().getWsDateN());
        }
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z970-CODICE-ADHOC-PRE<br>
	 * <pre>----
	 * ----  prevede statements AD HOC PRE Query
	 * ----</pre>*/
    private void z970CodiceAdhocPre() {
        // COB_CODE: IF L19-DT-QTZ IS NUMERIC
        //            END-IF
        //           END-IF.
        if (Functions.isNumber(quotzFndUnit.getL19DtQtz())) {
            // COB_CODE: IF L19-DT-QTZ GREATER ZEROES
            //              MOVE WS-DATE-X  TO L19-DT-QTZ-DB
            //           END-IF
            if (quotzFndUnit.getL19DtQtz() > 0) {
                // COB_CODE: MOVE L19-DT-QTZ TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(quotzFndUnit.getL19DtQtz(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X  TO L19-DT-QTZ-DB
                ws.getIdbvl193().setL19DtQtzDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: Z980-CODICE-ADHOC-POST<br>
	 * <pre>----
	 * ----  prevede statements AD HOC POST Query
	 * ----</pre>*/
    private void z980CodiceAdhocPost() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public int getCodCompAnia() {
        return quotzFndUnit.getL19CodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.quotzFndUnit.setL19CodCompAnia(codCompAnia);
    }

    @Override
    public String getCodFnd() {
        return quotzFndUnit.getL19CodFnd();
    }

    @Override
    public void setCodFnd(String codFnd) {
        this.quotzFndUnit.setL19CodFnd(codFnd);
    }

    @Override
    public char getDsOperSql() {
        return quotzFndUnit.getL19DsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.quotzFndUnit.setL19DsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return quotzFndUnit.getL19DsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.quotzFndUnit.setL19DsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsCptz() {
        return quotzFndUnit.getL19DsTsCptz();
    }

    @Override
    public void setDsTsCptz(long dsTsCptz) {
        this.quotzFndUnit.setL19DsTsCptz(dsTsCptz);
    }

    @Override
    public String getDsUtente() {
        return quotzFndUnit.getL19DsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.quotzFndUnit.setL19DsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return quotzFndUnit.getL19DsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.quotzFndUnit.setL19DsVer(dsVer);
    }

    @Override
    public String getDtQtzDb() {
        return ws.getIdbvl193().getL19DtQtzDb();
    }

    @Override
    public void setDtQtzDb(String dtQtzDb) {
        this.ws.getIdbvl193().setL19DtQtzDb(dtQtzDb);
    }

    @Override
    public String getDtRilevazioneNavDb() {
        return ws.getIdbvl193().getL19DtRilevazioneNavDb();
    }

    @Override
    public void setDtRilevazioneNavDb(String dtRilevazioneNavDb) {
        this.ws.getIdbvl193().setL19DtRilevazioneNavDb(dtRilevazioneNavDb);
    }

    @Override
    public String getDtRilevazioneNavDbObj() {
        if (ws.getIndQuotzFndUnit().getDtRilevazioneNav() >= 0) {
            return getDtRilevazioneNavDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtRilevazioneNavDbObj(String dtRilevazioneNavDbObj) {
        if (dtRilevazioneNavDbObj != null) {
            setDtRilevazioneNavDb(dtRilevazioneNavDbObj);
            ws.getIndQuotzFndUnit().setDtRilevazioneNav(((short)0));
        }
        else {
            ws.getIndQuotzFndUnit().setDtRilevazioneNav(((short)-1));
        }
    }

    @Override
    public char getFlNoNav() {
        return quotzFndUnit.getL19FlNoNav();
    }

    @Override
    public void setFlNoNav(char flNoNav) {
        this.quotzFndUnit.setL19FlNoNav(flNoNav);
    }

    @Override
    public Character getFlNoNavObj() {
        if (ws.getIndQuotzFndUnit().getFlNoNav() >= 0) {
            return ((Character)getFlNoNav());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlNoNavObj(Character flNoNavObj) {
        if (flNoNavObj != null) {
            setFlNoNav(((char)flNoNavObj));
            ws.getIndQuotzFndUnit().setFlNoNav(((short)0));
        }
        else {
            ws.getIndQuotzFndUnit().setFlNoNav(((short)-1));
        }
    }

    @Override
    public char getTpFnd() {
        return quotzFndUnit.getL19TpFnd();
    }

    @Override
    public void setTpFnd(char tpFnd) {
        this.quotzFndUnit.setL19TpFnd(tpFnd);
    }

    @Override
    public AfDecimal getValQuo() {
        return quotzFndUnit.getL19ValQuo().getL19ValQuo();
    }

    @Override
    public AfDecimal getValQuoAcq() {
        return quotzFndUnit.getL19ValQuoAcq().getL19ValQuoAcq();
    }

    @Override
    public void setValQuoAcq(AfDecimal valQuoAcq) {
        this.quotzFndUnit.getL19ValQuoAcq().setL19ValQuoAcq(valQuoAcq.copy());
    }

    @Override
    public AfDecimal getValQuoAcqObj() {
        if (ws.getIndQuotzFndUnit().getValQuoAcq() >= 0) {
            return getValQuoAcq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setValQuoAcqObj(AfDecimal valQuoAcqObj) {
        if (valQuoAcqObj != null) {
            setValQuoAcq(new AfDecimal(valQuoAcqObj, 12, 5));
            ws.getIndQuotzFndUnit().setValQuoAcq(((short)0));
        }
        else {
            ws.getIndQuotzFndUnit().setValQuoAcq(((short)-1));
        }
    }

    @Override
    public void setValQuo(AfDecimal valQuo) {
        this.quotzFndUnit.getL19ValQuo().setL19ValQuo(valQuo.copy());
    }

    @Override
    public AfDecimal getValQuoManfee() {
        return quotzFndUnit.getL19ValQuoManfee().getL19ValQuoManfee();
    }

    @Override
    public void setValQuoManfee(AfDecimal valQuoManfee) {
        this.quotzFndUnit.getL19ValQuoManfee().setL19ValQuoManfee(valQuoManfee.copy());
    }

    @Override
    public AfDecimal getValQuoManfeeObj() {
        if (ws.getIndQuotzFndUnit().getValQuoManfee() >= 0) {
            return getValQuoManfee();
        }
        else {
            return null;
        }
    }

    @Override
    public void setValQuoManfeeObj(AfDecimal valQuoManfeeObj) {
        if (valQuoManfeeObj != null) {
            setValQuoManfee(new AfDecimal(valQuoManfeeObj, 12, 5));
            ws.getIndQuotzFndUnit().setValQuoManfee(((short)0));
        }
        else {
            ws.getIndQuotzFndUnit().setValQuoManfee(((short)-1));
        }
    }

    @Override
    public AfDecimal getValQuoObj() {
        if (ws.getIndQuotzFndUnit().getValQuo() >= 0) {
            return getValQuo();
        }
        else {
            return null;
        }
    }

    @Override
    public void setValQuoObj(AfDecimal valQuoObj) {
        if (valQuoObj != null) {
            setValQuo(new AfDecimal(valQuoObj, 12, 5));
            ws.getIndQuotzFndUnit().setValQuo(((short)0));
        }
        else {
            ws.getIndQuotzFndUnit().setValQuo(((short)-1));
        }
    }
}
